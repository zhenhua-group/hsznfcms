﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;

namespace TG.BLL
{
    public class ProjectAuditListBP
    {
        private ProjectAuditListDA dal = new ProjectAuditListDA();

        /// <summary>
        /// 查询审核列表
        /// </summary>
        /// <param name="projectAuditListViewSysNo"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        public List<ProjectAuditListViewDataEntity> GetProjectAuditListViewEntity(List<SysMsgEntity> sysMsgList, string isAudit, int userSysNo)
        {
            List<ProjectAuditListViewDataEntity> pickList = new List<ProjectAuditListViewDataEntity>();

            sysMsgList.ForEach((msg) =>
            {
                pickList.Add(dal.GetProjectAuditListViewEntity(int.Parse(msg.ReferenceSysNo)));
            });

            List<ProjectAuditListViewDataEntity> resultList = null;

            if (isAudit == "A")
            {
                resultList = (from project in pickList where (string.IsNullOrEmpty(project.AuditUser)) == true select project).ToList<ProjectAuditListViewDataEntity>();
            }
            else
            {
                resultList = pickList.Where(delegate(ProjectAuditListViewDataEntity project)
                {
                    string[] userArray = null;
                    if (project.AuditUser != null)
                        userArray = project.AuditUser.Split(',');
                    if (!string.IsNullOrEmpty(project.AuditUser) && userArray.Contains(userSysNo.ToString()))
                        return true;
                    else
                        return false;
                }).ToList<ProjectAuditListViewDataEntity>();
            }

            return resultList;
        }
        public int GetCountOfProjectAuditListView(ResultEntityList<SysMsgEntity> msgList, string isAudit, int userSysNo)
        {
            List<ProjectAuditListViewDataEntity> pickList = new List<ProjectAuditListViewDataEntity>();

            msgList.Body.ForEach((msg) =>
            {
                pickList.Add(dal.GetProjectAuditListViewEntity(int.Parse(msg.ReferenceSysNo)));
            });

            if (isAudit == "A")
            {
                return (from project in pickList where (string.IsNullOrEmpty(project.AuditUser)) == true select project).Count();
            }
            else
            {
                int count = pickList.Where(delegate(ProjectAuditListViewDataEntity project)
                {
                    string[] userArray = null;
                    if (project.AuditUser != null)
                        userArray = project.AuditUser.Split(',');
                    if (!string.IsNullOrEmpty(project.AuditUser) && userArray.Contains(userSysNo.ToString()))
                        return true;
                    else
                        return false;
                }).Count();

                return count;
            }
        }
    }
}
