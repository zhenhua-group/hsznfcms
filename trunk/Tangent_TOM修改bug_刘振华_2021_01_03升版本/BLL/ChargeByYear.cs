﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public class ChargeByYear
    {
        TG.DAL.ChargeByYear TestDa = new DAL.ChargeByYear();
        //2013  240
        public List<TG.Model.ChargeByYear> ListGet(string yearnum, string unitid)
        {
            List<TG.Model.ChargeByYear> list = new List<Model.ChargeByYear>();
            if (unitid == "-1")
            {
                List<TG.Model.ChargeByYear> listFrist = TestDa.ListGet(yearnum, "WHERE a.unit_ParentID=238");
                TG.Model.ChargeByYear modelfrist = GetHeji(listFrist, "合计");
                listFrist.Add(modelfrist);
                List<TG.Model.ChargeByYear> listSecond = TestDa.ListGet(yearnum, "WHERE a.unit_ParentID=254");
                TG.Model.ChargeByYear modelsecond = GetHeji(listSecond, "施工合计");
                listSecond.Add(modelsecond);
                TG.Model.ChargeByYear modelAll = Getzongji(modelfrist, modelsecond);
                list.AddRange(listFrist);
                list.AddRange(listSecond);
                list.Add(modelAll);
            }
            else
            {
                unitid = "WHERE a.unit_ID=" + unitid;
                list = TestDa.ListGet(yearnum, unitid);
            }
            return list;
        }
        /// <summary>
        /// 求合计
        /// </summary>
        /// <param name="list"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public TG.Model.ChargeByYear GetHeji(List<TG.Model.ChargeByYear> list, string str)
        {
            TG.Model.ChargeByYear model = new Model.ChargeByYear();
            model.unit_Name = str;
            foreach (TG.Model.ChargeByYear item in list)
            {
                model.UnitAllot += Convert.ToInt32(item.UnitAllot);
                model.Count += Convert.ToDecimal(item.Count.ToString().Trim());
                model.BuildArea += Convert.ToDecimal(item.BuildArea.ToString().Trim());
                model.HeTonge += Convert.ToDecimal(item.HeTonge.ToString().Trim());
                model.YiWanCHeTonge += Convert.ToDecimal(item.YiWanCHeTonge.ToString().Trim());
                model.QianKuan += Convert.ToDecimal(item.QianKuan.ToString().Trim());
                model.NDShouFei += Convert.ToDecimal(item.NDShouFei.ToString().Trim());
            }
            return model;
        }
        /// <summary>
        /// 总计
        /// </summary>
        /// <param name="listfrist"></param>
        /// <param name="listSecond"></param>
        /// <returns></returns>
        public TG.Model.ChargeByYear Getzongji(TG.Model.ChargeByYear listfrist, TG.Model.ChargeByYear listSecond)
        {
            TG.Model.ChargeByYear Zongji = new Model.ChargeByYear();
            Zongji.unit_Name = "总计";
            Zongji.UnitAllot = listfrist.UnitAllot + listSecond.UnitAllot;
            Zongji.Count = listfrist.Count + listSecond.Count;
            Zongji.BuildArea = listfrist.BuildArea + listSecond.BuildArea;
            Zongji.HeTonge = listfrist.HeTonge + listSecond.HeTonge;
            Zongji.YiWanCHeTonge = listfrist.YiWanCHeTonge + listSecond.YiWanCHeTonge;
            Zongji.QianKuan = listfrist.QianKuan + listSecond.QianKuan;
            Zongji.NDShouFei = listfrist.NDShouFei + listSecond.NDShouFei;
            return Zongji;
        }
    }
}
