﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.BLL
{
    public class ProMSFeiZhuAndWai
    {
        TG.DAL.tg_unit tgunit = new DAL.tg_unit();
        TG.BLL.cm_UnitAllot cmunitallot = new BLL.cm_UnitAllot();
        TG.BLL.cm_Coperation coper = new cm_Coperation();
        TG.BLL.cm_ProjectCharge project = new cm_ProjectCharge();
        /// <summary>
        /// 创建月报表的收费情况。
        /// </summary>List<TG.Model.ProMSFeiZhu> listSmon
        /// <param name="listSmon">月报表</param>
        /// <returns>string</returns>
        public string CreateTree(string year, string beginMonth, string unitid, string endMonth, string riqi)
        {
            List<TG.Model.ProMSFeiZhu> listSmon = GetList(year, beginMonth, unitid, endMonth);
            TG.Model.ProMSFeiWai modelwai = new Model.ProMSFeiWai();
            TG.Model.ProMSFeiZhu modelzhu = GetHeji(listSmon);
            if (unitid == "-1")
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("<center><h3>" + GetDate(beginMonth) + "至" + GetDate(endMonth).Substring(5) + "收费（" + riqi + "）</h3></center>");
                builder.Append("<p id='labDanW'style='float: right;margin-right: 10px;font-size: 9pt;font-family: 楷体'>" + GetDate(DateTime.Now) + "</p>");
                builder.Append("<p id='labTime' style='font-size: 9pt;font-family: 微软雅黑'>单位:万元</p>");
                builder.Append("<table id='monthtab'>");
                builder.Append("<tr>");
                builder.Append("<td style='width:20%' rowspan='2'>单位</td>");
                builder.Append("<td style='width:20%' rowspan='2'>项目名称</td>");
                builder.Append("<td style='width:12%' rowspan='2'>目标值</td>");
                builder.Append("<td style='width:16%' rowspan='2'>本周收费（万元）</td>");
                builder.Append("<td style='width:18%' colspan='2'>累计收费（万元）</td>");
                builder.Append("<td style='width:14%' rowspan='2'>完成年目标值%</td>");
                builder.Append("<tr><td>当月</td><td>本年度</td><tr>");
                builder.Append("<tr><td colspan='7' style='width:10%'>一、设计、勘察、监理部分</td></tr>");
                foreach (TG.Model.ProMSFeiZhu item in listSmon)
                {
                    //SConpay, SCount, SMubiaozhi, SLeijibenniandu, SLeijidangyue, SOvermubiaozhi

                    //builder.Append("<tr><td><table border='1'>");
                    List<TG.Model.ProMSFeiWai> list = GetSwai(riqi, beginMonth, year, endMonth, item.SConpay.ToString(), item.Sid);

                    builder.AppendFormat("<tr><td rowspan='{7}'>{0}</td><td>{1}</td><td rowspan='{7}'>{2}</td><td>{3}</td><td rowspan='{7}'>{4}</td><td rowspan='{7}'>{5}</td><td rowspan='{7}'>{6}</td><tr>", item.SConpay, "", item.SMubiaozhi, "", item.SLeijidangyue, item.SLeijibenniandu, GetTwoMon(item.SOvermubiaozhi), item.SCount == 0 ? 1 : list.Count);
                    for (int i = 2; i < list.Count; i++)
                    {
                        if (list[i].SBenzhoushoufei != 0)
                        {
                            builder.Append("<tr>");
                            builder.Append("<td>" + list[i].Sxianmumingcheng + "</td>");
                            builder.Append("<td>" + list[i].SBenzhoushoufei + "</td>");
                            builder.Append("</tr>");
                        }
                        else
                        {
                            builder.Append("<tr>");
                            builder.Append("<td></td>");
                            builder.Append("<td></td>");
                            builder.Append("</tr>");
                        }
                    }
                    modelwai = GetHeji(list);
                }
                builder.Append("</tr>");
                builder.Append("<tr><td></td><td>" + modelwai.Sxianmumingcheng + "</td><td>" + modelzhu.SMubiaozhi + "</td><td>" + modelwai.SBenzhoushoufei + "</td><td>" + modelzhu.SLeijidangyue + "</td><td>" + modelzhu.SLeijibenniandu + "</td><td>" + modelzhu.SOvermubiaozhi + "</td></tr>");
                builder.Append("<tr><td colspan='7' style='width:10%'>二、多种经营部分</td></tr>");
                //多营部分
                List<TG.Model.ProMSFeiZhu> listSmon2 = GetList2(year, beginMonth, unitid, endMonth);
                TG.Model.ProMSFeiWai modelwai2 = new Model.ProMSFeiWai();
                TG.Model.ProMSFeiZhu modelzhu2 = GetHeji(listSmon2);
                foreach (TG.Model.ProMSFeiZhu item in listSmon2)
                {
                    //SConpay, SCount, SMubiaozhi, SLeijibenniandu, SLeijidangyue, SOvermubiaozhi

                    //builder.Append("<tr><td><table border='1'>");
                    List<TG.Model.ProMSFeiWai> list = GetSwai2(riqi, beginMonth, year, endMonth, item.SConpay.ToString(), item.Sid);

                    builder.AppendFormat("<tr><td rowspan='{7}'>{0}</td><td>{1}</td><td rowspan='{7}'>{2}</td><td>{3}</td><td rowspan='{7}'>{4}</td><td rowspan='{7}'>{5}</td><td rowspan='{7}'>{6}</td><tr>", item.SConpay, "", item.SMubiaozhi, "", item.SLeijibenniandu, item.SLeijidangyue, GetTwoMon(item.SOvermubiaozhi), item.SCount == 0 ? 1 : list.Count);
                    for (int i = 2; i < list.Count; i++)
                    {
                        if (list[i].SBenzhoushoufei != 0)
                        {
                            builder.Append("<tr>");
                            builder.Append("<td>" + list[i].Sxianmumingcheng + "</td>");
                            builder.Append("<td>" + list[i].SBenzhoushoufei + "</td>");
                            builder.Append("</tr>");
                        }
                        else
                        {
                            builder.Append("<tr>");
                            builder.Append("<td></td>");
                            builder.Append("<td></td>");
                            builder.Append("</tr>");
                        }
                    }
                    modelwai2 = GetHeji(list);
                }
                builder.Append("</tr>");
                builder.Append("<tr><td></td><td>" + modelwai2.Sxianmumingcheng + "</td><td>" + modelzhu2.SMubiaozhi + "</td><td>" + modelwai2.SBenzhoushoufei + "</td><td>" + modelzhu2.SLeijidangyue + "</td><td>" + modelzhu2.SLeijibenniandu + "</td><td>" + modelzhu2.SOvermubiaozhi + "</td></tr>");
                builder.Append("<tr><td></td><td>全院总收入</td><td>" + (modelzhu2.SMubiaozhi + modelzhu.SMubiaozhi) + "</td><td>" + (modelwai2.SBenzhoushoufei + modelwai.SBenzhoushoufei) + "</td><td>" + (modelzhu2.SLeijidangyue + modelzhu.SLeijidangyue) + "</td><td>" + (modelzhu2.SLeijibenniandu + modelzhu.SLeijibenniandu) + "</td><td>" + (modelzhu2.SOvermubiaozhi + modelzhu.SOvermubiaozhi) + "</td></tr>");
                builder.Append("</table>");
                return builder.ToString();
            }
            else
            {

                StringBuilder builder = new StringBuilder();
                builder.Append("<center><h3>" + GetDate(beginMonth) + "至" + GetDate(endMonth).Substring(5) + "收费（" + riqi + "）</h3></center>");
                builder.Append("<p id='labDanW'style='float: right;margin-right: 10px;font-size: 9pt;font-family: 楷体'>" + GetDate(DateTime.Now) + "</p>");
                builder.Append("<p id='labTime' style='font-size: 9pt;font-family: 微软雅黑'>单位:万元</p>");
                builder.Append("<table id='monthtab'>");
                builder.Append("<tr>");
                builder.Append("<td style='width:20%' rowspan='2'>单位</td>");
                builder.Append("<td style='width:20%' rowspan='2'>项目名称</td>");
                builder.Append("<td style='width:12%' rowspan='2'>目标值</td>");
                builder.Append("<td style='width:16%' rowspan='2'>本周收费（万元）</td>");
                builder.Append("<td style='width:18%' colspan='2'>累计收费（万元）</td>");
                builder.Append("<td style='width:14%' rowspan='2'>完成年目标值%</td>");
                builder.Append("<tr><td>当月</td><td>本年度</td><tr>");
                // builder.Append("<tr><td colspan='7' style='width:10%'>一、设计、勘察、监理部分</td></tr>");
                foreach (TG.Model.ProMSFeiZhu item in listSmon)
                {
                    //SConpay, SCount, SMubiaozhi, SLeijibenniandu, SLeijidangyue, SOvermubiaozhi

                    //builder.Append("<tr><td><table border='1'>");
                    List<TG.Model.ProMSFeiWai> list = GetSwai(riqi, beginMonth, year, endMonth, item.SConpay.ToString(), item.Sid);

                    builder.AppendFormat("<tr><td rowspan='{7}'>{0}</td><td>{1}</td><td rowspan='{7}'>{2}</td><td>{3}</td><td rowspan='{7}'>{4}</td><td rowspan='{7}'>{5}</td><td rowspan='{7}'>{6}</td><tr>", item.SConpay, "", item.SMubiaozhi, "", item.SLeijidangyue, item.SLeijibenniandu, GetTwoMon(item.SOvermubiaozhi), item.SCount == 0 ? 1 : list.Count);
                    for (int i = 2; i < list.Count; i++)
                    {
                        if (list[i].SBenzhoushoufei != 0)
                        {
                            builder.Append("<tr>");
                            builder.Append("<td>" + list[i].Sxianmumingcheng + "</td>");
                            builder.Append("<td>" + list[i].SBenzhoushoufei + "</td>");
                            builder.Append("</tr>");
                        }
                        else
                        {
                            builder.Append("<tr>");
                            builder.Append("<td></td>");
                            builder.Append("<td></td>");
                            builder.Append("</tr>");
                        }
                    }
                    modelwai = GetHeji(list);
                }
                builder.Append("</tr>");
                builder.Append("<tr><td></td><td>" + modelwai.Sxianmumingcheng + "</td><td>" + modelzhu.SMubiaozhi + "</td><td>" + modelwai.SBenzhoushoufei + "</td><td>" + modelzhu.SLeijidangyue + "</td><td>" + modelzhu.SLeijibenniandu + "</td><td>" + modelzhu.SOvermubiaozhi + "</td></tr>");
                builder.Append("</table>");
                return builder.ToString();
            }
        }




        /// <summary>
        /// 转换decimal类型的数据
        /// </summary>
        /// <param name="mon"></param>
        /// <returns></returns>
        protected double GetTwoMon(decimal mon)
        {
            double s = Convert.ToDouble(mon);
            return s;
        }
        //转换可空decimal类型的数据
        protected double GetTwoMon(decimal? mon)
        {
            double s = Convert.ToDouble(mon);
            return s;
        }
        //修改日期格式
        protected string GetDate(DateTime dt)
        {
            string date = dt.ToString("yyyy-MM-dd");
            string[] arr = date.Split('-');
            return arr[0] + "年" + arr[1] + "月" + arr[2] + "日";
        }
        protected string GetDate(string dt)
        {
            string[] arr = dt.Split('-');
            return arr[0] + "年" + arr[1] + "月" + arr[2] + "日";
        }
        //得到外键表里的值 主营//是按收费日期算的，不是按合同签订的日期.我D
        public List<Model.ProMSFeiWai> GetSwai(string riqi, string beginMonth, string year, string endMonth, string unitname, int zhuId)
        {
            List<TG.Model.ProMSFeiWai> listwai = new List<Model.ProMSFeiWai>();

            DataSet dsproject = coper.GetList("cpr_Unit='" + unitname + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31'");
            foreach (DataRow itempro in dsproject.Tables[0].Rows)
            {
                TG.Model.ProMSFeiWai modelwai = new Model.ProMSFeiWai();
                modelwai.Unitid = zhuId.ToString();
                modelwai.Sxianmumingcheng = itempro["cpr_Name"].ToString();
                string strBenzsf = "";
                if (riqi == "周报表")
                {
                    strBenzsf = "InAcountTime>=DATEADD(wk,DATEDIFF(wk,0,'" + beginMonth + "'),0) AND InAcountTime <=dateadd(wk,datediff(wk,0,'" + beginMonth + "'),6)AND cprID=" + itempro["cpr_Id"] + "AND Status='E'";
                }
                else
                {
                    strBenzsf = "datediff(week,InAcountTime," + beginMonth + ")=0 AND cprID=" + itempro["cpr_Id"] + "AND Status='E'";
                }
                if (int.Parse(beginMonth.Split('-')[1]) == DateTime.Now.Month)
                {
                    strBenzsf = "datediff(week,InAcountTime,getdate())=0 AND cprID=" + itempro["cpr_Id"] + "AND Status='E'";
                }
                //datediff(week,InAcountTime,getdate())=0 AND cprID=" + itempro["cpr_Id"] + "AND Status='E'";
                //string strBenzsf = "InAcountTime>'" + beginMonth + "'AND InAcountTime<'" + GetWeek(beginMonth) + "'AND cprID=" + itempro["cpr_Id"];
                //IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + unitname + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')";
                modelwai.SBenzhoushoufei = Convert.ToDecimal(YiWanchengHeT(strBenzsf));
                listwai.Add(modelwai);
            }
            return listwai;
        }
        //多营 附表
        public List<Model.ProMSFeiWai> GetSwai2(string riqi, string beginMonth, string year, string endMonth, string unitname, int zhuId)
        {
            List<TG.Model.ProMSFeiWai> listwai = new List<Model.ProMSFeiWai>();

            DataSet dsproject = coper.GetList("cpr_Unit='" + unitname + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31'");
            foreach (DataRow itempro in dsproject.Tables[0].Rows)
            {
                TG.Model.ProMSFeiWai modelwai = new Model.ProMSFeiWai();
                modelwai.Unitid = zhuId.ToString();
                modelwai.Sxianmumingcheng = itempro["cpr_Name"].ToString();
                string strBenzsf = "";
                if (riqi == "周报表")
                {
                    strBenzsf = "InAcountTime>=DATEADD(wk,DATEDIFF(wk,0,'" + beginMonth + "'),0) AND InAcountTime <=dateadd(wk,datediff(wk,0,'" + beginMonth + "'),6)AND cprID=" + itempro["cpr_Id"] + "AND Status='E'";
                }
                else
                {
                    strBenzsf = "datediff(week,InAcountTime," + beginMonth + ")=0 AND cprID=" + itempro["cpr_Id"] + "AND Status='E'";
                }
                if (int.Parse(beginMonth.Split('-')[1]) == DateTime.Now.Month)
                {
                    strBenzsf = "datediff(week,InAcountTime,getdate())=0 AND cprID=" + itempro["cpr_Id"] + "AND Status='E'";
                }
                //string strBenzsf = "InAcountTime>'" + beginMonth + "'AND InAcountTime<'" + GetWeek(beginMonth) + "'AND cprID=" + itempro["cpr_Id"];
                //IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + unitname + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')";
                modelwai.SBenzhoushoufei = Convert.ToDecimal(YiWanchengHeT(strBenzsf));
                listwai.Add(modelwai);
            }
            return listwai;
        }
        //获得自定义对象报表(主表)的集合  主营 
        public List<TG.Model.ProMSFeiZhu> GetList(string year, string beginMonth, string unitid, string endMonth)
        {

            //一般经营模式
            if (unitid == "-1")
            {
                List<TG.Model.ProMSFeiZhu> listzhu = new List<Model.ProMSFeiZhu>();
                DataSet dstgunit = tgunit.GetList("unit_ParentID=238");
                foreach (DataRow item in dstgunit.Tables[0].Rows)
                {
                    TG.Model.ProMSFeiZhu modelzhu = new Model.ProMSFeiZhu();
                    modelzhu.SConpay = item["unit_Name"].ToString();
                    modelzhu.Sid = Convert.ToInt32(item["unit_ID"]);
                    //目标值
                    string strwhere = "UnitID=" + item["unit_ID"] + "AND AllotYear='" + year + "'";
                    modelzhu.SMubiaozhi = Mubiaozhi(strwhere);
                    string dangyueStr = "InAcountTime BETWEEN '" + beginMonth + "'AND '" + endMonth + "'" + "AND Status='E' AND cprID IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')"; ;
                    modelzhu.SLeijidangyue = Convert.ToDecimal(YiWanchengHeT(dangyueStr));
                    string bennianStr = "InAcountTime>'" + year + "-01-01'AND InAcountTime<'" + year + "-12-31'AND Status='E' AND cprID IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')";
                    modelzhu.SLeijibenniandu = Convert.ToDecimal(YiWanchengHeT(bennianStr));
                    //完成年目标值
                    try
                    {
                        modelzhu.SOvermubiaozhi = Convert.ToDecimal(modelzhu.SLeijibenniandu * 100 / modelzhu.SMubiaozhi);
                    }
                    catch (Exception)
                    {
                        modelzhu.SOvermubiaozhi = 0.0000M;
                    }
                    string strCount = "cpr_Unit='" + item["unit_Name"].ToString().Trim() + "'AND  cpr_SignDate BETWEEN '" + year + "-01-01' AND '" + year + "-12-31'";
                    modelzhu.SCount = GetCount(strCount);
                    listzhu.Add(modelzhu);
                }
                return listzhu;
            }
            else
            {
                List<TG.Model.ProMSFeiZhu> listzhu = new List<Model.ProMSFeiZhu>();
                DataSet dstgunit = tgunit.GetList("unit_ID=" + unitid);
                foreach (DataRow item in dstgunit.Tables[0].Rows)
                {
                    TG.Model.ProMSFeiZhu modelzhu = new Model.ProMSFeiZhu();
                    modelzhu.SConpay = item["unit_Name"].ToString();
                    modelzhu.Sid = Convert.ToInt32(item["unit_ID"]);
                    //目标值
                    string strwhere = "UnitID=" + item["unit_ID"] + "AND AllotYear='" + year + "'";
                    modelzhu.SMubiaozhi = Mubiaozhi(strwhere);
                    string dangyueStr = @"InAcountTime BETWEEN '" + beginMonth + "'AND '" + endMonth + "'" + "AND Status='E' AND cprID IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')";
                    // 
                    //
                    modelzhu.SLeijidangyue = Convert.ToDecimal(YiWanchengHeT(dangyueStr));
                    string bennianStr = "InAcountTime>'" + year + "-01-01'AND InAcountTime<'" + year + "-12-31'AND Status='E' AND cprID IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')";
                    modelzhu.SLeijibenniandu = Convert.ToDecimal(YiWanchengHeT(bennianStr));
                    //完成年目标值
                    try
                    {
                        modelzhu.SOvermubiaozhi = Convert.ToDecimal(modelzhu.SLeijibenniandu * 100 / modelzhu.SMubiaozhi);
                    }
                    catch (Exception)
                    {
                        modelzhu.SOvermubiaozhi = 0.0000M;
                    }
                    string strCount = "cpr_Unit='" + item["unit_Name"].ToString().Trim() + "'AND cpr_SignDate BETWEEN '" + year + "-01-01' AND '" + year + "-12-31'";
                    modelzhu.SCount = GetCount(strCount);
                    listzhu.Add(modelzhu);
                }
                return listzhu;
            }
        }
        //多营 主表
        public List<TG.Model.ProMSFeiZhu> GetList2(string year, string beginMonth, string unitid, string endMonth)
        {

            //一般经营模式
            if (unitid == "-1")
            {
                List<TG.Model.ProMSFeiZhu> listzhu = new List<Model.ProMSFeiZhu>();
                DataSet dstgunit = tgunit.GetList("unit_ParentID=254");
                foreach (DataRow item in dstgunit.Tables[0].Rows)
                {
                    TG.Model.ProMSFeiZhu modelzhu = new Model.ProMSFeiZhu();
                    modelzhu.SConpay = item["unit_Name"].ToString();
                    modelzhu.Sid = Convert.ToInt32(item["unit_ID"]);
                    //目标值
                    string strwhere = "UnitID=" + item["unit_ID"] + "AND AllotYear='" + year + "'";
                    modelzhu.SMubiaozhi = Mubiaozhi(strwhere);
                    string dangyueStr = "InAcountTime BETWEEN '" + beginMonth + "'AND '" + endMonth + "'" + "AND Status='E' AND cprID IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')"; ;
                    modelzhu.SLeijidangyue = Convert.ToDecimal(YiWanchengHeT(dangyueStr));
                    string bennianStr = "InAcountTime>'" + year + "-01-01'AND InAcountTime<'" + year + "-12-31'AND Status='E' AND cprID IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')";
                    modelzhu.SLeijibenniandu = Convert.ToDecimal(YiWanchengHeT(bennianStr));
                    //完成年目标值
                    try
                    {
                        modelzhu.SOvermubiaozhi = Convert.ToDecimal(modelzhu.SLeijibenniandu * 100 / modelzhu.SMubiaozhi);
                    }
                    catch (Exception)
                    {
                        modelzhu.SOvermubiaozhi = 0.0000M;
                    }
                    string strCount = "cpr_Unit='" + item["unit_Name"].ToString().Trim() + "'AND  cpr_SignDate BETWEEN '" + year + "-01-01' AND '" + year + "-12-31'";
                    modelzhu.SCount = GetCount(strCount);
                    listzhu.Add(modelzhu);
                }
                return listzhu;
            }
            else
            {
                List<TG.Model.ProMSFeiZhu> listzhu = new List<Model.ProMSFeiZhu>();
                DataSet dstgunit = tgunit.GetList("unit_ID=" + unitid);
                foreach (DataRow item in dstgunit.Tables[0].Rows)
                {
                    TG.Model.ProMSFeiZhu modelzhu = new Model.ProMSFeiZhu();
                    modelzhu.SConpay = item["unit_Name"].ToString();
                    modelzhu.Sid = Convert.ToInt32(item["unit_ID"]);
                    //目标值
                    string strwhere = "UnitID=" + item["unit_ID"] + "AND AllotYear='" + year + "'";
                    modelzhu.SMubiaozhi = Mubiaozhi(strwhere);
                    string dangyueStr = "InAcountTime BETWEEN '" + beginMonth + "'AND '" + endMonth + "'" + "AND Status='E' AND cprID IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')"; ;
                    modelzhu.SLeijidangyue = Convert.ToDecimal(YiWanchengHeT(dangyueStr));
                    string bennianStr = "InAcountTime>'" + year + "-01-01'AND InAcountTime<'" + year + "-12-31'AND Status='E' AND cprID IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')";
                    modelzhu.SLeijibenniandu = Convert.ToDecimal(YiWanchengHeT(bennianStr));
                    //完成年目标值
                    try
                    {
                        modelzhu.SOvermubiaozhi = Convert.ToDecimal((Convert.ToDecimal(modelzhu.SLeijibenniandu * 100 / modelzhu.SMubiaozhi)).ToString("f2"));
                    }
                    catch (Exception)
                    {
                        modelzhu.SOvermubiaozhi = 0.0000M;
                    }
                    string strCount = "cpr_Unit='" + item["unit_Name"].ToString().Trim() + "'AND cpr_SignDate BETWEEN '" + year + "-01-01' AND '" + year + "-12-31'";
                    modelzhu.SCount = GetCount(strCount);
                    listzhu.Add(modelzhu);
                }
                return listzhu;
            }
        }
        /// <summary>
        /// 小合计 主表
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public TG.Model.ProMSFeiZhu GetHeji(List<TG.Model.ProMSFeiZhu> list)
        {
            TG.Model.ProMSFeiZhu model = new Model.ProMSFeiZhu();
            model.SConpay = "";
            model.SCount = 0;
            decimal leijibennian = 0.0000M;
            decimal leijidangyue = 0.0000M;
            foreach (TG.Model.ProMSFeiZhu item in list)
            {
                leijibennian += Convert.ToDecimal(item.SLeijibenniandu);
                leijidangyue += Convert.ToDecimal(item.SLeijidangyue);
                model.SMubiaozhi += item.SMubiaozhi;
                //model.SLeijibenniandu = Convert.ToDecimal(25.0000);
                //model.SLeijidangyue = Convert.ToDecimal(25.0000);
                //model.SMubiaozhi = 10000;
                //model.SBenzhoushoufei += item.SBenzhoushoufei;
            }
            try
            {
                model.SOvermubiaozhi = Convert.ToDecimal((Convert.ToDecimal(model.SLeijibenniandu) * 100 / model.SMubiaozhi).ToString("f2"));
            }
            catch (Exception e)
            {
                model.SOvermubiaozhi += 0;
                // throw;
            }
            model.SLeijibenniandu = leijibennian;
            model.SLeijidangyue = leijidangyue;
            return model;
        }
        /// <summary>
        /// 小合计  附表
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public TG.Model.ProMSFeiWai GetHeji(List<TG.Model.ProMSFeiWai> list)
        {
            TG.Model.ProMSFeiWai model = new Model.ProMSFeiWai();
            model.Sxianmumingcheng = "合计";
            model.Unitid = "-1";
            foreach (TG.Model.ProMSFeiWai item in list)
            {
                model.SBenzhoushoufei += item.SBenzhoushoufei;
            }
            return model;
        }
        //本周收费
        private string GetWeek(string beginmonth)
        {
            DateTime dt = Convert.ToDateTime(beginmonth);
            return dt.AddDays(7).ToString("yy-MM-dd");
        }
        //项目集合
        private List<string> GetPro(string strwhere)
        {
            List<string> listPro = new List<string>();
            List<TG.Model.cm_Coperation> list = coper.GetModelList(strwhere);
            foreach (TG.Model.cm_Coperation model in list)
            {
                string str = model.cpr_Name;
                listPro.Add(str);
            }
            return listPro;
        }
        //目标值
        private int Mubiaozhi(string strwhere)
        {
            int Mubiao = 0;
            List<TG.Model.cm_UnitAllot> list = cmunitallot.GetModelList(strwhere);
            if (list.Count != 0 && list != null)
            {
                foreach (TG.Model.cm_UnitAllot item in list)
                {
                    Mubiao = Convert.ToInt32(item.UnitAllot);
                }
                return Mubiao;
            }
            else
            {
                return Mubiao;
            }
        }
        //得到合同数
        private int GetCount(string strwhere)
        {
            return coper.GetModelList(strwhere).Count;
        }
        //得到已经完成合同额
        private double YiWanchengHeT(string strw)
        {
            double d = 0.0000;
            List<TG.Model.cm_ProjectCharge> list = project.GetModelList(strw);
            foreach (TG.Model.cm_ProjectCharge item in list)
            {
                d += Convert.ToDouble(item.Acount);
            }
            return d;
        }
        public List<TG.Model.ProMSFeiZhu> ListToExcel(string beginMonth, string endMonth, string unitid, string year)
        {
            if (unitid == "-1")
            {
                List<TG.Model.ProMSFeiZhu> listzhu = new List<Model.ProMSFeiZhu>();
                DataSet dstgunit = tgunit.GetList("unit_ParentID=238");
                foreach (DataRow item in dstgunit.Tables[0].Rows)
                {
                    DataSet dsproject = coper.GetList("cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31'");
                    foreach (DataRow itempro in dsproject.Tables[0].Rows)
                    {
                        TG.Model.ProMSFeiZhu modelzhu = new Model.ProMSFeiZhu();
                        modelzhu.SConpay = item["unit_Name"].ToString();
                        modelzhu.Sid = Convert.ToInt32(item["unit_ID"]);
                        //目标值
                        string strwhere = "UnitID=" + item["unit_ID"] + "AND AllotYear='" + year + "'";
                        modelzhu.SMubiaozhi = Mubiaozhi(strwhere);
                        string dangyueStr = "InAcountTime BETWEEN '" + beginMonth + "'AND '" + endMonth + "'" + "AND Status='E' AND cprID IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')"; ;
                        modelzhu.SLeijidangyue = Convert.ToDecimal(YiWanchengHeT(dangyueStr));
                        string bennianStr = "InAcountTime>'" + year + "-01-01'AND InAcountTime<'" + year + "-12-31'AND Status='E' AND cprID IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')";
                        modelzhu.SLeijibenniandu = Convert.ToDecimal(YiWanchengHeT(bennianStr));
                        //完成年目标值
                        try
                        {
                            modelzhu.SOvermubiaozhi = Convert.ToDecimal(modelzhu.SLeijibenniandu * 100 / modelzhu.SMubiaozhi);
                        }
                        catch (Exception)
                        {
                            modelzhu.SOvermubiaozhi = 0.0000M;
                        }
                        string strCount = "cpr_Unit='" + item["unit_Name"] + "'AND cpr_SignDate BETWEEN '" + year + "-01-01' AND '" + year + "-12-31'";
                        modelzhu.SCount = GetCount(strCount);
                        modelzhu.Sxianmumingcheng = itempro["cpr_Name"].ToString();
                        string strBenzsf = "InAcountTime>'" + beginMonth + "'AND InAcountTime<'" + GetWeek(beginMonth) + "'AND cprID=" + itempro["cpr_Id"];
                        //IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + unitname + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')";
                        modelzhu.SBenzhoushoufei = Convert.ToDecimal(YiWanchengHeT(strBenzsf));
                        listzhu.Add(modelzhu);
                    }
                }
                return listzhu;
            }
            else
            {
                List<TG.Model.ProMSFeiZhu> listzhu = new List<Model.ProMSFeiZhu>();
                DataSet dstgunit = tgunit.GetList("unit_ID=" + unitid);
                foreach (DataRow item in dstgunit.Tables[0].Rows)
                {
                    DataSet dsproject = coper.GetList("cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31'");
                    foreach (DataRow itempro in dsproject.Tables[0].Rows)
                    {
                        TG.Model.ProMSFeiZhu modelzhu = new Model.ProMSFeiZhu();
                        modelzhu.SConpay = item["unit_Name"].ToString();
                        modelzhu.Sid = Convert.ToInt32(item["unit_ID"]);
                        //目标值
                        string strwhere = "UnitID=" + item["unit_ID"] + "AND AllotYear='" + year + "'";
                        modelzhu.SMubiaozhi = Mubiaozhi(strwhere);
                        string dangyueStr = "InAcountTime BETWEEN '" + beginMonth + "'AND '" + endMonth + "'" + "AND Status='E' AND cprID IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')"; ;
                        modelzhu.SLeijidangyue = Convert.ToDecimal(YiWanchengHeT(dangyueStr));
                        string bennianStr = "InAcountTime>'" + year + "-01-01'AND InAcountTime<'" + year + "-12-31'AND Status='E' AND cprID IN (SELECT cm_Coperation.cpr_Id FROM cm_Coperation WHERE cpr_Unit='" + item["unit_Name"].ToString() + "' AND cm_Coperation.cpr_SignDate>'" + year + "-01-01' AND cm_Coperation.cpr_SignDate<'" + year + "-12-31')";
                        modelzhu.SLeijibenniandu = Convert.ToDecimal(YiWanchengHeT(bennianStr));
                        //完成年目标值
                        try
                        {
                            modelzhu.SOvermubiaozhi = Convert.ToDecimal(modelzhu.SLeijibenniandu * 100 / modelzhu.SMubiaozhi);
                        }
                        catch (Exception)
                        {
                            modelzhu.SOvermubiaozhi = 0.0000M;
                        }
                        //合同总数
                        string strCount = "cpr_Unit='" + item["unit_Name"] + "'AND cpr_SignDate BETWEEN '" + year + "-01-01' AND '" + year + "-12-31'";
                        modelzhu.SCount = GetCount(strCount);
                        //合同名称
                        modelzhu.Sxianmumingcheng = itempro["cpr_Name"].ToString();
                        string strBenzsf = "InAcountTime>'" + beginMonth + "'AND InAcountTime<'" + GetWeek(beginMonth) + "'AND Status='E' AND cprID=" + itempro["cpr_Id"];
                        modelzhu.SBenzhoushoufei = Convert.ToDecimal(YiWanchengHeT(strBenzsf));
                        listzhu.Add(modelzhu);
                    }
                }
                return listzhu;
            }
        }
    }
}
