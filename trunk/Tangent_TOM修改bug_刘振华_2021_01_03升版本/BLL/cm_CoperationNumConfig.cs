﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
    /// <summary>
    /// cm_CoperationNumConfig
    /// </summary>
    public partial class cm_CoperationNumConfig
    {
        private readonly TG.DAL.cm_CoperationNumConfig dal = new TG.DAL.cm_CoperationNumConfig();
        public cm_CoperationNumConfig()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TG.Model.cm_CoperationNumConfig model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CoperationNumConfig model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete()
        {
            //该表无主键信息，请自定义主键/条件字段
            return dal.Delete();
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationNumConfig GetModel(int id)
        {
            //该表无主键信息，请自定义主键/条件字段
            return dal.GetModel(id);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_CoperationNumConfig GetModelByCache()
        {
            //该表无主键信息，请自定义主键/条件字段
            string CacheKey = "cm_CoperationNumConfigModel-";
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel();
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_CoperationNumConfig)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CoperationNumConfig> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CoperationNumConfig> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_CoperationNumConfig> modelList = new List<TG.Model.cm_CoperationNumConfig>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_CoperationNumConfig model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_CoperationNumConfig();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["CprPfix"] != null && dt.Rows[n]["CprPfix"].ToString() != "")
                    {
                        model.CprPfix = dt.Rows[n]["CprPfix"].ToString();
                    }
                    if (dt.Rows[n]["CprNumStart"] != null && dt.Rows[n]["CprNumStart"].ToString() != "")
                    {
                        model.CprNumStart = dt.Rows[n]["CprNumStart"].ToString();
                    }
                    if (dt.Rows[n]["CprNumEnd"] != null && dt.Rows[n]["CprNumEnd"].ToString() != "")
                    {
                        model.CprNumEnd = dt.Rows[n]["CprNumEnd"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        public List<string> GetExistsCoperationNo(string coperationType)
        {
            return dal.GetExistsCoperationNo(coperationType);
        }

        #endregion  Method
    }
}

