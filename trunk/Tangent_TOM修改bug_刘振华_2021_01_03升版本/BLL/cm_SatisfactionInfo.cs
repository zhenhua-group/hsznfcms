﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_SatisfactionInfo
	/// </summary>
	public partial class cm_SatisfactionInfo
	{
		private readonly TG.DAL.cm_SatisfactionInfo dal=new TG.DAL.cm_SatisfactionInfo();
		public cm_SatisfactionInfo()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Sat_Id)
		{
			return dal.Exists(Sat_Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_SatisfactionInfo model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_SatisfactionInfo model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Sat_Id)
		{
			
			return dal.Delete(Sat_Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Sat_Idlist )
		{
			return dal.DeleteList(Sat_Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_SatisfactionInfo GetModel(int Sat_Id)
		{
			
			return dal.GetModel(Sat_Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_SatisfactionInfo GetModelByCache(int Sat_Id)
		{
			
			string CacheKey = "cm_SatisfactionInfoModel-" + Sat_Id;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Sat_Id);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_SatisfactionInfo)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_SatisfactionInfo> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_SatisfactionInfo> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_SatisfactionInfo> modelList = new List<TG.Model.cm_SatisfactionInfo>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_SatisfactionInfo model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_SatisfactionInfo();
					if(dt.Rows[n]["Sat_Id"]!=null && dt.Rows[n]["Sat_Id"].ToString()!="")
					{
						model.Sat_Id=int.Parse(dt.Rows[n]["Sat_Id"].ToString());
					}
					if(dt.Rows[n]["Cst_Id"]!=null && dt.Rows[n]["Cst_Id"].ToString()!="")
					{
                        model.Cst_Id = Convert.ToDecimal(dt.Rows[n]["Cst_Id"].ToString());
					}
					if(dt.Rows[n]["Sat_No"]!=null && dt.Rows[n]["Sat_No"].ToString()!="")
					{
					model.Sat_No=dt.Rows[n]["Sat_No"].ToString();
					}
					if(dt.Rows[n]["Sch_Context"]!=null && dt.Rows[n]["Sch_Context"].ToString()!="")
					{
					model.Sch_Context=dt.Rows[n]["Sch_Context"].ToString();
					}
					if(dt.Rows[n]["Sat_Leve"]!=null && dt.Rows[n]["Sat_Leve"].ToString()!="")
					{
						model.Sat_Leve=int.Parse(dt.Rows[n]["Sat_Leve"].ToString());
					}
					if(dt.Rows[n]["Sch_Time"]!=null && dt.Rows[n]["Sch_Time"].ToString()!="")
					{
						model.Sch_Time=DateTime.Parse(dt.Rows[n]["Sch_Time"].ToString());
					}
					if(dt.Rows[n]["Sch_Way"]!=null && dt.Rows[n]["Sch_Way"].ToString()!="")
					{
						model.Sch_Way=int.Parse(dt.Rows[n]["Sch_Way"].ToString());
					}
					if(dt.Rows[n]["Sch_Reason"]!=null && dt.Rows[n]["Sch_Reason"].ToString()!="")
					{
					model.Sch_Reason=dt.Rows[n]["Sch_Reason"].ToString();
					}
					if(dt.Rows[n]["Sat_Best"]!=null && dt.Rows[n]["Sat_Best"].ToString()!="")
					{
					model.Sat_Best=dt.Rows[n]["Sat_Best"].ToString();
					}
					if(dt.Rows[n]["Sat_NotBest"]!=null && dt.Rows[n]["Sat_NotBest"].ToString()!="")
					{
					model.Sat_NotBest=dt.Rows[n]["Sat_NotBest"].ToString();
					}
					if(dt.Rows[n]["Remark"]!=null && dt.Rows[n]["Remark"].ToString()!="")
					{
					model.Remark=dt.Rows[n]["Remark"].ToString();
					}
					if(dt.Rows[n]["UpdateBy"]!=null && dt.Rows[n]["UpdateBy"].ToString()!="")
					{
						model.UpdateBy=int.Parse(dt.Rows[n]["UpdateBy"].ToString());
					}
					if(dt.Rows[n]["LastUpdate"]!=null && dt.Rows[n]["LastUpdate"].ToString()!="")
					{
						model.LastUpdate=DateTime.Parse(dt.Rows[n]["LastUpdate"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

