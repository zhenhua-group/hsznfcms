﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_UnitManagerConfig
	/// </summary>
	public partial class cm_UnitManagerConfig
	{
		private readonly TG.DAL.cm_UnitManagerConfig dal=new TG.DAL.cm_UnitManagerConfig();
		public cm_UnitManagerConfig()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			return dal.Exists(id);
		}
         /// <summary>
        /// 是否存在该记录
        /// </summary>
        public int ExistsID(int unitid)
        {
            return dal.ExistsID(unitid);
        }
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_UnitManagerConfig model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_UnitManagerConfig model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			return dal.Delete(id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			return dal.DeleteList(idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_UnitManagerConfig GetModel(int id)
		{
			
			return dal.GetModel(id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_UnitManagerConfig GetModelByCache(int id)
		{
			
			string CacheKey = "cm_UnitManagerConfigModel-" + id;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(id);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_UnitManagerConfig)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_UnitManagerConfig> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_UnitManagerConfig> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_UnitManagerConfig> modelList = new List<TG.Model.cm_UnitManagerConfig>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_UnitManagerConfig model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_UnitManagerConfig();
					if(dt.Rows[n]["id"]!=null && dt.Rows[n]["id"].ToString()!="")
					{
						model.id=int.Parse(dt.Rows[n]["id"].ToString());
					}
					if(dt.Rows[n]["unit_id"]!=null && dt.Rows[n]["unit_id"].ToString()!="")
					{
						model.unit_id=int.Parse(dt.Rows[n]["unit_id"].ToString());
					}
					if(dt.Rows[n]["unitusers"]!=null && dt.Rows[n]["unitusers"].ToString()!="")
					{
					model.unitusers=dt.Rows[n]["unitusers"].ToString();
					}
					if(dt.Rows[n]["managerusers"]!=null && dt.Rows[n]["managerusers"].ToString()!="")
					{
					model.managerusers=dt.Rows[n]["managerusers"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="configTableName"></param>
        /// <returns></returns>
        public List<TG.Model.tg_member> GetUserEntityList(string sqlwhere,string columnstype)
        {
            List<TG.Model.tg_member> list = new List<TG.Model.tg_member>();
            List<TG.Model.cm_UnitManagerConfig> auditConfigBaseList = DataTableToList(dal.GetList(sqlwhere).Tables[0]);

            auditConfigBaseList.ForEach((umConfig) =>
            {
                if (columnstype == "unitusers")
                {
                    if (!string.IsNullOrEmpty(umConfig.unitusers))
                    {
                        string[] userSysNoArray = umConfig.unitusers.Split(',');
                        foreach (var userSysNo in userSysNoArray)
                        {
                            TG.Model.tg_member userInfo = new TG.BLL.tg_member().GetModel(int.Parse(userSysNo));
                            //得到用户信息
                            if (userInfo != null)
                            {
                                list.Add(userInfo);
                            }
                        }
                    }
                }
                else if (columnstype == "managerusers")
                {
                    if (!string.IsNullOrEmpty(umConfig.managerusers))
                    {
                        string[] userSysNoArray = umConfig.managerusers.Split(',');
                        foreach (var userSysNo in userSysNoArray)
                        {
                            TG.Model.tg_member userInfo = new TG.BLL.tg_member().GetModel(int.Parse(userSysNo));
                            //得到用户信息
                            if (userInfo != null)
                            {
                                list.Add(userInfo);
                            }
                        }
                    }
                }
                
            });
            return list;
        }

	}
}

