﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public class ChargeAndCoper
    {
        TG.DAL.ChargeAndCoper dal = new DAL.ChargeAndCoper();
        /// <summary>
        /// 得到所有合同额 
        /// </summary>
        /// <param name="unitId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="endmonth"></param>
        /// <returns></returns>
        public List<TG.Model.ChargeAndCoper> GetList(string unitId, string year, string begintime, string endtime, string endmonth)
        {
            List<TG.Model.ChargeAndCoper> listAll = new List<Model.ChargeAndCoper>();
            if (unitId == "-1")
            {
                List<TG.Model.ChargeAndCoper> listFrist;
                TG.Model.ChargeAndCoper modelfrist;
                ZhuUnit(year, begintime, endtime, out listFrist, out modelfrist, endmonth);


                List<TG.Model.ChargeAndCoper> listSecond;
                TG.Model.ChargeAndCoper modelsecond;
                ShiUnit(year, begintime, endtime, out listSecond, out modelsecond, endmonth);

                TG.Model.ChargeAndCoper modelAll = Getzongji(modelfrist, modelsecond);
                listAll.AddRange(listFrist);
                listAll.AddRange(listSecond);
                listAll.Add(modelAll);
            }
            else
            {
                unitId = " a.unit_ID=" + unitId;
                listAll = dal.GetListChargeAndCoper(unitId, year, begintime, endtime, endmonth);
            }
            return listAll;
        }
        /// <summary>
        /// 施工合计
        /// </summary>
        /// <param name="year">年份</param>
        /// <param name="beginTime">开始月份</param>
        /// <param name="endTime"></param>
        /// <param name="listSecond"></param>
        /// <param name="modelsecond"></param>
        public void ShiUnit(string year, string begintime, string endtime, out List<TG.Model.ChargeAndCoper> listSecond, out TG.Model.ChargeAndCoper modelsecond, string endmonth)
        {
            listSecond = dal.GetListChargeAndCoper("a.unit_ParentID=254", year, begintime, endtime, endmonth);
            modelsecond = GetHeji(listSecond, "施工合计");
            listSecond.Add(modelsecond);
        }
        /// <summary>
        /// 主要经营合计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="listFrist"></param>
        /// <param name="modelfrist"></param>
        public void ZhuUnit(string year, string begintime, string endtime, out List<TG.Model.ChargeAndCoper> listFrist, out TG.Model.ChargeAndCoper modelfrist, string endmonth)
        {
            listFrist = dal.GetListChargeAndCoper("a.unit_ParentID=238", year, begintime, endtime, endmonth);
            modelfrist = GetHeji(listFrist, "主营合计");
            listFrist.Add(modelfrist);
        }
        /// <summary>
        /// 合同额
        /// </summary>
        /// <param name="list"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public TG.Model.ChargeAndCoper GetHeji(List<TG.Model.ChargeAndCoper> list, string str)
        {
            TG.Model.ChargeAndCoper model = new Model.ChargeAndCoper();
            model.Unit = str;
            foreach (TG.Model.ChargeAndCoper item in list)
            {
                model.Frsitmonthcharge += Convert.ToDecimal(item.Frsitmonthcharge);
                model.Frsitmonthcoper += Convert.ToDecimal(item.Frsitmonthcoper.ToString().Trim());
                model.Secondmonthcharge += Convert.ToDecimal(item.Secondmonthcharge.ToString().Trim());
                model.Secondmonthcoper += Convert.ToDecimal(item.Secondmonthcoper.ToString().Trim());
                model.UnitAllotLast += Convert.ToInt32(Convert.ToDecimal(item.UnitAllotLast.ToString().Trim()));
            }
            return model;
        }
        /// <summary>
        /// 总计合同额
        /// </summary>
        /// <param name="listfrist"></param>
        /// <param name="listSecond"></param>
        /// <returns></returns>
        public TG.Model.ChargeAndCoper Getzongji(TG.Model.ChargeAndCoper listfrist, TG.Model.ChargeAndCoper listSecond)
        {
            TG.Model.ChargeAndCoper Zongji = new Model.ChargeAndCoper();
            Zongji.Unit = "全院总合计";
            Zongji.Frsitmonthcharge = listfrist.Frsitmonthcharge + listSecond.Frsitmonthcharge;
            Zongji.Frsitmonthcoper = listfrist.Frsitmonthcoper + listSecond.Frsitmonthcoper;
            Zongji.Secondmonthcharge = listfrist.Secondmonthcharge + listSecond.Secondmonthcharge;
            Zongji.Secondmonthcoper = listfrist.Secondmonthcoper + listSecond.Secondmonthcoper;
            Zongji.UnitAllotLast = listfrist.UnitAllotLast + listSecond.UnitAllotLast;
            return Zongji;
        }
    }
}
