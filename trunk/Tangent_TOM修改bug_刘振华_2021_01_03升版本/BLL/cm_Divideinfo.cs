﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public class cm_Divideinfo
    {
        private readonly TG.DAL.cm_Divideinfo dal = new TG.DAL.cm_Divideinfo();
        public cm_Divideinfo()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_Divideinfo model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_Divideinfo model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_Divideinfo GetModel(int ID)
        {

            return dal.GetModel(ID);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }
        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListByPageProc(query, startIndex, endIndex);
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return dal.GetListPageProcCount(query);
        }
        /// <summary>
        /// 判断是否重复
        /// </summary>
        /// <param name="cst_num"></param>
        /// <param name="cst_Name"></param>
        /// <returns></returns>
        public bool JudgeData(string unit, string dividePercent, string id, string year)
        {
            return dal.JudgeData(unit, dividePercent, id, year);
        }
        public DataSet GetDividedByYear(string year, string unitname)
        {
            return dal.GetDividedByYear(year, unitname);
        }
          /// <summary>
        /// 得到收入成本结余明细表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetUnitRevenueCostBalance(string strWhere,string year)
        {
            return dal.GetUnitRevenueCostBalance(strWhere,year);
        }
        #endregion  Method

    }
}

