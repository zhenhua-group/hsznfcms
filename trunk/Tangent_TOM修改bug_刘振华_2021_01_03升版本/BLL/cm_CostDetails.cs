﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    /// <summary>
    /// cm_CostDetails
    /// </summary>
    public partial class cm_CostDetails
    {
        private readonly TG.DAL.cm_CostDetails dal = new TG.DAL.cm_CostDetails();
        public cm_CostDetails()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            return dal.Exists(ID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CostDetails model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CostDetails model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CostDetails GetModel(int ID)
        {

            return dal.GetModel(ID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_CostDetails GetModelByCache(int ID)
        {

            string CacheKey = "cm_CostDetailsModel-" + ID;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(ID);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_CostDetails)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CostDetails> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CostDetails> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_CostDetails> modelList = new List<TG.Model.cm_CostDetails>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_CostDetails model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_CostDetails();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["costTime"] != null && dt.Rows[n]["costTime"].ToString() != "")
                    {
                        model.costTime = DateTime.Parse(dt.Rows[n]["costTime"].ToString());
                    }
                    if (dt.Rows[n]["costNum"] != null && dt.Rows[n]["costNum"].ToString() != "")
                    {
                        model.costNum = dt.Rows[n]["costNum"].ToString();
                    }
                    if (dt.Rows[n]["costSub"] != null && dt.Rows[n]["costSub"].ToString() != "")
                    {
                        model.costSub = dt.Rows[n]["costSub"].ToString();
                    }
                    if (dt.Rows[n]["costCharge"] != null && dt.Rows[n]["costCharge"].ToString() != "")
                    {
                        model.costCharge = decimal.Parse(dt.Rows[n]["costCharge"].ToString());
                    }
                    if (dt.Rows[n]["costOutCharge"] != null && dt.Rows[n]["costOutCharge"].ToString() != "")
                    {
                        model.costOutCharge = decimal.Parse(dt.Rows[n]["costOutCharge"].ToString());
                    }
                    if (dt.Rows[n]["costUnit"] != null && dt.Rows[n]["costUnit"].ToString() != "")
                    {
                        model.costUnit = int.Parse(dt.Rows[n]["costUnit"].ToString());
                    }
                    if (dt.Rows[n]["costUserId"] != null && dt.Rows[n]["costUserId"].ToString() != "")
                    {
                        model.costUserId = int.Parse(dt.Rows[n]["costUserId"].ToString());
                    }
                    if (dt.Rows[n]["costTypeID"] != null && dt.Rows[n]["costTypeID"].ToString() != "")
                    {
                        model.costTypeID = int.Parse(dt.Rows[n]["costTypeID"].ToString());
                    }
                    if (dt.Rows[n]["isexpord"] != null && dt.Rows[n]["isexpord"].ToString() != "")
                    {
                        model.isexpord = int.Parse(dt.Rows[n]["isexpord"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}

