﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    /// <summary>
    /// cm_PitCoperation
    /// </summary>
    public partial class cm_PitCoperation
    {
        private readonly TG.DAL.cm_PitCoperation dal = new TG.DAL.cm_PitCoperation();
        public cm_PitCoperation()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int cpr_Id)
        {
            return dal.Exists(cpr_Id);
        }
         /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string cprName,string sysno)
        {
            return dal.Exists(cprName,sysno);
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_PitCoperation model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_PitCoperation model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int cpr_Id)
        {

            return dal.Delete(cpr_Id);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string cpr_Idlist)
        {
            return dal.DeleteList(cpr_Idlist);
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public ArrayList DeleteDesignList(string cpr_Idlist)
        {
            return dal.DeleteDesignList(cpr_Idlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_PitCoperation GetModel(int cpr_Id)
        {

            return dal.GetModel(cpr_Id);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_PitCoperation GetModelByCache(int cpr_Id)
        {

            string CacheKey = "cm_PitCoperationModel-" + cpr_Id;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(cpr_Id);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_PitCoperation)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_PitCoperation> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_PitCoperation> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_PitCoperation> modelList = new List<TG.Model.cm_PitCoperation>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_PitCoperation model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_PitCoperation();
                    if (dt.Rows[n]["cpr_Id"] != null && dt.Rows[n]["cpr_Id"].ToString() != "")
                    {
                        model.cpr_Id = int.Parse(dt.Rows[n]["cpr_Id"].ToString());
                    }
                    if (dt.Rows[n]["cst_Id"] != null && dt.Rows[n]["cst_Id"].ToString() != "")
                    {
                        model.cst_Id = int.Parse(dt.Rows[n]["cst_Id"].ToString());
                    }
                    if (dt.Rows[n]["cpr_No"] != null && dt.Rows[n]["cpr_No"].ToString() != "")
                    {
                        model.cpr_No = dt.Rows[n]["cpr_No"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Type"] != null && dt.Rows[n]["cpr_Type"].ToString() != "")
                    {
                        model.cpr_Type = dt.Rows[n]["cpr_Type"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Name"] != null && dt.Rows[n]["cpr_Name"].ToString() != "")
                    {
                        model.cpr_Name = dt.Rows[n]["cpr_Name"].ToString();
                    }
                    if (dt.Rows[n]["BuildUnit"] != null && dt.Rows[n]["BuildUnit"].ToString() != "")
                    {
                        model.BuildUnit = dt.Rows[n]["BuildUnit"].ToString();
                    }
                    if (dt.Rows[n]["Floor"] != null && dt.Rows[n]["Floor"].ToString() != "")
                    {
                        model.Floor = dt.Rows[n]["Floor"].ToString();
                    }
                    if (dt.Rows[n]["ChgPeople"] != null && dt.Rows[n]["ChgPeople"].ToString() != "")
                    {
                        model.ChgPeople = dt.Rows[n]["ChgPeople"].ToString();
                    }
                    if (dt.Rows[n]["ChgPhone"] != null && dt.Rows[n]["ChgPhone"].ToString() != "")
                    {
                        model.ChgPhone = dt.Rows[n]["ChgPhone"].ToString();
                    }
                    if (dt.Rows[n]["ChgJia"] != null && dt.Rows[n]["ChgJia"].ToString() != "")
                    {
                        model.ChgJia = dt.Rows[n]["ChgJia"].ToString();
                    }
                    if (dt.Rows[n]["ChgJiaPhone"] != null && dt.Rows[n]["ChgJiaPhone"].ToString() != "")
                    {
                        model.ChgJiaPhone = dt.Rows[n]["ChgJiaPhone"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Unit"] != null && dt.Rows[n]["cpr_Unit"].ToString() != "")
                    {
                        model.cpr_Unit = dt.Rows[n]["cpr_Unit"].ToString();
                    }
                    if (dt.Rows[n]["BuildPosition"] != null && dt.Rows[n]["BuildPosition"].ToString() != "")
                    {
                        model.BuildPosition = dt.Rows[n]["BuildPosition"].ToString();
                    }
                    if (dt.Rows[n]["Industry"] != null && dt.Rows[n]["Industry"].ToString() != "")
                    {
                        model.Industry = dt.Rows[n]["Industry"].ToString();
                    }
                    if (dt.Rows[n]["BuildSrc"] != null && dt.Rows[n]["BuildSrc"].ToString() != "")
                    {
                        model.BuildSrc = dt.Rows[n]["BuildSrc"].ToString();
                    }
                    if (dt.Rows[n]["TableMaker"] != null && dt.Rows[n]["TableMaker"].ToString() != "")
                    {
                        model.TableMaker = dt.Rows[n]["TableMaker"].ToString();
                    }
                    if (dt.Rows[n]["cpr_DoneDate"] != null && dt.Rows[n]["cpr_DoneDate"].ToString() != "")
                    {
                        model.cpr_DoneDate = DateTime.Parse(dt.Rows[n]["cpr_DoneDate"].ToString());
                    }
                    if (dt.Rows[n]["cpr_Mark"] != null && dt.Rows[n]["cpr_Mark"].ToString() != "")
                    {
                        model.cpr_Mark = dt.Rows[n]["cpr_Mark"].ToString();
                    }
                    if (dt.Rows[n]["MultiBuild"] != null && dt.Rows[n]["MultiBuild"].ToString() != "")
                    {
                        model.MultiBuild = dt.Rows[n]["MultiBuild"].ToString();
                    }
                    if (dt.Rows[n]["PitLevel"] != null && dt.Rows[n]["PitLevel"].ToString() != "")
                    {
                        model.PitLevel = dt.Rows[n]["PitLevel"].ToString();
                    }
                    if (dt.Rows[n]["BuildArea"] != null && dt.Rows[n]["BuildArea"].ToString() != "")
                    {
                        model.BuildArea = decimal.Parse(dt.Rows[n]["BuildArea"].ToString());
                    }
                    if (dt.Rows[n]["PitHeight"] != null && dt.Rows[n]["PitHeight"].ToString() != "")
                    {
                        model.PitHeight = decimal.Parse(dt.Rows[n]["PitHeight"].ToString());
                    }
                    if (dt.Rows[n]["SlopeHeight"] != null && dt.Rows[n]["SlopeHeight"].ToString() != "")
                    {
                        model.SlopeHeight = decimal.Parse(dt.Rows[n]["SlopeHeight"].ToString());
                    }
                    if (dt.Rows[n]["PointNumber"] != null && dt.Rows[n]["PointNumber"].ToString() != "")
                    {
                        model.PointNumber = int.Parse(dt.Rows[n]["PointNumber"].ToString());
                    }
                    if (dt.Rows[n]["TestNumber"] != null && dt.Rows[n]["TestNumber"].ToString() != "")
                    {
                        model.TestNumber = int.Parse(dt.Rows[n]["TestNumber"].ToString());
                    }
                    if (dt.Rows[n]["TestCount"] != null && dt.Rows[n]["TestCount"].ToString() != "")
                    {
                        model.TestCount = int.Parse(dt.Rows[n]["TestCount"].ToString());
                    }
                    if (dt.Rows[n]["PointSum"] != null && dt.Rows[n]["PointSum"].ToString() != "")
                    {
                        model.PointSum = int.Parse(dt.Rows[n]["PointSum"].ToString());
                    }
                    if (dt.Rows[n]["BuildType"] != null && dt.Rows[n]["BuildType"].ToString() != "")
                    {
                        model.BuildType = dt.Rows[n]["BuildType"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Type2"] != null && dt.Rows[n]["cpr_Type2"].ToString() != "")
                    {
                        model.cpr_Type2 = dt.Rows[n]["cpr_Type2"].ToString();
                    }
                    if (dt.Rows[n]["cpr_SignDate"] != null && dt.Rows[n]["cpr_SignDate"].ToString() != "")
                    {
                        model.cpr_SignDate = DateTime.Parse(dt.Rows[n]["cpr_SignDate"].ToString());
                    }
                    if (dt.Rows[n]["cpr_SignDate2"] != null && dt.Rows[n]["cpr_SignDate2"].ToString() != "")
                    {
                        model.cpr_SignDate2 = DateTime.Parse(dt.Rows[n]["cpr_SignDate2"].ToString());
                    }
                    if (dt.Rows[n]["ProjectDate"] != null && dt.Rows[n]["ProjectDate"].ToString() != "")
                    {
                        model.ProjectDate = int.Parse(dt.Rows[n]["ProjectDate"].ToString());
                    }
                    if (dt.Rows[n]["projextDateOther"] != null && dt.Rows[n]["projextDateOther"].ToString() != "")
                    {
                        model.projextDateOther = dt.Rows[n]["projextDateOther"].ToString();
                    }
                    if (dt.Rows[n]["InsertUserID"] != null && dt.Rows[n]["InsertUserID"].ToString() != "")
                    {
                        model.InsertUserID = int.Parse(dt.Rows[n]["InsertUserID"].ToString());
                    }
                    if (dt.Rows[n]["InsertDate"] != null && dt.Rows[n]["InsertDate"].ToString() != "")
                    {
                        model.InsertDate = DateTime.Parse(dt.Rows[n]["InsertDate"].ToString());
                    }
                    if (dt.Rows[n]["IsParamterEdit"] != null && dt.Rows[n]["IsParamterEdit"].ToString() != "")
                    {
                        model.IsParamterEdit = int.Parse(dt.Rows[n]["IsParamterEdit"].ToString());
                    }
                    if (dt.Rows[n]["PMUserID"] != null && dt.Rows[n]["PMUserID"].ToString() != "")
                    {
                        model.PMUserID = int.Parse(dt.Rows[n]["PMUserID"].ToString());
                    }
                    if (dt.Rows[n]["cpr_Acount"] != null && dt.Rows[n]["cpr_Acount"].ToString() != "")
                    {
                        model.cpr_Acount = decimal.Parse(dt.Rows[n]["cpr_Acount"].ToString());
                    }
                    if (dt.Rows[n]["cpr_ShijiAcount"] != null && dt.Rows[n]["cpr_ShijiAcount"].ToString() != "")
                    {
                        model.cpr_ShijiAcount = decimal.Parse(dt.Rows[n]["cpr_ShijiAcount"].ToString());
                    }
                    if (dt.Rows[n]["RegTime"] != null && dt.Rows[n]["RegTime"].ToString() != "")
                    {
                        model.RegTime = DateTime.Parse(dt.Rows[n]["RegTime"].ToString());
                    }
                    if (dt.Rows[n]["UpdateBy"] != null && dt.Rows[n]["UpdateBy"].ToString() != "")
                    {
                        model.UpdateBy = dt.Rows[n]["UpdateBy"].ToString();
                    }
                    if (dt.Rows[n]["LastUpdate"] != null && dt.Rows[n]["LastUpdate"].ToString() != "")
                    {
                        model.LastUpdate = DateTime.Parse(dt.Rows[n]["LastUpdate"].ToString());
                    }
					if(dt.Rows[n]["cpr_FID"]!=null && dt.Rows[n]["cpr_FID"].ToString()!="")
					{
						model.cpr_FID=int.Parse(dt.Rows[n]["cpr_FID"].ToString());
					}
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}

