﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_SubCoperation
	/// </summary>
	public partial class cm_SubCoperation
	{
		private readonly TG.DAL.cm_SubCoperation dal=new TG.DAL.cm_SubCoperation();
		public cm_SubCoperation()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_SubCoperation model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_SubCoperation model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			return dal.Delete(ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			return dal.DeleteList(IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_SubCoperation GetModel(int ID)
		{
			
			return dal.GetModel(ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_SubCoperation GetModelByCache(int ID)
		{
			
			string CacheKey = "cm_SubCoperationModel-" + ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_SubCoperation)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_SubCoperation> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_SubCoperation> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_SubCoperation> modelList = new List<TG.Model.cm_SubCoperation>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_SubCoperation model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_SubCoperation();
					if(dt.Rows[n]["ID"]!=null && dt.Rows[n]["ID"].ToString()!="")
					{
						model.ID=int.Parse(dt.Rows[n]["ID"].ToString());
					}
					if(dt.Rows[n]["cpr_Id"]!=null && dt.Rows[n]["cpr_Id"].ToString()!="")
					{
						model.cpr_Id=int.Parse(dt.Rows[n]["cpr_Id"].ToString());
					}
					if(dt.Rows[n]["cpr_No"]!=null && dt.Rows[n]["cpr_No"].ToString()!="")
					{
					model.cpr_No=dt.Rows[n]["cpr_No"].ToString();
					}
					if(dt.Rows[n]["Item_Name"]!=null && dt.Rows[n]["Item_Name"].ToString()!="")
					{
					model.Item_Name=dt.Rows[n]["Item_Name"].ToString();
					}
					if(dt.Rows[n]["Item_Area"]!=null && dt.Rows[n]["Item_Area"].ToString()!="")
					{
					model.Item_Area=dt.Rows[n]["Item_Area"].ToString();
					}
					if(dt.Rows[n]["UpdateBy"]!=null && dt.Rows[n]["UpdateBy"].ToString()!="")
					{
					model.UpdateBy=dt.Rows[n]["UpdateBy"].ToString();
					}
					if(dt.Rows[n]["LastUpdate"]!=null && dt.Rows[n]["LastUpdate"].ToString()!="")
					{
						model.LastUpdate=DateTime.Parse(dt.Rows[n]["LastUpdate"].ToString());
					}
                    if (dt.Rows[n]["MoneyStatus"] != null && dt.Rows[n]["MoneyStatus"].ToString() != "")
                    {
                        model.MoneyStatus = dt.Rows[n]["MoneyStatus"].ToString();
                    }
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		/// <summary>
		/// 根据合同系统自增号查询合同分项信息
		/// </summary>
		/// <param name="coperationSysNo"></param>
		/// <returns></returns>
		public List<TG.Model.cm_SubCoperation> GetSubCoperationByCoperationSysNo(int coperationSysNo)
		{
			return dal.GetSubCoperationByCoperationSysNo(coperationSysNo);
		}
		#endregion  Method
	}
}

