﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;

namespace TG.BLL
{
    public class MapOfCity_gsBP
    {
        private MapOfCity_gsDA da = new MapOfCity_gsDA();
        /// <summary>
        /// 得到某一个省份的项目数
        /// </summary>
        /// <param name="provinceName"></param>
        /// <returns></returns>
        public List<MapOfChinaEntity> GetProjectCountByProvinceName()
        {
            return da.GetProjectCountByProvinceName();
        }

        /// <summary>
        /// 根据省份名称得到项目列表
        /// </summary>
        /// <param name="provinceName"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageCurrent"></param>
        /// <returns></returns>
        public List<ProjectForMap> GetProjectListByProvinceName(string provinceName, int pageSize, int pageCurrent)
        {
            return da.GetProjectListByProvinceName(provinceName, pageSize, pageCurrent);
        }
        public List<ProjectForMap> GetProjectListByProvinceName(string provinceName)
        {
            return da.GetProjectListByProvinceName(provinceName);
        }
        public ProjectDetailForMap GetProjectInfoByProjectSysNo(int projectSysNo)
        {
            return da.GetProjectInfoByProjectSysNo(projectSysNo);
        }
    }
}
