﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.BLL
{
    /// <summary>
    /// cm_ProjectDesignProcessValueConfig
    /// </summary>
    public partial class cm_ProjectDesignProcessValueConfig
    {
        private readonly TG.DAL.cm_ProjectDesignProcessValueConfig dal = new TG.DAL.cm_ProjectDesignProcessValueConfig();
        public cm_ProjectDesignProcessValueConfig()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectDesignProcessValueConfig model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectDesignProcessValueConfig model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectDesignProcessValueConfig GetModel(int ID)
        {

            return dal.GetModel(ID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_ProjectDesignProcessValueConfig GetModelByCache(int ID)
        {

            string CacheKey = "cm_ProjectDesignProcessValueConfigModel-" + ID;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(ID);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_ProjectDesignProcessValueConfig)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectDesignProcessValueConfig> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectDesignProcessValueConfig> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_ProjectDesignProcessValueConfig> modelList = new List<TG.Model.cm_ProjectDesignProcessValueConfig>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_ProjectDesignProcessValueConfig model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_ProjectDesignProcessValueConfig();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["Specialty"] != null && dt.Rows[n]["Specialty"].ToString() != "")
                    {
                        model.Specialty = dt.Rows[n]["Specialty"].ToString();
                    }
                    if (dt.Rows[n]["AuditPercent"] != null && dt.Rows[n]["AuditPercent"].ToString() != "")
                    {
                        model.AuditPercent = decimal.Parse(dt.Rows[n]["AuditPercent"].ToString());
                    }
                    if (dt.Rows[n]["SpecialtyHeadPercent"] != null && dt.Rows[n]["SpecialtyHeadPercent"].ToString() != "")
                    {
                        model.SpecialtyHeadPercent = decimal.Parse(dt.Rows[n]["SpecialtyHeadPercent"].ToString());
                    }
                    if (dt.Rows[n]["ProofreadPercent"] != null && dt.Rows[n]["ProofreadPercent"].ToString() != "")
                    {
                        model.ProofreadPercent = decimal.Parse(dt.Rows[n]["ProofreadPercent"].ToString());
                    }
                    if (dt.Rows[n]["DesignPercent"] != null && dt.Rows[n]["DesignPercent"].ToString() != "")
                    {
                        model.DesignPercent = decimal.Parse(dt.Rows[n]["DesignPercent"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}
