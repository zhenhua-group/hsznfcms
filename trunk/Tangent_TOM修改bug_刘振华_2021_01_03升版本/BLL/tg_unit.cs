﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
    /// <summary>
    /// tg_unit
    /// </summary>
    public partial class tg_unit
    {
        private readonly TG.DAL.tg_unit dal = new TG.DAL.tg_unit();
        public tg_unit()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.tg_unit model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.tg_unit model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int unit_ID)
        {

            return dal.Delete(unit_ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string unit_IDlist)
        {
            return dal.DeleteList(unit_IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.tg_unit GetModel(int unit_ID)
        {

            return dal.GetModel(unit_ID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.tg_unit GetModelByCache(int unit_ID)
        {

            string CacheKey = "tg_unitModel-" + unit_ID;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(unit_ID);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.tg_unit)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.tg_unit> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.tg_unit> DataTableToList(DataTable dt)
        {
            List<TG.Model.tg_unit> modelList = new List<TG.Model.tg_unit>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.tg_unit model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.tg_unit();
                    if (dt.Rows[n]["unit_ID"] != null && dt.Rows[n]["unit_ID"].ToString() != "")
                    {
                        model.unit_ID = int.Parse(dt.Rows[n]["unit_ID"].ToString());
                    }
                    if (dt.Rows[n]["unit_Name"] != null && dt.Rows[n]["unit_Name"].ToString() != "")
                    {
                        model.unit_Name = dt.Rows[n]["unit_Name"].ToString();
                    }
                    if (dt.Rows[n]["unit_Intro"] != null && dt.Rows[n]["unit_Intro"].ToString() != "")
                    {
                        model.unit_Intro = dt.Rows[n]["unit_Intro"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex, bool havetype)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex, havetype);
        }
        /// <summary>
        /// 分阶段获得数据
        /// </summary>
        /// <param name="strWhere">where语句</param>
        /// <param name="pagesize">每页数量</param>
        /// <param name="pageIndex">要得到第几页的数据</param>
        /// <returns>数据集</returns>
        public DataSet GetListByPage(string strWhere, int pagesize, int pageIndex)
        {
            return dal.GetListByPage(strWhere, pagesize, pageIndex);
        }
        #endregion  Method
    }
}

