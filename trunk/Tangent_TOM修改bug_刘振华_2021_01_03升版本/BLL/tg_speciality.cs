﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// tg_speciality
	/// </summary>
	public partial class tg_speciality
	{
		private readonly TG.DAL.tg_speciality dal=new TG.DAL.tg_speciality();
		public tg_speciality()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.tg_speciality model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_speciality model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int spe_ID)
		{
			
			return dal.Delete(spe_ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string spe_IDlist )
		{
			return dal.DeleteList(spe_IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_speciality GetModel(int spe_ID)
		{
			
			return dal.GetModel(spe_ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.tg_speciality GetModelByCache(int spe_ID)
		{
			
			string CacheKey = "tg_specialityModel-" + spe_ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(spe_ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.tg_speciality)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_speciality> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_speciality> DataTableToList(DataTable dt)
		{
			List<TG.Model.tg_speciality> modelList = new List<TG.Model.tg_speciality>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.tg_speciality model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.tg_speciality();
					if(dt.Rows[n]["spe_ID"]!=null && dt.Rows[n]["spe_ID"].ToString()!="")
					{
						model.spe_ID=int.Parse(dt.Rows[n]["spe_ID"].ToString());
					}
					if(dt.Rows[n]["spe_Name"]!=null && dt.Rows[n]["spe_Name"].ToString()!="")
					{
					model.spe_Name=dt.Rows[n]["spe_Name"].ToString();
					}
					if(dt.Rows[n]["spe_Intro"]!=null && dt.Rows[n]["spe_Intro"].ToString()!="")
					{
					model.spe_Intro=dt.Rows[n]["spe_Intro"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

