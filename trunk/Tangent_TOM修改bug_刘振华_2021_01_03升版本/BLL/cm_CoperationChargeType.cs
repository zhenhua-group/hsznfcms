﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
    /// <summary>
    /// cm_CoperationChargeType
    /// </summary>
    public partial class cm_CoperationChargeType
    {
        private readonly TG.DAL.cm_CoperationChargeType dal = new TG.DAL.cm_CoperationChargeType();
        public cm_CoperationChargeType()
        { }
        #region  Method


        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            return dal.Exists(ID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CoperationChargeType model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CoperationChargeType model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID, string type)
        {
            return dal.Delete(ID, type);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationChargeType GetModel(int ID)
        {
            return dal.GetModel(ID);
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationChargeType GetModel(int ID, string cprType)
        {
            return dal.GetModel(ID, cprType);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CoperationChargeType> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CoperationChargeType> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_CoperationChargeType> modelList = new List<TG.Model.cm_CoperationChargeType>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_CoperationChargeType model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_CoperationChargeType();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["cpr_ID"] != null && dt.Rows[n]["cpr_ID"].ToString() != "")
                    {
                        model.cpr_ID = decimal.Parse(dt.Rows[n]["cpr_ID"].ToString());
                    }
                    if (dt.Rows[n]["Times"] != null && dt.Rows[n]["Times"].ToString() != "")
                    {
                        model.Times = dt.Rows[n]["Times"].ToString();
                    }
                    if (dt.Rows[n]["persent"] != null && dt.Rows[n]["persent"].ToString() != "")
                    {
                        model.persent = decimal.Parse(dt.Rows[n]["persent"].ToString());
                    }
                    if (dt.Rows[n]["payCount"] != null && dt.Rows[n]["payCount"].ToString() != "")
                    {
                        model.payCount = decimal.Parse(dt.Rows[n]["payCount"].ToString());
                    }
                    if (dt.Rows[n]["paytime"] != null && dt.Rows[n]["paytime"].ToString() != "")
                    {
                        model.paytime = DateTime.Parse(dt.Rows[n]["paytime"].ToString());
                    }
                    if (dt.Rows[n]["payShiCount"] != null && dt.Rows[n]["payShiCount"].ToString() != "")
                    {
                        model.payShiCount = decimal.Parse(dt.Rows[n]["payShiCount"].ToString());
                    }
                    if (dt.Rows[n]["acceptuser"] != null && dt.Rows[n]["acceptuser"].ToString() != "")
                    {
                        model.acceptuser = dt.Rows[n]["acceptuser"].ToString();
                    }
                    if (dt.Rows[n]["mark"] != null && dt.Rows[n]["mark"].ToString() != "")
                    {
                        model.mark = dt.Rows[n]["mark"].ToString();
                    }
                    if (dt.Rows[n]["paytype"] != null && dt.Rows[n]["paytype"].ToString() != "")
                    {
                        model.paytype = dt.Rows[n]["paytype"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }

        #endregion  Method
    }
}

