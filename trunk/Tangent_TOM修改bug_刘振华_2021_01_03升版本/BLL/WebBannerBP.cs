﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;

namespace TG.BLL
{
        public class WebBannerBP
        {
                private WebBannerDA da = new WebBannerDA();

                public int InsertWebBanner(WebBannerViewEntity dataEntity)
                {
                        int count = 0;
                        count = da.InsertWebBanner(dataEntity);

                        dataEntity.ItemList.ForEach((item) =>
                        {
                                item.WebBannerSysNo = count;
                                da.InsertWebBannerItem(item);
                        });
                        return count;
                }

                public int InsertWorkLog(WorkLogViewEntity dataEntity)
                {
                        return da.InsertWorkLog(dataEntity);
                }

                public int UpdateWebBanner(WebBannerViewEntity dataEntity)
                {
                        int count = da.UpdateWebBanner(dataEntity);
                        //删除所有关于这个WebBanner的数据
                        da.DeleteWebBannerItemByWebBannerSysNo(dataEntity.SysNo);

                        dataEntity.ItemList.ForEach((user) =>
                        {
                                da.InsertWebBannerItem(user);
                        });
                        
                        return count;
                }

                public int UpdateWorkLog(WorkLogViewEntity dataEntity)
                {
                        return da.UpdateWorkLog(dataEntity);
                }

                public WorkLogViewEntity GetWorkLog(int sysNo)
                {
                        return da.GetWorkLog(sysNo);
                }
                public WebBannerViewEntity GetWebBanner(int sysNo)
                {
                        WebBannerViewEntity webBanner = da.GetWebBanner(sysNo);

                        webBanner.ItemList = da.GetWebBannerItemList(webBanner.SysNo);

                        return webBanner;
                }

                public List<WebBannerItemViewEntity> GetWebBannerListByWebBannerSysNo(int webBannerSysNo)
                {
                        return da.GetWebBannerItemList(webBannerSysNo);
                }

                public int InsertWebBannerItem(WebBannerItemViewEntity webBannerItemViewEntity)
                {

                        return da.InsertWebBannerItem(webBannerItemViewEntity);
                }

                public int DeleteWebBannerItem(WebBannerItemViewEntity webBannerItemViewEntity)
                {
                        return da.DeleteWebBannerItem(webBannerItemViewEntity);
                }

                public int DeleteWebBannerItemByWebBannerSysNo(int webBannerSysNo)
                {
                        return da.DeleteWebBannerItemByWebBannerSysNo(webBannerSysNo);
                }
        }
}
