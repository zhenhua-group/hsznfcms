﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace TG.BLL
{
    public class ImportFinancialData
    {
        private TG.DAL.ImportFinancialData dal = new DAL.ImportFinancialData();
        /// <summary>
        /// 批量插入数据
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public int InsertFinancial(DataTable dt, string type)
        {

            //给表格赋标题
            dt.Columns[0].ColumnName = "Month";
            dt.Columns[1].ColumnName = "FieldOfB";
            dt.Columns[2].ColumnName = "VoucherNo";
            dt.Columns[3].ColumnName = "Name";
            dt.Columns[4].ColumnName = "FieldOfF";
            dt.Columns[5].ColumnName = "Account";
            dt.Columns[6].ColumnName = "FieldOfH";
            dt.Columns[7].ColumnName = "FileOfI";


            //查询凭证号
            DataTable dtVoucherNo = dal.GetVoucherNo(type).Tables[0];

            //过滤重复的
            if (dtVoucherNo.Rows.Count > 0)
            {
                //删除上传重复的值
                List<DataRow> removelist = new List<DataRow>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bool rowdataisnull = true;
                    for (int j = 0; j < dtVoucherNo.Rows.Count; j++)
                    {

                        if (dt.Rows[i]["VoucherNo"].ToString().Trim().Equals(dtVoucherNo.Rows[j][0].ToString().Trim()))
                        {
                            rowdataisnull = false;
                            break;
                        }

                    }
                    if (!rowdataisnull)
                    {
                        removelist.Add(dt.Rows[i]);
                    }

                }
                for (int i = 0; i < removelist.Count; i++)
                {
                    dt.Rows.Remove(removelist[i]);
                }
            }

            if (dt.Rows.Count > 0)
            {
                // 过滤 取得月份
                var dtOrderList = new DataView(dt).ToTable("ORDER_LIST", true, "Month");

                //最终数据
                DataTable dtData = new DataTable();
                dtData.Columns.Add("Month", typeof(string));
                dtData.Columns.Add("FieldOfB", typeof(string));
                dtData.Columns.Add("VoucherNo", typeof(string));
                dtData.Columns.Add("Name", typeof(string));
                dtData.Columns.Add("FieldOfF", typeof(string));
                dtData.Columns.Add("Account", typeof(string));
                dtData.Columns.Add("FieldOfH", typeof(string));
                dtData.Columns.Add("FileOfI", typeof(string));
                dtData.Columns.Add("type", typeof(string));
                dtData.Columns.Add("Year", typeof(string));
                dtData.Columns.Add("FinancialNo", typeof(Int32));

                string year = DateTime.Now.Year.ToString();
                int maxFinancialID = 0;

                //循环月份
                for (int i = 0; i < dtOrderList.Rows.Count; i++)
                {
                    string mounth = dtOrderList.Rows[i][0].ToString();
                    //取得最大值
                    maxFinancialID = dal.GetMaxFinancialID(mounth, type);

                    //过滤 是当前月份的值
                    var dataTemp = new DataView(dt) { RowFilter = "Month='" + mounth + "'" }.ToTable();

                    //给datatable添加两列 赋予默认值
                    dataTemp.Columns.Add("type", typeof(string));
                    dataTemp.Columns.Add("Year", typeof(string));
                    dataTemp.Columns.Add("FinancialNo", typeof(Int32));

                    //循环给table添加单据号
                    foreach (DataRow row in dataTemp.Rows)
                    {
                        row["type"] = type;
                        row["Year"] = year;
                        row["FinancialNo"] = maxFinancialID;
                        maxFinancialID = maxFinancialID + 1;
                        dtData.ImportRow(row);
                    }

                }

                return dal.InsertFinancial(dtData);
            }
            //过滤完没有数据，说明数据库已上传该数据
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// 得到最大的ID
        /// </summary>
        /// <returns></returns>
        public int GetMaxFinancialID(string mounth, string type)
        {
            return dal.GetMaxFinancialID(mounth, type);
        }

        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListByPageProc(query, startIndex, endIndex);
        }

        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return dal.GetListPageProcCount(query);
        }

        /// <summary>
        /// 得到凭证单号
        /// </summary>
        /// <param name="name"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public DataSet GetFinanalNum(string name, decimal account,string inTime,string unit)
        {
            return dal.GetFinanalNum(name, account,inTime,unit);
        }

        public void UpdateFinanalStatus(int ID)
        {
            dal.UpdateFinanalStatus(ID);
        }
  /// <summary>
        /// 获取所有合同年份
        /// </summary>
        /// <returns></returns>
        public List<string> GetCoperationYear()
        {
            return dal.GetCoperationYear();
        }
    }
}
