﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;

namespace TG.BLL
{
    public class LoadLeftMenuBP
    {
        private LoadLeftMenuDA da = new LoadLeftMenuDA();

        /// <summary>
        /// 根据点击类型和用户SysNo得到该用户的LeftMenu
        /// </summary>
        /// <param name="leftMenuType"></param>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public List<LeftMenuViewEntity> GetLeftMenuList(LeftMenuType leftMenuType, int userSysNo)
        {
            List<TG.Model.cm_Role> roleList = new TG.BLL.cm_Role().GetRoleList();

            List<TG.Model.cm_Role> resultRoleList = new List<TG.Model.cm_Role>();

            roleList.ForEach((role) =>
            {
                string[] userSysNoArray = role.Users.Split(',');

                for (int i = 0; i < userSysNoArray.Length; i++)
                {
                    if (userSysNo == int.Parse(userSysNoArray[i]))
                    {
                        resultRoleList.Add(role);
                        break;
                    }
                }
            });

            //如果查询出来没有属于任何一个角色，则默认判断他属于普通员工
            if (resultRoleList.Count == 0)
            {
                resultRoleList.Add((from role in roleList where role.RoleName == "普通员工" select role).FirstOrDefault());
            }

            List<LeftMenuViewEntity> leftMenuList = da.GetLeftMenuList(leftMenuType);

            List<LeftMenuViewEntity> leftMenuResultList = new List<LeftMenuViewEntity>();

            int[] currentUserRoleArray = (from userRole in resultRoleList select userRole.SysNo).ToArray<int>();

            leftMenuList.ForEach((leftMune) =>
            {
                string[] roleSysNoArray = leftMune.Role.Split(',');
                for (int i = 0; i < roleSysNoArray.Length; i++)
                {
                    if (currentUserRoleArray.Contains(int.Parse(roleSysNoArray[i])))
                    {
                        if (!leftMenuResultList.Contains(leftMune))
                        {
                            leftMenuResultList.Add(leftMune);
                        }
                        break;
                    }
                }
            });

            TG.BLL.cm_Role roleBP = new cm_Role();

            leftMenuResultList.ForEach((leftMenu) =>
            {
                string[] roleSysNoArray = leftMenu.Role.Split(',');
                string roleNameString = "";
                foreach (string sysNo in roleSysNoArray)
                {
                    TG.Model.cm_Role role = roleBP.GetRole(int.Parse(sysNo));

                    roleNameString += role == null ? "" : role.RoleName + ",";
                }
                leftMenu.RoleNameString = roleNameString.Substring(0, roleNameString.Length - 1);
            });
            return leftMenuResultList;
        }

        public List<LeftMenuViewEntity> GetLeftMenuListAll(LeftMenuType leftMenuType)
        {
            List<LeftMenuViewEntity> resultList = da.GetLeftMenuList(leftMenuType);

            TG.BLL.cm_Role roleBP = new cm_Role();

            resultList.ForEach((leftMenu) =>
            {
                string[] roleSysNoArray = leftMenu.Role.Split(',');
                string roleNameString = "";
                if (!string.IsNullOrEmpty(leftMenu.Role) && roleSysNoArray.Length != 0)
                {
                    foreach (string sysNo in roleSysNoArray)
                    {
                        TG.Model.cm_Role role = roleBP.GetRole(int.Parse(sysNo));

                        roleNameString += role == null ? "" : role.RoleName + "<br/>";
                    }
                    leftMenu.RoleNameString = roleNameString.Substring(0, roleNameString.Length - 1);
                }
            });
            return resultList;
        }

        public int SaveLeftMenu(LeftMenuViewEntity leftMenuViewEntity)
        {
            return da.SaveLeftMenu(leftMenuViewEntity);
        }

        public LeftMenuViewEntity GetLeftMenuViewEntity(int sysNo)
        {
            return da.GetLeftMenuViewEntity(sysNo);
        }

        public int UpDateLeftMenu(LeftMenuViewEntity leftMenuViewEntity)
        {
            return da.UpDateLeftMenu(leftMenuViewEntity);
        }

        public int DeleteLeftMenu(int sysNo)
        {
            return da.DeleteLeftMenu(sysNo);
        }
    }
}
