﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public class CommClassOperator
    {
        TG.Common.TableOperator bll=new TG.Common.TableOperator();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="icolumn"></param>
        /// <param name="configs"></param>
        /// <param name="rowconfigs"></param>
        /// <param name="htmls"></param>
        /// <returns></returns>
        public string CreateTableRow(int icolumn,string[] configs,string[] rowconfigs,string[] htmls)
        {
            return bll.CreateTableRow(icolumn, configs, rowconfigs, htmls);
        }
    }
}
