﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_RelativeContractInfo
	/// </summary>
	public partial class cm_RelativeContractInfo
	{
		private readonly TG.DAL.cm_RelativeContractInfo dal=new TG.DAL.cm_RelativeContractInfo();
		public cm_RelativeContractInfo()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int RC_Id)
		{
			return dal.Exists(RC_Id);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_RelativeContractInfo model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_RelativeContractInfo model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int RC_Id)
		{
			
			return dal.Delete(RC_Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string RC_Idlist )
		{
			return dal.DeleteList(RC_Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_RelativeContractInfo GetModel(int RC_Id)
		{
			
			return dal.GetModel(RC_Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_RelativeContractInfo GetModelByCache(int RC_Id)
		{
			
			string CacheKey = "cm_RelativeContractInfoModel-" + RC_Id;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(RC_Id);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_RelativeContractInfo)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_RelativeContractInfo> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_RelativeContractInfo> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_RelativeContractInfo> modelList = new List<TG.Model.cm_RelativeContractInfo>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_RelativeContractInfo model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_RelativeContractInfo();
					if(dt.Rows[n]["RC_Id"]!=null && dt.Rows[n]["RC_Id"].ToString()!="")
					{
						model.RC_Id=int.Parse(dt.Rows[n]["RC_Id"].ToString());
					}
					if(dt.Rows[n]["Cst_Id"]!=null && dt.Rows[n]["Cst_Id"].ToString()!="")
					{
						model.Cst_Id=int.Parse(dt.Rows[n]["Cst_Id"].ToString());
					}
					if(dt.Rows[n]["ContractId"]!=null && dt.Rows[n]["ContractId"].ToString()!="")
					{
					model.ContractId=dt.Rows[n]["ContractId"].ToString();
					}
					if(dt.Rows[n]["ContractNo"]!=null && dt.Rows[n]["ContractNo"].ToString()!="")
					{
					model.ContractNo=dt.Rows[n]["ContractNo"].ToString();
					}
					if(dt.Rows[n]["ContractName"]!=null && dt.Rows[n]["ContractName"].ToString()!="")
					{
					model.ContractName=dt.Rows[n]["ContractName"].ToString();
					}
					if(dt.Rows[n]["ContractAccount"]!=null && dt.Rows[n]["ContractAccount"].ToString()!="")
					{
						model.ContractAccount=decimal.Parse(dt.Rows[n]["ContractAccount"].ToString());
					}
					if(dt.Rows[n]["SigneDate"]!=null && dt.Rows[n]["SigneDate"].ToString()!="")
					{
						model.SigneDate=DateTime.Parse(dt.Rows[n]["SigneDate"].ToString());
					}
					if(dt.Rows[n]["CompleteDate"]!=null && dt.Rows[n]["CompleteDate"].ToString()!="")
					{
						model.CompleteDate=DateTime.Parse(dt.Rows[n]["CompleteDate"].ToString());
					}
					if(dt.Rows[n]["Functionary"]!=null && dt.Rows[n]["Functionary"].ToString()!="")
					{
					model.Functionary=dt.Rows[n]["Functionary"].ToString();
					}
					if(dt.Rows[n]["UpdateBy"]!=null && dt.Rows[n]["UpdateBy"].ToString()!="")
					{
						model.UpdateBy=int.Parse(dt.Rows[n]["UpdateBy"].ToString());
					}
					if(dt.Rows[n]["LastUpdate"]!=null && dt.Rows[n]["LastUpdate"].ToString()!="")
					{
						model.LastUpdate=DateTime.Parse(dt.Rows[n]["LastUpdate"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

