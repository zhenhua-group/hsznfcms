﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;

namespace TG.BLL
{
    public class ChooseCustomerBP
    {
        private ChooseCustomerDA da = new ChooseCustomerDA();

        public ResultList<CustomerViewEntity> GetCustomerRecordList(ChooseCustomerQueryEntity queryEntity)
        {
            return da.GetCustomerRecordList(queryEntity);
        }
    }
}
