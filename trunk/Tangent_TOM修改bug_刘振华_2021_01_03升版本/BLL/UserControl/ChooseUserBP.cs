﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model.UserControl;

namespace TG.BLL
{
    public class ChooseUserBP
    {
        private TG.DAL.ChooseUserDA da = new DAL.ChooseUserDA();

        public List<ChooseUserViewEntity> GetUserList(ChooseUserQueryEntity queryEntity, out int totalCount)
        {
            return da.GetUserList(queryEntity, out totalCount);
        }
        /// <summary>
        /// 产值用
        /// </summary>
        /// <param name="queryEntity"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public List<ChooseProjectValueUserViewEntity> GetUserList(ChooseProjectValueUserEntity queryEntity, out int totalCount)
        {
            return da.GetUserList(queryEntity, out totalCount);
        }
        /// <summary>
        /// 外聘人员
        /// </summary>
        /// <param name="queryEntity"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public List<ChooseExternalUserViewEntity> GetUserList(ChooseExternalUserEntity queryEntity, out int totalCount)
        {
            return da.GetUserList(queryEntity, out totalCount);
        }

          /// <summary>
        /// 新增用户信息
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public int AddExternalUserInfo(AddExternalUser userInfo)
        {
            return da.AddExternalUserInfo(userInfo);
        }
    }
}
