﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
    /// <summary>
    /// tg_member
    /// </summary>
    public partial class InterView
    {
        private readonly TG.DAL.InterView dal = new TG.DAL.InterView();
        public InterView()
        { }
        #region  Method
        /// <summary>
        /// 训练主表里插入一条记录
        /// </summary>
        public int AddInterview(TG.Model.InterView model)
        {
            return dal.AddInterview(model);
        }
        public int deleteInterview(int Interviewid)
        {
            return dal.deleteInterview(Interviewid);
        }
        public List<string> GetTrainingYear()
        {
            return dal.GetTrainingYear();
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateInterview(TG.Model.InterView model)
        {
            return dal.UpdateInterview(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateTrainingInfoDetail(int id,List<TG.Model.Training> list)
        {
            return dal.UpdateTrainingInfoDetail(id,list);
        }
        /// <summary>
        /// 检查附件是否存在
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="filename">附件名称</param>
        /// <returns></returns>
        public bool Exists(int ID,string filename)
        {
            return dal.Exists(ID, filename);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Delete(int ID)
        {
            return dal.Delete(ID);
        }

        public bool ExistsTemp_No(string ID)
        {
            return dal.ExistsTemp_No(ID);
        }

        #endregion  Method
    }
}

