﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.Model;
using System.Data.SqlClient;

namespace TG.BLL
{
    public class cm_ProjectPlanBP
    {
        private TG.DAL.cm_ProjectPlanDA da = new TG.DAL.cm_ProjectPlanDA();

        /// <summary>
        /// 为提交评审的
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public DataSet GetProjectPlanListNo(string where)
        {
            var dataset1 = TransformSource.ConvertToDataSet<ProjectPlanListViewEntityNo>(da.GetProjectPlanListNo(where));
            return dataset1;
        }
        public DataSet GetProjectPlanListNo(string query, int startIndex, int endIndex)
        {
            var dataset1 = TransformSource.ConvertToDataSet<ProjectPlanListViewEntityNo>(da.GetProjectPlanListNo(query, startIndex, endIndex));
            return dataset1;
        }
        /// <summary>
        /// 返回需要分页的记录数
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageProcCount(string query)
        {
            return da.GetListPageProcCount(query);
        }
        /// <summary>
        /// 已提交评审的
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public DataSet GetProjectPlanListYes(string where)
        {
            var dataset2 = TransformSource.ConvertToDataSet<ProjectPlanListViewEntityYes>(da.GetProjectPlanListYes(where));
            return dataset2;
        }
        public DataSet GetProjectPlanListYes(string query, int startIndex, int endIndex)
        {
            var dataset2 = TransformSource.ConvertToDataSet<ProjectPlanListViewEntityYes>(da.GetProjectPlanListYes(query, startIndex, endIndex));
            return dataset2;
        }

        /// <summary>
        /// 返回需要分页的记录数
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageProcYesCount(string query)
        {
            return da.GetListPageProcYesCount(query);
        }
        /// <summary>
        /// 得到所有部门结果
        /// </summary>
        /// <returns></returns>
        public List<TreeDepartmentEntity> GetTreeDepartmentList()
        {
            List<TreeDepartmentEntity> departmentList = da.GetTreeDepartmentList();

            departmentList.ForEach((department) =>
            {
                department.Users = da.GetTreeUserListByDepartmentSysNo(department.DepartmentSysNo);
            });

            return departmentList;
        }

        /// <summary>
        /// 添加项目策划人员信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertPlanUser(TG.Model.PlanProjectViewEntity dataEntity)
        {
            dataEntity.RoleUser.ForEach((roleUser) =>
            {
                //20130830 by long 
                PlanUserViewEntity memUser = dataEntity.Member.Find(delegate(PlanUserViewEntity user) { return user.UserSysNo == roleUser.UserSysNo; });
                if (memUser != null)
                {
                    string roleSysNoString = dataEntity.Member.Find(delegate(PlanUserViewEntity user) { return user.UserSysNo == roleUser.UserSysNo; }).RoleSysNo += roleUser.RoleSysNo + ",";
                }
            });

            dataEntity.Member.ForEach((member) =>
            {
                if (!string.IsNullOrEmpty(member.RoleSysNo))
                    member.RoleSysNo = member.RoleSysNo.Substring(0, member.RoleSysNo.Length - 1);
            });

            int memCount = dataEntity.Member.Count;
            int designCount = dataEntity.DesignMember.Count;
            //判断设计人员
            for (int j = 0; j < designCount; j++)
            {
                bool isExsit = false;
                for (int i = 0; i < memCount; i++)
                {
                    //出现重复
                    if (dataEntity.Member[i].UserSysNo == dataEntity.DesignMember[j].UserSysNo)
                    {
                        dataEntity.Member[i].IsDesigner = 1;
                        isExsit = true;
                        break;
                    }
                }
                //不在
                if (!isExsit)
                {
                    dataEntity.Member.Add(dataEntity.DesignMember[j]);
                }
            }
            return da.InsertPlanUser(dataEntity);
        }

        public DataSet GetPlanUsers(int projectSysNo)
        {
            TG.Model.cm_Project cmProject = new TG.BLL.cm_Project().GetModel(projectSysNo);
            return da.GetPlanUsers(cmProject.ReferenceSysNo);
        }

        public cm_ProjectPlanAuditConfigEntity GetProjectPlanAuditConfigEntity(int position)
        {
            return da.GetProjectPlanAuditConfigEntity(position);
        }

        public List<string> GetAuditProcessDescription()
        {
            return da.GetAuditProcessDescription();
        }

        /// <summary>
        /// 得到所有的ProjectPlanUsers
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        public List<ProjectPlanRole> GetProjectPlanRoleAndUsers(int projectSysNo)
        {
            TG.Model.cm_Project cmProject = new TG.BLL.cm_Project().GetModel(projectSysNo);

            List<ProjectPlanRole> projectPlanRoleList = da.GetProjectPlanRole();

            projectPlanRoleList.ForEach((role) =>
            {
                role.Users = da.GetProjectPlanUsers(cmProject.ReferenceSysNo == 0 ? -1 : cmProject.ReferenceSysNo, role.SysNo);
            });

            return projectPlanRoleList;
        }

        //设计人员信息
        public ProjectDesignPlanRole GetProjectPlanDesignRoleAndUsers(int projectSysNo)
        {
            TG.Model.cm_Project cmProject = new TG.BLL.cm_Project().GetModel(projectSysNo);
            ProjectDesignPlanRole projectDesignPlanRole = new ProjectDesignPlanRole();
            projectDesignPlanRole.Users = da.GetProjectPlanUsers(cmProject.ReferenceSysNo == 0 ? -1 : cmProject.ReferenceSysNo);
            return projectDesignPlanRole;
        }

        public List<ProjectPlanSubItem> GetProjectPlanSubitemList(int projectSysNo)
        {
            TG.Model.cm_Project cmProject = new TG.BLL.cm_Project().GetModel(projectSysNo);
            if (cmProject.ReferenceSysNo == 0)
            {
                return new List<ProjectPlanSubItem>();
            }
            else
            {
                return da.GetProjectPlanSubitemList(cmProject.ReferenceSysNo);
            }

        }

        public int InsertProjectPlanAudit(cm_ProjectPlanAuditEntity dataEntity)
        {
            return da.InsertProjectPlanAudit(dataEntity);
        }

        public bool IsExistsProjectPlanAudit(int projectSysNo)
        {
            return da.IsExistsProjectPlanAudit(projectSysNo);
        }
        //--------------修改
        public int UpdateProjectPlanEditRecord(cm_ProjectPlanEditRecordEntity dataEntity)
        {
            return da.UpdateProjectPlanEditRecord(dataEntity);
        }
        public cm_ProjectPlanEditRecordEntity GetProjectPlanEditRecordEntity(cm_ProjectPlanEditRecordEntityQueryEntity queryEntity)
        {
            return da.GetProjectPlanEditRecordEntity(queryEntity);
        }
        public int InsertProjectPlanEdit(cm_ProjectPlanEditRecordEntity dataEntity)
        {
            return da.InsertProjectPlanEdit(dataEntity);
        }
        public bool IsExistsProjectPlanEditRecord(int projectSysNo)
        {
            return da.IsExistsProjectPlanEditRecord(projectSysNo);
        }
        //-------------修改
        public int GetProcessRoleSysNo(string auditStatus)
        {
            return da.GetProcessRoleSysNo(auditStatus);
        }

        public int DeleteProjectPlanSubItem(int subItemSysNo)
        {
            return da.DeleteProjectPlanSubItem(subItemSysNo);
        }

        public cm_ProjectPlanAuditEntity GetProjectPlanAuditEntity(cm_ProjectPlanAuditQueryEntity queryEntity)
        {
            return da.GetProjectPlanAuditEntity(queryEntity);
        }

        public int UpdateProjectPlanAudit(cm_ProjectPlanAuditEntity dataEntity)
        {
            return da.UpdateProjectPlanAudit(dataEntity);
        }

        public ProjectPlanViewParameterEntity GetProjectPlanViewParameterEntity(int projectSysNo)
        {
            return da.GetProjectPlanViewParameterEntity(projectSysNo);
        }
        /// <summary>
        /// 获取通过审批的项目
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetPassAuditPro(string strWhere)
        {
            return da.GetPassAuditPro(strWhere);
        }
        /// <summary>
        /// 新增的方法，判断这个项目的参与人员是不是已经有图纸信息了
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <param name="UserSysNo"></param>
        /// <returns></returns>
        public string IsTruePack(int projectSysNo, int UserSysNo)
        {
            return da.IsTruePack(projectSysNo, UserSysNo);
        }
    }
}
