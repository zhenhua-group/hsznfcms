﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public class cm_ProjectWageBenefits
    {
        private readonly TG.DAL.cm_ProjectWageBenefits dal = new TG.DAL.cm_ProjectWageBenefits();

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectWageBenefits model)
        {
            return dal.Add(model);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_ProjectWageBenefits model)
        {
            return dal.Update(model);
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectWageBenefits GetModel(int ID)
        {
            return dal.GetModel(ID);
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectWageBenefits GetModelByYear(int ID, string year)
        {
            return dal.GetModelByYear(ID, year);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetProjectWageBenefitsList(string query, string query1, string query2, string query3)
        {
            return dal.GetProjectWageBenefitsList(query, query1, query2, query3);
        }
    }
}
