﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public partial class cm_FundPercentSet
    {
        private readonly TG.DAL.cm_FundPercentSet dal = new TG.DAL.cm_FundPercentSet();
        public cm_FundPercentSet()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_FundPercentSet model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_FundPercentSet model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_FundPercentSet GetModel(int ID)
        {
            return dal.GetModel(ID);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_FundPercentSet> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_FundPercentSet> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_FundPercentSet> modelList = new List<TG.Model.cm_FundPercentSet>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_FundPercentSet model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_FundPercentSet();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["unit_id"] != null && dt.Rows[n]["unit_id"].ToString() != "")
                    {
                        model.unit_id = int.Parse(dt.Rows[n]["unit_id"].ToString());
                    }
                    if (dt.Rows[n]["fund_Year"] != null && dt.Rows[n]["fund_Year"].ToString() != "")
                    {
                        model.fund_Year = dt.Rows[n]["fund_Year"].ToString();
                    }
                    if (dt.Rows[n]["fund_percent"] != null && dt.Rows[n]["fund_percent"].ToString() != "")
                    {
                        model.fund_percent = decimal.Parse(dt.Rows[n]["fund_percent"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex,string type)
        {
            return dal.GetListByPageProc(query, startIndex, endIndex, type);
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query,string type)
        {
            return dal.GetListPageProcCount(query,type);
        }
          /// <summary>
        /// 得到收入成本结余明细表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetUnitRevenueCostBalance(string strWhere, string year)
        {
            return dal.GetUnitRevenueCostBalance(strWhere, year);
        }
        #endregion  Method
    }
}
