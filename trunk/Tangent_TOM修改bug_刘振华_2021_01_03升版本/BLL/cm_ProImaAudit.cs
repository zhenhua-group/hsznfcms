﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Model;
namespace TG.BLL
{
    /// <summary>
    /// cm_ProImaAudit
    /// </summary>
    public partial class cm_ProImaAudit
    {
        private readonly TG.DAL.cm_ProImaAudit dal = new TG.DAL.cm_ProImaAudit();
        public cm_ProImaAudit()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int SysNo)
        {
            return dal.Exists(SysNo);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProImaAudit model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProImaAudit model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SysNo)
        {

            return dal.Delete(SysNo);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string SysNolist)
        {
            return dal.DeleteList(SysNolist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProImaAudit GetModel(int SysNo)
        {

            return dal.GetModel(SysNo);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProImaAudit> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProImaAudit> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_ProImaAudit> modelList = new List<TG.Model.cm_ProImaAudit>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_ProImaAudit model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_ProImaAudit();
                    if (dt.Rows[n]["SysNo"] != null && dt.Rows[n]["SysNo"].ToString() != "")
                    {
                        model.SysNo = int.Parse(dt.Rows[n]["SysNo"].ToString());
                    }
                    if (dt.Rows[n]["ProjectSysNo"] != null && dt.Rows[n]["ProjectSysNo"].ToString() != "")
                    {
                        model.ProjectSysNo = int.Parse(dt.Rows[n]["ProjectSysNo"].ToString());
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = dt.Rows[n]["Status"].ToString();
                    }
                    if (dt.Rows[n]["OneSuggestion"] != null && dt.Rows[n]["OneSuggestion"].ToString() != "")
                    {
                        model.OneSuggestion = dt.Rows[n]["OneSuggestion"].ToString();
                    }
                    if (dt.Rows[n]["TwoSuggestion"] != null && dt.Rows[n]["TwoSuggestion"].ToString() != "")
                    {
                        model.TwoSuggestion = dt.Rows[n]["TwoSuggestion"].ToString();
                    }
                    if (dt.Rows[n]["AuditUser"] != null && dt.Rows[n]["AuditUser"].ToString() != "")
                    {
                        model.AuditUser = dt.Rows[n]["AuditUser"].ToString();
                    }
                    if (dt.Rows[n]["AudtiDate"] != null && dt.Rows[n]["AudtiDate"].ToString() != "")
                    {
                        model.AuditDate = dt.Rows[n]["AudtiDate"].ToString();
                    }
                    if (dt.Rows[n]["InDate"] != null && dt.Rows[n]["InDate"].ToString() != "")
                    {
                        model.InDate = DateTime.Parse(dt.Rows[n]["InDate"].ToString());
                    }
                    if (dt.Rows[n]["InUser"] != null && dt.Rows[n]["InUser"].ToString() != "")
                    {
                        model.InUser = int.Parse(dt.Rows[n]["InUser"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }

        /// <summary>
        /// 得到该审核记录当前的审核角色
        /// </summary>
        public TG.Model.cm_ProImageAuditConfig GetCoperationAuditConfigByPostion(int position)
        {
            return dal.GetCoperationAuditConfigByPostion(position);
        }


        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<TG.Model.cm_ProImaAuditListView> GetProjectImgAuditView(string whereSql,int startIndex,int endIndex)
        {
            //查询合同基本信息
            List<TG.Model.cm_ProImaAuditListView> proImageAuditList = dal.GetProjectImgAuditView(whereSql, startIndex,endIndex);
            //查询合同审核记录
            foreach (var pro in proImageAuditList)
            {
                TG.Model.cm_ProImaAudit proIma = dal.GetModelByProSysNo(pro.pro_ID);

                pro.cm_ProImaAuditEntity = proIma;

                pro.AuditRecordSysNo = proIma == null ? 0 : proIma.SysNo;
                pro.AuditStatus = proIma == null ? "" : proIma.Status;
            }
            return proImageAuditList;
        }

        #endregion  Method
    }
}

