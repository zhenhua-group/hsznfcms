﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace TG.BLL
{
    public class ProjectValuePagingParameters
    {
        public ProjectValuePagingParameters()
        { }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string OrderBy { get; set; }

        public string Sort { get; set; }

        public string Where { get; set; }

        public string ProcedureName { get; set; }
        public string Type { get; set; }
        //年份
        public string Year { get; set; }
        public int Total
        {
            get
            {
                if (_parameters.Count > 0)
                {
                    return (int)_parameters[5].Value;
                }
                return 0;
            }
        }

        private IList<SqlParameter> _parameters = new List<SqlParameter>();

        public SqlParameter[] ProcedureParameters
        {
            get
            {
                if (_parameters.Count == 0)
                {
                    _parameters.Add(new SqlParameter("PageIndex", PageIndex));
                    _parameters.Add(new SqlParameter("PageSize", PageSize));
                    _parameters.Add(new SqlParameter("Where", Where));
                    _parameters.Add(new SqlParameter("OrderBy", OrderBy));
                    _parameters.Add(new SqlParameter("Sort", Sort));
                    var outputTotal = new SqlParameter
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "Total",
                        DbType = DbType.Int32,
                        Size = 4
                    };

                    _parameters.Add(outputTotal);

                    _parameters.Add(new SqlParameter("Type", Type));
                    //年份
                    _parameters.Add(new SqlParameter("year", Year));
                }

                return _parameters.ToArray();
            }
            set { _parameters = value; }
        }
    }

    public class ProjectValueLendBorrowParameters
    {
        public ProjectValueLendBorrowParameters()
        { }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string OrderBy { get; set; }

        public string Sort { get; set; }

        public string Where { get; set; }

        public string ProcedureName { get; set; }
       
        //年份
        public string Year { get; set; }
        public int Total
        {
            get
            {
                if (_parameters.Count > 0)
                {
                    return (int)_parameters[5].Value;
                }
                return 0;
            }
        }

        private IList<SqlParameter> _parameters = new List<SqlParameter>();

        public SqlParameter[] ProcedureParameters
        {
            get
            {
                if (_parameters.Count == 0)
                {
                    _parameters.Add(new SqlParameter("PageIndex", PageIndex));
                    _parameters.Add(new SqlParameter("PageSize", PageSize));
                    _parameters.Add(new SqlParameter("Where", Where));
                    _parameters.Add(new SqlParameter("OrderBy", OrderBy));
                    _parameters.Add(new SqlParameter("Sort", Sort));
                    var outputTotal = new SqlParameter
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "Total",
                        DbType = DbType.Int32,
                        Size = 4
                    };

                    _parameters.Add(outputTotal);

                  
                    //年份
                    _parameters.Add(new SqlParameter("year", Year));
                }

                return _parameters.ToArray();
            }
            set { _parameters = value; }
        }
    }
}
