﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
using System.Data.SqlClient;
namespace TG.BLL
{
    /// <summary>
    /// cm_projectNumber
    /// </summary>
    public partial class cm_projectNumber
    {
        private readonly TG.DAL.cm_projectNumber dal = new TG.DAL.cm_projectNumber();
        public cm_projectNumber()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int proNum_id)
        {
            return dal.Exists(proNum_id);
        }

        public bool Exists(string pronum)
        {
            return dal.Exists(pronum);
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_projectNumber model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_projectNumber model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int proNum_id)
        {

            return dal.Delete(proNum_id);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string proNum_idlist)
        {
            return dal.DeleteList(proNum_idlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_projectNumber GetModel(int proNum_id)
        {

            return dal.GetModel(proNum_id);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        public string GetProjectJobNumberByProjectSysNo(int projectSysNo)
        {
            return dal.GetProjectJobNumberByProjectSysNo(projectSysNo);
        }
        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_projectNumber GetModelByCache(int proNum_id)
        {

            string CacheKey = "cm_projectNumberModel-" + proNum_id;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(proNum_id);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_projectNumber)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_projectNumber> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        //查询已存在的工号
        public List<string> GetExistsCoperationNo()
        {
            return dal.GetExistsCoperationNo();
        }
       
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_projectNumber> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_projectNumber> modelList = new List<TG.Model.cm_projectNumber>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_projectNumber model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_projectNumber();
                    if (dt.Rows[n]["id"] != null && dt.Rows[n]["id"].ToString() != "")
                    {
                        model.proNum_id = int.Parse(dt.Rows[n]["id"].ToString());
                    }
                    if (dt.Rows[n]["pro_id"] != null && dt.Rows[n]["pro_id"].ToString() != "")
                    {
                        model.pro_id = int.Parse(dt.Rows[n]["pro_id"].ToString());
                    }
                    if (dt.Rows[n]["SubmitDate"] != null && dt.Rows[n]["SubmitDate"].ToString() != "")
                    {
                        model.SubmitDate = DateTime.Parse(dt.Rows[n]["SubmitDate"].ToString());
                    }
                    if (dt.Rows[n]["SubmintPerson"] != null && dt.Rows[n]["SubmintPerson"].ToString() != "")
                    {
                        model.SubmintPerson = dt.Rows[n]["SubmintPerson"].ToString();
                    }
                    if (dt.Rows[n]["ProNumber"] != null && dt.Rows[n]["ProNumber"].ToString() != "")
                    {
                        model.ProNumber = dt.Rows[n]["ProNumber"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListByPageProc(query, startIndex, endIndex);
        }
        /// <summary>
        /// 返回需要分页的记录数
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageProcCount(string query)
        {
            return dal.GetListPageProcCount(query);
        }

        //工号分配的记录数
        public int ProNumcount()
        {
            return dal.ProNumcount();
        }
        #endregion  Method
    }
}

