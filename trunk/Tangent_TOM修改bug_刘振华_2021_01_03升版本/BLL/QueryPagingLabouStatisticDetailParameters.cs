﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public class QueryPagingLabouStatisticDetailParameters
    {
        public QueryPagingLabouStatisticDetailParameters()
        { }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string OrderBy { get; set; }

        public string Sort { get; set; }

        public string Where { get; set; }

        public string ProcedureName { get; set; }

        public string memID { get; set; }
        public int Total
        {
            get
            {
                if (_parameters.Count > 0)
                {
                    return (int)_parameters[6].Value;
                }
                return 0;
            }
        }

        private IList<SqlParameter> _parameters = new List<SqlParameter>();

        public SqlParameter[] ProcedureParameters
        {
            get
            {
                if (_parameters.Count == 0)
                {
                    _parameters.Add(new SqlParameter("PageIndex", PageIndex));
                    _parameters.Add(new SqlParameter("PageSize", PageSize));
                    _parameters.Add(new SqlParameter("Where", Where));
                    _parameters.Add(new SqlParameter("OrderBy", OrderBy));
                    _parameters.Add(new SqlParameter("Sort", Sort));
                    _parameters.Add(new SqlParameter("memID", memID));
                    var outputTotal = new SqlParameter
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "Total",
                        DbType = DbType.Int32,
                        Size = 4
                    };

                    _parameters.Add(outputTotal);

                }

                return _parameters.ToArray();
            }
            set { _parameters = value; }
        }
    }
}
