﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_ProImageAuditConfig
	/// </summary>
	public partial class cm_ProImageAuditConfig
	{
		private readonly TG.DAL.cm_ProImageAuditConfig dal=new TG.DAL.cm_ProImageAuditConfig();
		public cm_ProImageAuditConfig()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SysNo)
		{
			return dal.Exists(SysNo);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_ProImageAuditConfig model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ProImageAuditConfig model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SysNo)
		{
			
			return dal.Delete(SysNo);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string SysNolist )
		{
			return dal.DeleteList(SysNolist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ProImageAuditConfig GetModel(int SysNo)
		{
			
			return dal.GetModel(SysNo);
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ProImageAuditConfig> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ProImageAuditConfig> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_ProImageAuditConfig> modelList = new List<TG.Model.cm_ProImageAuditConfig>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_ProImageAuditConfig model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_ProImageAuditConfig();
					if(dt.Rows[n]["SysNo"]!=null && dt.Rows[n]["SysNo"].ToString()!="")
					{
						model.SysNo=int.Parse(dt.Rows[n]["SysNo"].ToString());
					}
					if(dt.Rows[n]["ProcessDescription"]!=null && dt.Rows[n]["ProcessDescription"].ToString()!="")
					{
					model.ProcessDescription=dt.Rows[n]["ProcessDescription"].ToString();
					}
					if(dt.Rows[n]["RoleSysNo"]!=null && dt.Rows[n]["RoleSysNo"].ToString()!="")
					{
						model.RoleSysNo=int.Parse(dt.Rows[n]["RoleSysNo"].ToString());
					}
					if(dt.Rows[n]["Position"]!=null && dt.Rows[n]["Position"].ToString()!="")
					{
						model.Position=int.Parse(dt.Rows[n]["Position"].ToString());
					}
					if(dt.Rows[n]["InUser"]!=null && dt.Rows[n]["InUser"].ToString()!="")
					{
						model.InUser=int.Parse(dt.Rows[n]["InUser"].ToString());
					}
					if(dt.Rows[n]["InDate"]!=null && dt.Rows[n]["InDate"].ToString()!="")
					{
						model.InDate=DateTime.Parse(dt.Rows[n]["InDate"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

