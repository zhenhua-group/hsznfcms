﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public class cm_SubConstruCoperartion
    {
        private readonly DAL.cm_SubConstruCoperartion dal = new DAL.cm_SubConstruCoperartion();
        public cm_SubConstruCoperartion()
        { }
        #region  Method

    
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            return dal.Exists(ID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Model.cm_SubConstruCoperartion model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.cm_SubConstruCoperartion model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Model.cm_SubConstruCoperartion GetModel(int ID)
        {
            return dal.GetModel(ID);
        }

    
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Model.cm_SubConstruCoperartion> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<Model.cm_SubConstruCoperartion> DataTableToList(DataTable dt)
        {
            List<Model.cm_SubConstruCoperartion> modelList = new List<Model.cm_SubConstruCoperartion>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Model.cm_SubConstruCoperartion model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new Model.cm_SubConstruCoperartion();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["cpr_Id"] != null && dt.Rows[n]["cpr_Id"].ToString() != "")
                    {
                        model.cpr_Id = int.Parse(dt.Rows[n]["cpr_Id"].ToString());
                    }
                    if (dt.Rows[n]["cpr_No"] != null && dt.Rows[n]["cpr_No"].ToString() != "")
                    {
                        model.cpr_No = dt.Rows[n]["cpr_No"].ToString();
                    }
                    if (dt.Rows[n]["Construction_Name"] != null && dt.Rows[n]["Construction_Name"].ToString() != "")
                    {
                        model.Construction_Name = dt.Rows[n]["Construction_Name"].ToString();
                    }
                    if (dt.Rows[n]["Project_Diameter"] != null && dt.Rows[n]["Project_Diameter"].ToString() != "")
                    {
                        model.Project_Diameter = decimal.Parse(dt.Rows[n]["Project_Diameter"].ToString());
                    }
                    if (dt.Rows[n]["Quantity"] != null && dt.Rows[n]["Quantity"].ToString() != "")
                    {
                        model.Quantity = int.Parse(dt.Rows[n]["Quantity"].ToString());
                    }
                    if (dt.Rows[n]["Pile_Length"] != null && dt.Rows[n]["Pile_Length"].ToString() != "")
                    {
                        model.Pile_Length = decimal.Parse(dt.Rows[n]["Pile_Length"].ToString());
                    }
                    if (dt.Rows[n]["Pit_Area"] != null && dt.Rows[n]["Pit_Area"].ToString() != "")
                    {
                        model.Pit_Area = decimal.Parse(dt.Rows[n]["Pit_Area"].ToString());
                    }
                    if (dt.Rows[n]["Depth"] != null && dt.Rows[n]["Depth"].ToString() != "")
                    {
                        model.Depth = decimal.Parse(dt.Rows[n]["Depth"].ToString());
                    }
                    if (dt.Rows[n]["Support_Form"] != null && dt.Rows[n]["Support_Form"].ToString() != "")
                    {
                        model.Support_Form = dt.Rows[n]["Support_Form"].ToString();
                    }
                    if (dt.Rows[n]["Well_Amount"] != null && dt.Rows[n]["Well_Amount"].ToString() != "")
                    {
                        model.Well_Amount = decimal.Parse(dt.Rows[n]["Well_Amount"].ToString());
                    }
                    if (dt.Rows[n]["Concrete_Amount"] != null && dt.Rows[n]["Concrete_Amount"].ToString() != "")
                    {
                        model.Concrete_Amount = decimal.Parse(dt.Rows[n]["Concrete_Amount"].ToString());
                    }
                    if (dt.Rows[n]["Reinforced_Amount"] != null && dt.Rows[n]["Reinforced_Amount"].ToString() != "")
                    {
                        model.Reinforced_Amount = decimal.Parse(dt.Rows[n]["Reinforced_Amount"].ToString());
                    }
                    if (dt.Rows[n]["UpdateBy"] != null && dt.Rows[n]["UpdateBy"].ToString() != "")
                    {
                        model.UpdateBy = dt.Rows[n]["UpdateBy"].ToString();
                    }
                    if (dt.Rows[n]["LastUpdate"] != null && dt.Rows[n]["LastUpdate"].ToString() != "")
                    {
                        model.LastUpdate = DateTime.Parse(dt.Rows[n]["LastUpdate"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
       

        #endregion  Method
    }
}
