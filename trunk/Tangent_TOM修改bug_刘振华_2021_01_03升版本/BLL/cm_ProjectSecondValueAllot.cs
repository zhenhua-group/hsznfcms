﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace TG.BLL
{
    /// <summary>
    /// 二次产值分配
    /// </summary>
    public partial class cm_ProjectSecondValueAllot
    {
        private readonly TG.DAL.cm_ProjectSecondValueAllot dal = new TG.DAL.cm_ProjectSecondValueAllot();
        public cm_ProjectSecondValueAllot()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectSecondValueAllot model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int UpdateAuditCount(int ID, decimal? auditCount, decimal? designCount)
        {
            return dal.UpdateAuditCount(ID, auditCount, designCount);
        }

        /// <summary>
        /// 更新二次分配状态
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public int UpdateAuditStatus(int ID, string status)
        {
            return dal.UpdateAuditStatus(ID, status);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectSecondValueAllot GetModel(int proID, int? AllotID)
        {
            return dal.GetModel(proID, AllotID);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectSecondValueAllot GetModel(int ID)
        {

            return dal.GetModel(ID);
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectSecondValueAllot> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectSecondValueAllot> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_ProjectSecondValueAllot> modelList = new List<TG.Model.cm_ProjectSecondValueAllot>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_ProjectSecondValueAllot model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_ProjectSecondValueAllot();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["ProID"] != null && dt.Rows[n]["ProID"].ToString() != "")
                    {
                        model.ProID = int.Parse(dt.Rows[n]["ProID"].ToString());
                    }
                    if (dt.Rows[n]["AllotID"] != null && dt.Rows[n]["AllotID"].ToString() != "")
                    {
                        model.AllotID = int.Parse(dt.Rows[n]["AllotID"].ToString());
                    }
                    if (dt.Rows[n]["TheDeptValueCount"] != null && dt.Rows[n]["TheDeptValueCount"].ToString() != "")
                    {
                        model.TheDeptValueCount = decimal.Parse(dt.Rows[n]["TheDeptValueCount"].ToString());
                    }
                    if (dt.Rows[n]["AuditCount"] != null && dt.Rows[n]["AuditCount"].ToString() != "")
                    {
                        model.AuditCount = decimal.Parse(dt.Rows[n]["AuditCount"].ToString());
                    }
                    if (dt.Rows[n]["DesignCount"] != null && dt.Rows[n]["DesignCount"].ToString() != "")
                    {
                        model.DesignCount = decimal.Parse(dt.Rows[n]["DesignCount"].ToString());
                    }
                    if (dt.Rows[n]["HavcCount"] != null && dt.Rows[n]["HavcCount"].ToString() != "")
                    {
                        model.HavcCount = decimal.Parse(dt.Rows[n]["HavcCount"].ToString());
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = dt.Rows[n]["Status"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        #endregion  Method
    }
}
