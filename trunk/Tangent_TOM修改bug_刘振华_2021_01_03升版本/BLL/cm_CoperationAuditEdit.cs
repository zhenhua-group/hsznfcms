﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.BLL
{

    /// <summary>
    /// cm_AuditRecordEdit
    /// </summary>
    public partial class cm_AuditRecordEdit
    {
        private readonly TG.DAL.cm_AuditRecordEdit dal = new TG.DAL.cm_AuditRecordEdit();
        public cm_AuditRecordEdit()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int SysNo)
        {
            return dal.Exists(SysNo);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CoperationAuditEdit model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CoperationAuditEdit model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SysNo)
        {

            return dal.Delete(SysNo);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string SysNolist)
        {
            return dal.DeleteList(SysNolist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationAuditEdit GetModel(int SysNo)
        {

            return dal.GetModel(SysNo);
        }
        public TG.Model.cm_CoperationAuditEdit GetModelByCoperationSysNo(int coperationSysNo)
        {
            return dal.GetModelByCoperationSysNo(coperationSysNo);
        }
        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_CoperationAuditEdit GetModelByCache(int SysNo)
        {

            string CacheKey = "cm_AuditRecordEditModel-" + SysNo;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(SysNo);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_CoperationAuditEdit)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CoperationAuditEdit> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CoperationAuditEdit> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_CoperationAuditEdit> modelList = new List<TG.Model.cm_CoperationAuditEdit>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_CoperationAuditEdit model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_CoperationAuditEdit();
                    if (dt.Rows[n]["SysNo"] != null && dt.Rows[n]["SysNo"].ToString() != "")
                    {
                        model.SysNo = int.Parse(dt.Rows[n]["SysNo"].ToString());
                    }
                    if (dt.Rows[n]["CoperationSysNo"] != null && dt.Rows[n]["CoperationSysNo"].ToString() != "")
                    {
                        model.CoperationSysNo = int.Parse(dt.Rows[n]["CoperationSysNo"].ToString());
                    }
                    if (dt.Rows[n]["Options"] != null && dt.Rows[n]["Options"].ToString() != "")
                    {
                        model.Options = dt.Rows[n]["Options"].ToString();
                    }
                    if (dt.Rows[n]["InUser"] != null && dt.Rows[n]["InUser"].ToString() != "")
                    {
                        model.InUser = int.Parse(dt.Rows[n]["InUser"].ToString());
                    }
                    if (dt.Rows[n]["InDate"] != null && dt.Rows[n]["InDate"].ToString() != "")
                    {
                        model.InDate = DateTime.Parse(dt.Rows[n]["InDate"].ToString());
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = dt.Rows[n]["Status"].ToString();
                    }
                    if (dt.Rows[n]["UndertakeProposal"] != null && dt.Rows[n]["UndertakeProposal"].ToString() != "")
                    {
                        model.UndertakeProposal = dt.Rows[n]["UndertakeProposal"].ToString();
                    }
                    if (dt.Rows[n]["OperateDepartmentProposal"] != null && dt.Rows[n]["OperateDepartmentProposal"].ToString() != "")
                    {
                        model.OperateDepartmentProposal = dt.Rows[n]["OperateDepartmentProposal"].ToString();
                    }
                    if (dt.Rows[n]["ManageLevel"] != null && dt.Rows[n]["ManageLevel"].ToString() != "")
                    {
                        model.ManageLevel = int.Parse(dt.Rows[n]["ManageLevel"].ToString());
                    }
                    if (dt.Rows[n]["NeedLegalAdviser"] != null && dt.Rows[n]["NeedLegalAdviser"].ToString() != "")
                    {
                        model.NeedLegalAdviser = int.Parse(dt.Rows[n]["NeedLegalAdviser"].ToString());
                    }
                    if (dt.Rows[n]["LegalAdviserProposal"] != null && dt.Rows[n]["LegalAdviserProposal"].ToString() != "")
                    {
                        model.LegalAdviserProposal = dt.Rows[n]["LegalAdviserProposal"].ToString();
                    }
                    if (dt.Rows[n]["TechnologyDepartmentProposal"] != null && dt.Rows[n]["TechnologyDepartmentProposal"].ToString() != "")
                    {
                        model.TechnologyDepartmentProposal = dt.Rows[n]["TechnologyDepartmentProposal"].ToString();
                    }
                    if (dt.Rows[n]["GeneralManagerProposal"] != null && dt.Rows[n]["GeneralManagerProposal"].ToString() != "")
                    {
                        model.GeneralManagerProposal = dt.Rows[n]["GeneralManagerProposal"].ToString();
                    }
                    if (dt.Rows[n]["BossProposal"] != null && dt.Rows[n]["BossProposal"].ToString() != "")
                    {
                        model.BossProposal = dt.Rows[n]["BossProposal"].ToString();
                    }
                    if (dt.Rows[n]["AuditUser"] != null && dt.Rows[n]["AuditUser"].ToString() != "")
                    {
                        model.AuditUser = dt.Rows[n]["AuditUser"].ToString();
                    }
                    if (dt.Rows[n]["AuditDate"] != null && dt.Rows[n]["AuditDate"].ToString() != "")
                    {
                        model.AuditDate = dt.Rows[n]["AuditDate"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method

        //新建合同审批流程
        public int InsertCoperatioAuditRecord(TG.Model.cm_CoperationAuditEdit coperation)
        {
            return dal.InsertCoperatioAuditRecord(coperation);
        }
        public int SendMsg(TG.Model.cm_SysMsg sysMsg)
        {
            return dal.SendMsg(sysMsg);
        }

        public int UpdateCoperatioAuditRecord(TG.Model.cm_CoperationAuditEdit coperation)
        {
            return dal.UpdateCoperatioAuditRecord(coperation);
        }

    }


}
