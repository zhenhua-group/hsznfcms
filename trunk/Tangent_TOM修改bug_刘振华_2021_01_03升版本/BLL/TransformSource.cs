﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.BLL
{
	public static class TransformSource
	{
		public static DataSet ConvertToDataSet<T>(List<T> list)
		{
			if (list == null || list.Count <= 0)
			{
				return null;
			}
			DataSet ds = new DataSet();
			DataTable dt = new DataTable(typeof(T).Name);
			DataColumn column;
			DataRow row;
			System.Reflection.PropertyInfo[] propertyInfo = typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
			foreach (T t in list)
			{
				if (t == null)
				{
					continue;
				}
				row = dt.NewRow();
				for (int i = 0, j = propertyInfo.Length; i < j; i++)
				{
					System.Reflection.PropertyInfo pi = propertyInfo[i];
					string name = pi.Name;
					//如果不存在该列，添加行列
					if (dt.Columns[name] == null)
					{
						//判断列的数据类型，已解决Nullable问题，此处针对list中的数据类型判断。
						//若是int32，则比较字符串为：System.Nullable`1[System.Int32]。下面的代码同理设置数据类型
						if (pi.PropertyType.UnderlyingSystemType.ToString() == "System.Nullable`1[System.DateTime]")
						{
							column = new DataColumn(name, typeof(DateTime));
							dt.Columns.Add(column);
							if (pi.GetValue(t, null) != null)
							{
								row[name] = pi.GetValue(t, null);
							}
							else
							{
								row[name] = System.DBNull.Value;
							}
						}
						else if (pi.PropertyType.UnderlyingSystemType.ToString() == "System.Nullable`1[System.Int32]")
						{

							column = new DataColumn(name, typeof(Int32));
							dt.Columns.Add(column);
							if (pi.GetValue(t, null) != null)
							{
								row[name] = pi.GetValue(t, null);
							}
							else
							{
								row[name] = System.DBNull.Value;
							}
						}
						else if (pi.PropertyType.UnderlyingSystemType.ToString() == "System.Nullable`1[System.Decimal]")
						{

							column = new DataColumn(name, typeof(Decimal));
							dt.Columns.Add(column);
							if (pi.GetValue(t, null) != null)
							{
								row[name] = pi.GetValue(t, null);
							}
							else
							{
								row[name] = System.DBNull.Value;
							}
						}
						else
						{
							column = new DataColumn(name, pi.PropertyType);
							dt.Columns.Add(column);
							row[name] = pi.GetValue(t, null);
						}
					}
					else
					{
						if (pi.GetValue(t, null) != null)
						{
							row[name] = pi.GetValue(t, null);
						}
						else
						{
							row[name] = DBNull.Value;
						}
					}
				}
				dt.Rows.Add(row);
			}
			ds.Tables.Add(dt);
			return ds;
		}
	}
}
