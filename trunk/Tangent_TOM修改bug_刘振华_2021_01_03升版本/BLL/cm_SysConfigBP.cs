﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;

namespace TG.BLL
{
	public class cm_SysConfigBP
	{
		private cm_SysConfigDA da = new cm_SysConfigDA();

		public int InsertProjectConfigFile(string facultyName, string configContent)
		{
			return da.InsertProjectConfigFile(facultyName, configContent);
		}
	}
}
