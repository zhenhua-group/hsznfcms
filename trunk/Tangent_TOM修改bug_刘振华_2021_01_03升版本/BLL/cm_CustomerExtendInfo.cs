﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_CustomerExtendInfo
	/// </summary>
	public partial class cm_CustomerExtendInfo
	{
		private readonly TG.DAL.cm_CustomerExtendInfo dal=new TG.DAL.cm_CustomerExtendInfo();
		public cm_CustomerExtendInfo()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Cst_ExtendInfoId)
		{
			return dal.Exists(Cst_ExtendInfoId);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_CustomerExtendInfo model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_CustomerExtendInfo model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Cst_ExtendInfoId)
		{
			
			return dal.Delete(Cst_ExtendInfoId);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Cst_ExtendInfoIdlist )
		{
			return dal.DeleteList(Cst_ExtendInfoIdlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_CustomerExtendInfo GetModel(int Cst_ExtendInfoId)
		{
			
			return dal.GetModel(Cst_ExtendInfoId);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_CustomerExtendInfo GetModelByCache(int Cst_ExtendInfoId)
		{
			
			string CacheKey = "cm_CustomerExtendInfoModel-" + Cst_ExtendInfoId;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Cst_ExtendInfoId);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_CustomerExtendInfo)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_CustomerExtendInfo> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_CustomerExtendInfo> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_CustomerExtendInfo> modelList = new List<TG.Model.cm_CustomerExtendInfo>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_CustomerExtendInfo model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_CustomerExtendInfo();
					if(dt.Rows[n]["Cst_ExtendInfoId"]!=null && dt.Rows[n]["Cst_ExtendInfoId"].ToString()!="")
					{
						model.Cst_ExtendInfoId=int.Parse(dt.Rows[n]["Cst_ExtendInfoId"].ToString());
					}
					if(dt.Rows[n]["Cst_Id"]!=null && dt.Rows[n]["Cst_Id"].ToString()!="")
					{
						model.Cst_Id=int.Parse(dt.Rows[n]["Cst_Id"].ToString());
					}
					if(dt.Rows[n]["Cst_EnglishName"]!=null && dt.Rows[n]["Cst_EnglishName"].ToString()!="")
					{
					model.Cst_EnglishName=dt.Rows[n]["Cst_EnglishName"].ToString();
					}
					if(dt.Rows[n]["Country"]!=null && dt.Rows[n]["Country"].ToString()!="")
					{
					model.Country=dt.Rows[n]["Country"].ToString();
					}
					if(dt.Rows[n]["Province"]!=null && dt.Rows[n]["Province"].ToString()!="")
					{
					model.Province=dt.Rows[n]["Province"].ToString();
					}
					if(dt.Rows[n]["City"]!=null && dt.Rows[n]["City"].ToString()!="")
					{
					model.City=dt.Rows[n]["City"].ToString();
					}
					if(dt.Rows[n]["Profession"]!=null && dt.Rows[n]["Profession"].ToString()!="")
					{
						model.Profession=int.Parse(dt.Rows[n]["Profession"].ToString());
					}
					if(dt.Rows[n]["Type"]!=null && dt.Rows[n]["Type"].ToString()!="")
					{
						model.Type=int.Parse(dt.Rows[n]["Type"].ToString());
					}
					if(dt.Rows[n]["Email"]!=null && dt.Rows[n]["Email"].ToString()!="")
					{
					model.Email=dt.Rows[n]["Email"].ToString();
					}
					if(dt.Rows[n]["RelationDepartment"]!=null && dt.Rows[n]["RelationDepartment"].ToString()!="")
					{
					model.RelationDepartment=dt.Rows[n]["RelationDepartment"].ToString();
					}
					if(dt.Rows[n]["BankName"]!=null && dt.Rows[n]["BankName"].ToString()!="")
					{
					model.BankName=dt.Rows[n]["BankName"].ToString();
					}
					if(dt.Rows[n]["TaxAccountNo"]!=null && dt.Rows[n]["TaxAccountNo"].ToString()!="")
					{
					model.TaxAccountNo=dt.Rows[n]["TaxAccountNo"].ToString();
					}
					if(dt.Rows[n]["BankAccountNo"]!=null && dt.Rows[n]["BankAccountNo"].ToString()!="")
					{
						model.BankAccountNo=dt.Rows[n]["BankAccountNo"].ToString();
					}
					if(dt.Rows[n]["Remark"]!=null && dt.Rows[n]["Remark"].ToString()!="")
					{
					model.Remark=dt.Rows[n]["Remark"].ToString();
					}
					if(dt.Rows[n]["IsPartner"]!=null && dt.Rows[n]["IsPartner"].ToString()!="")
					{
						model.IsPartner=int.Parse(dt.Rows[n]["IsPartner"].ToString());
					}
					if(dt.Rows[n]["BranchPart"]!=null && dt.Rows[n]["BranchPart"].ToString()!="")
					{
					model.BranchPart=dt.Rows[n]["BranchPart"].ToString();
					}
					if(dt.Rows[n]["Cpy_PrincipalSheet"]!=null && dt.Rows[n]["Cpy_PrincipalSheet"].ToString()!="")
					{
					model.Cpy_PrincipalSheet=dt.Rows[n]["Cpy_PrincipalSheet"].ToString();
					}
					if(dt.Rows[n]["CreateRelationTime"]!=null && dt.Rows[n]["CreateRelationTime"].ToString()!="")
					{
						model.CreateRelationTime=DateTime.Parse(dt.Rows[n]["CreateRelationTime"].ToString());
					}
					if(dt.Rows[n]["CreditLeve"]!=null && dt.Rows[n]["CreditLeve"].ToString()!="")
					{
						model.CreditLeve=int.Parse(dt.Rows[n]["CreditLeve"].ToString());
					}
					if(dt.Rows[n]["CloseLeve"]!=null && dt.Rows[n]["CloseLeve"].ToString()!="")
					{
						model.CloseLeve=int.Parse(dt.Rows[n]["CloseLeve"].ToString());
					}
					if(dt.Rows[n]["Cpy_Code"]!=null && dt.Rows[n]["Cpy_Code"].ToString()!="")
					{
					model.Cpy_Code=dt.Rows[n]["Cpy_Code"].ToString();
					}
					if(dt.Rows[n]["LawPerson"]!=null && dt.Rows[n]["LawPerson"].ToString()!="")
					{
					model.LawPerson=dt.Rows[n]["LawPerson"].ToString();
					}
					if(dt.Rows[n]["UpdateBy"]!=null && dt.Rows[n]["UpdateBy"].ToString()!="")
					{
						model.UpdateBy=int.Parse(dt.Rows[n]["UpdateBy"].ToString());
					}
					if(dt.Rows[n]["LastUpdate"]!=null && dt.Rows[n]["LastUpdate"].ToString()!="")
					{
						model.LastUpdate=DateTime.Parse(dt.Rows[n]["LastUpdate"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

