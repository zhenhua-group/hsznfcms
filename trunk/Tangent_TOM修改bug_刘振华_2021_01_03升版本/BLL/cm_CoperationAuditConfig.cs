﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public class cm_CoperationAuditConfig
    {
        private TG.DAL.cm_CoperationAuditConfig coperationAuditConfigBLL = new TG.DAL.cm_CoperationAuditConfig();

        /// <summary>
        /// 添加一条审核配置记录
        /// </summary>
        /// <param name="coperationAuditConfig"></param>
        /// <returns></returns>
        public int InsertCoperationAuditConfig(TG.Model.cm_CoperationAuditConfig coperationAuditConfig)
        {
            return coperationAuditConfigBLL.InsertCoperationAuditConfig(coperationAuditConfig);
        }

        /// <summary>
        /// 得到合同审核配置列表
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<TG.Model.cm_CoperationAuditConfig> GetCoperationAuditConfigList(string whereSql)
        {
            List<TG.Model.cm_CoperationAuditConfig> resultList = coperationAuditConfigBLL.GetCoperationAuditConfigList(whereSql);
            //查询用户信息
            foreach (var coperationAuditEntity in resultList)
            {
                coperationAuditEntity.UserInfoList = new List<TG.Model.tg_member>();
                //分割得到用户SysNo
                string[] userSysNoArray = coperationAuditEntity.UserSysNoArrayString.Split(',');
                foreach (var userSysNo in userSysNoArray)
                {
                    TG.Model.tg_member userInfo = GetUserInfo(int.Parse(userSysNo));
                    //得到用户信息
                    if (userInfo != null)
                    {
                        coperationAuditEntity.UserInfoList.Add(userInfo);
                    }
                }
            }
            return resultList;
        }

        /// <summary>
        /// 得到合同审核配置列表按顺序递归
        /// </summary>
        /// <returns></returns>
        public List<TG.Model.cm_CoperationAuditConfig> GetCoperationAuditConfigListOneByOne()
        {
            List<TG.Model.cm_CoperationAuditConfig> coperationAuditConfigList = coperationAuditConfigBLL.GetCoperationAuditConfigList("");
            List<TG.Model.cm_CoperationAuditConfig> resultList = new List<TG.Model.cm_CoperationAuditConfig>();
            if (coperationAuditConfigList != null && coperationAuditConfigList.Count > 0)
            {
                //递归排序
                RecursionSource(0, resultList, coperationAuditConfigList);
            }

            //查询用户信息
            foreach (var coperationAuditEntity in resultList)
            {
                coperationAuditEntity.UserInfoList = new List<TG.Model.tg_member>();
                //分割得到用户SysNo
                string[] userSysNoArray = coperationAuditEntity.UserSysNoArrayString.Split(',');
                foreach (var userSysNo in userSysNoArray)
                {
                    TG.Model.tg_member userInfo = GetUserInfo(int.Parse(userSysNo));
                    //得到用户信息
                    if (userInfo != null)
                    {
                        coperationAuditEntity.UserInfoList.Add(userInfo);
                    }
                }
            }
            return resultList;
        }

        /// <summary>
        /// 递归调用审核配置集合按顺序
        /// </summary>
        /// <param name="parentSysNo"></param>
        /// <param name="resultList"></param>
        /// <param name="sourceList"></param>
        private void RecursionSource(int parentSysNo, List<TG.Model.cm_CoperationAuditConfig> resultList, List<TG.Model.cm_CoperationAuditConfig> sourceList)
        {
            TG.Model.cm_CoperationAuditConfig firstCoperationAuditConfig = (from copertionAuditFirst in sourceList where copertionAuditFirst.Position == parentSysNo select copertionAuditFirst).SingleOrDefault<TG.Model.cm_CoperationAuditConfig>();
            //结束递归
            if (firstCoperationAuditConfig == null)
            {
                return;
            }
            //填充到集合
            resultList.Add(firstCoperationAuditConfig);
            //递归调用
            RecursionSource(firstCoperationAuditConfig.SysNo, resultList, sourceList);
        }

        /// <summary>
        /// 查询用户信息
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public TG.Model.tg_member GetUserInfo(int userSysNo)
        {
            return new TG.BLL.tg_member().GetModel(userSysNo);
        }

        /// <summary>
        /// 得到一个合同审核配置
        /// </summary>
        /// <param name="coperationAuditConfigSysNo"></param>
        /// <returns></returns>
        public TG.Model.cm_CoperationAuditConfig GetCoperationAuditConfig(int coperationAuditConfigSysNo)
        {
            return coperationAuditConfigBLL.GetCoperationAuditConfig(coperationAuditConfigSysNo);
        }
        //获取审批配置位置的角色
        public TG.Model.cm_CoperationAuditConfig GetCoperationAuditConfigByPostion(int position)
        {
            return coperationAuditConfigBLL.GetCoperationAuditConfigByPostion(position);
        }
        /// <summary>
        /// 修改合同审核配置状态
        /// </summary>
        /// <param name="coperationAuditConfig"></param>
        /// <returns></returns>
        public int UpdateCoperationAuditConfig(TG.Model.cm_CoperationAuditConfig coperationAuditConfig)
        {
            return coperationAuditConfigBLL.UpdateCoperationAuditConfig(coperationAuditConfig);
        }

        public static int GetDeparmentSysNo(ref List<TG.Model.tg_unit> sourceList, string departmentName)
        {
            var sysNo = (from department in sourceList where department.unit_Name.Contains(departmentName) select department.unit_ID).ToList<int>().FirstOrDefault();
            return sysNo;
        }
    }
}
