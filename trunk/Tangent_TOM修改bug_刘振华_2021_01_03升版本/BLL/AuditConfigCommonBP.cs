﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;

namespace TG.BLL
{
        public class AuditConfigCommonBP
        {
                private AuditConfigCommonDA dal = new AuditConfigCommonDA();

                /// <summary>
                /// 得到一个审核配置共通的方法
                /// </summary>
                /// <param name="configTableName"></param>
                /// <returns></returns>
                public List<AuditConfigBase> GetAudtConfigBaseEntityList(string configTableName)
                {
                        List<AuditConfigBase> auditConfigBaseList = dal.GetAudtConfigBaseEntityList(configTableName);

                        auditConfigBaseList.ForEach((projectAuditConfig) =>
                        {
                                projectAuditConfig.UserInfoList = new List<TG.Model.tg_member>();

                                if (!string.IsNullOrEmpty(projectAuditConfig.UserSysNoArrayString))
                                {
                                        string[] userSysNoArray = projectAuditConfig.UserSysNoArrayString.Split(',');
                                        foreach (var userSysNo in userSysNoArray)
                                        {
                                                TG.Model.tg_member userInfo = GetUserInfo(int.Parse(userSysNo));
                                                //得到用户信息
                                                if (userInfo != null)
                                                {
                                                        projectAuditConfig.UserInfoList.Add(userInfo);
                                                }
                                        }
                                }
                        });
                        return auditConfigBaseList;
                }

                /// <summary>
                /// 查询用户信息
                /// </summary>
                /// <param name="userSysNo"></param>
                /// <returns></returns>
                public TG.Model.tg_member GetUserInfo(int userSysNo)
                {
                        return new TG.BLL.tg_member().GetModel(userSysNo);
                }

                /// <summary>
                /// 修改流程所对应的角色
                /// </summary>
                /// <param name="auditConfigEntity"></param>
                /// <param name="tableName"></param>
                /// <returns></returns>
                public int UpdateProjectAuditConfigToRelationship(AuditConfigEntity auditConfigEntity, string tableName)
                {
                        return dal.UpdateProjectAuditConfigToRelationship(auditConfigEntity, tableName);
                }
        }
}
