﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;

namespace TG.BLL
{
	public class cm_ProjectDistributionJobNumberConfigBP
	{
		private TG.DAL.cm_ProjectDistributionJobNumberConfigDA da = new TG.DAL.cm_ProjectDistributionJobNumberConfigDA();

		/// <summary>
		/// 得到消息审核配置列表
		/// </summary>
		/// <returns></returns>
		public List<cm_ProjectDistributionJobNumberConfigEntity> GetDistributionJonNumberConfigEntityList()
		{
			List<cm_ProjectDistributionJobNumberConfigEntity> distributionJobNumberConfig = da.GetDistributionJonNumberConfigEntityList();

			distributionJobNumberConfig.ForEach((dis) =>
			{
				dis.UserInfoList = new List<TG.Model.tg_member>();

				string[] userSysNoArray = dis.UserSysNoArrayString.Split(',');
				foreach (var userSysNo in userSysNoArray)
				{
					TG.Model.tg_member userInfo = GetUserInfo(int.Parse(userSysNo));
					//得到用户信息
					if (userInfo != null)
					{
						dis.UserInfoList.Add(userInfo);
					}
				}
			});

			return distributionJobNumberConfig;
		}

		/// <summary>
		/// 查询用户信息
		/// </summary>
		/// <param name="userSysNo"></param>
		/// <returns></returns>
		public TG.Model.tg_member GetUserInfo(int userSysNo)
		{
			return new TG.BLL.tg_member().GetModel(userSysNo);
		}

		public cm_ProjectDistributionJobNumberConfigEntity GetDistributionJobNumberConfig(int position)
		{
			return da.GetDistributionJobNumberConfig(position);
		}
	}
}
