﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace TG.BLL
{

    public class MemberLabouStatistic
    {
        DAL.MemberLabouStatistic dal = new DAL.MemberLabouStatistic();

        /// <summary>
        /// 人员劳动力统计
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet getMemberLabouStatisticList(string strWhere,string strDate)
        {
            return dal.getMemberLabouStatisticList(strWhere, strDate);
        }

        /// <summary>
        /// 人员劳动力统计
        /// </summary>
        /// <param name="mem_id"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet getMemberLabouStatisticDetail(string strWhere, int mem_id)
        {
            return dal.getMemberLabouStatisticDetail(strWhere, mem_id);
        }
    }
}
