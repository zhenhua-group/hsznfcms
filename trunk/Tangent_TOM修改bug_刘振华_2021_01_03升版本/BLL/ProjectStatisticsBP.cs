﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using System.Data;
using TG.Model;

namespace TG.BLL
{
    public class ProjectStatisticsBP
    {
        private ProjectStatisticsDA da = new ProjectStatisticsDA();

        /// <summary>
        /// 按季度查询
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public List<List<ProjectStatisticsQuarterEntity>> GetProjectRecordByQuarter(string year, out int sumOfProject, string departmentName)
        {
            List<ProjectStatisticsQuarterEntity> sourceEntityList = da.GetProjectRecordByQuarter(year, departmentName);

            List<List<ProjectStatisticsQuarterEntity>> resultList = new List<List<ProjectStatisticsQuarterEntity>>();
            sumOfProject = 0;
            for (int i = 0; i < 4; i++)
            {
                List<ProjectStatisticsQuarterEntity> parEntity = (from project in sourceEntityList where project.Quarter == (i + 1) select project).ToList<ProjectStatisticsQuarterEntity>();
                sumOfProject += parEntity.Count;

                resultList.Add(parEntity);
            }
            return resultList;
        }

        /// <summary>
        /// 按照月份统计项目数
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public List<List<ProjectStatisticsMonthEntity>> GetProjectRecordByMonth(string year, string departmentName)
        {
            List<ProjectStatisticsMonthEntity> sourceEntityList = da.GetProjectRecordByMonth(year, departmentName);

            List<List<ProjectStatisticsMonthEntity>> resultList = new List<List<ProjectStatisticsMonthEntity>>();

            for (int i = 0; i < 12; i++)
            {
                List<ProjectStatisticsMonthEntity> parEntity = (from project in sourceEntityList where project.Month == (i + 1) select project).ToList<ProjectStatisticsMonthEntity>();

                resultList.Add(parEntity);
            }
            return resultList;
        }

        public DataSet GetProjectRecordDetailByQuarter(string year, int quarter)
        {
            return da.GetProjectRecordDetailByQuarter(year, quarter);
        }

        public DataSet GetProjectRecordDetailByMonth(int year, int month)
        {
            return da.GetProjectRecordDetailByMonth(year, month);
        }

        /// <summary>
        /// 根据项目类型和年份得到项目数
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataSet GetProjectCountByProjectTypeAndYear(int year, out int sumOfProject, string departmentName)
        {
            sumOfProject = 0;
            sumOfProject = da.GetProjectCountByYear(year, departmentName);
            return da.GetProjectCountByProjectTypeAndYear(year, departmentName);
        }

        /// <summary>
        /// 项目项目类型和月份得到数据
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public List<List<ProjectStatisticsType>> GetProjectCountByProjectTypeAndMonth(int year, string departmentName)
        {
            List<ProjectStatisticsType> sourceList = da.GetProjectCountByProjectTypeAndMonth(year, departmentName);

            List<List<ProjectStatisticsType>> resultList = new List<List<ProjectStatisticsType>>();

            for (int i = 0; i < 12; i++)
            {
                List<ProjectStatisticsType> parEntity = (from project in sourceList where project.Month == (i + 1) select project).ToList<ProjectStatisticsType>();
                resultList.Add(parEntity);
            }

            return resultList;
        }

        public DataSet GetProjectTypeDetailByYearAndType(int year, string projectType)
        {
            return da.GetProjectTypeDetailByYearAndType(year, projectType);
        }

        public List<ProjectStatisticeTypeByMonth> GetProjectTypeDetailByMonthAndYear(int year, int month)
        {
            //得到所有部门列表
            List<ProjectStatisticeTypeByMonth> deparmentList = da.GetProjectGroupByDepartment();

            deparmentList.ForEach((department) =>
            {
                department.ConstructionEngineeringCount = da.GetProjectTypeDetailByMonthAndYear(year, month, "建筑工程", department.Department);
                department.ConsultingCount = da.GetProjectTypeDetailByMonthAndYear(year, month, "咨询", department.Department);
                department.EngineeringSurveyCount = da.GetProjectTypeDetailByMonthAndYear(year, month, "工程勘察", department.Department);
                department.MunicipalEngineeringCount = da.GetProjectTypeDetailByMonthAndYear(year, month, "市政工程", department.Department);
                department.OtherCount = da.GetProjectTypeDetailByMonthAndYear(year, month, "其他", department.Department);
                department.UrbanAndRuralPlanningCount = da.GetProjectTypeDetailByMonthAndYear(year, month, "城乡规划", department.Department);
            });

            return deparmentList;
        }

        /// <summary>
        /// 得到项目季度平米总数
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public List<ProjectSquareMetersQuarter> GetProjectSquareMetersByQuarter(int year, string departmentName)
        {
            return da.GetProjectSquareMetersByQuarter(year, departmentName);
        }

        /// <summary>
        /// 根据月份得到项目平米数
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataSet GetProjectSquareMetersByMonth(int year)
        {
            return da.GetProjectSquareMetersByMonth(year);
        }


        /// <summary>
        /// 根据季度得到详细项目平米数信息
        /// </summary>
        /// <param name="year"></param>
        /// <param name="quarter"></param>
        /// <returns></returns>
        public DataSet GetProjectDetailSquareMetersByQuarter(int year, int quarter)
        {
            return da.GetProjectDetailSquareMetersByQuarter(year, quarter);
        }

        /// <summary>
        /// 根据月份得到详细项目平米数信息
        /// </summary>
        /// <param name="year"></param>
        /// <param name="quarter"></param>
        /// <returns></returns>
        public DataSet GetProjectDetailSquareMetersByMonth(int year, int month)
        {
            return da.GetProjectDetailSquareMetersByMonth(year, month);
        }
        /// <summary>
        /// 根据项目状态统计
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataSet GetProjectCountByStatus(int year, string departmentName)
        {
            return da.GetProjectCountByStatus(year, departmentName);
        }

        /// <summary>
        /// 得到项目详细信息
        /// </summary>
        /// <param name="year"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public DataSet GetProjectDetailByStatus(int year, string status)
        {
            return da.GetProjectDetailByStatus(year, status);
        }
    }
}
