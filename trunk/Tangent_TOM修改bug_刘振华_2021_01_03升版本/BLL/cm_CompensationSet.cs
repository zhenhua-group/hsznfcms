﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common;
using TG.Model;
using System.Data;

namespace TG.BLL
{
    /// <summary>
    /// cm_CompensationSet
    /// </summary>

    public partial class cm_CompensationSet
    {
        private readonly TG.DAL.cm_CompensationSet dal = new TG.DAL.cm_CompensationSet();
        public cm_CompensationSet()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CompensationSet model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CompensationSet model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CompensationSet GetModel(int ID)
        {

            return dal.GetModel(ID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_CompensationSet GetModelByCache(int ID)
        {

            string CacheKey = "cm_CompensationSet-" + ID;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(ID);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_CompensationSet)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CompensationSet> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CompensationSet> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_CompensationSet> modelList = new List<TG.Model.cm_CompensationSet>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_CompensationSet model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_CompensationSet();
                    
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["UnitId"] != null && dt.Rows[n]["UnitId"].ToString() != "")
                    {
                        model.UnitId = int.Parse(dt.Rows[n]["UnitId"].ToString());
                    }
                    if (dt.Rows[n]["MemberId"] != null && dt.Rows[n]["MemberId"].ToString() != "")
                    {
                        model.MemberId = int.Parse(dt.Rows[n]["MemberId"].ToString());
                    }
                    if (dt.Rows[n]["ComValue"] != null && dt.Rows[n]["ComValue"].ToString() != "")
                    {
                        model.ComValue = decimal.Parse(dt.Rows[n]["ComValue"].ToString());
                    }
                    if (dt.Rows[n]["ValueYear"] != null && dt.Rows[n]["ValueYear"].ToString() != "")
                    {
                        model.ValueYear = dt.Rows[n]["ValueYear"].ToString();
                    }
                   
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetUnitAllotListByPager(string strWhere, int startIndex, int endIndex)
        {
            return dal.GetUnitAllotListByPager(strWhere, startIndex, endIndex);
        }
        #endregion  Method
    }
}
