﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_ProimageSend
	/// </summary>
	public partial class cm_ProimageSend
	{
		private readonly TG.DAL.cm_ProimageSend dal=new TG.DAL.cm_ProimageSend();
		public cm_ProimageSend()
		{}
		#region  Method
        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            return dal.Exists(ID);
        }
        /// <summary>
        /// 判断是否存在该记录，并得到id
        /// </summary>
        public int GetSingle(int ID)
        {
            return dal.GetSingle(ID);
        }
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_ProimageSend model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ProimageSend model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			return dal.Delete(Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			return dal.DeleteList(Idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ProimageSend GetModel(int Id)
		{
			
			return dal.GetModel(Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_ProimageSend GetModelByCache(int Id)
		{
			
			string CacheKey = "cm_ProimageSendModel-" + Id;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(Id);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_ProimageSend)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ProimageSend> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ProimageSend> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_ProimageSend> modelList = new List<TG.Model.cm_ProimageSend>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_ProimageSend model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_ProimageSend();
					if(dt.Rows[n]["Id"]!=null && dt.Rows[n]["Id"].ToString()!="")
					{
						model.Id=int.Parse(dt.Rows[n]["Id"].ToString());
					}
					if(dt.Rows[n]["Pro_id"]!=null && dt.Rows[n]["Pro_id"].ToString()!="")
					{
						model.Pro_id=int.Parse(dt.Rows[n]["Pro_id"].ToString());
					}
					if(dt.Rows[n]["Pro_image"]!=null && dt.Rows[n]["Pro_image"].ToString()!="")
					{
						model.Pro_image=int.Parse(dt.Rows[n]["Pro_image"].ToString());
					}
					if(dt.Rows[n]["Send_number"]!=null && dt.Rows[n]["Send_number"].ToString()!="")
					{
						model.Send_number=int.Parse(dt.Rows[n]["Send_number"].ToString());
					}
					if(dt.Rows[n]["Send_company"]!=null && dt.Rows[n]["Send_company"].ToString()!="")
					{
					model.Send_company=dt.Rows[n]["Send_company"].ToString();
					}
					if(dt.Rows[n]["Send_A0"]!=null && dt.Rows[n]["Send_A0"].ToString()!="")
					{
						model.Send_A0=decimal.Parse(dt.Rows[n]["Send_A0"].ToString());
					}
					if(dt.Rows[n]["Send_A1"]!=null && dt.Rows[n]["Send_A1"].ToString()!="")
					{
						model.Send_A1=decimal.Parse(dt.Rows[n]["Send_A1"].ToString());
					}
					if(dt.Rows[n]["Send_A2"]!=null && dt.Rows[n]["Send_A2"].ToString()!="")
					{
						model.Send_A2=decimal.Parse(dt.Rows[n]["Send_A2"].ToString());
					}
					if(dt.Rows[n]["Send_A2_1"]!=null && dt.Rows[n]["Send_A2_1"].ToString()!="")
					{
						model.Send_A2_1=decimal.Parse(dt.Rows[n]["Send_A2_1"].ToString());
					}
					if(dt.Rows[n]["Send_A3"]!=null && dt.Rows[n]["Send_A3"].ToString()!="")
					{
						model.Send_A3=decimal.Parse(dt.Rows[n]["Send_A3"].ToString());
					}
					if(dt.Rows[n]["Send_A4"]!=null && dt.Rows[n]["Send_A4"].ToString()!="")
					{
						model.Send_A4=decimal.Parse(dt.Rows[n]["Send_A4"].ToString());
					}
					if(dt.Rows[n]["Adduser"]!=null && dt.Rows[n]["Adduser"].ToString()!="")
					{
						model.Adduser=int.Parse(dt.Rows[n]["Adduser"].ToString());
					}
					if(dt.Rows[n]["Addtime"]!=null && dt.Rows[n]["Addtime"].ToString()!="")
					{
						model.Addtime=DateTime.Parse(dt.Rows[n]["Addtime"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

