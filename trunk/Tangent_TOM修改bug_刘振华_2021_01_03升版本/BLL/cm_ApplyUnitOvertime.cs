﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_ApplyUnitOvertime
	/// </summary>
	public partial class cm_ApplyUnitOvertime
	{
		private readonly TG.DAL.cm_ApplyUnitOvertime dal=new TG.DAL.cm_ApplyUnitOvertime();
		public cm_ApplyUnitOvertime()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			return dal.Exists(ID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_ApplyUnitOvertime model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ApplyUnitOvertime model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			return dal.Delete(ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			return dal.DeleteList(IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ApplyUnitOvertime GetModel(int ID)
		{
			
			return dal.GetModel(ID);
		}
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public TG.Model.cm_ApplyUnitOvertime GetModelWhere(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            List<TG.Model.cm_ApplyUnitOvertime> list=DataTableToList(ds.Tables[0]);
            if (list!=null&&list.Count>0)
            {
                return list[0];
            }
            return null;
        }
		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_ApplyUnitOvertime GetModelByCache(int ID)
		{
			
			string CacheKey = "cm_ApplyUnitOvertimeModel-" + ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_ApplyUnitOvertime)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ApplyUnitOvertime> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ApplyUnitOvertime> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_ApplyUnitOvertime> modelList = new List<TG.Model.cm_ApplyUnitOvertime>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_ApplyUnitOvertime model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_ApplyUnitOvertime();
					if(dt.Rows[n]["ID"]!=null && dt.Rows[n]["ID"].ToString()!="")
					{
						model.ID=int.Parse(dt.Rows[n]["ID"].ToString());
					}
					if(dt.Rows[n]["mem_ID"]!=null && dt.Rows[n]["mem_ID"].ToString()!="")
					{
						model.mem_ID=int.Parse(dt.Rows[n]["mem_ID"].ToString());
					}
					if(dt.Rows[n]["OverYear"]!=null && dt.Rows[n]["OverYear"].ToString()!="")
					{
						model.OverYear=int.Parse(dt.Rows[n]["OverYear"].ToString());
					}
					if(dt.Rows[n]["OverMonth"]!=null && dt.Rows[n]["OverMonth"].ToString()!="")
					{
						model.OverMonth=int.Parse(dt.Rows[n]["OverMonth"].ToString());
					}
					if(dt.Rows[n]["Quotiety"]!=null && dt.Rows[n]["Quotiety"].ToString()!="")
					{
						model.Quotiety=decimal.Parse(dt.Rows[n]["Quotiety"].ToString());
					}
					if(dt.Rows[n]["QuotaMoney"]!=null && dt.Rows[n]["QuotaMoney"].ToString()!="")
					{
						model.QuotaMoney=dt.Rows[n]["QuotaMoney"].ToString();
					}
					if(dt.Rows[n]["addDate"]!=null && dt.Rows[n]["addDate"].ToString()!="")
					{
						model.addDate=DateTime.Parse(dt.Rows[n]["addDate"].ToString());
					}
					if(dt.Rows[n]["addUserId"]!=null && dt.Rows[n]["addUserId"].ToString()!="")
					{
						model.addUserId=int.Parse(dt.Rows[n]["addUserId"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

