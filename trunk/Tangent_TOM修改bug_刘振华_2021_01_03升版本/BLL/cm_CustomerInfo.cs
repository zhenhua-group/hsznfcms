﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
using System.Data.SqlClient;
namespace TG.BLL
{
    /// <summary>
    /// cm_CustomerInfo
    /// </summary>
    public partial class cm_CustomerInfo
    {
        private readonly TG.DAL.cm_CustomerInfo dal = new TG.DAL.cm_CustomerInfo();
        public cm_CustomerInfo()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int Cst_Id)
        {
            return dal.Exists(Cst_Id);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CustomerInfo model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CustomerInfo model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int Cst_Id)
        {

            return dal.Delete(Cst_Id);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string Cst_Idlist)
        {
            return dal.DeleteList(Cst_Idlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CustomerInfo GetModel(int Cst_Id)
        {

            return dal.GetModel(Cst_Id);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_CustomerInfo GetModelByCache(int Cst_Id)
        {

            string CacheKey = "cm_CustomerInfoModel-" + Cst_Id;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(Cst_Id);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_CustomerInfo)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CustomerInfo> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CustomerInfo> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_CustomerInfo> modelList = new List<TG.Model.cm_CustomerInfo>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_CustomerInfo model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_CustomerInfo();
                    if (dt.Rows[n]["Cst_Id"] != null && dt.Rows[n]["Cst_Id"].ToString() != "")
                    {
                        model.Cst_Id = int.Parse(dt.Rows[n]["Cst_Id"].ToString());
                    }
                    if (dt.Rows[n]["Cst_No"] != null && dt.Rows[n]["Cst_No"].ToString() != "")
                    {
                        model.Cst_No = dt.Rows[n]["Cst_No"].ToString();
                    }
                    if (dt.Rows[n]["Cst_Brief"] != null && dt.Rows[n]["Cst_Brief"].ToString() != "")
                    {
                        model.Cst_Brief = dt.Rows[n]["Cst_Brief"].ToString();
                    }
                    if (dt.Rows[n]["Cst_Name"] != null && dt.Rows[n]["Cst_Name"].ToString() != "")
                    {
                        model.Cst_Name = dt.Rows[n]["Cst_Name"].ToString();
                    }
                    if (dt.Rows[n]["Cpy_Address"] != null && dt.Rows[n]["Cpy_Address"].ToString() != "")
                    {
                        model.Cpy_Address = dt.Rows[n]["Cpy_Address"].ToString();
                    }
                    if (dt.Rows[n]["Code"] != null && dt.Rows[n]["Code"].ToString() != "")
                    {
                        model.Code = dt.Rows[n]["Code"].ToString();
                    }
                    if (dt.Rows[n]["Linkman"] != null && dt.Rows[n]["Linkman"].ToString() != "")
                    {
                        model.Linkman = dt.Rows[n]["Linkman"].ToString();
                    }
                    if (dt.Rows[n]["CallingCard"] != null && dt.Rows[n]["CallingCard"].ToString() != "")
                    {
                        model.CallingCard = (byte[])dt.Rows[n]["CallingCard"];
                    }
                    if (dt.Rows[n]["Cpy_Phone"] != null && dt.Rows[n]["Cpy_Phone"].ToString() != "")
                    {
                        model.Cpy_Phone = dt.Rows[n]["Cpy_Phone"].ToString();
                    }
                    if (dt.Rows[n]["Cpy_Fax"] != null && dt.Rows[n]["Cpy_Fax"].ToString() != "")
                    {
                        model.Cpy_Fax = dt.Rows[n]["Cpy_Fax"].ToString();
                    }
                    if (dt.Rows[n]["UpdateBy"] != null && dt.Rows[n]["UpdateBy"].ToString() != "")
                    {
                        model.UpdateBy = int.Parse(dt.Rows[n]["UpdateBy"].ToString());
                    }
                    if (dt.Rows[n]["LastUpdate"] != null && dt.Rows[n]["LastUpdate"].ToString() != "")
                    {
                        model.LastUpdate = DateTime.Parse(dt.Rows[n]["LastUpdate"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListByPageProc(query, startIndex, endIndex);
        }
        /// <summary>
        /// 返回需要分页的记录数
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageProcCount(string query)
        {
            return dal.GetListPageProcCount(query);
        }
        /// <summary>
        /// Where条件语句后半段。从AND 或者 OR 开始。
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public DataSet GetCustomerRecord(string whereSql)
        {
            return dal.GetCustomerRecord(whereSql);
        }
        //获取合同所有年份
        public List<string> GetCustomerYear()
        {
            return dal.GetCustomerYear();
        }
        /// <summary>
        /// 得到客户导出数据
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetCustomerExportInfo(string strWhere)
        {
            return dal.GetCustomerExportInfo(strWhere);
        }
        /// <summary>
        /// 得到客户导出数据
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetContactPersionInfoExportInfo(string strWhere)
        {
            return dal.GetContactPersionInfoExportInfo(strWhere);
        }
         /// <summary>
        /// 得到客户导出数据-客户前期资料
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet Get_AttachInfoExportInfo(string strWhere)
        {
            return dal.Get_AttachInfoExportInfo(strWhere);
        }
        #endregion  Method
    }
}

