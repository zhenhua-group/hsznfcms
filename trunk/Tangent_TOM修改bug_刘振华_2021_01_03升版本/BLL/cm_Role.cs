﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;

namespace TG.BLL
{
    public class cm_Role
    {
        private TG.DAL.cm_Role roleDAL = new TG.DAL.cm_Role();
        /// <summary>
        /// 得到所有的角色集合
        /// </summary>
        /// <returns></returns>
        public List<TG.Model.cm_Role> GetRoleList()
        {
            return roleDAL.GetRoleList();
        }

        /// <summary>
        /// 得到一个角色实体
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public TG.Model.cm_Role GetRole(int sysNo)
        {
            return roleDAL.GetRole(sysNo);
        }

        /// <summary>
        /// 修改一个角色实体
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public int UpdateRole(TG.Model.cm_Role role)
        {
            return roleDAL.UpdateRole(role);
        }

        /// <summary>
        /// 检查用户权限
        /// </summary>
        /// <param name="roleSysNo"></param>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public bool CheckPower(int roleSysNo, int userSysNo)
        {
            return roleDAL.CheckPower(roleSysNo, userSysNo);
        }

        /// <summary>
        /// 根据角色SysNo得到用户集合
        /// </summary>
        /// <param name="roleSysNo"></param>
        /// <returns></returns>
        public List<TG.Model.tg_member> GetRoleUsersList(int roleSysNo)
        {
            TG.Model.cm_Role role = roleDAL.GetRole(roleSysNo);

            List<TG.Model.tg_member> resultList = new List<TG.Model.tg_member>();

            TG.BLL.tg_member userBP = new tg_member();

            string[] userSysNoArray = role.Users.Split(',');

            foreach (string userSysNo in userSysNoArray)
            {
                resultList.Add(userBP.GetModel(int.Parse(userSysNo)));
            }
            return resultList;
        }

        /// <summary>
        /// 得到RoleViewEntityList
        /// </summary>
        /// <param name="queryEntity"></param>
        /// <returns></returns>
        public List<RoleViewEntity> GetRoleViewEntityList(RoleQueryEntity queryEntity)
        {
            List<RoleViewEntity> roleViewEntityList = roleDAL.GetRoleViewEntityList(queryEntity);

            TG.BLL.tg_member userBP = new tg_member();

            roleViewEntityList.ForEach((role) =>
            {
                Dictionary<int, string> dictionaryUser = new Dictionary<int, string>();
                string[] userSysNoArray = role.Users.Split(',');
                if (!string.IsNullOrEmpty(role.Users) && userSysNoArray.Length > 0)
                {
                    foreach (string userSysNo in userSysNoArray)
                    {
                        //查询用户名字
                        TG.Model.tg_member userEntity = userBP.GetModel(int.Parse(userSysNo));

                        dictionaryUser.Add(int.Parse(userSysNo), userEntity == null ? "" : userEntity.mem_Name);
                    }
                }
                role.DictionaryUser = dictionaryUser;
            });

            return roleViewEntityList;
        }

        /// <summary>
        /// 新规一个角色
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public int InsertRole(TG.Model.cm_Role role)
        {
            return roleDAL.InsertRole(role);
        }

        public RoleViewEntity GetRoleViewEntity(RoleQueryEntity queryEntity)
        {
            RoleViewEntity role = roleDAL.GetRoleViewEntity(queryEntity);

            TG.BLL.tg_member userBP = new tg_member();
            Dictionary<int, string> dictionaryUser = new Dictionary<int, string>();
            string[] userSysNoArray = role.Users.Split(',');
            if (!string.IsNullOrEmpty(role.Users) && userSysNoArray.Length > 0)
            {
                foreach (string userSysNo in userSysNoArray)
                {
                    //查询用户名字
                    TG.Model.tg_member userEntity = userBP.GetModel(int.Parse(userSysNo));

                    dictionaryUser.Add(int.Parse(userSysNo), userEntity == null ? "" : userEntity.mem_Name);
                }
            }
            role.DictionaryUser = dictionaryUser;
            return role;
        }
        //根据用户ID获取用户角色
        public List<TG.Model.cm_Role> GetRoleList(int userSysNo)
        {
            List<TG.Model.cm_Role> sourceRoleList = roleDAL.GetRoleList();

            var resultList = (from role in sourceRoleList where role.Users.Split(',').Contains<string>(userSysNo.ToString()) select role).ToList<TG.Model.cm_Role>();

            return resultList;
        }
    }
}
