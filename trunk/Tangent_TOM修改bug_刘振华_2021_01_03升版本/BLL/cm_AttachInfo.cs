﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
using System.Data.SqlClient;
namespace TG.BLL
{
    /// <summary>
    /// cm_AttachInfo
    /// </summary>
    public partial class cm_AttachInfo
    {
        private readonly TG.DAL.cm_AttachInfo dal = new TG.DAL.cm_AttachInfo();
        public cm_AttachInfo()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            return dal.Exists(ID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_AttachInfo model)
        {
            return dal.Add(model);
        }
        public int AddInterView(TG.Model.cm_AttachInfo model)
        {
            return dal.AddInterView(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_AttachInfo model)
        {
            return dal.Update(model);
        }
        public bool UpdateInterView(TG.Model.cm_AttachInfo model)
        {
            return dal.UpdateInterView(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_AttachInfo GetModel(int ID)
        {

            return dal.GetModel(ID);
        }
        public TG.Model.cm_AttachInfo GetModelInterView(int ID)
        {

            return dal.GetModelInterView(ID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_AttachInfo GetModelByCache(int ID)
        {

            string CacheKey = "cm_AttachInfoModel-" + ID;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(ID);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_AttachInfo)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        public DataSet GetListInterViews(string strWhere)
        {
            return dal.GetListInterView(strWhere);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListAboutUsername(string strWhere)
        {
            return dal.GetListAboutUsername(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_AttachInfo> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        public List<TG.Model.cm_AttachInfo> GetListInterView(string strWhere)
        {
            DataSet ds = dal.GetListInterView(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_AttachInfo> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_AttachInfo> modelList = new List<TG.Model.cm_AttachInfo>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_AttachInfo model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_AttachInfo();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["Cst_Id"] != null && dt.Rows[n]["Cst_Id"].ToString() != "")
                    {
                        model.Cst_Id = Convert.ToDecimal(dt.Rows[n]["Cst_Id"].ToString());
                    }
                    if (dt.Rows[n]["Temp_No"] != null && dt.Rows[n]["Temp_No"].ToString() != "")
                    {
                        model.Temp_No = dt.Rows[n]["Temp_No"].ToString();
                    }
                    if (dt.Rows[n]["FileName"] != null && dt.Rows[n]["FileName"].ToString() != "")
                    {
                        model.FileName = dt.Rows[n]["FileName"].ToString();
                    }
                    if (dt.Rows[n]["UploadTime"] != null && dt.Rows[n]["UploadTime"].ToString() != "")
                    {
                        model.UploadTime = DateTime.Parse(dt.Rows[n]["UploadTime"].ToString());
                    }
                    if (dt.Rows[n]["FileSize"] != null && dt.Rows[n]["FileSize"].ToString() != "")
                    {
                        model.FileSize = dt.Rows[n]["FileSize"].ToString();
                    }
                    if (dt.Rows[n]["FileType"] != null && dt.Rows[n]["FileType"].ToString() != "")
                    {
                        model.FileType = dt.Rows[n]["FileType"].ToString();
                    }
                    if (dt.Rows[n]["UploadUser"] != null && dt.Rows[n]["UploadUser"].ToString() != "")
                    {
                        model.UploadUser = dt.Rows[n]["UploadUser"].ToString();
                    }
                    if (dt.Rows[n]["FileUrl"] != null && dt.Rows[n]["FileUrl"].ToString() != "")
                    {
                        model.FileUrl = dt.Rows[n]["FileUrl"].ToString();
                    }
                    if (dt.Rows[n]["FileTypeImg"] != null && dt.Rows[n]["FileTypeImg"].ToString() != "")
                    {
                        model.FileTypeImg = dt.Rows[n]["FileTypeImg"].ToString();
                    }
                    if (dt.Rows[n]["OwnType"] != null && dt.Rows[n]["OwnType"].ToString() != "")
                    {
                        model.OwnType = dt.Rows[n]["OwnType"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListByPageProc(query, startIndex, endIndex);
        }
        /// <summary>
        /// 返回需要分页的记录数
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageProcCount(string query)
        {
            return dal.GetListPageProcCount(query);
        }
        /// <summary>
        /// 根据合同查询附件列表
        /// </summary>
        /// <param name="coperationSysNo"></param>
        /// <returns></returns>
        public List<TG.Model.cm_AttachInfo> GetAttachByCoperationSysNo(int coperationSysNo)
        {
            return dal.GetAttachByCoperationSysNo(coperationSysNo);
        }

        /// <summary>
        /// 查询是否有附件信息
        /// </summary>
        /// <param name="coperationSysNo"></param>
        /// <returns></returns>
        public int IsHavaAttach(int coperationSysNo)
        {
            return dal.IsHavaAttach(coperationSysNo);
        }
        #endregion  Method
    }
}

