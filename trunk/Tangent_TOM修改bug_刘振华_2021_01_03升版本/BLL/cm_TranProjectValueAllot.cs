﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;


namespace TG.BLL
{
    public class cm_TranProjectValueAllot
    {
        private readonly TG.DAL.cm_TranProjectValueAllot dal = new TG.DAL.cm_TranProjectValueAllot();
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_TranProjectValueAllot model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_TranProjectValueAllot model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 得到分页信息条数--查询转暖通-经济所信息
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public DataSet P_cm_TranProjectValuelNoListCount(string strWhere, string type)
        {
            return dal.P_cm_TranProjectValuelNoListCount(strWhere, type);
        }

         /// <summary>
        /// 得到对象
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public TG.Model.cm_TranProjectValueAllot GetModel(int ID)
        {
            return dal.GetModel(ID);
        }
        /// <summary>
        /// 得到对象
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public TG.Model.cm_TranProjectValueAllot GetModelByProID(int proID, int AllotID)
        {
            return dal.GetModelByProID(proID,AllotID);
        }

        /// <summary>
        /// 查询转暖通-经济所信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_TranProjectValuelNoByPager(int startIndex, int endIndex, string strWhere, string type)
        {
            return dal.P_cm_TranProjectValuelNoByPager(startIndex, endIndex, strWhere, type);
        }

        /// <summary>
        /// 查询转暖通-经济所信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_TranProjectValuelYesByPager(int startIndex, int endIndex, string strWhere, string type)
        {
            return dal.P_cm_TranProjectValuelYesByPager(startIndex, endIndex, strWhere, type);
        }

        /// <summary>
        /// 得到分页信息条数--查询转暖通-经济所信息
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet P_cm_TranProjectValuelYesListCount(string strWhere, string type)
        {
            return dal.P_cm_TranProjectValuelYesListCount(strWhere, type);
        }

        /// <summary>
        /// 得到人员信息
        /// </summary>
        /// <param name="speName"></param>
        /// <param name="pro_ID"></param>
        /// <returns></returns>
        public DataSet GetPlanMember(string speName, int pro_ID)
        {
            return dal.GetPlanMember(speName, pro_ID);
        }

        /// <summary>
        /// 审核人查看审核记录
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetTranProAuditedUser(int speID, string type, int allotID)
        {
            return dal.GetTranProAuditedUser(speID, type, allotID);
        }
        /// <summary>
        /// 审核人查看审核记录
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetDistinctTranProAuditedUser(int speID, string type, int allotID)
        {
            return dal.GetDistinctTranProAuditedUser(speID, type, allotID);
        }
        /// <summary>
        /// 新增产值分配人员明细信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertProjectAllotValueByMemberDetatil(TranProjectValueAllotByMemberEntity dataEntity)
        {
            return dal.InsertProjectAllotValueByMemberDetatil(dataEntity);
        }
          // <summary>
        /// 更新产值分配人员明细信息-转暖通所
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateProjectAllotValueByMemberDetatil(TranProjectValueAllotByMemberEntity dataEntity)
        {
            return dal.UpdateProjectAllotValueByMemberDetatil(dataEntity);
        }
    }
}
