﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using TG.DAL;

namespace TG.BLL
{
    public class TblAreaBll
    {
        TblAreaDal tbldal = new TblAreaDal();
        public List<TblAreaModel> GetListByararid(string areapid)
        {
            return tbldal.GetListByareapid(areapid);
        }
        public List<TblAreaModel> GetListByName(string areaname)
        {
            return tbldal.GetListByName(areaname);
        }
        public List<string> GetListName(string areapid)
        {
            List<string> name = new List<string>();
            List<TblAreaModel> list = GetListByararid(areapid);
            foreach (TblAreaModel item in list)
            {
                string str = item.AreaName;
                name.Add(str);
            }
            return name;
        }
        public string GetShow()
        {
            return tbldal.GetShow();
        }
        public int Update(string areaname, string toaspx)
        {
            tbldal.Update1(areaname);
            return tbldal.Update2(areaname, toaspx);
        }
        public int GetId(string areaname)
        {
            object o = tbldal.GetId(areaname);
            return Convert.ToInt32(o);
        }
        public string GetToAspx()
        {
            return tbldal.GetToAspx();
        }
    }
}
