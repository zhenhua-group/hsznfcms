﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public class cm_TestCoperation
    {
        private readonly TG.DAL.cm_TestCoperation dal = new TG.DAL.cm_TestCoperation();
        public cm_TestCoperation()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int cpr_Id)
        {
            return dal.Exists(cpr_Id);
        }
         /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string cprName,string sysno)
        {
            return dal.Exists(cprName,sysno);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_TestCoperation model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_TestCoperation model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int cpr_Id)
        {

            return dal.Delete(cpr_Id);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string cpr_Idlist)
        {
            return dal.DeleteList(cpr_Idlist);
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public ArrayList DeleteDesignList(string cpr_Idlist)
        {
            return dal.DeleteDesignList(cpr_Idlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_TestCoperation GetModel(int cpr_Id)
        {

            return dal.GetModel(cpr_Id);
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_TestCoperation> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_TestCoperation> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_TestCoperation> modelList = new List<TG.Model.cm_TestCoperation>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_TestCoperation model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_TestCoperation();
                    if (dt.Rows[n]["cpr_Id"] != null && dt.Rows[n]["cpr_Id"].ToString() != "")
                    {
                        model.cpr_Id = int.Parse(dt.Rows[n]["cpr_Id"].ToString());
                    }
                    if (dt.Rows[n]["cst_Id"] != null && dt.Rows[n]["cst_Id"].ToString() != "")
                    {
                        model.cst_Id = int.Parse(dt.Rows[n]["cst_Id"].ToString());
                    }
                    if (dt.Rows[n]["cpr_No"] != null && dt.Rows[n]["cpr_No"].ToString() != "")
                    {
                        model.cpr_No = dt.Rows[n]["cpr_No"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Type"] != null && dt.Rows[n]["cpr_Type"].ToString() != "")
                    {
                        model.cpr_Type = dt.Rows[n]["cpr_Type"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Name"] != null && dt.Rows[n]["cpr_Name"].ToString() != "")
                    {
                        model.cpr_Name = dt.Rows[n]["cpr_Name"].ToString();
                    }
                    if (dt.Rows[n]["BuildUnit"] != null && dt.Rows[n]["BuildUnit"].ToString() != "")
                    {
                        model.BuildUnit = dt.Rows[n]["BuildUnit"].ToString();
                    }
                    if (dt.Rows[n]["Floor"] != null && dt.Rows[n]["Floor"].ToString() != "")
                    {
                        model.Floor = dt.Rows[n]["Floor"].ToString();
                    }
                    if (dt.Rows[n]["ChgPeople"] != null && dt.Rows[n]["ChgPeople"].ToString() != "")
                    {
                        model.ChgPeople = dt.Rows[n]["ChgPeople"].ToString();
                    }
                    if (dt.Rows[n]["ChgPhone"] != null && dt.Rows[n]["ChgPhone"].ToString() != "")
                    {
                        model.ChgPhone = dt.Rows[n]["ChgPhone"].ToString();
                    }
                    if (dt.Rows[n]["ChgJia"] != null && dt.Rows[n]["ChgJia"].ToString() != "")
                    {
                        model.ChgJia = dt.Rows[n]["ChgJia"].ToString();
                    }
                    if (dt.Rows[n]["ChgJiaPhone"] != null && dt.Rows[n]["ChgJiaPhone"].ToString() != "")
                    {
                        model.ChgJiaPhone = dt.Rows[n]["ChgJiaPhone"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Unit"] != null && dt.Rows[n]["cpr_Unit"].ToString() != "")
                    {
                        model.cpr_Unit = dt.Rows[n]["cpr_Unit"].ToString();
                    }
                    if (dt.Rows[n]["BuildPosition"] != null && dt.Rows[n]["BuildPosition"].ToString() != "")
                    {
                        model.BuildPosition = dt.Rows[n]["BuildPosition"].ToString();
                    }
                    if (dt.Rows[n]["Industry"] != null && dt.Rows[n]["Industry"].ToString() != "")
                    {
                        model.Industry = dt.Rows[n]["Industry"].ToString();
                    }
                    if (dt.Rows[n]["BuildSrc"] != null && dt.Rows[n]["BuildSrc"].ToString() != "")
                    {
                        model.BuildSrc = dt.Rows[n]["BuildSrc"].ToString();
                    }
                    if (dt.Rows[n]["TableMaker"] != null && dt.Rows[n]["TableMaker"].ToString() != "")
                    {
                        model.TableMaker = dt.Rows[n]["TableMaker"].ToString();
                    }
                    if (dt.Rows[n]["cpr_DoneDate"] != null && dt.Rows[n]["cpr_DoneDate"].ToString() != "")
                    {
                        model.cpr_DoneDate = DateTime.Parse(dt.Rows[n]["cpr_DoneDate"].ToString());
                    }
                    if (dt.Rows[n]["cpr_Mark"] != null && dt.Rows[n]["cpr_Mark"].ToString() != "")
                    {
                        model.cpr_Mark = dt.Rows[n]["cpr_Mark"].ToString();
                    }
                    if (dt.Rows[n]["StaticPoint"] != null && dt.Rows[n]["StaticPoint"].ToString() != "")
                    {
                        model.StaticPoint = int.Parse(dt.Rows[n]["StaticPoint"].ToString());
                    }
                    if (dt.Rows[n]["StaticMax"] != null && dt.Rows[n]["StaticMax"].ToString() != "")
                    {
                        model.StaticMax = int.Parse(dt.Rows[n]["StaticMax"].ToString());
                    }
                    if (dt.Rows[n]["LoadingPoint"] != null && dt.Rows[n]["LoadingPoint"].ToString() != "")
                    {
                        model.LoadingPoint = int.Parse(dt.Rows[n]["LoadingPoint"].ToString());
                    }
                    if (dt.Rows[n]["LoadingMax"] != null && dt.Rows[n]["LoadingMax"].ToString() != "")
                    {
                        model.LoadingMax = int.Parse(dt.Rows[n]["LoadingMax"].ToString());
                    }
                    if (dt.Rows[n]["BigPoint"] != null && dt.Rows[n]["BigPoint"].ToString() != "")
                    {
                        model.BigPoint = int.Parse(dt.Rows[n]["BigPoint"].ToString());
                    }
                    if (dt.Rows[n]["SmallPoint"] != null && dt.Rows[n]["SmallPoint"].ToString() != "")
                    {
                        model.SmallPoint = int.Parse(dt.Rows[n]["SmallPoint"].ToString());
                    }
                    if (dt.Rows[n]["SampleNumber"] != null && dt.Rows[n]["SampleNumber"].ToString() != "")
                    {
                        model.SampleNumber = int.Parse(dt.Rows[n]["SampleNumber"].ToString());
                    }
                    if (dt.Rows[n]["OtherMethod"] != null && dt.Rows[n]["OtherMethod"].ToString() != "")
                    {
                        model.OtherMethod = dt.Rows[n]["OtherMethod"].ToString();
                    }
                    if (dt.Rows[n]["BuildType"] != null && dt.Rows[n]["BuildType"].ToString() != "")
                    {
                        model.BuildType = dt.Rows[n]["BuildType"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Type2"] != null && dt.Rows[n]["cpr_Type2"].ToString() != "")
                    {
                        model.cpr_Type2 = dt.Rows[n]["cpr_Type2"].ToString();
                    }
                    if (dt.Rows[n]["cpr_SignDate"] != null && dt.Rows[n]["cpr_SignDate"].ToString() != "")
                    {
                        model.cpr_SignDate = DateTime.Parse(dt.Rows[n]["cpr_SignDate"].ToString());
                    }
                    if (dt.Rows[n]["cpr_SignDate2"] != null && dt.Rows[n]["cpr_SignDate2"].ToString() != "")
                    {
                        model.cpr_SignDate2 = DateTime.Parse(dt.Rows[n]["cpr_SignDate2"].ToString());
                    }
                    if (dt.Rows[n]["MultiBuild"] != null && dt.Rows[n]["MultiBuild"].ToString() != "")
                    {
                        model.MultiBuild = dt.Rows[n]["MultiBuild"].ToString();
                    }
                    if (dt.Rows[n]["ProjectDate"] != null && dt.Rows[n]["ProjectDate"].ToString() != "")
                    {
                        model.ProjectDate = int.Parse(dt.Rows[n]["ProjectDate"].ToString());
                    }
                    if (dt.Rows[n]["InsertUserID"] != null && dt.Rows[n]["InsertUserID"].ToString() != "")
                    {
                        model.InsertUserID = int.Parse(dt.Rows[n]["InsertUserID"].ToString());
                    }
                    if (dt.Rows[n]["InsertDate"] != null && dt.Rows[n]["InsertDate"].ToString() != "")
                    {
                        model.InsertDate = DateTime.Parse(dt.Rows[n]["InsertDate"].ToString());
                    }
                    if (dt.Rows[n]["IsParamterEdit"] != null && dt.Rows[n]["IsParamterEdit"].ToString() != "")
                    {
                        model.IsParamterEdit = int.Parse(dt.Rows[n]["IsParamterEdit"].ToString());
                    }
                    if (dt.Rows[n]["PMUserID"] != null && dt.Rows[n]["PMUserID"].ToString() != "")
                    {
                        model.PMUserID = int.Parse(dt.Rows[n]["PMUserID"].ToString());
                    }
                    if (dt.Rows[n]["cpr_Acount"] != null && dt.Rows[n]["cpr_Acount"].ToString() != "")
                    {
                        model.cpr_Acount = decimal.Parse(dt.Rows[n]["cpr_Acount"].ToString());
                    }
                    if (dt.Rows[n]["cpr_ShijiAcount"] != null && dt.Rows[n]["cpr_ShijiAcount"].ToString() != "")
                    {
                        model.cpr_ShijiAcount = decimal.Parse(dt.Rows[n]["cpr_ShijiAcount"].ToString());
                    }
                    if (dt.Rows[n]["RegTime"] != null && dt.Rows[n]["RegTime"].ToString() != "")
                    {
                        model.RegTime = DateTime.Parse(dt.Rows[n]["RegTime"].ToString());
                    }
                    if (dt.Rows[n]["UpdateBy"] != null && dt.Rows[n]["UpdateBy"].ToString() != "")
                    {
                        model.UpdateBy = dt.Rows[n]["UpdateBy"].ToString();
                    }
                    if (dt.Rows[n]["LastUpdate"] != null && dt.Rows[n]["LastUpdate"].ToString() != "")
                    {
                        model.LastUpdate = DateTime.Parse(dt.Rows[n]["LastUpdate"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}
