﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_CostCompanyConfig
	/// </summary>
	public partial class cm_CostCompanyConfig
	{
		private readonly TG.DAL.cm_CostCompanyConfig dal=new TG.DAL.cm_CostCompanyConfig();
		public cm_CostCompanyConfig()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_CostCompanyConfig model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_CostCompanyConfig model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			return dal.Delete(ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			return dal.DeleteList(IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_CostCompanyConfig GetModel(int ID)
		{
			
			return dal.GetModel(ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_CostCompanyConfig GetModelByCache(int ID)
		{
			
			string CacheKey = "cm_CostCompanyConfigModel-" + ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_CostCompanyConfig)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_CostCompanyConfig> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_CostCompanyConfig> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_CostCompanyConfig> modelList = new List<TG.Model.cm_CostCompanyConfig>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_CostCompanyConfig model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_CostCompanyConfig();
					if(dt.Rows[n]["ID"]!=null && dt.Rows[n]["ID"].ToString()!="")
					{
						model.ID=int.Parse(dt.Rows[n]["ID"].ToString());
					}
					if(dt.Rows[n]["CmpMngPrt"]!=null && dt.Rows[n]["CmpMngPrt"].ToString()!="")
					{
						model.CmpMngPrt=decimal.Parse(dt.Rows[n]["CmpMngPrt"].ToString());
					}
					if(dt.Rows[n]["ProCostPrt"]!=null && dt.Rows[n]["ProCostPrt"].ToString()!="")
					{
						model.ProCostPrt=decimal.Parse(dt.Rows[n]["ProCostPrt"].ToString());
					}
					if(dt.Rows[n]["ProChgPrt"]!=null && dt.Rows[n]["ProChgPrt"].ToString()!="")
					{
						model.ProChgPrt=decimal.Parse(dt.Rows[n]["ProChgPrt"].ToString());
					}
					if(dt.Rows[n]["RegUserPrt"]!=null && dt.Rows[n]["RegUserPrt"].ToString()!="")
					{
						model.RegUserPrt=decimal.Parse(dt.Rows[n]["RegUserPrt"].ToString());
					}
					if(dt.Rows[n]["DesignPrt"]!=null && dt.Rows[n]["DesignPrt"].ToString()!="")
					{
						model.DesignPrt=decimal.Parse(dt.Rows[n]["DesignPrt"].ToString());
					}
					if(dt.Rows[n]["AuditPrt"]!=null && dt.Rows[n]["AuditPrt"].ToString()!="")
					{
						model.AuditPrt=decimal.Parse(dt.Rows[n]["AuditPrt"].ToString());
					}
					if(dt.Rows[n]["TotalUser"]!=null && dt.Rows[n]["TotalUser"].ToString()!="")
					{
						model.TotalUser=decimal.Parse(dt.Rows[n]["TotalUser"].ToString());
					}
					if(dt.Rows[n]["CollegeUser"]!=null && dt.Rows[n]["CollegeUser"].ToString()!="")
					{
						model.CollegeUser=decimal.Parse(dt.Rows[n]["CollegeUser"].ToString());
					}
					if(dt.Rows[n]["Used"]!=null && dt.Rows[n]["Used"].ToString()!="")
					{
						model.Used=int.Parse(dt.Rows[n]["Used"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}
        public DataSet GetCostCompanyListByPager(int startIndex, int endIndex)
        {

            return dal.GetCostCompanyListByPager(startIndex, endIndex);
        }
		#endregion  Method
	}
}

