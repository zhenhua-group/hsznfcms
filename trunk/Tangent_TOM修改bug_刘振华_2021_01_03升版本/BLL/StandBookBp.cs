﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;
using System.Data.SqlClient;
using System.Data;

namespace TG.BLL
{
    public class StandBookBp
    {
        StandBookDA dal = new StandBookDA();
        public List<StandBookEntity> GetList(string option, string unit_name, string year, string cprName)
        {
            List<TG.Model.StandBookEntity> list = dal.GetList(unit_name, year, cprName);
            foreach (TG.Model.StandBookEntity item in list)
            {
                List<TG.Model.AcoutAndTime> listacount = dal.GetListAcount(item.Cprid);
                foreach (TG.Model.AcoutAndTime model in listacount)
                {
                    if (option == "1")
                    {
                        item.Chargedetail += model.Time.ToString("yyyy-MM-dd") + "收款" + model.Acount.ToString("f2") + "万元";
                    }
                    else
                    {
                        item.Chargedetail += model.Time.ToString("yyyy-MM-dd") + "收款" + model.Acount.ToString("f2") + "万元<br/>";
                    }

                    //item.CprName += "+" + model.Acount.ToString("f2");
                    item.Acount += model.Acount;
                }
            }
            return list;
        }
        public List<ChargeDate> GetListChargeDate(string unit_name, string year)
        {
            List<TG.Model.ChargeDate> list = dal.GetListChargeDate(unit_name, year);

            return list;
        }
        public List<ChargeDate> GetListChargeDate(string unit_name, string year, string cprName)
        {
            List<TG.Model.ChargeDate> list = dal.GetListChargeDate(unit_name, year, cprName);

            return list;
        }
        public List<Outsummary> GetListSummary(string unit_name, string year, string cprName)
        {
            return dal.GetListSummary(unit_name, year, cprName);
        }
        public List<Outsummary> GetListSummary(string unit_name, string year)
        {
            return dal.GetListSummary(unit_name, year);
        }
        public List<Collection> GetListCollection(string beginmonth, string endmonth)
        {
            return dal.GetListCollection(beginmonth, endmonth);
        }
        /// <summary>
        /// 部门产值分配统计大表存储过程
        /// </summary>
        public List<TG.Model.ProjectValueAllot> GetMemberProjectValueAllotProc(string query, string query1, string query2)
        {
            return dal.GetMemberProjectValueAllotProc(query, query1, query2);
        }
        /// <summary>
        /// 部门产值分配统计全人员表存储过程
        /// </summary>
        /// <param name="unitname"></param>
        /// <param name="unitid"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable GetMemberProjectValueAllotDetailProc(string unitname, int unitid, string year)
        {
            DataTable dt = dal.GetMemberProjectValueAllotDetailProc(unitname, unitid, year);
            return dt;
        }
        /// <summary>
        /// 部门产值取得人员的产值
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public decimal GetMemberProjectValueAllotSql(string param0, string param1, string param2, string year)
        {
            return dal.GetMemberProjectValueAllotSql(param0, param1, param2, year);
        }
        /// <summary>
        /// 每个人所补的产值
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public decimal GetMemberProjectComValue(string query)
        {
            return dal.GetMemberProjectComValue(query);
        }
        /// <summary>
        /// 项目产值分配明细表存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, string query1, string query2, int startIndex, int endIndex)
        {
            return dal.GetListByPageProc(query, query1, query2, startIndex, endIndex);
        }
        /// <summary>
        /// 获取项目产值分配明细表需要分页的数据总数
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageProcCount(string query, string query1, string query2)
        {
            return dal.GetListPageProcCount(query, query1, query2);
        }
        /// <summary>
        /// 获取产值分配表需要字段数据总和
        /// </summary>
        public SqlDataReader GetListPageProcSum(string query, string query1, string query2)
        {
            return dal.GetListPageProcSum(query, query1, query2);
        }
        //某个项目的产值人员
        public DataTable GetMemberProjectValueAllot(int pro_id, string year, string sqlwhere)
        {
            return dal.GetMemberProjectValueAllot(pro_id, year, sqlwhere);
        }
        /// <summary>
        /// 项目产值分配表导出
        /// </summary>
        /// <returns></returns>
        public DataTable GetExcelProjectValueAllot(string query, string query1, string query2)
        {
            return dal.GetExcelProjectValueAllot(query, query1, query2);
        }
        /// <summary>
        /// 个人产值分配表(一)分页存储过程
        /// </summary>
        public SqlDataReader GetMemberValueAllotProc(string query, string query1, string query2, int startIndex, int endIndex)
        {
            return dal.GetMemberValueAllotProc(query, query1, query2, startIndex, endIndex);
        }
        /// <summary>
        /// 获取个人产值分配表(一)需要分页的数据总数
        /// </summary>
        public object GetMemberValueAllotCount(string query, string query1, string query2)
        {
            return dal.GetMemberValueAllotCount(query, query1, query2);
        }
        /// <summary>
        /// 获取个人产值分配表(一)需要字段数据总和
        /// </summary>
        public SqlDataReader GetMemberValueAllotSum(string query, string query1, string query2)
        {
            return dal.GetMemberValueAllotSum(query, query1, query2);
        }
        /// <summary>
        /// 个人产值分配表(一)导出
        /// </summary>
        /// <returns></returns>
        public DataTable GetExcelMemberValueAllot(string query, string query1, string query2)
        {
            return dal.GetExcelMemberValueAllot(query, query1, query2);
        }
        /// <summary>
        /// 个人产值分配表(二)分页存储过程2
        /// </summary>
        public SqlDataReader GetMemberValueAllotProc2(string query, string query1, string query2, int startIndex, int endIndex)
        {
            return dal.GetMemberValueAllotProc2(query, query1, query2, startIndex, endIndex);
        }
        /// <summary>
        /// 获取个人产值分配表(二)需要分页的总条数2
        /// </summary>
        public object GetMemberValueAllotCount2(string query, string query1, string query2)
        {
            return dal.GetMemberValueAllotCount2(query, query1, query2);
        }
        /// <summary>
        /// 获取个人产值分配表(二)需要字段数据总和2
        /// </summary>
        public SqlDataReader GetMemberValueAllotSum2(string query, string query1, string query2)
        {
            return dal.GetMemberValueAllotSum2(query, query1, query2);
        }
        /// <summary>
        /// 个人产值分配表(二)导出
        /// </summary>
        /// <returns></returns>
        public DataTable GetExcelMemberValueAllot2(string query, string query1, string query2)
        {
            return dal.GetExcelMemberValueAllot2(query, query1, query2);
        }
        /// <summary>
        /// 个人产值明细查询点击查看
        /// </summary>
        /// <returns></returns>
        public DataSet GetMemberProjectDetail(int memberid, string year)
        {
            return dal.GetMemberProjectDetail(memberid, year);
        }
        /// <summary>
        /// 方案补贴的未补贴总产值 zxq 20131226
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitname"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public object GetRemainAllotValue(string year, string unitname, int unitid)
        {
            return dal.GetRemainAllotValue(year, unitname, unitid);
        }
    }
}
