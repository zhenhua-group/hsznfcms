﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TG.Model;

namespace TG.BLL
{
    public class cm_ProjectInvoice
    {
        private TG.DAL.cm_ProjectInvoice dal = new TG.DAL.cm_ProjectInvoice();

         /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectInvoice model)
        {
            return dal.Update(model);
        }
         /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            return dal.Delete(ID);
        }
        /// <summary>
        /// 取得合同
        /// </summary>
        /// <param name="cpr_id"></param>
        /// <returns></returns>
        public TG.Model.cm_Coperation_Info GetCprModelInfo(int cpr_id)
        {
            return dal.GetCprModelInfo(cpr_id);
        }
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectInvoice GetModel(int ID)
        {
            return dal.GetModel(ID);
        }
        /// <summary>
        /// 新增开票
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int AddProjectInvoice(cm_ProjectInvoiceEntity model, string uerList)
        {
            return dal.AddProjectInvoice(model, uerList);
        }
          /// <summary>
        /// 合同开票导出
        /// </summary>
        /// <param name="sqlwhere"></param>
        /// <returns></returns>
        public DataSet GetCoperationInvoiceExport(string sqlwhere, string str)
        {
            return dal.GetCoperationInvoiceExport(sqlwhere, str);
        }
    }
}
