﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.Text;
using System.Linq;
namespace TG.BLL
{
    /// <summary>
    /// cm_SuperCoperation
    /// </summary>
    public partial class cm_SuperCoperation
    {
        private readonly TG.DAL.cm_SuperCoperation dal = new TG.DAL.cm_SuperCoperation();
        public cm_SuperCoperation()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int cpr_Id)
        {
            return dal.Exists(cpr_Id);
        }
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string cprName,string sysno)
        {
            return dal.Exists(cprName,sysno);
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_SuperCoperation model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_SuperCoperation model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int cpr_Id)
        {

            return dal.Delete(cpr_Id);
        }
        ///// <summary>
        ///// 删除一条数据
        ///// </summary>
        //public bool DeleteList(string cpr_Idlist)
        //{
        //    return dal.DeleteList(cpr_Idlist);
        //}
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public ArrayList DeleteList(string cpr_Idlist)
        {
            return dal.DeleteList(cpr_Idlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_SuperCoperation GetModel(int cpr_Id)
        {

            return dal.GetModel(cpr_Id);
        }
        /// <summary>
        /// 根据生产经营合同ID得到一个对象实体
        /// </summary>
        public TG.Model.cm_SuperCoperation GetFatherModel(int cpr_FID)
        {
            return dal.GetFatherModel(cpr_FID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_SuperCoperation GetModelByCache(int cpr_Id)
        {

            string CacheKey = "cm_SuperCoperationModel-" + cpr_Id;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(cpr_Id);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_SuperCoperation)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_SuperCoperation> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_SuperCoperation> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_SuperCoperation> modelList = new List<TG.Model.cm_SuperCoperation>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_SuperCoperation model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_SuperCoperation();
                    if (dt.Rows[n]["cpr_Id"] != null && dt.Rows[n]["cpr_Id"].ToString() != "")
                    {
                        model.cpr_Id = int.Parse(dt.Rows[n]["cpr_Id"].ToString());
                    }
                    if (dt.Rows[n]["cst_Id"] != null && dt.Rows[n]["cst_Id"].ToString() != "")
                    {
                        model.cst_Id = int.Parse(dt.Rows[n]["cst_Id"].ToString());
                    }
                    if (dt.Rows[n]["cpr_No"] != null && dt.Rows[n]["cpr_No"].ToString() != "")
                    {
                        model.cpr_No = dt.Rows[n]["cpr_No"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Type"] != null && dt.Rows[n]["cpr_Type"].ToString() != "")
                    {
                        model.cpr_Type = dt.Rows[n]["cpr_Type"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Type2"] != null && dt.Rows[n]["cpr_Type2"].ToString() != "")
                    {
                        model.cpr_Type2 = dt.Rows[n]["cpr_Type2"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Name"] != null && dt.Rows[n]["cpr_Name"].ToString() != "")
                    {
                        model.cpr_Name = dt.Rows[n]["cpr_Name"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Unit"] != null && dt.Rows[n]["cpr_Unit"].ToString() != "")
                    {
                        model.cpr_Unit = dt.Rows[n]["cpr_Unit"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Acount"] != null && dt.Rows[n]["cpr_Acount"].ToString() != "")
                    {
                        model.cpr_Acount = decimal.Parse(dt.Rows[n]["cpr_Acount"].ToString());
                    }
                    if (dt.Rows[n]["cpr_ShijiAcount"] != null && dt.Rows[n]["cpr_ShijiAcount"].ToString() != "")
                    {
                        model.cpr_ShijiAcount = decimal.Parse(dt.Rows[n]["cpr_ShijiAcount"].ToString());
                    }
                    if (dt.Rows[n]["cpr_Touzi"] != null && dt.Rows[n]["cpr_Touzi"].ToString() != "")
                    {
                        model.cpr_Touzi = decimal.Parse(dt.Rows[n]["cpr_Touzi"].ToString());
                    }
                    if (dt.Rows[n]["cpr_ShijiTouzi"] != null && dt.Rows[n]["cpr_ShijiTouzi"].ToString() != "")
                    {
                        model.cpr_ShijiTouzi = decimal.Parse(dt.Rows[n]["cpr_ShijiTouzi"].ToString());
                    }
                    if (dt.Rows[n]["cpr_Process"] != null && dt.Rows[n]["cpr_Process"].ToString() != "")
                    {
                        model.cpr_Process = dt.Rows[n]["cpr_Process"].ToString();
                    }
                    if (dt.Rows[n]["cpr_SignDate"] != null && dt.Rows[n]["cpr_SignDate"].ToString() != "")
                    {
                        model.cpr_SignDate = DateTime.Parse(dt.Rows[n]["cpr_SignDate"].ToString());
                    }
                    if (dt.Rows[n]["cpr_DoneDate"] != null && dt.Rows[n]["cpr_DoneDate"].ToString() != "")
                    {
                        model.cpr_DoneDate = DateTime.Parse(dt.Rows[n]["cpr_DoneDate"].ToString());
                    }
                    if (dt.Rows[n]["cpr_Mark"] != null && dt.Rows[n]["cpr_Mark"].ToString() != "")
                    {
                        model.cpr_Mark = dt.Rows[n]["cpr_Mark"].ToString();
                    }
                    if (dt.Rows[n]["BuildArea"] != null && dt.Rows[n]["BuildArea"].ToString() != "")
                    {
                        model.BuildArea = decimal.Parse(dt.Rows[n]["BuildArea"].ToString());
                    }
                    if (dt.Rows[n]["AreaUnit"] != null && dt.Rows[n]["AreaUnit"].ToString() != "")
                    {
                        model.AreaUnit = dt.Rows[n]["AreaUnit"].ToString();
                    }
                    if (dt.Rows[n]["ChgPeople"] != null && dt.Rows[n]["ChgPeople"].ToString() != "")
                    {
                        model.ChgPeople = dt.Rows[n]["ChgPeople"].ToString();
                    }
                    if (dt.Rows[n]["ChgPhone"] != null && dt.Rows[n]["ChgPhone"].ToString() != "")
                    {
                        model.ChgPhone = dt.Rows[n]["ChgPhone"].ToString();
                    }
                    if (dt.Rows[n]["ChgJia"] != null && dt.Rows[n]["ChgJia"].ToString() != "")
                    {
                        model.ChgJia = dt.Rows[n]["ChgJia"].ToString();
                    }
                    if (dt.Rows[n]["ChgJiaPhone"] != null && dt.Rows[n]["ChgJiaPhone"].ToString() != "")
                    {
                        model.ChgJiaPhone = dt.Rows[n]["ChgJiaPhone"].ToString();
                    }
                    if (dt.Rows[n]["BuildPosition"] != null && dt.Rows[n]["BuildPosition"].ToString() != "")
                    {
                        model.BuildPosition = dt.Rows[n]["BuildPosition"].ToString();
                    }
                    if (dt.Rows[n]["Industry"] != null && dt.Rows[n]["Industry"].ToString() != "")
                    {
                        model.Industry = dt.Rows[n]["Industry"].ToString();
                    }
                    if (dt.Rows[n]["BuildUnit"] != null && dt.Rows[n]["BuildUnit"].ToString() != "")
                    {
                        model.BuildUnit = dt.Rows[n]["BuildUnit"].ToString();
                    }
                    if (dt.Rows[n]["BuildSrc"] != null && dt.Rows[n]["BuildSrc"].ToString() != "")
                    {
                        model.BuildSrc = dt.Rows[n]["BuildSrc"].ToString();
                    }
                    if (dt.Rows[n]["TableMaker"] != null && dt.Rows[n]["TableMaker"].ToString() != "")
                    {
                        model.TableMaker = dt.Rows[n]["TableMaker"].ToString();
                    }
                    if (dt.Rows[n]["RegTime"] != null && dt.Rows[n]["RegTime"].ToString() != "")
                    {
                        model.RegTime = DateTime.Parse(dt.Rows[n]["RegTime"].ToString());
                    }
                    if (dt.Rows[n]["UpdateBy"] != null && dt.Rows[n]["UpdateBy"].ToString() != "")
                    {
                        model.UpdateBy = dt.Rows[n]["UpdateBy"].ToString();
                    }
                    if (dt.Rows[n]["LastUpdate"] != null && dt.Rows[n]["LastUpdate"].ToString() != "")
                    {
                        model.LastUpdate = DateTime.Parse(dt.Rows[n]["LastUpdate"].ToString());
                    }
                    if (dt.Rows[n]["BuildType"] != null && dt.Rows[n]["BuildType"].ToString() != "")
                    {
                        model.BuildType = dt.Rows[n]["BuildType"].ToString();
                    }
                    if (dt.Rows[n]["StructType"] != null && dt.Rows[n]["StructType"].ToString() != "")
                    {
                        model.StructType = dt.Rows[n]["StructType"].ToString();
                    }
                    if (dt.Rows[n]["Floor"] != null && dt.Rows[n]["Floor"].ToString() != "")
                    {
                        model.Floor = dt.Rows[n]["Floor"].ToString();
                    }
                    if (dt.Rows[n]["BuildStructType"] != null && dt.Rows[n]["BuildStructType"].ToString() != "")
                    {
                        model.BuildStructType = dt.Rows[n]["BuildStructType"].ToString();
                    }
                    if (dt.Rows[n]["MultiBuild"] != null && dt.Rows[n]["MultiBuild"].ToString() != "")
                    {
                        model.MultiBuild = dt.Rows[n]["MultiBuild"].ToString();
                    }
                    if (dt.Rows[n]["JieGou"] != null && dt.Rows[n]["JieGou"].ToString() != "")
                    {
                        model.JieGou = dt.Rows[n]["JieGou"].ToString();
                    }
                    if (dt.Rows[n]["GeiPs"] != null && dt.Rows[n]["GeiPs"].ToString() != "")
                    {
                        model.GeiPs = dt.Rows[n]["GeiPs"].ToString();
                    }
                    if (dt.Rows[n]["NuanT"] != null && dt.Rows[n]["NuanT"].ToString() != "")
                    {
                        model.NuanT = dt.Rows[n]["NuanT"].ToString();
                    }
                    if (dt.Rows[n]["DianQ"] != null && dt.Rows[n]["DianQ"].ToString() != "")
                    {
                        model.DianQ = dt.Rows[n]["DianQ"].ToString();
                    }
                    if (dt.Rows[n]["InsertUserID"] != null && dt.Rows[n]["InsertUserID"].ToString() != "")
                    {
                        model.InsertUserID = int.Parse(dt.Rows[n]["InsertUserID"].ToString());
                    }
                    if (dt.Rows[n]["InsertDate"] != null && dt.Rows[n]["InsertDate"].ToString() != "")
                    {
                        model.InsertDate = DateTime.Parse(dt.Rows[n]["InsertDate"].ToString());
                    }
                    if (dt.Rows[n]["IsParamterEdit"] != null && dt.Rows[n]["IsParamterEdit"].ToString() != "")
                    {
                        model.IsParamterEdit = int.Parse(dt.Rows[n]["IsParamterEdit"].ToString());
                    }
                    if (dt.Rows[n]["PMUserID"] != null && dt.Rows[n]["PMUserID"].ToString() != "")
                    {
                        model.PMUserID = int.Parse(dt.Rows[n]["PMUserID"].ToString());
                    }
                    if (dt.Rows[n]["cpr_FID"] != null && dt.Rows[n]["cpr_FID"].ToString() != "")
                    {
                        model.cpr_FID = int.Parse(dt.Rows[n]["cpr_FID"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
        public void UpdateByEdit(string p, int copsysno)
        {
            TG.DAL.cm_Coperation cpr_dal = new TG.DAL.cm_Coperation();
            TG.Model.cm_SuperCoperation model = dal.GetModel(copsysno);
            if (model != null)
            {
                //得到合同表的信息
                TG.Model.cm_Coperation cpr = cpr_dal.GetModel(model.cpr_FID);
                p = p.Replace("|*|", "\n");
                string[] arratList = p.Split('\n');
                for (int i = 0; i < arratList.Length; i++)
                {
                    string typestr = arratList[i].Split(':')[0];
                    switch (typestr)
                    {
                        case "cpr_No":
                            model.cpr_No = arratList[i].Split(':')[1];
                            cpr.cpr_No = model.cpr_No;
                            break;
                        case "cpr_Type":
                            model.cpr_Type = returnName(arratList[i].Split(':')[1]);
                            cpr.cpr_Type = model.cpr_Type;
                            break;
                        case "cpr_Type2":
                            model.cpr_Type2 = arratList[i].Split(':')[1];
                            cpr.cpr_Type2 = model.cpr_Type2;
                            break;
                        case "cpr_Name":
                            model.cpr_Name = arratList[i].Split(':')[1];
                            cpr.cpr_Name = model.cpr_Name;
                            break;
                        case "BuildArea":
                            model.BuildArea = Convert.ToDecimal(arratList[i].Split(':')[1]);
                            break;
                        case "AreaUnit":
                            model.AreaUnit = arratList[i].Split(':')[1];
                            break;
                        case "BuildUnit":
                            model.BuildUnit = arratList[i].Split(':')[1];
                            cpr.BuildUnit = model.BuildUnit;
                            break;
                        case "cpr_Area":
                            model.BuildArea = Convert.ToDecimal(arratList[i].Split(':')[1]);
                            break;
                        ////
                        case "StructType":
                            model.StructType = GetXML("SuperStructType", arratList[i].Split(':')[1]);
                            cpr.StructType = model.StructType;
                            break;
                        case "BuildStructType":
                            model.BuildStructType = GetXML("SuperStructType", arratList[i].Split(':')[1]);
                            cpr.BuildStructType = model.BuildStructType;
                            break;
                        case "BuildTypelevel":
                            model.BuildType = returnName(arratList[i].Split(':')[1]);
                            cpr.BuildType = model.BuildType;
                            break;
                        //////
                        case "ChgPeople":
                            model.ChgPeople = arratList[i].Split(':')[1];
                            cpr.ChgPeople = model.ChgPeople;
                            break;
                        case "ChgPhone":
                            model.ChgPhone = arratList[i].Split(':')[1];
                            cpr.ChgPhone = model.ChgPhone;
                            break;
                        case "HiddenPMUserID":
                            model.PMUserID = int.Parse(arratList[i].Split(':')[1]);
                            cpr.PMUserID = int.Parse(model.PMUserID.ToString());
                            break;

                        case "floor":
                            model.Floor = arratList[i].Split(':')[1];
                            cpr.Floor = model.Floor;
                            break;
                        case "ChgJia":
                            model.ChgJia = arratList[i].Split(':')[1];
                            cpr.ChgJia = model.ChgJia;
                            break;
                        case "ChgJiaPhone":
                            model.ChgJiaPhone = arratList[i].Split(':')[1];
                            cpr.ChgJiaPhone = model.ChgJiaPhone;
                            break;
                        case "cpr_Unit":
                            model.cpr_Unit = arratList[i].Split(':')[1];
                            cpr.cpr_Unit = model.cpr_Unit;
                            break;
                        case "cpr_Acount":
                            model.cpr_Acount = Convert.ToDecimal(arratList[i].Split(':')[1].Length < 1 ? "0" : arratList[i].Split(':')[1]);
                            cpr.cpr_Acount = model.cpr_Acount;
                            break;
                        case "cpr_Touzi":
                            model.cpr_Touzi = Convert.ToDecimal(arratList[i].Split(':')[1].Length < 1 ? "0" : arratList[i].Split(':')[1]);
                            cpr.cpr_Touzi = model.cpr_Touzi;
                            break;
                        case "BuildPosition":
                            model.BuildPosition = arratList[i].Split(':')[1];
                            cpr.BuildPosition = model.BuildPosition;
                            break;
                        case "cpr_ShijiAcount":
                            model.cpr_ShijiAcount = Convert.ToDecimal(arratList[i].Split(':')[1].Length < 1 ? "0" : arratList[i].Split(':')[1]);
                            cpr.cpr_ShijiAcount = model.cpr_ShijiAcount;
                            break;
                        case "cpr_ShijiTouzi":
                            model.cpr_ShijiTouzi = Convert.ToDecimal(arratList[i].Split(':')[1].Length < 1 ? "0" : arratList[i].Split(':')[1]);
                            cpr.cpr_ShijiTouzi = model.cpr_ShijiTouzi;
                            break;
                        case "Industry":
                            model.Industry = returnName(arratList[i].Split(':')[1]);
                            cpr.Industry = model.Industry;
                            break;
                        //
                        case "cpr_Process":
                            model.cpr_Process = returnPross(arratList[i].Split(':')[1]);
                            cpr.cpr_Process = model.cpr_Process;
                            break;
                        //
                        case "TableMaker":
                            model.TableMaker = arratList[i].Split(':')[1];
                            cpr.TableMaker = model.TableMaker;
                            break;
                        case "BuildSrc":
                            model.BuildSrc = returnName(arratList[i].Split(':')[1]);
                            cpr.BuildSrc = model.BuildSrc;
                            break;
                        case "cpr_SignDate":
                            model.cpr_SignDate = Convert.ToDateTime(arratList[i].Split(':')[1].Length < 1 ? DateTime.Now.ToString("yyyy-MM-dd") : arratList[i].Split(':')[1]);
                            cpr.cpr_SignDate = model.cpr_SignDate;
                            break;
                        case "cpr_SignDate2":
                            model.cpr_SignDate2 = Convert.ToDateTime(arratList[i].Split(':')[1].Length < 1 ? DateTime.Now.ToString("yyyy-MM-dd") : arratList[i].Split(':')[1]);
                            cpr.cpr_SignDate2 = model.cpr_SignDate2;
                            break;
                        case "cpr_DoneDate":
                            model.cpr_DoneDate = Convert.ToDateTime(arratList[i].Split(':')[1].Length < 1 ? DateTime.Now.ToString("yyyy-MM-dd") : arratList[i].Split(':')[1]);
                            cpr.cpr_DoneDate = model.cpr_DoneDate;
                            break;
                        case "MultiBuild":
                            model.MultiBuild = arratList[i].Split(':')[1];
                            cpr.MultiBuild = model.MultiBuild;
                            break;
                        case "cpr_Mark":
                            model.cpr_Mark = arratList[i].Split(':')[1];
                            cpr.cpr_Mark = model.cpr_Mark;
                            break;
                    }
                }
                //判断修改申请中包含建筑面积
                if (arratList.Where(a => a.Contains("BuildArea")).ToList().Count > 0)
                {
                    //平方千米判断
                    if (model.AreaUnit.Trim() != "㎡")
                    {
                        cpr.BuildArea = "0";
                    }
                    else
                    {
                        cpr.BuildArea = model.BuildArea.ToString();
                    }
                }
                //更新生产经营合同表信息
                if (cpr_dal.Update(cpr))
                {
                    //更新监理公司合同
                    dal.Update(model);
                }

            }
        }

        public string returnName(string pross)
        {
            string result = "";
            if (pross.Trim() != "")
            {
                string[] array = pross.Split(new char[] { ',' }, StringSplitOptions.None);
                TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

                for (int i = 0; i < array.Length; i++)
                {
                    result += bll_dic.GetModel(int.Parse(array[i])).dic_Name + ",";
                }
                result = result.IndexOf(",") > -1 ? result.Remove(result.Length - 1) : "";
            }
            return result;
        }
        public string GetXML(string type, string p)
        {
            StringBuilder builder = new StringBuilder();
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + type + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                //一级
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                Readxml(nodes, out builder, p);
            }
            if (builder.ToString().Length > 0)
            {
                return builder.ToString().Substring(1);
            }
            else
            {
                return builder.ToString();
            }
            //公共建筑^办公+工业建筑^重工厂房^轻工厂房
        }
        //第一级别
        public void Readxml(XmlNodeList xmlnl, out StringBuilder sb_, string p)
        {
            sb_ = new StringBuilder();
            string[] arraylist = p.Split(',');
            foreach (XmlNode xl in xmlnl)
            {
                XmlNodeList nodes = xl.ChildNodes;
                for (int i = 0; i < arraylist.Length; i++)
                {
                    if (xl.Attributes["name"].Value == arraylist[i])
                    {
                        if (xl.ChildNodes.Count != 0)
                        {
                            sb_.Append("+" + xl.Attributes["name"].Value);
                            ReadxmlChild(nodes, sb_, p);
                        }
                        else
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                        }
                    }
                }
            }
        }
        //第二级别和一下的级别
        public void ReadxmlChild(XmlNodeList xmlnl, StringBuilder sb_, string p)
        {

            string[] arraylist = p.Split(',');
            foreach (XmlNode xl in xmlnl)
            {
                XmlNodeList nodes = xl.ChildNodes;
                for (int i = 0; i < arraylist.Length; i++)
                {
                    if (xl.Attributes["name"].Value == arraylist[i])
                    {
                        if (xl.ChildNodes.Count != 0)
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                            ReadxmlChild(nodes, sb_, p);
                        }
                        else
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                        }
                    }
                }
            }
        }
        public string returnPross(string proess)
        {
            string result = "";
            string[] array = proess.Split(new char[] { ',' }, StringSplitOptions.None);
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

            for (int i = 0; i < array.Length - 1; i++)
            {
                result += bll_dic.GetModel(array[i]).ID + ",";
            }
            result = result.IndexOf(",") > -1 ? result.Remove(result.Length - 1) : "";
            return result;
        }
    }
}

