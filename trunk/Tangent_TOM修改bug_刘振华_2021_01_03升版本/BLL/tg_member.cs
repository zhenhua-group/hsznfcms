﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
    /// <summary>
    /// tg_member
    /// </summary>
    public partial class tg_member
    {
        private readonly TG.DAL.tg_member dal = new TG.DAL.tg_member();
        public tg_member()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int mem_ID)
        {
            return dal.Exists(mem_ID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.tg_member model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.tg_member model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int mem_ID)
        {

            return dal.Delete(mem_ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string mem_IDlist)
        {
            return dal.DeleteList(mem_IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.tg_member GetModel(int mem_ID)
        {

            return dal.GetModel(mem_ID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.tg_member GetModelByCache(int mem_ID)
        {

            string CacheKey = "tg_memberModel-" + mem_ID;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(mem_ID);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.tg_member)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.tg_member> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.tg_member> DataTableToList(DataTable dt)
        {
            List<TG.Model.tg_member> modelList = new List<TG.Model.tg_member>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.tg_member model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.tg_member();
                    if (dt.Rows[n]["mem_ID"] != null && dt.Rows[n]["mem_ID"].ToString() != "")
                    {
                        model.mem_ID = int.Parse(dt.Rows[n]["mem_ID"].ToString());
                    }
                    if (dt.Rows[n]["mem_Login"] != null && dt.Rows[n]["mem_Login"].ToString() != "")
                    {
                        model.mem_Login = dt.Rows[n]["mem_Login"].ToString();
                    }
                    if (dt.Rows[n]["mem_Name"] != null && dt.Rows[n]["mem_Name"].ToString() != "")
                    {
                        model.mem_Name = dt.Rows[n]["mem_Name"].ToString();
                    }
                    if (dt.Rows[n]["mem_Password"] != null && dt.Rows[n]["mem_Password"].ToString() != "")
                    {
                        model.mem_Password = dt.Rows[n]["mem_Password"].ToString();
                    }
                    if (dt.Rows[n]["mem_Birthday"] != null && dt.Rows[n]["mem_Birthday"].ToString() != "")
                    {
                        model.mem_Birthday = DateTime.Parse(dt.Rows[n]["mem_Birthday"].ToString());
                    }
                    if (dt.Rows[n]["mem_Sex"] != null && dt.Rows[n]["mem_Sex"].ToString() != "")
                    {
                        model.mem_Sex = dt.Rows[n]["mem_Sex"].ToString();
                    }
                    if (dt.Rows[n]["mem_Unit_ID"] != null && dt.Rows[n]["mem_Unit_ID"].ToString() != "")
                    {
                        model.mem_Unit_ID = int.Parse(dt.Rows[n]["mem_Unit_ID"].ToString());
                    }
                    if (dt.Rows[n]["mem_Speciality_ID"] != null && dt.Rows[n]["mem_Speciality_ID"].ToString() != "")
                    {
                        model.mem_Speciality_ID = int.Parse(dt.Rows[n]["mem_Speciality_ID"].ToString());
                    }
                    if (dt.Rows[n]["mem_Principalship_ID"] != null && dt.Rows[n]["mem_Principalship_ID"].ToString() != "")
                    {
                        model.mem_Principalship_ID = int.Parse(dt.Rows[n]["mem_Principalship_ID"].ToString());
                    }
                    if (dt.Rows[n]["mem_Telephone"] != null && dt.Rows[n]["mem_Telephone"].ToString() != "")
                    {
                        model.mem_Telephone = dt.Rows[n]["mem_Telephone"].ToString();
                    }
                    if (dt.Rows[n]["mem_Mobile"] != null && dt.Rows[n]["mem_Mobile"].ToString() != "")
                    {
                        model.mem_Mobile = dt.Rows[n]["mem_Mobile"].ToString();
                    }
                    if (dt.Rows[n]["mem_RegTime"] != null && dt.Rows[n]["mem_RegTime"].ToString() != "")
                    {
                        model.mem_RegTime = DateTime.Parse(dt.Rows[n]["mem_RegTime"].ToString());
                    }
                    if (dt.Rows[n]["mem_OutTime"] != null && dt.Rows[n]["mem_OutTime"].ToString() != "")
                    {
                        model.mem_OutTime = DateTime.Parse(dt.Rows[n]["mem_OutTime"].ToString());
                    }
                    if (dt.Rows[n]["mem_Status"] != null && dt.Rows[n]["mem_Status"].ToString() != "")
                    {
                        model.mem_Status = int.Parse(dt.Rows[n]["mem_Status"].ToString());
                    }
                    if (dt.Rows[n]["mem_Stats"] != null && dt.Rows[n]["mem_Stats"].ToString() != "")
                    {
                        model.mem_Stats = dt.Rows[n]["mem_Stats"].ToString();
                    }
                    if (dt.Rows[n]["mem_LastIP"] != null && dt.Rows[n]["mem_LastIP"].ToString() != "")
                    {
                        model.mem_LastIP = dt.Rows[n]["mem_LastIP"].ToString();
                    }
                    if (dt.Rows[n]["mem_HashKey"] != null && dt.Rows[n]["mem_HashKey"].ToString() != "")
                    {
                        model.mem_HashKey = dt.Rows[n]["mem_HashKey"].ToString();
                    }
                    if (dt.Rows[n]["mem_PostMessage"] != null && dt.Rows[n]["mem_PostMessage"].ToString() != "")
                    {
                        model.mem_PostMessage = int.Parse(dt.Rows[n]["mem_PostMessage"].ToString());
                    }
                    if (dt.Rows[n]["mem_Mailbox"] != null && dt.Rows[n]["mem_Mailbox"].ToString() != "")
                    {
                        model.mem_Mailbox = dt.Rows[n]["mem_Mailbox"].ToString();
                    }
                    if (dt.Rows[n]["mem_TPMlastver"] != null && dt.Rows[n]["mem_TPMlastver"].ToString() != "")
                    {
                        model.mem_TPMlastver = dt.Rows[n]["mem_TPMlastver"].ToString();
                    }
                    if (dt.Rows[n]["mem_isFired"] != null && dt.Rows[n]["mem_isFired"].ToString() != "")
                    {
                        model.mem_isFired = int.Parse(dt.Rows[n]["mem_isFired"].ToString());
                    }
                    if (dt.Rows[n]["mem_sign"] != null && dt.Rows[n]["mem_sign"].ToString() != "")
                    {
                        model.mem_sign = dt.Rows[n]["mem_sign"].ToString();
                    }
                    if (dt.Rows[n]["mem_SignPwd"] != null && dt.Rows[n]["mem_SignPwd"].ToString() != "")
                    {
                        model.mem_SignPwd = dt.Rows[n]["mem_SignPwd"].ToString();
                    }
                    if (dt.Rows[n]["mem_Stamp"] != null && dt.Rows[n]["mem_Stamp"].ToString() != "")
                    {
                        model.mem_Stamp = dt.Rows[n]["mem_Stamp"].ToString();
                    }
                    //2011 新加字段
                    if (dt.Rows[n]["mem_IsManager"] != null && dt.Rows[n]["mem_IsManager"].ToString() != "")
                    {
                        model.mem_IsManager = int.Parse(dt.Rows[n]["mem_IsManager"].ToString());
                    }
                    if (dt.Rows[n]["mem_IsProManager"] != null && dt.Rows[n]["mem_IsProManager"].ToString() != "")
                    {
                        model.mem_IsProManager = int.Parse(dt.Rows[n]["mem_IsProManager"].ToString());
                    }
                    if (dt.Rows[n]["mem_Hometown"] != null && dt.Rows[n]["mem_Hometown"].ToString() != "")
                    {
                        model.mem_Hometown = dt.Rows[n]["mem_Hometown"].ToString();
                    }
                    if (dt.Rows[n]["mem_GraduationTime"] != null && dt.Rows[n]["mem_GraduationTime"].ToString() != "")
                    {
                        model.mem_GraduationTime = dt.Rows[n]["mem_GraduationTime"].ToString();
                    }
                    if (dt.Rows[n]["mem_GraduationSchool"] != null && dt.Rows[n]["mem_GraduationSchool"].ToString() != "")
                    {
                        model.mem_GraduationSchool = dt.Rows[n]["mem_GraduationSchool"].ToString();
                    }
                    if (dt.Rows[n]["mem_Title"] != null && dt.Rows[n]["mem_Title"].ToString() != "")
                    {
                        model.mem_Title = dt.Rows[n]["mem_Title"].ToString();
                    }
                    if (dt.Rows[n]["mem_Major"] != null && dt.Rows[n]["mem_Major"].ToString() != "")
                    {
                        model.mem_Major = dt.Rows[n]["mem_Major"].ToString();
                    }
                    if (dt.Rows[n]["mem_Remark"] != null && dt.Rows[n]["mem_Remark"].ToString() != "")
                    {
                        model.mem_Remark = dt.Rows[n]["mem_Remark"].ToString();
                    }
                    if (dt.Rows[n]["mem_LastHostName"] != null && dt.Rows[n]["mem_LastHostName"].ToString() != "")
                    {
                        model.mem_LastHostName = dt.Rows[n]["mem_LastHostName"].ToString();
                    }
                    //排序
                    if (dt.Rows[n]["mem_Order"] != null && dt.Rows[n]["mem_Order"].ToString() != "")
                    {
                        model.mem_Order = Convert.ToInt32(dt.Rows[n]["mem_Order"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分阶段获得数据
        /// </summary>
        /// <param name="strWhere">where语句</param>
        /// <param name="pagesize">每页数量</param>
        /// <param name="pageIndex">要得到第几页的数据</param>
        /// <returns>数据集</returns>
        public DataSet GetListByPage(string strWhere, int pagesize, int pageIndex)
        {
            return dal.GetListByPage(strWhere, pagesize, pageIndex);
        }
        /// <summary>
        /// 得到用户列表，从 And 开始
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<TG.Model.tg_member> GetUsers(string whereSql)
        {
            return dal.GetUsers(whereSql);
        }

        /// <summary>
        /// 查询所有部门
        /// </summary>
        /// <returns></returns>
        public List<TG.Model.tg_unit> GetDepartments()
        {
            return dal.GetDepartments();
        }
         /// <summary>
        /// 根据条件获取用户列表
        /// </summary>
        /// <param name="memid">用户id</param>
        /// <param name="unitid">部门id</param>
        /// <param name="princid">角色id</param>
        /// <returns></returns>
        public DataSet GetUserList(int memid, int unitid, int princid)
        {
            return dal.GetUserList(memid, unitid, princid);
        }
        #endregion  Method
    }
}

