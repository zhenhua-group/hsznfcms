﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace TG.BLL
{
    public class QueryPagingParameters
    {
        public QueryPagingParameters()
        { }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string OrderBy { get; set; }

        public string Sort { get; set; }

        public string Where { get; set; }

        public string ProcedureName { get; set; }


        public int Total
        {
            get
            {
                if (_parameters.Count > 0)
                {
                    return (int)_parameters[5].Value;
                }
                return 0;
            }
        }

        private IList<SqlParameter> _parameters = new List<SqlParameter>();

        public SqlParameter[] ProcedureParameters
        {
            get
            {
                if (_parameters.Count == 0)
                {
                    _parameters.Add(new SqlParameter("PageIndex", PageIndex));
                    _parameters.Add(new SqlParameter("PageSize", PageSize));
                    _parameters.Add(new SqlParameter("Where", Where));
                    _parameters.Add(new SqlParameter("OrderBy", OrderBy));
                    _parameters.Add(new SqlParameter("Sort", Sort));

                    var outputTotal = new SqlParameter
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "Total",
                        DbType = DbType.Int32,
                        Size = 4
                    };

                    _parameters.Add(outputTotal);

                }

                return _parameters.ToArray();
            }
            set { _parameters = value; }
        }
    }
    public class QueryPagingParametersOther
    {
        public QueryPagingParametersOther()
        { }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string OrderBy { get; set; }

        public string Sort { get; set; }

        public string Where { get; set; }

        public string Where1 { get; set; }

        public string ProcedureName { get; set; }


        public int Total
        {
            get
            {
                if (_parameters.Count > 0)
                {
                    return (int)_parameters[6].Value;
                }
                return 0;
            }
        }

        private IList<SqlParameter> _parameters = new List<SqlParameter>();

        public SqlParameter[] ProcedureParameters
        {
            get
            {
                if (_parameters.Count == 0)
                {
                    _parameters.Add(new SqlParameter("PageIndex", PageIndex));
                    _parameters.Add(new SqlParameter("PageSize", PageSize));
                    _parameters.Add(new SqlParameter("Where", Where));
                    _parameters.Add(new SqlParameter("Where1", Where1));
                    _parameters.Add(new SqlParameter("OrderBy", OrderBy));
                    _parameters.Add(new SqlParameter("Sort", Sort));

                    var outputTotal = new SqlParameter
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "Total",
                        DbType = DbType.Int32,
                        Size = 4
                    };

                    _parameters.Add(outputTotal);

                }

                return _parameters.ToArray();
            }
            set { _parameters = value; }
        }
    }
    public class QueryPagingParametersAnOther
    {
        public QueryPagingParametersAnOther()
        { }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string OrderBy { get; set; }

        public string Sort { get; set; }

        public string Where { get; set; }

        public string Where1 { get; set; }
        public string Where2 { get; set; }
        public string ProcedureName { get; set; }


        public int Total
        {
            get
            {
                if (_parameters.Count > 0)
                {
                    return (int)_parameters[7].Value;
                }
                return 0;
            }
        }

        private IList<SqlParameter> _parameters = new List<SqlParameter>();

        public SqlParameter[] ProcedureParameters
        {
            get
            {
                if (_parameters.Count == 0)
                {
                    _parameters.Add(new SqlParameter("PageIndex", PageIndex));
                    _parameters.Add(new SqlParameter("PageSize", PageSize));
                    _parameters.Add(new SqlParameter("Where", Where));
                    _parameters.Add(new SqlParameter("Where1", Where1));
                    _parameters.Add(new SqlParameter("Where2", Where2));
                    _parameters.Add(new SqlParameter("OrderBy", OrderBy));
                    _parameters.Add(new SqlParameter("Sort", Sort));

                    var outputTotal = new SqlParameter
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "Total",
                        DbType = DbType.Int32,
                        Size = 4
                    };

                    _parameters.Add(outputTotal);

                }

                return _parameters.ToArray();
            }
            set { _parameters = value; }
        }
    }


    public class QueryPagingMeberParameters
    {
        public QueryPagingMeberParameters()
        { }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string OrderBy { get; set; }

        public string Sort { get; set; }

        public string Where { get; set; }

        public string ProjectDate { get; set; }

        public string ProcedureName { get; set; }


        public int Total
        {
            get
            {
                if (_parameters.Count > 0)
                {
                    return (int)_parameters[6].Value;
                }
                return 0;
            }
        }

        private IList<SqlParameter> _parameters = new List<SqlParameter>();

        public SqlParameter[] ProcedureParameters
        {
            get
            {
                if (_parameters.Count == 0)
                {
                    _parameters.Add(new SqlParameter("PageIndex", PageIndex));
                    _parameters.Add(new SqlParameter("PageSize", PageSize));
                    _parameters.Add(new SqlParameter("Where", Where));
                    _parameters.Add(new SqlParameter("ProjectDate", ProjectDate));
                    _parameters.Add(new SqlParameter("OrderBy", OrderBy));
                    _parameters.Add(new SqlParameter("Sort", Sort));

                    var outputTotal = new SqlParameter
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "Total",
                        DbType = DbType.Int32,
                        Size = 4
                    };

                    _parameters.Add(outputTotal);

                }

                return _parameters.ToArray();
            }
            set { _parameters = value; }
        }
    }
}
