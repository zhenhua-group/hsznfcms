﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.Model;

namespace TG.BLL
{
    /// <summary>
    /// cm_ProjectValueAllot
    /// </summary>
    public partial class cm_ProjectValueAllot
    {
        private readonly TG.DAL.cm_ProjectValueAllot dal = new TG.DAL.cm_ProjectValueAllot();
        public cm_ProjectValueAllot()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectValueAllot model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectValueAllot model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="allotID"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public int UpdateAllotDataInfo(int allotID, int ProId, string setStatus)
        {
            return dal.UpdateAllotDataInfo(allotID, ProId, setStatus);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectValueAllot GetModel(int? ID)
        {
            return dal.GetModel(ID);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectValueAllot GetModelByProID(int? proID)
        {
            return dal.GetModelByProID(proID);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectValueAllot> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectValueAllot> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_ProjectValueAllot> modelList = new List<TG.Model.cm_ProjectValueAllot>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_ProjectValueAllot model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_ProjectValueAllot();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["pro_ID"] != null && dt.Rows[n]["pro_ID"].ToString() != "")
                    {
                        model.pro_ID = int.Parse(dt.Rows[n]["pro_ID"].ToString());
                    }
                    if (dt.Rows[n]["AllotTimes"] != null && dt.Rows[n]["AllotTimes"].ToString() != "")
                    {
                        model.AllotTimes = dt.Rows[n]["AllotTimes"].ToString();
                    }
                    if (dt.Rows[n]["AllotCount"] != null && dt.Rows[n]["AllotCount"].ToString() != "")
                    {
                        model.AllotCount = decimal.Parse(dt.Rows[n]["AllotCount"].ToString());
                    }
                    if (dt.Rows[n]["AllotDate"] != null && dt.Rows[n]["AllotDate"].ToString() != "")
                    {
                        model.AllotDate = DateTime.Parse(dt.Rows[n]["AllotDate"].ToString());
                    }
                    if (dt.Rows[n]["PaidValuePercent"] != null && dt.Rows[n]["PaidValuePercent"].ToString() != "")
                    {
                        model.PaidValuePercent = decimal.Parse(dt.Rows[n]["PaidValuePercent"].ToString());
                    }
                    if (dt.Rows[n]["DesignManagerPercent"] != null && dt.Rows[n]["DesignManagerPercent"].ToString() != "")
                    {
                        model.DesignManagerPercent = decimal.Parse(dt.Rows[n]["DesignManagerPercent"].ToString());
                    }
                    if (dt.Rows[n]["AllotValuePercent"] != null && dt.Rows[n]["AllotValuePercent"].ToString() != "")
                    {
                        model.AllotValuePercent = dt.Rows[n]["AllotValuePercent"].ToString();
                    }
                    if (dt.Rows[n]["EconomyValuePercent"] != null && dt.Rows[n]["EconomyValuePercent"].ToString() != "")
                    {
                        model.EconomyValuePercent = decimal.Parse(dt.Rows[n]["EconomyValuePercent"].ToString());
                    }
                    if (dt.Rows[n]["UnitValueCount"] != null && dt.Rows[n]["UnitValueCount"].ToString() != "")
                    {
                        model.UnitValuePercent = decimal.Parse(dt.Rows[n]["UnitValueCount"].ToString());
                    }
                    if (dt.Rows[n]["AllotUser"] != null && dt.Rows[n]["AllotUser"].ToString() != "")
                    {
                        model.AllotUser = dt.Rows[n]["AllotUser"].ToString();
                    }
                    if (dt.Rows[n]["mark"] != null && dt.Rows[n]["mark"].ToString() != "")
                    {
                        model.mark = dt.Rows[n]["mark"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }

        /// <summary>
        /// 得到实际收款金额
        /// </summary>
        /// <param name="cprid">合同编号</param>
        /// <returns></returns>
        public DataSet GetPayShiAccount(int cprid)
        {
            return dal.GetPayShiAccount(cprid);
        }


        /// <summary>
        /// 得到数据列表
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public DataSet GetProjectValueRecordView(string where, string tempYear)
        {
            var ds = TransformSource.ConvertToDataSet<ProjectValueAuditViewEntity>(dal.GetProjectValueRecordView(where, tempYear));
            return ds;
        }

        public DataSet GetProjectValueRecordViewByPro(string where, string tempYear)
        {
            var ds = TransformSource.ConvertToDataSet<ProjectValueAuditViewEntityByPro>(dal.GetProjectValueRecordViewByPro(where, tempYear));
            return ds;
        }
        /// <summary>
        /// 得到数据列表--暖通所项目
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public DataSet GetHavcProjectValueRecordView(string where, string tempYear)
        {
            var ds = TransformSource.ConvertToDataSet<HavcProjectValueAuditViewEntity>(dal.GetHavcProjectValueRecordView(where, tempYear));
            return ds;
        }
        public DataSet GetHavcProjectValueRecordViewByPro(string where, string tempYear)
        {
            var ds = TransformSource.ConvertToDataSet<HavcProjectValueAuditViewEntityByPro>(dal.GetHavcProjectValueRecordViewByPro(where, tempYear));
            return ds;
        }
        /// <summary>
        /// 得到数据列表--经济所所项目
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public DataSet GetJjsProjectValueRecordView(string where, string tempYear)
        {
            var ds = TransformSource.ConvertToDataSet<JjsProjectValueAuditViewEntity>(dal.GetJjsProjectValueRecordView(where, tempYear));
            return ds;
        }
        public DataSet GetJjsProjectValueRecordViewByPro(string where, string tempYear)
        {
            var ds = TransformSource.ConvertToDataSet<JjsProjectValueAuditViewEntityByPro>(dal.GetJjsProjectValueRecordViewByPro(where, tempYear));
            return ds;
        }
        /// <summary>
        /// 查询经济所产值明细
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public DataSet GetjjsProjectValueRecordDetail(int proID)
        {
            return dal.GetjjsProjectValueRecordDetail(proID);
        }
        /// <summary>
        /// 二次产值分配列表详细信息
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public DataSet GetProjectSecondValueRecordView(string whereSql)
        {
            var ds = TransformSource.ConvertToDataSet<ProjectSecondValueAuditViewEntity>(dal.GetProjectSecondValueRecordView(whereSql));
            return ds;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_Coperation GetCprModel(int cpr_Id)
        {

            return dal.GetCprModel(cpr_Id);
        }

        /// <summary>
        /// 得到当前年份实收的金额
        /// </summary>
        /// <param name="year"></param>
        /// <param name="cprId"></param>
        /// <returns></returns>
        public decimal GetPayShiValueCurrent(string year, int cprId)
        {
            return dal.GetPayShiValueCurrent(year, cprId);
        }
          /// <summary>
        /// 得到分配金额
        /// </summary>
        /// <param name="year"></param>
        /// <param name="pro_id"></param>
        /// <returns></returns>
        public decimal GetAllotCount(string year, int pro_id)
        {
            return dal.GetAllotCount(year, pro_id);
        }
        /// <summary>
        /// 得到二次产值分配金额
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitName"></param>
        /// <returns></returns>
        public DataSet GetSecondValueCurrent(string year, string unitName)
        {
            return dal.GetSecondValueCurrent(year, unitName);
        }
         /// <summary>
        /// 删除项目产值
        /// </summary>
        /// <param name="proid"></param>
        /// <param name="allotId"></param>
        /// <param name="auditID"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public int DeleteProjectValue(string proid, string allotId, string auditID, string type)
        {
            return dal.DeleteProjectValue(proid, allotId, auditID, type);
        }
         /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public int UpdateProjectValueTStatus(int allotID)
        {
            return dal.UpdateProjectValueTStatus(allotID);
        }
         /// <summary>
        /// 借出。借入项目表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataSet GetProjectValueLendBorrowInfo(string strWhere, string year)
        {
            return dal.GetProjectValueLendBorrowInfo(strWhere, year);
        }
         /// <summary>
        /// 项目所留产值统计表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataSet GetProjectValueRetentionStatisticalInfo(string strWhere, string year)
        {
            return dal.GetProjectValueRetentionStatisticalInfo(strWhere, year);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataSet GetProjectAuditAndDesignerAllot(string strWhere, string year,string allottype)
        {
            return dal.GetProjectAuditAndDesignerAllot(strWhere, year,allottype);
        }
          /// <summary>
        /// 借出。借入项目表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataSet GetUnitValueLendBorrowInfo(string strWhere, string year)
        {
            return dal.GetUnitValueLendBorrowInfo(strWhere, year);
        }
        #endregion  Method
    }
}
