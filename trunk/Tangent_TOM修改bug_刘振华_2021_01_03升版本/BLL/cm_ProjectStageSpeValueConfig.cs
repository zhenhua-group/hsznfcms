﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.Model;

namespace TG.BLL
{
    /// <summary>
    /// cm_ProjectStageSpeValueConfig
    /// </summary>
    public partial class cm_ProjectStageSpeValueConfig
    {
        private readonly TG.DAL.cm_ProjectStageSpeValueConfig dal = new TG.DAL.cm_ProjectStageSpeValueConfig();
        public cm_ProjectStageSpeValueConfig()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectStageSpeValueConfig model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectStageSpeValueConfig model)
        {
            return dal.Update(model);
        }

         /// <summary>
        /// 批量更新数据
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateValueDetatil(TG.Model.cm_ProjectStageSpeValueConfigEntity dataEntity)
        {
            return dal.UpdateValueDetatil(dataEntity);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectStageSpeValueConfig GetModel(int ID)
        {

            return dal.GetModel(ID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_ProjectStageSpeValueConfig GetModelByCache(int ID)
        {

            string CacheKey = "cm_ProjectStageSpeValueConfigModel-" + ID;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(ID);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_ProjectStageSpeValueConfig)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectStageSpeValueConfig> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectStageSpeValueConfig> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_ProjectStageSpeValueConfig> modelList = new List<TG.Model.cm_ProjectStageSpeValueConfig>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_ProjectStageSpeValueConfig model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_ProjectStageSpeValueConfig();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["ProjectStage"] != null && dt.Rows[n]["ProjectStage"].ToString() != "")
                    {
                        model.ProjectStage = dt.Rows[n]["ProjectStage"].ToString();
                    }
                    if (dt.Rows[n]["bulidingPercent"] != null && dt.Rows[n]["bulidingPercent"].ToString() != "")
                    {
                        model.bulidingPercent = decimal.Parse(dt.Rows[n]["bulidingPercent"].ToString());
                    }
                    if (dt.Rows[n]["structurePercent"] != null && dt.Rows[n]["structurePercent"].ToString() != "")
                    {
                        model.structurePercent = decimal.Parse(dt.Rows[n]["structurePercent"].ToString());
                    }
                    if (dt.Rows[n]["drainPercent"] != null && dt.Rows[n]["drainPercent"].ToString() != "")
                    {
                        model.drainPercent = decimal.Parse(dt.Rows[n]["drainPercent"].ToString());
                    }
                    if (dt.Rows[n]["hvacPercent"] != null && dt.Rows[n]["hvacPercent"].ToString() != "")
                    {
                        model.hvacPercent = decimal.Parse(dt.Rows[n]["hvacPercent"].ToString());
                    }
                    if (dt.Rows[n]["electricPercent"] != null && dt.Rows[n]["electricPercent"].ToString() != "")
                    {
                        model.electricPercent = decimal.Parse(dt.Rows[n]["electricPercent"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}
