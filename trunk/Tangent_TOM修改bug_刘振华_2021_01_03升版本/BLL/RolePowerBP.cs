﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;

namespace TG.BLL
{
    public class RolePowerBP
    {
        private RolePowerDA da = new RolePowerDA();

        public string SaveRolePowerViewEntity(List<RolePowerViewEntity> rolePowerViewEntityList)
        {
            bool flag = true;
            for (int i = 0; i < rolePowerViewEntityList.Count; i++)
            {
                bool isExists = da.IsExists(rolePowerViewEntityList[i]);
                try
                {
                    if (isExists)
                    {
                        //编辑的场合
                        int count = da.UpDateRolePowerViewEntity(rolePowerViewEntityList[i]);
                        if (count <= 0)
                        {
                            flag = false;
                        }
                    }
                    else
                    {
                        //新规的场合
                        int count = da.InsertRolePowerViewEntity(rolePowerViewEntityList[i]);
                        if (count <= 0)
                        {
                            flag = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    flag = false;
                    continue;
                }
            }
            return flag == true ? "1" : "0";
        }

        public List<RolePowerListViewEntity> GetRolePowerListViewEntity(int roleSysNo)
        {
            return da.GetRolePowerListViewEntity(roleSysNo);
        }

        public List<RolePowerParameterEntity> GetRolePowerViewEntityList(int userSysNo, string pageName)
        {
            return da.GetRolePowerViewEntityList(userSysNo, pageName);
        }
    }
}
