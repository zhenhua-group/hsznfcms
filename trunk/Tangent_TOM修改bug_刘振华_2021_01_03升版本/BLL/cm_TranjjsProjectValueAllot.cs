﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Model;
namespace TG.BLL
{
    /// <summary>
    /// cm_TranjjsProjectValueAllot
    /// </summary>
    public partial class cm_TranjjsProjectValueAllot
    {
        private readonly TG.DAL.cm_TranjjsProjectValueAllot dal = new TG.DAL.cm_TranjjsProjectValueAllot();
        public cm_TranjjsProjectValueAllot()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_TranjjsProjectValueAllot model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_TranjjsProjectValueAllot model)
        {
            return dal.Update(model);
        }
          /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_TranjjsProjectValueAllot GetModelByProID(int proid, int allotID)
        {
            return dal.GetModelByProID(proid,allotID);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_TranjjsProjectValueAllot GetModel(int ID)
        {

            return dal.GetModel(ID);
        }



        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_TranjjsProjectValueAllot> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_TranjjsProjectValueAllot> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_TranjjsProjectValueAllot> modelList = new List<TG.Model.cm_TranjjsProjectValueAllot>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_TranjjsProjectValueAllot model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_TranjjsProjectValueAllot();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["Pro_ID"] != null && dt.Rows[n]["Pro_ID"].ToString() != "")
                    {
                        model.Pro_ID = int.Parse(dt.Rows[n]["Pro_ID"].ToString());
                    }
                    if (dt.Rows[n]["AllotID"] != null && dt.Rows[n]["AllotID"].ToString() != "")
                    {
                        model.AllotID = int.Parse(dt.Rows[n]["AllotID"].ToString());
                    }
                    if (dt.Rows[n]["TotalCount"] != null && dt.Rows[n]["TotalCount"].ToString() != "")
                    {
                        model.TotalCount = decimal.Parse(dt.Rows[n]["TotalCount"].ToString());
                    }
                    if (dt.Rows[n]["AllotCount"] != null && dt.Rows[n]["AllotCount"].ToString() != "")
                    {
                        model.AllotCount = decimal.Parse(dt.Rows[n]["AllotCount"].ToString());
                    }
                    if (dt.Rows[n]["ItemType"] != null && dt.Rows[n]["ItemType"].ToString() != "")
                    {
                        model.ItemType = int.Parse(dt.Rows[n]["ItemType"].ToString());
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = dt.Rows[n]["Status"].ToString();
                    }
                    if (dt.Rows[n]["Thedeptallotpercent"] != null && dt.Rows[n]["Thedeptallotpercent"].ToString() != "")
                    {
                        model.Thedeptallotpercent = decimal.Parse(dt.Rows[n]["Thedeptallotpercent"].ToString());
                    }
                    if (dt.Rows[n]["Thedeptallotcount"] != null && dt.Rows[n]["Thedeptallotcount"].ToString() != "")
                    {
                        model.Thedeptallotcount = decimal.Parse(dt.Rows[n]["Thedeptallotcount"].ToString());
                    }
                    if (dt.Rows[n]["ProgramPercent"] != null && dt.Rows[n]["ProgramPercent"].ToString() != "")
                    {
                        model.ProgramPercent = decimal.Parse(dt.Rows[n]["ProgramPercent"].ToString());
                    }
                    if (dt.Rows[n]["ProgramCount"] != null && dt.Rows[n]["ProgramCount"].ToString() != "")
                    {
                        model.ProgramCount = decimal.Parse(dt.Rows[n]["ProgramCount"].ToString());
                    }
                    if (dt.Rows[n]["DesignManagerPercent"] != null && dt.Rows[n]["DesignManagerPercent"].ToString() != "")
                    {
                        model.DesignManagerPercent = decimal.Parse(dt.Rows[n]["DesignManagerPercent"].ToString());
                    }
                    if (dt.Rows[n]["DesignManagerCount"] != null && dt.Rows[n]["DesignManagerCount"].ToString() != "")
                    {
                        model.DesignManagerCount = decimal.Parse(dt.Rows[n]["DesignManagerCount"].ToString());
                    }
                    if (dt.Rows[n]["ShouldBeValuePercent"] != null && dt.Rows[n]["ShouldBeValuePercent"].ToString() != "")
                    {
                        model.ShouldBeValuePercent = decimal.Parse(dt.Rows[n]["ShouldBeValuePercent"].ToString());
                    }
                    if (dt.Rows[n]["ShouldBeValueCount"] != null && dt.Rows[n]["ShouldBeValueCount"].ToString() != "")
                    {
                        model.ShouldBeValueCount = decimal.Parse(dt.Rows[n]["ShouldBeValueCount"].ToString());
                    }
                    if (dt.Rows[n]["AuditUser"] != null && dt.Rows[n]["AuditUser"].ToString() != "")
                    {
                        model.AllotUser = int.Parse(dt.Rows[n]["AuditUser"].ToString());
                    }
                    if (dt.Rows[n]["AuditDate"] != null && dt.Rows[n]["AuditDate"].ToString() != "")
                    {
                        model.AllotDate = DateTime.Parse(dt.Rows[n]["AuditDate"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }


        #endregion  Method
    }
}

