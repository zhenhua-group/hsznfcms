﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// tg_subproject
	/// </summary>
	public partial class tg_subproject
	{
		private readonly TG.DAL.tg_subproject dal=new TG.DAL.tg_subproject();
		public tg_subproject()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int pro_ID)
		{
			return dal.Exists(pro_ID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.tg_subproject model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_subproject model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int pro_ID)
		{
			
			return dal.Delete(pro_ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string pro_IDlist )
		{
			return dal.DeleteList(pro_IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_subproject GetModel(int pro_ID)
		{
			
			return dal.GetModel(pro_ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.tg_subproject GetModelByCache(int pro_ID)
		{
			
			string CacheKey = "tg_subprojectModel-" + pro_ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(pro_ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.tg_subproject)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_subproject> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_subproject> DataTableToList(DataTable dt)
		{
			List<TG.Model.tg_subproject> modelList = new List<TG.Model.tg_subproject>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.tg_subproject model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.tg_subproject();
					if(dt.Rows[n]["pro_ID"]!=null && dt.Rows[n]["pro_ID"].ToString()!="")
					{
						model.pro_ID=int.Parse(dt.Rows[n]["pro_ID"].ToString());
					}
					if(dt.Rows[n]["pro_Name"]!=null && dt.Rows[n]["pro_Name"].ToString()!="")
					{
					model.pro_Name=dt.Rows[n]["pro_Name"].ToString();
					}
					if(dt.Rows[n]["pro_parentID"]!=null && dt.Rows[n]["pro_parentID"].ToString()!="")
					{
						model.pro_parentID=int.Parse(dt.Rows[n]["pro_parentID"].ToString());
					}
					if(dt.Rows[n]["pro_descr"]!=null && dt.Rows[n]["pro_descr"].ToString()!="")
					{
					model.pro_descr=dt.Rows[n]["pro_descr"].ToString();
					}
					if(dt.Rows[n]["pro_IsTerminal"]!=null && dt.Rows[n]["pro_IsTerminal"].ToString()!="")
					{
						model.pro_IsTerminal=int.Parse(dt.Rows[n]["pro_IsTerminal"].ToString());
					}
					if(dt.Rows[n]["pro_parentSubID"]!=null && dt.Rows[n]["pro_parentSubID"].ToString()!="")
					{
						model.pro_parentSubID=int.Parse(dt.Rows[n]["pro_parentSubID"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

