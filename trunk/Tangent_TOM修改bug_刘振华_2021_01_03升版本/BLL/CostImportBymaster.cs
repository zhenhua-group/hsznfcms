﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public partial class CostImportBymaster
    {
        //全局变量
        private List<TG.Model.tg_unit> modellist;

        //部门
        TG.BLL.tg_unit unitBll = new TG.BLL.tg_unit();
        //支出
        TG.DAL.CostImportBymaster dal = new TG.DAL.CostImportBymaster();
        public int InsertFinancial(DataTable dt, string year, string type, string userId)
        {
            modellist = unitBll.GetModelList("1=1");
            //给表格赋标题

            dt.Columns[2].ColumnName = "yese";

            //先去重
            //查询凭证号
            DataTable dtVoucherNo = dal.GetVoucherNo().Tables[0];

            //过滤重复的
            if (dtVoucherNo.Rows.Count > 0)
            {
                //删除上传重复的值
                List<DataRow> removelist = new List<DataRow>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bool rowdataisnull = true;
                    for (int j = 0; j < dtVoucherNo.Rows.Count; j++)
                    {

                        if (dt.Rows[i]["yese"].ToString().Trim().Equals(dtVoucherNo.Rows[j][0].ToString().Trim()))
                        {
                            rowdataisnull = false;
                            break;
                        }

                    }
                    if (!rowdataisnull)
                    {
                        removelist.Add(dt.Rows[i]);
                    }

                }
                for (int i = 0; i < removelist.Count; i++)
                {
                    dt.Rows.Remove(removelist[i]);
                }
            }
            //新建datatable使和数据库储存类型一样
            DataTable dtnew = new DataTable();

            dtnew.Columns.Add("costTime");
            dtnew.Columns.Add("costNum");
            dtnew.Columns.Add("costSub");
            dtnew.Columns.Add("costCharge");
            dtnew.Columns.Add("costOutCharge");
            dtnew.Columns.Add("costUnit");
            dtnew.Columns.Add("costUserId");
            dtnew.Columns.Add("costTypeID");


            dt.Columns[0].ColumnName = "costTime";
            dt.Columns[1].ColumnName = "costNum";
            dt.Columns[2].ColumnName = "costSub";
            dt.Columns[3].ColumnName = "costCharge";
            dt.Columns[4].ColumnName = "costOutCharge";
            dt.Columns[5].ColumnName = "costUnit";
            dt.Columns[6].ColumnName = "costUserId";
            dt.Columns[7].ColumnName = "costTypeID";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dtnew.NewRow();
                //DataRow dr = dt.Rows[i];
                dtnew.Rows.Add(row);
                dtnew.Rows[i]["costTime"] = year + "-" + dt.Rows[i]["costTime"].ToString() + "-" + dt.Rows[i]["costNum"].ToString();
                dtnew.Rows[i]["costNum"] = dt.Rows[i]["costSub"].ToString();
                dtnew.Rows[i]["costSub"] = dt.Rows[i]["costCharge"].ToString();
                dtnew.Rows[i]["costCharge"] = dt.Rows[i]["costOutCharge"].ToString();
                dtnew.Rows[i]["costOutCharge"] = dt.Rows[i]["costTypeID"].ToString();
                dtnew.Rows[i]["costUnit"] = GetUnitId(dt.Rows[i]["costCharge"].ToString());
                dtnew.Rows[i]["costUserId"] = userId;
                dtnew.Rows[i]["costTypeID"] = type;
                //dtnew.Rows.Add(dr);
            }

            return dal.InsertFinancial(dtnew);
        }
        //dt.Columns[0].ColumnName = "Month";
        //   dt.Columns[1].ColumnName = "FieldOfB";
        //   dt.Columns[2].ColumnName = "yese";
        //   dt.Columns[3].ColumnName = "fdsf";
        //   dt.Columns[4].ColumnName = "sdfsd";
        //   dt.Columns[5].ColumnName = "Account";
        //   dt.Columns[6].ColumnName = "FieldOfH";
        //   dt.Columns[7].ColumnName = "dsfsadfsd";

        private string GetUnitId(string costSubString)
        {
            //   
            string returnUnitId = "1";
            foreach (var itemUnit in modellist)
            {
                if (costSubString.Contains(itemUnit.unit_Name.Trim()))
                {
                    returnUnitId = itemUnit.unit_ID.ToString();
                }
                else if (costSubString.Contains("审图办"))
                {
                    returnUnitId = "252";
                }
                else if (costSubString.Contains("总承包部"))
                {
                    returnUnitId = "257";
                }
                else if (costSubString.Contains("暖通所"))
                {
                    returnUnitId = "247";
                }

            }
            return returnUnitId;
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return dal.GetListPageProcCount(query);
        }
        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListByPageProc(query, startIndex, endIndex);
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetCostListPageProcCount(string query)
        {
            return dal.GetCostListPageProcCount(query);
        }
        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetCostListByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetCostListByPageProc(query, startIndex, endIndex);
        }
        public object GetCostAllByProc(string query, string param1, string param2)
        {
            return dal.GetCostAllByProc(query, param1, param2);
        }
        public object GetAllCostByProc(string query, string param1, string param2, string costgroup)
        {
            return dal.GetAllCostByProc(query, param1, param2, costgroup);
        }
    }
}
