﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
using System.Data.SqlClient;
namespace TG.BLL
{
    /// <summary>
    /// cm_ContactPersionInfo
    /// </summary>
    public partial class cm_ContactPersionInfo
    {
        private readonly TG.DAL.cm_ContactPersionInfo dal = new TG.DAL.cm_ContactPersionInfo();
        public cm_ContactPersionInfo()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int CP_Id)
        {
            return dal.Exists(CP_Id);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ContactPersionInfo model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_ContactPersionInfo model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int CP_Id)
        {

            return dal.Delete(CP_Id);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string CP_Idlist)
        {
            return dal.DeleteList(CP_Idlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ContactPersionInfo GetModel(int CP_Id)
        {

            return dal.GetModel(CP_Id);
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ContactPersionInfo GetModel(string CPWhere)
        {

            return dal.GetModel(CPWhere);
        }
        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_ContactPersionInfo GetModelByCache(int CP_Id)
        {

            string CacheKey = "cm_ContactPersionInfoModel-" + CP_Id;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(CP_Id);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_ContactPersionInfo)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ContactPersionInfo> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ContactPersionInfo> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_ContactPersionInfo> modelList = new List<TG.Model.cm_ContactPersionInfo>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_ContactPersionInfo model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_ContactPersionInfo();
                    if (dt.Rows[n]["CP_Id"] != null && dt.Rows[n]["CP_Id"].ToString() != "")
                    {
                        model.CP_Id = int.Parse(dt.Rows[n]["CP_Id"].ToString());
                    }
                    if (dt.Rows[n]["Cst_Id"] != null && dt.Rows[n]["Cst_Id"].ToString() != "")
                    {
                        model.Cst_Id = Convert.ToDecimal(dt.Rows[n]["Cst_Id"].ToString());
                    }
                    if (dt.Rows[n]["ContactNo"] != null && dt.Rows[n]["ContactNo"].ToString() != "")
                    {
                        model.ContactNo = dt.Rows[n]["ContactNo"].ToString();
                    }
                    if (dt.Rows[n]["Name"] != null && dt.Rows[n]["Name"].ToString() != "")
                    {
                        model.Name = dt.Rows[n]["Name"].ToString();
                    }
                    if (dt.Rows[n]["Duties"] != null && dt.Rows[n]["Duties"].ToString() != "")
                    {
                        model.Duties = dt.Rows[n]["Duties"].ToString();
                    }
                    if (dt.Rows[n]["Department"] != null && dt.Rows[n]["Department"].ToString() != "")
                    {
                        model.Department = dt.Rows[n]["Department"].ToString();
                    }
                    if (dt.Rows[n]["Phone"] != null && dt.Rows[n]["Phone"].ToString() != "")
                    {
                        model.Phone = dt.Rows[n]["Phone"].ToString();
                    }
                    if (dt.Rows[n]["BPhone"] != null && dt.Rows[n]["BPhone"].ToString() != "")
                    {
                        model.BPhone = dt.Rows[n]["BPhone"].ToString();
                    }
                    if (dt.Rows[n]["FPhone"] != null && dt.Rows[n]["FPhone"].ToString() != "")
                    {
                        model.FPhone = dt.Rows[n]["FPhone"].ToString();
                    }
                    if (dt.Rows[n]["FFax"] != null && dt.Rows[n]["FFax"].ToString() != "")
                    {
                        model.FFax = dt.Rows[n]["FFax"].ToString();
                    }
                    if (dt.Rows[n]["BFax"] != null && dt.Rows[n]["BFax"].ToString() != "")
                    {
                        model.BFax = dt.Rows[n]["BFax"].ToString();
                    }
                    if (dt.Rows[n]["Remark"] != null && dt.Rows[n]["Remark"].ToString() != "")
                    {
                        model.Remark = dt.Rows[n]["Remark"].ToString();
                    }
                    if (dt.Rows[n]["CallingCard"] != null && dt.Rows[n]["CallingCard"].ToString() != "")
                    {
                        model.CallingCard = (byte[])dt.Rows[n]["CallingCard"];
                    }
                    if (dt.Rows[n]["Email"] != null && dt.Rows[n]["Email"].ToString() != "")
                    {
                        model.Email = dt.Rows[n]["Email"].ToString();
                    }
                    if (dt.Rows[n]["UpdateBy"] != null && dt.Rows[n]["UpdateBy"].ToString() != "")
                    {
                        model.UpdateBy = int.Parse(dt.Rows[n]["UpdateBy"].ToString());
                    }
                    if (dt.Rows[n]["LastUpdate"] != null && dt.Rows[n]["LastUpdate"].ToString() != "")
                    {
                        model.LastUpdate = DateTime.Parse(dt.Rows[n]["LastUpdate"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分阶段获得数据
        /// </summary>
        /// <param name="strWhere">where语句</param>
        /// <param name="pagesize">每页数量</param>
        /// <param name="pageIndex">要得到第几页的数据</param>
        /// <returns>数据集</returns>
        public DataSet GetListByPage(string strWhere, int pagesize, int pageIndex)
        {
            return dal.GetListByPage(strWhere, pagesize, pageIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListByPageProc(query, startIndex, endIndex);
        }
        /// <summary>
        /// 返回需要分页的记录数
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageProcCount(string query)
        {
            return dal.GetListPageProcCount(query);
        }

        #endregion  Method
    }
}

