﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;

namespace TG.BLL
{
    public class cm_ProjectOutDoorValueConfig
    {
        private readonly TG.DAL.cm_ProjectOutDoorValueConfig dal = new TG.DAL.cm_ProjectOutDoorValueConfig();
        public cm_ProjectOutDoorValueConfig()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectOutDoorValueConfig model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectOutDoorValueConfig model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 批量更新数据
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateValueDetatil(cm_ProjectDesignProcessValueConfigEntity dataEntity)
        {
            return dal.UpdateValueDetatil(dataEntity);
        }

        #endregion  Method
    }
}
