﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// tg_ProRole
	/// </summary>
	public partial class tg_ProRole
	{
		private readonly TG.DAL.tg_ProRole dal=new TG.DAL.tg_ProRole();
		public tg_ProRole()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			return dal.Exists(ID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.tg_ProRole model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_ProRole model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			return dal.Delete(ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			return dal.DeleteList(IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_ProRole GetModel(int ID)
		{
			
			return dal.GetModel(ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.tg_ProRole GetModelByCache(int ID)
		{
			
			string CacheKey = "tg_ProRoleModel-" + ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.tg_ProRole)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_ProRole> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_ProRole> DataTableToList(DataTable dt)
		{
			List<TG.Model.tg_ProRole> modelList = new List<TG.Model.tg_ProRole>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.tg_ProRole model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.tg_ProRole();
					if(dt.Rows[n]["ID"]!=null && dt.Rows[n]["ID"].ToString()!="")
					{
						model.ID=int.Parse(dt.Rows[n]["ID"].ToString());
					}
					if(dt.Rows[n]["RoleName"]!=null && dt.Rows[n]["RoleName"].ToString()!="")
					{
					model.RoleName=dt.Rows[n]["RoleName"].ToString();
					}
					if(dt.Rows[n]["SubProLevel"]!=null && dt.Rows[n]["SubProLevel"].ToString()!="")
					{
					model.SubProLevel=dt.Rows[n]["SubProLevel"].ToString();
					}
					if(dt.Rows[n]["StageLevel"]!=null && dt.Rows[n]["StageLevel"].ToString()!="")
					{
					model.StageLevel=dt.Rows[n]["StageLevel"].ToString();
					}
					if(dt.Rows[n]["PurposeLevel"]!=null && dt.Rows[n]["PurposeLevel"].ToString()!="")
					{
					model.PurposeLevel=dt.Rows[n]["PurposeLevel"].ToString();
					}
					if(dt.Rows[n]["SpecLevel"]!=null && dt.Rows[n]["SpecLevel"].ToString()!="")
					{
					model.SpecLevel=dt.Rows[n]["SpecLevel"].ToString();
					}
					if(dt.Rows[n]["Designer"]!=null && dt.Rows[n]["Designer"].ToString()!="")
					{
					model.Designer=dt.Rows[n]["Designer"].ToString();
					}
					if(dt.Rows[n]["Level3"]!=null && dt.Rows[n]["Level3"].ToString()!="")
					{
					model.Level3=dt.Rows[n]["Level3"].ToString();
					}
					if(dt.Rows[n]["Level4"]!=null && dt.Rows[n]["Level4"].ToString()!="")
					{
					model.Level4=dt.Rows[n]["Level4"].ToString();
					}
					if(dt.Rows[n]["Level5"]!=null && dt.Rows[n]["Level5"].ToString()!="")
					{
					model.Level5=dt.Rows[n]["Level5"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

