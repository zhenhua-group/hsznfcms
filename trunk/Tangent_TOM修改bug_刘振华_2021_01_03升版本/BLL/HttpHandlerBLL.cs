﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace TG.BLL
{
    public class HttpHandlerBLL
    {
        public HttpHandlerBLL()
        { }
        /// <summary>
        /// 返回执行的数据集
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public static DataSet ExecuteDataset(string procName, SqlParameter[] sqlParams)
        {
            return TG.DAL.HttpHandlerDAL.ExecuteDataset(procName, sqlParams);
        }
    }
}
