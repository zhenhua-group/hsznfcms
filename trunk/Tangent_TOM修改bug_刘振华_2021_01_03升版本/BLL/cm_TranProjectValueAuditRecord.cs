﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;

namespace TG.BLL
{
    public class cm_TranProjectValueAuditRecord
    {
        private readonly TG.DAL.cm_TranProjectValueAuditRecord dal = new TG.DAL.cm_TranProjectValueAuditRecord();
        public cm_TranProjectValueAuditRecord()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_TranProjectValueAuditRecord model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_TranProjectValueAuditRecord model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SysNo)
        {

            return dal.Delete(SysNo);
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_TranProjectValueAuditRecord GetModel(int SysNo)
        {
            return dal.GetModel(SysNo);
        }

        /// <summary>
        /// 更新经济所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateTranJjsProjectAllotValue(TG.Model.cm_TranProjectValueAuditRecord dataEntity)
        {
            return dal.UpdateTranJjsProjectAllotValue(dataEntity);
        }
           /// <summary>
        /// 更新暖通所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateTranHavcProjectAllotValue(TG.Model.cm_TranProjectValueAuditRecord dataEntity)
        {
            return dal.UpdateTranHavcProjectAllotValue(dataEntity);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int DeleteTranJjsProjectAllotValue(int proid, int allotID, int sysID)
        {
            return dal.DeleteTranJjsProjectAllotValue(proid, allotID, sysID);
        }
          /// <summary>
        /// 删除转暖通所
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int DeleteTranHavcProjectAllotValue(int proid, int allotID, string sysID)
        {
            return dal.DeleteTranHavcProjectAllotValue(proid, allotID, sysID);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int DeleteJjsProjectAllotValue(int proid, int allotID)
        {
            return dal.DeleteJjsProjectAllotValue(proid, allotID);
        }

         /// <summary>
        /// 新增经济所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertTranBulidingProjectAllotValueByMemberDetail(TranBulidingProjectValueAllotByMemberEntity dataEntity)
        {
            return dal.InsertTranBulidingProjectAllotValueByMemberDetail(dataEntity);
        }

          /// <summary>
        ///转土建所
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateTranBulidingProjectAllotValue(TG.Model.cm_TranProjectValueAuditRecord dataEntity)
        {
            return dal.UpdateTranBulidingProjectAllotValue(dataEntity);
        }

         /// <summary>
        /// 删除转土建所
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int DeleteTranBudlingProjectAllotValue(int proid, int allotID, string sysID)
        {
            return dal.DeleteTranBudlingProjectAllotValue(proid, allotID, sysID);
        }
         /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="proid"></param>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public int UpdateTranStatus(int proid, int allotID, string status, string itemType)
        {
            return dal.UpdateTranStatus(proid, allotID, status, itemType);
        }
          /// <summary>
        /// 更新土建所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateTranBulidingProjectAllotValueByMemberDetail(TranBulidingProjectValueAllotByMemberEntity dataEntity)
        {
            return dal.UpdateTranBulidingProjectAllotValueByMemberDetail(dataEntity);
        }
        #endregion  Method
    }
}
