﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.Model;

namespace TG.BLL
{
    /// <summary>
    /// cm_ProjectValueAuditRecord
    /// </summary>
    public partial class cm_ProjectValueAuditRecord
    {
        private readonly TG.DAL.cm_ProjectValueAuditRecord dal = new TG.DAL.cm_ProjectValueAuditRecord();
        public cm_ProjectValueAuditRecord()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectValueAuditRecord model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectValueAuditRecord model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 更新数据-状态
        /// </summary>
        /// <param name="sysno"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public int Update(int sysno, string status)
        {
            return dal.Update(sysno, status);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SysNo)
        {

            return dal.Delete(SysNo);
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectValueAuditRecord GetModel(int SysNo)
        {

            return dal.GetModel(SysNo);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectValueAuditRecord GetModelByProID(int proID, int AllotID)
        {
            return dal.GetModelByProID(proID, AllotID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_ProjectValueAuditRecord GetModelByCache(int SysNo)
        {

            string CacheKey = "cm_ProjectValueAuditRecordModel-" + SysNo;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(SysNo);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_ProjectValueAuditRecord)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectValueAuditRecord> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectValueAuditRecord> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_ProjectValueAuditRecord> modelList = new List<TG.Model.cm_ProjectValueAuditRecord>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_ProjectValueAuditRecord model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_ProjectValueAuditRecord();
                    if (dt.Rows[n]["SysNo"] != null && dt.Rows[n]["SysNo"].ToString() != "")
                    {
                        model.SysNo = int.Parse(dt.Rows[n]["SysNo"].ToString());
                    }
                    if (dt.Rows[n]["ProSysNo"] != null && dt.Rows[n]["ProSysNo"].ToString() != "")
                    {
                        model.ProSysNo = int.Parse(dt.Rows[n]["ProSysNo"].ToString());
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = dt.Rows[n]["Status"].ToString();
                    }
                    if (dt.Rows[n]["OneSuggestion"] != null && dt.Rows[n]["OneSuggestion"].ToString() != "")
                    {
                        model.OneSuggestion = dt.Rows[n]["OneSuggestion"].ToString();
                    }
                    if (dt.Rows[n]["AuditUser"] != null && dt.Rows[n]["AuditUser"].ToString() != "")
                    {
                        model.AuditUser = dt.Rows[n]["AuditUser"].ToString();
                    }
                    if (dt.Rows[n]["AuditDate"] != null && dt.Rows[n]["AuditDate"].ToString() != "")
                    {
                        model.AuditDate = dt.Rows[n]["AuditDate"].ToString();
                    }
                    if (dt.Rows[n]["InUser"] != null && dt.Rows[n]["InUser"].ToString() != "")
                    {
                        model.InUser = int.Parse(dt.Rows[n]["InUser"].ToString());
                    }
                    if (dt.Rows[n]["InDate"] != null && dt.Rows[n]["InDate"].ToString() != "")
                    {
                        model.InDate = DateTime.Parse(dt.Rows[n]["InDate"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }


        /// <summary>
        /// 得到数据列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectValueAllotedView(string strWhere)
        {
            return dal.GetProjectValueAllotedView(strWhere);
        }

        public TG.Model.cm_ProjectFeeAllotConfig GetCoperationAuditConfigByPostion(int position)
        {
            return dal.GetValueAllotAuditConfigByPostion(position);
        }

        /// <summary>
        /// 得到数据列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetDataList(string strWhere)
        {
            return dal.GetDataList(strWhere);
        }

        /// <summary>
        /// 得到数据列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetAllotDataList(string strWhere)
        {
            return dal.GetAllotDataList(strWhere);
        }

        /// <summary>
        /// 得到数据列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetComList(string proId, string year)
        {
            return dal.GetComList(proId, year);
        }

        /// <summary>
        /// 得打数据列表--得到二次分配记录信息
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectSecondValueList(string strWhere)
        {
            return dal.GetProjectSecondValueList(strWhere);
        }

        /// <summary>
        /// 得到数据列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetCprProcessDataList(string itemType, int ProjectStatues)
        {
            return dal.GetCprProcessDataList(itemType, ProjectStatues);
        }



        /// <summary>
        /// 得打数据列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetCprProcessValueDataList(string itemType, int ProjectStatues, decimal allotCount, string specailtyName, string isRuoding)
        {
            return dal.GetCprProcessValueDataList(itemType, ProjectStatues, allotCount, specailtyName, isRuoding);
        }
        /// <summary>
        /// 室外工程系数
        /// </summary>
        /// <param name="specailtyName"></param>
        /// <returns></returns>
        public DataSet GetShiWaiProcessValueDataList(string specailtyName)
        {
            return dal.GetShiWaiProcessValueDataList(specailtyName);
        }

        /// <summary>
        /// 得到策划人员专业
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public DataSet GetPlanUserSpecialty(int proID)
        {
            return dal.GetPlanUserSpecialty(proID);
        }

        /// <summary>
        /// 取得设总及助理人员
        /// </summary>
        /// <param name="pro_id"></param>
        /// <returns></returns>
        public DataSet GetDesinManagerUser(int pro_id)
        {
            return dal.GetDesinManagerUser(pro_id);
        }
        /// <summary>
        /// 取得设总的产值
        /// </summary>
        /// <param name="pro_id"></param>
        /// <param name="allot_id"></param>
        /// <returns></returns>
        public DataSet GetDesinManagerUserValue(int pro_id, int allot_id)
        {
            return dal.GetDesinManagerUserValue(pro_id, allot_id);
        }
        /// <summary>
        /// 得到策划人员专业--暖通项目 包括暖通专业
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public DataSet GetPlanUserSpecialtyByHaveProject(int proID)
        {
            return dal.GetPlanUserSpecialtyByHaveProject(proID);
        }
        /// <summary>
        /// 该专业下 该审核角色是否有人员参与
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public int IsRolePlay(int roseID, string specialtyName, int cprID)
        {
            return dal.IsRolePlay(roseID, specialtyName, cprID);
        }

        /// <summary>
        /// 该专业下 该审核角色是否有设计人员参与
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public int IsDesignerPlay(int cprID, string specialtyName)
        {
            return dal.IsDesignerPlay(cprID, specialtyName);
        }

        /// <summary>
        /// 得到产值分配人员信息
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectValueByMemberDataList(int proid, int allotId, int SpeID)
        {
            return dal.GetProjectValueByMemberDataList(proid, allotId, SpeID);
        }

        /// <summary>
        /// 得到产值分配人员信息--转土建
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetTranBulidingProjectValueByMemberDataList(int proid)
        {
            return dal.GetTranBulidingProjectValueByMemberDataList(proid);
        }
        /// <summary>
        /// 得到产值没有分配人员信息
        /// </summary>
        /// <param name="proid"></param>
        /// <param name="speID"></param>
        /// <returns></returns>
        public DataSet GetProjectValueByMemberNoData(int proid, int speID)
        {
            return dal.GetProjectValueByMemberNoData(proid, speID);
        }

        /// <summary>
        /// 得到人员分配之后的详细信息
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetProjectValueByMemberAcount(int allotID)
        {
            return dal.GetProjectValueByMemberAcount(allotID);
        }

        /// <summary>
        /// 得到人员分配之后的详细信息--转土建所
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetTranBulidingProjectValueByMemberAcount(int allotID)
        {
            return dal.GetTranBulidingProjectValueByMemberAcount(allotID);
        }
        /// <summary>
        /// 得到人员分配之后的详细信息--经济所
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetjjsProjectValueByMemberAcount(int proID, int AllotID)
        {
            return dal.GetjjsProjectValueByMemberAcount(proID, AllotID);
        }
        /// <summary>
        /// 审核人查看审核记录
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetIsAuditedUserAcount(int allotID, int speID)
        {
            return dal.GetIsAuditedUserAcount(allotID, speID);
        }

        /// <summary>
        /// 判断是人员是否已保存
        /// </summary>
        /// <param name="proName"></param>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public bool UserValueIsSaved(int speID, int allotID)
        {
            return dal.UserValueIsSaved(speID, allotID);
        }
        /// <summary>
        /// 得到个人分配明细
        /// </summary>
        /// <param name="allotID"></param>
        /// <param name="useID"></param>
        /// <returns></returns>
        public DataSet GetUserValueByMemberAcount(int allotID, int useID)
        {
            return dal.GetUserValueByMemberAcount(allotID, useID);
        }

          /// <summary>
        /// 得到个人分配明细-专业
        /// </summary>
        /// <param name="allotID"></param>
        /// <param name="useID"></param>
        /// <returns></returns>
        public DataSet GetUserValueBySpeMemberAcount(int allotID, int useID)
        {
            return dal.GetUserValueBySpeMemberAcount(allotID, useID);
        }
        /// <summary>
        /// 人员分配产值
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetProjectValueByMemberTotalAcount(int allotID, string UnitName)
        {
            return dal.GetProjectValueByMemberTotalAcount(allotID, UnitName);
        }

        /// <summary>
        /// 人员分配产值
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetTranProjectValueByMemberTotalAcount(string strWhere, string UnitName)
        {
            return dal.GetTranProjectValueByMemberTotalAcount(strWhere, UnitName);
        }
        /// <summary>
        /// 得到数据列表
        /// </summary>
        /// <param name="allotId"></param>
        /// <returns></returns>
        public DataSet GetValuedDataList(int allotId)
        {
            return dal.GetValuedDataList(allotId);
        }
        /// <summary>
        /// 得打数据列表
        /// </summary>
        public DataSet GetValuedTranDataList(int allotId)
        {
            return dal.GetValuedTranDataList(allotId);
        }

        /// <summary>
        /// 得到工序值
        /// </summary>
        /// <param name="allotId"></param>
        /// <returns></returns>
        public DataSet GetDesignProcessDataList(int allotId)
        {
            return dal.GetDesignProcessDataList(allotId);
        }
        /// <summary>
        /// 新规信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertProjectAllotValueDetatil(TG.Model.ProjectValueAllotDetailEntity dataEntity)
        {
            return dal.InsertProjectAllotValueDetatil(dataEntity);
        }

        /// <summary>
        /// 新增产值分配人员明细信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertProjectAllotValueByMemberDetatil(ProjectValueAllotByMemberEntity dataEntity)
        {
            return dal.InsertProjectAllotValueByMemberDetatil(dataEntity);
        }

        /// <summary>
        /// 新增经济所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertJjsProjectAllotValueByMemberDetail(jjsProjectValueAllotByMemberEntity dataEntity, string isTran)
        {
            return dal.InsertJjsProjectAllotValueByMemberDetail(dataEntity, isTran);
        }

        /// <summary>
        /// 更新经济所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateJjsProjectAllotValue(TG.Model.cm_ProjectValueAuditRecord dataEntity)
        {
            return dal.UpdateJjsProjectAllotValue(dataEntity);
        }

        /// 更新经济所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateTranJjsProjectAllotValue(TG.Model.cm_TranjjsProjectValueAllot dataEntity)
        {
            return dal.UpdateTranJjsProjectAllotValue(dataEntity);
        }

        /// <summary>
        /// 得到专业负责人
        /// </summary>
        /// <param name="cprid"></param>
        /// <param name="userNo"></param>
        /// <returns></returns>
        public string GetSpecialtyHead(int cprid, int userNo)
        {
            return dal.GetSpecialtyHead(cprid, userNo);
        }

        /// <summary>
        /// 查询产值分配记录列表
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<TG.Model.cm_projectValueAllotView> P_cm_ProjectValuelNoListByPager(int startIndex, int endIndex, string strWhere)
        {
            //查询产值分配记录列表
            List<TG.Model.cm_projectValueAllotView> coperationAuditList = dal.P_cm_ProjectValuelNoListByPager(startIndex, endIndex, strWhere);
            //查询产值分配记录列表
            foreach (var coperation in coperationAuditList)
            {
                TG.Model.cm_ProjectValueAuditRecord coperationAuditEntity = dal.GetModelByProSysNo(coperation.pro_Id);

                coperation.CoperationAllotEntity = coperationAuditEntity;

                coperation.AuditRecordSysNo = coperationAuditEntity == null ? 0 : coperationAuditEntity.SysNo;
                coperation.AuditStatus = coperationAuditEntity == null ? "" : coperationAuditEntity.Status;
            }
            return coperationAuditList;
        }

        /// <summary>
        /// 查询产值分配记录列表
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public DataSet P_cm_ProjectValuelNoDataSetByPager(int startIndex, int endIndex, string strWhere)
        {
            return dal.P_cm_ProjectValuelNoDataSetByPager(startIndex, endIndex, strWhere);
        }
        /// <summary>
        /// 查询产值分配信息-经济所
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public List<TG.Model.cm_projectValueAllotView> P_cm_jjsProjectValuelNoDataSetByPager(int startIndex, int endIndex, string strWhere)
        {

            //查询产值分配记录列表
            List<TG.Model.cm_projectValueAllotView> coperationAuditList = dal.P_cm_jjsProjectValuelNoDataSetByPager(startIndex, endIndex, strWhere);
            //查询产值分配记录列表
            foreach (var coperation in coperationAuditList)
            {
                TG.Model.cm_ProjectValueAuditRecord coperationAuditEntity = dal.GetModelByProSysNo(coperation.pro_Id);

                coperation.CoperationAllotEntity = coperationAuditEntity;

                coperation.AuditRecordSysNo = coperationAuditEntity == null ? 0 : coperationAuditEntity.SysNo;
                coperation.AuditStatus = coperationAuditEntity == null ? "" : coperationAuditEntity.Status;
            }
            return coperationAuditList;
        }

        /// <summary>
        /// 查询二次产值分配列表
        /// </summary>
        /// <param name="startIndex">开始</param>
        /// <param name="endIndex">结束</param>
        /// <param name="strWhere">条件</param>
        /// <returns></returns>
        public List<TG.Model.cm_projectSecondValueAllotView> P_cm_ProjectSecondValuelListByPager(int startIndex, int endIndex, string strWhere)
        {
            //查询产值分配记录列表
            List<TG.Model.cm_projectSecondValueAllotView> proValueAuditList = dal.P_cm_ProjectSecondValuelListByPager(startIndex, endIndex, strWhere);
            //查询产值分配记录列表
            foreach (var pro in proValueAuditList)
            {
                TG.Model.cm_ProjectValueAuditRecord proValueAuditEntity = dal.GetModelByProSysNo(pro.pro_Id);

                pro.proAllotEntity = proValueAuditEntity;

                pro.AuditRecordSysNo = proValueAuditEntity == null ? 0 : proValueAuditEntity.SysNo;
                pro.AuditStatus = proValueAuditEntity == null ? "" : proValueAuditEntity.Status;
            }
            return proValueAuditList;
        }


        /// <summary>
        /// 查询合同基本信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_ProjectValuelYesListByPager(int startIndex, int endIndex, string strWhere)
        {
            return dal.P_cm_ProjectValuelYesListByPager(startIndex, endIndex, strWhere);
        }

        /// <summary>
        /// 查询产值分配信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_jjsProjectValuelYesListByPager(int startIndex, int endIndex, string strWhere)
        {
            return dal.P_cm_jjsProjectValuelYesListByPager(startIndex, endIndex, strWhere);
        }
        /// <summary>
        /// 得到分页信息条数
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet P_cm_ProjectValuelNoListCount(string strWhere)
        {
            return dal.P_cm_ProjectValuelNoListCount(strWhere);
        }

        /// <summary>
        /// 经济所条件
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet P_cm_jjsProjectValuelNoListCount(string strWhere)
        {
            return dal.P_cm_jjsProjectValuelNoListCount(strWhere);
        }

        /// <summary>
        /// 得到分页信息条数
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet P_cm_ProjectValuelYesListCount(string strWhere)
        {
            return dal.P_cm_ProjectValuelYesListCount(strWhere);
        }


        /// <summary>
        /// 经济所
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet P_cm_jjsProjectValuelYesListCount(string strWhere)
        {
            return dal.P_cm_jjsProjectValuelYesListCount(strWhere);
        }
        /// <summary>
        /// 得到分页信息条数-二次分配总数
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet P_cm_ProjectSecondValuelListCount(string strWhere)
        {
            return dal.P_cm_ProjectSecondValuelListCount(strWhere);
        }

        /// <summary>
        /// 得到二次分配产值列表 -导出用
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetSecondValueExport(string strWhere, string year)
        {
            return dal.GetSecondValueExport(strWhere, year);
        }
        /// <summary>
        /// 得到审核人员所在的部门
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public string GetUserUnitName(int userID)
        {
            return dal.GetUserUnitName(userID);
        }

        /// <summary>
        /// 得到建筑结构的审核信息
        /// </summary>
        /// <param name="AllotID"></param>
        /// <returns></returns>
        public decimal GetProcessBySpeJzJgAuditAmount(int? AllotID)
        {
            return dal.GetProcessBySpeJzJgAuditAmount(AllotID);
        }
        /// <summary>
        /// 得到二次分配金额明细
        /// </summary>
        /// <param name="proID"></param>
        /// <param name="AllotID"></param>
        /// <returns></returns>
        public DataSet GetSecondAuditDesignAmount(int proID, int AllotID)
        {
            return dal.GetSecondAuditDesignAmount(proID, AllotID);
        }
        /// <summary>
        /// 得到经济所人员产值信息
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public DataSet GetJjsProjectPlanUser(int proID)
        {
            return dal.GetJjsProjectPlanUser(proID);
        }

        /// <summary>
        /// 取得策划参与人员专业--经济所项目
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public DataSet GetJjsProjectPlanUserSpe(int proID)
        {
            return dal.GetJjsProjectPlanUserSpe(proID);
        }
        /// <summary>
        /// 所长更新产值阶段
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateProjectAllotValueProcess(string strWhere, int proid, int AllotId, decimal loanCount)
        {
            return dal.UpdateProjectAllotValueProcess(strWhere, proid, AllotId, loanCount);
        }

        /// <summary>
        /// 所有审批人员
        /// </summary>
        /// <param name="pro_id"></param>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetAuditMemberList(int pro_id, int? allotID, string where)
        {
            return dal.GetAuditMemberList(pro_id, allotID, where);
        }
        /// <summary>
        /// 所有审批人员-转
        /// </summary>
        /// <param name="pro_id"></param>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetAuditTranMemberList(int pro_id, int? allotID, string where)
        {
            return dal.GetAuditTranMemberList(pro_id, allotID, where);
        }
        /// <summary>
        /// 取得工序分配明细
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetProjectDesignProcessValueDetails(int allotID)
        {
            return dal.GetProjectDesignProcessValueDetails(allotID);
        }
        /// <summary>
        /// 更新产值分配人员明细信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateProjectAllotValueByMemberDetatil(ProjectValueAllotByMemberEntity dataEntity, int allotID, int speId)
        {
            return dal.UpdateProjectAllotValueByMemberDetatil(dataEntity, allotID, speId);
        }
        /// <summary>
        /// 更新经济所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateJjsProjectAllotValueByMemberDetail(jjsProjectValueAllotByMemberEntity dataEntity, string isTran)
        {
            return dal.UpdateJjsProjectAllotValueByMemberDetail(dataEntity, isTran);
        }
        /// <summary>
        ///  三所长全部通过，即结束流程.
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateThreeAllPassProcess(TG.Model.cm_ProjectValueAuditRecord model, int AllotID)
        {
            return dal.UpdateThreeAllPassProcess(model, AllotID);
        }
         /// <summary>
        ///  经济所全部通过，即结束流程.
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateJjsThreeAllPassProcess(TG.Model.cm_ProjectValueAuditRecord model, int AllotID)
        {
            return dal.UpdateJjsThreeAllPassProcess(model, AllotID);
        }
        #endregion  Method
    }
}
