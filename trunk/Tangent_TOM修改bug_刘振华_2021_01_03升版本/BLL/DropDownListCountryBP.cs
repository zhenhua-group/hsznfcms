﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.BLL
{
	public class DropDownListCountryBP
	{
		private TG.DAL.DropDownListCountryDA DA = new TG.DAL.DropDownListCountryDA();

		/// <summary>
		/// 得到国家列表
		/// </summary>
		/// <param name="queryEntity"></param>
		/// <returns></returns>
		public List<TG.Model.Country> GetCountryList(TG.Model.DropDownListQueryEntity queryEntity)
		{
			return DA.GetCountryList(queryEntity);
		}

		/// <summary>
		/// 得到城市列表
		/// </summary>
		/// <param name="countryID">国家ID</param>
		/// <returns></returns>
		public List<TG.Model.Province> GetProvinceList(string countryID)
		{
			return DA.GetProvinceList(countryID);
		}

		/// <summary>
		/// 得到城市列表
		/// </summary>
		/// <param name="provinceID"></param>
		/// <returns></returns>
		public List<TG.Model.City> GetCitys(string provinceID)
		{
			return DA.GetCitys(provinceID);
		}
	}
}
