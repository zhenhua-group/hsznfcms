﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.Model;

namespace TG.BLL
{
    public class cm_ProjectFileInfo
    {
        private readonly TG.DAL.cm_ProjectFileInfo dal = new TG.DAL.cm_ProjectFileInfo();

        /// <summary>
        /// 工程设计归档
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_ProjectFileInfoNoByPager(int startIndex, int endIndex, string strWhere)
        {
            return dal.P_cm_ProjectFileInfoNoByPager(startIndex, endIndex, strWhere);
        }
        /// <summary>
        /// 工程设计归档
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_ProjectFileInfoYesByPager(int startIndex, int endIndex, string strWhere)
        {
            return dal.P_cm_ProjectFileInfoYesByPager(startIndex, endIndex, strWhere);
        }

        /// <summary>
        /// 获取需要分页的数据总数--工程设计归档
        /// </summary>
        public object GetProjectFileInfoNoProcCount(string query)
        {
            return dal.GetProjectFileInfoNoProcCount(query);
        }

        /// <summary>
        /// 获取需要分页的数据总数--工程设计归档
        /// </summary>
        public object GetProjectFileInfoYesProcCount(string query)
        {
            return dal.GetProjectFileInfoYesProcCount(query);
        }
        /// <summary>
        /// 新增工程设计归档
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertProjectFileInfo(ProjectFileInfoEntity dataEntity)
        {
            return dal.InsertProjectFileInfo(dataEntity);
        }
        /// <summary>
        /// 得到归档信息
        /// </summary>
        /// <param name="proId"></param>
        /// <returns></returns>
        public DataSet GetFileInfo(int proId)
        {
            return dal.GetFileInfo(proId);
        }

        /// <summary>
        /// 查询审核记录列表
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<TG.Model.ProjectFileAuditList> GetProjectFileListView(string query, int startIndex, int endIndex)
        {
            //查询基本信息
            List<TG.Model.ProjectFileAuditList> plotList = dal.GetProjectFileAuditListView(query, startIndex, endIndex);
            //查询审核记录
            foreach (var list in plotList)
            {
                TG.Model.cm_ProjectFileInfoAudit projectPlotInfoAudit = dal.GetModelByProIDSysNo(list.Pro_ID);
                list.ProjectFileInfoAuditEntity = projectPlotInfoAudit;
                list.AuditRecordSysNo = projectPlotInfoAudit == null ? 0 : projectPlotInfoAudit.SysNo;
                list.AuditStatus = projectPlotInfoAudit == null ? "" : projectPlotInfoAudit.Status;
            }
            return plotList;
        }

        /// <summary>
        /// 新规一条审批记录
        /// </summary>
        /// <param name="coperation"></param>
        /// <returns></returns>
        public int InsertProjectFileAuditRecord(TG.Model.cm_ProjectFileInfoAudit model)
        {
            return dal.InsertProjectFileAuditRecord(model);
        }
        /// <summary>
        /// 更新审核信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateProjectFileAudit(cm_ProjectFileInfoAudit dataEntity)
        {
            return dal.UpdateProjectFileAudit(dataEntity);
        }
        /// <summary>
        /// 得到审核信息
        /// </summary>
        /// <param name="queryEntity"></param>
        /// <returns></returns>
        public cm_ProjectFileInfoAudit GetProjectFileInfoAuditEntity(cm_ProjectFileAuditQueryEntity queryEntity)
        {
            return dal.GetProjectFileInfoAuditEntity(queryEntity);
        }
        /// <summary>
        /// 得到审核配置信息
        /// </summary>
        /// <returns></returns>
        public List<string> GetAuditProcessDescription()
        {
            return dal.GetAuditProcessDescription();
        }
    }
}
