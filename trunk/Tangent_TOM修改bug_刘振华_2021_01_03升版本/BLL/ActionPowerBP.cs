﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;

namespace TG.BLL
{
    public class ActionPowerBP
    {
        private ActionPowerDA da = new ActionPowerDA();

        public List<ActionPowerViewEntity> GetActionPowerList(LeftMenuType leftMenuType)
        {
            List<ActionPowerViewEntity> resultList = da.GetActionPowerList(leftMenuType);

            return resultList;
        }

        public int InsertActionPower(ActionPowerViewEntity actionPowerViewEntity)
        {
            return da.InsertActionPower(actionPowerViewEntity);
        }

        public ActionPowerViewEntity GetActionPowerViewEntity(int actionPowerSysNo)
        {
            return da.GetActionPowerViewEntity(actionPowerSysNo);
        }
        public ActionPowerViewEntity GetActionPowerViewEntity(ActionPowerQueryEntity queryEntity)
        {
            return da.GetActionPowerViewEntity(queryEntity);
        }

        public int UpDateActionPower(ActionPowerViewEntity actionPower)
        {
            return da.UpDateActionPower(actionPower);
        }

        public int DeleteActionPower(int sysNo)
        {
            return da.DeleteActionPower(sysNo);
        }
    }
}
