﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.Model;

namespace TG.BLL
{
    public class cm_ProjectPlotInfo
    {
        private readonly TG.DAL.cm_ProjectPlotInfo dal = new TG.DAL.cm_ProjectPlotInfo();

        /// <summary>
        /// 工程设计出图卡
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_ProjectPlotInfoNoByPager(int startIndex, int endIndex, string strWhere)
        {
            return dal.P_cm_ProjectPlotInfoNoByPager(startIndex, endIndex, strWhere);
        }
        /// <summary>
        /// 工程设计出图卡
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_ProjectPlotInfoYesByPager(int startIndex, int endIndex, string strWhere)
        {
            return dal.P_cm_ProjectPlotInfoYesByPager(startIndex, endIndex, strWhere);
        }

        /// <summary>
        /// 获取需要分页的数据总数--工程出图卡
        /// </summary>
        public object GetProjectPlotInfoNoProcCount(string query)
        {
            return dal.GetProjectPlotInfoNoProcCount(query);
        }

        /// <summary>
        /// 获取需要分页的数据总数--工程出图卡
        /// </summary>
        public object GetProjectPlotInfoYesProcCount(string query)
        {
            return dal.GetProjectPlotInfoYesProcCount(query);
        }
        /// <summary>
        /// 新增工程设计出图卡
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertProjectPlotInfo(ProjectPlotInfoEntity dataEntity)
        {
            return dal.InsertProjectPlotInfo(dataEntity);
        }
        /// <summary>
        /// 得到设计出图卡信息
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public TG.Model.cm_ProjectPlotInfo GetPlotInfo(int proID)
        {
            return dal.GetPlotInfo(proID);
        }
        /// <summary>
        /// 得到子项工程出图卡
        /// </summary>
        /// <param name="proId"></param>
        /// <returns></returns>
        public DataSet GetSubPlotInfo(int proId)
        {
            return dal.GetSubPlotInfo(proId);
        }

        /// <summary>
        /// 查询审核记录列表
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<TG.Model.ProjectPlotAuditList> GetProjectPlotListView(string query, int startIndex, int endIndex)
        {
            //查询基本信息
            List<TG.Model.ProjectPlotAuditList> plotList = dal.GetProjectPlotAuditListView(query, startIndex, endIndex);
            //查询审核记录
            foreach (var list in plotList)
            {
                TG.Model.cm_ProjectPlotInfoAudit projectPlotInfoAudit = dal.GetModelByProIDSysNo(list.Pro_ID);
                list.ProjectPlotInfoAuditEntity = projectPlotInfoAudit;
                list.AuditRecordSysNo = projectPlotInfoAudit == null ? 0 : projectPlotInfoAudit.SysNo;
                list.AuditStatus = projectPlotInfoAudit == null ? "" : projectPlotInfoAudit.Status;
            }
            return plotList;
        }


        /// <summary>
        /// 新规一条审批记录
        /// </summary>
        /// <param name="coperation"></param>
        /// <returns></returns>
        public int InsertProjectPlotAuditRecord(TG.Model.cm_ProjectPlotInfoAudit plot)
        {
            return dal.InsertProjectPlotAuditRecord(plot);
        }

        /// <summary>
        /// 得到审核信息
        /// </summary>
        /// <param name="queryEntity"></param>
        /// <returns></returns>
        public cm_ProjectPlotInfoAudit GetProjectPlotInfoAuditEntity(cm_ProjectPlotAuditQueryEntity queryEntity)
        {
            return dal.GetProjectPlotInfoAuditEntity(queryEntity);
        }
        /// <summary>
        /// 得到审核配置信息
        /// </summary>
        /// <returns></returns>
        public List<string> GetAuditProcessDescription()
        {
            return dal.GetAuditProcessDescription();
        }
        /// <summary>
        /// 更新审核信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateProjectPlanAudit(cm_ProjectPlotInfoAudit dataEntity)
        {
            return dal.UpdateProjectPlanAudit(dataEntity);
        }
        /// <summary>
        /// 更新档案号
        /// </summary>
        /// <param name="ProID"></param>
        /// <param name="fileNum"></param>
        /// <returns></returns>
        public int UpdateFileNum(int ProID, string fileNum)
        {
            return dal.UpdateFileNum(ProID, fileNum);
        }
    }
}
