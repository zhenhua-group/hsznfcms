﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_ApplyStatisData
	/// </summary>
	public partial class cm_ApplyStatisData
	{
		private readonly TG.DAL.cm_ApplyStatisData dal=new TG.DAL.cm_ApplyStatisData();
		public cm_ApplyStatisData()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_ApplyStatisData model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ApplyStatisData model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			
			return dal.Delete(id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			return dal.DeleteList(idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ApplyStatisData GetModel(int id)
		{
			
			return dal.GetModel(id);
		}
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public TG.Model.cm_ApplyStatisData GetModelWhere(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            List<TG.Model.cm_ApplyStatisData> list = DataTableToList(ds.Tables[0]);
            if (list != null && list.Count > 0)
            {
                return list[0];
            }
            return null;
        }

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_ApplyStatisData GetModelByCache(int id)
		{
			
			string CacheKey = "cm_ApplyStatisDataModel-" + id;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(id);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_ApplyStatisData)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ApplyStatisData> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ApplyStatisData> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_ApplyStatisData> modelList = new List<TG.Model.cm_ApplyStatisData>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_ApplyStatisData model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_ApplyStatisData();
					if(dt.Rows[n]["id"]!=null && dt.Rows[n]["id"].ToString()!="")
					{
						model.id=int.Parse(dt.Rows[n]["id"].ToString());
					}
					if(dt.Rows[n]["mem_id"]!=null && dt.Rows[n]["mem_id"].ToString()!="")
					{
						model.mem_id=int.Parse(dt.Rows[n]["mem_id"].ToString());
					}
					if(dt.Rows[n]["dataYear"]!=null && dt.Rows[n]["dataYear"].ToString()!="")
					{
						model.dataYear=int.Parse(dt.Rows[n]["dataYear"].ToString());
					}
					if(dt.Rows[n]["dataMonth"]!=null && dt.Rows[n]["dataMonth"].ToString()!="")
					{
						model.dataMonth=int.Parse(dt.Rows[n]["dataMonth"].ToString());
					}
					if(dt.Rows[n]["dataDay"]!=null && dt.Rows[n]["dataDay"].ToString()!="")
					{
						model.dataDay=int.Parse(dt.Rows[n]["dataDay"].ToString());
					}
					if(dt.Rows[n]["dataValue"]!=null && dt.Rows[n]["dataValue"].ToString()!="")
					{
						model.dataValue=decimal.Parse(dt.Rows[n]["dataValue"].ToString());
					}
					if(dt.Rows[n]["dataType"]!=null && dt.Rows[n]["dataType"].ToString()!="")
					{
					model.dataType=dt.Rows[n]["dataType"].ToString();
					}
					if(dt.Rows[n]["dataSource"]!=null && dt.Rows[n]["dataSource"].ToString()!="")
					{
					model.dataSource=dt.Rows[n]["dataSource"].ToString();
					}
					if(dt.Rows[n]["dataContent"]!=null && dt.Rows[n]["dataContent"].ToString()!="")
					{
					model.dataContent=dt.Rows[n]["dataContent"].ToString();
					}
					if(dt.Rows[n]["addUserId"]!=null && dt.Rows[n]["addUserId"].ToString()!="")
					{
						model.addUserId=int.Parse(dt.Rows[n]["addUserId"].ToString());
					}
					if(dt.Rows[n]["addDate"]!=null && dt.Rows[n]["addDate"].ToString()!="")
					{
						model.addDate=DateTime.Parse(dt.Rows[n]["addDate"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

