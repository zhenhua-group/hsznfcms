﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
using System.Collections;
namespace TG.BLL
{
	/// <summary>
	/// cm_ApplyInfo
	/// </summary>
	public partial class cm_ApplyInfo
	{
		private readonly TG.DAL.cm_ApplyInfo dal=new TG.DAL.cm_ApplyInfo();
		public cm_ApplyInfo()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			return dal.Exists(ID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_ApplyInfo model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ApplyInfo model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			return dal.Delete(ID);
		}
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public ArrayList DeleteList(string bs_project_Idlist)
        {
            return dal.DeleteList(bs_project_Idlist);
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ApplyInfo GetModel(int ID)
		{
			
			return dal.GetModel(ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_ApplyInfo GetModelByCache(int ID)
		{
			
			string CacheKey = "cm_ApplyInfoModel-" + ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_ApplyInfo)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ApplyInfo> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ApplyInfo> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_ApplyInfo> modelList = new List<TG.Model.cm_ApplyInfo>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_ApplyInfo model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_ApplyInfo();
					if(dt.Rows[n]["ID"]!=null && dt.Rows[n]["ID"].ToString()!="")
					{
						model.ID=int.Parse(dt.Rows[n]["ID"].ToString());
					}
					if(dt.Rows[n]["applytype"]!=null && dt.Rows[n]["applytype"].ToString()!="")
					{
					model.applytype=dt.Rows[n]["applytype"].ToString();
					}
					if(dt.Rows[n]["reason"]!=null && dt.Rows[n]["reason"].ToString()!="")
					{
					model.reason=dt.Rows[n]["reason"].ToString();
					}
					if(dt.Rows[n]["address"]!=null && dt.Rows[n]["address"].ToString()!="")
					{
					model.address=dt.Rows[n]["address"].ToString();
					}
					if(dt.Rows[n]["starttime"]!=null && dt.Rows[n]["starttime"].ToString()!="")
					{
						model.starttime=DateTime.Parse(dt.Rows[n]["starttime"].ToString());
					}
					if(dt.Rows[n]["endtime"]!=null && dt.Rows[n]["endtime"].ToString()!="")
					{
						model.endtime=DateTime.Parse(dt.Rows[n]["endtime"].ToString());
					}
					if(dt.Rows[n]["totaltime"]!=null && dt.Rows[n]["totaltime"].ToString()!="")
					{
						model.totaltime=decimal.Parse(dt.Rows[n]["totaltime"].ToString());
					}
					if(dt.Rows[n]["iscar"]!=null && dt.Rows[n]["iscar"].ToString()!="")
					{
					model.iscar=dt.Rows[n]["iscar"].ToString();
					}
					if(dt.Rows[n]["kilometre"]!=null && dt.Rows[n]["kilometre"].ToString()!="")
					{
						model.kilometre=decimal.Parse(dt.Rows[n]["kilometre"].ToString());
					}
					if(dt.Rows[n]["remark"]!=null && dt.Rows[n]["remark"].ToString()!="")
					{
					model.remark=dt.Rows[n]["remark"].ToString();
					}
					if(dt.Rows[n]["adduser"]!=null && dt.Rows[n]["adduser"].ToString()!="")
					{
						model.adduser=int.Parse(dt.Rows[n]["adduser"].ToString());
					}
					if(dt.Rows[n]["addtime"]!=null && dt.Rows[n]["addtime"].ToString()!="")
					{
						model.addtime=DateTime.Parse(dt.Rows[n]["addtime"].ToString());
					}
                    if (dt.Rows[n]["IsDone"] != null && dt.Rows[n]["IsDone"].ToString() != "")
                    {
                        model.IsDone = dt.Rows[n]["IsDone"].ToString();
                    }
                    if (dt.Rows[n]["RelationID"] != null && dt.Rows[n]["RelationID"].ToString() != "")
                    {
                        model.RelationID = int.Parse(dt.Rows[n]["RelationID"].ToString());
                    }
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method

          /// <summary>
        /// 获取申请通过的数据
        /// </summary>
        public DataSet GetApplyList(string strWhere)
        {
            return dal.GetApplyList(strWhere);
        }
	}
}

