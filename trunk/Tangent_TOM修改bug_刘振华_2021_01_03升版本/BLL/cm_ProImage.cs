﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_ProImage
	/// </summary>
	public partial class cm_ProImage
	{
		private readonly TG.DAL.cm_ProImage dal=new TG.DAL.cm_ProImage();
		public cm_ProImage()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			return dal.Exists(ID);
		}
        /// <summary>
        /// 判断是否存在该记录，并得到id
        /// </summary>
        public int GetSingle(int ID)
        {
            return dal.GetSingle(ID);
        }
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_ProImage model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ProImage model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			return dal.Delete(ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			return dal.DeleteList(IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ProImage GetModel(int ID)
		{
			
			return dal.GetModel(ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_ProImage GetModelByCache(int ID)
		{
			
			string CacheKey = "cm_ProImageModel-" + ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_ProImage)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ProImage> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ProImage> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_ProImage> modelList = new List<TG.Model.cm_ProImage>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_ProImage model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_ProImage();
					if(dt.Rows[n]["ID"]!=null && dt.Rows[n]["ID"].ToString()!="")
					{
						model.ID=int.Parse(dt.Rows[n]["ID"].ToString());
					}
					if(dt.Rows[n]["pro_id"]!=null && dt.Rows[n]["pro_id"].ToString()!="")
					{
					model.pro_id=dt.Rows[n]["pro_id"].ToString();
					}
					if(dt.Rows[n]["pro_name"]!=null && dt.Rows[n]["pro_name"].ToString()!="")
					{
					model.pro_name=dt.Rows[n]["pro_name"].ToString();
					}
					if(dt.Rows[n]["pro_number"]!=null && dt.Rows[n]["pro_number"].ToString()!="")
					{
					model.pro_number=dt.Rows[n]["pro_number"].ToString();
					}
					if(dt.Rows[n]["pro_unit"]!=null && dt.Rows[n]["pro_unit"].ToString()!="")
					{
					model.pro_unit=dt.Rows[n]["pro_unit"].ToString();
					}
					if(dt.Rows[n]["pro_pmname"]!=null && dt.Rows[n]["pro_pmname"].ToString()!="")
					{
					model.pro_pmname=dt.Rows[n]["pro_pmname"].ToString();
					}
					if(dt.Rows[n]["pro_status"]!=null && dt.Rows[n]["pro_status"].ToString()!="")
					{
					model.pro_status=dt.Rows[n]["pro_status"].ToString();
					}
					if(dt.Rows[n]["jz_image"]!=null && dt.Rows[n]["jz_image"].ToString()!="")
					{
					model.jz_image=dt.Rows[n]["jz_image"].ToString();
					}
					if(dt.Rows[n]["jz_cadimage"]!=null && dt.Rows[n]["jz_cadimage"].ToString()!="")
					{
					model.jz_cadimage=dt.Rows[n]["jz_cadimage"].ToString();
					}
					if(dt.Rows[n]["jg_image"]!=null && dt.Rows[n]["jg_image"].ToString()!="")
					{
					model.jg_image=dt.Rows[n]["jg_image"].ToString();
					}
					if(dt.Rows[n]["jg_cadimage"]!=null && dt.Rows[n]["jg_cadimage"].ToString()!="")
					{
					model.jg_cadimage=dt.Rows[n]["jg_cadimage"].ToString();
					}
					if(dt.Rows[n]["jps_image"]!=null && dt.Rows[n]["jps_image"].ToString()!="")
					{
					model.jps_image=dt.Rows[n]["jps_image"].ToString();
					}
					if(dt.Rows[n]["jps_cadimage"]!=null && dt.Rows[n]["jps_cadimage"].ToString()!="")
					{
					model.jps_cadimage=dt.Rows[n]["jps_cadimage"].ToString();
					}
					if(dt.Rows[n]["nt_image"]!=null && dt.Rows[n]["nt_image"].ToString()!="")
					{
					model.nt_image=dt.Rows[n]["nt_image"].ToString();
					}
					if(dt.Rows[n]["nt_cadimage"]!=null && dt.Rows[n]["nt_cadimage"].ToString()!="")
					{
					model.nt_cadimage=dt.Rows[n]["nt_cadimage"].ToString();
					}
					if(dt.Rows[n]["dq_image"]!=null && dt.Rows[n]["dq_image"].ToString()!="")
					{
					model.dq_image=dt.Rows[n]["dq_image"].ToString();
					}
					if(dt.Rows[n]["dq_cadimage"]!=null && dt.Rows[n]["dq_cadimage"].ToString()!="")
					{
					model.dq_cadimage=dt.Rows[n]["dq_cadimage"].ToString();
					}
					if(dt.Rows[n]["zh"]!=null && dt.Rows[n]["zh"].ToString()!="")
					{
						model.zh=int.Parse(dt.Rows[n]["zh"].ToString());
					}
					if(dt.Rows[n]["image_type"]!=null && dt.Rows[n]["image_type"].ToString()!="")
					{
					model.image_type=dt.Rows[n]["image_type"].ToString();
					}
					if(dt.Rows[n]["guidang"]!=null && dt.Rows[n]["guidang"].ToString()!="")
					{
						model.guidang=int.Parse(dt.Rows[n]["guidang"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}

        /// <summary>
        /// 得到项目信息
        /// </summary>
        /// <param name="proid"></param>
        /// <returns></returns>
        public DataSet GetProjectInfo(int proid)
        {
            return dal.GetProjectInfo(proid);
        }
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

