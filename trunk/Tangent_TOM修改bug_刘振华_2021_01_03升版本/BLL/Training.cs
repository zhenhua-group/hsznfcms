﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
    /// <summary>
    /// tg_member
    /// </summary>
    public partial class Training
    {
        private readonly TG.DAL.Training dal = new TG.DAL.Training();
        public Training()
        { }
        #region  Method
        /// <summary>
        /// 训练主表里插入一条记录
        /// </summary>
        public int AddTrainingInfo(TG.Model.Training model)
        {
            return dal.AddTrainingInfo(model);
        }
        /// <summary>
        /// 训练明细表里插入数据
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public int AddTrainingInfoDetail(List<TG.Model.Training> list,int trainingInfoId)
        {
            return dal.AddTrainingInfoDetail(list, trainingInfoId);
        }
        //获取培训所有年份
        public List<string> GetTrainingYear()
        {
            return dal.GetTrainingYear();
        }
        //获取培训所有月份
        public List<string> GetTrainingMonth()
        {
            return dal.GetTrainingMonth();
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateTrainingInfo(TG.Model.Training model)
        {
            return dal.UpdateTrainingInfo(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateTrainingInfoDetail(int id,List<TG.Model.Training> list)
        {
            return dal.UpdateTrainingInfoDetail(id,list);
        }

        /// <summary>
        /// 获得特定时间段内的总人数
        /// </summary>
        public List<TG.Model.Training> GetCount(string begin, string end)
        {
            return dal.GetCount(begin, end);
        }

        /// <summary>
        /// 获得特定时间段内的总人数
        /// </summary>
        public List<TG.Model.Training> GetCount1(string begin, string end)
        {
            return dal.GetCount1(begin, end);
        }

        /// <summary>
        /// 各个部门的培训人数占总人数的比例
        /// </summary>
        public List<TG.Model.Training> GetCount2(string begin, string end)
        {
            return dal.GetCount2(begin, end);
        }

        /// <summary>
        /// 各个部门的培训费用合计占总费用的比例
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public List<TG.Model.Training> GetCount3(string begin, string end)
        {
            return dal.GetCount3(begin, end);
        }


        /// <summary>
        /// 各个员工的培训数量占总数的比例
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public List<TG.Model.Training> GetCount4(string begin, string end)
        {
            return dal.GetCount4(begin, end);
        }

        /// <summary>
        /// 各个员工的培训费用合计占总费数的比例
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public List<TG.Model.Training> GetCount5(string begin, string end)
        {
            return dal.GetCount5(begin, end);
        }

        /// <summary>
        /// 各个部门的覆盖率占总数的比例
        /// </summary>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public List<TG.Model.Training> GetCount6(string begin, string end)
        {
            return dal.GetCount6(begin, end);
        }
        #endregion  Method

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {

            return dal.Delete(id);
        }
    }
}

