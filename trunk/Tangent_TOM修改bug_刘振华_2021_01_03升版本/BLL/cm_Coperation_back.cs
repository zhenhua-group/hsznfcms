﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
using System.Data.SqlClient;
namespace TG.BLL
{
	/// <summary>
	/// cm_Coperation_back
	/// </summary>
	public partial class cm_Coperation_back
	{
		private readonly TG.DAL.cm_Coperation_back dal=new TG.DAL.cm_Coperation_back();
		public cm_Coperation_back()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_Coperation_back model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_Coperation_back model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int cpr_Id)
		{
			return dal.Delete(cpr_Id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string cpr_Idlist )
		{
			return dal.DeleteList(cpr_Idlist );
		}
        //删除一组数据
        public bool DeleteList(string[] cpr_Idlist)
        {
            return dal.DeleteList(cpr_Idlist);
        }
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_Coperation_back GetModel(int cpr_Id)
		{
			
			return dal.GetModel(cpr_Id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_Coperation_back GetModelByCache(int cpr_Id)
		{
			
			string CacheKey = "cm_Coperation_backModel-" + cpr_Id;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(cpr_Id);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_Coperation_back)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_Coperation_back> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_Coperation_back> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_Coperation_back> modelList = new List<TG.Model.cm_Coperation_back>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_Coperation_back model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_Coperation_back();
					if(dt.Rows[n]["cpr_Id"]!=null && dt.Rows[n]["cpr_Id"].ToString()!="")
					{
						model.cpr_Id=int.Parse(dt.Rows[n]["cpr_Id"].ToString());
					}
					if(dt.Rows[n]["cst_Id"]!=null && dt.Rows[n]["cst_Id"].ToString()!="")
					{
						model.cst_Id=int.Parse(dt.Rows[n]["cst_Id"].ToString());
					}
					if(dt.Rows[n]["cpr_Type"]!=null && dt.Rows[n]["cpr_Type"].ToString()!="")
					{
					model.cpr_Type=dt.Rows[n]["cpr_Type"].ToString();
					}
					if(dt.Rows[n]["cpr_Type2"]!=null && dt.Rows[n]["cpr_Type2"].ToString()!="")
					{
					model.cpr_Type2=dt.Rows[n]["cpr_Type2"].ToString();
					}
					if(dt.Rows[n]["cpr_Unit"]!=null && dt.Rows[n]["cpr_Unit"].ToString()!="")
					{
					model.cpr_Unit=dt.Rows[n]["cpr_Unit"].ToString();
					}
					if(dt.Rows[n]["cpr_Acount"]!=null && dt.Rows[n]["cpr_Acount"].ToString()!="")
					{
						model.cpr_Acount=decimal.Parse(dt.Rows[n]["cpr_Acount"].ToString());
					}
					if(dt.Rows[n]["cpr_ShijiAcount"]!=null && dt.Rows[n]["cpr_ShijiAcount"].ToString()!="")
					{
						model.cpr_ShijiAcount=decimal.Parse(dt.Rows[n]["cpr_ShijiAcount"].ToString());
					}
					if(dt.Rows[n]["cpr_Touzi"]!=null && dt.Rows[n]["cpr_Touzi"].ToString()!="")
					{
						model.cpr_Touzi=decimal.Parse(dt.Rows[n]["cpr_Touzi"].ToString());
					}
					if(dt.Rows[n]["cpr_ShijiTouzi"]!=null && dt.Rows[n]["cpr_ShijiTouzi"].ToString()!="")
					{
						model.cpr_ShijiTouzi=decimal.Parse(dt.Rows[n]["cpr_ShijiTouzi"].ToString());
					}
					if(dt.Rows[n]["cpr_Mark"]!=null && dt.Rows[n]["cpr_Mark"].ToString()!="")
					{
					model.cpr_Mark=dt.Rows[n]["cpr_Mark"].ToString();
					}
					if(dt.Rows[n]["cpr_Address"]!=null && dt.Rows[n]["cpr_Address"].ToString()!="")
					{
					model.cpr_Address=dt.Rows[n]["cpr_Address"].ToString();
					}
					if(dt.Rows[n]["BuildArea"]!=null && dt.Rows[n]["BuildArea"].ToString()!="")
					{
					model.BuildArea=dt.Rows[n]["BuildArea"].ToString();
					}
					if(dt.Rows[n]["ChgPeople"]!=null && dt.Rows[n]["ChgPeople"].ToString()!="")
					{
					model.ChgPeople=dt.Rows[n]["ChgPeople"].ToString();
					}
					if(dt.Rows[n]["ChgPhone"]!=null && dt.Rows[n]["ChgPhone"].ToString()!="")
					{
					model.ChgPhone=dt.Rows[n]["ChgPhone"].ToString();
					}
					if(dt.Rows[n]["ChgJia"]!=null && dt.Rows[n]["ChgJia"].ToString()!="")
					{
					model.ChgJia=dt.Rows[n]["ChgJia"].ToString();
					}
					if(dt.Rows[n]["ChgJiaPhone"]!=null && dt.Rows[n]["ChgJiaPhone"].ToString()!="")
					{
					model.ChgJiaPhone=dt.Rows[n]["ChgJiaPhone"].ToString();
					}
					if(dt.Rows[n]["UpdateBy"]!=null && dt.Rows[n]["UpdateBy"].ToString()!="")
					{
					model.UpdateBy=dt.Rows[n]["UpdateBy"].ToString();
					}
					if(dt.Rows[n]["BuildPosition"]!=null && dt.Rows[n]["BuildPosition"].ToString()!="")
					{
					model.BuildPosition=dt.Rows[n]["BuildPosition"].ToString();
					}
					if(dt.Rows[n]["Industry"]!=null && dt.Rows[n]["Industry"].ToString()!="")
					{
					model.Industry=dt.Rows[n]["Industry"].ToString();
					}
					if(dt.Rows[n]["BuildUnit"]!=null && dt.Rows[n]["BuildUnit"].ToString()!="")
					{
					model.BuildUnit=dt.Rows[n]["BuildUnit"].ToString();
					}
					if(dt.Rows[n]["BuildSrc"]!=null && dt.Rows[n]["BuildSrc"].ToString()!="")
					{
					model.BuildSrc=dt.Rows[n]["BuildSrc"].ToString();
					}
					if(dt.Rows[n]["RegTime"]!=null && dt.Rows[n]["RegTime"].ToString()!="")
					{
						model.RegTime=DateTime.Parse(dt.Rows[n]["RegTime"].ToString());
					}
					if(dt.Rows[n]["BuildType"]!=null && dt.Rows[n]["BuildType"].ToString()!="")
					{
					model.BuildType=dt.Rows[n]["BuildType"].ToString();
					}
					if(dt.Rows[n]["StructType"]!=null && dt.Rows[n]["StructType"].ToString()!="")
					{
					model.StructType=dt.Rows[n]["StructType"].ToString();
					}
					if(dt.Rows[n]["Floor"]!=null && dt.Rows[n]["Floor"].ToString()!="")
					{
					model.Floor=dt.Rows[n]["Floor"].ToString();
					}
					if(dt.Rows[n]["BuildStructType"]!=null && dt.Rows[n]["BuildStructType"].ToString()!="")
					{
					model.BuildStructType=dt.Rows[n]["BuildStructType"].ToString();
					}
					if(dt.Rows[n]["MultiBuild"]!=null && dt.Rows[n]["MultiBuild"].ToString()!="")
					{
					model.MultiBuild=dt.Rows[n]["MultiBuild"].ToString();
					}
					if(dt.Rows[n]["cpr_Name"]!=null && dt.Rows[n]["cpr_Name"].ToString()!="")
					{
					model.cpr_Name=dt.Rows[n]["cpr_Name"].ToString();
					}
					if(dt.Rows[n]["InsertUserID"]!=null && dt.Rows[n]["InsertUserID"].ToString()!="")
					{
						model.InsertUserID=int.Parse(dt.Rows[n]["InsertUserID"].ToString());
					}
					if(dt.Rows[n]["InsertDate"]!=null && dt.Rows[n]["InsertDate"].ToString()!="")
					{
						model.InsertDate=DateTime.Parse(dt.Rows[n]["InsertDate"].ToString());
					}
					if(dt.Rows[n]["proapproval"]!=null && dt.Rows[n]["proapproval"].ToString()!="")
					{
						model.proapproval=int.Parse(dt.Rows[n]["proapproval"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListByPageProc(query, startIndex, endIndex);
        }
        /// <summary>
        /// 返回需要分页的记录数
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageProcCount(string query)
        {
            return dal.GetListPageProcCount(query);
        }

		#endregion  Method
	}
}

