﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;


namespace TG.BLL
{
    public static class AuditLocusBP
    {
        private static AuditLocusDA da = new AuditLocusDA();

        /// <summary>
        /// 根据ReferenceSysNo得到审核记录
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="referenceSysNo"></param>
        /// <returns></returns>
        public static List<AuditLocusViewEntity> GetAuditRecordByReferenceSysNo<T>(int referenceSysNo)
        {
            object sourceEntity = da.GetAuditRecordByReferenceSysNo<T>(referenceSysNo);

            List<AuditLocusViewEntity> resultList = new List<AuditLocusViewEntity>();

            if (sourceEntity is TG.Model.cm_CoperationAudit)
            {
                #region MyRegion
                TG.Model.cm_CoperationAudit coperationAuditEntity = (TG.Model.cm_CoperationAudit)sourceEntity;

                string[] processDescriptionArray = da.GetProcessDescriptionArray("C");

                List<string> tempList = processDescriptionArray.ToList<string>();
                //根据合同审核配置移除processDescriptionArrayItem
                if (coperationAuditEntity.ManageLevel == 1)
                {
                    tempList.RemoveAt(tempList.Count - 1);
                }
                if (coperationAuditEntity.NeedLegalAdviser == 0 && coperationAuditEntity.Status != "A" && coperationAuditEntity.Status != "B" && coperationAuditEntity.Status != "C")
                {
                    tempList.RemoveAt(2);
                }
                processDescriptionArray = tempList.ToArray();

                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(coperationAuditEntity.AuditUser))
                {
                    auditUserNameArray = SplitString(coperationAuditEntity.AuditUser, false, ',');            
                }               

                string sqlwhere = " and MsgType=1 and ReferenceSysNo like '%CoperationAuditSysNo=" + coperationAuditEntity.SysNo + "&MessageStatus=" + coperationAuditEntity.Status.Trim() + "%' order by SysNo asc";
                //排除最后审批和不通过审批
                if (coperationAuditEntity.Status.Trim() != "J" && (coperationAuditEntity.Status.Trim() != "H" && coperationAuditEntity.ManageLevel != 1) && coperationAuditEntity.Status.Trim() != "C" && coperationAuditEntity.Status.Trim() != "E" && coperationAuditEntity.Status.Trim() != "G" && coperationAuditEntity.Status.Trim() != "I" && coperationAuditEntity.Status.Trim() != "K")
                {
                    DataTable dt = new TG.DAL.cm_SysMsg().GetListWhere(sqlwhere);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string userList = "";
                        for (int i = 0; i < (dt.Rows.Count > 3 ? 3 : dt.Rows.Count); i++)
                        {
                            userList += Convert.ToString(dt.Rows[i]["UserName"]) + ",";
                        }
                        userList = userList.Length > 0 ? userList.Remove((userList.Length) - 1) : "";
                        if (auditUserNameArray==null)
                        {
                            auditUserNameArray = new string[1];
                            auditUserNameArray[0] = userList;
                        }
                        else
                        {
                            List<string> tempArray = auditUserNameArray.ToList();
                            tempArray.Add(userList);
                            auditUserNameArray = tempArray.ToArray();
                        }
                        
                    }
                }


                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(coperationAuditEntity.AuditDate))
                    auditDateArray = SplitString(coperationAuditEntity.AuditDate, false, ',');

                resultList = CreateAuditLocusList(processDescriptionArray, auditUserNameArray, auditDateArray, coperationAuditEntity.Status, coperationAuditEntity.NeedLegalAdviser == 1 ? true : false, coperationAuditEntity.ManageLevel == 1 ? true : false);
                #endregion
            }
            else if (sourceEntity is TG.Model.cm_CoperationAuditEdit)
            {
                #region MyRegion
                TG.Model.cm_CoperationAuditEdit coperationAuditEntity = (TG.Model.cm_CoperationAuditEdit)sourceEntity;

                string[] processDescriptionArray = da.GetProcessDescriptionArray("EC");

                List<string> tempList = processDescriptionArray.ToList<string>();
                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(coperationAuditEntity.AuditUser))
                {
                    auditUserNameArray = SplitString(coperationAuditEntity.AuditUser, false, ',');
                }
                string sqlwhere = " and MsgType=12 and ReferenceSysNo like '%CoperationAuditSysNo=" + coperationAuditEntity.SysNo + "&MessageStatus=" + coperationAuditEntity.Status.Trim() + "%' order by SysNo asc";
                //排除最后审批和不通过审批
                if (coperationAuditEntity.Status.Trim() != "D" && coperationAuditEntity.Status.Trim() != "C" && coperationAuditEntity.Status.Trim() != "E" && coperationAuditEntity.Status.Trim() != "G" && coperationAuditEntity.Status.Trim() != "I" && coperationAuditEntity.Status.Trim() != "K")
                {
                    DataTable dt = new TG.DAL.cm_SysMsg().GetListWhere(sqlwhere);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string userList = "";
                        for (int i = 0; i < (dt.Rows.Count > 3 ? 3 : dt.Rows.Count); i++)
                        {
                            userList += Convert.ToString(dt.Rows[i]["UserName"]) + ",";
                        }
                        userList = userList.Length > 0 ? userList.Remove((userList.Length) - 1) : "";
                        if (auditUserNameArray == null)
                        {
                            auditUserNameArray = new string[1];
                            auditUserNameArray[0] = userList;
                        }
                        else
                        {
                            List<string> tempArray = auditUserNameArray.ToList();
                            tempArray.Add(userList);
                            auditUserNameArray = tempArray.ToArray();
                        }

                    }
                }

                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(coperationAuditEntity.AuditDate))
                    auditDateArray = SplitString(coperationAuditEntity.AuditDate, false, ',');
                resultList = CreateAuditLocusList(processDescriptionArray, auditUserNameArray, auditDateArray, coperationAuditEntity.Status, true, false);
                #endregion
            }
            else if (sourceEntity is TG.Model.cm_ProjectPlanAuditEntity)
            {
                #region MyRegion

                TG.Model.cm_ProjectPlanAuditEntity projectPlannAuditEntity = (TG.Model.cm_ProjectPlanAuditEntity)sourceEntity;

                //if (!string.IsNullOrEmpty(projectPlannAuditEntity.AuditUser))
                //{
                string[] processDescriptionArray = da.GetProcessDescriptionArray("PN");

                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(projectPlannAuditEntity.AuditUser))
                    auditUserNameArray = SplitString(projectPlannAuditEntity.AuditUser, true, ',');

                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(projectPlannAuditEntity.AuditDate))
                    auditDateArray = SplitString(projectPlannAuditEntity.AuditDate, true, ',');


                string sqlwhere = " and MsgType=4 and ReferenceSysNo like '%ProjectPlanAuditSysNo=" + projectPlannAuditEntity.SysNo + "&MessageStatus=" + projectPlannAuditEntity.Status.Trim() + "%' order by SysNo asc";
                //排除最后审批和不通过审批
                if (projectPlannAuditEntity.Status.Trim() != "C" && projectPlannAuditEntity.Status.Trim() != "E" && projectPlannAuditEntity.Status.Trim() != "G" && projectPlannAuditEntity.Status.Trim() != "F" )
                {
                    DataTable dt = new TG.DAL.cm_SysMsg().GetListWhere(sqlwhere);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string userList = "";
                        for (int i = 0; i < (dt.Rows.Count > 3 ? 3 : dt.Rows.Count); i++)
                        {
                            userList += Convert.ToString(dt.Rows[i]["UserName"]) + ",";
                        }
                        userList = userList.Length > 0 ? userList.Remove((userList.Length) - 1) : "";
                        if (auditUserNameArray == null)
                        {
                            auditUserNameArray = new string[1];
                            auditUserNameArray[0] = userList;
                        }
                        else
                        {
                            List<string> tempArray = auditUserNameArray.ToList();
                            tempArray.Add(userList);
                            auditUserNameArray = tempArray.ToArray();
                        }

                    }
                }

                resultList = CreateAuditLocusList(processDescriptionArray, auditUserNameArray, auditDateArray, projectPlannAuditEntity.Status, true, false);
                //} 
                #endregion
            }
            else if (sourceEntity is TG.Model.ProjectAuditDataEntity)
            {
                #region MyRegion
                TG.Model.ProjectAuditDataEntity projectAuditEntity = (TG.Model.ProjectAuditDataEntity)sourceEntity;

                //if (!string.IsNullOrEmpty(projectAuditEntity.AuditUser))
                //{
                string[] processDescriptionArray = da.GetProcessDescriptionArray("P");

                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(projectAuditEntity.AuditUser))
                    auditUserNameArray = SplitString(projectAuditEntity.AuditUser, true, ',');
                //判断是不是状态是最后一个而且不是不通过的状态，怎加上一个人（根据消息）
                if (projectAuditEntity.Status == "A" || projectAuditEntity.Status == "B" || projectAuditEntity.Status == "D")
                {
                    string memnext = GetNextmember(projectAuditEntity.Status, projectAuditEntity.SysNo.ToString(), "2");
                    if (projectAuditEntity.Status == "A")
                    {
                        auditUserNameArray = new string[1] { memnext };
                    }
                    else
                    {
                        List<string> ktls = auditUserNameArray.ToList();
                        ktls.Add(memnext);
                        auditUserNameArray = ktls.ToArray();
                    }

                }
                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(projectAuditEntity.AudtiDate))
                    auditDateArray = SplitString(projectAuditEntity.AudtiDate, true, ',');
                //string Datenext = GetNextDate(projectAuditEntity.Status, projectAuditEntity.SysNo.ToString());
                //if (projectAuditEntity.Status == "A")
                //{
                //    auditUserNameArray = new string[1] { Datenext };
                //}
                resultList = CreateAuditLocusList(processDescriptionArray, auditUserNameArray, auditDateArray, projectAuditEntity.Status, true, false);
                //} 
                #endregion
            }
            else if (sourceEntity is TG.Model.ProjectAuditEditEntity)
            {
                #region MyRegion
                TG.Model.ProjectAuditEditEntity projectAuditEntity = (TG.Model.ProjectAuditEditEntity)sourceEntity;

                //if (!string.IsNullOrEmpty(projectAuditEntity.AuditUser))
                //{
                string[] processDescriptionArray = da.GetProcessDescriptionArray("P");

                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(projectAuditEntity.AuditUser))
                    auditUserNameArray = SplitString(projectAuditEntity.AuditUser, true, ',');

                if (projectAuditEntity.Status == "A" || projectAuditEntity.Status == "B" || projectAuditEntity.Status == "D")
                {
                    string memnext = GetNextmember(projectAuditEntity.Status, projectAuditEntity.SysNo.ToString(), "11");
                    if (projectAuditEntity.Status == "A")
                    {
                        auditUserNameArray = new string[1] { memnext };
                    }
                    else
                    {
                        List<string> ktls = auditUserNameArray.ToList();
                        ktls.Add(memnext);
                        auditUserNameArray = ktls.ToArray();
                    }

                }
                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(projectAuditEntity.AudtiDate))
                {
                    auditDateArray = SplitString(projectAuditEntity.AudtiDate, true, ',');
                }
                bool hasLegalAdviser = true;
                //if (projectAuditEntity.isNeedByDYZ=="0")
                //{
                //    hasLegalAdviser = false;
                //    List<string> nameList = processDescriptionArray.ToList<string>();
                //    nameList.RemoveAt(nameList.Count - 1);
                //    processDescriptionArray = nameList.ToArray();
                //}                
                resultList = CreateAuditLocusList(processDescriptionArray, auditUserNameArray, auditDateArray, projectAuditEntity.Status, hasLegalAdviser, false);
                //}
                #endregion
            }
            //二次产值
            else if (sourceEntity is TG.Model.cm_ProjectValueAuditRecord)
            {
                #region MyRegion
                TG.Model.cm_ProjectValueAuditRecord allotAuditEntity = (TG.Model.cm_ProjectValueAuditRecord)sourceEntity;

                //if (!string.IsNullOrEmpty(projectAuditEntity.AuditUser))
                //{
                string[] processDescriptionArray = da.GetProcessDescriptionArray("A");

                List<string> tempList = processDescriptionArray.ToList<string>();
                tempList.RemoveAt(0);
                tempList.RemoveAt(1);
                tempList.RemoveAt(1);

                processDescriptionArray = tempList.ToArray();

                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(allotAuditEntity.AuditUser))
                    auditUserNameArray = SplitString(allotAuditEntity.AuditUser, false, ',');

                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(allotAuditEntity.AuditDate))
                    auditDateArray = SplitString(allotAuditEntity.AuditDate, true, ',');

                resultList = CreateAuditLocusAllotList(processDescriptionArray, auditUserNameArray, auditDateArray, allotAuditEntity.Status, true, false, allotAuditEntity);
                //} 
                #endregion
            }
            else if (sourceEntity is TG.Model.cm_ProImaAudit)
            {
                #region MyRegion
                TG.Model.cm_ProImaAudit proImaAuditEntity = (TG.Model.cm_ProImaAudit)sourceEntity;

                string[] processDescriptionArray = da.GetProcessDescriptionArray("M");

                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(proImaAuditEntity.AuditUser))
                    auditUserNameArray = SplitString(proImaAuditEntity.AuditUser, false, ',');

                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(proImaAuditEntity.AuditDate))
                    auditDateArray = SplitString(proImaAuditEntity.AuditDate, true, ',');

                resultList = CreateAuditLocusList(processDescriptionArray, auditUserNameArray, auditDateArray, proImaAuditEntity.Status, true, false);

                #endregion
            }
            //工程出图
            else if (sourceEntity is TG.Model.cm_ProjectPlotInfoAudit)
            {
                #region
                TG.Model.cm_ProjectPlotInfoAudit projectPlotInfoAudit = (TG.Model.cm_ProjectPlotInfoAudit)sourceEntity;
                string[] processDescriptionArray = da.GetProcessDescriptionArray("PLOT");

                //获取出图类别
                TG.BLL.cm_ProjectPlotInfo plot = new TG.BLL.cm_ProjectPlotInfo();
                TG.Model.cm_ProjectPlotInfo model = plot.GetPlotInfo(projectPlotInfoAudit.ProjectSysNo);
                if (model != null)
                {
                    if (model.PlotType.Trim().Equals("正常输出") || model.PlotType.Trim().Equals("补充图") || model.PlotType.Trim().Equals("变更图"))
                    {
                        List<string> tempList = processDescriptionArray.ToList<string>();
                        //根据合同审核配置移除processDescriptionArrayItem
                        tempList.RemoveAt(0);
                        tempList.RemoveAt(0);
                        tempList.RemoveAt(0);
                        tempList.RemoveAt(0);
                        tempList.RemoveAt(0);
                        processDescriptionArray = tempList.ToArray();
                    }
                }

                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(projectPlotInfoAudit.AuditUser))

                    auditUserNameArray = SplitString(projectPlotInfoAudit.AuditUser, true, ',');

                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(projectPlotInfoAudit.AuditDate))
                    auditDateArray = SplitString(projectPlotInfoAudit.AuditDate, true, ',');

                resultList = CreatePlotAuditLocusList(processDescriptionArray, auditUserNameArray, auditDateArray, projectPlotInfoAudit.Status, true, false);
                #endregion
            }//归档资料
            else if (sourceEntity is TG.Model.cm_ProjectFileInfoAudit)
            {

                TG.Model.cm_ProjectFileInfoAudit projectAuditEntity = (TG.Model.cm_ProjectFileInfoAudit)sourceEntity;

                string[] processDescriptionArray = da.GetProcessDescriptionArray("File");

                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(projectAuditEntity.AuditUser))

                    auditUserNameArray = SplitString(projectAuditEntity.AuditUser, true, ',');

                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(projectAuditEntity.AuditDate))
                    auditDateArray = SplitString(projectAuditEntity.AuditDate, true, ',');


                resultList = CreateAuditLocusList(processDescriptionArray, auditUserNameArray, auditDateArray, projectAuditEntity.Status, true, false);

            }
            //监理公司
            else if (sourceEntity is TG.Model.cm_SuperCoperationAudit)
            {
                #region MyRegion

                TG.Model.cm_SuperCoperationAudit projectPlannAuditEntity = (TG.Model.cm_SuperCoperationAudit)sourceEntity;

                //if (!string.IsNullOrEmpty(projectPlannAuditEntity.AuditUser))
                //{
                string[] processDescriptionArray = da.GetProcessDescriptionArray("SuperC");

                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(projectPlannAuditEntity.AuditUser))
                {
                    auditUserNameArray = SplitString(projectPlannAuditEntity.AuditUser, true, ',');
                }

                string sqlwhere = " and MsgType=23 and ReferenceSysNo like '%CoperationAuditSysNo=" + projectPlannAuditEntity.SysNo + "&MessageStatus=" + projectPlannAuditEntity.Status.Trim() + "%' order by SysNo asc";
                //排除最后审批和不通过审批
                if (projectPlannAuditEntity.Status.Trim() != "D" && projectPlannAuditEntity.Status.Trim() != "C" && projectPlannAuditEntity.Status.Trim() != "E")
                {
                    DataTable dt = new TG.DAL.cm_SysMsg().GetListWhere(sqlwhere);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string userList = "";
                        for (int i = 0; i < (dt.Rows.Count > 3 ? 3 : dt.Rows.Count); i++)
                        {
                            userList += Convert.ToString(dt.Rows[i]["UserName"]) + ",";
                        }
                        userList = userList.Length > 0 ? userList.Remove((userList.Length) - 1) : "";
                        if (auditUserNameArray == null)
                        {
                            auditUserNameArray = new string[1];
                            auditUserNameArray[0] = userList;
                        }
                        else
                        {
                            List<string> tempArray = auditUserNameArray.ToList();
                            tempArray.Add(userList);
                            auditUserNameArray = tempArray.ToArray();
                        }

                    }
                }

                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(projectPlannAuditEntity.AuditDate))
                    auditDateArray = SplitString(projectPlannAuditEntity.AuditDate, true, ',');

                resultList = CreateAuditLocusList(processDescriptionArray, auditUserNameArray, auditDateArray, projectPlannAuditEntity.Status, true, false);
                //} 
                #endregion
        }
            //监理修改申请
            else if (sourceEntity is TG.Model.cm_SuperCoperationAuditRepeat)
            {
                #region MyRegion

                TG.Model.cm_SuperCoperationAuditRepeat projectPlannAuditEntity = (TG.Model.cm_SuperCoperationAuditRepeat)sourceEntity;

                //if (!string.IsNullOrEmpty(projectPlannAuditEntity.AuditUser))
                //{
                string[] processDescriptionArray = da.GetProcessDescriptionArray("SuperEC");

                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(projectPlannAuditEntity.AuditUser))
                {
                    auditUserNameArray = SplitString(projectPlannAuditEntity.AuditUser, true, ',');
                }
                string sqlwhere = " and MsgType=24 and ReferenceSysNo like '%CoperationAuditSysNo=" + projectPlannAuditEntity.SysNo + "&MessageStatus=" + projectPlannAuditEntity.Status.Trim() + "%' order by SysNo asc";
                //排除最后审批和不通过审批
                if (projectPlannAuditEntity.Status.Trim() != "D" && projectPlannAuditEntity.Status.Trim() != "C" && projectPlannAuditEntity.Status.Trim() != "E")
                {
                    DataTable dt = new TG.DAL.cm_SysMsg().GetListWhere(sqlwhere);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string userList = "";
                        for (int i = 0; i < (dt.Rows.Count > 3 ? 3 : dt.Rows.Count); i++)
                        {
                            userList += Convert.ToString(dt.Rows[i]["UserName"]) + ",";
                        }
                        userList = userList.Length > 0 ? userList.Remove((userList.Length) - 1) : "";
                        if (auditUserNameArray == null)
                        {
                            auditUserNameArray = new string[1];
                            auditUserNameArray[0] = userList;
                        }
                        else
                        {
                            List<string> tempArray = auditUserNameArray.ToList();
                            tempArray.Add(userList);
                            auditUserNameArray = tempArray.ToArray();
                        }

                    }
                }

                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(projectPlannAuditEntity.AuditDate))
                    auditDateArray = SplitString(projectPlannAuditEntity.AuditDate, true, ',');

                resultList = CreateAuditLocusList(processDescriptionArray, auditUserNameArray, auditDateArray, projectPlannAuditEntity.Status, true, false);
                //} 
                #endregion
            }
            return resultList;

        }

        private static string[] SplitString(string text, bool needSubString, char splitChar)
        {
            if (needSubString)
            {
                text = text.Substring(0, text.Length - 1);
            }
            return text.Split(splitChar);
        }

        private static List<AuditLocusViewEntity> CreateAuditLocusList(string[] processDescriptionArray, string[] auditUserNameArray, string[] auditDateArray, string auditStatus, bool hasLegalAdviser, bool stopInH)
        {
            List<AuditLocusViewEntity> resultList = new List<AuditLocusViewEntity>();

            TG.DAL.tg_member userDA = new TG.DAL.tg_member();
            bool flag = false;
            for (int i = 0; i < processDescriptionArray.Length; i++)
            {
                AuditLocusViewEntity auditLocusViewEntity = new AuditLocusViewEntity
                {
                    ProcessDescription = processDescriptionArray[i]
                };
                if (auditDateArray == null || i > auditDateArray.Length - 1)
                {
                    auditLocusViewEntity.AuditDate = "未审核";
                 
                     auditLocusViewEntity.AuditUserName = "未审核";
                
                    auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";

                    if (!flag)
                    {
                        //下一步审批人
                        if (auditUserNameArray != null && i <= (auditUserNameArray.Length - 1))
                        {
                            flag = true;
                            auditLocusViewEntity.AuditUserName = auditUserNameArray[i];
                        }
                    }                   
                }
                else
                {
                    auditLocusViewEntity.AuditDate = auditDateArray[i];
                    TG.Model.tg_member user = userDA.GetModel(int.Parse(auditUserNameArray[i]));

                    auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                    auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                }
               

                switch (auditStatus)
                {
                    case "A":
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "B":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "C":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "D":
                        if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "E":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "F":
                        if (i < 3)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "G":
                        if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 3)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "H":
                        if (stopInH)
                        {
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                        {
                            int length = 3;
                            if (hasLegalAdviser)
                                length = 4;
                            if (i < length)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        break;
                    case "I":
                        int length2 = 2;
                        if (hasLegalAdviser)
                            length2 = 3;
                        if (stopInH)
                        {
                            if (i < length2)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        }
                        else
                        {
                            if (i < length2)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i == length2 + 1)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        break;
                    case "J":
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        break;
                    case "K":
                        if (i < processDescriptionArray.Length - 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        break;
                }
                resultList.Add(auditLocusViewEntity);
            }
            return resultList;
        }

        //工程出图所用
        private static List<AuditLocusViewEntity> CreatePlotAuditLocusList(string[] processDescriptionArray, string[] auditUserNameArray, string[] auditDateArray, string auditStatus, bool hasLegalAdviser, bool stopInH)
        {
            List<AuditLocusViewEntity> resultList = new List<AuditLocusViewEntity>();

            TG.DAL.tg_member userDA = new TG.DAL.tg_member();

            for (int i = 0; i < processDescriptionArray.Length; i++)
            {
                AuditLocusViewEntity auditLocusViewEntity = new AuditLocusViewEntity
                {
                    ProcessDescription = processDescriptionArray[i]
                };
                if (auditDateArray == null || i > auditDateArray.Length - 1)
                {
                    auditLocusViewEntity.AuditDate = "未审核";
                    auditLocusViewEntity.AuditUserName = "未审核";
                    auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                }
                else
                {
                    auditLocusViewEntity.AuditDate = auditDateArray[i];
                    TG.Model.tg_member user = userDA.GetModel(int.Parse(auditUserNameArray[i]));

                    auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                    auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                }

                switch (auditStatus)
                {
                    case "A":
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "B":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "C":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "D":
                        if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "E":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "F":
                        if (i < 3)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "G":
                        if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 3)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "H":
                        if (i < 4)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "I":
                        if (i < 3)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 4)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;

                    case "J":
                        if (i < 5)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "K":
                        if (i < 4)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 5)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;

                    case "L":
                        if (i < 6)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "M":
                        if (i < 5)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 6)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "N":
                        if (i < 7)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "O":
                        if (i < 6)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 7)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "P":
                        if (i < 8)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "Q":
                        if (i < 7)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 8)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                }
                resultList.Add(auditLocusViewEntity);
            }
            return resultList;
        }

        //产值分配-二次产值分配
        private static List<AuditLocusViewEntity> CreateAuditLocusAllotList(string[] processDescriptionArray, string[] auditUserNameArray, string[] auditDateArray, string auditStatus, bool hasLegalAdviser, bool stopInH, TG.Model.cm_ProjectValueAuditRecord allotValueRecord)
        {
            List<AuditLocusViewEntity> resultList = new List<AuditLocusViewEntity>();

            TG.DAL.tg_member userDA = new TG.DAL.tg_member();
            int pro_id = allotValueRecord.ProSysNo;
            int? allotID = allotValueRecord.AllotID;
            for (int i = 0; i < processDescriptionArray.Length; i++)
            {
                AuditLocusViewEntity auditLocusViewEntity = new AuditLocusViewEntity
                {
                    ProcessDescription = processDescriptionArray[i]
                };
                if (auditDateArray == null || i > auditDateArray.Length - 1)
                {
                    if (i == 3)
                    {
                        //取得个人确认数据
                        string where = " proid=" + pro_id + " and allotID=" + allotID + " and TranType is null  ";
                        DataTable dt = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetListMemberStatus(where).Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            string auditUserString = "";
                            string auditDateString = "";
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                auditUserString = auditUserString + "<br/>" + dt.Rows[j]["mem_Name"].ToString();
                                if (dt.Rows[j]["Status"].ToString() == "S")
                                {
                                    auditDateString = auditDateString + "<br/>" + dt.Rows[j]["AuditDate"].ToString();
                                }
                                else if (dt.Rows[j]["Status"].ToString() == "D")
                                {
                                    auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                                }
                                else
                                {
                                    auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                                }
                            }
                            auditLocusViewEntity.AuditUserName = auditUserString;
                            auditLocusViewEntity.AuditDate = auditDateString;
                        }
                        else
                        {
                            auditLocusViewEntity.AuditDate = "未审核";
                            auditLocusViewEntity.AuditUserName = "未审核";
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                        }
                    }
                    else if (i > 3)
                    {
                        TG.Model.tg_member user = new TG.Model.tg_member();
                        if ((i - 1) < auditUserNameArray.Length && (i - 1) >= 0)
                        {
                            if (!auditUserNameArray[i - 1].Contains("{"))
                            {
                                auditLocusViewEntity.AuditDate = auditDateArray[i - 1];
                                user = userDA.GetModel(int.Parse(auditUserNameArray[i - 1]));
                                auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                            }
                        }
                        else
                        {
                            auditLocusViewEntity.AuditDate = "未审核";
                            auditLocusViewEntity.AuditUserName = "未审核";
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                        }
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                    }
                }
                else
                {
                    if (i == 3)
                    {
                        //取得个人确认数据
                        string where = " proid=" + pro_id + " and allotID=" + allotID + " and TranType is null  ";
                        DataTable dt = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetListMemberStatus(where).Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            string auditUserString = "";
                            string auditDateString = "";
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                auditUserString = auditUserString + "<br/>" + dt.Rows[j]["mem_Name"].ToString();
                                if (dt.Rows[j]["Status"].ToString() == "S")
                                {
                                    auditDateString = auditDateString + "<br/>" + dt.Rows[j]["AuditDate"].ToString();
                                }
                                else if (dt.Rows[j]["Status"].ToString() == "D")
                                {
                                    auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                                }
                                else
                                {
                                    auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                                }
                            }
                            auditLocusViewEntity.AuditUserName = auditUserString;
                            auditLocusViewEntity.AuditDate = auditDateString;
                        }
                        else
                        {
                            auditLocusViewEntity.AuditDate = "未审核";
                            auditLocusViewEntity.AuditUserName = "未审核";
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                        }
                    }
                    else if (i > 3)
                    {

                        TG.Model.tg_member user = new TG.Model.tg_member();
                        if ((i - 1) < auditUserNameArray.Length && (i - 1) >= 0)
                        {
                            if (!auditUserNameArray[i - 1].Contains("{"))
                            {
                                auditLocusViewEntity.AuditDate = auditDateArray[i - 1];
                                user = userDA.GetModel(int.Parse(auditUserNameArray[i - 1]));
                                auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                            }
                        }
                        else
                        {
                            auditLocusViewEntity.AuditDate = "未审核";
                            auditLocusViewEntity.AuditUserName = "未审核";
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                        }

                    }
                    else
                    {
                        TG.Model.tg_member user = new TG.Model.tg_member();

                        auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                        if (!auditUserNameArray[i].Contains("{"))
                        {
                            user = userDA.GetModel(int.Parse(auditUserNameArray[i]));
                            auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                            auditLocusViewEntity.AuditDate = auditDateArray[i];
                        }
                        else
                        {
                            //专业负责人单独处理
                            string userName = "";
                            string[] tempUser = auditUserNameArray[i].Replace("{", "").Replace("}", "").ToString().Split(';');
                            foreach (var temp in tempUser)
                            {
                                user = new TG.BLL.tg_member().GetModel(int.Parse(temp));
                                userName = string.IsNullOrEmpty(userName) ? user.mem_Name : userName + "<br/>" + user.mem_Name;
                            }
                            auditLocusViewEntity.AuditUserName = user == null ? "" : userName;
                            string[] auditDate = auditDateArray[i].Replace("{", "").Replace("}", "").ToString().Split(';');
                            string auditDateString = "";
                            foreach (var temp in auditDate)
                            {
                                auditDateString = auditDateString + "<br/>" + temp;
                            }
                            auditLocusViewEntity.AuditDate = auditDateString;
                        }
                        auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                    }

                }

                switch (auditStatus)
                {
                    case "A":
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "B":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "C":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "D":
                        if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "E":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "F":
                        if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "G":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "H":
                        if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "I":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "J":
                        if (i < 3)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        break;
                    case "K":
                        if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 3)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "L":
                        if (i < 4)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "M":
                        if (i < 3)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 4)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "N":
                        if (i < 5)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "O":
                        if (i < 4)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else if (i < 5)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        }
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "P":
                        if (i < 6)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "Q":
                        if (i < 5)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else if (i < 6)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        }
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                }
                resultList.Add(auditLocusViewEntity);
            }
            return resultList;
        }

        /// <summary>
        /// 根据ReferenceSysNo得到审核记录--项目产值分配
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="referenceSysNo"></param>
        /// <returns></returns>
        public static List<AuditLocusViewEntity> GetProjectAllotAuditRecordByReferenceSysNo<T>(int referenceSysNo, string isTrunEconomy, string isTranHave)
        {
            object sourceEntity = da.GetAuditRecordByReferenceSysNo<T>(referenceSysNo);

            List<AuditLocusViewEntity> resultList = new List<AuditLocusViewEntity>();

            if (sourceEntity is TG.Model.cm_ProjectValueAuditRecord)
            {
                TG.Model.cm_ProjectValueAuditRecord allotAuditEntity = (TG.Model.cm_ProjectValueAuditRecord)sourceEntity;

                string[] processDescriptionArray = da.GetProcessDescriptionArray("A");

                List<string> tempList = processDescriptionArray.ToList<string>();


                if (isTrunEconomy.Equals("0") && isTranHave.Equals("0"))
                {
                    tempList.RemoveAt(2);
                    tempList.RemoveAt(2);
                }
                else if (isTrunEconomy.Equals("0") && !isTranHave.Equals("0"))
                {
                    tempList.RemoveAt(2);
                }
                else if (!isTrunEconomy.Equals("0") && isTranHave.Equals("0"))
                {
                    tempList.RemoveAt(3);
                }

                processDescriptionArray = tempList.ToArray();

                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(allotAuditEntity.AuditUser))
                    auditUserNameArray = SplitString(allotAuditEntity.AuditUser, false, ',');

                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(allotAuditEntity.AuditDate))
                    auditDateArray = SplitString(allotAuditEntity.AuditDate, true, ',');

                resultList = CreateAuditLocusAllotLists(processDescriptionArray, auditUserNameArray, auditDateArray, allotAuditEntity.Status, isTrunEconomy, isTranHave, allotAuditEntity);

            }

            return resultList;
        }

        /// <summary>
        /// 项目产值分配用
        /// </summary>
        /// <param name="processDescriptionArray"></param>
        /// <param name="auditUserNameArray"></param>
        /// <param name="auditDateArray"></param>
        /// <param name="auditStatus"></param>
        /// <param name="isTrunEconomy"></param>
        /// <param name="isTranHave"></param>
        /// <returns></returns>
        private static List<AuditLocusViewEntity> CreateAuditLocusAllotLists(string[] processDescriptionArray, string[] auditUserNameArray, string[] auditDateArray, string auditStatus, string isTrunEconomy, string isTranHave, TG.Model.cm_ProjectValueAuditRecord allotValueRecord)
        {
            List<AuditLocusViewEntity> resultList = new List<AuditLocusViewEntity>();

            TG.DAL.tg_member userDA = new TG.DAL.tg_member();
            TG.Model.tg_member user = new TG.Model.tg_member();
            int length = 0;

            if (isTranHave == "1" && isTrunEconomy == "1")
            {
                length = 3;
            }
            else if (isTranHave == "0" && isTrunEconomy == "0")
            {
                length = 1;
            }
            else
            {
                length = 2;
            }

            for (int i = 0; i < processDescriptionArray.Length; i++)
            {

                AuditLocusViewEntity auditLocusViewEntity = new AuditLocusViewEntity
                {
                    ProcessDescription = processDescriptionArray[i]
                };

                if (auditDateArray == null || i > auditDateArray.Length + length - 1)
                {
                    if ((auditDateArray.Length + length) >= 6)
                    {
                        auditLocusViewEntity = GetAuditLocusAllotListsDeatil(auditLocusViewEntity, i, length, auditUserNameArray, auditDateArray, isTrunEconomy, isTranHave, allotValueRecord);
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                    }
                }
                else
                {
                    auditLocusViewEntity = GetAuditLocusAllotListsDeatil(auditLocusViewEntity, i, length, auditUserNameArray, auditDateArray, isTrunEconomy, isTranHave, allotValueRecord);

                    auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                }

                switch (auditStatus)
                {
                    case "A":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "B":
                    case "C":
                    //if (i < 2)
                    //    auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                    //else
                    //    auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                    //break;
                    case "D":
                    case "E":
                    case "F":
                    case "G":

                        if (isTrunEconomy.Equals("0") && isTranHave.Equals("0"))
                        {
                            if (i == 0)
                            {
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            if (i > 1)
                            {
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                            }
                        }
                        else if (isTrunEconomy.Equals("1") && isTranHave.Equals("1"))
                        {
                            if (i == 0)
                            {
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            if (i > 3)
                            {
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                            }
                        }
                        else
                        {
                            if (i == 0)
                            {
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            if (i > 2)
                            {
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                            }
                        }
                        break;

                    case "H":
                        if (i < 5)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";

                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        break;
                    case "I":
                        if (i < 4)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 5)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";

                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        break;
                    case "J":
                        if (isTrunEconomy.Equals("0") && isTranHave.Equals("0"))
                        {
                            if (i < 4)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else if (!isTrunEconomy.Equals("0") && !isTranHave.Equals("0"))
                        {
                            if (i < 6)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                            }
                        }
                        else
                        {
                            if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        break;
                    case "K":
                        if (isTrunEconomy.Equals("0") && isTranHave.Equals("0"))
                        {
                            if (i < 3)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 4)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else if (!isTrunEconomy.Equals("0") && !isTranHave.Equals("0"))
                        {
                            if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 6)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        else
                        {
                            if (i < 4)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        break;
                    case "L":
                        if (isTrunEconomy.Equals("0") && isTranHave.Equals("0"))
                        {
                            if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else if (!isTrunEconomy.Equals("0") && !isTranHave.Equals("0"))
                        {
                            if (i < 7)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        else
                        {
                            if (i < 6)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        break;
                    case "M":
                        if (isTrunEconomy.Equals("0") && isTranHave.Equals("0"))
                        {
                            if (i < 4)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else if (!isTrunEconomy.Equals("0") && !isTranHave.Equals("0"))
                        {
                            if (i < 6)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 7)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        else
                        {
                            if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 4)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        break;
                    case "N":
                        if (isTrunEconomy.Equals("0") && isTranHave.Equals("0"))
                        {
                            if (i < 6)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else if (!isTrunEconomy.Equals("0") && !isTranHave.Equals("0"))
                        {
                            if (i < 8)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            else
                            {
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                            }
                        }
                        else
                        {
                            if (i < 7)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        break;
                    case "O":
                        if (isTrunEconomy.Equals("0") && isTranHave.Equals("0"))
                        {
                            if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 6)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else if (!isTrunEconomy.Equals("0") && !isTranHave.Equals("0"))
                        {
                            if (i < 7)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.png\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            else if (i < 8)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            }
                            else
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                            }
                        }
                        else
                        {
                            if (i < 6)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 7)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        break;
                    case "P":
                        if (isTrunEconomy.Equals("0") && isTranHave.Equals("0"))
                        {
                            if (i < 7)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else if (!isTrunEconomy.Equals("0") && !isTranHave.Equals("0"))
                        {
                            if (i < 9)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else
                        {
                            if (i < 8)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        break;
                    case "Q":
                        if (isTrunEconomy.Equals("0") && isTranHave.Equals("0"))
                        {
                            if (i < 6)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 7)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else if (!isTrunEconomy.Equals("0") && !isTranHave.Equals("0"))
                        {
                            if (i < 8)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 9)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            }
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else
                        {
                            if (i < 7)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 8)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        break;

                }
                resultList.Add(auditLocusViewEntity);
            }
            return resultList;
        }


        private static AuditLocusViewEntity GetAuditLocusAllotListsDeatil(AuditLocusViewEntity auditLocusViewEntity, int i, int length, string[] auditUserNameArray, string[] auditDateArray, string isTrunEconomy, string isTranHave, TG.Model.cm_ProjectValueAuditRecord allotValueRecord)
        {
            TG.DAL.tg_member userDA = new TG.DAL.tg_member();
            TG.Model.tg_member user = new TG.Model.tg_member();

            if (i == 0)
            {
                auditLocusViewEntity.AuditDate = auditDateArray[0];
                user = userDA.GetModel(int.Parse(auditUserNameArray[0]));
                auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
            }
            if (i == 1)
            {
                if (!string.IsNullOrEmpty(allotValueRecord.TwoSuggstion))
                {
                    auditLocusViewEntity.AuditDate = allotValueRecord.TwoAuditDate.ToString();
                    user = userDA.GetModel(allotValueRecord.TwoAuditUser);
                    auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                    if (allotValueRecord.TwoIsPass.Equals("1"))
                    {
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                    }
                    else
                    {
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                    }
                }
                else
                {
                    auditLocusViewEntity.AuditDate = "未审核";
                    auditLocusViewEntity.AuditUserName = "未审核";
                    auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                }
            }
            if (i == 2) //经济所 
            {
                if (isTranHave == "0" && isTrunEconomy == "0")
                {
                    auditLocusViewEntity.AuditDate = auditDateArray[1];
                    user = userDA.GetModel(int.Parse(auditUserNameArray[1]));
                    auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                }
                else if (isTrunEconomy == "1")
                {
                    if (!string.IsNullOrEmpty(allotValueRecord.ThreeSuggsion))
                    {
                        auditLocusViewEntity.AuditDate = allotValueRecord.ThreeAuditDate.ToString();
                        user = userDA.GetModel(allotValueRecord.ThreeAuditUser);
                        auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                        if (allotValueRecord.ThreeIsPass.Equals("1"))
                        {
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                        {
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        }
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                    }
                }
                else
                { //暖通所
                    if (!string.IsNullOrEmpty(allotValueRecord.FourSuggion))
                    {
                        auditLocusViewEntity.AuditDate = allotValueRecord.FourAuditDate.ToString();
                        user = userDA.GetModel(allotValueRecord.FourAuditUser);
                        auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                        if (allotValueRecord.FourIsPass.Equals("1"))
                        {
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                        {
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        }
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                    }
                }

            }

            if (i == 3) //暖通所
            {
                if (isTranHave == "0" && isTrunEconomy == "0")
                {
                    if (auditUserNameArray[3].Contains("{"))
                    {
                        string userName = "";
                        string[] tempUser = auditUserNameArray[3].Replace("{", "").Replace("}", "").ToString().Split(';');
                        foreach (var temp in tempUser)
                        {
                            user = new TG.BLL.tg_member().GetModel(int.Parse(temp));
                            userName = string.IsNullOrEmpty(userName) ? user.mem_Name : userName + "<br/>" + user.mem_Name;
                        }
                        auditLocusViewEntity.AuditUserName = user == null ? "" : userName;
                        string[] auditDate = auditDateArray[3].Replace("{", "").Replace("}", "").ToString().Split(';');
                        string auditDateString = "";
                        foreach (var temp in auditDate)
                        {
                            auditDateString = auditDateString + "<br/>" + temp;
                        }
                        auditLocusViewEntity.AuditDate = auditDateString;
                    }
                }
                else if (isTranHave == "1" && isTrunEconomy == "1")
                {
                    //暖通所
                    if (!string.IsNullOrEmpty(allotValueRecord.FourSuggion))
                    {
                        auditLocusViewEntity.AuditDate = allotValueRecord.FourAuditDate.ToString();
                        user = userDA.GetModel(allotValueRecord.FourAuditUser);
                        auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                        if (allotValueRecord.FourIsPass.Equals("1"))
                        {
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                        {
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        }
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                    }
                }
                else
                { //设总
                    auditLocusViewEntity.AuditDate = auditDateArray[2];
                    user = userDA.GetModel(int.Parse(auditUserNameArray[2]));
                    auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                }
            }


            if (i > 3)
            {
                if (i == 6)
                {
                    //取得个人确认数据
                    int pro_id = allotValueRecord.ProSysNo;
                    int? allotID = allotValueRecord.AllotID;
                    string where = " proid=" + pro_id + " and allotID=" + allotID + " and TranType is null  ";
                    DataTable dt = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetListMemberStatus(where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        string auditUserString = "";
                        string auditDateString = "";
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            auditUserString = auditUserString + "<br/>" + dt.Rows[j]["mem_Name"].ToString();
                            if (dt.Rows[j]["Status"].ToString() == "S")
                            {
                                auditDateString = auditDateString + "<br/>" + dt.Rows[j]["AuditDate"].ToString();
                            }
                            else if (dt.Rows[j]["Status"].ToString() == "D")
                            {
                                auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            }
                            else
                            {
                                auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                            }
                        }
                        auditLocusViewEntity.AuditUserName = auditUserString;
                        auditLocusViewEntity.AuditDate = auditDateString;
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                    }
                }
                else if (i >= 7)
                {
                    if ((i - length - 1) < auditUserNameArray.Length && (i - length - 1) >= 0)
                    {
                        if (!auditUserNameArray[i - length - 1].Contains("{"))
                        {
                            auditLocusViewEntity.AuditDate = auditDateArray[i - length - 1];
                            user = userDA.GetModel(int.Parse(auditUserNameArray[i - length - 1]));
                            auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                        }
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                    }
                }
                else
                {
                    //不包含"{" --说明不是专业负责人审核
                    if (!auditUserNameArray[i - length].Contains("{"))
                    {
                        auditLocusViewEntity.AuditDate = auditDateArray[i - length];
                        user = userDA.GetModel(int.Parse(auditUserNameArray[i - length]));
                        auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                    }
                    else
                    {
                        //专业负责人单独处理
                        string userName = "";
                        string[] tempUser = auditUserNameArray[2].Replace("{", "").Replace("}", "").ToString().Split(';');
                        foreach (var temp in tempUser)
                        {
                            user = new TG.BLL.tg_member().GetModel(int.Parse(temp));
                            userName = string.IsNullOrEmpty(userName) ? user.mem_Name : userName + "<br/>" + user.mem_Name;
                        }
                        auditLocusViewEntity.AuditUserName = user == null ? "" : userName;
                        string[] auditDate = auditDateArray[2].Replace("{", "").Replace("}", "").ToString().Split(';');
                        string auditDateString = "";
                        foreach (var temp in auditDate)
                        {
                            auditDateString = auditDateString + "<br/>" + temp;
                        }
                        auditLocusViewEntity.AuditDate = auditDateString;
                    }
                }
            }
            return auditLocusViewEntity;
        }


        /// <summary>
        /// 根据ReferenceSysNo得到审核记录--项目产值分配--暖通所项目
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="referenceSysNo"></param>
        /// <returns></returns>
        public static List<AuditLocusViewEntity> GetHavcProjectAllotAuditRecordByReferenceSysNo<T>(int referenceSysNo, string isTrunEconomy)
        {
            object sourceEntity = da.GetAuditRecordByReferenceSysNo<T>(referenceSysNo);

            List<AuditLocusViewEntity> resultList = new List<AuditLocusViewEntity>();

            if (sourceEntity is TG.Model.cm_ProjectValueAuditRecord)
            {
                TG.Model.cm_ProjectValueAuditRecord allotAuditEntity = (TG.Model.cm_ProjectValueAuditRecord)sourceEntity;

                string[] processDescriptionArray = da.GetProcessDescriptionArray("A");

                List<string> tempList = processDescriptionArray.ToList<string>();
                TG.Model.cm_Project model = new TG.BLL.cm_Project().GetModel(allotAuditEntity.ProSysNo);
                if (model != null)
                {
                    if (model.Unit.Contains("暖通"))
                    {
                        //删掉暖通
                        tempList.RemoveAt(3);
                        if (isTrunEconomy.Equals("0"))
                        {
                            tempList.RemoveAt(2);
                        }
                    }
                }

                processDescriptionArray = tempList.ToArray();

                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(allotAuditEntity.AuditUser))
                    auditUserNameArray = SplitString(allotAuditEntity.AuditUser, false, ',');

                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(allotAuditEntity.AuditDate))
                    auditDateArray = SplitString(allotAuditEntity.AuditDate, true, ',');

                resultList = CreateAuditLocusAllotListsByHavc(processDescriptionArray, auditUserNameArray, auditDateArray, allotAuditEntity.Status, isTrunEconomy, allotAuditEntity);

            }

            return resultList;
        }

        /// <summary>
        /// 项目产值分配用 --暖通所项目
        /// </summary>
        /// <param name="processDescriptionArray"></param>
        /// <param name="auditUserNameArray"></param>
        /// <param name="auditDateArray"></param>
        /// <param name="auditStatus"></param>
        /// <param name="isTrunEconomy"></param>
        /// <param name="isTranHave"></param>
        /// <returns></returns>
        private static List<AuditLocusViewEntity>
            CreateAuditLocusAllotListsByHavc(string[] processDescriptionArray, string[] auditUserNameArray, string[] auditDateArray, string auditStatus, string isTrunEconomy, TG.Model.cm_ProjectValueAuditRecord allotValueRecord)
        {
            List<AuditLocusViewEntity> resultList = new List<AuditLocusViewEntity>();

            TG.DAL.tg_member userDA = new TG.DAL.tg_member();
            TG.Model.tg_member user = new TG.Model.tg_member();
            int length = 0;

            if (isTrunEconomy == "0")
            {
                length = 1;
            }
            else
            {
                length = 2;
            }

            for (int i = 0; i < processDescriptionArray.Length; i++)
            {

                AuditLocusViewEntity auditLocusViewEntity = new AuditLocusViewEntity
                {
                    ProcessDescription = processDescriptionArray[i]
                };

                if (auditDateArray == null || i > auditDateArray.Length + length - 1)
                {
                    if ((auditDateArray.Length + length) >= 5)
                    {
                        auditLocusViewEntity = GetAuditLocusAllotListsDeatilByHavc(auditLocusViewEntity, i, length, auditUserNameArray, auditDateArray, isTrunEconomy, allotValueRecord);
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                    }
                }
                else
                {
                    auditLocusViewEntity = GetAuditLocusAllotListsDeatilByHavc(auditLocusViewEntity, i, length, auditUserNameArray, auditDateArray, isTrunEconomy, allotValueRecord);

                    auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                }

                switch (auditStatus)
                {
                    case "A":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "B":
                    case "C":

                    case "D":
                    case "E":
                    case "F":
                    case "G":
                        if (isTrunEconomy.Equals("0"))
                        {
                            if (i == 0)
                            {
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            if (i > 1)
                            {
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                            }
                        }
                        else
                        {
                            if (i == 0)
                            {
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            if (i > 2)
                            {
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                            }
                        }
                        break;
                    case "H":
                        if (i < 4)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        break;
                    case "I":
                        if (i < 3)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 4)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        break;
                    case "J":
                        if (isTrunEconomy.Equals("0"))
                        {
                            if (i < 4)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else
                        {
                            if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                            }
                        }
                        break;
                    case "K":
                        if (isTrunEconomy.Equals("0"))
                        {
                            if (i < 3)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 4)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else
                        {
                            if (i < 4)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        break;
                    case "L":
                        if (isTrunEconomy.Equals("0"))
                        {
                            if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }

                        else
                        {
                            if (i < 6)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        break;
                    case "M":
                        if (isTrunEconomy.Equals("0"))
                        {
                            if (i < 4)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else
                        {
                            if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 6)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        break;
                    case "N":
                        if (isTrunEconomy.Equals("0"))
                        {
                            if (i < 6)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }

                        else
                        {
                            if (i < 7)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        break;
                    case "O":
                        if (isTrunEconomy.Equals("0"))
                        {
                            if (i < 5)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 6)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else
                        {
                            if (i < 6)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            else if (i < 7)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            }
                            else
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                            }
                        }
                        break;
                    case "P":
                        if (isTrunEconomy.Equals("0"))
                        {
                            if (i < 7)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else
                        {
                            if (i < 8)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            }
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        break;
                    case "Q":
                        if (isTrunEconomy.Equals("0"))
                        {
                            if (i < 6)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 7)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            else
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        else
                        {
                            if (i < 7)
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                            else if (i < 8)
                            {
                                auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            }
                            else

                                auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";

                        }
                        break;
                }
                resultList.Add(auditLocusViewEntity);
            }
            return resultList;
        }

        /// <summary>
        /// 暖通所项目明细
        /// </summary>
        /// <param name="auditLocusViewEntity"></param>
        /// <param name="i"></param>
        /// <param name="length"></param>
        /// <param name="auditUserNameArray"></param>
        /// <param name="auditDateArray"></param>
        /// <param name="isTrunEconomy"></param>
        /// <param name="allotValueRecord"></param>
        /// <returns></returns>
        private static AuditLocusViewEntity GetAuditLocusAllotListsDeatilByHavc(AuditLocusViewEntity auditLocusViewEntity, int i, int length, string[] auditUserNameArray, string[] auditDateArray, string isTrunEconomy, TG.Model.cm_ProjectValueAuditRecord allotValueRecord)
        {
            TG.DAL.tg_member userDA = new TG.DAL.tg_member();
            TG.Model.tg_member user = new TG.Model.tg_member();

            if (i == 0)
            {
                auditLocusViewEntity.AuditDate = auditDateArray[0];
                user = userDA.GetModel(int.Parse(auditUserNameArray[0]));
                auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
            }
            if (i == 1)
            {
                if (!string.IsNullOrEmpty(allotValueRecord.TwoSuggstion))
                {
                    auditLocusViewEntity.AuditDate = allotValueRecord.TwoAuditDate.ToString();
                    user = userDA.GetModel(allotValueRecord.TwoAuditUser);
                    auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                    if (allotValueRecord.TwoIsPass.Equals("1"))
                    {
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                    }
                    else
                    {
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                    }
                }
                else
                {
                    auditLocusViewEntity.AuditDate = "未审核";
                    auditLocusViewEntity.AuditUserName = "未审核";
                    auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                }
            }
            if (i == 2) //经济所 
            {
                if (isTrunEconomy == "1")
                {
                    if (!string.IsNullOrEmpty(allotValueRecord.ThreeSuggsion))
                    {
                        auditLocusViewEntity.AuditDate = allotValueRecord.ThreeAuditDate.ToString();
                        user = userDA.GetModel(allotValueRecord.ThreeAuditUser);
                        auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                        if (allotValueRecord.ThreeIsPass.Equals("1"))
                        {
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                        {
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        }
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                    }
                }
                else
                {
                    //设总
                    auditLocusViewEntity.AuditDate = auditDateArray[2];
                    user = userDA.GetModel(int.Parse(auditUserNameArray[2]));
                    auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                }
            }

            if (i > 2)
            {
                if (i == 5)
                {
                    //取得个人确认数据
                    int pro_id = allotValueRecord.ProSysNo;
                    int? allotID = allotValueRecord.AllotID;
                    string where = " proid=" + pro_id + " and allotID=" + allotID + " and TranType is null  ";
                    DataTable dt = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetListMemberStatus(where).Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        string auditUserString = "";
                        string auditDateString = "";
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            auditUserString = auditUserString + "<br/>" + dt.Rows[j]["mem_Name"].ToString();
                            if (dt.Rows[j]["Status"].ToString() == "S")
                            {
                                auditDateString = auditDateString + "<br/>" + dt.Rows[j]["AuditDate"].ToString();
                            }
                            else if (dt.Rows[j]["Status"].ToString() == "D")
                            {
                                auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                            }
                            else
                            {
                                auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                            }
                        }
                        auditLocusViewEntity.AuditUserName = auditUserString;
                        auditLocusViewEntity.AuditDate = auditDateString;
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                    }
                }
                else if (i >= 6)
                {
                    if ((i - length - 1) < auditUserNameArray.Length && (i - length - 1) >= 0)
                    {
                        if (!auditUserNameArray[i - length - 1].Contains("{"))
                        {
                            auditLocusViewEntity.AuditDate = auditDateArray[i - length - 1];
                            user = userDA.GetModel(int.Parse(auditUserNameArray[i - length - 1]));
                            auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                        }
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                    }
                }
                else
                {
                    //不包含"{" --说明不是专业负责人审核
                    if (!auditUserNameArray[i - length].Contains("{"))
                    {
                        auditLocusViewEntity.AuditDate = auditDateArray[i - length];
                        user = userDA.GetModel(int.Parse(auditUserNameArray[i - length]));
                        auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                    }
                    else
                    {
                        //专业负责人单独处理
                        string userName = "";
                        string[] tempUser = auditUserNameArray[2].Replace("{", "").Replace("}", "").ToString().Split(';');
                        foreach (var temp in tempUser)
                        {
                            user = new TG.BLL.tg_member().GetModel(int.Parse(temp));
                            userName = string.IsNullOrEmpty(userName) ? user.mem_Name : userName + "<br/>" + user.mem_Name;
                        }
                        auditLocusViewEntity.AuditUserName = user == null ? "" : userName;
                        string[] auditDate = auditDateArray[2].Replace("{", "").Replace("}", "").ToString().Split(';');
                        string auditDateString = "";
                        foreach (var temp in auditDate)
                        {
                            auditDateString = auditDateString + "<br/>" + temp;
                        }
                        auditLocusViewEntity.AuditDate = auditDateString;
                    }
                }
            }
            return auditLocusViewEntity;
        }

        /// <summary>
        /// 经济所产值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="referenceSysNo"></param>
        /// <returns></returns>
        public static List<AuditLocusViewEntity> GetJjsProjectAllotAuditRecordByReferenceSysNo<T>(int referenceSysNo)
        {
            object sourceEntity = da.GetAuditRecordByReferenceSysNo<T>(referenceSysNo);
            List<AuditLocusViewEntity> resultList = new List<AuditLocusViewEntity>();
            if (sourceEntity is TG.Model.cm_ProjectValueAuditRecord)
            {
                TG.Model.cm_ProjectValueAuditRecord allotAuditEntity = (TG.Model.cm_ProjectValueAuditRecord)sourceEntity;
                string[] processDescriptionArray = da.GetProcessDescriptionArray("JJS");
                List<string> tempList = processDescriptionArray.ToList<string>();
                processDescriptionArray = tempList.ToArray();
                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(allotAuditEntity.AuditUser))
                    auditUserNameArray = SplitString(allotAuditEntity.AuditUser, false, ',');
                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(allotAuditEntity.AuditDate))
                    auditDateArray = SplitString(allotAuditEntity.AuditDate, true, ',');
                resultList = CreateAuditLocusAllotListsByJjs(processDescriptionArray, auditUserNameArray, auditDateArray, allotAuditEntity.Status, allotAuditEntity);
            }
            return resultList;
        }
        private static List<AuditLocusViewEntity>
            CreateAuditLocusAllotListsByJjs(string[] processDescriptionArray, string[] auditUserNameArray, string[] auditDateArray, string auditStatus, TG.Model.cm_ProjectValueAuditRecord allotValueRecord)
        {
            List<AuditLocusViewEntity> resultList = new List<AuditLocusViewEntity>();
            TG.DAL.tg_member userDA = new TG.DAL.tg_member();
            for (int i = 0; i < processDescriptionArray.Length; i++)
            {
                AuditLocusViewEntity auditLocusViewEntity = new AuditLocusViewEntity
                {
                    ProcessDescription = processDescriptionArray[i]
                };
                if (auditDateArray == null || i > auditDateArray.Length - 1)
                {

                    if (i == 2)
                    {
                        //取得个人确认数据
                        string where = " proid=" + allotValueRecord.ProSysNo + " and allotID=" + allotValueRecord.AllotID + " and TranType is null ";
                        DataTable dt = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetListMemberStatus(where).Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            string auditUserString = "";
                            string auditDateString = "";
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                auditUserString = auditUserString + "<br/>" + dt.Rows[j]["mem_Name"].ToString();
                                if (dt.Rows[j]["Status"].ToString() == "S")
                                {
                                    auditDateString = auditDateString + "<br/>" + dt.Rows[j]["AuditDate"].ToString();
                                }
                                else if (dt.Rows[j]["Status"].ToString() == "D")
                                {
                                    auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                                }
                                else
                                {
                                    auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                                }
                            }
                            auditLocusViewEntity.AuditUserName = auditUserString;
                            auditLocusViewEntity.AuditDate = auditDateString;
                        }
                        else
                        {
                            auditLocusViewEntity.AuditDate = "未审核";
                            auditLocusViewEntity.AuditUserName = "未审核";
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                        }
                    }
                    else if (i > 2)
                    {
                        TG.Model.tg_member user = new TG.Model.tg_member();
                        if ((i - 1) < auditUserNameArray.Length && (i - 1) >= 0)
                        {
                            auditLocusViewEntity.AuditDate = auditDateArray[i - 1];
                            user = userDA.GetModel(int.Parse(auditUserNameArray[i - 1]));
                            auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                        }
                        else
                        {
                            auditLocusViewEntity.AuditDate = "未审核";
                            auditLocusViewEntity.AuditUserName = "未审核";
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                        }
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                    }
                }
                else
                {
                    if (i == 2)
                    {
                        //取得个人确认数据
                        string where = " proid=" + allotValueRecord.ProSysNo + " and allotID=" + allotValueRecord.AllotID + " and trantype is null";
                        DataTable dt = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetListMemberStatus(where).Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            string auditUserString = "";
                            string auditDateString = "";
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                auditUserString = auditUserString + "<br/>" + dt.Rows[j]["mem_Name"].ToString();
                                if (dt.Rows[j]["Status"].ToString() == "S")
                                {
                                    auditDateString = auditDateString + "<br/>" + dt.Rows[j]["AuditDate"].ToString();
                                }
                                else if (dt.Rows[j]["Status"].ToString() == "D")
                                {
                                    auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                                }
                                else
                                {
                                    auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                                }
                            }
                            auditLocusViewEntity.AuditUserName = auditUserString;
                            auditLocusViewEntity.AuditDate = auditDateString;
                        }
                        else
                        {
                            auditLocusViewEntity.AuditDate = "未审核";
                            auditLocusViewEntity.AuditUserName = "未审核";
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                        }
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = auditDateArray[i];
                        TG.Model.tg_member user = userDA.GetModel(int.Parse(auditUserNameArray[i]));
                        auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                        auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                    }
                }
                switch (auditStatus)
                {
                    case "A":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "B":
                        if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        break;
                    case "C":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "D":
                        if (i < 3)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "E":
                        if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 3)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "F":
                        if (i < 4)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "G":
                        if (i < 3)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else if (i < 4)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        }
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                }
                resultList.Add(auditLocusViewEntity);
            }
            return resultList;
        }

        /// <summary>
        /// 转经济所产值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="referenceSysNo"></param>
        /// <returns></returns>
        public static List<AuditLocusViewEntity> GetTranJjsProjectAllotAuditRecordByReferenceSysNo<T>(int referenceSysNo)
        {
            object sourceEntity = da.GetAuditRecordByReferenceSysNo<T>(referenceSysNo);
            List<AuditLocusViewEntity> resultList = new List<AuditLocusViewEntity>();
            if (sourceEntity is TG.Model.cm_TranProjectValueAuditRecord)
            {
                TG.Model.cm_TranProjectValueAuditRecord allotAuditEntity = (TG.Model.cm_TranProjectValueAuditRecord)sourceEntity;
                string[] processDescriptionArray = da.GetProcessDescriptionArray("TranJJS");
                List<string> tempList = processDescriptionArray.ToList<string>();
                processDescriptionArray = tempList.ToArray();
                string[] auditUserNameArray = null;
                if (!string.IsNullOrEmpty(allotAuditEntity.AuditUser))
                    auditUserNameArray = SplitString(allotAuditEntity.AuditUser, false, ',');
                string[] auditDateArray = null;
                if (!string.IsNullOrEmpty(allotAuditEntity.AuditDate))
                    auditDateArray = SplitString(allotAuditEntity.AuditDate, true, ',');
                resultList = CreateAuditLocusAllotListsByTranJjs(processDescriptionArray, auditUserNameArray, auditDateArray, allotAuditEntity.Status, allotAuditEntity);
            }
            return resultList;
        }

        private static List<AuditLocusViewEntity>
         CreateAuditLocusAllotListsByTranJjs(string[] processDescriptionArray, string[] auditUserNameArray, string[] auditDateArray, string auditStatus, TG.Model.cm_TranProjectValueAuditRecord allotValueRecord)
        {
            List<AuditLocusViewEntity> resultList = new List<AuditLocusViewEntity>();
            TG.DAL.tg_member userDA = new TG.DAL.tg_member();

            int length = 0;
            if (allotValueRecord.InUser != null)
            {
                length = length + 1;
            }

            if (allotValueRecord.AuditUser != null)
            {
                length = length + 1;
            }
            string tranType = allotValueRecord.ItemType.Trim();
            //取得个人确认数据
            string where = " proid=" + allotValueRecord.ProSysNo + " and allotID=" + allotValueRecord.AllotID + " and TranType='" + tranType + "'";
            DataTable dt = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetListMemberStatus(where).Tables[0];

            if (dt.Rows.Count > 0)
            {
                length = length + 1;
            }

            for (int i = 0; i < processDescriptionArray.Length; i++)
            {
                AuditLocusViewEntity auditLocusViewEntity = new AuditLocusViewEntity
                {
                    ProcessDescription = processDescriptionArray[i]
                };

                if (i > length - 1)
                {

                    auditLocusViewEntity.AuditDate = "未审核";
                    auditLocusViewEntity.AuditUserName = "未审核";
                    auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";

                }
                else
                {

                    if (i == 0)
                    {
                        auditLocusViewEntity.AuditDate = allotValueRecord.InDate.ToString();
                        TG.Model.tg_member user = userDA.GetModel(int.Parse(allotValueRecord.InUser.ToString()));
                        auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                        auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";

                    }
                    else if (i == 1)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            string auditUserString = "";
                            string auditDateString = "";
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                auditUserString = auditUserString + "<br/>" + dt.Rows[j]["mem_Name"].ToString();
                                if (dt.Rows[j]["Status"].ToString() == "S")
                                {
                                    auditDateString = auditDateString + "<br/>" + dt.Rows[j]["AuditDate"].ToString();
                                }
                                else if (dt.Rows[j]["Status"].ToString() == "D")
                                {
                                    auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                                }
                                else
                                {
                                    auditDateString = auditDateString + "<br/>" + "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                                }
                            }
                            auditLocusViewEntity.AuditUserName = auditUserString;
                            auditLocusViewEntity.AuditDate = auditDateString;
                        }
                        else
                        {
                            auditLocusViewEntity.AuditDate = "未审核";
                            auditLocusViewEntity.AuditUserName = "未审核";
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                        }
                    }
                    else
                    {
                        auditLocusViewEntity.AuditDate = allotValueRecord.AuditDate;
                        if (allotValueRecord.AuditUser != null)
                        {
                            TG.Model.tg_member user = userDA.GetModel(int.Parse(allotValueRecord.AuditUser));
                            auditLocusViewEntity.AuditUserName = user == null ? "" : user.mem_Name;
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                        }
                    }

                }
                switch (auditStatus)
                {
                    case "A":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else
                        {

                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        break;
                    case "B":
                        if (i < 2)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                        {
                            auditLocusViewEntity.AuditDate = "未审核";
                            auditLocusViewEntity.AuditUserName = "未审核";
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        break;
                    case "C":
                        if (i < 1)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        else if (i < 2)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.AuditDate = "未审核";
                        auditLocusViewEntity.AuditUserName = "未审核";
                        auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                        auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "D":
                        if (i < 3)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        }
                        break;
                    case "E":
                        if (i < 2)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else if (i < 3)
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "F":
                        if (i < 4)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                    case "G":
                        if (i < 3)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/yes.png\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_yes.png\" style=\"width:16px;height:16px;\" alt=\"通过\">";
                        }
                        else if (i < 4)
                        {
                            auditLocusViewEntity.AuditStatus = "<img src=\"/Images/status/no.gif\" style=\"width:16px;height:16px;\">";
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_no.png\" style=\"width:16px;height:16px;\" alt=\"不通过\">";
                        }
                        else
                            auditLocusViewEntity.IsPass = "<img src=\"/Images/status/pass_precess.png\" style=\"width:16px;height:16px;\" alt=\"未审批\">";
                        break;
                }
                resultList.Add(auditLocusViewEntity);
            }
            return resultList;
        }

        public static string[] GetProcessDescriptionArray(string Action)
        {
            return da.GetProcessDescriptionArray(Action);
        }
        public static string GetNextmember(string states, string auditno, string type)
        {
            return da.GetNextmember(states, auditno, type);
        }
    }
}
