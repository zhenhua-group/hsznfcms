﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public class cpr_AcountAndIndustry
    {
        TG.DAL.cpr_AcountAndIndustry dal = new DAL.cpr_AcountAndIndustry();
        /// <summary>
        /// 得到所有合同额 
        /// </summary>
        /// <param name="unitId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="endmonth"></param>
        /// <returns></returns>
        public List<TG.Model.cpr_AcountAndIndustry> GetList(string unitId, string year, string beginTime, string endTime)
        {
            List<TG.Model.cpr_AcountAndIndustry> listAll = new List<Model.cpr_AcountAndIndustry>();
            if (unitId == "-1")
            {
                List<TG.Model.cpr_AcountAndIndustry> listFrist;
                TG.Model.cpr_AcountAndIndustry modelfrist;
                ZhuUnit(year, beginTime, endTime, out listFrist, out modelfrist);


                List<TG.Model.cpr_AcountAndIndustry> listSecond;
                TG.Model.cpr_AcountAndIndustry modelsecond;
                ShiUnit(year, beginTime, endTime, out listSecond, out modelsecond);

                TG.Model.cpr_AcountAndIndustry modelAll = Getzongji(modelfrist, modelsecond);
                listAll.AddRange(listFrist);
                listAll.AddRange(listSecond);
                listAll.Add(modelAll);
            }
            else
            {
                unitId = " t.unit_ID=" + unitId;
                listAll = dal.GetListAcountIndustry(unitId, year, beginTime, endTime);
            }
            return listAll;
        }
        /// <summary>
        /// 施工合计
        /// </summary>
        /// <param name="year">年份</param>
        /// <param name="beginTime">开始月份</param>
        /// <param name="endTime"></param>
        /// <param name="listSecond"></param>
        /// <param name="modelsecond"></param>
        public void ShiUnit( string year, string beginTime, string endTime, out List<TG.Model.cpr_AcountAndIndustry> listSecond, out TG.Model.cpr_AcountAndIndustry modelsecond)
        {
            listSecond = dal.GetListAcountIndustry("t.unit_ParentID=254", year, beginTime, endTime);
            modelsecond = GetHeji(listSecond, "施工合计");
            listSecond.Add(modelsecond);
        }
        /// <summary>
        /// 主要经营合计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="listFrist"></param>
        /// <param name="modelfrist"></param>
        public void ZhuUnit(string year, string beginTime, string endTime, out List<TG.Model.cpr_AcountAndIndustry> listFrist, out TG.Model.cpr_AcountAndIndustry modelfrist)
        {
            listFrist = dal.GetListAcountIndustry("t.unit_ParentID=238", year, beginTime, endTime);
            modelfrist = GetHeji(listFrist, "主营合计");
            listFrist.Add(modelfrist);
        }
        /// <summary>
        /// 合同额
        /// </summary>
        /// <param name="list"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public TG.Model.cpr_AcountAndIndustry GetHeji(List<TG.Model.cpr_AcountAndIndustry> list, string str)
        {
            TG.Model.cpr_AcountAndIndustry model = new Model.cpr_AcountAndIndustry();
            model.Unit = str;
            foreach (TG.Model.cpr_AcountAndIndustry item in list)
            {
                model.LastPublicBuild += Convert.ToInt32(item.LastPublicBuild);
                model.LastComprehensive += Convert.ToDecimal(item.LastComprehensive.ToString().Trim());
                model.LastAffordHouse += Convert.ToDecimal(item.LastAffordHouse.ToString().Trim());
                model.LastEstate += Convert.ToDecimal(item.LastEstate.ToString().Trim());
                model.LastTotal += Convert.ToInt32(Convert.ToDecimal(item.LastTotal.ToString().Trim()));
                model.NowTotal += Convert.ToInt32(item.NowTotal);
                model.NowPublicBuild += Convert.ToDecimal(item.NowPublicBuild.ToString().Trim());
                model.NowEstate += Convert.ToDecimal(item.NowEstate.ToString().Trim());
                model.NowComprehensive += Convert.ToDecimal(item.NowComprehensive.ToString().Trim());
                model.NowAffordHouse += Convert.ToInt32(Convert.ToDecimal(item.NowAffordHouse.ToString().Trim()));
            }
            return model;
        }
        /// <summary>
        /// 总计合同额
        /// </summary>
        /// <param name="listfrist"></param>
        /// <param name="listSecond"></param>
        /// <returns></returns>
        public TG.Model.cpr_AcountAndIndustry Getzongji(TG.Model.cpr_AcountAndIndustry listfrist, TG.Model.cpr_AcountAndIndustry listSecond)
        {
            TG.Model.cpr_AcountAndIndustry Zongji = new Model.cpr_AcountAndIndustry();
            Zongji.Unit = "全院总合计";
            Zongji.LastPublicBuild = listfrist.LastPublicBuild + listSecond.LastPublicBuild;
            Zongji.LastComprehensive = listfrist.LastComprehensive + listSecond.LastComprehensive;
            Zongji.LastAffordHouse = listfrist.LastAffordHouse + listSecond.LastAffordHouse;
            Zongji.LastEstate = listfrist.LastEstate + listSecond.LastEstate;
            Zongji.LastTotal = listfrist.LastTotal + listSecond.LastTotal;
            Zongji.NowTotal = listfrist.NowTotal + listSecond.NowTotal;
            Zongji.NowPublicBuild = listfrist.NowPublicBuild + listSecond.NowPublicBuild;
            Zongji.NowEstate = listfrist.NowEstate + listSecond.NowEstate;
            Zongji.NowComprehensive = listfrist.NowComprehensive + listSecond.NowComprehensive;
            Zongji.NowAffordHouse = listfrist.NowAffordHouse + listSecond.NowAffordHouse;

            return Zongji;
        }
    }
}
