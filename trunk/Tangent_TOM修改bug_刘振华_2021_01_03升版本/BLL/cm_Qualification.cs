﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.BLL
{
    public class cm_Qualification
    {
        TG.DAL.cm_Qualification quadal = new DAL.cm_Qualification();
        //增加资质
        public int Add(TG.Model.cm_Qualification model)
        {
            return quadal.Add(model);
        }
        //更新
        public bool Update(TG.Model.cm_Qualification model)
        {
            return quadal.Update(model);
        }
        //获取资质列表根据条件
        public DataSet GetQuaSearchList(string str)
        {
            return quadal.GetList(str);
        }
        //删除资质
        public bool DelectCoperation(string strall)
        {
            return quadal.Delete(strall);
        }
        //查询单个资质根据quaid
        public TG.Model.cm_Qualification GetSingleModel(string quaid)
        {
            return quadal.GetModel(Convert.ToInt32(quaid));
        }
        public string GetquaNum()
        {
            return quadal.GetList().Tables[0].Rows.Count.ToString();
        }
        public string GetattInfoid(string quaid)
        {
            return quadal.GetattInfoid(quaid);
        }

    }
}
