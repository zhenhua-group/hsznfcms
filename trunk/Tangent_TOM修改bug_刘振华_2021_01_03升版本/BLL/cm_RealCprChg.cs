﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_RealCprChg
	/// </summary>
	public partial class cm_RealCprChg
	{
		private readonly TG.DAL.cm_RealCprChg dal=new TG.DAL.cm_RealCprChg();
		public cm_RealCprChg()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_RealCprChg model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_RealCprChg model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			return dal.Delete(ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			return dal.DeleteList(IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_RealCprChg GetModel(int ID)
		{
			
			return dal.GetModel(ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_RealCprChg GetModelByCache(int ID)
		{
			
			string CacheKey = "cm_RealCprChgModel-" + ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_RealCprChg)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_RealCprChg> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_RealCprChg> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_RealCprChg> modelList = new List<TG.Model.cm_RealCprChg>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_RealCprChg model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_RealCprChg();
					if(dt.Rows[n]["ID"]!=null && dt.Rows[n]["ID"].ToString()!="")
					{
						model.ID=int.Parse(dt.Rows[n]["ID"].ToString());
					}
					if(dt.Rows[n]["chg_id"]!=null && dt.Rows[n]["chg_id"].ToString()!="")
					{
						model.chg_id=int.Parse(dt.Rows[n]["chg_id"].ToString());
					}
					if(dt.Rows[n]["payCount"]!=null && dt.Rows[n]["payCount"].ToString()!="")
					{
						model.payCount=decimal.Parse(dt.Rows[n]["payCount"].ToString());
					}
					if(dt.Rows[n]["payTime"]!=null && dt.Rows[n]["payTime"].ToString()!="")
					{
						model.payTime=DateTime.Parse(dt.Rows[n]["payTime"].ToString());
					}
					if(dt.Rows[n]["acceptuser"]!=null && dt.Rows[n]["acceptuser"].ToString()!="")
					{
					model.acceptuser=dt.Rows[n]["acceptuser"].ToString();
					}
					if(dt.Rows[n]["isover"]!=null && dt.Rows[n]["isover"].ToString()!="")
					{
						model.isover=int.Parse(dt.Rows[n]["isover"].ToString());
					}
					if(dt.Rows[n]["mark"]!=null && dt.Rows[n]["mark"].ToString()!="")
					{
					model.mark=dt.Rows[n]["mark"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

