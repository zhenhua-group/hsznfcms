﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.BLL
{
    public partial class cm_TranBulidingProjectValueAllot
    {
        private readonly TG.DAL.cm_TranBulidingProjectValueAllot dal = new TG.DAL.cm_TranBulidingProjectValueAllot();
        public cm_TranBulidingProjectValueAllot()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_TranBulidingProjectValueAllot model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_TranBulidingProjectValueAllot model)
        {
            return dal.Update(model);
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_TranBulidingProjectValueAllot GetModel(int ID)
        {

            return dal.GetModel(ID);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_TranBulidingProjectValueAllot GetModelByProID(int proid, int allotID)
        {
            return dal.GetModelByProID(proid, allotID);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }


        #endregion  Method
    }
}


