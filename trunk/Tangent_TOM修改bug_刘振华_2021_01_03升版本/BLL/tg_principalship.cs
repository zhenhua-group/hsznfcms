﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// tg_principalship
	/// </summary>
	public partial class tg_principalship
	{
		private readonly TG.DAL.tg_principalship dal=new TG.DAL.tg_principalship();
		public tg_principalship()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.tg_principalship model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_principalship model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int pri_ID)
		{
			
			return dal.Delete(pri_ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string pri_IDlist )
		{
			return dal.DeleteList(pri_IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_principalship GetModel(int pri_ID)
		{
			
			return dal.GetModel(pri_ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.tg_principalship GetModelByCache(int pri_ID)
		{
			
			string CacheKey = "tg_principalshipModel-" + pri_ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(pri_ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.tg_principalship)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_principalship> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_principalship> DataTableToList(DataTable dt)
		{
			List<TG.Model.tg_principalship> modelList = new List<TG.Model.tg_principalship>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.tg_principalship model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.tg_principalship();
					if(dt.Rows[n]["pri_ID"]!=null && dt.Rows[n]["pri_ID"].ToString()!="")
					{
						model.pri_ID=int.Parse(dt.Rows[n]["pri_ID"].ToString());
					}
					if(dt.Rows[n]["pri_Name"]!=null && dt.Rows[n]["pri_Name"].ToString()!="")
					{
					model.pri_Name=dt.Rows[n]["pri_Name"].ToString();
					}
					if(dt.Rows[n]["pri_Intro"]!=null && dt.Rows[n]["pri_Intro"].ToString()!="")
					{
					model.pri_Intro=dt.Rows[n]["pri_Intro"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}

		#endregion  Method
	}
}

