﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
using System.Xml;
using System.Text;
namespace TG.BLL
{
    /// <summary>
    /// tg_project
    /// </summary>
    public partial class tg_project
    {
        private readonly TG.DAL.tg_project dal = new TG.DAL.tg_project();
        public tg_project()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int pro_ID)
        {
            return dal.Exists(pro_ID);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.tg_project model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.tg_project model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int pro_ID)
        {

            return dal.Delete(pro_ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string pro_IDlist)
        {
            return dal.DeleteList(pro_IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.tg_project GetModel(int pro_ID)
        {

            return dal.GetModel(pro_ID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.tg_project GetModelByCache(int pro_ID)
        {

            string CacheKey = "tg_projectModel-" + pro_ID;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(pro_ID);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.tg_project)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.tg_project> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.tg_project> DataTableToList(DataTable dt)
        {
            List<TG.Model.tg_project> modelList = new List<TG.Model.tg_project>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.tg_project model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.tg_project();
                    if (dt.Rows[n]["pro_ID"] != null && dt.Rows[n]["pro_ID"].ToString() != "")
                    {
                        model.pro_ID = int.Parse(dt.Rows[n]["pro_ID"].ToString());
                    }
                    if (dt.Rows[n]["pro_Name"] != null && dt.Rows[n]["pro_Name"].ToString() != "")
                    {
                        model.pro_Name = dt.Rows[n]["pro_Name"].ToString();
                    }
                    if (dt.Rows[n]["pro_Order"] != null && dt.Rows[n]["pro_Order"].ToString() != "")
                    {
                        model.pro_Order = dt.Rows[n]["pro_Order"].ToString();
                    }
                    if (dt.Rows[n]["pro_Intro"] != null && dt.Rows[n]["pro_Intro"].ToString() != "")
                    {
                        model.pro_Intro = dt.Rows[n]["pro_Intro"].ToString();
                    }
                    if (dt.Rows[n]["pro_count"] != null && dt.Rows[n]["pro_count"].ToString() != "")
                    {
                        model.pro_count = int.Parse(dt.Rows[n]["pro_count"].ToString());
                    }
                    if (dt.Rows[n]["pro_Kinds"] != null && dt.Rows[n]["pro_Kinds"].ToString() != "")
                    {
                        model.pro_Kinds = dt.Rows[n]["pro_Kinds"].ToString();
                    }
                    if (dt.Rows[n]["pro_Icon"] != null && dt.Rows[n]["pro_Icon"].ToString() != "")
                    {
                        model.pro_Icon = int.Parse(dt.Rows[n]["pro_Icon"].ToString());
                    }
                    if (dt.Rows[n]["pro_StartTime"] != null && dt.Rows[n]["pro_StartTime"].ToString() != "")
                    {
                        model.pro_StartTime = DateTime.Parse(dt.Rows[n]["pro_StartTime"].ToString());
                    }
                    if (dt.Rows[n]["pro_FinishTime"] != null && dt.Rows[n]["pro_FinishTime"].ToString() != "")
                    {
                        model.pro_FinishTime = DateTime.Parse(dt.Rows[n]["pro_FinishTime"].ToString());
                    }
                    if (dt.Rows[n]["pro_Status"] != null && dt.Rows[n]["pro_Status"].ToString() != "")
                    {
                        model.pro_Status = dt.Rows[n]["pro_Status"].ToString();
                    }
                    if (dt.Rows[n]["pro_PostArticle"] != null && dt.Rows[n]["pro_PostArticle"].ToString() != "")
                    {
                        model.pro_PostArticle = int.Parse(dt.Rows[n]["pro_PostArticle"].ToString());
                    }
                    if (dt.Rows[n]["pro_PostComms"] != null && dt.Rows[n]["pro_PostComms"].ToString() != "")
                    {
                        model.pro_PostComms = int.Parse(dt.Rows[n]["pro_PostComms"].ToString());
                    }
                    if (dt.Rows[n]["pro_BuildUnit"] != null && dt.Rows[n]["pro_BuildUnit"].ToString() != "")
                    {
                        model.pro_BuildUnit = dt.Rows[n]["pro_BuildUnit"].ToString();
                    }
                    if (dt.Rows[n]["pro_ArchArea"] != null && dt.Rows[n]["pro_ArchArea"].ToString() != "")
                    {
                        model.pro_ArchArea = decimal.Parse(dt.Rows[n]["pro_ArchArea"].ToString());
                    }
                    if (dt.Rows[n]["pro_FloorNum"] != null && dt.Rows[n]["pro_FloorNum"].ToString() != "")
                    {
                        model.pro_FloorNum = int.Parse(dt.Rows[n]["pro_FloorNum"].ToString());
                    }
                    if (dt.Rows[n]["pro_StruType"] != null && dt.Rows[n]["pro_StruType"].ToString() != "")
                    {
                        model.pro_StruType = dt.Rows[n]["pro_StruType"].ToString();
                    }
                    if (dt.Rows[n]["pro_ArchHeight"] != null && dt.Rows[n]["pro_ArchHeight"].ToString() != "")
                    {
                        model.pro_ArchHeight = decimal.Parse(dt.Rows[n]["pro_ArchHeight"].ToString());
                    }
                    if (dt.Rows[n]["pro_Serial"] != null && dt.Rows[n]["pro_Serial"].ToString() != "")
                    {
                        model.pro_Serial = dt.Rows[n]["pro_Serial"].ToString();
                    }
                    if (dt.Rows[n]["pro_DesignUnit"] != null && dt.Rows[n]["pro_DesignUnit"].ToString() != "")
                    {
                        model.pro_DesignUnit = dt.Rows[n]["pro_DesignUnit"].ToString();
                    }
                    if (dt.Rows[n]["pro_Level"] != null && dt.Rows[n]["pro_Level"].ToString() != "")
                    {
                        model.pro_Level = int.Parse(dt.Rows[n]["pro_Level"].ToString());
                    }
                    if (dt.Rows[n]["pro_UsedSpace"] != null && dt.Rows[n]["pro_UsedSpace"].ToString() != "")
                    {
                        model.pro_UsedSpace = decimal.Parse(dt.Rows[n]["pro_UsedSpace"].ToString());
                    }
                    if (dt.Rows[n]["pro_AllSpace"] != null && dt.Rows[n]["pro_AllSpace"].ToString() != "")
                    {
                        model.pro_AllSpace = decimal.Parse(dt.Rows[n]["pro_AllSpace"].ToString());
                    }
                    if (dt.Rows[n]["pro_category"] != null && dt.Rows[n]["pro_category"].ToString() != "")
                    {
                        model.pro_category = int.Parse(dt.Rows[n]["pro_category"].ToString());
                    }
                    if (dt.Rows[n]["pro_ExInfo1"] != null && dt.Rows[n]["pro_ExInfo1"].ToString() != "")
                    {
                        model.pro_ExInfo1 = int.Parse(dt.Rows[n]["pro_ExInfo1"].ToString());
                    }
                    if (dt.Rows[n]["pro_ExInfo2"] != null && dt.Rows[n]["pro_ExInfo2"].ToString() != "")
                    {
                        model.pro_ExInfo2 = dt.Rows[n]["pro_ExInfo2"].ToString();
                    }
                    if (dt.Rows[n]["pro_ExInfo3"] != null && dt.Rows[n]["pro_ExInfo3"].ToString() != "")
                    {
                        model.pro_ExInfo3 = dt.Rows[n]["pro_ExInfo3"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCountNew(string strWhere)
        {
            return dal.GetRecordCountNew(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        //      
        public DataSet GetListByWhere(string strWhere)
        {
            return dal.GetListByWhere(strWhere);
        }
        #endregion  Method

        public void UpdateByEdit(int pro_id, string opinions)
        {
            TG.BLL.cm_Project cm_bll = new TG.BLL.cm_Project();
            TG.Model.cm_Project cm_model = cm_bll.GetModel(pro_id);
            TG.BLL.cm_Coperation cop_bll = new TG.BLL.cm_Coperation();
            TG.Model.tg_project model = dal.GetModel(cm_model.ReferenceSysNo);
            bool IsRevite = false;
            string cpr_ID = "";
            opinions = opinions.Replace("|*|", "\n");
            string[] arratList = opinions.Split('\n');
            string projectStatus = "";
            for (int i = 0; i < arratList.Length; i++)
            {
                string typestr = arratList[i].Split(':')[0];
                switch (typestr)
                {
                    case "pro_name":
                        model.pro_Name = arratList[i].Split(':')[1];
                        break;
                    case "cpr_coper":
                        IsRevite = true;
                        cpr_ID = arratList[i].Split(':')[1];
                        break;
                    case "pro_level":
                        model.pro_Level = int.Parse(arratList[i].Split(':')[1]);
                        break;
                    case "ProjType":
                        //model.ProjType = arratList[i].Split(':')[1];
                        break;
                    case "ProjectScale":
                        model.pro_ArchArea = Convert.ToDecimal(arratList[i].Split(':')[1]);
                        break;
                    case "pro_buildUnit":
                        // model.pro_buildUnit = arratList[i].Split(':')[1];
                        break;
                    case "ProjArea":
                        //model.ProjArea = Convert.ToDecimal(arratList[i].Split(':')[1]);
                        break;
                    case "pro_StruType":
                        model.pro_StruType = GetXML("StructType", arratList[i].Split(':')[1]);
                        break;
                    case "pro_kinds":
                        model.pro_Intro = GetXML("BuildType", arratList[i].Split(':')[1]);
                        break;
                    //alter
                    case "buildType":
                        // model.BuildType = GetXML("BdsdduildType", arratList[i].Split(':')[1]);
                        break;
                    case "PMName":
                        // model.PMName = arratList[i].Split(':')[1];
                        break;
                    case "PMPhone":
                        // model.PMPhone = arratList[i].Split(':')[1];
                        break;
                    case "ChgJia":
                        // model.ChgJia = arratList[i].Split(':')[1];
                        break;
                    case "Phone":
                        model.pro_Phone = arratList[i].Split(':')[1];
                        break;
                    case "Unit":
                        model.pro_DesignUnit = arratList[i].Split(':')[1];
                        break;
                    case "cpr_Acount":
                        //model.Cpr_Acount = Convert.ToDecimal(arratList[i].Split(':')[1]);
                        break;
                    case "BuildAddress":
                        model.pro_Adress = arratList[i].Split(':')[1];
                        break;
                    case "Industry":
                        //model.Industry = returnName(arratList[i].Split(':')[1]);
                        break;
                    case "pro_status":
                        projectStatus = arratList[i].Split(':')[1];
                        break;
                    case "Pro_src":
                        //model.Pro_src = int.Parse(arratList[i].Split(':')[1]);
                        break;
                    case "pro_startTime":
                        model.pro_StartTime = Convert.ToDateTime(arratList[i].Split(':')[1]);
                        break;
                    case "pro_finishTime":
                        model.pro_FinishTime = Convert.ToDateTime(arratList[i].Split(':')[1]);
                        break;
                    case "ProjSub":
                        //model.ProjSub = arratList[i].Split(':')[1];
                        break;
                    case "pro_Intro":
                        //model.pro_Intro = arratList[i].Split(':')[1];
                        break;
                }

            }
            //更新项目cm里的项目表
            if (IsRevite)
            {
                TG.Model.cm_Coperation modelcop = cop_bll.GetModel(int.Parse(cpr_ID));
                model.pro_Adress = modelcop.BuildPosition;
                model.pro_ArchArea = Convert.ToDecimal(modelcop.BuildArea);
                //ProjectScale 
            }
            dal.Update(model);

            //修改tcd里面的图纸信息
            //if (projectStatus != "")
            //{
            //    dal.UpdateStatus(projectStatus, model.pro_ID);
            //}
        }
        public string returnName(string pross)
        {
            string result = "";
            if (pross.Trim() != "")
            {
                string[] array = pross.Split(new char[] { ',' }, StringSplitOptions.None);
                TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

                for (int i = 0; i < array.Length; i++)
                {
                    result += bll_dic.GetModel(int.Parse(array[i])).dic_Name + ",";
                }
                result = result.IndexOf(",") > -1 ? result.Remove(result.Length - 1) : "";
            }
            return result;
        }
        public string GetXML(string type, string p)
        {
            StringBuilder builder = new StringBuilder();
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + type + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                //一级
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                Readxml(nodes, out builder, p);
            }
            if (builder.ToString().Length > 0)
            {
                return builder.ToString().Substring(1);
            }
            else
            {
                return builder.ToString();
            }
            //公共建筑^办公+工业建筑^重工厂房^轻工厂房
        }
        //第一级别
        public void Readxml(XmlNodeList xmlnl, out StringBuilder sb_, string p)
        {
            sb_ = new StringBuilder();
            string[] arraylist = p.Split(',');
            foreach (XmlNode xl in xmlnl)
            {
                XmlNodeList nodes = xl.ChildNodes;
                for (int i = 0; i < arraylist.Length; i++)
                {
                    if (xl.Attributes["name"].Value == arraylist[i])
                    {
                        if (xl.ChildNodes.Count != 0)
                        {
                            sb_.Append("+" + xl.Attributes["name"].Value);
                            ReadxmlChild(nodes, sb_, p);
                        }
                        else
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                        }
                    }
                }
            }
        }
        //第二级别和一下的级别
        public void ReadxmlChild(XmlNodeList xmlnl, StringBuilder sb_, string p)
        {

            string[] arraylist = p.Split(',');
            foreach (XmlNode xl in xmlnl)
            {
                XmlNodeList nodes = xl.ChildNodes;
                for (int i = 0; i < arraylist.Length; i++)
                {
                    if (xl.Attributes["name"].Value == arraylist[i])
                    {
                        if (xl.ChildNodes.Count != 0)
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                            ReadxmlChild(nodes, sb_, p);
                        }
                        else
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                        }
                    }
                }
            }
        }
        /// <summary>
        ///  导出
        /// </summary>
        public DataSet GetListExport(string strWhere)
        {
            return dal.GetListExport(strWhere);
        }

    }
}

