﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.BLL
{
    public class cm_jjsProjectValueAllotConfig
    {
        private readonly TG.DAL.cm_jjsProjectValueAllotConfig dal = new TG.DAL.cm_jjsProjectValueAllotConfig();
        public cm_jjsProjectValueAllotConfig()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_jjsProjectValueAllotConfig model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_jjsProjectValueAllotConfig model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_jjsProjectValueAllotConfig GetModel(int ID)
        {
            return dal.GetModel(ID);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_jjsProjectValueAllotConfig GetModelByType(int type)
        {
            return dal.GetModelByType(type);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_jjsProjectValueAllotConfig> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_jjsProjectValueAllotConfig> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_jjsProjectValueAllotConfig> modelList = new List<TG.Model.cm_jjsProjectValueAllotConfig>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_jjsProjectValueAllotConfig model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_jjsProjectValueAllotConfig();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["typeStatus"] != null && dt.Rows[n]["typeStatus"].ToString() != "")
                    {
                        model.typeStatus = int.Parse(dt.Rows[n]["typeStatus"].ToString());
                    }
                    if (dt.Rows[n]["typeDetail"] != null && dt.Rows[n]["typeDetail"].ToString() != "")
                    {
                        model.typeDetail = dt.Rows[n]["typeDetail"].ToString();
                    }
                    if (dt.Rows[n]["ProofreadPercent"] != null && dt.Rows[n]["ProofreadPercent"].ToString() != "")
                    {
                        model.ProofreadPercent = decimal.Parse(dt.Rows[n]["ProofreadPercent"].ToString());
                    }
                    if (dt.Rows[n]["BuildingPercent"] != null && dt.Rows[n]["BuildingPercent"].ToString() != "")
                    {
                        model.BuildingPercent = decimal.Parse(dt.Rows[n]["BuildingPercent"].ToString());
                    }
                    if (dt.Rows[n]["StructurePercent"] != null && dt.Rows[n]["StructurePercent"].ToString() != "")
                    {
                        model.StructurePercent = decimal.Parse(dt.Rows[n]["StructurePercent"].ToString());
                    }
                    if (dt.Rows[n]["DrainPercent"] != null && dt.Rows[n]["DrainPercent"].ToString() != "")
                    {
                        model.DrainPercent = decimal.Parse(dt.Rows[n]["DrainPercent"].ToString());
                    }
                    if (dt.Rows[n]["HavcPercent"] != null && dt.Rows[n]["HavcPercent"].ToString() != "")
                    {
                        model.HavcPercent = decimal.Parse(dt.Rows[n]["HavcPercent"].ToString());
                    }
                    if (dt.Rows[n]["ElectricPercent"] != null && dt.Rows[n]["ElectricPercent"].ToString() != "")
                    {
                        model.ElectricPercent = decimal.Parse(dt.Rows[n]["ElectricPercent"].ToString());
                    }
                    if (dt.Rows[n]["TotalBuildingPercent"] != null && dt.Rows[n]["TotalBuildingPercent"].ToString() != "")
                    {
                        model.Totalbuildingpercent = decimal.Parse(dt.Rows[n]["TotalBuildingPercent"].ToString());
                    }
                    if (dt.Rows[n]["TotalInstallationPercent"] != null && dt.Rows[n]["TotalInstallationPercent"].ToString() != "")
                    {
                        model.TotalInstallationpercent = decimal.Parse(dt.Rows[n]["TotalInstallationPercent"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_jjsProjectValueAllotDetail GetDetailModel(int proID,int allotID)
        {
            return dal.GetDetailModel(proID,allotID);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }
        #endregion

    }
}
