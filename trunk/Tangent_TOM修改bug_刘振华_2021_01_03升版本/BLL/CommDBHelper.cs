﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.BLL
{
    public partial class CommDBHelper
    {
        private readonly TG.DAL.CommDBHelper dal = new TG.DAL.CommDBHelper();
        /// <summary>
        /// 根据sql返回数据集
        /// </summary>
        /// <param name="Sql"></param>
        /// <returns></returns>
        public DataSet GetList(string Sql)
        {
            return dal.GetList(Sql);
        }
        //返回执行结果
        public int ExcuteBySql(string Sql)
        {
            return dal.ExcuteBySql(Sql);
        }
        //联系人附件
        public int ConnactAttach(int cpr_id)
        {
            return dal.ConnactAttach(cpr_id);
        }
        //分页数据
        public int GetDataPageListCount(string query, string tableName)
        {
            return dal.GetDataPageListCount(query, tableName);
        }
        public int GetDataPageListCount(string strSql)
        {
            return dal.GetDataPageListCount(strSql);
        }
        //获取分页
        public DataSet GetDataPageList(string query, string orderBy, int startIndex, int recordCount, string tableName)
        {
            return dal.GetDataPageList(query, orderBy, startIndex, recordCount, tableName);
        }
        public DataSet GetDataPageList(string strSql, int startindex, int recordCount)
        {
            return dal.GetDataPageList(strSql, startindex, recordCount);
        }
    }
}
