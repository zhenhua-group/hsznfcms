﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.BLL
{
    public  class cm_QuaAttach
    {
        TG.DAL.cm_QuaAttach dal = new DAL.cm_QuaAttach();
        public int Add(TG.Model. cm_QuaAttach model)
        {
            return dal.Add(model);
        }
        public DataSet GetDate(string str)
        {
            return dal.GetList(str);
        }
        public bool Delete(string str)
        {
            return dal.DeleteList(str);
        }
    }
}
