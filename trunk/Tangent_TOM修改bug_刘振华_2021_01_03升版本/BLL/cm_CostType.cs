﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    /// <summary>
    /// cm_CostType
    /// </summary>
    public partial class cm_CostType
    {
        private readonly TG.DAL.cm_CostType dal = new TG.DAL.cm_CostType();
        public cm_CostType()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CostType model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CostType model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            //该表无主键信息，请自定义主键/条件字段
            return dal.Delete(id);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CostType GetModel(int id)
        {
            //该表无主键信息，请自定义主键/条件字段
            return dal.GetModel(id);
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CostType> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_CostType> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_CostType> modelList = new List<TG.Model.cm_CostType>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_CostType model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_CostType();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["costName"] != null && dt.Rows[n]["costName"].ToString() != "")
                    {
                        model.costName = dt.Rows[n]["costName"].ToString();
                    }
                    if (dt.Rows[n]["costGroup"] != null && dt.Rows[n]["costGroup"].ToString() != "")
                    {
                        model.costGroup = int.Parse(dt.Rows[n]["costGroup"].ToString());
                    }
                    if (dt.Rows[n]["isInput"] != null && dt.Rows[n]["isInput"].ToString() != "")
                    {
                        model.isInput = int.Parse(dt.Rows[n]["isInput"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListByPageProc(query, startIndex, endIndex);
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return dal.GetListPageProcCount(query);
        }
        /// <summary>
        /// 判断是否重复
        /// </summary>
        /// <param name="cst_num"></param>
        /// <param name="cst_Name"></param>
        /// <returns></returns>
        public bool JudgeData(string cost_unit, string cost_Name, string cost_id, string isInput)
        {
            return dal.JudgeData(cost_unit, cost_Name, cost_id, isInput);
        }

        #endregion  Method
    }
}


