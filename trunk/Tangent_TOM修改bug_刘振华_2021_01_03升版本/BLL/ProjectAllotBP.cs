﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;
using System.Data;

namespace TG.BLL
{
    public class ProjectAllotBP
    {
        private ProjectAllotDA da = new ProjectAllotDA();

        public int SavePartOneRecord(PartOne partOneEntity)
        {
            return da.SavePartOneRecord(partOneEntity);
        }
        public List<string> GetAllSpeciality()
        {
            return da.GetAllSpeciality();
        }

        public PartOne GetPartOneRecord(string proid)
        {
            return da.GetPartOneRecord(proid);
        }

        public PartOne GetPartOneRecordCprunitprice(string proid)
        {
            return da.GetPartOneRecordCprunitprice(proid);
        }

        public PartOne GetPartOneRecord(int partOneSysNo)
        {
            return da.GetPartOneRecordBySysNo(partOneSysNo);
        }

        public int GetRoleSysNoByPosition(int position)
        {
            return da.GetRoleSysNoByPosition(position);
        }

        public int SavePartTwoRecord(PartTwo partTwoEntity)
        {
            return da.SavePartTwoRecord(partTwoEntity);
        }

        public PartTwo GetPartTwoRecord(int partOneSysNo)
        {
            return da.GetPartTwoRecord(partOneSysNo);
        }
        /// <summary>
        /// 得到一个项目分配实体
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        //public ProjectAllotDetailViewEntity GetProjectAllotDetailViewEntityRecord(int costSysNo)
        //{
        //    ProjectAllotDetailViewEntity projectAllotDetailViewEntity = new ProjectAllotDetailViewEntity
        //    {
        //        //ProjectSysNo = projectSysNo
        //    };

        //    projectAllotDetailViewEntity.PartOne = da.GetPartOneRecord(costSysNo);

        //                if (projectAllotDetailViewEntity.PartOne == null)
        //                {
        //                        projectAllotDetailViewEntity.PartOne = new PartOne();
        //                }
        //                else
        //                {
        //                        projectAllotDetailViewEntity.PartTwo = da.GetPartTwoRecord(projectAllotDetailViewEntity.PartOne.SysNo);

        //                        if (projectAllotDetailViewEntity.PartTwo == null)
        //                        {
        //                                projectAllotDetailViewEntity.PartTwo = new PartTwo();
        //                        }

        //                        projectAllotDetailViewEntity.PartThree = da.GetPartThreeRecord(projectAllotDetailViewEntity.PartOne.SysNo);

        //                        if (projectAllotDetailViewEntity.PartThree == null)
        //                        {
        //                                projectAllotDetailViewEntity.PartThree = new PartThree();
        //                        }

        //                        projectAllotDetailViewEntity.PartFour = da.GetPartFourRecord(projectAllotDetailViewEntity.PartOne.SysNo);

        //                        if (projectAllotDetailViewEntity.PartFour == null)
        //                        {
        //                                projectAllotDetailViewEntity.PartFour = new PartFour();
        //                        }
        //                }
        //                return projectAllotDetailViewEntity;
        //        }
        public int SavePartThreeAndPartFourRecord(PartThree partThree, PartFour partFour)
        {
            int count = 0;
            count = da.SavePartThreeRecord(partThree);

            count = da.SavePartFourRecord(partFour);


            return count;
        }

        public PartFour GetPartFourRecord(int partOneSysNo)
        {
            return da.GetPartFourRecord(partOneSysNo);
        }

        /// <summary>
        /// 删除跟该记录有关的所有记录
        /// </summary>
        /// <param name="costSysNo"></param>
        /// <returns></returns>
        public int DeleteAllRecord(int costSysNo)
        {
            return da.DeleteAllRecord(costSysNo);
        }

        public CostCompanyConfig GetCostCompanyConfig()
        {
            return da.GetCostCompanyConfig();
        }

        public CostUserConfig GetCostUserConfig()
        {
            return da.GetCostUserConfig();
        }

        public Dictionary<string, List<AllotUserViewEntity>> GetAllotUsers(int projectSysNo)
        {
            List<AllotUserViewEntity> sourceList = da.GetAllotUsers(projectSysNo);

            Dictionary<string, List<AllotUserViewEntity>> dictionaryAllotUser = new Dictionary<string, List<AllotUserViewEntity>>();

            dictionaryAllotUser.Add("建筑", (from allotUser in sourceList where allotUser.SpecialtyName == "建筑" select allotUser).ToList<AllotUserViewEntity>());
            dictionaryAllotUser.Add("结构", (from allotUser in sourceList where allotUser.SpecialtyName == "结构" select allotUser).ToList<AllotUserViewEntity>());
            dictionaryAllotUser.Add("设备", (from allotUser in sourceList where allotUser.SpecialtyName == "设备" select allotUser).ToList<AllotUserViewEntity>());
            dictionaryAllotUser.Add("电气", (from allotUser in sourceList where allotUser.SpecialtyName == "电气" select allotUser).ToList<AllotUserViewEntity>());

            return dictionaryAllotUser;
        }
        /// <summary>
        /// 得到第三部分数据
        /// </summary>
        /// <param name="partOneSysNo"></param>
        /// <returns></returns>
        public PartThree GetPartThreeRecord(int partOneSysNo)
        {
            return da.GetPartThreeRecord(partOneSysNo);
        }


        /// <summary>
        /// 分配中的项目
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        public string GetAllotPercent(string projectSysNo)
        {
            return da.GetAllotPercent(projectSysNo);
        }

        public DataSet GetPassproject(string strWhere)
        {
            return da.GetPassproject(strWhere);
        }
        
        // <summary>
        /// 取得分配信息
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        public DataSet GetAllotInfo(string projectSysNo)
        {
            return da.GetAllotInfo(projectSysNo);
        }
    }
}
