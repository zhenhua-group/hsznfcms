﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;

namespace TG.BLL
{
	public class WebBannerAttachBP
	{
		private WebBannerAttachDA da = new WebBannerAttachDA();

		public int AddWebBannerAttach(WebBannerAttachViewEntity dataEntity)
		{
			return da.AddWebBannerAttach(dataEntity);
		}

		public int UpdateWebBannerAttach(string oldTempSysNo, int replaceSysNo)
		{
			return da.UpdateWebBannerAttach(oldTempSysNo, replaceSysNo);
		}

        public List<WebBannerAttachViewEntity> GetWebBannerViewEntityList(string webBannerSysNo)
		{
			return da.GetWebBannerViewEntityList(webBannerSysNo);
		}

		public bool ExistsSameNameByFile(string fileName, out int sameNameFileCount)
		{
			return da.ExistsSameNameByFile(fileName, out sameNameFileCount);
		}
	}
}
