﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_SubCoperationType
	/// </summary>
	public partial class cm_SubCoperationType
	{
		private readonly TG.DAL.cm_SubCoperationType dal=new TG.DAL.cm_SubCoperationType();
		public cm_SubCoperationType()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			return dal.Exists(ID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_SubCoperationType model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_SubCoperationType model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			return dal.Delete(ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			return dal.DeleteList(IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_SubCoperationType GetModel(int ID)
		{
			
			return dal.GetModel(ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_SubCoperationType GetModelByCache(int ID)
		{
			
			string CacheKey = "cm_SubCoperationTypeModel-" + ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_SubCoperationType)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_SubCoperationType> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_SubCoperationType> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_SubCoperationType> modelList = new List<TG.Model.cm_SubCoperationType>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_SubCoperationType model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_SubCoperationType();
					if(dt.Rows[n]["ID"]!=null && dt.Rows[n]["ID"].ToString()!="")
					{
						model.ID=int.Parse(dt.Rows[n]["ID"].ToString());
					}
					if(dt.Rows[n]["cpr_Id"]!=null && dt.Rows[n]["cpr_Id"].ToString()!="")
					{
						model.cpr_Id=int.Parse(dt.Rows[n]["cpr_Id"].ToString());
					}
					if(dt.Rows[n]["cpr_No"]!=null && dt.Rows[n]["cpr_No"].ToString()!="")
					{
					model.cpr_No=dt.Rows[n]["cpr_No"].ToString();
					}
					if(dt.Rows[n]["Item_Name"]!=null && dt.Rows[n]["Item_Name"].ToString()!="")
					{
					model.Item_Name=dt.Rows[n]["Item_Name"].ToString();
					}
					if(dt.Rows[n]["Item_Area"]!=null && dt.Rows[n]["Item_Area"].ToString()!="")
					{
						model.Item_Area=decimal.Parse(dt.Rows[n]["Item_Area"].ToString());
					}
					if(dt.Rows[n]["UpdateBy"]!=null && dt.Rows[n]["UpdateBy"].ToString()!="")
					{
					model.UpdateBy=dt.Rows[n]["UpdateBy"].ToString();
					}
					if(dt.Rows[n]["LastUpdate"]!=null && dt.Rows[n]["LastUpdate"].ToString()!="")
					{
						model.LastUpdate=DateTime.Parse(dt.Rows[n]["LastUpdate"].ToString());
					}
					if(dt.Rows[n]["Money"]!=null && dt.Rows[n]["Money"].ToString()!="")
					{
						model.Money=decimal.Parse(dt.Rows[n]["Money"].ToString());
					}
					if(dt.Rows[n]["Remark"]!=null && dt.Rows[n]["Remark"].ToString()!="")
					{
					model.Remark=dt.Rows[n]["Remark"].ToString();
					}
					if(dt.Rows[n]["SubType"]!=null && dt.Rows[n]["SubType"].ToString()!="")
					{
					model.SubType=dt.Rows[n]["SubType"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

