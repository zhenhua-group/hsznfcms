﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using TG.DAL;

namespace TG.BLL
{
        public class CoperationAcountBP
        {
                private CoperationAcountDA da = new CoperationAcountDA();

                public List<CoperationAcountOfMoneyViewEntity> GetCoperationAccountsBetweenYear(int startYear, int endYear,string unit)
                {
                        List<CoperationAcountOfMoneyViewEntity> resultList = new List<CoperationAcountOfMoneyViewEntity>();

                        for (int i = startYear; i <= endYear; i++)
                        {
                                resultList.Add(da.GetCoperationAccountsOfYear(i,unit));
                        }

                        return resultList;
                }

                public Dictionary<string, List<CoperationAcountOfMoneyViewEntity>> GetCoperationAccountsByTypeBetweenYear(int startYear, int endYear,string unit)
                {
                        List<string> copeartionTypeList = da.GetCoperationTypeList();

                        Dictionary<string, List<CoperationAcountOfMoneyViewEntity>> resultDictionary = new Dictionary<string, List<CoperationAcountOfMoneyViewEntity>>();

                        foreach (string typeItem in copeartionTypeList)
                        {
                                List<CoperationAcountOfMoneyViewEntity> subItemList = new List<CoperationAcountOfMoneyViewEntity>();

                                for (int i = startYear; i <= endYear; i++)
                                {
                                        subItemList.Add(da.GetCoperationAccountsByTyoeOfYear(i, typeItem,unit));
                                }
                                resultDictionary.Add(typeItem, subItemList);
                        }

                        return resultDictionary;
                }

                public List<string> GetAllCoperationTypeList()
                {
                        return da.GetCoperationTypeList();
                }

                public List<CoperationCountViewEntity> GetCoperationCountBetweenYear(int startYear, int endYear,string unit)
                {
                        List<CoperationCountViewEntity> resultList = new List<CoperationCountViewEntity>();

                        for (int i = startYear; i <= endYear; i++)
                        {
                                resultList.Add(da.GetCoperationCountOfYear(i,unit));
                        }

                        return resultList;
                }

                public Dictionary<string, List<CoperationCountViewEntity>> GetCoperationCountByTypeOfYear(int startYear, int endYear,string unit)
                {
                        List<string> copeartionTypeList = da.GetCoperationTypeList();

                        Dictionary<string, List<CoperationCountViewEntity>> resultDictionary = new Dictionary<string, List<CoperationCountViewEntity>>();

                        foreach (string typeItem in copeartionTypeList)
                        {
                                List<CoperationCountViewEntity> subItemList = new List<CoperationCountViewEntity>();

                                for (int i = startYear; i <= endYear; i++)
                                {
                                        subItemList.Add(da.GetCoperationCountByTypeOfYear(i, typeItem,unit));
                                }
                                resultDictionary.Add(typeItem, subItemList);
                        }

                        return resultDictionary;
                }

                public Dictionary<string, List<CoperationCountViewEntity>> GetCoperationCountByStatusOfYear(int startYear, int endYear,string unit)
                {
                        Dictionary<string, List<CoperationCountViewEntity>> resultDictionary = new Dictionary<string, List<CoperationCountViewEntity>>();

                        resultDictionary.Add("进行中", new List<CoperationCountViewEntity>());
                        resultDictionary.Add("暂停", new List<CoperationCountViewEntity>());
                        resultDictionary.Add("完成", new List<CoperationCountViewEntity>());

                        foreach (KeyValuePair<string, List<CoperationCountViewEntity>> copertionItem in resultDictionary)
                        {
                                for (int i = startYear; i <= endYear; i++)
                                {
                                        copertionItem.Value.Add(new CoperationCountViewEntity { Year = i, Count = da.GetCoperationCountByStatusOfYear(i, copertionItem.Key,unit) });
                                }
                        }

                        return resultDictionary;
                }

                public List<object> GetCoperationList(string whereCondition, int pageCurrent, int pageSize)
                {
                        List<object> resultList = new List<object>();

                        int dataCount = 0;
                        List<CoperationListViewEntity> list = da.GetCoperationList(whereCondition, pageCurrent, pageSize, out dataCount);

                        resultList.Add(dataCount);

                        resultList.Add(list);

                        return resultList;
                }

                public List<object> GetCoperationStatusList(string whereCondition, int pageCurrent, int pageSize)
                {
                        List<object> resultList = new List<object>();

                        int dataCount = 0;
                        List<CoperationListViewEntity> list = da.GetCoperationList(whereCondition, pageCurrent, pageSize, out dataCount);

                        resultList.Add(dataCount);

                        resultList.Add(list);

                        return resultList;
                }
        }
}
