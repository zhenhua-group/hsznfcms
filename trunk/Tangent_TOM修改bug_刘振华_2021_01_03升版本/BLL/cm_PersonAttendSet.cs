﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_PersonAttendSet
	/// </summary>
	public partial class cm_PersonAttendSet
	{
		private readonly TG.DAL.cm_PersonAttendSet dal=new TG.DAL.cm_PersonAttendSet();
		public cm_PersonAttendSet()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int attend_id)
		{
			return dal.Exists(attend_id);
		}
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public int ExistsBackID(int mem_id, int month,int year)
        {
            return dal.ExistsBackID(mem_id, month,year);
        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_PersonAttendSet model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_PersonAttendSet model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int attend_id)
		{
			
			return dal.Delete(attend_id);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string attend_idlist )
		{
			return dal.DeleteList(attend_idlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_PersonAttendSet GetModel(int attend_id)
		{
			
			return dal.GetModel(attend_id);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_PersonAttendSet GetModelByCache(int attend_id)
		{
			
			string CacheKey = "cm_PersonAttendSetModel-" + attend_id;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(attend_id);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_PersonAttendSet)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_PersonAttendSet> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_PersonAttendSet> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_PersonAttendSet> modelList = new List<TG.Model.cm_PersonAttendSet>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_PersonAttendSet model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_PersonAttendSet();
					if(dt.Rows[n]["attend_id"]!=null && dt.Rows[n]["attend_id"].ToString()!="")
					{
						model.attend_id=int.Parse(dt.Rows[n]["attend_id"].ToString());
					}
					if(dt.Rows[n]["mem_ID"]!=null && dt.Rows[n]["mem_ID"].ToString()!="")
					{
						model.mem_ID=int.Parse(dt.Rows[n]["mem_ID"].ToString());
					}
					if(dt.Rows[n]["attend_month"]!=null && dt.Rows[n]["attend_month"].ToString()!="")
					{
						model.attend_month=int.Parse(dt.Rows[n]["attend_month"].ToString());
					}
					if(dt.Rows[n]["ToWork"]!=null && dt.Rows[n]["ToWork"].ToString()!="")
					{
					model.ToWork=dt.Rows[n]["ToWork"].ToString();
					}
					if(dt.Rows[n]["OffWork"]!=null && dt.Rows[n]["OffWork"].ToString()!="")
					{
					model.OffWork=dt.Rows[n]["OffWork"].ToString();
					}
                    if (dt.Rows[n]["attend_year"] != null && dt.Rows[n]["attend_year"].ToString() != "")
                    {
                        model.attend_year = int.Parse(dt.Rows[n]["attend_year"].ToString());
                    }
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

