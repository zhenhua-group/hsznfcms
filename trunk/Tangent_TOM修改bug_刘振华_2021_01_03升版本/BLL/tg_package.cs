﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// tg_package
	/// </summary>
	public partial class tg_package
	{
		private readonly TG.DAL.tg_package dal=new TG.DAL.tg_package();
		public tg_package()
		{}
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(decimal package_ID)
		{
			return dal.Exists(package_ID);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public decimal Add(TG.Model.tg_package model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_package model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(decimal package_ID)
		{
			
			return dal.Delete(package_ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string package_IDlist )
		{
			return dal.DeleteList(package_IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_package GetModel(decimal package_ID)
		{
			
			return dal.GetModel(package_ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.tg_package GetModelByCache(decimal package_ID)
		{
			
			string CacheKey = "tg_packageModel-" + package_ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(package_ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.tg_package)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_package> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_package> DataTableToList(DataTable dt)
		{
			List<TG.Model.tg_package> modelList = new List<TG.Model.tg_package>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.tg_package model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.tg_package();
					if(dt.Rows[n]["package_ID"]!=null && dt.Rows[n]["package_ID"].ToString()!="")
					{
						model.package_ID=decimal.Parse(dt.Rows[n]["package_ID"].ToString());
					}
					if(dt.Rows[n]["subpro_ID"]!=null && dt.Rows[n]["subpro_ID"].ToString()!="")
					{
						model.subpro_ID=int.Parse(dt.Rows[n]["subpro_ID"].ToString());
					}
					if(dt.Rows[n]["class_ID"]!=null && dt.Rows[n]["class_ID"].ToString()!="")
					{
						model.class_ID=int.Parse(dt.Rows[n]["class_ID"].ToString());
					}
					if(dt.Rows[n]["user_ID"]!=null && dt.Rows[n]["user_ID"].ToString()!="")
					{
						model.user_ID=int.Parse(dt.Rows[n]["user_ID"].ToString());
					}
					if(dt.Rows[n]["filename"]!=null && dt.Rows[n]["filename"].ToString()!="")
					{
					model.filename=dt.Rows[n]["filename"].ToString();
					}
					if(dt.Rows[n]["package"]!=null && dt.Rows[n]["package"].ToString()!="")
					{
						model.package=(byte[])dt.Rows[n]["package"];
					}
					if(dt.Rows[n]["remember"]!=null && dt.Rows[n]["remember"].ToString()!="")
					{
					model.remember=dt.Rows[n]["remember"].ToString();
					}
					if(dt.Rows[n]["ver_date"]!=null && dt.Rows[n]["ver_date"].ToString()!="")
					{
						model.ver_date=DateTime.Parse(dt.Rows[n]["ver_date"].ToString());
					}
					if(dt.Rows[n]["save_flag"]!=null && dt.Rows[n]["save_flag"].ToString()!="")
					{
						model.save_flag=int.Parse(dt.Rows[n]["save_flag"].ToString());
					}
					if(dt.Rows[n]["file_size"]!=null && dt.Rows[n]["file_size"].ToString()!="")
					{
						model.file_size=int.Parse(dt.Rows[n]["file_size"].ToString());
					}
					if(dt.Rows[n]["file_state"]!=null && dt.Rows[n]["file_state"].ToString()!="")
					{
						model.file_state=int.Parse(dt.Rows[n]["file_state"].ToString());
					}
					if(dt.Rows[n]["lastModifyUser_ID"]!=null && dt.Rows[n]["lastModifyUser_ID"].ToString()!="")
					{
						model.lastModifyUser_ID=int.Parse(dt.Rows[n]["lastModifyUser_ID"].ToString());
					}
					if(dt.Rows[n]["purpose_ID"]!=null && dt.Rows[n]["purpose_ID"].ToString()!="")
					{
						model.purpose_ID=int.Parse(dt.Rows[n]["purpose_ID"].ToString());
					}
					if(dt.Rows[n]["version"]!=null && dt.Rows[n]["version"].ToString()!="")
					{
					model.version=dt.Rows[n]["version"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

