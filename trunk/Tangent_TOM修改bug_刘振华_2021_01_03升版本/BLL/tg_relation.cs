﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// tg_relation
	/// </summary>
	public partial class tg_relation
	{
		private readonly TG.DAL.tg_relation dal=new TG.DAL.tg_relation();
		public tg_relation()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TG.Model.tg_relation model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_relation model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.Delete();
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_relation GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.GetModel();
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.tg_relation GetModelByCache()
		{
			//该表无主键信息，请自定义主键/条件字段
			string CacheKey = "tg_relationModel-" ;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel();
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.tg_relation)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_relation> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_relation> DataTableToList(DataTable dt)
		{
			List<TG.Model.tg_relation> modelList = new List<TG.Model.tg_relation>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.tg_relation model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.tg_relation();
					if(dt.Rows[n]["pro_ID"]!=null && dt.Rows[n]["pro_ID"].ToString()!="")
					{
						model.pro_ID=int.Parse(dt.Rows[n]["pro_ID"].ToString());
					}
					if(dt.Rows[n]["mem_ID"]!=null && dt.Rows[n]["mem_ID"].ToString()!="")
					{
						model.mem_ID=int.Parse(dt.Rows[n]["mem_ID"].ToString());
					}
					if(dt.Rows[n]["mem_isManage"]!=null && dt.Rows[n]["mem_isManage"].ToString()!="")
					{
						model.mem_isManage=int.Parse(dt.Rows[n]["mem_isManage"].ToString());
					}
					if(dt.Rows[n]["mem_isSpecLead"]!=null && dt.Rows[n]["mem_isSpecLead"].ToString()!="")
					{
						model.mem_isSpecLead=int.Parse(dt.Rows[n]["mem_isSpecLead"].ToString());
					}
					if(dt.Rows[n]["mem_isAssistant"]!=null && dt.Rows[n]["mem_isAssistant"].ToString()!="")
					{
						model.mem_isAssistant=int.Parse(dt.Rows[n]["mem_isAssistant"].ToString());
					}
					if(dt.Rows[n]["mem_isAssessor"]!=null && dt.Rows[n]["mem_isAssessor"].ToString()!="")
					{
						model.mem_isAssessor=int.Parse(dt.Rows[n]["mem_isAssessor"].ToString());
					}
					if(dt.Rows[n]["mem_isCorrector"]!=null && dt.Rows[n]["mem_isCorrector"].ToString()!="")
					{
						model.mem_isCorrector=int.Parse(dt.Rows[n]["mem_isCorrector"].ToString());
					}
					if(dt.Rows[n]["mem_isValidator"]!=null && dt.Rows[n]["mem_isValidator"].ToString()!="")
					{
						model.mem_isValidator=int.Parse(dt.Rows[n]["mem_isValidator"].ToString());
					}
					if(dt.Rows[n]["mem_isDirector"]!=null && dt.Rows[n]["mem_isDirector"].ToString()!="")
					{
						model.mem_isDirector=int.Parse(dt.Rows[n]["mem_isDirector"].ToString());
					}
					if(dt.Rows[n]["mem_RoleIDs"]!=null && dt.Rows[n]["mem_RoleIDs"].ToString()!="")
					{
					model.mem_RoleIDs=dt.Rows[n]["mem_RoleIDs"].ToString();
					}
					if(dt.Rows[n]["mem_bCanViewSpec"]!=null && dt.Rows[n]["mem_bCanViewSpec"].ToString()!="")
					{
						model.mem_bCanViewSpec=int.Parse(dt.Rows[n]["mem_bCanViewSpec"].ToString());
					}
					if(dt.Rows[n]["mem_bCanViewAllSpec"]!=null && dt.Rows[n]["mem_bCanViewAllSpec"].ToString()!="")
					{
						model.mem_bCanViewAllSpec=int.Parse(dt.Rows[n]["mem_bCanViewAllSpec"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

