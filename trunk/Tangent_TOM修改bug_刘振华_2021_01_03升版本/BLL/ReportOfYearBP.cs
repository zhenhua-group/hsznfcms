﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.Model;

namespace TG.BLL
{
    public class ReportOfYearBP
    {
        private TG.DAL.ReportOfYearDA da = new TG.DAL.ReportOfYearDA();

        public List<ReportOfYearViewEntity> GetProjectCountBetweenYear(int startYear, int endYear, string unit)
        {
            List<ReportOfYearViewEntity> sourceList = da.GetProjectCountBetweenYear(startYear, endYear, unit);

            List<ReportOfYearViewEntity> resultList = new List<ReportOfYearViewEntity>();

            for (int i = startYear; i <= endYear; i++)
            {
                ReportOfYearViewEntity reportOfYearViewEntity = (from reportOfYear in sourceList where reportOfYear.Year == i select reportOfYear).FirstOrDefault();

                resultList.Add(reportOfYearViewEntity);
            }

            return resultList;
        }

        public List<ProjectOfReportOfYearViewEntity> GetProjectInfoByWhereCondition(string whereCondition, int pageCurrent, int pageSize, out int dataCount)
        {
            return da.GetProjectInfoByWhereCondition(whereCondition, pageCurrent, pageSize, out dataCount);
        }

        public ProjectOfReportOfYearViewEntity GetProjectInfoByWhereCondition(string whereCondition)
        {
            return da.GetProjectInfoByWhereCondition(whereCondition);
        }

        public Dictionary<int, List<ProjectCountOfProjectTypeViewEntity>> GetProjectCountOfProjectTypeBetweenYear(int startYear, int endYear)
        {
            Dictionary<int, List<ProjectCountOfProjectTypeViewEntity>> dictionaryResult = new Dictionary<int, List<ProjectCountOfProjectTypeViewEntity>>();

            for (int i = startYear; i <= endYear; i++)
            {
                List<ProjectCountOfProjectTypeViewEntity> projectCountOfProjectTypeViewEntityList = da.GetProjectCountOfProjectTypeByYear(i);

                dictionaryResult.Add(i, projectCountOfProjectTypeViewEntityList);
            }
            return dictionaryResult;
        }

        public int GetProjectCountOfProjectTypeByYear(int year, string projectType, string unit)
        {
            return da.GetProjectCountOfProjectTypeByYear(year, projectType, unit);
        }

        public Dictionary<int, ProjectScaleViewEntity> GetProjectScaleBetweenYear(int startYear, int endYear,string unit)
        {
            Dictionary<int, ProjectScaleViewEntity> resultDictionary = new Dictionary<int, ProjectScaleViewEntity>();

            for (int i = startYear; i <= endYear; i++)
            {
                resultDictionary.Add(i, da.GetProjectScaleByYear(i,unit));
            }

            return resultDictionary;
        }

        public Dictionary<int, ProjectStatusViewEntity> GetProjectStatusCountByBetweenYear(int startYear, int endYear,string unit)
        {
            Dictionary<int, ProjectStatusViewEntity> resultDictionary = new Dictionary<int, ProjectStatusViewEntity>();

            for (int i = startYear; i <= endYear; i++)
            {
                resultDictionary.Add(i, da.GetProjectStatusCountByByYear(i,unit));
            }
            return resultDictionary;
        }

        /// <summary>
        /// 根据用户SysNoString查询用户角色名和用户名
        /// </summary>
        /// <param name="userSysNoString"></param>
        /// <returns></returns>
        public List<List<string>> GetProjectAuditUserRoleAndName(int[] userSysNoArray)
        {
            List<List<string>> resultList = new List<List<string>>();

            resultList.Add(da.GetProjectAuditConfigProcessDescription());

            resultList.Add(da.GetProjectAuditUserName(userSysNoArray));

            return resultList;
        }
    }
}
