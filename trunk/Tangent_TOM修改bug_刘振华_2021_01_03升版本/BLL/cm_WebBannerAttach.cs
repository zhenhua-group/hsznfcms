﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_WebBannerAttach
	/// </summary>
	public partial class cm_WebBannerAttach
	{
		private readonly TG.DAL.cm_WebBannerAttach dal=new TG.DAL.cm_WebBannerAttach();
		public cm_WebBannerAttach()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TG.Model.cm_WebBannerAttach model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_WebBannerAttach model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int sysno)
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.Delete(sysno);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_WebBannerAttach GetModel()
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.GetModel();
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_WebBannerAttach GetModelByCache()
		{
			//该表无主键信息，请自定义主键/条件字段
			string CacheKey = "cm_WebBannerAttachModel-" ;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel();
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_WebBannerAttach)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_WebBannerAttach> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_WebBannerAttach> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_WebBannerAttach> modelList = new List<TG.Model.cm_WebBannerAttach>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_WebBannerAttach model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_WebBannerAttach();
					if(dt.Rows[n]["SysNo"]!=null && dt.Rows[n]["SysNo"].ToString()!="")
					{
						model.SysNo=int.Parse(dt.Rows[n]["SysNo"].ToString());
					}
					if(dt.Rows[n]["RelativePath"]!=null && dt.Rows[n]["RelativePath"].ToString()!="")
					{
					model.RelativePath=dt.Rows[n]["RelativePath"].ToString();
					}
					if(dt.Rows[n]["FileName"]!=null && dt.Rows[n]["FileName"].ToString()!="")
					{
					model.FileName=dt.Rows[n]["FileName"].ToString();
					}
					if(dt.Rows[n]["FileType"]!=null && dt.Rows[n]["FileType"].ToString()!="")
					{
					model.FileType=dt.Rows[n]["FileType"].ToString();
					}
					if(dt.Rows[n]["FileSize"]!=null && dt.Rows[n]["FileSize"].ToString()!="")
					{
						model.FileSize=int.Parse(dt.Rows[n]["FileSize"].ToString());
					}
					if(dt.Rows[n]["UpLoadDate"]!=null && dt.Rows[n]["UpLoadDate"].ToString()!="")
					{
						model.UpLoadDate=DateTime.Parse(dt.Rows[n]["UpLoadDate"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

