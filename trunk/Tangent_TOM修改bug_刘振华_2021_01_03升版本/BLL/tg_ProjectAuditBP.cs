﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using System.Data;

namespace TG.BLL
{
    public static class tg_ProjectAuditBP
    {
        private static TG.DAL.tg_ProjectAuditDA dal = new TG.DAL.tg_ProjectAuditDA();

        public static TG.Model.ProjectAuditDataEntity GetProjectAuditEntity(int projectAuditSysNo)
        {
            return dal.GetProjectAuditEntity(projectAuditSysNo);
        }
        //
        public static TG.Model.ProjectAuditEditEntity GetProjectAuditEditEntity(int projectAuditSysNo)
        {
            return dal.GetProjectAuditEditEntity(projectAuditSysNo);
        }
        //
        public static int UpdateProjectAudit(ProjectAuditDataEntity dataEntity)
        {
            return dal.UpdateProjectAudit(dataEntity);
        }
        //
        public static int UpdateProjectAuditEdit(ProjectAuditEditEntity dataEntity)
        {
            return dal.UpdateProjectAuditEdit(dataEntity);
        }

        //
        /// <summary>
        /// 根据审核状态查询RoleSysNo
        /// </summary>
        /// <param name="auditStatus"></param>
        /// <returns></returns>
        public static int GetProcessRoleSysNo(string auditStatus)
        {
            return dal.GetProcessRoleSysNo(auditStatus);
        }

        /// <summary>
        /// 新规一条审核记录
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public static int InsertProjectAudit(ProjectAuditDataEntity dataEntity)
        {
            return dal.InsertProjectAudit(dataEntity);
        }
        //
        public static int InsertProjectAuditEdit(ProjectAuditEditEntity dataEntity)
        {
            return dal.InsertProjectAuditEdit(dataEntity);
        }
        //
        //
        public static int InsertProjectValueAuditEdit(cm_ProjectVolltAuditEdit dataEntity)
        {
            return dal.InsertProjectValueAuditEdit(dataEntity);
        }
        /// <summary>
        /// 新规一个项目
        /// </summary>
        /// <param name="sourceProject">项目源信息载体</param>
        /// <param name="xmlPath">建项规则XML路径</param>
        /// <returns></returns>
        public static int CreateProject(TG.Model.cm_Project sourceProject, string xmlPath, string isoPath, string userid, string ftpPath)
        {
            return dal.CreateProject(sourceProject, xmlPath, isoPath, userid, ftpPath);
        }
        public static int UpdateProject(TG.Model.cm_Project sourceProject, string xmlPath, string isoPath, string userid, string ftpPath, bool isfixed)
        {
            return dal.UpdateProject(sourceProject, xmlPath, isoPath, userid, ftpPath, isfixed);
        }
        /// <summary>
        /// 修复一个项目
        /// </summary>
        /// <param name="sourceProject">项目源信息载体</param>
        /// <param name="xmlPath">建项规则XML路径</param>
        /// <returns></returns>
        public static int CreateProject(TG.Model.cm_Project sourceProject, string xmlPath, string isoPath, string userid, string ftpPath, bool isfixed)
        {
            return dal.CreateProject(sourceProject, xmlPath, isoPath, userid, ftpPath, isfixed);
        }
        public static int UpdateProjectName(string projectNameB, string projectNameA, string ftpPath)
        {
            return dal.UpdateProjectName(projectNameB, projectNameA, ftpPath);
        }
        /// <summary>
        /// 新规一个项目工号信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public static int InsertProjectJobNumber(cm_ProjectJobNumber dataEntity)
        {
            return dal.InsertProjectJobNumber(dataEntity);
        }

        /// <summary>
        /// 得到项目审核工号列表
        /// </summary>
        /// <returns></returns>
        public static DataSet GetDistribuionJobNumberList()
        {
            return dal.GetDistribuionJobNumberList();
        }

        //通过评审的项目数
        public static int ProjectCount()
        {
            return dal.ProjectCount();
        }
        /// <summary>
        /// 判断是否提交审核
        /// </summary>
        /// <param name="projectNo"></param>
        /// <returns></returns>
        public static int CheckAudit(int projectNo)
        {
            return dal.CheckAudit(projectNo);
        }

        /// 获取通过评审的项目
        /// </summary>
        /// <returns></returns>
        public static DataSet GetList()
        {
            return dal.GetList();
        }
        /// <summary>
        /// 获取审批通过项目信息
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public static DataSet GetList(string where)
        {
            return dal.GetList(where);
        }
        /// <summary>
        /// 获取审批通过项目信息
        /// </summary>
        /// <param name="Top"></param>
        /// <param name="strWhere"></param>
        /// <param name="fieldOrder"></param>
        /// <returns></returns>
        public static DataSet GetList(int Top, string strWhere, string fieldOrder)
        {
            return dal.GetList(Top, strWhere, fieldOrder);
        }
        /// <summary>
        /// 根据用户SysNo得到用户Name
        /// </summary>
        /// <param name="userSysNoArray"></param>
        /// <returns></returns>
        public static string[] GetUserNameArray(string[] userSysNoArray)
        {
            return dal.GetUserNameArray(userSysNoArray);
        }
    }
}
