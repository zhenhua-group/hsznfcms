﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_ProjectCharge
	/// </summary>
	public partial class cm_ProjectCharge
	{
		private readonly TG.DAL.cm_ProjectCharge dal=new TG.DAL.cm_ProjectCharge();
		public cm_ProjectCharge()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_ProjectCharge model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ProjectCharge model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			return dal.Delete(ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			return dal.DeleteList(IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ProjectCharge GetModel(int ID)
		{
			
			return dal.GetModel(ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_ProjectCharge GetModelByCache(int ID)
		{
			
			string CacheKey = "cm_ProjectChargeModel-" + ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_ProjectCharge)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ProjectCharge> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ProjectCharge> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_ProjectCharge> modelList = new List<TG.Model.cm_ProjectCharge>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_ProjectCharge model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_ProjectCharge();
					if(dt.Rows[n]["ID"]!=null && dt.Rows[n]["ID"].ToString()!="")
					{
						model.ID=int.Parse(dt.Rows[n]["ID"].ToString());
					}
					if(dt.Rows[n]["projID"]!=null && dt.Rows[n]["projID"].ToString()!="")
					{
						model.projID=int.Parse(dt.Rows[n]["projID"].ToString());
					}
					if(dt.Rows[n]["cprID"]!=null && dt.Rows[n]["cprID"].ToString()!="")
					{
						model.cprID=int.Parse(dt.Rows[n]["cprID"].ToString());
					}
					if(dt.Rows[n]["Acount"]!=null && dt.Rows[n]["Acount"].ToString()!="")
					{
						model.Acount=decimal.Parse(dt.Rows[n]["Acount"].ToString());
					}
					if(dt.Rows[n]["FromUser"]!=null && dt.Rows[n]["FromUser"].ToString()!="")
					{
					model.FromUser=dt.Rows[n]["FromUser"].ToString();
					}
					if(dt.Rows[n]["InAcountUser"]!=null && dt.Rows[n]["InAcountUser"].ToString()!="")
					{
					model.InAcountUser=dt.Rows[n]["InAcountUser"].ToString();
					}
					if(dt.Rows[n]["InAcountTime"]!=null && dt.Rows[n]["InAcountTime"].ToString()!="")
					{
						model.InAcountTime=DateTime.Parse(dt.Rows[n]["InAcountTime"].ToString());
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
					model.Status=dt.Rows[n]["Status"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
        //获取合同额和已收合同额
        public DataSet GetCoperationChargeCount(string query,string start,string end)
        {
            return dal.GetCoperationChargeCount(query,start,end);
        }
        //获取项目合同收费
        public DataSet GetCoperationAcountCount(string query)
        {
            return dal.GetCoperationAcountCount(query);
        }
		#endregion  Method
	}
}

