﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;

namespace TG.BLL
{

    public partial class SetUnitChargeAllot
    {
        private readonly TG.DAL.SetUnitChargeAllot dal = new TG.DAL.SetUnitChargeAllot();
        public SetUnitChargeAllot()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.SetUnitChargeAllot model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.SetUnitChargeAllot model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.SetUnitChargeAllot GetModel(int ID)
        {

            return dal.GetModel(ID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.SetUnitChargeAllot GetModelByCache(int ID)
        {

            string CacheKey = "SetUnitChargeAllotModel-" + ID;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(ID);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.SetUnitChargeAllot)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.SetUnitChargeAllot> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.SetUnitChargeAllot> DataTableToList(DataTable dt)
        {
            List<TG.Model.SetUnitChargeAllot> modelList = new List<TG.Model.SetUnitChargeAllot>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.SetUnitChargeAllot model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.SetUnitChargeAllot();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["UnitID"] != null && dt.Rows[n]["UnitID"].ToString() != "")
                    {
                        model.UnitID = int.Parse(dt.Rows[n]["UnitID"].ToString());
                    }
                    if (dt.Rows[n]["UnitAllot"] != null && dt.Rows[n]["UnitAllot"].ToString() != "")
                    {
                        model.UnitAllot = decimal.Parse(dt.Rows[n]["UnitAllot"].ToString());
                    }
                    if (dt.Rows[n]["AllotYear"] != null && dt.Rows[n]["AllotYear"].ToString() != "")
                    {
                        model.AllotYear = dt.Rows[n]["AllotYear"].ToString();
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = int.Parse(dt.Rows[n]["Status"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetUnitAllotListByPager(string strWhere, int startIndex, int endIndex)
        {
            return dal.GetUnitAllotListByPager(strWhere, startIndex, endIndex);
        }
        #endregion  Method
    }
}
