﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// tg_ProjectExtendInfo
	/// </summary>
	public partial class tg_ProjectExtendInfo
	{
		private readonly TG.DAL.tg_ProjectExtendInfo dal = new TG.DAL.tg_ProjectExtendInfo();
		public tg_ProjectExtendInfo()
		{ }
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ProExtendInfoid)
		{
			return dal.Exists(ProExtendInfoid);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.tg_ProjectExtendInfo model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_ProjectExtendInfo model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ProExtendInfoid)
		{

			return dal.Delete(ProExtendInfoid);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string ProExtendInfoidlist)
		{
			return dal.DeleteList(ProExtendInfoidlist);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_ProjectExtendInfo GetModel(int ProExtendInfoid)
		{

			return dal.GetModel(ProExtendInfoid);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.tg_ProjectExtendInfo GetModelByCache(int ProExtendInfoid)
		{

			string CacheKey = "tg_ProjectExtendInfoModel-" + ProExtendInfoid;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ProExtendInfoid);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch { }
			}
			return (TG.Model.tg_ProjectExtendInfo)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top, string strWhere, string filedOrder)
		{
			return dal.GetList(Top, strWhere, filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_ProjectExtendInfo> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.tg_ProjectExtendInfo> DataTableToList(DataTable dt)
		{
			List<TG.Model.tg_ProjectExtendInfo> modelList = new List<TG.Model.tg_ProjectExtendInfo>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.tg_ProjectExtendInfo model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.tg_ProjectExtendInfo();
					if (dt.Rows[n]["ProExtendInfoid"] != null && dt.Rows[n]["ProExtendInfoid"].ToString() != "")
					{
						model.ProExtendInfoid = int.Parse(dt.Rows[n]["ProExtendInfoid"].ToString());
					}
					if (dt.Rows[n]["project_Id"] != null && dt.Rows[n]["project_Id"].ToString() != "")
					{
						model.project_Id = int.Parse(dt.Rows[n]["project_Id"].ToString());
					}
					if (dt.Rows[n]["Pro_src"] != null && dt.Rows[n]["Pro_src"].ToString() != "")
					{
						model.Pro_src = int.Parse(dt.Rows[n]["Pro_src"].ToString());
					}
					if (dt.Rows[n]["ChgJia"] != null && dt.Rows[n]["ChgJia"].ToString() != "")
					{
						model.ChgJia = dt.Rows[n]["ChgJia"].ToString();
					}
					if (dt.Rows[n]["Phone"] != null && dt.Rows[n]["Phone"].ToString() != "")
					{
						model.Phone = dt.Rows[n]["Phone"].ToString();
					}
					if (dt.Rows[n]["Project_reletive"] != null && dt.Rows[n]["Project_reletive"].ToString() != "")
					{
						model.Project_reletive = dt.Rows[n]["Project_reletive"].ToString();
					}
					if (dt.Rows[n]["Cpr_Acount"] != null && dt.Rows[n]["Cpr_Acount"].ToString() != "")
					{
						model.Cpr_Acount = decimal.Parse(dt.Rows[n]["Cpr_Acount"].ToString());
					}
					if (dt.Rows[n]["Unit"] != null && dt.Rows[n]["Unit"].ToString() != "")
					{
						model.Unit = dt.Rows[n]["Unit"].ToString();
					}
					if (dt.Rows[n]["ProjectScale"] != null && dt.Rows[n]["ProjectScale"].ToString() != "")
					{
						model.ProjectScale = decimal.Parse(dt.Rows[n]["ProjectScale"].ToString());
					}
					if (dt.Rows[n]["BuildAddress"] != null && dt.Rows[n]["BuildAddress"].ToString() != "")
					{
						model.BuildAddress = dt.Rows[n]["BuildAddress"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
		//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		/// <summary>
		/// 得到项目扩展信息
		/// </summary>
		/// <param name="ProjectExtendSysNo">项目扩展信息SysNo</param>
		/// <param name="ProjectSysNo">项目SysNo</param>
		/// <returns></returns>
		public TG.Model.tg_ProjectExtendInfo GetProjectExtendInfo(int ProjectExtendSysNo, int ProjectSysNo)
		{
			string whereSql = "";

			if (ProjectExtendSysNo != 0)
				whereSql += " AND ProExtendInfoid=" + ProjectExtendSysNo;
			if (ProjectSysNo != 0)
				whereSql += " AND project_Id=" + ProjectSysNo;

			return dal.GetProjectExtendInfo(whereSql);
		}

		#endregion  Method
	}
}

