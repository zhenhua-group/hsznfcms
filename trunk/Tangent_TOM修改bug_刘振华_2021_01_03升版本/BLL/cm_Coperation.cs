﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.Text;
namespace TG.BLL
{
    /// <summary>
    /// cm_Coperation
    /// </summary>
    public partial class cm_Coperation
    {
        private readonly TG.DAL.cm_Coperation dal = new TG.DAL.cm_Coperation();
        public cm_Coperation()
        { }
        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int cpr_Id)
        {
            return dal.Exists(cpr_Id);
        }
          /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string cprName,string sysno)
        {
            return dal.Exists(cprName, sysno);
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_Coperation model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_Coperation model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int cpr_Id)
        {

            return dal.Delete(cpr_Id);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public ArrayList DeleteList(string cpr_Idlist)
        {
            return dal.DeleteList(cpr_Idlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_Coperation GetModel(int cpr_Id)
        {

            return dal.GetModel(cpr_Id);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_Coperation GetModelByCache(int cpr_Id)
        {

            string CacheKey = "cm_CoperationModel-" + cpr_Id;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(cpr_Id);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_Coperation)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        public DataSet GetCoperationExportInfo(string strWhere)
        {
            return dal.GetCoperationExportInfo(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_Coperation> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        public List<TG.Model.cm_Coperation> GetModelListDistinct(string strWhere)
        {
            DataSet ds = dal.GetListDistinct(strWhere);
            return DataTableToListDistinct(ds.Tables[0]);
        }
        public List<TG.Model.cm_Coperation> DataTableToListDistinct(DataTable dt)
        {
            List<TG.Model.cm_Coperation> modelList = new List<TG.Model.cm_Coperation>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_Coperation model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_Coperation();
                    if (dt.Rows[n]["cpr_No"] != null && dt.Rows[n]["cpr_No"].ToString() != "")
                    {
                        model.cpr_No = dt.Rows[n]["cpr_No"].ToString();
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_Coperation> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_Coperation> modelList = new List<TG.Model.cm_Coperation>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_Coperation model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_Coperation();
                    if (dt.Rows[n]["cpr_Id"] != null && dt.Rows[n]["cpr_Id"].ToString() != "")
                    {
                        model.cpr_Id = int.Parse(dt.Rows[n]["cpr_Id"].ToString());
                    }
                    if (dt.Rows[n]["cst_Id"] != null && dt.Rows[n]["cst_Id"].ToString() != "")
                    {
                        model.cst_Id = int.Parse(dt.Rows[n]["cst_Id"].ToString());
                    }
                    if (dt.Rows[n]["cpr_No"] != null && dt.Rows[n]["cpr_No"].ToString() != "")
                    {
                        model.cpr_No = dt.Rows[n]["cpr_No"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Type"] != null && dt.Rows[n]["cpr_Type"].ToString() != "")
                    {
                        model.cpr_Type = dt.Rows[n]["cpr_Type"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Type2"] != null && dt.Rows[n]["cpr_Type2"].ToString() != "")
                    {
                        model.cpr_Type2 = dt.Rows[n]["cpr_Type2"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Name"] != null && dt.Rows[n]["cpr_Name"].ToString() != "")
                    {
                        model.cpr_Name = dt.Rows[n]["cpr_Name"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Unit"] != null && dt.Rows[n]["cpr_Unit"].ToString() != "")
                    {
                        model.cpr_Unit = dt.Rows[n]["cpr_Unit"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Acount"] != null && dt.Rows[n]["cpr_Acount"].ToString() != "")
                    {
                        model.cpr_Acount = decimal.Parse(dt.Rows[n]["cpr_Acount"].ToString());
                    }
                    if (dt.Rows[n]["cpr_ShijiAcount"] != null && dt.Rows[n]["cpr_ShijiAcount"].ToString() != "")
                    {
                        model.cpr_ShijiAcount = decimal.Parse(dt.Rows[n]["cpr_ShijiAcount"].ToString());
                    }
                    if (dt.Rows[n]["cpr_Touzi"] != null && dt.Rows[n]["cpr_Touzi"].ToString() != "")
                    {
                        model.cpr_Touzi = decimal.Parse(dt.Rows[n]["cpr_Touzi"].ToString());
                    }
                    if (dt.Rows[n]["cpr_ShijiTouzi"] != null && dt.Rows[n]["cpr_ShijiTouzi"].ToString() != "")
                    {
                        model.cpr_ShijiTouzi = decimal.Parse(dt.Rows[n]["cpr_ShijiTouzi"].ToString());
                    }
                    if (dt.Rows[n]["cpr_Process"] != null && dt.Rows[n]["cpr_Process"].ToString() != "")
                    {
                        model.cpr_Process = dt.Rows[n]["cpr_Process"].ToString();
                    }
                    if (dt.Rows[n]["cpr_SignDate"] != null && dt.Rows[n]["cpr_SignDate"].ToString() != "")
                    {
                        model.cpr_SignDate = DateTime.Parse(dt.Rows[n]["cpr_SignDate"].ToString());
                    }
                    if (dt.Rows[n]["cpr_DoneDate"] != null && dt.Rows[n]["cpr_DoneDate"].ToString() != "")
                    {
                        model.cpr_DoneDate = DateTime.Parse(dt.Rows[n]["cpr_DoneDate"].ToString());
                    }
                    if (dt.Rows[n]["BuildType"] != null && dt.Rows[n]["BuildType"].ToString() != "")
                    {
                        model.BuildType = dt.Rows[n]["BuildType"].ToString();
                    }
                    if (dt.Rows[n]["cpr_Mark"] != null && dt.Rows[n]["cpr_Mark"].ToString() != "")
                    {
                        model.cpr_Mark = dt.Rows[n]["cpr_Mark"].ToString();
                    }
                    if (dt.Rows[n]["BuildArea"] != null && dt.Rows[n]["BuildArea"].ToString() != "")
                    {
                        model.BuildArea = dt.Rows[n]["BuildArea"].ToString();
                    }
                    if (dt.Rows[n]["ChgPeople"] != null && dt.Rows[n]["ChgPeople"].ToString() != "")
                    {
                        model.ChgPeople = dt.Rows[n]["ChgPeople"].ToString();
                    }
                    if (dt.Rows[n]["ChgPhone"] != null && dt.Rows[n]["ChgPhone"].ToString() != "")
                    {
                        model.ChgPhone = dt.Rows[n]["ChgPhone"].ToString();
                    }
                    if (dt.Rows[n]["ChgJia"] != null && dt.Rows[n]["ChgJia"].ToString() != "")
                    {
                        model.ChgJia = dt.Rows[n]["ChgJia"].ToString();
                    }
                    if (dt.Rows[n]["ChgJiaPhone"] != null && dt.Rows[n]["ChgJiaPhone"].ToString() != "")
                    {
                        model.ChgJiaPhone = dt.Rows[n]["ChgJiaPhone"].ToString();
                    }
                    if (dt.Rows[n]["BuildPosition"] != null && dt.Rows[n]["BuildPosition"].ToString() != "")
                    {
                        model.BuildPosition = dt.Rows[n]["BuildPosition"].ToString();
                    }
                    if (dt.Rows[n]["Industry"] != null && dt.Rows[n]["Industry"].ToString() != "")
                    {
                        model.Industry = dt.Rows[n]["Industry"].ToString();
                    }
                    if (dt.Rows[n]["BuildUnit"] != null && dt.Rows[n]["BuildUnit"].ToString() != "")
                    {
                        model.BuildUnit = dt.Rows[n]["BuildUnit"].ToString();
                    }
                    if (dt.Rows[n]["BuildSrc"] != null && dt.Rows[n]["BuildSrc"].ToString() != "")
                    {
                        model.BuildSrc = dt.Rows[n]["BuildSrc"].ToString();
                    }
                    if (dt.Rows[n]["TableMaker"] != null && dt.Rows[n]["TableMaker"].ToString() != "")
                    {
                        model.TableMaker = dt.Rows[n]["TableMaker"].ToString();
                    }
                    if (dt.Rows[n]["RegTime"] != null && dt.Rows[n]["RegTime"].ToString() != "")
                    {
                        model.RegTime = DateTime.Parse(dt.Rows[n]["RegTime"].ToString());
                    }
                    if (dt.Rows[n]["UpdateBy"] != null && dt.Rows[n]["UpdateBy"].ToString() != "")
                    {
                        model.UpdateBy = dt.Rows[n]["UpdateBy"].ToString();
                    }
                    if (dt.Rows[n]["LastUpdate"] != null && dt.Rows[n]["LastUpdate"].ToString() != "")
                    {
                        model.LastUpdate = DateTime.Parse(dt.Rows[n]["LastUpdate"].ToString());
                    }
                    //统计年份 zxq 20151027
                    if (dt.Rows[0]["cpr_SignDate2"] != null && dt.Rows[0]["cpr_SignDate2"].ToString() != "")
                    {
                        model.cpr_SignDate2 = DateTime.Parse(dt.Rows[0]["cpr_SignDate2"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListByPageProc(query, startIndex, endIndex);
        }
        /// <summary>
        /// 返回需要分页的记录数
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageProcCount(string query)
        {
            return dal.GetListPageProcCount(query);
        }
        /// <summary>
        /// 分阶段获得数据-不用RowNumber
        /// </summary>
        /// <param name="strWhere">where语句</param>
        /// <param name="pagesize">每页数量</param>
        /// <param name="pageIndex">要得到第几页的数据</param>
        /// <returns>数据集</returns>
        public DataSet GetListByPage(string strWhere, int pagesize, int pageIndex, string orderBy)
        {
            return dal.GetListByPage(strWhere, pagesize, pageIndex, orderBy);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        /// <param name="coperationName">合同名称</param>
        /// <param name="pageSize">没页大小</param>
        /// <param name="pageCurrent">当前页</param>
        /// <returns></returns>
        public List<TG.Model.cm_Coperation> GetCoperationList(string coperationName, int pageSize, int pageCurrent)
        {
            return dal.GetCoperationList(coperationName, pageSize, pageCurrent);
        }

        /// <summary>
        /// 获取所有合同年份
        /// </summary>
        /// <returns></returns>
        public List<string> GetCoperationYear()
        {
            return dal.GetCoperationYear();
        }
        /// <summary>
        /// 获取所有合同统计年份
        /// </summary>
        /// <returns></returns>
        public List<string> GetCoperationYear2()
        {
            return dal.GetCoperationYear2();
        }
        /// <summary>
        /// 统计合同列表，目标值，本月收费，年度收费
        /// </summary>
        public SqlDataReader GetCoperationChargeAndYearAllot(string query, string firstday, string lastday, string year, int startIndex, int endIndex)
        {
            return dal.GetCoperationChargeAndYearAllot(query, firstday, lastday, year, startIndex, endIndex);
        }

        /// <summary>
        /// 解析项目结构字符串
        /// </summary>
        /// <param name="structString">待解析的项目结构字符串</param>
        /// <returns></returns>
        public Dictionary<string, List<string>> ResolveProjectStruct(string structString)
        {
            //声明字典
            Dictionary<string, List<string>> resultDictionray = new Dictionary<string, List<string>>();
            if (string.IsNullOrEmpty(structString))
            {
                List<string> list = new List<string>();
                list.Add("");
                resultDictionray.Add("", list);
            }
            else
            {
                string[] sturctCategoryArray = structString.Trim().Split('+');
                for (int i = 0; i < sturctCategoryArray.Length; i++)
                {
                    string inputString = sturctCategoryArray[i];
                    if (!string.IsNullOrEmpty(inputString.Trim()))
                    {
                        //截取大类名称
                        int indexOfCategoryName = inputString.IndexOf('^');
                        string categoryName = inputString.Substring(0, indexOfCategoryName);

                        //分割小类
                        inputString = inputString.Substring(indexOfCategoryName);
                        string[] subCategoryNameArray = inputString.Split('^');

                        List<string> subCategoryNameList = new List<string>();
                        //获取小类
                        foreach (string subCategoryName in subCategoryNameArray)
                        {
                            if (!string.IsNullOrEmpty(subCategoryName))
                                subCategoryNameList.Add(subCategoryName);
                        }
                        resultDictionray.Add(categoryName, subCategoryNameList);
                    }
                }
            }
            return resultDictionray;
        }
        /// <summary>
        /// 拼接前台字符串
        /// </summary>
        /// <param name="dictionaryCategory"></param>
        /// <returns></returns>
        public string CreateULHTML(Dictionary<string, List<string>> dictionaryCategory)
        {
            string ulString = "";
            ulString += "<div class=\"menu\"><ul>";
            foreach (KeyValuePair<string, List<string>> Category in dictionaryCategory)
            {
                ulString += "<li><a href=\"#\">" + Category.Key + "<table><tr><td nowrap>";
                ulString += "<ul>";
                foreach (string subCategory in Category.Value)
                {
                    ulString += "<li><a href=\"#\">" + subCategory + "</a></li>";
                }
                ulString += "</ul>";
                ulString += "</td></tr></table></a></li>";
            }
            ulString += "</ul></div>";
            return ulString;
        }

        /// <summary>
        /// 项目产值系数设置是否编辑
        /// </summary>
        /// <param name="CprID"></param>
        /// <param name="IsEidtStatus"></param>
        /// <returns></returns>
        public int UpdateProjectValueParamterIsEidt(int CprID, int IsEidtStatus)
        {
            return dal.UpdateProjectValueParamterIsEidt(CprID, IsEidtStatus);
        }

        /// <summary>
        /// 统计合同合同，收款，目标值，完成比例
        /// </summary>
        public DataSet CountCoperationTarget(string year, string startyear,string endyear, string starttime, string endtime, int? unitid)
        {
            return dal.CountCoperationTarget(year, startyear, endyear, starttime, endtime, unitid);
        }
        /// <summary>
        /// 统计合同合同，收款，目标值，完成比例
        /// </summary>
        public DataSet CountCoperationTargetBySql(string year, string sqlwhere, string starttime, string endtime, string strWhere)
        {
            return dal.CountCoperationTargetBySql(year, sqlwhere, starttime, endtime, strWhere);
        }
        /// <summary>
        /// 合同综合统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <param name="type">0查询生产部门，1表示全院</param>
        /// <returns></returns>
        public DataSet CountCoperationZong(string year, string starttime, string endtime, int? unitid, string type)
        {
            return dal.CountCoperationZong(year, starttime, endtime, unitid, type);
        }
        /// <summary>
        /// 统计合同按季度统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountCoperationJD(string year, string starttime, string endtime, int? unitid, string type)
        {
            return dal.CountCoperationJD(year, starttime, endtime, unitid, type);
        }
        /// <summary>
        /// 统计合同按月份统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountCoperationMonth(string year, string starttime, string endtime, int? unitid, string type)
        {
            return dal.CountCoperationMonth(year, starttime, endtime, unitid, type);
        }
        /// <summary>
        /// 统计合同按类型统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountCoperationType(string year, string starttime, string endtime, int? unitid, string type)
        {
            return dal.CountCoperationType(year, starttime, endtime, unitid, type);
        }
        /// <summary>
        /// 合同类型综合统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public DataSet CountCoperationTypeBySql(string year, string starttime, string endtime, string strWhere, int type)
        {
            return dal.CountCoperationTypeBySql(year, starttime, endtime, strWhere, type);
        }
        /// <summary>
        /// 合同审批情况统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountCoperationAudit(string year, string starttime, string endtime, int? unitid)
        {
            return dal.CountCoperationAudit(year, starttime, endtime, unitid);
        }
        /// <summary>
        /// 项目审批情况统计
        /// </summary>
        public DataSet CountCoperationAuditBySql(string year, string starttime, string endtime, string strWhere)
        {
            return dal.CountCoperationAuditBySql(year, starttime, endtime, strWhere);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountCoperationLevel(string year, string starttime, string endtime, int? unitid)
        {
            return dal.CountCoperationLevel(year, starttime, endtime, unitid);
        }
        /// <summary>
        /// 合同等级统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountCoperationLevelBySql(string year, string starttime, string endtime, string strWhere)
        {
            return dal.CountCoperationLevelBySql(year, starttime, endtime, strWhere);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public DataSet CountCoperationType(string year, string starttime, string endtime, int? unitid, int type)
        {
            return dal.CountCoperationType(year, starttime, endtime, unitid, type);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public DataSet CountCoperationIndustry(string year, string starttime, string endtime, int? unitid, int type)
        {
            return dal.CountCoperationIndustry(year, starttime, endtime, unitid, type);
        }

        /// <summary>
        /// 综合统计项目性质
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public DataSet CountCoperationIndustryBySql(string year, string starttime, string endtime, string strWhere, int type)
        {
            return dal.CountCoperationIndustryBySql(year, starttime, endtime, strWhere, type);
        }
        public void UpdateByEdit(string p, int copsysno)
        {
            TG.Model.cm_Coperation model = dal.GetModel(copsysno);
            p = p.Replace("|*|", "\n");
            string[] arratList = p.Split('\n');
            for (int i = 0; i < arratList.Length; i++)
            {
                string typestr = arratList[i].Split(':')[0];
                switch (typestr)
                {
                    case "cpr_No":
                        model.cpr_No = arratList[i].Split(':')[1];
                        break;
                    case "cpr_Type":
                        model.cpr_Type = returnName(arratList[i].Split(':')[1]);
                        break;
                    case "cpr_Type2":
                        model.cpr_Type2 = arratList[i].Split(':')[1];
                        break;
                    case "cpr_Name":
                        model.cpr_Name = arratList[i].Split(':')[1];
                        break;
                    case "BuildArea":
                        model.BuildArea = arratList[i].Split(':')[1];
                        break;
                    case "BuildUnit":
                        model.BuildUnit = arratList[i].Split(':')[1];
                        break;
                    case "cpr_Area":
                        model.BuildArea = arratList[i].Split(':')[1];
                        break;
                    ////
                    case "StructType":
                        model.StructType = GetXML("StructType", arratList[i].Split(':')[1]);
                        //arratList[i].Split(':')[1];
                        break;
                    case "BuildStructType":
                        model.BuildStructType = GetXML("BuildType", arratList[i].Split(':')[1]);
                        break;
                    case "BuildTypelevel":
                        model.BuildType = returnName(arratList[i].Split(':')[1]);
                        break;
                    //////
                    case "ChgPeople":
                        model.ChgPeople = arratList[i].Split(':')[1];
                        break;
                    case "ChgPhone":
                        model.ChgPhone = arratList[i].Split(':')[1];
                        break;
                    case "HiddenPMUserID":
                        model.PMUserID = int.Parse(arratList[i].Split(':')[1]);
                        break;

                    case "floor":
                        model.Floor = arratList[i].Split(':')[1];
                        break;
                    case "ChgJia":
                        model.ChgJia = arratList[i].Split(':')[1];
                        break;
                    case "ChgJiaPhone":
                        model.ChgJiaPhone = arratList[i].Split(':')[1];
                        break;
                    case "cpr_Unit":
                        model.cpr_Unit = arratList[i].Split(':')[1];
                        break;
                    case "cpr_Acount":
                        model.cpr_Acount = Convert.ToDecimal(arratList[i].Split(':')[1].Length < 1 ? "0" : arratList[i].Split(':')[1]);
                        break;
                    case "cpr_Touzi":
                        model.cpr_Touzi = Convert.ToDecimal(arratList[i].Split(':')[1].Length < 1 ? "0" : arratList[i].Split(':')[1]);
                        break;
                    case "BuildPosition":
                        model.BuildPosition = arratList[i].Split(':')[1];
                        break;
                    case "cpr_ShijiAcount":
                        model.cpr_ShijiAcount = Convert.ToDecimal(arratList[i].Split(':')[1].Length < 1 ? "0" : arratList[i].Split(':')[1]);
                        break;
                    case "cpr_ShijiTouzi":
                        model.cpr_ShijiTouzi = Convert.ToDecimal(arratList[i].Split(':')[1].Length < 1 ? "0" : arratList[i].Split(':')[1]);
                        break;
                    case "Industry":
                        model.Industry = returnName(arratList[i].Split(':')[1]);
                        break;
                    //
                    case "cpr_Process":
                        model.cpr_Process = returnPross(arratList[i].Split(':')[1]);
                        break;
                    //
                    case "TableMaker":
                        model.TableMaker = arratList[i].Split(':')[1];
                        break;
                    case "BuildSrc":
                        model.BuildSrc = returnName(arratList[i].Split(':')[1]);
                        break;
                    case "cpr_SignDate":
                        model.cpr_SignDate = Convert.ToDateTime(arratList[i].Split(':')[1].Length < 1 ? DateTime.Now.ToString("yyyy-MM-dd") : arratList[i].Split(':')[1]);
                        break;
                    case "cpr_DoneDate":
                        model.cpr_DoneDate = Convert.ToDateTime(arratList[i].Split(':')[1].Length < 1 ? DateTime.Now.ToString("yyyy-MM-dd") : arratList[i].Split(':')[1]);
                        break;
                    case "cpr_SignDate2":
                        model.cpr_SignDate2 = Convert.ToDateTime(arratList[i].Split(':')[1].Length < 1 ? DateTime.Now.ToString("yyyy-MM-dd") : arratList[i].Split(':')[1]);
                        break;
                    case "MultiBuild":
                        model.MultiBuild = arratList[i].Split(':')[1];
                        break;
                    case "cpr_Mark":
                        model.cpr_Mark = arratList[i].Split(':')[1];
                        break;
                }
            }
            dal.Update(model);
        }

        public string returnName(string pross)
        {
            string result = "";
            if (pross.Trim() != "")
            {
                string[] array = pross.Split(new char[] { ',' }, StringSplitOptions.None);
                TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

                for (int i = 0; i < array.Length; i++)
                {
                    result += bll_dic.GetModel(int.Parse(array[i])).dic_Name + ",";
                }
                result = result.IndexOf(",") > -1 ? result.Remove(result.Length - 1) : "";
            }
            return result;
        }
        public string GetXML(string type, string p)
        {
            StringBuilder builder = new StringBuilder();
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + type + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                //一级
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                Readxml(nodes, out builder, p);
            }
            if (builder.ToString().Length > 0)
            {
                return builder.ToString().Substring(1);
            }
            else
            {
                return builder.ToString();
            }
            //公共建筑^办公+工业建筑^重工厂房^轻工厂房
        }
        //第一级别
        public void Readxml(XmlNodeList xmlnl, out StringBuilder sb_, string p)
        {
            sb_ = new StringBuilder();
            string[] arraylist = p.Split(',');
            foreach (XmlNode xl in xmlnl)
            {
                XmlNodeList nodes = xl.ChildNodes;
                for (int i = 0; i < arraylist.Length; i++)
                {
                    if (xl.Attributes["name"].Value == arraylist[i])
                    {
                        if (xl.ChildNodes.Count != 0)
                        {
                            sb_.Append("+" + xl.Attributes["name"].Value);
                            ReadxmlChild(nodes, sb_, p);
                        }
                        else
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                        }
                    }
                }
            }
        }
        //第二级别和一下的级别
        public void ReadxmlChild(XmlNodeList xmlnl, StringBuilder sb_, string p)
        {

            string[] arraylist = p.Split(',');
            foreach (XmlNode xl in xmlnl)
            {
                XmlNodeList nodes = xl.ChildNodes;
                for (int i = 0; i < arraylist.Length; i++)
                {
                    if (xl.Attributes["name"].Value == arraylist[i])
                    {
                        if (xl.ChildNodes.Count != 0)
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                            ReadxmlChild(nodes, sb_, p);
                        }
                        else
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                        }
                    }
                }
            }
        }
        public string returnPross(string proess)
        {
            string result = "";
            string[] array = proess.Split(new char[] { ',' }, StringSplitOptions.None);
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

            for (int i = 0; i < array.Length - 1; i++)
            {
                result += bll_dic.GetModel(array[i]).ID + ",";
            }
            result = result.IndexOf(",") > -1 ? result.Remove(result.Length - 1) : "";
            return result;
        }
        //合同收款明细
        public DataSet GetCoperationListExport(string sqlwhere)
        {
            return dal.GetCoperationListExport(sqlwhere);
        }
        //合同收款明细
        public DataSet GetScrCoperationListExport(string sqlwhere, string sbTime, string sbCharge)
        {
            return dal.GetScrCoperationListExport(sqlwhere, sbTime, sbCharge);
        }
        /// <summary>
        /// 合同审批导出
        /// </summary>
        /// <param name="sqlwhere"></param>
        /// <returns></returns>
        public DataSet GetCoperationAuditListExport(string sqlwhere)
        {
            return dal.GetCoperationAuditListExport(sqlwhere);
        }
        /// <summary>
        /// 合同修改审批导出
        /// </summary>
        /// <param name="sqlwhere"></param>
        /// <returns></returns>
        public DataSet GetCoperationAuditEditListExport(string sqlwhere)
        {
            return dal.GetCoperationAuditEditListExport(sqlwhere);
        }
        /// <summary>
        /// 合同收费导出
        /// </summary>
        /// <param name="sqlwhere"></param>
        /// <returns></returns>
        public DataSet GetCoperationChargeExport(string sqlwhere, string str)
        {
            return dal.GetCoperationChargeExport(sqlwhere, str);
        }
    }
}

