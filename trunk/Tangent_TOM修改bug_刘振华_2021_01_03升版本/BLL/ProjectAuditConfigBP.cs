﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DAL;
using TG.Model;

namespace TG.BLL
{
	public class ProjectAuditConfigBP
	{
		private ProjectAuditConfigDA da = new ProjectAuditConfigDA();

		/// <summary>
		/// 得到项目审核列表
		/// </summary>
		/// <returns></returns>
		public List<ProjectAuditConfigViewEntity> GetProjectAuditConfigViewEntity()
		{
			//取得项目审核情报集合
			List<ProjectAuditConfigViewEntity> projectAuditConfigList = da.GetProjectAuditConfigViewEntity();

			projectAuditConfigList.ForEach((projectAuditConfig) =>
			{
				projectAuditConfig.UserInfoList = new List<TG.Model.tg_member>();

				string[] userSysNoArray = projectAuditConfig.UserSysNoArrayString.Split(',');
				foreach (var userSysNo in userSysNoArray)
				{
					TG.Model.tg_member userInfo = GetUserInfo(int.Parse(userSysNo));
					//得到用户信息
					if (userInfo != null)
					{
						projectAuditConfig.UserInfoList.Add(userInfo);
					}
				}
			});
			return projectAuditConfigList;
		}

		/// <summary>
		/// 查询用户信息
		/// </summary>
		/// <param name="userSysNo"></param>
		/// <returns></returns>
		public TG.Model.tg_member GetUserInfo(int userSysNo)
		{
			return new TG.BLL.tg_member().GetModel(userSysNo);
		}

		public int UpdateProjectAuditConfigToRelationship(AuditConfigEntity auditConfigEntity)
		{
			return da.UpdateProjectAuditConfigToRelationship(auditConfigEntity);
		}

		/// <summary>
		/// 根据索引位置得到项目审核配置
		/// </summary>
		/// <param name="position"></param>
		/// <returns></returns>
		public ProjectAuditConfigViewEntity GetProjectRoleSysNoByPoisition(int position)
		{
			string whereSql = " and Position=" + position;

			return da.GetProjectConfigViewEntity(whereSql);
		}
	}
}
