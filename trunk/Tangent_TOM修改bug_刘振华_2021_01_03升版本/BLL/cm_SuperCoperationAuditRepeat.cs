﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_SuperCoperationAuditRepeat
	/// </summary>
	public partial class cm_SuperCoperationAuditRepeat
	{
		private readonly TG.DAL.cm_SuperCoperationAuditRepeat dal=new TG.DAL.cm_SuperCoperationAuditRepeat();
		public cm_SuperCoperationAuditRepeat()
		{}
		#region  Method
      
		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SysNo)
		{
			return dal.Exists(SysNo);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_SuperCoperationAuditRepeat model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_SuperCoperationAuditRepeat model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SysNo)
		{
			
			return dal.Delete(SysNo);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string SysNolist )
		{
			return dal.DeleteList(SysNolist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_SuperCoperationAuditRepeat GetModel(int SysNo)
		{
			
			return dal.GetModel(SysNo);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_SuperCoperationAuditRepeat GetModelByCache(int SysNo)
		{
			
			string CacheKey = "cm_SuperCoperationAuditRepeatModel-" + SysNo;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(SysNo);
					if (objModel != null)
					{
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_SuperCoperationAuditRepeat)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_SuperCoperationAuditRepeat> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_SuperCoperationAuditRepeat> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_SuperCoperationAuditRepeat> modelList = new List<TG.Model.cm_SuperCoperationAuditRepeat>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_SuperCoperationAuditRepeat model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_SuperCoperationAuditRepeat();
					if(dt.Rows[n]["SysNo"]!=null && dt.Rows[n]["SysNo"].ToString()!="")
					{
						model.SysNo=int.Parse(dt.Rows[n]["SysNo"].ToString());
					}
					if(dt.Rows[n]["CoperationSysNo"]!=null && dt.Rows[n]["CoperationSysNo"].ToString()!="")
					{
						model.CoperationSysNo=int.Parse(dt.Rows[n]["CoperationSysNo"].ToString());
					}
					if(dt.Rows[n]["options"]!=null && dt.Rows[n]["options"].ToString()!="")
					{
					model.options=dt.Rows[n]["options"].ToString();
					}
					if(dt.Rows[n]["Status"]!=null && dt.Rows[n]["Status"].ToString()!="")
					{
					model.Status=dt.Rows[n]["Status"].ToString();
					}
					if(dt.Rows[n]["Suggestion"]!=null && dt.Rows[n]["Suggestion"].ToString()!="")
					{
					model.Suggestion=dt.Rows[n]["Suggestion"].ToString();
					}
					if(dt.Rows[n]["AuditUser"]!=null && dt.Rows[n]["AuditUser"].ToString()!="")
					{
					model.AuditUser=dt.Rows[n]["AuditUser"].ToString();
					}
					if(dt.Rows[n]["AuditDate"]!=null && dt.Rows[n]["AuditDate"].ToString()!="")
					{
					model.AuditDate=dt.Rows[n]["AuditDate"].ToString();
					}
					if(dt.Rows[n]["InDate"]!=null && dt.Rows[n]["InDate"].ToString()!="")
					{
						model.InDate=DateTime.Parse(dt.Rows[n]["InDate"].ToString());
					}
					if(dt.Rows[n]["InUser"]!=null && dt.Rows[n]["InUser"].ToString()!="")
					{
						model.InUser=int.Parse(dt.Rows[n]["InUser"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
        //新建合同审批流程
        public int InsertCoperatioAuditRecord(TG.Model.cm_SuperCoperationAuditRepeat coperation)
        {
            return dal.InsertCoperatioAuditRecord(coperation);
        }

        public int SendMsg(TG.Model.cm_SysMsg sysMsg)
        {
            return dal.SendMsg(sysMsg);
        }

        public int UpdateCoperatioAuditRecord(TG.Model.cm_SuperCoperationAuditRepeat coperation)
        {
            return dal.UpdateCoperatioAuditRecord(coperation);
        }

        /// <summary>
        ///  判断是否存在此条审核记录
        /// </summary>
        /// <param name="coperationSysNo"></param>
        /// <returns></returns>
        public bool IsExist(int coperationSysNo)
        {
            if (dal.IsExist(coperationSysNo) > 0)
                return true;
            else
                return false;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_SuperCoperationAuditRepeat GetModelBySuperCoperationSysNo(int coperationSysNo)
        {
            return dal.GetModelBySuperCoperationSysNo(coperationSysNo);
        }
	}
}

