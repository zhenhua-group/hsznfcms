﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public class cm_CoperationAudit
    {
        private TG.DAL.cm_CoperationAudit dal = new TG.DAL.cm_CoperationAudit();
        //新建合同审批流程
        public int InsertCoperatioAuditRecord(TG.Model.cm_CoperationAudit coperation)
        {
            return dal.InsertCoperatioAuditRecord(coperation);
        }

        public int SendMsg(TG.Model.cm_SysMsg sysMsg)
        {
            return dal.SendMsg(sysMsg);
        }

        public int UpdateCoperatioAuditRecord(TG.Model.cm_CoperationAudit coperation)
        {
            return dal.UpdateCoperatioAuditRecord(coperation);
        }
        public TG.Model.cm_CoperationAudit GetModel(int SysNo)
        {
            return dal.GetModel(SysNo);
        }

        public TG.Model.cm_CoperationAudit GetModelByCoperationSysNo(int coperationSysNo)
        {
            return dal.GetModelByCoperationSysNo(coperationSysNo);
        }

        /// <summary>
        ///  判断是否存在此条审核记录
        /// </summary>
        /// <param name="coperationSysNo"></param>
        /// <returns></returns>
        public bool IsExist(int coperationSysNo)
        {
            if (dal.IsExist(coperationSysNo) > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// 查询合同审核记录列表
        /// </summary>
        /// <param name="coperationSysNo"></param>
        /// <returns></returns>
        public List<TG.Model.cm_CoperationAuditListView> GetCoperationListView()
        {
            //查询合同基本信息
            List<TG.Model.cm_CoperationAuditListView> coperationAuditList = dal.GetCoperationListView(string.Empty);
            //查询合同审核记录
            foreach (var coperation in coperationAuditList)
            {
                TG.Model.cm_CoperationAudit coperationAuditEntity = dal.GetModelByCoperationSysNo(coperation.cpr_Id);
                coperation.AuditRecordSysNo = coperationAuditEntity == null ? 0 : coperationAuditEntity.SysNo;
                coperation.AuditStatus = coperationAuditEntity == null ? "" : coperationAuditEntity.Status;
            }
            return coperationAuditList;
        }

        /// <summary>
        /// 查询合同审核记录列表
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<TG.Model.cm_CoperationAuditListView> GetCoperationListView(string query, int startIndex, int endIndex)
        {
            //查询合同基本信息
            List<TG.Model.cm_CoperationAuditListView> coperationAuditList = dal.GetCoperationListView(query, startIndex, endIndex);
            //查询合同审核记录
            foreach (var coperation in coperationAuditList)
            {
                TG.Model.cm_CoperationAudit coperationAuditEntity = dal.GetModelByCoperationSysNo(coperation.cpr_Id);
                coperation.CoperationAuditEntity = coperationAuditEntity;
                coperation.AuditRecordSysNo = coperationAuditEntity == null ? 0 : coperationAuditEntity.SysNo;
                coperation.AuditStatus = coperationAuditEntity == null ? "" : coperationAuditEntity.Status;
            }
            return coperationAuditList;
        }
        /// <summary>
        /// 查询合同审核记录列表
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<TG.Model.cm_CoperationAuditListView> GetCoperationListView(string whereSql)
        {
            //查询合同基本信息
            List<TG.Model.cm_CoperationAuditListView> coperationAuditList = dal.GetCoperationListView(whereSql);
            //查询合同审核记录
            foreach (var coperation in coperationAuditList)
            {
                TG.Model.cm_CoperationAudit coperationAuditEntity = dal.GetModelByCoperationSysNo(coperation.cpr_Id);

                coperation.CoperationAuditEntity = coperationAuditEntity;

                coperation.AuditRecordSysNo = coperationAuditEntity == null ? 0 : coperationAuditEntity.SysNo;
                coperation.AuditStatus = coperationAuditEntity == null ? "" : coperationAuditEntity.Status;
            }
            return coperationAuditList;
        }

        /// <summary>
        /// 查询合同审核记录列表方法重载-----修改使用
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<TG.Model.cm_CoperationAuditEditListView> GetCoperationEditListView(string query, int startIndex, int endIndex, string status)
        {
            //查询合同基本信息
            List<TG.Model.cm_CoperationAuditEditListView> coperationAuditList = dal.GetCoperationEditListView(query, startIndex, endIndex);
            //定义一个新的容器接收状态为J的审核记录

            //查询合同审核记录
            foreach (var coperation in coperationAuditList)
            {
                TG.Model.cm_CoperationAuditEdit coperationAuditEntity = dal.GetModelEditByCopno(coperation.cpr_Id);

                coperation.CoperationAuditEntity = coperationAuditEntity;
                coperation.AuditRecordSysNo = coperationAuditEntity == null ? 0 : coperationAuditEntity.SysNo;
                coperation.AuditStatus = coperationAuditEntity == null ? "" : coperationAuditEntity.Status.Trim();
            }
            return coperationAuditList;
        }
        /// <summary>
        /// 返回需要分页的记录数
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageProcCount(string query)
        {
            return dal.GetListPageProcCount(query);
        }
        /// <summary>
        /// 返回需要分页的记录数-申请修改
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageProcEditCount(string query)
        {
            return dal.GetListPageProcEditCount(query);
        }
    }
}
