﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.BLL
{
    /// <summary>
    /// cm_ProjectStageValueConfig
    /// </summary>
    public partial class cm_ProjectStageValueConfig
    {
        private readonly TG.DAL.cm_ProjectStageValueConfig dal = new TG.DAL.cm_ProjectStageValueConfig();
        public cm_ProjectStageValueConfig()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectStageValueConfig model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectStageValueConfig model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            return dal.Delete(ID);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            return dal.DeleteList(IDlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectStageValueConfig GetModel(int ID)
        {

            return dal.GetModel(ID);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_ProjectStageValueConfig GetModelByCache(int ID)
        {

            string CacheKey = "cm_ProjectStageValueConfigModel-" + ID;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(ID);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_ProjectStageValueConfig)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListAll()
        {
            return dal.GetListAll();
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectStageValueConfig> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectStageValueConfig> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_ProjectStageValueConfig> modelList = new List<TG.Model.cm_ProjectStageValueConfig>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_ProjectStageValueConfig model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_ProjectStageValueConfig();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["ProjectStatues"] != null && dt.Rows[n]["ProjectStatues"].ToString() != "")
                    {
                        model.ProjectStatues = int.Parse(dt.Rows[n]["ProjectStatues"].ToString());
                    }
                    if (dt.Rows[n]["ItemType"] != null && dt.Rows[n]["ItemType"].ToString() != "")
                    {
                        model.ItemType = dt.Rows[n]["ItemType"].ToString();
                    }
                    if (dt.Rows[n]["ProgramPercent"] != null && dt.Rows[n]["ProgramPercent"].ToString() != "")
                    {
                        model.ProgramPercent = decimal.Parse(dt.Rows[n]["ProgramPercent"].ToString());
                    }
                    if (dt.Rows[n]["preliminaryPercent"] != null && dt.Rows[n]["preliminaryPercent"].ToString() != "")
                    {
                        model.preliminaryPercent = decimal.Parse(dt.Rows[n]["preliminaryPercent"].ToString());
                    }
                    if (dt.Rows[n]["WorkDrawPercent"] != null && dt.Rows[n]["WorkDrawPercent"].ToString() != "")
                    {
                        model.WorkDrawPercent = decimal.Parse(dt.Rows[n]["WorkDrawPercent"].ToString());
                    }
                    if (dt.Rows[n]["LateStagePercent"] != null && dt.Rows[n]["LateStagePercent"].ToString() != "")
                    {
                        model.LateStagePercent = decimal.Parse(dt.Rows[n]["LateStagePercent"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  Method
    }
}
