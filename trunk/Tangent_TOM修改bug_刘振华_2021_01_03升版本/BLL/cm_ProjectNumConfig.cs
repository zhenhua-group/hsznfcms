﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_ProjectNumConfig
	/// </summary>
	public partial class cm_ProjectNumConfig
	{
		private readonly TG.DAL.cm_ProjectNumConfig dal=new TG.DAL.cm_ProjectNumConfig();
		public cm_ProjectNumConfig()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_ProjectNumConfig model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ProjectNumConfig model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			return dal.Delete(ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			return dal.DeleteList(IDlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ProjectNumConfig GetModel(int ID)
		{
			
			return dal.GetModel(ID);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_ProjectNumConfig GetModelByCache(int ID)
		{
			
			string CacheKey = "cm_ProjectNumConfigModel-" + ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_ProjectNumConfig)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ProjectNumConfig> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_ProjectNumConfig> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_ProjectNumConfig> modelList = new List<TG.Model.cm_ProjectNumConfig>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_ProjectNumConfig model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_ProjectNumConfig();
					if(dt.Rows[n]["ID"]!=null && dt.Rows[n]["ID"].ToString()!="")
					{
						model.ID=int.Parse(dt.Rows[n]["ID"].ToString());
					}
					if(dt.Rows[n]["PreFix"]!=null && dt.Rows[n]["PreFix"].ToString()!="")
					{
					model.PreFix=dt.Rows[n]["PreFix"].ToString();
					}
					if(dt.Rows[n]["StartNum"]!=null && dt.Rows[n]["StartNum"].ToString()!="")
					{
					model.StartNum=dt.Rows[n]["StartNum"].ToString();
					}
					if(dt.Rows[n]["EndNum"]!=null && dt.Rows[n]["EndNum"].ToString()!="")
					{
					model.EndNum=dt.Rows[n]["EndNum"].ToString();
					}
					if(dt.Rows[n]["Mark"]!=null && dt.Rows[n]["Mark"].ToString()!="")
					{
					model.Mark=dt.Rows[n]["Mark"].ToString();
					}
					if(dt.Rows[n]["ProType"]!=null && dt.Rows[n]["ProType"].ToString()!="")
					{
					model.ProType=dt.Rows[n]["ProType"].ToString();
					}
					if(dt.Rows[n]["Flag"]!=null && dt.Rows[n]["Flag"].ToString()!="")
					{
						model.Flag=int.Parse(dt.Rows[n]["Flag"].ToString());
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

