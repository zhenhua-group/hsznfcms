﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
using System.Data.SqlClient;
using System.Collections;
using System.Xml;
using System.Text;
namespace TG.BLL
{
    /// <summary>
    /// cm_Project
    /// </summary>
    public partial class cm_Project
    {
        private readonly TG.DAL.cm_Project dal = new TG.DAL.cm_Project();
        public cm_Project()
        { }
        #region  Method

        public DataSet GetAllYearList()
        {
            return dal.GetAllYearList();
        }
        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int bs_project_Id)
        {
            return dal.Exists(bs_project_Id);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_Project model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_Project model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int bs_project_Id)
        {

            return dal.Delete(bs_project_Id);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public ArrayList DeleteList(string bs_project_Idlist)
        {
            return dal.DeleteList(bs_project_Idlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_Project GetModel(int bs_project_Id)
        {
            return dal.GetModel(bs_project_Id);
        }


        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public TG.Model.cm_Project GetModelByCache(int bs_project_Id)
        {

            string CacheKey = "cm_ProjectModel-" + bs_project_Id;
            object objModel = TG.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(bs_project_Id);
                    if (objModel != null)
                    {
                        int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
                        TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (TG.Model.cm_Project)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        //获取项目策划通过项目列表
        public DataSet GetListPassPlanProject(int Top, string strWhere, string filedOrder)
        {
            return dal.GetListPassPlanProject(Top, strWhere, filedOrder);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_Project> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_Project> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_Project> modelList = new List<TG.Model.cm_Project>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_Project model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_Project();
                    if (dt.Rows[n]["pro_ID"] != null && dt.Rows[n]["pro_ID"].ToString() != "")
                    {
                        model.bs_project_Id = int.Parse(dt.Rows[n]["pro_ID"].ToString());
                    }
                    if (dt.Rows[n]["pro_order"] != null && dt.Rows[n]["pro_order"].ToString() != "")
                    {
                        model.pro_order = dt.Rows[n]["pro_order"].ToString();
                    }
                    if (dt.Rows[n]["pro_name"] != null && dt.Rows[n]["pro_name"].ToString() != "")
                    {
                        model.pro_name = dt.Rows[n]["pro_name"].ToString();
                    }
                    if (dt.Rows[n]["pro_kinds"] != null && dt.Rows[n]["pro_kinds"].ToString() != "")
                    {
                        model.pro_kinds = dt.Rows[n]["pro_kinds"].ToString();
                    }
                    if (dt.Rows[n]["pro_buildUnit"] != null && dt.Rows[n]["pro_buildUnit"].ToString() != "")
                    {
                        model.pro_buildUnit = dt.Rows[n]["pro_buildUnit"].ToString();
                    }
                    if (dt.Rows[n]["pro_status"] != null && dt.Rows[n]["pro_status"].ToString() != "")
                    {
                        model.pro_status = dt.Rows[n]["pro_status"].ToString();
                    }
                    if (dt.Rows[n]["pro_level"] != null && dt.Rows[n]["pro_level"].ToString() != "")
                    {
                        model.pro_level = int.Parse(dt.Rows[n]["pro_level"].ToString());
                    }
                    if (dt.Rows[n]["pro_startTime"] != null && dt.Rows[n]["pro_startTime"].ToString() != "")
                    {
                        model.pro_startTime = DateTime.Parse(dt.Rows[n]["pro_startTime"].ToString());
                    }
                    if (dt.Rows[n]["pro_finishTime"] != null && dt.Rows[n]["pro_finishTime"].ToString() != "")
                    {
                        model.pro_finishTime = DateTime.Parse(dt.Rows[n]["pro_finishTime"].ToString());
                    }
                    if (dt.Rows[n]["Pro_src"] != null && dt.Rows[n]["Pro_src"].ToString() != "")
                    {
                        model.Pro_src = int.Parse(dt.Rows[n]["Pro_src"].ToString());
                    }
                    if (dt.Rows[n]["ChgJia"] != null && dt.Rows[n]["ChgJia"].ToString() != "")
                    {
                        model.ChgJia = dt.Rows[n]["ChgJia"].ToString();
                    }
                    if (dt.Rows[n]["Phone"] != null && dt.Rows[n]["Phone"].ToString() != "")
                    {
                        model.Phone = dt.Rows[n]["Phone"].ToString();
                    }
                    if (dt.Rows[n]["Project_reletive"] != null && dt.Rows[n]["Project_reletive"].ToString() != "")
                    {
                        model.Project_reletive = dt.Rows[n]["Project_reletive"].ToString();
                    }
                    if (dt.Rows[n]["Cpr_Acount"] != null && dt.Rows[n]["Cpr_Acount"].ToString() != "")
                    {
                        model.Cpr_Acount = decimal.Parse(dt.Rows[n]["Cpr_Acount"].ToString());
                    }
                    if (dt.Rows[n]["Unit"] != null && dt.Rows[n]["Unit"].ToString() != "")
                    {
                        model.Unit = dt.Rows[n]["Unit"].ToString();
                    }
                    if (dt.Rows[n]["ProjectScale"] != null && dt.Rows[n]["ProjectScale"].ToString() != "")
                    {
                        model.ProjectScale = decimal.Parse(dt.Rows[n]["ProjectScale"].ToString());
                    }
                    if (dt.Rows[n]["BuildAddress"] != null && dt.Rows[n]["BuildAddress"].ToString() != "")
                    {
                        model.BuildAddress = dt.Rows[n]["BuildAddress"].ToString();
                    }
                    if (dt.Rows[n]["pro_Intro"] != null && dt.Rows[n]["pro_Intro"].ToString() != "")
                    {
                        model.pro_Intro = dt.Rows[n]["pro_Intro"].ToString();
                    }
                    if (dt.Rows[n]["CoperationSysNo"] != null && dt.Rows[n]["CoperationSysNo"].ToString() != "")
                    {
                        model.cprID = dt.Rows[n]["CoperationSysNo"].ToString();
                    }

                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }
        /// <summary>
        /// 返回需要分页的记录数
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageProcCount(string query)
        {
            return dal.GetListPageProcCount(query);
        }
        /// <summary>
        /// 申请修改返回需要分页的记录数
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageEditProcCount(string query)
        {
            return dal.GetListPageEditProcCount(query);
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListByPageProc(query, startIndex, endIndex);
        }
        /// <summary>
        /// 申请修改分页获取数据列表
        /// </summary>
        public SqlDataReader GetListEditByPageProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListEditByPageProc(query, startIndex, endIndex);
        }
        /// <summary>
        /// 审批通过的项目 20130808 long
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object GetListPageAuditProcCount(string query)
        {
            return dal.GetListPageAuditProcCount(query);
        }
        /// <summary>
        /// 分页获取数据列表 审批通过的项目 20130808 long
        /// </summary>
        public SqlDataReader GetListByPageAuditProc(string query, int startIndex, int endIndex)
        {
            return dal.GetListByPageAuditProc(query, startIndex, endIndex);
        }


        public DataSet GetListByWhere(string strWhere)
        {
            return dal.GetListByWhere(strWhere);
        }
        //20130508 long
        public DataSet GetListByWhere(int Top, string strWhere, string fileOrder)
        {
            return dal.GetListByWhere(Top, strWhere, fileOrder);
        }
        /*
          edit bu Sago 2012.10.17
         */
        public int GetSetUpProjectCount(string whereSql)
        {
            return dal.GetSetUpProjectCount(whereSql);
        }


        /// <summary>
        /// 解析项目结构字符串
        /// </summary>
        /// <param name="structString">待解析的项目结构字符串</param>
        /// <returns></returns>
        public Dictionary<string, List<string>> ResolveProjectStruct(string structString)
        {


            Dictionary<string, List<string>> resultDictionray = new Dictionary<string, List<string>>();
            if (string.IsNullOrEmpty(structString))
            {
                List<string> list = new List<string>();
                list.Add("");

                resultDictionray.Add("", list);
            }
            else
            {
                string[] sturctCategoryArray = structString.Trim().Split('+');

                for (int i = 0; i < sturctCategoryArray.Length; i++)
                {
                    string inputString = sturctCategoryArray[i];
                    if (!string.IsNullOrEmpty(inputString.Trim()))
                    {
                        //截取大类名称
                        int indexOfCategoryName = inputString.IndexOf('^');
                        string categoryName = inputString.Substring(0, indexOfCategoryName);

                        //分割小类
                        inputString = inputString.Substring(indexOfCategoryName);
                        string[] subCategoryNameArray = inputString.Split('^');

                        List<string> subCategoryNameList = new List<string>();
                        //获取小类
                        foreach (string subCategoryName in subCategoryNameArray)
                        {
                            if (!string.IsNullOrEmpty(subCategoryName))
                                subCategoryNameList.Add(subCategoryName);
                        }
                        resultDictionray.Add(categoryName, subCategoryNameList);
                    }
                }
            }

            return resultDictionray;
        }

        /// <summary>
        /// 拼接前台字符串
        /// </summary>
        /// <param name="dictionaryCategory"></param>
        /// <returns></returns>
        public string CreateULHTML(Dictionary<string, List<string>> dictionaryCategory)
        {
            string ulString = "";
            ulString += "<div class=\"menu\"><ul>";
            foreach (KeyValuePair<string, List<string>> Category in dictionaryCategory)
            {
                ulString += "<li><a href=\"#\">" + Category.Key + "<table><tr><td nowrap>";
                ulString += "<ul>";
                foreach (string subCategory in Category.Value)
                {
                    ulString += "<li><a href=\"#\">" + subCategory + "</a></li>";
                }
                ulString += "</ul>";
                ulString += "</td></tr></table></a></li>";
            }
            ulString += "</ul></div>";
            return ulString;
        }


        public bool ExistsInTGProject(string projectName,string sysno)
        {
            return dal.ExistsInTGProject(projectName,sysno);
        }

        //获取合同所有年份
        public List<string> GetProjectYear()
        {
            return dal.GetProjectYear();
        }
        /// <summary>
        /// 按季度统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountProjectByJD(string year, int? unitid, string type)
        {
            return dal.CountProjectByJD(year, unitid, type);
        }
        /// <summary>
        /// 按月份统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountProjectByMonth(string year, int? unitid, string type)
        {
            return dal.CountProjectByMonth(year, unitid, type);
        }
        /// <summary>
        /// 按性质统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountProjectByIndustry(string year, int? unitid, string type)
        {
            return dal.CountProjectByIndustry(year, unitid, type);
        }
        /// <summary>
        /// 按项目状态
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountProjectByStatus(string year, int? unitid, string type)
        {
            return dal.CountProjectByStatus(year, unitid, type);
        }
        /// <summary>
        /// 按项目状态
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountProjectByLevel(string year, int? unitid)
        {
            return dal.CountProjectByLevel(year, unitid);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountProjectBySrc(string year, int? unitid)
        {
            return dal.CountProjectBySrc(year, unitid);
        }
        /// <summary>
        ///  自定义查询
        /// </summary>
        /// <param name="year"></param>
        /// <param name="type"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public DataSet CountProjectByCustom(string timewhere, string type, string where)
        {
            return dal.CountProjectByCustom(timewhere, type, where);
        }
        #endregion  Method
        public void UpdateByEdit(int pro_id, string opinions)
        {
            TG.BLL.cm_Coperation cop_bll = new TG.BLL.cm_Coperation();
            TG.Model.cm_Project model = dal.GetModel(pro_id);
            bool IsRevite = false;
            string cpr_ID = "";
            opinions = opinions.Replace("|*|", "\n");
            string[] arratList = opinions.Split('\n');
            for (int i = 0; i < arratList.Length; i++)
            {
                string typestr = arratList[i].Split(':')[0];
                switch (typestr)
                {
                    case "pro_name":
                        model.pro_name = arratList[i].Split(':')[1];
                        break;
                    case "cpr_coper":
                        IsRevite = true;
                        cpr_ID = arratList[i].Split(':')[1];
                        model.cprID = arratList[i].Split(':')[1];
                        break;
                    case "cpr_copername":
                        model.Project_reletive = arratList[i].Split(':')[1];
                        break;
                    case "pro_level":
                        model.pro_level = int.Parse(arratList[i].Split(':')[1]);
                        break;
                    case "AuditLevel":
                        model.AuditLevel = GetAuditLevel(arratList[i].Split(':')[1]);
                        break;
                    case "ProjType":
                        model.pro_StruType = arratList[i].Split(':')[1];
                        break;
                    case "ProjectScale":
                        model.ProjectScale = Convert.ToDecimal(arratList[i].Split(':')[1].Length < 1 ? "0" : arratList[i].Split(':')[1]);
                        break;
                    case "pro_buildUnit":
                        model.pro_buildUnit = arratList[i].Split(':')[1];
                        break;
                    case "ProjArea":
                        model.Cpr_Acount = Convert.ToDecimal(arratList[i].Split(':')[1].Length < 1 ? "0" : arratList[i].Split(':')[1]);
                        break;
                    case "pro_StruType":
                        model.pro_StruType = GetXML("StructType", arratList[i].Split(':')[1]);
                        break;
                    case "pro_kinds":
                        model.pro_kinds = GetXML("BuildType", arratList[i].Split(':')[1]);
                        break;
                    //alter
                    case "BuildTypelevel":
                        model.BuildType = returnName(arratList[i].Split(':')[1]);
                        break;
                    case "PMName":
                        model.PMName = arratList[i].Split(':')[1];
                        break;
                    case "PMPhone":
                        model.PMPhone = arratList[i].Split(':')[1];
                        break;
                    case "PMNameID":
                        model.PMUserID = int.Parse(arratList[i].Split(':')[1]);
                        break;
                    case "ChgJia":
                        model.ChgJia = arratList[i].Split(':')[1];
                        break;
                    case "Phone":
                        model.Phone = arratList[i].Split(':')[1];
                        break;
                    case "Unit":
                        model.Unit = arratList[i].Split(':')[1];
                        break;
                    case "cpr_Acount":
                        model.Cpr_Acount = Convert.ToDecimal(arratList[i].Split(':')[1].Length < 1 ? "0" : arratList[i].Split(':')[1]);
                        break;
                    case "BuildAddress":
                        model.BuildAddress = arratList[i].Split(':')[1];
                        break;
                    case "Industry":
                        model.Industry = returnName(arratList[i].Split(':')[1]);
                        break;
                    case "pro_status":
                        model.pro_status = arratList[i].Split(':')[1];
                        //修改TCD阶段信息
                        AddTgProjectStatus(model);
                        break;
                    case "Pro_src":
                        model.Pro_src = int.Parse(arratList[i].Split(':')[1]);
                        break;
                    case "cpr_join":
                        if (arratList[i].Split(',')[0].Split(':')[2] == "1")
                        {
                            model.ISTrunEconomy = "1";
                        }
                        else
                        {
                            model.ISTrunEconomy = "0";
                        }
                        if (arratList[i].Split(',')[1].Split(':')[1] == "1")
                        {
                            model.ISHvac = "1";
                        }
                        else
                        {
                            model.ISHvac = "0";
                        }
                        if (arratList[i].Split(',')[2].Split(':')[1] == "1")
                        {
                            model.ISArch = "1";

                        }
                        else
                        {
                            model.ISArch = "0";
                        }
                        break;
                    case "pro_startTime":
                        model.pro_startTime = Convert.ToDateTime(arratList[i].Split(':')[1].Length < 1 ? DateTime.Now.ToString("yyyy-MM-dd") : arratList[i].Split(':')[1]);
                        break;
                    case "pro_finishTime":
                        model.pro_finishTime = Convert.ToDateTime(arratList[i].Split(':')[1].Length < 1 ? DateTime.Now.ToString("yyyy-MM-dd") : arratList[i].Split(':')[1]);
                        break;
                    case "ProjSub":
                        model.ProjSub = arratList[i].Split(':')[1];
                        break;
                    case "pro_Intro":
                        model.pro_Intro = arratList[i].Split(':')[1];
                        break;

                }

            }
            if (IsRevite)
            {
                TG.Model.cm_Coperation modelcop = cop_bll.GetModel(int.Parse(cpr_ID));
                model.BuildAddress = modelcop.BuildPosition;
                model.ProjectScale = Convert.ToDecimal(modelcop.BuildArea);
                model.Cpr_Acount = modelcop.cpr_Acount;
                model.Unit = modelcop.cpr_Unit;
        
            }
            //更新项目cm里的项目表
            dal.Update(model);

        }

        private string GetAuditLevel(string p)
        {
            //throw new NotImplementedException();
            string level = "";
            if (p == "0")
            {
                level = "1,0";
            }
            else if (p == "1")
            {
                level = "0,1";
            }
            else
            {
                level = "1,1";
            }
            return level;
        }
        //添加TCD项目阶段信息
        public void AddTgProjectStatus(TG.Model.cm_Project sourceProject)
        {
            //TCD项目ID
            int refrenceSysNo = sourceProject.ReferenceSysNo;
            //阶段信息
            string[] proStatus = sourceProject.pro_status.Split(',');

            if (proStatus.Length > 0)
            {
                TG.DAL.cm_Project dal = new TG.DAL.cm_Project();
                foreach (string status in proStatus)
                {
                    if (status.Trim() != "")
                    {
                        dal.UpdateTgProjectStatus(refrenceSysNo.ToString(), status);
                    }
                }
            }
        }
        public string returnName(string pross)
        {
            string result = "";
            if (pross.Trim() != "")
            {
                string[] array = pross.Split(new char[] { ',' }, StringSplitOptions.None);
                TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

                for (int i = 0; i < array.Length; i++)
                {
                    result += bll_dic.GetModel(int.Parse(array[i])).dic_Name + ",";
                }
                result = result.IndexOf(",") > -1 ? result.Remove(result.Length - 1) : "";
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public string GetXML(string type, string p)
        {
            StringBuilder builder = new StringBuilder();
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + type + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                //一级
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                Readxml(nodes, out builder, p);
            }
            if (builder.ToString().Length > 0)
            {
                return builder.ToString().Substring(1);
            }
            else
            {
                return builder.ToString();
            }
            //公共建筑^办公+工业建筑^重工厂房^轻工厂房
        }
        //第一级别
        public void Readxml(XmlNodeList xmlnl, out StringBuilder sb_, string p)
        {
            sb_ = new StringBuilder();
            string[] arraylist = p.Split(',');
            foreach (XmlNode xl in xmlnl)
            {
                XmlNodeList nodes = xl.ChildNodes;
                for (int i = 0; i < arraylist.Length; i++)
                {
                    if (xl.Attributes["name"].Value == arraylist[i])
                    {
                        if (xl.ChildNodes.Count != 0)
                        {
                            sb_.Append("+" + xl.Attributes["name"].Value);
                            ReadxmlChild(nodes, sb_, p);
                        }
                        else
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                        }
                    }
                }
            }
        }
        //第二级别和一下的级别
        public void ReadxmlChild(XmlNodeList xmlnl, StringBuilder sb_, string p)
        {

            string[] arraylist = p.Split(',');
            foreach (XmlNode xl in xmlnl)
            {
                XmlNodeList nodes = xl.ChildNodes;
                for (int i = 0; i < arraylist.Length; i++)
                {
                    if (xl.Attributes["name"].Value == arraylist[i])
                    {
                        if (xl.ChildNodes.Count != 0)
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                            ReadxmlChild(nodes, sb_, p);
                        }
                        else
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 得出导出的数据
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectExportInfo(string strWhere)
        {
            return dal.GetProjectExportInfo(strWhere);
        }
        public DataSet GetScrProjectExportInfo(string strWhere)
        {
            return dal.GetScrProjectExportInfo(strWhere);
        }
        /// <summary>
        /// <summary>
        /// 项目产值分配查看-导出
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="strWhere2"></param>
        /// <returns></returns>
        public DataSet GetProjectValueShowExportInfo(string strWhere, string strWhere2)
        {
            return dal.GetProjectValueShowExportInfo(strWhere, strWhere2);
        }
        /// 得出导出的数据
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectEditExportInfo(string strWhere)
        {
            return dal.GetProjectEditExportInfo(strWhere);
        }
        /// <summary>
        /// 得出导出的数据--项目工号
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectNumberExportInfo(string strWhere)
        {
            return dal.GetProjectNumberExportInfo(strWhere);
        }
        /// <summary>
        /// 得出导出的数据--未策划
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectNoPlanExportInfo(string strWhere)
        {
            return dal.GetProjectNoPlanExportInfo(strWhere);
        }
        /// <summary>
        /// 得出导出的数据--已策划
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectYesPlanExportInfo(string strWhere)
        {
            return dal.GetProjectYesPlanExportInfo(strWhere);
        }
        /// <summary>
        /// 得出导出的数据--项目进度审批表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectScheduleExportInfo(string strWhere)
        {
            return dal.GetProjectScheduleExportInfo(strWhere);
        }

        /// <summary>
        /// 分页获取数据列表 2016年5月12日
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表 2016年6月12日
        /// </summary>
        public DataSet GetListByPage(string strWhere,string strWhere2, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, strWhere2,orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表 2016年5月12日
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
    }
}

