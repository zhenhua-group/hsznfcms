﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.Model;

namespace TG.BLL
{
    public class CopContrast
    {
        #region 麻烦方法
        TG.DAL.tg_unit tgunit = new DAL.tg_unit();
        TG.BLL.cm_UnitAllot cmunitallot = new BLL.cm_UnitAllot();
        TG.BLL.cm_Coperation coper = new cm_Coperation();
        /// <summary>
        /// 主营集合
        /// </summary>
        /// <param name="unitId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>238/254
        public List<TG.Model.CopContrast> GetListduo(string unitId, int year, int month, string unitparentId)
        {
            List<TG.Model.CopContrast> list = GetListContrast(year, month, unitparentId);
            TG.Model.CopContrast model = new Model.CopContrast();
            model.Unit = "主营合计";
            foreach (TG.Model.CopContrast item in list)
            {
                model.AcountMonth += item.AcountMonth;
                model.AcountMonthNow += item.AcountMonthNow;
                model.AcountYear += item.AcountYear;
                model.AcountYearNow += item.AcountYearNow;
                model.AllotUnitLast += item.AllotUnitLast;
                model.AllotUnitNow += item.AllotUnitNow;

            }
            list.Add(model);
            return list;
        }

        /// <summary>
        /// 施工集合
        /// </summary>
        /// <param name="unitId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="unitparentId"></param>
        /// <returns></returns>
        public List<TG.Model.CopContrast> GetListshi(string unitId, int year, int month, string unitparentId)
        {
            List<TG.Model.CopContrast> list = GetListContrast(year, month, unitparentId);
            TG.Model.CopContrast model = new Model.CopContrast();
            model.Unit = "施工合计";
            foreach (TG.Model.CopContrast item in list)
            {
                model.AcountMonth += item.AcountMonth;
                model.AcountMonthNow += item.AcountMonthNow;
                model.AcountYear += item.AcountYear;
                model.AcountYearNow += item.AcountYearNow;
                model.AllotUnitLast += item.AllotUnitLast;
                model.AllotUnitNow += item.AllotUnitNow;

            }
            list.Add(model);
            return list;
        }
        /// <summary>
        /// 全院总合计
        /// </summary>
        /// <param name="GetListshi"></param>
        /// <param name="GetListduo"></param>
        /// <returns></returns>
        public TG.Model.CopContrast AllContrast(List<TG.Model.CopContrast> GetListshi, List<TG.Model.CopContrast> GetListduo)
        {

            TG.Model.CopContrast model = new Model.CopContrast();
            model.Unit = "全院总合计";
            foreach (TG.Model.CopContrast item in GetListduo)
            {
                if (item.Unit == "主营合计")
                {
                    foreach (TG.Model.CopContrast item2 in GetListshi)
                    {
                        model.AcountMonth = item.AcountMonth + item2.AcountMonth;
                        model.AcountMonthNow = item.AcountMonthNow + item2.AcountMonthNow;
                        model.AcountYear = item.AcountYear + item2.AcountYear;
                        model.AcountYearNow = item.AcountYearNow + item2.AcountYearNow;
                        model.AllotUnitLast = item.AllotUnitLast + item2.AllotUnitLast;
                        model.AllotUnitNow = item.AllotUnitNow + item2.AllotUnitNow;
                    }
                }
            }
            return model;
        }
        /// <summary>
        /// 得到数据集合
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="unitparentId"></param>
        /// <returns></returns>
        private List<Model.CopContrast> GetListContrast(int year, int month, string unitparentId)
        {
            List<TG.Model.CopContrast> modelcontrast = new List<Model.CopContrast>();
            DataSet dstgunit = tgunit.GetList("unit_ParentID=" + unitparentId);
            foreach (DataRow item in dstgunit.Tables[0].Rows)
            {
                TG.Model.CopContrast model = new Model.CopContrast();
                //单位
                model.Unit = item["unit_Name"].ToString();
                //本年的目标值
                string strallotnow = "UnitID=" + item["unit_ID"] + "AND AllotYear='" + DateTime.Now.Year + "'";
                model.AllotUnitNow = Mubiaozhi(strallotnow);
                //所对比的目标值
                string strallotlast = "UnitID=" + item["unit_ID"] + "AND AllotYear='" + year + "'";
                model.AllotUnitLast = Mubiaozhi(strallotnow);
                //所对比的年合同额
                string strUnit = "cpr_Unit='" + item["unit_Name"].ToString() + "'" + "AND cpr_SignDate>'" + year + "-01-01'AND cpr_SignDate<'" + year + "-12-31'";
                model.AcountYear = Convert.ToDecimal(HeTongZongH(strUnit));
                //今年合同额
                string strUnitnow = "cpr_Unit='" + item["unit_Name"].ToString() + "'" + "AND cpr_SignDate>'" + DateTime.Now.Year + "-01-01'AND cpr_SignDate<'" + DateTime.Now.Year + "-12-31'";
                model.AcountYearNow = Convert.ToDecimal(HeTongZongH(strUnitnow));
                //所对比的月合同额
                string strUnitmonthlast = "cpr_Unit='" + item["unit_Name"].ToString() + "'" + "AND cpr_SignDate>'" + year + "-" + month + "-01'AND cpr_SignDate<'" + year + "-" + (month == 12 ? 01 : month + 1) + "-01'";
                model.AcountMonth = Convert.ToDecimal(HeTongZongH(strUnitmonthlast));
                //本年的月合同额
                string strUnitmonthnow = "cpr_Unit='" + item["unit_Name"].ToString() + "'" + "AND cpr_SignDate>'" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-01'AND cpr_SignDate<'" + DateTime.Now.AddMonths(1) + "'";
                model.AcountMonthNow = Convert.ToDecimal(HeTongZongH(strUnitmonthnow));
                modelcontrast.Add(model);
            }
            return modelcontrast;
        }
        //目标值
        private int Mubiaozhi(string strwhere)
        {
            int Mubiao = 0;
            List<TG.Model.cm_UnitAllot> list = cmunitallot.GetModelList(strwhere);
            if (list.Count != 0 && list != null)
            {
                foreach (TG.Model.cm_UnitAllot item in list)
                {
                    Mubiao = Convert.ToInt32(item.UnitAllot);
                }
                return Mubiao;
            }
            else
            {
                return Mubiao;
            }
        }
        /// <summary>
        /// 合同额
        /// </summary>
        /// <param name="strwhere">条件</param>
        /// <returns></returns>
        private double HeTongZongH(string strwhere)
        {
            double d = 0.00;
            List<TG.Model.cm_Coperation> list = coper.GetModelList(strwhere);
            if (list.Count == 0 || list == null)
            {
                return d;
            }
            foreach (TG.Model.cm_Coperation item in list)
            {
                d += Convert.ToDouble(item.cpr_Acount);
            }
            return d;
        }
        #endregion

        TG.DAL.CopContrast dal = new DAL.CopContrast();

        /// <summary>
        /// 得到所有合同额 
        /// </summary>
        /// <param name="unitId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="endmonth"></param>
        /// <returns></returns>
        public List<TG.Model.CopContrast> GetList(string unitcheck, string unitId, string year, string beginTime, string endTime)
        {
            List<TG.Model.CopContrast> listAll = new List<Model.CopContrast>();
            if (unitId == "-1")
            {
                List<TG.Model.CopContrast> listFrist;
                TG.Model.CopContrast modelfrist;
                ZhuUnit(unitcheck, year, beginTime, endTime, out listFrist, out modelfrist);


                List<TG.Model.CopContrast> listSecond;
                TG.Model.CopContrast modelsecond;
                ShiUnit(unitcheck, year, beginTime, endTime, out listSecond, out modelsecond);

                TG.Model.CopContrast modelAll = Getzongji(modelfrist, modelsecond);
                listAll.AddRange(listFrist);
                listAll.AddRange(listSecond);
                listAll.Add(modelAll);
            }
            else
            {
                unitId = " a.unit_ID=" + unitId;
                listAll = dal.GetList(unitcheck, unitId, year, beginTime, endTime);
            }
            return listAll;
        }
        /// <summary>
        /// 施工合计
        /// </summary>
        /// <param name="year">年份</param>
        /// <param name="beginTime">开始月份</param>
        /// <param name="endTime"></param>
        /// <param name="listSecond"></param>
        /// <param name="modelsecond"></param>
        public void ShiUnit(string unitcheck, string year, string beginTime, string endTime, out List<TG.Model.CopContrast> listSecond, out TG.Model.CopContrast modelsecond)
        {
            listSecond = dal.GetList(unitcheck, "a.unit_ParentID=254", year, beginTime, endTime);
            modelsecond = GetHeji(listSecond, "施工合计");
            listSecond.Add(modelsecond);
        }
        /// <summary>
        /// 主要经营合计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="listFrist"></param>
        /// <param name="modelfrist"></param>
        public void ZhuUnit(string unitcheck, string year, string beginTime, string endTime, out List<TG.Model.CopContrast> listFrist, out TG.Model.CopContrast modelfrist)
        {
            listFrist = dal.GetList(unitcheck, "a.unit_ParentID=238", year, beginTime, endTime);
            modelfrist = GetHeji(listFrist, "主营合计");
            listFrist.Add(modelfrist);
        }
        /// <summary>
        /// 合同额
        /// </summary>
        /// <param name="list"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public TG.Model.CopContrast GetHeji(List<TG.Model.CopContrast> list, string str)
        {
            TG.Model.CopContrast model = new Model.CopContrast();
            model.Unit = str;
            foreach (TG.Model.CopContrast item in list)
            {
                model.AcountMonth += Convert.ToDecimal(item.AcountMonth);
                model.AcountMonthNow += Convert.ToDecimal(item.AcountMonthNow.ToString().Trim());
                model.AcountYear += Convert.ToDecimal(item.AcountYear.ToString().Trim());
                model.AcountYearNow += Convert.ToDecimal(item.AcountYearNow.ToString().Trim());
                model.AllotUnitLast += Convert.ToInt32(Convert.ToDecimal(item.AllotUnitLast.ToString().Trim()));
                model.AllotUnitNow += Convert.ToInt32(Convert.ToDecimal(item.AllotUnitNow.ToString().Trim()));

            }
            return model;
        }
        /// <summary>
        /// 总计合同额
        /// </summary>
        /// <param name="listfrist"></param>
        /// <param name="listSecond"></param>
        /// <returns></returns>
        public TG.Model.CopContrast Getzongji(TG.Model.CopContrast listfrist, TG.Model.CopContrast listSecond)
        {
            TG.Model.CopContrast Zongji = new Model.CopContrast();
            Zongji.Unit = "全院总合计";
            Zongji.AcountMonth = listfrist.AcountMonth + listSecond.AcountMonth;
            Zongji.AcountMonthNow = listfrist.AcountMonthNow + listSecond.AcountMonthNow;
            Zongji.AcountYear = listfrist.AcountYear + listSecond.AcountYear;
            Zongji.AcountYearNow = listfrist.AcountYearNow + listSecond.AcountYearNow;
            Zongji.AllotUnitLast = listfrist.AllotUnitLast + listSecond.AllotUnitLast;
            Zongji.AllotUnitNow = listfrist.AllotUnitNow + listSecond.AllotUnitNow;
            return Zongji;
        }

    }
}
