﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace TG.BLL
{
    public class cm_ProjectValueByMemberAuditStatus
    {

        private readonly TG.DAL.cm_ProjectValueByMemberAuditStatus dal = new TG.DAL.cm_ProjectValueByMemberAuditStatus();
        public cm_ProjectValueByMemberAuditStatus()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectValueByMemberAuditStatus model)
        {
            return dal.Add(model);
        }
        /// <summary>
        /// 新增人员审批表信息
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="pro_id"></param>
        /// <param name="allotID"></param>
        /// <param name="insertUser"></param>
        /// <returns></returns>
        public int AddAduitMemberUserInfo(DataTable dt, int pro_id, int? allotID, int insertUser)
        {
            return dal.AddAduitMemberUserInfo(dt, pro_id, allotID, insertUser);
        }
        /// <summary>
        /// 新增人员审批表信息-转
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="pro_id"></param>
        /// <param name="allotID"></param>
        /// <param name="insertUser"></param>
        /// <returns></returns>
        public int AddAduitTranMemberUserInfo(DataTable dt, int pro_id, int? allotID, int insertUser, string tranType)
        {
            return dal.AddAduitTranMemberUserInfo(dt, pro_id, allotID, insertUser, tranType);
        }
        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Update(TG.Model.ProjectValueByMemberAuditStatusEntity model)
        {
            return dal.Update(model);
        }
        /// <summary>
        /// 批量更新数据
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public int Update(DataTable dt)
        {
            return dal.Update(dt);
        }
        /// <summary>
        /// 更新状态-转
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int TranUpdate(TG.Model.ProjectValueByMemberAuditStatusEntity model, string tranType)
        {
            return dal.TranUpdate(model, tranType);
        }
        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Delete(TG.Model.ProjectValueByMemberAuditStatusEntity model)
        {
            return dal.Delete(model);
        }
        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Delete(int pro_id, int allotID)
        {
            return dal.Delete(pro_id, allotID);
        }
        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Delete(int pro_id, int allotID, int mem_id)
        {
            return dal.Delete(pro_id, allotID, mem_id);
        }
        /// <summary>
        /// 删除信息-转信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Delete(int pro_id, int allotID, string tranType)
        {
            return dal.Delete(pro_id, allotID, tranType);
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectValueByMemberAuditStatus GetModel(int pro_id, int allotID, int mem_id)
        {
            return dal.GetModel(pro_id, allotID, mem_id);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        /// 查询个人确认信息
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetListMemberStatus(string strWhere)
        {
            return dal.GetListMemberStatus(strWhere);
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectValueByMemberAuditStatus> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<TG.Model.cm_ProjectValueByMemberAuditStatus> DataTableToList(DataTable dt)
        {
            List<TG.Model.cm_ProjectValueByMemberAuditStatus> modelList = new List<TG.Model.cm_ProjectValueByMemberAuditStatus>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                TG.Model.cm_ProjectValueByMemberAuditStatus model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new TG.Model.cm_ProjectValueByMemberAuditStatus();
                    if (dt.Rows[n]["ID"] != null && dt.Rows[n]["ID"].ToString() != "")
                    {
                        model.ID = int.Parse(dt.Rows[n]["ID"].ToString());
                    }
                    if (dt.Rows[n]["proID"] != null && dt.Rows[n]["proID"].ToString() != "")
                    {
                        model.proID = int.Parse(dt.Rows[n]["proID"].ToString());
                    }
                    if (dt.Rows[n]["AllotID"] != null && dt.Rows[n]["AllotID"].ToString() != "")
                    {
                        model.AllotID = int.Parse(dt.Rows[n]["AllotID"].ToString());
                    }
                    if (dt.Rows[n]["mem_id"] != null && dt.Rows[n]["mem_id"].ToString() != "")
                    {
                        model.mem_id = int.Parse(dt.Rows[n]["mem_id"].ToString());
                    }
                    if (dt.Rows[n]["Status"] != null && dt.Rows[n]["Status"].ToString() != "")
                    {
                        model.Status = dt.Rows[n]["Status"].ToString();
                    }
                    if (dt.Rows[n]["AuditUser"] != null && dt.Rows[n]["AuditUser"].ToString() != "")
                    {
                        model.AuditUser = int.Parse(dt.Rows[n]["AuditUser"].ToString());
                    }
                    if (dt.Rows[n]["AuditDate"] != null && dt.Rows[n]["AuditDate"].ToString() != "")
                    {
                        model.AuditDate = DateTime.Parse(dt.Rows[n]["AuditDate"].ToString());
                    }
                    if (dt.Rows[n]["InsertUser"] != null && dt.Rows[n]["InsertUser"].ToString() != "")
                    {
                        model.InsertUser = int.Parse(dt.Rows[n]["InsertUser"].ToString());
                    }
                    if (dt.Rows[n]["InsertDate"] != null && dt.Rows[n]["InsertDate"].ToString() != "")
                    {
                        model.InsertDate = DateTime.Parse(dt.Rows[n]["InsertDate"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }


        #endregion  Method
    }
}
