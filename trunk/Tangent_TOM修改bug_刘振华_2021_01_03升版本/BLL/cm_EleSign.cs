﻿using System;
using System.Data;
using System.Collections.Generic;
using TG.Common;
using TG.Model;
namespace TG.BLL
{
	/// <summary>
	/// cm_EleSign
	/// </summary>
	public partial class cm_EleSign
	{
		private readonly TG.DAL.cm_EleSign dal=new TG.DAL.cm_EleSign();
		public cm_EleSign()
		{}
		#region  Method

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(TG.Model.cm_EleSign model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
        public int Update(TG.Model.cm_EleSign model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			return dal.Delete(ID);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			return dal.DeleteList(IDlist );
		}
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteListMemID(string IDlist)
        {
            return dal.DeleteListMemID(IDlist);
        }
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_EleSign GetModel(int ID)
		{
			
			return dal.GetModel(ID);
		}
        /// <summary>
        /// 根据会员id得到一个对象实体
        /// </summary>
        public TG.Model.cm_EleSign GetModel2(int memid)
        {

            return dal.GetModel2(memid);
        }

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public TG.Model.cm_EleSign GetModelByCache(int ID)
		{
			
			string CacheKey = "cm_EleSignModel-" + ID;
			object objModel = TG.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ID);
					if (objModel != null)
					{
						int ModelCache = TG.Common.ConfigHelper.GetConfigInt("ModelCache");
						TG.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (TG.Model.cm_EleSign)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_EleSign> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<TG.Model.cm_EleSign> DataTableToList(DataTable dt)
		{
			List<TG.Model.cm_EleSign> modelList = new List<TG.Model.cm_EleSign>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				TG.Model.cm_EleSign model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new TG.Model.cm_EleSign();
					if(dt.Rows[n]["ID"]!=null && dt.Rows[n]["ID"].ToString()!="")
					{
						model.ID=int.Parse(dt.Rows[n]["ID"].ToString());
					}
					if(dt.Rows[n]["mem_id"]!=null && dt.Rows[n]["mem_id"].ToString()!="")
					{
						model.mem_id=int.Parse(dt.Rows[n]["mem_id"].ToString());
					}
					if(dt.Rows[n]["elesign"]!=null && dt.Rows[n]["elesign"].ToString()!="")
					{
					model.elesign=dt.Rows[n]["elesign"].ToString();
					}
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  Method
	}
}

