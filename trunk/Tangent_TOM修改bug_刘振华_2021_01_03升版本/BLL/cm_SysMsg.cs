﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using System.Collections;

namespace TG.BLL
{
    public class cm_SysMsg
    {
        private TG.DAL.cm_SysMsg SysMsgBLL = new TG.DAL.cm_SysMsg();

        private ProjectAuditListBP projectAuditListBP = new ProjectAuditListBP();

        public int InsertSysMsg(TG.Model.cm_SysMsg sysMsg)
        {
            return SysMsgBLL.InsertSysMsg(sysMsg);
        }

        public List<TG.Model.cm_SysMsg> GetSysMsgList(int pageSize, int PageCurrent, int userSysNo, string coperationName, char sysMsgStatus)
        {
            //查询该用户所有的消息
            List<TG.Model.cm_SysMsg> sysMsgList = SysMsgBLL.GetSysMsgList(pageSize, PageCurrent, userSysNo, coperationName, sysMsgStatus);

            foreach (var sysMsg in sysMsgList)
            {
                //查询是否有附件信息
                int count = new TG.BLL.cm_AttachInfo().IsHavaAttach(sysMsg.CoperationSysNo);
                sysMsg.HaveElectronicCoperation = count > 0 ? "有" : "无";
            }
            return sysMsgList;
            //TG.BLL.cm_CprAttach cprAttachBLL = new cm_CprAttach();
            //TG.BLL.cm_CoperationAudit copertionAuditBLL = new cm_CoperationAudit();
            //List<TG.Model.AuditSysMsg> resultList = new List<TG.Model.AuditSysMsg>();

            //foreach (var sysMsg in sysMsgList)
            //{
            //        //查询该消息对应的审核记录
            //        TG.Model.cm_CoperationAudit coperationAuditEntity = copertionAuditBLL.GetModel(sysMsg.AuditRecordSysNo);
            //        TG.Model.AuditSysMsg auditSysMsg = new TG.Model.AuditSysMsg
            //        {
            //                SysMsgSysNo = sysMsg.MsgSysNo,
            //                CoperationAuditSysNo = coperationAuditEntity.SysNo,
            //                CoperationInDate = coperationAuditEntity.InDate,
            //                CoperationStatus = coperationAuditEntity.Status,
            //                CoperationSysNo = coperationAuditEntity.CoperationSysNo
            //        };
            //        //查询名称
            //        TG.Model.cm_Coperation coperation = new TG.BLL.cm_Coperation().GetModel(coperationAuditEntity.CoperationSysNo);
            //        auditSysMsg.CoperationName = coperation == null ? "" : coperation.cpr_Name;

            //        //查询是否有附件
            //        int count = cprAttachBLL.IsHavaAttach(coperation.cpr_Id);
            //        auditSysMsg.HaveElectronicCoperation = count > 0 ? "有" : "无";
            //        resultList.Add(auditSysMsg);
            //}



            //TG.BLL.cm_CprAttach cprAttachBLL = new cm_CprAttach();
            //foreach (var item in resultList)
            //{
            //        //查询是否有附件信息
            //        int count = cprAttachBLL.GetAttachByCoperation(item.CoperationSysNo);
            //        item.HaveElectronicCoperation = count > 0 ? "有" : "无";
            //}
        }

        ////public List<TG.Model.cm_SysMsg> GetUserSysMsg(int userSysNo)
        //{
        //        //查询用户角色
        //        List<int> roleList = GetRoleByUserSysNo(userSysNo);
        //        return SysMsgBLL.GetUserSysMsg(userSysNo, roleList);
        //}

        /// <summary>
        /// 查询用户角色返回角色SysNo集合
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public List<int> GetRoleByUserSysNo(int userSysNo)
        {
            List<TG.Model.cm_Role> roleList = SysMsgBLL.GetRoleByUserSysNo(userSysNo);
            List<int> roleList1 = new List<int>();
            foreach (var role in roleList)
            {
                roleList1.Add(role.SysNo);
            }
            return roleList1;
        }

        /// <summary>
        /// 返回用户角色集合
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public List<TG.Model.cm_Role> GetRoleListByUserSysNo(int userSysNo)
        {
            return SysMsgBLL.GetRoleByUserSysNo(userSysNo);
        }
        /// <summary>
        /// new 20140317 史国庆  母版页的消息体
        /// </summary>
        /// <param name="usersysno"></param>
        /// <returns></returns>
        public List<SYSMsgQueryEntityByMain> GetListForMain(int usersysno, string wherestring)
        {
            return SysMsgBLL.GetListForMain(usersysno, wherestring);
        }
        public List<SYSMsgQueryEntityByMain> GetListForMainAboutMsg(int usersysno, string wherestring)
        {
            return SysMsgBLL.GetListForMainAboutMsg(usersysno, wherestring);
        }
        /// <summary>
        /// 修改系统消息状态
        /// </summary>
        /// <param name="SysMsgSysNo"></param>
        /// <returns></returns>
        public int UpdateSysMsgStatus(int SysMsgSysNo)
        {
            return SysMsgBLL.UpdateSysMsgStatus(SysMsgSysNo);
        }
        /// <summary>
        /// 修改待办事项的状态
        /// </summary>
        /// <param name="SysMsgSysNo"></param>
        /// <returns></returns>
        public int UpdateSysMsgDoneStatus(int SysMsgSysNo)
        {
            return SysMsgBLL.UpdateSysMsgDoneStatus(SysMsgSysNo);
        }
         //更改代办事项 根据ID
        public int UpdateSysMsgDoneStatusByID(int SysMsgSysNo)
        {
            return SysMsgBLL.UpdateSysMsgDoneStatusByID(SysMsgSysNo);
        }
        /// <summary>
        /// 修改系统消息状态
        /// </summary>
        /// <param name="coperationSysNo"></param>
        /// <returns></returns>
        public int UpDateSysMsgStatusByCoperationSysNo(int coperationSysNo)
        {
            return SysMsgBLL.UpDateSysMsgStatusByCoperationSysNo(coperationSysNo);
        }

        /// <summary>
        /// 查询用户消息数
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public int GetSysMsgCount(int userSysNo)
        {
            return SysMsgBLL.GetSysMsgCount(userSysNo);
        }
        /// <summary>
        /// 获取消息数
        /// </summary>
        public object GetSysMessageCountProc(SysMessageQueryEntity queryEntity)
        {
            return SysMsgBLL.GetSysMessageCountProc(queryEntity);
        }
        /// <summary>
        /// 待办事项数
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        /// <summary>
        public int GetSysMsgDoneCount(int userSysNo)
        {
            return SysMsgBLL.GetSysMsgDoneCount(userSysNo);
        }
          //待办事项个数-产值
        public int GetSysValueMsgDoneCount(int userSysNo)
        {
            return SysMsgBLL.GetSysValueMsgDoneCount(userSysNo);
        }
        /// 得到消息列表
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public List<SysMsgEntity> GetMsgList(SysMsgQueryEntity queryEntity)
        {
            return SysMsgBLL.GetMsgList(queryEntity);
        }

        /// <summary>
        /// 得到该用户关于项目审核消息列表。已审核和未审核
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public ResultEntityList<SysMsgEntity> GetProjectAuditMsg(SysMsgQueryEntity queryEntity)
        {
            List<SysMsgEntity> resultList = GetMsgList(queryEntity);

            ResultEntityList<SysMsgEntity> resultEntityList = new ResultEntityList<SysMsgEntity>() { Body = resultList };

            resultEntityList.TotalCount = SysMsgBLL.GetSysMessageCount(queryEntity);
            //List<SysMsgEntity> lstSource = null;
            //switch (projectStatus)
            //{
            //        case "A":
            //                lstSource = (from auditEntity in resultList where auditEntity.ProjectAuditStatus == "A" select auditEntity).ToList<SysMsgEntity>();
            //                break;
            //        case "D":
            //                lstSource = (from auditEntity in resultList where auditEntity.ProjectAuditStatus != "A" select auditEntity).ToList<SysMsgEntity>();
            //                break;
            //}
            return resultEntityList;
        }

        public List<ProjectAuditListViewDataEntity> GetProjectAuditListViewEntity(int userSysNo, int pageSize, int pageCurrent, string Status, out int dataCount)
        {
            List<ProjectAuditListViewDataEntity> sourceList = SysMsgBLL.GetProjectAuditListViewEntity(userSysNo);

            List<ProjectAuditListViewDataEntity> resultList = null;

            //未审核的场合
            if (Status == "A")
            {
                resultList = sourceList.Where(source =>
                  {
                      bool flag = false;
                      int index = 0;
                      if (string.IsNullOrEmpty(source.Suggestion))
                      {
                          flag = true;
                      }
                      else
                      {
                          string[] suggestionArray = source.Suggestion.Split('|');
                          switch (source.ProjectStatus)
                          {
                              case "B":
                                  index = 1;
                                  break;
                          }
                          if (index <= suggestionArray.Length)
                          {
                              if (string.IsNullOrEmpty(suggestionArray[index]))
                              {
                                  flag = true;
                              }
                          }
                      }
                      return flag;
                  }).ToList<ProjectAuditListViewDataEntity>();
            }

            else
            {
                resultList = sourceList.Where(source =>
                {
                    bool flag = false;
                    int index = 0;

                    if (!string.IsNullOrEmpty(source.Suggestion))
                    {
                        string[] suggestionArray = source.Suggestion.Split('|');
                        switch (source.ProjectStatus)
                        {
                            case "B":
                                index = 1;
                                break;
                        }
                        if (index <= suggestionArray.Length)
                        {
                            if (!string.IsNullOrEmpty(suggestionArray[index]))
                            {
                                flag = true;
                            }
                        }
                    }
                    return flag;
                }).ToList<ProjectAuditListViewDataEntity>();
            }

            dataCount = resultList.Count;
            return resultList.Skip((pageCurrent - 1) * pageSize).Take(pageSize).ToList<ProjectAuditListViewDataEntity>();
        }

        /// <summary>
        /// 插入一条新消息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int CreateNewMsg(SysMsgEntity dataEntity)
        {
            return SysMsgBLL.CreateNewMsg(dataEntity);
        }

        /// <summary>
        /// 取得系统消息情报
        /// </summary>
        /// <returns></returns>
        public List<SysMessageViewEntity> GetSysMessageRecord(SysMessageQueryEntity queryEntity, int startIndex, int endIndex)
        {
            List<SysMessageViewEntity> msgList = SysMsgBLL.GetSysMessageRecordProc(queryEntity, startIndex, endIndex);

            TG.DAL.cm_Role roleDA = new TG.DAL.cm_Role();
            TG.DAL.tg_member userDA = new TG.DAL.tg_member();

            //取得RoleUserName
            msgList.ForEach((msg) =>
            {
                if (msg.ToRole != "0")
                {
                    string userNameString = "";
                    //取得当前Role实体
                    var roleEntity = roleDA.GetRole(int.Parse(msg.ToRole));

                    string[] userSysNoArray = roleEntity.Users.Split(',');

                    foreach (string userSysNo in userSysNoArray)
                    {
                        var userEntity = userDA.GetModel(int.Parse(userSysNo));
                        userNameString += userEntity == null ? "" : userEntity.mem_Name + ",";
                    }
                    userNameString = userNameString.Substring(0, userNameString.Length - 1);
                    msg.RoleUserName = userNameString;
                }
            });
            return msgList;
        }

        /// <summary>
        /// 新规一条消息
        /// </summary>
        /// <param name="sysMessageDataEntity"></param>
        /// <returns></returns>
        public int InsertSysMessage(SysMessageViewEntity sysMessageDataEntity)
        {
            return SysMsgBLL.InsertSysMessage(sysMessageDataEntity);
        }
        /// <summary>
        /// 得到待办状态
        /// </summary>
        /// <param name="messageID"></param>
        /// <returns></returns>
        public string IsDone(int messageID)
        {
            return SysMsgBLL.IsDone(messageID);
        }
    }
}
