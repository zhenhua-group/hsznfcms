﻿using System;
using System.Configuration;
namespace TG.DBUtility
{
    
    public class PubConstant
    {        
        /// <summary>
        /// 获取连接字符串
        /// </summary>
        public static string ConnectionString
        {           
            get 
            {
                string _connectionString = ConfigurationManager.AppSettings["ConnectionString"];       
                string ConStringEncrypt = ConfigurationManager.AppSettings["ConStringEncrypt"];
                if (ConStringEncrypt == "true")
                {
                    _connectionString = DESEncrypt.Decrypt(_connectionString);
                }
                return _connectionString; 
            }
        }

        /// <summary>
        /// 得到web.config里配置项的数据库连接字符串。
        /// </summary>
        /// <param name="configName"></param>
        /// <returns></returns>
        public static string GetConnectionString(string configName)
        {
            string connectionString = ConfigurationManager.AppSettings[configName];
            string ConStringEncrypt = ConfigurationManager.AppSettings["ConStringEncrypt"];
            if (ConStringEncrypt == "true")
            {
                connectionString = DESEncrypt.Decrypt(connectionString);
            }
            return connectionString;
        }

        /// <summary>
        /// 获取excel连接字符串
        /// </summary>
        public static string ConnectionExcel
        {
            get
            {
                string _connectionString = ConfigurationManager.AppSettings["ConnectionExcel"];
                _connectionString = _connectionString.Replace("{0}", ExcelPath);

                string ConStringEncrypt = ConfigurationManager.AppSettings["ConStringEncrypt"];

                if (ConStringEncrypt == "true")
                {
                    _connectionString = DESEncrypt.Decrypt(_connectionString);
                }
                return _connectionString;
            }
        }

        /// <summary>
        /// 获取excel连接字符串
        /// </summary>
        public static string ConnectionExcel2
        {
            get
            {
                string _connectionString = ConfigurationManager.AppSettings["ConnectionExcel"];
                _connectionString = _connectionString.Replace("{0}", ExcelPath2);

                string ConStringEncrypt = ConfigurationManager.AppSettings["ConStringEncrypt"];

                if (ConStringEncrypt == "true")
                {
                    _connectionString = DESEncrypt.Decrypt(_connectionString);
                }
                return _connectionString;
            }
        }

        /// <summary>
        /// excel数据库路径
        /// </summary>
        public static string ExcelPath
        {
            get;
            set;
        }
        /// <summary>
        /// excel数据库路径
        /// </summary>
        public static string ExcelPath2
        {
            get;
            set;
        }

    }
}
