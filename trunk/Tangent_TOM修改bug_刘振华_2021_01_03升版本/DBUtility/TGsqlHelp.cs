﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace TG.DBUtility
{
    public class TGsqlHelp
    {
        /// <summary>
        /// 获得数据库连接字符串。
        /// </summary>
        public static string connsql = ConfigurationManager.AppSettings["ConnectionString"].ToString();
        /// <summary>
        /// 增删改语句
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <param name="param">参数</param>
        /// <returns>受影响行数</returns>
        public static int ExecuteNonQuery(string sql, params SqlParameter[] param)
        {
            using (SqlConnection conn = new SqlConnection(connsql))
            {
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    if (param != null)
                    {
                        cmd.Parameters.AddRange(param);
                    }
                    conn.Open();
                    return cmd.ExecuteNonQuery();
                }
            }
        }
        /// <summary>
        /// 查询一条数据，object类型
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <param name="param">参数数组</param>
        /// <returns>返回object类型</returns>
        public static object ExecuteScalar(string sql, params SqlParameter[] param)
        {
            using (SqlConnection conn = new SqlConnection(connsql))
            {
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    if (param != null)
                    {
                        cmd.Parameters.AddRange(param);
                    }
                    conn.Open();
                    return cmd.ExecuteScalar();
                }
            }
        }

        /// <summary>
        /// 只读只进查询  一条一条
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <param name="param">参数列表</param>
        /// <returns>返回datareader</returns>
        public static SqlDataReader ExecuteReader(string sql, params SqlParameter[] param)
        {
            SqlConnection conn = new SqlConnection(connsql);
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                if (param != null)
                {
                    cmd.Parameters.AddRange(param);
                }
                conn.Open();
                return cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            }
        }
        /// <summary>
        /// 返回一个表（table）的集合。
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <param name="param">参数列表</param>
        /// <returns>返回值是一个table的集合</returns>
        public static DataSet dataset(string sql, params SqlParameter[] param)
        {
            DataSet ds = new DataSet();
            using (SqlDataAdapter sda=new SqlDataAdapter (sql,connsql))
            {
                if (param != null)
                {
                    sda.SelectCommand.Parameters.AddRange(param);
                }
                sda.Fill(ds);
                return ds;
            }
        }
    }
}
