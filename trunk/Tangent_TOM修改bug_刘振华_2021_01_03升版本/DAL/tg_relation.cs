﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:tg_relation
	/// </summary>
	public partial class tg_relation
	{
		public tg_relation()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TG.Model.tg_relation model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.pro_ID != null)
			{
				strSql1.Append("pro_ID,");
				strSql2.Append(""+model.pro_ID+",");
			}
			if (model.mem_ID != null)
			{
				strSql1.Append("mem_ID,");
				strSql2.Append(""+model.mem_ID+",");
			}
			if (model.mem_isManage != null)
			{
				strSql1.Append("mem_isManage,");
				strSql2.Append(""+model.mem_isManage+",");
			}
			if (model.mem_isSpecLead != null)
			{
				strSql1.Append("mem_isSpecLead,");
				strSql2.Append(""+model.mem_isSpecLead+",");
			}
			if (model.mem_isAssistant != null)
			{
				strSql1.Append("mem_isAssistant,");
				strSql2.Append(""+model.mem_isAssistant+",");
			}
			if (model.mem_isAssessor != null)
			{
				strSql1.Append("mem_isAssessor,");
				strSql2.Append(""+model.mem_isAssessor+",");
			}
			if (model.mem_isCorrector != null)
			{
				strSql1.Append("mem_isCorrector,");
				strSql2.Append(""+model.mem_isCorrector+",");
			}
			if (model.mem_isValidator != null)
			{
				strSql1.Append("mem_isValidator,");
				strSql2.Append(""+model.mem_isValidator+",");
			}
			if (model.mem_isDirector != null)
			{
				strSql1.Append("mem_isDirector,");
				strSql2.Append(""+model.mem_isDirector+",");
			}
			if (model.mem_RoleIDs != null)
			{
				strSql1.Append("mem_RoleIDs,");
				strSql2.Append("'"+model.mem_RoleIDs+"',");
			}
			if (model.mem_bCanViewSpec != null)
			{
				strSql1.Append("mem_bCanViewSpec,");
				strSql2.Append(""+model.mem_bCanViewSpec+",");
			}
			if (model.mem_bCanViewAllSpec != null)
			{
				strSql1.Append("mem_bCanViewAllSpec,");
				strSql2.Append(""+model.mem_bCanViewAllSpec+",");
			}
			strSql.Append("insert into tg_relation(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_relation model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update tg_relation set ");
			if (model.pro_ID != null)
			{
				strSql.Append("pro_ID="+model.pro_ID+",");
			}
			else
			{
				strSql.Append("pro_ID= null ,");
			}
			if (model.mem_ID != null)
			{
				strSql.Append("mem_ID="+model.mem_ID+",");
			}
			else
			{
				strSql.Append("mem_ID= null ,");
			}
			if (model.mem_isManage != null)
			{
				strSql.Append("mem_isManage="+model.mem_isManage+",");
			}
			else
			{
				strSql.Append("mem_isManage= null ,");
			}
			if (model.mem_isSpecLead != null)
			{
				strSql.Append("mem_isSpecLead="+model.mem_isSpecLead+",");
			}
			else
			{
				strSql.Append("mem_isSpecLead= null ,");
			}
			if (model.mem_isAssistant != null)
			{
				strSql.Append("mem_isAssistant="+model.mem_isAssistant+",");
			}
			if (model.mem_isAssessor != null)
			{
				strSql.Append("mem_isAssessor="+model.mem_isAssessor+",");
			}
			if (model.mem_isCorrector != null)
			{
				strSql.Append("mem_isCorrector="+model.mem_isCorrector+",");
			}
			if (model.mem_isValidator != null)
			{
				strSql.Append("mem_isValidator="+model.mem_isValidator+",");
			}
			if (model.mem_isDirector != null)
			{
				strSql.Append("mem_isDirector="+model.mem_isDirector+",");
			}
			if (model.mem_RoleIDs != null)
			{
				strSql.Append("mem_RoleIDs='"+model.mem_RoleIDs+"',");
			}
			else
			{
				strSql.Append("mem_RoleIDs= null ,");
			}
			if (model.mem_bCanViewSpec != null)
			{
				strSql.Append("mem_bCanViewSpec="+model.mem_bCanViewSpec+",");
			}
			if (model.mem_bCanViewAllSpec != null)
			{
				strSql.Append("mem_bCanViewAllSpec="+model.mem_bCanViewAllSpec+",");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_relation ");
			strSql.Append(" where " );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_relation GetModel()
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" pro_ID,mem_ID,mem_isManage,mem_isSpecLead,mem_isAssistant,mem_isAssessor,mem_isCorrector,mem_isValidator,mem_isDirector,mem_RoleIDs,mem_bCanViewSpec,mem_bCanViewAllSpec ");
			strSql.Append(" from tg_relation ");
			strSql.Append(" where " );
			TG.Model.tg_relation model=new TG.Model.tg_relation();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["pro_ID"]!=null && ds.Tables[0].Rows[0]["pro_ID"].ToString()!="")
				{
					model.pro_ID=int.Parse(ds.Tables[0].Rows[0]["pro_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_ID"]!=null && ds.Tables[0].Rows[0]["mem_ID"].ToString()!="")
				{
					model.mem_ID=int.Parse(ds.Tables[0].Rows[0]["mem_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_isManage"]!=null && ds.Tables[0].Rows[0]["mem_isManage"].ToString()!="")
				{
					model.mem_isManage=int.Parse(ds.Tables[0].Rows[0]["mem_isManage"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_isSpecLead"]!=null && ds.Tables[0].Rows[0]["mem_isSpecLead"].ToString()!="")
				{
					model.mem_isSpecLead=int.Parse(ds.Tables[0].Rows[0]["mem_isSpecLead"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_isAssistant"]!=null && ds.Tables[0].Rows[0]["mem_isAssistant"].ToString()!="")
				{
					model.mem_isAssistant=int.Parse(ds.Tables[0].Rows[0]["mem_isAssistant"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_isAssessor"]!=null && ds.Tables[0].Rows[0]["mem_isAssessor"].ToString()!="")
				{
					model.mem_isAssessor=int.Parse(ds.Tables[0].Rows[0]["mem_isAssessor"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_isCorrector"]!=null && ds.Tables[0].Rows[0]["mem_isCorrector"].ToString()!="")
				{
					model.mem_isCorrector=int.Parse(ds.Tables[0].Rows[0]["mem_isCorrector"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_isValidator"]!=null && ds.Tables[0].Rows[0]["mem_isValidator"].ToString()!="")
				{
					model.mem_isValidator=int.Parse(ds.Tables[0].Rows[0]["mem_isValidator"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_isDirector"]!=null && ds.Tables[0].Rows[0]["mem_isDirector"].ToString()!="")
				{
					model.mem_isDirector=int.Parse(ds.Tables[0].Rows[0]["mem_isDirector"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_RoleIDs"]!=null && ds.Tables[0].Rows[0]["mem_RoleIDs"].ToString()!="")
				{
					model.mem_RoleIDs=ds.Tables[0].Rows[0]["mem_RoleIDs"].ToString();
				}
				if(ds.Tables[0].Rows[0]["mem_bCanViewSpec"]!=null && ds.Tables[0].Rows[0]["mem_bCanViewSpec"].ToString()!="")
				{
					model.mem_bCanViewSpec=int.Parse(ds.Tables[0].Rows[0]["mem_bCanViewSpec"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_bCanViewAllSpec"]!=null && ds.Tables[0].Rows[0]["mem_bCanViewAllSpec"].ToString()!="")
				{
					model.mem_bCanViewAllSpec=int.Parse(ds.Tables[0].Rows[0]["mem_bCanViewAllSpec"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select pro_ID,mem_ID,mem_isManage,mem_isSpecLead,mem_isAssistant,mem_isAssessor,mem_isCorrector,mem_isValidator,mem_isDirector,mem_RoleIDs,mem_bCanViewSpec,mem_bCanViewAllSpec ");
			strSql.Append(" FROM tg_relation ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" pro_ID,mem_ID,mem_isManage,mem_isSpecLead,mem_isAssistant,mem_isAssessor,mem_isCorrector,mem_isValidator,mem_isDirector,mem_RoleIDs,mem_bCanViewSpec,mem_bCanViewAllSpec ");
			strSql.Append(" FROM tg_relation ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM tg_relation ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from tg_relation T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

