﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using System.Collections.Generic;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_AttachInfo
    /// </summary>
    public partial class cm_AttachInfo
    {
        public cm_AttachInfo()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("ID", "cm_AttachInfo");
        }


        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_AttachInfo");
            strSql.Append(" where ID=" + ID + " ");
            return DbHelperSQL.Exists(strSql.ToString());
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_AttachInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.Cst_Id != null)
            {
                strSql1.Append("Cst_Id,");
                strSql2.Append("" + model.Cst_Id + ",");
            }
            if (model.Cpr_Id != null)
            {
                strSql1.Append("Cpr_Id,");
                strSql2.Append("" + model.Cpr_Id + ",");
            }
            if (model.Temp_No != null)
            {
                strSql1.Append("Temp_No,");
                strSql2.Append("'" + model.Temp_No + "',");
            }
            if (model.FileName != null)
            {
                strSql1.Append("FileName,");
                strSql2.Append("'" + model.FileName + "',");
            }
            if (model.UploadTime != null)
            {
                strSql1.Append("UploadTime,");
                strSql2.Append("'" + model.UploadTime + "',");
            }
            if (model.FileSize != null)
            {
                strSql1.Append("FileSize,");
                strSql2.Append("'" + model.FileSize + "',");
            }
            if (model.FileType != null)
            {
                strSql1.Append("FileType,");
                strSql2.Append("'" + model.FileType + "',");
            }
            if (model.UploadUser != null)
            {
                strSql1.Append("UploadUser,");
                strSql2.Append("'" + model.UploadUser + "',");
            }
            if (model.FileUrl != null)
            {
                strSql1.Append("FileUrl,");
                strSql2.Append("'" + model.FileUrl + "',");
            }
            if (model.FileTypeImg != null)
            {
                strSql1.Append("FileTypeImg,");
                strSql2.Append("'" + model.FileTypeImg + "',");
            }
            if (model.OwnType != null)
            {
                strSql1.Append("OwnType,");
                strSql2.Append("'" + model.OwnType + "',");
            }
            if (model.Proj_Id != null)
            {
                strSql1.Append("Proj_Id,");
                strSql2.Append("" + model.Proj_Id + ",");
            }
            strSql.Append("insert into cm_AttachInfo(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        public int AddInterView(TG.Model.cm_AttachInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.Cst_Id != null)
            {
                strSql1.Append("Cst_Id,");
                strSql2.Append("" + model.Cst_Id + ",");
            }
            if (model.Cpr_Id != null)
            {
                strSql1.Append("Cpr_Id,");
                strSql2.Append("" + model.Cpr_Id + ",");
            }
            if (model.Temp_No != null)
            {
                strSql1.Append("Temp_No,");
                strSql2.Append("'" + model.Temp_No + "',");
            }
            if (model.FileName != null)
            {
                strSql1.Append("FileName,");
                strSql2.Append("'" + model.FileName + "',");
            }
            if (model.UploadTime != null)
            {
                strSql1.Append("UploadTime,");
                strSql2.Append("'" + model.UploadTime + "',");
            }
            if (model.FileSize != null)
            {
                strSql1.Append("FileSize,");
                strSql2.Append("'" + model.FileSize + "',");
            }
            if (model.FileType != null)
            {
                strSql1.Append("FileType,");
                strSql2.Append("'" + model.FileType + "',");
            }
            if (model.UploadUser != null)
            {
                strSql1.Append("UploadUser,");
                strSql2.Append("'" + model.UploadUser + "',");
            }
            if (model.FileUrl != null)
            {
                strSql1.Append("FileUrl,");
                strSql2.Append("'" + model.FileUrl + "',");
            }
            if (model.FileTypeImg != null)
            {
                strSql1.Append("FileTypeImg,");
                strSql2.Append("'" + model.FileTypeImg + "',");
            }
            if (model.OwnType != null)
            {
                strSql1.Append("OwnType,");
                strSql2.Append("'" + model.OwnType + "',");
            }
            if (model.Proj_Id != null)
            {
                strSql1.Append("Proj_Id,");
                strSql2.Append("" + model.Proj_Id + ",");
            }
            strSql.Append("insert into cm_InterviewAttachInfo(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_AttachInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_AttachInfo set ");
            if (model.Cst_Id != null)
            {
                strSql.Append("Cst_Id=" + model.Cst_Id + ",");
            }
            else
            {
                strSql.Append("Cst_Id= null ,");
            }
            if (model.Cpr_Id != null)
            {
                strSql.Append("Cpr_Id=" + model.Cpr_Id + ",");
            }
            else
            {
                strSql.Append("Cpr_Id= null ,");
            }
            if (model.Temp_No != null)
            {
                strSql.Append("Temp_No='" + model.Temp_No + "',");
            }
            else
            {
                strSql.Append("Temp_No= null ,");
            }
            if (model.FileName != null)
            {
                strSql.Append("FileName='" + model.FileName + "',");
            }
            else
            {
                strSql.Append("FileName= null ,");
            }
            if (model.UploadTime != null)
            {
                strSql.Append("UploadTime='" + model.UploadTime + "',");
            }
            else
            {
                strSql.Append("UploadTime= null ,");
            }
            if (model.FileSize != null)
            {
                strSql.Append("FileSize='" + model.FileSize + "',");
            }
            else
            {
                strSql.Append("FileSize= null ,");
            }
            if (model.FileType != null)
            {
                strSql.Append("FileType='" + model.FileType + "',");
            }
            else
            {
                strSql.Append("FileType= null ,");
            }
            if (model.UploadUser != null)
            {
                strSql.Append("UploadUser='" + model.UploadUser + "',");
            }
            else
            {
                strSql.Append("UploadUser= null ,");
            }
            if (model.FileUrl != null)
            {
                strSql.Append("FileUrl='" + model.FileUrl + "',");
            }
            else
            {
                strSql.Append("FileUrl= null ,");
            }
            if (model.FileTypeImg != null)
            {
                strSql.Append("FileTypeImg='" + model.FileTypeImg + "',");
            }
            else
            {
                strSql.Append("FileTypeImg= null ,");
            }
            if (model.OwnType != null)
            {
                strSql.Append("OwnType='" + model.OwnType + "',");
            }
            else
            {
                strSql.Append("OwnType= null ,");
            }
            if (model.Proj_Id != null)
            {
                strSql.Append("Proj_Id=" + model.Proj_Id + ",");
            }
            else
            {
                strSql.Append("Proj_Id= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where ID=" + model.ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdateInterView(TG.Model.cm_AttachInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_InterviewAttachInfo set ");
            if (model.Cst_Id != null)
            {
                strSql.Append("Cst_Id=" + model.Cst_Id + ",");
            }
            else
            {
                strSql.Append("Cst_Id= null ,");
            }
            if (model.Cpr_Id != null)
            {
                strSql.Append("Cpr_Id=" + model.Cpr_Id + ",");
            }
            else
            {
                strSql.Append("Cpr_Id= null ,");
            }
            if (model.Temp_No != null)
            {
                strSql.Append("Temp_No='" + model.Temp_No + "',");
            }
            else
            {
                strSql.Append("Temp_No= null ,");
            }
            if (model.FileName != null)
            {
                strSql.Append("FileName='" + model.FileName + "',");
            }
            else
            {
                strSql.Append("FileName= null ,");
            }
            if (model.UploadTime != null)
            {
                strSql.Append("UploadTime='" + model.UploadTime + "',");
            }
            else
            {
                strSql.Append("UploadTime= null ,");
            }
            if (model.FileSize != null)
            {
                strSql.Append("FileSize='" + model.FileSize + "',");
            }
            else
            {
                strSql.Append("FileSize= null ,");
            }
            if (model.FileType != null)
            {
                strSql.Append("FileType='" + model.FileType + "',");
            }
            else
            {
                strSql.Append("FileType= null ,");
            }
            if (model.UploadUser != null)
            {
                strSql.Append("UploadUser='" + model.UploadUser + "',");
            }
            else
            {
                strSql.Append("UploadUser= null ,");
            }
            if (model.FileUrl != null)
            {
                strSql.Append("FileUrl='" + model.FileUrl + "',");
            }
            else
            {
                strSql.Append("FileUrl= null ,");
            }
            if (model.FileTypeImg != null)
            {
                strSql.Append("FileTypeImg='" + model.FileTypeImg + "',");
            }
            else
            {
                strSql.Append("FileTypeImg= null ,");
            }
            if (model.OwnType != null)
            {
                strSql.Append("OwnType='" + model.OwnType + "',");
            }
            else
            {
                strSql.Append("OwnType= null ,");
            }
            if (model.Proj_Id != null)
            {
                strSql.Append("Proj_Id=" + model.Proj_Id + ",");
            }
            else
            {
                strSql.Append("Proj_Id= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where ID=" + model.ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_AttachInfo ");
            strSql.Append(" where ID=" + ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_AttachInfo ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_AttachInfo GetModel(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" ID,Cst_Id,Cpr_Id,Temp_No,FileName,UploadTime,FileSize,FileType,UploadUser,FileUrl,FileTypeImg,OwnType,Proj_Id ");
            strSql.Append(" from cm_AttachInfo ");
            strSql.Append(" where ID=" + ID + "");
            TG.Model.cm_AttachInfo model = new TG.Model.cm_AttachInfo();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Cst_Id"] != null && ds.Tables[0].Rows[0]["Cst_Id"].ToString() != "")
                {
                    model.Cst_Id = Convert.ToDecimal(ds.Tables[0].Rows[0]["Cst_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Cpr_Id"] != null && ds.Tables[0].Rows[0]["Cpr_Id"].ToString() != "")
                {
                    model.Cpr_Id = Convert.ToDecimal(ds.Tables[0].Rows[0]["Cpr_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Temp_No"] != null && ds.Tables[0].Rows[0]["Temp_No"].ToString() != "")
                {
                    model.Temp_No = ds.Tables[0].Rows[0]["Temp_No"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FileName"] != null && ds.Tables[0].Rows[0]["FileName"].ToString() != "")
                {
                    model.FileName = ds.Tables[0].Rows[0]["FileName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UploadTime"] != null && ds.Tables[0].Rows[0]["UploadTime"].ToString() != "")
                {
                    model.UploadTime = DateTime.Parse(ds.Tables[0].Rows[0]["UploadTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FileSize"] != null && ds.Tables[0].Rows[0]["FileSize"].ToString() != "")
                {
                    model.FileSize = ds.Tables[0].Rows[0]["FileSize"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FileType"] != null && ds.Tables[0].Rows[0]["FileType"].ToString() != "")
                {
                    model.FileType = ds.Tables[0].Rows[0]["FileType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UploadUser"] != null && ds.Tables[0].Rows[0]["UploadUser"].ToString() != "")
                {
                    model.UploadUser = ds.Tables[0].Rows[0]["UploadUser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FileUrl"] != null && ds.Tables[0].Rows[0]["FileUrl"].ToString() != "")
                {
                    model.FileUrl = ds.Tables[0].Rows[0]["FileUrl"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FileTypeImg"] != null && ds.Tables[0].Rows[0]["FileTypeImg"].ToString() != "")
                {
                    model.FileTypeImg = ds.Tables[0].Rows[0]["FileTypeImg"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OwnType"] != null && ds.Tables[0].Rows[0]["OwnType"].ToString() != "")
                {
                    model.OwnType = ds.Tables[0].Rows[0]["OwnType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Proj_Id"] != null && ds.Tables[0].Rows[0]["Proj_Id"].ToString() != "")
                {
                    model.Proj_Id = Convert.ToDecimal(ds.Tables[0].Rows[0]["Proj_Id"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_AttachInfo GetModelInterView(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" ID,Cst_Id,Cpr_Id,Temp_No,FileName,UploadTime,FileSize,FileType,UploadUser,FileUrl,FileTypeImg,OwnType,Proj_Id ");
            strSql.Append(" from cm_InterviewAttachInfo ");
            strSql.Append(" where ID=" + ID + "");
            TG.Model.cm_AttachInfo model = new TG.Model.cm_AttachInfo();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Cst_Id"] != null && ds.Tables[0].Rows[0]["Cst_Id"].ToString() != "")
                {
                    model.Cst_Id = Convert.ToDecimal(ds.Tables[0].Rows[0]["Cst_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Cpr_Id"] != null && ds.Tables[0].Rows[0]["Cpr_Id"].ToString() != "")
                {
                    model.Cpr_Id = Convert.ToDecimal(ds.Tables[0].Rows[0]["Cpr_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Temp_No"] != null && ds.Tables[0].Rows[0]["Temp_No"].ToString() != "")
                {
                    model.Temp_No = ds.Tables[0].Rows[0]["Temp_No"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FileName"] != null && ds.Tables[0].Rows[0]["FileName"].ToString() != "")
                {
                    model.FileName = ds.Tables[0].Rows[0]["FileName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UploadTime"] != null && ds.Tables[0].Rows[0]["UploadTime"].ToString() != "")
                {
                    model.UploadTime = DateTime.Parse(ds.Tables[0].Rows[0]["UploadTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FileSize"] != null && ds.Tables[0].Rows[0]["FileSize"].ToString() != "")
                {
                    model.FileSize = ds.Tables[0].Rows[0]["FileSize"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FileType"] != null && ds.Tables[0].Rows[0]["FileType"].ToString() != "")
                {
                    model.FileType = ds.Tables[0].Rows[0]["FileType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UploadUser"] != null && ds.Tables[0].Rows[0]["UploadUser"].ToString() != "")
                {
                    model.UploadUser = ds.Tables[0].Rows[0]["UploadUser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FileUrl"] != null && ds.Tables[0].Rows[0]["FileUrl"].ToString() != "")
                {
                    model.FileUrl = ds.Tables[0].Rows[0]["FileUrl"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FileTypeImg"] != null && ds.Tables[0].Rows[0]["FileTypeImg"].ToString() != "")
                {
                    model.FileTypeImg = ds.Tables[0].Rows[0]["FileTypeImg"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OwnType"] != null && ds.Tables[0].Rows[0]["OwnType"].ToString() != "")
                {
                    model.OwnType = ds.Tables[0].Rows[0]["OwnType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Proj_Id"] != null && ds.Tables[0].Rows[0]["Proj_Id"].ToString() != "")
                {
                    model.Proj_Id = Convert.ToDecimal(ds.Tables[0].Rows[0]["Proj_Id"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,Cst_Id,Cpr_Id,Temp_No,FileName,UploadTime,FileSize,FileType,UploadUser,FileUrl,FileTypeImg,OwnType,Proj_Id,(Convert(int,FileSize)/1024)as FileSizeString ");
            strSql.Append(" FROM cm_AttachInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        public DataSet GetListInterView(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,Cst_Id,Cpr_Id,Temp_No,FileName,UploadTime,FileSize,FileType,UploadUser,FileUrl,FileTypeImg,OwnType,Proj_Id,(Convert(int,FileSize)/1024)as FileSizeString ");
            strSql.Append(" FROM cm_InterviewAttachInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListAboutUsername(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,Cst_Id,Cpr_Id,Temp_No,FileName,UploadTime,FileSize,FileType,(SELECT mem_Name FROM tg_member WHERE mem_ID=UploadUser)AS UploadUser,FileUrl,FileTypeImg,OwnType,Proj_Id,(Convert(int,FileSize)/1024)as FileSizeString ");
            strSql.Append(" FROM cm_AttachInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,Cst_Id,Cpr_Id,Temp_No,FileName,UploadTime,FileSize,FileType,UploadUser,FileUrl,FileTypeImg,OwnType,Proj_Id,(Convert(int,FileSize)/1024)as FileSizeString ");
            strSql.Append(" FROM cm_AttachInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_AttachInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            /*没有调用此方法，无需修改。Created by Sago*/
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBE() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_AttachInfo T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_AttachInfo", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_AttachInfo_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 根据合同编号查询附件集合
        /// </summary>
        /// <param name="coperationSysNo"></param>
        /// <returns></returns>
        public List<TG.Model.cm_AttachInfo> GetAttachByCoperationSysNo(int coperationSysNo)
        {
            string sql = "select ID,cpr_ID,cpr_No,FileName,FileSize,FileType, FileUrl,UploadTime, UploadUser from cm_CprAttach WHERE cpr_ID = " + coperationSysNo;
            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<TG.Model.cm_AttachInfo> resultList = new List<TG.Model.cm_AttachInfo>();

            while (reader.Read())
            {
                TG.Model.cm_AttachInfo cprAtach = new TG.Model.cm_AttachInfo
                {
                    ID = reader["ID"] == null ? 0 : Convert.ToInt32(reader["ID"]),
                    Cpr_Id = reader["cpr_ID"] == null ? 0 : Convert.ToDecimal(reader["cpr_ID"]),
                    //cpr_No = reader["cpr_No"] == null ? "" : reader["cpr_No"].ToString(),
                    FileName = reader["FileName"] == null ? "" : reader["FileName"].ToString(),
                    FileSize = reader["FileSize"] == null ? "" : reader["FileSize"].ToString(),
                    FileType = reader["FileType"] == null ? "" : reader["FileType"].ToString(),
                    UploadTime = reader["UploadTime"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["UploadTime"]),
                    UploadUser = reader["UploadUser"] == null ? "" : reader["UploadUser"].ToString(),
                    FileUrl = reader["FileUrl"] == null ? "" : reader["FileUrl"].ToString()
                };
                resultList.Add(cprAtach);
            }
            reader.Close();
            return resultList;
        }

        /// <summary>
        /// 查询是否有附件信息
        /// </summary>
        /// <param name="coperationSysNo"></param>
        /// <returns></returns>
        public int IsHavaAttach(int coperationSysNo)
        {
            object count = DbHelperSQL.GetSingle("select top 1 1 from cm_AttachInfo WHERE cpr_Id = " + coperationSysNo);
            return count == null ? 0 : (int)count; ;
        }
        #endregion  Method
    }
}

