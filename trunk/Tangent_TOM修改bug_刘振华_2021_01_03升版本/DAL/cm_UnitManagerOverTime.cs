﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_UnitManagerOverTime
	/// </summary>
	public partial class cm_UnitManagerOverTime
	{
		public cm_UnitManagerOverTime()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "cm_UnitManagerOverTime"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_UnitManagerOverTime");
			strSql.Append(" where id="+id+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_UnitManagerOverTime model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.manager_id != null)
			{
				strSql1.Append("manager_id,");
				strSql2.Append(""+model.manager_id+",");
			}
			if (model.manager_name != null)
			{
				strSql1.Append("manager_name,");
				strSql2.Append("'"+model.manager_name+"',");
			}
			if (model.mem_id != null)
			{
				strSql1.Append("mem_id,");
				strSql2.Append(""+model.mem_id+",");
			}
			if (model.mem_name != null)
			{
				strSql1.Append("mem_name,");
				strSql2.Append("'"+model.mem_name+"',");
			}
			if (model.over_year != null)
			{
				strSql1.Append("over_year,");
				strSql2.Append(""+model.over_year+",");
			}
			if (model.over_month != null)
			{
				strSql1.Append("over_month,");
				strSql2.Append(""+model.over_month+",");
			}
			if (model.over_type != null)
			{
				strSql1.Append("over_type,");
				strSql2.Append("'"+model.over_type+"',");
			}
			strSql.Append("insert into cm_UnitManagerOverTime(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_UnitManagerOverTime model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_UnitManagerOverTime set ");
			if (model.manager_id != null)
			{
				strSql.Append("manager_id="+model.manager_id+",");
			}
			if (model.manager_name != null)
			{
				strSql.Append("manager_name='"+model.manager_name+"',");
			}
			if (model.mem_id != null)
			{
				strSql.Append("mem_id="+model.mem_id+",");
			}
			if (model.mem_name != null)
			{
				strSql.Append("mem_name='"+model.mem_name+"',");
			}
			if (model.over_year != null)
			{
				strSql.Append("over_year="+model.over_year+",");
			}
			else
			{
				strSql.Append("over_year= null ,");
			}
			if (model.over_month != null)
			{
				strSql.Append("over_month="+model.over_month+",");
			}
			else
			{
				strSql.Append("over_month= null ,");
			}
			if (model.over_type != null)
			{
				strSql.Append("over_type='"+model.over_type+"',");
			}
			else
			{
				strSql.Append("over_type= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where id="+ model.id+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_UnitManagerOverTime ");
			strSql.Append(" where id="+id+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_UnitManagerOverTime ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_UnitManagerOverTime GetModel(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" id,manager_id,manager_name,mem_id,mem_name,over_year,over_month,over_type ");
			strSql.Append(" from cm_UnitManagerOverTime ");
			strSql.Append(" where id="+id+"" );
			TG.Model.cm_UnitManagerOverTime model=new TG.Model.cm_UnitManagerOverTime();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_UnitManagerOverTime DataRowToModel(DataRow row)
		{
			TG.Model.cm_UnitManagerOverTime model=new TG.Model.cm_UnitManagerOverTime();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["manager_id"]!=null && row["manager_id"].ToString()!="")
				{
					model.manager_id=int.Parse(row["manager_id"].ToString());
				}
				if(row["manager_name"]!=null)
				{
					model.manager_name=row["manager_name"].ToString();
				}
				if(row["mem_id"]!=null && row["mem_id"].ToString()!="")
				{
					model.mem_id=int.Parse(row["mem_id"].ToString());
				}
				if(row["mem_name"]!=null)
				{
					model.mem_name=row["mem_name"].ToString();
				}
				if(row["over_year"]!=null && row["over_year"].ToString()!="")
				{
					model.over_year=int.Parse(row["over_year"].ToString());
				}
				if(row["over_month"]!=null && row["over_month"].ToString()!="")
				{
					model.over_month=int.Parse(row["over_month"].ToString());
				}
				if(row["over_type"]!=null)
				{
					model.over_type=row["over_type"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,manager_id,manager_name,mem_id,mem_name,over_year,over_month,over_type ");
			strSql.Append(" FROM cm_UnitManagerOverTime ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,manager_id,manager_name,mem_id,mem_name,over_year,over_month,over_type ");
			strSql.Append(" FROM cm_UnitManagerOverTime ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_UnitManagerOverTime ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from cm_UnitManagerOverTime T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
		#region  MethodEx

		#endregion  MethodEx
	}
}

