﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TG.DBUtility;
using TG.Model;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class cm_ProjectPlotInfo
    {
        /// <summary>
        /// 工程设计出图卡
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_ProjectPlotInfoNoByPager(int startIndex, int endIndex, string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", SqlDbType.Int,4),
					new SqlParameter("@endIndex", SqlDbType.Int,4),
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = strWhere;

            return DbHelperSQL.RunProcedure("P_cm_ProjectPlotInfoNo", parameters, "ds");
        }
        /// <summary>
        /// 工程设计出图卡
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_ProjectPlotInfoYesByPager(int startIndex, int endIndex, string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", SqlDbType.Int,4),
					new SqlParameter("@endIndex", SqlDbType.Int,4),
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = strWhere;

            return DbHelperSQL.RunProcedure("P_cm_ProjectPlotInfoYes", parameters, "ds");
        }

        /// <summary>
        /// 获取需要分页的数据总数--工程出图卡
        /// </summary>
        public object GetProjectPlotInfoNoProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_ProjectPlotInfoNo_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }

        /// <summary>
        /// 获取需要分页的数据总数--工程出图卡
        /// </summary>
        public object GetProjectPlotInfoYesProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_ProjectPlotInfoYes_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }

        /// <summary>
        /// 得到设计出图卡信息
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public TG.Model.cm_ProjectPlotInfo GetPlotInfo(int proID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  * from cm_ProjectPlotInfo where pro_Id=@pro_ID");
            SqlParameter[] parameters = {
					new SqlParameter("@pro_ID", SqlDbType.Int,4)
            };
            parameters[0].Value = proID;

            TG.Model.cm_ProjectPlotInfo model = new TG.Model.cm_ProjectPlotInfo();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.Id = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_ID"] != null && ds.Tables[0].Rows[0]["pro_ID"].ToString() != "")
                {
                    model.Pro_id = int.Parse(ds.Tables[0].Rows[0]["pro_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Plot_Type"] != null && ds.Tables[0].Rows[0]["Plot_Type"].ToString() != "")
                {
                    model.PlotType = ds.Tables[0].Rows[0]["Plot_Type"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BlueprintCounts"] != null && ds.Tables[0].Rows[0]["BlueprintCounts"].ToString() != "")
                {
                    model.BlueprintCounts = int.Parse(ds.Tables[0].Rows[0]["BlueprintCounts"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PlotUserID"] != null && ds.Tables[0].Rows[0]["PlotUserID"].ToString() != "")
                {
                    model.PlotUserID = int.Parse(ds.Tables[0].Rows[0]["PlotUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PlotUserName"] != null && ds.Tables[0].Rows[0]["PlotUserName"].ToString() != "")
                {
                    model.PlotUserName = ds.Tables[0].Rows[0]["PlotUserName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FileNum"] != null && ds.Tables[0].Rows[0]["FileNum"].ToString() != "")
                {
                    model.FileNum = ds.Tables[0].Rows[0]["FileNum"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到子项工程出图卡
        /// </summary>
        /// <param name="proId"></param>
        /// <returns></returns>
        public DataSet GetSubPlotInfo(int proId)
        {
            string sql = "select * from cm_ProjectPlotSubInfo where  pro_Id=" + proId + "";
            DataSet ds = DbHelperSQL.Query(sql);
            return ds;
        }
        /// <summary>
        /// 新增工程设计出图卡
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertProjectPlotInfo(ProjectPlotInfoEntity dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //删除
                command.CommandText = "delete from  cm_ProjectPlotInfo where pro_Id=" + dataEntity.ProNo + "";
                command.ExecuteNonQuery();

                //删除
                command.CommandText = "delete from  cm_ProjectPlotSubInfo where pro_Id=" + dataEntity.ProNo + "";
                command.ExecuteNonQuery();

                //新增
                command.CommandText = "insert into cm_ProjectPlotInfo(pro_Id,Plot_Type,BlueprintCounts,PlotUserID,PlotUserName)values(" + dataEntity.ProNo + ",'" + dataEntity.PlotType + "'," + dataEntity.BlueprintCounts + "," + dataEntity.PlotUserID + ",'" + dataEntity.PlotUserName + "')";
                command.ExecuteNonQuery();

                //新规子项
                dataEntity.PlotSub.ForEach((stage) =>
                {
                    command.CommandText = "insert into cm_ProjectPlotSubInfo(pro_Id,Specialty,Mapsheet,StandardCount,QuarterCount,SecondQuarterCount,ThreeQuarterCount,FourQuarterCount,TotalCount)values(" + dataEntity.ProNo + ",'" + stage.Specialty + "','" + stage.Mapsheet + "'," + stage.StandardCount + "," + stage.QuarterCount + "," + stage.SecondQuarterCount + "," + stage.ThreeQuarterCount + "," + stage.FourQuarterCount + "," + stage.TotalCount + ")";
                    command.ExecuteNonQuery();
                });


                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        /// 查询信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public List<ProjectPlotAuditList> GetProjectPlotAuditListView(string query, int startIndex, int endIndex)
        {
            List<ProjectPlotAuditList> resultList = new List<ProjectPlotAuditList>();

            SqlDataReader reader = DBUtility.DbHelperSQL.RunProcedure("P_cm_ProjectPlotInfoYes", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });

            while (reader.Read())
            {
                ProjectPlotAuditList auditListView = new ProjectPlotAuditList
                {
                    Pro_ID = Convert.ToInt32(reader["pro_ID"]),
                    Pro_name = reader["pro_name"] == null ? string.Empty : reader["pro_name"].ToString(),
                    Pro_level = reader["pro_level"] == null ? string.Empty : reader["pro_level"].ToString(),
                    AuditLevel = reader["AuditLevel"] == null ? string.Empty : reader["AuditLevel"].ToString(),
                    Unit = reader["Unit"] == null ? string.Empty : reader["Unit"].ToString(),
                    Pro_number = reader["Pro_number"] == null ? string.Empty : reader["Pro_number"].ToString(),
                    PMName = reader["PMName"] == null ? string.Empty : reader["PMName"].ToString(),
                    Pro_status = reader["pro_status"] == null ? string.Empty : reader["pro_status"].ToString(),
                    InsertUserID = Convert.ToInt32(reader["InsertUserID"]),
                    Pro_startTime = reader["pro_startTime"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["pro_startTime"].ToString()),
                    Status = reader["Status"] == null ? string.Empty : reader["Status"].ToString(),
                    PlotID = Convert.ToInt32(reader["PlotID"]),
                    Plot_Type = reader["Plot_Type"] == null ? string.Empty : reader["Plot_Type"].ToString()
                };
                resultList.Add(auditListView);
            }
            return resultList;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectPlotInfoAudit GetModelByProIDSysNo(int? SysNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * ");
            strSql.Append(" from cm_ProjectPlotInfoAudit ");
            strSql.Append(" where ProjectSysNo=" + SysNo + " order by SysNo DESC");

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

            reader.Read();

            TG.Model.cm_ProjectPlotInfoAudit projectPlotInfoAudit = TG.Common.EntityBuilder.EntityBuilder<TG.Model.cm_ProjectPlotInfoAudit>.BuilderEntity(reader);

            reader.Close();

            return projectPlotInfoAudit;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectPlotInfoAudit GetProjectPlotAuditModelSysNo(int? SysNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * ");
            strSql.Append(" from cm_ProjectPlotInfoAudit ");
            strSql.Append(" where SysNo=" + SysNo + " order by SysNo DESC");

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

            reader.Read();

            TG.Model.cm_ProjectPlotInfoAudit projectPlotInfoAudit = TG.Common.EntityBuilder.EntityBuilder<TG.Model.cm_ProjectPlotInfoAudit>.BuilderEntity(reader);

            reader.Close();

            return projectPlotInfoAudit;
        }
        /// <summary>
        /// 新规一条审批记录
        /// </summary>
        /// <param name="coperation"></param>
        /// <returns></returns>
        public int InsertProjectPlotAuditRecord(TG.Model.cm_ProjectPlotInfoAudit plot)
        {
            string sql = "insert into cm_ProjectPlotInfoAudit(ProjectSysNo,InUser,InDate,Status) values(";
            sql += "'" + plot.ProjectSysNo + "',";
            sql += "'" + plot.InUser + "',";
            sql += "'" + DateTime.Now + "',";
            sql += "'A');select @@IDENTITY";
            int count = Convert.ToInt32(DbHelperSQL.GetSingle(sql));
            return count;
        }

        /// <summary>
        /// 得到审核信息
        /// </summary>
        /// <param name="queryEntity"></param>
        /// <returns></returns>
        public cm_ProjectPlotInfoAudit GetProjectPlotInfoAuditEntity(cm_ProjectPlotAuditQueryEntity queryEntity)
        {
            string whereSql = " where 1=1 ";
            whereSql += " and SysNo =" + queryEntity.ProjectPlotAuditSysNo;

            if (queryEntity.ProjectSysNo != 0)
            {
                whereSql += " and ProjectSysNo=" + queryEntity.ProjectSysNo;
            }
            string sql = "select SysNo,ProjectSysNo,Status,Suggestion,AuditUser,AuditDate,InDate,InUser from cm_ProjectPlotInfoAudit " + whereSql;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();
            cm_ProjectPlotInfoAudit resultEntity = EntityBuilder<cm_ProjectPlotInfoAudit>.BuilderEntity(reader);
            reader.Close();

            return resultEntity;
        }

        /// <summary>
        /// 得到审核配置信息
        /// </summary>
        /// <returns></returns>
        public List<string> GetAuditProcessDescription()
        {
            string sql = "select r.RoleName from cm_ProjectPlotInfoConfig  ppc join cm_Role r on  r.SysNo = ppc.RoleSysNo order by ppc.position asc";

            List<string> resultString = new List<string>();

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    resultString.Add(reader["RoleName"].ToString());
                }
                reader.Close();
            }

            return resultString;
        }

        /// <summary>
        /// 更新审核信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateProjectPlanAudit(cm_ProjectPlotInfoAudit dataEntity)
        {
            string ChildrenSql = "";

            if (!string.IsNullOrEmpty(dataEntity.Suggestion))
            {

                ChildrenSql += " Suggestion = N'" + dataEntity.Suggestion + "|',";
            }
            if (!string.IsNullOrEmpty(dataEntity.AuditUser))
            {
                ChildrenSql += " AuditUser = N'" + dataEntity.AuditUser + ",',";
            }
            if (!string.IsNullOrEmpty(dataEntity.AuditDate))
            {
                ChildrenSql += " AuditDate = N'" + dataEntity.AuditDate + ",',";
            }
            if (!string.IsNullOrEmpty(dataEntity.Status))
            {
                ChildrenSql += " Status=N'" + dataEntity.Status + "',";
            }
            if (!string.IsNullOrEmpty(ChildrenSql))
            {
                string finalSql = "UPDATE cm_ProjectPlotInfoAudit SET " + ChildrenSql.Substring(0, ChildrenSql.Length - 1) + " WHERE SysNo=" + dataEntity.SysNo;
                return DbHelperSQL.ExecuteSql(finalSql);
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// 更新档案号
        /// </summary>
        /// <param name="ProID"></param>
        /// <param name="fileNum"></param>
        /// <returns></returns>
        public int UpdateFileNum(int ProID, string fileNum)
        {
            string sql = "update cm_ProjectPlotInfo set FileNum='" + fileNum + "' where pro_Id=" + ProID + "";
            return DbHelperSQL.ExecuteSql(sql);
        }
    }
}
