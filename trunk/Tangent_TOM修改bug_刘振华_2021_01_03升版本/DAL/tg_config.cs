﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:tg_config
	/// </summary>
	public partial class tg_config
	{
		public tg_config()
		{}
		#region  Method


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(string conf_name)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from tg_config");
			strSql.Append(" where conf_name='"+conf_name+"' ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TG.Model.tg_config model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.conf_name != null)
			{
				strSql1.Append("conf_name,");
				strSql2.Append("'"+model.conf_name+"',");
			}
			if (model.conf_value != null)
			{
				strSql1.Append("conf_value,");
				strSql2.Append("'"+model.conf_value+"',");
			}
			if (model.conf_desc != null)
			{
				strSql1.Append("conf_desc,");
				strSql2.Append("'"+model.conf_desc+"',");
			}
			strSql.Append("insert into tg_config(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_config model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update tg_config set ");
			if (model.conf_value != null)
			{
				strSql.Append("conf_value='"+model.conf_value+"',");
			}
			if (model.conf_desc != null)
			{
				strSql.Append("conf_desc='"+model.conf_desc+"',");
			}
			else
			{
				strSql.Append("conf_desc= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where conf_name='"+ model.conf_name+"' ");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(string conf_name)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_config ");
			strSql.Append(" where conf_name='"+conf_name+"' " );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string conf_namelist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_config ");
			strSql.Append(" where conf_name in ("+conf_namelist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_config GetModel(string conf_name)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" conf_name,conf_value,conf_desc ");
			strSql.Append(" from tg_config ");
			strSql.Append(" where conf_name='"+conf_name+"' " );
			TG.Model.tg_config model=new TG.Model.tg_config();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["conf_name"]!=null && ds.Tables[0].Rows[0]["conf_name"].ToString()!="")
				{
					model.conf_name=ds.Tables[0].Rows[0]["conf_name"].ToString();
				}
				if(ds.Tables[0].Rows[0]["conf_value"]!=null && ds.Tables[0].Rows[0]["conf_value"].ToString()!="")
				{
					model.conf_value=ds.Tables[0].Rows[0]["conf_value"].ToString();
				}
				if(ds.Tables[0].Rows[0]["conf_desc"]!=null && ds.Tables[0].Rows[0]["conf_desc"].ToString()!="")
				{
					model.conf_desc=ds.Tables[0].Rows[0]["conf_desc"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select conf_name,conf_value,conf_desc ");
			strSql.Append(" FROM tg_config ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" conf_name,conf_value,conf_desc ");
			strSql.Append(" FROM tg_config ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM tg_config ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.conf_name desc");
			}
			strSql.Append(")AS Row, T.*  from tg_config T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

