﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeProjName
	/// </summary>
	public partial class cm_KaoHeProjName
	{
		public cm_KaoHeProjName()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeProjName model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeProjName(");
			strSql.Append("KaoHeProjId,KName,AllScale,DoneScale,Sub,LeftScale,HisScale,proID,InserUserID,InsertDate,Stat)");
			strSql.Append(" values (");
			strSql.Append("@KaoHeProjId,@KName,@AllScale,@DoneScale,@Sub,@LeftScale,@HisScale,@proID,@InserUserID,@InsertDate,@Stat)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@KaoHeProjId", SqlDbType.Int,4),
					new SqlParameter("@KName", SqlDbType.NVarChar,50),
					new SqlParameter("@AllScale", SqlDbType.Decimal,9),
					new SqlParameter("@DoneScale", SqlDbType.Decimal,9),
					new SqlParameter("@Sub", SqlDbType.NVarChar,50),
					new SqlParameter("@LeftScale", SqlDbType.Decimal,9),
					new SqlParameter("@HisScale", SqlDbType.Decimal,9),
					new SqlParameter("@proID", SqlDbType.Int,4),
					new SqlParameter("@InserUserID", SqlDbType.Int,4),
					new SqlParameter("@InsertDate", SqlDbType.DateTime),
					new SqlParameter("@Stat", SqlDbType.Int,4)};
			parameters[0].Value = model.KaoHeProjId;
			parameters[1].Value = model.KName;
			parameters[2].Value = model.AllScale;
			parameters[3].Value = model.DoneScale;
			parameters[4].Value = model.Sub;
			parameters[5].Value = model.LeftScale;
			parameters[6].Value = model.HisScale;
			parameters[7].Value = model.proID;
			parameters[8].Value = model.InserUserID;
			parameters[9].Value = model.InsertDate;
			parameters[10].Value = model.Stat;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeProjName model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeProjName set ");
			strSql.Append("KaoHeProjId=@KaoHeProjId,");
			strSql.Append("KName=@KName,");
			strSql.Append("AllScale=@AllScale,");
			strSql.Append("DoneScale=@DoneScale,");
			strSql.Append("Sub=@Sub,");
			strSql.Append("LeftScale=@LeftScale,");
			strSql.Append("HisScale=@HisScale,");
			strSql.Append("proID=@proID,");
			strSql.Append("InserUserID=@InserUserID,");
			strSql.Append("InsertDate=@InsertDate,");
			strSql.Append("Stat=@Stat");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@KaoHeProjId", SqlDbType.Int,4),
					new SqlParameter("@KName", SqlDbType.NVarChar,50),
					new SqlParameter("@AllScale", SqlDbType.Decimal,9),
					new SqlParameter("@DoneScale", SqlDbType.Decimal,9),
					new SqlParameter("@Sub", SqlDbType.NVarChar,50),
					new SqlParameter("@LeftScale", SqlDbType.Decimal,9),
					new SqlParameter("@HisScale", SqlDbType.Decimal,9),
					new SqlParameter("@proID", SqlDbType.Int,4),
					new SqlParameter("@InserUserID", SqlDbType.Int,4),
					new SqlParameter("@InsertDate", SqlDbType.DateTime),
					new SqlParameter("@Stat", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.KaoHeProjId;
			parameters[1].Value = model.KName;
			parameters[2].Value = model.AllScale;
			parameters[3].Value = model.DoneScale;
			parameters[4].Value = model.Sub;
			parameters[5].Value = model.LeftScale;
			parameters[6].Value = model.HisScale;
			parameters[7].Value = model.proID;
			parameters[8].Value = model.InserUserID;
			parameters[9].Value = model.InsertDate;
			parameters[10].Value = model.Stat;
			parameters[11].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeProjName ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeProjName ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeProjName GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,KaoHeProjId,KName,AllScale,DoneScale,Sub,LeftScale,HisScale,proID,InserUserID,InsertDate,Stat from cm_KaoHeProjName ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeProjName model=new TG.Model.cm_KaoHeProjName();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeProjName DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeProjName model=new TG.Model.cm_KaoHeProjName();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["KaoHeProjId"]!=null && row["KaoHeProjId"].ToString()!="")
				{
					model.KaoHeProjId=int.Parse(row["KaoHeProjId"].ToString());
				}
				if(row["KName"]!=null)
				{
					model.KName=row["KName"].ToString();
				}
				if(row["AllScale"]!=null && row["AllScale"].ToString()!="")
				{
					model.AllScale=decimal.Parse(row["AllScale"].ToString());
				}
				if(row["DoneScale"]!=null && row["DoneScale"].ToString()!="")
				{
					model.DoneScale=decimal.Parse(row["DoneScale"].ToString());
				}
				if(row["Sub"]!=null)
				{
					model.Sub=row["Sub"].ToString();
				}
				if(row["LeftScale"]!=null && row["LeftScale"].ToString()!="")
				{
					model.LeftScale=decimal.Parse(row["LeftScale"].ToString());
				}
				if(row["HisScale"]!=null && row["HisScale"].ToString()!="")
				{
					model.HisScale=decimal.Parse(row["HisScale"].ToString());
				}
				if(row["proID"]!=null && row["proID"].ToString()!="")
				{
					model.proID=int.Parse(row["proID"].ToString());
				}
				if(row["InserUserID"]!=null && row["InserUserID"].ToString()!="")
				{
					model.InserUserID=int.Parse(row["InserUserID"].ToString());
				}
				if(row["InsertDate"]!=null && row["InsertDate"].ToString()!="")
				{
					model.InsertDate=DateTime.Parse(row["InsertDate"].ToString());
				}
				if(row["Stat"]!=null && row["Stat"].ToString()!="")
				{
					model.Stat=int.Parse(row["Stat"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,KaoHeProjId,KName,AllScale,DoneScale,Sub,LeftScale,HisScale,proID,InserUserID,InsertDate,Stat ");
			strSql.Append(" FROM cm_KaoHeProjName ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,KaoHeProjId,KName,AllScale,DoneScale,Sub,LeftScale,HisScale,proID,InserUserID,InsertDate,Stat ");
			strSql.Append(" FROM cm_KaoHeProjName ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeProjName ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeProjName T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeProjName";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

