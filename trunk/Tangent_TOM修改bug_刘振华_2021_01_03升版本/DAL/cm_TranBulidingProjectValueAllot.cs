﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.DBUtility;
using System.Data.SqlClient;

namespace TG.DAL
{
    public partial class cm_TranBulidingProjectValueAllot
    {
        public cm_TranBulidingProjectValueAllot()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_TranBulidingProjectValueAllot model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_TranBulidingProjectValueAllot(");
            strSql.Append("Pro_ID,AllotID,TotalCount,Thedeptallotpercent,Thedeptallotcount,AllotPercent,AllotCount,ProgramPercent,ProgramCount,ItemType,Status,AllotDate,AllotUser,ActualAllountTime,DesignUserName,DesignUserID,DesignManagerPercent,DesignManagerCount,LastItemType,mark)");
            strSql.Append(" values (");
            strSql.Append("@Pro_ID,@AllotID,@TotalCount,@Thedeptallotpercent,@Thedeptallotcount,@AllotPercent,@AllotCount,@ProgramPercent,@ProgramCount,@ItemType,@Status,@AllotDate,@AllotUser,@ActualAllountTime,@DesignUserName,@DesignUserID,@DesignManagerPercent,@DesignManagerCount,@LastItemType,@mark)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@Pro_ID", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@TotalCount", SqlDbType.Decimal,9),
					new SqlParameter("@Thedeptallotpercent", SqlDbType.Decimal,9),
					new SqlParameter("@Thedeptallotcount", SqlDbType.Decimal,9),
					new SqlParameter("@AllotPercent", SqlDbType.Decimal,9),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,9),
					new SqlParameter("@ProgramPercent", SqlDbType.Decimal,9),
					new SqlParameter("@ProgramCount", SqlDbType.Decimal,9),
					new SqlParameter("@ItemType", SqlDbType.NVarChar,10),
					new SqlParameter("@Status", SqlDbType.NChar,1),
					new SqlParameter("@AllotDate", SqlDbType.DateTime),
					new SqlParameter("@AllotUser", SqlDbType.Int,4),
                    new SqlParameter("@ActualAllountTime",SqlDbType.NChar,10),
                    new SqlParameter("@DesignUserName",SqlDbType.NChar,30),
                    new SqlParameter("@DesignUserID", SqlDbType.Int,4),  
                    new SqlParameter("@DesignManagerPercent", SqlDbType.Decimal,9),
					new SqlParameter("@DesignManagerCount", SqlDbType.Decimal,9) ,  
                    new SqlParameter("@LastItemType", SqlDbType.NChar,10) ,
                    new SqlParameter("@mark", SqlDbType.NVarChar,300) };
            parameters[0].Value = model.Pro_ID;
            parameters[1].Value = model.AllotID;
            parameters[2].Value = model.TotalCount;
            parameters[3].Value = model.Thedeptallotpercent;
            parameters[4].Value = model.Thedeptallotcount;
            parameters[5].Value = model.AllotPercent;
            parameters[6].Value = model.AllotCount;
            parameters[7].Value = model.ProgramPercent;
            parameters[8].Value = model.ProgramCount;
            parameters[9].Value = model.ItemType;
            parameters[10].Value = model.Status;
            parameters[11].Value = model.AllotDate;
            parameters[12].Value = model.AllotUser;
            parameters[13].Value = model.ActualAllountTime;
            parameters[14].Value = model.DesignUserName;
            parameters[15].Value = model.DesignUserID;
            parameters[16].Value = model.DesignManagerPercent;
            parameters[17].Value = model.DesignManagerCount;
            parameters[18].Value = model.LastItemType;
            parameters[19].Value = model.Mark;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_TranBulidingProjectValueAllot model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_TranBulidingProjectValueAllot set ");
            strSql.Append("Pro_ID=@Pro_ID,");
            strSql.Append("AllotID=@AllotID,");
            strSql.Append("TotalCount=@TotalCount,");
            strSql.Append("Thedeptallotpercent=@Thedeptallotpercent,");
            strSql.Append("Thedeptallotcount=@Thedeptallotcount,");
            strSql.Append("AllotPercent=@AllotPercent,");
            strSql.Append("AllotCount=@AllotCount,");
            strSql.Append("ProgramPercent=@ProgramPercent,");
            strSql.Append("ProgramCount=@ProgramCount,");
            strSql.Append("ItemType=@ItemType,");
            strSql.Append("Status=@Status,");
            strSql.Append("AllotDate=@AllotDate,");
            strSql.Append("AllotUser=@AllotUser,");
            strSql.Append("DesignUserName=@DesignUserName,");
            strSql.Append("DesignUserID=@DesignUserID,");
            strSql.Append("DesignManagerPercent=@DesignManagerPercent,");
            strSql.Append("DesignManagerCount=@DesignManagerCount,");
            strSql.Append("ActualAllountTime=@ActualAllountTime,");
            strSql.Append("mark=@mark");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@Pro_ID", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@TotalCount", SqlDbType.Decimal,9),
					new SqlParameter("@Thedeptallotpercent", SqlDbType.Decimal,9),
					new SqlParameter("@Thedeptallotcount", SqlDbType.Decimal,9),
					new SqlParameter("@AllotPercent", SqlDbType.Decimal,9),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,9),
					new SqlParameter("@ProgramPercent", SqlDbType.Decimal,9),
					new SqlParameter("@ProgramCount", SqlDbType.Decimal,9),
					new SqlParameter("@ItemType", SqlDbType.NVarChar,10),
					new SqlParameter("@Status", SqlDbType.NChar,1),
					new SqlParameter("@AllotDate", SqlDbType.DateTime),
					new SqlParameter("@AllotUser", SqlDbType.Int,4),
                    new SqlParameter("@DesignUserName",SqlDbType.NChar,30),
                    new SqlParameter("@DesignUserID", SqlDbType.Int,4),  
                    new SqlParameter("@DesignManagerPercent", SqlDbType.Decimal,9),
					new SqlParameter("@DesignManagerCount", SqlDbType.Decimal,9) ,
                    new SqlParameter("@ActualAllountTime",SqlDbType.NChar,10),
                     new SqlParameter("@mark",SqlDbType.NVarChar,300),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.Pro_ID;
            parameters[1].Value = model.AllotID;
            parameters[2].Value = model.TotalCount;
            parameters[3].Value = model.Thedeptallotpercent;
            parameters[4].Value = model.Thedeptallotcount;
            parameters[5].Value = model.AllotPercent;
            parameters[6].Value = model.AllotCount;
            parameters[7].Value = model.ProgramPercent;
            parameters[8].Value = model.ProgramCount;
            parameters[9].Value = model.ItemType;
            parameters[10].Value = model.Status;
            parameters[11].Value = model.AllotDate;
            parameters[12].Value = model.AllotUser;
            parameters[13].Value = model.DesignUserName;
            parameters[14].Value = model.DesignUserID;
            parameters[15].Value = model.DesignManagerPercent;
            parameters[16].Value = model.DesignManagerCount;
            parameters[17].Value = model.ActualAllountTime;
            parameters[18].Value = model.Mark;
            parameters[19].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_TranBulidingProjectValueAllot GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,Pro_ID,AllotID,TotalCount,Thedeptallotpercent,Thedeptallotcount,AllotPercent,AllotCount,ProgramPercent,ProgramCount,ItemType,Status,AllotDate,AllotUser,LastItemType from cm_TranBulidingProjectValueAllot ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_TranBulidingProjectValueAllot model = new TG.Model.cm_TranBulidingProjectValueAllot();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Pro_ID"] != null && ds.Tables[0].Rows[0]["Pro_ID"].ToString() != "")
                {
                    model.Pro_ID = int.Parse(ds.Tables[0].Rows[0]["Pro_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotID"] != null && ds.Tables[0].Rows[0]["AllotID"].ToString() != "")
                {
                    model.AllotID = int.Parse(ds.Tables[0].Rows[0]["AllotID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalCount"] != null && ds.Tables[0].Rows[0]["TotalCount"].ToString() != "")
                {
                    model.TotalCount = decimal.Parse(ds.Tables[0].Rows[0]["TotalCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Thedeptallotpercent"] != null && ds.Tables[0].Rows[0]["Thedeptallotpercent"].ToString() != "")
                {
                    model.Thedeptallotpercent = decimal.Parse(ds.Tables[0].Rows[0]["Thedeptallotpercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Thedeptallotcount"] != null && ds.Tables[0].Rows[0]["Thedeptallotcount"].ToString() != "")
                {
                    model.Thedeptallotcount = decimal.Parse(ds.Tables[0].Rows[0]["Thedeptallotcount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotPercent"] != null && ds.Tables[0].Rows[0]["AllotPercent"].ToString() != "")
                {
                    model.AllotPercent = decimal.Parse(ds.Tables[0].Rows[0]["AllotPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotCount"] != null && ds.Tables[0].Rows[0]["AllotCount"].ToString() != "")
                {
                    model.AllotCount = decimal.Parse(ds.Tables[0].Rows[0]["AllotCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProgramPercent"] != null && ds.Tables[0].Rows[0]["ProgramPercent"].ToString() != "")
                {
                    model.ProgramPercent = decimal.Parse(ds.Tables[0].Rows[0]["ProgramPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProgramCount"] != null && ds.Tables[0].Rows[0]["ProgramCount"].ToString() != "")
                {
                    model.ProgramCount = decimal.Parse(ds.Tables[0].Rows[0]["ProgramCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ItemType"] != null && ds.Tables[0].Rows[0]["ItemType"].ToString() != "")
                {
                    model.ItemType = ds.Tables[0].Rows[0]["ItemType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AllotDate"] != null && ds.Tables[0].Rows[0]["AllotDate"].ToString() != "")
                {
                    model.AllotDate = DateTime.Parse(ds.Tables[0].Rows[0]["AllotDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotUser"] != null && ds.Tables[0].Rows[0]["AllotUser"].ToString() != "")
                {
                    model.AllotUser = int.Parse(ds.Tables[0].Rows[0]["AllotUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LastItemType"] != null && ds.Tables[0].Rows[0]["LastItemType"].ToString() != "")
                {
                    model.LastItemType = ds.Tables[0].Rows[0]["LastItemType"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_TranBulidingProjectValueAllot GetModelByProID(int proid, int allotID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from cm_TranBulidingProjectValueAllot ");
            strSql.Append(" where Pro_ID=@Pro_ID and AllotID=@AllotID");
            SqlParameter[] parameters = {
					new SqlParameter("@Pro_ID", SqlDbType.Int,4),
                    new SqlParameter("@AllotID", SqlDbType.Int,4)
			};
            parameters[0].Value = proid;
            parameters[1].Value = allotID;

            TG.Model.cm_TranBulidingProjectValueAllot model = new TG.Model.cm_TranBulidingProjectValueAllot();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Pro_ID"] != null && ds.Tables[0].Rows[0]["Pro_ID"].ToString() != "")
                {
                    model.Pro_ID = int.Parse(ds.Tables[0].Rows[0]["Pro_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotID"] != null && ds.Tables[0].Rows[0]["AllotID"].ToString() != "")
                {
                    model.AllotID = int.Parse(ds.Tables[0].Rows[0]["AllotID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalCount"] != null && ds.Tables[0].Rows[0]["TotalCount"].ToString() != "")
                {
                    model.TotalCount = decimal.Parse(ds.Tables[0].Rows[0]["TotalCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Thedeptallotpercent"] != null && ds.Tables[0].Rows[0]["Thedeptallotpercent"].ToString() != "")
                {
                    model.Thedeptallotpercent = decimal.Parse(ds.Tables[0].Rows[0]["Thedeptallotpercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Thedeptallotcount"] != null && ds.Tables[0].Rows[0]["Thedeptallotcount"].ToString() != "")
                {
                    model.Thedeptallotcount = decimal.Parse(ds.Tables[0].Rows[0]["Thedeptallotcount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotPercent"] != null && ds.Tables[0].Rows[0]["AllotPercent"].ToString() != "")
                {
                    model.AllotPercent = decimal.Parse(ds.Tables[0].Rows[0]["AllotPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotCount"] != null && ds.Tables[0].Rows[0]["AllotCount"].ToString() != "")
                {
                    model.AllotCount = decimal.Parse(ds.Tables[0].Rows[0]["AllotCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProgramPercent"] != null && ds.Tables[0].Rows[0]["ProgramPercent"].ToString() != "")
                {
                    model.ProgramPercent = decimal.Parse(ds.Tables[0].Rows[0]["ProgramPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProgramCount"] != null && ds.Tables[0].Rows[0]["ProgramCount"].ToString() != "")
                {
                    model.ProgramCount = decimal.Parse(ds.Tables[0].Rows[0]["ProgramCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ItemType"] != null && ds.Tables[0].Rows[0]["ItemType"].ToString() != "")
                {
                    model.ItemType = ds.Tables[0].Rows[0]["ItemType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AllotDate"] != null && ds.Tables[0].Rows[0]["AllotDate"].ToString() != "")
                {
                    model.AllotDate = DateTime.Parse(ds.Tables[0].Rows[0]["AllotDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotUser"] != null && ds.Tables[0].Rows[0]["AllotUser"].ToString() != "")
                {
                    model.AllotUser = int.Parse(ds.Tables[0].Rows[0]["AllotUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ActualAllountTime"] != null && ds.Tables[0].Rows[0]["ActualAllountTime"].ToString() != "")
                {
                    model.ActualAllountTime = ds.Tables[0].Rows[0]["ActualAllountTime"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DesignUserID"] != null && ds.Tables[0].Rows[0]["DesignUserID"].ToString() != "")
                {
                    model.DesignUserID = int.Parse(ds.Tables[0].Rows[0]["DesignUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DesignUserName"] != null && ds.Tables[0].Rows[0]["DesignUserName"].ToString() != "")
                {
                    model.DesignUserName = ds.Tables[0].Rows[0]["DesignUserName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DesignManagerPercent"] != null && ds.Tables[0].Rows[0]["DesignManagerPercent"].ToString() != "")
                {
                    model.DesignManagerPercent = decimal.Parse(ds.Tables[0].Rows[0]["DesignManagerPercent"].ToString());
                }
                else
                {
                    model.DesignManagerPercent = 0;
                }
                if (ds.Tables[0].Rows[0]["DesignManagerCount"] != null && ds.Tables[0].Rows[0]["DesignManagerCount"].ToString() != "")
                {
                    model.DesignManagerCount = decimal.Parse(ds.Tables[0].Rows[0]["DesignManagerCount"].ToString());
                }
                else
                {
                    model.DesignManagerCount = 0;
                }
                if (ds.Tables[0].Rows[0]["LastItemType"] != null && ds.Tables[0].Rows[0]["LastItemType"].ToString() != "")
                {
                    model.LastItemType = ds.Tables[0].Rows[0]["LastItemType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mark"] != null && ds.Tables[0].Rows[0]["mark"].ToString() != "")
                {
                    model.Mark = ds.Tables[0].Rows[0]["mark"].ToString();
                }

                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,Pro_ID,AllotID,TotalCount,Thedeptallotpercent,Thedeptallotcount,AllotPercent,AllotCount,ProgramPercent,ProgramCount,ItemType,Status,AllotDate,AllotUser ");
            strSql.Append(" FROM cm_TranBulidingProjectValueAllot ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }


        #endregion  Method
    }
}

