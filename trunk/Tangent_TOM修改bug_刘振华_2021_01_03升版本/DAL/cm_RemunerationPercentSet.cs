﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TG.DBUtility;
using System.Data.SqlClient;

namespace TG.DAL
{
    public partial class cm_RemunerationPercentSet
    {
        public cm_RemunerationPercentSet()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_RemunerationPercentSet model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_RemunerationPercentSet(");
            strSql.Append("unit_id,Remuneration_Year,Remuneration_percent)");
            strSql.Append(" values (");
            strSql.Append("@unit_id,@Remuneration_Year,@Remuneration_percent)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@unit_id", SqlDbType.Int,4),
					new SqlParameter("@Remuneration_Year", SqlDbType.NChar,10),
					new SqlParameter("@Remuneration_percent", SqlDbType.Decimal,9)};
            parameters[0].Value = model.unit_id;
            parameters[1].Value = model.Remuneration_Year;
            parameters[2].Value = model.Remuneration_percent;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_RemunerationPercentSet model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_RemunerationPercentSet set ");
            strSql.Append("Remuneration_percent=@Remuneration_percent");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@Remuneration_percent", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.Remuneration_percent;
            parameters[1].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_RemunerationPercentSet ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_RemunerationPercentSet ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_RemunerationPercentSet GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,unit_id,Remuneration_Year,Remuneration_percent from cm_RemunerationPercentSet ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_RemunerationPercentSet model = new TG.Model.cm_RemunerationPercentSet();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["unit_id"] != null && ds.Tables[0].Rows[0]["unit_id"].ToString() != "")
                {
                    model.unit_id = int.Parse(ds.Tables[0].Rows[0]["unit_id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Remuneration_Year"] != null && ds.Tables[0].Rows[0]["Remuneration_Year"].ToString() != "")
                {
                    model.Remuneration_Year = ds.Tables[0].Rows[0]["Remuneration_Year"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Remuneration_percent"] != null && ds.Tables[0].Rows[0]["Remuneration_percent"].ToString() != "")
                {
                    model.Remuneration_percent = decimal.Parse(ds.Tables[0].Rows[0]["Remuneration_percent"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,unit_id,Remuneration_Year,Remuneration_percent ");
            strSql.Append(" FROM cm_RemunerationPercentSet ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex, string type)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_CoefficientSetting", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query), new SqlParameter("@type", type) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query, string type)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_CoefficientSetting_Count", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@type", type) });
        }
        #endregion  Method
    }
}
