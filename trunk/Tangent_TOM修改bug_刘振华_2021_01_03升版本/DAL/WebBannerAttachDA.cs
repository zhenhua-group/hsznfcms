﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using TG.DBUtility;
using System.Data.SqlClient;

namespace TG.DAL
{
    public class WebBannerAttachDA
    {
        public int AddWebBannerAttach(WebBannerAttachViewEntity dataEntity)
        {
            string sql = string.Format(@"insert into cm_WebBannerAttach(WebBannerSysNo,RelativePath,FileName,FileType,FileSize,IconPath)values(N'{0}',N'{1}',N'{2}',N'{3}',{4},N'{5}');select @@identity",
                                                                                                                                        dataEntity.WebBannerSysNo,
                                                                                                                                        dataEntity.RelativePath,
                                                                                                                                        dataEntity.FileName,
                                                                                                                                        dataEntity.FileType,
                                                                                                                                        dataEntity.FileSize,
                                                                                                                                        dataEntity.IconPath);

            int sysNo = Convert.ToInt32(DbHelperSQL.GetSingle(sql));
            return sysNo;
        }

        public int UpdateWebBannerAttach(string oldTempSysNo, int replaceSysNo)
        {
            string sql = string.Format("update cm_WebBannerAttach set WebBannerSysNo=N'{0}' where WebBannerSysNo=N'{1}'", replaceSysNo, oldTempSysNo);

            int count = DbHelperSQL.ExecuteSql(sql);

            return count;
        }

        public List<WebBannerAttachViewEntity> GetWebBannerViewEntityList(string webBannerSysNo)
        {
            string sql = string.Format("select * from cm_WebBannerAttach where WebBannerSysNo=N'{0}'", webBannerSysNo);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<WebBannerAttachViewEntity> resultList = TG.Common.EntityBuilder.EntityBuilder<WebBannerAttachViewEntity>.BuilderEntityList(reader);

            return resultList;
        }

        public bool ExistsSameNameByFile(string fileName, out int sameNameFileCount)
        {
            sameNameFileCount = 0;

            string sql = string.Format("select  top 1 1 from cm_WebBannerAttach where fileName=N'{0}'", fileName);

            object objResult = DbHelperSQL.GetSingle(sql);

            sameNameFileCount = Convert.ToInt32(DbHelperSQL.GetSingle("select isnull(count(*),0) from cm_WebBannerAttach where fileName=N'" + fileName + "'"));

            return objResult == null ? false : true;
        }
    }
}
