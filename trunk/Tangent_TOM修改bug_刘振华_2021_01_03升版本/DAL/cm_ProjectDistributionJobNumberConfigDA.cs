﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using System.Data.SqlClient;
using TG.DBUtility;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
	public class cm_ProjectDistributionJobNumberConfigDA
	{
		public List<cm_ProjectDistributionJobNumberConfigEntity> GetDistributionJonNumberConfigEntityList()
		{
			string sql = "select dn.SysNo,dn.ProcessDescription,dn.RoleSysNo,dn.Position,dn.InUser,dn.InDate,r.Users,r.RoleName from cm_ProjectDistributionJobNumberConfig dn join cm_Role r on r.SysNo = dn.RoleSysNo";

			SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

			List<cm_ProjectDistributionJobNumberConfigEntity> resultList = EntityBuilder<cm_ProjectDistributionJobNumberConfigEntity>.BuilderEntityList(reader);

			return resultList;
		}

		public cm_ProjectDistributionJobNumberConfigEntity GetDistributionJobNumberConfig(int position)
		{
			string sql = "select dn.SysNo,dn.ProcessDescription,dn.RoleSysNo,dn.Position,dn.InUser,dn.InDate,r.Users,r.RoleName from cm_ProjectDistributionJobNumberConfig dn join cm_Role r on r.SysNo = dn.RoleSysNo where dn.Position = " + position;

			SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

			reader.Read();

			cm_ProjectDistributionJobNumberConfigEntity resultEntity = EntityBuilder<cm_ProjectDistributionJobNumberConfigEntity>.BuilderEntity(reader);

			reader.Close();

			return resultEntity;
		}
	}
}
