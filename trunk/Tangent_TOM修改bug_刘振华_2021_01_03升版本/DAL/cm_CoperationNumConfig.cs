﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using System.Collections.Generic;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_CoperationNumConfig
    /// </summary>
    public partial class cm_CoperationNumConfig
    {
        public cm_CoperationNumConfig()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(TG.Model.cm_CoperationNumConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.ID != null)
            {
                strSql1.Append("ID,");
                strSql2.Append("" + model.ID + ",");
            }
            if (model.CprPfix != null)
            {
                strSql1.Append("CprPfix,");
                strSql2.Append("'" + model.CprPfix + "',");
            }
            if (model.CprNumStart != null)
            {
                strSql1.Append("CprNumStart,");
                strSql2.Append("" + model.CprNumStart + ",");
            }
            if (model.CprNumEnd != null)
            {
                strSql1.Append("CprNumEnd,");
                strSql2.Append("" + model.CprNumEnd + ",");
            }
            strSql.Append("insert into cm_CoperationNumConfig(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CoperationNumConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_CoperationNumConfig set ");
            if (model.CprPfix != null)
            {
                strSql.Append("CprPfix='" + model.CprPfix + "',");
            }
            else
            {
                strSql.Append("CprPfix= N'' ,");
            }
            if (model.CprNumStart != null)
            {
                strSql.Append("CprNumStart=N'" + model.CprNumStart + "',");
            }
            else
            {
                strSql.Append("CprNumStart= N'' ,");
            }
            if (model.CprNumEnd != null)
            {
                strSql.Append("CprNumEnd=N'" + model.CprNumEnd + "',");
            }
            else
            {
                strSql.Append("CprNumEnd= N'' ,");
            }
            strSql.Append("CprType=N'" + model.CprType + "',");
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where [ID]=" + model.ID);
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_CoperationNumConfig ");
            strSql.Append(" where ");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationNumConfig GetModel(int id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" ID,CprPfix,CprNumStart,CprNumEnd ");
            strSql.Append(" from cm_CoperationNumConfig ");
            strSql.Append(" where ID =" + id);
            TG.Model.cm_CoperationNumConfig model = new TG.Model.cm_CoperationNumConfig();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CprPfix"] != null && ds.Tables[0].Rows[0]["CprPfix"].ToString() != "")
                {
                    model.CprPfix = ds.Tables[0].Rows[0]["CprPfix"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CprNumStart"] != null && ds.Tables[0].Rows[0]["CprNumStart"].ToString() != "")
                {
                    model.CprNumStart = ds.Tables[0].Rows[0]["CprNumStart"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CprNumEnd"] != null && ds.Tables[0].Rows[0]["CprNumEnd"].ToString() != "")
                {
                    model.CprNumEnd = ds.Tables[0].Rows[0]["CprNumEnd"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        public TG.Model.cm_CoperationNumConfig GetModel()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" ID,CprPfix,CprNumStart,CprNumEnd ");
            strSql.Append(" from cm_CoperationNumConfig ");
            strSql.Append(" where 1=1");
            TG.Model.cm_CoperationNumConfig model = new TG.Model.cm_CoperationNumConfig();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CprPfix"] != null && ds.Tables[0].Rows[0]["CprPfix"].ToString() != "")
                {
                    model.CprPfix = ds.Tables[0].Rows[0]["CprPfix"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CprNumStart"] != null && ds.Tables[0].Rows[0]["CprNumStart"].ToString() != "")
                {
                    model.CprNumStart = ds.Tables[0].Rows[0]["CprNumStart"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CprNumEnd"] != null && ds.Tables[0].Rows[0]["CprNumEnd"].ToString() != "")
                {
                    model.CprNumEnd = ds.Tables[0].Rows[0]["CprNumEnd"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,CprPfix,CprNumStart,CprNumEnd,CprType ");
            strSql.Append(" FROM cm_CoperationNumConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,CprPfix,CprNumStart,CprNumEnd,CprType ");
            strSql.Append(" FROM cm_CoperationNumConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_CoperationNumConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            /*没有调用此方法，无需修改。Created by Sago*/
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBE() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T. desc");
            }
            strSql.Append(")AS Row, T.*  from cm_CoperationNumConfig T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        public List<string> GetExistsCoperationNo(string coperationType)
        {
            string sql = "select cpr_No from cm_Coperation where isnull(cpr_No,N'') <> N'' and cpr_Type =N'" + coperationType + "'";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<string> resultStringList = new List<string>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    resultStringList.Add(reader["cpr_No"].ToString());
                }
                reader.Close();
            }
            return resultStringList;
        }

        #endregion  Method
    }
}

