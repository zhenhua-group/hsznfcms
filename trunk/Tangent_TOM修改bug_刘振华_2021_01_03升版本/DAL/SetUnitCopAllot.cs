﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references

namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:SetUnitCopAllot
    /// </summary>

    public partial class SetUnitCopAllot
    {
        public SetUnitCopAllot()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.SetUnitCopAllot model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.UnitID != null)
            {
                strSql1.Append("UnitID,");
                strSql2.Append("" + model.UnitID + ",");
            }
            if (model.UnitAllot != null)
            {
                strSql1.Append("UnitAllot,");
                strSql2.Append("" + model.UnitAllot + ",");
            }
            if (model.AllotYear != null)
            {
                strSql1.Append("AllotYear,");
                strSql2.Append("'" + model.AllotYear + "',");
            }
            if (model.Status != null)
            {
                strSql1.Append("Status,");
                strSql2.Append("" + model.Status + ",");
            }
            strSql.Append("insert into cm_UnitCopAllot(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.SetUnitCopAllot model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_UnitCopAllot set ");
            if (model.UnitID != null)
            {
                strSql.Append("UnitID=" + model.UnitID + ",");
            }
            else
            {
                strSql.Append("UnitID= null ,");
            }
            if (model.UnitAllot != null)
            {
                strSql.Append("UnitAllot=" + model.UnitAllot + ",");
            }
            else
            {
                strSql.Append("UnitAllot= null ,");
            }
            if (model.AllotYear != null)
            {
                strSql.Append("AllotYear='" + model.AllotYear + "',");
            }
            else
            {
                strSql.Append("AllotYear= null ,");
            }
            if (model.Status != null)
            {
                strSql.Append("Status=" + model.Status + ",");
            }
            else
            {
                strSql.Append("Status= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where ID=" + model.ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_UnitCopAllot ");
            strSql.Append(" where ID=" + ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_UnitCopAllot ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.SetUnitCopAllot GetModel(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" ID,UnitID,UnitAllot,AllotYear,Status ");
            strSql.Append(" from cm_UnitCopAllot ");
            strSql.Append(" where ID=" + ID + "");
            TG.Model.SetUnitCopAllot model = new TG.Model.SetUnitCopAllot();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UnitID"] != null && ds.Tables[0].Rows[0]["UnitID"].ToString() != "")
                {
                    model.UnitID = int.Parse(ds.Tables[0].Rows[0]["UnitID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UnitAllot"] != null && ds.Tables[0].Rows[0]["UnitAllot"].ToString() != "")
                {
                    model.UnitAllot = decimal.Parse(ds.Tables[0].Rows[0]["UnitAllot"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotYear"] != null && ds.Tables[0].Rows[0]["AllotYear"].ToString() != "")
                {
                    model.AllotYear = ds.Tables[0].Rows[0]["AllotYear"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = int.Parse(ds.Tables[0].Rows[0]["Status"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,UnitID,UnitAllot,AllotYear,Status ");
            strSql.Append(" FROM cm_UnitCopAllot ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,UnitID,UnitAllot,AllotYear,Status ");
            strSql.Append(" FROM cm_UnitCopAllot ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_UnitCopAllot ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_UnitCopAllot T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        */
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetUnitAllotListByPager(string strWhere, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,UnitID,UnitAllot,AllotYear,Status ");
            strSql.Append(" FROM cm_UnitCopAllot ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString(), startIndex, endIndex);
        }
        #endregion  Method
    }
}
