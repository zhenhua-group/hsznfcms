﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeUnitAllotHis
	/// </summary>
	public partial class cm_KaoHeUnitAllotHis
	{
		public cm_KaoHeUnitAllotHis()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeUnitAllotHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeUnitAllotHis(");
			strSql.Append("RenwuID,RenwuName,UnitName,PJYGZ,BMJX,BMJJZB,BMZJJ,YGZBS,XMJJ,XMJJZB,BMNJJ,BeforeYear,AfterYear,AllCount,UnitID)");
			strSql.Append(" values (");
			strSql.Append("@RenwuID,@RenwuName,@UnitName,@PJYGZ,@BMJX,@BMJJZB,@BMZJJ,@YGZBS,@XMJJ,@XMJJZB,@BMNJJ,@BeforeYear,@AfterYear,@AllCount,@UnitID)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@RenwuName", SqlDbType.VarChar,50),
					new SqlParameter("@UnitName", SqlDbType.VarChar,50),
					new SqlParameter("@PJYGZ", SqlDbType.Decimal,9),
					new SqlParameter("@BMJX", SqlDbType.Decimal,9),
					new SqlParameter("@BMJJZB", SqlDbType.Decimal,9),
					new SqlParameter("@BMZJJ", SqlDbType.Decimal,9),
					new SqlParameter("@YGZBS", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJ", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJZB", SqlDbType.Decimal,9),
					new SqlParameter("@BMNJJ", SqlDbType.Decimal,9),
					new SqlParameter("@BeforeYear", SqlDbType.Decimal,9),
					new SqlParameter("@AfterYear", SqlDbType.Decimal,9),
					new SqlParameter("@AllCount", SqlDbType.Decimal,9),
					new SqlParameter("@UnitID", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.RenwuName;
			parameters[2].Value = model.UnitName;
			parameters[3].Value = model.PJYGZ;
			parameters[4].Value = model.BMJX;
			parameters[5].Value = model.BMJJZB;
			parameters[6].Value = model.BMZJJ;
			parameters[7].Value = model.YGZBS;
			parameters[8].Value = model.XMJJ;
			parameters[9].Value = model.XMJJZB;
			parameters[10].Value = model.BMNJJ;
			parameters[11].Value = model.BeforeYear;
			parameters[12].Value = model.AfterYear;
			parameters[13].Value = model.AllCount;
			parameters[14].Value = model.UnitID;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeUnitAllotHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeUnitAllotHis set ");
			strSql.Append("RenwuID=@RenwuID,");
			strSql.Append("RenwuName=@RenwuName,");
			strSql.Append("UnitName=@UnitName,");
			strSql.Append("PJYGZ=@PJYGZ,");
			strSql.Append("BMJX=@BMJX,");
			strSql.Append("BMJJZB=@BMJJZB,");
			strSql.Append("BMZJJ=@BMZJJ,");
			strSql.Append("YGZBS=@YGZBS,");
			strSql.Append("XMJJ=@XMJJ,");
			strSql.Append("XMJJZB=@XMJJZB,");
			strSql.Append("BMNJJ=@BMNJJ,");
			strSql.Append("BeforeYear=@BeforeYear,");
			strSql.Append("AfterYear=@AfterYear,");
			strSql.Append("AllCount=@AllCount,");
			strSql.Append("UnitID=@UnitID");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@RenwuName", SqlDbType.VarChar,50),
					new SqlParameter("@UnitName", SqlDbType.VarChar,50),
					new SqlParameter("@PJYGZ", SqlDbType.Decimal,9),
					new SqlParameter("@BMJX", SqlDbType.Decimal,9),
					new SqlParameter("@BMJJZB", SqlDbType.Decimal,9),
					new SqlParameter("@BMZJJ", SqlDbType.Decimal,9),
					new SqlParameter("@YGZBS", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJ", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJZB", SqlDbType.Decimal,9),
					new SqlParameter("@BMNJJ", SqlDbType.Decimal,9),
					new SqlParameter("@BeforeYear", SqlDbType.Decimal,9),
					new SqlParameter("@AfterYear", SqlDbType.Decimal,9),
					new SqlParameter("@AllCount", SqlDbType.Decimal,9),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.RenwuName;
			parameters[2].Value = model.UnitName;
			parameters[3].Value = model.PJYGZ;
			parameters[4].Value = model.BMJX;
			parameters[5].Value = model.BMJJZB;
			parameters[6].Value = model.BMZJJ;
			parameters[7].Value = model.YGZBS;
			parameters[8].Value = model.XMJJ;
			parameters[9].Value = model.XMJJZB;
			parameters[10].Value = model.BMNJJ;
			parameters[11].Value = model.BeforeYear;
			parameters[12].Value = model.AfterYear;
			parameters[13].Value = model.AllCount;
			parameters[14].Value = model.UnitID;
			parameters[15].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeUnitAllotHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeUnitAllotHis ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeUnitAllotHis GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,RenwuID,RenwuName,UnitName,PJYGZ,BMJX,BMJJZB,BMZJJ,YGZBS,XMJJ,XMJJZB,BMNJJ,BeforeYear,AfterYear,AllCount,UnitID from cm_KaoHeUnitAllotHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeUnitAllotHis model=new TG.Model.cm_KaoHeUnitAllotHis();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeUnitAllotHis DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeUnitAllotHis model=new TG.Model.cm_KaoHeUnitAllotHis();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["RenwuID"]!=null && row["RenwuID"].ToString()!="")
				{
					model.RenwuID=int.Parse(row["RenwuID"].ToString());
				}
				if(row["RenwuName"]!=null)
				{
					model.RenwuName=row["RenwuName"].ToString();
				}
				if(row["UnitName"]!=null)
				{
					model.UnitName=row["UnitName"].ToString();
				}
				if(row["PJYGZ"]!=null && row["PJYGZ"].ToString()!="")
				{
					model.PJYGZ=decimal.Parse(row["PJYGZ"].ToString());
				}
				if(row["BMJX"]!=null && row["BMJX"].ToString()!="")
				{
					model.BMJX=decimal.Parse(row["BMJX"].ToString());
				}
				if(row["BMJJZB"]!=null && row["BMJJZB"].ToString()!="")
				{
					model.BMJJZB=decimal.Parse(row["BMJJZB"].ToString());
				}
				if(row["BMZJJ"]!=null && row["BMZJJ"].ToString()!="")
				{
					model.BMZJJ=decimal.Parse(row["BMZJJ"].ToString());
				}
				if(row["YGZBS"]!=null && row["YGZBS"].ToString()!="")
				{
					model.YGZBS=decimal.Parse(row["YGZBS"].ToString());
				}
				if(row["XMJJ"]!=null && row["XMJJ"].ToString()!="")
				{
					model.XMJJ=decimal.Parse(row["XMJJ"].ToString());
				}
				if(row["XMJJZB"]!=null && row["XMJJZB"].ToString()!="")
				{
					model.XMJJZB=decimal.Parse(row["XMJJZB"].ToString());
				}
				if(row["BMNJJ"]!=null && row["BMNJJ"].ToString()!="")
				{
					model.BMNJJ=decimal.Parse(row["BMNJJ"].ToString());
				}
				if(row["BeforeYear"]!=null && row["BeforeYear"].ToString()!="")
				{
					model.BeforeYear=decimal.Parse(row["BeforeYear"].ToString());
				}
				if(row["AfterYear"]!=null && row["AfterYear"].ToString()!="")
				{
					model.AfterYear=decimal.Parse(row["AfterYear"].ToString());
				}
				if(row["AllCount"]!=null && row["AllCount"].ToString()!="")
				{
					model.AllCount=decimal.Parse(row["AllCount"].ToString());
				}
				if(row["UnitID"]!=null && row["UnitID"].ToString()!="")
				{
					model.UnitID=int.Parse(row["UnitID"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,RenwuID,RenwuName,UnitName,PJYGZ,BMJX,BMJJZB,BMZJJ,YGZBS,XMJJ,XMJJZB,BMNJJ,BeforeYear,AfterYear,AllCount,UnitID ");
			strSql.Append(" FROM cm_KaoHeUnitAllotHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,RenwuID,RenwuName,UnitName,PJYGZ,BMJX,BMJJZB,BMZJJ,YGZBS,XMJJ,XMJJZB,BMNJJ,BeforeYear,AfterYear,AllCount,UnitID ");
			strSql.Append(" FROM cm_KaoHeUnitAllotHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeUnitAllotHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeUnitAllotHis T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeUnitAllotHis";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

