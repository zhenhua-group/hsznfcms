﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.Model.UserControl;
using TG.DBUtility;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class ChooseUserDA
    {
        public List<ChooseUserViewEntity> GetUserList(ChooseUserQueryEntity queryEntity, out int totalCount)
        {
            string whereSql = "where 1=1 ";
            if (!string.IsNullOrEmpty(queryEntity.NameOrLoginName))
            {
                whereSql += string.Format(" and (u.mem_Login=N'{0}' or u.mem_Name like N'%{0}%')", queryEntity.NameOrLoginName);
            }
            if (queryEntity.DepartmentSysNo != 0)
            {
                whereSql += string.Format(" and u.mem_Unit_ID =" + queryEntity.DepartmentSysNo);
            }
            if (!string.IsNullOrEmpty(queryEntity.PhoneNumber))
            {
                whereSql += string.Format(" and u.mem_Telephone like N'{0}'", queryEntity.PhoneNumber);
            }
            string sql = string.Format(@"select Top {1} u.mem_ID as UserSysNo
                                                        ,u.mem_Name as Name
                                                        ,u.mem_Unit_ID as DepartmentSysNo
                                                        ,da.unit_Name as DepartmentName
                                                        ,u.mem_Mobile as PhoneNumber
                                                        from tg_member u join tg_unit da on da.unit_ID = u.mem_Unit_ID 
                                                        {0} and u.mem_ID not in
                                                        (
                                                        select TOP {3} u.mem_ID
                                                          from tg_member u join tg_unit da on da.unit_ID = u.mem_Unit_ID 
                                                          {0}
                                                        )", whereSql, queryEntity.PageSize, queryEntity.PageCurrent - 1, (queryEntity.PageCurrent - 1) * queryEntity.PageSize);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ChooseUserViewEntity> resultList = EntityBuilder<ChooseUserViewEntity>.BuilderEntityList(reader);

            string countSql = string.Format("select COUNT(*) from tg_member u join tg_unit da on da.unit_ID = u.mem_Unit_ID {0} ", whereSql);

            object countObj = DbHelperSQL.GetSingle(countSql);

            totalCount = countObj == null ? 0 : Convert.ToInt32(countObj);

            return resultList;
        }

        /// <summary>
        /// 产值专用
        /// </summary>
        /// <param name="queryEntity"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public List<ChooseProjectValueUserViewEntity> GetUserList(ChooseProjectValueUserEntity queryEntity, out int totalCount)
        {
            string whereSql = "where 1=1 ";
            if (!string.IsNullOrEmpty(queryEntity.NameOrLoginName))
            {
                whereSql += string.Format(" and (u.mem_Login=N'{0}' or u.mem_Name like N'%{0}%')", queryEntity.NameOrLoginName);
            }//部门
            if (queryEntity.DepartmentName != 0)
            {
                whereSql += string.Format(" and u.mem_Unit_ID =" + queryEntity.DepartmentName);
            }
            if (!string.IsNullOrEmpty(queryEntity.SpeName))
            {
                whereSql += string.Format(" and s.spe_ID like N'{0}'", queryEntity.SpeName);
            }
            string sql = string.Format(@"select Top {1} u.mem_ID as UserSysNo
                                                        ,u.mem_Name as Name
                                                        ,u.mem_Principalship_ID as Principalship
                                                        ,u.mem_Unit_ID as DepartmentSysNo
                                                        ,da.unit_Name as DepartmentName
                                                        ,s.spe_Name as SpeName
                                                        from tg_member u join tg_unit da on da.unit_ID = u.mem_Unit_ID 
                                                          join tg_speciality s on u.mem_Speciality_ID=s.spe_ID
                                                        {0} and u.mem_ID not in
                                                        (
                                                        select TOP {3} u.mem_ID
                                                          from tg_member u join tg_unit da on da.unit_ID = u.mem_Unit_ID 
                                                         join tg_speciality s on u.mem_Speciality_ID=s.spe_ID
                                                          {0}
                                                        )", whereSql, queryEntity.PageSize, queryEntity.PageCurrent - 1, (queryEntity.PageCurrent - 1) * queryEntity.PageSize);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ChooseProjectValueUserViewEntity> resultList = EntityBuilder<ChooseProjectValueUserViewEntity>.BuilderEntityList(reader);

            string countSql = string.Format("select COUNT(*) from tg_member u join tg_unit da on da.unit_ID = u.mem_Unit_ID  join tg_speciality s on u.mem_Speciality_ID=s.spe_ID {0} ", whereSql);

            object countObj = DbHelperSQL.GetSingle(countSql);

            totalCount = countObj == null ? 0 : Convert.ToInt32(countObj);

            return resultList;
        }

        /// <summary>
        /// 外聘人员
        /// </summary>
        /// <param name="queryEntity"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public List<ChooseExternalUserViewEntity> GetUserList(ChooseExternalUserEntity queryEntity, out int totalCount)
        {
            string whereSql = "where 1=1 ";
            if (!string.IsNullOrEmpty(queryEntity.NameOrLoginName))
            {
                whereSql += string.Format(" and u.mem_Name like N'%{0}%'", queryEntity.NameOrLoginName);
            }//部门
            if (queryEntity.DepartmentName != 0)
            {
                whereSql += string.Format(" and u.mem_Unit_ID =" + queryEntity.DepartmentName);
            }
            if (!string.IsNullOrEmpty(queryEntity.SpeName))
            {
                whereSql += string.Format(" and s.spe_ID like N'{0}'", queryEntity.SpeName);
            }
            string sql = string.Format(@"select Top {1} u.mem_ID as UserSysNo
                                                        ,u.mem_Name as Name
                                                        ,u.mem_Unit_ID as DepartmentSysNo
                                                        ,da.unit_Name as DepartmentName
                                                        ,s.spe_Name as SpeName
                                                        from cm_externalMember u join tg_unit da on da.unit_ID = u.mem_Unit_ID 
                                                          join tg_speciality s on u.mem_Speciality_ID=s.spe_ID
                                                        {0} and u.mem_ID not in
                                                        (
                                                        select TOP {3} u.mem_ID
                                                          from cm_externalMember u join tg_unit da on da.unit_ID = u.mem_Unit_ID 
                                                         join tg_speciality s on u.mem_Speciality_ID=s.spe_ID
                                                          {0}
                                                        )", whereSql, queryEntity.PageSize, queryEntity.PageCurrent - 1, (queryEntity.PageCurrent - 1) * queryEntity.PageSize);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ChooseExternalUserViewEntity> resultList = EntityBuilder<ChooseExternalUserViewEntity>.BuilderEntityList(reader);

            string countSql = string.Format("select COUNT(*) from cm_externalMember u join tg_unit da on da.unit_ID = u.mem_Unit_ID  join tg_speciality s on u.mem_Speciality_ID=s.spe_ID {0} ", whereSql);

            object countObj = DbHelperSQL.GetSingle(countSql);

            totalCount = countObj == null ? 0 : Convert.ToInt32(countObj);

            return resultList;
        }

        /// <summary>
        /// 新增用户信息
        /// </summary>
        /// <param name="userInfo"></param>
        /// <returns></returns>
        public int AddExternalUserInfo(AddExternalUser userInfo)
        {
            string countSql = "select count(*) from cm_externalMember where mem_Name='" + userInfo.MemName + "' and mem_Speciality_ID=" + userInfo.memSpecialityID + " and mem_Unit_ID=" + userInfo.memUnitID + "";

            object countObj = DbHelperSQL.GetSingle(countSql);

            int totalCount = countObj == null ? 0 : Convert.ToInt32(countObj);

            if (totalCount > 0)
            {
                return 1;
            }
            else
            {
                //新增

                string addSql = "insert into cm_externalMember(mem_Name,mem_Speciality_ID,mem_Unit_ID) values('" + userInfo.MemName + "'," + userInfo.memSpecialityID + "," + userInfo.memUnitID + ")";

                int count = DbHelperSQL.ExecuteSql(addSql);

                if (count > 0)
                {
                    return 2;
                }
                else
                {
                    return 0;
                }

            }
        }
    }
}
