﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;
using TG.Model;
using TG.DBUtility;

namespace TG.DAL
{
    public class ChooseCustomerDA
    {
        public ResultList<CustomerViewEntity> GetCustomerRecordList(ChooseCustomerQueryEntity queryEntity)
        {
            StringBuilder sb = new StringBuilder(" where 1=1 ");
            if (!string.IsNullOrEmpty(queryEntity.CustomerName))
            {
                sb.AppendFormat(" and c.Cst_Name like N'%{0}%'", queryEntity.CustomerName);
            }
            if (queryEntity.CustomerLevel != "-1")
            {
                sb.AppendFormat(" and cx.Type ={0}", queryEntity.CustomerLevel);
            }
            if (!string.IsNullOrEmpty(queryEntity.LinkMan))
            {
                sb.AppendFormat(" and c.Linkman like N'%{0}%'", queryEntity.LinkMan);
            }
            if (!string.IsNullOrEmpty(queryEntity.ProvinceName))
            {
                sb.AppendFormat(" and cx.Province like N'%{0}%'", queryEntity.ProvinceName);
            }
            if (!string.IsNullOrEmpty(queryEntity.CityName))
            {
                sb.AppendFormat(" and cx.City like N'%{0}%'", queryEntity.CityName);
            }
            if (queryEntity.PreviewPower=="0")
            {
                sb.AppendFormat(" and c.UpdateBy={0}", queryEntity.UserSysNo);
            }
            else if (queryEntity.PreviewPower=="2")
            {
                sb.AppendFormat(" AND (c.UpdateBy IN (Select mem_ID From tg_member Where mem_Unit_ID IN (Select mem_Unit_ID From tg_member WHere mem_ID={0})))", queryEntity.UserSysNo);
            }
            string sql = string.Format(@"SELECT TOP {0} 
                                                        c.Cst_Id as SysNo
                                                        ,c.Cst_No as CustomerNo
                                                        ,c.Cst_Name as CustomerName
                                                        ,c.Cst_Brief as CustomerShortName
                                                        ,c.Cpy_Address as Address
                                                        ,c.Code as ZipCode
                                                        ,c.Linkman as LinkMan
                                                        ,c.Cpy_Phone as Phone
                                                        ,c.Cpy_Fax as Fax
                                                        FROM cm_CustomerInfo c
                                                        JOIN cm_CustomerExtendInfo cx on cx.Cst_Id = c.Cst_Id
                                                        {1}  and c.Cst_Id not in
                                                        (
                                                            SELECT TOP {2} c.Cst_Id 
                                                            FROM cm_CustomerInfo c
                                                            JOIN cm_CustomerExtendInfo cx on cx.Cst_Id = c.Cst_Id
                                                            {1} 
                                                            order by c.Cst_Id DESC
                                                        )
                                                        ORDER BY c.Cst_Id desc", queryEntity.PageSize, sb.ToString(), (queryEntity.PageCurrent - 1) * queryEntity.PageSize);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<CustomerViewEntity> customerViewEntityList = EntityBuilder<CustomerViewEntity>.BuilderEntityList(reader);

            string countSql = string.Format(@"select count(*) FROM cm_CustomerInfo c
	                                                            JOIN cm_CustomerExtendInfo cx on cx.Cst_Id = c.Cst_Id {0}", sb.ToString());

            object countObj = DbHelperSQL.GetSingle(countSql);

            ResultList<CustomerViewEntity> resultList = new ResultList<CustomerViewEntity>
            {
                Body = customerViewEntityList,
                TotalCount = countObj == null ? 0 : Convert.ToInt32(countObj)
            };

            return resultList;
        }
    }
}
