﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeProjHis
	/// </summary>
	public partial class cm_KaoHeProjHis
	{
		public cm_KaoHeProjHis()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeProjHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeProjHis(");
			strSql.Append("RenwuID,RenwuName,IsFired,UnitName,UnitID,SpeName,UserName,MemID,ProjAllot,UnitOrder,HPOrder,ProjAllot2,IsOther)");
			strSql.Append(" values (");
			strSql.Append("@RenwuID,@RenwuName,@IsFired,@UnitName,@UnitID,@SpeName,@UserName,@MemID,@ProjAllot,@UnitOrder,@HPOrder,@ProjAllot2,@IsOther)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@RenwuName", SqlDbType.VarChar,50),
					new SqlParameter("@IsFired", SqlDbType.VarChar,30),
					new SqlParameter("@UnitName", SqlDbType.VarChar,50),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@SpeName", SqlDbType.VarChar,50),
					new SqlParameter("@UserName", SqlDbType.VarChar,50),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@ProjAllot", SqlDbType.Decimal,9),
					new SqlParameter("@UnitOrder", SqlDbType.Int,4),
					new SqlParameter("@HPOrder", SqlDbType.Int,4),
					new SqlParameter("@ProjAllot2", SqlDbType.Decimal,9),
					new SqlParameter("@IsOther", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.RenwuName;
			parameters[2].Value = model.IsFired;
			parameters[3].Value = model.UnitName;
			parameters[4].Value = model.UnitID;
			parameters[5].Value = model.SpeName;
			parameters[6].Value = model.UserName;
			parameters[7].Value = model.MemID;
			parameters[8].Value = model.ProjAllot;
			parameters[9].Value = model.UnitOrder;
			parameters[10].Value = model.HPOrder;
			parameters[11].Value = model.ProjAllot2;
			parameters[12].Value = model.IsOther;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeProjHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeProjHis set ");
			strSql.Append("RenwuID=@RenwuID,");
			strSql.Append("RenwuName=@RenwuName,");
			strSql.Append("IsFired=@IsFired,");
			strSql.Append("UnitName=@UnitName,");
			strSql.Append("UnitID=@UnitID,");
			strSql.Append("SpeName=@SpeName,");
			strSql.Append("UserName=@UserName,");
			strSql.Append("MemID=@MemID,");
			strSql.Append("ProjAllot=@ProjAllot,");
			strSql.Append("UnitOrder=@UnitOrder,");
			strSql.Append("HPOrder=@HPOrder,");
			strSql.Append("ProjAllot2=@ProjAllot2,");
			strSql.Append("IsOther=@IsOther");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@RenwuName", SqlDbType.VarChar,50),
					new SqlParameter("@IsFired", SqlDbType.VarChar,30),
					new SqlParameter("@UnitName", SqlDbType.VarChar,50),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@SpeName", SqlDbType.VarChar,50),
					new SqlParameter("@UserName", SqlDbType.VarChar,50),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@ProjAllot", SqlDbType.Decimal,9),
					new SqlParameter("@UnitOrder", SqlDbType.Int,4),
					new SqlParameter("@HPOrder", SqlDbType.Int,4),
					new SqlParameter("@ProjAllot2", SqlDbType.Decimal,9),
					new SqlParameter("@IsOther", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.RenwuName;
			parameters[2].Value = model.IsFired;
			parameters[3].Value = model.UnitName;
			parameters[4].Value = model.UnitID;
			parameters[5].Value = model.SpeName;
			parameters[6].Value = model.UserName;
			parameters[7].Value = model.MemID;
			parameters[8].Value = model.ProjAllot;
			parameters[9].Value = model.UnitOrder;
			parameters[10].Value = model.HPOrder;
			parameters[11].Value = model.ProjAllot2;
			parameters[12].Value = model.IsOther;
			parameters[13].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeProjHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeProjHis ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeProjHis GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,RenwuID,RenwuName,IsFired,UnitName,UnitID,SpeName,UserName,MemID,ProjAllot,UnitOrder,HPOrder,ProjAllot2,IsOther from cm_KaoHeProjHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeProjHis model=new TG.Model.cm_KaoHeProjHis();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeProjHis DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeProjHis model=new TG.Model.cm_KaoHeProjHis();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["RenwuID"]!=null && row["RenwuID"].ToString()!="")
				{
					model.RenwuID=int.Parse(row["RenwuID"].ToString());
				}
				if(row["RenwuName"]!=null)
				{
					model.RenwuName=row["RenwuName"].ToString();
				}
				if(row["IsFired"]!=null)
				{
					model.IsFired=row["IsFired"].ToString();
				}
				if(row["UnitName"]!=null)
				{
					model.UnitName=row["UnitName"].ToString();
				}
				if(row["UnitID"]!=null && row["UnitID"].ToString()!="")
				{
					model.UnitID=int.Parse(row["UnitID"].ToString());
				}
				if(row["SpeName"]!=null)
				{
					model.SpeName=row["SpeName"].ToString();
				}
				if(row["UserName"]!=null)
				{
					model.UserName=row["UserName"].ToString();
				}
				if(row["MemID"]!=null && row["MemID"].ToString()!="")
				{
					model.MemID=int.Parse(row["MemID"].ToString());
				}
				if(row["ProjAllot"]!=null && row["ProjAllot"].ToString()!="")
				{
					model.ProjAllot=decimal.Parse(row["ProjAllot"].ToString());
				}
				if(row["UnitOrder"]!=null && row["UnitOrder"].ToString()!="")
				{
					model.UnitOrder=int.Parse(row["UnitOrder"].ToString());
				}
				if(row["HPOrder"]!=null && row["HPOrder"].ToString()!="")
				{
					model.HPOrder=int.Parse(row["HPOrder"].ToString());
				}
				if(row["ProjAllot2"]!=null && row["ProjAllot2"].ToString()!="")
				{
					model.ProjAllot2=decimal.Parse(row["ProjAllot2"].ToString());
				}
				if(row["IsOther"]!=null && row["IsOther"].ToString()!="")
				{
					model.IsOther=int.Parse(row["IsOther"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,RenwuID,RenwuName,IsFired,UnitName,UnitID,SpeName,UserName,MemID,ProjAllot,UnitOrder,HPOrder,ProjAllot2,IsOther ");
			strSql.Append(" FROM cm_KaoHeProjHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,RenwuID,RenwuName,IsFired,UnitName,UnitID,SpeName,UserName,MemID,ProjAllot,UnitOrder,HPOrder,ProjAllot2,IsOther ");
			strSql.Append(" FROM cm_KaoHeProjHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeProjHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeProjHis T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeProjHis";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

