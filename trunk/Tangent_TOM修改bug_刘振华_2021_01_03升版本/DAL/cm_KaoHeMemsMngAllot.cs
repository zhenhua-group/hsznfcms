﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeMemsMngAllot
	/// </summary>
	public partial class cm_KaoHeMemsMngAllot
	{
		public cm_KaoHeMemsMngAllot()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeMemsMngAllot model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeMemsMngAllot(");
			strSql.Append("RenwuID,MemID,MemName,UnitID,UnitName,jbgz,gzsj,pjgz,zphu,orderid,bmjlpj,orderid2,jqxs,bmnjs,xmjjjs,xmjjmng,xmjjtz,yxyg,hj,beizhu,beforyear,chazhi,beforyear2,chazhi2,insertUserID,weights,Stat,hjyf)");
			strSql.Append(" values (");
			strSql.Append("@RenwuID,@MemID,@MemName,@UnitID,@UnitName,@jbgz,@gzsj,@pjgz,@zphu,@orderid,@bmjlpj,@orderid2,@jqxs,@bmnjs,@xmjjjs,@xmjjmng,@xmjjtz,@yxyg,@hj,@beizhu,@beforyear,@chazhi,@beforyear2,@chazhi2,@insertUserID,@weights,@Stat,@hjyf)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.NVarChar,50),
					new SqlParameter("@jbgz", SqlDbType.Decimal,9),
					new SqlParameter("@gzsj", SqlDbType.Decimal,9),
					new SqlParameter("@pjgz", SqlDbType.Decimal,9),
					new SqlParameter("@zphu", SqlDbType.Decimal,9),
					new SqlParameter("@orderid", SqlDbType.Decimal,9),
					new SqlParameter("@bmjlpj", SqlDbType.Decimal,9),
					new SqlParameter("@orderid2", SqlDbType.Decimal,9),
					new SqlParameter("@jqxs", SqlDbType.Decimal,9),
					new SqlParameter("@bmnjs", SqlDbType.Decimal,9),
					new SqlParameter("@xmjjjs", SqlDbType.Decimal,9),
					new SqlParameter("@xmjjmng", SqlDbType.Decimal,9),
					new SqlParameter("@xmjjtz", SqlDbType.Decimal,9),
					new SqlParameter("@yxyg", SqlDbType.Decimal,9),
					new SqlParameter("@hj", SqlDbType.Decimal,9),
					new SqlParameter("@beizhu", SqlDbType.Decimal,9),
					new SqlParameter("@beforyear", SqlDbType.Decimal,9),
					new SqlParameter("@chazhi", SqlDbType.Decimal,9),
					new SqlParameter("@beforyear2", SqlDbType.Decimal,9),
					new SqlParameter("@chazhi2", SqlDbType.Decimal,9),
					new SqlParameter("@insertUserID", SqlDbType.Int,4),
					new SqlParameter("@weights", SqlDbType.Decimal,9),
					new SqlParameter("@Stat", SqlDbType.Int,4),
					new SqlParameter("@hjyf", SqlDbType.Decimal,9)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.MemID;
			parameters[2].Value = model.MemName;
			parameters[3].Value = model.UnitID;
			parameters[4].Value = model.UnitName;
			parameters[5].Value = model.jbgz;
			parameters[6].Value = model.gzsj;
			parameters[7].Value = model.pjgz;
			parameters[8].Value = model.zphu;
			parameters[9].Value = model.orderid;
			parameters[10].Value = model.bmjlpj;
			parameters[11].Value = model.orderid2;
			parameters[12].Value = model.jqxs;
			parameters[13].Value = model.bmnjs;
			parameters[14].Value = model.xmjjjs;
			parameters[15].Value = model.xmjjmng;
			parameters[16].Value = model.xmjjtz;
			parameters[17].Value = model.yxyg;
			parameters[18].Value = model.hj;
			parameters[19].Value = model.beizhu;
			parameters[20].Value = model.beforyear;
			parameters[21].Value = model.chazhi;
			parameters[22].Value = model.beforyear2;
			parameters[23].Value = model.chazhi2;
			parameters[24].Value = model.insertUserID;
			parameters[25].Value = model.weights;
			parameters[26].Value = model.Stat;
			parameters[27].Value = model.hjyf;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeMemsMngAllot model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeMemsMngAllot set ");
			strSql.Append("RenwuID=@RenwuID,");
			strSql.Append("MemID=@MemID,");
			strSql.Append("MemName=@MemName,");
			strSql.Append("UnitID=@UnitID,");
			strSql.Append("UnitName=@UnitName,");
			strSql.Append("jbgz=@jbgz,");
			strSql.Append("gzsj=@gzsj,");
			strSql.Append("pjgz=@pjgz,");
			strSql.Append("zphu=@zphu,");
			strSql.Append("orderid=@orderid,");
			strSql.Append("bmjlpj=@bmjlpj,");
			strSql.Append("orderid2=@orderid2,");
			strSql.Append("jqxs=@jqxs,");
			strSql.Append("bmnjs=@bmnjs,");
			strSql.Append("xmjjjs=@xmjjjs,");
			strSql.Append("xmjjmng=@xmjjmng,");
			strSql.Append("xmjjtz=@xmjjtz,");
			strSql.Append("yxyg=@yxyg,");
			strSql.Append("hj=@hj,");
			strSql.Append("beizhu=@beizhu,");
			strSql.Append("beforyear=@beforyear,");
			strSql.Append("chazhi=@chazhi,");
			strSql.Append("beforyear2=@beforyear2,");
			strSql.Append("chazhi2=@chazhi2,");
			strSql.Append("insertUserID=@insertUserID,");
			strSql.Append("weights=@weights,");
			strSql.Append("Stat=@Stat,");
			strSql.Append("hjyf=@hjyf");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.NVarChar,50),
					new SqlParameter("@jbgz", SqlDbType.Decimal,9),
					new SqlParameter("@gzsj", SqlDbType.Decimal,9),
					new SqlParameter("@pjgz", SqlDbType.Decimal,9),
					new SqlParameter("@zphu", SqlDbType.Decimal,9),
					new SqlParameter("@orderid", SqlDbType.Decimal,9),
					new SqlParameter("@bmjlpj", SqlDbType.Decimal,9),
					new SqlParameter("@orderid2", SqlDbType.Decimal,9),
					new SqlParameter("@jqxs", SqlDbType.Decimal,9),
					new SqlParameter("@bmnjs", SqlDbType.Decimal,9),
					new SqlParameter("@xmjjjs", SqlDbType.Decimal,9),
					new SqlParameter("@xmjjmng", SqlDbType.Decimal,9),
					new SqlParameter("@xmjjtz", SqlDbType.Decimal,9),
					new SqlParameter("@yxyg", SqlDbType.Decimal,9),
					new SqlParameter("@hj", SqlDbType.Decimal,9),
					new SqlParameter("@beizhu", SqlDbType.Decimal,9),
					new SqlParameter("@beforyear", SqlDbType.Decimal,9),
					new SqlParameter("@chazhi", SqlDbType.Decimal,9),
					new SqlParameter("@beforyear2", SqlDbType.Decimal,9),
					new SqlParameter("@chazhi2", SqlDbType.Decimal,9),
					new SqlParameter("@insertUserID", SqlDbType.Int,4),
					new SqlParameter("@weights", SqlDbType.Decimal,9),
					new SqlParameter("@Stat", SqlDbType.Int,4),
					new SqlParameter("@hjyf", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.MemID;
			parameters[2].Value = model.MemName;
			parameters[3].Value = model.UnitID;
			parameters[4].Value = model.UnitName;
			parameters[5].Value = model.jbgz;
			parameters[6].Value = model.gzsj;
			parameters[7].Value = model.pjgz;
			parameters[8].Value = model.zphu;
			parameters[9].Value = model.orderid;
			parameters[10].Value = model.bmjlpj;
			parameters[11].Value = model.orderid2;
			parameters[12].Value = model.jqxs;
			parameters[13].Value = model.bmnjs;
			parameters[14].Value = model.xmjjjs;
			parameters[15].Value = model.xmjjmng;
			parameters[16].Value = model.xmjjtz;
			parameters[17].Value = model.yxyg;
			parameters[18].Value = model.hj;
			parameters[19].Value = model.beizhu;
			parameters[20].Value = model.beforyear;
			parameters[21].Value = model.chazhi;
			parameters[22].Value = model.beforyear2;
			parameters[23].Value = model.chazhi2;
			parameters[24].Value = model.insertUserID;
			parameters[25].Value = model.weights;
			parameters[26].Value = model.Stat;
			parameters[27].Value = model.hjyf;
			parameters[28].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsMngAllot ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsMngAllot ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeMemsMngAllot GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,RenwuID,MemID,MemName,UnitID,UnitName,jbgz,gzsj,pjgz,zphu,orderid,bmjlpj,orderid2,jqxs,bmnjs,xmjjjs,xmjjmng,xmjjtz,yxyg,hj,beizhu,beforyear,chazhi,beforyear2,chazhi2,insertUserID,weights,Stat,hjyf from cm_KaoHeMemsMngAllot ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeMemsMngAllot model=new TG.Model.cm_KaoHeMemsMngAllot();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeMemsMngAllot DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeMemsMngAllot model=new TG.Model.cm_KaoHeMemsMngAllot();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["RenwuID"]!=null && row["RenwuID"].ToString()!="")
				{
					model.RenwuID=int.Parse(row["RenwuID"].ToString());
				}
				if(row["MemID"]!=null && row["MemID"].ToString()!="")
				{
					model.MemID=int.Parse(row["MemID"].ToString());
				}
				if(row["MemName"]!=null)
				{
					model.MemName=row["MemName"].ToString();
				}
				if(row["UnitID"]!=null && row["UnitID"].ToString()!="")
				{
					model.UnitID=int.Parse(row["UnitID"].ToString());
				}
				if(row["UnitName"]!=null)
				{
					model.UnitName=row["UnitName"].ToString();
				}
				if(row["jbgz"]!=null && row["jbgz"].ToString()!="")
				{
					model.jbgz=decimal.Parse(row["jbgz"].ToString());
				}
				if(row["gzsj"]!=null && row["gzsj"].ToString()!="")
				{
					model.gzsj=decimal.Parse(row["gzsj"].ToString());
				}
				if(row["pjgz"]!=null && row["pjgz"].ToString()!="")
				{
					model.pjgz=decimal.Parse(row["pjgz"].ToString());
				}
				if(row["zphu"]!=null && row["zphu"].ToString()!="")
				{
					model.zphu=decimal.Parse(row["zphu"].ToString());
				}
				if(row["orderid"]!=null && row["orderid"].ToString()!="")
				{
					model.orderid=decimal.Parse(row["orderid"].ToString());
				}
				if(row["bmjlpj"]!=null && row["bmjlpj"].ToString()!="")
				{
					model.bmjlpj=decimal.Parse(row["bmjlpj"].ToString());
				}
				if(row["orderid2"]!=null && row["orderid2"].ToString()!="")
				{
					model.orderid2=decimal.Parse(row["orderid2"].ToString());
				}
				if(row["jqxs"]!=null && row["jqxs"].ToString()!="")
				{
					model.jqxs=decimal.Parse(row["jqxs"].ToString());
				}
				if(row["bmnjs"]!=null && row["bmnjs"].ToString()!="")
				{
					model.bmnjs=decimal.Parse(row["bmnjs"].ToString());
				}
				if(row["xmjjjs"]!=null && row["xmjjjs"].ToString()!="")
				{
					model.xmjjjs=decimal.Parse(row["xmjjjs"].ToString());
				}
				if(row["xmjjmng"]!=null && row["xmjjmng"].ToString()!="")
				{
					model.xmjjmng=decimal.Parse(row["xmjjmng"].ToString());
				}
				if(row["xmjjtz"]!=null && row["xmjjtz"].ToString()!="")
				{
					model.xmjjtz=decimal.Parse(row["xmjjtz"].ToString());
				}
				if(row["yxyg"]!=null && row["yxyg"].ToString()!="")
				{
					model.yxyg=decimal.Parse(row["yxyg"].ToString());
				}
				if(row["hj"]!=null && row["hj"].ToString()!="")
				{
					model.hj=decimal.Parse(row["hj"].ToString());
				}
				if(row["beizhu"]!=null && row["beizhu"].ToString()!="")
				{
					model.beizhu=decimal.Parse(row["beizhu"].ToString());
				}
				if(row["beforyear"]!=null && row["beforyear"].ToString()!="")
				{
					model.beforyear=decimal.Parse(row["beforyear"].ToString());
				}
				if(row["chazhi"]!=null && row["chazhi"].ToString()!="")
				{
					model.chazhi=decimal.Parse(row["chazhi"].ToString());
				}
				if(row["beforyear2"]!=null && row["beforyear2"].ToString()!="")
				{
					model.beforyear2=decimal.Parse(row["beforyear2"].ToString());
				}
				if(row["chazhi2"]!=null && row["chazhi2"].ToString()!="")
				{
					model.chazhi2=decimal.Parse(row["chazhi2"].ToString());
				}
				if(row["insertUserID"]!=null && row["insertUserID"].ToString()!="")
				{
					model.insertUserID=int.Parse(row["insertUserID"].ToString());
				}
				if(row["weights"]!=null && row["weights"].ToString()!="")
				{
					model.weights=decimal.Parse(row["weights"].ToString());
				}
				if(row["Stat"]!=null && row["Stat"].ToString()!="")
				{
					model.Stat=int.Parse(row["Stat"].ToString());
				}
				if(row["hjyf"]!=null && row["hjyf"].ToString()!="")
				{
					model.hjyf=decimal.Parse(row["hjyf"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,RenwuID,MemID,MemName,UnitID,UnitName,jbgz,gzsj,pjgz,zphu,orderid,bmjlpj,orderid2,jqxs,bmnjs,xmjjjs,xmjjmng,xmjjtz,yxyg,hj,beizhu,beforyear,chazhi,beforyear2,chazhi2,insertUserID,weights,Stat,hjyf ");
			strSql.Append(" FROM cm_KaoHeMemsMngAllot ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,RenwuID,MemID,MemName,UnitID,UnitName,jbgz,gzsj,pjgz,zphu,orderid,bmjlpj,orderid2,jqxs,bmnjs,xmjjjs,xmjjmng,xmjjtz,yxyg,hj,beizhu,beforyear,chazhi,beforyear2,chazhi2,insertUserID,weights,Stat,hjyf ");
			strSql.Append(" FROM cm_KaoHeMemsMngAllot ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeMemsMngAllot ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeMemsMngAllot T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeMemsMngAllot";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

