﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeAllMemsHis
	/// </summary>
	public partial class cm_KaoHeAllMemsHis
	{
		public cm_KaoHeAllMemsHis()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeAllMemsHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeAllMemsHis(");
			strSql.Append("RenwuID,UnitName,UserName,IsFired,BMJLTZ,GSTZ,YFJJ,XMJJJS,BMJJJS,XMJJ,BMNJJ,XMGLJ,YXYG,TCGX,BIM,GHBZ,HJ,ZCFY)");
			strSql.Append(" values (");
            strSql.Append("@RenwuID,@UnitName,@UserName,@IsFired,@BMJLTZ,@GSTZ,@YFJJ,@XMJJJS,@BMJJJS,@XMJJ,@BMNJJ,@XMGLJ,@YXYG,@TCGX,@BIM,@GHBZ,@HJ,@ZCFY)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.VarChar,50),
					new SqlParameter("@UserName", SqlDbType.VarChar,50),
					new SqlParameter("@IsFired", SqlDbType.VarChar,30),
					new SqlParameter("@BMJLTZ", SqlDbType.Decimal,9),
					new SqlParameter("@GSTZ", SqlDbType.Decimal,9),
					new SqlParameter("@YFJJ", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJJS", SqlDbType.Decimal,9),
					new SqlParameter("@BMJJJS", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJ", SqlDbType.Decimal,9),
					new SqlParameter("@BMNJJ", SqlDbType.Decimal,9),
					new SqlParameter("@XMGLJ", SqlDbType.Decimal,9),
					new SqlParameter("@YXYG", SqlDbType.Decimal,9),
					new SqlParameter("@TCGX", SqlDbType.Decimal,9),
					new SqlParameter("@BIM", SqlDbType.Decimal,9),
					new SqlParameter("@GHBZ", SqlDbType.Decimal,9),
					new SqlParameter("@HJ", SqlDbType.Decimal,9),
                   new SqlParameter("@ZCFY", SqlDbType.Decimal,9)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.UnitName;
			parameters[2].Value = model.UserName;
			parameters[3].Value = model.IsFired;
			parameters[4].Value = model.BMJLTZ;
			parameters[5].Value = model.GSTZ;
			parameters[6].Value = model.YFJJ;
			parameters[7].Value = model.XMJJJS;
			parameters[8].Value = model.BMJJJS;
			parameters[9].Value = model.XMJJ;
			parameters[10].Value = model.BMNJJ;
			parameters[11].Value = model.XMGLJ;
			parameters[12].Value = model.YXYG;
			parameters[13].Value = model.TCGX;
			parameters[14].Value = model.BIM;
			parameters[15].Value = model.GHBZ;
			parameters[16].Value = model.HJ;
            parameters[17].Value = model.ZCFY;
			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeAllMemsHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeAllMemsHis set ");
			strSql.Append("RenwuID=@RenwuID,");
			strSql.Append("UnitName=@UnitName,");
			strSql.Append("UserName=@UserName,");
			strSql.Append("IsFired=@IsFired,");
			strSql.Append("BMJLTZ=@BMJLTZ,");
			strSql.Append("GSTZ=@GSTZ,");
			strSql.Append("YFJJ=@YFJJ,");
			strSql.Append("XMJJJS=@XMJJJS,");
			strSql.Append("BMJJJS=@BMJJJS,");
			strSql.Append("XMJJ=@XMJJ,");
			strSql.Append("BMNJJ=@BMNJJ,");
			strSql.Append("XMGLJ=@XMGLJ,");
			strSql.Append("YXYG=@YXYG,");
			strSql.Append("TCGX=@TCGX,");
			strSql.Append("BIM=@BIM,");
			strSql.Append("GHBZ=@GHBZ,");
			strSql.Append("HJ=@HJ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.VarChar,50),
					new SqlParameter("@UserName", SqlDbType.VarChar,50),
					new SqlParameter("@IsFired", SqlDbType.VarChar,30),
					new SqlParameter("@BMJLTZ", SqlDbType.Decimal,9),
					new SqlParameter("@GSTZ", SqlDbType.Decimal,9),
					new SqlParameter("@YFJJ", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJJS", SqlDbType.Decimal,9),
					new SqlParameter("@BMJJJS", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJ", SqlDbType.Decimal,9),
					new SqlParameter("@BMNJJ", SqlDbType.Decimal,9),
					new SqlParameter("@XMGLJ", SqlDbType.Decimal,9),
					new SqlParameter("@YXYG", SqlDbType.Decimal,9),
					new SqlParameter("@TCGX", SqlDbType.Decimal,9),
					new SqlParameter("@BIM", SqlDbType.Decimal,9),
					new SqlParameter("@GHBZ", SqlDbType.Decimal,9),
					new SqlParameter("@HJ", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.UnitName;
			parameters[2].Value = model.UserName;
			parameters[3].Value = model.IsFired;
			parameters[4].Value = model.BMJLTZ;
			parameters[5].Value = model.GSTZ;
			parameters[6].Value = model.YFJJ;
			parameters[7].Value = model.XMJJJS;
			parameters[8].Value = model.BMJJJS;
			parameters[9].Value = model.XMJJ;
			parameters[10].Value = model.BMNJJ;
			parameters[11].Value = model.XMGLJ;
			parameters[12].Value = model.YXYG;
			parameters[13].Value = model.TCGX;
			parameters[14].Value = model.BIM;
			parameters[15].Value = model.GHBZ;
			parameters[16].Value = model.HJ;
			parameters[17].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeAllMemsHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeAllMemsHis ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeAllMemsHis GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,RenwuID,UnitName,UserName,IsFired,BMJLTZ,GSTZ,YFJJ,XMJJJS,BMJJJS,XMJJ,BMNJJ,XMGLJ,YXYG,TCGX,BIM,GHBZ,HJ from cm_KaoHeAllMemsHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeAllMemsHis model=new TG.Model.cm_KaoHeAllMemsHis();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeAllMemsHis DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeAllMemsHis model=new TG.Model.cm_KaoHeAllMemsHis();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["RenwuID"]!=null && row["RenwuID"].ToString()!="")
				{
					model.RenwuID=int.Parse(row["RenwuID"].ToString());
				}
				if(row["UnitName"]!=null)
				{
					model.UnitName=row["UnitName"].ToString();
				}
				if(row["UserName"]!=null)
				{
					model.UserName=row["UserName"].ToString();
				}
				if(row["IsFired"]!=null)
				{
					model.IsFired=row["IsFired"].ToString();
				}
				if(row["BMJLTZ"]!=null && row["BMJLTZ"].ToString()!="")
				{
					model.BMJLTZ=decimal.Parse(row["BMJLTZ"].ToString());
				}
				if(row["GSTZ"]!=null && row["GSTZ"].ToString()!="")
				{
					model.GSTZ=decimal.Parse(row["GSTZ"].ToString());
				}
				if(row["YFJJ"]!=null && row["YFJJ"].ToString()!="")
				{
					model.YFJJ=decimal.Parse(row["YFJJ"].ToString());
				}
				if(row["XMJJJS"]!=null && row["XMJJJS"].ToString()!="")
				{
					model.XMJJJS=decimal.Parse(row["XMJJJS"].ToString());
				}
				if(row["BMJJJS"]!=null && row["BMJJJS"].ToString()!="")
				{
					model.BMJJJS=decimal.Parse(row["BMJJJS"].ToString());
				}
				if(row["XMJJ"]!=null && row["XMJJ"].ToString()!="")
				{
					model.XMJJ=decimal.Parse(row["XMJJ"].ToString());
				}
				if(row["BMNJJ"]!=null && row["BMNJJ"].ToString()!="")
				{
					model.BMNJJ=decimal.Parse(row["BMNJJ"].ToString());
				}
				if(row["XMGLJ"]!=null && row["XMGLJ"].ToString()!="")
				{
					model.XMGLJ=decimal.Parse(row["XMGLJ"].ToString());
				}
				if(row["YXYG"]!=null && row["YXYG"].ToString()!="")
				{
					model.YXYG=decimal.Parse(row["YXYG"].ToString());
				}
				if(row["TCGX"]!=null && row["TCGX"].ToString()!="")
				{
					model.TCGX=decimal.Parse(row["TCGX"].ToString());
				}
				if(row["BIM"]!=null && row["BIM"].ToString()!="")
				{
					model.BIM=decimal.Parse(row["BIM"].ToString());
				}
				if(row["GHBZ"]!=null && row["GHBZ"].ToString()!="")
				{
					model.GHBZ=decimal.Parse(row["GHBZ"].ToString());
				}
				if(row["HJ"]!=null && row["HJ"].ToString()!="")
				{
					model.HJ=decimal.Parse(row["HJ"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select ID,RenwuID,UnitName,UserName,IsFired,BMJLTZ,GSTZ,YFJJ,XMJJJS,BMJJJS,XMJJ,BMNJJ,XMGLJ,YXYG,TCGX,BIM,GHBZ,HJ,ZCFY");
			strSql.Append(" FROM cm_KaoHeAllMemsHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,RenwuID,UnitName,UserName,IsFired,BMJLTZ,GSTZ,YFJJ,XMJJJS,BMJJJS,XMJJ,BMNJJ,XMGLJ,YXYG,TCGX,BIM,GHBZ,HJ ");
			strSql.Append(" FROM cm_KaoHeAllMemsHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeAllMemsHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeAllMemsHis T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeAllMemsHis";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

