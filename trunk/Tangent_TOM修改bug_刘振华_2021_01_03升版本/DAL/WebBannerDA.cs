﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using TG.DBUtility;
using System.Data.SqlClient;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class WebBannerDA
    {
        public int InsertWebBanner(WebBannerViewEntity dataEntity)
        {
            string sql = string.Format("insert into cm_WebBanner(Title,[Content],InUser,InDate) values(N'{0}',N'{1}',{2},'{3}');select @@IDENTITY", dataEntity.Title, dataEntity.Content, dataEntity.InUser, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            object objResult = DbHelperSQL.GetSingle(sql);
            return objResult == null ? 0 : Convert.ToInt32(objResult);
        }

        public int InsertWorkLog(WorkLogViewEntity dataEntity)
        {
            string sql = string.Format("insert into cm_WorkLog(Title,[Content],InUser,InDate) values(N'{0}',N'{1}',{2},'{3}');select @@IDENTITY", dataEntity.Title, dataEntity.Content, dataEntity.InUser, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            object objResult = DbHelperSQL.GetSingle(sql);
            return objResult == null ? 0 : Convert.ToInt32(objResult);
        }

        public int UpdateWebBanner(WebBannerViewEntity dataEntity)
        {
            string sql = string.Format("update cm_WebBanner set Title = N'{0}',[Content] =N'{1}',InUser={2},InDate='{3}' where SysNo={4}", dataEntity.Title, dataEntity.Content, dataEntity.InUser, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), dataEntity.SysNo);
            return DbHelperSQL.ExecuteSql(sql);
        }

        public int UpdateWorkLog(WorkLogViewEntity dataEntity)
        {
            string sql = string.Format("update cm_WorkLog set Title = N'{0}',[Content] =N'{1}',InUser={2},InDate='{3}' where SysNo={4}", dataEntity.Title, dataEntity.Content, dataEntity.InUser, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), dataEntity.SysNo);
            return DbHelperSQL.ExecuteSql(sql);
        }

        public WorkLogViewEntity GetWorkLog(int sysNo)
        {
            string sql = "select * from cm_WorkLog where SysNo=" + sysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            WorkLogViewEntity resultEntity = EntityBuilder<WorkLogViewEntity>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }
        public WebBannerViewEntity GetWebBanner(int sysNo)
        {
            string sql = "select * from cm_WebBanner where SysNo=" + sysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            WebBannerViewEntity resultEntity = EntityBuilder<WebBannerViewEntity>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }

        public List<WebBannerItemViewEntity> GetWebBannerItemList(int webBannerSysNo)
        {
            string sql = "select wi.*,tm.mem_Name as UserName from cm_WenBannerItem wi join tg_member tm on tm.mem_ID = wi.ToUser where WebBannerSysNo=" + webBannerSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<WebBannerItemViewEntity> webBannerList = EntityBuilder<WebBannerItemViewEntity>.BuilderEntityList(reader);

            return webBannerList;
        }

        public int InsertWebBannerItem(WebBannerItemViewEntity webBannerItemViewEntity)
        {
            string sql = string.Format("insert into cm_WenBannerItem(WebBannerSysNo,ToUser)values({0},{1});select @@IDENTITY", webBannerItemViewEntity.WebBannerSysNo, webBannerItemViewEntity.ToUser);

            object objResult = DbHelperSQL.GetSingle(sql);

            return objResult == null ? 0 : Convert.ToInt32(objResult);
        }

        public int DeleteWebBannerItem(WebBannerItemViewEntity webBannerItemViewEntity)
        {
            string sql = string.Format("delete cm_WenBannerItem where SysNo={0}", webBannerItemViewEntity.SysNo);

            return DbHelperSQL.ExecuteSql(sql);
        }

        public int DeleteWebBannerItemByWebBannerSysNo(int webBannerSysNo)
        {
            string sql = "delete cm_WenBannerItem where WebBannerSysNo=" + webBannerSysNo;

            return DbHelperSQL.ExecuteSql(sql);
        }
    }
}
