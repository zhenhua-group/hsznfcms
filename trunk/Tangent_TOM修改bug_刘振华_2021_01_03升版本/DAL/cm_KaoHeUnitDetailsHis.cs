﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeUnitDetailsHis
	/// </summary>
	public partial class cm_KaoHeUnitDetailsHis
	{
		public cm_KaoHeUnitDetailsHis()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeUnitDetailsHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeUnitDetailsHis(");
			strSql.Append("RenwuID,RenwuName,UnitID,UnitName,UnitAllCount,ZTCJXS,UserName,JBGZ,GZSJ,PJYGZ,YFJJ,ZPHP,PM,BMJLPJ,PM2,JQXS,BMNJJJS,XMJJJS,XMJJJLTZ,XMJJTZ,YXYG,HJ,HJYF,GZBS,SBN,SBN2,XBN,XBN2)");
			strSql.Append(" values (");
			strSql.Append("@RenwuID,@RenwuName,@UnitID,@UnitName,@UnitAllCount,@ZTCJXS,@UserName,@JBGZ,@GZSJ,@PJYGZ,@YFJJ,@ZPHP,@PM,@BMJLPJ,@PM2,@JQXS,@BMNJJJS,@XMJJJS,@XMJJJLTZ,@XMJJTZ,@YXYG,@HJ,@HJYF,@GZBS,@SBN,@SBN2,@XBN,@XBN2)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@RenwuName", SqlDbType.VarChar,50),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.VarChar,50),
					new SqlParameter("@UnitAllCount", SqlDbType.Decimal,9),
					new SqlParameter("@ZTCJXS", SqlDbType.Decimal,9),
					new SqlParameter("@UserName", SqlDbType.VarChar,50),
					new SqlParameter("@JBGZ", SqlDbType.Decimal,9),
					new SqlParameter("@GZSJ", SqlDbType.Decimal,9),
					new SqlParameter("@PJYGZ", SqlDbType.Decimal,9),
					new SqlParameter("@YFJJ", SqlDbType.Decimal,9),
					new SqlParameter("@ZPHP", SqlDbType.Decimal,9),
					new SqlParameter("@PM", SqlDbType.Decimal,9),
					new SqlParameter("@BMJLPJ", SqlDbType.Decimal,9),
					new SqlParameter("@PM2", SqlDbType.Decimal,9),
					new SqlParameter("@JQXS", SqlDbType.Decimal,9),
					new SqlParameter("@BMNJJJS", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJJS", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJJLTZ", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJTZ", SqlDbType.Decimal,9),
					new SqlParameter("@YXYG", SqlDbType.Decimal,9),
					new SqlParameter("@HJ", SqlDbType.Decimal,9),
					new SqlParameter("@HJYF", SqlDbType.Decimal,9),
					new SqlParameter("@GZBS", SqlDbType.Decimal,9),
					new SqlParameter("@SBN", SqlDbType.Decimal,9),
					new SqlParameter("@SBN2", SqlDbType.Decimal,9),
					new SqlParameter("@XBN", SqlDbType.Decimal,9),
					new SqlParameter("@XBN2", SqlDbType.Decimal,9)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.RenwuName;
			parameters[2].Value = model.UnitID;
			parameters[3].Value = model.UnitName;
			parameters[4].Value = model.UnitAllCount;
			parameters[5].Value = model.ZTCJXS;
			parameters[6].Value = model.UserName;
			parameters[7].Value = model.JBGZ;
			parameters[8].Value = model.GZSJ;
			parameters[9].Value = model.PJYGZ;
			parameters[10].Value = model.YFJJ;
			parameters[11].Value = model.ZPHP;
			parameters[12].Value = model.PM;
			parameters[13].Value = model.BMJLPJ;
			parameters[14].Value = model.PM2;
			parameters[15].Value = model.JQXS;
			parameters[16].Value = model.BMNJJJS;
			parameters[17].Value = model.XMJJJS;
			parameters[18].Value = model.XMJJJLTZ;
			parameters[19].Value = model.XMJJTZ;
			parameters[20].Value = model.YXYG;
			parameters[21].Value = model.HJ;
			parameters[22].Value = model.HJYF;
			parameters[23].Value = model.GZBS;
			parameters[24].Value = model.SBN;
			parameters[25].Value = model.SBN2;
			parameters[26].Value = model.XBN;
			parameters[27].Value = model.XBN2;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeUnitDetailsHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeUnitDetailsHis set ");
			strSql.Append("RenwuID=@RenwuID,");
			strSql.Append("RenwuName=@RenwuName,");
			strSql.Append("UnitID=@UnitID,");
			strSql.Append("UnitName=@UnitName,");
			strSql.Append("UnitAllCount=@UnitAllCount,");
			strSql.Append("ZTCJXS=@ZTCJXS,");
			strSql.Append("UserName=@UserName,");
			strSql.Append("JBGZ=@JBGZ,");
			strSql.Append("GZSJ=@GZSJ,");
			strSql.Append("PJYGZ=@PJYGZ,");
			strSql.Append("YFJJ=@YFJJ,");
			strSql.Append("ZPHP=@ZPHP,");
			strSql.Append("PM=@PM,");
			strSql.Append("BMJLPJ=@BMJLPJ,");
			strSql.Append("PM2=@PM2,");
			strSql.Append("JQXS=@JQXS,");
			strSql.Append("BMNJJJS=@BMNJJJS,");
			strSql.Append("XMJJJS=@XMJJJS,");
			strSql.Append("XMJJJLTZ=@XMJJJLTZ,");
			strSql.Append("XMJJTZ=@XMJJTZ,");
			strSql.Append("YXYG=@YXYG,");
			strSql.Append("HJ=@HJ,");
			strSql.Append("HJYF=@HJYF,");
			strSql.Append("GZBS=@GZBS,");
			strSql.Append("SBN=@SBN,");
			strSql.Append("SBN2=@SBN2,");
			strSql.Append("XBN=@XBN,");
			strSql.Append("XBN2=@XBN2");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@RenwuName", SqlDbType.VarChar,50),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.VarChar,50),
					new SqlParameter("@UnitAllCount", SqlDbType.Decimal,9),
					new SqlParameter("@ZTCJXS", SqlDbType.Decimal,9),
					new SqlParameter("@UserName", SqlDbType.VarChar,50),
					new SqlParameter("@JBGZ", SqlDbType.Decimal,9),
					new SqlParameter("@GZSJ", SqlDbType.Decimal,9),
					new SqlParameter("@PJYGZ", SqlDbType.Decimal,9),
					new SqlParameter("@YFJJ", SqlDbType.Decimal,9),
					new SqlParameter("@ZPHP", SqlDbType.Decimal,9),
					new SqlParameter("@PM", SqlDbType.Decimal,9),
					new SqlParameter("@BMJLPJ", SqlDbType.Decimal,9),
					new SqlParameter("@PM2", SqlDbType.Decimal,9),
					new SqlParameter("@JQXS", SqlDbType.Decimal,9),
					new SqlParameter("@BMNJJJS", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJJS", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJJLTZ", SqlDbType.Decimal,9),
					new SqlParameter("@XMJJTZ", SqlDbType.Decimal,9),
					new SqlParameter("@YXYG", SqlDbType.Decimal,9),
					new SqlParameter("@HJ", SqlDbType.Decimal,9),
					new SqlParameter("@HJYF", SqlDbType.Decimal,9),
					new SqlParameter("@GZBS", SqlDbType.Decimal,9),
					new SqlParameter("@SBN", SqlDbType.Decimal,9),
					new SqlParameter("@SBN2", SqlDbType.Decimal,9),
					new SqlParameter("@XBN", SqlDbType.Decimal,9),
					new SqlParameter("@XBN2", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.RenwuName;
			parameters[2].Value = model.UnitID;
			parameters[3].Value = model.UnitName;
			parameters[4].Value = model.UnitAllCount;
			parameters[5].Value = model.ZTCJXS;
			parameters[6].Value = model.UserName;
			parameters[7].Value = model.JBGZ;
			parameters[8].Value = model.GZSJ;
			parameters[9].Value = model.PJYGZ;
			parameters[10].Value = model.YFJJ;
			parameters[11].Value = model.ZPHP;
			parameters[12].Value = model.PM;
			parameters[13].Value = model.BMJLPJ;
			parameters[14].Value = model.PM2;
			parameters[15].Value = model.JQXS;
			parameters[16].Value = model.BMNJJJS;
			parameters[17].Value = model.XMJJJS;
			parameters[18].Value = model.XMJJJLTZ;
			parameters[19].Value = model.XMJJTZ;
			parameters[20].Value = model.YXYG;
			parameters[21].Value = model.HJ;
			parameters[22].Value = model.HJYF;
			parameters[23].Value = model.GZBS;
			parameters[24].Value = model.SBN;
			parameters[25].Value = model.SBN2;
			parameters[26].Value = model.XBN;
			parameters[27].Value = model.XBN2;
			parameters[28].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeUnitDetailsHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeUnitDetailsHis ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeUnitDetailsHis GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,RenwuID,RenwuName,UnitID,UnitName,UnitAllCount,ZTCJXS,UserName,JBGZ,GZSJ,PJYGZ,YFJJ,ZPHP,PM,BMJLPJ,PM2,JQXS,BMNJJJS,XMJJJS,XMJJJLTZ,XMJJTZ,YXYG,HJ,HJYF,GZBS,SBN,SBN2,XBN,XBN2 from cm_KaoHeUnitDetailsHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeUnitDetailsHis model=new TG.Model.cm_KaoHeUnitDetailsHis();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeUnitDetailsHis DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeUnitDetailsHis model=new TG.Model.cm_KaoHeUnitDetailsHis();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["RenwuID"]!=null && row["RenwuID"].ToString()!="")
				{
					model.RenwuID=int.Parse(row["RenwuID"].ToString());
				}
				if(row["RenwuName"]!=null)
				{
					model.RenwuName=row["RenwuName"].ToString();
				}
				if(row["UnitID"]!=null && row["UnitID"].ToString()!="")
				{
					model.UnitID=int.Parse(row["UnitID"].ToString());
				}
				if(row["UnitName"]!=null)
				{
					model.UnitName=row["UnitName"].ToString();
				}
				if(row["UnitAllCount"]!=null && row["UnitAllCount"].ToString()!="")
				{
					model.UnitAllCount=decimal.Parse(row["UnitAllCount"].ToString());
				}
				if(row["ZTCJXS"]!=null && row["ZTCJXS"].ToString()!="")
				{
					model.ZTCJXS=decimal.Parse(row["ZTCJXS"].ToString());
				}
				if(row["UserName"]!=null)
				{
					model.UserName=row["UserName"].ToString();
				}
				if(row["JBGZ"]!=null && row["JBGZ"].ToString()!="")
				{
					model.JBGZ=decimal.Parse(row["JBGZ"].ToString());
				}
				if(row["GZSJ"]!=null && row["GZSJ"].ToString()!="")
				{
					model.GZSJ=decimal.Parse(row["GZSJ"].ToString());
				}
				if(row["PJYGZ"]!=null && row["PJYGZ"].ToString()!="")
				{
					model.PJYGZ=decimal.Parse(row["PJYGZ"].ToString());
				}
				if(row["YFJJ"]!=null && row["YFJJ"].ToString()!="")
				{
					model.YFJJ=decimal.Parse(row["YFJJ"].ToString());
				}
				if(row["ZPHP"]!=null && row["ZPHP"].ToString()!="")
				{
					model.ZPHP=decimal.Parse(row["ZPHP"].ToString());
				}
				if(row["PM"]!=null && row["PM"].ToString()!="")
				{
					model.PM=decimal.Parse(row["PM"].ToString());
				}
				if(row["BMJLPJ"]!=null && row["BMJLPJ"].ToString()!="")
				{
					model.BMJLPJ=decimal.Parse(row["BMJLPJ"].ToString());
				}
				if(row["PM2"]!=null && row["PM2"].ToString()!="")
				{
					model.PM2=decimal.Parse(row["PM2"].ToString());
				}
				if(row["JQXS"]!=null && row["JQXS"].ToString()!="")
				{
					model.JQXS=decimal.Parse(row["JQXS"].ToString());
				}
				if(row["BMNJJJS"]!=null && row["BMNJJJS"].ToString()!="")
				{
					model.BMNJJJS=decimal.Parse(row["BMNJJJS"].ToString());
				}
				if(row["XMJJJS"]!=null && row["XMJJJS"].ToString()!="")
				{
					model.XMJJJS=decimal.Parse(row["XMJJJS"].ToString());
				}
				if(row["XMJJJLTZ"]!=null && row["XMJJJLTZ"].ToString()!="")
				{
					model.XMJJJLTZ=decimal.Parse(row["XMJJJLTZ"].ToString());
				}
				if(row["XMJJTZ"]!=null && row["XMJJTZ"].ToString()!="")
				{
					model.XMJJTZ=decimal.Parse(row["XMJJTZ"].ToString());
				}
				if(row["YXYG"]!=null && row["YXYG"].ToString()!="")
				{
					model.YXYG=decimal.Parse(row["YXYG"].ToString());
				}
				if(row["HJ"]!=null && row["HJ"].ToString()!="")
				{
					model.HJ=decimal.Parse(row["HJ"].ToString());
				}
				if(row["HJYF"]!=null && row["HJYF"].ToString()!="")
				{
					model.HJYF=decimal.Parse(row["HJYF"].ToString());
				}
				if(row["GZBS"]!=null && row["GZBS"].ToString()!="")
				{
					model.GZBS=decimal.Parse(row["GZBS"].ToString());
				}
				if(row["SBN"]!=null && row["SBN"].ToString()!="")
				{
					model.SBN=decimal.Parse(row["SBN"].ToString());
				}
				if(row["SBN2"]!=null && row["SBN2"].ToString()!="")
				{
					model.SBN2=decimal.Parse(row["SBN2"].ToString());
				}
				if(row["XBN"]!=null && row["XBN"].ToString()!="")
				{
					model.XBN=decimal.Parse(row["XBN"].ToString());
				}
				if(row["XBN2"]!=null && row["XBN2"].ToString()!="")
				{
					model.XBN2=decimal.Parse(row["XBN2"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,RenwuID,RenwuName,UnitID,UnitName,UnitAllCount,ZTCJXS,UserName,JBGZ,GZSJ,PJYGZ,YFJJ,ZPHP,PM,BMJLPJ,PM2,JQXS,BMNJJJS,XMJJJS,XMJJJLTZ,XMJJTZ,YXYG,HJ,HJYF,GZBS,SBN,SBN2,XBN,XBN2 ");
			strSql.Append(" FROM cm_KaoHeUnitDetailsHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,RenwuID,RenwuName,UnitID,UnitName,UnitAllCount,ZTCJXS,UserName,JBGZ,GZSJ,PJYGZ,YFJJ,ZPHP,PM,BMJLPJ,PM2,JQXS,BMNJJJS,XMJJJS,XMJJJLTZ,XMJJTZ,YXYG,HJ,HJYF,GZBS,SBN,SBN2,XBN,XBN2 ");
			strSql.Append(" FROM cm_KaoHeUnitDetailsHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeUnitDetailsHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeUnitDetailsHis T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeUnitDetailsHis";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

