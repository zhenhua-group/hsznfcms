﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:tg_unit
    /// </summary>
    public partial class tg_unit
    {
        public tg_unit()
        { }
        #region  Method
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.tg_unit model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.unit_Name != null)
            {
                strSql1.Append("unit_Name,");
                strSql2.Append("'" + model.unit_Name + "',");
            }
            if (model.unit_ParentID != null)
            {
                strSql1.Append("unit_ParentID,");
                strSql2.Append("" + model.unit_ParentID + ",");
            }
            if (model.unit_IsEndUnit != null)
            {
                strSql1.Append("unit_IsEndUnit,");
                strSql2.Append("" + model.unit_IsEndUnit + ",");
            }
            if (model.unit_Intro != null)
            {
                strSql1.Append("unit_Intro,");
                strSql2.Append("'" + model.unit_Intro + "',");
            }
            strSql.Append("insert into tg_unit(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.tg_unit model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update tg_unit set ");
            if (model.unit_Name != null)
            {
                strSql.Append("unit_Name='" + model.unit_Name + "',");
            }
            else
            {
                strSql.Append("unit_Name= null ,");
            }
            if (model.unit_ParentID != null)
            {
                strSql.Append("unit_ParentID=" + model.unit_ParentID + ",");
            }
            else
            {
                strSql.Append("unit_ParentID= null ,");
            }
            if (model.unit_IsEndUnit != null)
            {
                strSql.Append("unit_IsEndUnit=" + model.unit_IsEndUnit + ",");
            }
            else
            {
                strSql.Append("unit_IsEndUnit= null ,");
            }
            if (model.unit_Intro != null)
            {
                strSql.Append("unit_Intro='" + model.unit_Intro + "',");
            }
            else
            {
                strSql.Append("unit_Intro= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where unit_ID=" + model.unit_ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int unit_ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tg_unit ");
            strSql.Append(" where unit_ID=" + unit_ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string unit_IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tg_unit ");
            strSql.Append(" where unit_ID in (" + unit_IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.tg_unit GetModel(int unit_ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" unit_ID,unit_Name,unit_ParentID,unit_IsEndUnit,unit_Intro ");
            strSql.Append(" from tg_unit ");
            strSql.Append(" where unit_ID=" + unit_ID + "");
            TG.Model.tg_unit model = new TG.Model.tg_unit();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["unit_ID"] != null && ds.Tables[0].Rows[0]["unit_ID"].ToString() != "")
                {
                    model.unit_ID = int.Parse(ds.Tables[0].Rows[0]["unit_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["unit_Name"] != null && ds.Tables[0].Rows[0]["unit_Name"].ToString() != "")
                {
                    model.unit_Name = ds.Tables[0].Rows[0]["unit_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["unit_ParentID"] != null && ds.Tables[0].Rows[0]["unit_ParentID"].ToString() != "")
                {
                    model.unit_ParentID = Convert.ToInt32(ds.Tables[0].Rows[0]["unit_ParentID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["unit_IsEndUnit"] != null && ds.Tables[0].Rows[0]["unit_IsEndUnit"].ToString() != "")
                {
                    model.unit_IsEndUnit = Convert.ToInt32(ds.Tables[0].Rows[0]["unit_IsEndUnit"].ToString());
                }
                if (ds.Tables[0].Rows[0]["unit_Intro"] != null && ds.Tables[0].Rows[0]["unit_Intro"].ToString() != "")
                {
                    model.unit_Intro = ds.Tables[0].Rows[0]["unit_Intro"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select unit_ID,unit_Name,unit_ParentID,unit_IsEndUnit,unit_Intro ");
            strSql.Append(" FROM tg_unit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" unit_ID,unit_Name,unit_ParentID,unit_IsEndUnit,unit_Intro ");
            strSql.Append(" FROM tg_unit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM tg_unit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select unit_ID,unit_Name,unit_ParentID,unit_IsEndUnit,unit_Intro ");
            sb.Append(" FROM tg_unit ");
            if (strWhere.Trim() != "")
            {
                sb.Append(" where " + strWhere);
            }
            if (orderby.Trim() != "")
            {
                sb.Append(" Order by " + orderby);
            }
            return DbHelperSQL.Query(sb.ToString(), startIndex, endIndex);
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex,bool haveType)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select A.unit_ID,A.unit_Name,unit_ParentID,unit_IsEndUnit,unit_Intro,B.unit_Type,B.unit_Order ");
            sb.Append(" FROM tg_unit A join tg_unitExt B on A.unit_ID=B.unit_ID ");
            if (strWhere.Trim() != "")
            {
                sb.Append(" where " + strWhere);
            }
            if (orderby.Trim() != "")
            {
                sb.Append(" Order by " + orderby);
            }
            return DbHelperSQL.Query(sb.ToString(), startIndex, endIndex);
        }
        /// <summary>
        /// 分阶段获得数据
        /// </summary>
        /// <param name="strWhere">where语句</param>
        /// <param name="pagesize">每页数量</param>
        /// <param name="pageIndex">要得到第几页的数据</param>
        /// <returns>数据集</returns>
        public DataSet GetListByPage(string strWhere, int pagesize, int pageIndex)
        {
            StringBuilder sqlStr = new StringBuilder();
            int notInTop = (pageIndex - 1) * 10;
            sqlStr.Append("select top " + pagesize + "* from tg_unit where unit_ID not in ");
            sqlStr.Append(" (select TOP " + notInTop + " unit_ID from tg_unit ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sqlStr.AppendFormat("where ({0})", strWhere);
            }
            sqlStr.Append(")");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sqlStr.AppendFormat(" and ({0})", strWhere);
            }
            return DbHelperSQL.Query(sqlStr.ToString());
        }

        #endregion  Method
    }
}

