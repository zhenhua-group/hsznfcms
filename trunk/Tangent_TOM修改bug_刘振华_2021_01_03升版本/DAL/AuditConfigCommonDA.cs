﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using TG.DBUtility;
using System.Data.SqlClient;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    /// <summary>
    /// 审核配置共通类
    /// </summary>
    public class AuditConfigCommonDA
    {
        /// <summary>
        /// 得到一个审核配置共通的方法
        /// </summary>
        /// <param name="configTableName"></param>
        /// <returns></returns>
        public List<AuditConfigBase> GetAudtConfigBaseEntityList(string configTableName)
        {
            string sql = "select pa.SysNo,pa.ProcessDescription,pa.RoleSysNo,pa.Position,pa.InUser,pa.InDate,r.Users,r.RoleName from " + configTableName + " pa join cm_Role r on r.SysNo = pa.RoleSysNo order by pa.Position";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<AuditConfigBase> resultList = EntityBuilder<AuditConfigBase>.BuilderEntityList(reader);

            return resultList;
        }

        /// <summary>
        /// 修改流程所对应的角色
        /// </summary>
        /// <param name="auditConfigEntity"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public int UpdateProjectAuditConfigToRelationship(AuditConfigEntity auditConfigEntity, string tableName)
        {
            string sql = "update " + tableName + " set RoleSysNo=" + auditConfigEntity.RoleSysNo + " where SysNo= " + auditConfigEntity.AuditConfigSysNo;

            return DbHelperSQL.ExecuteSql(sql);
        }
    }
}
