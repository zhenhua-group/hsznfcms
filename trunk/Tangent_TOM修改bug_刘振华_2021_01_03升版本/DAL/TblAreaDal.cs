﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TG.Model;
using TG.DBUtility;

namespace TG.DAL
{
    public class TblAreaDal
    {
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TblAreaModel GetModel(int AreaId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 AreaId,AreaName,AreaPId,AreaSrcCity,ToYJYCount from TblArea ");
            strSql.Append(" where AreaId=@AreaId");
            SqlParameter[] parameters = {
					new SqlParameter("@AreaId", SqlDbType.Int,4)
			};
            parameters[0].Value = AreaId;

            TblAreaModel model = new TblAreaModel();
            DataSet ds = TGsqlHelp.dataset(strSql.ToString(), parameters);
            return DataToModel(model, ds);
        }
        public string GetToAspx()
        {
            string sql = "SELECT TblArea.Toaspx FROM TblArea  WHERE isShow=1";
            return TG.DBUtility.DbHelperSQL.GetSingle(sql).ToString();
        }
        /// <summary>
        /// reader的数据转换成model对象
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private static TblAreaModel ReaderToModel(SqlDataReader reader)
        {
            TblAreaModel model = new TblAreaModel();
            if (reader["AreaId"] != null && reader["AreaId"].ToString() != "")
            {
                model.AreaId = int.Parse(reader["AreaId"].ToString());
            }
            if (reader["AreaName"] != null && reader["AreaName"].ToString() != "")
            {
                model.AreaName = reader["AreaName"].ToString();
            }
            if (reader["AreaPId"] != null && reader["AreaPId"].ToString() != "")
            {
                model.AreaPId = int.Parse(reader["AreaPId"].ToString());
            }
            if (reader["AreaSrcCity"] != null && reader["AreaSrcCity"].ToString() != "")
            {
                model.AreaSrcCity = reader["AreaSrcCity"].ToString();
            }
            if (reader["ToYJYCount"] != null && reader["ToYJYCount"].ToString() != "")
            {
                model.ToYJYCount = int.Parse(reader["ToYJYCount"].ToString());
            }
            return model;
        }
        /// <summary>
        /// 将数据库数据dataset转变成model实体
        /// </summary>
        /// <param name="model"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        private static TblAreaModel DataToModel(TblAreaModel model, DataSet ds)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["AreaId"] != null && ds.Tables[0].Rows[0]["AreaId"].ToString() != "")
                {
                    model.AreaId = int.Parse(ds.Tables[0].Rows[0]["AreaId"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AreaName"] != null && ds.Tables[0].Rows[0]["AreaName"].ToString() != "")
                {
                    model.AreaName = ds.Tables[0].Rows[0]["AreaName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AreaPId"] != null && ds.Tables[0].Rows[0]["AreaPId"].ToString() != "")
                {
                    model.AreaPId = int.Parse(ds.Tables[0].Rows[0]["AreaPId"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AreaSrcCity"] != null && ds.Tables[0].Rows[0]["AreaSrcCity"].ToString() != "")
                {
                    model.AreaSrcCity = ds.Tables[0].Rows[0]["AreaSrcCity"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ToYJYCount"] != null && ds.Tables[0].Rows[0]["ToYJYCount"].ToString() != "")
                {
                    model.ToYJYCount = int.Parse(ds.Tables[0].Rows[0]["ToYJYCount"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select AreaId,AreaName,AreaPId,AreaSrcCity,ToYJYCount ");
            strSql.Append(" FROM TblArea ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return TGsqlHelp.dataset(strSql.ToString());
        }
        public string GetAreaSrc(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select AreaSrcCity");
            strSql.Append(" FROM TblArea ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return TGsqlHelp.ExecuteScalar(strSql.ToString()).ToString();
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public List<TblAreaModel> GetListByareapid(string areapid)
        {
            List<TblAreaModel> list = new List<TblAreaModel>();
            string sql = "select AreaId,AreaName,AreaPId,AreaSrcCity,ToYJYCount FROM TblArea where AreaPId=@areapid";
            SqlParameter sp = new SqlParameter("@areapid", areapid);
            using (SqlDataReader reader = TGsqlHelp.ExecuteReader(sql, sp))
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TblAreaModel model = ReaderToModel(reader);
                        list.Add(model);
                    }
                }
            }
            return list;
        }
        //更新城市页面的选择
        public int Update1(string areaname)
        {
            string sql = " UPDATE TblArea SET isShow=0,Toaspx='' WHERE AreaName!='" + areaname + "'";
            return TG.DBUtility.DbHelperSQL.ExecuteSql(sql);
        }
        public object GetId(string areaname)
        {
            string sql = "SELECT TblArea.AreaId FROM TblArea WHERE AreaName='" + areaname + "'";
            return TG.DBUtility.DbHelperSQL.GetSingle(sql);
        }
        public int Update2(string areaname, string toaspx)
        {
            string sql = "UPDATE TblArea SET isShow=1,Toaspx=@toaspx WHERE AreaName='" + areaname + "'";
            SqlParameter sp = new SqlParameter("@toaspx", toaspx);
            return TG.DBUtility.DbHelperSQL.ExecuteSql(sql, sp);
        }
        public string GetShow()
        {
            string sql = "SELECT TblArea.AreaName FROM TblArea  WHERE isShow=1";
            return TG.DBUtility.DbHelperSQL.GetSingle(sql).ToString();
        }
        public List<TblAreaModel> GetListByName(string areaname)
        {
            List<TblAreaModel> list = new List<TblAreaModel>();
            string sql = "select AreaId,AreaName,AreaPId,AreaSrcCity,ToYJYCount FROM TblArea where AreaPId =( select AreaId from TblArea where AreaName=@areaname)";
            SqlParameter sp = new SqlParameter("@areaname", areaname);
            using (SqlDataReader reader = TGsqlHelp.ExecuteReader(sql, sp))
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TblAreaModel model = ReaderToModel(reader);
                        list.Add(model);
                    }
                }
            }
            return list;
        }
    }
}
