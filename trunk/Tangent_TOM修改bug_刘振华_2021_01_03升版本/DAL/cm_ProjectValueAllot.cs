﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TG.DBUtility;
using TG.Model;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_ProjectValueAllot
    /// </summary>
    public partial class cm_ProjectValueAllot
    {
        public cm_ProjectValueAllot()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectValueAllot model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_ProjectValueAllot(");
            strSql.Append("pro_ID,AllotTimes,AllotCount,AllotDate,persent,PaidValuePercent,PaidValueCount,DesignManagerPercent,DesignManagerCount,AllotValuePercent,EconomyValuePercent,EconomyValueCount,UnitValuePercent,UnitValueCount,AllotUser,Status,mark,ItemType,OtherDeptValuePercent,OtherDeptValueCount,TheDeptValuePercent,TheDeptValueCount,HavcPercent,HavcCount,ProgramPercent,ProgramCount,ShouldBeValuePercent,ShouldBeValueCount,AllotValueCount,SecondValue,TranBulidingPercent,TranBulidingCount,UnitId,ActualAllountTime,BulidType,FinanceValuePercent,FinanceValueCount,TheDeptShouldValuePercent,TheDeptShouldValueCount,DividedPercent)");
            strSql.Append(" values (");
            strSql.Append("@pro_ID,@AllotTimes,@AllotCount,@AllotDate,@persent,@PaidValuePercent,@PaidValueCount,@DesignManagerPercent,@DesignManagerCount,@AllotValuePercent,@EconomyValuePercent,@EconomyValueCount,@UnitValuePercent,@UnitValueCount,@AllotUser,@Status,@mark,@ItemType,@OtherDeptValuePercent,@OtherDeptValueCount,@TheDeptValuePercent,@TheDeptValueCount,@HavcPercent,@HavcCount,@ProgramPercent,@ProgramCount,@ShouldBeValuePercent,@ShouldBeValueCount,@AllotValueCount,@SecondValue,@TranBulidingPercent,@TranBulidingCount,@UnitId,@ActualAllountTime,@BulidType,@FinanceValuePercent,@FinanceValueCount,@TheDeptShouldValuePercent,@TheDeptShouldValueCount,@DividedPercent)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@pro_ID", SqlDbType.Int,4),
					new SqlParameter("@AllotTimes", SqlDbType.NVarChar,50),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,18),
					new SqlParameter("@AllotDate", SqlDbType.DateTime),
					new SqlParameter("@persent", SqlDbType.Decimal,18),
					new SqlParameter("@PaidValuePercent", SqlDbType.Decimal,18),
					new SqlParameter("@PaidValueCount", SqlDbType.Decimal,18),
					new SqlParameter("@DesignManagerPercent", SqlDbType.Decimal,18),
					new SqlParameter("@DesignManagerCount", SqlDbType.Decimal,18),
					new SqlParameter("@AllotValuePercent", SqlDbType.NVarChar,50),
					new SqlParameter("@EconomyValuePercent", SqlDbType.Decimal,18),
					new SqlParameter("@EconomyValueCount", SqlDbType.Decimal,18),
					new SqlParameter("@UnitValuePercent", SqlDbType.Decimal,18),
					new SqlParameter("@UnitValueCount", SqlDbType.Decimal,18),
					new SqlParameter("@AllotUser", SqlDbType.NVarChar,50),
					new SqlParameter("@Status", SqlDbType.Char,1),
					new SqlParameter("@mark", SqlDbType.NVarChar,50),
                    new SqlParameter("@ItemType",SqlDbType.Int,4),
                    new SqlParameter("@OtherDeptValuePercent", SqlDbType.Decimal,18),
                    new SqlParameter("@OtherDeptValueCount", SqlDbType.Decimal,18),
                    new SqlParameter("@TheDeptValuePercent", SqlDbType.Decimal,18),  
                    new SqlParameter("@TheDeptValueCount", SqlDbType.Decimal,18),
                    new SqlParameter("@HavcPercent", SqlDbType.Decimal,18),
                    new SqlParameter("@HavcCount", SqlDbType.Decimal,18),
                    new SqlParameter("@ProgramPercent", SqlDbType.Decimal,18),  
                    new SqlParameter("@ProgramCount", SqlDbType.Decimal,18),
                    new SqlParameter("@ShouldBeValuePercent", SqlDbType.Decimal,18),  
                    new SqlParameter("@ShouldBeValueCount", SqlDbType.Decimal,18),
                    new SqlParameter("@AllotValueCount", SqlDbType.Decimal,18),
                    new SqlParameter("@SecondValue", SqlDbType.Char,1  ),
                    new SqlParameter("@TranBulidingPercent", SqlDbType.Decimal,18),
                    new SqlParameter("@TranBulidingCount", SqlDbType.Decimal,18),
                    new SqlParameter("@UnitId",SqlDbType.Int,4),
                    new SqlParameter("@ActualAllountTime", SqlDbType.NVarChar,10),
                    new SqlParameter("@BulidType", SqlDbType.NVarChar,10), 
                    new SqlParameter("@FinanceValuePercent", SqlDbType.Decimal,18),
                    new SqlParameter("@FinanceValueCount", SqlDbType.Decimal,18),
                    new SqlParameter("@TheDeptShouldValuePercent", SqlDbType.Decimal,18),
                    new SqlParameter("@TheDeptShouldValueCount", SqlDbType.Decimal,18),
                    new SqlParameter("@DividedPercent", SqlDbType.Decimal,18)};

            parameters[0].Value = model.pro_ID;
            parameters[1].Value = model.AllotTimes;
            parameters[2].Value = model.AllotCount;
            parameters[3].Value = model.AllotDate;
            parameters[4].Value = model.persent;
            parameters[5].Value = model.PaidValuePercent;
            parameters[6].Value = model.PaidValueCount;
            parameters[7].Value = model.DesignManagerPercent;
            parameters[8].Value = model.DesignManagerCount;
            parameters[9].Value = model.AllotValuePercent;
            parameters[10].Value = model.EconomyValuePercent;
            parameters[11].Value = model.EconomyValueCount;
            parameters[12].Value = model.UnitValuePercent;
            parameters[13].Value = model.UnitValueCount;
            parameters[14].Value = model.AllotUser;
            parameters[15].Value = model.Status;
            parameters[16].Value = model.mark;
            parameters[17].Value = model.Itemtype;
            parameters[18].Value = model.Otherdeptallotpercent;
            parameters[19].Value = model.Otherdeptallotcount;
            parameters[20].Value = model.Thedeptallotpercent;
            parameters[21].Value = model.Thedeptallotcount;
            parameters[22].Value = model.HavcValuePercent;
            parameters[23].Value = model.HavcValueCount;
            parameters[24].Value = model.ProgramPercent;
            parameters[25].Value = model.ProgramCount;
            parameters[26].Value = model.ShouldBeValuePercent;
            parameters[27].Value = model.ShouldBeValueCount;
            parameters[28].Value = model.Allotvaluecount;
            parameters[29].Value = model.SecondValue;
            parameters[30].Value = model.TranBulidingPercent;
            parameters[31].Value = model.TranBulidingCount;
            parameters[32].Value = model.UnitId;
            parameters[33].Value = model.ActualAllountTime;
            parameters[34].Value = model.BulidType;
            parameters[35].Value = model.FinanceValuePercent;
            parameters[36].Value = model.FinanceValueCount;
            parameters[37].Value = model.TheDeptShouldValuePercent;
            parameters[38].Value = model.TheDeptShouldValueCount;
            parameters[39].Value = model.DividedPercent;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectValueAllot model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ProjectValueAllot set ");
            strSql.Append("pro_ID=@pro_ID,");
            strSql.Append("AllotTimes=@AllotTimes,");
            strSql.Append("AllotCount=@AllotCount,");
            strSql.Append("AllotDate=@AllotDate,");
            strSql.Append("persent=@persent,");
            strSql.Append("PaidValuePercent=@PaidValuePercent,");
            strSql.Append("PaidValueCount=@PaidValueCount,");
            strSql.Append("DesignManagerPercent=@DesignManagerPercent,");
            strSql.Append("DesignManagerCount=@DesignManagerCount,");
            strSql.Append("AllotValuePercent=@AllotValuePercent,");
            strSql.Append("EconomyValuePercent=@EconomyValuePercent,");
            strSql.Append("EconomyValueCount=@EconomyValueCount,");
            strSql.Append("UnitValuePercent=@UnitValuePercent,");
            strSql.Append("UnitValueCount=@UnitValueCount,");
            strSql.Append("AllotUser=@AllotUser,");
            strSql.Append("Status=@Status,");
            strSql.Append("mark=@mark,");
            strSql.Append("HavcPercent=@HavcPercent,");
            strSql.Append("HavcCount=@HavcCount,");
            strSql.Append("ProgramPercent=@ProgramPercent,");
            strSql.Append("ProgramCount=@ProgramCount,");
            strSql.Append("ShouldBeValuePercent=@ShouldBeValuePercent,");
            strSql.Append("ShouldBeValueCount=@ShouldBeValueCount");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@pro_ID", SqlDbType.Int,4),
					new SqlParameter("@AllotTimes", SqlDbType.NVarChar,50),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,18),
					new SqlParameter("@AllotDate", SqlDbType.DateTime),
					new SqlParameter("@persent", SqlDbType.Decimal,18),
					new SqlParameter("@PaidValuePercent", SqlDbType.Decimal,18),
					new SqlParameter("@PaidValueCount", SqlDbType.Decimal,18),
					new SqlParameter("@DesignManagerPercent", SqlDbType.Decimal,18),
					new SqlParameter("@DesignManagerCount", SqlDbType.Decimal,18),
					new SqlParameter("@AllotValuePercent", SqlDbType.NVarChar,50),
					new SqlParameter("@EconomyValuePercent", SqlDbType.Decimal,18),
					new SqlParameter("@EconomyValueCount", SqlDbType.Decimal,18),
					new SqlParameter("@UnitValuePercent", SqlDbType.Decimal,18),
					new SqlParameter("@UnitValueCount", SqlDbType.Decimal,18),
					new SqlParameter("@AllotUser", SqlDbType.NVarChar,50),
					new SqlParameter("@Status", SqlDbType.Char,1),
					new SqlParameter("@mark", SqlDbType.NVarChar,300),
                    new SqlParameter("@HavcPercent", SqlDbType.Decimal,18),
                    new SqlParameter("@HavcCount", SqlDbType.Decimal,18),
                    new SqlParameter("@ProgramPercent", SqlDbType.Decimal,18),  
                    new SqlParameter("@ProgramCount", SqlDbType.Decimal,18),
                    new SqlParameter("@ShouldBeValuePercent", SqlDbType.Decimal,18),  
                    new SqlParameter("@ShouldBeValueCount", SqlDbType.Decimal,18),
                    new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.pro_ID;
            parameters[1].Value = model.AllotTimes;
            parameters[2].Value = model.AllotCount;
            parameters[3].Value = model.AllotDate;
            parameters[4].Value = model.persent;
            parameters[5].Value = model.PaidValuePercent;
            parameters[6].Value = model.PaidValueCount;
            parameters[7].Value = model.DesignManagerPercent;
            parameters[8].Value = model.DesignManagerCount;
            parameters[9].Value = model.AllotValuePercent;
            parameters[10].Value = model.EconomyValuePercent;
            parameters[11].Value = model.EconomyValueCount;
            parameters[12].Value = model.UnitValuePercent;
            parameters[13].Value = model.UnitValueCount;
            parameters[14].Value = model.AllotUser;
            parameters[15].Value = model.Status;
            parameters[16].Value = model.mark;
            parameters[17].Value = model.HavcValuePercent;
            parameters[18].Value = model.HavcValueCount;
            parameters[19].Value = model.ProgramPercent;
            parameters[20].Value = model.ProgramCount;
            parameters[21].Value = model.ShouldBeValuePercent;
            parameters[22].Value = model.ShouldBeValueCount;
            parameters[23].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            return rows;
        }

        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="allotID"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public int UpdateAllotDataInfo(int? allotID, int ProId, string status)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //更新产值分配表状态
                command.CommandText = "update cm_ProjectValueAllot set Status='" + status + "' where  ID=" + allotID + "";
                command.ExecuteNonQuery();

                //更新二次产值分配
                command.CommandText = "update cm_ProjectSecondValueAllot set Status='" + status + "'  where AllotID=" + allotID + " and ProID=" + ProId + "";
                command.ExecuteNonQuery();

                //更新人员状态
                command.CommandText = "update cm_ProjectValueByMemberDetails set Status='" + status + "' where AllotID=" + allotID + " and ProID=" + ProId + "";
                command.ExecuteNonQuery();

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProjectValueAllot ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProjectValueAllot ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectValueAllot GetModel(int? ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from cm_ProjectValueAllot ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_ProjectValueAllot model = new TG.Model.cm_ProjectValueAllot();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_ID"] != null && ds.Tables[0].Rows[0]["pro_ID"].ToString() != "")
                {
                    model.pro_ID = int.Parse(ds.Tables[0].Rows[0]["pro_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotTimes"] != null && ds.Tables[0].Rows[0]["AllotTimes"].ToString() != "")
                {
                    model.AllotTimes = ds.Tables[0].Rows[0]["AllotTimes"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AllotCount"] != null && ds.Tables[0].Rows[0]["AllotCount"].ToString() != "")
                {
                    model.AllotCount = decimal.Parse(ds.Tables[0].Rows[0]["AllotCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotDate"] != null && ds.Tables[0].Rows[0]["AllotDate"].ToString() != "")
                {
                    model.AllotDate = DateTime.Parse(ds.Tables[0].Rows[0]["AllotDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["persent"] != null && ds.Tables[0].Rows[0]["persent"].ToString() != "")
                {
                    model.persent = decimal.Parse(ds.Tables[0].Rows[0]["persent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PaidValuePercent"] != null && ds.Tables[0].Rows[0]["PaidValuePercent"].ToString() != "")
                {
                    model.PaidValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["PaidValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PaidValueCount"] != null && ds.Tables[0].Rows[0]["PaidValueCount"].ToString() != "")
                {
                    model.PaidValueCount = decimal.Parse(ds.Tables[0].Rows[0]["PaidValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DesignManagerPercent"] != null && ds.Tables[0].Rows[0]["DesignManagerPercent"].ToString() != "")
                {
                    model.DesignManagerPercent = decimal.Parse(ds.Tables[0].Rows[0]["DesignManagerPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DesignManagerCount"] != null && ds.Tables[0].Rows[0]["DesignManagerCount"].ToString() != "")
                {
                    model.DesignManagerCount = decimal.Parse(ds.Tables[0].Rows[0]["DesignManagerCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotValuePercent"] != null && ds.Tables[0].Rows[0]["AllotValuePercent"].ToString() != "")
                {
                    model.AllotValuePercent = ds.Tables[0].Rows[0]["AllotValuePercent"].ToString();
                }
                if (ds.Tables[0].Rows[0]["EconomyValuePercent"] != null && ds.Tables[0].Rows[0]["EconomyValuePercent"].ToString() != "")
                {
                    model.EconomyValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["EconomyValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["EconomyValueCount"] != null && ds.Tables[0].Rows[0]["EconomyValueCount"].ToString() != "")
                {
                    model.EconomyValueCount = decimal.Parse(ds.Tables[0].Rows[0]["EconomyValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UnitValuePercent"] != null && ds.Tables[0].Rows[0]["UnitValuePercent"].ToString() != "")
                {
                    model.UnitValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["UnitValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UnitValueCount"] != null && ds.Tables[0].Rows[0]["UnitValueCount"].ToString() != "")
                {
                    model.UnitValueCount = decimal.Parse(ds.Tables[0].Rows[0]["UnitValueCount"].ToString());
                }

                if (ds.Tables[0].Rows[0]["AllotUser"] != null && ds.Tables[0].Rows[0]["AllotUser"].ToString() != "")
                {
                    model.AllotUser = ds.Tables[0].Rows[0]["AllotUser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ItemType"] != null && ds.Tables[0].Rows[0]["ItemType"].ToString() != "")
                {
                    model.Itemtype = int.Parse(ds.Tables[0].Rows[0]["ItemType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mark"] != null && ds.Tables[0].Rows[0]["mark"].ToString() != "")
                {
                    model.mark = ds.Tables[0].Rows[0]["mark"].ToString();
                }

                if (ds.Tables[0].Rows[0]["HavcPercent"] != null && ds.Tables[0].Rows[0]["HavcPercent"].ToString() != "")
                {
                    model.HavcValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["HavcPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HavcCount"] != null && ds.Tables[0].Rows[0]["HavcCount"].ToString() != "")
                {
                    model.HavcValueCount = decimal.Parse(ds.Tables[0].Rows[0]["HavcCount"].ToString());
                }

                if (ds.Tables[0].Rows[0]["ProgramPercent"] != null && ds.Tables[0].Rows[0]["ProgramPercent"].ToString() != "")
                {
                    model.ProgramPercent = decimal.Parse(ds.Tables[0].Rows[0]["ProgramPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProgramCount"] != null && ds.Tables[0].Rows[0]["ProgramCount"].ToString() != "")
                {
                    model.ProgramCount = decimal.Parse(ds.Tables[0].Rows[0]["ProgramCount"].ToString());
                }

                if (ds.Tables[0].Rows[0]["ShouldBeValuePercent"] != null && ds.Tables[0].Rows[0]["ShouldBeValuePercent"].ToString() != "")
                {
                    model.ShouldBeValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["ShouldBeValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ShouldBeValueCount"] != null && ds.Tables[0].Rows[0]["ShouldBeValueCount"].ToString() != "")
                {
                    model.ShouldBeValueCount = decimal.Parse(ds.Tables[0].Rows[0]["ShouldBeValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TheDeptValueCount"] != null && ds.Tables[0].Rows[0]["TheDeptValueCount"].ToString() != "")
                {
                    model.Thedeptallotcount = decimal.Parse(ds.Tables[0].Rows[0]["TheDeptValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TheDeptValuePercent"] != null && ds.Tables[0].Rows[0]["TheDeptValuePercent"].ToString() != "")
                {
                    model.Thedeptallotpercent = decimal.Parse(ds.Tables[0].Rows[0]["TheDeptValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TranBulidingCount"] != null && ds.Tables[0].Rows[0]["TranBulidingCount"].ToString() != "")
                {
                    model.TranBulidingCount = decimal.Parse(ds.Tables[0].Rows[0]["TranBulidingCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TranBulidingPercent"] != null && ds.Tables[0].Rows[0]["TranBulidingPercent"].ToString() != "")
                {
                    model.TranBulidingPercent = decimal.Parse(ds.Tables[0].Rows[0]["TranBulidingPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UnitId"] != null && ds.Tables[0].Rows[0]["UnitId"].ToString() != "")
                {
                    model.UnitId = int.Parse(ds.Tables[0].Rows[0]["UnitId"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ActualAllountTime"] != null && ds.Tables[0].Rows[0]["ActualAllountTime"].ToString() != "")
                {
                    model.ActualAllountTime = ds.Tables[0].Rows[0]["ActualAllountTime"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ActualAllotCount"] != null && ds.Tables[0].Rows[0]["ActualAllotCount"].ToString() != "")
                {
                    model.ActualAllotCount = decimal.Parse(ds.Tables[0].Rows[0]["ActualAllotCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LoanValueCount"] != null && ds.Tables[0].Rows[0]["LoanValueCount"].ToString() != "")
                {
                    model.LoanValueCount = decimal.Parse(ds.Tables[0].Rows[0]["LoanValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BulidType"] != null && ds.Tables[0].Rows[0]["BulidType"].ToString() != "")
                {
                    model.BulidType = ds.Tables[0].Rows[0]["BulidType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FinanceValuePercent"] != null && ds.Tables[0].Rows[0]["FinanceValuePercent"].ToString() != "")
                {
                    model.FinanceValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["FinanceValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FinanceValueCount"] != null && ds.Tables[0].Rows[0]["FinanceValueCount"].ToString() != "")
                {
                    model.FinanceValueCount = decimal.Parse(ds.Tables[0].Rows[0]["FinanceValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TheDeptShouldValuePercent"] != null && ds.Tables[0].Rows[0]["TheDeptShouldValuePercent"].ToString() != "")
                {
                    model.TheDeptShouldValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["TheDeptShouldValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TheDeptShouldValueCount"] != null && ds.Tables[0].Rows[0]["TheDeptShouldValueCount"].ToString() != "")
                {
                    model.TheDeptShouldValueCount = decimal.Parse(ds.Tables[0].Rows[0]["TheDeptShouldValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DividedPercent"] != null && ds.Tables[0].Rows[0]["DividedPercent"].ToString() != "")
                {
                    model.DividedPercent = decimal.Parse(ds.Tables[0].Rows[0]["DividedPercent"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectValueAllot GetModelByProID(int? proID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from cm_ProjectValueAllot ");
            strSql.Append(" where pro_ID=@proID  And SecondValue='1'");

            strSql.Append(" order by AllotDate desc");
            SqlParameter[] parameters = {
					new SqlParameter("@proID", SqlDbType.Int,4)
			};
            parameters[0].Value = proID;

            TG.Model.cm_ProjectValueAllot model = new TG.Model.cm_ProjectValueAllot();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_ID"] != null && ds.Tables[0].Rows[0]["pro_ID"].ToString() != "")
                {
                    model.pro_ID = int.Parse(ds.Tables[0].Rows[0]["pro_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotTimes"] != null && ds.Tables[0].Rows[0]["AllotTimes"].ToString() != "")
                {
                    model.AllotTimes = ds.Tables[0].Rows[0]["AllotTimes"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AllotCount"] != null && ds.Tables[0].Rows[0]["AllotCount"].ToString() != "")
                {
                    model.AllotCount = decimal.Parse(ds.Tables[0].Rows[0]["AllotCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotDate"] != null && ds.Tables[0].Rows[0]["AllotDate"].ToString() != "")
                {
                    model.AllotDate = DateTime.Parse(ds.Tables[0].Rows[0]["AllotDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["persent"] != null && ds.Tables[0].Rows[0]["persent"].ToString() != "")
                {
                    model.persent = decimal.Parse(ds.Tables[0].Rows[0]["persent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PaidValuePercent"] != null && ds.Tables[0].Rows[0]["PaidValuePercent"].ToString() != "")
                {
                    model.PaidValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["PaidValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PaidValueCount"] != null && ds.Tables[0].Rows[0]["PaidValueCount"].ToString() != "")
                {
                    model.PaidValueCount = decimal.Parse(ds.Tables[0].Rows[0]["PaidValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DesignManagerPercent"] != null && ds.Tables[0].Rows[0]["DesignManagerPercent"].ToString() != "")
                {
                    model.DesignManagerPercent = decimal.Parse(ds.Tables[0].Rows[0]["DesignManagerPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DesignManagerCount"] != null && ds.Tables[0].Rows[0]["DesignManagerCount"].ToString() != "")
                {
                    model.DesignManagerCount = decimal.Parse(ds.Tables[0].Rows[0]["DesignManagerCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotValuePercent"] != null && ds.Tables[0].Rows[0]["AllotValuePercent"].ToString() != "")
                {
                    model.AllotValuePercent = ds.Tables[0].Rows[0]["AllotValuePercent"].ToString();
                }
                if (ds.Tables[0].Rows[0]["EconomyValuePercent"] != null && ds.Tables[0].Rows[0]["EconomyValuePercent"].ToString() != "")
                {
                    model.EconomyValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["EconomyValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["EconomyValueCount"] != null && ds.Tables[0].Rows[0]["EconomyValueCount"].ToString() != "")
                {
                    model.EconomyValueCount = decimal.Parse(ds.Tables[0].Rows[0]["EconomyValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UnitValuePercent"] != null && ds.Tables[0].Rows[0]["UnitValuePercent"].ToString() != "")
                {
                    model.UnitValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["UnitValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UnitValueCount"] != null && ds.Tables[0].Rows[0]["UnitValueCount"].ToString() != "")
                {
                    model.UnitValueCount = decimal.Parse(ds.Tables[0].Rows[0]["UnitValueCount"].ToString());
                }

                if (ds.Tables[0].Rows[0]["AllotUser"] != null && ds.Tables[0].Rows[0]["AllotUser"].ToString() != "")
                {
                    model.AllotUser = ds.Tables[0].Rows[0]["AllotUser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mark"] != null && ds.Tables[0].Rows[0]["mark"].ToString() != "")
                {
                    model.mark = ds.Tables[0].Rows[0]["mark"].ToString();
                }

                if (ds.Tables[0].Rows[0]["HavcPercent"] != null && ds.Tables[0].Rows[0]["HavcPercent"].ToString() != "")
                {
                    model.HavcValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["HavcPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HavcCount"] != null && ds.Tables[0].Rows[0]["HavcCount"].ToString() != "")
                {
                    model.HavcValueCount = decimal.Parse(ds.Tables[0].Rows[0]["HavcCount"].ToString());
                }

                if (ds.Tables[0].Rows[0]["ProgramPercent"] != null && ds.Tables[0].Rows[0]["ProgramPercent"].ToString() != "")
                {
                    model.ProgramPercent = decimal.Parse(ds.Tables[0].Rows[0]["ProgramPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProgramCount"] != null && ds.Tables[0].Rows[0]["ProgramCount"].ToString() != "")
                {
                    model.ProgramCount = decimal.Parse(ds.Tables[0].Rows[0]["ProgramCount"].ToString());
                }

                if (ds.Tables[0].Rows[0]["ShouldBeValuePercent"] != null && ds.Tables[0].Rows[0]["ShouldBeValuePercent"].ToString() != "")
                {
                    model.ShouldBeValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["ShouldBeValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ShouldBeValueCount"] != null && ds.Tables[0].Rows[0]["ShouldBeValueCount"].ToString() != "")
                {
                    model.ShouldBeValueCount = decimal.Parse(ds.Tables[0].Rows[0]["ShouldBeValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TheDeptValueCount"] != null && ds.Tables[0].Rows[0]["TheDeptValueCount"].ToString() != "")
                {
                    model.Thedeptallotcount = decimal.Parse(ds.Tables[0].Rows[0]["TheDeptValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TheDeptValuePercent"] != null && ds.Tables[0].Rows[0]["TheDeptValuePercent"].ToString() != "")
                {
                    model.Thedeptallotpercent = decimal.Parse(ds.Tables[0].Rows[0]["TheDeptValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ItemType"] != null && ds.Tables[0].Rows[0]["ItemType"].ToString() != "")
                {
                    model.Itemtype = int.Parse(ds.Tables[0].Rows[0]["ItemType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TranBulidingCount"] != null && ds.Tables[0].Rows[0]["TranBulidingCount"].ToString() != "")
                {
                    model.TranBulidingCount = decimal.Parse(ds.Tables[0].Rows[0]["TranBulidingCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TranBulidingPercent"] != null && ds.Tables[0].Rows[0]["TranBulidingPercent"].ToString() != "")
                {
                    model.TranBulidingPercent = decimal.Parse(ds.Tables[0].Rows[0]["TranBulidingPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FinanceValuePercent"] != null && ds.Tables[0].Rows[0]["FinanceValuePercent"].ToString() != "")
                {
                    model.FinanceValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["FinanceValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FinanceValueCount"] != null && ds.Tables[0].Rows[0]["FinanceValueCount"].ToString() != "")
                {
                    model.FinanceValueCount = decimal.Parse(ds.Tables[0].Rows[0]["FinanceValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TheDeptShouldValuePercent"] != null && ds.Tables[0].Rows[0]["TheDeptShouldValuePercent"].ToString() != "")
                {
                    model.TheDeptShouldValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["TheDeptShouldValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TheDeptShouldValueCount"] != null && ds.Tables[0].Rows[0]["TheDeptShouldValueCount"].ToString() != "")
                {
                    model.TheDeptShouldValueCount = decimal.Parse(ds.Tables[0].Rows[0]["TheDeptShouldValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DividedPercent"] != null && ds.Tables[0].Rows[0]["DividedPercent"].ToString() != "")
                {
                    model.DividedPercent = decimal.Parse(ds.Tables[0].Rows[0]["DividedPercent"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,pro_ID,AllotTimes,AllotCount,AllotDate,persent,PaidValuePercent,PaidValueCount,DesignManagerPercent,DesignManagerCount,AllotValuePercent,EconomyValuePercent,EconomyValueCount,UnitValuePercent,UnitValueCount,AllotUser,Status,mark ");
            strSql.Append(" FROM cm_ProjectValueAllot ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append("select ID,pro_ID,AllotTimes,AllotCount,AllotDate,persent,PaidValuePercent,PaidValueCount,DesignManagerPercent,DesignManagerCount,AllotValuePercent,EconomyValuePercent,EconomyValueCount,UnitValuePercent,UnitValueCount,AllotUser,Status,mark ");
            strSql.Append(" FROM cm_ProjectValueAllot ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_ProjectValueAllot ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectValueAllot GetModelByCoperationSysNo(int proSysNO)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" * ");
            strSql.Append(" from cm_ProjectValueAllot ");
            strSql.Append(" where pro_ID=" + proSysNO + " order by SysNo DESC");

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

            reader.Read();

            TG.Model.cm_ProjectValueAllot coperationAuditEntity = TG.Common.EntityBuilder.EntityBuilder<TG.Model.cm_ProjectValueAllot>.BuilderEntity(reader);

            reader.Close();

            return coperationAuditEntity;
        }
        /// <summary>
        /// 获取实收金额
        /// </summary>
        /// <param name="cprid">合同编码</param>
        /// <returns></returns>
        public DataSet GetPayShiAccount(int cprid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT SUM(payCount) as PayShiCount FROM  dbo.cm_RealCprChg  WHERE Status='S' And chg_id=@ID group by chg_id
                           SELECT SUM(AllotCount) AS TOTALCOUNT   FROM dbo.cm_ProjectValueAllot WHERE pro_ID=@ID group by pro_ID ");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = cprid;

            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 一次产值分配列表详细信息
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<ProjectValueAuditViewEntity> GetProjectValueRecordView(string whereSql, string year)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT
                            ALLOT.ID,
                            ALLOT.pro_ID,
                            ALLOT. AllotTimes,
                            ALLOT.AllotCount,
                            ALLOT.AllotDate,
                            ALLOT.persent,
                            ALLOT.PaidValuePercent,
                            RECORD.SysNo AS AuditSysNo,
                            RECORD.Status,
                             PaidValueCount,
                            ALLOT.DesignManagerPercent,
                             DesignManagerCount,
                           ALLOT. AllotValuePercent,
                           ALLOT. EconomyValuePercent,
                             EconomyValueCount,
                             ALLOT.UnitValuePercent,
                           ActualAllountTime,
                            UnitValueCount,
                            TheDeptValuePercent,
                             ProgramCount,
                            IsTAllPass,
                            isnull(BorrowValueCount,0) as BorrowValueCount,
                            isnull(ActualAllotCount,0) as ActualAllotCount,
                            isnull(LoanValueCount,0) as LoanValueCount,
                           (SELECT Convert(decimal(18,4),SUM(Acount)) AS PayShiCount  FROM cm_ProjectCharge WHERE  Status<>'B'  and cprID=CM.cpr_Id   and ((" + year + " is null OR (InAcountTime  between '" + year + "'+'-1-1' and '" + year + "'+'-12-31' )) )) as PayShiCount," + @"
                            isnull( P.ISTrunEconomy,0) as ISTrunEconomy,
                            isnull( P.ISHvac,0) as ISTrunHavc  
                             FROM dbo.cm_ProjectValueAllot ALLOT
                            INNER JOIN dbo.cm_ProjectValueAuditRecord RECORD
                             ON ALLOT.ID=RECORD.AllotID
                             INNER JOIN cm_Project P
                              ON ALLOT.pro_ID=P.pro_ID
                             INNER JOIN cm_Coperation CM
                             ON P.CoperationSysNo=CM.cpr_Id WHERE  ALLOT.SecondValue <> '2' and ALLOT.pro_ID=@id Order BY AllotDate"
                        );
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = whereSql;
            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString(), parameters);

            List<ProjectValueAuditViewEntity> resultList = EntityBuilder<ProjectValueAuditViewEntity>.BuilderEntityList(reader);

            return resultList;
        }

        /// <summary>
        /// 一次产值分配列表详细信息
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<ProjectValueAuditViewEntityByPro> GetProjectValueRecordViewByPro(string whereSql, string year)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT
                            ALLOT.ID,
                            ALLOT.pro_ID,
                            ALLOT. AllotTimes,
                            ALLOT.AllotCount,
                            ALLOT.AllotDate,
                            ALLOT.persent,
                            ALLOT.PaidValuePercent,
                            RECORD.SysNo AS AuditSysNo,
                            RECORD.Status,
                             PaidValueCount,
                            ALLOT.DesignManagerPercent,
                             DesignManagerCount,
                           ALLOT. AllotValuePercent,
                           ALLOT. EconomyValuePercent,
                             EconomyValueCount,
                             ALLOT.UnitValuePercent,
                           ActualAllountTime,
                            UnitValueCount,
                            TheDeptValuePercent,
                             ProgramCount,
                           (SELECT Convert(decimal(18,4),SUM(Acount)) AS PayShiCount  FROM cm_ProjectCharge WHERE  Status<>'B'  and cprID=CM.cpr_Id   and ((" + year + " is null OR (InAcountTime  between '" + year + "'+'-1-1' and '" + year + "'+'-12-31' )) )) as PayShiCount," + @"
                            isnull( P.ISTrunEconomy,0) as ISTrunEconomy,
                            isnull( P.ISHvac,0) as ISTrunHavc  
                             FROM dbo.cm_ProjectValueAllot ALLOT
                            INNER JOIN dbo.cm_ProjectValueAuditRecord RECORD
                             ON ALLOT.ID=RECORD.AllotID
                             INNER JOIN cm_Project P
                              ON ALLOT.pro_ID=P.pro_ID
                             INNER JOIN cm_Coperation CM
                             ON P.CoperationSysNo=CM.cpr_Id WHERE  ALLOT.SecondValue <> '2' and ALLOT.pro_ID=@id Order BY AllotDate"
                        );
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = whereSql;
            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString(), parameters);

            List<ProjectValueAuditViewEntityByPro> resultList = EntityBuilder<ProjectValueAuditViewEntityByPro>.BuilderEntityList(reader);

            return resultList;
        }

        /// <summary>
        /// 一次产值分配列表详细信息--暖通所项目
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<HavcProjectValueAuditViewEntity> GetHavcProjectValueRecordView(string whereSql, string year)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT
                            ALLOT.ID,
                            ALLOT.pro_ID,
                            ALLOT. AllotTimes,
                            ALLOT.AllotCount,
                            ALLOT.AllotDate,
                            ALLOT.persent,
                            ALLOT.PaidValuePercent,
                            RECORD.SysNo AS AuditSysNo,
                            RECORD.Status,
                             PaidValueCount,
                            ALLOT.DesignManagerPercent,
                             DesignManagerCount,
                           ALLOT. AllotValuePercent,
                           ALLOT. EconomyValuePercent,
                             EconomyValueCount,
                             ALLOT.UnitValuePercent,
                            UnitValueCount,
                            TheDeptValuePercent,
                            ActualAllountTime,
                             ProgramCount,
                            IsTAllPass,
                            isnull(BorrowValueCount,0) as BorrowValueCount,
                            isnull(ActualAllotCount,0) as ActualAllotCount,
                            isnull(LoanValueCount,0) as LoanValueCount,
                          (SELECT Convert(decimal(18,4),SUM(Acount)) AS PayShiCount  FROM cm_ProjectCharge WHERE  Status<>'B'  and cprID=CM.cpr_Id   and ((" + year + " is null OR (InAcountTime  between '" + year + "'+'-1-1' and '" + year + "'+'-12-31' )) )) as PayShiCount," + @"
                            isnull( P.ISTrunEconomy,0) as ISTrunEconomy,
                            isnull( P.ISHvac,0) as ISTrunHavc  
                             FROM dbo.cm_ProjectValueAllot ALLOT
                            INNER JOIN dbo.cm_ProjectValueAuditRecord RECORD
                             ON ALLOT.ID=RECORD.AllotID
                             INNER JOIN cm_Project P
                              ON ALLOT.pro_ID=P.pro_ID
                             INNER JOIN cm_Coperation CM
                             ON P.CoperationSysNo=CM.cpr_Id WHERE  ALLOT.SecondValue <> '2' and ALLOT.pro_ID=@id Order BY AllotDate"
                        );
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = whereSql;
            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString(), parameters);

            List<HavcProjectValueAuditViewEntity> resultList = EntityBuilder<HavcProjectValueAuditViewEntity>.BuilderEntityList(reader);

            return resultList;
        }
        public List<HavcProjectValueAuditViewEntityByPro> GetHavcProjectValueRecordViewByPro(string whereSql, string year)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT
                            ALLOT.ID,
                            ALLOT.pro_ID,
                            ALLOT. AllotTimes,
                            ALLOT.AllotCount,
                            ALLOT.AllotDate,
                            ALLOT.persent,
                            ALLOT.PaidValuePercent,
                            RECORD.SysNo AS AuditSysNo,
                            RECORD.Status,
                             PaidValueCount,
                            ALLOT.DesignManagerPercent,
                             DesignManagerCount,
                           ALLOT. AllotValuePercent,
                           ALLOT. EconomyValuePercent,
                             EconomyValueCount,
                             ALLOT.UnitValuePercent,
                            UnitValueCount,
                            TheDeptValuePercent,
                            ActualAllountTime,
                             ProgramCount,
                          (SELECT Convert(decimal(18,4),SUM(Acount)) AS PayShiCount  FROM cm_ProjectCharge WHERE  Status<>'B'  and cprID=CM.cpr_Id   and ((" + year + " is null OR (InAcountTime  between '" + year + "'+'-1-1' and '" + year + "'+'-12-31' )) )) as PayShiCount," + @"
                            isnull( P.ISTrunEconomy,0) as ISTrunEconomy,
                            isnull( P.ISHvac,0) as ISTrunHavc  
                             FROM dbo.cm_ProjectValueAllot ALLOT
                            INNER JOIN dbo.cm_ProjectValueAuditRecord RECORD
                             ON ALLOT.ID=RECORD.AllotID
                             INNER JOIN cm_Project P
                              ON ALLOT.pro_ID=P.pro_ID
                             INNER JOIN cm_Coperation CM
                             ON P.CoperationSysNo=CM.cpr_Id WHERE  ALLOT.SecondValue <> '2' and ALLOT.pro_ID=@id Order BY AllotDate"
                        );
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = whereSql;
            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString(), parameters);

            List<HavcProjectValueAuditViewEntityByPro> resultList = EntityBuilder<HavcProjectValueAuditViewEntityByPro>.BuilderEntityList(reader);

            return resultList;
        }

        /// <summary>
        /// 一次产值分配列表详细信息--经济所项目
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<JjsProjectValueAuditViewEntity> GetJjsProjectValueRecordView(string whereSql, string year)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT
                            ALLOT.ID,
                            ALLOT.pro_ID,
                            ALLOT. AllotTimes,
                            ALLOT.AllotCount,
                            ALLOT.AllotDate,
                            ALLOT.persent,
                            ALLOT.PaidValuePercent,
                            RECORD.SysNo AS AuditSysNo,
                            RECORD.Status,
                             PaidValueCount,
                            ALLOT.DesignManagerPercent,
                             DesignManagerCount,
                           ALLOT. AllotValuePercent,
                           ALLOT. EconomyValuePercent,
                             EconomyValueCount,
                             ALLOT.UnitValuePercent,
                            UnitValueCount,
                            TheDeptValuePercent,
                             ProgramPercent,
                            IsTAllPass,
                            ActualAllountTime,
                            isnull(BorrowValueCount,0) as BorrowValueCount,
                            isnull(ActualAllotCount,0) as ActualAllotCount,
                            isnull(LoanValueCount,0) as LoanValueCount,
                            (SELECT Convert(decimal(18,4),SUM(Acount)) AS PayShiCount  FROM cm_ProjectCharge WHERE  Status<>'B'  and cprID=CM.cpr_Id   and ((" + year + " is null OR (InAcountTime  between '" + year + "'+'-1-1' and '" + year + "'+'-12-31' )) )) as PayShiCount," + @"
                           case when RECORD.Status='A' then '待所长确认' when RECORD.Status='B' then '待生产经营部确认' when RECORD.Status='C' then '所长不通过' when RECORD.Status='D' then '审核全部通过' when RECORD.Status='E' then '经营部不通过'  end as StatusString
                             FROM dbo.cm_ProjectValueAllot ALLOT
                            INNER JOIN dbo.cm_ProjectValueAuditRecord RECORD
                             ON ALLOT.ID=RECORD.AllotID
                             INNER JOIN cm_Project P
                              ON ALLOT.pro_ID=P.pro_ID
                             INNER JOIN cm_Coperation CM
                             ON P.CoperationSysNo=CM.cpr_Id WHERE  ALLOT.SecondValue = '1' and ALLOT.pro_ID=@id Order BY AllotDate"
                        );
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = whereSql;
            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString(), parameters);

            List<JjsProjectValueAuditViewEntity> resultList = EntityBuilder<JjsProjectValueAuditViewEntity>.BuilderEntityList(reader);

            return resultList;
        }
        public List<JjsProjectValueAuditViewEntityByPro> GetJjsProjectValueRecordViewByPro(string whereSql, string year)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT
                            ALLOT.ID,
                            ALLOT.pro_ID,
                            ALLOT. AllotTimes,
                            ALLOT.AllotCount,
                            ALLOT.AllotDate,
                            ALLOT.persent,
                            ALLOT.PaidValuePercent,
                            RECORD.SysNo AS AuditSysNo,
                            RECORD.Status,
                             PaidValueCount,
                            ALLOT.DesignManagerPercent,
                             DesignManagerCount,
                           ALLOT. AllotValuePercent,
                           ALLOT. EconomyValuePercent,
                             EconomyValueCount,
                             ALLOT.UnitValuePercent,
                            UnitValueCount,
                            TheDeptValuePercent,
                             ProgramPercent,
                            (SELECT Convert(decimal(18,4),SUM(Acount)) AS PayShiCount  FROM cm_ProjectCharge WHERE  Status<>'B'  and cprID=CM.cpr_Id   and ((" + year + " is null OR (InAcountTime  between '" + year + "'+'-1-1' and '" + year + "'+'-12-31' )) )) as PayShiCount," + @"
                           case when RECORD.Status='A' then '待所长确认' when RECORD.Status='B' then '待生产经营部确认' when RECORD.Status='C' then '所长不通过' when RECORD.Status='D' then '审核全部通过' when RECORD.Status='E' then '经营部不通过'  end as StatusString
                             FROM dbo.cm_ProjectValueAllot ALLOT
                            INNER JOIN dbo.cm_ProjectValueAuditRecord RECORD
                             ON ALLOT.ID=RECORD.AllotID
                             INNER JOIN cm_Project P
                              ON ALLOT.pro_ID=P.pro_ID
                             INNER JOIN cm_Coperation CM
                             ON P.CoperationSysNo=CM.cpr_Id WHERE  ALLOT.SecondValue = '1' and ALLOT.pro_ID=@id Order BY AllotDate"
                        );
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = whereSql;
            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString(), parameters);

            List<JjsProjectValueAuditViewEntityByPro> resultList = EntityBuilder<JjsProjectValueAuditViewEntityByPro>.BuilderEntityList(reader);

            return resultList;
        }

        /// <summary>
        /// 查询经济所产值明细
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public DataSet GetjjsProjectValueRecordDetail(int proID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT
                             ALLOT.ID as AllotID,
                            ALLOT.pro_ID,
                            ALLOT. AllotTimes,
                            ALLOT.AllotCount,
                            ALLOT.AllotDate,
                            RECORD.SysNo AS AuditSysNo,
                            RECORD.Status,
                            ALLOT.DesignManagerPercent,
                            TheDeptValuePercent,
                            ProgramPercent,
                            ISNULL( TranBulidingPercent,0.00) as TranBulidingPercent,
                           (SELECT Convert(decimal(18,2),SUM(Acount)) AS PayShiCount  FROM cm_ProjectCharge WHERE  ( Status='C' or Status='E')  and cprID=CM.cpr_Id) as PayShiCount,
                           case when  RECORD.Status='A' then '经营部待审核' when RECORD.Status='D'   then '经营部不通过'  when RECORD.Status='S' then '经营部通过' end  StatusString
                             FROM dbo.cm_ProjectValueAllot ALLOT
                            INNER JOIN dbo.cm_ProjectValueAuditRecord RECORD
                             ON ALLOT.ID=RECORD.AllotID
                             INNER JOIN cm_Project P
                              ON ALLOT.pro_ID=P.pro_ID
                             INNER JOIN cm_Coperation CM
                             ON P.CoperationSysNo=CM.cpr_Id  WHERE  ALLOT.pro_ID=@id Order BY AllotDate"
                        );
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = proID;

            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 二次产值分配列表详细信息
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<ProjectSecondValueAuditViewEntity> GetProjectSecondValueRecordView(string whereSql)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@" SELECT
                            ALLOT.ID,
                            ALLOT.pro_ID,
                            ALLOT. AllotTimes,
                            ALLOT.AllotCount,
                            ALLOT.AllotDate,
                            RECORD.SysNo AS AuditSysNo,
                            RECORD.Status  ,
                            ISNULL(SECOND.AuditCount,0)AuditCount,
                            ISNULL(SECOND.DesignCount,0)DesignCount
                             FROM dbo.cm_ProjectValueAllot ALLOT
                            INNER JOIN dbo.cm_ProjectValueAuditRecord RECORD
                             ON ALLOT.ID=RECORD.AllotID
                             INNER JOIN cm_ProjectSecondValueAllot SECOND
                             ON SECOND.AllotID=ALLOT.ID
                              WHERE  ALLOT.SecondValue='2' and ALLOT.pro_ID=@id Order BY AllotDate"
                        );
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = whereSql;
            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString(), parameters);

            List<ProjectSecondValueAuditViewEntity> resultList = EntityBuilder<ProjectSecondValueAuditViewEntity>.BuilderEntityList(reader);

            return resultList;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_Coperation GetCprModel(int cpr_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" cpr_Id,pro_ID,cpr_No,cpr_Type,cpr_Type2,cpr_Name,cpr_Unit,cpr_Acount,cpr_ShijiAcount,cpr_Touzi,cpr_ShijiTouzi,cpr_Process,cpr_SignDate,cpr_DoneDate,cpr_Mark,BuildArea,ChgPeople,ChgPhone,ChgJia,ChgJiaPhone,BuildPosition,Industry,BuildUnit,BuildSrc,TableMaker,RegTime,UpdateBy,LastUpdate,BuildType,StructType,Floor,BuildStructType,MultiBuild,JieGou,GeiPs,NuanT,DianQ ");
            strSql.Append(" from cm_Coperation ");
            strSql.Append(" where cpr_Id=" + cpr_Id + "");
            TG.Model.cm_Coperation model = new TG.Model.cm_Coperation();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["cpr_Id"] != null && ds.Tables[0].Rows[0]["cpr_Id"].ToString() != "")
                {
                    model.cpr_Id = int.Parse(ds.Tables[0].Rows[0]["cpr_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_Name"] != null && ds.Tables[0].Rows[0]["cpr_Name"].ToString() != "")
                {
                    model.cpr_Name = ds.Tables[0].Rows[0]["cpr_Name"].ToString();
                }

                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到 是否转经济所
        /// </summary>
        /// <param name="cprid"></param>
        /// <returns></returns>
        public string GetIsTrunEconomy(int cprid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT cpr_Id,ISTrunEconomy
                             FROM dbo.cm_Coperation AS CM INNER JOIN dbo.cm_Project PJ
                             ON CM.cpr_Id=PJ.CoperationSysNo  where CM.cpr_Id=@id "
                        );
            SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
            parameters[0].Value = cprid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0]["ISTrunEconomy"].ToString();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到当前年份实收的金额
        /// </summary>
        /// <param name="year"></param>
        /// <param name="cprId"></param>
        /// <returns></returns>
        public decimal GetPayShiValueCurrent(string year, int cprId)
        {
            string sql = "";
            if (year == "-1")
            {

                sql = string.Format("select isnull( Convert(decimal(18,2),SUM(Acount)),0) from cm_ProjectCharge  where Status<>'B' AND cprID={0} group by cprID", cprId);
            }
            else
            {
                sql = string.Format("select isnull( Convert(decimal(18,2),SUM(Acount)),0) from cm_ProjectCharge  where Status<>'B' AND cprID={0} and InAcountTime  between '{1}-01-1' and '{1}-12-31'group by cprID", cprId, year);
            }
            DataSet ds = DbHelperSQL.Query(sql);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return decimal.Parse(ds.Tables[0].Rows[0][0].ToString());
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 得到分配金额
        /// </summary>
        /// <param name="year"></param>
        /// <param name="pro_id"></param>
        /// <returns></returns>
        public decimal GetAllotCount(string year, int pro_id)
        {
            string sql = "";
            if (year == "-1")
            {

                sql = string.Format("SELECT ISNULL(SUM(AllotCount),0.00) AS AllotCount  FROM dbo.cm_ProjectValueAllot  where Status<>'D' and SecondValue='1' and pro_ID={0}  ", pro_id);
            }
            else
            {
                sql = string.Format("SELECT ISNULL(SUM(AllotCount),0.00) AS AllotCount  FROM dbo.cm_ProjectValueAllot  where Status<>'D' and SecondValue='1' AND ActualAllountTime ='{1}' and pro_ID={0}  ", pro_id, year);
            }
            DataSet ds = DbHelperSQL.Query(sql);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return decimal.Parse(ds.Tables[0].Rows[0][0].ToString());
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// 得到二次产值分配金额
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitName"></param>
        /// <returns></returns>
        public DataSet GetSecondValueCurrent(string year, string unitName)
        {
            string sql = "";

            string tranEql = "";
            if (unitName.Contains("暖通"))
            {
                if (year == "-1" || string.IsNullOrEmpty(year))
                {
                    tranEql = @"select ISNULL( SUM( c.Thedeptallotcount),0)  as TranAllotCount
                            from cm_ProjectValueAllot a					
							left join   cm_TranProjectValueAllot c
							on c.Pro_ID=a.pro_ID and
							c.AllotID=a.Id
							where a.status='S'
							and c.Status='S'
                            ";
                }
                else
                {
                    tranEql = @"select ISNULL( SUM( c.Thedeptallotcount),0)  as TranAllotCount
                            from cm_ProjectValueAllot a					
							left join   cm_TranProjectValueAllot c
							on c.Pro_ID=a.pro_ID and
							c.AllotID=a.Id
							where a.status ='S'
							and c.Status ='S'
							and c.ActualAllountTime='" + year + "'  ";
                }
            }
            else if (unitName.Contains("经济"))
            {

                if (year == "-1" || string.IsNullOrEmpty(year))
                {
                    tranEql = @"select ISNULL( SUM( c.Thedeptallotcount),0)  as TranAllotCount
                            from cm_ProjectValueAllot a					
							left join   cm_TranjjsProjectValueAllot c
							on c.Pro_ID=a.pro_ID and
							c.AllotID=a.Id
							where a.status ='S'
							and c.Status!='D' ";
                }
                else
                {
                    tranEql = @"select ISNULL( SUM( c.Thedeptallotcount),0)  as TranAllotCount
                            from cm_ProjectValueAllot a					
							left join   cm_TranjjsProjectValueAllot c
							on c.Pro_ID=a.pro_ID and
							c.AllotID=a.Id
							where a.status='S'
							and c.Status!='D'
							and c.ActualAllountTime='" + year + "'";
                }
            }
            else
            {
                if (year == "-1" || string.IsNullOrEmpty(year))
                {
                    tranEql = @" 
                                                                              
                                  select    isnull(
                                     (
                                  
                                      select ISNULL( SUM( c.TranCount),0)  as TranAllotCount
                                       from cm_ProjectValueAllot a	
                                       inner join cm_Project e
                                       on e.pro_ID=a.pro_ID				
									   inner join  tg_unit b
									    on e.Unit=b.unit_Name
										left join   cm_TranProjectValueAllot c
										on c.Pro_ID=a.pro_ID and
										c.AllotID=a.Id
										where a.status='S'
										and c.Status!='D'
										 and b.unit_Name like '%" + unitName + "%'" + @"
										
									   )
										 
										+
										(
                                       select ISNULL( SUM( c.Thedeptallotcount),0)  as TranAllotCount
                                       from cm_ProjectValueAllot a					
									   inner join  tg_unit b
									    on a.UnitId=b.unit_ID
										left join   cm_TranBulidingProjectValueAllot c
										on c.Pro_ID=a.pro_ID and
										c.AllotID=a.Id
										where a.status='S'
										and c.Status!='D'
										 and b.unit_Name like '%" + unitName + "%'" + @"
										 ),0)";
                }
                else
                {
                    tranEql = @" 
                                  select    isnull(
                                     (
                                  
                                      select ISNULL( SUM( c.TranCount),0)  as TranAllotCount
                                       from cm_ProjectValueAllot a	
                                       inner join cm_Project e
                                       on e.pro_ID=a.pro_ID				
									   inner join  tg_unit b
									    on e.Unit=b.unit_Name
										left join   cm_TranProjectValueAllot c
										on c.Pro_ID=a.pro_ID and
										c.AllotID=a.Id
										where a.status='S'
										and c.Status!='D'
										 and c.ActualAllountTime='" + year + "' and b.unit_Name like '%" + unitName + "%'" + @"
										
									   )
										 
										+
										(
                                       select ISNULL( SUM( c.Thedeptallotcount),0)  as TranAllotCount
                                       from cm_ProjectValueAllot a					
									   inner join  tg_unit b
									    on a.UnitId=b.unit_ID
										left join   cm_TranBulidingProjectValueAllot c
										on c.Pro_ID=a.pro_ID and
										c.AllotID=a.Id
										where a.status='S'
										and c.Status!='D'
										 and c.ActualAllountTime='" + year + "' and b.unit_Name like '%" + unitName + "%'" + @"
										 ),0)
                           ";
                }
            }
            if (year == "-1" || string.IsNullOrEmpty(year))
            {
                sql = string.Format(@"	Select 
		                            isnull(sum((TheDeptValueCount+AuditCount+DesignCount+LoanValueCount)),0)totalCount,--总产值
		                            isNULL((SELECT SUM(AllotCount)FROM dbo.cm_ProjectValueAllot a
		                             inner join cm_Project p
		                            on a.pro_ID=p.pro_ID
		                            and p.Unit='{0}' 
                                     where
                                    Status <>'D' and SecondValue='2'   
		                            ),0)as allountCount,
		                            (select isnull(SUM(ComValue),0) 
                                     from cm_CompensationSet  where 1=1
                                     and UnitId=(select unit_id from tg_unit where unit_name='{0}') 
                                     ) as ProgramCount,  --方案补贴
                                     ({1}) as TranAllotCount
		                            From
    	                            (select a.ProID,
		                            isnull(SUM(TheDeptValueCount),0) as TheDeptValueCount ,
		                            isnull(SUM(AuditCount),0) as AuditCount,
		                            isnull(SUM(DesignCount),0) as DesignCount,
		                            isnull(SUM(HavcCount),0) as HavcCount,
		                            0 as LoanValueCount
		                            from cm_ProjectSecondValueAllot a
		                            inner join cm_Project p
		                            on a.ProID=p.pro_ID
		                            and p.Unit='{0}' 
	                                where Status='S' 
		                            group by ProID)
		                            second		 
                                   where 
                                    1=1 ", unitName, tranEql);
            }
            else
            {

                sql = string.Format(@"	Select 
		                           isnull(sum((TheDeptValueCount+AuditCount+DesignCount+LoanValueCount)),0)totalCount,--总产值
		                           isNULL((SELECT SUM(AllotCount)FROM dbo.cm_ProjectValueAllot a
		                           inner join cm_Project p
		                            on a.pro_ID=p.pro_ID
		                            and p.Unit='{0}' 
                                   where Status <>'D' and SecondValue='2'  
		                           AND ActualAllountTime='{1}'
		                            ),0)as allountCount,
		                            (select isnull(SUM(ComValue),0) 
                                     from cm_CompensationSet  where ValueYear='{1}' 
                                     and UnitId=(select unit_id from tg_unit where unit_name='{0}')
                                     ) as ProgramCount,  --方案补贴
                                     ({2}) as TranAllotCount --转其他所产值
		                            From
    	                            (select a.ProID,
		                            isnull(SUM(TheDeptValueCount),0) as TheDeptValueCount ,
		                            isnull(SUM(AuditCount),0) as AuditCount,
		                            isnull(SUM(DesignCount),0) as DesignCount,
		                            isnull(SUM(HavcCount),0) as HavcCount,
		                            0 as LoanValueCount
		                            from cm_ProjectSecondValueAllot a
		                            inner join cm_Project p
		                            on a.ProID=p.pro_ID
		                            and p.Unit='{0}' 
	                                where Status='S'  AND ActualAllountTime='{1}'
		                            group by a.ProID)
		                            second		 
                                   where 
                                   1=1 ", unitName, year, tranEql);
            }

            return DbHelperSQL.Query(sql);
        }


        /// <summary>
        /// 删除项目产值
        /// </summary>
        /// <param name="proid"></param>
        /// <param name="allotId"></param>
        /// <param name="auditID"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public int DeleteProjectValue(string proid, string allotId, string auditID, string type)
        {

            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@pro_id", SqlDbType.Int,4),
					 new SqlParameter("@allot_id", SqlDbType.Int,4),
                     new SqlParameter("@audit_id", SqlDbType.Int,4),
                     new SqlParameter("@type", SqlDbType.NVarChar,10),
			};
                parameters[0].Value = proid;
                parameters[1].Value = allotId;
                parameters[2].Value = auditID;
                parameters[3].Value = type;
                DbHelperSQL.RunProcedure("P_cm_DelelteProjectValue", parameters);
                return 1;
            }
            catch (System.Exception ex)
            {
                return 0;
            }

        }

        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public int UpdateProjectValueTStatus(int allotID)
        {
            string sql = "update  cm_ProjectValueAllot set TStatus='S' where ID=" + allotID + "";

            return DbHelperSQL.ExecuteSql(sql);
        }

        /// <summary>
        /// 借出。借入项目表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataSet GetProjectValueLendBorrowInfo(string strWhere, string year)
        {
            string sql = @" SELECT P.*,
				             isnull(LoanValueCount,0) as LoanValueCount,
				             isnull(BorrowValueCount,0) as BorrowValueCount
				             FROM cm_Project P
				             inner  JOIN 
			                 (
			                 select  pro_ID,isnull(SUM(LoanValueCount),0) as LoanValueCount,ISNULL( SUM(BorrowValueCount),0) as BorrowValueCount  
			                 FROM dbo.cm_ProjectValueAllot  where Status='S' and SecondValue='1'
			                 AND (  (" + year + " is null)  OR (ActualAllountTime ='" + year + "'  )) " + @"
			                 group by pro_ID
			                 ) allot
			                 ON ALLOT.pro_ID=P.pro_ID

		    	             where 1=1  " + strWhere + "  order by p.pro_id desc";

            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 项目所留产值统计表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataSet GetProjectValueRetentionStatisticalInfo(string strWhere, string year)
        {
            string sql = @" SELECT P.*,ISNULL(CG.PayShiCount,0.00)AS PayShiCount , 
				 CONVERT(DECIMAL(18,4), ISNULL(ALLOT.AllotCount,0.00))*10000 AS AllotCount ,
				 ALLOT.TheDeptValueCount,
				 second.AuditCount,
				 second.DesignCount
				  FROM cm_Project P
				 left JOIN cm_Coperation CM
				 ON P.CoperationSysNo=CM.cpr_Id
				 left  JOIN 
			     ( SELECT  distinct(cprID) as cpr_Id  ,ISNULL(SUM(Acount),0.00) as PayShiCount from cm_ProjectCharge
			     where Status<>'B' AND (  (" + year + " is null)  OR (InAcountTime  between' " + year + "-1-1' and '" + year + "-12-31' ) ) " + @"
			     GROUP BY cprID) CG
			     ON Cm.cpr_Id=CG.cpr_Id
			     inner JOIN
			     (
			     SELECT pro_ID , ISNULL(SUM(AllotCount),0.0000) AS AllotCount, ISNULL(SUM(ActualAllotCount),0.00) AS ActualAllotCount , ISNULL(SUM(TheDeptValueCount),0.00) AS TheDeptValueCount
			     FROM 
			     (
			     SELECT pro_ID,ISNULL(SUM(isnull(CAST( HavcCount as decimal(10,4)),0.0000)+isnull(CAST(EconomyValueCount as decimal(10,4)),0.00)),0.00)  AS AllotCount,0 as  ActualAllotCount ,0 as TheDeptValueCount FROM dbo.cm_ProjectValueAllot  where TStatus='S' AND Status='D' and SecondValue='1'
			     AND (  (" + @year + " is null)  OR (ActualAllountTime ='" + @year + "'  ))" + @"
			     GROUP BY pro_ID  
			     UNION ALL
			     SELECT pro_ID,ISNULL(SUM(AllotCount),0.0000) AS AllotCount , ISNULL(SUM(ActualAllotCount),0.00) AS ActualAllotCount ,ISNULL(SUM(TheDeptValueCount),0.00) AS TheDeptValueCount FROM dbo.cm_ProjectValueAllot  where Status<>'D'  and SecondValue='1'
			     AND (  (" + @year + " is null)  OR (ActualAllountTime ='" + @year + "'  ))" + @"
			     GROUP BY pro_ID 
			    
			     ) TEST GROUP BY pro_ID 
			     ) AS ALLOT
			     ON ALLOT.pro_ID=P.pro_ID
			    left join 
				(select ProID,
				
				isnull(SUM(AuditCount),0) as AuditCount,
				isnull(SUM(DesignCount),0) as DesignCount,
				isnull(SUM(HavcCount),0) as HavcCount
			
			    from cm_ProjectSecondValueAllot
				 where Status<>'D'   AND (  (" + @year + " is null)  OR (ActualAllountTime ='" + @year + "'  )) " + @"
				group by ProID)second
			    on p.pro_ID=second.ProID

		        where 1=1  " + strWhere + "  order by p.pro_id desc";

            return DbHelperSQL.Query(sql);
        }

        public DataSet GetProjectAuditAndDesignerAllot(string strWhere, string year, string allottype)
        {
            string strSql = string.Format(@"SELECT [ID]
                                      ,p.pro_name
                                      ,p.Cpr_Acount
                                      ,p.Unit
                                      ,[UserName]
                                      ,[AllotPrt]
                                      ,[AllotMoney]
                                      ,[AllotType]
                                      ,[AllotYear]
                                      ,(Select spe_Name From dbo.tg_speciality Where spe_ID=A.SpeID) AS SpeName
                                  FROM cm_AuditDesignCoefficient A left join cm_Project p ON A.ProID=p.pro_ID
                                  Where 1=1 {0} AND AllotYear='{1}' AND AllotType='{2}' order by A.ID DESC ", strWhere, year, allottype);

            return DbHelperSQL.Query(strSql);
        }
        /// <summary>
        /// 借出。借入项目表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataSet GetUnitValueLendBorrowInfo(string strWhere, string year)
        {
            string sql = @"   SELECT *,  
			              isnull((
			               select sum(LoanValueCount) from dbo.cm_ProjectValueAllot  where Status='S' and SecondValue='1'
			                AND (  (" + year + " is null)  OR (ActualAllountTime ='" + year + "'  )) " + @"
			                and pro_id in (select pro_id from cm_project where 1=1  and  unit=u.unit_name )
			               ),0) LoanValueCount,
			               isnull( (
			               select sum(BorrowValueCount) from dbo.cm_ProjectValueAllot  where Status='S' and SecondValue='1'
			                AND (  (" + year + " is null)  OR (ActualAllountTime ='" + year + "'  )) " + @"
			                and pro_id in (select pro_id from cm_project where 1=1 and  unit=u.unit_name )
			               ) ,0) BorrowValueCount
				         from tg_unit  u
			   
		    	         where 1=1 " + strWhere;

            return DbHelperSQL.Query(sql);
        }
        #endregion  Method
    }
}
