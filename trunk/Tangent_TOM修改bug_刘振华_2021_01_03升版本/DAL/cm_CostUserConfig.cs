﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_CostUserConfig
	/// </summary>
	public partial class cm_CostUserConfig
	{
		public cm_CostUserConfig()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_CostUserConfig model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.DisgnUserPrt != null)
			{
				strSql1.Append("DisgnUserPrt,");
				strSql2.Append(""+model.DisgnUserPrt+",");
			}
			if (model.AuditDPrt != null)
			{
				strSql1.Append("AuditDPrt,");
				strSql2.Append(""+model.AuditDPrt+",");
			}
			if (model.SpeRegPrt != null)
			{
				strSql1.Append("SpeRegPrt,");
				strSql2.Append(""+model.SpeRegPrt+",");
			}
			if (model.AuditPrt != null)
			{
				strSql1.Append("AuditPrt,");
				strSql2.Append(""+model.AuditPrt+",");
			}
			if (model.ExamPrt != null)
			{
				strSql1.Append("ExamPrt,");
				strSql2.Append(""+model.ExamPrt+",");
			}
			if (model.Used != null)
			{
				strSql1.Append("Used,");
				strSql2.Append(""+model.Used+",");
			}
			strSql.Append("insert into cm_CostUserConfig(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_CostUserConfig model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_CostUserConfig set ");
			if (model.DisgnUserPrt != null)
			{
				strSql.Append("DisgnUserPrt="+model.DisgnUserPrt+",");
			}
			else
			{
				strSql.Append("DisgnUserPrt= null ,");
			}
			if (model.AuditDPrt != null)
			{
				strSql.Append("AuditDPrt="+model.AuditDPrt+",");
			}
			else
			{
				strSql.Append("AuditDPrt= null ,");
			}
			if (model.SpeRegPrt != null)
			{
				strSql.Append("SpeRegPrt="+model.SpeRegPrt+",");
			}
			else
			{
				strSql.Append("SpeRegPrt= null ,");
			}
			if (model.AuditPrt != null)
			{
				strSql.Append("AuditPrt="+model.AuditPrt+",");
			}
			else
			{
				strSql.Append("AuditPrt= null ,");
			}
			if (model.ExamPrt != null)
			{
				strSql.Append("ExamPrt="+model.ExamPrt+",");
			}
			else
			{
				strSql.Append("ExamPrt= null ,");
			}
			if (model.Used != null)
			{
				strSql.Append("Used="+model.Used+",");
			}
			else
			{
				strSql.Append("Used= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ID="+ model.ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_CostUserConfig ");
			strSql.Append(" where ID="+ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_CostUserConfig ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_CostUserConfig GetModel(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" ID,DisgnUserPrt,AuditDPrt,SpeRegPrt,AuditPrt,ExamPrt,Used ");
			strSql.Append(" from cm_CostUserConfig ");
			strSql.Append(" where ID="+ID+"" );
			TG.Model.cm_CostUserConfig model=new TG.Model.cm_CostUserConfig();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["ID"]!=null && ds.Tables[0].Rows[0]["ID"].ToString()!="")
				{
					model.ID=int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DisgnUserPrt"]!=null && ds.Tables[0].Rows[0]["DisgnUserPrt"].ToString()!="")
				{
					model.DisgnUserPrt=decimal.Parse(ds.Tables[0].Rows[0]["DisgnUserPrt"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AuditDPrt"]!=null && ds.Tables[0].Rows[0]["AuditDPrt"].ToString()!="")
				{
					model.AuditDPrt=decimal.Parse(ds.Tables[0].Rows[0]["AuditDPrt"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SpeRegPrt"]!=null && ds.Tables[0].Rows[0]["SpeRegPrt"].ToString()!="")
				{
					model.SpeRegPrt=decimal.Parse(ds.Tables[0].Rows[0]["SpeRegPrt"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AuditPrt"]!=null && ds.Tables[0].Rows[0]["AuditPrt"].ToString()!="")
				{
					model.AuditPrt=decimal.Parse(ds.Tables[0].Rows[0]["AuditPrt"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ExamPrt"]!=null && ds.Tables[0].Rows[0]["ExamPrt"].ToString()!="")
				{
					model.ExamPrt=decimal.Parse(ds.Tables[0].Rows[0]["ExamPrt"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Used"]!=null && ds.Tables[0].Rows[0]["Used"].ToString()!="")
				{
					model.Used=int.Parse(ds.Tables[0].Rows[0]["Used"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,DisgnUserPrt,AuditDPrt,SpeRegPrt,AuditPrt,ExamPrt,Used ");
			strSql.Append(" FROM cm_CostUserConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,DisgnUserPrt,AuditDPrt,SpeRegPrt,AuditPrt,ExamPrt,Used ");
			strSql.Append(" FROM cm_CostUserConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_CostUserConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_CostUserConfig T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetCostUserConfigListByPager(int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,DisgnUserPrt,AuditDPrt,SpeRegPrt,AuditPrt,ExamPrt,Used ");
            strSql.Append(" FROM cm_CostUserConfig ");

            return DbHelperSQL.Query(strSql.ToString(), startIndex, endIndex);
        }
		#endregion  Method
	}
}

