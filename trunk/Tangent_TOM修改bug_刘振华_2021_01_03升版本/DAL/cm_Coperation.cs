﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using System.Collections.Generic;
using System.Collections;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_Coperation
    /// </summary>
    public partial class cm_Coperation
    {
        public cm_Coperation()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("cpr_Id", "cm_Coperation");
        }


        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int cpr_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_Coperation");
            strSql.Append(" where cpr_Id=" + cpr_Id + " ");
            return DbHelperSQL.Exists(strSql.ToString());
        }
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string cprName,string sysno)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_Coperation");
            strSql.Append(" where cpr_Name=N'" + cprName.Trim() + "' and cpr_ID<>" + sysno + "");
            return DbHelperSQL.Exists(strSql.ToString());
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_Coperation model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.cst_Id != null)
            {
                strSql1.Append("cst_Id,");
                strSql2.Append("" + model.cst_Id + ",");
            }
            if (model.cpr_No != null)
            {
                strSql1.Append("cpr_No,");
                strSql2.Append("'" + model.cpr_No + "',");
            }
            if (model.cpr_Type != null)
            {
                strSql1.Append("cpr_Type,");
                strSql2.Append("'" + model.cpr_Type + "',");
            }
            if (model.cpr_Type2 != null)
            {
                strSql1.Append("cpr_Type2,");
                strSql2.Append("'" + model.cpr_Type2 + "',");
            }
            if (model.cpr_Name != null)
            {
                strSql1.Append("cpr_Name,");
                strSql2.Append("'" + model.cpr_Name + "',");
            }
            if (model.cpr_Unit != null)
            {
                strSql1.Append("cpr_Unit,");
                strSql2.Append("'" + model.cpr_Unit + "',");
            }
            if (model.cpr_Acount != null)
            {
                strSql1.Append("cpr_Acount,");
                strSql2.Append("" + model.cpr_Acount + ",");
            }
            if (model.cpr_ShijiAcount != null)
            {
                strSql1.Append("cpr_ShijiAcount,");
                strSql2.Append("" + model.cpr_ShijiAcount + ",");
            }
            if (model.cpr_Touzi != null)
            {
                strSql1.Append("cpr_Touzi,");
                strSql2.Append("" + model.cpr_Touzi + ",");
            }
            if (model.cpr_ShijiTouzi != null)
            {
                strSql1.Append("cpr_ShijiTouzi,");
                strSql2.Append("" + model.cpr_ShijiTouzi + ",");
            }
            if (model.cpr_Process != null)
            {
                strSql1.Append("cpr_Process,");
                strSql2.Append("'" + model.cpr_Process + "',");
            }
            if (model.cpr_SignDate != null)
            {
                strSql1.Append("cpr_SignDate,");
                strSql2.Append("'" + model.cpr_SignDate + "',");
            }
            if (model.cpr_SignDate2 != null)
            {
                strSql1.Append("cpr_SignDate2,");
                strSql2.Append("'" + model.cpr_SignDate2 + "',");
            }
            if (model.cpr_DoneDate != null)
            {
                strSql1.Append("cpr_DoneDate,");
                strSql2.Append("'" + model.cpr_DoneDate + "',");
            }
            if (model.cpr_Mark != null)
            {
                strSql1.Append("cpr_Mark,");
                strSql2.Append("'" + model.cpr_Mark + "',");
            }
            if (model.BuildArea != null)
            {
                strSql1.Append("BuildArea,");
                strSql2.Append("'" + model.BuildArea + "',");
            }
            if (model.ChgPeople != null)
            {
                strSql1.Append("ChgPeople,");
                strSql2.Append("'" + model.ChgPeople + "',");
            }
            if (model.ChgPhone != null)
            {
                strSql1.Append("ChgPhone,");
                strSql2.Append("'" + model.ChgPhone + "',");
            }
            if (model.ChgJia != null)
            {
                strSql1.Append("ChgJia,");
                strSql2.Append("'" + model.ChgJia + "',");
            }
            if (model.ChgJiaPhone != null)
            {
                strSql1.Append("ChgJiaPhone,");
                strSql2.Append("'" + model.ChgJiaPhone + "',");
            }
            if (model.BuildPosition != null)
            {
                strSql1.Append("BuildPosition,");
                strSql2.Append("'" + model.BuildPosition + "',");
            }
            if (model.Industry != null)
            {
                strSql1.Append("Industry,");
                strSql2.Append("'" + model.Industry + "',");
            }
            if (model.BuildUnit != null)
            {
                strSql1.Append("BuildUnit,");
                strSql2.Append("'" + model.BuildUnit + "',");
            }
            if (model.BuildSrc != null)
            {
                strSql1.Append("BuildSrc,");
                strSql2.Append("'" + model.BuildSrc + "',");
            }
            if (model.TableMaker != null)
            {
                strSql1.Append("TableMaker,");
                strSql2.Append("'" + model.TableMaker + "',");
            }
            if (model.RegTime != null)
            {
                strSql1.Append("RegTime,");
                strSql2.Append("'" + model.RegTime + "',");
            }
            if (model.UpdateBy != null)
            {
                strSql1.Append("UpdateBy,");
                strSql2.Append("'" + model.UpdateBy + "',");
            }
            if (model.LastUpdate != null)
            {
                strSql1.Append("LastUpdate,");
                strSql2.Append("'" + model.LastUpdate + "',");
            }
            if (model.BuildType != null)
            {
                strSql1.Append("BuildType,");
                strSql2.Append("'" + model.BuildType + "',");
            }
            if (model.StructType != null)
            {
                strSql1.Append("StructType,");
                strSql2.Append("'" + model.StructType + "',");
            }
            if (model.Floor != null)
            {
                strSql1.Append("Floor,");
                strSql2.Append("'" + model.Floor + "',");
            }
            if (model.BuildStructType != null)
            {
                strSql1.Append("BuildStructType,");
                strSql2.Append("'" + model.BuildStructType + "',");
            }
            if (model.MultiBuild != null)
            {
                strSql1.Append("MultiBuild,");
                strSql2.Append("'" + model.MultiBuild + "',");
            }

            if (model.JieGou != null)
            {
                strSql1.Append("JieGou,");
                strSql2.Append("'" + model.JieGou + "',");
            }

            if (model.Geips != null)
            {
                strSql1.Append("Geips,");
                strSql2.Append("'" + model.Geips + "',");
            }

            if (model.Nuant != null)
            {
                strSql1.Append("Nuant,");
                strSql2.Append("'" + model.Nuant + "',");
            }
            if (model.Dianq != null)
            {
                strSql1.Append("Dianq,");
                strSql2.Append("'" + model.Dianq + "',");
            }
            //by long  20130517
            if (model.InsertUserID != null)
            {
                strSql1.Append("InsertUserID,");
                strSql2.Append("" + model.InsertUserID + ",");
            }
            if (model.InsertDate != null)
            {
                strSql1.Append("InsertDate,");
                strSql2.Append("'" + model.InsertDate + "',");
            }

            //添加项目经理ID  qpl 20131225
            if (model.PMUserID != null)
            {
                strSql1.Append("PMUserID,");
                strSql2.Append("" + model.PMUserID + ",");
            }
          
            strSql.Append("insert into cm_Coperation(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_Coperation model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_Coperation set ");
            if (model.cst_Id != null)
            {
                strSql.Append("cst_Id=" + model.cst_Id + ",");
            }
            else
            {
                strSql.Append("cst_Id= null ,");
            }
            if (model.cpr_No != null)
            {
                strSql.Append("cpr_No='" + model.cpr_No + "',");
            }
            else
            {
                strSql.Append("cpr_No= null ,");
            }
            if (model.cpr_Type != null)
            {
                strSql.Append("cpr_Type='" + model.cpr_Type + "',");
            }
            else
            {
                strSql.Append("cpr_Type= null ,");
            }
            if (model.cpr_Type2 != null)
            {
                strSql.Append("cpr_Type2='" + model.cpr_Type2 + "',");
            }
            else
            {
                strSql.Append("cpr_Type2= null ,");
            }
            if (model.cpr_Name != null)
            {
                strSql.Append("cpr_Name='" + model.cpr_Name + "',");
            }
            else
            {
                strSql.Append("cpr_Name= null ,");
            }
            if (model.cpr_Unit != null)
            {
                strSql.Append("cpr_Unit='" + model.cpr_Unit + "',");
            }
            else
            {
                strSql.Append("cpr_Unit= null ,");
            }
            if (model.cpr_Acount != null)
            {
                strSql.Append("cpr_Acount=" + model.cpr_Acount + ",");
            }
            else
            {
                strSql.Append("cpr_Acount= null ,");
            }
            if (model.cpr_ShijiAcount != null)
            {
                strSql.Append("cpr_ShijiAcount=" + model.cpr_ShijiAcount + ",");
            }
            else
            {
                strSql.Append("cpr_ShijiAcount= null ,");
            }
            if (model.cpr_Touzi != null)
            {
                strSql.Append("cpr_Touzi=" + model.cpr_Touzi + ",");
            }
            else
            {
                strSql.Append("cpr_Touzi= null ,");
            }
            if (model.cpr_ShijiTouzi != null)
            {
                strSql.Append("cpr_ShijiTouzi=" + model.cpr_ShijiTouzi + ",");
            }
            else
            {
                strSql.Append("cpr_ShijiTouzi= null ,");
            }
            if (model.cpr_Process != null)
            {
                strSql.Append("cpr_Process='" + model.cpr_Process + "',");
            }
            else
            {
                strSql.Append("cpr_Process= null ,");
            }
            if (model.cpr_SignDate != null)
            {
                strSql.Append("cpr_SignDate='" + model.cpr_SignDate + "',");
            }
            else
            {
                strSql.Append("cpr_SignDate= null ,");
            }
            if (model.cpr_SignDate2 != null)
            {
                strSql.Append("cpr_SignDate2='" + model.cpr_SignDate2 + "',");
            }
            else
            {
                strSql.Append("cpr_SignDate2= null ,");
            }
            if (model.cpr_DoneDate != null)
            {
                strSql.Append("cpr_DoneDate='" + model.cpr_DoneDate + "',");
            }
            else
            {
                strSql.Append("cpr_DoneDate= null ,");
            }
            if (model.cpr_Mark != null)
            {
                strSql.Append("cpr_Mark='" + model.cpr_Mark + "',");
            }
            else
            {
                strSql.Append("cpr_Mark= null ,");
            }
            if (model.BuildArea != null)
            {
                strSql.Append("BuildArea='" + model.BuildArea + "',");
            }
            else
            {
                strSql.Append("BuildArea= null ,");
            }
            if (model.ChgPeople != null)
            {
                strSql.Append("ChgPeople='" + model.ChgPeople + "',");
            }
            else
            {
                strSql.Append("ChgPeople= null ,");
            }
            if (model.ChgPhone != null)
            {
                strSql.Append("ChgPhone='" + model.ChgPhone + "',");
            }
            else
            {
                strSql.Append("ChgPhone= null ,");
            }
            if (model.ChgJia != null)
            {
                strSql.Append("ChgJia='" + model.ChgJia + "',");
            }
            else
            {
                strSql.Append("ChgJia= null ,");
            }
            if (model.ChgJiaPhone != null)
            {
                strSql.Append("ChgJiaPhone='" + model.ChgJiaPhone + "',");
            }
            else
            {
                strSql.Append("ChgJiaPhone= null ,");
            }
            if (model.BuildPosition != null)
            {
                strSql.Append("BuildPosition='" + model.BuildPosition + "',");
            }
            else
            {
                strSql.Append("BuildPosition= null ,");
            }
            if (model.Industry != null)
            {
                strSql.Append("Industry='" + model.Industry + "',");
            }
            else
            {
                strSql.Append("Industry= null ,");
            }
            if (model.BuildUnit != null)
            {
                strSql.Append("BuildUnit='" + model.BuildUnit + "',");
            }
            else
            {
                strSql.Append("BuildUnit= null ,");
            }
            if (model.BuildSrc != null)
            {
                strSql.Append("BuildSrc='" + model.BuildSrc + "',");
            }
            else
            {
                strSql.Append("BuildSrc= null ,");
            }
            if (model.TableMaker != null)
            {
                strSql.Append("TableMaker='" + model.TableMaker + "',");
            }
            else
            {
                strSql.Append("TableMaker= null ,");
            }
            if (model.RegTime != null)
            {
                strSql.Append("RegTime='" + model.RegTime + "',");
            }
            else
            {
                strSql.Append("RegTime= null ,");
            }
            if (model.UpdateBy != null)
            {
                strSql.Append("UpdateBy='" + model.UpdateBy + "',");
            }
            else
            {
                strSql.Append("UpdateBy= null ,");
            }
            if (model.LastUpdate != null)
            {
                strSql.Append("LastUpdate='" + model.LastUpdate + "',");
            }
            else
            {
                strSql.Append("LastUpdate= null ,");
            }
            if (model.BuildType != null)
            {
                strSql.Append("BuildType='" + model.BuildType + "',");
            }
            else
            {
                strSql.Append("BuildType= null ,");
            }
            if (model.StructType != null)
            {
                strSql.Append("StructType='" + model.StructType + "',");
            }
            else
            {
                strSql.Append("StructType= null ,");
            }
            if (model.Floor != null)
            {
                strSql.Append("Floor='" + model.Floor + "',");
            }
            else
            {
                strSql.Append("Floor= null ,");
            }
            if (model.BuildStructType != null)
            {
                strSql.Append("BuildStructType='" + model.BuildStructType + "',");
            }
            else
            {
                strSql.Append("BuildStructType= null ,");
            }
            if (model.MultiBuild != null)
            {
                strSql.Append("MultiBuild='" + model.MultiBuild + "',");
            }
            else
            {
                strSql.Append("MultiBuild= null ,");
            }
            if (model.JieGou != null)
            {
                strSql.Append("JieGou='" + model.JieGou + "',");
            }
            else
            {
                strSql.Append("JieGou= null ,");
            }
            if (model.Geips != null)
            {
                strSql.Append("Geips='" + model.Geips + "',");
            }
            else
            {
                strSql.Append("Geips= null ,");
            }
            if (model.Nuant != null)
            {
                strSql.Append("Nuant='" + model.Nuant + "',");
            }
            else
            {
                strSql.Append("Nuant= null ,");
            }
            if (model.Dianq != null)
            {
                strSql.Append("Dianq='" + model.Dianq + "',");
            }
            else
            {
                strSql.Append("Dianq= null ,");
            }
            //by long 20130517 -修改去掉
            //if (model.InsertUserID != null)
            //{
            //    strSql.Append("InsertUserID=" + model.InsertUserID + ",");
            //}
            //else
            //{
            //    strSql.Append("InsertUserID= null ,");
            //}
            //if (model.InsertDate != null)
            //{
            //    strSql.Append("InsertDate='" + model.InsertDate + "',");
            //}
            //else
            //{
            //    strSql.Append("InsertDate= null ,");
            //}
            
            //添加项目经理ID qpl 20131225 
            if (model.PMUserID != null)
            {
                strSql.Append("PMUserID=" + model.PMUserID + ",");
            }
            else
            {
                strSql.Append("PMUserID= null ,");
            }
           
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where cpr_Id=" + model.cpr_Id + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int cpr_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_Coperation ");
            strSql.Append(" where cpr_Id=" + cpr_Id + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public ArrayList DeleteList(string cpr_Idlist)
        {
            int count = 0;
            string[] idArray = cpr_Idlist.Split(',');

            ArrayList listAudit = new ArrayList();

            foreach (string id in idArray)
            {
                if (!string.IsNullOrEmpty(id.Trim()) && id.Trim() != ",")
                {
                    //判断 是否在审核状态 中
                    if (!IsAudit(id))
                    {
                        count = DbHelperSQL.ExecuteSql("delete cm_Coperation where cpr_Id=" + id);
                    }
                    else
                    {
                        listAudit.Add(id);
                    }
                }
            }
            return listAudit;
        }

        /// <summary>
        /// 是否在审核中
        /// </summary>
        /// <param name="cprID"></param>
        /// <returns></returns>
        public bool IsAudit(string cprID)
        {
            string sql = @"select  top 1 COUNT(SysNo) from dbo.cm_AuditRecord 
                       where (Status='A' OR  Status='B' OR Status='D' OR Status='F' OR Status='H' OR Status='J')
                        and CoperationSysNo=" + cprID + "";

            DataSet ds = DbHelperSQL.Query(sql);

            if (int.Parse(ds.Tables[0].Rows[0][0].ToString()) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_Coperation GetModel(int cpr_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" *");
            strSql.Append(" from cm_Coperation ");
            strSql.Append(" where cpr_Id=" + cpr_Id + "");
            TG.Model.cm_Coperation model = new TG.Model.cm_Coperation();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["cpr_Id"] != null && ds.Tables[0].Rows[0]["cpr_Id"].ToString() != "")
                {
                    model.cpr_Id = int.Parse(ds.Tables[0].Rows[0]["cpr_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cst_Id"] != null && ds.Tables[0].Rows[0]["cst_Id"].ToString() != "")
                {
                    model.cst_Id = int.Parse(ds.Tables[0].Rows[0]["cst_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_No"] != null && ds.Tables[0].Rows[0]["cpr_No"].ToString() != "")
                {
                    model.cpr_No = ds.Tables[0].Rows[0]["cpr_No"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Type"] != null && ds.Tables[0].Rows[0]["cpr_Type"].ToString() != "")
                {
                    model.cpr_Type = ds.Tables[0].Rows[0]["cpr_Type"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Type2"] != null && ds.Tables[0].Rows[0]["cpr_Type2"].ToString() != "")
                {
                    model.cpr_Type2 = ds.Tables[0].Rows[0]["cpr_Type2"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Name"] != null && ds.Tables[0].Rows[0]["cpr_Name"].ToString() != "")
                {
                    model.cpr_Name = ds.Tables[0].Rows[0]["cpr_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Unit"] != null && ds.Tables[0].Rows[0]["cpr_Unit"].ToString() != "")
                {
                    model.cpr_Unit = ds.Tables[0].Rows[0]["cpr_Unit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Acount"] != null && ds.Tables[0].Rows[0]["cpr_Acount"].ToString() != "")
                {
                    model.cpr_Acount = decimal.Parse(ds.Tables[0].Rows[0]["cpr_Acount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_ShijiAcount"] != null && ds.Tables[0].Rows[0]["cpr_ShijiAcount"].ToString() != "")
                {
                    model.cpr_ShijiAcount = decimal.Parse(ds.Tables[0].Rows[0]["cpr_ShijiAcount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_Touzi"] != null && ds.Tables[0].Rows[0]["cpr_Touzi"].ToString() != "")
                {
                    model.cpr_Touzi = decimal.Parse(ds.Tables[0].Rows[0]["cpr_Touzi"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_ShijiTouzi"] != null && ds.Tables[0].Rows[0]["cpr_ShijiTouzi"].ToString() != "")
                {
                    model.cpr_ShijiTouzi = decimal.Parse(ds.Tables[0].Rows[0]["cpr_ShijiTouzi"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_Process"] != null && ds.Tables[0].Rows[0]["cpr_Process"].ToString() != "")
                {
                    model.cpr_Process = ds.Tables[0].Rows[0]["cpr_Process"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_SignDate"] != null && ds.Tables[0].Rows[0]["cpr_SignDate"].ToString() != "")
                {
                    model.cpr_SignDate = DateTime.Parse(ds.Tables[0].Rows[0]["cpr_SignDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_SignDate2"] != null && ds.Tables[0].Rows[0]["cpr_SignDate2"].ToString() != "")
                {
                    model.cpr_SignDate2 = DateTime.Parse(ds.Tables[0].Rows[0]["cpr_SignDate2"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_DoneDate"] != null && ds.Tables[0].Rows[0]["cpr_DoneDate"].ToString() != "")
                {
                    model.cpr_DoneDate = DateTime.Parse(ds.Tables[0].Rows[0]["cpr_DoneDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_Mark"] != null && ds.Tables[0].Rows[0]["cpr_Mark"].ToString() != "")
                {
                    model.cpr_Mark = ds.Tables[0].Rows[0]["cpr_Mark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildArea"] != null && ds.Tables[0].Rows[0]["BuildArea"].ToString() != "")
                {
                    model.BuildArea = ds.Tables[0].Rows[0]["BuildArea"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChgPeople"] != null && ds.Tables[0].Rows[0]["ChgPeople"].ToString() != "")
                {
                    model.ChgPeople = ds.Tables[0].Rows[0]["ChgPeople"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChgPhone"] != null && ds.Tables[0].Rows[0]["ChgPhone"].ToString() != "")
                {
                    model.ChgPhone = ds.Tables[0].Rows[0]["ChgPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChgJia"] != null && ds.Tables[0].Rows[0]["ChgJia"].ToString() != "")
                {
                    model.ChgJia = ds.Tables[0].Rows[0]["ChgJia"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChgJiaPhone"] != null && ds.Tables[0].Rows[0]["ChgJiaPhone"].ToString() != "")
                {
                    model.ChgJiaPhone = ds.Tables[0].Rows[0]["ChgJiaPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildPosition"] != null && ds.Tables[0].Rows[0]["BuildPosition"].ToString() != "")
                {
                    model.BuildPosition = ds.Tables[0].Rows[0]["BuildPosition"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Industry"] != null && ds.Tables[0].Rows[0]["Industry"].ToString() != "")
                {
                    model.Industry = ds.Tables[0].Rows[0]["Industry"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildUnit"] != null && ds.Tables[0].Rows[0]["BuildUnit"].ToString() != "")
                {
                    model.BuildUnit = ds.Tables[0].Rows[0]["BuildUnit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildSrc"] != null && ds.Tables[0].Rows[0]["BuildSrc"].ToString() != "")
                {
                    model.BuildSrc = ds.Tables[0].Rows[0]["BuildSrc"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TableMaker"] != null && ds.Tables[0].Rows[0]["TableMaker"].ToString() != "")
                {
                    model.TableMaker = ds.Tables[0].Rows[0]["TableMaker"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegTime"] != null && ds.Tables[0].Rows[0]["RegTime"].ToString() != "")
                {
                    model.RegTime = DateTime.Parse(ds.Tables[0].Rows[0]["RegTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdateBy"] != null && ds.Tables[0].Rows[0]["UpdateBy"].ToString() != "")
                {
                    model.UpdateBy = ds.Tables[0].Rows[0]["UpdateBy"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LastUpdate"] != null && ds.Tables[0].Rows[0]["LastUpdate"].ToString() != "")
                {
                    model.LastUpdate = DateTime.Parse(ds.Tables[0].Rows[0]["LastUpdate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BuildType"] != null && ds.Tables[0].Rows[0]["BuildType"].ToString() != "")
                {
                    model.BuildType = ds.Tables[0].Rows[0]["BuildType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StructType"] != null && ds.Tables[0].Rows[0]["StructType"].ToString() != "")
                {
                    model.StructType = ds.Tables[0].Rows[0]["StructType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Floor"] != null && ds.Tables[0].Rows[0]["Floor"].ToString() != "")
                {
                    model.Floor = ds.Tables[0].Rows[0]["Floor"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildStructType"] != null && ds.Tables[0].Rows[0]["BuildStructType"].ToString() != "")
                {
                    model.BuildStructType = ds.Tables[0].Rows[0]["BuildStructType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["MultiBuild"] != null && ds.Tables[0].Rows[0]["MultiBuild"].ToString() != "")
                {
                    model.MultiBuild = ds.Tables[0].Rows[0]["MultiBuild"].ToString();
                }
                if (ds.Tables[0].Rows[0]["JieGou"] != null && ds.Tables[0].Rows[0]["JieGou"].ToString() != "")
                {
                    model.JieGou = ds.Tables[0].Rows[0]["JieGou"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GeiPs"] != null && ds.Tables[0].Rows[0]["GeiPs"].ToString() != "")
                {
                    model.Geips = ds.Tables[0].Rows[0]["GeiPs"].ToString();
                }
                if (ds.Tables[0].Rows[0]["NuanT"] != null && ds.Tables[0].Rows[0]["NuanT"].ToString() != "")
                {
                    model.Nuant = ds.Tables[0].Rows[0]["NuanT"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DianQ"] != null && ds.Tables[0].Rows[0]["DianQ"].ToString() != "")
                {
                    model.Dianq = ds.Tables[0].Rows[0]["DianQ"].ToString();
                }
                //by long 20130517
                if (ds.Tables[0].Rows[0]["InsertUserID"] != null && ds.Tables[0].Rows[0]["InsertUserID"].ToString() != "")
                {
                    model.InsertUserID = int.Parse(ds.Tables[0].Rows[0]["InsertUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InsertDate"] != null && ds.Tables[0].Rows[0]["InsertDate"].ToString() != "")
                {
                    model.InsertDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["InsertDate"].ToString());
                }
                //by long 20131225
                if (ds.Tables[0].Rows[0]["PMUserID"] != null && ds.Tables[0].Rows[0]["PMUserID"].ToString() != "")
                {
                    model.PMUserID = int.Parse(ds.Tables[0].Rows[0]["PMUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["IsParamterEdit"] != null && ds.Tables[0].Rows[0]["IsParamterEdit"].ToString() != "")
                {
                    model.IsEdit = ds.Tables[0].Rows[0]["IsParamterEdit"].ToString();
                }
              
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select cpr_Id,cst_Id,cpr_No,cpr_Type,cpr_Type2,cpr_Name,cpr_Unit,cpr_Acount,cpr_ShijiAcount,cpr_Touzi,cpr_ShijiTouzi,cpr_Process,cpr_SignDate,cpr_DoneDate,cpr_Mark,BuildArea,ChgPeople,ChgPhone,ChgJia,ChgJiaPhone,BuildPosition,Industry,BuildUnit,BuildSrc,TableMaker,RegTime,UpdateBy,LastUpdate,BuildType,StructType,Floor,BuildStructType,MultiBuild,JieGou,GeiPs,NuanT,DianQ,(Select mem_Name From tg_member Where mem_ID=InsertUserID) AS InsertUser,InsertDate,PMUserID,cpr_SignDate2 ");
            strSql.Append(" FROM cm_Coperation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" ORDER BY cpr_Id DESC ");
            return DbHelperSQL.Query(strSql.ToString());
        }
        public DataSet GetCoperationExportInfo(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"select * from(select *,
					Convert(decimal(18,2),(Select isnull(SUM(Acount),0.00) From cm_ProjectCharge Where Status<>'B' AND cprID=C.cpr_Id )) as ssze,
					convert(varchar(10),cpr_SignDate2,120) as qdrq,
                    convert(varchar(10),cpr_SignDate,120) as tjrq,
					(select Cst_Name from cm_CustomerInfo where Cst_Id=C.cst_Id) as cstName,
					Convert(decimal(18,2),(case when cpr_ShijiAcount<>0 then (Select isnull(SUM(Acount),0.00) From cm_ProjectCharge Where Status<>'B' AND cprID=C.cpr_Id )/cpr_ShijiAcount *100 else 0 end)) as sfjd,
					Convert(decimal(18,2),(cpr_ShijiAcount-(Select isnull(SUM(Acount),0.00) From cm_ProjectCharge Where Status<>'B' AND cprID=C.cpr_Id))) as wsk,                    
					 convert(varchar(10),cpr_DoneDate,120) as wcrq,
					(Select mem_Name From tg_member Where mem_ID=InsertUserID) AS InsertUser,
					CONVERT(nvarchar(100),C.InsertDate,23) as lrsj,
                    (case when (ChgPeople='''' or ChgPeople is null) then (select mem_Name from tg_member where mem_ID=PMUserID) else ChgPeople end) as PMUserName,
					(Select Top 1 convert(varchar(10),InAcountTime,120) From cm_ProjectCharge Where cprID=C.cpr_Id Order By ID DESC) as newdate
				from cm_Coperation C) cc ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where 1=1" + strWhere);
            }
            strSql.Append(" ORDER BY cpr_Id DESC ");
            return DbHelperSQL.Query(strSql.ToString());
        }
        public DataSet GetListDistinct(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select Distinct cpr_No ");
            strSql.Append(" FROM cm_Coperation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" cpr_Id,cst_Id,cpr_No,cpr_Type,cpr_Type2,cpr_Name,cpr_Unit,cpr_Acount,cpr_ShijiAcount,cpr_Touzi,cpr_ShijiTouzi,cpr_Process,cpr_SignDate,cpr_DoneDate,cpr_Mark,BuildArea,ChgPeople,ChgPhone,ChgJia,ChgJiaPhone,BuildPosition,Industry,BuildUnit,BuildSrc,TableMaker,RegTime,UpdateBy,LastUpdate,BuildType,StructType,Floor,BuildStructType,MultiBuild,JieGou,GeiPs,NuanT,DianQ,(Select mem_Name From tg_member Where mem_ID=InsertUserID) AS InsertUser,InsertDate,cpr_SignDate2 ");
            strSql.Append(" FROM cm_Coperation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_Coperation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where 1=1   " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            /*没有调用此方法，无需修改。Created by Sago*/

            StringBuilder strSql = new StringBuilder();

            string sql = string.Format(@"SELECT TOP (20) *
                                                    FROM cm_Coperation c
                                                    WHERE (cpr_Id NOT IN (SELECT TOP (20) cpr_Id 
                                                    FROM cm_Coperation 
                                                    where 1=1 {0} ORDER BY {1}))ORDER BY cpr_Id", strWhere, orderby);
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_Coperation", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_Coperation_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 分阶段获得数据-不用RowNumber
        /// </summary>
        /// <param name="strWhere">where语句</param>
        /// <param name="pagesize">每页数量</param>
        /// <param name="pageIndex">要得到第几页的数据</param>
        /// <returns>数据集</returns>
        public DataSet GetListByPage(string strWhere, int pagesize, int pageIndex, string orderBy)
        {
            StringBuilder sqlStrTotal = new StringBuilder();
            sqlStrTotal.Append("select top " + pagesize + "* from cm_Coperation where cpr_Id not in ");
            StringBuilder sqlStr = new StringBuilder();
            int notInTop = (pageIndex - 1) * pagesize;

            sqlStr.Append(" (select TOP " + notInTop + " cpr_Id from cm_Coperation ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sqlStr.AppendFormat(" where 1=1 {0}", strWhere);
            }
            if (!String.IsNullOrEmpty(orderBy))
            {
                sqlStr.AppendFormat(" order By {0} ", orderBy);
            }
            sqlStr.Append(")");
            sqlStrTotal.Append(sqlStr.ToString());

            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sqlStrTotal.AppendFormat("  {0}", strWhere);
            }
            if (!String.IsNullOrEmpty(orderBy))
            {
                sqlStrTotal.AppendFormat(" order By {0} ", orderBy);
            }

            return DbHelperSQL.Query(sqlStrTotal.ToString());
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        /// <param name="coperationName">合同名称</param>
        /// <param name="pageSize">没页大小</param>
        /// <param name="pageCurrent">当前页</param>
        /// <returns></returns>
        public List<TG.Model.cm_Coperation> GetCoperationList(string coperationName, int pageSize, int pageCurrent)
        {
            string whereSql = " where 1=1 ";
            if (!string.IsNullOrEmpty(coperationName))
            {
                whereSql += " and c.cpr_Name = N'" + coperationName + "'";
            }

            string sql = string.Format(@"SELECT TOP {0} *
                                                    FROM cm_Coperation c
                                                    {2} cpr_Id >
                                                              (
                                                              SELECT ISNULL(MAX(cpr_Id),0) 
                                                              FROM 
                                                                    (
                                                                    SELECT TOP {3} cpr_Id FROM cm_Coperation c 
                                                                    {2}
                                                                    order by c.cpr_Id DESC
                                                                    ) TT
                                                              ) 
                                                    ORDER BY cpr_Id", pageSize, pageCurrent, whereSql, pageCurrent * pageSize);

            //string sql = " SELECT TOP(" + pageSize + ")* FROM ( SELECT ROW_NUMBE() OVER(ORDER BY c.cpr_Id DESC) as RowID,c.cpr_Id,c.cst_Id,c.cpr_No,c.cpr_Type,c.cpr_Type2,c.cpr_Name,c.cpr_Unit,c.cpr_Acount,c.cpr_ShijiAcount,c.cpr_Touzi,c.cpr_ShijiTouzi,c.cpr_Process,c.cpr_SignDate,c.cpr_DoneDate,c.cpr_Mark,c.BuildArea,c.ChgPeople,c.ChgPhone,c.ChgJia,c.ChgJiaPhone,c.BuildPosition,c.Industry,c.BuildUnit,c.BuildSrc,c.TableMaker,c.RegTime,c.UpdateBy,c.LastUpdate  FROM cm_Coperation c " + whereSql + ")  TT WHERE RowID > " + pageCurrent + " * " + pageSize + "";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);
            List<TG.Model.cm_Coperation> resultList = new List<TG.Model.cm_Coperation>();
            while (reader.Read())
            {
                TG.Model.cm_Coperation coperationEntity = new TG.Model.cm_Coperation
                {
                    cpr_Id = (int)reader["cpr_Id"],
                    cst_Id = (int)reader["cst_Id"],
                    cpr_No = reader["cpr_No"].ToString(),
                    cpr_Type = reader["cpr_Type"].ToString(),
                    cpr_Type2 = reader["cpr_Type2"].ToString(),
                    cpr_Name = reader["cpr_Name"].ToString(),
                    cpr_Unit = reader["cpr_Unit"].ToString(),
                    cpr_Acount = Convert.ToDecimal(reader["cpr_Acount"]),
                    cpr_ShijiAcount = Convert.ToDecimal(reader["cpr_ShijiAcount"]),
                    cpr_Touzi = Convert.ToDecimal(reader["cpr_Touzi"]),
                    cpr_ShijiTouzi = Convert.ToDecimal(reader["cpr_ShijiTouzi"]),
                    cpr_Process = reader["cpr_Process"].ToString(),
                    cpr_SignDate = Convert.ToDateTime(reader["cpr_SignDate"].ToString()),
                    cpr_DoneDate = Convert.ToDateTime(reader["cpr_DoneDate"]),
                    cpr_Mark = reader["cpr_Mark"].ToString(),
                    BuildArea = reader["BuildArea"].ToString(),
                    ChgPeople = reader["ChgPeople"].ToString(),
                    ChgPhone = reader["ChgPhone"].ToString(),
                    ChgJia = reader["ChgJia"].ToString(),
                    ChgJiaPhone = reader["ChgJiaPhone"].ToString(),
                    BuildPosition = reader["BuildPosition"].ToString(),
                    Industry = reader["Industry"].ToString(),
                    BuildUnit = reader["BuildUnit"].ToString(),
                    BuildSrc = reader["BuildSrc"].ToString(),
                    TableMaker = reader["TableMaker"].ToString(),
                    RegTime = Convert.ToDateTime(reader["RegTime"].ToString()),
                    UpdateBy = reader["UpdateBy"].ToString(),
                    LastUpdate = Convert.ToDateTime(reader["LastUpdate"].ToString()),
                    BuildType = reader["BuildType"].ToString(),
                    StructType = reader["StructType"].ToString(),
                    Floor = reader["Floor"].ToString(),
                    BuildStructType = reader["BuildStructType"].ToString(),
                    MultiBuild = reader["MultiBuild"].ToString(),
                    PMUserID = int.Parse(reader["PMUserID"].ToString())
                };
                resultList.Add(coperationEntity);
            }
            return resultList;
        }
        /// <summary>
        /// 获取所有合同年份
        /// </summary>
        /// <returns></returns>
        public List<string> GetCoperationYear()
        {
            List<string> list = new List<string>();
            string strSql = " Select DISTINCT year(cpr_SignDate) AS SignDate From cm_Coperation order by SignDate DESC";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i][0].ToString());
                }
            }
            return list;
        }
        /// <summary>
        /// 获取所有合同统计年份
        /// </summary>
        /// <returns></returns>
        public List<string> GetCoperationYear2()
        {
            List<string> list = new List<string>();
            string strSql = " Select DISTINCT year(cpr_SignDate2) AS SignDate From cm_Coperation order by SignDate DESC";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i][0].ToString());
                }
            }
            return list;
        }
        /// <summary>
        /// 统计合同列表，目标值，本月收费，年度收费
        /// </summary>
        public SqlDataReader GetCoperationChargeAndYearAllot(string query, string firstday, string lastday, string year, int startIndex, int endIndex)
        {
            SqlParameter[] param = new SqlParameter[]{
                                new SqlParameter("@firstday",firstday),
                                new SqlParameter("@lastday",lastday),
                                new SqlParameter("@year",year),
                                new SqlParameter("@startIndex",startIndex),
                                new SqlParameter("@endIndex",endIndex),
                                new SqlParameter("@query",query)
            };
            return DbHelperSQL.RunProcedure("P_ProjectChargeYear_SUM", param);
        }
        /// <summary>
        /// 项目产值系数设置是否编辑
        /// </summary>
        public int UpdateProjectValueParamterIsEidt(int CprID, int IsEidtStatus)
        {
            string sql = "Update cm_Coperation set IsParamterEdit =" + IsEidtStatus + " where cpr_Id=" + CprID + " ";

            int rowsAffected = DbHelperSQL.ExecuteSql(sql);

            return rowsAffected;
        }

        /// <summary>
        /// 统计合同合同，收款，目标值，完成比例
        /// </summary>
        public DataSet CountCoperationTarget(string year,string startyear,string endyear, string starttime, string endtime, int? unitid)
        {
            //参数
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@startyear",startyear),
                 new SqlParameter("@endyear",endyear),
                new SqlParameter("@starttime",starttime),
                new SqlParameter("@endtime",endtime),
                new SqlParameter("@unitid",unitid)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountCprByYear", paras, "Table1");
        }

        /// <summary>
        /// 统计合同合同，收款，目标值，完成比例
        /// </summary>
        public DataSet CountCoperationTargetBySql(string year, string sqlwhere, string starttime, string endtime, string strWhere)
        {
            string sql = "";

            sql = string.Format(@"  SELECT * FROM 
                                        (
	                                    SELECT unit_ID AS [ID],unit_Name AS [Name],cpryear,CprTarget,CprAllCount,case when CprTarget=0 then 0 else Convert(decimal(10,2),(CprAllCount/CprTarget)*100) end AS Cprprt,StartTime,EndTime,AllotTarget,AllotCount,case when AllotTarget=0 then 0 else Convert(decimal(10,2),(AllotCount/AllotTarget)*100) end AS AllotPrt
	                                    FROM
	                                    (
		                                    SELECT unit_ID,unit_Name,{0} as cpryear,
		                                    isnull((SELECT TOP 1 UnitAllot FROM cm_UnitCopAllot WHERE ( (AllotYear={0}) OR ({0} is null)) AND UnitID=U.unit_ID ),0) AS CprTarget,
		                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {4} AND cpr_Unit=U.unit_Name),0) AS CprAllCount,
		                                    '{1}' as StartTime,
		                                    '{2}' as EndTime,
		                                    isnull((SELECT TOP 1 UnitAllot FROM cm_UnitAllot WHERE ((AllotYear={0}) OR ({0} is null)) AND UnitID=U.unit_ID),0) AS AllotTarget,
		                                    isnull((SELECT SUM(Acount) FROM cm_ProjectCharge WHERE (InAcountTime BETWEEN  '{1}'  AND '{2}' ) AND Status<>'B' AND (cprID IN (SELECT cpr_ID From cm_Coperation WHERE cpr_Unit=U.unit_Name ))),0) AS AllotCount
		                                    FROM tg_Unit U
		                                    
	                                    ) A ) A WHERE 1=1 {3}", year, starttime, endtime, strWhere,sqlwhere);

            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            return ds;
        }

        /// <summary>
        /// 合同综合统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <param name="type">0查询生产部门，1表示全院</param>
        /// <returns></returns>
        public DataSet CountCoperationZong(string year, string starttime, string endtime, int? unitid, string type)
        {
            //参数
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@starttime",starttime),
                new SqlParameter("@endtime",endtime),
                new SqlParameter("@unit_id",unitid),
                new SqlParameter("@type",type)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountCprAll", paras, "Table1");
        }
        /// <summary>
        /// 统计合同按季度统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountCoperationJD(string year, string starttime, string endtime, int? unitid, string type)
        {
            //参数
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@starttime",starttime),
                new SqlParameter("@endtime",endtime),
                new SqlParameter("@unit_id",unitid),
                new SqlParameter("@type",type)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountCprByJD", paras, "Table1");
        }
        /// <summary>
        /// 统计合同按月份统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountCoperationMonth(string year, string starttime, string endtime, int? unitid, string type)
        {
            //参数
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@starttime",starttime),
                new SqlParameter("@endtime",endtime),
                new SqlParameter("@unit_id",unitid),
                new SqlParameter("@type",type)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountCprByMonth", paras, "Table1");
        }
        /// <summary>
        /// 统计合同按类型统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountCoperationType(string year, string starttime, string endtime, int? unitid, string type)
        {
            //参数
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@starttime",starttime),
                new SqlParameter("@endtime",endtime),
                new SqlParameter("@unit_id",unitid),
                new SqlParameter("@type",type)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountCprByType", paras, "Table1");
        }
        /// <summary>
        /// 合同审批情况统计
        /// </summary>
        public DataSet CountCoperationAudit(string year, string starttime, string endtime, int? unitid)
        {
            //参数
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@starttime",starttime),
                new SqlParameter("@endtime",endtime),
                new SqlParameter("@unit_id",unitid)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountCprAudit", paras, "Table1");
        }

        /// <summary>
        /// 合同审批情况统计
        /// </summary>
        public DataSet CountCoperationAuditBySql(string year, string starttime, string endtime, string strWhere)
        {

            string sql = "";

            sql = string.Format(@"  SELECT [ID],[Name],A,B,C,D,E,F,G
                                    FROM(
                                      SELECT  [ID],[Name],A,B,C,D,E,F,G
                                      FROM
                                       (  
	                                    SELECT unit_ID AS [ID],unit_Name AS [Name],
	                                    isnull((SELECT COUNT(*) FROM cm_Coperation WHERE cpr_Unit=U.unit_Name  {0}  ),0) AS A,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE cpr_Unit=U.unit_Name {0} ),0) AS B,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE cpr_Unit=U.unit_Name {0} ),0) AS C,
	                                    isnull((SELECT SUM(Acount) FROM cm_ProjectCharge WHERE cprID IN (SELECT cpr_ID FROM cm_Coperation WHERE cpr_Unit=U.unit_Name) AND (InAcountTime BETWEEN '{1}' AND '{2}') AND ([Status]<>'B')),0) AS D,
	                                    isnull(
                                               (select SUM(sl) from 
				                                (
                                                  SELECT COUNT(*) as sl FROM cm_Coperation WHERE cpr_Unit=U.unit_Name {0}  AND (SELECT COUNT(1) FROM cm_AuditRecord WHERE CoperationSysNo=cpr_ID AND ([Status]='J' OR ([Status]='H' AND NeedLegalAdviser=0)))>0
                                                  union all
				                                  SELECT COUNT(*) as sl FROM cm_Coperation WHERE cpr_Unit=U.unit_Name {0}  AND (select count(1) from cm_SuperCoperationAudit where CoperationSysNo=cpr_Id and [Status]='D')>0
				                                ) T
                                        ),0) AS E,	                                                
                                        isnull(
                                                (select SUM(sl) from 
                                                  ( 
                                                   SELECT COUNT(*) as sl FROM cm_Coperation WHERE cpr_Unit=U.unit_Name {0}  AND cpr_ID in (SELECT CoperationSysNo FROM cm_AuditRecord WHERE CoperationSysNo=cpr_ID) and cpr_ID not in (SELECT CoperationSysNo FROM cm_AuditRecord WHERE CoperationSysNo=cpr_ID AND ([Status]='J' OR ([Status]='H' AND NeedLegalAdviser=0)))
                                                   union all
                                                   SELECT COUNT(*) as sl FROM cm_Coperation WHERE cpr_Unit=U.unit_Name {0}  and cpr_ID in (SELECT CoperationSysNo FROM cm_SuperCoperationAudit WHERE CoperationSysNo=cpr_ID) and cpr_ID not in (select CoperationSysNo from cm_SuperCoperationAudit where CoperationSysNo=cpr_Id and [Status]='D')
                                                  ) T
                                        ),0) AS F,
	                                    isnull(
                                                (select SUM(sl) from 
                                                  (
                                                    SELECT COUNT(*) as sl FROM cm_Coperation WHERE cpr_Unit=U.unit_Name {0}  AND (SELECT COUNT(1) FROM cm_AuditRecord WHERE CoperationSysNo=cpr_ID)=0
                                                    union all
                                                    SELECT COUNT(*) as sl FROM cm_Coperation WHERE cpr_Unit=U.unit_Name {0}   and (select COUNT(1) from cm_SuperCoperationAudit where CoperationSysNo=cpr_Id)=0
                                                  ) T	
                                        ),0) AS G
	                                    FROM tg_unit U
	                                    ) A
	                                    WHERE 1=1 {3}
	                                    Union 
	                                    SELECT 500 AS [ID],'全院总计' AS [Name],
	                                    (SELECT COUNT(*) FROM cm_Coperation WHERE 1=1 {0}) AS A,
	                                    (SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} ) AS B,
	                                    (SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} ) AS C,
	                                    (SELECT SUM(Acount) FROM cm_ProjectCharge WHERE cprID IN (SELECT cpr_ID FROM cm_Coperation) AND (InAcountTime BETWEEN '{1}' AND '{2}' ) AND ([Status]<>'B')) AS D,
	                                    (
                                            select SUM(sl) from(
                                                SELECT COUNT(*) as sl FROM cm_Coperation WHERE 1=1 {0}  AND (SELECT COUNT(1) FROM cm_AuditRecord WHERE CoperationSysNo=cpr_ID AND ([Status]='J' OR ([Status]='H' AND NeedLegalAdviser=0)))>0
                                                union all
                                                select COUNT(*) as sl from cm_Coperation where 1=1 {0}  and (select count(1) from cm_SuperCoperationAudit where CoperationSysNo=cpr_Id and [Status]='D')>0
                                              ) T
                                        ) AS E,
	                                    (
                                            select SUM(sl) from(
                                                SELECT COUNT(*) as sl FROM cm_Coperation WHERE 1=1 {0}  AND cpr_ID in (SELECT CoperationSysNo FROM cm_AuditRecord WHERE CoperationSysNo=cpr_ID) AND cpr_ID not in(SELECT CoperationSysNo FROM cm_AuditRecord WHERE CoperationSysNo=cpr_ID AND ([Status]='J' OR ([Status]='H' AND NeedLegalAdviser=0)))
                                                union all
	                                            SELECT COUNT(*) as sl FROM cm_Coperation WHERE 1=1 {0}  and cpr_ID in (SELECT CoperationSysNo FROM cm_SuperCoperationAudit WHERE CoperationSysNo=cpr_ID) and cpr_ID not in (select CoperationSysNo from cm_SuperCoperationAudit where CoperationSysNo=cpr_Id and [Status]='D')
	                                        ) T
                                        ) AS F,
	                                    (
                                             select SUM(sl) from(
                                                SELECT COUNT(*) as sl FROM cm_Coperation WHERE 1=1 {0}  AND (SELECT COUNT(1) FROM cm_AuditRecord WHERE CoperationSysNo=cpr_ID )=0
                                                union all
		                                        SELECT COUNT(*) as sl FROM cm_Coperation WHERE 1=1 {0}  and (select COUNT(1) from cm_SuperCoperationAudit where CoperationSysNo=cpr_Id)=0
	                                         ) T
                                        ) AS G
                                    ) T Order by [ID]", year, starttime, endtime, strWhere);

            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            return ds;

        }

        /// <summary>
        /// 合同等级统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountCoperationLevel(string year, string starttime, string endtime, int? unitid)
        {
            //参数
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@starttime",starttime),
                new SqlParameter("@endtime",endtime),
                new SqlParameter("@unit_id",unitid)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountCprByLevel", paras, "Table1");
        }

        /// <summary>
        /// 合同等级统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountCoperationLevelBySql(string year, string starttime, string endtime, string strWhere)
        {
            string sql = "";

            sql = string.Format(@"  SELECT [ID],[Name],A,A1,A2,B,B1,B2,C,C1,C2,D,D1,D2
                                    FROM(
                                       SELECT [ID],[Name],A,A1,A2,B,B1,B2,C,C1,C2,D,D1,D2 
                                        FROM 
                                        (
	                                    SELECT unit_ID AS [ID],unit_Name AS [Name],
	                                    (Select Count(*) From cm_Coperation Where cpr_Unit=U.unit_Name {0} AND BuildType='特级') AS A,
	                                    isnull((Select SUM(cpr_Acount) From cm_Coperation Where cpr_Unit=U.unit_Name {0} AND BuildType='特级'),0) AS A1,
	                                    isnull((SELECT SUM(Acount) FROM cm_ProjectCharge WHERE cprID IN (SELECT cpr_ID FROM cm_Coperation WHERE cpr_Unit=U.unit_Name AND BuildType='特级') AND (InAcountTime BETWEEN '{1}' AND '{2}') AND [Status]<>'B'),0) AS A2,
	                                    (Select Count(*) From cm_Coperation Where cpr_Unit=U.unit_Name  {0} AND BuildType='一级') AS B,
	                                    isnull((Select SUM(cpr_Acount) From cm_Coperation Where cpr_Unit=U.unit_Name {0} AND BuildType='一级'),0) AS B1,
	                                    isnull((SELECT SUM(Acount) FROM cm_ProjectCharge WHERE cprID IN (SELECT cpr_ID FROM cm_Coperation WHERE cpr_Unit=U.unit_Name AND BuildType='一级') AND (InAcountTime BETWEEN '{1}' AND '{2}') AND [Status]<>'B'),0) AS B2,
	                                    (Select Count(*) From cm_Coperation Where cpr_Unit=U.unit_Name {0} AND BuildType='二级') AS C,
	                                    isnull((Select SUM(cpr_Acount) From cm_Coperation Where cpr_Unit=U.unit_Name {0} AND BuildType='二级'),0) AS C1,
	                                    isnull((SELECT SUM(Acount) FROM cm_ProjectCharge WHERE cprID IN (SELECT cpr_ID FROM cm_Coperation WHERE cpr_Unit=U.unit_Name AND BuildType='二级') AND (InAcountTime BETWEEN '{1}' AND '{2}') AND [Status]<>'B'),0) AS C2,
	                                    (Select Count(*) From cm_Coperation Where cpr_Unit=U.unit_Name {0} AND BuildType='三级') AS D,
	                                    isnull((Select SUM(cpr_Acount) From cm_Coperation Where cpr_Unit=U.unit_Name {0} AND BuildType='三级'),0) AS D1,
	                                    isnull((SELECT SUM(Acount) FROM cm_ProjectCharge WHERE cprID IN (SELECT cpr_ID FROM cm_Coperation WHERE cpr_Unit=U.unit_Name AND BuildType='三级') AND (InAcountTime BETWEEN '{1}' AND '{2}') AND [Status]<>'B'),0) AS D2
	                                    FROM tg_unit U
	                                      ) A
	                                    WHERE 1=1 {3}
	                                    UNION
	                                    SELECT 300 AS [ID],'全院总计' AS [Name],
	                                    (Select Count(*) From cm_Coperation Where 1=1 {0} AND BuildType='特级') AS A,
	                                    isnull((Select SUM(cpr_Acount) From cm_Coperation Where 1=1 {0} AND BuildType='特级'),0) AS A1,
	                                    isnull((SELECT SUM(Acount) FROM cm_ProjectCharge WHERE cprID IN (SELECT cpr_ID FROM cm_Coperation WHERE BuildType='特级') AND (InAcountTime BETWEEN '{1}' AND '{2}') AND [Status]<>'B'),0) AS A2,
	                                    (Select Count(*) From cm_Coperation Where 1=1 {0} AND BuildType='一级') AS B,
	                                    isnull((Select SUM(cpr_Acount) From cm_Coperation Where 1=1 {0} AND BuildType='一级'),0) AS B1,
	                                    isnull((SELECT SUM(Acount) FROM cm_ProjectCharge WHERE cprID IN (SELECT cpr_ID FROM cm_Coperation WHERE BuildType='一级') AND (InAcountTime BETWEEN '{1}' AND '{2}') AND [Status]<>'B'),0) AS B2,
	                                    (Select Count(*) From cm_Coperation Where 1=1 {0} AND BuildType='二级') AS C,
	                                    isnull((Select SUM(cpr_Acount) From cm_Coperation Where 1=1 {0} AND BuildType='二级'),0) AS C1,
	                                    isnull((SELECT SUM(Acount) FROM cm_ProjectCharge WHERE cprID IN (SELECT cpr_ID FROM cm_Coperation WHERE BuildType='二级') AND (InAcountTime BETWEEN '{1}' AND '{2}') AND [Status]<>'B'),0) AS C2,
	                                    (Select Count(*) From cm_Coperation Where 1=1 {0} AND BuildType='三级') AS D,
	                                    isnull((Select SUM(cpr_Acount) From cm_Coperation Where 1=1  {0} AND BuildType='三级'),0) AS D1,
	                                    isnull((SELECT SUM(Acount) FROM cm_ProjectCharge WHERE cprID IN (SELECT cpr_ID FROM cm_Coperation WHERE BuildType='三级') AND (InAcountTime BETWEEN '{1}' AND '{2}') AND [Status]<>'B'),0) AS D2
                                    ) T Order by [ID]", year, starttime, endtime, strWhere);

            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            return ds;
        }

        /// <summary>
        /// 合同类型综合统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public DataSet CountCoperationType(string year, string starttime, string endtime, int? unitid, int type)
        {
            //参数
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@starttime",starttime),
                new SqlParameter("@endtime",endtime),
                new SqlParameter("@unit_id",unitid),
                new SqlParameter("@type",type)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountCprByType2", paras, "Table1");
        }

        /// <summary>
        /// 合同类型综合统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public DataSet CountCoperationTypeBySql(string year, string starttime, string endtime, string strWhere, int type)
        {

            string sql = "";

            if (type == 1)
            {
                sql = string.Format(@"  SELECT [ID],[Name],A,B,C,D,E,F,G,H,I
                                FROM(
                                SELECT [ID],[Name],A,B,C,D,E,F,G,H,I
                                 FROM 
                                 (
	                            SELECT unit_ID AS [ID],unit_Name AS [Name],
	                            (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='民用建筑合同')   AS A,
	                            (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工业建筑合同')   AS B,
	                            (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程勘察合同')   AS C,
	                            (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程监理合同')   AS D,
	                            (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程施工合同')   AS E,
	                            (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name)  {0} AND cpr_Type='工程咨询合同')   AS F,
	                            (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name)  {0} AND cpr_Type='工程总承包合同') AS G,
	                            (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name)  {0} AND cpr_Type='工程代建合同')   AS H,
	                            (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name)  {0} AND cpr_Type='项目协议')       AS I 
	                            FROM tg_unit U
	                            )A
	                            WHERE 1=1 {1}
	                            UNION
	                            SELECT 300 AS [ID],'全院总计' AS [Name],
	                            (SELECT Count(*) FROM cm_Coperation WHERE 1=1  {0} AND cpr_Type='民用建筑合同')   AS A,
	                            (SELECT Count(*) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工业建筑合同')   AS B,
	                            (SELECT Count(*) FROM cm_Coperation WHERE 1=1  {0} AND cpr_Type='工程勘察合同')   AS C,
	                            (SELECT Count(*) FROM cm_Coperation WHERE 1=1  {0} AND cpr_Type='工程监理合同')   AS D,
	                            (SELECT Count(*) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程施工合同')   AS E,
	                            (SELECT Count(*) FROM cm_Coperation WHERE  1=1 {0} AND cpr_Type='工程咨询合同')   AS F,
	                            (SELECT Count(*) FROM cm_Coperation WHERE 1=1  {0} AND cpr_Type='工程总承包合同') AS G,
	                            (SELECT Count(*) FROM cm_Coperation WHERE  1=1 {0} AND cpr_Type='工程代建合同')   AS H,
	                            (SELECT Count(*) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='项目协议')       AS I 
                            ) T Order By [ID]", year, strWhere);
            }
            else if (type == 2)
            {
                sql = string.Format(@" SELECT [ID],[Name],A,B,C,D,E,F,G,H,I
                                    FROM(
                                       SELECT  [ID],[Name],A,B,C,D,E,F,G,H,I FROM 
                                       (
	                                    SELECT unit_ID AS [ID],unit_Name AS [Name],
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='民用建筑合同'),0)   AS A,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工业建筑合同'),0)   AS B,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程勘察合同'),0)   AS C,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程监理合同'),0)   AS D,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程施工合同'),0)   AS E,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程咨询合同'),0)   AS F,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程总承包合同'),0) AS G,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程代建合同'),0)   AS H,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='项目协议'),0)       AS I 
	                                    FROM tg_unit U
	                                    ) A
	                                    WHERE 1=1 {1}
	                                    UNION
	                                    SELECT 300 AS [ID],'全院总计' AS [Name],
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='民用建筑合同'),0)   AS A,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工业建筑合同'),0)   AS B,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程勘察合同'),0)   AS C,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程监理合同'),0)   AS D,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程施工合同'),0)   AS E,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程咨询合同'),0)   AS F,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程总承包合同'),0) AS G,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程代建合同'),0)   AS H,
	                                    isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='项目协议'),0)       AS I 
                                    ) T Order By [ID]", year, strWhere);
            }
            else if (type == 3)
            {
                sql = string.Format(@" SELECT [ID],[Name],A,B,C,D,E,F,G,H,I
                                    FROM(
                                       SELECT [ID],[Name],A,B,C,D,E,F,G,H,I FROM 
                                       (
	                                    SELECT unit_ID AS [ID],unit_Name AS [Name],
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='民用建筑合同'),0)   AS A,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工业建筑合同'),0)   AS B,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程勘察合同'),0)   AS C,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程监理合同'),0)   AS D,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程施工合同'),0)   AS E,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程咨询合同'),0)   AS F,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程总承包合同'),0) AS G,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='工程代建合同'),0)   AS H,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND cpr_Type='项目协议'),0)       AS I 
	                                    FROM tg_unit U
	                                    ) A
	                                    WHERE 1=1 {1}
	                                    UNION
	                                    SELECT 300 AS [ID],'全院总计' AS [Name],
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='民用建筑合同'),0)   AS A,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工业建筑合同'),0)   AS B,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程勘察合同'),0)   AS C,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程监理合同'),0)   AS D,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程施工合同'),0)   AS E,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程咨询合同'),0)   AS F,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程总承包合同'),0) AS G,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='工程代建合同'),0)   AS H,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND cpr_Type='项目协议'),0)       AS I 
                                    ) T Order By [ID]", year, strWhere);
            }
            else if (type == 4)
            {
                sql = string.Format(@" SELECT [ID],[Name],A,B,C,D,E,F,G,H,I
                                        FROM(
                                        SELECT [ID],[Name],A,B,C,D,E,F,G,H,I FROM 
                                        (
	                                        SELECT unit_ID AS [ID],unit_Name AS [Name],
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND cpr_Type='民用建筑合同')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS A,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND cpr_Type='工业建筑合同')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS B,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND cpr_Type='工程勘察合同')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS C,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND cpr_Type='工程监理合同')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS D,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND cpr_Type='工程施工合同')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS E,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND cpr_Type='工程咨询合同')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS F,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND cpr_Type='工程总承包合同')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS G,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND cpr_Type='工程代建合同')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS H,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND cpr_Type='项目协议')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS I
	                                        FROM tg_unit U
	                                        ) A
	                                        WHERE 1=1 {2}
	                                        UNION
	                                        SELECT 300 AS [ID],'全院总计' AS [Name],
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Type='民用建筑合同')) AND Status<>'B' AND (InAcountTime BETWEEN  '{0}' AND '{1}')),0) AS A,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Type='工业建筑合同')) AND Status<>'B' AND (InAcountTime BETWEEN  '{0}' AND '{1}')),0) AS B,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Type='工程勘察合同')) AND Status<>'B' AND (InAcountTime BETWEEN  '{0}' AND '{1}')),0) AS C,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Type='工程监理合同')) AND Status<>'B' AND (InAcountTime BETWEEN  '{0}' AND '{1}')),0) AS D,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Type='工程施工合同')) AND Status<>'B' AND (InAcountTime BETWEEN  '{0}' AND '{1}')),0) AS E,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Type='工程咨询合同')) AND Status<>'B' AND (InAcountTime BETWEEN  '{0}' AND '{1}')),0) AS F,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Type='工程总承包合同')) AND Status<>'B' AND (InAcountTime BETWEEN  '{0}' AND '{1}')),0) AS G,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Type='工程代建合同')) AND Status<>'B' AND (InAcountTime BETWEEN  '{0}' AND '{1}')),0) AS H,
	                                        isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Type='项目协议')) AND Status<>'B' AND (InAcountTime BETWEEN  '{0}' AND '{1}')),0) AS I
                                        ) T Order By [ID]	", starttime, endtime, strWhere);
            }
            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            return ds;

        }
        /// <summary>
        /// 综合统计项目性质
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public DataSet CountCoperationIndustry(string year, string starttime, string endtime, int? unitid, int type)
        {
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@starttime",starttime),
                new SqlParameter("@endtime",endtime),
                new SqlParameter("@unit_id",unitid),
                new SqlParameter("@type",type)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountCprByIndustry", paras, "Table1");
        }

        /// <summary>
        /// 综合统计项目性质
        /// </summary>
        /// <param name="year"></param>
        /// <param name="starttime"></param>
        /// <param name="endtime"></param>
        /// <param name="unitid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public DataSet CountCoperationIndustryBySql(string year, string starttime, string endtime, string strWhere, int type)
        {
            string sql = "";

            if (type == 1)
            {
                sql = string.Format(@"  SELECT [ID],[Name],A,B,C,D,E,F,G,H,I
                                        FROM (
                                            SELECT [ID],[Name],A,B,C,D,E,F,G,H,I FROM 
                                            (
	                                        SELECT unit_ID AS [ID],unit_Name AS [Name],
	                                        (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='公建')   AS A,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='房地产') AS B,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='市政')   AS C,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='医院')   AS D,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='电力')   AS E,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='通信')   AS F,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='银行')   AS G,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='学校')   AS H,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='涉外')   AS I 
	                                        FROM tg_unit U
	                                        )A
	                                        WHERE 1=1 {1}
	                                        UNION
	                                        SELECT 300 AS [ID],'全院总计' AS[Name],
	                                        (SELECT Count(*) FROM cm_Coperation WHERE 1=1 {0} AND Industry='公建')   AS A,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE 1=1 {0} AND Industry='房地产') AS B,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE 1=1 {0} AND Industry='市政')   AS C,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE 1=1 {0} AND Industry='医院')   AS D,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE 1=1 {0} AND Industry='电力')   AS E,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE 1=1 {0} AND Industry='通信')   AS F,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE 1=1 {0} AND Industry='银行')   AS G,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE 1=1 {0} AND Industry='学校')   AS H,
	                                        (SELECT Count(*) FROM cm_Coperation WHERE 1=1 {0} AND Industry='涉外')   AS I 
                                        ) AS T Order by [ID]", year, strWhere);
            }
            else if (type == 2)
            {
                sql = string.Format(@" SELECT [ID],[Name],A,B,C,D,E,F,G,H,I
                                        FROM (
                                        SELECT [ID],[Name],A,B,C,D,E,F,G,H,I FROM 
                                          (
	                                        SELECT unit_ID AS [ID],unit_Name AS [Name],
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='公建'),0)  AS A,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='房地产'),0) AS B,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='市政'),0)   AS C,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='医院'),0)   AS D,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='电力'),0)   AS E,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='通信'),0)   AS F,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='银行'),0)   AS G,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='学校'),0)   AS H,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='涉外'),0)   AS I 
	                                        FROM tg_unit U
	                                        ) A
	                                        WHERE 1=1 {1}
	                                        UNION
	                                        SELECT 300 AS [ID],'全院总计' AS[Name],
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND Industry='公建'),0)  AS A,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND Industry='房地产'),0) AS B,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND Industry='市政'),0)   AS C,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND Industry='医院'),0)   AS D,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND Industry='电力'),0)   AS E,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND Industry='通信'),0)   AS F,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND Industry='银行'),0)   AS G,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND Industry='学校'),0)   AS H,
	                                        isnull((SELECT SUM(cpr_Acount) FROM cm_Coperation WHERE 1=1 {0} AND Industry='涉外'),0)   AS I 
                                        ) AS T Order by [ID]", year, strWhere);
            }
            else if (type == 3)
            {
                sql = string.Format(@" SELECT [ID],[Name],A,B,C,D,E,F,G,H,I
                                    FROM (
                                       SELECT [ID],[Name],A,B,C,D,E,F,G,H,I FROM (
	                                    SELECT unit_ID AS [ID],unit_Name AS [Name],
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name){0} AND Industry='公建'),0)   AS A,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='房地产'),0) AS B,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name){0} AND Industry='市政'),0)   AS C,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='医院'),0)   AS D,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='电力'),0)   AS E,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='通信'),0)   AS F,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='银行'),0)   AS G,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='学校'),0)   AS H,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE (cpr_Unit=U.unit_Name) {0} AND Industry='涉外'),0)   AS I 
	                                    FROM tg_unit U ) A
	                                    WHERE 1=1 {1}
	                                    UNION
	                                    SELECT 300 AS [ID],'全院总计' AS[Name],
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND Industry='公建'),0)   AS A,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND Industry='房地产'),0) AS B,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND Industry='市政'),0)   AS C,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND Industry='医院'),0)   AS D,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND Industry='电力'),0)   AS E,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0}  AND Industry='通信'),0)   AS F,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND Industry='银行'),0)   AS G,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND Industry='学校'),0)   AS H,
	                                    isnull((SELECT SUM(Convert(decimal(10,2),BuildArea)) FROM cm_Coperation WHERE 1=1 {0} AND Industry='涉外'),0)   AS I 
                                    ) AS T Order by [ID]", year, strWhere);
            }
            else if (type == 4)
            {
                sql = string.Format(@" SELECT [ID],[Name],A,B,C,D,E,F,G,H,I
                                    FROM (
                                        SELECT [ID],[Name],A,B,C,D,E,F,G,H,I FROM 
                                        (
	                                    SELECT unit_ID AS [ID],unit_Name AS [Name],
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND Industry='公建')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS A,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND Industry='房地产')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS B,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND Industry='市政')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS C,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND Industry='医院')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS D,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND Industry='电力')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS E,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND Industry='通信')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS F,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND Industry='银行')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS G,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND Industry='学校')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS H,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where cpr_Unit=U.unit_Name AND Industry='涉外')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS I
	                                    FROM tg_unit U ) A
	                                    WHERE 1=1 {2}
	                                    UNION
	                                    SELECT 300 AS [ID],'全院总计' AS[Name],
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where Industry='公建')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS A,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where Industry='房地产')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS B,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where Industry='市政')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS C,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where Industry='医院')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS D,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where Industry='电力')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS E,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where Industry='通信')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS F,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where Industry='银行')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS G,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where Industry='学校')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS H,
	                                    isnull((Select SUM(Acount) From cm_ProjectCharge Where (cprID IN (Select cpr_ID From cm_Coperation Where Industry='涉外')) AND Status<>'B' AND (InAcountTime BETWEEN '{0}' AND '{1}')),0) AS I
                                    ) AS T Order by [ID]	", starttime, endtime, strWhere);
            }
            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            return ds;
        }
        //合同收款明细
        public DataSet GetCoperationListExport(string sqlwhere)
        {
            string sql = string.Format(@"select *,
                                         Convert(decimal(18,2),(Select isnull(SUM(Acount),0.00) From cm_ProjectCharge Where Status<>'B' AND cprID=C.cpr_Id )) as ssze,
					                    convert(varchar(10),cpr_SignDate,120) as qdrq,
                                        CONVERT(nvarchar(100),C.InsertDate,23) as lrsj,
					                    Convert(decimal(18,2),(case when cpr_Acount<>0 then (Select isnull(SUM(Acount),0.00) From cm_ProjectCharge Where Status<>'B' AND cprID=C.cpr_Id )/cpr_Acount *100 else 0 end)) as sfjd 
                                 from cm_Coperation C where 1=1 {0} order by C.cpr_id desc", sqlwhere);
            return DbHelperSQL.Query(sql);
        }
        public DataSet GetScrCoperationListExport(string sqlwhere, string sbTime, string sbCharge)
        {
            string sql = string.Format(@"select * from (select *,
                                         Convert(decimal(18,2),(Select isnull(SUM(Acount),0.00) From cm_ProjectCharge Where Status<>'B' AND cprID=C.cpr_Id " + sbTime + " )) as ssze, " + @"
					                    convert(varchar(10),cpr_SignDate,120) as qdrq,
                                        CONVERT(nvarchar(100),C.InsertDate,23) as lrsj,
					                    Convert(decimal(18,2),(case when cpr_Acount<>0 then (Select isnull(SUM(Acount),0.00) From cm_ProjectCharge Where Status<>'B' AND cprID=C.cpr_Id  " + sbTime + " )/cpr_Acount *100 else 0 end)) as sfjd  " + @"
                                 from cm_Coperation C " + sbCharge + " ) CC where 1=1 {0} order by CC.cpr_id desc", sqlwhere);
            return DbHelperSQL.Query(sql);
        }
        /// <summary>
        /// 合同审批导出
        /// </summary>
        /// <param name="sqlwhere"></param>
        /// <returns></returns>
        public DataSet GetCoperationAuditListExport(string sqlwhere)
        {
            string sql = @"select *,(select top 1 SysNo from cm_AuditRecord where CoperationSysNo=cpr_ID order by SysNo DESC) as AuditSysNo,
					 (select top 1 Status from cm_AuditRecord where CoperationSysNo=cpr_ID order by SysNo DESC) as AuditStatus,
					 (select top 1 ManageLevel from cm_AuditRecord where CoperationSysNo=cpr_ID order by SysNo DESC) as ManageLevel,
					 (select top 1 NeedLegalAdviser from cm_AuditRecord where CoperationSysNo=cpr_ID order by SysNo DESC) as NeedLegalAdviser,
					 (select cst_name from cm_CustomerInfo where Cst_Id=C.cst_Id) as cst_name,
					 convert(varchar(10),cpr_SignDate2,120) as qdrq,
                     convert(varchar(10),cpr_SignDate,120) as tjrq,
					 convert(varchar(10),cpr_DoneDate,120) as wcrq,
                    (case when (ChgPeople='''' or ChgPeople is null) then (select mem_Name from tg_member where mem_ID=PMUserID) else ChgPeople end) as PMUserName,
                    CONVERT(nvarchar(100),C.InsertDate,23) as lrsj,
					(Select mem_Name From tg_member Where mem_ID=InsertUserID) AS InsertUser					
				from cm_Coperation C where 1=1 " + sqlwhere + " order by cpr_id desc ";
            return DbHelperSQL.Query(sql);
        }
        /// <summary>
        /// 合同修改审批导出
        /// </summary>
        /// <param name="sqlwhere"></param>
        /// <returns></returns>
        public DataSet GetCoperationAuditEditListExport(string sqlwhere)
        {
            string sql = @"select *,M.SysNo as AuditSysNo,M.Status as AuditStatus,M.ManageLevel as ManageLevel,M.NeedLegalAdviser as NeedLegalAdviser,
					 (select cst_name from cm_CustomerInfo where Cst_Id=C.cst_Id) as cst_name,
isnull((select top 1 M.Status 
                from cm_AuditRecordEdit M  where m.CoperationSysNo=C.cpr_Id  order
                 by m.SysNo desc),'') as statusE,
					 convert(varchar(10),cpr_SignDate2,120) as qdrq,
 convert(varchar(10),cpr_SignDate,120) as tjrq,
					 convert(varchar(10),cpr_DoneDate,120) as wcrq,
                    (case when (ChgPeople='''' or ChgPeople is null) then (select mem_Name from tg_member where mem_ID=PMUserID) else ChgPeople end) as PMUserName,
                    CONVERT(nvarchar(100),C.InsertDate,23) as lrsj,
					(Select mem_Name From tg_member Where mem_ID=InsertUserID) AS InsertUser					
				from cm_Coperation C
				left join cm_AuditRecord M on M.CoperationSysNo=c.cpr_Id where (M.Status='J' or (M.Status='H' and M.ManageLevel=1 )) " + sqlwhere + " order by cpr_id desc ";
            return DbHelperSQL.Query(sql);
        }
        /// <summary>
        /// 合同收费导出
        /// </summary>
        /// <param name="sqlwhere"></param>
        /// <returns></returns>
        public DataSet GetCoperationChargeExport(string sqlwhere, string str)
        {
            string sql = string.Format(@"	select *,
					Convert(decimal(18,2),(Select isnull(SUM(Acount),0.00) From cm_ProjectCharge Where Status<>'B' {0} AND cprID=C.cpr_Id)) as ssze,
					convert(varchar(10),cpr_SignDate2,120) as qdrq,
                     convert(varchar(10),cpr_SignDate,120) as tjrq,
					 convert(varchar(10),cpr_DoneDate,120) as wcrq,
					Convert(decimal(18,2),(case when cpr_ShijiAcount<>0 then (Select isnull(SUM(Acount),0.00) From cm_ProjectCharge Where Status<>'B' AND cprID=C.cpr_Id )/cpr_ShijiAcount *100 else 0 end)) as sfjd,
					Convert(decimal(18,2),(cpr_ShijiAcount-(Select isnull(SUM(Acount),0.00) From cm_ProjectCharge Where Status<>'B' {0} AND cprID=C.cpr_Id ))) as wsk,
                    (Select mem_Name From tg_member Where mem_ID=InsertUserID) AS InsertUser,
                    CONVERT(nvarchar(100),C.InsertDate,23) as lrsj,
                    (case when (ChgPeople='''' or ChgPeople is null) then (select mem_Name from tg_member where mem_ID=PMUserID) else ChgPeople end) as PMUserName
				from cm_Coperation C where 1=1 " + sqlwhere + " order by cpr_id desc ", str);
            return DbHelperSQL.Query(sql);
        }
        #endregion  Method
    }
}

