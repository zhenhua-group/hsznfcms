﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using System.Data.SqlClient;
using TG.DBUtility;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class CoperationAcountDA
    {
        //按时间段统计合同额度
        public CoperationAcountOfMoneyViewEntity GetCoperationAccountsOfYear(int year, string unit)
        {
            //部门
            if (unit != "")
            {
                unit = " AND cpr_Unit='" + unit + "' ";
            }
            string sql = string.Format("SELECT {0} as year, sum(cpr_Acount) as money FROM [dbo].[cm_Coperation] where [cpr_SignDate] between N'{0}-01-01' and N'{0}-12-31' {1}", year, unit);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            CoperationAcountOfMoneyViewEntity resultEntity = EntityBuilder<CoperationAcountOfMoneyViewEntity>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }
        //获取合同类型
        public List<string> GetCoperationTypeList()
        {
            string sql = "SELECT [dic_Name] FROM [dbo].[cm_Dictionary] where dic_Type=N'cpr_fl'";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<string> resultList = new List<string>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    resultList.Add(reader["dic_Name"].ToString());
                }
                reader.Close();
            }

            return resultList;
        }
        //获取对应时间段不同合同类型的合同额
        public CoperationAcountOfMoneyViewEntity GetCoperationAccountsByTyoeOfYear(int year, string typeString, string unit)
        {
            //部门
            if (unit != "")
            {
                unit = " AND cpr_Unit='" + unit + "' ";
            }

            string sql = string.Format("SELECT {0} as year, sum(cpr_Acount) as money FROM [dbo].[cm_Coperation] where cpr_Type like N'%{1}%' and [cpr_SignDate] between N'{0}-01-01' and N'{0}-12-31' {2}", year, typeString, unit);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            CoperationAcountOfMoneyViewEntity resultEntity = EntityBuilder<CoperationAcountOfMoneyViewEntity>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }
        //获取对应时间段合同数量
        public CoperationCountViewEntity GetCoperationCountOfYear(int year, string unit)
        {
            //部门
            if (unit != "")
            {
                unit = " AND cpr_Unit='" + unit + "' ";
            }

            string sql = string.Format("SELECT {0} as year, count(*) as count FROM [dbo].[cm_Coperation] where [cpr_SignDate] between N'{0}-01-01' and N'{0}-12-31' {1}", year, unit);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            CoperationCountViewEntity resultEntity = EntityBuilder<CoperationCountViewEntity>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }
        //获取对应时间段不同合同类型的合同数量
        public CoperationCountViewEntity GetCoperationCountByTypeOfYear(int year, string type, string unit)
        {
            //部门
            if (unit != "")
            {
                unit = " AND cpr_Unit='" + unit + "' ";
            }

            string sql = string.Format("SELECT {0} as year, count(*) as count FROM [dbo].[cm_Coperation] where cpr_Type like N'%{1}%' and [cpr_SignDate] between N'{0}-01-01' and N'{0}-12-31' {2}", year, type, unit);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            CoperationCountViewEntity resultEntity = EntityBuilder<CoperationCountViewEntity>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;

        }
        //获取合同执行情况
        public int GetCoperationCountByStatusOfYear(int year, string type, string unit)
        {
            //部门
            if (unit != "")
            {
                unit = " AND cpr_Unit='" + unit + "' ";
            }
            //修改于20130516
            string sql = string.Format(@"SELECT count(*) 
                                                            From cm_Coperation CP inner join cm_Project PJ ON CP.cpr_ID = PJ.CoperationSysNo 
                                                            Where (Select pro_status From tg_project Where pro_ID=PJ.ReferenceSysNo)='{1}' AND [cpr_SignDate] between N'{0}-01-01' and N'{0}-12-31' {2}", year, type, unit);

            object objResult = DbHelperSQL.GetSingle(sql);

            return objResult == null ? 0 : Convert.ToInt32(objResult);
        }

        public List<CoperationListViewEntity> GetCoperationList(string whereCondition, int pageCurrent, int pageSize, out int dataCount)
        {
            string sql = string.Format(@"SELECT TOP {0} *
                                                        FROM cm_Coperation c
                                                        WHERE 1=1 {2} and cpr_Id not in
                                                                  (
                                                                      SELECT TOP {3} cpr_Id 
                                                                      FROM cm_Coperation c 
                                                                      where 1=1 {2}
                                                                      order by c.cpr_Id DESC
                                                                  )
                                                        ORDER BY cpr_Id desc", pageSize, pageCurrent - 1, whereCondition, pageSize * (pageCurrent - 1));

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            string countSql = "select COUNT(*) from cm_Coperation where 1=1 " + whereCondition;

            object objCount = DbHelperSQL.GetSingle(countSql);

            dataCount = objCount == null ? 0 : Convert.ToInt32(objCount);

            List<CoperationListViewEntity> resultList = EntityBuilder<CoperationListViewEntity>.BuilderEntityList(reader);

            return resultList;
        }

        public List<CoperationListViewEntityTwo> GetCoperationStatusList(string whereCondition, int pageCurrent, int pageSize, out int dataCount)
        {
            string sql = string.Format(@"SELECT TOP {0} 
	                                                     c.[cpr_Id]
	                                                    ,c.[cst_Id]
	                                                    ,c.[cpr_No]
	                                                    ,c.[cpr_Type]
	                                                    ,c.[cpr_Type2]
	                                                    ,c.[cpr_Name]
	                                                    ,c.[cpr_Unit]
	                                                    ,c.[cpr_Acount]
	                                                    ,c.[cpr_ShijiAcount]
	                                                    ,c.[cpr_Touzi]
	                                                    ,c.[cpr_ShijiTouzi]
	                                                    ,c.[cpr_Process]
	                                                    ,c.[cpr_SignDate]
	                                                    ,c.[cpr_DoneDate]
	                                                    ,c.[cpr_Mark]
	                                                    ,c.[BuildArea]
	                                                    ,c.[ChgPeople]
	                                                    ,c.[ChgPhone]
	                                                    ,c.[ChgJia]
	                                                    ,c.[ChgJiaPhone]
	                                                    ,c.[BuildPosition]
	                                                    ,c.[Industry]
	                                                    ,c.[BuildUnit]
	                                                    ,c.[BuildSrc]
	                                                    ,c.[TableMaker]
	                                                    ,c.[RegTime]
	                                                    ,c.[UpdateBy]
	                                                    ,c.[LastUpdate]
	                                                    ,tp.pro_Status
	                                                    FROM ([dbo].[cm_Coperation] c left join  cm_Project cp on cp.CoperationSysNo = c.cpr_Id) 
	                                                    left join tg_project tp on tp.pro_ID = cp.ReferenceSysNo
	                                                    where c.cpr_Id not in (
		                                                    SELECT TOP {3} c.cpr_id FROM 
		                                                    ([dbo].[cm_Coperation] c left join cm_Project cp on cp.CoperationSysNo = c.cpr_Id) 
		                                                    left join tg_project tp on tp.pro_ID = cp.ReferenceSysNo 
		                                                    where 1=1 {2}
		                                                    ORDER BY cpr_Id desc
	                                                    )", pageSize, pageCurrent, whereCondition, pageSize * (pageCurrent - 1));

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<CoperationListViewEntityTwo> resultList = EntityBuilder<CoperationListViewEntityTwo>.BuilderEntityList(reader);

            string countSql = "select COUNT(*) from ([dbo].[cm_Coperation] c left join  cm_Project cp on cp.CoperationSysNo = c.cpr_Id) left join tg_project tp on tp.pro_ID = cp.ReferenceSysNo where 1=1 " + whereCondition;

            object objCount = DbHelperSQL.GetSingle(countSql);

            dataCount = objCount == null ? 0 : Convert.ToInt32(objCount);

            return resultList;
        }
    }
}
