﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_Dictionary
    /// </summary>
    public partial class cm_Dictionary
    {
        public cm_Dictionary()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_Dictionary model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.dic_Name != null)
            {
                strSql1.Append("dic_Name,");
                strSql2.Append("'" + model.dic_Name + "',");
            }
            if (model.dic_Type != null)
            {
                strSql1.Append("dic_Type,");
                strSql2.Append("'" + model.dic_Type + "',");
            }
            if (model.dic_info != null)
            {
                strSql1.Append("dic_info,");
                strSql2.Append("'" + model.dic_info + "',");
            }
            strSql.Append("insert into cm_Dictionary(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_Dictionary model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_Dictionary set ");
            if (model.dic_Name != null)
            {
                strSql.Append("dic_Name='" + model.dic_Name + "',");
            }
            else
            {
                strSql.Append("dic_Name= null ,");
            }
            if (model.dic_Type != null)
            {
                strSql.Append("dic_Type='" + model.dic_Type + "',");
            }
            else
            {
                strSql.Append("dic_Type= null ,");
            }
            if (model.dic_info != null)
            {
                strSql.Append("dic_info='" + model.dic_info + "',");
            }
            else
            {
                strSql.Append("dic_info= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where ID=" + model.ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_Dictionary ");
            strSql.Append(" where ID=" + ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_Dictionary ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_Dictionary GetModel(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" ID,dic_Name,dic_Type,dic_info ");
            strSql.Append(" from cm_Dictionary ");
            strSql.Append(" where ID=" + ID + "");
            TG.Model.cm_Dictionary model = new TG.Model.cm_Dictionary();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["dic_Name"] != null && ds.Tables[0].Rows[0]["dic_Name"].ToString() != "")
                {
                    model.dic_Name = ds.Tables[0].Rows[0]["dic_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["dic_Type"] != null && ds.Tables[0].Rows[0]["dic_Type"].ToString() != "")
                {
                    model.dic_Type = ds.Tables[0].Rows[0]["dic_Type"].ToString();
                }
                if (ds.Tables[0].Rows[0]["dic_info"] != null && ds.Tables[0].Rows[0]["dic_info"].ToString() != "")
                {
                    model.dic_info = ds.Tables[0].Rows[0]["dic_info"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_Dictionary GetModel(string name)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" ID,dic_Name,dic_Type,dic_info ");
            strSql.Append(" from cm_Dictionary ");
            strSql.Append(" where dic_Name='" + name + "'");
            TG.Model.cm_Dictionary model = new TG.Model.cm_Dictionary();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["dic_Name"] != null && ds.Tables[0].Rows[0]["dic_Name"].ToString() != "")
                {
                    model.dic_Name = ds.Tables[0].Rows[0]["dic_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["dic_Type"] != null && ds.Tables[0].Rows[0]["dic_Type"].ToString() != "")
                {
                    model.dic_Type = ds.Tables[0].Rows[0]["dic_Type"].ToString();
                }
                if (ds.Tables[0].Rows[0]["dic_info"] != null && ds.Tables[0].Rows[0]["dic_info"].ToString() != "")
                {
                    model.dic_info = ds.Tables[0].Rows[0]["dic_info"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,dic_Name,dic_Type,dic_info ");
            strSql.Append(" FROM cm_Dictionary ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,dic_Name,dic_Type,dic_info ");
            strSql.Append(" FROM cm_Dictionary ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_Dictionary ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from cm_Dictionary ");
            if (strWhere.Trim() != "")
            {
                sb.Append(" where " + strWhere);
            }
            if (orderby.Trim() != "")
            {
                sb.Append(" Order by " + orderby);
            }
            return DbHelperSQL.Query(sb.ToString(), startIndex, endIndex);
        }

        #endregion  Method
    }
}

