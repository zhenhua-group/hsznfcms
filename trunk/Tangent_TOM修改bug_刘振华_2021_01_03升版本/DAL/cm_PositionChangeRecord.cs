﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_PositionChangeRecord
	/// </summary>
	public partial class cm_PositionChangeRecord
	{
		public cm_PositionChangeRecord()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_PositionChangeRecord model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_PositionChangeRecord(");
			strSql.Append("mem_ID,MagRole,MagRoleName,MagRole2,MagRoleName2,TecRole,TecRoleName,TecRole2,TecRoleName2,JiaoTong,JiaoTong2,Tongxun,Tongxun2,ChangeDate,InsertUserID)");
			strSql.Append(" values (");
			strSql.Append("@mem_ID,@MagRole,@MagRoleName,@MagRole2,@MagRoleName2,@TecRole,@TecRoleName,@TecRole2,@TecRoleName2,@JiaoTong,@JiaoTong2,@Tongxun,@Tongxun2,@ChangeDate,@InsertUserID)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_ID", SqlDbType.Int,4),
					new SqlParameter("@MagRole", SqlDbType.Int,4),
					new SqlParameter("@MagRoleName", SqlDbType.NVarChar,50),
					new SqlParameter("@MagRole2", SqlDbType.Int,4),
					new SqlParameter("@MagRoleName2", SqlDbType.NVarChar,50),
					new SqlParameter("@TecRole", SqlDbType.Int,4),
					new SqlParameter("@TecRoleName", SqlDbType.NVarChar,50),
					new SqlParameter("@TecRole2", SqlDbType.Int,4),
					new SqlParameter("@TecRoleName2", SqlDbType.NVarChar,50),
					new SqlParameter("@JiaoTong", SqlDbType.Int,4),
					new SqlParameter("@JiaoTong2", SqlDbType.Int,4),
					new SqlParameter("@Tongxun", SqlDbType.Int,4),
					new SqlParameter("@Tongxun2", SqlDbType.Int,4),
					new SqlParameter("@ChangeDate", SqlDbType.DateTime),
					new SqlParameter("@InsertUserID", SqlDbType.Int,4)};
			parameters[0].Value = model.mem_ID;
			parameters[1].Value = model.MagRole;
			parameters[2].Value = model.MagRoleName;
			parameters[3].Value = model.MagRole2;
			parameters[4].Value = model.MagRoleName2;
			parameters[5].Value = model.TecRole;
			parameters[6].Value = model.TecRoleName;
			parameters[7].Value = model.TecRole2;
			parameters[8].Value = model.TecRoleName2;
			parameters[9].Value = model.JiaoTong;
			parameters[10].Value = model.JiaoTong2;
			parameters[11].Value = model.Tongxun;
			parameters[12].Value = model.Tongxun2;
			parameters[13].Value = model.ChangeDate;
			parameters[14].Value = model.InsertUserID;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_PositionChangeRecord model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_PositionChangeRecord set ");
			strSql.Append("mem_ID=@mem_ID,");
			strSql.Append("MagRole=@MagRole,");
			strSql.Append("MagRoleName=@MagRoleName,");
			strSql.Append("MagRole2=@MagRole2,");
			strSql.Append("MagRoleName2=@MagRoleName2,");
			strSql.Append("TecRole=@TecRole,");
			strSql.Append("TecRoleName=@TecRoleName,");
			strSql.Append("TecRole2=@TecRole2,");
			strSql.Append("TecRoleName2=@TecRoleName2,");
			strSql.Append("JiaoTong=@JiaoTong,");
			strSql.Append("JiaoTong2=@JiaoTong2,");
			strSql.Append("Tongxun=@Tongxun,");
			strSql.Append("Tongxun2=@Tongxun2,");
			strSql.Append("ChangeDate=@ChangeDate,");
			strSql.Append("InsertUserID=@InsertUserID");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_ID", SqlDbType.Int,4),
					new SqlParameter("@MagRole", SqlDbType.Int,4),
					new SqlParameter("@MagRoleName", SqlDbType.NVarChar,50),
					new SqlParameter("@MagRole2", SqlDbType.Int,4),
					new SqlParameter("@MagRoleName2", SqlDbType.NVarChar,50),
					new SqlParameter("@TecRole", SqlDbType.Int,4),
					new SqlParameter("@TecRoleName", SqlDbType.NVarChar,50),
					new SqlParameter("@TecRole2", SqlDbType.Int,4),
					new SqlParameter("@TecRoleName2", SqlDbType.NVarChar,50),
					new SqlParameter("@JiaoTong", SqlDbType.Int,4),
					new SqlParameter("@JiaoTong2", SqlDbType.Int,4),
					new SqlParameter("@Tongxun", SqlDbType.Int,4),
					new SqlParameter("@Tongxun2", SqlDbType.Int,4),
					new SqlParameter("@ChangeDate", SqlDbType.DateTime),
					new SqlParameter("@InsertUserID", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.mem_ID;
			parameters[1].Value = model.MagRole;
			parameters[2].Value = model.MagRoleName;
			parameters[3].Value = model.MagRole2;
			parameters[4].Value = model.MagRoleName2;
			parameters[5].Value = model.TecRole;
			parameters[6].Value = model.TecRoleName;
			parameters[7].Value = model.TecRole2;
			parameters[8].Value = model.TecRoleName2;
			parameters[9].Value = model.JiaoTong;
			parameters[10].Value = model.JiaoTong2;
			parameters[11].Value = model.Tongxun;
			parameters[12].Value = model.Tongxun2;
			parameters[13].Value = model.ChangeDate;
			parameters[14].Value = model.InsertUserID;
			parameters[15].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_PositionChangeRecord ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_PositionChangeRecord ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_PositionChangeRecord GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,mem_ID,MagRole,MagRoleName,MagRole2,MagRoleName2,TecRole,TecRoleName,TecRole2,TecRoleName2,JiaoTong,JiaoTong2,Tongxun,Tongxun2,ChangeDate,InsertUserID from cm_PositionChangeRecord ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_PositionChangeRecord model=new TG.Model.cm_PositionChangeRecord();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_PositionChangeRecord DataRowToModel(DataRow row)
		{
			TG.Model.cm_PositionChangeRecord model=new TG.Model.cm_PositionChangeRecord();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["mem_ID"]!=null && row["mem_ID"].ToString()!="")
				{
					model.mem_ID=int.Parse(row["mem_ID"].ToString());
				}
				if(row["MagRole"]!=null && row["MagRole"].ToString()!="")
				{
					model.MagRole=int.Parse(row["MagRole"].ToString());
				}
				if(row["MagRoleName"]!=null)
				{
					model.MagRoleName=row["MagRoleName"].ToString();
				}
				if(row["MagRole2"]!=null && row["MagRole2"].ToString()!="")
				{
					model.MagRole2=int.Parse(row["MagRole2"].ToString());
				}
				if(row["MagRoleName2"]!=null)
				{
					model.MagRoleName2=row["MagRoleName2"].ToString();
				}
				if(row["TecRole"]!=null && row["TecRole"].ToString()!="")
				{
					model.TecRole=int.Parse(row["TecRole"].ToString());
				}
				if(row["TecRoleName"]!=null)
				{
					model.TecRoleName=row["TecRoleName"].ToString();
				}
				if(row["TecRole2"]!=null && row["TecRole2"].ToString()!="")
				{
					model.TecRole2=int.Parse(row["TecRole2"].ToString());
				}
				if(row["TecRoleName2"]!=null)
				{
					model.TecRoleName2=row["TecRoleName2"].ToString();
				}
				if(row["JiaoTong"]!=null && row["JiaoTong"].ToString()!="")
				{
					model.JiaoTong=int.Parse(row["JiaoTong"].ToString());
				}
				if(row["JiaoTong2"]!=null && row["JiaoTong2"].ToString()!="")
				{
					model.JiaoTong2=int.Parse(row["JiaoTong2"].ToString());
				}
				if(row["Tongxun"]!=null && row["Tongxun"].ToString()!="")
				{
					model.Tongxun=int.Parse(row["Tongxun"].ToString());
				}
				if(row["Tongxun2"]!=null && row["Tongxun2"].ToString()!="")
				{
					model.Tongxun2=int.Parse(row["Tongxun2"].ToString());
				}
				if(row["ChangeDate"]!=null && row["ChangeDate"].ToString()!="")
				{
					model.ChangeDate=DateTime.Parse(row["ChangeDate"].ToString());
				}
				if(row["InsertUserID"]!=null && row["InsertUserID"].ToString()!="")
				{
					model.InsertUserID=int.Parse(row["InsertUserID"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,mem_ID,MagRole,MagRoleName,MagRole2,MagRoleName2,TecRole,TecRoleName,TecRole2,TecRoleName2,JiaoTong,JiaoTong2,Tongxun,Tongxun2,ChangeDate,InsertUserID ");
			strSql.Append(" FROM cm_PositionChangeRecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,mem_ID,MagRole,MagRoleName,MagRole2,MagRoleName2,TecRole,TecRoleName,TecRole2,TecRoleName2,JiaoTong,JiaoTong2,Tongxun,Tongxun2,ChangeDate,InsertUserID ");
			strSql.Append(" FROM cm_PositionChangeRecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_PositionChangeRecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_PositionChangeRecord T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_PositionChangeRecord";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

