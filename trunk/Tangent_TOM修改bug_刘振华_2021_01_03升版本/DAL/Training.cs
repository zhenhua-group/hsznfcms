﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using System.Collections.Generic;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:tg_member
    /// </summary>
    public partial class Training
    {
        public Training()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int AddTrainingInfo(TG.Model.Training model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.Property != null)
            {
                strSql1.Append("Property,");
                strSql2.Append("'" + model.Property + "',");
            }
            if (model.Title != null)
            {
                strSql1.Append("Title,");
                strSql2.Append("'" + model.Title + "',");
            }
            if (model.BeginDate != null)
            {
                strSql1.Append("BeginDate,");
                strSql2.Append("'" + model.BeginDate + "',");
            }
            if (model.EndDate != null)
            {
                strSql1.Append("EndDate,");
                strSql2.Append("'" + model.EndDate + "',");
            }
            strSql.Append("insert into TrainingInfo(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        public int AddTrainingInfoDetail(List<TG.Model.Training> list, int trainingInfoId)
        {
            var sql = @"insert TrainingInfoDetail(TrainingInfoId,BaoMingFee,ZhuShuFee,JiPiaoFee,OtherFee,CanYuRen,UnitName,Total)
values";

            var sql1 = "";
            foreach (var model in list)
            {
                sql1 += "(" + trainingInfoId + "," + model.BaoMingFee + "," + model.ZhuShuFee + "," + model.JiPiaoFee + "," + model.OtherFee + ",'" + model.CanYuRen + "','" + model.UnitName + "'," + model.Total + "),";
            }
            var sql2 = sql1.Substring(0, sql1.Length - 1);
            sql += sql2;
            sql += ";select @@IDENTITY";
            object obj = DbHelperSQL.GetSingle(sql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }

        }
        /// <summary>
        /// 获取所有培训年份
        /// </summary>
        public List<string> GetTrainingYear()
        {
            List<string> list = new List<string>();
            string strSql = " Select Distinct year(BeginDate) From TrainingInfo order by year(BeginDate) desc";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i][0].ToString());
                }
            }
            return list;
        }
        /// <summary>
        /// 获取所有培训月份
        /// </summary>
        public List<string> GetTrainingMonth()
        {
            List<string> list = new List<string>();
            string strSql = " Select Distinct Month(BeginDate) From TrainingInfo order by Month(BeginDate) desc";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i][0].ToString());
                }
            }
            return list;
        }
        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(distinct(t.id)) FROM TrainingInfo t JOIN TrainingInfoDetail detail ON t.Id = detail.TrainingInfoId where 1=1");
            if (strWhere.Trim() != "")
            {
                strSql.Append(strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        ///  数据量小的查询分页
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"   WITH v as(
        SELECT id,ROW_NUMBER() over(order BY insertdate desc) AS row FROM TrainingInfo
        )
          SELECT  * ,
                detail.Id AS detailId
        FROM    TrainingInfo t
                JOIN TrainingInfoDetail detail ON t.Id = detail.TrainingInfoId
         JOIN v ON t.id=v.id WHERE v.row>" + startIndex + " AND v.row<=" + endIndex + strWhere + "ORDER BY t.insertdate desc");
            return DbHelperSQL.Query(sb.ToString());
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateTrainingInfo(TG.Model.Training model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("UPDATE  TrainingInfo SET Property='" + model.Property + "',title='" + model.Title + "',BeginDate='" + model.BeginDate + "',EndDate='" + model.EndDate + "' WHERE id=" + model.Id);
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateTrainingInfoDetail(int id, List<TG.Model.Training> list)
        {

            var sql = @"delete dbo.TrainingInfoDetail WHERE TrainingInfoId=" + id + @";insert TrainingInfoDetail(TrainingInfoId,BaoMingFee,ZhuShuFee,JiPiaoFee,OtherFee,CanYuRen,UnitName,Total)
values";

            var sql1 = "";
            foreach (var model in list)
            {
                sql1 += "(" + id + "," + model.BaoMingFee + "," + model.ZhuShuFee + "," + model.JiPiaoFee + "," + model.OtherFee + ",'" + model.CanYuRen + "','" + model.UnitName + "'," + model.Total + "),";
            }
            var sql2 = sql1.Substring(0, sql1.Length - 1);
            sql += sql2;
            sql += ";select @@IDENTITY";
            int rowsAffected = DbHelperSQL.ExecuteSql(sql);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //各个月的培训数量占总数的比例
        public List<TG.Model.Training> GetCount(string begin, string end)
        {
            string where = " where 1=1";
            if (begin != "")
            {
                where += "and a.BeginDate>='" + Convert.ToDateTime(begin) + "'";
            }
            if (end != "")
            {
                where += " AND a.BeginDate<='" + Convert.ToDateTime(end) + "'";
            }
            string sql = @"SELECT YEAR(a.BeginDate) year,MONTH(a.BeginDate) month,COUNT(*) count FROM TrainingInfo a JOIN TrainingInfoDetail b
    ON a.Property='外部' AND a.id=b.TrainingInfoId" + where + " GROUP BY YEAR(a.BeginDate),MONTH(a.BeginDate)";

            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            List<TG.Model.Training> list = new List<TG.Model.Training>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TG.Model.Training a = new TG.Model.Training();
                    a.Year = dt.Rows[i][0].ToString();
                    a.Month = dt.Rows[i][1].ToString();
                    a.Count = dt.Rows[i][2].ToString();
                    list.Add(a);
                }
            }
            return list;
        }

        //各个月的培训合计占总数的比例
        public List<TG.Model.Training> GetCount1(string begin, string end)
        {
            string where = " where 1=1";
            if (begin != "")
            {
                where += "and a.BeginDate>='" + Convert.ToDateTime(begin) + "'";
            }
            if (end != "")
            {
                where += " AND a.BeginDate<='" + Convert.ToDateTime(end) + "'";
            }

            string sql = @"SELECT YEAR(a.BeginDate) year,MONTH(a.BeginDate) month,sum(b.total) total FROM TrainingInfo a JOIN TrainingInfoDetail b
     ON a.Property='外部' AND a.id=b.TrainingInfoId" + where + " GROUP BY YEAR(a.BeginDate),MONTH(a.BeginDate)";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            List<TG.Model.Training> list = new List<TG.Model.Training>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TG.Model.Training a = new TG.Model.Training();
                    a.Year = dt.Rows[i][0].ToString();
                    a.Month = dt.Rows[i][1].ToString();
                    a.Totals = dt.Rows[i][2].ToString();
                    list.Add(a);
                }
            }
            return list;
        }

        //各个部门的培训人数占总人数的比例
        public List<TG.Model.Training> GetCount2(string begin, string end)
        {
            string where = " where 1=1";
            if (begin != "")
            {
                where += "and a.BeginDate>='" + Convert.ToDateTime(begin) + "'";
            }
            if (end != "")
            {
                where += " AND a.BeginDate<='" + Convert.ToDateTime(end) + "'";
            }


            string sql = @"SELECT b.UnitName ,COUNT(*) count FROM TrainingInfo a JOIN TrainingInfoDetail b
     ON a.Property='外部' AND a.id=b.TrainingInfoId" + where + " GROUP BY b.UnitName";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            List<TG.Model.Training> list = new List<TG.Model.Training>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TG.Model.Training a = new TG.Model.Training();
                    a.UnitName = dt.Rows[i][0].ToString();
                    a.Count = dt.Rows[i][1].ToString();
                    list.Add(a);
                }
            }
            return list;
        }

        //各个部门的培训费用合计占总费用的比例
        public List<TG.Model.Training> GetCount3(string begin, string end)
        {
            string where = " where 1=1";
            if (begin != "")
            {
                where += "and a.BeginDate>='" + Convert.ToDateTime(begin) + "'";
            }
            if (end != "")
            {
                where += " AND a.BeginDate<='" + Convert.ToDateTime(end) + "'";
            }


            string sql = @"SELECT b.UnitName ,sum(b.total) total FROM TrainingInfo a JOIN TrainingInfoDetail b
     ON a.Property='外部' AND a.id=b.TrainingInfoId" + where + " GROUP BY b.UnitName";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            List<TG.Model.Training> list = new List<TG.Model.Training>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TG.Model.Training a = new TG.Model.Training();
                    a.UnitName = dt.Rows[i][0].ToString();
                    a.Totals = dt.Rows[i][1].ToString();
                    list.Add(a);
                }
            }
            return list;
        }

        //各个员工的培训数量占总数的比例
        public List<TG.Model.Training> GetCount4(string begin, string end)
        {
            string where = " where 1=1";
            if (begin != "")
            {
                where += "and a.BeginDate>='" + Convert.ToDateTime(begin) + "'";
            }
            if (end != "")
            {
                where += " AND a.BeginDate<='" + Convert.ToDateTime(end) + "'";
            }


            string sql = @"SELECT b.CanYuRen ,count(*) count FROM TrainingInfo a JOIN TrainingInfoDetail b
     ON a.Property='外部' AND a.id=b.TrainingInfoId" + where + " GROUP BY b.UnitName,b.CanYuRen";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            List<TG.Model.Training> list = new List<TG.Model.Training>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TG.Model.Training a = new TG.Model.Training();
                    a.CanYuRen = dt.Rows[i][0].ToString();
                    a.Count = dt.Rows[i][1].ToString();
                    list.Add(a);
                }
            }
            return list;
        }

        //各个员工的培训数量占总数的比例
        public List<TG.Model.Training> GetCount5(string begin, string end)
        {
            string where = " where 1=1";
            if (begin != "")
            {
                where += "and a.BeginDate>='" + Convert.ToDateTime(begin) + "'";
            }
            if (end != "")
            {
                where += " AND a.BeginDate<='" + Convert.ToDateTime(end) + "'";
            }

            string sql = @"SELECT b.CanYuRen ,sum(b.total) Total FROM TrainingInfo a JOIN TrainingInfoDetail b
     ON a.Property='外部' AND a.id=b.TrainingInfoId " + where +
                         " GROUP BY b.UnitName,b.CanYuRen";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            List<TG.Model.Training> list = new List<TG.Model.Training>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TG.Model.Training a = new TG.Model.Training();
                    a.CanYuRen = dt.Rows[i][0].ToString();
                    a.Totals = dt.Rows[i][1].ToString();
                    list.Add(a);
                }
            }
            return list;
        }
        //各个员工的培训数量占总数的比例
        public List<TG.Model.Training> GetCount6(string begin, string end)
        {
            string where = " where 1=1";
            if (begin != "")
            {
                where += "and a.BeginDate>='" + Convert.ToDateTime(begin) + "'";
            }
            if (end != "")
            {
                where += " AND a.BeginDate<='" + Convert.ToDateTime(end) + "'";
            }
            string sql = @"
               SELECT t.unit_Name,k.count,k.Total FROM dbo.tg_unit t LEFT JOIN (
    SELECT  d.UnitName ,
            COUNT(DISTINCT(d.CanYuRen)) count ,
            ( SELECT    COUNT(*) count
              FROM      dbo.tg_unit a
                        LEFT JOIN tg_member b ON a.unit_ID = b.mem_Unit_ID
              WHERE     a.unit_Name = d.UnitName 
            ) Total
    FROM    TrainingInfo a
            JOIN TrainingInfoDetail d ON a.Property='外部' AND a.Id = d.TrainingInfoId" + where + " GROUP BY d.UnitName) k ON  t.unit_Name=k.UnitName Where t.unit_ParentID<>0";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            List<TG.Model.Training> list = new List<TG.Model.Training>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TG.Model.Training a = new TG.Model.Training();
                    a.UnitName = dt.Rows[i][0].ToString();
                    a.Count = dt.Rows[i][1].ToString();
                    a.Totals = dt.Rows[i][2].ToString();
                    list.Add(a);
                }
            }
            return list;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from TrainingInfo ");
            strSql.Append(" where id=" + id + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }	
        #endregion  Method
    }
}

