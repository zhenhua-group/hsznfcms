﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
using TG.Model;
using System.Data.SqlClient;
using TG.Common.EntityBuilder;
using System.Collections.Generic;
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_SuperCoperationAudit
	/// </summary>
	public partial class cm_SuperCoperationAudit
	{
		public cm_SuperCoperationAudit()
		{}
		#region  Method
       
		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("SysNo", "cm_SuperCoperationAudit"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SysNo)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_SuperCoperationAudit");
			strSql.Append(" where SysNo=@SysNo");
			SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
			parameters[0].Value = SysNo;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_SuperCoperationAudit model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_SuperCoperationAudit(");
			strSql.Append("CoperationSysNo,Status,Suggestion,AuditUser,AuditDate,InDate,InUser)");
			strSql.Append(" values (");
			strSql.Append("@CoperationSysNo,@Status,@Suggestion,@AuditUser,@AuditDate,@InDate,@InUser)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@CoperationSysNo", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Char,1),
					new SqlParameter("@Suggestion", SqlDbType.NVarChar,300),
					new SqlParameter("@AuditUser", SqlDbType.NVarChar,100),
					new SqlParameter("@AuditDate", SqlDbType.NVarChar,200),
					new SqlParameter("@InDate", SqlDbType.DateTime),
					new SqlParameter("@InUser", SqlDbType.Int,4)};
			parameters[0].Value = model.CoperationSysNo;
			parameters[1].Value = model.Status;
			parameters[2].Value = model.Suggestion;
			parameters[3].Value = model.AuditUser;
			parameters[4].Value = model.AuditDate;
			parameters[5].Value = model.InDate;
			parameters[6].Value = model.InUser;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_SuperCoperationAudit model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_SuperCoperationAudit set ");			
			strSql.Append("Status=@Status,");
			strSql.Append("Suggestion=@Suggestion,");
			strSql.Append("AuditUser=@AuditUser,");
			strSql.Append("AuditDate=@AuditDate");			
			strSql.Append(" where SysNo=@SysNo");
			SqlParameter[] parameters = {					
					new SqlParameter("@Status", SqlDbType.Char,1),
					new SqlParameter("@Suggestion", SqlDbType.NVarChar,300),
					new SqlParameter("@AuditUser", SqlDbType.NVarChar,100),
					new SqlParameter("@AuditDate", SqlDbType.NVarChar,200),				
					new SqlParameter("@SysNo", SqlDbType.Int,4)};
			
			parameters[0].Value = model.Status;
			parameters[1].Value = model.Suggestion;
			parameters[2].Value = model.AuditUser;
			parameters[3].Value = model.AuditDate;		
			parameters[4].Value = model.SysNo;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SysNo)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_SuperCoperationAudit ");
			strSql.Append(" where SysNo=@SysNo");
			SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
			parameters[0].Value = SysNo;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SysNolist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_SuperCoperationAudit ");
			strSql.Append(" where SysNo in ("+SysNolist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}     

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_SuperCoperationAudit GetModel(int SysNo)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SysNo,CoperationSysNo,Status,Suggestion,AuditUser,AuditDate,InDate,InUser from cm_SuperCoperationAudit ");
			strSql.Append(" where SysNo=@SysNo");
			SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
			parameters[0].Value = SysNo;

			TG.Model.cm_SuperCoperationAudit model=new TG.Model.cm_SuperCoperationAudit();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["SysNo"]!=null && ds.Tables[0].Rows[0]["SysNo"].ToString()!="")
				{
					model.SysNo=int.Parse(ds.Tables[0].Rows[0]["SysNo"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CoperationSysNo"]!=null && ds.Tables[0].Rows[0]["CoperationSysNo"].ToString()!="")
				{
					model.CoperationSysNo=int.Parse(ds.Tables[0].Rows[0]["CoperationSysNo"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Status"]!=null && ds.Tables[0].Rows[0]["Status"].ToString()!="")
				{
					model.Status=ds.Tables[0].Rows[0]["Status"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Suggestion"]!=null && ds.Tables[0].Rows[0]["Suggestion"].ToString()!="")
				{
					model.Suggestion=ds.Tables[0].Rows[0]["Suggestion"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AuditUser"]!=null && ds.Tables[0].Rows[0]["AuditUser"].ToString()!="")
				{
					model.AuditUser=ds.Tables[0].Rows[0]["AuditUser"].ToString();
				}
				if(ds.Tables[0].Rows[0]["AuditDate"]!=null && ds.Tables[0].Rows[0]["AuditDate"].ToString()!="")
				{
					model.AuditDate=ds.Tables[0].Rows[0]["AuditDate"].ToString();
				}
				if(ds.Tables[0].Rows[0]["InDate"]!=null && ds.Tables[0].Rows[0]["InDate"].ToString()!="")
				{
					model.InDate=DateTime.Parse(ds.Tables[0].Rows[0]["InDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["InUser"]!=null && ds.Tables[0].Rows[0]["InUser"].ToString()!="")
				{
					model.InUser=int.Parse(ds.Tables[0].Rows[0]["InUser"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SysNo,CoperationSysNo,Status,Suggestion,AuditUser,AuditDate,InDate,InUser ");
			strSql.Append(" FROM cm_SuperCoperationAudit ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SysNo,CoperationSysNo,Status,Suggestion,AuditUser,AuditDate,InDate,InUser ");
			strSql.Append(" FROM cm_SuperCoperationAudit ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_SuperCoperationAudit ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SysNo desc");
			}
			strSql.Append(")AS Row, T.*  from cm_SuperCoperationAudit T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_SuperCoperationAudit";
			parameters[1].Value = "SysNo";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
        /// <summary>
        /// 新规一条记录
        /// </summary>
        /// <param name="coperation"></param>
        /// <returns></returns>
        public int InsertCoperatioAuditRecord(TG.Model.cm_SuperCoperationAudit coperation)
        {
            string sql = "insert into cm_SuperCoperationAudit(CoperationSysNo,InUser,InDate,Status) values(";
            sql += "'" + coperation.CoperationSysNo + "',";
            sql += "'" + coperation.InUser + "',";
            sql += "'" + DateTime.Now + "',";
            sql += "'A');select @@IDENTITY";
            int count = Convert.ToInt32(DbHelperSQL.GetSingle(sql));
            return count;
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="sysMsg"></param>
        /// <returns></returns>
        public int SendMsg(TG.Model.cm_SysMsg sysMsg)
        {
            string sql = "insert into cm_SysMsg(AuditRecordSysNo,FromUser,ToRole,Status,InDate)values(";
            sql += sysMsg.AuditRecordSysNo + ",";
            sql += sysMsg.FromUser + ",";
            sql += sysMsg.ToRole + ",";
            sql += sysMsg.MsgStatus + ",";
            sql += sysMsg.MsgInDate + ")";
            int count = DbHelperSQL.ExecuteSql(sql);
            return count;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="coperation"></param>
        /// <returns></returns>
        public int UpdateCoperatioAuditRecord(TG.Model.cm_SuperCoperationAudit coperation)
        {
            string sql = "update cm_SuperCoperationAudit set Status='";
            sql += coperation.Status + "',";
            sql += "Suggestion='" + coperation.Suggestion + "',";
            sql += "AuditUser='" + coperation.AuditUser + "',";
            sql += "AuditDate='" + coperation.AuditDate + "'";
            sql += " where SysNo=" + coperation.SysNo;
            int count = DbHelperSQL.ExecuteSql(sql);
            return count;
        }

        /// <summary>
        ///  判断是否存在此条审核记录
        /// </summary>
        /// <param name="coperationSysNo"></param>
        /// <returns></returns>
        public int IsExist(int coperationSysNo)
        {
            bool flag = true;
            //查询合同信息
            string existsSql = string.Format("select TOP 1 SysNo from cm_SuperCoperationAudit ar where CoperationSysNo = {0} order by ar.SysNo DESC", coperationSysNo);


            object SysNo = DbHelperSQL.GetSingle(existsSql);

            if (SysNo == null)
            {
                //没有查询到审核记录的场合
                flag = false;
            }
            else
            {
                string sql = string.Format("select top 1 1 from cm_SuperCoperationAudit where SysNo = {0} and Status in (N'C',N'E') order by SysNo DESC", SysNo.ToString());
                object obj = DbHelperSQL.GetSingle(sql);
                flag = obj == null ? true : false;
            }
            return flag == true ? 1 : 0;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_SuperCoperationAudit GetModelBySuperCoperationSysNo(int coperationSysNo)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" SysNo,CoperationSysNo,Status,Suggestion,AuditUser,AuditDate,InDate,InUser ");
            strSql.Append(" from cm_SuperCoperationAudit ");
            strSql.Append(" where CoperationSysNo=" + coperationSysNo + " order by SysNo DESC");

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

            reader.Read();
            TG.Model.cm_SuperCoperationAudit resultEntity = EntityBuilder<TG.Model.cm_SuperCoperationAudit>.BuilderEntity(reader);
            reader.Close();

            return resultEntity;
        }
        /// <summary>
        /// 获取审批流程
        /// </summary>
        /// <returns></returns>
        public List<string> GetAuditProcessDescription()
        {
            string sql = "select r.RoleName from cm_SuperCoperationAuditConfig  ppc join cm_Role r on  r.SysNo = ppc.RoleSysNo order by ppc.position asc";

            List<string> resultString = new List<string>();

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    resultString.Add(reader["RoleName"].ToString());
                }
                reader.Close();
            }

            return resultString;
        }
	}
}

