﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.DAL
{
    public class ChargeAndCoper
    {
        public List<TG.Model.ChargeAndCoper> GetListChargeAndCoper(string unitStr, string year, string begintime, string endtime, string endmonth)
        {
            //a.unit_ParentID=238 
            List<TG.Model.ChargeAndCoper> list = new List<Model.ChargeAndCoper>();
            string sql = @"select unit_Name as Unit,SUM(lastcharge) as Frsitmonthcharge,SUM(nowcharge) as Secondmonthcharge,
SUM(lastcoper) as Frsitmonthcoper,SUM(nowcoper) as Secondmonthcoper,sum(unitallot)AS UnitAllotLast
from (
SELECT a.unit_ID,a.unit_name,
isnull(sum(b.cpr_Acount),0) AS lastcoper,
0 AS lastcharge,0 AS nowcoper,
0 AS nowcharge,0 AS unitallot
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
AND b.cpr_SignDate between '{0}-{1}' and '{0}-{2}'
 WHERE {5}  
GROUP BY a.unit_Name,a.unit_ID 
UNION ALL 
SELECT a.unit_ID,a.unit_name,0 AS lastcoper,0 AS lastcharge,
isnull(sum(b.cpr_Acount),0) AS nowcoper,
0 AS nowcharge,0 AS unitallot
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
AND  b.cpr_SignDate between '{0}-{3}' and '{0}-{4}'
WHERE {5}
GROUP BY a.unit_Name,a.unit_ID 
UNION ALL 
SELECT a.unit_ID,a.unit_name,0 AS lastcoper,isnull(sum(c.Acount),0)AS lastcharge,
0 AS nowcoper,
0  AS nowcharge,0 AS unitallot
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
LEFT JOIN cm_ProjectCharge c ON b.cpr_Id =c.cprID AND c.InAcountTime
BETWEEN '{0}-{1}'AND '{0}-{2}' AND c.Status!='B'  WHERE {5}
GROUP BY a.unit_Name,a.unit_ID 

UNION ALL 
SELECT a.unit_ID,a.unit_name,0 AS lastcoper,0 AS lastcharge,
0 AS nowcoper,
isnull(sum(c.Acount),0) AS nowcharge,0 AS unitallot
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
LEFT JOIN cm_ProjectCharge c ON b.cpr_Id =c.cprID AND c.InAcountTime
BETWEEN '{0}-{3}'AND '{0}-{4}' AND c.Status!='B'  WHERE {5}
GROUP BY a.unit_Name,a.unit_ID 
) d group by unit_Name,unit_ID";
            sql = string.Format(sql, year, begintime, endtime, endmonth + "-01", endmonth + "-" + TG.Common.TimeParser.GetMonthLastDate(int.Parse(year), int.Parse(endmonth)), unitStr);
            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                TG.Model.ChargeAndCoper model = new Model.ChargeAndCoper();
                model.Unit = item["Unit"].ToString().Trim();
                model.Frsitmonthcharge = Convert.ToDecimal(item["Frsitmonthcharge"]);
                model.Frsitmonthcoper = Convert.ToDecimal(item["Frsitmonthcoper"].ToString().Trim());
                model.Secondmonthcharge = Convert.ToDecimal(item["Secondmonthcharge"].ToString().Trim());
                model.Secondmonthcoper = Convert.ToDecimal(item["Secondmonthcoper"].ToString().Trim());
                model.UnitAllotLast = Convert.ToInt32(Convert.ToDecimal(item["UnitAllotLast"].ToString().Trim()));
                list.Add(model);
            }
            return list;
        }
    }
}
