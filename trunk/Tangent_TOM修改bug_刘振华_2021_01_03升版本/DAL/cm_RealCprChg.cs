﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_RealCprChg
    /// </summary>
    public partial class cm_RealCprChg
    {
        public cm_RealCprChg()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_RealCprChg model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.chg_id != null)
            {
                strSql1.Append("chg_id,");
                strSql2.Append("" + model.chg_id + ",");
            }
            if (model.payCount != null)
            {
                strSql1.Append("payCount,");
                strSql2.Append("" + model.payCount + ",");
            }
            if (model.payTime != null)
            {
                strSql1.Append("payTime,");
                strSql2.Append("'" + model.payTime + "',");
            }
            if (model.acceptuser != null)
            {
                strSql1.Append("acceptuser,");
                strSql2.Append("'" + model.acceptuser + "',");
            }
            if (model.isover != null)
            {
                strSql1.Append("isover,");
                strSql2.Append("" + model.isover + ",");
            }
            if (model.mark != null)
            {
                strSql1.Append("mark,");
                strSql2.Append("'" + model.mark + "',");
            }
            if (!string.IsNullOrEmpty(model.BillNo))
            {
                strSql1.Append("BillNo,");
                strSql2.Append("'" + model.BillNo + "',");
            }
            if (!string.IsNullOrEmpty(model.Remitter))
            {
                strSql1.Append("Remitter,");
                strSql2.Append("'" + model.Remitter + "',");
            }
            strSql.Append("insert into cm_RealCprChg(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_RealCprChg model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_RealCprChg set ");
            if (model.chg_id != null)
            {
                strSql.Append("chg_id=" + model.chg_id + ",");
            }
            else
            {
                strSql.Append("chg_id= null ,");
            }
            if (model.payCount != null)
            {
                strSql.Append("payCount=" + model.payCount + ",");
            }
            else
            {
                strSql.Append("payCount= null ,");
            }
            if (model.payTime != null)
            {
                strSql.Append("payTime='" + model.payTime + "',");
            }
            else
            {
                strSql.Append("payTime= null ,");
            }
            if (model.acceptuser != null)
            {
                strSql.Append("acceptuser='" + model.acceptuser + "',");
            }
            else
            {
                strSql.Append("acceptuser= null ,");
            }
            if (model.isover != null)
            {
                strSql.Append("isover=" + model.isover + ",");
            }
            else
            {
                strSql.Append("isover= null ,");
            }
            if (model.mark != null)
            {
                strSql.Append("mark='" + model.mark + "',");
            }
            else
            {
                strSql.Append("mark= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where ID=" + model.ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_RealCprChg ");
            strSql.Append(" where ID=" + ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_RealCprChg ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_RealCprChg GetModel(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" ID,chg_id,payCount,payTime,acceptuser,isover,mark,Status,Remitter,BillNo ");
            strSql.Append(" from cm_RealCprChg ");
            strSql.Append(" where ID=" + ID + "");
            TG.Model.cm_RealCprChg model = new TG.Model.cm_RealCprChg();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["chg_id"] != null && ds.Tables[0].Rows[0]["chg_id"].ToString() != "")
                {
                    model.chg_id = int.Parse(ds.Tables[0].Rows[0]["chg_id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["payCount"] != null && ds.Tables[0].Rows[0]["payCount"].ToString() != "")
                {
                    model.payCount = decimal.Parse(ds.Tables[0].Rows[0]["payCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["payTime"] != null && ds.Tables[0].Rows[0]["payTime"].ToString() != "")
                {
                    model.payTime = DateTime.Parse(ds.Tables[0].Rows[0]["payTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["acceptuser"] != null && ds.Tables[0].Rows[0]["acceptuser"].ToString() != "")
                {
                    model.acceptuser = ds.Tables[0].Rows[0]["acceptuser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["isover"] != null && ds.Tables[0].Rows[0]["isover"].ToString() != "")
                {
                    model.isover = int.Parse(ds.Tables[0].Rows[0]["isover"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mark"] != null && ds.Tables[0].Rows[0]["mark"].ToString() != "")
                {
                    model.mark = ds.Tables[0].Rows[0]["mark"].ToString();
                }

                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Remitter"] != null && ds.Tables[0].Rows[0]["Remitter"].ToString() != "")
                {
                    model.Remitter = ds.Tables[0].Rows[0]["Remitter"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BillNo"] != null && ds.Tables[0].Rows[0]["BillNo"].ToString() != "")
                {
                    model.BillNo = ds.Tables[0].Rows[0]["BillNo"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,chg_id,payCount,payTime,acceptuser,isover,mark ");
            strSql.Append(" FROM cm_RealCprChg ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,chg_id,payCount,payTime,acceptuser,isover,mark ");
            strSql.Append(" FROM cm_RealCprChg ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_RealCprChg ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            /*没有调用此方法，无需修改。Created by Sago*/
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBE() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_RealCprChg T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        */

        #endregion  Method
    }
}

