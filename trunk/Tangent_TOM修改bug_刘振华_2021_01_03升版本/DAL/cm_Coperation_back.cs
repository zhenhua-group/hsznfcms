﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_Coperation_back
    /// </summary>
    public partial class cm_Coperation_back
    {
        public cm_Coperation_back()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_Coperation_back model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.cst_Id != null)
            {
                strSql1.Append("cst_Id,");
                strSql2.Append("" + model.cst_Id + ",");
            }
            if (model.cpr_Type != null)
            {
                strSql1.Append("cpr_Type,");
                strSql2.Append("'" + model.cpr_Type + "',");
            }
            if (model.cpr_Type2 != null)
            {
                strSql1.Append("cpr_Type2,");
                strSql2.Append("'" + model.cpr_Type2 + "',");
            }
            if (model.cpr_Unit != null)
            {
                strSql1.Append("cpr_Unit,");
                strSql2.Append("'" + model.cpr_Unit + "',");
            }
            if (model.cpr_Acount != null)
            {
                strSql1.Append("cpr_Acount,");
                strSql2.Append("" + model.cpr_Acount + ",");
            }
            if (model.cpr_ShijiAcount != null)
            {
                strSql1.Append("cpr_ShijiAcount,");
                strSql2.Append("" + model.cpr_ShijiAcount + ",");
            }
            if (model.cpr_Touzi != null)
            {
                strSql1.Append("cpr_Touzi,");
                strSql2.Append("" + model.cpr_Touzi + ",");
            }
            if (model.cpr_ShijiTouzi != null)
            {
                strSql1.Append("cpr_ShijiTouzi,");
                strSql2.Append("" + model.cpr_ShijiTouzi + ",");
            }
            if (model.cpr_Mark != null)
            {
                strSql1.Append("cpr_Mark,");
                strSql2.Append("'" + model.cpr_Mark + "',");
            }
            if (model.cpr_Address != null)
            {
                strSql1.Append("cpr_Address,");
                strSql2.Append("'" + model.cpr_Address + "',");
            }
            if (model.BuildArea != null)
            {
                strSql1.Append("BuildArea,");
                strSql2.Append("'" + model.BuildArea + "',");
            }
            if (model.ChgPeople != null)
            {
                strSql1.Append("ChgPeople,");
                strSql2.Append("'" + model.ChgPeople + "',");
            }
            if (model.ChgPhone != null)
            {
                strSql1.Append("ChgPhone,");
                strSql2.Append("'" + model.ChgPhone + "',");
            }
            if (model.ChgJia != null)
            {
                strSql1.Append("ChgJia,");
                strSql2.Append("'" + model.ChgJia + "',");
            }
            if (model.ChgJiaPhone != null)
            {
                strSql1.Append("ChgJiaPhone,");
                strSql2.Append("'" + model.ChgJiaPhone + "',");
            }
            if (model.UpdateBy != null)
            {
                strSql1.Append("UpdateBy,");
                strSql2.Append("'" + model.UpdateBy + "',");
            }
            if (model.BuildPosition != null)
            {
                strSql1.Append("BuildPosition,");
                strSql2.Append("'" + model.BuildPosition + "',");
            }
            if (model.Industry != null)
            {
                strSql1.Append("Industry,");
                strSql2.Append("'" + model.Industry + "',");
            }
            if (model.BuildUnit != null)
            {
                strSql1.Append("BuildUnit,");
                strSql2.Append("'" + model.BuildUnit + "',");
            }
            if (model.BuildSrc != null)
            {
                strSql1.Append("BuildSrc,");
                strSql2.Append("'" + model.BuildSrc + "',");
            }
            if (model.RegTime != null)
            {
                strSql1.Append("RegTime,");
                strSql2.Append("'" + model.RegTime + "',");
            }
            if (model.BuildType != null)
            {
                strSql1.Append("BuildType,");
                strSql2.Append("'" + model.BuildType + "',");
            }
            if (model.StructType != null)
            {
                strSql1.Append("StructType,");
                strSql2.Append("'" + model.StructType + "',");
            }
            if (model.Floor != null)
            {
                strSql1.Append("Floor,");
                strSql2.Append("'" + model.Floor + "',");
            }
            if (model.BuildStructType != null)
            {
                strSql1.Append("BuildStructType,");
                strSql2.Append("'" + model.BuildStructType + "',");
            }
            if (model.MultiBuild != null)
            {
                strSql1.Append("MultiBuild,");
                strSql2.Append("'" + model.MultiBuild + "',");
            }
            if (model.cpr_Name != null)
            {
                strSql1.Append("cpr_Name,");
                strSql2.Append("'" + model.cpr_Name + "',");
            }
            if (model.InsertUserID != null)
            {
                strSql1.Append("InsertUserID,");
                strSql2.Append("" + model.InsertUserID + ",");
            }
            if (model.InsertDate != null)
            {
                strSql1.Append("InsertDate,");
                strSql2.Append("'" + model.InsertDate + "',");
            }
            if (model.proapproval != null)
            {
                strSql1.Append("proapproval,");
                strSql2.Append("" + model.proapproval + ",");
            }
            strSql.Append("insert into cm_Coperation_back(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_Coperation_back model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_Coperation_back set ");
            if (model.cst_Id != null)
            {
                strSql.Append("cst_Id=" + model.cst_Id + ",");
            }
            else
            {
                strSql.Append("cst_Id= null ,");
            }
            if (model.cpr_Type != null)
            {
                strSql.Append("cpr_Type='" + model.cpr_Type + "',");
            }
            else
            {
                strSql.Append("cpr_Type= null ,");
            }
            if (model.cpr_Type2 != null)
            {
                strSql.Append("cpr_Type2='" + model.cpr_Type2 + "',");
            }
            else
            {
                strSql.Append("cpr_Type2= null ,");
            }
            if (model.cpr_Unit != null)
            {
                strSql.Append("cpr_Unit='" + model.cpr_Unit + "',");
            }
            else
            {
                strSql.Append("cpr_Unit= null ,");
            }
            if (model.cpr_Acount != null)
            {
                strSql.Append("cpr_Acount=" + model.cpr_Acount + ",");
            }
            else
            {
                strSql.Append("cpr_Acount= null ,");
            }
            if (model.cpr_ShijiAcount != null)
            {
                strSql.Append("cpr_ShijiAcount=" + model.cpr_ShijiAcount + ",");
            }
            else
            {
                strSql.Append("cpr_ShijiAcount= null ,");
            }
            if (model.cpr_Touzi != null)
            {
                strSql.Append("cpr_Touzi=" + model.cpr_Touzi + ",");
            }
            else
            {
                strSql.Append("cpr_Touzi= null ,");
            }
            if (model.cpr_ShijiTouzi != null)
            {
                strSql.Append("cpr_ShijiTouzi=" + model.cpr_ShijiTouzi + ",");
            }
            else
            {
                strSql.Append("cpr_ShijiTouzi= null ,");
            }
            if (model.cpr_Mark != null)
            {
                strSql.Append("cpr_Mark='" + model.cpr_Mark + "',");
            }
            else
            {
                strSql.Append("cpr_Mark= null ,");
            }
            if (model.cpr_Address != null)
            {
                strSql.Append("cpr_Address='" + model.cpr_Address + "',");
            }
            else
            {
                strSql.Append("cpr_Address= null ,");
            }
            if (model.BuildArea != null)
            {
                strSql.Append("BuildArea='" + model.BuildArea + "',");
            }
            else
            {
                strSql.Append("BuildArea= null ,");
            }
            if (model.ChgPeople != null)
            {
                strSql.Append("ChgPeople='" + model.ChgPeople + "',");
            }
            else
            {
                strSql.Append("ChgPeople= null ,");
            }
            if (model.ChgPhone != null)
            {
                strSql.Append("ChgPhone='" + model.ChgPhone + "',");
            }
            else
            {
                strSql.Append("ChgPhone= null ,");
            }
            if (model.ChgJia != null)
            {
                strSql.Append("ChgJia='" + model.ChgJia + "',");
            }
            else
            {
                strSql.Append("ChgJia= null ,");
            }
            if (model.ChgJiaPhone != null)
            {
                strSql.Append("ChgJiaPhone='" + model.ChgJiaPhone + "',");
            }
            else
            {
                strSql.Append("ChgJiaPhone= null ,");
            }
            if (model.UpdateBy != null)
            {
                strSql.Append("UpdateBy='" + model.UpdateBy + "',");
            }
            else
            {
                strSql.Append("UpdateBy= null ,");
            }
            if (model.BuildPosition != null)
            {
                strSql.Append("BuildPosition='" + model.BuildPosition + "',");
            }
            else
            {
                strSql.Append("BuildPosition= null ,");
            }
            if (model.Industry != null)
            {
                strSql.Append("Industry='" + model.Industry + "',");
            }
            else
            {
                strSql.Append("Industry= null ,");
            }
            if (model.BuildUnit != null)
            {
                strSql.Append("BuildUnit='" + model.BuildUnit + "',");
            }
            else
            {
                strSql.Append("BuildUnit= null ,");
            }
            if (model.BuildSrc != null)
            {
                strSql.Append("BuildSrc='" + model.BuildSrc + "',");
            }
            else
            {
                strSql.Append("BuildSrc= null ,");
            }
            if (model.RegTime != null)
            {
                strSql.Append("RegTime='" + model.RegTime + "',");
            }
            else
            {
                strSql.Append("RegTime= null ,");
            }
            if (model.BuildType != null)
            {
                strSql.Append("BuildType='" + model.BuildType + "',");
            }
            else
            {
                strSql.Append("BuildType= null ,");
            }
            if (model.StructType != null)
            {
                strSql.Append("StructType='" + model.StructType + "',");
            }
            else
            {
                strSql.Append("StructType= null ,");
            }
            if (model.Floor != null)
            {
                strSql.Append("Floor='" + model.Floor + "',");
            }
            else
            {
                strSql.Append("Floor= null ,");
            }
            if (model.BuildStructType != null)
            {
                strSql.Append("BuildStructType='" + model.BuildStructType + "',");
            }
            else
            {
                strSql.Append("BuildStructType= null ,");
            }
            if (model.MultiBuild != null)
            {
                strSql.Append("MultiBuild='" + model.MultiBuild + "',");
            }
            else
            {
                strSql.Append("MultiBuild= null ,");
            }
            if (model.cpr_Name != null)
            {
                strSql.Append("cpr_Name='" + model.cpr_Name + "',");
            }
            else
            {
                strSql.Append("cpr_Name= null ,");
            }
            if (model.InsertUserID != null)
            {
                strSql.Append("InsertUserID=" + model.InsertUserID + ",");
            }
            else
            {
                strSql.Append("InsertUserID= null ,");
            }
            if (model.InsertDate != null)
            {
                strSql.Append("InsertDate='" + model.InsertDate + "',");
            }
            else
            {
                strSql.Append("InsertDate= null ,");
            }
            if (model.proapproval != null)
            {
                strSql.Append("proapproval=" + model.proapproval + ",");
            }
            else
            {
                strSql.Append("proapproval= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where cpr_Id=" + model.cpr_Id + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int cpr_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_Coperation_back ");
            strSql.Append(" where cpr_Id=" + cpr_Id + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string cpr_Idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_Coperation_back ");
            strSql.Append(" where cpr_Id in (" + cpr_Idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除报备信息
        /// </summary>
        /// <param name="cpr_idlist"></param>
        /// <returns></returns>
        public bool DeleteList(string[] cpr_idlist)
        {
            int affectRow = 0;

            if (cpr_idlist.Length > 0)
            {
                for (int i = 0; i < cpr_idlist.Length; i++)
                {
                    string strSql = "Delete From cm_Coperation_back Where  cpr_ID=" + cpr_idlist[i].ToString();
                    affectRow = DbHelperSQL.ExecuteSql(strSql);
                }
            }

            if (affectRow > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_Coperation_back GetModel(int cpr_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" cpr_Id,cst_Id,cpr_Type,cpr_Type2,cpr_Unit,cpr_Acount,cpr_ShijiAcount,cpr_Touzi,cpr_ShijiTouzi,cpr_Mark,cpr_Address,BuildArea,ChgPeople,ChgPhone,ChgJia,ChgJiaPhone,UpdateBy,BuildPosition,Industry,BuildUnit,BuildSrc,RegTime,BuildType,StructType,Floor,BuildStructType,MultiBuild,cpr_Name,InsertUserID,InsertDate,proapproval ");
            strSql.Append(" from cm_Coperation_back ");
            strSql.Append(" where cpr_Id=" + cpr_Id + "");
            TG.Model.cm_Coperation_back model = new TG.Model.cm_Coperation_back();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["cpr_Id"] != null && ds.Tables[0].Rows[0]["cpr_Id"].ToString() != "")
                {
                    model.cpr_Id = int.Parse(ds.Tables[0].Rows[0]["cpr_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cst_Id"] != null && ds.Tables[0].Rows[0]["cst_Id"].ToString() != "")
                {
                    model.cst_Id = int.Parse(ds.Tables[0].Rows[0]["cst_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_Type"] != null && ds.Tables[0].Rows[0]["cpr_Type"].ToString() != "")
                {
                    model.cpr_Type = ds.Tables[0].Rows[0]["cpr_Type"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Type2"] != null && ds.Tables[0].Rows[0]["cpr_Type2"].ToString() != "")
                {
                    model.cpr_Type2 = ds.Tables[0].Rows[0]["cpr_Type2"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Unit"] != null && ds.Tables[0].Rows[0]["cpr_Unit"].ToString() != "")
                {
                    model.cpr_Unit = ds.Tables[0].Rows[0]["cpr_Unit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Acount"] != null && ds.Tables[0].Rows[0]["cpr_Acount"].ToString() != "")
                {
                    model.cpr_Acount = decimal.Parse(ds.Tables[0].Rows[0]["cpr_Acount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_ShijiAcount"] != null && ds.Tables[0].Rows[0]["cpr_ShijiAcount"].ToString() != "")
                {
                    model.cpr_ShijiAcount = decimal.Parse(ds.Tables[0].Rows[0]["cpr_ShijiAcount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_Touzi"] != null && ds.Tables[0].Rows[0]["cpr_Touzi"].ToString() != "")
                {
                    model.cpr_Touzi = decimal.Parse(ds.Tables[0].Rows[0]["cpr_Touzi"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_ShijiTouzi"] != null && ds.Tables[0].Rows[0]["cpr_ShijiTouzi"].ToString() != "")
                {
                    model.cpr_ShijiTouzi = decimal.Parse(ds.Tables[0].Rows[0]["cpr_ShijiTouzi"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_Mark"] != null && ds.Tables[0].Rows[0]["cpr_Mark"].ToString() != "")
                {
                    model.cpr_Mark = ds.Tables[0].Rows[0]["cpr_Mark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Address"] != null && ds.Tables[0].Rows[0]["cpr_Address"].ToString() != "")
                {
                    model.cpr_Address = ds.Tables[0].Rows[0]["cpr_Address"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildArea"] != null && ds.Tables[0].Rows[0]["BuildArea"].ToString() != "")
                {
                    model.BuildArea = ds.Tables[0].Rows[0]["BuildArea"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChgPeople"] != null && ds.Tables[0].Rows[0]["ChgPeople"].ToString() != "")
                {
                    model.ChgPeople = ds.Tables[0].Rows[0]["ChgPeople"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChgPhone"] != null && ds.Tables[0].Rows[0]["ChgPhone"].ToString() != "")
                {
                    model.ChgPhone = ds.Tables[0].Rows[0]["ChgPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChgJia"] != null && ds.Tables[0].Rows[0]["ChgJia"].ToString() != "")
                {
                    model.ChgJia = ds.Tables[0].Rows[0]["ChgJia"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChgJiaPhone"] != null && ds.Tables[0].Rows[0]["ChgJiaPhone"].ToString() != "")
                {
                    model.ChgJiaPhone = ds.Tables[0].Rows[0]["ChgJiaPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UpdateBy"] != null && ds.Tables[0].Rows[0]["UpdateBy"].ToString() != "")
                {
                    model.UpdateBy = ds.Tables[0].Rows[0]["UpdateBy"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildPosition"] != null && ds.Tables[0].Rows[0]["BuildPosition"].ToString() != "")
                {
                    model.BuildPosition = ds.Tables[0].Rows[0]["BuildPosition"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Industry"] != null && ds.Tables[0].Rows[0]["Industry"].ToString() != "")
                {
                    model.Industry = ds.Tables[0].Rows[0]["Industry"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildUnit"] != null && ds.Tables[0].Rows[0]["BuildUnit"].ToString() != "")
                {
                    model.BuildUnit = ds.Tables[0].Rows[0]["BuildUnit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildSrc"] != null && ds.Tables[0].Rows[0]["BuildSrc"].ToString() != "")
                {
                    model.BuildSrc = ds.Tables[0].Rows[0]["BuildSrc"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RegTime"] != null && ds.Tables[0].Rows[0]["RegTime"].ToString() != "")
                {
                    model.RegTime = DateTime.Parse(ds.Tables[0].Rows[0]["RegTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BuildType"] != null && ds.Tables[0].Rows[0]["BuildType"].ToString() != "")
                {
                    model.BuildType = ds.Tables[0].Rows[0]["BuildType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StructType"] != null && ds.Tables[0].Rows[0]["StructType"].ToString() != "")
                {
                    model.StructType = ds.Tables[0].Rows[0]["StructType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Floor"] != null && ds.Tables[0].Rows[0]["Floor"].ToString() != "")
                {
                    model.Floor = ds.Tables[0].Rows[0]["Floor"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildStructType"] != null && ds.Tables[0].Rows[0]["BuildStructType"].ToString() != "")
                {
                    model.BuildStructType = ds.Tables[0].Rows[0]["BuildStructType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["MultiBuild"] != null && ds.Tables[0].Rows[0]["MultiBuild"].ToString() != "")
                {
                    model.MultiBuild = ds.Tables[0].Rows[0]["MultiBuild"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Name"] != null && ds.Tables[0].Rows[0]["cpr_Name"].ToString() != "")
                {
                    model.cpr_Name = ds.Tables[0].Rows[0]["cpr_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["InsertUserID"] != null && ds.Tables[0].Rows[0]["InsertUserID"].ToString() != "")
                {
                    model.InsertUserID = int.Parse(ds.Tables[0].Rows[0]["InsertUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InsertDate"] != null && ds.Tables[0].Rows[0]["InsertDate"].ToString() != "")
                {
                    model.InsertDate = DateTime.Parse(ds.Tables[0].Rows[0]["InsertDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["proapproval"] != null && ds.Tables[0].Rows[0]["proapproval"].ToString() != "")
                {
                    model.proapproval = int.Parse(ds.Tables[0].Rows[0]["proapproval"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select cpr_Id,cst_Id,cpr_Type,cpr_Type2,cpr_Unit,cpr_Acount,cpr_ShijiAcount,cpr_Touzi,cpr_ShijiTouzi,cpr_Mark,cpr_Address,BuildArea,ChgPeople,ChgPhone,ChgJia,ChgJiaPhone,UpdateBy,BuildPosition,Industry,BuildUnit,BuildSrc,RegTime,BuildType,StructType,Floor,BuildStructType,MultiBuild,cpr_Name,InsertUserID,InsertDate,proapproval ");
            strSql.Append(" FROM cm_Coperation_back ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" cpr_Id,cst_Id,cpr_Type,cpr_Type2,cpr_Unit,cpr_Acount,cpr_ShijiAcount,cpr_Touzi,cpr_ShijiTouzi,cpr_Mark,cpr_Address,BuildArea,ChgPeople,ChgPhone,ChgJia,ChgJiaPhone,UpdateBy,BuildPosition,Industry,BuildUnit,BuildSrc,RegTime,BuildType,StructType,Floor,BuildStructType,MultiBuild,cpr_Name,InsertUserID,InsertDate,proapproval ");
            strSql.Append(" FROM cm_Coperation_back ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_Coperation_back ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.cpr_Id desc");
            }
            strSql.Append(")AS Row, T.*  from cm_Coperation_back T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_Coperation_Bak", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_Coperation_Bak_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }

        #endregion  Method
    }
}

