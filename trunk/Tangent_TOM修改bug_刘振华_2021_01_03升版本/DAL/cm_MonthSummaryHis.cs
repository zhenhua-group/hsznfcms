﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_MonthSummaryHis
	/// </summary>
	public partial class cm_MonthSummaryHis
	{
		public cm_MonthSummaryHis()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("ID", "cm_MonthSummaryHis"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_MonthSummaryHis");
			strSql.Append(" where ID="+ID+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_MonthSummaryHis model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.mem_id != null)
			{
				strSql1.Append("mem_id,");
				strSql2.Append(""+model.mem_id+",");
			}
			if (model.mem_name != null)
			{
				strSql1.Append("mem_name,");
				strSql2.Append("'"+model.mem_name+"',");
			}
			if (model.mem_unit_ID != null)
			{
				strSql1.Append("mem_unit_ID,");
				strSql2.Append(""+model.mem_unit_ID+",");
			}
			if (model.dataDate != null)
			{
				strSql1.Append("dataDate,");
				strSql2.Append("'"+model.dataDate+"',");
			}
			if (model.dataYear != null)
			{
				strSql1.Append("dataYear,");
				strSql2.Append(""+model.dataYear+",");
			}
			if (model.dataMonth != null)
			{
				strSql1.Append("dataMonth,");
				strSql2.Append(""+model.dataMonth+",");
			}
			if (model.overtime != null)
			{
				strSql1.Append("overtime,");
				strSql2.Append(""+model.overtime+",");
			}
			if (model.weekovertime != null)
			{
				strSql1.Append("weekovertime,");
				strSql2.Append(""+model.weekovertime+",");
			}
			if (model.content != null)
			{
				strSql1.Append("content,");
				strSql2.Append("'"+model.content+"',");
			}
			if (model.isiswdkjl != null)
			{
				strSql1.Append("isiswdkjl,");
				strSql2.Append("'"+model.isiswdkjl+"',");
			}
			strSql.Append("insert into cm_MonthSummaryHis(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_MonthSummaryHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_MonthSummaryHis set ");
			if (model.mem_id != null)
			{
				strSql.Append("mem_id="+model.mem_id+",");
			}
			if (model.mem_name != null)
			{
				strSql.Append("mem_name='"+model.mem_name+"',");
			}
			if (model.mem_unit_ID != null)
			{
				strSql.Append("mem_unit_ID="+model.mem_unit_ID+",");
			}
			if (model.dataDate != null)
			{
				strSql.Append("dataDate='"+model.dataDate+"',");
			}
			if (model.dataYear != null)
			{
				strSql.Append("dataYear="+model.dataYear+",");
			}
			if (model.dataMonth != null)
			{
				strSql.Append("dataMonth="+model.dataMonth+",");
			}
			if (model.overtime != null)
			{
				strSql.Append("overtime="+model.overtime+",");
			}
			else
			{
				strSql.Append("overtime= null ,");
			}
			if (model.weekovertime != null)
			{
				strSql.Append("weekovertime="+model.weekovertime+",");
			}
			else
			{
				strSql.Append("weekovertime= null ,");
			}
			if (model.content != null)
			{
				strSql.Append("content='"+model.content+"',");
			}
			else
			{
				strSql.Append("content= null ,");
			}
			if (model.isiswdkjl != null)
			{
				strSql.Append("isiswdkjl='"+model.isiswdkjl+"',");
			}
			else
			{
				strSql.Append("isiswdkjl= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ID="+ model.ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_MonthSummaryHis ");
			strSql.Append(" where ID="+ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_MonthSummaryHis ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_MonthSummaryHis GetModel(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" ID,mem_id,mem_name,mem_unit_ID,dataDate,dataYear,dataMonth,overtime,weekovertime,content,isiswdkjl ");
			strSql.Append(" from cm_MonthSummaryHis ");
			strSql.Append(" where ID="+ID+"" );
			TG.Model.cm_MonthSummaryHis model=new TG.Model.cm_MonthSummaryHis();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_MonthSummaryHis DataRowToModel(DataRow row)
		{
			TG.Model.cm_MonthSummaryHis model=new TG.Model.cm_MonthSummaryHis();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["mem_id"]!=null && row["mem_id"].ToString()!="")
				{
					model.mem_id=int.Parse(row["mem_id"].ToString());
				}
				if(row["mem_name"]!=null)
				{
					model.mem_name=row["mem_name"].ToString();
				}
				if(row["mem_unit_ID"]!=null && row["mem_unit_ID"].ToString()!="")
				{
					model.mem_unit_ID=int.Parse(row["mem_unit_ID"].ToString());
				}
				if(row["dataDate"]!=null)
				{
					model.dataDate=row["dataDate"].ToString();
				}
				if(row["dataYear"]!=null && row["dataYear"].ToString()!="")
				{
					model.dataYear=int.Parse(row["dataYear"].ToString());
				}
				if(row["dataMonth"]!=null && row["dataMonth"].ToString()!="")
				{
					model.dataMonth=int.Parse(row["dataMonth"].ToString());
				}
				if(row["overtime"]!=null && row["overtime"].ToString()!="")
				{
					model.overtime=decimal.Parse(row["overtime"].ToString());
				}
				if(row["weekovertime"]!=null && row["weekovertime"].ToString()!="")
				{
					model.weekovertime=decimal.Parse(row["weekovertime"].ToString());
				}
				if(row["content"]!=null)
				{
					model.content=row["content"].ToString();
				}
				if(row["isiswdkjl"]!=null)
				{
					model.isiswdkjl=row["isiswdkjl"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,mem_id,mem_name,mem_unit_ID,dataDate,dataYear,dataMonth,overtime,weekovertime,content,isiswdkjl ");
			strSql.Append(" FROM cm_MonthSummaryHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,mem_id,mem_name,mem_unit_ID,dataDate,dataYear,dataMonth,overtime,weekovertime,content,isiswdkjl ");
			strSql.Append(" FROM cm_MonthSummaryHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_MonthSummaryHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_MonthSummaryHis T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
		#region  MethodEx

		#endregion  MethodEx
	}
}

