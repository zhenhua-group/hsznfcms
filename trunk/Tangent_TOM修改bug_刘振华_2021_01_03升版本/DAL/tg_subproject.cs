﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:tg_subproject
	/// </summary>
	public partial class tg_subproject
	{
		public tg_subproject()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("pro_ID", "tg_subproject"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int pro_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from tg_subproject");
			strSql.Append(" where pro_ID="+pro_ID+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.tg_subproject model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.pro_Name != null)
			{
				strSql1.Append("pro_Name,");
				strSql2.Append("'"+model.pro_Name+"',");
			}
			if (model.pro_parentID != null)
			{
				strSql1.Append("pro_parentID,");
				strSql2.Append(""+model.pro_parentID+",");
			}
			if (model.pro_descr != null)
			{
				strSql1.Append("pro_descr,");
				strSql2.Append("'"+model.pro_descr+"',");
			}
			if (model.pro_IsTerminal != null)
			{
				strSql1.Append("pro_IsTerminal,");
				strSql2.Append(""+model.pro_IsTerminal+",");
			}
			if (model.pro_parentSubID != null)
			{
				strSql1.Append("pro_parentSubID,");
				strSql2.Append(""+model.pro_parentSubID+",");
			}
			strSql.Append("insert into tg_subproject(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_subproject model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update tg_subproject set ");
			if (model.pro_Name != null)
			{
				strSql.Append("pro_Name='"+model.pro_Name+"',");
			}
			else
			{
				strSql.Append("pro_Name= null ,");
			}
			if (model.pro_parentID != null)
			{
				strSql.Append("pro_parentID="+model.pro_parentID+",");
			}
			else
			{
				strSql.Append("pro_parentID= null ,");
			}
			if (model.pro_descr != null)
			{
				strSql.Append("pro_descr='"+model.pro_descr+"',");
			}
			else
			{
				strSql.Append("pro_descr= null ,");
			}
			if (model.pro_IsTerminal != null)
			{
				strSql.Append("pro_IsTerminal="+model.pro_IsTerminal+",");
			}
			else
			{
				strSql.Append("pro_IsTerminal= null ,");
			}
			if (model.pro_parentSubID != null)
			{
				strSql.Append("pro_parentSubID="+model.pro_parentSubID+",");
			}
			else
			{
				strSql.Append("pro_parentSubID= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where pro_ID="+ model.pro_ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int pro_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_subproject ");
			strSql.Append(" where pro_ID="+pro_ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string pro_IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_subproject ");
			strSql.Append(" where pro_ID in ("+pro_IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_subproject GetModel(int pro_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" pro_ID,pro_Name,pro_parentID,pro_descr,pro_IsTerminal,pro_parentSubID ");
			strSql.Append(" from tg_subproject ");
			strSql.Append(" where pro_ID="+pro_ID+"" );
			TG.Model.tg_subproject model=new TG.Model.tg_subproject();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["pro_ID"]!=null && ds.Tables[0].Rows[0]["pro_ID"].ToString()!="")
				{
					model.pro_ID=int.Parse(ds.Tables[0].Rows[0]["pro_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["pro_Name"]!=null && ds.Tables[0].Rows[0]["pro_Name"].ToString()!="")
				{
					model.pro_Name=ds.Tables[0].Rows[0]["pro_Name"].ToString();
				}
				if(ds.Tables[0].Rows[0]["pro_parentID"]!=null && ds.Tables[0].Rows[0]["pro_parentID"].ToString()!="")
				{
					model.pro_parentID=int.Parse(ds.Tables[0].Rows[0]["pro_parentID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["pro_descr"]!=null && ds.Tables[0].Rows[0]["pro_descr"].ToString()!="")
				{
					model.pro_descr=ds.Tables[0].Rows[0]["pro_descr"].ToString();
				}
				if(ds.Tables[0].Rows[0]["pro_IsTerminal"]!=null && ds.Tables[0].Rows[0]["pro_IsTerminal"].ToString()!="")
				{
					model.pro_IsTerminal=int.Parse(ds.Tables[0].Rows[0]["pro_IsTerminal"].ToString());
				}
				if(ds.Tables[0].Rows[0]["pro_parentSubID"]!=null && ds.Tables[0].Rows[0]["pro_parentSubID"].ToString()!="")
				{
					model.pro_parentSubID=int.Parse(ds.Tables[0].Rows[0]["pro_parentSubID"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select pro_ID,pro_Name,pro_parentID,pro_descr,pro_IsTerminal,pro_parentSubID ");
			strSql.Append(" FROM tg_subproject ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" pro_ID,pro_Name,pro_parentID,pro_descr,pro_IsTerminal,pro_parentSubID ");
			strSql.Append(" FROM tg_subproject ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM tg_subproject ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.pro_ID desc");
			}
			strSql.Append(")AS Row, T.*  from tg_subproject T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

