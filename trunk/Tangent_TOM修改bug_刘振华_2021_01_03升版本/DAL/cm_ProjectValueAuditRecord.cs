﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using System.Data;
using TG.Model;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_ProjectValueAuditRecord
    /// </summary>
    public partial class cm_ProjectValueAuditRecord
    {
        public cm_ProjectValueAuditRecord()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectValueAuditRecord model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_ProjectValueAuditRecord(");
            strSql.Append("ProSysNo,AllotID,Status,OneSuggestion,TwoSuggstion,ThreeSuggsion,FourSuggion,FiveSuggion,SixSuggsion,SevenSuggsion,AuditUser,AuditDate,InUser,InDate,SecondValue)");
            strSql.Append(" values (");
            strSql.Append("@ProSysNo,@AllotID,@Status,@OneSuggestion,@TwoSuggstion,@ThreeSuggsion,@FourSuggion,@FiveSuggion,@SixSuggsion,@SevenSuggsion,@AuditUser,@AuditDate,@InUser,@InDate,@SecondValue)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ProSysNo", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Char,1),
					new SqlParameter("@OneSuggestion", SqlDbType.NVarChar,600),
					new SqlParameter("@TwoSuggstion", SqlDbType.NVarChar,600),
					new SqlParameter("@ThreeSuggsion", SqlDbType.NVarChar,600),
					new SqlParameter("@FourSuggion", SqlDbType.NVarChar,600),
					new SqlParameter("@FiveSuggion", SqlDbType.NVarChar,600),
                    new SqlParameter("@SixSuggsion", SqlDbType.NVarChar,600),
					new SqlParameter("@SevenSuggsion", SqlDbType.NVarChar,600),
					new SqlParameter("@AuditUser", SqlDbType.NVarChar,300),
					new SqlParameter("@AuditDate", SqlDbType.NVarChar,300),
					new SqlParameter("@InUser", SqlDbType.Int,4),
					new SqlParameter("@InDate", SqlDbType.DateTime),
                    new SqlParameter("@SecondValue", SqlDbType.Char,1)                   };
            parameters[0].Value = model.ProSysNo;
            parameters[1].Value = model.AllotID;
            parameters[2].Value = model.Status;
            parameters[3].Value = model.OneSuggestion;
            parameters[4].Value = model.TwoSuggstion;
            parameters[5].Value = model.ThreeSuggsion;
            parameters[6].Value = model.FourSuggion;
            parameters[7].Value = model.FiveSuggion;
            parameters[8].Value = model.SixSuggsion;
            parameters[9].Value = model.SevenSuggsion;
            parameters[10].Value = model.AuditUser;
            parameters[11].Value = model.AuditDate;
            parameters[12].Value = model.InUser;
            parameters[13].Value = DateTime.Now;
            parameters[14].Value = model.SecondValue;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectValueAuditRecord model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ProjectValueAuditRecord set ");
            strSql.Append("ProSysNo=@ProSysNo,");
            strSql.Append("AllotID=@AllotID,");
            strSql.Append("Status=@Status,");
            strSql.Append("OneSuggestion=@OneSuggestion,");
            strSql.Append("TwoSuggstion=@TwoSuggstion,");
            strSql.Append("ThreeSuggsion=@ThreeSuggsion,");
            strSql.Append("FourSuggion=@FourSuggion,");
            strSql.Append("FiveSuggion=@FiveSuggion,");
            strSql.Append("SixSuggsion=@SixSuggsion,");
            strSql.Append("SevenSuggsion=@SevenSuggsion,");
            strSql.Append("AuditUser=@AuditUser,");
            strSql.Append("AuditDate=@AuditDate,");
            strSql.Append("InUser=@InUser,");
            strSql.Append("InDate=@InDate,");
            strSql.Append("SprcialtyAuditCount=@SprcialtyAuditCount,");
            strSql.Append("HeadAuditCount=@HeadAuditCount,");
            strSql.Append("TwoAuditUser=@TwoAuditUser,");
            strSql.Append("ThreeAuditUser=@ThreeAuditUser,");
            strSql.Append("FourAuditUser=@FourAuditUser,");
            strSql.Append("TwoAuditDate=@TwoAuditDate,");
            strSql.Append("ThreeAuditDate=@ThreeAuditDate,");
            strSql.Append("FourAuditDate=@FourAuditDate,");
            strSql.Append("TwoIsPass=@TwoIsPass,");
            strSql.Append("ThreeIsPass=@ThreeIsPass,");
            strSql.Append("FourIsPass=@FourIsPass,");
            strSql.Append("EightSuggstion=@EightSuggstion");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@ProSysNo", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Char,1),
					new SqlParameter("@OneSuggestion", SqlDbType.NVarChar,600),
					new SqlParameter("@TwoSuggstion", SqlDbType.NVarChar,600),
					new SqlParameter("@ThreeSuggsion", SqlDbType.NVarChar,600),
					new SqlParameter("@FourSuggion", SqlDbType.NVarChar,600),
					new SqlParameter("@FiveSuggion", SqlDbType.NVarChar,600),
                    new SqlParameter("@SixSuggsion", SqlDbType.NVarChar,600),
					new SqlParameter("@SevenSuggsion", SqlDbType.NVarChar,600),
					new SqlParameter("@AuditUser",  SqlDbType.NVarChar,300),
					new SqlParameter("@AuditDate",SqlDbType.NVarChar,300 ),
					new SqlParameter("@InUser", SqlDbType.Int,4),
					new SqlParameter("@InDate",SqlDbType.DateTime),
                    new SqlParameter("@SprcialtyAuditCount", SqlDbType.Int,4),
                    new SqlParameter("@HeadAuditCount", SqlDbType.Int,4),
                    new SqlParameter("@TwoAuditUser", SqlDbType.Int,4),
                    new SqlParameter("@ThreeAuditUser", SqlDbType.Int,4),
                    new SqlParameter("@FourAuditUser", SqlDbType.Int,4),
                    new SqlParameter("@TwoAuditDate", SqlDbType.DateTime),
                    new SqlParameter("@ThreeAuditDate", SqlDbType.DateTime),
                    new SqlParameter("@FourAuditDate", SqlDbType.DateTime),
                    new SqlParameter("@TwoIsPass", SqlDbType.Int,4),
                    new SqlParameter("@ThreeIsPass", SqlDbType.Int,4),
                    new SqlParameter("@FourIsPass", SqlDbType.Int,4),
                    new SqlParameter("@EightSuggstion", SqlDbType.NVarChar,600),
					new SqlParameter("@SysNo", SqlDbType.Int,4)};
            parameters[0].Value = model.ProSysNo;
            parameters[1].Value = model.AllotID;
            parameters[2].Value = model.Status;
            parameters[3].Value = model.OneSuggestion;
            parameters[4].Value = model.TwoSuggstion;
            parameters[5].Value = model.ThreeSuggsion;
            parameters[6].Value = model.FourSuggion;
            parameters[7].Value = model.FiveSuggion;
            parameters[8].Value = model.SixSuggsion;
            parameters[9].Value = model.SevenSuggsion;
            parameters[10].Value = model.AuditUser;
            parameters[11].Value = model.AuditDate;
            parameters[12].Value = model.InUser;
            parameters[13].Value = model.InDate;
            parameters[14].Value = model.SprcialtyAuditCount;
            parameters[15].Value = model.HeadAuditCount;
            parameters[16].Value = model.TwoAuditUser;
            parameters[17].Value = model.ThreeAuditUser;
            parameters[18].Value = model.FourAuditUser;
            parameters[19].Value = model.TwoAuditDate;
            parameters[20].Value = model.ThreeAuditDate;
            parameters[21].Value = model.FourAuditDate;
            parameters[22].Value = model.TwoIsPass;
            parameters[23].Value = model.ThreeIsPass;
            parameters[24].Value = model.FourIsPass;
            parameters[25].Value = model.EightSuggstion;
            parameters[26].Value = model.SysNo;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            return rows;
        }

        /// <summary>
        /// 更新数据-状态
        /// </summary>
        /// <param name="sysno"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public int Update(int sysno, string status)
        {
            string sql = @"update cm_ProjectValueAuditRecord set Status='" + status + "'  where SysNo=" + sysno + "";
            return DbHelperSQL.ExecuteSql(sql);
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SysNo)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProjectValueAuditRecord ");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectValueAuditRecord GetModel(int SysNo)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  * from cm_ProjectValueAuditRecord ");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            TG.Model.cm_ProjectValueAuditRecord model = new TG.Model.cm_ProjectValueAuditRecord();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["SysNo"] != null && ds.Tables[0].Rows[0]["SysNo"].ToString() != "")
                {
                    model.SysNo = int.Parse(ds.Tables[0].Rows[0]["SysNo"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProSysNo"] != null && ds.Tables[0].Rows[0]["ProSysNo"].ToString() != "")
                {
                    model.ProSysNo = int.Parse(ds.Tables[0].Rows[0]["ProSysNo"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotID"] != null && ds.Tables[0].Rows[0]["AllotID"].ToString() != "")
                {
                    model.AllotID = int.Parse(ds.Tables[0].Rows[0]["AllotID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OneSuggestion"] != null && ds.Tables[0].Rows[0]["OneSuggestion"].ToString() != "")
                {
                    model.OneSuggestion = ds.Tables[0].Rows[0]["OneSuggestion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TwoSuggstion"] != null && ds.Tables[0].Rows[0]["TwoSuggstion"].ToString() != "")
                {
                    model.TwoSuggstion = ds.Tables[0].Rows[0]["TwoSuggstion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TwoAuditUser"] != null && ds.Tables[0].Rows[0]["TwoAuditUser"].ToString() != "")
                {
                    model.TwoAuditUser = int.Parse(ds.Tables[0].Rows[0]["TwoAuditUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TwoAuditDate"] != null && ds.Tables[0].Rows[0]["TwoAuditDate"].ToString() != "")
                {
                    model.TwoAuditDate = DateTime.Parse(ds.Tables[0].Rows[0]["TwoAuditDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ThreeSuggsion"] != null && ds.Tables[0].Rows[0]["ThreeSuggsion"].ToString() != "")
                {
                    model.ThreeSuggsion = ds.Tables[0].Rows[0]["ThreeSuggsion"].ToString();
                } if (ds.Tables[0].Rows[0]["ThreeAuditUser"] != null && ds.Tables[0].Rows[0]["ThreeAuditUser"].ToString() != "")
                {
                    model.ThreeAuditUser = int.Parse(ds.Tables[0].Rows[0]["ThreeAuditUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ThreeAuditDate"] != null && ds.Tables[0].Rows[0]["ThreeAuditDate"].ToString() != "")
                {
                    model.ThreeAuditDate = DateTime.Parse(ds.Tables[0].Rows[0]["ThreeAuditDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FourSuggion"] != null && ds.Tables[0].Rows[0]["FourSuggion"].ToString() != "")
                {
                    model.FourSuggion = ds.Tables[0].Rows[0]["FourSuggion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FourAuditUser"] != null && ds.Tables[0].Rows[0]["FourAuditUser"].ToString() != "")
                {
                    model.FourAuditUser = int.Parse(ds.Tables[0].Rows[0]["FourAuditUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FourAuditDate"] != null && ds.Tables[0].Rows[0]["FourAuditDate"].ToString() != "")
                {
                    model.FourAuditDate = DateTime.Parse(ds.Tables[0].Rows[0]["FourAuditDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FiveSuggion"] != null && ds.Tables[0].Rows[0]["FiveSuggion"].ToString() != "")
                {
                    model.FiveSuggion = ds.Tables[0].Rows[0]["FiveSuggion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SixSuggsion"] != null && ds.Tables[0].Rows[0]["SixSuggsion"].ToString() != "")
                {
                    model.SixSuggsion = ds.Tables[0].Rows[0]["SixSuggsion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SevenSuggsion"] != null && ds.Tables[0].Rows[0]["SevenSuggsion"].ToString() != "")
                {
                    model.SevenSuggsion = ds.Tables[0].Rows[0]["SevenSuggsion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["EightSuggstion"] != null && ds.Tables[0].Rows[0]["EightSuggstion"].ToString() != "")
                {
                    model.EightSuggstion = ds.Tables[0].Rows[0]["EightSuggstion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditUser"] != null && ds.Tables[0].Rows[0]["AuditUser"].ToString() != "")
                {
                    model.AuditUser = ds.Tables[0].Rows[0]["AuditUser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditDate"] != null && ds.Tables[0].Rows[0]["AuditDate"].ToString() != "")
                {
                    model.AuditDate = ds.Tables[0].Rows[0]["AuditDate"].ToString();
                }
                if (ds.Tables[0].Rows[0]["InUser"] != null && ds.Tables[0].Rows[0]["InUser"].ToString() != "")
                {
                    model.InUser = int.Parse(ds.Tables[0].Rows[0]["InUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InDate"] != null && ds.Tables[0].Rows[0]["InDate"].ToString() != "")
                {
                    model.InDate = DateTime.Parse(ds.Tables[0].Rows[0]["InDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SprcialtyAuditCount"] != null && ds.Tables[0].Rows[0]["SprcialtyAuditCount"].ToString() != "")
                {
                    model.SprcialtyAuditCount = int.Parse(ds.Tables[0].Rows[0]["SprcialtyAuditCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HeadAuditCount"] != null && ds.Tables[0].Rows[0]["HeadAuditCount"].ToString() != "")
                {
                    model.HeadAuditCount = int.Parse(ds.Tables[0].Rows[0]["HeadAuditCount"].ToString());
                }

                if (ds.Tables[0].Rows[0]["TwoIsPass"] != null && ds.Tables[0].Rows[0]["TwoIsPass"].ToString() != "")
                {
                    model.TwoIsPass = ds.Tables[0].Rows[0]["TwoIsPass"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ThreeIsPass"] != null && ds.Tables[0].Rows[0]["ThreeIsPass"].ToString() != "")
                {
                    model.ThreeIsPass = ds.Tables[0].Rows[0]["ThreeIsPass"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FourIsPass"] != null && ds.Tables[0].Rows[0]["FourIsPass"].ToString() != "")
                {
                    model.FourIsPass = ds.Tables[0].Rows[0]["FourIsPass"].ToString();
                }
                //是否二次分配
                if (ds.Tables[0].Rows[0]["SecondValue"] != null && ds.Tables[0].Rows[0]["SecondValue"].ToString() != "")
                {
                    model.SecondValue = ds.Tables[0].Rows[0]["SecondValue"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectValueAuditRecord GetModelByProID(int proID, int AllotID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  * from cm_ProjectValueAuditRecord ");
            strSql.Append(" where ProSysNo=@ProSysNo and AllotID=@AllotID");
            SqlParameter[] parameters = {
					new SqlParameter("@ProSysNo", SqlDbType.Int,4),
                    new SqlParameter("@AllotID", SqlDbType.Int,4)
			};
            parameters[0].Value = proID;
            parameters[1].Value = AllotID;
            TG.Model.cm_ProjectValueAuditRecord model = new TG.Model.cm_ProjectValueAuditRecord();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["SysNo"] != null && ds.Tables[0].Rows[0]["SysNo"].ToString() != "")
                {
                    model.SysNo = int.Parse(ds.Tables[0].Rows[0]["SysNo"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProSysNo"] != null && ds.Tables[0].Rows[0]["ProSysNo"].ToString() != "")
                {
                    model.ProSysNo = int.Parse(ds.Tables[0].Rows[0]["ProSysNo"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotID"] != null && ds.Tables[0].Rows[0]["AllotID"].ToString() != "")
                {
                    model.AllotID = int.Parse(ds.Tables[0].Rows[0]["AllotID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OneSuggestion"] != null && ds.Tables[0].Rows[0]["OneSuggestion"].ToString() != "")
                {
                    model.OneSuggestion = ds.Tables[0].Rows[0]["OneSuggestion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TwoSuggstion"] != null && ds.Tables[0].Rows[0]["TwoSuggstion"].ToString() != "")
                {
                    model.TwoSuggstion = ds.Tables[0].Rows[0]["TwoSuggstion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TwoAuditUser"] != null && ds.Tables[0].Rows[0]["TwoAuditUser"].ToString() != "")
                {
                    model.TwoAuditUser = int.Parse(ds.Tables[0].Rows[0]["TwoAuditUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TwoAuditDate"] != null && ds.Tables[0].Rows[0]["TwoAuditDate"].ToString() != "")
                {
                    model.TwoAuditDate = DateTime.Parse(ds.Tables[0].Rows[0]["TwoAuditDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ThreeSuggsion"] != null && ds.Tables[0].Rows[0]["ThreeSuggsion"].ToString() != "")
                {
                    model.ThreeSuggsion = ds.Tables[0].Rows[0]["ThreeSuggsion"].ToString();
                } if (ds.Tables[0].Rows[0]["ThreeAuditUser"] != null && ds.Tables[0].Rows[0]["ThreeAuditUser"].ToString() != "")
                {
                    model.ThreeAuditUser = int.Parse(ds.Tables[0].Rows[0]["ThreeAuditUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ThreeAuditDate"] != null && ds.Tables[0].Rows[0]["ThreeAuditDate"].ToString() != "")
                {
                    model.ThreeAuditDate = DateTime.Parse(ds.Tables[0].Rows[0]["ThreeAuditDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FourSuggion"] != null && ds.Tables[0].Rows[0]["FourSuggion"].ToString() != "")
                {
                    model.FourSuggion = ds.Tables[0].Rows[0]["FourSuggion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FourAuditUser"] != null && ds.Tables[0].Rows[0]["FourAuditUser"].ToString() != "")
                {
                    model.FourAuditUser = int.Parse(ds.Tables[0].Rows[0]["FourAuditUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FourAuditDate"] != null && ds.Tables[0].Rows[0]["FourAuditDate"].ToString() != "")
                {
                    model.FourAuditDate = DateTime.Parse(ds.Tables[0].Rows[0]["FourAuditDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["FiveSuggion"] != null && ds.Tables[0].Rows[0]["FiveSuggion"].ToString() != "")
                {
                    model.FiveSuggion = ds.Tables[0].Rows[0]["FiveSuggion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SixSuggsion"] != null && ds.Tables[0].Rows[0]["SixSuggsion"].ToString() != "")
                {
                    model.SixSuggsion = ds.Tables[0].Rows[0]["SixSuggsion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SevenSuggsion"] != null && ds.Tables[0].Rows[0]["SevenSuggsion"].ToString() != "")
                {
                    model.SevenSuggsion = ds.Tables[0].Rows[0]["SevenSuggsion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditUser"] != null && ds.Tables[0].Rows[0]["AuditUser"].ToString() != "")
                {
                    model.AuditUser = ds.Tables[0].Rows[0]["AuditUser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditDate"] != null && ds.Tables[0].Rows[0]["AuditDate"].ToString() != "")
                {
                    model.AuditDate = ds.Tables[0].Rows[0]["AuditDate"].ToString();
                }
                if (ds.Tables[0].Rows[0]["InUser"] != null && ds.Tables[0].Rows[0]["InUser"].ToString() != "")
                {
                    model.InUser = int.Parse(ds.Tables[0].Rows[0]["InUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InDate"] != null && ds.Tables[0].Rows[0]["InDate"].ToString() != "")
                {
                    model.InDate = DateTime.Parse(ds.Tables[0].Rows[0]["InDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SprcialtyAuditCount"] != null && ds.Tables[0].Rows[0]["SprcialtyAuditCount"].ToString() != "")
                {
                    model.SprcialtyAuditCount = int.Parse(ds.Tables[0].Rows[0]["SprcialtyAuditCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HeadAuditCount"] != null && ds.Tables[0].Rows[0]["HeadAuditCount"].ToString() != "")
                {
                    model.HeadAuditCount = int.Parse(ds.Tables[0].Rows[0]["HeadAuditCount"].ToString());
                }

                if (ds.Tables[0].Rows[0]["TwoIsPass"] != null && ds.Tables[0].Rows[0]["TwoIsPass"].ToString() != "")
                {
                    model.TwoIsPass = ds.Tables[0].Rows[0]["TwoIsPass"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ThreeIsPass"] != null && ds.Tables[0].Rows[0]["ThreeIsPass"].ToString() != "")
                {
                    model.ThreeIsPass = ds.Tables[0].Rows[0]["ThreeIsPass"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FourIsPass"] != null && ds.Tables[0].Rows[0]["FourIsPass"].ToString() != "")
                {
                    model.FourIsPass = ds.Tables[0].Rows[0]["FourIsPass"].ToString();
                }
                //是否二次分配
                if (ds.Tables[0].Rows[0]["SecondValue"] != null && ds.Tables[0].Rows[0]["SecondValue"].ToString() != "")
                {
                    model.SecondValue = ds.Tables[0].Rows[0]["SecondValue"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SysNo,ProSysNo,AllotID,Status,OneSuggestion,TwoSuggstion,ThreeSuggsion,FourSuggion,FiveSuggion,SixSuggsion,SevenSuggsion,AuditUser,AuditDate,InUser,InDate ");
            strSql.Append(" FROM cm_ProjectValueAuditRecord ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectValueAuditRecord GetModelByProSysNo(int proID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" * ");
            strSql.Append(" from cm_ProjectValueAuditRecord ");
            strSql.Append(" where ProSysNo=" + proID + " order by SysNo DESC");

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

            reader.Read();

            TG.Model.cm_ProjectValueAuditRecord coperationAuditEntity = TG.Common.EntityBuilder.EntityBuilder<TG.Model.cm_ProjectValueAuditRecord>.BuilderEntity(reader);

            reader.Close();

            return coperationAuditEntity;
        }

        /// <summary>
        /// 查询合同基本信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        //        public List<cm_projectValueAllotView> GetProjectValueAllotView(string whereSql)
        //        {
        //            StringBuilder strSql = new StringBuilder();
        //            strSql.Append(@"SELECT * from
        //                                            (  
        //                                                 select
        //                                                cpr_Id,
        //                                                cst_Id,
        //                                                 cpr_No,
        //                                                 cpr_Type,
        //                                                 cpr_Name,
        //                                                  cpr_Unit,
        //                                                 CM.cpr_Acount,
        //                                                cpr_ShijiAcount,
        //                                                 cpr_Process,
        //                                                 cpr_SignDate,
        //                                                 cpr_DoneDate,
        //                                                 cpr_Mark,
        //                                                 BuildArea,
        //                                                ChgPeople,
        //                                                ChgPhone,
        //                                                CM.ChgJia,
        //                                                ChgJiaPhone,
        //                                                CM.BuildType,
        //                                                CM.UpdateBy,
        //                                                ISNULL( (SELECT Convert(decimal(18,2),SUM(Acount)) FROM cm_ProjectCharge  WHERE CM.cpr_Id=cprID AND (Status='C' OR Status='E')),0.00)as PayShiCount,
        //                                                 ISNULL(( SELECT SUM(AllotCount)FROM dbo.cm_ProjectValueAllot  where Status='S' AND CM.cpr_Id=cpr_ID GROUP BY cpr_ID ) ,0.00) as AllotCount
        //                                                FROM dbo.cm_Coperation AS CM 
        //                                                INNER JOIN cm_Project P
        //                                                ON P.CoperationSysNo=CM.cpr_Id
        //                                                LEFT JOIN cm_ProjectPlanAudit AU
        //                                                ON P.pro_ID=AU.ProjectSysNo
        //                                                WHERE AU.Status='F'
        //                                             ) test WHERE 1=1 AND PayShiCount>0.00 AND AllotCount<PayShiCount ");
        //            if (whereSql.Trim() != "")
        //            {
        //                strSql.Append(whereSql);
        //            }
        //            strSql.Append(" order by cpr_Id DESC");
        //            List<cm_projectValueAllotView> resultList = new List<cm_projectValueAllotView>();

        //            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

        //            while (reader.Read())
        //            {
        //                cm_projectValueAllotView auditListView = new cm_projectValueAllotView
        //                {
        //                    pro_Id = Convert.ToInt32(reader["cpr_Id"]),
        //                    cst_Id = Convert.ToInt32(reader["cst_Id"]),
        //                    cpr_No = reader["cpr_No"] == null ? string.Empty : reader["cpr_No"].ToString(),
        //                    cpr_Type = reader["cpr_Type"] == null ? string.Empty : reader["cpr_Type"].ToString(),
        //                    //cpr_Type2 = reader["cpr_Type2"] == null ? string.Empty : reader["cpr_Type2"].ToString(),
        //                    Pro_Name = reader["cpr_Name"] == null ? string.Empty : reader["cpr_Name"].ToString(),
        //                    cpr_Unit = reader["cpr_Unit"] == null ? string.Empty : reader["cpr_Unit"].ToString(),
        //                    cpr_Acount = reader["cpr_Acount"] == null ? 0 : Convert.ToDecimal(reader["cpr_Acount"]),
        //                    cpr_ShijiAcount = reader["cpr_ShijiAcount"] == null ? 0 : Convert.ToDecimal(reader["cpr_ShijiAcount"]),
        //                    //cpr_Touzi = reader["cpr_Touzi"] == null ? 0 : Convert.ToDecimal(reader["cpr_Touzi"]),
        //                    //cpr_ShijiTouzi = reader["cpr_ShijiTouzi"] == null ? 0 : Convert.ToDecimal(reader["cpr_ShijiTouzi"]),
        //                    cpr_Process = reader["cpr_Process"] == null ? string.Empty : reader["cpr_Process"].ToString(),
        //                    cpr_SignDate = reader["cpr_SignDate"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["cpr_SignDate"]),
        //                    cpr_DoneDate = reader["cpr_DoneDate"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["cpr_DoneDate"]),
        //                    cpr_Mark = reader["cpr_Mark"] == null ? string.Empty : reader["cpr_Mark"].ToString(),
        //                    BuildArea = reader["BuildArea"] == null ? string.Empty : reader["BuildArea"].ToString(),
        //                    ChgPeople = reader["ChgPeople"] == null ? string.Empty : reader["ChgPeople"].ToString(),
        //                    ChgPhone = reader["ChgPhone"] == null ? string.Empty : reader["ChgPhone"].ToString(),
        //                    ChgJia = reader["ChgJia"] == null ? string.Empty : reader["ChgJia"].ToString(),
        //                    ChgJiaPhone = reader["ChgJiaPhone"] == null ? string.Empty : reader["ChgJiaPhone"].ToString(),
        //                    PayShiCount = reader["PayShiCount"] == null ? 0 : Convert.ToDecimal(reader["PayShiCount"]),
        //                    Allotcount = reader["Allotcount"] == null ? 0 : Convert.ToDecimal(reader["Allotcount"]),
        //                    BuildType = reader["BuildType"] == null ? string.Empty : reader["BuildType"].ToString()

        //                };
        //                resultList.Add(auditListView);
        //            }

        //            return resultList;
        //        }

        /// <summary>
        /// 查询合同基本信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet GetProjectValueAllotedView(string whereSql)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT * from
                                            (  
                                                select
                                                cpr_Id,
                                                cst_Id,
                                                 cpr_No,
                                                 cpr_Type,
                                                 cpr_Name,
                                                  cpr_Unit,
                                                 CM.cpr_Acount,
                                                cpr_ShijiAcount,
                                                 cpr_Process,
                                                 cpr_SignDate,
                                                 cpr_DoneDate,
                                                 cpr_Mark,
                                                 BuildArea,
                                                ChgPeople,
                                                ChgPhone,
                                                CM.ChgJia,
                                                ChgJiaPhone,
                                                CM.BuildType,
                                                CM.UpdateBy,
                                                ISNULL( (SELECT Convert(decimal(18,2),SUM(Acount)) FROM cm_ProjectCharge  WHERE CM.cpr_Id=cprID AND(Status='C' OR Status='E')),0.00)as PayShiCount,
                                                 ISNULL(( SELECT SUM(AllotCount)FROM dbo.cm_ProjectValueAllot  where Status='S' AND CM.cpr_Id=cpr_ID GROUP BY cpr_ID ) ,0.00) as AllotCount
                                                FROM dbo.cm_Coperation AS CM 
                                                INNER JOIN cm_Project P
                                                ON P.CoperationSysNo=CM.cpr_Id
                                                LEFT JOIN cm_ProjectPlanAudit AU
                                                ON P.pro_ID=AU.ProjectSysNo
                                                WHERE AU.Status='F'
                                             ) test WHERE 1=1 AND PayShiCount>0.00 AND AllotCount>=PayShiCount ");
            if (whereSql.Trim() != "")
            {
                strSql.Append(whereSql);
            }
            strSql.Append(" order by cpr_Id DESC");

            return DbHelperSQL.Query(strSql.ToString());
        }


        public TG.Model.cm_ProjectFeeAllotConfig GetValueAllotAuditConfigByPostion(int position)
        {
            string sql = "select c.SysNo,c.ProcessDescription,c.RoleSysNo,c.Position,r.RoleName,r.Users from cm_ProjectFeeAllotConfig c join cm_Role r on c.RoleSysNo = r.SysNo where c.Position =" + position + "";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            TG.Model.cm_ProjectFeeAllotConfig resultEntity = EntityBuilder<TG.Model.cm_ProjectFeeAllotConfig>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }


        /// <summary>
        /// 得打数据列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetDataList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                            SELECT 
                            ID,
                            cpr_ID,
                            AllotTimes,
                            AllotCount,
                            AllotDate,
                            persent,
                            PaidValuePercent,
                            (PaidValuePercent*AllotCount) AS PaidValueCount,
                            DesignManagerPercent,
                            (DesignManagerPercent*AllotCount) DesignManagerCount,
                            AllotValuePercent,
                            EconomyValuePercent,
                            (EconomyValuePercent*AllotCount) EconomyValueCount,
                            UnitValuePercent,
                            (UnitValuePercent*AllotCount) UnitValueCount,
                           ISNUll((SELECT SUM(Acount) FROM cm_ProjectCharge  WHERE Status<>'B' And cprID=cm_ProjectValueAllot.cpr_ID group by chg_id),0.00)AS PayShiCount
                            FROM dbo.cm_ProjectValueAllot WHERE ID=@id Order BY AllotDate"
                        );
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = strWhere;

            return DbHelperSQL.Query(strSql.ToString(), parameters);

        }

        /// <summary>
        /// 得打数据列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetAllotDataList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                                 SELECT  
                                 * ,
                                 (select isnull(SUM(LoanValueCount),0) from cm_ProjectValueAllot  a
                                    inner join cm_Project b
                                    on a.pro_ID=b.pro_ID
                                    where b.Unit= ( select  Unit from cm_Project where pro_ID=(select  pro_ID from cm_ProjectValueAllot where ID=e.ID))
                                     and  a.ActualAllountTime=( select  ActualAllountTime from cm_ProjectValueAllot where ID=e.ID)
                                     and a.tStatus='S'
                                  )
                                  as totalLoanValueCount
                                  
                                  FROM [dbo].[cm_ProjectValueAllot] e WHERE e.ID=@id "
                        );
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = strWhere;

            return DbHelperSQL.Query(strSql.ToString(), parameters);

        }
        /// <summary>
        /// 得打数据列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetComList(string proID, string year)
        {
            string sql = "";
            if (string.IsNullOrEmpty(year))
            {
                sql = string.Format(@" select
                                        pro_ID,
                                        pro_name,
                                        Unit,
                                        cpr_ShijiAcount,
                                        ISNULL(CM.IsParamterEdit,0) as IsParamterEdit,
                                        ISNULL( PC.PayShiCount,0.00)as PayShiCount,
                                       ISNULL(( SELECT SUM(AllotCount)FROM dbo.cm_ProjectValueAllot  where Status <>'D' and SecondValue='1'  AND PJ.pro_ID=pro_ID GROUP BY pro_ID 
                                        ) ,0.00)  as AllotCount,
                                        ISNULL(ISTrunEconomy,0)AS ISTrunEconomy,
                                        ISNULL(ISHvac,0) AS IsTrunHavc,
                                        ISNULL(ReferenceSysNo,'0') as ReferenceSysNo
                                         FROM  dbo.cm_Project PJ 
                                         LEFT JOIN dbo.cm_Coperation AS CM 
                                         ON CM.cpr_Id=PJ.CoperationSysNo 
                                         LEFT JOIN 
                                         (SELECT Convert(decimal(18,4),SUM(Acount)) AS PayShiCount ,cprID FROM cm_ProjectCharge WHERE  Status<>'B' GROUP BY cprID  ) AS PC
                                         ON PC.cprID=CM.cpr_Id Where pro_ID={0} ", proID);
            }
            else
            {
                sql = string.Format(@" select
                                        pro_ID,
                                        pro_name,
                                        Unit,
                                        cpr_ShijiAcount,
                                        ISNULL(CM.IsParamterEdit,0) as IsParamterEdit,
                                        ISNULL( PC.PayShiCount,0.00)as PayShiCount,
                                        ISNULL(( SELECT SUM(AllotCount)FROM dbo.cm_ProjectValueAllot  where Status <>'D' and SecondValue='1' and ActualAllountTime='" + year + "' AND PJ.pro_ID=pro_ID GROUP BY pro_ID ) ,0.00) " + @"
                                       
                                         as AllotCount, 
                                        ISNULL(ISTrunEconomy,0)AS ISTrunEconomy,
                                        ISNULL(ISHvac,0) AS IsTrunHavc,
                                        ISNULL(ReferenceSysNo,'0') as ReferenceSysNo
                                         FROM  dbo.cm_Project PJ 
                                         LEFT JOIN dbo.cm_Coperation AS CM 
                                         ON CM.cpr_Id=PJ.CoperationSysNo 
                                         LEFT JOIN 
                                         (SELECT Convert(decimal(18,4),SUM(Acount)) AS PayShiCount ,cprID FROM cm_ProjectCharge WHERE  Status<>'B' and InAcountTime  between '{0}-1-1' and '{0}-12-31' GROUP BY cprID  ) AS PC
                                         ON PC.cprID=CM.cpr_Id Where pro_ID={1} ", year, proID);
            }


            return DbHelperSQL.Query(sql);

        }

        /// <summary>
        /// 得打数据列表--得到二次分配记录信息
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectSecondValueList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                          select p.pro_ID,pro_name,Unit,pro_startTime,P.UpdateBy,P.InsertUserID 
						  TheDeptValueCount,
						  AuditCount,
						  DesignCount,
						  HavcCount,
						  (TheDeptValueCount+AuditCount+DesignCount+HavcCount)totalCount,
						  ISNULL(ReferenceSysNo,'0') as ReferenceSysNo,
						   ISNULL(CM.IsParamterEdit,0) as IsParamterEdit,
                            ISNULL(( select isnull(SUM(ComValue),0) as programCount  from cm_CompensationSet  where ValueYear=year(getdate()) and MemberId in (select mem_id from tg_member
                            left join 
                            tg_unit on
                            tg_member.mem_Unit_ID=tg_unit.unit_ID
                            where tg_unit.unit_Name=@id)),0)as programCount,
						  ISNULL(( SELECT SUM(AllotCount)FROM dbo.cm_ProjectValueAllot  where Status <>'D'and SecondValue='2'  AND P.pro_ID=pro_ID GROUP BY pro_ID ) ,0.00) as AllotCount,
						  ISNULL(ReferenceSysNo,'0') as ReferenceSysNo
						  from cm_project p
						   join 
							(select ProID,
							isnull(SUM(TheDeptValueCount),0) as TheDeptValueCount ,
							isnull(SUM(AuditCount),0) as AuditCount,
							isnull(SUM(DesignCount),0) as DesignCount,
							isnull(SUM(HavcCount),0) as HavcCount
							 from cm_ProjectSecondValueAllot
							 where Status='S'
							 group by ProID)second
						 on p.pro_ID=second.ProID
						 LEFT JOIN dbo.cm_Coperation AS CM 
                          ON CM.cpr_Id=P.CoperationSysNo  Where p.Unit=@id "
                        );
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.NVarChar,16)
			};
            parameters[0].Value = strWhere;

            return DbHelperSQL.Query(strSql.ToString(), parameters);

        }

        /// <summary>
        /// 分配记录表
        /// </summary>
        /// <param name="allotAuditSysNo"></param>
        /// <returns></returns>
        public TG.Model.cm_ProjectValueAuditRecord GetAllotAuditEntity(int allotAuditSysNo)
        {
            string sql = " SELECT * FROM dbo.cm_ProjectValueAuditRecord WHERE SysNo=" + allotAuditSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            TG.Model.cm_ProjectValueAuditRecord resultEntity = EntityBuilder<TG.Model.cm_ProjectValueAuditRecord>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }

        /// <summary>
        /// 得打数据列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetCprProcessDataList(string itemType, int ProjectStatues)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                         SELECT [ID]
                          ,[ProjectStatues]
                          ,[ItemType]
                          ,[ProgramPercent]
                          ,[preliminaryPercent]
                          ,[WorkDrawPercent]
                          ,[LateStagePercent]
                          from [cm_ProjectStageValueConfig]
                          where ProjectStatues=@ProjectStatues and ItemType=@ItemType
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@ProjectStatues", SqlDbType.Int,4),
					new SqlParameter("@ItemType", SqlDbType.NVarChar,50)
			};
            parameters[0].Value = ProjectStatues;
            parameters[1].Value = itemType;

            return DbHelperSQL.Query(strSql.ToString(), parameters);

        }



        /// <summary>
        /// 得打数据列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetCprProcessValueDataList(string itemType, int ProjectStatues, decimal allotCount, string specailtyName, string isRounding)
        {

            SqlParameter[] parameters = {
                    new SqlParameter("@ProjectStatues", SqlDbType.Int,4),
					new SqlParameter("@ItemType", SqlDbType.NVarChar,50),
                    new SqlParameter("@UNITCOUNT",SqlDbType.Decimal,18),
                     new SqlParameter("@SpecialtyName",SqlDbType.NVarChar,200),
                      new SqlParameter("@isRounding",SqlDbType.NVarChar,200)
			};
            parameters[0].Value = ProjectStatues;
            parameters[1].Value = itemType;
            parameters[2].Value = allotCount;
            parameters[3].Value = specailtyName;
            parameters[4].Value = isRounding;

            return DbHelperSQL.RunProcedure("P_CM_ProjectValue", parameters, "ds");


        }


        /// <summary>
        /// 室外工程系数
        /// </summary>
        /// <param name="specailtyName"></param>
        /// <returns></returns>
        public DataSet GetShiWaiProcessValueDataList(string specailtyName)
        {
            string sql = @"SELECT * FROM dbo.cm_ProjectOutDoorValueConfig where  status=4 and Specialty in ('"+specailtyName+"')  ";
            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 得到产值分配人员信息
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectValueByMemberDataList(int proid, int allotId, int speID)
        {

            SqlParameter[] parameters = {
                    new SqlParameter("@PROID", SqlDbType.Int,4),
					 new SqlParameter("@AllotID", SqlDbType.Int,4),
                     new SqlParameter("@SpeID", SqlDbType.Int,4)
			};
            parameters[0].Value = proid;
            parameters[1].Value = allotId;
            parameters[2].Value = speID;
            return DbHelperSQL.RunProcedure("P_CM_ProjectValueAllotByMember", parameters, "ds");


        }

        /// <summary>
        /// 得到产值分配人员信息--转土建
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetTranBulidingProjectValueByMemberDataList(int proid)
        {

            string sql = @" SELECT M.mem_ID, mem_Name, mem_RoleIDs ,r.mem_isDesigner, s.spe_Name,s.spe_ID,m.mem_Principalship_ID
		                FROM   dbo.tg_relation R
		               INNER JOIN tg_member M
		               ON R.mem_ID=M.mem_ID
		               join tg_speciality  s on s.spe_ID = m.mem_Speciality_ID
		   
		               WHERE   r.pro_ID= @PROID 
                      and s.spe_Name in ('建筑','结构','给排水','电气')
                        and  r.mem_RoleIDs <>'1'
                                       and r.mem_RoleIDs <>'5'
                                       and r.mem_RoleIDs <>'6' 
                       UNION     
                       --  审定 助理 设总 参与 设计人员            
                       SELECT DISTINCT M.mem_ID, mem_Name, mem_RoleIDs ,r.mem_isDesigner, s.spe_Name,s.spe_ID,m.mem_Principalship_ID
		                FROM   dbo.tg_relation R
		               INNER JOIN tg_member M
		                 ON R.mem_ID=M.mem_ID
		                join tg_speciality  s on s.spe_ID = m.mem_Speciality_ID
		   
		                 WHERE   r.pro_ID= @PROID 
                         and s.spe_Name in ('建筑','结构','给排水','电气')
                                           and( r.mem_RoleIDs <>'1'
                                           or r.mem_RoleIDs <>'5'
                                           or r.mem_RoleIDs <>'6'
                                            ) and r.mem_isDesigner=1    order by s.spe_ID     ";

            SqlParameter[] parameters = {
                    new SqlParameter("@PROID", SqlDbType.Int,4)
			};

            parameters[0].Value = proid;

            return DbHelperSQL.Query(sql, parameters);

        }

        /// <summary>
        /// 得到产值没有分配人员信息
        /// </summary>
        /// <param name="proid"></param>
        /// <param name="speID"></param>
        /// <returns></returns>
        public DataSet GetProjectValueByMemberNoData(int proid, int speID)
        {
            string sql = @"SELECT M.mem_ID, mem_Name, mem_RoleIDs ,r.mem_isDesigner, s.spe_Name,s.spe_ID
		                    FROM   dbo.tg_relation R
		                    INNER JOIN tg_member M
		                    ON R.mem_ID=M.mem_ID
		                    join tg_speciality  s on s.spe_ID = m.mem_Speciality_ID
		                    WHERE   r.pro_ID= " + proid + " and s.spe_ID=" + speID + "";

            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 得到人员分配之后的详细信息
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetProjectValueByMemberAcount(int allotID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                        select a.ItemType,a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,a.DesignPercent,
	                   cast(round(a.DesignCount,0) as int) DesignCount ,a.SpecialtyHeadPercent,cast(round(a.SpecialtyHeadCount,0) as int) SpecialtyHeadCount,
	                    a.AuditPercent,cast(round(a.AuditCount,0) as int)as AuditCount,a.ProofreadPercent,cast(round(a.ProofreadCount,0) as int) as ProofreadCount
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                    where A.AllotId=@allotID
                         and TranType is null and IsExternal='0'
	                     union all
                         select a.ItemType,a.mem_ID,b.mem_Name+'(外聘)' as mem_Name,c.spe_Name,c.spe_ID ,a.DesignPercent,
	                   cast(round(a.DesignCount,0) as int) DesignCount ,a.SpecialtyHeadPercent,cast(round(a.SpecialtyHeadCount,0) as int) SpecialtyHeadCount,
	                    a.AuditPercent,cast(round(a.AuditCount,0) as int)as AuditCount,a.ProofreadPercent,cast(round(a.ProofreadCount,0) as int) as ProofreadCount
	                    from cm_ProjectValueByMemberDetails a
	                    join cm_externalMember b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
                          where A.AllotId=@allotID
                         and TranType is null and IsExternal='1'
                           order by c.spe_ID
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@allotID", SqlDbType.Int,4)
			};

            parameters[0].Value = allotID;

            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 得到人员分配之后的详细信息--转土建所
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetTranBulidingProjectValueByMemberAcount(int allotID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                        select a.ItemType,a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,a.DesignPercent,
	                   cast(round(a.DesignCount,0) as int) DesignCount ,a.SpecialtyHeadPercent,cast(round(a.SpecialtyHeadCount,0) as int) SpecialtyHeadCount,
	                    a.AuditPercent,cast(round(a.AuditCount,0) as int)as AuditCount,a.ProofreadPercent,cast(round(a.ProofreadCount,0) as int) as ProofreadCount
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                    where A.AllotId=@allotID
                         and TranType='tjs' and IsExternal='0'
	                     union all
                         select a.ItemType,a.mem_ID,b.mem_Name+'(外聘)' as mem_Name,c.spe_Name,c.spe_ID ,a.DesignPercent,
	                   cast(round(a.DesignCount,0) as int) DesignCount ,a.SpecialtyHeadPercent,cast(round(a.SpecialtyHeadCount,0) as int) SpecialtyHeadCount,
	                    a.AuditPercent,cast(round(a.AuditCount,0) as int)as AuditCount,a.ProofreadPercent,cast(round(a.ProofreadCount,0) as int) as ProofreadCount
	                    from cm_ProjectValueByMemberDetails a
	                    join cm_externalMember b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
                          where A.AllotId=@allotID
                         and TranType='tjs' and IsExternal='1'
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@allotID", SqlDbType.Int,4)
			};

            parameters[0].Value = allotID;

            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 得到人员分配之后的详细信息--经济所
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetjjsProjectValueByMemberAcount(int proID, int AllotID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                        select a.ItemType,a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,a.DesignPercent,
	                   cast(round(a.DesignCount,0) as int) DesignCount ,a.SpecialtyHeadPercent,cast(round(a.SpecialtyHeadCount,0) as int) SpecialtyHeadCount,
	                    a.AuditPercent,cast(round(a.AuditCount,0) as int)as AuditCount,a.ProofreadPercent,cast(round(a.ProofreadCount,0) as int) as ProofreadCount
                         ,IsExternal
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                    where A.ProID=@proID and IsExternal='0'
                          and A.AllotID=@AllotID and TranType is null
	                    union all
	                    select a.ItemType,a.mem_ID,b.mem_Name+'(外聘)' as mem_Name,c.spe_Name,c.spe_ID ,a.DesignPercent,
	                   cast(round(a.DesignCount,0) as int) DesignCount ,a.SpecialtyHeadPercent,cast(round(a.SpecialtyHeadCount,0) as int) SpecialtyHeadCount,
	                    a.AuditPercent,cast(round(a.AuditCount,0) as int)as AuditCount,a.ProofreadPercent,cast(round(a.ProofreadCount,0) as int) as ProofreadCount,
                        IsExternal
	                    from cm_ProjectValueByMemberDetails a
	                    join cm_externalMember b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
                        where A.ProID=@proID and IsExternal='1' and TranType is null
                         and A.AllotID=@AllotID
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@proID", SqlDbType.Int,4),
                     new SqlParameter("@AllotID", SqlDbType.Int,4)
			};

            parameters[0].Value = proID;
            parameters[1].Value = AllotID;
            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 审核人查看审核记录
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetIsAuditedUserAcount(int allotID, int speID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                        select a.ItemType,a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,a.DesignPercent,AuditUser,
	                   cast(round(a.DesignCount,0) as int) DesignCount ,a.SpecialtyHeadPercent,cast(round(a.SpecialtyHeadCount,0) as int) SpecialtyHeadCount,
	                    a.AuditPercent,cast(round(a.AuditCount,0) as int)as AuditCount,a.ProofreadPercent,cast(round(a.ProofreadCount,0) as int) as ProofreadCount,
                         b.mem_Principalship_ID,IsExternal
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                    where A.AllotId=@allotID
                         and  b.mem_Speciality_ID=@speID
                         and a.IsExternal='0'
                         and TranType is null
                         union all
	                     select a.ItemType,a.mem_ID,b.mem_Name+'(外聘)' as mem_Name,c.spe_Name,c.spe_ID ,a.DesignPercent,AuditUser,
	                   cast(round(a.DesignCount,0) as int) DesignCount ,a.SpecialtyHeadPercent,cast(round(a.SpecialtyHeadCount,0) as int) SpecialtyHeadCount,
	                    a.AuditPercent,cast(round(a.AuditCount,0) as int)as AuditCount,a.ProofreadPercent,cast(round(a.ProofreadCount,0) as int) as ProofreadCount,
                        '',IsExternal
	                    from cm_ProjectValueByMemberDetails a
	                    join cm_externalMember b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
                        where A.AllotId=@allotID
                         and  b.mem_Speciality_ID=@speID
                         and a.IsExternal='1'
                         and TranType is null
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@allotID", SqlDbType.Int,4),
                     new SqlParameter("@speID", SqlDbType.Int,4)
			};

            parameters[0].Value = allotID;
            parameters[1].Value = speID;
            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 判断是人员是否已保存
        /// </summary>
        /// <param name="proName"></param>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public bool UserValueIsSaved(int speID, int allotID)
        {
            string sql = @"select COUNT(a.mem_id) from cm_ProjectValueByMemberDetails a
	                     join tg_member b
	                     on a.mem_ID=b.mem_ID
	                     where  IsExternal='0' and  b.mem_Speciality_ID=" + speID + " and a.AllotID=" + allotID + "";
            DataTable dt = DbHelperSQL.Query(sql).Tables[0];
            if (int.Parse(dt.Rows[0][0].ToString()) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到个人分配明细
        /// </summary>
        /// <param name="allotID"></param>
        /// <param name="useID"></param>
        /// <returns></returns>
        public DataSet GetUserValueByMemberAcount(int allotID, int useID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                         select a.*,b.mem_Name from cm_ProjectValueByMemberDetails a left join tg_member b on a.mem_ID=b.mem_ID where AllotID=@allotID and a.mem_ID=@mem_ID and DesignType is null
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@allotID", SqlDbType.Int,4),
                      new SqlParameter("@mem_ID", SqlDbType.Int,4)
			};

            parameters[0].Value = allotID;
            parameters[1].Value = useID;
            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 得到个人分配明细-专业
        /// </summary>
        /// <param name="allotID"></param>
        /// <param name="useID"></param>
        /// <returns></returns>
        public DataSet GetUserValueBySpeMemberAcount(int allotID, int useID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                         select a.*,b.mem_Name from cm_ProjectValueByMemberDetails a left join tg_member b on a.mem_ID=b.mem_ID 
                        inner join (select mem_Speciality_ID,mem_ID  from tg_member where mem_ID=@mem_ID ) c
                         on  b.mem_Speciality_ID=c.mem_Speciality_ID
                         where AllotID=@allotID 
                         --and a.mem_ID=@mem_ID 
                         and DesignType is null
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@allotID", SqlDbType.Int,4),
                      new SqlParameter("@mem_ID", SqlDbType.Int,4)
			};

            parameters[0].Value = allotID;
            parameters[1].Value = useID;
            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 人员分配产值
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetProjectValueByMemberTotalAcount(int allotID, string UnitName)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                          select  mem_ID,mem_Name,spe_Name,spe_ID , SUM(totalCount) as totalCount
                         from (
                         select a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,Sum(cast(round((DesignCount+SpecialtyHeadCount+AuditCount+ProofreadCount),0) as int)) as totalCount
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                    join tg_unit u
	                    on u.unit_ID=b.mem_Unit_ID
	                    where  A.AllotId=@allotID AND TranType is null and IsExternal='0' and u.unit_Name like '%" + UnitName + "%'" + @" 
                        group by a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID 
                         union all
                       select a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,Sum(cast(round(( ISNULL( DesignManagerCount,0)),0) as int)) as totalCount
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                    join tg_unit u
	                    on u.unit_ID=b.mem_Unit_ID
	                    where  A.AllotId=@allotID AND TranType is null and DesignType='designManager'  and u.unit_Name like '%" + UnitName + "%'" + @" 
                        group by a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID 
                        ) a  group by mem_ID,mem_Name,spe_Name,spe_ID 

                         select  mem_ID,mem_Name,spe_Name,spe_ID , SUM(totalCount) as totalCount
                         from (
	                   select a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,Sum(cast(round((DesignCount+SpecialtyHeadCount+AuditCount+ProofreadCount),0) as int)) as totalCount
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                   join tg_unit u
	                    on u.unit_ID=b.mem_Unit_ID
	                    where A.AllotId=@allotID AND TranType is null and IsExternal='0' and u.unit_Name not like '%" + UnitName + "%'" + @" 
                        group by a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID
                        union all
                       select a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,Sum(cast(round((ISNULL( DesignManagerCount,0)),0) as int)) as totalCount
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                   join tg_unit u
	                    on u.unit_ID=b.mem_Unit_ID
	                    where A.AllotId=@allotID AND TranType is null and  DesignType='designManager' and u.unit_Name not like '%" + UnitName + "%'" + @" 
                        group by a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID
                        ) a  group by mem_ID,mem_Name,spe_Name,spe_ID 

                        select a.mem_ID,b.mem_Name+'(外聘)' as mem_Name,c.spe_Name,c.spe_ID ,Sum(cast(round((DesignCount+SpecialtyHeadCount+AuditCount+ProofreadCount),0) as int)) as totalCount
	                    from cm_ProjectValueByMemberDetails a
	                    join cm_externalMember b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                     join cm_Project p
	                    on p.pro_ID=a.ProID
	                    where  A.AllotId=@allotID AND TranType is null  and IsExternal='1'
                        group by a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID 
	                    order by c.spe_ID
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@allotID", SqlDbType.Int,4)
			};

            parameters[0].Value = allotID;

            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 人员分配产值--打印经济所 暖通所
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetTranProjectValueByMemberTotalAcount(string strWHere, string UnitName)
        {

            string sql = @" 
                          select  mem_ID,mem_Name,spe_Name,spe_ID , SUM(totalCount) as totalCount
                        from
                        (
                        select a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,Sum(cast(round((DesignCount+SpecialtyHeadCount+AuditCount+ProofreadCount),0) as int)) as totalCount
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                     join tg_unit u
	                    on u.unit_ID=b.mem_Unit_ID
	                    where 1=1 
                       AND u.unit_Name like '" + UnitName + "' and IsExternal='0'   " + strWHere + " " + @" 
                        group by a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID 
                         union all
                         select a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,Sum(cast(round((ISNULL( DesignManagerCount,0)),0) as int)) as totalCount
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                     join tg_unit u
	                    on u.unit_ID=b.mem_Unit_ID
	                    where 1=1 
                       AND u.unit_Name like '" + UnitName + "' and DesignType='designManager'   " + strWHere + " " + @" 
                        group by a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID 
                        ) a  group by mem_ID,mem_Name,spe_Name,spe_ID 

                        select  mem_ID,mem_Name,spe_Name,spe_ID , SUM(totalCount) as totalCount
                        from
                        (
	                     select a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,Sum(cast(round((DesignCount+SpecialtyHeadCount+AuditCount+ProofreadCount),0) as int)) as totalCount
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                    join tg_unit u
	                    on u.unit_ID=b.mem_Unit_ID
	                    where 1=1  AND u.unit_Name not like '" + UnitName + "' and IsExternal='0'   " + strWHere + " " + @" 
	                     group by a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID   
                        union all
                        select a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,Sum(cast(round((ISNULL( DesignManagerCount,0)),0) as int)) as totalCount
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                    join tg_unit u
	                    on u.unit_ID=b.mem_Unit_ID
	                    where 1=1  AND u.unit_Name not like '" + UnitName + "' and DesignType='designManager'   " + strWHere + " " + @" 
	                     group by a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID   
                         ) a  group by mem_ID,mem_Name,spe_Name,spe_ID 

	                    select a.mem_ID,b.mem_Name+'(外聘)' as mem_Name,c.spe_Name,c.spe_ID ,
	                    Sum(cast(round((DesignCount+SpecialtyHeadCount+AuditCount+ProofreadCount),0) as int)) as totalCount 
	                     from cm_ProjectValueByMemberDetails a 
	                    join cm_externalMember b on a.mem_ID=b.mem_ID 
	                     join tg_speciality c on b.mem_Speciality_ID=c.spe_ID 
	                      where 1=1 and a.IsExternal='1'  " + strWHere + " group by a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID  ";

            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 得到策划人员专业
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public DataSet GetPlanUserSpecialty(int proID)
        {
            //2013-10-12增加 and s.spe_Name in ('建筑','结构','给排水','电气') 这里面的参与产值分配
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                          select DISTINCT s.spe_Name,s.spe_ID
                         from tg_relation r join tg_member u
                         on u.mem_ID = r.mem_ID
                          join tg_speciality  s on s.spe_ID = u.mem_Speciality_ID
                          join cm_Project p
                          on p.ReferenceSysNo=r.pro_ID
                          where p.pro_ID=@proID
                          and s.spe_Name in ('建筑','结构','给排水','电气')
                           and r.mem_RoleIDs <>'1'
                           and r.mem_RoleIDs <>'5'
                           and r.mem_RoleIDs <>'6'
                          union
                           select DISTINCT s.spe_Name,s.spe_ID
                         from tg_relation r join tg_member u
                         on u.mem_ID = r.mem_ID
                          join tg_speciality  s on s.spe_ID = u.mem_Speciality_ID
                          join cm_Project p
                          on p.ReferenceSysNo=r.pro_ID
                          where p.pro_ID=@proID
                          and s.spe_Name in ('建筑','结构','给排水','电气')
                           and( r.mem_RoleIDs <>'1'
                           or r.mem_RoleIDs <>'5'
                           or r.mem_RoleIDs <>'6'
                           ) and r.mem_isDesigner=1
                         order by s.spe_ID 
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@proID", SqlDbType.Int,4)
			};

            parameters[0].Value = proID;

            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 取得设总及助理人员
        /// </summary>
        /// <param name="pro_id"></param>
        /// <returns></returns>
        public DataSet GetDesinManagerUser(int pro_id)
        {
            string sql = @"SELECT p.pro_ID,M.mem_ID, mem_Name  FROM   dbo.tg_relation R
                                INNER JOIN tg_member M
                                 ON R.mem_ID=M.mem_ID  
                                 INNER JOIN cm_Project p 
                                 ON R.pro_ID=p.ReferenceSysNo
                                WHERE (mem_RoleIDs like N'%1%' or mem_RoleIDs like N'%6%') and p.pro_ID=" + pro_id + "";
            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 取得设总的产值
        /// </summary>
        /// <param name="pro_id"></param>
        /// <param name="allot_id"></param>
        /// <returns></returns>
        public DataSet GetDesinManagerUserValue(int pro_id, int allot_id)
        {
            string sql = " select a.*,b.mem_Name  from cm_ProjectValueByMemberDetails  a join tg_member b on a.mem_ID=b.mem_ID  where  ProID=" + pro_id + " and AllotID=" + allot_id + " and DesignType='designManager'";
            return DbHelperSQL.Query(sql);
        }
        /// <summary>
        /// 得到策划人员专业--暖通项目 包括暖通专业
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public DataSet GetPlanUserSpecialtyByHaveProject(int proID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                          select DISTINCT s.spe_Name,s.spe_ID
                         from tg_relation r join tg_member u
                         on u.mem_ID = r.mem_ID
                          join tg_speciality  s on s.spe_ID = u.mem_Speciality_ID
                          join cm_Project p
                          on p.ReferenceSysNo=r.pro_ID
                          where p.pro_ID=@proID
                          and s.spe_Name in ('建筑','结构','给排水','电气','暖通')
                           and r.mem_RoleIDs <>'1'
                           and r.mem_RoleIDs <>'5'
                           and r.mem_RoleIDs <>'6'
                          union
                           select DISTINCT s.spe_Name,s.spe_ID
                         from tg_relation r join tg_member u
                         on u.mem_ID = r.mem_ID
                          join tg_speciality  s on s.spe_ID = u.mem_Speciality_ID
                          join cm_Project p
                          on p.ReferenceSysNo=r.pro_ID
                          where p.pro_ID=@proID
                          and s.spe_Name in ('建筑','结构','给排水','电气','暖通')
                           and( r.mem_RoleIDs <>'1'
                           or r.mem_RoleIDs <>'5'
                           or r.mem_RoleIDs <>'6'
                           ) and r.mem_isDesigner=1
                         order by s.spe_ID 
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@proID", SqlDbType.Int,4)
			};

            parameters[0].Value = proID;

            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 该专业下 该审核角色是否有人员参与
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public int IsRolePlay(int roseID, string specialtyName, int cprID)
        {

            string sql = " select COUNT( r.mem_ID) as SpecialtyName from tg_relation r join tg_member u on u.mem_ID = r.mem_ID join tg_speciality  s on s.spe_ID = u.mem_Speciality_ID join cm_Project p on p.ReferenceSysNo=r.pro_ID  where p.pro_ID =" + cprID + " and  s.spe_Name= N'" + specialtyName + "' and  r.mem_RoleIDs like N'%" + roseID + "%'";

            return int.Parse(DbHelperSQL.Query(sql).Tables[0].Rows[0][0].ToString());
        }

        /// <summary>
        /// 该专业下 该审核角色是否有设计人员参与
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public int IsDesignerPlay(int cprID, string specialtyName)
        {
            string sql = " select COUNT( r.mem_ID) as SpecialtyName from tg_relation r join tg_member u on u.mem_ID = r.mem_ID join tg_speciality  s on s.spe_ID = u.mem_Speciality_ID join cm_Project p on p.ReferenceSysNo=r.pro_ID  where p.pro_ID =" + cprID + " and  s.spe_Name= N'" + specialtyName + "' and  r.mem_isDesigner =1 ";
            return int.Parse(DbHelperSQL.Query(sql).Tables[0].Rows[0][0].ToString());
        }


        /// <summary>
        /// 得打数据列表
        /// </summary>
        public DataSet GetValuedDataList(int allotId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                        SELECT ID,ALLOTID,ItemType,ProgramPercent,ProgramAmount,preliminaryPercent,preliminaryAmount,WorkDrawPercent,WorkDrawAmount,LateStagePercent,LateStageAmount,(ProgramAmount+preliminaryAmount+WorkDrawAmount+LateStageAmount) as TotalCount FROM dbo.cm_ProjectStageValueDetails WHERE ALLOTID=@ALLOTID
                        SELECT * from cm_ProjectStageSpeDetails WHERE ALLOT=@ALLOTID order by ID
                        SELECT * ,(AuditAmount+ProofreadAmount+DesignAmount+SpecialtyHeadAmount) as TotalAmount FROM cm_ProjectDesignProcessValueDetails WHERE   AllotID=@ALLOTID
                        SELECT * FROM dbo.cm_ProjectOutDoorValueDetails WHERE ALLOT=@ALLOTID  order by ID"
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@ALLOTID", SqlDbType.Int,4),
			};
            parameters[0].Value = allotId;

            return DbHelperSQL.Query(strSql.ToString(), parameters);

        }
        /// <summary>
        /// 得打数据列表
        /// </summary>
        public DataSet GetValuedTranDataList(int allotId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                        SELECT ID,ALLOTID,ItemType,ProgramPercent,ProgramAmount as ProgramCOUNT,preliminaryPercent,preliminaryAmount as preliminaryCOUNT,WorkDrawPercent ,WorkDrawAmount as WorkDrawCOUNT,LateStagePercent,LateStageAmount as LateStageCOUNT,(ProgramAmount+preliminaryAmount+WorkDrawAmount+LateStageAmount) as AllotCount FROM dbo.cm_ProjectStageValueDetails WHERE ALLOTID=@ALLOTID
                        SELECT * from cm_ProjectStageSpeDetails WHERE ALLOT=@ALLOTID order by ID
                        SELECT * ,(AuditAmount+ProofreadAmount+DesignAmount+SpecialtyHeadAmount) as TotalAmount FROM cm_ProjectDesignProcessValueDetails WHERE   AllotID=@ALLOTID
                        SELECT * FROM dbo.cm_ProjectOutDoorValueDetails WHERE ALLOT=@ALLOTID  order by ID"
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@ALLOTID", SqlDbType.Int,4),
			};
            parameters[0].Value = allotId;

            return DbHelperSQL.Query(strSql.ToString(), parameters);

        }
        /// <summary>
        /// 得到工序值
        /// </summary>
        /// <param name="allotId"></param>
        /// <returns></returns>
        public DataSet GetDesignProcessDataList(int allotId)
        {
            string sql = "SELECT * ,(AuditAmount+ProofreadAmount+DesignAmount+SpecialtyHeadAmount) as TotalAmount FROM cm_ProjectDesignProcessValueDetails WHERE   AllotID=" + allotId + "";
            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 新增产值分配明细信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertProjectAllotValueDetatil(ProjectValueAllotDetailEntity dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                TG.Model.cm_ProjectValueAuditRecord tempEntity = new TG.DAL.cm_ProjectValueAuditRecord().GetModel(dataEntity.SysNo);

                int? allotID = 0;
                if (tempEntity != null)
                {
                    allotID = tempEntity.AllotID;
                }

                //设总产值
                if (dataEntity.DesinManagerUser != null)
                {
                    dataEntity.DesinManagerUser.ForEach((stage) =>
                    {
                        command.CommandText = "insert into cm_ProjectValueByMemberDetails(ProID,AllotID,mem_ID,DesignManagerPercent,DesignManagerCount,DesignType,Status)values(" + dataEntity.ProNo + "," + allotID + ", " + stage.mem_ID + "," + stage.DesignManagerPercent + "," + stage.DesignManagerCount + ",'" + stage.DesignType + "','A')";
                        command.ExecuteNonQuery();
                    });
                }
                //新规项目各设计阶段产值分配
                dataEntity.Stage.ForEach((stage) =>
                {
                    command.CommandText = "insert into cm_ProjectStageValueDetails(ALLOTID,[ItemType],[ProgramAmount],[preliminaryAmount],[WorkDrawAmount],[LateStageAmount],ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent)values(" + allotID + ",N' " + stage.ItemType + "'," + stage.ProgramAmount + "," + stage.preliminaryAmount + "," + stage.WorkDrawAmount + "," + stage.LateStageAmount + "," + stage.ProgramPercent + "," + stage.preliminaryPercent + "," + stage.WorkDrawPercent + "," + stage.LateStagePercent + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段专业之间产值分配

                dataEntity.StageSpe.ForEach((stageSpe) =>
                {
                    command.CommandText = "insert into cm_ProjectStageSpeDetails(ALLOT,Specialty,ProgramCount,preliminaryCount,WorkDrawCount,LateStageCount,ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent)values(" + allotID + ",N' " + stageSpe.Specialty + "'," + stageSpe.ProgramCount + "," + stageSpe.preliminaryCount + "," + stageSpe.WorkDrawCount + "," + stageSpe.LateStageCount + "," + stageSpe.ProgramPercent + "," + stageSpe.preliminaryPercent + "," + stageSpe.WorkDrawPercent + "," + stageSpe.LateStagePercent + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段专业之间产值分配
                dataEntity.DesignProcess.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + allotID + "," + 0 + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段专业之间产值分配
                dataEntity.DesignProcessTwo.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + allotID + "," + 1 + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段专业之间产值分配
                dataEntity.DesignProcessThree.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + allotID + "," + 2 + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                dataEntity.DesignProcessFour.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + allotID + "," + 3 + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                dataEntity.DesignProcessFive.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + allotID + "," + dataEntity.type + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                //室外工程
                dataEntity.OutDoor.ForEach((stageSpe) =>
                {
                    command.CommandText = "insert into cm_ProjectOutDoorValueDetails(ALLOT,Status,Specialty,SpecialtyPercent,SpecialtyCount)values(" + allotID + "," + stageSpe.Status + ",'" + stageSpe.Specialty + "'," + stageSpe.SpecialtyPercent + "," + stageSpe.SpecialtyCount + ")";
                    command.ExecuteNonQuery();
                });

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        /// 新增产值分配人员明细信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertProjectAllotValueByMemberDetatil(ProjectValueAllotByMemberEntity dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                TG.Model.cm_ProjectValueAuditRecord tempEntity = new TG.DAL.cm_ProjectValueAuditRecord().GetModel(dataEntity.SysNo);

                int? allotID = 0;
                string secondValue = "";
                if (tempEntity != null)
                {
                    allotID = tempEntity.AllotID;
                    secondValue = tempEntity.SecondValue == "" ? "1" : tempEntity.SecondValue;
                }

                decimal designSecondAmount = 0;
                //新规项目各设计阶段产值分配
                dataEntity.ValueByMember.ForEach((stage) =>
                {
                    decimal programPercent = 0;
                    if (!string.IsNullOrEmpty(stage.DesignPercent))
                    {
                        programPercent = decimal.Parse(stage.DesignPercent);
                    }
                    decimal preliminaryPercent = 0;
                    if (!string.IsNullOrEmpty(stage.SpecialtyHeadPercent))
                    {
                        preliminaryPercent = decimal.Parse(stage.SpecialtyHeadPercent);
                    }

                    decimal WorkDrawPercent = 0;
                    if (!string.IsNullOrEmpty(stage.AuditPercent))
                    {
                        WorkDrawPercent = decimal.Parse(stage.AuditPercent);
                    }

                    decimal LateStagePercent = 0;
                    if (!string.IsNullOrEmpty(stage.ProofreadPercent))
                    {
                        LateStagePercent = decimal.Parse(stage.ProofreadPercent);
                    }
                    decimal designCount = 0;
                    if (stage.ItemType == 2)
                    {
                        if (stage.SpeName == "建筑" || stage.SpeName == "结构")
                        {
                            if (stage.IsHead.Equals("1") || stage.IsHead.Equals("2") || stage.IsHead.Equals("3"))
                            {
                                designCount = stage.DesignCount * decimal.Parse("0.5");
                                designSecondAmount = designSecondAmount + designCount;
                            }
                            else
                            {
                                designCount = stage.DesignCount;
                            }
                        }
                        else
                        {
                            designCount = stage.DesignCount;
                        }
                    }
                    else
                    {
                        designCount = stage.DesignCount;
                    }

                    command.CommandText = "insert into cm_ProjectValueByMemberDetails(ProID,AllotID,ItemType,mem_ID,AuditUser,DesignPercent,DesignCount,SpecialtyHeadPercent,SpecialtyHeadCount,AuditPercent,AuditCount,ProofreadPercent,ProofreadCount,SecondValue,Status,IsExternal)values(" + dataEntity.ProNo + "," + allotID + "," + stage.ItemType + "," + stage.mem_ID + "," + dataEntity.AuditUser + "," + programPercent + "," + designCount + "," + preliminaryPercent + "," + stage.SpecialtyHeadCount + "," + WorkDrawPercent + "," + stage.AuditCount + "," + LateStagePercent + "," + stage.ProofreadCount + "," + secondValue + ",'" + dataEntity.Status + "','" + stage.IsExternal + "')";
                    command.ExecuteNonQuery();
                });

                TG.DAL.cm_ProjectSecondValueAllot dal = new cm_ProjectSecondValueAllot();
                //查询出
                TG.Model.cm_ProjectSecondValueAllot secondModel = dal.GetModel(tempEntity.ProSysNo, allotID);
                decimal tempDesignAmount = secondModel.DesignCount == null ? 0 : decimal.Parse(secondModel.DesignCount.ToString());

                secondModel.DesignCount = tempDesignAmount + designSecondAmount;

                command.CommandText = "update cm_ProjectSecondValueAllot set DesignCount=" + secondModel.DesignCount + " where ID=" + secondModel.ID + "";
                command.ExecuteNonQuery();

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        /// 新增经济所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertJjsProjectAllotValueByMemberDetail(jjsProjectValueAllotByMemberEntity dataEntity, string isTran)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //新增配置表信息
                command.CommandText = @"INSERT INTO cm_jjsProjectValueAllotDetail ([proID]
                                       ,[typeStatus]
                                       ,[ProofreadPercent]
                                       ,[ProofreadCount]
                                       ,[BuildingPercent]
                                       ,[BuildingCount]
                                       ,[StructurePercent]
                                       ,[StructureCount]
                                       ,[DrainPercent]
                                       ,[DrainCount]
                                       ,[HavcPercent]
                                       ,[HavcCount]
                                       ,[ElectricPercent]
                                       ,[ElectricCount],TotalBuildingPercent,TotalBuildingCount,TotalInstallationPercent,TotalInstallationCount,AllotID)
                                        VALUES
                                  (" + dataEntity.ProNo + "," + dataEntity.typeStatus + "," + dataEntity.ProofreadPercent + "," + dataEntity.ProofreadCount + "," + dataEntity.BuildingPercent + "," + dataEntity.BuildingCount + "," + dataEntity.StructurePercent + "," + dataEntity.StructureCount + "," + dataEntity.DrainPercent + "," + dataEntity.DrainCount + "," + dataEntity.HavcPercent + "," + dataEntity.HavcCount + "," + dataEntity.ElectricPercent + "," + dataEntity.ElectricCount + "," + dataEntity.TotalBuildingPercent + "," + dataEntity.TotalBuildingCount + "," + dataEntity.TotalInstallationPercent + "," + dataEntity.TotalInstallationCount + "," + dataEntity.AllotID + ")";
                command.ExecuteNonQuery();

                if (isTran.Equals("1"))
                {
                    //新规项目各设计阶段产值分配
                    dataEntity.ValueByMember.ForEach((stage) =>
                    {
                        decimal designPercent = 0;
                        if (!string.IsNullOrEmpty(stage.DesignPercent))
                        {
                            designPercent = decimal.Parse(stage.DesignPercent);
                        }

                        decimal prooPercent = 0;
                        if (!string.IsNullOrEmpty(stage.ProofreadPercent))
                        {
                            prooPercent = decimal.Parse(stage.ProofreadPercent);
                        }

                        command.CommandText = "insert into cm_ProjectValueByMemberDetails(ProID,AllotID,ItemType,mem_ID,AuditUser,DesignPercent,DesignCount,SpecialtyHeadPercent,SpecialtyHeadCount,AuditPercent,AuditCount,ProofreadPercent,ProofreadCount,SecondValue,Status,TranType,IsExternal)values(" + dataEntity.ProNo + "," + dataEntity.AllotID + "," + stage.ItemType + "," + stage.mem_ID + "," + dataEntity.AuditUser + "," + designPercent + "," + stage.DesignCount + "," + 0 + "," + stage.SpecialtyHeadCount + "," + 0 + "," + 0 + "," + prooPercent + "," + stage.ProofreadCount + "," + 1 + ",'A','jjs','" + stage.IsExternal + "')";
                        command.ExecuteNonQuery();

                    });

                }
                else
                {
                    //新规项目各设计阶段产值分配
                    dataEntity.ValueByMember.ForEach((stage) =>
                    {
                        decimal designPercent = 0;
                        if (!string.IsNullOrEmpty(stage.DesignPercent))
                        {
                            designPercent = decimal.Parse(stage.DesignPercent);
                        }

                        decimal prooPercent = 0;
                        if (!string.IsNullOrEmpty(stage.ProofreadPercent))
                        {
                            prooPercent = decimal.Parse(stage.ProofreadPercent);
                        }


                        command.CommandText = "insert into cm_ProjectValueByMemberDetails(ProID,AllotID,ItemType,mem_ID,AuditUser,DesignPercent,DesignCount,SpecialtyHeadPercent,SpecialtyHeadCount,AuditPercent,AuditCount,ProofreadPercent,ProofreadCount,SecondValue,Status,IsExternal)values(" + dataEntity.ProNo + "," + dataEntity.AllotID + "," + stage.ItemType + "," + stage.mem_ID + "," + dataEntity.AuditUser + "," + designPercent + "," + stage.DesignCount + "," + 0 + "," + stage.SpecialtyHeadCount + "," + 0 + "," + 0 + "," + prooPercent + "," + stage.ProofreadCount + "," + 1 + ",'A','" + stage.IsExternal + "')";
                        command.ExecuteNonQuery();

                    });

                    //设总产值
                    dataEntity.DesinManagerUser.ForEach((stage) =>
                    {
                        command.CommandText = "insert into cm_ProjectValueByMemberDetails(ProID,AllotID,mem_ID,DesignManagerPercent,DesignManagerCount,DesignType,Status)values(" + dataEntity.ProNo + "," + dataEntity.AllotID + ", " + stage.mem_ID + "," + stage.DesignManagerPercent + "," + stage.DesignManagerCount + ",'" + stage.DesignType + "','A')";
                        command.ExecuteNonQuery();
                    });
                }


                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }


        /// <summary>
        /// 更新经济所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateJjsProjectAllotValue(TG.Model.cm_ProjectValueAuditRecord dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //更新 分配表状态
                //command.CommandText = "update cm_ProjectValueAuditRecord set Status='" + dataEntity.Status + "',OneSuggestion='" + dataEntity.OneSuggestion + "',AuditUser='" + dataEntity.AuditUser + "',AuditDate='" + DateTime.Now + "' where SysNo=" + dataEntity.SysNo + " ";
                //command.ExecuteNonQuery();

                var status = "";
                if (dataEntity.Status == "F")
                {
                    status = "S";
                    //更新 分配表状态
                    command.CommandText = "update cm_ProjectValueAllot set Status='" + status + "' where pro_ID=" + dataEntity.ProSysNo + " and ID=" + dataEntity.AllotID + "";
                    command.ExecuteNonQuery();

                    //更新 分配表状态
                    command.CommandText = "update cm_jjsProjectValueAllotDetail set Status='" + status + "' where proID=" + dataEntity.ProSysNo + " and AllotID=" + dataEntity.AllotID + "";
                    command.ExecuteNonQuery();

                    //更新 分配表状态
                    command.CommandText = "update cm_ProjectValueByMemberDetails set Status='" + status + "' where proID=" + dataEntity.ProSysNo + " and AllotID=" + dataEntity.AllotID + " ";
                    command.ExecuteNonQuery();
                }
                else if (dataEntity.Status == "C" || dataEntity.Status == "E" || dataEntity.Status == "G")
                {
                    status = "D";
                    //更新 分配表状态
                    command.CommandText = "update cm_ProjectValueAllot set Status='" + status + "' where pro_ID=" + dataEntity.ProSysNo + " and ID=" + dataEntity.AllotID + "";
                    command.ExecuteNonQuery();

                    //更新 分配表状态
                    command.CommandText = "update cm_jjsProjectValueAllotDetail set Status='" + status + "' where proID=" + dataEntity.ProSysNo + " and AllotID=" + dataEntity.AllotID + "";
                    command.ExecuteNonQuery();

                    //更新 分配表状态
                    command.CommandText = "update cm_ProjectValueByMemberDetails set Status='" + status + "' where proID=" + dataEntity.ProSysNo + " and AllotID=" + dataEntity.AllotID + " ";
                    command.ExecuteNonQuery();
                }

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        /// 更新经济所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateTranJjsProjectAllotValue(TG.Model.cm_TranjjsProjectValueAllot dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //更新 分配表状态
                command.CommandText = "update cm_ProjectValueAuditRecord set Status='" + dataEntity.Status + "' where pro_ID=" + dataEntity.Pro_ID + " and AllotID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "update cm_ProjectValueAllot set Status='" + dataEntity.Status + "' where pro_ID=" + dataEntity.Pro_ID + " and ID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "update cm_jjsProjectValueAllotDetail set Status='" + dataEntity.Status + "' where proID=" + dataEntity.Pro_ID + " and AllotID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "update cm_ProjectValueByMemberDetails set Status='" + dataEntity.Status + "' where proID=" + dataEntity.Pro_ID + " and AllotID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        /// 得到专业负责人
        /// </summary>
        /// <param name="cprid"></param>
        /// <param name="userNo"></param>
        /// <returns></returns>
        public string GetSpecialtyHead(int cprid, int userNo)
        {

            string sql = @"SELECT pro_ID,M.mem_ID, mem_Name, mem_RoleIDs,cpr_Id FROM   dbo.tg_relation R
                                INNER JOIN tg_member M
                                 ON R.mem_ID=M.mem_ID
                                 INNER JOIN ( SELECT cpr_Id,P.ReferenceSysNo FROM cm_Project p LEFT JOIN   cm_Coperation  C
                                 ON P.CoperationSysNo=C.cpr_Id ) N
                                 ON R.pro_ID=N.ReferenceSysNo
                                WHERE mem_RoleIDs like '%2%'  AND  M.mem_ID=" + userNo + "";

            DataSet resultObj = TG.DBUtility.DbHelperSQL.Query(sql);
            if (resultObj.Tables[0].Rows.Count > 0)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }


        /// <summary>
        /// 查询产值分配信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_ProjectValuelNoDataSetByPager(int startIndex, int endIndex, string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", SqlDbType.Int,4),
					new SqlParameter("@endIndex", SqlDbType.Int,4),
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = strWhere;
            return DbHelperSQL.RunProcedure("P_cm_ProjectValuelNoList", parameters, "ds");
        }

        /// <summary>
        /// 查询产值分配信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public List<cm_projectValueAllotView> P_cm_jjsProjectValuelNoDataSetByPager(int startIndex, int endIndex, string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", SqlDbType.Int,4),
					new SqlParameter("@endIndex", SqlDbType.Int,4),
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = strWhere;

            List<cm_projectValueAllotView> resultList = new List<cm_projectValueAllotView>();

            SqlDataReader reader = DbHelperSQL.RunProcedure("P_cm_ProjectValuelNoList", parameters);

            while (reader.Read())
            {
                cm_projectValueAllotView auditListView = new cm_projectValueAllotView
                {
                    pro_Id = Convert.ToInt32(reader["pro_ID"]),
                    Pro_Name = reader["pro_name"] == null ? string.Empty : reader["pro_name"].ToString(),
                    cpr_Unit = reader["Unit"] == null ? string.Empty : reader["Unit"].ToString(),
                    cpr_Acount = reader["Cpr_Acount"] == null ? 0 : Convert.ToDecimal(reader["Cpr_Acount"]),
                    cpr_ShijiAcount = reader["cpr_ShijiAcount"] == null ? 0 : Convert.ToDecimal(reader["cpr_ShijiAcount"]),
                    cpr_Process = reader["pro_status"] == null ? string.Empty : reader["pro_status"].ToString(),
                    cpr_SignDate = reader["pro_startTime"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["pro_startTime"]),
                    PayShiCount = reader["PayShiCount"] == null ? 0 : Convert.ToDecimal(reader["PayShiCount"]),
                    Allotcount = reader["Allotcount"] == null ? 0 : Convert.ToDecimal(reader["Allotcount"]),
                    BuildType = reader["BuildType"] == null ? string.Empty : reader["BuildType"].ToString(),
                };
                resultList.Add(auditListView);
            }

            return resultList;
        }

        /// <summary>
        /// 查询产值分配信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public List<cm_projectValueAllotView> P_cm_ProjectValuelNoListByPager(int startIndex, int endIndex, string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", SqlDbType.Int,4),
					new SqlParameter("@endIndex", SqlDbType.Int,4),
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = strWhere;


            List<cm_projectValueAllotView> resultList = new List<cm_projectValueAllotView>();

            SqlDataReader reader = DbHelperSQL.RunProcedure("P_cm_ProjectValuelNoList", parameters);

            while (reader.Read())
            {
                cm_projectValueAllotView auditListView = new cm_projectValueAllotView
                {
                    pro_Id = Convert.ToInt32(reader["pro_ID"]),
                    Pro_Name = reader["pro_name"] == null ? string.Empty : reader["pro_name"].ToString(),
                    cpr_Unit = reader["Unit"] == null ? string.Empty : reader["Unit"].ToString(),
                    cpr_Acount = reader["Cpr_Acount"] == null ? 0 : Convert.ToDecimal(reader["Cpr_Acount"]),
                    cpr_ShijiAcount = reader["cpr_ShijiAcount"] == null ? 0 : Convert.ToDecimal(reader["cpr_ShijiAcount"]),
                    cpr_Process = reader["pro_status"] == null ? string.Empty : reader["pro_status"].ToString(),
                    cpr_SignDate = reader["pro_startTime"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["pro_startTime"]),
                    PayShiCount = reader["PayShiCount"] == null ? 0 : Convert.ToDecimal(reader["PayShiCount"]),
                    Allotcount = reader["Allotcount"] == null ? 0 : Convert.ToDecimal(reader["Allotcount"]),
                    BuildType = reader["BuildType"] == null ? string.Empty : reader["BuildType"].ToString(),
                };
                resultList.Add(auditListView);
            }

            return resultList;
        }


        /// <summary>
        /// 查询二次产值分配
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public List<cm_projectSecondValueAllotView> P_cm_ProjectSecondValuelListByPager(int startIndex, int endIndex, string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", SqlDbType.Int,4),
					new SqlParameter("@endIndex", SqlDbType.Int,4),
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = strWhere;


            List<cm_projectSecondValueAllotView> resultList = new List<cm_projectSecondValueAllotView>();

            SqlDataReader reader = DbHelperSQL.RunProcedure("P_cm_ProjectSecondValuelList", parameters);

            while (reader.Read())
            {
                cm_projectSecondValueAllotView auditListView = new cm_projectSecondValueAllotView
                {
                    pro_Id = Convert.ToInt32(reader["pro_ID"]),
                    Pro_Name = reader["pro_name"] == null ? string.Empty : reader["pro_name"].ToString(),
                    Unit = reader["Unit"] == null ? string.Empty : reader["Unit"].ToString(),
                    TheDeptValueCount = reader["TheDeptValueCount"] == null ? 0 : Convert.ToDecimal(reader["TheDeptValueCount"]),
                    AuditCount = reader["AuditCount"] == null ? 0 : Convert.ToDecimal(reader["AuditCount"]),
                    DesignCount = reader["DesignCount"] == null ? 0 : Convert.ToDecimal(reader["DesignCount"]),
                    HavcCount = reader["HavcCount"] == null ? 0 : Convert.ToDecimal(reader["HavcCount"]),
                    TotalCount = reader["totalCount"] == null ? 0 : Convert.ToDecimal(reader["totalCount"]),
                    AllotCount = reader["AllotCount"] == null ? 0 : Convert.ToDecimal(reader["AllotCount"]),
                    LoanCount = reader["LoanValueCount"] == null ? 0 : Convert.ToDecimal(reader["LoanValueCount"])
                };
                resultList.Add(auditListView);
            }

            return resultList;
        }

        /// <summary>
        /// 查询产值分配信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_ProjectValuelYesListByPager(int startIndex, int endIndex, string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", SqlDbType.Int,4),
					new SqlParameter("@endIndex", SqlDbType.Int,4),
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = strWhere;

            return DbHelperSQL.RunProcedure("P_cm_ProjectValuelYesList", parameters, "ds");
        }

        /// <summary>
        /// 查询产值分配信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_jjsProjectValuelYesListByPager(int startIndex, int endIndex, string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", SqlDbType.Int,4),
					new SqlParameter("@endIndex", SqlDbType.Int,4),
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = strWhere;

            return DbHelperSQL.RunProcedure("P_cm_ProjectValuelYesList", parameters, "ds");
        }

        /// <summary>
        /// 得到分页信息条数
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet P_cm_ProjectValuelNoListCount(string strWhere)
        {

            SqlParameter[] parameters = {
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = strWhere;

            return DbHelperSQL.RunProcedure("P_cm_ProjectValuelNoList_Count", parameters, "ds");
        }

        /// <summary>
        /// 经济所条件
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet P_cm_jjsProjectValuelNoListCount(string strWhere)
        {

            SqlParameter[] parameters = {
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = strWhere;

            return DbHelperSQL.RunProcedure("P_cm_ProjectValuelNoList_Count", parameters, "ds");
        }
        /// <summary>
        /// 得到分页信息条数
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet P_cm_ProjectValuelYesListCount(string strWhere)
        {

            SqlParameter[] parameters = {
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = strWhere;

            return DbHelperSQL.RunProcedure("P_cm_ProjectValuelYesList_Count", parameters, "ds");
        }

        /// <summary>
        /// 经济所
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet P_cm_jjsProjectValuelYesListCount(string strWhere)
        {

            SqlParameter[] parameters = {
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = strWhere;

            return DbHelperSQL.RunProcedure("P_cm_ProjectValuelYesList_Count", parameters, "ds");
        }
        /// <summary>
        /// 得到分页信息条数-二次分配总数
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet P_cm_ProjectSecondValuelListCount(string strWhere)
        {

            SqlParameter[] parameters = {
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = strWhere;

            return DbHelperSQL.RunProcedure("P_cm_ProjectSecondValuelList_Count", parameters, "ds");
        }

        /// <summary>
        /// 得到二次分配产值列表 -导出用
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetSecondValueExport(string strWhere, string year)
        {
            string sql = @"select p.pro_ID,pro_name,Unit,pro_startTime,P.UpdateBy,P.InsertUserID ,
						TheDeptValueCount,AuditCount,
						DesignCount,HavcCount,LoanValueCount,
						isnull((TheDeptValueCount+AuditCount+DesignCount+HavcCount+LoanValueCount-isnull(AllotCount,0)),0) as totalCount,
						isnull(AllotCount,0) as AllotCount
						from cm_project p
						join 
							(select ProID,
							isnull(SUM(TheDeptValueCount),0) as TheDeptValueCount ,
							isnull(SUM(AuditCount),0) as AuditCount,
							isnull(SUM(DesignCount),0) as DesignCount,
							isnull(SUM(HavcCount),0) as HavcCount,
                            0 as LoanValueCount
							 from cm_ProjectSecondValueAllot
							 where Status='S'
							 group by ProID)second
						 on p.pro_ID=second.ProID
						 left join
						 ( SELECT pro_ID, isnull(SUM(AllotCount),0) as AllotCount FROM dbo.cm_ProjectValueAllot  where secondvalue='2' and Status!='D' group by pro_ID
						  ) allot
						  on p.pro_ID=allot.pro_ID                        
					    inner join 
					   (select  distinct(cprID) as cpr_Id  from cm_ProjectCharge where  (  (" + @year + " is null)  OR (InAcountTime  between '" + @year + "'+'-1-1' and '" + @year + "'+'-12-31' ) )) CG  on p.CoperationSysNo=CG.cpr_Id  WHERE 1=1 " + strWhere + "  order by p.pro_ID desc";

            return TG.DBUtility.DbHelperSQL.Query(sql);
        }
        /// <summary>
        /// 得到审核人员所在的部门
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public string GetUserUnitName(int userID)
        {
            string sql = @"select b.unit_Name  from tg_member  a 
                          left join  tg_unit  b on a.mem_Unit_ID = b.unit_ID where a.mem_ID=" + userID + "";

            DataSet resultObj = TG.DBUtility.DbHelperSQL.Query(sql);
            if (resultObj.Tables[0].Rows.Count > 0)
            {
                return resultObj.Tables[0].Rows[0]["unit_Name"].ToString();
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 得到建筑结构的审核信息
        /// </summary>
        /// <param name="AllotID"></param>
        /// <returns></returns>
        public decimal GetProcessBySpeJzJgAuditAmount(int? AllotID)
        {
            string sql = "select  SUM(AuditAmount) as AuditAmount from cm_ProjectDesignProcessValueDetails where (Specialty='建筑' OR Specialty='结构' ) And AllotID=" + AllotID + "";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(sql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                return decimal.Parse(dt.Rows[0][0].ToString());
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 得到二次分配金额明细
        /// </summary>
        /// <param name="proID"></param>
        /// <param name="AllotID"></param>
        /// <returns></returns>
        public DataSet GetSecondAuditDesignAmount(int proID, int AllotID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                      select ID,proId,allotId,status,SecondValue, ISNULL(TheDeptValueCount,0) TheDeptValueCount ,
                        ISNULL(AuditCount,0) AuditCount ,
                        ISNULL(DesignCount,0) DesignCount ,
                        ISNULL(HavcCount,0) HavcCount 
                        from cm_ProjectSecondValueAllot where ProID =@ProID and AllotID=@AllotID
                          "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@ProID", SqlDbType.Int,4),
                    new SqlParameter("@AllotID", SqlDbType.Int,4)
			};
            parameters[0].Value = proID;
            parameters[1].Value = AllotID;
            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 得到经济所人员产值信息
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public DataSet GetJjsProjectPlanUser(int proID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                        select DISTINCT r.mem_ID,
						 u.mem_Name,r.mem_RoleIDs, 
						 r.mem_isDesigner,s.spe_Name,
						 s.spe_ID,u.mem_Principalship_ID
                         from tg_relation r join tg_member u
                         on u.mem_ID = r.mem_ID
                          join tg_speciality  s 
                          on s.spe_ID = u.mem_Speciality_ID
                          join tg_unit unit
                          on  unit.unit_ID=u.mem_Unit_ID
                          join cm_Project p
                          on p.ReferenceSysNo=r.pro_ID
                          where p.pro_ID=@ProID and
                           unit.unit_Name like '%经济%'
                           and s.spe_Name in ('预算','其他')
                          "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@ProID", SqlDbType.Int,4)
			};
            parameters[0].Value = proID;

            return DbHelperSQL.Query(strSql.ToString(), parameters);

        }

        /// <summary>
        /// 取得策划参与人员专业--经济所项目
        /// </summary>
        /// <param name="proID"></param>
        /// <returns></returns>
        public DataSet GetJjsProjectPlanUserSpe(int proID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                       select DISTINCT s.spe_Name,
						 s.spe_ID
                         from tg_relation r join tg_member u
                         on u.mem_ID = r.mem_ID
                          join tg_speciality  s on s.spe_ID = u.mem_Speciality_ID
                          join cm_Project p
                          on p.ReferenceSysNo=r.pro_ID
                          where p.pro_ID=@ProID and
                          s.spe_Name in ('建筑','结构','给排水','电气','暖通')
                          "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@ProID", SqlDbType.Int,4)
			};
            parameters[0].Value = proID;

            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 所长更新产值阶段
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateProjectAllotValueProcess(string strWhere, int proid, int AllotId, decimal locaCount)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //更新 分配表状态
                command.CommandText = strWhere;
                command.ExecuteNonQuery();


                TG.DAL.cm_ProjectSecondValueAllot secondBll = new DAL.cm_ProjectSecondValueAllot();


                TG.Model.cm_ProjectSecondValueAllot secondModel = secondBll.GetModel(proid, AllotId);

                if (secondModel != null)
                {
                    //更新 分配表状态
                    command.CommandText = "update cm_ProjectSecondValueAllot set LoanValueCount=" + locaCount + " where ID=" + secondModel.ID + "";
                    command.ExecuteNonQuery();
                }

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }


        /// <summary>
        /// 所有审批人员
        /// </summary>
        /// <param name="pro_id"></param>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetAuditMemberList(int pro_id, int? allotID, string where)
        {
            string sql = @"select  distinct mem_ID  from cm_ProjectValueByMemberDetails where AllotID=" + allotID + " and IsExternal='0' and TranType is null " + where;

            return DbHelperSQL.Query(sql);
        }
        /// <summary>
        /// 所有审批人员-转
        /// </summary>
        /// <param name="pro_id"></param>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetAuditTranMemberList(int pro_id, int? allotID, string where)
        {
            string sql = @"select  distinct mem_ID  from cm_ProjectValueByMemberDetails where AllotID=" + allotID + " and IsExternal='0' " + where;

            return DbHelperSQL.Query(sql);
        }
        /// <summary>
        /// 取得工序分配明细
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetProjectDesignProcessValueDetails(int allotID)
        {
            string sql = @" SELECT *,(AuditAmount+SpecialtyHeadAmount+ProofreadAmount+DesignAmount)as TotalCount
                            FROM dbo.cm_ProjectDesignProcessValueDetails 
                            WHERE AllotID=" + allotID + "";
            return DbHelperSQL.Query(sql);
        }
        #endregion  Method


        /// <summary>
        /// 更新产值分配人员明细信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateProjectAllotValueByMemberDetatil(ProjectValueAllotByMemberEntity dataEntity, int allotID, int speId)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //取得之前本专业人员信息
                DataTable dt = GetIsAuditedUserAcount(allotID, speId).Tables[0];

                decimal olddesignSecondAmount = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["ItemType"].ToString() == "2")
                    {
                        if (dt.Rows[i]["spe_Name"].ToString() == "建筑" || dt.Rows[i]["spe_Name"].ToString() == "结构")
                        {
                            if (dt.Rows[i]["mem_Principalship_ID"].ToString().Equals("1") || dt.Rows[i]["mem_Principalship_ID"].ToString().Equals("2") || dt.Rows[i]["mem_Principalship_ID"].ToString().Equals("3"))
                            {
                                decimal designCount = decimal.Parse(dt.Rows[i]["DesignCount"].ToString());
                                olddesignSecondAmount = olddesignSecondAmount + designCount;
                            }
                        }
                    }
                    //删除之前的数据
                    command.CommandText = "delete from cm_ProjectValueByMemberDetails where proid=" + dataEntity.ProNo + " and allotID=" + allotID + " and mem_id=" + dt.Rows[i]["mem_ID"].ToString() + " and IsExternal=" + dt.Rows[i]["IsExternal"].ToString() + "";
                    command.ExecuteNonQuery();
                }

                TG.Model.cm_ProjectValueAuditRecord tempEntity = new TG.DAL.cm_ProjectValueAuditRecord().GetModel(dataEntity.SysNo);


                string secondValue = "";
                if (tempEntity != null)
                {
                    secondValue = tempEntity.SecondValue == "" ? "1" : tempEntity.SecondValue;
                }

                decimal designSecondAmount = 0;
                //新规项目各设计阶段产值分配
                dataEntity.ValueByMember.ForEach((stage) =>
                {
                    decimal programPercent = 0;
                    if (!string.IsNullOrEmpty(stage.DesignPercent))
                    {
                        programPercent = decimal.Parse(stage.DesignPercent);
                    }
                    decimal preliminaryPercent = 0;
                    if (!string.IsNullOrEmpty(stage.SpecialtyHeadPercent))
                    {
                        preliminaryPercent = decimal.Parse(stage.SpecialtyHeadPercent);
                    }

                    decimal WorkDrawPercent = 0;
                    if (!string.IsNullOrEmpty(stage.AuditPercent))
                    {
                        WorkDrawPercent = decimal.Parse(stage.AuditPercent);
                    }

                    decimal LateStagePercent = 0;
                    if (!string.IsNullOrEmpty(stage.ProofreadPercent))
                    {
                        LateStagePercent = decimal.Parse(stage.ProofreadPercent);
                    }
                    decimal designCount = 0;
                    if (stage.ItemType == 2)
                    {
                        if (stage.SpeName == "建筑" || stage.SpeName == "结构")
                        {
                            if (stage.IsHead.Equals("1") || stage.IsHead.Equals("2") || stage.IsHead.Equals("3"))
                            {
                                designCount = stage.DesignCount * decimal.Parse("0.5");
                                designSecondAmount = designSecondAmount + designCount;
                            }
                            else
                            {
                                designCount = stage.DesignCount;
                            }
                        }
                        else
                        {
                            designCount = stage.DesignCount;
                        }
                    }
                    else
                    {
                        designCount = stage.DesignCount;
                    }

                    command.CommandText = "insert into cm_ProjectValueByMemberDetails(ProID,AllotID,ItemType,mem_ID,AuditUser,DesignPercent,DesignCount,SpecialtyHeadPercent,SpecialtyHeadCount,AuditPercent,AuditCount,ProofreadPercent,ProofreadCount,SecondValue,Status,IsExternal)values(" + dataEntity.ProNo + "," + allotID + "," + stage.ItemType + "," + stage.mem_ID + "," + dataEntity.AuditUser + "," + programPercent + "," + designCount + "," + preliminaryPercent + "," + stage.SpecialtyHeadCount + "," + WorkDrawPercent + "," + stage.AuditCount + "," + LateStagePercent + "," + stage.ProofreadCount + "," + secondValue + ",'" + dataEntity.Status + "','" + stage.IsExternal + "')";
                    command.ExecuteNonQuery();
                });

                TG.DAL.cm_ProjectSecondValueAllot dal = new cm_ProjectSecondValueAllot();
                //查询出
                TG.Model.cm_ProjectSecondValueAllot secondModel = dal.GetModel(tempEntity.ProSysNo, allotID);
                decimal tempDesignAmount = secondModel.DesignCount == null ? 0 : decimal.Parse(secondModel.DesignCount.ToString());

                secondModel.DesignCount = tempDesignAmount + designSecondAmount - olddesignSecondAmount;

                command.CommandText = "update cm_ProjectSecondValueAllot set DesignCount=" + secondModel.DesignCount + " where ID=" + secondModel.ID + "";
                command.ExecuteNonQuery();

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        /// 更新经济所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateJjsProjectAllotValueByMemberDetail(jjsProjectValueAllotByMemberEntity dataEntity, string isTran)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                command.CommandText = "delete from cm_jjsProjectValueAllotDetail where proID=" + dataEntity.ProNo + " and AllotID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                //转经济所
                if (isTran.Equals("1"))
                {
                    //删除之前的数据
                    command.CommandText = "delete from cm_ProjectValueByMemberDetails where proid=" + dataEntity.ProNo + " and allotID=" + dataEntity.AllotID + " and TranType='jjs'";
                    command.ExecuteNonQuery();
                }
                else
                {
                    //删除之前的数据
                    command.CommandText = "delete from cm_ProjectValueByMemberDetails where proid=" + dataEntity.ProNo + " and allotID=" + dataEntity.AllotID + " and TranType is null";
                    command.ExecuteNonQuery();
                }


                //新增配置表信息
                command.CommandText = @"INSERT INTO cm_jjsProjectValueAllotDetail ([proID]
                                       ,[typeStatus]
                                       ,[ProofreadPercent]
                                       ,[ProofreadCount]
                                       ,[BuildingPercent]
                                       ,[BuildingCount]
                                       ,[StructurePercent]
                                       ,[StructureCount]
                                       ,[DrainPercent]
                                       ,[DrainCount]
                                       ,[HavcPercent]
                                       ,[HavcCount]
                                       ,[ElectricPercent]
                                       ,[ElectricCount],TotalBuildingPercent,TotalBuildingCount,TotalInstallationPercent,TotalInstallationCount,AllotID)
                                        VALUES
                                  (" + dataEntity.ProNo + "," + dataEntity.typeStatus + "," + dataEntity.ProofreadPercent + "," + dataEntity.ProofreadCount + "," + dataEntity.BuildingPercent + "," + dataEntity.BuildingCount + "," + dataEntity.StructurePercent + "," + dataEntity.StructureCount + "," + dataEntity.DrainPercent + "," + dataEntity.DrainCount + "," + dataEntity.HavcPercent + "," + dataEntity.HavcCount + "," + dataEntity.ElectricPercent + "," + dataEntity.ElectricCount + "," + dataEntity.TotalBuildingPercent + "," + dataEntity.TotalBuildingCount + "," + dataEntity.TotalInstallationPercent + "," + dataEntity.TotalInstallationCount + "," + dataEntity.AllotID + ")";
                command.ExecuteNonQuery();


                if (isTran.Equals("1"))
                {
                    //新规项目各设计阶段产值分配
                    dataEntity.ValueByMember.ForEach((stage) =>
                    {
                        decimal designPercent = 0;
                        if (!string.IsNullOrEmpty(stage.DesignPercent))
                        {
                            designPercent = decimal.Parse(stage.DesignPercent);
                        }

                        decimal prooPercent = 0;
                        if (!string.IsNullOrEmpty(stage.ProofreadPercent))
                        {
                            prooPercent = decimal.Parse(stage.ProofreadPercent);
                        }

                        command.CommandText = "insert into cm_ProjectValueByMemberDetails(ProID,AllotID,ItemType,mem_ID,AuditUser,DesignPercent,DesignCount,SpecialtyHeadPercent,SpecialtyHeadCount,AuditPercent,AuditCount,ProofreadPercent,ProofreadCount,SecondValue,Status,TranType,IsExternal)values(" + dataEntity.ProNo + "," + dataEntity.AllotID + "," + stage.ItemType + "," + stage.mem_ID + "," + dataEntity.AuditUser + "," + designPercent + "," + stage.DesignCount + "," + 0 + "," + stage.SpecialtyHeadCount + "," + 0 + "," + 0 + "," + prooPercent + "," + stage.ProofreadCount + "," + 1 + ",'A','jjs','" + stage.IsExternal + "')";
                        command.ExecuteNonQuery();

                    });

                }
                else
                {
                    //新规项目各设计阶段产值分配
                    dataEntity.ValueByMember.ForEach((stage) =>
                    {
                        decimal designPercent = 0;
                        if (!string.IsNullOrEmpty(stage.DesignPercent))
                        {
                            designPercent = decimal.Parse(stage.DesignPercent);
                        }

                        decimal prooPercent = 0;
                        if (!string.IsNullOrEmpty(stage.ProofreadPercent))
                        {
                            prooPercent = decimal.Parse(stage.ProofreadPercent);
                        }


                        command.CommandText = "insert into cm_ProjectValueByMemberDetails(ProID,AllotID,ItemType,mem_ID,AuditUser,DesignPercent,DesignCount,SpecialtyHeadPercent,SpecialtyHeadCount,AuditPercent,AuditCount,ProofreadPercent,ProofreadCount,SecondValue,Status,IsExternal)values(" + dataEntity.ProNo + "," + dataEntity.AllotID + "," + stage.ItemType + "," + stage.mem_ID + "," + dataEntity.AuditUser + "," + designPercent + "," + stage.DesignCount + "," + 0 + "," + stage.SpecialtyHeadCount + "," + 0 + "," + 0 + "," + prooPercent + "," + stage.ProofreadCount + "," + 1 + ",'A','" + stage.IsExternal + "')";
                        command.ExecuteNonQuery();

                    });
                }

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        ///  三所长全部通过，即结束流程.
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateThreeAllPassProcess(TG.Model.cm_ProjectValueAuditRecord model, int AllotID)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;


                //更新 分配表状态
                command.CommandText = "update cm_ProjectValueAuditRecord set TwoSuggstion='" + model.TwoSuggstion + "',TwoAuditUser=" + model.TwoAuditUser + " ,TwoAuditDate='" + DateTime.Now + "' ,TwoIsPass='1',Status='" + model.Status + "' ,HeadAuditCount=" + model.HeadAuditCount + " where SysNo=" + model.SysNo + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "update  cm_ProjectValueAllot set TStatus='S',Status='S' where ID=" + AllotID + "";
                command.ExecuteNonQuery();

                //更新二次产值分配的状态
                command.CommandText = "update cm_ProjectSecondValueAllot set Status='S'  where AllotID=" + AllotID + " and ProID=" + model.ProSysNo + "";
                command.ExecuteNonQuery();

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }


        /// <summary>
        ///  经济所全部通过，即结束流程.
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateJjsThreeAllPassProcess(TG.Model.cm_ProjectValueAuditRecord model, int AllotID)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;


                //更新 分配表状态
                command.CommandText = "update cm_ProjectValueAuditRecord set AuditUser='" + model.AuditUser + "',AuditDate='" + model.AuditDate + "' ,TwoIsPass='1',Status='" + model.Status + "'  where SysNo=" + model.SysNo + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "update  cm_ProjectValueAllot set TStatus='S',Status='S'  where ID=" + AllotID + "";
                command.ExecuteNonQuery();

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }
    }
}
