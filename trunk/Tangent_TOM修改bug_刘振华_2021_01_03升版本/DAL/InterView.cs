﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using System.Collections.Generic;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:tg_member
    /// </summary>
    public partial class InterView
    {
        public InterView()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int AddInterview(TG.Model.InterView model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.Interview_Name != null)
            {
                strSql1.Append("Interview_Name,");
                strSql2.Append("'" + model.Interview_Name + "',");
            }
            if (model.Interview_Unit != null)
            {
                strSql1.Append("Interview_Unit,");
                strSql2.Append("'" + model.Interview_Unit + "',");
            }
            if (model.Interview_Date != null)
            {
                strSql1.Append("Interview_Date,");
                strSql2.Append("'" + model.Interview_Date + "',");
            }
            if (model.Interview_results != null)
            {
                strSql1.Append("Interview_results,");
                strSql2.Append("'" + model.Interview_results + "',");
            }
            if (model.Interview_Remark != null)
            {
                strSql1.Append("Interview_Remark,");
                strSql2.Append("'" + model.Interview_Remark + "',");
            }
            strSql.Append("insert into cm_Interview(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
    
       

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_Interview t where 1=1");
            if (strWhere.Trim() != "")
            {
                strSql.Append(strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
       
        /// <summary>
        ///  数据量小的查询分页
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder sb = new StringBuilder();
//            sb.Append(@"WITH v as(
//        SELECT interview_Id,ROW_NUMBER() over(order BY interview_Id desc) AS row FROM cm_Interview
//        )
//          SELECT  * 
//        FROM    cm_Interview t
//              JOIN v ON t.interview_Id=v.interview_Id WHERE v.row>" + startIndex + " AND v.row<=" + endIndex + strWhere + "ORDER BY t.interview_Id desc");
            sb.Append(@"   WITH v as(
        SELECT interview_Id,ROW_NUMBER() over(order BY interview_Id desc) AS row FROM cm_Interview
        )
          SELECT  * ,
                detail.Cpr_Id AS detailId
        FROM    cm_Interview t
                JOIN cm_InterviewAttachInfo detail ON t.interview_Id = detail.Cpr_Id
         JOIN v ON t.interview_Id=v.interview_Id WHERE v.row>" + startIndex + " AND v.row<=" + endIndex + strWhere + "ORDER BY t.interview_Id desc");
 
           return DbHelperSQL.Query(sb.ToString());
        }
        /// <summary>
        /// 获取所有培训年份
        /// </summary>
        public List<string> GetTrainingYear()
        {
            List<string> list = new List<string>();
            string strSql = " Select Distinct year(Interview_Date) From cm_Interview order by year(Interview_Date) desc";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i][0].ToString());
                }
            }
            return list;
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateInterview(TG.Model.InterView model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("UPDATE  cm_Interview SET Interview_Name='" + model.Interview_Name + "',Interview_Unit='" + model.Interview_Unit + "',Interview_Date='" + model.Interview_Date + "',Interview_results='" + model.Interview_results + "',Interview_Remark='" + model.Interview_Remark + "' WHERE Interview_ID=" + model.Id);
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID,string filename)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_InterviewAttachInfo");
            strSql.Append(" where Cpr_Id=" + ID + " AND FileName='" + filename.Trim() + "'");
            return DbHelperSQL.Exists(strSql.ToString());
        }
        public bool ExistsTemp_No(string  ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_InterviewAttachInfo");
            strSql.Append(" where temp_No='" + ID + "' ");
            return DbHelperSQL.Exists(strSql.ToString());
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_InterviewAttachInfo ");
            strSql.Append(" where Cpr_Id=" + ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条
        /// </summary>
        /// <param name="Interviewid"></param>
        /// <returns></returns>
        public int deleteInterview(int Interviewid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("DELETE from  cm_Interview where Interview_ID=" + Interviewid + " ");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
       
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool UpdateTrainingInfoDetail(int id,List<TG.Model.Training> list)
        {

            var sql = @"delete dbo.TrainingInfoDetail WHERE TrainingInfoId=" + id+@";insert TrainingInfoDetail(TrainingInfoId,BaoMingFee,ZhuShuFee,JiPiaoFee,OtherFee,CanYuRen,UnitName,Total)
values";

            var sql1 = "";
            foreach (var model in list)
            {
                sql1 += "(" + id + "," + model.BaoMingFee + "," + model.ZhuShuFee + "," + model.JiPiaoFee + "," + model.OtherFee + ",'" + model.CanYuRen + "','" + model.UnitName + "'," + model.Total + "),";
            }
            var sql2 = sql1.Substring(0, sql1.Length - 1);
            sql += sql2;
            sql += ";select @@IDENTITY";
           int rowsAffected = DbHelperSQL.ExecuteSql(sql);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion  Method
    }
}

