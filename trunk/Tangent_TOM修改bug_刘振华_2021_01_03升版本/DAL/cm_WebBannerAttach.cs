﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_WebBannerAttach
	/// </summary>
	public partial class cm_WebBannerAttach
	{
		public cm_WebBannerAttach()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TG.Model.cm_WebBannerAttach model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.SysNo != null)
			{
				strSql1.Append("SysNo,");
				strSql2.Append(""+model.SysNo+",");
			}
			if (model.RelativePath != null)
			{
				strSql1.Append("RelativePath,");
				strSql2.Append("'"+model.RelativePath+"',");
			}
			if (model.FileName != null)
			{
				strSql1.Append("FileName,");
				strSql2.Append("'"+model.FileName+"',");
			}
			if (model.FileType != null)
			{
				strSql1.Append("FileType,");
				strSql2.Append("'"+model.FileType+"',");
			}
			if (model.FileSize != null)
			{
				strSql1.Append("FileSize,");
				strSql2.Append(""+model.FileSize+",");
			}
			if (model.UpLoadDate != null)
			{
				strSql1.Append("UpLoadDate,");
				strSql2.Append("'"+model.UpLoadDate+"',");
			}
            if (model.TempNo != null)
            {
                strSql1.Append("TempNo,");
                strSql2.Append("'" + model.TempNo + "',");
            }
			strSql.Append("insert into cm_WebBannerAttach(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_WebBannerAttach model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_WebBannerAttach set ");
			if (model.SysNo != null)
			{
				strSql.Append("SysNo="+model.SysNo+",");
			}
			if (model.RelativePath != null)
			{
				strSql.Append("RelativePath='"+model.RelativePath+"',");
			}
			if (model.FileName != null)
			{
				strSql.Append("FileName='"+model.FileName+"',");
			}
			else
			{
				strSql.Append("FileName= null ,");
			}
			if (model.FileType != null)
			{
				strSql.Append("FileType='"+model.FileType+"',");
			}
			else
			{
				strSql.Append("FileType= null ,");
			}
			if (model.FileSize != null)
			{
				strSql.Append("FileSize="+model.FileSize+",");
			}
			else
			{
				strSql.Append("FileSize= null ,");
			}
			if (model.UpLoadDate != null)
			{
				strSql.Append("UpLoadDate='"+model.UpLoadDate+"',");
			}
            if (model.TempNo != null)
            {
                strSql.Append("TempNo=" + model.TempNo + ",");
            }
            else
            {
                strSql.Append("TempNo= null ,");
            }
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int sysno)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_WebBannerAttach ");
			strSql.Append(" where Sysno="+sysno );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_WebBannerAttach GetModel()
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
            strSql.Append(" SysNo,RelativePath,FileName,FileType,FileSize,UpLoadDate,TempNo ");
			strSql.Append(" from cm_WebBannerAttach ");
			strSql.Append(" where " );
			TG.Model.cm_WebBannerAttach model=new TG.Model.cm_WebBannerAttach();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["SysNo"]!=null && ds.Tables[0].Rows[0]["SysNo"].ToString()!="")
				{
					model.SysNo=int.Parse(ds.Tables[0].Rows[0]["SysNo"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RelativePath"]!=null && ds.Tables[0].Rows[0]["RelativePath"].ToString()!="")
				{
					model.RelativePath=ds.Tables[0].Rows[0]["RelativePath"].ToString();
				}
				if(ds.Tables[0].Rows[0]["FileName"]!=null && ds.Tables[0].Rows[0]["FileName"].ToString()!="")
				{
					model.FileName=ds.Tables[0].Rows[0]["FileName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["FileType"]!=null && ds.Tables[0].Rows[0]["FileType"].ToString()!="")
				{
					model.FileType=ds.Tables[0].Rows[0]["FileType"].ToString();
				}
				if(ds.Tables[0].Rows[0]["FileSize"]!=null && ds.Tables[0].Rows[0]["FileSize"].ToString()!="")
				{
					model.FileSize=int.Parse(ds.Tables[0].Rows[0]["FileSize"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpLoadDate"]!=null && ds.Tables[0].Rows[0]["UpLoadDate"].ToString()!="")
				{
					model.UpLoadDate=DateTime.Parse(ds.Tables[0].Rows[0]["UpLoadDate"].ToString());
				}

                if (ds.Tables[0].Rows[0]["TempNo"] != null && ds.Tables[0].Rows[0]["TempNo"].ToString() != "")
                {
                    model.TempNo = int.Parse(ds.Tables[0].Rows[0]["TempNo"].ToString());
                }
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select SysNo,RelativePath,FileName,FileType,FileSize,UpLoadDate,TempNo ");
			strSql.Append(" FROM cm_WebBannerAttach ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" SysNo,RelativePath,FileName,FileType,FileSize,UpLoadDate,TempNo ");
			strSql.Append(" FROM cm_WebBannerAttach ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_WebBannerAttach ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T. desc");
			}
			strSql.Append(")AS Row, T.*  from cm_WebBannerAttach T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

