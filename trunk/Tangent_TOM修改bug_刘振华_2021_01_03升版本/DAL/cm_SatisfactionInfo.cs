﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_SatisfactionInfo
	/// </summary>
	public partial class cm_SatisfactionInfo
	{
		public cm_SatisfactionInfo()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Sat_Id", "cm_SatisfactionInfo"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Sat_Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_SatisfactionInfo");
			strSql.Append(" where Sat_Id="+Sat_Id+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_SatisfactionInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.Cst_Id != null)
			{
				strSql1.Append("Cst_Id,");
				strSql2.Append(""+model.Cst_Id+",");
			}
			if (model.Sat_No != null)
			{
				strSql1.Append("Sat_No,");
				strSql2.Append("'"+model.Sat_No+"',");
			}
			if (model.Sch_Context != null)
			{
				strSql1.Append("Sch_Context,");
				strSql2.Append("'"+model.Sch_Context+"',");
			}
			if (model.Sat_Leve != null)
			{
				strSql1.Append("Sat_Leve,");
				strSql2.Append(""+model.Sat_Leve+",");
			}
			if (model.Sch_Time != null)
			{
				strSql1.Append("Sch_Time,");
				strSql2.Append("'"+model.Sch_Time+"',");
			}
			if (model.Sch_Way != null)
			{
				strSql1.Append("Sch_Way,");
				strSql2.Append(""+model.Sch_Way+",");
			}
			if (model.Sch_Reason != null)
			{
				strSql1.Append("Sch_Reason,");
				strSql2.Append("'"+model.Sch_Reason+"',");
			}
			if (model.Sat_Best != null)
			{
				strSql1.Append("Sat_Best,");
				strSql2.Append("'"+model.Sat_Best+"',");
			}
			if (model.Sat_NotBest != null)
			{
				strSql1.Append("Sat_NotBest,");
				strSql2.Append("'"+model.Sat_NotBest+"',");
			}
			if (model.Remark != null)
			{
				strSql1.Append("Remark,");
				strSql2.Append("'"+model.Remark+"',");
			}
			if (model.UpdateBy != null)
			{
				strSql1.Append("UpdateBy,");
				strSql2.Append(""+model.UpdateBy+",");
			}
			if (model.LastUpdate != null)
			{
				strSql1.Append("LastUpdate,");
				strSql2.Append("'"+model.LastUpdate+"',");
			}
			strSql.Append("insert into cm_SatisfactionInfo(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_SatisfactionInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_SatisfactionInfo set ");
			if (model.Cst_Id != null)
			{
				strSql.Append("Cst_Id="+model.Cst_Id+",");
			}
			if (model.Sat_No != null)
			{
				strSql.Append("Sat_No='"+model.Sat_No+"',");
			}
			else
			{
				strSql.Append("Sat_No= null ,");
			}
			if (model.Sch_Context != null)
			{
				strSql.Append("Sch_Context='"+model.Sch_Context+"',");
			}
			else
			{
				strSql.Append("Sch_Context= null ,");
			}
			if (model.Sat_Leve != null)
			{
				strSql.Append("Sat_Leve="+model.Sat_Leve+",");
			}
			else
			{
				strSql.Append("Sat_Leve= null ,");
			}
			if (model.Sch_Time != null)
			{
				strSql.Append("Sch_Time='"+model.Sch_Time+"',");
			}
			else
			{
				strSql.Append("Sch_Time= null ,");
			}
			if (model.Sch_Way != null)
			{
				strSql.Append("Sch_Way="+model.Sch_Way+",");
			}
			else
			{
				strSql.Append("Sch_Way= null ,");
			}
			if (model.Sch_Reason != null)
			{
				strSql.Append("Sch_Reason='"+model.Sch_Reason+"',");
			}
			else
			{
				strSql.Append("Sch_Reason= null ,");
			}
			if (model.Sat_Best != null)
			{
				strSql.Append("Sat_Best='"+model.Sat_Best+"',");
			}
			else
			{
				strSql.Append("Sat_Best= null ,");
			}
			if (model.Sat_NotBest != null)
			{
				strSql.Append("Sat_NotBest='"+model.Sat_NotBest+"',");
			}
			else
			{
				strSql.Append("Sat_NotBest= null ,");
			}
			if (model.Remark != null)
			{
				strSql.Append("Remark='"+model.Remark+"',");
			}
			else
			{
				strSql.Append("Remark= null ,");
			}
			if (model.UpdateBy != null)
			{
				strSql.Append("UpdateBy="+model.UpdateBy+",");
			}
			else
			{
				strSql.Append("UpdateBy= null ,");
			}
			if (model.LastUpdate != null)
			{
				strSql.Append("LastUpdate='"+model.LastUpdate+"',");
			}
			else
			{
				strSql.Append("LastUpdate= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where Sat_Id="+ model.Sat_Id+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Sat_Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_SatisfactionInfo ");
			strSql.Append(" where Sat_Id="+Sat_Id+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Sat_Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_SatisfactionInfo ");
			strSql.Append(" where Sat_Id in ("+Sat_Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_SatisfactionInfo GetModel(int Sat_Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" Sat_Id,Cst_Id,Sat_No,Sch_Context,Sat_Leve,Sch_Time,Sch_Way,Sch_Reason,Sat_Best,Sat_NotBest,Remark,UpdateBy,LastUpdate ");
			strSql.Append(" from cm_SatisfactionInfo ");
			strSql.Append(" where Sat_Id="+Sat_Id+"" );
			TG.Model.cm_SatisfactionInfo model=new TG.Model.cm_SatisfactionInfo();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Sat_Id"]!=null && ds.Tables[0].Rows[0]["Sat_Id"].ToString()!="")
				{
					model.Sat_Id=int.Parse(ds.Tables[0].Rows[0]["Sat_Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Cst_Id"]!=null && ds.Tables[0].Rows[0]["Cst_Id"].ToString()!="")
				{
                    model.Cst_Id = Convert.ToDecimal(ds.Tables[0].Rows[0]["Cst_Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Sat_No"]!=null && ds.Tables[0].Rows[0]["Sat_No"].ToString()!="")
				{
					model.Sat_No=ds.Tables[0].Rows[0]["Sat_No"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Sch_Context"]!=null && ds.Tables[0].Rows[0]["Sch_Context"].ToString()!="")
				{
					model.Sch_Context=ds.Tables[0].Rows[0]["Sch_Context"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Sat_Leve"]!=null && ds.Tables[0].Rows[0]["Sat_Leve"].ToString()!="")
				{
					model.Sat_Leve=int.Parse(ds.Tables[0].Rows[0]["Sat_Leve"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Sch_Time"]!=null && ds.Tables[0].Rows[0]["Sch_Time"].ToString()!="")
				{
					model.Sch_Time=DateTime.Parse(ds.Tables[0].Rows[0]["Sch_Time"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Sch_Way"]!=null && ds.Tables[0].Rows[0]["Sch_Way"].ToString()!="")
				{
					model.Sch_Way=int.Parse(ds.Tables[0].Rows[0]["Sch_Way"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Sch_Reason"]!=null && ds.Tables[0].Rows[0]["Sch_Reason"].ToString()!="")
				{
					model.Sch_Reason=ds.Tables[0].Rows[0]["Sch_Reason"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Sat_Best"]!=null && ds.Tables[0].Rows[0]["Sat_Best"].ToString()!="")
				{
					model.Sat_Best=ds.Tables[0].Rows[0]["Sat_Best"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Sat_NotBest"]!=null && ds.Tables[0].Rows[0]["Sat_NotBest"].ToString()!="")
				{
					model.Sat_NotBest=ds.Tables[0].Rows[0]["Sat_NotBest"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Remark"]!=null && ds.Tables[0].Rows[0]["Remark"].ToString()!="")
				{
					model.Remark=ds.Tables[0].Rows[0]["Remark"].ToString();
				}
				if(ds.Tables[0].Rows[0]["UpdateBy"]!=null && ds.Tables[0].Rows[0]["UpdateBy"].ToString()!="")
				{
					model.UpdateBy=int.Parse(ds.Tables[0].Rows[0]["UpdateBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LastUpdate"]!=null && ds.Tables[0].Rows[0]["LastUpdate"].ToString()!="")
				{
					model.LastUpdate=DateTime.Parse(ds.Tables[0].Rows[0]["LastUpdate"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select Sat_Id,Cst_Id,Sat_No,Sch_Context,case Sat_Leve when 1 then N'满意'  when 2 then N'非常满意'  when 3 then N'不满意' when 4 then N'非常不满意' end as Sat_Leve,Sch_Time, case Sch_Way when 1 then N'问卷调查' when 2 then N'统计调查' when 3 then N'询问调查' when 4 then N'电话调查'  end as Sch_Way,Sch_Reason,Sat_Best,Sat_NotBest,Remark,UpdateBy,LastUpdate ");
			strSql.Append(" FROM cm_SatisfactionInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Sat_Id,Cst_Id,Sat_No,Sch_Context,Sat_Leve,Sch_Time,Sch_Way,Sch_Reason,Sat_Best,Sat_NotBest,Remark,UpdateBy,LastUpdate ");
			strSql.Append(" FROM cm_SatisfactionInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_SatisfactionInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Sat_Id desc");
			}
			strSql.Append(")AS Row, T.*  from cm_SatisfactionInfo T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

