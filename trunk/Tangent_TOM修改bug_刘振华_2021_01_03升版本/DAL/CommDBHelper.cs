﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.DBUtility;
namespace TG.DAL
{
    public partial class CommDBHelper
    {
        /// <summary>
        /// 根据查询语句返回数据集
        /// </summary>
        /// <param name="Sql"></param>
        /// <returns></returns>
        public DataSet GetList(string Sql)
        {
            return DbHelperSQL.Query(Sql.ToString());
        }
        //返回执行结果
        public int ExcuteBySql(string Sql)
        {
            return DbHelperSQL.ExecuteSql(Sql);
        }
        /// <summary>
        /// 关联合同和合同Id
        /// </summary>
        /// <param name="cpr_id"></param>
        public int ConnactAttach(int cpr_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_CoperationAttach set ");
            strSql.Append("cpr_Id=" + cpr_id + "where cpr_Id=0");

            return DbHelperSQL.ExecuteSql(strSql.ToString());
        }
        //分页数量
        public int GetDataPageListCount(string query, string tableName)
        {
            string strSql = " select count(*) from " + tableName;
            if (query != "")
            {
                strSql += " where 1=1 " + query;
            }
            return int.Parse(DbHelperSQL.GetSingle(strSql).ToString());
        }
        public int GetDataPageListCount(string strSql)
        {
            if (DbHelperSQL.GetSingle(strSql)!= null)
            {
                return int.Parse(DbHelperSQL.GetSingle(strSql).ToString());
            }
            else
            {
                return 0;
            }
        }
        //分页
        public DataSet GetDataPageList(string query, string orderBy, int startIndex, int recordCount, string tableName)
        {
            string strSql = " select * from " + tableName;
            if (query != "")
            {
                strSql += " where 1=1 " + query + " " + orderBy;
            }
            else
            {
                strSql += " " + orderBy;
            }
            return DbHelperSQL.Query(strSql, startIndex, recordCount);
        }

        public DataSet GetDataPageList(string strSql, int startIndex, int recordCount)
        {
            return DbHelperSQL.Query(strSql, startIndex, recordCount);
        }
    }
}
