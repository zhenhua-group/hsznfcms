﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_PersonAttendSet
	/// </summary>
	public partial class cm_PersonAttendSet
	{
		public cm_PersonAttendSet()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("attend_id", "cm_PersonAttendSet"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int attend_id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_PersonAttendSet");
			strSql.Append(" where attend_id="+attend_id+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public int ExistsBackID(int mem_id,int month,int year)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select top 1 attend_id from cm_PersonAttendSet");
            strSql.Append(" where mem_ID=" + mem_id + " and attend_month=" + month + " and attend_year="+year);
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj!=null)
            {
                return Convert.ToInt32(obj);
            }
            return 0;
        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_PersonAttendSet model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.mem_ID != null)
			{
				strSql1.Append("mem_ID,");
				strSql2.Append(""+model.mem_ID+",");
			}
			if (model.attend_month != null)
			{
				strSql1.Append("attend_month,");
				strSql2.Append(""+model.attend_month+",");
			}
			if (model.ToWork != null)
			{
				strSql1.Append("ToWork,");
				strSql2.Append("'"+model.ToWork+"',");
			}
			if (model.OffWork != null)
			{
				strSql1.Append("OffWork,");
				strSql2.Append("'"+model.OffWork+"',");
			}
            if (model.attend_year != null)
            {
                strSql1.Append("attend_year,");
                strSql2.Append("" + model.attend_year + ",");
            }
			strSql.Append("insert into cm_PersonAttendSet(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_PersonAttendSet model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_PersonAttendSet set ");
			if (model.mem_ID != null)
			{
				strSql.Append("mem_ID="+model.mem_ID+",");
			}
			if (model.attend_month != null)
			{
				strSql.Append("attend_month="+model.attend_month+",");
			}
			if (model.ToWork != null)
			{
				strSql.Append("ToWork='"+model.ToWork+"',");
			}
			else
			{
				strSql.Append("ToWork= null ,");
			}
			if (model.OffWork != null)
			{
				strSql.Append("OffWork='"+model.OffWork+"',");
			}
			else
			{
				strSql.Append("OffWork= null ,");
			}
            if (model.attend_year != null)
            {
                strSql.Append("attend_year=" + model.attend_year + ",");
            }
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where attend_id="+ model.attend_id+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int attend_id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_PersonAttendSet ");
			strSql.Append(" where attend_id="+attend_id+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string attend_idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_PersonAttendSet ");
			strSql.Append(" where attend_id in ("+attend_idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_PersonAttendSet GetModel(int attend_id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
            strSql.Append(" attend_id,mem_ID,attend_month,ToWork,OffWork,attend_year");
			strSql.Append(" from cm_PersonAttendSet ");
			strSql.Append(" where attend_id="+attend_id+"" );
			TG.Model.cm_PersonAttendSet model=new TG.Model.cm_PersonAttendSet();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["attend_id"]!=null && ds.Tables[0].Rows[0]["attend_id"].ToString()!="")
				{
					model.attend_id=int.Parse(ds.Tables[0].Rows[0]["attend_id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_ID"]!=null && ds.Tables[0].Rows[0]["mem_ID"].ToString()!="")
				{
					model.mem_ID=int.Parse(ds.Tables[0].Rows[0]["mem_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["attend_month"]!=null && ds.Tables[0].Rows[0]["attend_month"].ToString()!="")
				{
					model.attend_month=int.Parse(ds.Tables[0].Rows[0]["attend_month"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ToWork"]!=null && ds.Tables[0].Rows[0]["ToWork"].ToString()!="")
				{
					model.ToWork=ds.Tables[0].Rows[0]["ToWork"].ToString();
				}
				if(ds.Tables[0].Rows[0]["OffWork"]!=null && ds.Tables[0].Rows[0]["OffWork"].ToString()!="")
				{
					model.OffWork=ds.Tables[0].Rows[0]["OffWork"].ToString();
				}
                if (ds.Tables[0].Rows[0]["attend_year"] != null && ds.Tables[0].Rows[0]["attend_year"].ToString() != "")
                {
                    model.attend_year = int.Parse(ds.Tables[0].Rows[0]["attend_year"].ToString());
                }
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select attend_id,mem_ID,attend_month,ToWork,OffWork,attend_year ");
			strSql.Append(" FROM cm_PersonAttendSet ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" attend_id,mem_ID,attend_month,ToWork,OffWork,attend_year ");
			strSql.Append(" FROM cm_PersonAttendSet ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_PersonAttendSet ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.attend_id desc");
			}
			strSql.Append(")AS Row, T.*  from cm_PersonAttendSet T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

