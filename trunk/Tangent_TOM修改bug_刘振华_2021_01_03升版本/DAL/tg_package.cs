﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:tg_package
	/// </summary>
	public partial class tg_package
	{
		public tg_package()
		{}
		#region  Method


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(decimal package_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from tg_package");
			strSql.Append(" where package_ID="+package_ID+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public decimal Add(TG.Model.tg_package model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.subpro_ID != null)
			{
				strSql1.Append("subpro_ID,");
				strSql2.Append(""+model.subpro_ID+",");
			}
			if (model.class_ID != null)
			{
				strSql1.Append("class_ID,");
				strSql2.Append(""+model.class_ID+",");
			}
			if (model.user_ID != null)
			{
				strSql1.Append("user_ID,");
				strSql2.Append(""+model.user_ID+",");
			}
			if (model.filename != null)
			{
				strSql1.Append("filename,");
				strSql2.Append("'"+model.filename+"',");
			}
			if (model.package != null)
			{
				strSql1.Append("package,");
				strSql2.Append(""+model.package+",");
			}
			if (model.remember != null)
			{
				strSql1.Append("remember,");
				strSql2.Append("'"+model.remember+"',");
			}
			if (model.ver_date != null)
			{
				strSql1.Append("ver_date,");
				strSql2.Append("'"+model.ver_date+"',");
			}
			if (model.save_flag != null)
			{
				strSql1.Append("save_flag,");
				strSql2.Append(""+model.save_flag+",");
			}
			if (model.file_size != null)
			{
				strSql1.Append("file_size,");
				strSql2.Append(""+model.file_size+",");
			}
			if (model.file_state != null)
			{
				strSql1.Append("file_state,");
				strSql2.Append(""+model.file_state+",");
			}
			if (model.lastModifyUser_ID != null)
			{
				strSql1.Append("lastModifyUser_ID,");
				strSql2.Append(""+model.lastModifyUser_ID+",");
			}
			if (model.purpose_ID != null)
			{
				strSql1.Append("purpose_ID,");
				strSql2.Append(""+model.purpose_ID+",");
			}
			if (model.version != null)
			{
				strSql1.Append("version,");
				strSql2.Append("'"+model.version+"',");
			}
			strSql.Append("insert into tg_package(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToDecimal(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_package model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update tg_package set ");
			if (model.subpro_ID != null)
			{
				strSql.Append("subpro_ID="+model.subpro_ID+",");
			}
			if (model.class_ID != null)
			{
				strSql.Append("class_ID="+model.class_ID+",");
			}
			if (model.user_ID != null)
			{
				strSql.Append("user_ID="+model.user_ID+",");
			}
			if (model.filename != null)
			{
				strSql.Append("filename='"+model.filename+"',");
			}
			if (model.package != null)
			{
				strSql.Append("package="+model.package+",");
			}
			else
			{
				strSql.Append("package= null ,");
			}
			if (model.remember != null)
			{
				strSql.Append("remember='"+model.remember+"',");
			}
			else
			{
				strSql.Append("remember= null ,");
			}
			if (model.ver_date != null)
			{
				strSql.Append("ver_date='"+model.ver_date+"',");
			}
			else
			{
				strSql.Append("ver_date= null ,");
			}
			if (model.save_flag != null)
			{
				strSql.Append("save_flag="+model.save_flag+",");
			}
			else
			{
				strSql.Append("save_flag= null ,");
			}
			if (model.file_size != null)
			{
				strSql.Append("file_size="+model.file_size+",");
			}
			else
			{
				strSql.Append("file_size= null ,");
			}
			if (model.file_state != null)
			{
				strSql.Append("file_state="+model.file_state+",");
			}
			else
			{
				strSql.Append("file_state= null ,");
			}
			if (model.lastModifyUser_ID != null)
			{
				strSql.Append("lastModifyUser_ID="+model.lastModifyUser_ID+",");
			}
			else
			{
				strSql.Append("lastModifyUser_ID= null ,");
			}
			if (model.purpose_ID != null)
			{
				strSql.Append("purpose_ID="+model.purpose_ID+",");
			}
			if (model.version != null)
			{
				strSql.Append("version='"+model.version+"',");
			}
			else
			{
				strSql.Append("version= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where package_ID="+ model.package_ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(decimal package_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_package ");
			strSql.Append(" where package_ID="+package_ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string package_IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_package ");
			strSql.Append(" where package_ID in ("+package_IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_package GetModel(decimal package_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" package_ID,subpro_ID,class_ID,user_ID,filename,package,remember,ver_date,save_flag,file_size,file_state,lastModifyUser_ID,purpose_ID,version ");
			strSql.Append(" from tg_package ");
			strSql.Append(" where package_ID="+package_ID+"" );
			TG.Model.tg_package model=new TG.Model.tg_package();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["package_ID"]!=null && ds.Tables[0].Rows[0]["package_ID"].ToString()!="")
				{
					model.package_ID=decimal.Parse(ds.Tables[0].Rows[0]["package_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["subpro_ID"]!=null && ds.Tables[0].Rows[0]["subpro_ID"].ToString()!="")
				{
					model.subpro_ID=int.Parse(ds.Tables[0].Rows[0]["subpro_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["class_ID"]!=null && ds.Tables[0].Rows[0]["class_ID"].ToString()!="")
				{
					model.class_ID=int.Parse(ds.Tables[0].Rows[0]["class_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["user_ID"]!=null && ds.Tables[0].Rows[0]["user_ID"].ToString()!="")
				{
					model.user_ID=int.Parse(ds.Tables[0].Rows[0]["user_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["filename"]!=null && ds.Tables[0].Rows[0]["filename"].ToString()!="")
				{
					model.filename=ds.Tables[0].Rows[0]["filename"].ToString();
				}
				if(ds.Tables[0].Rows[0]["package"]!=null && ds.Tables[0].Rows[0]["package"].ToString()!="")
				{
					model.package=(byte[])ds.Tables[0].Rows[0]["package"];
				}
				if(ds.Tables[0].Rows[0]["remember"]!=null && ds.Tables[0].Rows[0]["remember"].ToString()!="")
				{
					model.remember=ds.Tables[0].Rows[0]["remember"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ver_date"]!=null && ds.Tables[0].Rows[0]["ver_date"].ToString()!="")
				{
					model.ver_date=DateTime.Parse(ds.Tables[0].Rows[0]["ver_date"].ToString());
				}
				if(ds.Tables[0].Rows[0]["save_flag"]!=null && ds.Tables[0].Rows[0]["save_flag"].ToString()!="")
				{
					model.save_flag=int.Parse(ds.Tables[0].Rows[0]["save_flag"].ToString());
				}
				if(ds.Tables[0].Rows[0]["file_size"]!=null && ds.Tables[0].Rows[0]["file_size"].ToString()!="")
				{
					model.file_size=int.Parse(ds.Tables[0].Rows[0]["file_size"].ToString());
				}
				if(ds.Tables[0].Rows[0]["file_state"]!=null && ds.Tables[0].Rows[0]["file_state"].ToString()!="")
				{
					model.file_state=int.Parse(ds.Tables[0].Rows[0]["file_state"].ToString());
				}
				if(ds.Tables[0].Rows[0]["lastModifyUser_ID"]!=null && ds.Tables[0].Rows[0]["lastModifyUser_ID"].ToString()!="")
				{
					model.lastModifyUser_ID=int.Parse(ds.Tables[0].Rows[0]["lastModifyUser_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["purpose_ID"]!=null && ds.Tables[0].Rows[0]["purpose_ID"].ToString()!="")
				{
					model.purpose_ID=int.Parse(ds.Tables[0].Rows[0]["purpose_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["version"]!=null && ds.Tables[0].Rows[0]["version"].ToString()!="")
				{
					model.version=ds.Tables[0].Rows[0]["version"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select package_ID,subpro_ID,class_ID,user_ID,filename,package,remember,ver_date,save_flag,file_size,file_state,lastModifyUser_ID,purpose_ID,version ");
			strSql.Append(" FROM tg_package ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" package_ID,subpro_ID,class_ID,user_ID,filename,package,remember,ver_date,save_flag,file_size,file_state,lastModifyUser_ID,purpose_ID,version ");
			strSql.Append(" FROM tg_package ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM tg_package ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.package_ID desc");
			}
			strSql.Append(")AS Row, T.*  from tg_package T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

