﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using TG.Common.EntityBuilder;
using TG.Model;
using System.IO;
using System.Data;

namespace TG.DAL
{
    public class tg_ProjectAuditDA
    {
        public TG.Model.ProjectAuditDataEntity GetProjectAuditEntity(int projectAuditSysNo)
        {
            string sql = "select SysNo,ProjectSysNo,Status,Suggestion,AuditUser,AudtiDate,InDate,InUser from cm_ProjectAudit pa where SysNo=" + projectAuditSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            ProjectAuditDataEntity resultEntity = EntityBuilder<ProjectAuditDataEntity>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }
        //
        public TG.Model.ProjectAuditEditEntity GetProjectAuditEditEntity(int projectAuditSysNo)
        {
            string sql = "select SysNo,ProjectSysNo,options,ChangeDetail,Status,Suggestion,AuditUser,AudtiDate,InDate,InUser from cm_ProjectAuditRepeat pa where SysNo=" + projectAuditSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            ProjectAuditEditEntity resultEntity = EntityBuilder<ProjectAuditEditEntity>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }
        //
        public int UpdateProjectAudit(ProjectAuditDataEntity dataEntity)
        {
            string ChildrenSql = "";

            if (!string.IsNullOrEmpty(dataEntity.Suggestion))
            {

                ChildrenSql += " Suggestion = N'" + dataEntity.Suggestion + "|',";
            }
            if (!string.IsNullOrEmpty(dataEntity.AuditUser))
            {
                ChildrenSql += " AuditUser = N'" + dataEntity.AuditUser + ",',";
            }
            if (!string.IsNullOrEmpty(dataEntity.AudtiDate))
            {
                ChildrenSql += " AudtiDate = N'" + dataEntity.AudtiDate + ",',";
            }
            if (!string.IsNullOrEmpty(dataEntity.Status))
            {
                ChildrenSql += " Status=N'" + dataEntity.Status + "',";
            }
            if (!string.IsNullOrEmpty(ChildrenSql))
            {
                string finalSql = "UPDATE cm_ProjectAudit SET " + ChildrenSql.Substring(0, ChildrenSql.Length - 1) + " WHERE SysNo=" + dataEntity.SysNo;
                return DbHelperSQL.ExecuteSql(finalSql);
            }
            else
            {
                return 0;
            }
        }
        //
        public int UpdateProjectAuditEdit(ProjectAuditEditEntity dataEntity)
        {
            string ChildrenSql = "";

            if (!string.IsNullOrEmpty(dataEntity.Suggestion))
            {

                ChildrenSql += " Suggestion = N'" + dataEntity.Suggestion + "|',";
            }
            if (!string.IsNullOrEmpty(dataEntity.AuditUser))
            {
                ChildrenSql += " AuditUser = N'" + dataEntity.AuditUser + ",',";
            }
            if (!string.IsNullOrEmpty(dataEntity.AudtiDate))
            {
                ChildrenSql += " AudtiDate = N'" + dataEntity.AudtiDate + ",',";
            }
            if (!string.IsNullOrEmpty(dataEntity.Status))
            {
                ChildrenSql += " Status=N'" + dataEntity.Status + "',";
            }
            if (!string.IsNullOrEmpty(ChildrenSql))
            {
                string finalSql = "UPDATE cm_ProjectAuditRepeat SET " + ChildrenSql.Substring(0, ChildrenSql.Length - 1) + " WHERE SysNo=" + dataEntity.SysNo;
                return DbHelperSQL.ExecuteSql(finalSql);
            }
            else
            {
                return 0;
            }
        }
        //
        public int GetProcessRoleSysNo(string auditStatus)
        {
            int position = 1;
            switch (auditStatus)
            {
                case "B":
                    position = 2;
                    break;
            }
            string sql = "select RoleSysNo from cm_ProjectAuditConfig WHERE Position=" + position;

            object obj = DbHelperSQL.GetSingle(sql);
            int roleSysNo = obj != null ? (int)obj : 0;
            return roleSysNo;
        }

        public int InsertProjectAudit(ProjectAuditDataEntity dataEntity)
        {
            string sql = "insert into cm_ProjectAudit (ProjectSysNo,Status,InDate,InUser) values(";

            sql += dataEntity.ProjectSysNo + ",N'A',N'" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "'," + dataEntity.InUser + ");select @@IDENTITY";

            int identitySysNo = Convert.ToInt32(DbHelperSQL.GetSingle(sql));

            return identitySysNo;
        }
        //
        public int InsertProjectAuditEdit(ProjectAuditEditEntity dataEntity)
        {
            string sql = "insert into cm_ProjectAuditRepeat (ProjectSysNo,options,ChangeDetail,Status,InDate,InUser) values(";

            sql += dataEntity.ProjectSysNo + ",'" + dataEntity.options + "','" + dataEntity.ChangeDetail + "',N'A',N'" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "'," + dataEntity.InUser + ");select @@IDENTITY";

            int identitySysNo = Convert.ToInt32(DbHelperSQL.GetSingle(sql));

            return identitySysNo;
        }
        public int InsertProjectValueAuditEdit(cm_ProjectVolltAuditEdit dataEntity)
        {
            string sql = "insert into cm_ProjectVolltAuditEdit (ProjectSysNo,options,ProjectUrlDetail,Status,InDate,InUser) values(";

            sql += dataEntity.ProjectSysNo + ",'" + dataEntity.options + "','" + dataEntity.ProjectUrlDetail + "',N'A',N'" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "'," + dataEntity.InUser + ");select @@IDENTITY";
            int identitySysNo = Convert.ToInt32(DbHelperSQL.GetSingle(sql));
            return identitySysNo;
        }
        //
        public int InsertProjectJobNumber(cm_ProjectJobNumber dataEntity)
        {
            string sql = "insert into cm_ProjectNumber values(" + dataEntity.ProjectSysNo + ",N'" + dataEntity.SubmitDate + "',N'" + dataEntity.SubmitUser + "',N'" + dataEntity.ProjectNum + "');select @@IDENTITY";
            int count = Convert.ToInt32(DbHelperSQL.GetSingle(sql));

            return count;
        }
        //获取合同编号
        public string GetCoperationNo(int cprid)
        {
            TG.DAL.cm_Coperation bll = new TG.DAL.cm_Coperation();
            TG.Model.cm_Coperation model = bll.GetModel(cprid);
            if (model != null)
            {
                return model.cpr_No ?? "";
            }
            else
            {
                return "";
            }
        }
        //获取楼上楼下
        public int[] Floors(int cprid)
        {
            int[] floos = new int[2];
            TG.DAL.cm_Coperation bll = new TG.DAL.cm_Coperation();
            TG.Model.cm_Coperation model = bll.GetModel(cprid);
            if (model != null)
            {
                if (model.Floor.IndexOf('|') > -1)
                {
                    string[] array = model.Floor.Split('|');
                    if (array[0].Trim() == "")
                    {
                        floos[0] = 0;
                    }
                    else
                    {
                        floos[0] = int.Parse(array[0]);
                    }

                    if (array[1].Trim() == "")
                    {
                        floos[1] = 0;
                    }
                    else
                    {
                        floos[1] = int.Parse(array[1]);
                    }
                }
            }
            return floos;
        }
        //创建项目
        public int CreateProject(TG.Model.cm_Project sourceProject, string xmlPath, string isoPath, string userID, string ftpPath)
        {
            SqlTransaction transaction = null;

            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand { Connection = conn };
                conn.Open();
                transaction = command.Connection.BeginTransaction();
                command.Transaction = transaction;
                int[] floors = Floors(sourceProject.CoperationSysNo);

                //插入原始数据

                TG.Model.tg_project projectImportInfo = new TG.Model.tg_project
                {
                    pro_ID = sourceProject.bs_project_Id,
                    pro_BuildUnit = sourceProject.pro_buildUnit == null ? "" : sourceProject.pro_buildUnit.Trim(),
                    pro_Order = sourceProject.pro_order == null ? "" : sourceProject.pro_order.Trim(),
                    pro_Name = sourceProject.pro_name == null ? "" : sourceProject.pro_name.Trim(),
                    pro_Kinds = " ",
                    pro_Status = " ",
                    pro_Level = sourceProject.pro_level,
                    pro_StartTime = sourceProject.pro_startTime,
                    pro_FinishTime = sourceProject.pro_finishTime,
                    pro_Serial = sourceProject.JobNum,

                    pro_DesignUnit = sourceProject.Unit == null ? "" : sourceProject.Unit.Trim(),
                    pro_Intro = sourceProject.pro_kinds,
                    pro_ArchArea = sourceProject.ProjectScale,
                    pro_ArchHeight = 0,
                    pro_DesignUnitID = GetUnitByName(sourceProject.Unit),
                    pro_StruType = sourceProject.pro_StruType == null ? "" : sourceProject.pro_StruType.Trim(),
                    //标示新版T-CD
                    pro_FloorUp = floors[0],
                    pro_FloorDown = floors[1],
                    pro_flag = 1,
                    pro_Phone = sourceProject.Phone,
                    pro_ContractNo = GetCoperationNo(sourceProject.CoperationSysNo).Trim(),
                    pro_Adress = sourceProject.BuildAddress,
                    pro_Summary = sourceProject.pro_Intro
                };

                //得到新规项目信息IdentitySysNo
                int projectSysNo = new TG.DAL.tg_project().Add(projectImportInfo);
                //向cm_project中插入新规项目信息的IdentitySysNo
                string strSql = "Update cm_project set ReferenceSysNo=" + projectSysNo + " where pro_id=" + sourceProject.bs_project_Id;
                DbHelperSQL.ExecuteSql(strSql);

                //读取XML配置文件信息
                FileStream fileStream = new FileStream(xmlPath, FileMode.Open, FileAccess.Read);
                //反序列化XML为对象
                TPMTemplateXMLEntity xmlEntity = XMLHelper.Deserialize<TPMTemplateXMLEntity>(fileStream);

                //插入SubProject数据
                xmlEntity.SubProjects.ForEach((subProject) =>
                {
                    //插入subproject
                    string insertSubProjectSql = "insert into tg_subproject (pro_Name,pro_parentID,pro_parentSubID) values(N'" + subProject.Name + "'," + projectSysNo + ",0);Select @@IDENTITY";
                    command.CommandText = insertSubProjectSql;
                    //获取subproject ID
                    int subProjectSysNo = Convert.ToInt32(command.ExecuteScalar());

                    //插入Purpose
                    subProject.Purposes.ForEach((purpose) =>
                    {
                        //获取Purpose信息
                        int purposeID = int.Parse(purpose.DescrArray[1].Replace("id=", ""));
                        string insertPurposeSql = "";
                        string purposeName = purpose.DescrArray[0];
                        //阶段可选项
                        string[] projectStatus = sourceProject.pro_status.Split(',');
                        //插入Purpose
                        //可选项目阶段目录ID，7：方案设计，8：初步设计，9：施工图设计
                        if (purposeID == 7 || purposeID == 8 || purposeID == 9)
                        {
                            //插入endsubitem
                            foreach (string status in projectStatus)
                            {
                                if (status.Trim() == purposeName.Trim())
                                {
                                    insertPurposeSql = "insert into tg_endSubItem values(" + purposeID + "," + subProjectSysNo + ")";
                                    //执行
                                    command.CommandText = insertPurposeSql;
                                    command.ExecuteNonQuery();

                                    //插入subpurpose      
                                    //新键Folder
                                    purpose.Folders.ForEach((folder) =>
                                    {
                                        insertPurposeSql = "insert into tg_subpurpose  (purposeid,parentid,subproid,name) values(" + purposeID + ",0," + subProjectSysNo + ",N'" + folder.Name + "')";
                                        //执行
                                        command.CommandText = insertPurposeSql;
                                        command.ExecuteNonQuery();
                                    });

                                }
                            }

                        }
                        else
                        {
                            insertPurposeSql = "insert into tg_endSubItem values(" + purposeID + "," + subProjectSysNo + ")";
                            //执行
                            command.CommandText = insertPurposeSql;
                            command.ExecuteNonQuery();


                            purpose.Folders.ForEach((folder) =>
                            {
                                insertPurposeSql = "insert into tg_subpurpose (purposeid,parentid,subproid,name) values(" + purposeID + ",0," + subProjectSysNo + ",N'" + folder.Name + "')";
                                //执行
                                command.CommandText = insertPurposeSql;
                                command.ExecuteNonQuery();
                            });

                        }


                        //插入工程表单节点
                        if (purposeID == 4)
                        {
                            string subproid = subProjectSysNo.ToString();
                            string classid = "0";
                            string parentid = "0";
                            string userid = userID;
                            string purposeid = purposeID.ToString();

                            //原表单路径
                            string srciosPath = isoPath;
                            string ftpProjectPath = ftpPath + sourceProject.pro_name + "\\" + subproid + "\\" + purposeName;
                            GetDirectoryInfoAndFiles(srciosPath, ftpProjectPath, command, sourceProject, parentid, subproid, classid, purposeid, userid);
                        }
                    });
                });

                //提交Transaction
                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }
        public int UpdateProject(TG.Model.cm_Project sourceProject, string xmlPath, string isoPath, string userID, string ftpPath, bool isFixed)
        {
            //声明事务
            SqlTransaction transaction = null;
            //创建连接
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand { Connection = conn };
                conn.Open();
                transaction = command.Connection.BeginTransaction();
                command.Transaction = transaction;
                //读取XML配置文件信息
                FileStream fileStream = new FileStream(xmlPath, FileMode.Open, FileAccess.Read);
                //反序列化XML为对象
                TPMTemplateXMLEntity xmlEntity = XMLHelper.Deserialize<TPMTemplateXMLEntity>(fileStream);
                //项目ID

                string projectSysNo = sourceProject.bs_project_Id.ToString();
                //插入SubProject数据
                xmlEntity.SubProjects.ForEach((subProject) =>
                {
                    //插入subproject
                    string insertSubProjectSql = "insert into tg_subproject (pro_Name,pro_parentID,pro_parentSubID) values(N'" + subProject.Name + "'," + projectSysNo + ",0);Select @@IDENTITY";
                    command.CommandText = insertSubProjectSql;
                    //获取subproject ID
                    int subProjectSysNo = Convert.ToInt32(command.ExecuteScalar());

                    //插入Purpose
                    subProject.Purposes.ForEach((purpose) =>
                    {
                        //获取Purpose信息
                        int purposeID = int.Parse(purpose.DescrArray[1].Replace("id=", ""));
                        string insertPurposeSql = "";
                        string selectPurposeSql = "";
                        string purposeName = purpose.DescrArray[0];
                        //阶段可选项
                        string[] projectStatus = sourceProject.pro_status.Split(',');

                        //可选项目阶段目录ID，7：方案设计，8：初步设计，9：施工图设计
                        if (purposeID == 7 || purposeID == 8 || purposeID == 9)
                        {

                            foreach (string status in projectStatus)
                            {
                                if (status.Trim() == purposeName.Trim())
                                {
                                    //插入Purpose
                                    #region  插入endsubitem

                                    //插入endsubitem
                                    //查询
                                    selectPurposeSql = "select count(*) from tg_endSubItem where purposeID=" + purposeID + " AND subproID=" + subProjectSysNo;
                                    command.CommandText = selectPurposeSql;
                                    int count = Convert.ToInt32(command.ExecuteScalar());
                                    //如果不存在子项
                                    if (count == 0)
                                    {
                                        insertPurposeSql = "insert into tg_endSubItem values(" + purposeID + "," + subProjectSysNo + ")";
                                        //执行
                                        command.CommandText = insertPurposeSql;
                                        command.ExecuteNonQuery();
                                    }
                                    #endregion

                                    //插入subpurpose
                                    #region 新键Folder

                                    //新键Folder
                                    purpose.Folders.ForEach((folder) =>
                                    {
                                        //查询
                                        selectPurposeSql = "select count(*) from tg_subpurpose where purposeID=" + purposeID + " AND parentid=0 AND subproid=" + subProjectSysNo + " AND name='" + folder.Name + "'";
                                        command.CommandText = selectPurposeSql;
                                        int count2 = Convert.ToInt32(command.ExecuteScalar());
                                        //如果不存在子项
                                        if (count2 == 0)
                                        {
                                            //执行
                                            insertPurposeSql = "insert into tg_subpurpose  (purposeid,parentid,subproid,name) values(" + purposeID + ",0," + subProjectSysNo + ",N'" + folder.Name + "')";
                                            command.CommandText = selectPurposeSql;
                                            command.ExecuteNonQuery();
                                        }
                                    });
                                    #endregion
                                }
                            }
                        }

                        //有修改
                    });
                });

                //提交Transaction
                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }
        //修复项目
        public int CreateProject(TG.Model.cm_Project sourceProject, string xmlPath, string isoPath, string userID, string ftpPath, bool isFixed)
        {
            //声明事务
            SqlTransaction transaction = null;
            //创建连接
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand { Connection = conn };
                conn.Open();
                transaction = command.Connection.BeginTransaction();
                command.Transaction = transaction;
                int[] floors = Floors(sourceProject.CoperationSysNo);

                //更新TCD实体
                TG.Model.tg_project projectImportInfo = new TG.Model.tg_project
                {
                    pro_ID = sourceProject.ReferenceSysNo,
                    pro_BuildUnit = sourceProject.pro_buildUnit == null ? "" : sourceProject.pro_buildUnit.Trim(),
                    pro_Order = sourceProject.pro_order == null ? "" : sourceProject.pro_order.Trim(),
                    pro_Name = sourceProject.pro_name == null ? "" : sourceProject.pro_name.Trim(),
                    pro_Kinds = " ",
                    pro_Status = " ",
                    pro_Level = sourceProject.pro_level,
                    pro_StartTime = sourceProject.pro_startTime,
                    pro_FinishTime = sourceProject.pro_finishTime,
                    pro_Serial = sourceProject.JobNum,

                    pro_DesignUnit = sourceProject.Unit == null ? "" : sourceProject.Unit.Trim(),
                    pro_Intro = sourceProject.pro_kinds,
                    pro_ArchArea = sourceProject.ProjectScale,
                    pro_ArchHeight = 0,
                    pro_DesignUnitID = GetUnitByName(sourceProject.Unit),
                    pro_StruType = sourceProject.pro_StruType == null ? "" : sourceProject.pro_StruType.Trim(),
                    //标示新版T-CD
                    pro_FloorUp = floors[0],
                    pro_FloorDown = floors[1],
                    pro_flag = 1,
                    pro_Phone = sourceProject.Phone,
                    pro_ContractNo = GetCoperationNo(sourceProject.CoperationSysNo).Trim(),
                    pro_Adress = sourceProject.BuildAddress,
                    pro_Summary = sourceProject.pro_Intro
                };

                //更新tg_project
                bool IsUpdate = new TG.DAL.tg_project().Update(projectImportInfo);
                //项目ID
                string projectSysNo = projectImportInfo.pro_ID.ToString();

                if (IsUpdate)
                {
                    //读取XML配置文件信息
                    FileStream fileStream = new FileStream(xmlPath, FileMode.Open, FileAccess.Read);
                    //反序列化XML为对象
                    TPMTemplateXMLEntity xmlEntity = XMLHelper.Deserialize<TPMTemplateXMLEntity>(fileStream);
                    //插入SubProject数据
                    xmlEntity.SubProjects.ForEach((subProject) =>
                    {
                        //插入subproject
                        string insertSubProjectSql = "insert into tg_subproject (pro_Name,pro_parentID,pro_parentSubID) values(N'" + subProject.Name + "'," + projectSysNo + ",0);Select @@IDENTITY";
                        command.CommandText = insertSubProjectSql;
                        //获取subproject ID
                        int subProjectSysNo = Convert.ToInt32(command.ExecuteScalar());

                        //插入Purpose
                        subProject.Purposes.ForEach((purpose) =>
                        {
                            //获取Purpose信息
                            int purposeID = int.Parse(purpose.DescrArray[1].Replace("id=", ""));
                            string insertPurposeSql = "";
                            string selectPurposeSql = "";
                            string purposeName = purpose.DescrArray[0];
                            //阶段可选项
                            string[] projectStatus = sourceProject.pro_status.Split(',');

                            //可选项目阶段目录ID，7：方案设计，8：初步设计，9：施工图设计
                            if (purposeID == 7 || purposeID == 8 || purposeID == 9)
                            {

                                foreach (string status in projectStatus)
                                {
                                    if (status.Trim() == purposeName.Trim())
                                    {
                                        //插入Purpose
                                        #region  插入endsubitem

                                        //插入endsubitem
                                        //查询
                                        selectPurposeSql = "select count(*) from tg_endSubItem where purposeID=" + purposeID + " AND subproID=" + subProjectSysNo;
                                        command.CommandText = selectPurposeSql;
                                        int count = Convert.ToInt32(command.ExecuteScalar());
                                        //如果不存在子项
                                        if (count == 0)
                                        {
                                            insertPurposeSql = "insert into tg_endSubItem values(" + purposeID + "," + subProjectSysNo + ")";
                                            //执行
                                            command.CommandText = insertPurposeSql;
                                            command.ExecuteNonQuery();
                                        }
                                        #endregion

                                        //插入subpurpose
                                        #region 新键Folder

                                        //新键Folder
                                        purpose.Folders.ForEach((folder) =>
                                        {
                                            //查询
                                            selectPurposeSql = "select count(*) from tg_subpurpose where purposeID=" + purposeID + " AND parentid=0 AND subproid=" + subProjectSysNo + " AND name='" + folder.Name + "'";
                                            command.CommandText = selectPurposeSql;
                                            int count2 = Convert.ToInt32(command.ExecuteScalar());
                                            //如果不存在子项
                                            if (count2 == 0)
                                            {
                                                //执行
                                                insertPurposeSql = "insert into tg_subpurpose  (purposeid,parentid,subproid,name) values(" + purposeID + ",0," + subProjectSysNo + ",N'" + folder.Name + "')";
                                                command.CommandText = selectPurposeSql;
                                                command.ExecuteNonQuery();
                                            }
                                        });
                                        #endregion
                                    }
                                }
                            }
                            else
                            {
                                //插入tg_endSubItem
                                selectPurposeSql = "select count(*) from tg_endSubItem where purposeID=" + purposeID + " AND subproID=" + subProjectSysNo;
                                command.CommandText = selectPurposeSql;
                                int count = Convert.ToInt32(command.ExecuteScalar());
                                //如果不存在子项
                                if (count == 0)
                                {
                                    insertPurposeSql = "insert into tg_endSubItem values(" + purposeID + "," + subProjectSysNo + ")";
                                    //执行
                                    command.CommandText = insertPurposeSql;
                                    command.ExecuteNonQuery();
                                }

                                //插入subpurpose
                                purpose.Folders.ForEach((folder) =>
                                {
                                    //查询
                                    selectPurposeSql = "select count(*) from tg_subpurpose where purposeID=" + purposeID + " AND parentid=0 AND subproid=" + subProjectSysNo + " AND name='" + folder.Name + "'";
                                    command.CommandText = selectPurposeSql;
                                    int count2 = Convert.ToInt32(command.ExecuteScalar());
                                    //如果不存在子项
                                    if (count2 == 0)
                                    {
                                        //执行
                                        insertPurposeSql = "insert into tg_subpurpose (purposeid,parentid,subproid,name) values(" + purposeID + ",0," + subProjectSysNo + ",N'" + folder.Name + "')";
                                        command.CommandText = insertPurposeSql;
                                        command.ExecuteNonQuery();
                                    }
                                });

                            }



                            //插入工程表单节点
                            if (purposeID == 4)
                            {
                                string subproid = subProjectSysNo.ToString();
                                string classid = "0";
                                string parentid = "0";
                                string userid = userID;
                                string purposeid = purposeID.ToString();

                                //原表单路径
                                string srciosPath = isoPath;
                                string ftpProjectPath = ftpPath + sourceProject.pro_name + "\\" + subproid + "\\" + purposeName;
                                GetDirectoryInfoAndFiles(srciosPath, ftpProjectPath, command, sourceProject, parentid, subproid, classid, purposeid, userid);
                            }
                        });
                    });
                }
                //提交Transaction
                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }
        //创建文件夹
        public void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
        //移动文件
        public void CopyFile(string source, string target, string targetfile)
        {
            if (Directory.Exists(target))
            {
                System.IO.File.Copy(source, targetfile, true);
            }
        }
        //修改文件夹名字、
        public int UpdateProjectName(string projectNameB, string projectNameA, string ftpPath)
        {
            //创建新建文件夹
            string newftpPath = ftpPath + projectNameA;

            ftpPath = ftpPath + projectNameB;
            if (newftpPath != ftpPath)
            {
                MoveDir(ftpPath, newftpPath);
            }
            return 0;
        }
        public static void CopyDir(string fromDir, string toDir)
        {
            if (!Directory.Exists(fromDir))
                return;

            if (!Directory.Exists(toDir))
            {
                Directory.CreateDirectory(toDir);
            }

            string[] files = Directory.GetFiles(fromDir);
            foreach (string formFileName in files)
            {
                string fileName = Path.GetFileName(formFileName);
                string toFileName = Path.Combine(toDir, fileName);
                System.IO.File.Copy(formFileName, toFileName);

            }
            string[] fromDirs = Directory.GetDirectories(fromDir);
            foreach (string fromDirName in fromDirs)
            {
                string dirName = Path.GetFileName(fromDirName);
                string toDirName = Path.Combine(toDir, dirName);
                CopyDir(fromDirName, toDirName);
            }
        }

        public static void MoveDir(string fromDir, string toDir)
        {
            if (!Directory.Exists(fromDir))
                return;

            CopyDir(fromDir, toDir);
            Directory.Delete(fromDir, true);
        }
        //迭代文件夹 插入文件信息
        public void GetDirectoryInfoAndFiles(string srciosPath, string targetPath, SqlCommand cmd, TG.Model.cm_Project srcProject, string parentID, string subProid, string classID, string purposeID, string userID)
        {
            //移动文件
            string[] filespath = Directory.GetFiles(srciosPath);
            string subproid = subProid;
            string classid = classID;
            string userid = userID;
            string parentid = parentID;

            //创建文件夹
            CreateDirectory(targetPath);

            //遍历所有表单文件
            foreach (string file in filespath)
            {
                FileInfo fileinfo = new FileInfo(file.ToString());
                string filename = fileinfo.Name;
                string verdate = fileinfo.LastWriteTime.ToString();
                string filesize = fileinfo.Length.ToString();
                //插入数据库
                string insertFile = " INSERT INTO dbo.tg_package(subpro_id, class_id, user_id, filename, ver_date, file_size, lastmodifyuser_id, purpose_id)  VALUES (" + subproid + "," + classid + "," + userid + ",'" + filename + "','" + verdate + "'," + filesize + "," + userid + "," + purposeID + ")";
                //执行
                cmd.CommandText = insertFile;
                if (cmd.ExecuteNonQuery() > 0)
                {
                    string targetFilePath = targetPath + "\\" + filename;
                    //拷贝文件
                    CopyFile(fileinfo.FullName, targetPath, targetFilePath);
                }
            }

            //移动文件夹
            string[] dirs = Directory.GetDirectories(srciosPath);
            foreach (string dir in dirs)
            {
                DirectoryInfo directory = new DirectoryInfo(dir);
                //插入文件夹
                string insertDir = "  insert into tg_subpurpose (purposeid,parentid,subproid,name) values(" + purposeID + "," + parentid + "," + subproid + ",N'" + directory.Name + "');Select @@IDENTITY";
                //执行
                cmd.CommandText = insertDir;
                string resultID = Convert.ToString(cmd.ExecuteScalar());
                //创建文件夹路径
                string targetDirPath = targetPath + directory.Name;
                //创建目录
                CreateDirectory(targetDirPath);
                //重写classid
                string subclassid = resultID;
                //重写parentid
                string subparentid = resultID;
                //迭代
                string[] subdirs = Directory.GetFileSystemEntries(directory.FullName);
                if (subdirs.Length > 0)
                {
                    GetDirectoryInfoAndFiles(directory.FullName, targetDirPath, cmd, srcProject, subparentid, subproid, subclassid, purposeID, userid);
                }
            }
        }
        /// <summary>
        /// 获取PurposeID 
        /// </summary>
        /// <param name="purname"></param>
        /// <returns></returns>
        public int GetPurposeIDByName(string purname)
        {
            string strSql = " Select TOP 1 purpose_ID From  tg_purpose Where purpose_Name Like '%" + purname + "%'";
            DataSet ds = DbHelperSQL.Query(strSql);
            int flag = -1;
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    flag = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                }
            }
            return flag;
        }
        public int GetUnitByName(string unitname)
        {
            string strSql = " Select Top 1 unit_ID From tg_unit where unit_Name='" + unitname + "' ";
            return Convert.ToInt32(DbHelperSQL.Query(strSql).Tables[0].Rows[0]["unit_ID"].ToString());
        }
        public DataSet GetDistribuionJobNumberList()
        {
            string sql = "select p.pro_name as ProjectName,pn.SubmitDate,u.mem_Name as SubmitUserName ,p.pro_ID as ProjectSysNo,pn.ProNumber from cm_Project p join cm_ProjectAudit pa on p.pro_ID = pa.ProjectSysNo left join cm_ProjectNumber pn on pn.Pro_id =p.pro_ID left join tg_member u on u.mem_ID = pn.SubmintPerson where pa.Status = N'D'";

            return DbHelperSQL.Query(sql);
        }

        //通过评审的项目
        public DataSet GetList()
        {
            string dsSql = "select * from cm_ProjectAudit pa inner join cm_project p on p.Pro_id=pa.ProjectSysNo where status='D' order by SysNo DESC";
            return DbHelperSQL.Query(dsSql);
        }
        //通过评审的项目
        public DataSet GetList(string where)
        {
            string dsSql = "select * from cm_ProjectAudit pa inner join cm_project p on p.Pro_id=pa.ProjectSysNo where status='D' " + where + " order by SysNo DESC";
            return DbHelperSQL.Query(dsSql);
        }
        //通过评审的项目
        public DataSet GetList(int Top, string strWhere, string fieldOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" * from cm_ProjectAudit PA inner join cm_project PJ on PJ.Pro_id=PA.ProjectSysNo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + fieldOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }
        //通过评审的项目数
        public int ProjectCount()
        {
            string strSql = "select count(*) from cm_ProjectAudit where status='D'";
            return int.Parse(DbHelperSQL.GetSingle(strSql).ToString());
        }
        /// <summary>
        /// 判断是否提交审核
        /// </summary>
        /// <param name="projectNo"></param>
        /// <returns></returns>
        public int CheckAudit(int projectNo)
        {
            string strSql = "select count(*) from cm_ProjectAudit where status NOT IN ('C','E')  AND  projectSysNo=" + projectNo;
            return int.Parse(DbHelperSQL.GetSingle(strSql).ToString());
        }
        public string[] GetUserNameArray(string[] userSysNoArray)
        {
            List<string> list = new List<string>();

            if (userSysNoArray.Length > 0)
            {
                foreach (string userSysNo in userSysNoArray)
                {
                    list.Add(DbHelperSQL.GetSingle("select mem_Name from dbo.tg_member where mem_ID =" + userSysNo + "").ToString());
                }
            }
            return list.ToArray();
        }
        public string GetNextmember(string states, string auditno, string type)
        {
            string memNameList = "";
            string sqlNextmem = @"SELECT mem_Name FROM tg_member WHERE mem_ID IN (SELECT FromUser FROM cm_SysMsg WHERE ReferenceSysNo ='projectAuditSysNo=" + auditno + "&MessageStatus=" + states + "'AND MsgType=" + type + ")";
            DataSet ds = DbHelperSQL.Query(sqlNextmem);
            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        memNameList += dt.Rows[i]["mem_Name"].ToString() + "&";
                    }
                }
            }
            if (memNameList.Length > 1)
            {
                return memNameList.Substring(0, memNameList.Length - 1);
            }
            else
            {
                return memNameList;
            }

        }

    }
}
