﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:tg_memberExtInfo
	/// </summary>
	public partial class tg_memberExtInfo
	{
		public tg_memberExtInfo()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("mem_ID", "tg_memberExtInfo"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int mem_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from tg_memberExtInfo");
			strSql.Append(" where mem_ID=@mem_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_ID", SqlDbType.Int,4)			};
			parameters[0].Value = mem_ID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.tg_memberExtInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into tg_memberExtInfo(");
            strSql.Append("mem_ID,mem_Code,mem_CodeAddr,mem_Age,mem_School,mem_SchLevel,mem_SchSpec,mem_SchOutDate,mem_School2,mem_SchLevel2,mem_SchSpec2,mem_SchOutDate2,mem_ArchLevel,mem_ArchLevelTime,mem_ArchReg,mem_ArchRegTime,mem_ArchRegAddr,mem_WorkTime,mem_WorkDiff,mem_WorkDiffSub,mem_WorkYear,mem_WorkTemp,mem_InCompanyTime,mem_InCompanyDiff,mem_InCompanyDiffSub,mem_InCompanyYear,mem_InCompanyTemp,mem_FileAddr,mem_Holidaybase,mem_HolidayYear,mem_MagPishi,mem_ZhuanZheng,mem_GongZi,mem_Jiaotong,mem_Tongxun,mem_YingFa,mem_SheBaobase,mem_Gongjijin,mem_GongjijinSub,mem_Yufa,mem_CoperationDate,mem_CoperationType,mem_CoperationDateEnd,mem_CoperationSub,mem_OutCompany,mem_OutCompanyDate,mem_OutCompanyType,mem_OutCompanySub,mem_YearCharge)");
			strSql.Append(" values (");
            strSql.Append("@mem_ID,@mem_Code,@mem_CodeAddr,@mem_Age,@mem_School,@mem_SchLevel,@mem_SchSpec,@mem_SchOutDate,@mem_School2,@mem_SchLevel2,@mem_SchSpec2,@mem_SchOutDate2,@mem_ArchLevel,@mem_ArchLevelTime,@mem_ArchReg,@mem_ArchRegTime,@mem_ArchRegAddr,@mem_WorkTime,@mem_WorkDiff,@mem_WorkDiffSub,@mem_WorkYear,@mem_WorkTemp,@mem_InCompanyTime,@mem_InCompanyDiff,@mem_InCompanyDiffSub,@mem_InCompanyYear,@mem_InCompanyTemp,@mem_FileAddr,@mem_Holidaybase,@mem_HolidayYear,@mem_MagPishi,@mem_ZhuanZheng,@mem_GongZi,@mem_Jiaotong,@mem_Tongxun,@mem_YingFa,@mem_SheBaobase,@mem_Gongjijin,@mem_GongjijinSub,@mem_Yufa,@mem_CoperationDate,@mem_CoperationType,@mem_CoperationDateEnd,@mem_CoperationSub,@mem_OutCompany,@mem_OutCompanyDate,@mem_OutCompanyType,@mem_OutCompanySub,@mem_YearCharge)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_ID", SqlDbType.Int,4),
					new SqlParameter("@mem_Code", SqlDbType.NVarChar,30),
					new SqlParameter("@mem_CodeAddr", SqlDbType.NVarChar,300),
					new SqlParameter("@mem_Age", SqlDbType.Int,4),
					new SqlParameter("@mem_School", SqlDbType.NVarChar,300),
					new SqlParameter("@mem_SchLevel", SqlDbType.NVarChar,50),
					new SqlParameter("@mem_SchSpec", SqlDbType.NVarChar,50),
					new SqlParameter("@mem_SchOutDate", SqlDbType.DateTime),
					new SqlParameter("@mem_School2", SqlDbType.NVarChar,300),
					new SqlParameter("@mem_SchLevel2", SqlDbType.NVarChar,50),
					new SqlParameter("@mem_SchSpec2", SqlDbType.NVarChar,50),
					new SqlParameter("@mem_SchOutDate2", SqlDbType.DateTime),
					new SqlParameter("@mem_ArchLevel", SqlDbType.Int,4),
					new SqlParameter("@mem_ArchLevelTime", SqlDbType.DateTime),
					new SqlParameter("@mem_ArchReg", SqlDbType.Int,4),
					new SqlParameter("@mem_ArchRegTime", SqlDbType.DateTime),
					new SqlParameter("@mem_ArchRegAddr", SqlDbType.Int,4),
					new SqlParameter("@mem_WorkTime", SqlDbType.DateTime),
					new SqlParameter("@mem_WorkDiff", SqlDbType.Int,4),
					new SqlParameter("@mem_WorkDiffSub", SqlDbType.NVarChar,50),
					new SqlParameter("@mem_WorkYear", SqlDbType.Int,4),
					new SqlParameter("@mem_WorkTemp", SqlDbType.Int,4),
					new SqlParameter("@mem_InCompanyTime", SqlDbType.DateTime),
					new SqlParameter("@mem_InCompanyDiff", SqlDbType.Int,4),
					new SqlParameter("@mem_InCompanyDiffSub", SqlDbType.NVarChar,50),
					new SqlParameter("@mem_InCompanyYear", SqlDbType.Int,4),
					new SqlParameter("@mem_InCompanyTemp", SqlDbType.Int,4),
					new SqlParameter("@mem_FileAddr", SqlDbType.NVarChar,300),
					new SqlParameter("@mem_Holidaybase", SqlDbType.Int,4),
					new SqlParameter("@mem_HolidayYear", SqlDbType.Int,4),
					new SqlParameter("@mem_MagPishi", SqlDbType.NVarChar,200),
					new SqlParameter("@mem_ZhuanZheng", SqlDbType.DateTime),
					new SqlParameter("@mem_GongZi", SqlDbType.Decimal,9),
					new SqlParameter("@mem_Jiaotong", SqlDbType.Int,4),
					new SqlParameter("@mem_Tongxun", SqlDbType.Int,4),
					new SqlParameter("@mem_YingFa", SqlDbType.Decimal,9),
					new SqlParameter("@mem_SheBaobase", SqlDbType.Int,4),
					new SqlParameter("@mem_Gongjijin", SqlDbType.Decimal,9),
					new SqlParameter("@mem_GongjijinSub", SqlDbType.NVarChar,200),
					new SqlParameter("@mem_Yufa", SqlDbType.Decimal,9),
					new SqlParameter("@mem_CoperationDate", SqlDbType.DateTime),
					new SqlParameter("@mem_CoperationType", SqlDbType.Int,4),
					new SqlParameter("@mem_CoperationDateEnd", SqlDbType.DateTime),
					new SqlParameter("@mem_CoperationSub", SqlDbType.NVarChar,200),
					new SqlParameter("@mem_OutCompany", SqlDbType.NVarChar,300),
					new SqlParameter("@mem_OutCompanyDate", SqlDbType.DateTime),
					new SqlParameter("@mem_OutCompanyType", SqlDbType.Int,4),
					new SqlParameter("@mem_OutCompanySub", SqlDbType.NVarChar,300),
                    new SqlParameter("@mem_YearCharge", SqlDbType.Decimal,9)};
			parameters[0].Value = model.mem_ID;
			parameters[1].Value = model.mem_Code;
			parameters[2].Value = model.mem_CodeAddr;
			parameters[3].Value = model.mem_Age;
			parameters[4].Value = model.mem_School;
			parameters[5].Value = model.mem_SchLevel;
			parameters[6].Value = model.mem_SchSpec;
			parameters[7].Value = model.mem_SchOutDate;
			parameters[8].Value = model.mem_School2;
			parameters[9].Value = model.mem_SchLevel2;
			parameters[10].Value = model.mem_SchSpec2;
			parameters[11].Value = model.mem_SchOutDate2;
			parameters[12].Value = model.mem_ArchLevel;
			parameters[13].Value = model.mem_ArchLevelTime;
			parameters[14].Value = model.mem_ArchReg;
			parameters[15].Value = model.mem_ArchRegTime;
			parameters[16].Value = model.mem_ArchRegAddr;
			parameters[17].Value = model.mem_WorkTime;
			parameters[18].Value = model.mem_WorkDiff;
			parameters[19].Value = model.mem_WorkDiffSub;
			parameters[20].Value = model.mem_WorkYear;
			parameters[21].Value = model.mem_WorkTemp;
			parameters[22].Value = model.mem_InCompanyTime;
			parameters[23].Value = model.mem_InCompanyDiff;
			parameters[24].Value = model.mem_InCompanyDiffSub;
			parameters[25].Value = model.mem_InCompanyYear;
			parameters[26].Value = model.mem_InCompanyTemp;
			parameters[27].Value = model.mem_FileAddr;
			parameters[28].Value = model.mem_Holidaybase;
			parameters[29].Value = model.mem_HolidayYear;
			parameters[30].Value = model.mem_MagPishi;
			parameters[31].Value = model.mem_ZhuanZheng;
			parameters[32].Value = model.mem_GongZi;
			parameters[33].Value = model.mem_Jiaotong;
			parameters[34].Value = model.mem_Tongxun;
			parameters[35].Value = model.mem_YingFa;
			parameters[36].Value = model.mem_SheBaobase;
			parameters[37].Value = model.mem_Gongjijin;
			parameters[38].Value = model.mem_GongjijinSub;
			parameters[39].Value = model.mem_Yufa;
			parameters[40].Value = model.mem_CoperationDate;
			parameters[41].Value = model.mem_CoperationType;
			parameters[42].Value = model.mem_CoperationDateEnd;
			parameters[43].Value = model.mem_CoperationSub;
			parameters[44].Value = model.mem_OutCompany;
			parameters[45].Value = model.mem_OutCompanyDate;
			parameters[46].Value = model.mem_OutCompanyType;
            parameters[47].Value = model.mem_OutCompanySub;
            parameters[48].Value = model.mem_YearCharge;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_memberExtInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update tg_memberExtInfo set ");
			strSql.Append("mem_Code=@mem_Code,");
			strSql.Append("mem_CodeAddr=@mem_CodeAddr,");
			strSql.Append("mem_Age=@mem_Age,");
			strSql.Append("mem_School=@mem_School,");
			strSql.Append("mem_SchLevel=@mem_SchLevel,");
			strSql.Append("mem_SchSpec=@mem_SchSpec,");
			strSql.Append("mem_SchOutDate=@mem_SchOutDate,");
			strSql.Append("mem_School2=@mem_School2,");
			strSql.Append("mem_SchLevel2=@mem_SchLevel2,");
			strSql.Append("mem_SchSpec2=@mem_SchSpec2,");
			strSql.Append("mem_SchOutDate2=@mem_SchOutDate2,");
			strSql.Append("mem_ArchLevel=@mem_ArchLevel,");
			strSql.Append("mem_ArchLevelTime=@mem_ArchLevelTime,");
			strSql.Append("mem_ArchReg=@mem_ArchReg,");
			strSql.Append("mem_ArchRegTime=@mem_ArchRegTime,");
			strSql.Append("mem_ArchRegAddr=@mem_ArchRegAddr,");
			strSql.Append("mem_WorkTime=@mem_WorkTime,");
			strSql.Append("mem_WorkDiff=@mem_WorkDiff,");
			strSql.Append("mem_WorkDiffSub=@mem_WorkDiffSub,");
			strSql.Append("mem_WorkYear=@mem_WorkYear,");
			strSql.Append("mem_WorkTemp=@mem_WorkTemp,");
			strSql.Append("mem_InCompanyTime=@mem_InCompanyTime,");
			strSql.Append("mem_InCompanyDiff=@mem_InCompanyDiff,");
			strSql.Append("mem_InCompanyDiffSub=@mem_InCompanyDiffSub,");
			strSql.Append("mem_InCompanyYear=@mem_InCompanyYear,");
			strSql.Append("mem_InCompanyTemp=@mem_InCompanyTemp,");
			strSql.Append("mem_FileAddr=@mem_FileAddr,");
			strSql.Append("mem_Holidaybase=@mem_Holidaybase,");
			strSql.Append("mem_HolidayYear=@mem_HolidayYear,");
			strSql.Append("mem_MagPishi=@mem_MagPishi,");
			strSql.Append("mem_ZhuanZheng=@mem_ZhuanZheng,");
			strSql.Append("mem_GongZi=@mem_GongZi,");
			strSql.Append("mem_Jiaotong=@mem_Jiaotong,");
			strSql.Append("mem_Tongxun=@mem_Tongxun,");
			strSql.Append("mem_YingFa=@mem_YingFa,");
			strSql.Append("mem_SheBaobase=@mem_SheBaobase,");
			strSql.Append("mem_Gongjijin=@mem_Gongjijin,");
			strSql.Append("mem_GongjijinSub=@mem_GongjijinSub,");
			strSql.Append("mem_Yufa=@mem_Yufa,");
			strSql.Append("mem_CoperationDate=@mem_CoperationDate,");
			strSql.Append("mem_CoperationType=@mem_CoperationType,");
			strSql.Append("mem_CoperationDateEnd=@mem_CoperationDateEnd,");
			strSql.Append("mem_CoperationSub=@mem_CoperationSub,");
			strSql.Append("mem_OutCompany=@mem_OutCompany,");
			strSql.Append("mem_OutCompanyDate=@mem_OutCompanyDate,");
			strSql.Append("mem_OutCompanyType=@mem_OutCompanyType,");
            strSql.Append("mem_OutCompanySub=@mem_OutCompanySub,");
            strSql.Append("mem_YearCharge=@mem_YearCharge");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_Code", SqlDbType.NVarChar,30),
					new SqlParameter("@mem_CodeAddr", SqlDbType.NVarChar,300),
					new SqlParameter("@mem_Age", SqlDbType.Int,4),
					new SqlParameter("@mem_School", SqlDbType.NVarChar,300),
					new SqlParameter("@mem_SchLevel", SqlDbType.NVarChar,50),
					new SqlParameter("@mem_SchSpec", SqlDbType.NVarChar,50),
					new SqlParameter("@mem_SchOutDate", SqlDbType.DateTime),
					new SqlParameter("@mem_School2", SqlDbType.NVarChar,300),
					new SqlParameter("@mem_SchLevel2", SqlDbType.NVarChar,50),
					new SqlParameter("@mem_SchSpec2", SqlDbType.NVarChar,50),
					new SqlParameter("@mem_SchOutDate2", SqlDbType.DateTime),
					new SqlParameter("@mem_ArchLevel", SqlDbType.Int,4),
					new SqlParameter("@mem_ArchLevelTime", SqlDbType.DateTime),
					new SqlParameter("@mem_ArchReg", SqlDbType.Int,4),
					new SqlParameter("@mem_ArchRegTime", SqlDbType.DateTime),
					new SqlParameter("@mem_ArchRegAddr", SqlDbType.Int,4),
					new SqlParameter("@mem_WorkTime", SqlDbType.DateTime),
					new SqlParameter("@mem_WorkDiff", SqlDbType.Int,4),
					new SqlParameter("@mem_WorkDiffSub", SqlDbType.NVarChar,50),
					new SqlParameter("@mem_WorkYear", SqlDbType.Int,4),
					new SqlParameter("@mem_WorkTemp", SqlDbType.Int,4),
					new SqlParameter("@mem_InCompanyTime", SqlDbType.DateTime),
					new SqlParameter("@mem_InCompanyDiff", SqlDbType.Int,4),
					new SqlParameter("@mem_InCompanyDiffSub", SqlDbType.NVarChar,50),
					new SqlParameter("@mem_InCompanyYear", SqlDbType.Int,4),
					new SqlParameter("@mem_InCompanyTemp", SqlDbType.Int,4),
					new SqlParameter("@mem_FileAddr", SqlDbType.NVarChar,300),
					new SqlParameter("@mem_Holidaybase", SqlDbType.Int,4),
					new SqlParameter("@mem_HolidayYear", SqlDbType.Int,4),
					new SqlParameter("@mem_MagPishi", SqlDbType.NVarChar,200),
					new SqlParameter("@mem_ZhuanZheng", SqlDbType.DateTime),
					new SqlParameter("@mem_GongZi", SqlDbType.Decimal,9),
					new SqlParameter("@mem_Jiaotong", SqlDbType.Int,4),
					new SqlParameter("@mem_Tongxun", SqlDbType.Int,4),
					new SqlParameter("@mem_YingFa", SqlDbType.Decimal,9),
					new SqlParameter("@mem_SheBaobase", SqlDbType.Int,4),
					new SqlParameter("@mem_Gongjijin", SqlDbType.Decimal,9),
					new SqlParameter("@mem_GongjijinSub", SqlDbType.NVarChar,200),
					new SqlParameter("@mem_Yufa", SqlDbType.Decimal,9),
					new SqlParameter("@mem_CoperationDate", SqlDbType.DateTime),
					new SqlParameter("@mem_CoperationType", SqlDbType.Int,4),
					new SqlParameter("@mem_CoperationDateEnd", SqlDbType.DateTime),
					new SqlParameter("@mem_CoperationSub", SqlDbType.NVarChar,200),
					new SqlParameter("@mem_OutCompany", SqlDbType.NVarChar,300),
					new SqlParameter("@mem_OutCompanyDate", SqlDbType.DateTime),
					new SqlParameter("@mem_OutCompanyType", SqlDbType.Int,4),
					new SqlParameter("@mem_OutCompanySub", SqlDbType.NVarChar,300),
					new SqlParameter("@ID", SqlDbType.Int,4),
					new SqlParameter("@mem_ID", SqlDbType.Int,4),
					new SqlParameter("@mem_YearCharge", SqlDbType.Decimal,9)};
			parameters[0].Value = model.mem_Code;
			parameters[1].Value = model.mem_CodeAddr;
			parameters[2].Value = model.mem_Age;
			parameters[3].Value = model.mem_School;
			parameters[4].Value = model.mem_SchLevel;
			parameters[5].Value = model.mem_SchSpec;
			parameters[6].Value = model.mem_SchOutDate;
			parameters[7].Value = model.mem_School2;
			parameters[8].Value = model.mem_SchLevel2;
			parameters[9].Value = model.mem_SchSpec2;
			parameters[10].Value = model.mem_SchOutDate2;
			parameters[11].Value = model.mem_ArchLevel;
			parameters[12].Value = model.mem_ArchLevelTime;
			parameters[13].Value = model.mem_ArchReg;
			parameters[14].Value = model.mem_ArchRegTime;
			parameters[15].Value = model.mem_ArchRegAddr;
			parameters[16].Value = model.mem_WorkTime;
			parameters[17].Value = model.mem_WorkDiff;
			parameters[18].Value = model.mem_WorkDiffSub;
			parameters[19].Value = model.mem_WorkYear;
			parameters[20].Value = model.mem_WorkTemp;
			parameters[21].Value = model.mem_InCompanyTime;
			parameters[22].Value = model.mem_InCompanyDiff;
			parameters[23].Value = model.mem_InCompanyDiffSub;
			parameters[24].Value = model.mem_InCompanyYear;
			parameters[25].Value = model.mem_InCompanyTemp;
			parameters[26].Value = model.mem_FileAddr;
			parameters[27].Value = model.mem_Holidaybase;
			parameters[28].Value = model.mem_HolidayYear;
			parameters[29].Value = model.mem_MagPishi;
			parameters[30].Value = model.mem_ZhuanZheng;
			parameters[31].Value = model.mem_GongZi;
			parameters[32].Value = model.mem_Jiaotong;
			parameters[33].Value = model.mem_Tongxun;
			parameters[34].Value = model.mem_YingFa;
			parameters[35].Value = model.mem_SheBaobase;
			parameters[36].Value = model.mem_Gongjijin;
			parameters[37].Value = model.mem_GongjijinSub;
			parameters[38].Value = model.mem_Yufa;
			parameters[39].Value = model.mem_CoperationDate;
			parameters[40].Value = model.mem_CoperationType;
			parameters[41].Value = model.mem_CoperationDateEnd;
			parameters[42].Value = model.mem_CoperationSub;
			parameters[43].Value = model.mem_OutCompany;
			parameters[44].Value = model.mem_OutCompanyDate;
			parameters[45].Value = model.mem_OutCompanyType;
			parameters[46].Value = model.mem_OutCompanySub;
			parameters[47].Value = model.ID;
            parameters[48].Value = model.mem_ID;
            parameters[49].Value = model.mem_YearCharge;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_memberExtInfo ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteByMemID(int mem_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_memberExtInfo ");
			strSql.Append(" where mem_ID=@mem_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_ID", SqlDbType.Int,4)			};
			parameters[0].Value = mem_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_memberExtInfo ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_memberExtInfo GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select  top 1 ID,mem_ID,mem_Code,mem_CodeAddr,mem_Age,mem_School,mem_SchLevel,mem_SchSpec,mem_SchOutDate,mem_School2,mem_SchLevel2,mem_SchSpec2,mem_SchOutDate2,mem_ArchLevel,mem_ArchLevelTime,mem_ArchReg,mem_ArchRegTime,mem_ArchRegAddr,mem_WorkTime,mem_WorkDiff,mem_WorkDiffSub,mem_WorkYear,mem_WorkTemp,mem_InCompanyTime,mem_InCompanyDiff,mem_InCompanyDiffSub,mem_InCompanyYear,mem_InCompanyTemp,mem_FileAddr,mem_Holidaybase,mem_HolidayYear,mem_MagPishi,mem_ZhuanZheng,mem_GongZi,mem_Jiaotong,mem_Tongxun,mem_YingFa,mem_SheBaobase,mem_Gongjijin,mem_GongjijinSub,mem_Yufa,mem_CoperationDate,mem_CoperationType,mem_CoperationDateEnd,mem_CoperationSub,mem_OutCompany,mem_OutCompanyDate,mem_OutCompanyType,mem_OutCompanySub,mem_YearCharge from tg_memberExtInfo ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.tg_memberExtInfo model=new TG.Model.tg_memberExtInfo();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_memberExtInfo DataRowToModel(DataRow row)
		{
			TG.Model.tg_memberExtInfo model=new TG.Model.tg_memberExtInfo();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["mem_ID"]!=null && row["mem_ID"].ToString()!="")
				{
					model.mem_ID=int.Parse(row["mem_ID"].ToString());
				}
				if(row["mem_Code"]!=null)
				{
					model.mem_Code=row["mem_Code"].ToString();
				}
				if(row["mem_CodeAddr"]!=null)
				{
					model.mem_CodeAddr=row["mem_CodeAddr"].ToString();
				}
				if(row["mem_Age"]!=null && row["mem_Age"].ToString()!="")
				{
					model.mem_Age=int.Parse(row["mem_Age"].ToString());
				}
				if(row["mem_School"]!=null)
				{
					model.mem_School=row["mem_School"].ToString();
				}
				if(row["mem_SchLevel"]!=null)
				{
					model.mem_SchLevel=row["mem_SchLevel"].ToString();
				}
				if(row["mem_SchSpec"]!=null)
				{
					model.mem_SchSpec=row["mem_SchSpec"].ToString();
				}
				if(row["mem_SchOutDate"]!=null && row["mem_SchOutDate"].ToString()!="")
				{
					model.mem_SchOutDate=DateTime.Parse(row["mem_SchOutDate"].ToString());
				}
				if(row["mem_School2"]!=null)
				{
					model.mem_School2=row["mem_School2"].ToString();
				}
				if(row["mem_SchLevel2"]!=null)
				{
					model.mem_SchLevel2=row["mem_SchLevel2"].ToString();
				}
				if(row["mem_SchSpec2"]!=null)
				{
					model.mem_SchSpec2=row["mem_SchSpec2"].ToString();
				}
				if(row["mem_SchOutDate2"]!=null && row["mem_SchOutDate2"].ToString()!="")
				{
					model.mem_SchOutDate2=DateTime.Parse(row["mem_SchOutDate2"].ToString());
				}
				if(row["mem_ArchLevel"]!=null && row["mem_ArchLevel"].ToString()!="")
				{
					model.mem_ArchLevel=int.Parse(row["mem_ArchLevel"].ToString());
				}
				if(row["mem_ArchLevelTime"]!=null && row["mem_ArchLevelTime"].ToString()!="")
				{
					model.mem_ArchLevelTime=DateTime.Parse(row["mem_ArchLevelTime"].ToString());
				}
				if(row["mem_ArchReg"]!=null && row["mem_ArchReg"].ToString()!="")
				{
					model.mem_ArchReg=int.Parse(row["mem_ArchReg"].ToString());
				}
				if(row["mem_ArchRegTime"]!=null && row["mem_ArchRegTime"].ToString()!="")
				{
					model.mem_ArchRegTime=DateTime.Parse(row["mem_ArchRegTime"].ToString());
				}
				if(row["mem_ArchRegAddr"]!=null && row["mem_ArchRegAddr"].ToString()!="")
				{
					model.mem_ArchRegAddr=int.Parse(row["mem_ArchRegAddr"].ToString());
				}
				if(row["mem_WorkTime"]!=null && row["mem_WorkTime"].ToString()!="")
				{
					model.mem_WorkTime=DateTime.Parse(row["mem_WorkTime"].ToString());
				}
				if(row["mem_WorkDiff"]!=null && row["mem_WorkDiff"].ToString()!="")
				{
					model.mem_WorkDiff=int.Parse(row["mem_WorkDiff"].ToString());
				}
				if(row["mem_WorkDiffSub"]!=null)
				{
					model.mem_WorkDiffSub=row["mem_WorkDiffSub"].ToString();
				}
				if(row["mem_WorkYear"]!=null && row["mem_WorkYear"].ToString()!="")
				{
					model.mem_WorkYear=int.Parse(row["mem_WorkYear"].ToString());
				}
				if(row["mem_WorkTemp"]!=null && row["mem_WorkTemp"].ToString()!="")
				{
					model.mem_WorkTemp=int.Parse(row["mem_WorkTemp"].ToString());
				}
				if(row["mem_InCompanyTime"]!=null && row["mem_InCompanyTime"].ToString()!="")
				{
					model.mem_InCompanyTime=DateTime.Parse(row["mem_InCompanyTime"].ToString());
				}
				if(row["mem_InCompanyDiff"]!=null && row["mem_InCompanyDiff"].ToString()!="")
				{
					model.mem_InCompanyDiff=int.Parse(row["mem_InCompanyDiff"].ToString());
				}
				if(row["mem_InCompanyDiffSub"]!=null)
				{
					model.mem_InCompanyDiffSub=row["mem_InCompanyDiffSub"].ToString();
				}
				if(row["mem_InCompanyYear"]!=null && row["mem_InCompanyYear"].ToString()!="")
				{
					model.mem_InCompanyYear=int.Parse(row["mem_InCompanyYear"].ToString());
				}
				if(row["mem_InCompanyTemp"]!=null && row["mem_InCompanyTemp"].ToString()!="")
				{
					model.mem_InCompanyTemp=int.Parse(row["mem_InCompanyTemp"].ToString());
				}
				if(row["mem_FileAddr"]!=null)
				{
					model.mem_FileAddr=row["mem_FileAddr"].ToString();
				}
				if(row["mem_Holidaybase"]!=null && row["mem_Holidaybase"].ToString()!="")
				{
					model.mem_Holidaybase=int.Parse(row["mem_Holidaybase"].ToString());
				}
				if(row["mem_HolidayYear"]!=null && row["mem_HolidayYear"].ToString()!="")
				{
					model.mem_HolidayYear=int.Parse(row["mem_HolidayYear"].ToString());
				}
				if(row["mem_MagPishi"]!=null)
				{
					model.mem_MagPishi=row["mem_MagPishi"].ToString();
				}
				if(row["mem_ZhuanZheng"]!=null && row["mem_ZhuanZheng"].ToString()!="")
				{
					model.mem_ZhuanZheng=DateTime.Parse(row["mem_ZhuanZheng"].ToString());
				}
				if(row["mem_GongZi"]!=null && row["mem_GongZi"].ToString()!="")
				{
					model.mem_GongZi=decimal.Parse(row["mem_GongZi"].ToString());
				}
				if(row["mem_Jiaotong"]!=null && row["mem_Jiaotong"].ToString()!="")
				{
					model.mem_Jiaotong=int.Parse(row["mem_Jiaotong"].ToString());
				}
				if(row["mem_Tongxun"]!=null && row["mem_Tongxun"].ToString()!="")
				{
					model.mem_Tongxun=int.Parse(row["mem_Tongxun"].ToString());
				}
				if(row["mem_YingFa"]!=null && row["mem_YingFa"].ToString()!="")
				{
					model.mem_YingFa=decimal.Parse(row["mem_YingFa"].ToString());
				}
				if(row["mem_SheBaobase"]!=null && row["mem_SheBaobase"].ToString()!="")
				{
					model.mem_SheBaobase=int.Parse(row["mem_SheBaobase"].ToString());
				}
				if(row["mem_Gongjijin"]!=null && row["mem_Gongjijin"].ToString()!="")
				{
					model.mem_Gongjijin=decimal.Parse(row["mem_Gongjijin"].ToString());
				}
				if(row["mem_GongjijinSub"]!=null)
				{
					model.mem_GongjijinSub=row["mem_GongjijinSub"].ToString();
				}
				if(row["mem_Yufa"]!=null && row["mem_Yufa"].ToString()!="")
				{
					model.mem_Yufa=decimal.Parse(row["mem_Yufa"].ToString());
				}
				if(row["mem_CoperationDate"]!=null && row["mem_CoperationDate"].ToString()!="")
				{
					model.mem_CoperationDate=DateTime.Parse(row["mem_CoperationDate"].ToString());
				}
				if(row["mem_CoperationType"]!=null && row["mem_CoperationType"].ToString()!="")
				{
					model.mem_CoperationType=int.Parse(row["mem_CoperationType"].ToString());
				}
				if(row["mem_CoperationDateEnd"]!=null && row["mem_CoperationDateEnd"].ToString()!="")
				{
					model.mem_CoperationDateEnd=DateTime.Parse(row["mem_CoperationDateEnd"].ToString());
				}
				if(row["mem_CoperationSub"]!=null)
				{
					model.mem_CoperationSub=row["mem_CoperationSub"].ToString();
				}
				if(row["mem_OutCompany"]!=null)
				{
					model.mem_OutCompany=row["mem_OutCompany"].ToString();
				}
				if(row["mem_OutCompanyDate"]!=null && row["mem_OutCompanyDate"].ToString()!="")
				{
					model.mem_OutCompanyDate=DateTime.Parse(row["mem_OutCompanyDate"].ToString());
				}
				if(row["mem_OutCompanyType"]!=null && row["mem_OutCompanyType"].ToString()!="")
				{
					model.mem_OutCompanyType=int.Parse(row["mem_OutCompanyType"].ToString());
				}
				if(row["mem_OutCompanySub"]!=null)
				{
					model.mem_OutCompanySub=row["mem_OutCompanySub"].ToString();
				}
                if (row["mem_YearCharge"] != null && row["mem_YearCharge"].ToString() != "")
                {
                    model.mem_YearCharge = decimal.Parse(row["mem_YearCharge"].ToString());
                }
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select ID,mem_ID,mem_Code,mem_CodeAddr,mem_Age,mem_School,mem_SchLevel,mem_SchSpec,mem_SchOutDate,mem_School2,mem_SchLevel2,mem_SchSpec2,mem_SchOutDate2,mem_ArchLevel,mem_ArchLevelTime,mem_ArchReg,mem_ArchRegTime,mem_ArchRegAddr,mem_WorkTime,mem_WorkDiff,mem_WorkDiffSub,mem_WorkYear,mem_WorkTemp,mem_InCompanyTime,mem_InCompanyDiff,mem_InCompanyDiffSub,mem_InCompanyYear,mem_InCompanyTemp,mem_FileAddr,mem_Holidaybase,mem_HolidayYear,mem_MagPishi,mem_ZhuanZheng,mem_GongZi,mem_Jiaotong,mem_Tongxun,mem_YingFa,mem_SheBaobase,mem_Gongjijin,mem_GongjijinSub,mem_Yufa,mem_CoperationDate,mem_CoperationType,mem_CoperationDateEnd,mem_CoperationSub,mem_OutCompany,mem_OutCompanyDate,mem_OutCompanyType,mem_OutCompanySub,mem_YearCharge ");
			strSql.Append(" FROM tg_memberExtInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" ID,mem_ID,mem_Code,mem_CodeAddr,mem_Age,mem_School,mem_SchLevel,mem_SchSpec,mem_SchOutDate,mem_School2,mem_SchLevel2,mem_SchSpec2,mem_SchOutDate2,mem_ArchLevel,mem_ArchLevelTime,mem_ArchReg,mem_ArchRegTime,mem_ArchRegAddr,mem_WorkTime,mem_WorkDiff,mem_WorkDiffSub,mem_WorkYear,mem_WorkTemp,mem_InCompanyTime,mem_InCompanyDiff,mem_InCompanyDiffSub,mem_InCompanyYear,mem_InCompanyTemp,mem_FileAddr,mem_Holidaybase,mem_HolidayYear,mem_MagPishi,mem_ZhuanZheng,mem_GongZi,mem_Jiaotong,mem_Tongxun,mem_YingFa,mem_SheBaobase,mem_Gongjijin,mem_GongjijinSub,mem_Yufa,mem_CoperationDate,mem_CoperationType,mem_CoperationDateEnd,mem_CoperationSub,mem_OutCompany,mem_OutCompanyDate,mem_OutCompanyType,mem_OutCompanySub,mem_YearCharge ");
			strSql.Append(" FROM tg_memberExtInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM tg_memberExtInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from tg_memberExtInfo T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "tg_memberExtInfo";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

