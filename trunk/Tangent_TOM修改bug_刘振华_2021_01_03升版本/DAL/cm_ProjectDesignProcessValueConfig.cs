﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DBUtility;
using System.Data.SqlClient;
using System.Data;

namespace TG.DAL
{

    /// <summary>
    /// 数据访问类:cm_ProjectDesignProcessValueConfig
    /// </summary>
    public partial class cm_ProjectDesignProcessValueConfig
    {
        public cm_ProjectDesignProcessValueConfig()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectDesignProcessValueConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_ProjectDesignProcessValueConfig(");
            strSql.Append("Specialty,AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)");
            strSql.Append(" values (");
            strSql.Append("@Specialty,@AuditPercent,@SpecialtyHeadPercent,@ProofreadPercent,@DesignPercent)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@Specialty", SqlDbType.NVarChar,50),
					new SqlParameter("@AuditPercent", SqlDbType.Decimal,9),
					new SqlParameter("@SpecialtyHeadPercent", SqlDbType.Decimal,9),
					new SqlParameter("@ProofreadPercent", SqlDbType.Decimal,9),
					new SqlParameter("@DesignPercent", SqlDbType.Decimal,9)};
            parameters[0].Value = model.Specialty;
            parameters[1].Value = model.AuditPercent;
            parameters[2].Value = model.SpecialtyHeadPercent;
            parameters[3].Value = model.ProofreadPercent;
            parameters[4].Value = model.DesignPercent;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectDesignProcessValueConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ProjectDesignProcessValueConfig set ");
            strSql.Append("Specialty=@Specialty,");
            strSql.Append("AuditPercent=@AuditPercent,");
            strSql.Append("SpecialtyHeadPercent=@SpecialtyHeadPercent,");
            strSql.Append("ProofreadPercent=@ProofreadPercent,");
            strSql.Append("DesignPercent=@DesignPercent");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@Specialty", SqlDbType.NVarChar,50),
					new SqlParameter("@AuditPercent", SqlDbType.Decimal,9),
					new SqlParameter("@SpecialtyHeadPercent", SqlDbType.Decimal,9),
					new SqlParameter("@ProofreadPercent", SqlDbType.Decimal,9),
					new SqlParameter("@DesignPercent", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.Specialty;
            parameters[1].Value = model.AuditPercent;
            parameters[2].Value = model.SpecialtyHeadPercent;
            parameters[3].Value = model.ProofreadPercent;
            parameters[4].Value = model.DesignPercent;
            parameters[5].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            return rows;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProjectDesignProcessValueConfig ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProjectDesignProcessValueConfig ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectDesignProcessValueConfig GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,Specialty,AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent from cm_ProjectDesignProcessValueConfig ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_ProjectDesignProcessValueConfig model = new TG.Model.cm_ProjectDesignProcessValueConfig();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Specialty"] != null && ds.Tables[0].Rows[0]["Specialty"].ToString() != "")
                {
                    model.Specialty = ds.Tables[0].Rows[0]["Specialty"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditPercent"] != null && ds.Tables[0].Rows[0]["AuditPercent"].ToString() != "")
                {
                    model.AuditPercent = decimal.Parse(ds.Tables[0].Rows[0]["AuditPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SpecialtyHeadPercent"] != null && ds.Tables[0].Rows[0]["SpecialtyHeadPercent"].ToString() != "")
                {
                    model.SpecialtyHeadPercent = decimal.Parse(ds.Tables[0].Rows[0]["SpecialtyHeadPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProofreadPercent"] != null && ds.Tables[0].Rows[0]["ProofreadPercent"].ToString() != "")
                {
                    model.ProofreadPercent = decimal.Parse(ds.Tables[0].Rows[0]["ProofreadPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DesignPercent"] != null && ds.Tables[0].Rows[0]["DesignPercent"].ToString() != "")
                {
                    model.DesignPercent = decimal.Parse(ds.Tables[0].Rows[0]["DesignPercent"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,Specialty,AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent ");
            strSql.Append(" FROM cm_ProjectDesignProcessValueConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,Specialty,AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent ");
            strSql.Append(" FROM cm_ProjectDesignProcessValueConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_ProjectDesignProcessValueConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_ProjectDesignProcessValueConfig T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "cm_ProjectDesignProcessValueConfig";
            parameters[1].Value = "ID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}
