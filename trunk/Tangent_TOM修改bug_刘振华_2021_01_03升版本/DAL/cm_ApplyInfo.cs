﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
using System.Collections;
using System.Collections.Generic;
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_ApplyInfo
	/// </summary>
	public partial class cm_ApplyInfo
	{
		public cm_ApplyInfo()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("ID", "cm_ApplyInfo"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_ApplyInfo");
			strSql.Append(" where ID="+ID+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_ApplyInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.applytype != null)
			{
				strSql1.Append("applytype,");
				strSql2.Append("'"+model.applytype+"',");
			}
			if (model.reason != null)
			{
				strSql1.Append("reason,");
				strSql2.Append("'"+model.reason+"',");
			}
			if (model.address != null)
			{
				strSql1.Append("address,");
				strSql2.Append("'"+model.address+"',");
			}
			if (model.starttime != null)
			{
				strSql1.Append("starttime,");
				strSql2.Append("'"+model.starttime+"',");
			}
			if (model.endtime != null)
			{
				strSql1.Append("endtime,");
				strSql2.Append("'"+model.endtime+"',");
			}
			if (model.totaltime != null)
			{
				strSql1.Append("totaltime,");
				strSql2.Append(""+model.totaltime+",");
			}
			if (model.iscar != null)
			{
				strSql1.Append("iscar,");
				strSql2.Append("'"+model.iscar+"',");
			}
			if (model.kilometre != null)
			{
				strSql1.Append("kilometre,");
				strSql2.Append(""+model.kilometre+",");
			}
			if (model.remark != null)
			{
				strSql1.Append("remark,");
				strSql2.Append("'"+model.remark+"',");
			}
			if (model.adduser != null)
			{
				strSql1.Append("adduser,");
				strSql2.Append(""+model.adduser+",");
			}
			if (model.addtime != null)
			{
				strSql1.Append("addtime,");
				strSql2.Append("'"+model.addtime+"',");
			}
            if (model.IsDone != null)
            {
                strSql1.Append("IsDone,");
                strSql2.Append("'" + model.IsDone + "',");
            }
            if (model.RelationID != null)
            {
                strSql1.Append("RelationID,");
                strSql2.Append("" + model.RelationID + ",");
            }
			strSql.Append("insert into cm_ApplyInfo(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ApplyInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_ApplyInfo set ");
			if (model.applytype != null)
			{
				strSql.Append("applytype='"+model.applytype+"',");
			}
			if (model.reason != null)
			{
				strSql.Append("reason='"+model.reason+"',");
			}
			else
			{
				strSql.Append("reason= null ,");
			}
			if (model.address != null)
			{
				strSql.Append("address='"+model.address+"',");
			}
			else
			{
				strSql.Append("address= null ,");
			}
			if (model.starttime != null)
			{
				strSql.Append("starttime='"+model.starttime+"',");
			}
			if (model.endtime != null)
			{
				strSql.Append("endtime='"+model.endtime+"',");
			}
			if (model.totaltime != null)
			{
				strSql.Append("totaltime="+model.totaltime+",");
			}
			else
			{
				strSql.Append("totaltime= null ,");
			}
			if (model.iscar != null)
			{
				strSql.Append("iscar='"+model.iscar+"',");
			}
			else
			{
				strSql.Append("iscar= null ,");
			}
			if (model.kilometre != null)
			{
				strSql.Append("kilometre="+model.kilometre+",");
			}
			else
			{
				strSql.Append("kilometre= null ,");
			}
			if (model.remark != null)
			{
				strSql.Append("remark='"+model.remark+"',");
			}
			else
			{
				strSql.Append("remark= null ,");
			}
			if (model.adduser != null)
			{
				strSql.Append("adduser="+model.adduser+",");
			}
			if (model.addtime != null)
			{
				strSql.Append("addtime='"+model.addtime+"',");
			}
            if (model.IsDone != null)
            {
                strSql.Append("IsDone='" + model.IsDone + "',");
            }
            else
            {
                strSql.Append("IsDone= null ,");
            }
            if (model.RelationID != null)
            {
                strSql.Append("RelationID=" + model.RelationID + ",");
            }
            else
            {
                strSql.Append("RelationID= null ,");
            }
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ID="+ model.ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ApplyInfo ");
			strSql.Append(" where ID="+ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
        public ArrayList DeleteList(string IDlist)
		{
            int count = 0;
            string[] idArray = IDlist.Split(',');

            ArrayList listAudit = new ArrayList();

            foreach (string id in idArray)
            {
                if (!string.IsNullOrEmpty(id.Trim()) && id.Trim() != ",")
                {
                    //判断 是否在审核状态 中或者管理员是否通过删除消息,通过删除消息，isdone='B'
                    if (!IsAudit(id)||IsAdmin(id))
                    {
                        count = DbHelperSQL.ExecuteSql("delete cm_ApplyInfo where ID=" + id);
                    }
                    else
                    {                       
                            listAudit.Add(id);
                    }
                }
            }

            return listAudit;			
		}
        /// <summary>
        /// 是否在审核中
        /// </summary>
        /// <param name="cprID"></param>
        /// <returns></returns>
        public bool IsAudit(string cprID)
        {
            string sql = @"select  top 1 COUNT(SysNo) from dbo.cm_ApplyInfoAudit 
                       where (Status='B' OR Status='D')
                        and ApplyID=" + cprID + "";

            DataSet ds = DbHelperSQL.Query(sql);

            if (int.Parse(ds.Tables[0].Rows[0][0].ToString()) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 管理员是否通过
        /// </summary>
        /// <param name="cprID"></param>
        /// <returns></returns>
        public bool IsAdmin(string cprID)
        {
            string sql = @"select ID from dbo.cm_ApplyInfo
                       where IsDone='B'
                        and ID=" + cprID + "";

            DataSet ds = DbHelperSQL.Query(sql);
            if (ds!=null&&ds.Tables[0]!=null&&ds.Tables[0].Rows.Count>0&&int.Parse(ds.Tables[0].Rows[0][0].ToString()) > 0)            
            {
                return true;
            }
            else
            {
                return false;
            }
        }

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ApplyInfo GetModel(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
            strSql.Append(" ID,applytype,reason,address,starttime,endtime,totaltime,iscar,kilometre,remark,adduser,addtime,IsDone,RelationID ");
			strSql.Append(" from cm_ApplyInfo ");
			strSql.Append(" where ID="+ID+"" );
			TG.Model.cm_ApplyInfo model=new TG.Model.cm_ApplyInfo();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["ID"]!=null && ds.Tables[0].Rows[0]["ID"].ToString()!="")
				{
					model.ID=int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["applytype"]!=null && ds.Tables[0].Rows[0]["applytype"].ToString()!="")
				{
					model.applytype=ds.Tables[0].Rows[0]["applytype"].ToString();
				}
				if(ds.Tables[0].Rows[0]["reason"]!=null && ds.Tables[0].Rows[0]["reason"].ToString()!="")
				{
					model.reason=ds.Tables[0].Rows[0]["reason"].ToString();
				}
				if(ds.Tables[0].Rows[0]["address"]!=null && ds.Tables[0].Rows[0]["address"].ToString()!="")
				{
					model.address=ds.Tables[0].Rows[0]["address"].ToString();
				}
				if(ds.Tables[0].Rows[0]["starttime"]!=null && ds.Tables[0].Rows[0]["starttime"].ToString()!="")
				{
					model.starttime=DateTime.Parse(ds.Tables[0].Rows[0]["starttime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["endtime"]!=null && ds.Tables[0].Rows[0]["endtime"].ToString()!="")
				{
					model.endtime=DateTime.Parse(ds.Tables[0].Rows[0]["endtime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["totaltime"]!=null && ds.Tables[0].Rows[0]["totaltime"].ToString()!="")
				{
					model.totaltime=decimal.Parse(ds.Tables[0].Rows[0]["totaltime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["iscar"]!=null && ds.Tables[0].Rows[0]["iscar"].ToString()!="")
				{
					model.iscar=ds.Tables[0].Rows[0]["iscar"].ToString();
				}
				if(ds.Tables[0].Rows[0]["kilometre"]!=null && ds.Tables[0].Rows[0]["kilometre"].ToString()!="")
				{
					model.kilometre=decimal.Parse(ds.Tables[0].Rows[0]["kilometre"].ToString());
				}
				if(ds.Tables[0].Rows[0]["remark"]!=null && ds.Tables[0].Rows[0]["remark"].ToString()!="")
				{
					model.remark=ds.Tables[0].Rows[0]["remark"].ToString();
				}
				if(ds.Tables[0].Rows[0]["adduser"]!=null && ds.Tables[0].Rows[0]["adduser"].ToString()!="")
				{
					model.adduser=int.Parse(ds.Tables[0].Rows[0]["adduser"].ToString());
				}
				if(ds.Tables[0].Rows[0]["addtime"]!=null && ds.Tables[0].Rows[0]["addtime"].ToString()!="")
				{
					model.addtime=DateTime.Parse(ds.Tables[0].Rows[0]["addtime"].ToString());
				}
                if (ds.Tables[0].Rows[0]["IsDone"] != null && ds.Tables[0].Rows[0]["IsDone"].ToString() != "")
                {
                    model.IsDone = ds.Tables[0].Rows[0]["IsDone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RelationID"] != null && ds.Tables[0].Rows[0]["RelationID"].ToString() != "")
                {
                    model.RelationID = int.Parse(ds.Tables[0].Rows[0]["RelationID"].ToString());
                }
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select ID,applytype,reason,address,starttime,endtime,totaltime,iscar,kilometre,remark,adduser,addtime,IsDone,RelationID ");
			strSql.Append(" FROM cm_ApplyInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" ID,applytype,reason,address,starttime,endtime,totaltime,iscar,kilometre,remark,adduser,addtime,IsDone,RelationID ");
			strSql.Append(" FROM cm_ApplyInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_ApplyInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_ApplyInfo T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method

        /// <summary>
        /// 获取申请通过的数据
        /// </summary>
        public DataSet GetApplyList(string strWhere)
        {
            string userlist="0";
            string SpecialPerson = System.Configuration.ConfigurationManager.AppSettings["SpecialPerson"];
            SpecialPerson = SpecialPerson.Replace("|", ",");
            string[] personlist = SpecialPerson.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            SpecialPerson = "'" + (string.Join("','",personlist))+ "'";

            DataTable mem_list = new tg_member().GetList(" mem_Name in (" + SpecialPerson + ")").Tables[0];
            if (mem_list!=null&&mem_list.Rows.Count>0)
            {
                foreach (DataRow dr in mem_list.Rows)
                {
                    userlist = userlist + "," + dr["mem_ID"];
                }
            }

            StringBuilder strSql = new StringBuilder();

            strSql.Append(@"select * from
                                 ( select *
		                            ,(select top 1 status from cm_ApplyInfoAudit where ApplyID=ID order by SysNo desc) as auditstatus
		                            ,(select top 1 status from cm_ApplyInfoAudit where ApplyID=ID order by SysNo desc) as auditsysno
                                    ,(select mem_Name from tg_member where mem_ID=adduser) as AddUserName
	                                from cm_ApplyInfo
                                 ) a
                            where ((a.auditstatus='D' and ((a.totaltime/7.5)>=5)) or (a.auditstatus='B' and (a.adduser in (" + userlist + ") or ((a.totaltime/7.5)<5))))  ");
           
            if (strWhere.Trim() != "")
            {
                strSql.Append(strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

	}
}

