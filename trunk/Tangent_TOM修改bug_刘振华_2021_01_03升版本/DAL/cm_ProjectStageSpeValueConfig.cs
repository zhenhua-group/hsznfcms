﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using TG.DBUtility;
using TG.Model;

namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_ProjectStageSpeValueConfig
    /// </summary>
    public partial class cm_ProjectStageSpeValueConfig
    {
        public cm_ProjectStageSpeValueConfig()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectStageSpeValueConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_ProjectStageSpeValueConfig(");
            strSql.Append("ProjectStage,bulidingPercent,structurePercent,drainPercent,hvacPercent,electricPercent)");
            strSql.Append(" values (");
            strSql.Append("@ProjectStage,@bulidingPercent,@structurePercent,@drainPercent,@hvacPercent,@electricPercent)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ProjectStage", SqlDbType.NVarChar,50),
					new SqlParameter("@bulidingPercent", SqlDbType.Decimal,9),
					new SqlParameter("@structurePercent", SqlDbType.Decimal,9),
					new SqlParameter("@drainPercent", SqlDbType.Decimal,9),
					new SqlParameter("@hvacPercent", SqlDbType.Decimal,9),
					new SqlParameter("@electricPercent", SqlDbType.Decimal,9)};
            parameters[0].Value = model.ProjectStage;
            parameters[1].Value = model.bulidingPercent;
            parameters[2].Value = model.structurePercent;
            parameters[3].Value = model.drainPercent;
            parameters[4].Value = model.hvacPercent;
            parameters[5].Value = model.electricPercent;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectStageSpeValueConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ProjectStageSpeValueConfig set ");
            strSql.Append("ProjectStage=@ProjectStage,");
            strSql.Append("bulidingPercent=@bulidingPercent,");
            strSql.Append("structurePercent=@structurePercent,");
            strSql.Append("drainPercent=@drainPercent,");
            strSql.Append("hvacPercent=@hvacPercent,");
            strSql.Append("electricPercent=@electricPercent");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ProjectStage", SqlDbType.NVarChar,50),
					new SqlParameter("@bulidingPercent", SqlDbType.Decimal,9),
					new SqlParameter("@structurePercent", SqlDbType.Decimal,9),
					new SqlParameter("@drainPercent", SqlDbType.Decimal,9),
					new SqlParameter("@hvacPercent", SqlDbType.Decimal,9),
					new SqlParameter("@electricPercent", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.ProjectStage;
            parameters[1].Value = model.bulidingPercent;
            parameters[2].Value = model.structurePercent;
            parameters[3].Value = model.drainPercent;
            parameters[4].Value = model.hvacPercent;
            parameters[5].Value = model.electricPercent;
            parameters[6].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            return rows;
        }


        /// <summary>
        /// 批量更新数据
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateValueDetatil(cm_ProjectStageSpeValueConfigEntity dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                string projectStage = dataEntity.ProjectStage;

                if (projectStage.Trim().Equals("方案设计"))
                {
                    dataEntity.Specialty.ForEach((spe) =>
                    {
                        string specialtyName = spe.SpecialtyName;
                        if (spe.SpecialtyName.Contains("："))
                        {
                            specialtyName = spe.SpecialtyName.Replace("：", "");
                        }
                        command.CommandText = "update  cm_ProjectStageSpeValueConfig set ProgramPercent=" + decimal.Parse(spe.SpecialtyValue) + " where Specialty like '%" + specialtyName + "%' and IsHaved=" + dataEntity.IsHaved + "";
                        command.ExecuteNonQuery();
                    });

                }
                else if (projectStage.Trim().Equals("初步设计"))
                {
                    dataEntity.Specialty.ForEach((spe) =>
                    {
                        string specialtyName = "";
                        if (spe.SpecialtyName.Contains("："))
                        {
                            specialtyName = spe.SpecialtyName.Replace("：", "");
                        }
                        command.CommandText = "update  cm_ProjectStageSpeValueConfig set preliminaryPercent=" + decimal.Parse(spe.SpecialtyValue) + " where Specialty  like'%" + specialtyName + "%'and IsHaved=" + dataEntity.IsHaved + "";
                        command.ExecuteNonQuery();
                    });
                }
                else if (projectStage.Trim().Equals("施工图设计"))
                {
                    dataEntity.Specialty.ForEach((spe) =>
                    {
                        string specialtyName = "";
                        if (spe.SpecialtyName.Contains("："))
                        {
                            specialtyName = spe.SpecialtyName.Replace("：", "");
                        }
                        command.CommandText = "update  cm_ProjectStageSpeValueConfig set WorkDrawPercent=" + decimal.Parse(spe.SpecialtyValue) + " where Specialty like '%" + specialtyName + "%'and IsHaved=" + dataEntity.IsHaved + "";
                        command.ExecuteNonQuery();
                    });
                }
                else
                {
                    dataEntity.Specialty.ForEach((spe) =>
                    {
                        string specialtyName = "";
                        if (spe.SpecialtyName.Contains("："))
                        {
                            specialtyName = spe.SpecialtyName.Replace("：", "");
                        }
                        command.CommandText = "update  cm_ProjectStageSpeValueConfig set LateStagePercent=" + decimal.Parse(spe.SpecialtyValue) + " where Specialty like'%" + specialtyName + "%'and IsHaved=" + dataEntity.IsHaved + "";
                        command.ExecuteNonQuery();
                    });
                }

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProjectStageSpeValueConfig ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProjectStageSpeValueConfig ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectStageSpeValueConfig GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,ProjectStage,bulidingPercent,structurePercent,drainPercent,hvacPercent,electricPercent from cm_ProjectStageSpeValueConfig ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_ProjectStageSpeValueConfig model = new TG.Model.cm_ProjectStageSpeValueConfig();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProjectStage"] != null && ds.Tables[0].Rows[0]["ProjectStage"].ToString() != "")
                {
                    model.ProjectStage = ds.Tables[0].Rows[0]["ProjectStage"].ToString();
                }
                if (ds.Tables[0].Rows[0]["bulidingPercent"] != null && ds.Tables[0].Rows[0]["bulidingPercent"].ToString() != "")
                {
                    model.bulidingPercent = decimal.Parse(ds.Tables[0].Rows[0]["bulidingPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["structurePercent"] != null && ds.Tables[0].Rows[0]["structurePercent"].ToString() != "")
                {
                    model.structurePercent = decimal.Parse(ds.Tables[0].Rows[0]["structurePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["drainPercent"] != null && ds.Tables[0].Rows[0]["drainPercent"].ToString() != "")
                {
                    model.drainPercent = decimal.Parse(ds.Tables[0].Rows[0]["drainPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["hvacPercent"] != null && ds.Tables[0].Rows[0]["hvacPercent"].ToString() != "")
                {
                    model.hvacPercent = decimal.Parse(ds.Tables[0].Rows[0]["hvacPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["electricPercent"] != null && ds.Tables[0].Rows[0]["electricPercent"].ToString() != "")
                {
                    model.electricPercent = decimal.Parse(ds.Tables[0].Rows[0]["electricPercent"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,ProjectStage,bulidingPercent,structurePercent,drainPercent,hvacPercent,electricPercent ");
            strSql.Append(" FROM cm_ProjectStageSpeValueConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,ProjectStage,bulidingPercent,structurePercent,drainPercent,hvacPercent,electricPercent ");
            strSql.Append(" FROM cm_ProjectStageSpeValueConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_ProjectStageSpeValueConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_ProjectStageSpeValueConfig T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "cm_ProjectStageSpeValueConfig";
            parameters[1].Value = "ID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}
