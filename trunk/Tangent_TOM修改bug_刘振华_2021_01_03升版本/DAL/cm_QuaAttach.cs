﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DBUtility;
using System.Data;
using System.Data.SqlClient;

namespace TG.DAL
{
    public class cm_QuaAttach
    {
        #region  Method
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_QuaAttach model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_QuaAttach(");
            strSql.Append("RelativePath,FileName,FileType,FileSize,UpLoadDate,WebBannerSysNo,IconPath)");
            strSql.Append(" values (");
            strSql.Append("@RelativePath,@FileName,@FileType,@FileSize,@UpLoadDate,@WebBannerSysNo,@IconPath)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@RelativePath", SqlDbType.NVarChar,200),
					new SqlParameter("@FileName", SqlDbType.NVarChar,100),
					new SqlParameter("@FileType", SqlDbType.VarChar,50),
					new SqlParameter("@FileSize", SqlDbType.Int,4),
					new SqlParameter("@UpLoadDate", SqlDbType.DateTime),
					new SqlParameter("@WebBannerSysNo", SqlDbType.NVarChar,200),
					new SqlParameter("@IconPath", SqlDbType.VarChar,50)};
            parameters[0].Value = model.RelativePath;
            parameters[1].Value = model.FileName;
            parameters[2].Value = model.FileType;
            parameters[3].Value = model.FileSize;
            parameters[4].Value = model.UpLoadDate;
            parameters[5].Value = model.WebBannerSysNo;
            parameters[6].Value = model.IconPath;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_QuaAttach model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_QuaAttach set ");
            strSql.Append("RelativePath=@RelativePath,");
            strSql.Append("FileName=@FileName,");
            strSql.Append("FileType=@FileType,");
            strSql.Append("FileSize=@FileSize,");
            strSql.Append("UpLoadDate=@UpLoadDate,");
            strSql.Append("WebBannerSysNo=@WebBannerSysNo,");
            strSql.Append("IconPath=@IconPath");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@RelativePath", SqlDbType.NVarChar,200),
					new SqlParameter("@FileName", SqlDbType.NVarChar,100),
					new SqlParameter("@FileType", SqlDbType.VarChar,50),
					new SqlParameter("@FileSize", SqlDbType.Int,4),
					new SqlParameter("@UpLoadDate", SqlDbType.DateTime),
					new SqlParameter("@WebBannerSysNo", SqlDbType.NVarChar,200),
					new SqlParameter("@IconPath", SqlDbType.VarChar,50),
					new SqlParameter("@SysNo", SqlDbType.Int,4)};
            parameters[0].Value = model.RelativePath;
            parameters[1].Value = model.FileName;
            parameters[2].Value = model.FileType;
            parameters[3].Value = model.FileSize;
            parameters[4].Value = model.UpLoadDate;
            parameters[5].Value = model.WebBannerSysNo;
            parameters[6].Value = model.IconPath;
            parameters[7].Value = model.SysNo;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SysNo)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_QuaAttach ");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SysNolist)
        {
            //cDELETE cm_QuaAttach WHERE WebBannerSysNo='18'
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_QuaAttach ");
            strSql.Append(" where WebBannerSysNo ='" + SysNolist + "'");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_QuaAttach GetModel(int SysNo)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SysNo,RelativePath,FileName,FileType,FileSize,UpLoadDate,WebBannerSysNo,IconPath from cm_QuaAttach ");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            TG.Model.cm_QuaAttach model = new TG.Model.cm_QuaAttach();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["SysNo"] != null && ds.Tables[0].Rows[0]["SysNo"].ToString() != "")
                {
                    model.SysNo = int.Parse(ds.Tables[0].Rows[0]["SysNo"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RelativePath"] != null && ds.Tables[0].Rows[0]["RelativePath"].ToString() != "")
                {
                    model.RelativePath = ds.Tables[0].Rows[0]["RelativePath"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FileName"] != null && ds.Tables[0].Rows[0]["FileName"].ToString() != "")
                {
                    model.FileName = ds.Tables[0].Rows[0]["FileName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FileType"] != null && ds.Tables[0].Rows[0]["FileType"].ToString() != "")
                {
                    model.FileType = ds.Tables[0].Rows[0]["FileType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FileSize"] != null && ds.Tables[0].Rows[0]["FileSize"].ToString() != "")
                {
                    model.FileSize = int.Parse(ds.Tables[0].Rows[0]["FileSize"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpLoadDate"] != null && ds.Tables[0].Rows[0]["UpLoadDate"].ToString() != "")
                {
                    model.UpLoadDate = DateTime.Parse(ds.Tables[0].Rows[0]["UpLoadDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["WebBannerSysNo"] != null && ds.Tables[0].Rows[0]["WebBannerSysNo"].ToString() != "")
                {
                    model.WebBannerSysNo = ds.Tables[0].Rows[0]["WebBannerSysNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["IconPath"] != null && ds.Tables[0].Rows[0]["IconPath"].ToString() != "")
                {
                    model.IconPath = ds.Tables[0].Rows[0]["IconPath"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SysNo,RelativePath,FileName,FileType,FileSize,UpLoadDate,WebBannerSysNo,IconPath ");
            strSql.Append(" FROM cm_QuaAttach ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" SysNo,RelativePath,FileName,FileType,FileSize,UpLoadDate,WebBannerSysNo,IconPath ");
            strSql.Append(" FROM cm_QuaAttach ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_QuaAttach ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.SysNo desc");
            }
            strSql.Append(")AS Row, T.*  from cm_QuaAttach T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "cm_QuaAttach";
            parameters[1].Value = "SysNo";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}
