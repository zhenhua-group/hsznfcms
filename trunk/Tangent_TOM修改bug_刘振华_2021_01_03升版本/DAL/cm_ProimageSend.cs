﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_ProimageSend
	/// </summary>
	public partial class cm_ProimageSend
	{
		public cm_ProimageSend()
		{}
		#region  Method
        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("ID", "cm_ProimageSend");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_ProimageSend");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 判断是否存在该记录，并得到id
        /// </summary>
        public int GetSingle(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select top 1 ID from cm_ProimageSend");
            strSql.Append(" where pro_id=" + ID + "");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());

            return obj == null ? 0 : Convert.ToInt32(obj);
        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_ProimageSend model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_ProimageSend(");
			strSql.Append("Pro_id,Pro_image,Send_number,Send_company,Send_A0,Send_A1,Send_A2,Send_A2_1,Send_A3,Send_A4,Adduser,Addtime)");
			strSql.Append(" values (");
			strSql.Append("@Pro_id,@Pro_image,@Send_number,@Send_company,@Send_A0,@Send_A1,@Send_A2,@Send_A2_1,@Send_A3,@Send_A4,@Adduser,@Addtime)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@Pro_id", SqlDbType.Int,4),
					new SqlParameter("@Pro_image", SqlDbType.Int,4),
					new SqlParameter("@Send_number", SqlDbType.Int,4),
					new SqlParameter("@Send_company", SqlDbType.NVarChar,50),
					new SqlParameter("@Send_A0", SqlDbType.Decimal,9),
					new SqlParameter("@Send_A1", SqlDbType.Decimal,9),
					new SqlParameter("@Send_A2", SqlDbType.Decimal,9),
					new SqlParameter("@Send_A2_1", SqlDbType.Decimal,9),
					new SqlParameter("@Send_A3", SqlDbType.Decimal,9),
					new SqlParameter("@Send_A4", SqlDbType.Decimal,9),
					new SqlParameter("@Adduser", SqlDbType.Int,4),
					new SqlParameter("@Addtime", SqlDbType.DateTime)};
			parameters[0].Value = model.Pro_id;
			parameters[1].Value = model.Pro_image;
			parameters[2].Value = model.Send_number;
			parameters[3].Value = model.Send_company;
			parameters[4].Value = model.Send_A0;
			parameters[5].Value = model.Send_A1;
			parameters[6].Value = model.Send_A2;
			parameters[7].Value = model.Send_A2_1;
			parameters[8].Value = model.Send_A3;
			parameters[9].Value = model.Send_A4;
			parameters[10].Value = model.Adduser;
			parameters[11].Value = model.Addtime;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ProimageSend model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_ProimageSend set ");
			strSql.Append("Pro_id=@Pro_id,");
			strSql.Append("Pro_image=@Pro_image,");
			strSql.Append("Send_number=@Send_number,");
			strSql.Append("Send_company=@Send_company,");
			strSql.Append("Send_A0=@Send_A0,");
			strSql.Append("Send_A1=@Send_A1,");
			strSql.Append("Send_A2=@Send_A2,");
			strSql.Append("Send_A2_1=@Send_A2_1,");
			strSql.Append("Send_A3=@Send_A3,");
			strSql.Append("Send_A4=@Send_A4,");
			strSql.Append("Adduser=@Adduser,");
			strSql.Append("Addtime=@Addtime");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Pro_id", SqlDbType.Int,4),
					new SqlParameter("@Pro_image", SqlDbType.Int,4),
					new SqlParameter("@Send_number", SqlDbType.Int,4),
					new SqlParameter("@Send_company", SqlDbType.NVarChar,50),
					new SqlParameter("@Send_A0", SqlDbType.Decimal,9),
					new SqlParameter("@Send_A1", SqlDbType.Decimal,9),
					new SqlParameter("@Send_A2", SqlDbType.Decimal,9),
					new SqlParameter("@Send_A2_1", SqlDbType.Decimal,9),
					new SqlParameter("@Send_A3", SqlDbType.Decimal,9),
					new SqlParameter("@Send_A4", SqlDbType.Decimal,9),
					new SqlParameter("@Adduser", SqlDbType.Int,4),
					new SqlParameter("@Addtime", SqlDbType.DateTime),
					new SqlParameter("@Id", SqlDbType.Int,4)};
			parameters[0].Value = model.Pro_id;
			parameters[1].Value = model.Pro_image;
			parameters[2].Value = model.Send_number;
			parameters[3].Value = model.Send_company;
			parameters[4].Value = model.Send_A0;
			parameters[5].Value = model.Send_A1;
			parameters[6].Value = model.Send_A2;
			parameters[7].Value = model.Send_A2_1;
			parameters[8].Value = model.Send_A3;
			parameters[9].Value = model.Send_A4;
			parameters[10].Value = model.Adduser;
			parameters[11].Value = model.Addtime;
			parameters[12].Value = model.Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ProimageSend ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ProimageSend ");
			strSql.Append(" where Id in ("+Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ProimageSend GetModel(int Id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 Id,Pro_id,Pro_image,Send_number,Send_company,Send_A0,Send_A1,Send_A2,Send_A2_1,Send_A3,Send_A4,Adduser,Addtime from cm_ProimageSend ");
			strSql.Append(" where Id=@Id");
			SqlParameter[] parameters = {
					new SqlParameter("@Id", SqlDbType.Int,4)
			};
			parameters[0].Value = Id;

			TG.Model.cm_ProimageSend model=new TG.Model.cm_ProimageSend();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Id"]!=null && ds.Tables[0].Rows[0]["Id"].ToString()!="")
				{
					model.Id=int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Pro_id"]!=null && ds.Tables[0].Rows[0]["Pro_id"].ToString()!="")
				{
					model.Pro_id=int.Parse(ds.Tables[0].Rows[0]["Pro_id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Pro_image"]!=null && ds.Tables[0].Rows[0]["Pro_image"].ToString()!="")
				{
					model.Pro_image=int.Parse(ds.Tables[0].Rows[0]["Pro_image"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Send_number"]!=null && ds.Tables[0].Rows[0]["Send_number"].ToString()!="")
				{
					model.Send_number=int.Parse(ds.Tables[0].Rows[0]["Send_number"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Send_company"]!=null && ds.Tables[0].Rows[0]["Send_company"].ToString()!="")
				{
					model.Send_company=ds.Tables[0].Rows[0]["Send_company"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Send_A0"]!=null && ds.Tables[0].Rows[0]["Send_A0"].ToString()!="")
				{
					model.Send_A0=decimal.Parse(ds.Tables[0].Rows[0]["Send_A0"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Send_A1"]!=null && ds.Tables[0].Rows[0]["Send_A1"].ToString()!="")
				{
					model.Send_A1=decimal.Parse(ds.Tables[0].Rows[0]["Send_A1"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Send_A2"]!=null && ds.Tables[0].Rows[0]["Send_A2"].ToString()!="")
				{
					model.Send_A2=decimal.Parse(ds.Tables[0].Rows[0]["Send_A2"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Send_A2_1"]!=null && ds.Tables[0].Rows[0]["Send_A2_1"].ToString()!="")
				{
					model.Send_A2_1=decimal.Parse(ds.Tables[0].Rows[0]["Send_A2_1"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Send_A3"]!=null && ds.Tables[0].Rows[0]["Send_A3"].ToString()!="")
				{
					model.Send_A3=decimal.Parse(ds.Tables[0].Rows[0]["Send_A3"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Send_A4"]!=null && ds.Tables[0].Rows[0]["Send_A4"].ToString()!="")
				{
					model.Send_A4=decimal.Parse(ds.Tables[0].Rows[0]["Send_A4"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Adduser"]!=null && ds.Tables[0].Rows[0]["Adduser"].ToString()!="")
				{
					model.Adduser=int.Parse(ds.Tables[0].Rows[0]["Adduser"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Addtime"]!=null && ds.Tables[0].Rows[0]["Addtime"].ToString()!="")
				{
					model.Addtime=DateTime.Parse(ds.Tables[0].Rows[0]["Addtime"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Id,Pro_id,Pro_image,Send_number,Send_company,Send_A0,Send_A1,Send_A2,Send_A2_1,Send_A3,Send_A4,Adduser,Addtime ");
			strSql.Append(" FROM cm_ProimageSend ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Id,Pro_id,Pro_image,Send_number,Send_company,Send_A0,Send_A1,Send_A2,Send_A2_1,Send_A3,Send_A4,Adduser,Addtime ");
			strSql.Append(" FROM cm_ProimageSend ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_ProimageSend ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Id desc");
			}
			strSql.Append(")AS Row, T.*  from cm_ProimageSend T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_ProimageSend";
			parameters[1].Value = "Id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

