﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using System.Data.SqlClient;
using TG.DBUtility;
using TG.Common.EntityBuilder;
using System.Data;

namespace TG.DAL
{
    public class StandBookDA
    {
        /// <summary>
        /// 得到所有的对象
        /// </summary>
        /// <param name="unit_name"></param>
        /// <returns></returns>
        public List<StandBookEntity> GetList(string unit_name, string year, string cprName)
        {
            string sql = "";
            if (unit_name != "-1")
            {
                if (year != "-1")
                {
                    sql = string.Format(@"SELECT a.cpr_Id as 'Cprid', cpr_No AS 'CprNo' ,BuildArea AS 'BuildArea',cpr_Name AS 'CprName',
cpr_Acount AS 'CprAcount' from
 cm_Coperation a where  a.cpr_Unit='{0}'  AND a.cpr_SignDate BETWEEN '{1}-01-01'AND '{1}-12-31' {2}", unit_name, year, cprName);
                }
                else
                {
                    sql = string.Format(@"SELECT a.cpr_Id as 'Cprid', cpr_No AS 'CprNo' ,BuildArea AS 'BuildArea',cpr_Name AS 'CprName',
cpr_Acount AS 'CprAcount' from
 cm_Coperation a where  a.cpr_Unit='{0}' {1}  ", unit_name, cprName);
                }

            }
            else
            {
                if (year != "-1")
                {
                    sql = string.Format(@"SELECT a.cpr_Id as 'Cprid', cpr_No AS 'CprNo' ,BuildArea AS 'BuildArea',cpr_Name AS 'CprName',
cpr_Acount AS 'CprAcount' from
 cm_Coperation a where   a.cpr_SignDate BETWEEN '{0}-01-01'AND '{0}-12-31' {1}", year, cprName);
                }
                else
                {
                    sql = string.Format(@"SELECT a.cpr_Id as 'Cprid', cpr_No AS 'CprNo' ,BuildArea AS 'BuildArea',cpr_Name AS 'CprName',
cpr_Acount AS 'CprAcount' from
 cm_Coperation a where 1=1 {0}", cprName);
                }

            }

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<StandBookEntity> resultList = EntityBuilder<StandBookEntity>.BuilderEntityList(reader);

            return resultList;
        }
        public List<TG.Model.AcoutAndTime> GetListAcount(int cptid)
        {
            string sql = @"SELECT p.Acount,p.InAcountTime  FROM cm_ProjectCharge AS p WHERE p.Status!='E'  and p.cprID=
" + cptid;
            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            List<TG.Model.AcoutAndTime> resultList = new List<Model.AcoutAndTime>();
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                TG.Model.AcoutAndTime model = new Model.AcoutAndTime();
                model.Acount = Convert.ToDecimal(item["Acount"].ToString().Trim());
                model.Time = Convert.ToDateTime(item["InAcountTime"]);
                resultList.Add(model);
            }
            return resultList;
        }
        /// <summary>
        /// 得到所有的对象
        /// </summary>
        /// <param name="unit_name"></param>
        /// <returns></returns>
        public List<Collection> GetListCollection(string beginmonth, string endmonth)
        {
            string sql = string.Format(@"SELECT a.Acount as 'Acount',a.InAcountCode as 'CprNo',
                                c.cpr_Unit as 'Unit', c.BuildUnit as 'BuildUnit',CONVERT(nvarchar(100), a.InAcountTime,23) as InAcountTime
                               FROM cm_Coperation AS  c 
                               RIGHT JOIN cm_ProjectCharge AS a ON a.cprID=c.cpr_Id 
                               WHERE  a.InAcountTime BETWEEN '{0}'AND '{1}' AND a.Status!='B' order by a.InAcountTime desc", beginmonth, endmonth);
            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            List<TG.Model.Collection> resultList = new List<Model.Collection>();
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                TG.Model.Collection model = new Model.Collection();
                model.Acount = Convert.ToDecimal(item["Acount"].ToString().Trim());
                model.BuildUnit = item["BuildUnit"].ToString().Trim();
                model.CprNo = item["CprNo"].ToString();
                model.InAcountTime = item["InAcountTime"].ToString();
                model.Unit = item["Unit"].ToString();
                resultList.Add(model);
            }
            return resultList;
        }
        /// <summary>
        /// 得的对象
        /// </summary>
        /// <param name="unit_name"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public List<Outsummary> GetListSummary(string unit_name, string year, string cpr_Name)
        {
            string sql = "";
            if (unit_name != "-1")
            {
                if (year != "-1")
                {
                    sql = string.Format(@"SELECT d.pro_Name as cpr_Name ,
                                (SELECT [dbo].[fun_cm_ProjectCharge](A.cpr_Id))as  acountAdd ,
                                ISNULL(PayShiCount,0) AS SUMAcount,--实收
                                ISNULL((PayShiCount-AllotCount),0) AS WeiFenValue,--为分配金额
                                ISNULL(EconomyValueCount*0.0001,0) AS ToOtherValue,--转经济所
                                ISNULL(HavcCount*0.0001,0) AS HavcCount,--转暖通所
                                ISNULL(tranBuild*0.0001,0) AS TranBulidingCount,--转土建所
                                ISNULL(AllotCount,0) AS ActualValue --本部门实际产值
                                FROM   cm_Project D inner join cm_Coperation  A
                                ON D.CoperationSysNo=A.cpr_Id
                                 LEFT JOIN 
                                ( Select pro_ID, SUM(AllotCount) AS AllotCount , SUM(EconomyValueCount) AS EconomyValueCount,SUM(HavcCount) HavcCount,SUM(TranBulidingCount) tranBuild,SUM(UnitValueCount) AS TheDeptValueCount from cm_ProjectValueAllot   WHERE Status='S' and SecondValue='1' group by pro_ID ) B
                                ON D.pro_ID=B.pro_ID
                                LEFT JOIN
                                (Select cprID, SUM(Acount) AS PayShiCount from cm_ProjectCharge WHERE (Status !='B' AND InAcountTime BETWEEN '{1}-01-01' AND '{1}-12-31' )  group by cprID) AS C
                                ON A.cpr_Id=C.cprID WHERE d.Unit='{0}' and d.pro_Name like '%{2}%' and  PayShiCount>0  ", unit_name.Trim(), year, cpr_Name);
                }
                else
                {
                    sql = string.Format(@"SELECT d.pro_name as cpr_Name ,
                                (SELECT [dbo].[fun_cm_ProjectCharge](A.cpr_Id))as  acountAdd ,
                                ISNULL(PayShiCount,0) AS SUMAcount,--实收
                                ISNULL((PayShiCount-AllotCount),0) AS WeiFenValue,--为分配金额
                                ISNULL(EconomyValueCount*0.0001,0) AS ToOtherValue,--转经济所
                                ISNULL(HavcCount*0.0001,0) AS HavcCount,--转暖通所
                                ISNULL(tranBuild*0.0001,0) AS TranBulidingCount,--转土建所
                                ISNULL(AllotCount,0) AS ActualValue --本部门实际产值
                                FROM cm_Project D inner join cm_Coperation  A	
                                ON D.CoperationSysNo=A.cpr_Id
                                 LEFT JOIN 
                                ( Select pro_ID, SUM(AllotCount) AS AllotCount , SUM(EconomyValueCount) AS EconomyValueCount,SUM(HavcCount) HavcCount,SUM(TranBulidingCount) tranBuild,SUM(UnitValueCount) AS TheDeptValueCount from cm_ProjectValueAllot   WHERE Status='S' and SecondValue='1' group by pro_ID ) B
                                ON D.pro_ID=B.pro_ID
                                LEFT JOIN
                                (Select cprID, SUM(Acount) AS PayShiCount from cm_ProjectCharge WHERE (Status !='B' )  group by cprID) AS C
                                ON A.cpr_Id=C.cprID WHERE d.Unit='{0}' and d.pro_Name like '%{1}%'   and  PayShiCount>0 ", unit_name.Trim(), cpr_Name);
                }

            }
            else
            {
                //                sql = string.Format(@"SELECT cpr_Name , (SELECT [dbo].[fun_cm_ProjectCharge](a.cpr_Id))as  acountAdd ,isnull(SUMAcount,0) AS SUMAcount
                //FROM dbo.cm_Coperation AS a
                // LEFT JOIN 
                //(Select cprID, SUM(Acount) AS SUMAcount from cm_ProjectCharge  AS c WHERE c.Status='E'  group by cprID) AS b
                //ON a.cpr_Id=b.cprID WHERE  a.cpr_SignDate BETWEEN '{0}-01-01'AND '{0}-12-31'", year);
                if (year != "-1")
                {
                    sql = string.Format(@"SELECT d.pro_name as cpr_Name ,
                            (SELECT [dbo].[fun_cm_ProjectCharge](A.cpr_Id))as  acountAdd ,
                            ISNULL(PayShiCount,0) AS SUMAcount,--实收
                            ISNULL((PayShiCount-AllotCount),0) AS WeiFenValue,--为分配金额
                            ISNULL(EconomyValueCount*0.0001,0) AS ToOtherValue,--转经济所
                            ISNULL(HavcCount*0.0001,0) AS HavcCount,--转暖通所
                            ISNULL(tranBuild*0.0001,0) AS TranBulidingCount,--转土建所
                            ISNULL(AllotCount,0) AS ActualValue --本部门实际产值
                            FROM cm_Project D inner join cm_Coperation  A		
                            ON D.CoperationSysNo=A.cpr_Id
                             LEFT JOIN 
                            ( Select pro_ID, SUM(AllotCount) AS AllotCount , SUM(EconomyValueCount) AS EconomyValueCount,SUM(HavcCount) HavcCount,SUM(TranBulidingCount) tranBuild,SUM(UnitValueCount) AS TheDeptValueCount from cm_ProjectValueAllot   WHERE Status='S' and SecondValue='1' group by pro_ID ) B
                            ON D.pro_ID=B.pro_ID
                            LEFT JOIN
                            (Select cprID, SUM(Acount) AS PayShiCount from cm_ProjectCharge WHERE (Status !='B' AND InAcountTime BETWEEN '{0}-01-01' AND '{0}-12-31')  group by cprID) AS C
                            ON A.cpr_Id=C.cprID WHERE  d.pro_Name like '%{1}%' and d.pro_startTime BETWEEN '{0}-01-01' AND '{0}-12-31' and  PayShiCount>0", year, cpr_Name);
                }
                else
                {
                    sql = string.Format(@"SELECT d.pro_name cpr_Name ,
                            (SELECT [dbo].[fun_cm_ProjectCharge](A.cpr_Id))as  acountAdd ,
                            ISNULL(PayShiCount,0) AS SUMAcount,--实收
                            ISNULL((PayShiCount-AllotCount),0) AS WeiFenValue,--为分配金额
                            ISNULL(EconomyValueCount*0.0001,0) AS ToOtherValue,--转经济所
                            ISNULL(HavcCount*0.0001,0) AS HavcCount,--转暖通所
                            ISNULL(tranBuild*0.0001,0) AS TranBulidingCount,--转土建所
                            ISNULL(AllotCount,0) AS ActualValue --本部门实际产值
                            FROM cm_Project D inner join cm_Coperation  A	
                            ON D.CoperationSysNo=A.cpr_Id
                             LEFT JOIN 
                            ( Select pro_ID, SUM(AllotCount) AS AllotCount , SUM(EconomyValueCount) AS EconomyValueCount,SUM(HavcCount) HavcCount,SUM(TranBulidingCount) tranBuild,SUM(UnitValueCount) AS TheDeptValueCount from cm_ProjectValueAllot   WHERE Status='S' and SecondValue='1' group by pro_ID ) B
                            ON D.pro_ID=B.pro_ID
                            LEFT JOIN
                            (Select cprID, SUM(Acount) AS PayShiCount from cm_ProjectCharge WHERE (Status !='B' )  group by cprID) AS C
                            ON A.cpr_Id=C.cprID WHERE  d.pro_Name like '%{0}%' and  PayShiCount>0", cpr_Name);
                }

            }


            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            List<TG.Model.Outsummary> resultList = new List<Model.Outsummary>();
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                TG.Model.Outsummary model = new Model.Outsummary();
                model.CprName = item["cpr_Name"].ToString().Trim();
                model.Charge = Convert.ToDecimal(item["SUMAcount"].ToString().Trim());
                model.Chargelist = item["acountAdd"].ToString().Trim();
                model.Mark = "";
                model.ToOtherValue = Convert.ToDecimal(item["ToOtherValue"].ToString().Trim());
                model.WeiFenValue = Convert.ToDecimal(item["WeiFenValue"].ToString().Trim());
                model.ActualValue = Convert.ToDecimal(item["ActualValue"].ToString().Trim());
                model.HavcCount = Convert.ToDecimal(item["HavcCount"].ToString().Trim());
                model.TranBuild = Convert.ToDecimal(item["TranBulidingCount"].ToString().Trim());
                resultList.Add(model);
            }
            return resultList;
        }

        public List<Outsummary> GetListSummary(string unit_name, string year)
        {
            string sql = "";
            if (unit_name != "-1")
            {
                if (year != "-1")
                {
                    sql = string.Format(@"SELECT d.pro_name as cpr_Name ,
                                (SELECT [dbo].[fun_cm_ProjectCharge](A.cpr_Id))as  acountAdd ,
                                ISNULL(PayShiCount,0) AS SUMAcount,--实收
                                ISNULL((PayShiCount-AllotCount),0) AS WeiFenValue,--为分配金额
                                ISNULL(EconomyValueCount*0.0001,0) AS ToOtherValue,--转经济所
                                ISNULL(HavcCount*0.0001,0) AS HavcCount,--转暖通所
                                ISNULL(tranBuild*0.0001,0) AS TranBulidingCount,--转土建所
                                ISNULL(AllotCount,0) AS ActualValue --本部门实际产值
                                FROM cm_Project D inner join cm_Coperation  A
                                ON D.CoperationSysNo=A.cpr_Id
                                 LEFT JOIN 
                                ( Select pro_ID, SUM(AllotCount) AS AllotCount , SUM(EconomyValueCount) AS EconomyValueCount,SUM(HavcCount) HavcCount,SUM(TranBulidingCount) tranBuild,SUM(UnitValueCount) AS TheDeptValueCount from cm_ProjectValueAllot   WHERE Status='S' and SecondValue='1' group by pro_ID ) B
                                ON D.pro_ID=B.pro_ID
                                LEFT JOIN
                                (Select cprID, SUM(Acount) AS PayShiCount from cm_ProjectCharge WHERE (Status !='B' AND InAcountTime BETWEEN '{1}-01-01' AND '{2}-12-31')  group by cprID) AS C
                                ON A.cpr_Id=C.cprID WHERE d.Unit='{0}' and  PayShiCount>0", unit_name.Trim(), year, year);
                }
                else
                {
                    sql = string.Format(@"SELECT d.pro_name cpr_Name ,
                                (SELECT [dbo].[fun_cm_ProjectCharge](A.cpr_Id))as  acountAdd ,
                                ISNULL(PayShiCount,0) AS SUMAcount,--实收
                                ISNULL((PayShiCount-AllotCount),0) AS WeiFenValue,--为分配金额
                                ISNULL(EconomyValueCount*0.0001,0) AS ToOtherValue,--转经济所
                                ISNULL(HavcCount*0.0001,0) AS HavcCount,--转暖通所
                                ISNULL(tranBuild*0.0001,0) AS TranBulidingCount,--转土建所
                                ISNULL(AllotCount,0) AS ActualValue --本部门实际产值
                                FROM cm_Project D inner join cm_Coperation  A
                                ON D.CoperationSysNo=A.cpr_Id
                                 LEFT JOIN 
                                ( Select pro_ID, SUM(AllotCount) AS AllotCount , SUM(EconomyValueCount) AS EconomyValueCount,SUM(HavcCount) HavcCount,SUM(TranBulidingCount) tranBuild,SUM(UnitValueCount) AS TheDeptValueCount from cm_ProjectValueAllot   WHERE Status='S' and SecondValue='1' group by pro_ID ) B
                                ON D.pro_ID=B.pro_ID
                                LEFT JOIN
                                (Select cprID, SUM(Acount) AS PayShiCount from cm_ProjectCharge WHERE (Status !='B' )  group by cprID) AS C
                                ON A.cpr_Id=C.cprID WHERE d.Unit='{0}' and  PayShiCount>0 ", unit_name.Trim());
                }

            }
            else
            {
                //                sql = string.Format(@"SELECT cpr_Name , (SELECT [dbo].[fun_cm_ProjectCharge](a.cpr_Id))as  acountAdd ,isnull(SUMAcount,0) AS SUMAcount
                //FROM dbo.cm_Coperation AS a
                // LEFT JOIN 
                //(Select cprID, SUM(Acount) AS SUMAcount from cm_ProjectCharge  AS c WHERE c.Status='E'  group by cprID) AS b
                //ON a.cpr_Id=b.cprID WHERE  a.cpr_SignDate BETWEEN '{0}-01-01'AND '{0}-12-31'", year);
                if (year != "-1")
                {
                    sql = string.Format(@"SELECT d.pro_name as cpr_Name ,
                            (SELECT [dbo].[fun_cm_ProjectCharge](A.cpr_Id))as  acountAdd ,
                            ISNULL(PayShiCount,0) AS SUMAcount,--实收
                            ISNULL((PayShiCount-AllotCount),0) AS WeiFenValue,--为分配金额
                            ISNULL(EconomyValueCount*0.0001,0) AS ToOtherValue,--转经济所
                            ISNULL(HavcCount*0.0001,0) AS HavcCount,--转暖通所
                            ISNULL(tranBuild*0.0001,0) AS TranBulidingCount,--转土建所
                            ISNULL(AllotCount,0) AS ActualValue --本部门实际产值
                            FROM cm_Project D inner join cm_Coperation  A
                            ON D.CoperationSysNo=A.cpr_Id
                             LEFT JOIN 
                            ( Select pro_ID, SUM(AllotCount) AS AllotCount , SUM(EconomyValueCount) AS EconomyValueCount,SUM(HavcCount) HavcCount,SUM(TranBulidingCount) tranBuild,SUM(UnitValueCount) AS TheDeptValueCount from cm_ProjectValueAllot   WHERE Status='S' and SecondValue='1' group by pro_ID ) B
                            ON D.pro_ID=B.pro_ID
                            LEFT JOIN
                            (Select cprID, SUM(Acount) AS PayShiCount from cm_ProjectCharge WHERE (Status !='B' AND InAcountTime BETWEEN '{0}-01-01' AND '{1}-12-31')  group by cprID) AS C
                            ON A.cpr_Id=C.cprID  where 1=1 and  PayShiCount>0", year, year);
                }
                else
                {
                    sql = string.Format(@"SELECT d.pro_name as cpr_Name ,
                            (SELECT [dbo].[fun_cm_ProjectCharge](A.cpr_Id))as  acountAdd ,
                            ISNULL(PayShiCount,0) AS SUMAcount,--实收
                            ISNULL((PayShiCount-AllotCount),0) AS WeiFenValue,--为分配金额
                            ISNULL(EconomyValueCount*0.0001,0) AS ToOtherValue,--转经济所
                            ISNULL(HavcCount*0.0001,0) AS HavcCount,--转暖通所
                            ISNULL(tranBuild*0.0001,0) AS TranBulidingCount,--转土建所
                            ISNULL(AllotCount,0) AS ActualValue --实际产值
                            FROM cm_Project D inner join cm_Coperation  A	
                            ON D.CoperationSysNo=A.cpr_Id
                             LEFT JOIN 
                            ( Select pro_ID, SUM(AllotCount) AS AllotCount , SUM(EconomyValueCount) AS EconomyValueCount,SUM(HavcCount) HavcCount,SUM(TranBulidingCount) tranBuild,SUM(UnitValueCount) AS TheDeptValueCount from cm_ProjectValueAllot   WHERE Status='S' and SecondValue='1' group by pro_ID ) B
                            ON D.pro_ID=B.pro_ID
                            LEFT JOIN
                            (Select cprID, SUM(Acount) AS PayShiCount from cm_ProjectCharge WHERE (Status !='B')  group by cprID) AS C
                            ON A.cpr_Id=C.cprID where 1=1 and  PayShiCount>0");
                }

            }


            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            List<TG.Model.Outsummary> resultList = new List<Model.Outsummary>();
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                TG.Model.Outsummary model = new Model.Outsummary();
                model.CprName = item["cpr_Name"].ToString().Trim();
                model.Charge = Convert.ToDecimal(item["SUMAcount"].ToString().Trim());
                model.Chargelist = item["acountAdd"].ToString().Trim();
                model.Mark = "";
                model.ToOtherValue = Convert.ToDecimal(item["ToOtherValue"].ToString().Trim());
                model.WeiFenValue = Convert.ToDecimal(item["WeiFenValue"].ToString().Trim());
                model.ActualValue = Convert.ToDecimal(item["ActualValue"].ToString().Trim());
                model.HavcCount = Convert.ToDecimal(item["HavcCount"].ToString().Trim());
                model.TranBuild = Convert.ToDecimal(item["TranBulidingCount"].ToString().Trim());
                resultList.Add(model);
            }
            return resultList;
        }
        public List<ChargeDate> GetListChargeDate(string unit_name, string year)
        {
            string sql = "";
            if (unit_name != "-1")
            {
                sql = @"select cpr_Id,cpr_Name,
(SELECT dbo.fun_cm_ProjectCharge (cpr_Id))acountAdd,
(select case cs when 1 then '*' when 2  then '**' when 3 then '***' when 4 then '****' when 5 then '*****' end from (select COUNT(cprID) as cs from cm_ProjectCharge where cprID=cpr_Id and Status!='B' ) as temp) as xx,
(select sum(Acount) from cm_ProjectCharge where cprID=cpr_Id and Status!='B') as Acount,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id and Status!='B' order by InAcountTime asc) as FristTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id and Status!='B' and ID not in (select top 1 ID from cm_ProjectCharge where cprID=x.cprID  order by InAcountTime asc)) as SecondTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id and Status!='B' and ID not in (select top 2 ID from cm_ProjectCharge where cprID=x.cprID order by InAcountTime asc)) as ThirdTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id  and Status!='B' and ID not in (select top 3 ID from cm_ProjectCharge where cprID=x.cprID  order by InAcountTime asc)) as FourthTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id  and Status!='B' and ID not in (select top 4 ID from cm_ProjectCharge where cprID=x.cprID order by InAcountTime asc)) as FifthTime,
TableMaker AS Mark
from cm_Coperation p join cm_ProjectCharge pc on p.cpr_Id=pc.cprID where cpr_Unit='{0}' and YEAR(InAcountTime)={1}  and Status!='B' group by cpr_Id,cpr_Name,TableMaker
";
                sql = string.Format(sql, unit_name, year);
            }
            else
            {
                sql = @"select cpr_Id,cpr_Name,
(SELECT dbo.fun_cm_ProjectCharge (cpr_Id))acountAdd,
(select case cs when 1 then '*' when 2  then '**' when 3 then '***' when 4 then '****' when 5 then '*****' end from (select COUNT(cprID) as cs from cm_ProjectCharge where cprID=cpr_Id and Status!='B' ) as temp) as xx,
(select sum(Acount) from cm_ProjectCharge where cprID=cpr_Id and Status!='B') as Acount,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id and Status!='B' order by InAcountTime asc) as FristTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id and Status!='B' and ID not in (select top 1 ID from cm_ProjectCharge where cprID=x.cprID  order by InAcountTime asc)) as SecondTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id and Status!='B' and ID not in (select top 2 ID from cm_ProjectCharge where cprID=x.cprID order by InAcountTime asc)) as ThirdTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id  and Status!='B' and ID not in (select top 3 ID from cm_ProjectCharge where cprID=x.cprID  order by InAcountTime asc)) as FourthTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id  and Status!='B' and ID not in (select top 4 ID from cm_ProjectCharge where cprID=x.cprID order by InAcountTime asc)) as FifthTime,
TableMaker AS Mark
from cm_Coperation p join cm_ProjectCharge pc on p.cpr_Id=pc.cprID where YEAR(InAcountTime)={0}   and Status!='B' group by cpr_Id,cpr_Name,TableMaker
";
                sql = string.Format(sql, year);
            }


            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ChargeDate> resultList = EntityBuilder<ChargeDate>.BuilderEntityList(reader);
            return resultList;
        }
        public List<ChargeDate> GetListChargeDate(string unit_name, string year, string cprName)
        {
            string sql = "";
            if (unit_name != "-1")
            {
                sql = @"select cpr_Id,cpr_Name,
(SELECT dbo.fun_cm_ProjectCharge (cpr_Id))acountAdd,
(select case cs when 1 then '*' when 2  then '**' when 3 then '***' when 4 then '****' when 5 then '*****' end from (select COUNT(cprID) as cs from cm_ProjectCharge where cprID=cpr_Id and Status!='B' ) as temp) as xx,
(select sum(Acount) from cm_ProjectCharge where cprID=cpr_Id and Status!='B') as Acount,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id and Status!='B' order by InAcountTime asc) as FristTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id and Status!='B' and ID not in (select top 1 ID from cm_ProjectCharge where cprID=x.cprID  order by InAcountTime asc)) as SecondTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id and Status!='B' and ID not in (select top 2 ID from cm_ProjectCharge where cprID=x.cprID order by InAcountTime asc)) as ThirdTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id  and Status!='B' and ID not in (select top 3 ID from cm_ProjectCharge where cprID=x.cprID  order by InAcountTime asc)) as FourthTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id  and Status!='B' and ID not in (select top 4 ID from cm_ProjectCharge where cprID=x.cprID order by InAcountTime asc)) as FifthTime,
TableMaker AS Mark
from cm_Coperation p join cm_ProjectCharge pc on p.cpr_Id=pc.cprID where cpr_Unit='{0}' and cpr_Name like '%{2}%'and YEAR(InAcountTime)={1}  and Status!='B' group by cpr_Id,cpr_Name,TableMaker
";
                sql = string.Format(sql, unit_name, year, cprName);
            }
            else
            {
                sql = @"select cpr_Id,cpr_Name,
(SELECT dbo.fun_cm_ProjectCharge (cpr_Id))acountAdd,
(select case cs when 1 then '*' when 2  then '**' when 3 then '***' when 4 then '****' when 5 then '*****' end from (select COUNT(cprID) as cs from cm_ProjectCharge where cprID=cpr_Id and Status!='B' ) as temp) as xx,
(select sum(Acount) from cm_ProjectCharge where cprID=cpr_Id and Status!='B') as Acount,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id and Status!='B' order by InAcountTime asc) as FristTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id and Status!='B' and ID not in (select top 1 ID from cm_ProjectCharge where cprID=x.cprID  order by InAcountTime asc)) as SecondTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id and Status!='B' and ID not in (select top 2 ID from cm_ProjectCharge where cprID=x.cprID order by InAcountTime asc)) as ThirdTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id  and Status!='B' and ID not in (select top 3 ID from cm_ProjectCharge where cprID=x.cprID  order by InAcountTime asc)) as FourthTime,
(select top 1 convert(NVARCHAR, InAcountTime,102) from cm_ProjectCharge x where cprID=p.cpr_Id  and Status!='B' and ID not in (select top 4 ID from cm_ProjectCharge where cprID=x.cprID order by InAcountTime asc)) as FifthTime,
TableMaker AS Mark
from cm_Coperation p join cm_ProjectCharge pc on p.cpr_Id=pc.cprID where cpr_Name like '%{1}%'and YEAR(InAcountTime)={0}   and Status!='B' group by cpr_Id,cpr_Name,TableMaker
";
                sql = string.Format(sql, year, cprName);
            }


            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ChargeDate> resultList = EntityBuilder<ChargeDate>.BuilderEntityList(reader);
            return resultList;
        }
        /// <summary>
        /// 部门产值分配统计大表存储过程
        /// </summary>
        public List<TG.Model.ProjectValueAllot> GetMemberProjectValueAllotProc(string query, string query1, string query2)
        {
            SqlDataReader reader = DBUtility.DbHelperSQL.RunProcedure("P_cm_MemberProjectValueAllot", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) });
            List<TG.Model.ProjectValueAllot> resultList = EntityBuilder<TG.Model.ProjectValueAllot>.BuilderEntityList(reader);

            return resultList;
        }
        /// <summary>
        /// 部门产值分配所有项目统计全人员表存储过程
        /// </summary>
        /// <param name="unitname"></param>
        /// <param name="unitid"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable GetMemberProjectValueAllotDetailProc(string unitname, int unitid, string year)
        {
            DataTable dt = DBUtility.DbHelperSQL.RunProcedure("P_cm_MemberProjectValueAllotDetail", new SqlParameter[] { new SqlParameter("@unitname", unitname), new SqlParameter("@unitid", unitid), new SqlParameter("@year", year) }, "MemberProjectValueAllotDetail").Tables["MemberProjectValueAllotDetail"];
            return dt;
        }
        /// <summary>
        /// 部门产值表某个项目取得某个人员的产值   暂无用此方法
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public decimal GetMemberProjectValueAllotSql(string param0, string param1, string param2, string year)
        {
            string where1 = "";
            string where2 = "";
            string where3 = "";
            string sqlwhere = "";
            if (param2 != "")
            {
                where1 = " and pvbd.proID=" + param2;
                where2 = " and pro_ID=" + param2;
                where3 = " and b.Pro_ID=" + param2;
            }
            if (year != "" && year != "-1")
            {
                where1 = where1 + " and pva.ActualAllountTime='" + year + "'";
                where2 = where2 + " and ActualAllountTime='" + year + "'";
                where3 = where3 + " and b.ActualAllountTime='" + year + "'";
            }
            if (param0 == "1")
            {
                sqlwhere = @" union all			         
            			   select SUM(b.DesignManagerCount),aa.mem_ID from tg_relation aa left join tg_member e on aa.mem_ID=e.mem_ID left join tg_project c on aa.pro_ID=c.pro_ID left join
            					cm_Project d on c.pro_ID=d.ReferenceSysNo left join cm_TranjjsProjectValueAllot b on d.pro_ID=b.Pro_ID 
            					where  b.[Status]='S' and 
            					e.mem_Speciality_ID=(select spe_ID from tg_speciality where spe_Name='预算') and
            					charindex('2',aa.mem_RoleIDs)>0 and aa.mem_ID=" + param1 + " " + where3 + " group by b.Pro_ID,aa.mem_ID ";
            }
            string sql = @"select ISNULL(SUM(totalcount),0) as totalcount from (
                        select isnull(sum(DesignCount+SpecialtyHeadCount+AuditCount+ProofreadCount),0) totalcount,mem_ID from cm_ProjectValueByMemberDetails pvbd left join cm_ProjectValueAllot pva
			            on pvbd.AllotID=pva.ID
                        where pvbd.[Status]='S' and pvbd.SecondValue='{0}' and mem_ID={1} {2} group by mem_ID
                        union all
                        select sjcount,a.PMUserID from cm_Project a right join ( 
                        select isnull(SUM(DesignManagerCount),0) as sjcount,pro_ID from cm_ProjectValueAllot
                        where [Status]='S' and SecondValue='{0}' {3} group by pro_ID) b on a.pro_ID=b.pro_ID where a.PMUserID={1} {4} ) pp";
            sql = string.Format(sql, param0, param1, where1, where2, sqlwhere);

            object o = DbHelperSQL.GetSingle(sql);

            return o == null ? 0 : Convert.ToDecimal(o);
        }
        /// <summary>
        /// 每个人所补的产值
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public decimal GetMemberProjectComValue(string query)
        {
            object o = DbHelperSQL.GetSingle("select isnull(sum(ComValue),0.00) from cm_CompensationSet where 1=1 " + query + "");
            return o == null ? 0 : Convert.ToDecimal(o);
        }
        /// <summary>
        /// 项目产值分配明细表存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, string query1, string query2, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_ProjectValueAllot", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) });
        }
        /// <summary>
        /// 获取项目产值分配明细表需要分页的总条数
        /// </summary>
        public object GetListPageProcCount(string query, string query1, string query2)
        {
            object obj = "0";
            SqlDataReader sqr = DBUtility.DbHelperSQL.RunProcedure("P_cm_ProjectValueAllot_Count", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) });

            if (sqr != null && sqr.HasRows)
            {
                while (sqr.Read())
                {
                    obj = sqr.GetValue(0);
                }
            }
            return obj;
        }

        /// <summary>
        /// 获取产值分配表需要字段数据总和
        /// </summary>
        public SqlDataReader GetListPageProcSum(string query, string query1, string query2)
        {

            return DBUtility.DbHelperSQL.RunProcedure("P_cm_ProjectValueAllot_Count", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) });

        }

        /// <summary>
        /// //某个项目的所有产值人员
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public DataTable GetMemberProjectValueAllot(int pro_id, string year, string sqlwhere)
        {
            #region 暂无用

            //             string sql = @"select  a.mem_ID,(case when IsExternal='0' then (select mem_Name from tg_member where mem_ID=a.mem_ID) else (select mem_Name from cm_externalMember where mem_ID=a.mem_ID) end) as mem_Name,
            //            isnull(sum(a.totalCount),0) as totalCount from 
            //            (
            //                select b.mem_ID,b.IsExternal,sum(cast(round((DesignCount+SpecialtyHeadCount+AuditCount+ProofreadCount),0) as int)) as totalCount from cm_ProjectValueByMemberDetails b 
            //                     left join  cm_Project f on f.pro_ID=b.ProID  
            //                     where f.pro_ID={0} and b.[Status]='S' group by b.mem_ID,b.IsExternal
            //                union all	
            //                select (select PMUserID from cm_Project where pro_ID=pv.pro_ID),'0',SUM(cast(round(DesignManagerCount,0) as decimal(18,2))) from cm_ProjectValueAllot pv where pro_ID={0} and [Status]='S' group by pro_ID
            //               union all			         
            //			   select aa.mem_ID,'0',SUM(b.DesignManagerCount) from tg_relation aa left join tg_member e on aa.mem_ID=e.mem_ID left join tg_project c on aa.pro_ID=c.pro_ID left join
            //					cm_Project d on c.pro_ID=d.ReferenceSysNo left join cm_TranjjsProjectValueAllot b on d.pro_ID=b.Pro_ID 
            //					where  b.[Status]='S' and 
            //					e.mem_Speciality_ID=(select spe_ID from tg_speciality where spe_Name='预算') and
            //					charindex('2',aa.mem_RoleIDs)>0 and b.Pro_ID={0} 
            //					group by b.Pro_ID,aa.mem_ID
            //            ) a  group by a.mem_ID,a.IsExternal having isnull(sum(a.totalCount),0)<>0 {1} order by a.mem_ID desc";
            //            sql = string.Format(sql, pro_id, sqlwhere);
            //            DataTable dt = DbHelperSQL.Query(sql).Tables[0];
            #endregion
            DataTable dt = DBUtility.DbHelperSQL.RunProcedure("P_cm_ProjectAllMemberValue", new SqlParameter[] { new SqlParameter("@pro_id", pro_id), new SqlParameter("@year", year), new SqlParameter("@query", sqlwhere) }, "ProjectAllMemberValue").Tables["ProjectAllMemberValue"];
            return dt;
        }
        /// <summary>
        /// 项目产值分配表导出
        /// </summary>
        /// <returns></returns>
        public DataTable GetExcelProjectValueAllot(string query, string query1, string query2)
        {
            #region 暂无用

            //            string sql = @"select p.pro_ID as CprId,p.pro_name as CprName,isnull(sscz,0) as Charge,(isnull(zjjs,0)*0.0001) as EconomyValue,(isnull(znts,0)*0.0001) as HavcValue,(isnull(ztjs,0)*0.0001) as TranBulidingValue,(isnull(bscz,0)*0.0001) as UnitValue,(isnull(sl,0)*0.0001) as TheDeptValue from
            //                    cm_project p 
            //					left join cm_Coperation a on p.CoperationSysNo=a.cpr_Id
            //					left join 
            //					(select cprID,isnull(SUM(Acount),0) as sscz from cm_ProjectCharge where ([Status]='C' or [Status]='E') group by cprID) b 
            //						on a.cpr_Id=b.cprID 
            //					left join 
            //					(select pro_ID,sum(HavcCount) as znts,sum(EconomyValueCount) as zjjs,sum(TranBulidingCount) as ztjs,sum(UnitValueCount) as bscz,SUM(TheDeptValueCount) AS sl  from cm_ProjectValueAllot where [Status]='S' group by pro_ID) c
            //						on p.pro_ID=c.pro_ID  where 1=1 " + query;

            //            return DBUtility.DbHelperSQL.Query(sql).Tables[0];
            #endregion
            DataTable dt = DBUtility.DbHelperSQL.RunProcedure("P_cm_ProjectValueAllot_Export", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) }, "ProjectValueAllot_Export").Tables["ProjectValueAllot_Export"];
            return dt;
        }
        /// <summary>
        /// 个人产值分配表(一)分页存储过程
        /// </summary>
        public SqlDataReader GetMemberValueAllotProc(string query, string query1, string query2, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_MembeValueAllot", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) });
        }
        /// <summary>
        /// 获取个人产值分配表(一)需要分页的数据总数
        /// </summary>
        public object GetMemberValueAllotCount(string query, string query1, string query2)
        {
            // return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_MembeValueAllot_count", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) });

            object obj = "0";
            SqlDataReader sqr = DBUtility.DbHelperSQL.RunProcedure("P_cm_MembeValueAllot_count", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) });

            if (sqr != null && sqr.HasRows)
            {
                while (sqr.Read())
                {
                    obj = sqr.GetValue(0);
                }
            }
            return obj;
        }
        /// <summary>
        /// 获取个人产值分配表(一)需要字段数据总和
        /// </summary>
        public SqlDataReader GetMemberValueAllotSum(string query, string query1, string query2)
        {

            return DBUtility.DbHelperSQL.RunProcedure("P_cm_MembeValueAllot_count", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) });

        }
        /// <summary>
        /// 个人产值分配表(一)导出
        /// </summary>
        /// <returns></returns>
        public DataTable GetExcelMemberValueAllot(string query, string query1, string query2)
        {
            #region 暂无用
            //            string sql = @"select me.mem_ID,me.mem_Name,isnull(cz.totalCount,0) as totalcount,isnull(cs.ComValue,0) as sbcount  from tg_member me 
            //				     left join (select SUM(totalCount) as totalCount,mem_ID from (  
            //				    select a.mem_ID,sum(cast(round((DesignCount+SpecialtyHeadCount+AuditCount+ProofreadCount),0) as decimal(18,2))) as totalCount
            //			        from cm_ProjectValueByMemberDetails a left join cm_ProjectValueAllot b on a.AllotID=b.ID where a.[Status]='S' and a.IsExternal='0' {2}  group by a.mem_ID
            //			        union all
            //					select pj.PMUserID,szcount from cm_project pj right join
            //			        (select pro_ID,SUM(DesignManagerCount) as szcount from cm_ProjectValueAllot b where [Status]='S' {2} group by pro_ID) pp 
            //			         on pp.pro_ID=pj.pro_ID
            //                     union all			         
            //			         select a.mem_ID,SUM(b.DesignManagerCount) from tg_relation a left join tg_member e on a.mem_ID=e.mem_ID left join tg_project c on a.pro_ID=c.pro_ID left join
            //					cm_Project d on c.pro_ID=d.ReferenceSysNo left join cm_TranjjsProjectValueAllot b on d.pro_ID=b.Pro_ID 
            //					where  b.[Status]='S' and 
            //					e.mem_Speciality_ID=(select spe_ID from tg_speciality where spe_Name='预算') and
            //					charindex('2',a.mem_RoleIDs)>0 {2} 
            //					group by b.Pro_ID,a.mem_ID	
            //                ) ppj group by mem_ID) cz on me.mem_ID=cz.mem_ID left join cm_CompensationSet cs on cs.MemberId=me.mem_ID {1}  where 1=1  {0}";

            //   sql = string.Format(sql, query, query1, query2);
            //   return DBUtility.DbHelperSQL.Query(sql).Tables[0];
            #endregion

            DataTable dt = DBUtility.DbHelperSQL.RunProcedure("P_cm_MembeValueAllot_Export", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) }, "MembeValueAllot_Export").Tables["MembeValueAllot_Export"];
            return dt;
        }

        /// <summary>
        /// 个人产值分配表(二)分页存储过程2
        /// </summary>
        public SqlDataReader GetMemberValueAllotProc2(string query, string query1, string query2, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_MembeValueAllot2", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) });
        }
        /// <summary>
        /// 获取个人产值分配表(二)需要分页的总条数2
        /// </summary>
        public object GetMemberValueAllotCount2(string query, string query1, string query2)
        {
            // return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_MembeValueAllot_count2", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) });
            object obj = "0";
            SqlDataReader sqr = DBUtility.DbHelperSQL.RunProcedure("P_cm_MembeValueAllot_count2", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) });

            if (sqr != null && sqr.HasRows)
            {
                while (sqr.Read())
                {
                    obj = sqr.GetValue(0);
                }
            }
            return obj;
        }

        /// <summary>
        /// 获取个人产值分配表(二)需要字段数据总和2
        /// </summary>
        public SqlDataReader GetMemberValueAllotSum2(string query, string query1, string query2)
        {

            return DBUtility.DbHelperSQL.RunProcedure("P_cm_MembeValueAllot_count2", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", query2) });

        }
        /// <summary>
        /// 个人产值分配表(二)导出
        /// </summary>
        /// <returns></returns>
        public DataTable GetExcelMemberValueAllot2(string query, string query1, string query2)
        {
            DataTable dt = DBUtility.DbHelperSQL.RunProcedure("P_cm_MembeValueAllot2_Export", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@query1", query1), new SqlParameter("@query2", "") }, "MembeValueAllot2_Export").Tables["MembeValueAllot2_Export"];

            return dt;
        }

        /// <summary>
        /// 个人产值明细查询点击查看
        /// </summary>
        /// <returns></returns>
        public DataSet GetMemberProjectDetail(int memberid, string year)
        {
            DataSet set = DBUtility.DbHelperSQL.RunProcedure("P_cm_MemberProjectValueDetail", new SqlParameter[] { new SqlParameter("@memberid", memberid), new SqlParameter("@year", year) }, "MemberProjectValueDetail");
            return set;
        }
        /// <summary>
        /// 方案补贴的未补贴总产值 zxq 20131226
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitname"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public object GetRemainAllotValue(string year, string unitname, int unitid)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_RemainAllotValue", new SqlParameter[] { new SqlParameter("@year", year), new SqlParameter("@unitname", unitname), new SqlParameter("@unitid", unitid) });
        }

    }
}
