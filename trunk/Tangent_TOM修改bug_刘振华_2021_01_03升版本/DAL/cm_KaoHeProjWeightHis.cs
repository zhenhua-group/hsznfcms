﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeProjWeightHis
	/// </summary>
	public partial class cm_KaoHeProjWeightHis
	{
		public cm_KaoHeProjWeightHis()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeProjWeightHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeProjWeightHis(");
			strSql.Append("RenwuID,RenwuName,WeightName,JZ,JG,SB,DQ,Stat,ProjCount)");
			strSql.Append(" values (");
			strSql.Append("@RenwuID,@RenwuName,@WeightName,@JZ,@JG,@SB,@DQ,@Stat,@ProjCount)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@RenwuName", SqlDbType.VarChar,50),
					new SqlParameter("@WeightName", SqlDbType.VarChar,50),
					new SqlParameter("@JZ", SqlDbType.Decimal,9),
					new SqlParameter("@JG", SqlDbType.Decimal,9),
					new SqlParameter("@SB", SqlDbType.Decimal,9),
					new SqlParameter("@DQ", SqlDbType.Decimal,9),
					new SqlParameter("@Stat", SqlDbType.Int,4),
					new SqlParameter("@ProjCount", SqlDbType.Decimal,9)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.RenwuName;
			parameters[2].Value = model.WeightName;
			parameters[3].Value = model.JZ;
			parameters[4].Value = model.JG;
			parameters[5].Value = model.SB;
			parameters[6].Value = model.DQ;
			parameters[7].Value = model.Stat;
			parameters[8].Value = model.ProjCount;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeProjWeightHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeProjWeightHis set ");
			strSql.Append("RenwuID=@RenwuID,");
			strSql.Append("RenwuName=@RenwuName,");
			strSql.Append("WeightName=@WeightName,");
			strSql.Append("JZ=@JZ,");
			strSql.Append("JG=@JG,");
			strSql.Append("SB=@SB,");
			strSql.Append("DQ=@DQ,");
			strSql.Append("Stat=@Stat,");
			strSql.Append("ProjCount=@ProjCount");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@RenwuName", SqlDbType.VarChar,50),
					new SqlParameter("@WeightName", SqlDbType.VarChar,50),
					new SqlParameter("@JZ", SqlDbType.Decimal,9),
					new SqlParameter("@JG", SqlDbType.Decimal,9),
					new SqlParameter("@SB", SqlDbType.Decimal,9),
					new SqlParameter("@DQ", SqlDbType.Decimal,9),
					new SqlParameter("@Stat", SqlDbType.Int,4),
					new SqlParameter("@ProjCount", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.RenwuName;
			parameters[2].Value = model.WeightName;
			parameters[3].Value = model.JZ;
			parameters[4].Value = model.JG;
			parameters[5].Value = model.SB;
			parameters[6].Value = model.DQ;
			parameters[7].Value = model.Stat;
			parameters[8].Value = model.ProjCount;
			parameters[9].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeProjWeightHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeProjWeightHis ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeProjWeightHis GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,RenwuID,RenwuName,WeightName,JZ,JG,SB,DQ,Stat,ProjCount from cm_KaoHeProjWeightHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeProjWeightHis model=new TG.Model.cm_KaoHeProjWeightHis();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeProjWeightHis DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeProjWeightHis model=new TG.Model.cm_KaoHeProjWeightHis();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["RenwuID"]!=null && row["RenwuID"].ToString()!="")
				{
					model.RenwuID=int.Parse(row["RenwuID"].ToString());
				}
				if(row["RenwuName"]!=null)
				{
					model.RenwuName=row["RenwuName"].ToString();
				}
				if(row["WeightName"]!=null)
				{
					model.WeightName=row["WeightName"].ToString();
				}
				if(row["JZ"]!=null && row["JZ"].ToString()!="")
				{
					model.JZ=decimal.Parse(row["JZ"].ToString());
				}
				if(row["JG"]!=null && row["JG"].ToString()!="")
				{
					model.JG=decimal.Parse(row["JG"].ToString());
				}
				if(row["SB"]!=null && row["SB"].ToString()!="")
				{
					model.SB=decimal.Parse(row["SB"].ToString());
				}
				if(row["DQ"]!=null && row["DQ"].ToString()!="")
				{
					model.DQ=decimal.Parse(row["DQ"].ToString());
				}
				if(row["Stat"]!=null && row["Stat"].ToString()!="")
				{
					model.Stat=int.Parse(row["Stat"].ToString());
				}
				if(row["ProjCount"]!=null && row["ProjCount"].ToString()!="")
				{
					model.ProjCount=decimal.Parse(row["ProjCount"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,RenwuID,RenwuName,WeightName,JZ,JG,SB,DQ,Stat,ProjCount ");
			strSql.Append(" FROM cm_KaoHeProjWeightHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,RenwuID,RenwuName,WeightName,JZ,JG,SB,DQ,Stat,ProjCount ");
			strSql.Append(" FROM cm_KaoHeProjWeightHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeProjWeightHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeProjWeightHis T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeProjWeightHis";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

