﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;
using TG.DBUtility;
using TG.Model;

namespace TG.DAL
{
    public class RolePowerDA
    {
        public int InsertRolePowerViewEntity(RolePowerViewEntity entity)
        {
            string sql = string.Format("insert into cm_RolePower(RoleSysNo,ActionPowerSysNo,InUser,Power)values({0},{1},{2},N'{3}');select @@Identity", entity.RoleSysNo, entity.ActionPowerSysNo, entity.InUser, entity.Power);

            int count = Convert.ToInt32(DbHelperSQL.GetSingle(sql));

            return count;
        }

        public int UpDateRolePowerViewEntity(RolePowerViewEntity entity)
        {
            string sql = "Update cm_RolePower set RoleSysNo=" + entity.RoleSysNo + ",ActionPowerSysNo=" + entity.ActionPowerSysNo + ",InUser=" + entity.InUser + " ,Power =N'" + entity.Power + "' where SysNo=" + entity.SysNo;

            int count = DbHelperSQL.ExecuteSql(sql);

            return count;
        }

        public bool BatchInsertRolePowerViewEntity(List<RolePowerViewEntity> entityList)
        {
            bool flag = true;
            for (int i = 0; i < entityList.Count; i++)
            {
                try
                {
                    int identitySysNo = InsertRolePowerViewEntity(entityList[i]);
                    if (identitySysNo <= 0)
                        flag = false;
                }
                catch (Exception ex)
                {
                    continue;
                }
            }
            return flag;
        }

        public bool IsExists(RolePowerViewEntity entity)
        {
            string sql = string.Format("select top 1 1 from cm_RolePower where RoleSysNo=" + entity.RoleSysNo + " and ActionPowerSysNo=" + entity.ActionPowerSysNo);

            object objResult = DbHelperSQL.GetSingle(sql);

            return objResult == null ? false : true;
        }

        public List<RolePowerListViewEntity> GetRolePowerListViewEntity(int roleSysNo)
        {
            string isExistsSql = "select top 1 1 from cm_RolePower where RoleSysNo=" + roleSysNo;

            object objResult = DbHelperSQL.GetSingle(isExistsSql);

            string selectSql = "";
            if (objResult == null)
            {
                //没有查到的场合，单表查询
                selectSql = "select ap.Description,ap.PageName,0 as RolePowerSysNo,ap.SysNo as ActionPowerSysNo, N'0,1,1' as Power from cm_ActionPower ap ";
            }
            else
            {
                //查询到的场合，连表查询 20130507 by qpl
                selectSql = string.Format(@"select case  when rp.RoleSysNo is NULL then '0' else rp.RoleSysNo end as RoleSysNo,
                                                                        ap.Description,ap.PageName,
                                                                        case when rp.SysNo IS NULL then 0 else rp.SysNo end as RolePowerSysNo,
                                                                        case when rp.ActionPowerSysNo is Null then (Select SysNo From cm_ActionPower Where InDate =ap.InDate) else rp.ActionPowerSysNo end as ActionPowerSysNo,
                                                                        case when rp.Power is null then '0,1,1' else rp.Power end as Power
                                                                        From cm_ActionPower ap left join cm_RolePower rp on rp.ActionPowerSysNo = ap.SysNo AND rp.RoleSysNo={0}", roleSysNo);
            }

            SqlDataReader reader = DbHelperSQL.ExecuteReader(selectSql);

            List<RolePowerListViewEntity> resultList = EntityBuilder<RolePowerListViewEntity>.BuilderEntityList(reader);

            return resultList;
        }
        //查看当前用户对本页面的查看，编辑权限
        public List<RolePowerParameterEntity> GetRolePowerViewEntityList(int userSysNo, string pageName)
        {
            //查询出所有的Role
            List<TG.Model.cm_Role> roleList = new cm_Role().GetRoleList(userSysNo);
            string roleString = string.Empty;
            if (roleList.Count > 0)
            {
                roleList.ForEach(role => roleString += role.SysNo + ",");
                roleString = roleString.Substring(0, roleString.Length - 1);
            }
            else
            {
                //如果没有给用户分配角色，就采用默认普通员工权限
                string defaultSql = string.Format("select SysNo from cm_Role where RoleName =N'{0}'", "普通员工");
                roleString = Convert.ToInt32(DbHelperSQL.GetSingle(defaultSql)).ToString();
            }

            string sql = "select rp.Power from cm_RolePower rp join cm_ActionPower ap on ap.SysNo = rp.ActionPowerSysNo where PageName=N'" + pageName + "' and RoleSysNo in(" + roleString + ")";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<RolePowerParameterEntity> resultList = EntityBuilder<RolePowerParameterEntity>.BuilderEntityList(reader);

            return resultList;
        }
    }
}
