﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeMemsAllotChange
	/// </summary>
	public partial class cm_KaoHeMemsAllotChange
	{
		public cm_KaoHeMemsAllotChange()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeMemsAllotChange model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeMemsAllotChange(");
			strSql.Append("RenwuID,IsFired,UnitID,UnitName,SpeID,SpeName,MemID,MemName,AllotCount,UnitOrder,UnitJudgeOrder,AllotCountBefore,Stat)");
			strSql.Append(" values (");
			strSql.Append("@RenwuID,@IsFired,@UnitID,@UnitName,@SpeID,@SpeName,@MemID,@MemName,@AllotCount,@UnitOrder,@UnitJudgeOrder,@AllotCountBefore,@Stat)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@IsFired", SqlDbType.Int,4),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.NVarChar,50),
					new SqlParameter("@SpeID", SqlDbType.Int,4),
					new SqlParameter("@SpeName", SqlDbType.NVarChar,50),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,9),
					new SqlParameter("@UnitOrder", SqlDbType.Int,4),
					new SqlParameter("@UnitJudgeOrder", SqlDbType.Int,4),
					new SqlParameter("@AllotCountBefore", SqlDbType.Decimal,9),
					new SqlParameter("@Stat", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.IsFired;
			parameters[2].Value = model.UnitID;
			parameters[3].Value = model.UnitName;
			parameters[4].Value = model.SpeID;
			parameters[5].Value = model.SpeName;
			parameters[6].Value = model.MemID;
			parameters[7].Value = model.MemName;
			parameters[8].Value = model.AllotCount;
			parameters[9].Value = model.UnitOrder;
			parameters[10].Value = model.UnitJudgeOrder;
			parameters[11].Value = model.AllotCountBefore;
			parameters[12].Value = model.Stat;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeMemsAllotChange model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeMemsAllotChange set ");
			strSql.Append("RenwuID=@RenwuID,");
			strSql.Append("IsFired=@IsFired,");
			strSql.Append("UnitID=@UnitID,");
			strSql.Append("UnitName=@UnitName,");
			strSql.Append("SpeID=@SpeID,");
			strSql.Append("SpeName=@SpeName,");
			strSql.Append("MemID=@MemID,");
			strSql.Append("MemName=@MemName,");
			strSql.Append("AllotCount=@AllotCount,");
			strSql.Append("UnitOrder=@UnitOrder,");
			strSql.Append("UnitJudgeOrder=@UnitJudgeOrder,");
			strSql.Append("AllotCountBefore=@AllotCountBefore,");
			strSql.Append("Stat=@Stat");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@IsFired", SqlDbType.Int,4),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.NVarChar,50),
					new SqlParameter("@SpeID", SqlDbType.Int,4),
					new SqlParameter("@SpeName", SqlDbType.NVarChar,50),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,9),
					new SqlParameter("@UnitOrder", SqlDbType.Int,4),
					new SqlParameter("@UnitJudgeOrder", SqlDbType.Int,4),
					new SqlParameter("@AllotCountBefore", SqlDbType.Decimal,9),
					new SqlParameter("@Stat", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.IsFired;
			parameters[2].Value = model.UnitID;
			parameters[3].Value = model.UnitName;
			parameters[4].Value = model.SpeID;
			parameters[5].Value = model.SpeName;
			parameters[6].Value = model.MemID;
			parameters[7].Value = model.MemName;
			parameters[8].Value = model.AllotCount;
			parameters[9].Value = model.UnitOrder;
			parameters[10].Value = model.UnitJudgeOrder;
			parameters[11].Value = model.AllotCountBefore;
			parameters[12].Value = model.Stat;
			parameters[13].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsAllotChange ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsAllotChange ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeMemsAllotChange GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,RenwuID,IsFired,UnitID,UnitName,SpeID,SpeName,MemID,MemName,AllotCount,UnitOrder,UnitJudgeOrder,AllotCountBefore,Stat from cm_KaoHeMemsAllotChange ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeMemsAllotChange model=new TG.Model.cm_KaoHeMemsAllotChange();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeMemsAllotChange DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeMemsAllotChange model=new TG.Model.cm_KaoHeMemsAllotChange();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["RenwuID"]!=null && row["RenwuID"].ToString()!="")
				{
					model.RenwuID=int.Parse(row["RenwuID"].ToString());
				}
				if(row["IsFired"]!=null && row["IsFired"].ToString()!="")
				{
					model.IsFired=int.Parse(row["IsFired"].ToString());
				}
				if(row["UnitID"]!=null && row["UnitID"].ToString()!="")
				{
					model.UnitID=int.Parse(row["UnitID"].ToString());
				}
				if(row["UnitName"]!=null)
				{
					model.UnitName=row["UnitName"].ToString();
				}
				if(row["SpeID"]!=null && row["SpeID"].ToString()!="")
				{
					model.SpeID=int.Parse(row["SpeID"].ToString());
				}
				if(row["SpeName"]!=null)
				{
					model.SpeName=row["SpeName"].ToString();
				}
				if(row["MemID"]!=null && row["MemID"].ToString()!="")
				{
					model.MemID=int.Parse(row["MemID"].ToString());
				}
				if(row["MemName"]!=null)
				{
					model.MemName=row["MemName"].ToString();
				}
				if(row["AllotCount"]!=null && row["AllotCount"].ToString()!="")
				{
					model.AllotCount=decimal.Parse(row["AllotCount"].ToString());
				}
				if(row["UnitOrder"]!=null && row["UnitOrder"].ToString()!="")
				{
					model.UnitOrder=int.Parse(row["UnitOrder"].ToString());
				}
				if(row["UnitJudgeOrder"]!=null && row["UnitJudgeOrder"].ToString()!="")
				{
					model.UnitJudgeOrder=int.Parse(row["UnitJudgeOrder"].ToString());
				}
				if(row["AllotCountBefore"]!=null && row["AllotCountBefore"].ToString()!="")
				{
					model.AllotCountBefore=decimal.Parse(row["AllotCountBefore"].ToString());
				}
				if(row["Stat"]!=null && row["Stat"].ToString()!="")
				{
					model.Stat=int.Parse(row["Stat"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,RenwuID,IsFired,UnitID,UnitName,SpeID,SpeName,MemID,MemName,AllotCount,UnitOrder,UnitJudgeOrder,AllotCountBefore,Stat ");
			strSql.Append(" FROM cm_KaoHeMemsAllotChange ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,RenwuID,IsFired,UnitID,UnitName,SpeID,SpeName,MemID,MemName,AllotCount,UnitOrder,UnitJudgeOrder,AllotCountBefore,Stat ");
			strSql.Append(" FROM cm_KaoHeMemsAllotChange ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeMemsAllotChange ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeMemsAllotChange T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeMemsAllotChange";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

