﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoheProjType
	/// </summary>
	public partial class cm_KaoheProjType
	{
		public cm_KaoheProjType()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoheProjType model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoheProjType(");
			strSql.Append("KaoheNameID,KaoheTypeID,KaoheTypeName,RealScale,TypeXishu,InsertUserID,InsertDate,virallotCount)");
			strSql.Append(" values (");
			strSql.Append("@KaoheNameID,@KaoheTypeID,@KaoheTypeName,@RealScale,@TypeXishu,@InsertUserID,@InsertDate,@virallotCount)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@KaoheNameID", SqlDbType.Int,4),
					new SqlParameter("@KaoheTypeID", SqlDbType.Int,4),
					new SqlParameter("@KaoheTypeName", SqlDbType.NVarChar,50),
					new SqlParameter("@RealScale", SqlDbType.Decimal,9),
					new SqlParameter("@TypeXishu", SqlDbType.Decimal,9),
					new SqlParameter("@InsertUserID", SqlDbType.Int,4),
					new SqlParameter("@InsertDate", SqlDbType.DateTime),
					new SqlParameter("@virallotCount", SqlDbType.Decimal,9)};
			parameters[0].Value = model.KaoheNameID;
			parameters[1].Value = model.KaoheTypeID;
			parameters[2].Value = model.KaoheTypeName;
			parameters[3].Value = model.RealScale;
			parameters[4].Value = model.TypeXishu;
			parameters[5].Value = model.InsertUserID;
			parameters[6].Value = model.InsertDate;
			parameters[7].Value = model.virallotCount;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoheProjType model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoheProjType set ");
			strSql.Append("KaoheNameID=@KaoheNameID,");
			strSql.Append("KaoheTypeID=@KaoheTypeID,");
			strSql.Append("KaoheTypeName=@KaoheTypeName,");
			strSql.Append("RealScale=@RealScale,");
			strSql.Append("TypeXishu=@TypeXishu,");
			strSql.Append("InsertUserID=@InsertUserID,");
			strSql.Append("InsertDate=@InsertDate,");
			strSql.Append("virallotCount=@virallotCount");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@KaoheNameID", SqlDbType.Int,4),
					new SqlParameter("@KaoheTypeID", SqlDbType.Int,4),
					new SqlParameter("@KaoheTypeName", SqlDbType.NVarChar,50),
					new SqlParameter("@RealScale", SqlDbType.Decimal,9),
					new SqlParameter("@TypeXishu", SqlDbType.Decimal,9),
					new SqlParameter("@InsertUserID", SqlDbType.Int,4),
					new SqlParameter("@InsertDate", SqlDbType.DateTime),
					new SqlParameter("@virallotCount", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.KaoheNameID;
			parameters[1].Value = model.KaoheTypeID;
			parameters[2].Value = model.KaoheTypeName;
			parameters[3].Value = model.RealScale;
			parameters[4].Value = model.TypeXishu;
			parameters[5].Value = model.InsertUserID;
			parameters[6].Value = model.InsertDate;
			parameters[7].Value = model.virallotCount;
			parameters[8].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoheProjType ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoheProjType ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoheProjType GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,KaoheNameID,KaoheTypeID,KaoheTypeName,RealScale,TypeXishu,InsertUserID,InsertDate,virallotCount from cm_KaoheProjType ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoheProjType model=new TG.Model.cm_KaoheProjType();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoheProjType DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoheProjType model=new TG.Model.cm_KaoheProjType();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["KaoheNameID"]!=null && row["KaoheNameID"].ToString()!="")
				{
					model.KaoheNameID=int.Parse(row["KaoheNameID"].ToString());
				}
				if(row["KaoheTypeID"]!=null && row["KaoheTypeID"].ToString()!="")
				{
					model.KaoheTypeID=int.Parse(row["KaoheTypeID"].ToString());
				}
				if(row["KaoheTypeName"]!=null)
				{
					model.KaoheTypeName=row["KaoheTypeName"].ToString();
				}
				if(row["RealScale"]!=null && row["RealScale"].ToString()!="")
				{
					model.RealScale=decimal.Parse(row["RealScale"].ToString());
				}
				if(row["TypeXishu"]!=null && row["TypeXishu"].ToString()!="")
				{
					model.TypeXishu=decimal.Parse(row["TypeXishu"].ToString());
				}
				if(row["InsertUserID"]!=null && row["InsertUserID"].ToString()!="")
				{
					model.InsertUserID=int.Parse(row["InsertUserID"].ToString());
				}
				if(row["InsertDate"]!=null && row["InsertDate"].ToString()!="")
				{
					model.InsertDate=DateTime.Parse(row["InsertDate"].ToString());
				}
				if(row["virallotCount"]!=null && row["virallotCount"].ToString()!="")
				{
					model.virallotCount=decimal.Parse(row["virallotCount"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,KaoheNameID,KaoheTypeID,KaoheTypeName,RealScale,TypeXishu,InsertUserID,InsertDate,virallotCount ");
			strSql.Append(" FROM cm_KaoheProjType ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,KaoheNameID,KaoheTypeID,KaoheTypeName,RealScale,TypeXishu,InsertUserID,InsertDate,virallotCount ");
			strSql.Append(" FROM cm_KaoheProjType ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoheProjType ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoheProjType T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoheProjType";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

