﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.DBUtility;
using TG.Model;

namespace TG.DAL
{
    public class cm_ProjectInvoice
    {
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectInvoice model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ProjectInvoice set ");
            strSql.Append("projID=@projID,");
            strSql.Append("cprID=@cprID,");
            strSql.Append("Status=@Status,");
            strSql.Append("InvoiceName=@InvoiceName,");
            strSql.Append("InvoiceAmount=@InvoiceAmount,");
            strSql.Append("InvoiceCode=@InvoiceCode,");
            strSql.Append("InvoiceType=@InvoiceType,");
            strSql.Append("InvoiceDate=@InvoiceDate,");
            strSql.Append("CaiwuMark=@CaiwuMark,");
            strSql.Append("AuditUserID=@AuditUserID,");
            strSql.Append("AuditDate=@AuditDate,");
            strSql.Append("CountryAddress=@CountryAddress,");
            strSql.Append("CountryPhone=@CountryPhone,");
            strSql.Append("Bankaccount=@Bankaccount,");
            strSql.Append("Accountinfo=@Accountinfo");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@projID", SqlDbType.Int,4),
					new SqlParameter("@cprID", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Char,5),
					new SqlParameter("@InvoiceName", SqlDbType.NVarChar,200),
					new SqlParameter("@InvoiceAmount", SqlDbType.Decimal,9),
					new SqlParameter("@InvoiceCode", SqlDbType.NVarChar,50),
					new SqlParameter("@InvoiceType", SqlDbType.NVarChar,50),
					new SqlParameter("@InvoiceDate", SqlDbType.DateTime),
					new SqlParameter("@CaiwuMark", SqlDbType.VarChar,500),
					new SqlParameter("@AuditUserID", SqlDbType.Int,4),
					new SqlParameter("@AuditDate", SqlDbType.DateTime),
                    new SqlParameter("@CountryAddress", SqlDbType.VarChar,500),
                    new SqlParameter("@CountryPhone", SqlDbType.NVarChar,50),
                    new SqlParameter("@Bankaccount", SqlDbType.NVarChar,50),
                    new SqlParameter("@Accountinfo", SqlDbType.NVarChar,50),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.projID;
            parameters[1].Value = model.cprID;
            parameters[2].Value = model.Status;
            parameters[3].Value = model.InvoiceName;
            parameters[4].Value = model.InvoiceAmount;
            parameters[5].Value = model.InvoiceCode;
            parameters[6].Value = model.InvoiceType;
            parameters[7].Value = model.InvoiceDate;
            parameters[8].Value = model.CaiwuMark;
            parameters[9].Value = model.AuditUserID;
            parameters[10].Value = model.AuditDate;
            parameters[11].Value = model.CountryAddress;
            parameters[12].Value = model.CountryPhone;
            parameters[13].Value = model.Bankaccount;
            parameters[14].Value = model.Accountinfo;
            parameters[15].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            return rows;
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProjectInvoice ");
            strSql.Append(" where ID=" + ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		
        /// <summary>
        /// 取得合同
        /// </summary>
        /// <param name="cpr_id"></param>
        /// <returns></returns>
        public TG.Model.cm_Coperation_Info GetCprModelInfo(int cpr_id)
        {
            string sql = @"select top 1 a.cpr_id, a.cpr_Acount,a.cpr_Name,b.Cst_Name,a.cpr_Unit,a.cpr_No,isnull((select top 1 pro_name from cm_project where CoperationSysNo=a.cpr_id order by pro_id desc),'') as pro_name from cm_Coperation  a
                           inner join cm_CustomerInfo b
                          on a.cst_Id=b.Cst_Id  where a.cpr_id=" + cpr_id + "";
            TG.Model.cm_Coperation_Info model = new TG.Model.cm_Coperation_Info();
            DataSet ds = DbHelperSQL.Query(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["cpr_id"] != null && ds.Tables[0].Rows[0]["cpr_id"].ToString() != "")
                {
                    model.cpr_id = ds.Tables[0].Rows[0]["cpr_id"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Acount"] != null && ds.Tables[0].Rows[0]["cpr_Acount"].ToString() != "")
                {
                    model.cpr_Acount = ds.Tables[0].Rows[0]["cpr_Acount"].ToString();
                }
               
                if (ds.Tables[0].Rows[0]["cpr_Name"] != null && ds.Tables[0].Rows[0]["cpr_Name"].ToString() != "")
                {
                    model.cpr_Name = ds.Tables[0].Rows[0]["cpr_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Unit"] != null && ds.Tables[0].Rows[0]["cpr_Unit"].ToString() != "")
                {
                    model.cpr_Unit = ds.Tables[0].Rows[0]["cpr_Unit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Cst_Name"] != null && ds.Tables[0].Rows[0]["Cst_Name"].ToString() != "")
                {
                    model.Cst_Name = ds.Tables[0].Rows[0]["Cst_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_No"] != null && ds.Tables[0].Rows[0]["cpr_No"].ToString() != "")
                {
                    model.cpr_No = ds.Tables[0].Rows[0]["cpr_No"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_name"] != null && ds.Tables[0].Rows[0]["pro_name"].ToString() != "")
                {
                    model.pro_Name = ds.Tables[0].Rows[0]["pro_name"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectInvoice GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,projID,cprID,Status,ApplyMark,ApplyId,InvoiceName,InvoiceAmount,InvoiceCode,InvoiceType,InvoiceDate,CaiwuMark,AuditUserID,AuditDate,InsertUserID,b.mem_Name as InsertUserName, InsertDate,CountryAddress,CountryPhone,Bankaccount,Accountinfo from cm_ProjectInvoice a inner join tg_member b on a.InsertUserID=b.mem_ID ");
            strSql.Append(" where a.ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_ProjectInvoice model = new TG.Model.cm_ProjectInvoice();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["projID"] != null && ds.Tables[0].Rows[0]["projID"].ToString() != "")
                {
                    model.projID = int.Parse(ds.Tables[0].Rows[0]["projID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cprID"] != null && ds.Tables[0].Rows[0]["cprID"].ToString() != "")
                {
                    model.cprID = int.Parse(ds.Tables[0].Rows[0]["cprID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ApplyMark"] != null && ds.Tables[0].Rows[0]["ApplyMark"].ToString() != "")
                {
                    model.ApplyMark = ds.Tables[0].Rows[0]["ApplyMark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ApplyId"] != null && ds.Tables[0].Rows[0]["ApplyId"].ToString() != "")
                {
                    model.ApplyId = int.Parse(ds.Tables[0].Rows[0]["ApplyId"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InvoiceName"] != null && ds.Tables[0].Rows[0]["InvoiceName"].ToString() != "")
                {
                    model.InvoiceName = ds.Tables[0].Rows[0]["InvoiceName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["InvoiceAmount"] != null && ds.Tables[0].Rows[0]["InvoiceAmount"].ToString() != "")
                {
                    model.InvoiceAmount = decimal.Parse(ds.Tables[0].Rows[0]["InvoiceAmount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InvoiceCode"] != null && ds.Tables[0].Rows[0]["InvoiceCode"].ToString() != "")
                {
                    model.InvoiceCode = ds.Tables[0].Rows[0]["InvoiceCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["InvoiceType"] != null && ds.Tables[0].Rows[0]["InvoiceType"].ToString() != "")
                {
                    model.InvoiceType = ds.Tables[0].Rows[0]["InvoiceType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["InvoiceDate"] != null && ds.Tables[0].Rows[0]["InvoiceDate"].ToString() != "")
                {
                    model.InvoiceDate = DateTime.Parse(ds.Tables[0].Rows[0]["InvoiceDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CaiwuMark"] != null && ds.Tables[0].Rows[0]["CaiwuMark"].ToString() != "")
                {
                    model.CaiwuMark = ds.Tables[0].Rows[0]["CaiwuMark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditUserID"] != null && ds.Tables[0].Rows[0]["AuditUserID"].ToString() != "")
                {
                    model.AuditUserID = int.Parse(ds.Tables[0].Rows[0]["AuditUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AuditDate"] != null && ds.Tables[0].Rows[0]["AuditDate"].ToString() != "")
                {
                    model.AuditDate = DateTime.Parse(ds.Tables[0].Rows[0]["AuditDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InsertUserID"] != null && ds.Tables[0].Rows[0]["InsertUserID"].ToString() != "")
                {
                    model.InsertUserID = int.Parse(ds.Tables[0].Rows[0]["InsertUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InsertUserName"] != null && ds.Tables[0].Rows[0]["InsertUserName"].ToString() != "")
                {
                    model.InsertUserName = ds.Tables[0].Rows[0]["InsertUserName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["InsertDate"] != null && ds.Tables[0].Rows[0]["InsertDate"].ToString() != "")
                {
                    model.InsertDate = DateTime.Parse(ds.Tables[0].Rows[0]["InsertDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CountryAddress"] != null && ds.Tables[0].Rows[0]["CountryAddress"].ToString() != "")
                {
                    model.CountryAddress = ds.Tables[0].Rows[0]["CountryAddress"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CountryPhone"] != null && ds.Tables[0].Rows[0]["CountryPhone"].ToString() != "")
                {
                    model.CountryPhone = ds.Tables[0].Rows[0]["CountryPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Bankaccount"] != null && ds.Tables[0].Rows[0]["Bankaccount"].ToString() != "")
                {
                    model.Bankaccount = ds.Tables[0].Rows[0]["Bankaccount"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Accountinfo"] != null && ds.Tables[0].Rows[0]["Accountinfo"].ToString() != "")
                {
                    model.Accountinfo = ds.Tables[0].Rows[0]["Accountinfo"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 新增开票
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int AddProjectInvoice(cm_ProjectInvoiceEntity model, string uerList)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                TG.Model.cm_Coperation cpr = new TG.DAL.cm_Coperation().GetModel(int.Parse(model.cpr_id));


                command.CommandText = "insert into cm_ProjectInvoice(projID,cprID,Status,ApplyMark,InvoiceName,InvoiceAmount,InvoiceType,InvoiceDate,InsertUserID,InsertDate) values(" + model.pro_id + "," + model.cpr_id + ",'A','" + model.ApplyMark + "','" + model.InvoiceName + "','" + model.InvoiceAmount + "','" + model.InvoiceType + "','" + model.InvoiceDate + "'," + model.user_id + ",'" + DateTime.Now + "');select @@IDENTITY";
                object obj = command.ExecuteScalar();
                if (obj != null)
                {
                    //int Allot_ID = Convert.ToInt32(obj);
                    //string MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", cpr.cpr_Name, "申请开票");

                    //if (uerList.Contains(","))
                    //{
                    //    string[] _uerList = uerList.Split(',');
                    //    for (int j = 0; j < uerList.Length; j++)
                    //    {

                    //        int userID = int.Parse(_uerList[j]);

                    //        //新增消息
                    //        string ReferenceSysNo = string.Format("cpr_id={0}&invoice_id={1}&MessageStatus={2}", model.cpr_id, Allot_ID, "A");

                    //        command.CommandText = string.Format("insert into cm_SysMsg (FromUser,ToRole,MsgType,ReferenceSysNo,InUser,InDate,MessageContent,QueryCondition,ExtendField,IsDone)values({0},N'{1}',{2},N'{3}',{4},N'{5}',N'{6}',N'{7}',N'{8}',N'{9}')"
                    //        , userID
                    //        , "0"
                    //        , 30
                    //        , ReferenceSysNo
                    //        , model.user_id == null ? 0 : int.Parse(model.user_id)
                    //        , DateTime.Now.ToString()
                    //        , MessageContent
                    //        , cpr.cpr_Name
                    //        , ""
                    //        , "A");

                    //        command.ExecuteNonQuery();

                    //    }

                    //}
                    //else
                    //{
                    //    int userID = int.Parse(uerList);
                    //    //新增消息
                    //    string ReferenceSysNo = string.Format("cpr_id={0}&invoice_id={1}&MessageStatus={2}", model.cpr_id, Allot_ID, "A");

                    //    command.CommandText = string.Format("insert into cm_SysMsg (FromUser,ToRole,MsgType,ReferenceSysNo,InUser,InDate,MessageContent,QueryCondition,ExtendField,IsDone)values({0},N'{1}',{2},N'{3}',{4},N'{5}',N'{6}',N'{7}',N'{8}',N'{9}')"
                    //    , userID
                    //    , "0"
                    //    , 30
                    //    , ReferenceSysNo
                    //    , model.user_id == null ? 0 : int.Parse(model.user_id)
                    //    , DateTime.Now.ToString()
                    //    , MessageContent
                    //    , cpr.cpr_Name
                    //    , ""
                    //    , "A");

                    //    command.ExecuteNonQuery();
                    //}
                    
                }

                transaction.Commit();
                return Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        /// 合同开票导出
        /// </summary>
        /// <param name="sqlwhere"></param>
        /// <returns></returns>
        public DataSet GetCoperationInvoiceExport(string sqlwhere, string str)
        {
            string sql = string.Format(@"	select *,
					Convert(decimal(18,2),(Select isnull(SUM(Acount),0.00) From cm_ProjectCharge Where Status<>'B' AND cprID=C.cpr_Id)) as ssze,
					Convert(decimal(18,2),(Select isnull(SUM(InvoiceAmount),0.00) From cm_ProjectInvoice Where Status<>'B' {0} AND cprID=C.cpr_Id)) as kpje,
                    convert(varchar(10),cpr_SignDate,120) as qdrq,
					Convert(decimal(18,2),(case when cpr_ShijiAcount<>0 then (Select isnull(SUM(InvoiceAmount),0.00) From cm_ProjectInvoice Where Status<>'B'  AND cprID=C.cpr_Id )/cpr_ShijiAcount *100 else 0 end)) as kpjd,
					Convert(decimal(18,2),(cpr_ShijiAcount-(Select isnull(SUM(Acount),0.00) From cm_ProjectCharge Where Status<>'B' AND cprID=C.cpr_Id ))) as wsk
				from cm_Coperation C where 1=1 " + sqlwhere + " order by cpr_id desc ", str);
            return DbHelperSQL.Query(sql);
        }
    }
}
