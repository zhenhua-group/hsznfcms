﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeAllMemsCompareHis
	/// </summary>
	public partial class cm_KaoHeAllMemsCompareHis
	{
		public cm_KaoHeAllMemsCompareHis()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeAllMemsCompareHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeAllMemsCompareHis(");
			strSql.Append("RenwuID,UnitName,UserName,IsFired,JJHJ,PJYGZ,PJYGZBS,PJYGZPM,BMNHPPM,SBN,SBNZF,XBN,XBNZF)");
			strSql.Append(" values (");
			strSql.Append("@RenwuID,@UnitName,@UserName,@IsFired,@JJHJ,@PJYGZ,@PJYGZBS,@PJYGZPM,@BMNHPPM,@SBN,@SBNZF,@XBN,@XBNZF)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.VarChar,50),
					new SqlParameter("@UserName", SqlDbType.VarChar,50),
					new SqlParameter("@IsFired", SqlDbType.VarChar,30),
					new SqlParameter("@JJHJ", SqlDbType.Decimal,9),
					new SqlParameter("@PJYGZ", SqlDbType.Decimal,9),
					new SqlParameter("@PJYGZBS", SqlDbType.Decimal,9),
					new SqlParameter("@PJYGZPM", SqlDbType.Decimal,9),
					new SqlParameter("@BMNHPPM", SqlDbType.Decimal,9),
					new SqlParameter("@SBN", SqlDbType.Decimal,9),
					new SqlParameter("@SBNZF", SqlDbType.Decimal,9),
					new SqlParameter("@XBN", SqlDbType.Decimal,9),
					new SqlParameter("@XBNZF", SqlDbType.Decimal,9)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.UnitName;
			parameters[2].Value = model.UserName;
			parameters[3].Value = model.IsFired;
			parameters[4].Value = model.JJHJ;
			parameters[5].Value = model.PJYGZ;
			parameters[6].Value = model.PJYGZBS;
			parameters[7].Value = model.PJYGZPM;
			parameters[8].Value = model.BMNHPPM;
			parameters[9].Value = model.SBN;
			parameters[10].Value = model.SBNZF;
			parameters[11].Value = model.XBN;
			parameters[12].Value = model.XBNZF;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeAllMemsCompareHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeAllMemsCompareHis set ");
			strSql.Append("RenwuID=@RenwuID,");
			strSql.Append("UnitName=@UnitName,");
			strSql.Append("UserName=@UserName,");
			strSql.Append("IsFired=@IsFired,");
			strSql.Append("JJHJ=@JJHJ,");
			strSql.Append("PJYGZ=@PJYGZ,");
			strSql.Append("PJYGZBS=@PJYGZBS,");
			strSql.Append("PJYGZPM=@PJYGZPM,");
			strSql.Append("BMNHPPM=@BMNHPPM,");
			strSql.Append("SBN=@SBN,");
			strSql.Append("SBNZF=@SBNZF,");
			strSql.Append("XBN=@XBN,");
			strSql.Append("XBNZF=@XBNZF");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.VarChar,50),
					new SqlParameter("@UserName", SqlDbType.VarChar,50),
					new SqlParameter("@IsFired", SqlDbType.VarChar,30),
					new SqlParameter("@JJHJ", SqlDbType.Decimal,9),
					new SqlParameter("@PJYGZ", SqlDbType.Decimal,9),
					new SqlParameter("@PJYGZBS", SqlDbType.Decimal,9),
					new SqlParameter("@PJYGZPM", SqlDbType.Decimal,9),
					new SqlParameter("@BMNHPPM", SqlDbType.Decimal,9),
					new SqlParameter("@SBN", SqlDbType.Decimal,9),
					new SqlParameter("@SBNZF", SqlDbType.Decimal,9),
					new SqlParameter("@XBN", SqlDbType.Decimal,9),
					new SqlParameter("@XBNZF", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.UnitName;
			parameters[2].Value = model.UserName;
			parameters[3].Value = model.IsFired;
			parameters[4].Value = model.JJHJ;
			parameters[5].Value = model.PJYGZ;
			parameters[6].Value = model.PJYGZBS;
			parameters[7].Value = model.PJYGZPM;
			parameters[8].Value = model.BMNHPPM;
			parameters[9].Value = model.SBN;
			parameters[10].Value = model.SBNZF;
			parameters[11].Value = model.XBN;
			parameters[12].Value = model.XBNZF;
			parameters[13].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeAllMemsCompareHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeAllMemsCompareHis ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeAllMemsCompareHis GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,RenwuID,UnitName,UserName,IsFired,JJHJ,PJYGZ,PJYGZBS,PJYGZPM,BMNHPPM,SBN,SBNZF,XBN,XBNZF from cm_KaoHeAllMemsCompareHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeAllMemsCompareHis model=new TG.Model.cm_KaoHeAllMemsCompareHis();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeAllMemsCompareHis DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeAllMemsCompareHis model=new TG.Model.cm_KaoHeAllMemsCompareHis();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["RenwuID"]!=null && row["RenwuID"].ToString()!="")
				{
					model.RenwuID=int.Parse(row["RenwuID"].ToString());
				}
				if(row["UnitName"]!=null)
				{
					model.UnitName=row["UnitName"].ToString();
				}
				if(row["UserName"]!=null)
				{
					model.UserName=row["UserName"].ToString();
				}
				if(row["IsFired"]!=null)
				{
					model.IsFired=row["IsFired"].ToString();
				}
				if(row["JJHJ"]!=null && row["JJHJ"].ToString()!="")
				{
					model.JJHJ=decimal.Parse(row["JJHJ"].ToString());
				}
				if(row["PJYGZ"]!=null && row["PJYGZ"].ToString()!="")
				{
					model.PJYGZ=decimal.Parse(row["PJYGZ"].ToString());
				}
				if(row["PJYGZBS"]!=null && row["PJYGZBS"].ToString()!="")
				{
					model.PJYGZBS=decimal.Parse(row["PJYGZBS"].ToString());
				}
				if(row["PJYGZPM"]!=null && row["PJYGZPM"].ToString()!="")
				{
					model.PJYGZPM=decimal.Parse(row["PJYGZPM"].ToString());
				}
				if(row["BMNHPPM"]!=null && row["BMNHPPM"].ToString()!="")
				{
					model.BMNHPPM=decimal.Parse(row["BMNHPPM"].ToString());
				}
				if(row["SBN"]!=null && row["SBN"].ToString()!="")
				{
					model.SBN=decimal.Parse(row["SBN"].ToString());
				}
				if(row["SBNZF"]!=null && row["SBNZF"].ToString()!="")
				{
					model.SBNZF=decimal.Parse(row["SBNZF"].ToString());
				}
				if(row["XBN"]!=null && row["XBN"].ToString()!="")
				{
					model.XBN=decimal.Parse(row["XBN"].ToString());
				}
				if(row["XBNZF"]!=null && row["XBNZF"].ToString()!="")
				{
					model.XBNZF=decimal.Parse(row["XBNZF"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,RenwuID,UnitName,UserName,IsFired,JJHJ,PJYGZ,PJYGZBS,PJYGZPM,BMNHPPM,SBN,SBNZF,XBN,XBNZF ");
			strSql.Append(" FROM cm_KaoHeAllMemsCompareHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,RenwuID,UnitName,UserName,IsFired,JJHJ,PJYGZ,PJYGZBS,PJYGZPM,BMNHPPM,SBN,SBNZF,XBN,XBNZF ");
			strSql.Append(" FROM cm_KaoHeAllMemsCompareHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeAllMemsCompareHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeAllMemsCompareHis T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeAllMemsCompareHis";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

