﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_ProImageAuditConfig
	/// </summary>
	public partial class cm_ProImageAuditConfig
	{
		public cm_ProImageAuditConfig()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("SysNo", "cm_ProImageAuditConfig"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SysNo)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_ProImageAuditConfig");
			strSql.Append(" where SysNo=@SysNo");
			SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
			parameters[0].Value = SysNo;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_ProImageAuditConfig model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_ProImageAuditConfig(");
			strSql.Append("ProcessDescription,RoleSysNo,Position,InUser,InDate)");
			strSql.Append(" values (");
			strSql.Append("@ProcessDescription,@RoleSysNo,@Position,@InUser,@InDate)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ProcessDescription", SqlDbType.NVarChar,50),
					new SqlParameter("@RoleSysNo", SqlDbType.Int,4),
					new SqlParameter("@Position", SqlDbType.Int,4),
					new SqlParameter("@InUser", SqlDbType.Int,4),
					new SqlParameter("@InDate", SqlDbType.DateTime)};
			parameters[0].Value = model.ProcessDescription;
			parameters[1].Value = model.RoleSysNo;
			parameters[2].Value = model.Position;
			parameters[3].Value = model.InUser;
			parameters[4].Value = model.InDate;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ProImageAuditConfig model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_ProImageAuditConfig set ");
			strSql.Append("ProcessDescription=@ProcessDescription,");
			strSql.Append("RoleSysNo=@RoleSysNo,");
			strSql.Append("Position=@Position,");
			strSql.Append("InUser=@InUser,");
			strSql.Append("InDate=@InDate");
			strSql.Append(" where SysNo=@SysNo");
			SqlParameter[] parameters = {
					new SqlParameter("@ProcessDescription", SqlDbType.NVarChar,50),
					new SqlParameter("@RoleSysNo", SqlDbType.Int,4),
					new SqlParameter("@Position", SqlDbType.Int,4),
					new SqlParameter("@InUser", SqlDbType.Int,4),
					new SqlParameter("@InDate", SqlDbType.DateTime),
					new SqlParameter("@SysNo", SqlDbType.Int,4)};
			parameters[0].Value = model.ProcessDescription;
			parameters[1].Value = model.RoleSysNo;
			parameters[2].Value = model.Position;
			parameters[3].Value = model.InUser;
			parameters[4].Value = model.InDate;
			parameters[5].Value = model.SysNo;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SysNo)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ProImageAuditConfig ");
			strSql.Append(" where SysNo=@SysNo");
			SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
			parameters[0].Value = SysNo;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SysNolist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ProImageAuditConfig ");
			strSql.Append(" where SysNo in ("+SysNolist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ProImageAuditConfig GetModel(int SysNo)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 SysNo,ProcessDescription,RoleSysNo,Position,InUser,InDate from cm_ProImageAuditConfig ");
			strSql.Append(" where SysNo=@SysNo");
			SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
			parameters[0].Value = SysNo;

			TG.Model.cm_ProImageAuditConfig model=new TG.Model.cm_ProImageAuditConfig();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["SysNo"]!=null && ds.Tables[0].Rows[0]["SysNo"].ToString()!="")
				{
					model.SysNo=int.Parse(ds.Tables[0].Rows[0]["SysNo"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ProcessDescription"]!=null && ds.Tables[0].Rows[0]["ProcessDescription"].ToString()!="")
				{
					model.ProcessDescription=ds.Tables[0].Rows[0]["ProcessDescription"].ToString();
				}
				if(ds.Tables[0].Rows[0]["RoleSysNo"]!=null && ds.Tables[0].Rows[0]["RoleSysNo"].ToString()!="")
				{
					model.RoleSysNo=int.Parse(ds.Tables[0].Rows[0]["RoleSysNo"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Position"]!=null && ds.Tables[0].Rows[0]["Position"].ToString()!="")
				{
					model.Position=int.Parse(ds.Tables[0].Rows[0]["Position"].ToString());
				}
				if(ds.Tables[0].Rows[0]["InUser"]!=null && ds.Tables[0].Rows[0]["InUser"].ToString()!="")
				{
					model.InUser=int.Parse(ds.Tables[0].Rows[0]["InUser"].ToString());
				}
				if(ds.Tables[0].Rows[0]["InDate"]!=null && ds.Tables[0].Rows[0]["InDate"].ToString()!="")
				{
					model.InDate=DateTime.Parse(ds.Tables[0].Rows[0]["InDate"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SysNo,ProcessDescription,RoleSysNo,Position,InUser,InDate ");
			strSql.Append(" FROM cm_ProImageAuditConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SysNo,ProcessDescription,RoleSysNo,Position,InUser,InDate ");
			strSql.Append(" FROM cm_ProImageAuditConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_ProImageAuditConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SysNo desc");
			}
			strSql.Append(")AS Row, T.*  from cm_ProImageAuditConfig T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_ProImageAuditConfig";
			parameters[1].Value = "SysNo";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

