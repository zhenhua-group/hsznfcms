﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.DBUtility;

namespace TG.DAL
{
    public class cm_ProjectValueByMemberAuditStatus
    {
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectValueByMemberAuditStatus model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_ProjectValueByMemberAuditStatus(");
            strSql.Append("proID,AllotID,mem_id,Status,AuditUser,AuditDate,InsertUser,InsertDate,TranType)");
            strSql.Append(" values (");
            strSql.Append("@proID,@AllotID,@mem_id,@Status,@AuditUser,@AuditDate,@InsertUser,@InsertDate,@TranType)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@proID", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@mem_id", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Char,1),
					new SqlParameter("@AuditUser", SqlDbType.Int,4),
					new SqlParameter("@AuditDate", SqlDbType.DateTime),
					new SqlParameter("@InsertUser", SqlDbType.Int,4),
					new SqlParameter("@InsertDate", SqlDbType.DateTime),
                    new SqlParameter("@TranType", SqlDbType.NVarChar)};
            parameters[0].Value = model.proID;
            parameters[1].Value = model.AllotID;
            parameters[2].Value = model.mem_id;
            parameters[3].Value = model.Status;
            parameters[4].Value = model.AuditUser;
            parameters[5].Value = model.AuditDate;
            parameters[6].Value = model.InsertUser;
            parameters[7].Value = model.InsertDate;
            parameters[8].Value = model.TranType;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 新增人员审批表信息
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="pro_id"></param>
        /// <param name="allotID"></param>
        /// <param name="insertUser"></param>
        /// <returns></returns>
        public int AddAduitMemberUserInfo(DataTable dt, int pro_id, int? allotID, int insertUser)
        {
            int count = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                TG.Model.cm_ProjectValueByMemberAuditStatus model = new TG.Model.cm_ProjectValueByMemberAuditStatus();
                model.proID = pro_id;
                model.AllotID = allotID;
                model.mem_id = int.Parse(dt.Rows[i]["mem_ID"].ToString());
                model.Status = "A";
                model.InsertUser = insertUser;
                model.InsertDate = DateTime.Now;
                count = count + Add(model);
            }
            return count;
        }

        /// <summary>
        /// 新增人员审批表信息-转
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="pro_id"></param>
        /// <param name="allotID"></param>
        /// <param name="insertUser"></param>
        /// <returns></returns>
        public int AddAduitTranMemberUserInfo(DataTable dt, int pro_id, int? allotID, int insertUser, string tranType)
        {
            int count = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                TG.Model.cm_ProjectValueByMemberAuditStatus model = new TG.Model.cm_ProjectValueByMemberAuditStatus();
                model.proID = pro_id;
                model.AllotID = allotID;
                model.mem_id = int.Parse(dt.Rows[i]["mem_ID"].ToString());
                model.Status = "A";
                model.InsertUser = insertUser;
                model.InsertDate = DateTime.Now;
                model.TranType = tranType;
                count = count + Add(model);
            }
            return count;
        }
        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Update(TG.Model.ProjectValueByMemberAuditStatusEntity model)
        {
            string sql = @"update cm_ProjectValueByMemberAuditStatus set AuditUser=" + model.mem_id + "," + @"
                           AuditDate='" + DateTime.Now + "' ," + @"
                           Status='" + model.Status + "', AuditSuggsion='" + model.AuditSuggsion + "' where proID=" + model.ProID + " and AllotID=" + model.AllotID + " and mem_id=" + model.mem_id + "  and  TranType is null";
            int rows = DbHelperSQL.ExecuteSql(sql);
            return rows;
        }

        /// <summary>
        /// 批量更新数据
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public int Update(DataTable dt)
        {
            int count = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string sql = "update cm_ProjectValueByMemberAuditStatus set  Status='A' where id=" + dt.Rows[i]["ID"] + "";
                count = DbHelperSQL.ExecuteSql(sql);
            }
            return count;
        }
        /// <summary>
        /// 更新状态-转
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int TranUpdate(TG.Model.ProjectValueByMemberAuditStatusEntity model, string tranType)
        {
            string sql = @"update cm_ProjectValueByMemberAuditStatus set AuditUser=" + model.mem_id + "," + @"
                           AuditDate='" + DateTime.Now + "' ," + @"
                           Status='" + model.Status + "', AuditSuggsion='" + model.AuditSuggsion + "' where proID=" + model.ProID + " and AllotID=" + model.AllotID + " and mem_id=" + model.mem_id + "  and  TranType='" + tranType + "'";
            int rows = DbHelperSQL.ExecuteSql(sql);
            return rows;
        }
        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Delete(TG.Model.ProjectValueByMemberAuditStatusEntity model)
        {
            string sql = @"delete from cm_ProjectValueByMemberAuditStatus
                           where proID=" + model.ProID + " and AllotID=" + model.AllotID + "  and mem_id in  " + @"
                           (
                             select mem_ID  from tg_member 
                             where mem_Speciality_ID=(select mem_Speciality_ID from tg_member where mem_ID= " + model.mem_id + ") " + @"
                            )";
            int rows = DbHelperSQL.ExecuteSql(sql);
            return rows;
        }

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Delete(int pro_id, int allotID, int mem_id)
        {
            string sql = @"delete from cm_ProjectValueByMemberAuditStatus
                           where proID=" + pro_id + " and AllotID=" + allotID + "  and mem_id in  " + @"
                           (
                             select mem_ID  from tg_member 
                             where mem_Speciality_ID=(select mem_Speciality_ID from tg_member where mem_ID= " + mem_id + ") " + @"
                            ) and TranType is null";
            int rows = DbHelperSQL.ExecuteSql(sql);
            return rows;
        }
        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Delete(int pro_id, int allotID)
        {
            string sql = @"delete from cm_ProjectValueByMemberAuditStatus
                           where proID=" + pro_id + " and AllotID=" + allotID + " and  TranType is null ";

            int rows = DbHelperSQL.ExecuteSql(sql);
            return rows;
        }

        /// <summary>
        /// 删除信息-转信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Delete(int pro_id, int allotID, string tranType)
        {
            string sql = @"delete from cm_ProjectValueByMemberAuditStatus
                           where proID=" + pro_id + " and AllotID=" + allotID + " and  TranType ='" + tranType + "' ";

            int rows = DbHelperSQL.ExecuteSql(sql);
            return rows;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectValueByMemberAuditStatus GetModel(int pro_id, int allotID, int mem_id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,proID,AllotID,mem_id,Status,AuditUser,AuditDate,InsertUser,InsertDate,AuditSuggsion from cm_ProjectValueByMemberAuditStatus ");
            strSql.Append(" where proID=@pro_id and AllotID=@AllotID and mem_id=@mem_id");
            SqlParameter[] parameters = {
					new SqlParameter("@pro_id", SqlDbType.Int,4),
                    new SqlParameter("@AllotID", SqlDbType.Int,4),
                    new SqlParameter("@mem_id", SqlDbType.Int,4)
			};
            parameters[0].Value = pro_id;
            parameters[1].Value = allotID;
            parameters[2].Value = mem_id;

            TG.Model.cm_ProjectValueByMemberAuditStatus model = new TG.Model.cm_ProjectValueByMemberAuditStatus();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["proID"] != null && ds.Tables[0].Rows[0]["proID"].ToString() != "")
                {
                    model.proID = int.Parse(ds.Tables[0].Rows[0]["proID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotID"] != null && ds.Tables[0].Rows[0]["AllotID"].ToString() != "")
                {
                    model.AllotID = int.Parse(ds.Tables[0].Rows[0]["AllotID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_id"] != null && ds.Tables[0].Rows[0]["mem_id"].ToString() != "")
                {
                    model.mem_id = int.Parse(ds.Tables[0].Rows[0]["mem_id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditUser"] != null && ds.Tables[0].Rows[0]["AuditUser"].ToString() != "")
                {
                    model.AuditUser = int.Parse(ds.Tables[0].Rows[0]["AuditUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AuditDate"] != null && ds.Tables[0].Rows[0]["AuditDate"].ToString() != "")
                {
                    model.AuditDate = DateTime.Parse(ds.Tables[0].Rows[0]["AuditDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InsertUser"] != null && ds.Tables[0].Rows[0]["InsertUser"].ToString() != "")
                {
                    model.InsertUser = int.Parse(ds.Tables[0].Rows[0]["InsertUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InsertDate"] != null && ds.Tables[0].Rows[0]["InsertDate"].ToString() != "")
                {
                    model.InsertDate = DateTime.Parse(ds.Tables[0].Rows[0]["InsertDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AuditSuggsion"] != null && ds.Tables[0].Rows[0]["AuditSuggsion"].ToString() != "")
                {
                    model.AuditSuggsion = ds.Tables[0].Rows[0]["AuditSuggsion"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,proID,AllotID,mem_id,Status,AuditUser,AuditDate,InsertUser,InsertDate ");
            strSql.Append(" FROM cm_ProjectValueByMemberAuditStatus ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 查询个人确认信息
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetListMemberStatus(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,proID,AllotID,a.mem_id,Status,AuditUser,AuditDate,InsertUser,InsertDate,b.mem_Name ");
            strSql.Append(" FROM cm_ProjectValueByMemberAuditStatus  a inner join tg_member b on a.mem_id=b.mem_ID ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Equals(" order by b.mem_Speciality_ID ");
            return DbHelperSQL.Query(strSql.ToString());
        }
    }
}
