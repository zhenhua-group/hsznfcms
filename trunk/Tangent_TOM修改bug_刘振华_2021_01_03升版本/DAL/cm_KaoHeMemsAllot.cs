﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeMemsAllot
	/// </summary>
	public partial class cm_KaoHeMemsAllot
	{
		public cm_KaoHeMemsAllot()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeMemsAllot model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeMemsAllot(");
			strSql.Append("ProKHNameId,AllotID,SpeID,SpeName,MemID,MemName,Zt,Xd,Sh,Fa,Zc,AllotCount)");
			strSql.Append(" values (");
			strSql.Append("@ProKHNameId,@AllotID,@SpeID,@SpeName,@MemID,@MemName,@Zt,@Xd,@Sh,@Fa,@Zc,@AllotCount)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ProKHNameId", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@SpeID", SqlDbType.Int,4),
					new SqlParameter("@SpeName", SqlDbType.NVarChar,50),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@Zt", SqlDbType.Decimal,9),
					new SqlParameter("@Xd", SqlDbType.Decimal,9),
					new SqlParameter("@Sh", SqlDbType.Decimal,9),
					new SqlParameter("@Fa", SqlDbType.Decimal,9),
					new SqlParameter("@Zc", SqlDbType.Decimal,9),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,9)};
			parameters[0].Value = model.ProKHNameId;
			parameters[1].Value = model.AllotID;
			parameters[2].Value = model.SpeID;
			parameters[3].Value = model.SpeName;
			parameters[4].Value = model.MemID;
			parameters[5].Value = model.MemName;
			parameters[6].Value = model.Zt;
			parameters[7].Value = model.Xd;
			parameters[8].Value = model.Sh;
			parameters[9].Value = model.Fa;
			parameters[10].Value = model.Zc;
			parameters[11].Value = model.AllotCount;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeMemsAllot model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeMemsAllot set ");
			strSql.Append("ProKHNameId=@ProKHNameId,");
			strSql.Append("AllotID=@AllotID,");
			strSql.Append("SpeID=@SpeID,");
			strSql.Append("SpeName=@SpeName,");
			strSql.Append("MemID=@MemID,");
			strSql.Append("MemName=@MemName,");
			strSql.Append("Zt=@Zt,");
			strSql.Append("Xd=@Xd,");
			strSql.Append("Sh=@Sh,");
			strSql.Append("Fa=@Fa,");
			strSql.Append("Zc=@Zc,");
			strSql.Append("AllotCount=@AllotCount");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ProKHNameId", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@SpeID", SqlDbType.Int,4),
					new SqlParameter("@SpeName", SqlDbType.NVarChar,50),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@Zt", SqlDbType.Decimal,9),
					new SqlParameter("@Xd", SqlDbType.Decimal,9),
					new SqlParameter("@Sh", SqlDbType.Decimal,9),
					new SqlParameter("@Fa", SqlDbType.Decimal,9),
					new SqlParameter("@Zc", SqlDbType.Decimal,9),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.ProKHNameId;
			parameters[1].Value = model.AllotID;
			parameters[2].Value = model.SpeID;
			parameters[3].Value = model.SpeName;
			parameters[4].Value = model.MemID;
			parameters[5].Value = model.MemName;
			parameters[6].Value = model.Zt;
			parameters[7].Value = model.Xd;
			parameters[8].Value = model.Sh;
			parameters[9].Value = model.Fa;
			parameters[10].Value = model.Zc;
			parameters[11].Value = model.AllotCount;
			parameters[12].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsAllot ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsAllot ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeMemsAllot GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,ProKHNameId,AllotID,SpeID,SpeName,MemID,MemName,Zt,Xd,Sh,Fa,Zc,AllotCount from cm_KaoHeMemsAllot ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeMemsAllot model=new TG.Model.cm_KaoHeMemsAllot();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeMemsAllot DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeMemsAllot model=new TG.Model.cm_KaoHeMemsAllot();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["ProKHNameId"]!=null && row["ProKHNameId"].ToString()!="")
				{
					model.ProKHNameId=int.Parse(row["ProKHNameId"].ToString());
				}
				if(row["AllotID"]!=null && row["AllotID"].ToString()!="")
				{
					model.AllotID=int.Parse(row["AllotID"].ToString());
				}
				if(row["SpeID"]!=null && row["SpeID"].ToString()!="")
				{
					model.SpeID=int.Parse(row["SpeID"].ToString());
				}
				if(row["SpeName"]!=null)
				{
					model.SpeName=row["SpeName"].ToString();
				}
				if(row["MemID"]!=null && row["MemID"].ToString()!="")
				{
					model.MemID=int.Parse(row["MemID"].ToString());
				}
				if(row["MemName"]!=null)
				{
					model.MemName=row["MemName"].ToString();
				}
				if(row["Zt"]!=null && row["Zt"].ToString()!="")
				{
					model.Zt=decimal.Parse(row["Zt"].ToString());
				}
				if(row["Xd"]!=null && row["Xd"].ToString()!="")
				{
					model.Xd=decimal.Parse(row["Xd"].ToString());
				}
				if(row["Sh"]!=null && row["Sh"].ToString()!="")
				{
					model.Sh=decimal.Parse(row["Sh"].ToString());
				}
				if(row["Fa"]!=null && row["Fa"].ToString()!="")
				{
					model.Fa=decimal.Parse(row["Fa"].ToString());
				}
				if(row["Zc"]!=null && row["Zc"].ToString()!="")
				{
					model.Zc=decimal.Parse(row["Zc"].ToString());
				}
				if(row["AllotCount"]!=null && row["AllotCount"].ToString()!="")
				{
					model.AllotCount=decimal.Parse(row["AllotCount"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,ProKHNameId,AllotID,SpeID,SpeName,MemID,MemName,Zt,Xd,Sh,Fa,Zc,AllotCount ");
			strSql.Append(" FROM cm_KaoHeMemsAllot ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,ProKHNameId,AllotID,SpeID,SpeName,MemID,MemName,Zt,Xd,Sh,Fa,Zc,AllotCount ");
			strSql.Append(" FROM cm_KaoHeMemsAllot ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeMemsAllot ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeMemsAllot T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeMemsAllot";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

