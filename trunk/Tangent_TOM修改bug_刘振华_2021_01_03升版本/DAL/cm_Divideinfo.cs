﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.DBUtility;

namespace TG.DAL
{
    public class cm_Divideinfo
    {
        public cm_Divideinfo()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_Divideinfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_Divideinfo(");
            strSql.Append("UnitID,DivideYear,DividePercent,InsertUserID,InsertDate)");
            strSql.Append(" values (");
            strSql.Append("@UnitID,@DivideYear,@DividePercent,@InsertUserID,@InsertDate)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@DivideYear", SqlDbType.Char,10),
					new SqlParameter("@DividePercent", SqlDbType.Decimal,9),
					new SqlParameter("@InsertUserID", SqlDbType.Int,4),
					new SqlParameter("@InsertDate", SqlDbType.DateTime)};
            parameters[0].Value = model.UnitID;
            parameters[1].Value = model.DivideYear;
            parameters[2].Value = model.DividePercent;
            parameters[3].Value = model.InsertUserID;
            parameters[4].Value = DateTime.Now;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_Divideinfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_Divideinfo set ");
            strSql.Append("UnitID=@UnitID,");
            strSql.Append("DivideYear=@DivideYear,");
            strSql.Append("DividePercent=@DividePercent");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
                    new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@DivideYear", SqlDbType.NChar,10),
					new SqlParameter("@DividePercent", SqlDbType.Decimal,9),	
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.UnitID;
            parameters[1].Value = model.DivideYear;
            parameters[2].Value = model.DividePercent;
            parameters[3].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_Divideinfo ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_Divideinfo ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_Divideinfo GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,UnitID,DivideYear,DividePercent,InsertUserID,InsertDate from cm_Divideinfo ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_Divideinfo model = new TG.Model.cm_Divideinfo();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UnitID"] != null && ds.Tables[0].Rows[0]["UnitID"].ToString() != "")
                {
                    model.UnitID = int.Parse(ds.Tables[0].Rows[0]["UnitID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DivideYear"] != null && ds.Tables[0].Rows[0]["DivideYear"].ToString() != "")
                {
                    model.DivideYear = ds.Tables[0].Rows[0]["DivideYear"].ToString();
                }
                if (ds.Tables[0].Rows[0]["DividePercent"] != null && ds.Tables[0].Rows[0]["DividePercent"].ToString() != "")
                {
                    model.DividePercent = decimal.Parse(ds.Tables[0].Rows[0]["DividePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InsertUserID"] != null && ds.Tables[0].Rows[0]["InsertUserID"].ToString() != "")
                {
                    model.InsertUserID = int.Parse(ds.Tables[0].Rows[0]["InsertUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InsertDate"] != null && ds.Tables[0].Rows[0]["InsertDate"].ToString() != "")
                {
                    model.InsertDate = DateTime.Parse(ds.Tables[0].Rows[0]["InsertDate"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,UnitID,DivideYear,DividePercent,InsertUserID,InsertDate ");
            strSql.Append(" FROM cm_Divideinfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_Divideinfo", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_Divideinfo_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 判断是否重复
        /// </summary>
        /// <param name="cst_num"></param>
        /// <param name="cst_Name"></param>
        /// <returns></returns>
        public bool JudgeData(string unit, string dividePercent, string id, string year)
        {

            bool flag = false;

            string sql = @"SELECT count(*) FROM cm_Divideinfo WHERE DividePercent=" + dividePercent + " and  UnitID=" + unit + "  and DivideYear=" + year + "";

            if (!string.IsNullOrEmpty(id))
            {
                sql = sql + " AND  id !=" + id + "";
            }
            object result = TG.DBUtility.DbHelperSQL.GetSingle(sql);

            if (int.Parse(result.ToString()) > 0)
            {
                flag = true;
            }
            else
            {
                flag = false;
            }

            return flag;
        }


        public DataSet GetDividedByYear(string year, string unitname)
        {
            string sql = @"select DivideYear,DividePercent,unit_Name from cm_Divideinfo a
                      left join tg_unit  b
                      on a.UnitID=b.unit_ID where 1=1 ";

            if (!string.IsNullOrEmpty(year))
            {
                sql = sql + " AND DivideYear='" + year + "'";
            }
            if (!string.IsNullOrEmpty(unitname))
            {
                sql = sql + " AND unit_Name='" + unitname + "'";
            }
            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 得到收入成本结余明细表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetUnitRevenueCostBalance(string strWhere, string year)
        {
            string sql = @"select  
                            isnull((select SUM(AllotCount) from cm_ProjectValueAllot c
                             inner join cm_Project d
                             on c.pro_ID=d.pro_ID
                             where d.Unit=a.unit_Name and c.DividedPercent=b.DividePercent
                             and c.SecondValue=1 and c.Status<>'D'";
            if (!string.IsNullOrEmpty(year))
            {
                sql = sql + @" and ActualAllountTime='" + year + "' ";
            }
            sql = sql + @"  
                          group by d.Unit
                            ) ,0) as AllotCount,
                             isnull((select sum(costCharge) from cm_CostDetails where costUnit=a.unit_id";

            if (!string.IsNullOrEmpty(year))
            {
                sql = sql + @" and YEAR(costTime)='" + year + "' ";
            }
            sql = sql + @"  
                            ),0.00) as costCharge," + @"
                            isnull(b.DividePercent,0.00) as DividePercent,a.unit_Name,a.unit_ID,b.DivideYear,";
            sql = sql + @"  isnull((select fund_percent from cm_FundPercentSet where unit_id=a.unit_ID";
            if (!string.IsNullOrEmpty(year))
            {
                sql = sql + @" and YEAR(fund_Year)='" + year + "' ";
            }
            else
            {
                sql = sql + @" and YEAR(fund_Year)='" + DateTime.Now.Year + "' ";
            }
            sql = sql + @" ),0.00) as fund_percent
 
                            from tg_unit  a
                            left join cm_Divideinfo b
                            on a.unit_ID=b.UnitID
                            where  1=1 
                            AND  unit_Name like '%所'  " + strWhere + "" + @"
                            group by b.DividePercent,a.unit_Name,a.unit_ID,b.DivideYear
                            order by a.unit_ID 
                           ";
            return DbHelperSQL.Query(sql);
        }
        #endregion  Method
    }
}

