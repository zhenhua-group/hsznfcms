﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeMemsAllotChangeList
	/// </summary>
	public partial class cm_KaoHeMemsAllotChangeList
	{
		public cm_KaoHeMemsAllotChangeList()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeMemsAllotChangeList model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeMemsAllotChangeList(");
			strSql.Append("proID,proName,renwuID,renwuName,proKHid,KhNameId,KhName)");
			strSql.Append(" values (");
			strSql.Append("@proID,@proName,@renwuID,@renwuName,@proKHid,@KhNameId,@KhName)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@proID", SqlDbType.Int,4),
					new SqlParameter("@proName", SqlDbType.NVarChar,200),
					new SqlParameter("@renwuID", SqlDbType.Int,4),
					new SqlParameter("@renwuName", SqlDbType.NVarChar,100),
					new SqlParameter("@proKHid", SqlDbType.Int,4),
					new SqlParameter("@KhNameId", SqlDbType.Int,4),
					new SqlParameter("@KhName", SqlDbType.NVarChar,200)};
			parameters[0].Value = model.proID;
			parameters[1].Value = model.proName;
			parameters[2].Value = model.renwuID;
			parameters[3].Value = model.renwuName;
			parameters[4].Value = model.proKHid;
			parameters[5].Value = model.KhNameId;
			parameters[6].Value = model.KhName;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeMemsAllotChangeList model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeMemsAllotChangeList set ");
			strSql.Append("proID=@proID,");
			strSql.Append("proName=@proName,");
			strSql.Append("renwuID=@renwuID,");
			strSql.Append("renwuName=@renwuName,");
			strSql.Append("proKHid=@proKHid,");
			strSql.Append("KhNameId=@KhNameId,");
			strSql.Append("KhName=@KhName");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@proID", SqlDbType.Int,4),
					new SqlParameter("@proName", SqlDbType.NVarChar,200),
					new SqlParameter("@renwuID", SqlDbType.Int,4),
					new SqlParameter("@renwuName", SqlDbType.NVarChar,100),
					new SqlParameter("@proKHid", SqlDbType.Int,4),
					new SqlParameter("@KhNameId", SqlDbType.Int,4),
					new SqlParameter("@KhName", SqlDbType.NVarChar,200),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.proID;
			parameters[1].Value = model.proName;
			parameters[2].Value = model.renwuID;
			parameters[3].Value = model.renwuName;
			parameters[4].Value = model.proKHid;
			parameters[5].Value = model.KhNameId;
			parameters[6].Value = model.KhName;
			parameters[7].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsAllotChangeList ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsAllotChangeList ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeMemsAllotChangeList GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,proID,proName,renwuID,renwuName,proKHid,KhNameId,KhName from cm_KaoHeMemsAllotChangeList ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeMemsAllotChangeList model=new TG.Model.cm_KaoHeMemsAllotChangeList();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeMemsAllotChangeList DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeMemsAllotChangeList model=new TG.Model.cm_KaoHeMemsAllotChangeList();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["proID"]!=null && row["proID"].ToString()!="")
				{
					model.proID=int.Parse(row["proID"].ToString());
				}
				if(row["proName"]!=null)
				{
					model.proName=row["proName"].ToString();
				}
				if(row["renwuID"]!=null && row["renwuID"].ToString()!="")
				{
					model.renwuID=int.Parse(row["renwuID"].ToString());
				}
				if(row["renwuName"]!=null)
				{
					model.renwuName=row["renwuName"].ToString();
				}
				if(row["proKHid"]!=null && row["proKHid"].ToString()!="")
				{
					model.proKHid=int.Parse(row["proKHid"].ToString());
				}
				if(row["KhNameId"]!=null && row["KhNameId"].ToString()!="")
				{
					model.KhNameId=int.Parse(row["KhNameId"].ToString());
				}
				if(row["KhName"]!=null)
				{
					model.KhName=row["KhName"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,proID,proName,renwuID,renwuName,proKHid,KhNameId,KhName ");
			strSql.Append(" FROM cm_KaoHeMemsAllotChangeList ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,proID,proName,renwuID,renwuName,proKHid,KhNameId,KhName ");
			strSql.Append(" FROM cm_KaoHeMemsAllotChangeList ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeMemsAllotChangeList ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeMemsAllotChangeList T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeMemsAllotChangeList";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

