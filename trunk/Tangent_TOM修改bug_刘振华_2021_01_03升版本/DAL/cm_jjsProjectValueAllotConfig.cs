﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.DBUtility;
using System.Data.SqlClient;

namespace TG.DAL
{
    [Serializable]
    public class cm_jjsProjectValueAllotConfig
    {
        public cm_jjsProjectValueAllotConfig()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_jjsProjectValueAllotConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_jjsProjectValueAllotConfig(");
            strSql.Append("typeStatus,typeDetail,ProofreadPercent,BuildingPercent,StructurePercent,DrainPercent,HavcPercent,ElectricPercent)");
            strSql.Append(" values (");
            strSql.Append("@typeStatus,@typeDetail,@ProofreadPercent,@BuildingPercent,@StructurePercent,@DrainPercent,@HavcPercent,@ElectricPercent)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@typeStatus", SqlDbType.Int,4),
					new SqlParameter("@typeDetail", SqlDbType.NVarChar,50),
					new SqlParameter("@ProofreadPercent", SqlDbType.Decimal,9),
					new SqlParameter("@BuildingPercent", SqlDbType.Decimal,9),
					new SqlParameter("@StructurePercent", SqlDbType.Decimal,9),
					new SqlParameter("@DrainPercent", SqlDbType.Decimal,9),
					new SqlParameter("@HavcPercent", SqlDbType.Decimal,9),
					new SqlParameter("@ElectricPercent", SqlDbType.Decimal,9)};
            parameters[0].Value = model.typeStatus;
            parameters[1].Value = model.typeDetail;
            parameters[2].Value = model.ProofreadPercent;
            parameters[3].Value = model.BuildingPercent;
            parameters[4].Value = model.StructurePercent;
            parameters[5].Value = model.DrainPercent;
            parameters[6].Value = model.HavcPercent;
            parameters[7].Value = model.ElectricPercent;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_jjsProjectValueAllotConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_jjsProjectValueAllotConfig set ");
            strSql.Append("typeStatus=@typeStatus,");
            strSql.Append("typeDetail=@typeDetail,");
            strSql.Append("ProofreadPercent=@ProofreadPercent,");
            strSql.Append("BuildingPercent=@BuildingPercent,");
            strSql.Append("StructurePercent=@StructurePercent,");
            strSql.Append("DrainPercent=@DrainPercent,");
            strSql.Append("HavcPercent=@HavcPercent,");
            strSql.Append("ElectricPercent=@ElectricPercent,");
            strSql.Append("TotalBuildingPercent=@TotalBuildingPercent,");
            strSql.Append("TotalInstallationPercent=@TotalInstallationPercent");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@typeStatus", SqlDbType.Int,4),
					new SqlParameter("@typeDetail", SqlDbType.NVarChar,50),
					new SqlParameter("@ProofreadPercent", SqlDbType.Decimal,9),
					new SqlParameter("@BuildingPercent", SqlDbType.Decimal,9),
					new SqlParameter("@StructurePercent", SqlDbType.Decimal,9),
					new SqlParameter("@DrainPercent", SqlDbType.Decimal,9),
					new SqlParameter("@HavcPercent", SqlDbType.Decimal,9),
					new SqlParameter("@ElectricPercent", SqlDbType.Decimal,9),
                    new SqlParameter("@TotalBuildingPercent", SqlDbType.Decimal,9),
                    new SqlParameter("@TotalInstallationPercent", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.typeStatus;
            parameters[1].Value = model.typeDetail;
            parameters[2].Value = model.ProofreadPercent;
            parameters[3].Value = model.BuildingPercent;
            parameters[4].Value = model.StructurePercent;
            parameters[5].Value = model.DrainPercent;
            parameters[6].Value = model.HavcPercent;
            parameters[7].Value = model.ElectricPercent;
            parameters[8].Value = model.Totalbuildingpercent;
            parameters[9].Value = model.TotalInstallationpercent;
            parameters[10].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_jjsProjectValueAllotConfig ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_jjsProjectValueAllotConfig GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,typeStatus,typeDetail,ProofreadPercent,BuildingPercent,StructurePercent,DrainPercent,HavcPercent,ElectricPercent from cm_jjsProjectValueAllotConfig ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_jjsProjectValueAllotConfig model = new TG.Model.cm_jjsProjectValueAllotConfig();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["typeStatus"] != null && ds.Tables[0].Rows[0]["typeStatus"].ToString() != "")
                {
                    model.typeStatus = int.Parse(ds.Tables[0].Rows[0]["typeStatus"].ToString());
                }
                if (ds.Tables[0].Rows[0]["typeDetail"] != null && ds.Tables[0].Rows[0]["typeDetail"].ToString() != "")
                {
                    model.typeDetail = ds.Tables[0].Rows[0]["typeDetail"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProofreadPercent"] != null && ds.Tables[0].Rows[0]["ProofreadPercent"].ToString() != "")
                {
                    model.ProofreadPercent = decimal.Parse(ds.Tables[0].Rows[0]["ProofreadPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BuildingPercent"] != null && ds.Tables[0].Rows[0]["BuildingPercent"].ToString() != "")
                {
                    model.BuildingPercent = decimal.Parse(ds.Tables[0].Rows[0]["BuildingPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["StructurePercent"] != null && ds.Tables[0].Rows[0]["StructurePercent"].ToString() != "")
                {
                    model.StructurePercent = decimal.Parse(ds.Tables[0].Rows[0]["StructurePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DrainPercent"] != null && ds.Tables[0].Rows[0]["DrainPercent"].ToString() != "")
                {
                    model.DrainPercent = decimal.Parse(ds.Tables[0].Rows[0]["DrainPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HavcPercent"] != null && ds.Tables[0].Rows[0]["HavcPercent"].ToString() != "")
                {
                    model.HavcPercent = decimal.Parse(ds.Tables[0].Rows[0]["HavcPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ElectricPercent"] != null && ds.Tables[0].Rows[0]["ElectricPercent"].ToString() != "")
                {
                    model.ElectricPercent = decimal.Parse(ds.Tables[0].Rows[0]["ElectricPercent"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_jjsProjectValueAllotConfig GetModelByType(int type)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from cm_jjsProjectValueAllotConfig ");
            strSql.Append(" where typeStatus=@type");
            SqlParameter[] parameters = {
					new SqlParameter("@type", SqlDbType.Int,4)
			};
            parameters[0].Value = type;

            TG.Model.cm_jjsProjectValueAllotConfig model = new TG.Model.cm_jjsProjectValueAllotConfig();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["typeStatus"] != null && ds.Tables[0].Rows[0]["typeStatus"].ToString() != "")
                {
                    model.typeStatus = int.Parse(ds.Tables[0].Rows[0]["typeStatus"].ToString());
                }
                if (ds.Tables[0].Rows[0]["typeDetail"] != null && ds.Tables[0].Rows[0]["typeDetail"].ToString() != "")
                {
                    model.typeDetail = ds.Tables[0].Rows[0]["typeDetail"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProofreadPercent"] != null && ds.Tables[0].Rows[0]["ProofreadPercent"].ToString() != "")
                {
                    model.ProofreadPercent = decimal.Parse(ds.Tables[0].Rows[0]["ProofreadPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BuildingPercent"] != null && ds.Tables[0].Rows[0]["BuildingPercent"].ToString() != "")
                {
                    model.BuildingPercent = decimal.Parse(ds.Tables[0].Rows[0]["BuildingPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["StructurePercent"] != null && ds.Tables[0].Rows[0]["StructurePercent"].ToString() != "")
                {
                    model.StructurePercent = decimal.Parse(ds.Tables[0].Rows[0]["StructurePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DrainPercent"] != null && ds.Tables[0].Rows[0]["DrainPercent"].ToString() != "")
                {
                    model.DrainPercent = decimal.Parse(ds.Tables[0].Rows[0]["DrainPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HavcPercent"] != null && ds.Tables[0].Rows[0]["HavcPercent"].ToString() != "")
                {
                    model.HavcPercent = decimal.Parse(ds.Tables[0].Rows[0]["HavcPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ElectricPercent"] != null && ds.Tables[0].Rows[0]["ElectricPercent"].ToString() != "")
                {
                    model.ElectricPercent = decimal.Parse(ds.Tables[0].Rows[0]["ElectricPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalInstallationPercent"] != null && ds.Tables[0].Rows[0]["TotalInstallationPercent"].ToString() != "")
                {
                    model.TotalInstallationpercent = decimal.Parse(ds.Tables[0].Rows[0]["TotalInstallationPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalBuildingPercent"] != null && ds.Tables[0].Rows[0]["TotalBuildingPercent"].ToString() != "")
                {
                    model.Totalbuildingpercent = decimal.Parse(ds.Tables[0].Rows[0]["TotalBuildingPercent"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

      
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_jjsProjectValueAllotDetail GetDetailModel(int proID, int AllotID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from cm_jjsProjectValueAllotDetail ");
            strSql.Append(" where proID=@proID");
            if (AllotID != 0)
            {
                strSql.Append(" AND AllotID="+AllotID+"");
            }
            SqlParameter[] parameters = {
					new SqlParameter("@proID", SqlDbType.Int,4)
			};
            parameters[0].Value = proID;

            TG.Model.cm_jjsProjectValueAllotDetail model = new TG.Model.cm_jjsProjectValueAllotDetail();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["proID"] != null && ds.Tables[0].Rows[0]["proID"].ToString() != "")
                {
                    model.proID = int.Parse(ds.Tables[0].Rows[0]["proID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotID"] != null && ds.Tables[0].Rows[0]["AllotID"].ToString() != "")
                {
                    model.AllotID = int.Parse(ds.Tables[0].Rows[0]["AllotID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["typeStatus"] != null && ds.Tables[0].Rows[0]["typeStatus"].ToString() != "")
                {
                    model.typeStatus = int.Parse(ds.Tables[0].Rows[0]["typeStatus"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProofreadPercent"] != null && ds.Tables[0].Rows[0]["ProofreadPercent"].ToString() != "")
                {
                    model.ProofreadPercent = decimal.Parse(ds.Tables[0].Rows[0]["ProofreadPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProofreadCount"] != null && ds.Tables[0].Rows[0]["ProofreadCount"].ToString() != "")
                {
                    model.ProofreadCount = decimal.Parse(ds.Tables[0].Rows[0]["ProofreadCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BuildingPercent"] != null && ds.Tables[0].Rows[0]["BuildingPercent"].ToString() != "")
                {
                    model.BuildingPercent = decimal.Parse(ds.Tables[0].Rows[0]["BuildingPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BuildingCount"] != null && ds.Tables[0].Rows[0]["BuildingCount"].ToString() != "")
                {
                    model.BuildingCount = decimal.Parse(ds.Tables[0].Rows[0]["BuildingCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["StructurePercent"] != null && ds.Tables[0].Rows[0]["StructurePercent"].ToString() != "")
                {
                    model.StructurePercent = decimal.Parse(ds.Tables[0].Rows[0]["StructurePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["StructureCount"] != null && ds.Tables[0].Rows[0]["StructureCount"].ToString() != "")
                {
                    model.StructureCount = decimal.Parse(ds.Tables[0].Rows[0]["StructureCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DrainPercent"] != null && ds.Tables[0].Rows[0]["DrainPercent"].ToString() != "")
                {
                    model.DrainPercent = decimal.Parse(ds.Tables[0].Rows[0]["DrainPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DrainCount"] != null && ds.Tables[0].Rows[0]["DrainCount"].ToString() != "")
                {
                    model.DrainCount = decimal.Parse(ds.Tables[0].Rows[0]["DrainCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HavcPercent"] != null && ds.Tables[0].Rows[0]["HavcPercent"].ToString() != "")
                {
                    model.HavcPercent = decimal.Parse(ds.Tables[0].Rows[0]["HavcPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HavcCount"] != null && ds.Tables[0].Rows[0]["HavcCount"].ToString() != "")
                {
                    model.HavcCount = decimal.Parse(ds.Tables[0].Rows[0]["HavcCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ElectricPercent"] != null && ds.Tables[0].Rows[0]["ElectricPercent"].ToString() != "")
                {
                    model.ElectricPercent = decimal.Parse(ds.Tables[0].Rows[0]["ElectricPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ElectricCount"] != null && ds.Tables[0].Rows[0]["ElectricCount"].ToString() != "")
                {
                    model.ElectricCount = decimal.Parse(ds.Tables[0].Rows[0]["ElectricCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalInstallationPercent"] != null && ds.Tables[0].Rows[0]["TotalInstallationPercent"].ToString() != "")
                {
                    model.TotalInstallationpercent = decimal.Parse(ds.Tables[0].Rows[0]["TotalInstallationPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalBuildingPercent"] != null && ds.Tables[0].Rows[0]["TotalBuildingPercent"].ToString() != "")
                {
                    model.Totalbuildingpercent = decimal.Parse(ds.Tables[0].Rows[0]["TotalBuildingPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalBuildingCount"] != null && ds.Tables[0].Rows[0]["TotalBuildingCount"].ToString() != "")
                {
                    model.Totalbuildingcount = decimal.Parse(ds.Tables[0].Rows[0]["TotalBuildingCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalInstallationCount"] != null && ds.Tables[0].Rows[0]["TotalInstallationCount"].ToString() != "")
                {
                    model.TotalInstallationcount = decimal.Parse(ds.Tables[0].Rows[0]["TotalInstallationCount"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM cm_jjsProjectValueAllotConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        #endregion  Method
    }
}
