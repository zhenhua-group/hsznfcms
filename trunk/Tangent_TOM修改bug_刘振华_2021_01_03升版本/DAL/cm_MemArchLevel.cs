﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_MemArchLevel
	/// </summary>
	public partial class cm_MemArchLevel
	{
		public cm_MemArchLevel()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_MemArchLevel model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.Name != null)
			{
				strSql1.Append("Name,");
				strSql2.Append("'"+model.Name+"',");
			}
			if (model.AllotPrt != null)
			{
				strSql1.Append("AllotPrt,");
				strSql2.Append(""+model.AllotPrt+",");
			}
			if (model.LevelOrder != null)
			{
				strSql1.Append("LevelOrder,");
				strSql2.Append(""+model.LevelOrder+",");
			}
			if (model.Info != null)
			{
				strSql1.Append("Info,");
				strSql2.Append("'"+model.Info+"',");
			}
			strSql.Append("insert into cm_MemArchLevel(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_MemArchLevel model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_MemArchLevel set ");
			if (model.Name != null)
			{
				strSql.Append("Name='"+model.Name+"',");
			}
			if (model.AllotPrt != null)
			{
				strSql.Append("AllotPrt="+model.AllotPrt+",");
			}
			else
			{
				strSql.Append("AllotPrt= null ,");
			}
			if (model.LevelOrder != null)
			{
				strSql.Append("LevelOrder="+model.LevelOrder+",");
			}
			else
			{
				strSql.Append("LevelOrder= null ,");
			}
			if (model.Info != null)
			{
				strSql.Append("Info='"+model.Info+"',");
			}
			else
			{
				strSql.Append("Info= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ArchLevel_ID="+ model.ArchLevel_ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ArchLevel_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_MemArchLevel ");
			strSql.Append(" where ArchLevel_ID="+ArchLevel_ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string ArchLevel_IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_MemArchLevel ");
			strSql.Append(" where ArchLevel_ID in ("+ArchLevel_IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_MemArchLevel GetModel(int ArchLevel_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" ArchLevel_ID,Name,AllotPrt,LevelOrder,Info ");
			strSql.Append(" from cm_MemArchLevel ");
			strSql.Append(" where ArchLevel_ID="+ArchLevel_ID+"" );
			TG.Model.cm_MemArchLevel model=new TG.Model.cm_MemArchLevel();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_MemArchLevel DataRowToModel(DataRow row)
		{
			TG.Model.cm_MemArchLevel model=new TG.Model.cm_MemArchLevel();
			if (row != null)
			{
				if(row["ArchLevel_ID"]!=null && row["ArchLevel_ID"].ToString()!="")
				{
					model.ArchLevel_ID=int.Parse(row["ArchLevel_ID"].ToString());
				}
				if(row["Name"]!=null)
				{
					model.Name=row["Name"].ToString();
				}
				if(row["AllotPrt"]!=null && row["AllotPrt"].ToString()!="")
				{
					model.AllotPrt=decimal.Parse(row["AllotPrt"].ToString());
				}
				if(row["LevelOrder"]!=null && row["LevelOrder"].ToString()!="")
				{
					model.LevelOrder=int.Parse(row["LevelOrder"].ToString());
				}
				if(row["Info"]!=null)
				{
					model.Info=row["Info"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ArchLevel_ID,Name,AllotPrt,LevelOrder,Info ");
			strSql.Append(" FROM cm_MemArchLevel ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ArchLevel_ID,Name,AllotPrt,LevelOrder,Info ");
			strSql.Append(" FROM cm_MemArchLevel ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_MemArchLevel ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            StringBuilder sb = new StringBuilder();
            sb.Append("select [ArchLevel_ID],[Name],[AllotPrt],[LevelOrder],[Info] ");
            sb.Append(" FROM cm_MemArchLevel ");
            if (strWhere.Trim() != "")
            {
                sb.Append(" where " + strWhere);
            }
            if (orderby.Trim() != "")
            {
                sb.Append(" Order by " + orderby);
            }
            return DbHelperSQL.Query(sb.ToString(), startIndex, endIndex);
		}

		/*
		*/

		#endregion  Method
		#region  MethodEx

		#endregion  MethodEx
	}
}

