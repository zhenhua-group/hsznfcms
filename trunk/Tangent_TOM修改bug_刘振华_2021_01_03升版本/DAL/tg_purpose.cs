﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:tg_purpose
	/// </summary>
	public partial class tg_purpose
	{
		public tg_purpose()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TG.Model.tg_purpose model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.purpose_ID != null)
			{
				strSql1.Append("purpose_ID,");
				strSql2.Append(""+model.purpose_ID+",");
			}
			if (model.purpose_Name != null)
			{
				strSql1.Append("purpose_Name,");
				strSql2.Append("'"+model.purpose_Name+"',");
			}
			if (model.folder != null)
			{
				strSql1.Append("folder,");
				strSql2.Append(""+model.folder+",");
			}
			strSql.Append("insert into tg_purpose(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_purpose model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update tg_purpose set ");
			if (model.purpose_ID != null)
			{
				strSql.Append("purpose_ID="+model.purpose_ID+",");
			}
			if (model.purpose_Name != null)
			{
				strSql.Append("purpose_Name='"+model.purpose_Name+"',");
			}
			if (model.folder != null)
			{
				strSql.Append("folder="+model.folder+",");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_purpose ");
			strSql.Append(" where " );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_purpose GetModel()
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" purpose_ID,purpose_Name,folder ");
			strSql.Append(" from tg_purpose ");
			strSql.Append(" where " );
			TG.Model.tg_purpose model=new TG.Model.tg_purpose();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["purpose_ID"]!=null && ds.Tables[0].Rows[0]["purpose_ID"].ToString()!="")
				{
					model.purpose_ID=int.Parse(ds.Tables[0].Rows[0]["purpose_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["purpose_Name"]!=null && ds.Tables[0].Rows[0]["purpose_Name"].ToString()!="")
				{
					model.purpose_Name=ds.Tables[0].Rows[0]["purpose_Name"].ToString();
				}
				if(ds.Tables[0].Rows[0]["folder"]!=null && ds.Tables[0].Rows[0]["folder"].ToString()!="")
				{
					model.folder=int.Parse(ds.Tables[0].Rows[0]["folder"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select purpose_ID,purpose_Name,folder ");
			strSql.Append(" FROM tg_purpose ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" purpose_ID,purpose_Name,folder ");
			strSql.Append(" FROM tg_purpose ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM tg_purpose ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.pro_ID desc");
			}
			strSql.Append(")AS Row, T.*  from tg_purpose T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

