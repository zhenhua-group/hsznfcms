﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using System.Collections.Generic;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:tg_member
    /// </summary>
    public partial class tg_member
    {
        public tg_member()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("mem_ID", "tg_member");
        }


        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int mem_ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from tg_member");
            strSql.Append(" where mem_ID=" + mem_ID + " ");
            return DbHelperSQL.Exists(strSql.ToString());
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.tg_member model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.mem_Login != null)
            {
                strSql1.Append("mem_Login,");
                strSql2.Append("'" + model.mem_Login + "',");
            }
            if (model.mem_Name != null)
            {
                strSql1.Append("mem_Name,");
                strSql2.Append("'" + model.mem_Name + "',");
            }
            if (model.mem_Password != null)
            {
                strSql1.Append("mem_Password,");
                strSql2.Append("'" + model.mem_Password + "',");
            }
            if (model.mem_Birthday != null)
            {
                strSql1.Append("mem_Birthday,");
                strSql2.Append("'" + model.mem_Birthday + "',");
            }
            if (model.mem_Sex != null)
            {
                strSql1.Append("mem_Sex,");
                strSql2.Append("'" + model.mem_Sex + "',");
            }
            if (model.mem_Unit_ID != null)
            {
                strSql1.Append("mem_Unit_ID,");
                strSql2.Append("" + model.mem_Unit_ID + ",");
            }
            if (model.mem_Speciality_ID != null)
            {
                strSql1.Append("mem_Speciality_ID,");
                strSql2.Append("" + model.mem_Speciality_ID + ",");
            }
            if (model.mem_Principalship_ID != null)
            {
                strSql1.Append("mem_Principalship_ID,");
                strSql2.Append("" + model.mem_Principalship_ID + ",");
            }
            if (model.mem_Telephone != null)
            {
                strSql1.Append("mem_Telephone,");
                strSql2.Append("'" + model.mem_Telephone + "',");
            }
            if (model.mem_Mobile != null)
            {
                strSql1.Append("mem_Mobile,");
                strSql2.Append("'" + model.mem_Mobile + "',");
            }
            if (model.mem_RegTime != null)
            {
                strSql1.Append("mem_RegTime,");
                strSql2.Append("'" + model.mem_RegTime + "',");
            }
            if (model.mem_OutTime != null)
            {
                strSql1.Append("mem_OutTime,");
                strSql2.Append("'" + model.mem_OutTime + "',");
            }
            if (model.mem_Status != null)
            {
                strSql1.Append("mem_Status,");
                strSql2.Append("" + model.mem_Status + ",");
            }
            if (model.mem_Stats != null)
            {
                strSql1.Append("mem_Stats,");
                strSql2.Append("'" + model.mem_Stats + "',");
            }
            if (model.mem_LastIP != null)
            {
                strSql1.Append("mem_LastIP,");
                strSql2.Append("'" + model.mem_LastIP + "',");
            }
            if (model.mem_HashKey != null)
            {
                strSql1.Append("mem_HashKey,");
                strSql2.Append("'" + model.mem_HashKey + "',");
            }
            if (model.mem_PostMessage != null)
            {
                strSql1.Append("mem_PostMessage,");
                strSql2.Append("" + model.mem_PostMessage + ",");
            }
            if (model.mem_Mailbox != null)
            {
                strSql1.Append("mem_Mailbox,");
                strSql2.Append("'" + model.mem_Mailbox + "',");
            }
            if (model.mem_TPMlastver != null)
            {
                strSql1.Append("mem_TPMlastver,");
                strSql2.Append("'" + model.mem_TPMlastver + "',");
            }
            if (model.mem_isFired != null)
            {
                strSql1.Append("mem_isFired,");
                strSql2.Append("" + model.mem_isFired + ",");
            }
            if (model.mem_sign != null)
            {
                strSql1.Append("mem_sign,");
                strSql2.Append("'" + model.mem_sign + "',");
            }
            if (model.mem_SignPwd != null)
            {
                strSql1.Append("mem_SignPwd,");
                strSql2.Append("'" + model.mem_SignPwd + "',");
            }
            if (model.mem_Stamp != null)
            {
                strSql1.Append("mem_Stamp,");
                strSql2.Append("'" + model.mem_Stamp + "',");
            }
            if (model.mem_IsManager != null)
            {
                strSql1.Append("mem_IsManager,");
                strSql2.Append("" + model.mem_IsManager + ",");
            }
            if (model.mem_IsProManager != null)
            {
                strSql1.Append("mem_IsProManager,");
                strSql2.Append("" + model.mem_IsProManager + ",");
            }
            if (model.mem_Hometown != null)
            {
                strSql1.Append("mem_Hometown,");
                strSql2.Append("'" + model.mem_Hometown + "',");
            }
            if (model.mem_GraduationTime != null)
            {
                strSql1.Append("mem_GraduationTime,");
                strSql2.Append("'" + model.mem_GraduationTime + "',");
            }
            if (model.mem_GraduationSchool != null)
            {
                strSql1.Append("mem_GraduationSchool,");
                strSql2.Append("'" + model.mem_GraduationSchool + "',");
            }
            if (model.mem_Title != null)
            {
                strSql1.Append("mem_Title,");
                strSql2.Append("'" + model.mem_Title + "',");
            }
            if (model.mem_Major != null)
            {
                strSql1.Append("mem_Major,");
                strSql2.Append("'" + model.mem_Major + "',");
            }
            if (model.mem_Remark != null)
            {
                strSql1.Append("mem_Remark,");
                strSql2.Append("'" + model.mem_Remark + "',");
            }
            if (model.mem_LastHostName != null)
            {
                strSql1.Append("mem_LastHostName,");
                strSql2.Append("'" + model.mem_LastHostName + "',");
            }
            if (model.mem_Order != null)
            {
                strSql1.Append("mem_Order,");
                strSql2.Append("" + model.mem_Order + ",");
            }
            strSql.Append("insert into tg_member(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.tg_member model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update tg_member set ");
            if (model.mem_Login != null)
            {
                strSql.Append("mem_Login='" + model.mem_Login + "',");
            }
            if (model.mem_Name != null)
            {
                strSql.Append("mem_Name='" + model.mem_Name + "',");
            }
            if (model.mem_Password != null)
            {
                strSql.Append("mem_Password='" + model.mem_Password + "',");
            }
            if (model.mem_Birthday != null)
            {
                strSql.Append("mem_Birthday='" + model.mem_Birthday + "',");
            }
            else
            {
                strSql.Append("mem_Birthday= null ,");
            }
            if (model.mem_Sex != null)
            {
                strSql.Append("mem_Sex='" + model.mem_Sex + "',");
            }
            else
            {
                strSql.Append("mem_Sex= null ,");
            }
            if (model.mem_Unit_ID != null)
            {
                strSql.Append("mem_Unit_ID=" + model.mem_Unit_ID + ",");
            }
            if (model.mem_Speciality_ID != null)
            {
                strSql.Append("mem_Speciality_ID=" + model.mem_Speciality_ID + ",");
            }
            if (model.mem_Principalship_ID != null)
            {
                strSql.Append("mem_Principalship_ID=" + model.mem_Principalship_ID + ",");
            }
            if (model.mem_Telephone != null)
            {
                strSql.Append("mem_Telephone='" + model.mem_Telephone + "',");
            }
            else
            {
                strSql.Append("mem_Telephone= null ,");
            }
            if (model.mem_Mobile != null)
            {
                strSql.Append("mem_Mobile='" + model.mem_Mobile + "',");
            }
            else
            {
                strSql.Append("mem_Mobile= null ,");
            }
            if (model.mem_RegTime != null)
            {
                strSql.Append("mem_RegTime='" + model.mem_RegTime + "',");
            }
            else
            {
                strSql.Append("mem_RegTime= null ,");
            }
            if (model.mem_OutTime != null)
            {
                strSql.Append("mem_OutTime='" + model.mem_OutTime + "',");
            }
            else
            {
                strSql.Append("mem_OutTime= null ,");
            }
            if (model.mem_Status != null)
            {
                strSql.Append("mem_Status=" + model.mem_Status + ",");
            }
            else
            {
                strSql.Append("mem_Status= null ,");
            }
            if (model.mem_Stats != null)
            {
                strSql.Append("mem_Stats='" + model.mem_Stats + "',");
            }
            else
            {
                strSql.Append("mem_Stats= null ,");
            }
            if (model.mem_LastIP != null)
            {
                strSql.Append("mem_LastIP='" + model.mem_LastIP + "',");
            }
            else
            {
                strSql.Append("mem_LastIP= null ,");
            }
            if (model.mem_HashKey != null)
            {
                strSql.Append("mem_HashKey='" + model.mem_HashKey + "',");
            }
            else
            {
                strSql.Append("mem_HashKey= null ,");
            }
            if (model.mem_PostMessage != null)
            {
                strSql.Append("mem_PostMessage=" + model.mem_PostMessage + ",");
            }
            else
            {
                strSql.Append("mem_PostMessage= null ,");
            }
            if (model.mem_Mailbox != null)
            {
                strSql.Append("mem_Mailbox='" + model.mem_Mailbox + "',");
            }
            else
            {
                strSql.Append("mem_Mailbox= null ,");
            }
            if (model.mem_TPMlastver != null)
            {
                strSql.Append("mem_TPMlastver='" + model.mem_TPMlastver + "',");
            }
            else
            {
                strSql.Append("mem_TPMlastver= null ,");
            }
            if (model.mem_isFired != null)
            {
                strSql.Append("mem_isFired=" + model.mem_isFired + ",");
            }
            else
            {
                strSql.Append("mem_isFired= null ,");
            }
            if (model.mem_sign != null)
            {
                strSql.Append("mem_sign='" + model.mem_sign + "',");
            }
            else
            {
                strSql.Append("mem_sign= null ,");
            }
            if (model.mem_SignPwd != null)
            {
                strSql.Append("mem_SignPwd='" + model.mem_SignPwd + "',");
            }
            else
            {
                strSql.Append("mem_SignPwd= null ,");
            }
            if (model.mem_Stamp != null)
            {
                strSql.Append("mem_Stamp='" + model.mem_Stamp + "',");
            }
            else
            {
                strSql.Append("mem_Stamp= null ,");
            }
            if (model.mem_IsManager != null)
            {
                strSql.Append("mem_IsManager=" + model.mem_IsManager + ",");
            }
            else
            {
                strSql.Append("mem_IsManager= null ,");
            }
            if (model.mem_IsProManager != null)
            {
                strSql.Append("mem_IsProManager=" + model.mem_IsProManager + ",");
            }
            else
            {
                strSql.Append("mem_IsProManager= null ,");
            }
            if (model.mem_Hometown != null)
            {
                strSql.Append("mem_Hometown='" + model.mem_Hometown + "',");
            }
            else
            {
                strSql.Append("mem_Hometown= null ,");
            }
            if (model.mem_GraduationTime != null)
            {
                strSql.Append("mem_GraduationTime='" + model.mem_GraduationTime + "',");
            }
            else
            {
                strSql.Append("mem_GraduationTime= null ,");
            }
            if (model.mem_GraduationSchool != null)
            {
                strSql.Append("mem_GraduationSchool='" + model.mem_GraduationSchool + "',");
            }
            else
            {
                strSql.Append("mem_GraduationSchool= null ,");
            }
            if (model.mem_Title != null)
            {
                strSql.Append("mem_Title='" + model.mem_Title + "',");
            }
            else
            {
                strSql.Append("mem_Title= null ,");
            }
            if (model.mem_Major != null)
            {
                strSql.Append("mem_Major='" + model.mem_Major + "',");
            }
            else
            {
                strSql.Append("mem_Major= null ,");
            }
            if (model.mem_Remark != null)
            {
                strSql.Append("mem_Remark='" + model.mem_Remark + "',");
            }
            else
            {
                strSql.Append("mem_Remark= null ,");
            }
            if (model.mem_LastHostName != null)
            {
                strSql.Append("mem_LastHostName='" + model.mem_LastHostName + "',");
            }
            else
            {
                strSql.Append("mem_LastHostName= null ,");
            }
            //人员排序 2017年4月18日
            if (model.mem_Order != null)
            {
                strSql.Append("mem_Order=" + model.mem_Order + ",");
            }
            else
            {
                strSql.Append("mem_Order= 0 ,");
            }


            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where mem_ID=" + model.mem_ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int mem_ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tg_member ");
            strSql.Append(" where mem_ID=" + mem_ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string mem_IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tg_member ");
            strSql.Append(" where mem_ID in (" + mem_IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.tg_member GetModel(int mem_ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" mem_ID,mem_Login,mem_Name,mem_Password,mem_Birthday,mem_Sex,mem_Unit_ID,mem_Speciality_ID,mem_Principalship_ID,mem_Telephone,mem_Mobile,mem_RegTime,mem_OutTime,mem_Status,mem_Stats,mem_LastIP,mem_HashKey,mem_PostMessage,mem_Mailbox,mem_TPMlastver,mem_isFired,mem_sign,mem_SignPwd,mem_Stamp,mem_IsManager,mem_IsProManager,mem_Hometown,mem_GraduationTime,mem_GraduationSchool,mem_Title,mem_Major,mem_Remark,mem_LastHostName,mem_Order ");
            strSql.Append(" from tg_member ");
            strSql.Append(" where mem_ID=" + mem_ID + "");
            TG.Model.tg_member model = new TG.Model.tg_member();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["mem_ID"] != null && ds.Tables[0].Rows[0]["mem_ID"].ToString() != "")
                {
                    model.mem_ID = int.Parse(ds.Tables[0].Rows[0]["mem_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_Login"] != null && ds.Tables[0].Rows[0]["mem_Login"].ToString() != "")
                {
                    model.mem_Login = ds.Tables[0].Rows[0]["mem_Login"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_Name"] != null && ds.Tables[0].Rows[0]["mem_Name"].ToString() != "")
                {
                    model.mem_Name = ds.Tables[0].Rows[0]["mem_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_Password"] != null && ds.Tables[0].Rows[0]["mem_Password"].ToString() != "")
                {
                    model.mem_Password = ds.Tables[0].Rows[0]["mem_Password"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_Birthday"] != null && ds.Tables[0].Rows[0]["mem_Birthday"].ToString() != "")
                {
                    model.mem_Birthday = DateTime.Parse(ds.Tables[0].Rows[0]["mem_Birthday"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_Sex"] != null && ds.Tables[0].Rows[0]["mem_Sex"].ToString() != "")
                {
                    model.mem_Sex = ds.Tables[0].Rows[0]["mem_Sex"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_Unit_ID"] != null && ds.Tables[0].Rows[0]["mem_Unit_ID"].ToString() != "")
                {
                    model.mem_Unit_ID = int.Parse(ds.Tables[0].Rows[0]["mem_Unit_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_Speciality_ID"] != null && ds.Tables[0].Rows[0]["mem_Speciality_ID"].ToString() != "")
                {
                    model.mem_Speciality_ID = int.Parse(ds.Tables[0].Rows[0]["mem_Speciality_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_Principalship_ID"] != null && ds.Tables[0].Rows[0]["mem_Principalship_ID"].ToString() != "")
                {
                    model.mem_Principalship_ID = int.Parse(ds.Tables[0].Rows[0]["mem_Principalship_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_Telephone"] != null && ds.Tables[0].Rows[0]["mem_Telephone"].ToString() != "")
                {
                    model.mem_Telephone = ds.Tables[0].Rows[0]["mem_Telephone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_Mobile"] != null && ds.Tables[0].Rows[0]["mem_Mobile"].ToString() != "")
                {
                    model.mem_Mobile = ds.Tables[0].Rows[0]["mem_Mobile"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_RegTime"] != null && ds.Tables[0].Rows[0]["mem_RegTime"].ToString() != "")
                {
                    model.mem_RegTime = DateTime.Parse(ds.Tables[0].Rows[0]["mem_RegTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_OutTime"] != null && ds.Tables[0].Rows[0]["mem_OutTime"].ToString() != "")
                {
                    model.mem_OutTime = DateTime.Parse(ds.Tables[0].Rows[0]["mem_OutTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_Status"] != null && ds.Tables[0].Rows[0]["mem_Status"].ToString() != "")
                {
                    model.mem_Status = int.Parse(ds.Tables[0].Rows[0]["mem_Status"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_Stats"] != null && ds.Tables[0].Rows[0]["mem_Stats"].ToString() != "")
                {
                    model.mem_Stats = ds.Tables[0].Rows[0]["mem_Stats"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_LastIP"] != null && ds.Tables[0].Rows[0]["mem_LastIP"].ToString() != "")
                {
                    model.mem_LastIP = ds.Tables[0].Rows[0]["mem_LastIP"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_HashKey"] != null && ds.Tables[0].Rows[0]["mem_HashKey"].ToString() != "")
                {
                    model.mem_HashKey = ds.Tables[0].Rows[0]["mem_HashKey"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_PostMessage"] != null && ds.Tables[0].Rows[0]["mem_PostMessage"].ToString() != "")
                {
                    model.mem_PostMessage = int.Parse(ds.Tables[0].Rows[0]["mem_PostMessage"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_Mailbox"] != null && ds.Tables[0].Rows[0]["mem_Mailbox"].ToString() != "")
                {
                    model.mem_Mailbox = ds.Tables[0].Rows[0]["mem_Mailbox"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_TPMlastver"] != null && ds.Tables[0].Rows[0]["mem_TPMlastver"].ToString() != "")
                {
                    model.mem_TPMlastver = ds.Tables[0].Rows[0]["mem_TPMlastver"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_isFired"] != null && ds.Tables[0].Rows[0]["mem_isFired"].ToString() != "")
                {
                    model.mem_isFired = int.Parse(ds.Tables[0].Rows[0]["mem_isFired"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_sign"] != null && ds.Tables[0].Rows[0]["mem_sign"].ToString() != "")
                {
                    model.mem_sign = ds.Tables[0].Rows[0]["mem_sign"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_SignPwd"] != null && ds.Tables[0].Rows[0]["mem_SignPwd"].ToString() != "")
                {
                    model.mem_SignPwd = ds.Tables[0].Rows[0]["mem_SignPwd"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_Stamp"] != null && ds.Tables[0].Rows[0]["mem_Stamp"].ToString() != "")
                {
                    model.mem_Stamp = ds.Tables[0].Rows[0]["mem_Stamp"].ToString();
                }
                //2011 新加字段
                if (ds.Tables[0].Rows[0]["mem_IsManager"] != null && ds.Tables[0].Rows[0]["mem_IsManager"].ToString() != "")
                {
                    model.mem_IsManager = int.Parse(ds.Tables[0].Rows[0]["mem_IsManager"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_IsProManager"] != null && ds.Tables[0].Rows[0]["mem_IsProManager"].ToString() != "")
                {
                    model.mem_IsProManager = int.Parse(ds.Tables[0].Rows[0]["mem_IsProManager"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_Hometown"] != null && ds.Tables[0].Rows[0]["mem_Hometown"].ToString() != "")
                {
                    model.mem_Hometown = ds.Tables[0].Rows[0]["mem_Hometown"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_GraduationTime"] != null && ds.Tables[0].Rows[0]["mem_GraduationTime"].ToString() != "")
                {
                    model.mem_GraduationTime = ds.Tables[0].Rows[0]["mem_GraduationTime"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_GraduationSchool"] != null && ds.Tables[0].Rows[0]["mem_GraduationSchool"].ToString() != "")
                {
                    model.mem_GraduationSchool = ds.Tables[0].Rows[0]["mem_GraduationSchool"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_Title"] != null && ds.Tables[0].Rows[0]["mem_Title"].ToString() != "")
                {
                    model.mem_Title = ds.Tables[0].Rows[0]["mem_Title"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_Major"] != null && ds.Tables[0].Rows[0]["mem_Major"].ToString() != "")
                {
                    model.mem_Major = ds.Tables[0].Rows[0]["mem_Major"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_Remark"] != null && ds.Tables[0].Rows[0]["mem_Remark"].ToString() != "")
                {
                    model.mem_Remark = ds.Tables[0].Rows[0]["mem_Remark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mem_LastHostName"] != null && ds.Tables[0].Rows[0]["mem_LastHostName"].ToString() != "")
                {
                    model.mem_LastHostName = ds.Tables[0].Rows[0]["mem_LastHostName"].ToString();
                }
                //排序
                if (ds.Tables[0].Rows[0]["mem_Order"] != null && ds.Tables[0].Rows[0]["mem_Order"].ToString() != "")
                {
                    model.mem_Order = Convert.ToInt32(ds.Tables[0].Rows[0]["mem_Order"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select mem_ID,mem_Login,mem_Name,mem_Password,mem_Birthday,mem_Sex,mem_Unit_ID,mem_Speciality_ID,mem_Principalship_ID,mem_Telephone,mem_Mobile,mem_RegTime,mem_OutTime,mem_Status,mem_Stats,mem_LastIP,mem_HashKey,mem_PostMessage,mem_Mailbox,mem_TPMlastver,mem_isFired,mem_sign,mem_SignPwd,mem_Stamp,mem_IsManager,mem_IsProManager,mem_Hometown,mem_GraduationTime,mem_GraduationSchool,mem_Title,mem_Major,mem_Remark,mem_LastHostName,mem_Order ");
            strSql.Append(" FROM tg_member ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" mem_ID,mem_Login,mem_Name,mem_Password,mem_Birthday,mem_Sex,mem_Unit_ID,mem_Speciality_ID,mem_Principalship_ID,mem_Telephone,mem_Mobile,mem_RegTime,mem_OutTime,mem_Status,mem_Stats,mem_LastIP,mem_HashKey,mem_PostMessage,mem_Mailbox,mem_TPMlastver,mem_isFired,mem_sign,mem_SignPwd,mem_Stamp,mem_IsManager,mem_IsProManager,mem_Hometown,mem_GraduationTime,mem_GraduationSchool,mem_Title,mem_Major,mem_Remark,mem_LastHostName ");
            strSql.Append(" FROM tg_member ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        public List<TG.Model.tg_member> GetUsers(string whereSql)
        {
            string sql = "select mem_ID,mem_Login,mem_Name,mem_Password,mem_Birthday,mem_Sex,mem_Unit_ID,mem_Speciality_ID,mem_Principalship_ID,mem_Telephone,mem_Mobile,mem_RegTime,mem_OutTime,mem_Status,mem_Stats,mem_LastIP,mem_HashKey,mem_PostMessage,mem_Mailbox,mem_TPMlastver,mem_isFired,mem_sign,mem_SignPwd,mem_Stamp,mem_IsManager,mem_IsProManager,mem_Hometown,mem_GraduationTime,mem_GraduationSchool,mem_Title,mem_Major,mem_Remark,mem_LastHostName FROM tg_member where 1=1 " + whereSql;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<TG.Model.tg_member> resultList = new List<TG.Model.tg_member>();

            while (reader.Read())
            {
                TG.Model.tg_member user = new TG.Model.tg_member
                {
                    mem_ID = reader["mem_ID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["mem_ID"]),
                    mem_Login = reader["mem_Login"] == DBNull.Value ? "" : reader["mem_Login"].ToString(),
                    mem_Name = reader["mem_Name"] == DBNull.Value ? "" : reader["mem_Name"].ToString()
                };
                resultList.Add(user);
            }
            reader.Close();
            return resultList;
        }

        /// <summary>
        /// 查询所有部门
        /// </summary>
        /// <returns></returns>
        public List<TG.Model.tg_unit> GetDepartments()
        {
            string sql = "select unit_ID,unit_Name,unit_Intro from tg_unit";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<TG.Model.tg_unit> resultList = new List<TG.Model.tg_unit>();

            while (reader.Read())
            {
                TG.Model.tg_unit unit = new TG.Model.tg_unit
                {
                    unit_ID = reader["unit_ID"] == null ? 0 : Convert.ToInt32(reader["unit_ID"]),
                    unit_Name = reader["unit_Name"] == null ? "" : reader["unit_Name"].ToString()
                };
                resultList.Add(unit);
            }
            reader.Close();
            return resultList;
        }
        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM tg_member A ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        ///  数据量小的查询分页
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select A.* ");
            sb.Append(" ,B.RoleIdTec,B.RoleIdMag ");
            sb.Append(" ,isnull((select roleName from cm_RoleMag where ID=B.RoleIdMag),'') As MagName ");
            sb.Append(" ,isnull((select roleName from cm_RoleMag where ID=B.RoleIdTec),'') As TecName");
            sb.Append(" ,isnull(C.mem_InTime,convert(nvarchar,C.mem_InTime)) As mem_InTime");
            sb.Append(" ,isnull(C.mem_OutTime,convert(nvarchar,C.mem_OutTime)) As mem_OutTime2");
            sb.Append(" ,(Select TOP 1 unit_Name From tg_unit where unit_ID=B.mem_Unit_ID) As unitName2");
            sb.Append(" FROM tg_member A left join dbo.tg_memberRole B on A.mem_ID=B.mem_ID left join dbo.tg_memberExt C on A.mem_ID=C.mem_ID ");
            if (strWhere.Trim() != "")
            {
                sb.Append(" where " + strWhere);
            }
            if (orderby.Trim() != "")
            {
                sb.Append(" Order by " + orderby);
            }
            return DbHelperSQL.Query(sb.ToString(), startIndex, endIndex);
        }
        /// <summary>
        /// 分阶段获得数据
        /// </summary>
        /// <param name="strWhere">where语句</param>
        /// <param name="pagesize">每页数量</param>
        /// <param name="pageIndex">要得到第几页的数据</param>
        /// <returns>数据集</returns>
        public DataSet GetListByPage(string strWhere, int pagesize, int pageIndex)
        {
            StringBuilder sqlStr = new StringBuilder();
            int notInTop = (pageIndex - 1) * 10;
            //sqlStr.Append("select top " + pagesize + "* from tg_member where mem_ID not in ");
            //sqlStr.Append(" (select TOP " + notInTop + " mem_ID from tg_member ");
            //if (!string.IsNullOrEmpty(strWhere.Trim()))
            //{
            //    sqlStr.AppendFormat(" where ({0})", strWhere);
            //}
            //sqlStr.Append(")");
            //if (!string.IsNullOrEmpty(strWhere.Trim()))
            //{
            //    sqlStr.AppendFormat(" and ({0})", strWhere);
            //}
            //return DbHelperSQL.Query(sqlStr.ToString());

            sqlStr.Append("select top " + pagesize + "   * from ( select * from (select ROW_NUMBER() over( order by s.mem_OutTime, t.mem_ID) as rowid, t.* from tg_member t left join tg_memberExt s on t.mem_ID=s.mem_ID  ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sqlStr.AppendFormat("  where  ({0})  ) as b where  b.mem_OutTime>( select dateadd(yy,-1,getdate())) or b.mem_OutTime is null ) a where a.rowid>({1})", strWhere, notInTop);
            }
         return DbHelperSQL.Query(sqlStr.ToString());


        }
        /// <summary>
        /// 分页存储过程
        /// </summary>
        public DataSet GetMemberByPageProc(string query, int startIndex, int endIndex, string orderString)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", startIndex),
                    new SqlParameter("@endIndex", endIndex),
                    new SqlParameter("@query",SqlDbType.NVarChar,4000),
                     new SqlParameter("@orderString", orderString) 

			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = query;
            parameters[3].Value = orderString;

            return DbHelperSQL.RunProcedure("P_cm_ProjectValueStatistics", parameters, "ds");
        }

        /// <summary>
        /// 根据条件获取用户列表
        /// </summary>
        /// <param name="memid">用户id</param>
        /// <param name="unitid">部门id</param>
        /// <param name="princid">角色id</param>
        /// <returns></returns>
        public DataSet GetUserList(int memid, int unitid, int princid)
        {
            string sql = "";
            if (unitid == 8||memid==1)
            {
                sql = "select mem_Name,mem_ID,mem_Login,mem_Unit_ID from tg_member where mem_isFired=0 order by mem_Name asc,mem_Unit_ID desc";
            }
            else if (princid == 2)
            {
                sql = "select mem_Name,mem_ID,mem_Login,mem_Unit_ID from tg_member where mem_isFired=0 and mem_Unit_ID = " + unitid + "  order by mem_Name asc  ,mem_Unit_ID desc";
            }
            else
            {
                sql = string.Format(@"SELECT mem_Name,mem_ID,mem_Login,mem_Unit_ID from tg_member,tg_staffRelation where mem_ID=id and mem_isFired=0 and firstleader in
                                    (
                                        SELECT id from tg_member,tg_staffRelation where mem_ID=id and mem_isFired=0 and 
                                                firstleader in
                                                       (
                                                         SELECT id from tg_member,tg_staffRelation where mem_ID=id and 
                                                                mem_isFired=0 and firstleader in
                                                                      (
                                                                        SELECT id from tg_member,tg_staffRelation where mem_ID=id
                                                                                    and mem_isFired=0 and firstleader in 
                                                                                            (
                                                                                            select id from tg_staffRelation 
                                                                                                    where firstleader={0}
                                                                                            )
                                                                       )
                                                        ) 
                                     )
                         union 
                                SELECT mem_Name,id,mem_Login,mem_Unit_ID from tg_member,tg_staffRelation where mem_ID=id and mem_isFired=0 and firstleader in
                                (
                                    SELECT id from tg_member,tg_staffRelation where mem_ID=id and mem_isFired=0 and 
                                                firstleader in
                                                      (
                                                            SELECT id from tg_member,tg_staffRelation where mem_ID=id and 
                                                                    mem_isFired=0 and firstleader in 
                                                                         (
                                                                            select id from tg_staffRelation where firstleader={0}
                                                                         )
                                                       )
                                ) 
                        union 
                                SELECT mem_Name,id,mem_Login,mem_Unit_ID from tg_member,tg_staffRelation where mem_ID=id and mem_isFired=0 and firstleader in
                                (
                                    SELECT id from tg_member,tg_staffRelation where mem_ID=id and mem_isFired=0 and 
                                                firstleader in 
                                                      (
                                                            select id from tg_staffRelation where firstleader={0}
                                                      ) 
                                )  
                        union 
                                SELECT mem_Name,id,mem_Login,mem_Unit_ID from tg_member,tg_staffRelation where mem_ID=id and mem_isFired=0 and firstleader in 
                                (
                                    select id from tg_staffRelation where firstleader={0}
                                ) 
                        union 
                                SELECT mem_Name,id,mem_Login,mem_Unit_ID from tg_member,tg_staffRelation where mem_ID=id and mem_isFired=0 and firstleader={0} 
                        union 
                                select mem_Name,mem_ID,mem_Login,mem_Unit_ID from tg_member where mem_ID={0} order by mem_Name asc ,mem_Unit_ID desc", memid);
            }
            return DbHelperSQL.Query(sql);
        }
        #endregion  Method
    }
}

