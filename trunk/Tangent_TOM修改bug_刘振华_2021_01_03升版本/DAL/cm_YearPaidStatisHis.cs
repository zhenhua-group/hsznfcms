﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_YearPaidStatisHis
	/// </summary>
	public partial class cm_YearPaidStatisHis
	{
		public cm_YearPaidStatisHis()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("ID", "cm_YearPaidStatisHis"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_YearPaidStatisHis");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_YearPaidStatisHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_YearPaidStatisHis(");
            strSql.Append("mem_id,mem_name,mem_unit_ID,dataDate,time_leave,time_sick,time_over,time_marry,time_mater,time_die,time_late,time_year,time_sum,privMonthDay,currentMonthDay,content,priMonthCurrDay,lastMonthDay)");
			strSql.Append(" values (");
            strSql.Append("@mem_id,@mem_name,@mem_unit_ID,@dataDate,@time_leave,@time_sick,@time_over,@time_marry,@time_mater,@time_die,@time_late,@time_year,@time_sum,@privMonthDay,@currentMonthDay,@content,@priMonthCurrDay,@lastMonthDay)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_id", SqlDbType.Int,4),
					new SqlParameter("@mem_name", SqlDbType.NVarChar,100),
					new SqlParameter("@mem_unit_ID", SqlDbType.Int,4),				
					new SqlParameter("@dataDate", SqlDbType.VarChar,100),
					new SqlParameter("@time_leave", SqlDbType.Decimal,9),
					new SqlParameter("@time_sick", SqlDbType.Decimal,9),
					new SqlParameter("@time_over", SqlDbType.Decimal,9),
					new SqlParameter("@time_marry", SqlDbType.Decimal,9),
					new SqlParameter("@time_mater", SqlDbType.Decimal,9),
					new SqlParameter("@time_die", SqlDbType.Decimal,9),
					new SqlParameter("@time_late", SqlDbType.Int,4),
					new SqlParameter("@time_year", SqlDbType.Decimal,9),
					new SqlParameter("@time_sum", SqlDbType.Decimal,9),
					new SqlParameter("@privMonthDay", SqlDbType.Decimal,9),
					new SqlParameter("@currentMonthDay", SqlDbType.Decimal,9),
					new SqlParameter("@content", SqlDbType.NVarChar,1000),
                    new SqlParameter("@priMonthCurrDay", SqlDbType.Decimal,9),
					new SqlParameter("@lastMonthDay", SqlDbType.Decimal,9)};
			parameters[0].Value = model.mem_id;
			parameters[1].Value = model.mem_name;
			parameters[2].Value = model.mem_unit_ID;		
			parameters[3].Value = model.dataDate;
			parameters[4].Value = model.time_leave;
			parameters[5].Value = model.time_sick;
			parameters[6].Value = model.time_over;
			parameters[7].Value = model.time_marry;
			parameters[8].Value = model.time_mater;
			parameters[9].Value = model.time_die;
			parameters[10].Value = model.time_late;
			parameters[11].Value = model.time_year;
			parameters[12].Value = model.time_sum;
			parameters[13].Value = model.privMonthDay;
			parameters[14].Value = model.currentMonthDay;
			parameters[15].Value = model.content;
            parameters[16].Value = model.priMonthCurrDay;
            parameters[17].Value = model.lastMonthDay;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_YearPaidStatisHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_YearPaidStatisHis set ");
			strSql.Append("mem_id=@mem_id,");
			strSql.Append("mem_name=@mem_name,");
			strSql.Append("mem_unit_ID=@mem_unit_ID,");		
			strSql.Append("dataDate=@dataDate,");
			strSql.Append("time_leave=@time_leave,");
			strSql.Append("time_sick=@time_sick,");
			strSql.Append("time_over=@time_over,");
			strSql.Append("time_marry=@time_marry,");
			strSql.Append("time_mater=@time_mater,");
			strSql.Append("time_die=@time_die,");
			strSql.Append("time_late=@time_late,");
			strSql.Append("time_year=@time_year,");
			strSql.Append("time_sum=@time_sum,");
			strSql.Append("privMonthDay=@privMonthDay,");
			strSql.Append("currentMonthDay=@currentMonthDay,");
            strSql.Append("priMonthCurrDay=@priMonthCurrDay,");
            strSql.Append("lastMonthDay=@lastMonthDay,");
			strSql.Append("content=@content");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_id", SqlDbType.Int,4),
					new SqlParameter("@mem_name", SqlDbType.NVarChar,100),
					new SqlParameter("@mem_unit_ID", SqlDbType.Int,4),					
					new SqlParameter("@dataDate", SqlDbType.VarChar,100),
					new SqlParameter("@time_leave", SqlDbType.Decimal,9),
					new SqlParameter("@time_sick", SqlDbType.Decimal,9),
					new SqlParameter("@time_over", SqlDbType.Decimal,9),
					new SqlParameter("@time_marry", SqlDbType.Decimal,9),
					new SqlParameter("@time_mater", SqlDbType.Decimal,9),
					new SqlParameter("@time_die", SqlDbType.Decimal,9),
					new SqlParameter("@time_late", SqlDbType.Int,4),
					new SqlParameter("@time_year", SqlDbType.Decimal,9),
					new SqlParameter("@time_sum", SqlDbType.Decimal,9),
					new SqlParameter("@privMonthDay", SqlDbType.Decimal,9),
					new SqlParameter("@currentMonthDay", SqlDbType.Decimal,9),
                    new SqlParameter("@priMonthCurrDay", SqlDbType.Decimal,9),
					new SqlParameter("@lastMonthDay", SqlDbType.Decimal,9),
					new SqlParameter("@content", SqlDbType.NVarChar,1000),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.mem_id;
			parameters[1].Value = model.mem_name;
			parameters[2].Value = model.mem_unit_ID;			
			parameters[3].Value = model.dataDate;
			parameters[4].Value = model.time_leave;
			parameters[5].Value = model.time_sick;
			parameters[6].Value = model.time_over;
			parameters[7].Value = model.time_marry;
			parameters[8].Value = model.time_mater;
			parameters[9].Value = model.time_die;
			parameters[10].Value = model.time_late;
			parameters[11].Value = model.time_year;
			parameters[12].Value = model.time_sum;
			parameters[13].Value = model.privMonthDay;
			parameters[14].Value = model.currentMonthDay;
            parameters[15].Value = model.priMonthCurrDay;
            parameters[16].Value = model.lastMonthDay;
			parameters[17].Value = model.content;
			parameters[18].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_YearPaidStatisHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_YearPaidStatisHis ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_YearPaidStatisHis GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select  top 1 ID,mem_id,mem_name,mem_unit_ID,dataDate,time_leave,time_sick,time_over,time_marry,time_mater,time_die,time_late,time_year,time_sum,privMonthDay,currentMonthDay,content,priMonthCurrDay,lastMonthDay from cm_YearPaidStatisHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_YearPaidStatisHis model=new TG.Model.cm_YearPaidStatisHis();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_YearPaidStatisHis DataRowToModel(DataRow row)
		{
			TG.Model.cm_YearPaidStatisHis model=new TG.Model.cm_YearPaidStatisHis();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["mem_id"]!=null && row["mem_id"].ToString()!="")
				{
					model.mem_id=int.Parse(row["mem_id"].ToString());
				}
				if(row["mem_name"]!=null)
				{
					model.mem_name=row["mem_name"].ToString();
				}
				if(row["mem_unit_ID"]!=null && row["mem_unit_ID"].ToString()!="")
				{
					model.mem_unit_ID=int.Parse(row["mem_unit_ID"].ToString());
				}			
				if(row["dataDate"]!=null)
				{
					model.dataDate=row["dataDate"].ToString();
				}
				if(row["time_leave"]!=null && row["time_leave"].ToString()!="")
				{
					model.time_leave=decimal.Parse(row["time_leave"].ToString());
				}
				if(row["time_sick"]!=null && row["time_sick"].ToString()!="")
				{
					model.time_sick=decimal.Parse(row["time_sick"].ToString());
				}
				if(row["time_over"]!=null && row["time_over"].ToString()!="")
				{
					model.time_over=decimal.Parse(row["time_over"].ToString());
				}
				if(row["time_marry"]!=null && row["time_marry"].ToString()!="")
				{
					model.time_marry=decimal.Parse(row["time_marry"].ToString());
				}
				if(row["time_mater"]!=null && row["time_mater"].ToString()!="")
				{
					model.time_mater=decimal.Parse(row["time_mater"].ToString());
				}
				if(row["time_die"]!=null && row["time_die"].ToString()!="")
				{
					model.time_die=decimal.Parse(row["time_die"].ToString());
				}
				if(row["time_late"]!=null && row["time_late"].ToString()!="")
				{
					model.time_late=int.Parse(row["time_late"].ToString());
				}
				if(row["time_year"]!=null && row["time_year"].ToString()!="")
				{
					model.time_year=decimal.Parse(row["time_year"].ToString());
				}
				if(row["time_sum"]!=null && row["time_sum"].ToString()!="")
				{
					model.time_sum=decimal.Parse(row["time_sum"].ToString());
				}
				if(row["privMonthDay"]!=null && row["privMonthDay"].ToString()!="")
				{
					model.privMonthDay=decimal.Parse(row["privMonthDay"].ToString());
				}
				if(row["currentMonthDay"]!=null && row["currentMonthDay"].ToString()!="")
				{
					model.currentMonthDay=decimal.Parse(row["currentMonthDay"].ToString());
				}
				if(row["content"]!=null)
				{
					model.content=row["content"].ToString();
				}
                if (row["priMonthCurrDay"] != null && row["priMonthCurrDay"].ToString() != "")
                {
                    model.priMonthCurrDay = decimal.Parse(row["priMonthCurrDay"].ToString());
                }
                if (row["lastMonthDay"] != null && row["lastMonthDay"].ToString() != "")
                {
                    model.lastMonthDay = decimal.Parse(row["lastMonthDay"].ToString());
                }
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select ID,mem_id,mem_name,mem_unit_ID,dataDate,time_leave,time_sick,time_over,time_marry,time_mater,time_die,time_late,time_year,time_sum,privMonthDay,currentMonthDay,content,priMonthCurrDay,lastMonthDay ");
			strSql.Append(" FROM cm_YearPaidStatisHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" ID,mem_id,mem_name,mem_unit_ID,dataDate,time_leave,time_sick,time_over,time_marry,time_mater,time_die,time_late,time_year,time_sum,privMonthDay,currentMonthDay,content,priMonthCurrDay,lastMonthDay ");
			strSql.Append(" FROM cm_YearPaidStatisHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_YearPaidStatisHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_YearPaidStatisHis T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_YearPaidStatisHis";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

