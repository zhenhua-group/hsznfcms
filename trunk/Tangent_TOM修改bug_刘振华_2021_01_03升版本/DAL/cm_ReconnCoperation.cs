﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.DBUtility;

namespace TG.DAL
{/// <summary>
    /// 数据访问类:cm_ReconnCoperation
    /// </summary>
    public partial class cm_ReconnCoperation
    {
        public cm_ReconnCoperation()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("cpr_Id", "cm_ReconnCoperation");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int cpr_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_ReconnCoperation");
            strSql.Append(" where cpr_Id=@cpr_Id");
            SqlParameter[] parameters = {
					new SqlParameter("@cpr_Id", SqlDbType.Int,4)
			};
            parameters[0].Value = cpr_Id;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string cprName,string sysno)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_ReconnCoperation");
            strSql.Append(" where cpr_Name=N'" + cprName.Trim() + "' and cpr_ID<>" + sysno + "");
            return DbHelperSQL.Exists(strSql.ToString());
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ReconnCoperation model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_ReconnCoperation(");
            strSql.Append("cst_Id,cpr_No,cpr_Type,cpr_Name,BuildUnit,Floor,ChgPeople,ChgPhone,ChgJia,ChgJiaPhone,cpr_Unit,BuildPosition,Industry,BuildSrc,TableMaker,cpr_DoneDate,cpr_Mark,SurveyClass,EarthUnit,BuildArea,BuildNumber,HoleNumber,HoleHeight,WellsNumber,WellsHeight,TotalLength,BuildType,cpr_Type2,cpr_StartDate,cpr_SignDate,cpr_SignDate2,cpr_Process,MultiBuild,BuildStructType,PointNumber,WaveNumber,WaveHeight,ControlNumber,ControlHeight,GenerNumber,GenerHeight,ProjectArea,Features,ProjectDate,InsertUserID,InsertDate,IsParamterEdit,PMUserID,cpr_Acount,cpr_ShijiAcounnt,RegTime,UpdateBy,LastUpdate,cpr_FID)");
            strSql.Append(" values (");
            strSql.Append("@cst_Id,@cpr_No,@cpr_Type,@cpr_Name,@BuildUnit,@Floor,@ChgPeople,@ChgPhone,@ChgJia,@ChgJiaPhone,@cpr_Unit,@BuildPosition,@Industry,@BuildSrc,@TableMaker,@cpr_DoneDate,@cpr_Mark,@SurveyClass,@EarthUnit,@BuildArea,@BuildNumber,@HoleNumber,@HoleHeight,@WellsNumber,@WellsHeight,@TotalLength,@BuildType,@cpr_Type2,@cpr_StartDate,@cpr_SignDate,@cpr_SignDate2,@cpr_Process,@MultiBuild,@BuildStructType,@PointNumber,@WaveNumber,@WaveHeight,@ControlNumber,@ControlHeight,@GenerNumber,@GenerHeight,@ProjectArea,@Features,@ProjectDate,@InsertUserID,@InsertDate,@IsParamterEdit,@PMUserID,@cpr_Acount,@cpr_ShijiAcounnt,@RegTime,@UpdateBy,@LastUpdate,@cpr_FID)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@cst_Id", SqlDbType.Int,4),
					new SqlParameter("@cpr_No", SqlDbType.VarChar,50),
					new SqlParameter("@cpr_Type", SqlDbType.VarChar,50),
					new SqlParameter("@cpr_Name", SqlDbType.VarChar,200),
					new SqlParameter("@BuildUnit", SqlDbType.VarChar,200),
					new SqlParameter("@Floor", SqlDbType.Char,20),
					new SqlParameter("@ChgPeople", SqlDbType.Char,20),
					new SqlParameter("@ChgPhone", SqlDbType.Char,20),
					new SqlParameter("@ChgJia", SqlDbType.Char,20),
					new SqlParameter("@ChgJiaPhone", SqlDbType.Char,20),
					new SqlParameter("@cpr_Unit", SqlDbType.VarChar,100),
					new SqlParameter("@BuildPosition", SqlDbType.VarChar,50),
					new SqlParameter("@Industry", SqlDbType.VarChar,50),
					new SqlParameter("@BuildSrc", SqlDbType.VarChar,50),
					new SqlParameter("@TableMaker", SqlDbType.VarChar,50),
					new SqlParameter("@cpr_DoneDate", SqlDbType.DateTime),
					new SqlParameter("@cpr_Mark", SqlDbType.VarChar,600),
					new SqlParameter("@SurveyClass", SqlDbType.VarChar,100),
					new SqlParameter("@EarthUnit", SqlDbType.VarChar,100),
					new SqlParameter("@BuildArea", SqlDbType.Decimal,9),
					new SqlParameter("@BuildNumber", SqlDbType.Int,4),
					new SqlParameter("@HoleNumber", SqlDbType.Int,4),
					new SqlParameter("@HoleHeight", SqlDbType.Decimal,9),
					new SqlParameter("@WellsNumber", SqlDbType.Int,4),
					new SqlParameter("@WellsHeight", SqlDbType.Decimal,9),
					new SqlParameter("@TotalLength", SqlDbType.Decimal,9),
					new SqlParameter("@BuildType", SqlDbType.VarChar,50),
					new SqlParameter("@cpr_Type2", SqlDbType.VarChar,50),
                    new SqlParameter("@cpr_SignDate", SqlDbType.DateTime),					
					new SqlParameter("@cpr_Process", SqlDbType.Char,20),
					new SqlParameter("@MultiBuild", SqlDbType.VarChar,1000),
					new SqlParameter("@BuildStructType", SqlDbType.VarChar,1000),
					new SqlParameter("@PointNumber", SqlDbType.Int,4),
					new SqlParameter("@WaveNumber", SqlDbType.Int,4),
					new SqlParameter("@WaveHeight", SqlDbType.Decimal,9),
					new SqlParameter("@ControlNumber", SqlDbType.Int,4),
					new SqlParameter("@ControlHeight", SqlDbType.Decimal,9),
					new SqlParameter("@GenerNumber", SqlDbType.Int,4),
					new SqlParameter("@GenerHeight", SqlDbType.Decimal,9),
					new SqlParameter("@ProjectArea", SqlDbType.Decimal,9),
					new SqlParameter("@Features", SqlDbType.VarChar,200),
					new SqlParameter("@ProjectDate", SqlDbType.Int,4),
					new SqlParameter("@InsertUserID", SqlDbType.Int,4),
					new SqlParameter("@InsertDate", SqlDbType.DateTime),
					new SqlParameter("@IsParamterEdit", SqlDbType.Int,4),
					new SqlParameter("@PMUserID", SqlDbType.Int,4),
					new SqlParameter("@cpr_Acount", SqlDbType.Decimal,9),
					new SqlParameter("@cpr_ShijiAcounnt", SqlDbType.Decimal,9),
					new SqlParameter("@RegTime", SqlDbType.DateTime),
					new SqlParameter("@UpdateBy", SqlDbType.Char,20),
					new SqlParameter("@LastUpdate", SqlDbType.DateTime),
                    new SqlParameter("@cpr_StartDate",SqlDbType.DateTime),
					new SqlParameter("@cpr_FID", SqlDbType.Int,4),
                    new SqlParameter("@cpr_SignDate2", SqlDbType.DateTime)};
            parameters[0].Value = model.cst_Id;
            parameters[1].Value = model.cpr_No;
            parameters[2].Value = model.cpr_Type;
            parameters[3].Value = model.cpr_Name;
            parameters[4].Value = model.BuildUnit;
            parameters[5].Value = model.Floor;
            parameters[6].Value = model.ChgPeople;
            parameters[7].Value = model.ChgPhone;
            parameters[8].Value = model.ChgJia;
            parameters[9].Value = model.ChgJiaPhone;
            parameters[10].Value = model.cpr_Unit;
            parameters[11].Value = model.BuildPosition;
            parameters[12].Value = model.Industry;
            parameters[13].Value = model.BuildSrc;
            parameters[14].Value = model.TableMaker;
            parameters[15].Value = model.cpr_DoneDate;
            parameters[16].Value = model.cpr_Mark;
            parameters[17].Value = model.SurveyClass;
            parameters[18].Value = model.EarthUnit;
            parameters[19].Value = model.BuildArea;
            parameters[20].Value = model.BuildNumber;
            parameters[21].Value = model.HoleNumber;
            parameters[22].Value = model.HoleHeight;
            parameters[23].Value = model.WellsNumber;
            parameters[24].Value = model.WellsHeight;
            parameters[25].Value = model.TotalLength;
            parameters[26].Value = model.BuildType;
            parameters[27].Value = model.cpr_Type2;
            parameters[28].Value = model.cpr_SignDate;
            parameters[29].Value = model.cpr_Process;
            parameters[30].Value = model.MultiBuild;
            parameters[31].Value = model.BuildStructType;
            parameters[32].Value = model.PointNumber;
            parameters[33].Value = model.WaveNumber;
            parameters[34].Value = model.WaveHeight;
            parameters[35].Value = model.ControlNumber;
            parameters[36].Value = model.ControlHeight;
            parameters[37].Value = model.GenerNumber;
            parameters[38].Value = model.GenerHeight;
            parameters[39].Value = model.ProjectArea;
            parameters[40].Value = model.Features;
            parameters[41].Value = model.ProjectDate;
            parameters[42].Value = model.InsertUserID;
            parameters[43].Value = model.InsertDate;
            parameters[44].Value = model.IsParamterEdit;
            parameters[45].Value = model.PMUserID;
            parameters[46].Value = model.cpr_Acount;
            parameters[47].Value = model.cpr_ShijiAcounnt;
            parameters[48].Value = model.RegTime;
            parameters[49].Value = model.UpdateBy;
            parameters[50].Value = model.LastUpdate;
            parameters[51].Value = model.cpr_StartDate;
            parameters[52].Value = model.cpr_FID;
            parameters[53].Value = model.cpr_SignDate2;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_ReconnCoperation model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ReconnCoperation set ");
            strSql.Append("cst_Id=@cst_Id,");
            strSql.Append("cpr_No=@cpr_No,");
            strSql.Append("cpr_Type=@cpr_Type,");
            strSql.Append("cpr_Name=@cpr_Name,");
            strSql.Append("BuildUnit=@BuildUnit,");
            strSql.Append("Floor=@Floor,");
            strSql.Append("ChgPeople=@ChgPeople,");
            strSql.Append("ChgPhone=@ChgPhone,");
            strSql.Append("ChgJia=@ChgJia,");
            strSql.Append("ChgJiaPhone=@ChgJiaPhone,");
            strSql.Append("cpr_Unit=@cpr_Unit,");
            strSql.Append("BuildPosition=@BuildPosition,");
            strSql.Append("Industry=@Industry,");
            strSql.Append("BuildSrc=@BuildSrc,");
            strSql.Append("TableMaker=@TableMaker,");
            strSql.Append("cpr_DoneDate=@cpr_DoneDate,");
            strSql.Append("cpr_Mark=@cpr_Mark,");
            strSql.Append("SurveyClass=@SurveyClass,");
            strSql.Append("EarthUnit=@EarthUnit,");
            strSql.Append("BuildArea=@BuildArea,");
            strSql.Append("BuildNumber=@BuildNumber,");
            strSql.Append("HoleNumber=@HoleNumber,");
            strSql.Append("HoleHeight=@HoleHeight,");
            strSql.Append("WellsNumber=@WellsNumber,");
            strSql.Append("WellsHeight=@WellsHeight,");
            strSql.Append("TotalLength=@TotalLength,");
            strSql.Append("BuildType=@BuildType,");
            strSql.Append("cpr_Type2=@cpr_Type2,");
            strSql.Append("cpr_StartDate=@cpr_StartDate,");
            strSql.Append("cpr_SignDate=@cpr_SignDate,");
            strSql.Append("cpr_SignDate2=@cpr_SignDate2,");
            strSql.Append("cpr_Process=@cpr_Process,");
            strSql.Append("MultiBuild=@MultiBuild,");
            strSql.Append("BuildStructType=@BuildStructType,");
            strSql.Append("PointNumber=@PointNumber,");
            strSql.Append("WaveNumber=@WaveNumber,");
            strSql.Append("WaveHeight=@WaveHeight,");
            strSql.Append("ControlNumber=@ControlNumber,");
            strSql.Append("ControlHeight=@ControlHeight,");
            strSql.Append("GenerNumber=@GenerNumber,");
            strSql.Append("GenerHeight=@GenerHeight,");
            strSql.Append("ProjectArea=@ProjectArea,");
            strSql.Append("Features=@Features,");
            strSql.Append("ProjectDate=@ProjectDate,");
            strSql.Append("InsertUserID=@InsertUserID,");
            strSql.Append("InsertDate=@InsertDate,");
            strSql.Append("IsParamterEdit=@IsParamterEdit,");
            strSql.Append("PMUserID=@PMUserID,");
            strSql.Append("cpr_Acount=@cpr_Acount,");
            strSql.Append("cpr_ShijiAcounnt=@cpr_ShijiAcounnt,");
            strSql.Append("RegTime=@RegTime,");
            strSql.Append("UpdateBy=@UpdateBy,");
            strSql.Append("LastUpdate=@LastUpdate,");
            strSql.Append("cpr_FID=@cpr_FID");
            strSql.Append(" where cpr_Id=@cpr_Id");
            SqlParameter[] parameters = {
					new SqlParameter("@cst_Id", SqlDbType.Int,4),
					new SqlParameter("@cpr_No", SqlDbType.VarChar,50),
					new SqlParameter("@cpr_Type", SqlDbType.VarChar,50),
					new SqlParameter("@cpr_Name", SqlDbType.VarChar,200),
					new SqlParameter("@BuildUnit", SqlDbType.VarChar,200),
					new SqlParameter("@Floor", SqlDbType.Char,20),
					new SqlParameter("@ChgPeople", SqlDbType.Char,20),
					new SqlParameter("@ChgPhone", SqlDbType.Char,20),
					new SqlParameter("@ChgJia", SqlDbType.Char,20),
					new SqlParameter("@ChgJiaPhone", SqlDbType.Char,20),
					new SqlParameter("@cpr_Unit", SqlDbType.VarChar,100),
					new SqlParameter("@BuildPosition", SqlDbType.VarChar,50),
					new SqlParameter("@Industry", SqlDbType.VarChar,50),
					new SqlParameter("@BuildSrc", SqlDbType.VarChar,50),
					new SqlParameter("@TableMaker", SqlDbType.VarChar,50),
					new SqlParameter("@cpr_DoneDate", SqlDbType.DateTime),
					new SqlParameter("@cpr_Mark", SqlDbType.VarChar,600),
					new SqlParameter("@SurveyClass", SqlDbType.VarChar,100),
					new SqlParameter("@EarthUnit", SqlDbType.VarChar,100),
					new SqlParameter("@BuildArea", SqlDbType.Decimal,9),
					new SqlParameter("@BuildNumber", SqlDbType.Int,4),
					new SqlParameter("@HoleNumber", SqlDbType.Int,4),
					new SqlParameter("@HoleHeight", SqlDbType.Decimal,9),
					new SqlParameter("@WellsNumber", SqlDbType.Int,4),
					new SqlParameter("@WellsHeight", SqlDbType.Decimal,9),
					new SqlParameter("@TotalLength", SqlDbType.Decimal,9),
					new SqlParameter("@BuildType", SqlDbType.VarChar,50),
					new SqlParameter("@cpr_Type2", SqlDbType.VarChar,50),
					new SqlParameter("@cpr_SignDate", SqlDbType.DateTime),
					new SqlParameter("@cpr_Process", SqlDbType.Char,20),
					new SqlParameter("@MultiBuild", SqlDbType.VarChar,1000),
					new SqlParameter("@BuildStructType", SqlDbType.VarChar,1000),
					new SqlParameter("@PointNumber", SqlDbType.Int,4),
					new SqlParameter("@WaveNumber", SqlDbType.Int,4),
					new SqlParameter("@WaveHeight", SqlDbType.Decimal,9),
					new SqlParameter("@ControlNumber", SqlDbType.Int,4),
					new SqlParameter("@ControlHeight", SqlDbType.Decimal,9),
					new SqlParameter("@GenerNumber", SqlDbType.Int,4),
					new SqlParameter("@GenerHeight", SqlDbType.Decimal,9),
					new SqlParameter("@ProjectArea", SqlDbType.Decimal,9),
					new SqlParameter("@Features", SqlDbType.VarChar,200),
					new SqlParameter("@ProjectDate", SqlDbType.Int,4),
					new SqlParameter("@InsertUserID", SqlDbType.Int,4),
					new SqlParameter("@InsertDate", SqlDbType.DateTime),
					new SqlParameter("@IsParamterEdit", SqlDbType.Int,4),
					new SqlParameter("@PMUserID", SqlDbType.Int,4),
					new SqlParameter("@cpr_Acount", SqlDbType.Decimal,9),
					new SqlParameter("@cpr_ShijiAcounnt", SqlDbType.Decimal,9),
					new SqlParameter("@RegTime", SqlDbType.DateTime),
					new SqlParameter("@UpdateBy", SqlDbType.Char,20),
					new SqlParameter("@LastUpdate", SqlDbType.DateTime),
						new SqlParameter("@cpr_FID", SqlDbType.Int,4),
					new SqlParameter("@cpr_Id", SqlDbType.Int,4),
                    new SqlParameter("@cpr_StartDate",SqlDbType.DateTime),
					new SqlParameter("@cpr_SignDate2", SqlDbType.DateTime)};
            parameters[0].Value = model.cst_Id;
            parameters[1].Value = model.cpr_No;
            parameters[2].Value = model.cpr_Type;
            parameters[3].Value = model.cpr_Name;
            parameters[4].Value = model.BuildUnit;
            parameters[5].Value = model.Floor;
            parameters[6].Value = model.ChgPeople;
            parameters[7].Value = model.ChgPhone;
            parameters[8].Value = model.ChgJia;
            parameters[9].Value = model.ChgJiaPhone;
            parameters[10].Value = model.cpr_Unit;
            parameters[11].Value = model.BuildPosition;
            parameters[12].Value = model.Industry;
            parameters[13].Value = model.BuildSrc;
            parameters[14].Value = model.TableMaker;
            parameters[15].Value = model.cpr_DoneDate;
            parameters[16].Value = model.cpr_Mark;
            parameters[17].Value = model.SurveyClass;
            parameters[18].Value = model.EarthUnit;
            parameters[19].Value = model.BuildArea;
            parameters[20].Value = model.BuildNumber;
            parameters[21].Value = model.HoleNumber;
            parameters[22].Value = model.HoleHeight;
            parameters[23].Value = model.WellsNumber;
            parameters[24].Value = model.WellsHeight;
            parameters[25].Value = model.TotalLength;
            parameters[26].Value = model.BuildType;
            parameters[27].Value = model.cpr_Type2;
            parameters[28].Value = model.cpr_SignDate;
            parameters[29].Value = model.cpr_Process;
            parameters[30].Value = model.MultiBuild;
            parameters[31].Value = model.BuildStructType;
            parameters[32].Value = model.PointNumber;
            parameters[33].Value = model.WaveNumber;
            parameters[34].Value = model.WaveHeight;
            parameters[35].Value = model.ControlNumber;
            parameters[36].Value = model.ControlHeight;
            parameters[37].Value = model.GenerNumber;
            parameters[38].Value = model.GenerHeight;
            parameters[39].Value = model.ProjectArea;
            parameters[40].Value = model.Features;
            parameters[41].Value = model.ProjectDate;
            parameters[42].Value = model.InsertUserID;
            parameters[43].Value = model.InsertDate;
            parameters[44].Value = model.IsParamterEdit;
            parameters[45].Value = model.PMUserID;
            parameters[46].Value = model.cpr_Acount;
            parameters[47].Value = model.cpr_ShijiAcounnt;
            parameters[48].Value = model.RegTime;
            parameters[49].Value = model.UpdateBy;
            parameters[50].Value = model.LastUpdate;
            parameters[51].Value = model.cpr_FID;
            parameters[52].Value = model.cpr_Id;
            parameters[53].Value = model.cpr_StartDate;
            parameters[54].Value = model.cpr_SignDate2;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int cpr_Id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ReconnCoperation ");
            strSql.Append(" where cpr_Id=@cpr_Id");
            SqlParameter[] parameters = {
					new SqlParameter("@cpr_Id", SqlDbType.Int,4)
			};
            parameters[0].Value = cpr_Id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>        
        public ArrayList DeleteList(string cpr_Idlist)
        {
            int count = 0;
            string[] idArray = cpr_Idlist.Split(',');

            ArrayList listAudit = new ArrayList();

            foreach (string id in idArray)
            {
                if (!string.IsNullOrEmpty(id.Trim()) && id.Trim() != ",")
                {
                    //判断 是否在审核状态 中
                    if (!IsAudit(id))
                    {
                        count = DbHelperSQL.ExecuteSql("delete from cm_Coperation where cpr_Id in (select cpr_FID from cm_ReconnCoperation  where cpr_Id=" + id + ");delete cm_ReconnCoperation where cpr_Id=" + id);
                    }
                    else
                    {
                        listAudit.Add(id);
                    }
                }
            }
            return listAudit;
        }
        /// <summary>
        /// 是否在审核中
        /// </summary>
        /// <param name="cprID"></param>
        /// <returns></returns>
        public bool IsAudit(string cprID)
        {
            string sql = @"select  top 1 COUNT(SysNo) from dbo.cm_AuditRecord 
                       where (Status='A' OR  Status='B' OR Status='D' OR Status='F' OR Status='H' OR Status='J')
                        and CoperationSysNo=" + cprID + "";

            DataSet ds = DbHelperSQL.Query(sql);

            if (int.Parse(ds.Tables[0].Rows[0][0].ToString()) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ReconnCoperation GetModel(int cpr_Id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 cpr_Id,cst_Id,cpr_No,cpr_Type,cpr_Name,BuildUnit,Floor,ChgPeople,ChgPhone,ChgJia,ChgJiaPhone,cpr_Unit,BuildPosition,Industry,BuildSrc,TableMaker,cpr_DoneDate,cpr_Mark,SurveyClass,EarthUnit,BuildArea,BuildNumber,HoleNumber,HoleHeight,WellsNumber,WellsHeight,TotalLength,BuildType,cpr_Type2,cpr_StartDate,cpr_SignDate,cpr_SignDate2,cpr_Process,MultiBuild,BuildStructType,PointNumber,WaveNumber,WaveHeight,ControlNumber,ControlHeight,GenerNumber,GenerHeight,ProjectArea,Features,ProjectDate,InsertUserID,InsertDate,IsParamterEdit,PMUserID,cpr_Acount,cpr_ShijiAcounnt,RegTime,UpdateBy,LastUpdate,cpr_FID from cm_ReconnCoperation ");
            strSql.Append(" where cpr_Id=@cpr_Id");
            SqlParameter[] parameters = {
					new SqlParameter("@cpr_Id", SqlDbType.Int,4)
			};
            parameters[0].Value = cpr_Id;

            TG.Model.cm_ReconnCoperation model = new TG.Model.cm_ReconnCoperation();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["cpr_Id"] != null && ds.Tables[0].Rows[0]["cpr_Id"].ToString() != "")
                {
                    model.cpr_Id = int.Parse(ds.Tables[0].Rows[0]["cpr_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cst_Id"] != null && ds.Tables[0].Rows[0]["cst_Id"].ToString() != "")
                {
                    model.cst_Id = int.Parse(ds.Tables[0].Rows[0]["cst_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_No"] != null && ds.Tables[0].Rows[0]["cpr_No"].ToString() != "")
                {
                    model.cpr_No = ds.Tables[0].Rows[0]["cpr_No"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Type"] != null && ds.Tables[0].Rows[0]["cpr_Type"].ToString() != "")
                {
                    model.cpr_Type = ds.Tables[0].Rows[0]["cpr_Type"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Name"] != null && ds.Tables[0].Rows[0]["cpr_Name"].ToString() != "")
                {
                    model.cpr_Name = ds.Tables[0].Rows[0]["cpr_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildUnit"] != null && ds.Tables[0].Rows[0]["BuildUnit"].ToString() != "")
                {
                    model.BuildUnit = ds.Tables[0].Rows[0]["BuildUnit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Floor"] != null && ds.Tables[0].Rows[0]["Floor"].ToString() != "")
                {
                    model.Floor = ds.Tables[0].Rows[0]["Floor"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChgPeople"] != null && ds.Tables[0].Rows[0]["ChgPeople"].ToString() != "")
                {
                    model.ChgPeople = ds.Tables[0].Rows[0]["ChgPeople"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChgPhone"] != null && ds.Tables[0].Rows[0]["ChgPhone"].ToString() != "")
                {
                    model.ChgPhone = ds.Tables[0].Rows[0]["ChgPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChgJia"] != null && ds.Tables[0].Rows[0]["ChgJia"].ToString() != "")
                {
                    model.ChgJia = ds.Tables[0].Rows[0]["ChgJia"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChgJiaPhone"] != null && ds.Tables[0].Rows[0]["ChgJiaPhone"].ToString() != "")
                {
                    model.ChgJiaPhone = ds.Tables[0].Rows[0]["ChgJiaPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Unit"] != null && ds.Tables[0].Rows[0]["cpr_Unit"].ToString() != "")
                {
                    model.cpr_Unit = ds.Tables[0].Rows[0]["cpr_Unit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildPosition"] != null && ds.Tables[0].Rows[0]["BuildPosition"].ToString() != "")
                {
                    model.BuildPosition = ds.Tables[0].Rows[0]["BuildPosition"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Industry"] != null && ds.Tables[0].Rows[0]["Industry"].ToString() != "")
                {
                    model.Industry = ds.Tables[0].Rows[0]["Industry"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildSrc"] != null && ds.Tables[0].Rows[0]["BuildSrc"].ToString() != "")
                {
                    model.BuildSrc = ds.Tables[0].Rows[0]["BuildSrc"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TableMaker"] != null && ds.Tables[0].Rows[0]["TableMaker"].ToString() != "")
                {
                    model.TableMaker = ds.Tables[0].Rows[0]["TableMaker"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_DoneDate"] != null && ds.Tables[0].Rows[0]["cpr_DoneDate"].ToString() != "")
                {
                    model.cpr_DoneDate = DateTime.Parse(ds.Tables[0].Rows[0]["cpr_DoneDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_Mark"] != null && ds.Tables[0].Rows[0]["cpr_Mark"].ToString() != "")
                {
                    model.cpr_Mark = ds.Tables[0].Rows[0]["cpr_Mark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SurveyClass"] != null && ds.Tables[0].Rows[0]["SurveyClass"].ToString() != "")
                {
                    model.SurveyClass = ds.Tables[0].Rows[0]["SurveyClass"].ToString();
                }
                if (ds.Tables[0].Rows[0]["EarthUnit"] != null && ds.Tables[0].Rows[0]["EarthUnit"].ToString() != "")
                {
                    model.EarthUnit = ds.Tables[0].Rows[0]["EarthUnit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildArea"] != null && ds.Tables[0].Rows[0]["BuildArea"].ToString() != "")
                {
                    model.BuildArea = decimal.Parse(ds.Tables[0].Rows[0]["BuildArea"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BuildNumber"] != null && ds.Tables[0].Rows[0]["BuildNumber"].ToString() != "")
                {
                    model.BuildNumber = int.Parse(ds.Tables[0].Rows[0]["BuildNumber"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HoleNumber"] != null && ds.Tables[0].Rows[0]["HoleNumber"].ToString() != "")
                {
                    model.HoleNumber = int.Parse(ds.Tables[0].Rows[0]["HoleNumber"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HoleHeight"] != null && ds.Tables[0].Rows[0]["HoleHeight"].ToString() != "")
                {
                    model.HoleHeight = decimal.Parse(ds.Tables[0].Rows[0]["HoleHeight"].ToString());
                }
                if (ds.Tables[0].Rows[0]["WellsNumber"] != null && ds.Tables[0].Rows[0]["WellsNumber"].ToString() != "")
                {
                    model.WellsNumber = int.Parse(ds.Tables[0].Rows[0]["WellsNumber"].ToString());
                }
                if (ds.Tables[0].Rows[0]["WellsHeight"] != null && ds.Tables[0].Rows[0]["WellsHeight"].ToString() != "")
                {
                    model.WellsHeight = decimal.Parse(ds.Tables[0].Rows[0]["WellsHeight"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalLength"] != null && ds.Tables[0].Rows[0]["TotalLength"].ToString() != "")
                {
                    model.TotalLength = decimal.Parse(ds.Tables[0].Rows[0]["TotalLength"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BuildType"] != null && ds.Tables[0].Rows[0]["BuildType"].ToString() != "")
                {
                    model.BuildType = ds.Tables[0].Rows[0]["BuildType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_Type2"] != null && ds.Tables[0].Rows[0]["cpr_Type2"].ToString() != "")
                {
                    model.cpr_Type2 = ds.Tables[0].Rows[0]["cpr_Type2"].ToString();
                }
                if (ds.Tables[0].Rows[0]["cpr_StartDate"] != null && ds.Tables[0].Rows[0]["cpr_StartDate"].ToString() != "")
                {
                    model.cpr_StartDate = DateTime.Parse(ds.Tables[0].Rows[0]["cpr_StartDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_SignDate"] != null && ds.Tables[0].Rows[0]["cpr_SignDate"].ToString() != "")
                {
                    model.cpr_SignDate = DateTime.Parse(ds.Tables[0].Rows[0]["cpr_SignDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_SignDate2"] != null && ds.Tables[0].Rows[0]["cpr_SignDate2"].ToString() != "")
                {
                    model.cpr_SignDate2 = DateTime.Parse(ds.Tables[0].Rows[0]["cpr_SignDate2"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_Process"] != null && ds.Tables[0].Rows[0]["cpr_Process"].ToString() != "")
                {
                    model.cpr_Process = ds.Tables[0].Rows[0]["cpr_Process"].ToString();
                }
                if (ds.Tables[0].Rows[0]["MultiBuild"] != null && ds.Tables[0].Rows[0]["MultiBuild"].ToString() != "")
                {
                    model.MultiBuild = ds.Tables[0].Rows[0]["MultiBuild"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildStructType"] != null && ds.Tables[0].Rows[0]["BuildStructType"].ToString() != "")
                {
                    model.BuildStructType = ds.Tables[0].Rows[0]["BuildStructType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PointNumber"] != null && ds.Tables[0].Rows[0]["PointNumber"].ToString() != "")
                {
                    model.PointNumber = int.Parse(ds.Tables[0].Rows[0]["PointNumber"].ToString());
                }
                if (ds.Tables[0].Rows[0]["WaveNumber"] != null && ds.Tables[0].Rows[0]["WaveNumber"].ToString() != "")
                {
                    model.WaveNumber = int.Parse(ds.Tables[0].Rows[0]["WaveNumber"].ToString());
                }
                if (ds.Tables[0].Rows[0]["WaveHeight"] != null && ds.Tables[0].Rows[0]["WaveHeight"].ToString() != "")
                {
                    model.WaveHeight = decimal.Parse(ds.Tables[0].Rows[0]["WaveHeight"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ControlNumber"] != null && ds.Tables[0].Rows[0]["ControlNumber"].ToString() != "")
                {
                    model.ControlNumber = int.Parse(ds.Tables[0].Rows[0]["ControlNumber"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ControlHeight"] != null && ds.Tables[0].Rows[0]["ControlHeight"].ToString() != "")
                {
                    model.ControlHeight = decimal.Parse(ds.Tables[0].Rows[0]["ControlHeight"].ToString());
                }
                if (ds.Tables[0].Rows[0]["GenerNumber"] != null && ds.Tables[0].Rows[0]["GenerNumber"].ToString() != "")
                {
                    model.GenerNumber = int.Parse(ds.Tables[0].Rows[0]["GenerNumber"].ToString());
                }
                if (ds.Tables[0].Rows[0]["GenerHeight"] != null && ds.Tables[0].Rows[0]["GenerHeight"].ToString() != "")
                {
                    model.GenerHeight = decimal.Parse(ds.Tables[0].Rows[0]["GenerHeight"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProjectArea"] != null && ds.Tables[0].Rows[0]["ProjectArea"].ToString() != "")
                {
                    model.ProjectArea = decimal.Parse(ds.Tables[0].Rows[0]["ProjectArea"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Features"] != null && ds.Tables[0].Rows[0]["Features"].ToString() != "")
                {
                    model.Features = ds.Tables[0].Rows[0]["Features"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProjectDate"] != null && ds.Tables[0].Rows[0]["ProjectDate"].ToString() != "")
                {
                    model.ProjectDate = int.Parse(ds.Tables[0].Rows[0]["ProjectDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InsertUserID"] != null && ds.Tables[0].Rows[0]["InsertUserID"].ToString() != "")
                {
                    model.InsertUserID = int.Parse(ds.Tables[0].Rows[0]["InsertUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InsertDate"] != null && ds.Tables[0].Rows[0]["InsertDate"].ToString() != "")
                {
                    model.InsertDate = DateTime.Parse(ds.Tables[0].Rows[0]["InsertDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["IsParamterEdit"] != null && ds.Tables[0].Rows[0]["IsParamterEdit"].ToString() != "")
                {
                    model.IsParamterEdit = int.Parse(ds.Tables[0].Rows[0]["IsParamterEdit"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PMUserID"] != null && ds.Tables[0].Rows[0]["PMUserID"].ToString() != "")
                {
                    model.PMUserID = int.Parse(ds.Tables[0].Rows[0]["PMUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_Acount"] != null && ds.Tables[0].Rows[0]["cpr_Acount"].ToString() != "")
                {
                    model.cpr_Acount = decimal.Parse(ds.Tables[0].Rows[0]["cpr_Acount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_ShijiAcounnt"] != null && ds.Tables[0].Rows[0]["cpr_ShijiAcounnt"].ToString() != "")
                {
                    model.cpr_ShijiAcounnt = decimal.Parse(ds.Tables[0].Rows[0]["cpr_ShijiAcounnt"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RegTime"] != null && ds.Tables[0].Rows[0]["RegTime"].ToString() != "")
                {
                    model.RegTime = DateTime.Parse(ds.Tables[0].Rows[0]["RegTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdateBy"] != null && ds.Tables[0].Rows[0]["UpdateBy"].ToString() != "")
                {
                    model.UpdateBy = ds.Tables[0].Rows[0]["UpdateBy"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LastUpdate"] != null && ds.Tables[0].Rows[0]["LastUpdate"].ToString() != "")
                {
                    model.LastUpdate = DateTime.Parse(ds.Tables[0].Rows[0]["LastUpdate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_FID"] != null && ds.Tables[0].Rows[0]["cpr_FID"].ToString() != "")
                {
                    model.cpr_FID = int.Parse(ds.Tables[0].Rows[0]["cpr_FID"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select cpr_Id,cst_Id,cpr_No,cpr_Type,cpr_Name,BuildUnit,Floor,ChgPeople,ChgPhone,ChgJia,ChgJiaPhone,cpr_Unit,BuildPosition,Industry,BuildSrc,TableMaker,cpr_DoneDate,cpr_Mark,SurveyClass,EarthUnit,BuildArea,BuildNumber,HoleNumber,HoleHeight,WellsNumber,WellsHeight,TotalLength,BuildType,cpr_Type2,cpr_StartDate,cpr_SignDate,cpr_SignDate2,cpr_Process,MultiBuild,BuildStructType,PointNumber,WaveNumber,WaveHeight,ControlNumber,ControlHeight,GenerNumber,GenerHeight,ProjectArea,Features,ProjectDate,InsertUserID,InsertDate,IsParamterEdit,PMUserID,cpr_Acount,cpr_ShijiAcounnt,RegTime,UpdateBy,LastUpdate,cpr_FID ");
            strSql.Append(" FROM cm_ReconnCoperation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" cpr_Id,cst_Id,cpr_No,cpr_Type,cpr_Name,BuildUnit,Floor,ChgPeople,ChgPhone,ChgJia,ChgJiaPhone,cpr_Unit,BuildPosition,Industry,BuildSrc,TableMaker,cpr_DoneDate,cpr_Mark,SurveyClass,EarthUnit,BuildArea,BuildNumber,HoleNumber,HoleHeight,WellsNumber,WellsHeight,TotalLength,BuildType,cpr_Type2,cpr_StartDate,cpr_SignDate,cpr_SignDate2,cpr_Process,MultiBuild,BuildStructType,PointNumber,WaveNumber,WaveHeight,ControlNumber,ControlHeight,GenerNumber,GenerHeight,ProjectArea,Features,ProjectDate,InsertUserID,InsertDate,IsParamterEdit,PMUserID,cpr_Acount,cpr_ShijiAcounnt,RegTime,UpdateBy,LastUpdate,cpr_FID ");
            strSql.Append(" FROM cm_ReconnCoperation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_ReconnCoperation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.cpr_Id desc");
            }
            strSql.Append(")AS Row, T.*  from cm_ReconnCoperation T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "cm_ReconnCoperation";
            parameters[1].Value = "cpr_Id";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}



