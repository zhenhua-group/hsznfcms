﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TG.DBUtility;

namespace TG.DAL
{
    public class MemberLabouStatistic
    {

        /// <summary>
        /// 得打人员劳动力产值统计
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet getMemberLabouStatisticList(string strWhere,string strDate)
        {
            string sql = @"  select * from 
                          (  select
					        mem_Name,
					        mem_ID,
					        unit_ID,
					        unit_Name,
					        mem_Sex,
					        spe_Name,
				          (select COUNT(distinct temp1.pro_ID) from  tg_relation temp1 left join tg_project temp2
				           on temp1.pro_ID=temp2.pro_ID
				           where temp1.mem_ID=a.mem_ID " + strDate + "  )as TotalCount," + @"
				            (select COUNT(distinct temp1.pro_ID) from  tg_relation temp1 left join tg_project temp2
				           on temp1.pro_ID=temp2.pro_ID
				           where temp1.mem_ID=a.mem_ID and (temp2.pro_Status='进行中' OR temp2.pro_Status='' ) " + strDate + "   ) as JinXingCount," + @"
				           (select COUNT(distinct temp1.pro_ID) from  tg_relation temp1 left join tg_project temp2
				           on temp1.pro_ID=temp2.pro_ID
				           where temp1.mem_ID=a.mem_ID and temp2.pro_Status='完工' " + strDate + " ) as WanGongCount," + @"
				           (select COUNT(distinct temp1.pro_ID) from  tg_relation temp1 left join tg_project temp2
				           on temp1.pro_ID=temp2.pro_ID
				           where temp1.mem_ID=a.mem_ID and temp2.pro_Status='暂停' " + strDate + ") as ZantingCount " + @"
                           from tg_member a left join  tg_unit b on a.mem_unit_id=b.unit_ID left join tg_speciality c
                           on a.mem_Speciality_ID=c.spe_ID ) temp  where 1=1 " + strWhere + "  ";

            return DbHelperSQL.Query(sql.ToString());
        }

        /// <summary>
        /// 得打人员劳动力产值统计
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet getMemberLabouStatisticDetail(string strWhere, int mem_id)
        {
            string sql = @"select 
					         a.pro_ID,
					         pro_Name,
					         pro_DesignUnit,
					         case when b.pro_Status='' then '进行中' else  b.pro_Status end as pro_Status,
					         isnull((select dbo.fun_cm_memberRoles("+mem_id+",b.pro_ID)),'') as memRoles, "+@"
					        convert(nvarchar(10), pro_StartTime,120) pro_StartTime,
				 	         convert(nvarchar(10), pro_FinishTime,120) pro_FinishTime,
				 	        (SELECT  Isnull(purpose_Name,'') FROM tg_purpose a1
					         where purpose_ID in
					         ( 
					            SELECT  max(purpose_ID) FROM tg_package WHERE ver_date=
						        (SELECT max(ver_date) as MaxDate
						        FROM tg_package 
						        WHERE subpro_ID in ( SELECT pro_ID FROM tg_subproject WHERE pro_parentID=b.pro_ID))		
					        ) 
                             AND a1.purpose_Name<>'往来公函' AND a1.purpose_Name<>'工程表单' AND a1.purpose_Name<>'变更' AND a1.purpose_Name<>'会议纪要' ) as ProjectCurrentStatus --当前项目状态
					        from tg_relation a
					        inner join  tg_project b
					        on a.pro_ID=b.pro_ID
					        where 1=1 " + strWhere + " order by a.pro_ID desc";

            return DbHelperSQL.Query(sql.ToString());
        }
    }
}
