﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_CoperationCharge
    /// </summary>
    public partial class cm_CoperationCharge
    {
        public cm_CoperationCharge()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CoperationCharge model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.cpr_ID != null)
            {
                strSql1.Append("cpr_ID,");
                strSql2.Append("" + model.cpr_ID + ",");
            }
            if (model.Times != null)
            {
                strSql1.Append("Times,");
                strSql2.Append("'" + model.Times + "',");
            }
            if (model.persent != null)
            {
                strSql1.Append("persent,");
                strSql2.Append("" + model.persent + ",");
            }
            if (model.payCount != null)
            {
                strSql1.Append("payCount,");
                strSql2.Append("" + model.payCount + ",");
            }
            if (model.paytime != null)
            {
                strSql1.Append("paytime,");
                strSql2.Append("'" + model.paytime + "',");
            }
            if (model.payShiCount != null)
            {
                strSql1.Append("payShiCount,");
                strSql2.Append("" + model.payShiCount + ",");
            }
            if (model.acceptuser != null)
            {
                strSql1.Append("acceptuser,");
                strSql2.Append("'" + model.acceptuser + "',");
            }
            if (model.mark != null)
            {
                strSql1.Append("mark,");
                strSql2.Append("'" + model.mark + "',");
            }
            strSql.Append("insert into cm_CoperationCharge(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CoperationCharge model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_CoperationCharge set ");
            if (model.cpr_ID != null)
            {
                strSql.Append("cpr_ID=" + model.cpr_ID + ",");
            }
            else
            {
                strSql.Append("cpr_ID= null ,");
            }
            if (model.Times != null)
            {
                strSql.Append("Times='" + model.Times + "',");
            }
            else
            {
                strSql.Append("Times= null ,");
            }
            if (model.persent != null)
            {
                strSql.Append("persent=" + model.persent + ",");
            }
            else
            {
                strSql.Append("persent= null ,");
            }
            if (model.payCount != null)
            {
                strSql.Append("payCount=" + model.payCount + ",");
            }
            else
            {
                strSql.Append("payCount= null ,");
            }

            if (model.paytime != null)
            {
                strSql.Append("paytime='" + model.paytime + "',");
            }
            else
            {
                strSql.Append("paytime= null ,");
            }
            if (model.payShiCount != null)
            {
                strSql.Append("payShiCount=" + model.payShiCount + ",");
            }
            else
            {
                strSql.Append("payShiCount= null ,");
            }
            if (model.acceptuser != null)
            {
                strSql.Append("acceptuser='" + model.acceptuser + "',");
            }
            else
            {
                strSql.Append("acceptuser= null ,");
            }
            if (model.mark != null)
            {
                strSql.Append("mark='" + model.mark + "',");
            }
            else
            {
                strSql.Append("mark= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where ID=" + model.ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_CoperationCharge ");
            strSql.Append(" where ID=" + ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_CoperationCharge ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationCharge GetModel(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" ID,cpr_ID,Times,persent,payCount,payShiCount,paytime,acceptuser,mark ");
            strSql.Append(" from cm_CoperationCharge ");
            strSql.Append(" where ID=" + ID + "");
            TG.Model.cm_CoperationCharge model = new TG.Model.cm_CoperationCharge();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_ID"] != null && ds.Tables[0].Rows[0]["cpr_ID"].ToString() != "")
                {
                    model.cpr_ID = Convert.ToDecimal(ds.Tables[0].Rows[0]["cpr_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Times"] != null && ds.Tables[0].Rows[0]["Times"].ToString() != "")
                {
                    model.Times = ds.Tables[0].Rows[0]["Times"].ToString();
                }
                if (ds.Tables[0].Rows[0]["persent"] != null && ds.Tables[0].Rows[0]["persent"].ToString() != "")
                {
                    model.persent = decimal.Parse(ds.Tables[0].Rows[0]["persent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["payCount"] != null && ds.Tables[0].Rows[0]["payCount"].ToString() != "")
                {
                    model.payCount = decimal.Parse(ds.Tables[0].Rows[0]["payCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["paytime"] != null && ds.Tables[0].Rows[0]["paytime"].ToString() != "")
                {
                    model.paytime = DateTime.Parse(ds.Tables[0].Rows[0]["paytime"].ToString());
                } 
                if (ds.Tables[0].Rows[0]["payShiCount"] != null && ds.Tables[0].Rows[0]["payShiCount"].ToString() != "")
                {
                    model.payShiCount = decimal.Parse(ds.Tables[0].Rows[0]["payShiCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["acceptuser"] != null && ds.Tables[0].Rows[0]["acceptuser"].ToString() != "")
                {
                    model.acceptuser = ds.Tables[0].Rows[0]["acceptuser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mark"] != null && ds.Tables[0].Rows[0]["mark"].ToString() != "")
                {
                    model.mark = ds.Tables[0].Rows[0]["mark"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,cpr_ID,Times,persent,payCount,payShiCount,Convert(char(10),paytime,120) as paytime,acceptuser,mark ");
            strSql.Append(" FROM cm_CoperationCharge ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,cpr_ID,Times,persent,payCount,payShiCount,paytime,acceptuser,mark ");
            strSql.Append(" FROM cm_CoperationCharge ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_CoperationCharge ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            /*没有调用此方法，无需修改。Created by Sago*/
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBE() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_CoperationCharge T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        */

        #endregion  Method
    }
}

