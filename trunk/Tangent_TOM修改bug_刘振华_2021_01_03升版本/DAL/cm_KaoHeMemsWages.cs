﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeMemsWages
	/// </summary>
	public partial class cm_KaoHeMemsWages
	{
		public cm_KaoHeMemsWages()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("MemID", "cm_KaoHeMemsWages"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int MemID,int SendYear,int SendMonth,decimal yfjj,decimal yfgz)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_KaoHeMemsWages");
			strSql.Append(" where MemID=@MemID and SendYear=@SendYear and SendMonth=@SendMonth and yfjj=@yfjj and yfgz=@yfgz ");
			SqlParameter[] parameters = {
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@SendYear", SqlDbType.Int,4),
					new SqlParameter("@SendMonth", SqlDbType.Int,4),
					new SqlParameter("@yfjj", SqlDbType.Decimal,9),
					new SqlParameter("@yfgz", SqlDbType.Decimal,9)			};
			parameters[0].Value = MemID;
			parameters[1].Value = SendYear;
			parameters[2].Value = SendMonth;
			parameters[3].Value = yfjj;
			parameters[4].Value = yfgz;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeMemsWages model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeMemsWages(");
			strSql.Append("MemID,MemName,SendYear,SendMonth,jbgz,gwgz,yfjj,jtbt,txbt,jbbz,cbcb,qita,qkkk,yfgz,dksb,sksj,sfgz,zxgz,gscb,gwjt)");
			strSql.Append(" values (");
			strSql.Append("@MemID,@MemName,@SendYear,@SendMonth,@jbgz,@gwgz,@yfjj,@jtbt,@txbt,@jbbz,@cbcb,@qita,@qkkk,@yfgz,@dksb,@sksj,@sfgz,@zxgz,@gscb,@gwjt)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@SendYear", SqlDbType.Int,4),
					new SqlParameter("@SendMonth", SqlDbType.Int,4),
					new SqlParameter("@jbgz", SqlDbType.Decimal,9),
					new SqlParameter("@gwgz", SqlDbType.Decimal,9),
					new SqlParameter("@yfjj", SqlDbType.Decimal,9),
					new SqlParameter("@jtbt", SqlDbType.Decimal,9),
					new SqlParameter("@txbt", SqlDbType.Decimal,9),
					new SqlParameter("@jbbz", SqlDbType.Decimal,9),
					new SqlParameter("@cbcb", SqlDbType.Decimal,9),
					new SqlParameter("@qita", SqlDbType.Decimal,9),
					new SqlParameter("@qkkk", SqlDbType.Decimal,9),
					new SqlParameter("@yfgz", SqlDbType.Decimal,9),
					new SqlParameter("@dksb", SqlDbType.Decimal,9),
					new SqlParameter("@sksj", SqlDbType.Decimal,9),
					new SqlParameter("@sfgz", SqlDbType.Decimal,9),
					new SqlParameter("@zxgz", SqlDbType.Decimal,9),
					new SqlParameter("@gscb", SqlDbType.Decimal,9),
					new SqlParameter("@gwjt", SqlDbType.Decimal,9)};
			parameters[0].Value = model.MemID;
			parameters[1].Value = model.MemName;
			parameters[2].Value = model.SendYear;
			parameters[3].Value = model.SendMonth;
			parameters[4].Value = model.jbgz;
			parameters[5].Value = model.gwgz;
			parameters[6].Value = model.yfjj;
			parameters[7].Value = model.jtbt;
			parameters[8].Value = model.txbt;
			parameters[9].Value = model.jbbz;
			parameters[10].Value = model.cbcb;
			parameters[11].Value = model.qita;
			parameters[12].Value = model.qkkk;
			parameters[13].Value = model.yfgz;
			parameters[14].Value = model.dksb;
			parameters[15].Value = model.sksj;
			parameters[16].Value = model.sfgz;
			parameters[17].Value = model.zxgz;
			parameters[18].Value = model.gscb;
			parameters[19].Value = model.gwjt;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeMemsWages model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeMemsWages set ");
			strSql.Append("MemName=@MemName,");
			strSql.Append("jbgz=@jbgz,");
			strSql.Append("gwgz=@gwgz,");
			strSql.Append("jtbt=@jtbt,");
			strSql.Append("txbt=@txbt,");
			strSql.Append("jbbz=@jbbz,");
			strSql.Append("cbcb=@cbcb,");
			strSql.Append("qita=@qita,");
			strSql.Append("qkkk=@qkkk,");
			strSql.Append("dksb=@dksb,");
			strSql.Append("sksj=@sksj,");
			strSql.Append("sfgz=@sfgz,");
			strSql.Append("zxgz=@zxgz,");
			strSql.Append("gscb=@gscb,");
            strSql.Append("gwjt=@gwjt,");
            //加上预发奖金与应发工资项
            strSql.Append("yfjj=@yfjj,");
            strSql.Append("yfgz=@yfgz");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@jbgz", SqlDbType.Decimal,9),
					new SqlParameter("@gwgz", SqlDbType.Decimal,9),
					new SqlParameter("@jtbt", SqlDbType.Decimal,9),
					new SqlParameter("@txbt", SqlDbType.Decimal,9),
					new SqlParameter("@jbbz", SqlDbType.Decimal,9),
					new SqlParameter("@cbcb", SqlDbType.Decimal,9),
					new SqlParameter("@qita", SqlDbType.Decimal,9),
					new SqlParameter("@qkkk", SqlDbType.Decimal,9),
					new SqlParameter("@dksb", SqlDbType.Decimal,9),
					new SqlParameter("@sksj", SqlDbType.Decimal,9),
					new SqlParameter("@sfgz", SqlDbType.Decimal,9),
					new SqlParameter("@zxgz", SqlDbType.Decimal,9),
					new SqlParameter("@gscb", SqlDbType.Decimal,9),
					new SqlParameter("@gwjt", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@SendYear", SqlDbType.Int,4),
					new SqlParameter("@SendMonth", SqlDbType.Int,4),
					new SqlParameter("@yfjj", SqlDbType.Decimal,9),
					new SqlParameter("@yfgz", SqlDbType.Decimal,9)};
			parameters[0].Value = model.MemName;
			parameters[1].Value = model.jbgz;
			parameters[2].Value = model.gwgz;
			parameters[3].Value = model.jtbt;
			parameters[4].Value = model.txbt;
			parameters[5].Value = model.jbbz;
			parameters[6].Value = model.cbcb;
			parameters[7].Value = model.qita;
			parameters[8].Value = model.qkkk;
			parameters[9].Value = model.dksb;
			parameters[10].Value = model.sksj;
			parameters[11].Value = model.sfgz;
			parameters[12].Value = model.zxgz;
			parameters[13].Value = model.gscb;
			parameters[14].Value = model.gwjt;
			parameters[15].Value = model.ID;
			parameters[16].Value = model.MemID;
			parameters[17].Value = model.SendYear;
			parameters[18].Value = model.SendMonth;
			parameters[19].Value = model.yfjj;
			parameters[20].Value = model.yfgz;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsWages ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int MemID,int SendYear,int SendMonth,decimal yfjj,decimal yfgz)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsWages ");
			strSql.Append(" where MemID=@MemID and SendYear=@SendYear and SendMonth=@SendMonth and yfjj=@yfjj and yfgz=@yfgz ");
			SqlParameter[] parameters = {
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@SendYear", SqlDbType.Int,4),
					new SqlParameter("@SendMonth", SqlDbType.Int,4),
					new SqlParameter("@yfjj", SqlDbType.Decimal,9),
					new SqlParameter("@yfgz", SqlDbType.Decimal,9)			};
			parameters[0].Value = MemID;
			parameters[1].Value = SendYear;
			parameters[2].Value = SendMonth;
			parameters[3].Value = yfjj;
			parameters[4].Value = yfgz;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsWages ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeMemsWages GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,MemID,MemName,SendYear,SendMonth,jbgz,gwgz,yfjj,jtbt,txbt,jbbz,cbcb,qita,qkkk,yfgz,dksb,sksj,sfgz,zxgz,gscb,gwjt from cm_KaoHeMemsWages ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeMemsWages model=new TG.Model.cm_KaoHeMemsWages();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeMemsWages DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeMemsWages model=new TG.Model.cm_KaoHeMemsWages();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["MemID"]!=null && row["MemID"].ToString()!="")
				{
					model.MemID=int.Parse(row["MemID"].ToString());
				}
				if(row["MemName"]!=null)
				{
					model.MemName=row["MemName"].ToString();
				}
				if(row["SendYear"]!=null && row["SendYear"].ToString()!="")
				{
					model.SendYear=int.Parse(row["SendYear"].ToString());
				}
				if(row["SendMonth"]!=null && row["SendMonth"].ToString()!="")
				{
					model.SendMonth=int.Parse(row["SendMonth"].ToString());
				}
				if(row["jbgz"]!=null && row["jbgz"].ToString()!="")
				{
					model.jbgz=decimal.Parse(row["jbgz"].ToString());
				}
				if(row["gwgz"]!=null && row["gwgz"].ToString()!="")
				{
					model.gwgz=decimal.Parse(row["gwgz"].ToString());
				}
				if(row["yfjj"]!=null && row["yfjj"].ToString()!="")
				{
					model.yfjj=decimal.Parse(row["yfjj"].ToString());
				}
				if(row["jtbt"]!=null && row["jtbt"].ToString()!="")
				{
					model.jtbt=decimal.Parse(row["jtbt"].ToString());
				}
				if(row["txbt"]!=null && row["txbt"].ToString()!="")
				{
					model.txbt=decimal.Parse(row["txbt"].ToString());
				}
				if(row["jbbz"]!=null && row["jbbz"].ToString()!="")
				{
					model.jbbz=decimal.Parse(row["jbbz"].ToString());
				}
				if(row["cbcb"]!=null && row["cbcb"].ToString()!="")
				{
					model.cbcb=decimal.Parse(row["cbcb"].ToString());
				}
				if(row["qita"]!=null && row["qita"].ToString()!="")
				{
					model.qita=decimal.Parse(row["qita"].ToString());
				}
				if(row["qkkk"]!=null && row["qkkk"].ToString()!="")
				{
					model.qkkk=decimal.Parse(row["qkkk"].ToString());
				}
				if(row["yfgz"]!=null && row["yfgz"].ToString()!="")
				{
					model.yfgz=decimal.Parse(row["yfgz"].ToString());
				}
				if(row["dksb"]!=null && row["dksb"].ToString()!="")
				{
					model.dksb=decimal.Parse(row["dksb"].ToString());
				}
				if(row["sksj"]!=null && row["sksj"].ToString()!="")
				{
					model.sksj=decimal.Parse(row["sksj"].ToString());
				}
				if(row["sfgz"]!=null && row["sfgz"].ToString()!="")
				{
					model.sfgz=decimal.Parse(row["sfgz"].ToString());
				}
				if(row["zxgz"]!=null && row["zxgz"].ToString()!="")
				{
					model.zxgz=decimal.Parse(row["zxgz"].ToString());
				}
				if(row["gscb"]!=null && row["gscb"].ToString()!="")
				{
					model.gscb=decimal.Parse(row["gscb"].ToString());
				}
				if(row["gwjt"]!=null && row["gwjt"].ToString()!="")
				{
					model.gwjt=decimal.Parse(row["gwjt"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,MemID,MemName,SendYear,SendMonth,jbgz,gwgz,yfjj,jtbt,txbt,jbbz,cbcb,qita,qkkk,yfgz,dksb,sksj,sfgz,zxgz,gscb,gwjt ");
			strSql.Append(" FROM cm_KaoHeMemsWages ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,MemID,MemName,SendYear,SendMonth,jbgz,gwgz,yfjj,jtbt,txbt,jbbz,cbcb,qita,qkkk,yfgz,dksb,sksj,sfgz,zxgz,gscb,gwjt ");
			strSql.Append(" FROM cm_KaoHeMemsWages ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeMemsWages ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeMemsWages T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeMemsWages";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

