﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using TG.Model;

namespace TG.DAL
{
    public class AuditLocusDA
    {
        public object GetAuditRecordByReferenceSysNo<T>(int referenceSysNo)
        {
            object resultObj = null;
            if (typeof(T).Equals(typeof(TG.Model.cm_CoperationAudit)))
            {
                resultObj = new TG.DAL.cm_CoperationAudit().GetModelByCoperationSysNo(referenceSysNo);
            }
            else if (typeof(T).Equals(typeof(TG.Model.cm_CoperationAuditEdit)))
            {
                resultObj = new TG.DAL.cm_CoperationAudit().GetEditModelByCoperationSysNo(referenceSysNo);
            }
            else if (typeof(T).Equals(typeof(TG.Model.cm_ProjectPlanAuditEntity)))
            {
                resultObj = new TG.DAL.cm_ProjectPlanDA().GetProjectPlanAuditEntity(new cm_ProjectPlanAuditQueryEntity { ProjectPlanAuditSysNo = referenceSysNo });
            }
            else if (typeof(T).Equals(typeof(TG.Model.ProjectAuditListViewDataEntity)))
            {
                resultObj = new TG.DAL.tg_ProjectAuditDA().GetProjectAuditEntity(referenceSysNo);
            }
            else if (typeof(T).Equals(typeof(TG.Model.ProjectAuditEditEntity)))
            {
                resultObj = new TG.DAL.tg_ProjectAuditDA().GetProjectAuditEditEntity(referenceSysNo);
            }
            else if (typeof(T).Equals(typeof(TG.Model.cm_ProjectValueAuditRecord)))//产值
            {
                resultObj = new TG.DAL.cm_ProjectValueAuditRecord().GetAllotAuditEntity(referenceSysNo);
            }
            else if (typeof(T).Equals(typeof(TG.Model.cm_TranProjectValueAuditRecord)))//转经济产值
            {
                resultObj = new TG.DAL.cm_TranProjectValueAuditRecord().GetAllotAuditEntity(referenceSysNo);
            }
            else if (typeof(T).Equals(typeof(TG.Model.cm_ProImaAudit)))
            {
                resultObj = new TG.DAL.cm_ProImaAudit().GetProImaAuditEntity(referenceSysNo);
            }
            else if (typeof(T).Equals(typeof(TG.Model.cm_ProjectPlotInfoAudit)))//出图卡
            {
                resultObj = new TG.DAL.cm_ProjectPlotInfo().GetProjectPlotAuditModelSysNo(referenceSysNo);
            }
            else if (typeof(T).Equals(typeof(TG.Model.cm_ProjectFileInfoAudit)))//归档资料
            {
                resultObj = new TG.DAL.cm_ProjectFileInfo().GetProjectFileAuditModelSysNo(referenceSysNo);
            }
            else if (typeof(T).Equals(typeof(TG.Model.cm_SuperCoperationAudit)))//监理公司
            {
                resultObj = new TG.DAL.cm_SuperCoperationAudit().GetModelBySuperCoperationSysNo(referenceSysNo);
            }
            else if (typeof(T).Equals(typeof(TG.Model.cm_SuperCoperationAuditRepeat)))//监理公司修改申请
            {
                resultObj = new TG.DAL.cm_SuperCoperationAuditRepeat().GetModelBySuperCoperationSysNo(referenceSysNo);
            }
            return resultObj;
        }

        public string[] GetProcessDescriptionArray(string Action)
        {
            string configTableName = "";
            switch (Action)
            {
                case "C":
                    configTableName = "cm_CoperationAuditConfig";
                    break;
                case "EC"://合同修改申请
                    configTableName = "cm_CoperationAuditEditConfig";
                    break;
                case "PN":
                    configTableName = "cm_ProjectPlanAuditConfig";
                    break;
                case "P":
                    configTableName = "cm_ProjectAuditConfig";
                    break;
                case "A"://add by nxr 20130616
                    configTableName = "cm_ProjectFeeAllotConfig";
                    break;
                case "M":
                    configTableName = "cm_ProImageAuditConfig";
                    break;
                case "PLOT": //工程出图
                    configTableName = "cm_ProjectPlotInfoConfig";
                    break;
                case "File": //工程出图
                    configTableName = "cm_ProjectFileAuditConfig";
                    break;
                case "JJS": //经济所
                    configTableName = "cm_jjsProjectFeeAllotConfig";
                    break;
                case "TranJJS": //转经济所
                    configTableName = "cm_TranProjectFeeAllotConfig";
                    break;
                case "SuperC": //监理公司
                    configTableName = "cm_SuperCoperationAuditConfig";
                    break;
                case "SuperEC": //监理公司修改申请
                    configTableName = "cm_SuperCoperationAuditConfig";
                    break;
            }
            string sql = "select r.RoleName from " + configTableName + "  c join cm_Role r on c.RoleSysNo = r.SysNo ORDER BY c.Position ASC";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);
            List<string> UserArrayList = new List<string>();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    UserArrayList.Add(reader["RoleName"].ToString());
                }
                reader.Close();
            }
            return UserArrayList.ToArray();
        }
        public string GetNextmember(string states, string auditno, string type)
        {

            return new TG.DAL.tg_ProjectAuditDA().GetNextmember(states, auditno, type);
        }


    }
}
