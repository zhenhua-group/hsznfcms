﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using TG.Model;
using System.Collections.Generic;
using TG.Common.EntityBuilder;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_ProImaAudit
    /// </summary>
    public partial class cm_ProImaAudit
    {
        public cm_ProImaAudit()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("SysNo", "cm_ProImaAudit");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int SysNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_ProImaAudit");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProImaAudit model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_ProImaAudit(");
            strSql.Append("ProjectSysNo,Status,OneSuggestion,TwoSuggestion,AuditUser,AuditDate,InDate,InUser)");
            strSql.Append(" values (");
            strSql.Append("@ProjectSysNo,@Status,@OneSuggestion,@TwoSuggestion,@AuditUser,@AuditDate,@InDate,@InUser)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ProjectSysNo", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Char,10),
					new SqlParameter("@OneSuggestion", SqlDbType.NVarChar,500),
                    new SqlParameter("@TwoSuggestion", SqlDbType.NVarChar,500),
					new SqlParameter("@AuditUser", SqlDbType.NVarChar,100),
					new SqlParameter("@AuditDate", SqlDbType.NVarChar,100),
					new SqlParameter("@InDate", SqlDbType.DateTime),
					new SqlParameter("@InUser", SqlDbType.Int,4)};
            parameters[0].Value = model.ProjectSysNo;
            parameters[1].Value = model.Status;
            parameters[2].Value = model.OneSuggestion;
            parameters[3].Value = model.TwoSuggestion;
            parameters[4].Value = model.AuditUser;
            parameters[5].Value = model.AuditDate;
            parameters[6].Value = model.InDate;
            parameters[7].Value = model.InUser;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProImaAudit model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ProImaAudit set ");
            strSql.Append("ProjectSysNo=@ProjectSysNo,");
            strSql.Append("Status=@Status,");
            strSql.Append("OneSuggestion=@OneSuggestion,");
            strSql.Append("TwoSuggestion=@TwoSuggestion,");
            strSql.Append("AuditUser=@AuditUser,");
            strSql.Append("AuditDate=@AuditDate,");
            strSql.Append("InDate=@InDate,");
            strSql.Append("InUser=@InUser");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@ProjectSysNo", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Char,10),
					new SqlParameter("@OneSuggestion", SqlDbType.NVarChar,500),
                    new SqlParameter("@TwoSuggestion", SqlDbType.NVarChar,500),
					new SqlParameter("@AuditUser", SqlDbType.NVarChar,100),
					new SqlParameter("@AuditDate", SqlDbType.NVarChar,100),
					new SqlParameter("@InDate", SqlDbType.DateTime),
					new SqlParameter("@InUser", SqlDbType.Int,4),
					new SqlParameter("@SysNo", SqlDbType.Int,4)};
            parameters[0].Value = model.ProjectSysNo;
            parameters[1].Value = model.Status;
            parameters[2].Value = model.OneSuggestion;
            parameters[3].Value = model.TwoSuggestion;
            parameters[4].Value = model.AuditUser;
            parameters[5].Value = model.AuditDate;
            parameters[6].Value = model.InDate;
            parameters[7].Value = model.InUser;
            parameters[8].Value = model.SysNo;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            return rows;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SysNo)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProImaAudit ");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SysNolist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProImaAudit ");
            strSql.Append(" where SysNo in (" + SysNolist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProImaAudit GetModel(int SysNo)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SysNo,ProjectSysNo,Status,OneSuggestion,TwoSuggestion,AuditUser,AuditDate,InDate,InUser from cm_ProImaAudit ");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            TG.Model.cm_ProImaAudit model = new TG.Model.cm_ProImaAudit();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["SysNo"] != null && ds.Tables[0].Rows[0]["SysNo"].ToString() != "")
                {
                    model.SysNo = int.Parse(ds.Tables[0].Rows[0]["SysNo"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProjectSysNo"] != null && ds.Tables[0].Rows[0]["ProjectSysNo"].ToString() != "")
                {
                    model.ProjectSysNo = int.Parse(ds.Tables[0].Rows[0]["ProjectSysNo"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OneSuggestion"] != null && ds.Tables[0].Rows[0]["OneSuggestion"].ToString() != "")
                {
                    model.OneSuggestion = ds.Tables[0].Rows[0]["OneSuggestion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TwoSuggestion"] != null && ds.Tables[0].Rows[0]["TwoSuggestion"].ToString() != "")
                {
                    model.TwoSuggestion = ds.Tables[0].Rows[0]["TwoSuggestion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditUser"] != null && ds.Tables[0].Rows[0]["AuditUser"].ToString() != "")
                {
                    model.AuditUser = ds.Tables[0].Rows[0]["AuditUser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditDate"] != null && ds.Tables[0].Rows[0]["AuditDate"].ToString() != "")
                {
                    model.AuditDate = ds.Tables[0].Rows[0]["AuditDate"].ToString();
                }
                if (ds.Tables[0].Rows[0]["InDate"] != null && ds.Tables[0].Rows[0]["InDate"].ToString() != "")
                {
                    model.InDate = DateTime.Parse(ds.Tables[0].Rows[0]["InDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InUser"] != null && ds.Tables[0].Rows[0]["InUser"].ToString() != "")
                {
                    model.InUser = int.Parse(ds.Tables[0].Rows[0]["InUser"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SysNo,ProjectSysNo,Status,OneSuggestion,TwoSuggestion,AuditUser,AuditDate,InDate,InUser ");
            strSql.Append(" FROM cm_ProImaAudit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" SysNo,ProjectSysNo,Status,OneSuggestion,TwoSuggestion,AuditUser,AudtiDate,InDate,InUser ");
            strSql.Append(" FROM cm_ProImaAudit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_ProImaAudit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
         /// <summary>
        /// 得到该审核记录当前的审核角色
        /// </summary>

        public TG.Model.cm_ProImageAuditConfig GetCoperationAuditConfigByPostion(int position)
        {
            string sql = "select c.SysNo,c.ProcessDescription,c.RoleSysNo,c.Position,r.RoleName,r.Users from cm_ProImageAuditConfig c join cm_Role r on c.RoleSysNo = r.SysNo where c.Position =" + position + "";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            TG.Model.cm_ProImageAuditConfig resultEntity = EntityBuilder<TG.Model.cm_ProImageAuditConfig>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }


        /// <summary>
        /// 查询工程设计出图
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public List<cm_ProImaAuditListView> GetProjectImgAuditView(string whereSql,int startIndex,int endIndex)
        {

            List<cm_ProImaAuditListView> resultList = new List<cm_ProImaAuditListView>();

            SqlDataReader reader = DbHelperSQL.RunProcedure("P_cm_Project", new SqlParameter[] { new SqlParameter("@query", whereSql), new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex) });

            while (reader.Read())
            {
                cm_ProImaAuditListView auditListView = new cm_ProImaAuditListView
                {
                    pro_ID = reader["pro_ID"] == null ? 0 : Convert.ToInt32(reader["pro_ID"]),

                    pro_name = reader["pro_name"] == null ? string.Empty : reader["pro_name"].ToString(),

                    pro_StruType = reader["pro_StruType"] == null ? string.Empty : reader["pro_StruType"].ToString(),
                    pro_kinds = reader["pro_kinds"] == null ? string.Empty : reader["pro_kinds"].ToString(),

                    pro_buildUnit = reader["pro_buildUnit"] == null ? string.Empty : reader["pro_buildUnit"].ToString(),
                    pro_status = reader["pro_status"] == null ? string.Empty : reader["pro_status"].ToString(),

                    pro_level = reader["pro_level"] == null ? 0 : int.Parse(reader["pro_level"].ToString()),

                    pro_startTime = reader["pro_startTime"] == null ? DateTime.MinValue : DateTime.Parse(reader["pro_startTime"].ToString()),

                    pro_finishTime = reader["pro_finishTime"] == null ? DateTime.MinValue : DateTime.Parse(reader["pro_finishTime"].ToString()),

                    Pro_src = reader["Pro_src"] == null ? 0 : int.Parse(reader["Pro_src"].ToString()),

                    ChgJia = reader["ChgJia"] == null ? string.Empty : reader["ChgJia"].ToString(),

                    Phone = reader["Phone"] == null ? string.Empty : reader["Phone"].ToString(),

                    Project_reletive = reader["Project_reletive"] == null ? string.Empty : reader["Project_reletive"].ToString(),

                    Cpr_Acount = reader["Cpr_Acount"] == null ? 0 : decimal.Parse(reader["Cpr_Acount"].ToString()),

                    Unit = reader["Unit"] == null ? string.Empty : reader["Unit"].ToString(),

                    ProjectScale = reader["ProjectScale"] == null ? 0 : decimal.Parse(reader["ProjectScale"].ToString()),

                    BuildAddress = reader["BuildAddress"] == null ? string.Empty : reader["BuildAddress"].ToString(),

                    pro_Intro = reader["pro_Intro"] == null ? string.Empty : reader["pro_Intro"].ToString(),

                    cprID = reader["CoperationSysNo"] == null ? string.Empty : reader["CoperationSysNo"].ToString(),

                    UpdateBy = reader["UpdateBy"] == null ? 0 : int.Parse(reader["UpdateBy"].ToString()),

                    CoperationSysNo = reader["CoperationSysNo"] == null ? 0 : int.Parse(reader["CoperationSysNo"].ToString()),

                    PMName = reader["PMName"] == null ? string.Empty : reader["PMName"].ToString(),

                    PMPhone = reader["PMPhone"] == null ? string.Empty : reader["PMPhone"].ToString(),

                    Industry = reader["Industry"] == null ? string.Empty : reader["Industry"].ToString(),

                    BuildType = reader["BuildType"] == null ? string.Empty : reader["BuildType"].ToString(),

                    ProjSub = reader["ProjSub"] == null ? string.Empty : reader["ProjSub"].ToString()

                };
                resultList.Add(auditListView);
            }

            return resultList;
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProImaAudit GetModelByProSysNo(int SysNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"SELECT  [SysNo]
                          ,[ProjectSysNo]
                          ,[Status]
                          ,[OneSuggestion]
                          ,[TwoSuggestion]
                          ,[AuditUser]
                          ,[AuditDate]
                          ,[InDate]
                          ,[InUser]
                      FROM cm_ProImaAudit where ProjectSysNo=@id ");

            SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString(), parameters);

            reader.Read();

            TG.Model.cm_ProImaAudit proImaAuditEntity = TG.Common.EntityBuilder.EntityBuilder<TG.Model.cm_ProImaAudit>.BuilderEntity(reader);

            reader.Close();

            return proImaAuditEntity;
        }

        public TG.Model.cm_ProImaAudit GetProImaAuditEntity(int allotAuditSysNo)
        {
            string sql = " SELECT SysNo,ProjectSysNo,OneSuggestion,TwoSuggestion,Status,AuditUser,AuditDate,InUser ,InDate FROM dbo.cm_ProImaAudit WHERE SysNo=" + allotAuditSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            TG.Model.cm_ProImaAudit resultEntity = EntityBuilder<TG.Model.cm_ProImaAudit>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }

        #endregion  Method
    }
}

