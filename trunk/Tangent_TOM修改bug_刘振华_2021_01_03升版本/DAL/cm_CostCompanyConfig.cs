﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_CostCompanyConfig
	/// </summary>
	public partial class cm_CostCompanyConfig
	{
		public cm_CostCompanyConfig()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_CostCompanyConfig model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.CmpMngPrt != null)
			{
				strSql1.Append("CmpMngPrt,");
				strSql2.Append(""+model.CmpMngPrt+",");
			}
			if (model.ProCostPrt != null)
			{
				strSql1.Append("ProCostPrt,");
				strSql2.Append(""+model.ProCostPrt+",");
			}
			if (model.ProChgPrt != null)
			{
				strSql1.Append("ProChgPrt,");
				strSql2.Append(""+model.ProChgPrt+",");
			}
			if (model.RegUserPrt != null)
			{
				strSql1.Append("RegUserPrt,");
				strSql2.Append(""+model.RegUserPrt+",");
			}
			if (model.DesignPrt != null)
			{
				strSql1.Append("DesignPrt,");
				strSql2.Append(""+model.DesignPrt+",");
			}
			if (model.AuditPrt != null)
			{
				strSql1.Append("AuditPrt,");
				strSql2.Append(""+model.AuditPrt+",");
			}
			if (model.TotalUser != null)
			{
				strSql1.Append("TotalUser,");
				strSql2.Append(""+model.TotalUser+",");
			}
			if (model.CollegeUser != null)
			{
				strSql1.Append("CollegeUser,");
				strSql2.Append(""+model.CollegeUser+",");
			}
			if (model.Used != null)
			{
				strSql1.Append("Used,");
				strSql2.Append(""+model.Used+",");
			}
			strSql.Append("insert into cm_CostCompanyConfig(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_CostCompanyConfig model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_CostCompanyConfig set ");
			if (model.CmpMngPrt != null)
			{
				strSql.Append("CmpMngPrt="+model.CmpMngPrt+",");
			}
			else
			{
				strSql.Append("CmpMngPrt= null ,");
			}
			if (model.ProCostPrt != null)
			{
				strSql.Append("ProCostPrt="+model.ProCostPrt+",");
			}
			else
			{
				strSql.Append("ProCostPrt= null ,");
			}
			if (model.ProChgPrt != null)
			{
				strSql.Append("ProChgPrt="+model.ProChgPrt+",");
			}
			else
			{
				strSql.Append("ProChgPrt= null ,");
			}
			if (model.RegUserPrt != null)
			{
				strSql.Append("RegUserPrt="+model.RegUserPrt+",");
			}
			else
			{
				strSql.Append("RegUserPrt= null ,");
			}
			if (model.DesignPrt != null)
			{
				strSql.Append("DesignPrt="+model.DesignPrt+",");
			}
			else
			{
				strSql.Append("DesignPrt= null ,");
			}
			if (model.AuditPrt != null)
			{
				strSql.Append("AuditPrt="+model.AuditPrt+",");
			}
			else
			{
				strSql.Append("AuditPrt= null ,");
			}
			if (model.TotalUser != null)
			{
				strSql.Append("TotalUser="+model.TotalUser+",");
			}
			else
			{
				strSql.Append("TotalUser= null ,");
			}
			if (model.CollegeUser != null)
			{
				strSql.Append("CollegeUser="+model.CollegeUser+",");
			}
			else
			{
				strSql.Append("CollegeUser= null ,");
			}
			if (model.Used != null)
			{
				strSql.Append("Used="+model.Used+",");
			}
			else
			{
				strSql.Append("Used= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ID="+ model.ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_CostCompanyConfig ");
			strSql.Append(" where ID="+ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_CostCompanyConfig ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_CostCompanyConfig GetModel(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" ID,CmpMngPrt,ProCostPrt,ProChgPrt,RegUserPrt,DesignPrt,AuditPrt,TotalUser,CollegeUser,Used ");
			strSql.Append(" from cm_CostCompanyConfig ");
			strSql.Append(" where ID="+ID+"" );
			TG.Model.cm_CostCompanyConfig model=new TG.Model.cm_CostCompanyConfig();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["ID"]!=null && ds.Tables[0].Rows[0]["ID"].ToString()!="")
				{
					model.ID=int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CmpMngPrt"]!=null && ds.Tables[0].Rows[0]["CmpMngPrt"].ToString()!="")
				{
					model.CmpMngPrt=decimal.Parse(ds.Tables[0].Rows[0]["CmpMngPrt"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ProCostPrt"]!=null && ds.Tables[0].Rows[0]["ProCostPrt"].ToString()!="")
				{
					model.ProCostPrt=decimal.Parse(ds.Tables[0].Rows[0]["ProCostPrt"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ProChgPrt"]!=null && ds.Tables[0].Rows[0]["ProChgPrt"].ToString()!="")
				{
					model.ProChgPrt=decimal.Parse(ds.Tables[0].Rows[0]["ProChgPrt"].ToString());
				}
				if(ds.Tables[0].Rows[0]["RegUserPrt"]!=null && ds.Tables[0].Rows[0]["RegUserPrt"].ToString()!="")
				{
					model.RegUserPrt=decimal.Parse(ds.Tables[0].Rows[0]["RegUserPrt"].ToString());
				}
				if(ds.Tables[0].Rows[0]["DesignPrt"]!=null && ds.Tables[0].Rows[0]["DesignPrt"].ToString()!="")
				{
					model.DesignPrt=decimal.Parse(ds.Tables[0].Rows[0]["DesignPrt"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AuditPrt"]!=null && ds.Tables[0].Rows[0]["AuditPrt"].ToString()!="")
				{
					model.AuditPrt=decimal.Parse(ds.Tables[0].Rows[0]["AuditPrt"].ToString());
				}
				if(ds.Tables[0].Rows[0]["TotalUser"]!=null && ds.Tables[0].Rows[0]["TotalUser"].ToString()!="")
				{
					model.TotalUser=decimal.Parse(ds.Tables[0].Rows[0]["TotalUser"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CollegeUser"]!=null && ds.Tables[0].Rows[0]["CollegeUser"].ToString()!="")
				{
					model.CollegeUser=decimal.Parse(ds.Tables[0].Rows[0]["CollegeUser"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Used"]!=null && ds.Tables[0].Rows[0]["Used"].ToString()!="")
				{
					model.Used=int.Parse(ds.Tables[0].Rows[0]["Used"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,CmpMngPrt,ProCostPrt,ProChgPrt,RegUserPrt,DesignPrt,AuditPrt,TotalUser,CollegeUser,Used ");
			strSql.Append(" FROM cm_CostCompanyConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,CmpMngPrt,ProCostPrt,ProChgPrt,RegUserPrt,DesignPrt,AuditPrt,TotalUser,CollegeUser,Used ");
			strSql.Append(" FROM cm_CostCompanyConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_CostCompanyConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_CostCompanyConfig T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

        /*
        */
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetCostCompanyListByPager(int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,CmpMngPrt,ProCostPrt,ProChgPrt,RegUserPrt,DesignPrt,AuditPrt,TotalUser,CollegeUser,Used ");
            strSql.Append(" FROM cm_CostCompanyConfig ");

            return DbHelperSQL.Query(strSql.ToString(), startIndex, endIndex);
        }

		#endregion  Method
	}
}

