﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.DBUtility;

namespace TG.DAL
{
    public class cm_FundPercentSet
    {
        public cm_FundPercentSet()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_FundPercentSet model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_FundPercentSet(");
            strSql.Append("unit_id,fund_Year,fund_percent)");
            strSql.Append(" values (");
            strSql.Append("@unit_id,@fund_Year,@fund_percent)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@unit_id", SqlDbType.Int,4),
					new SqlParameter("@fund_Year", SqlDbType.NChar,10),
					new SqlParameter("@fund_percent", SqlDbType.Decimal,9)};
            parameters[0].Value = model.unit_id;
            parameters[1].Value = model.fund_Year;
            parameters[2].Value = model.fund_percent;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_FundPercentSet model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_FundPercentSet set ");
            strSql.Append("fund_percent=@fund_percent");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@fund_percent", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.fund_percent;
            parameters[1].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_FundPercentSet ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_FundPercentSet ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_FundPercentSet GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,unit_id,fund_Year,fund_percent from cm_FundPercentSet ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_FundPercentSet model = new TG.Model.cm_FundPercentSet();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["unit_id"] != null && ds.Tables[0].Rows[0]["unit_id"].ToString() != "")
                {
                    model.unit_id = int.Parse(ds.Tables[0].Rows[0]["unit_id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["fund_Year"] != null && ds.Tables[0].Rows[0]["fund_Year"].ToString() != "")
                {
                    model.fund_Year = ds.Tables[0].Rows[0]["fund_Year"].ToString();
                }
                if (ds.Tables[0].Rows[0]["fund_percent"] != null && ds.Tables[0].Rows[0]["fund_percent"].ToString() != "")
                {
                    model.fund_percent = decimal.Parse(ds.Tables[0].Rows[0]["fund_percent"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,unit_id,fund_Year,fund_percent ");
            strSql.Append(" FROM cm_FundPercentSet ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }


        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex, string type)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_CoefficientSetting", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query), new SqlParameter("@type", type) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query, string type)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_CoefficientSetting_Count", new SqlParameter[] { new SqlParameter("@query", query), new SqlParameter("@type", type) });
        }
        /// <summary>
        /// 得到收入成本结余明细表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetUnitRevenueCostBalance(string strWhere, string year)
        {
            string sql = @"select  
                            isnull((select SUM(AllotCount) from cm_ProjectValueAllot c
                             inner join cm_Project d
                             on c.pro_ID=d.pro_ID
                             where d.Unit=a.unit_Name 
                             and c.SecondValue=1 and c.Status<>'D'";
            if (!string.IsNullOrEmpty(year))
            {
                sql = sql + @" and ActualAllountTime='" + year + "' ";
            }
            sql = sql + @"  
                          group by d.Unit
                            ) ,0) as AllotCount,
                             isnull((select sum(costCharge) from cm_CostDetails where costUnit=a.unit_id";

            if (!string.IsNullOrEmpty(year))
            {
                sql = sql + @" and YEAR(costTime)='" + year + "' ";
            }
            sql = sql + @"  
                            ),0.00) as costCharge," + @"
                            a.unit_Name,a.unit_ID,
                            isnull((select fund_percent from cm_FundPercentSet where unit_id=a.unit_ID";
            if (!string.IsNullOrEmpty(year))
            {
                sql = sql + @" and YEAR(fund_Year)='" + year + "' ";
            }
            else
            {
                sql = sql + @" and YEAR(fund_Year)='" + DateTime.Now.Year + "' ";
            }
            sql = sql + @" ),0.00) as fund_percent,";
            sql = sql + @"isnull((select Remuneration_percent from cm_RemunerationPercentSet where unit_id=a.unit_ID";
            if (!string.IsNullOrEmpty(year))
            {
                sql = sql + @" and YEAR(Remuneration_Year)='" + year + "' ";
            }
            else
            {
                sql = sql + @" and YEAR(Remuneration_Year)='" + DateTime.Now.Year + "' ";
            }
            sql = sql + @" ),0.00) as Remuneration_percent
                            from tg_unit  a
                            
                            where  1=1 
                            " + strWhere + "" + @"
                            group by a.unit_Name,a.unit_ID
                            order by a.unit_ID 
                           ";
            return DbHelperSQL.Query(sql);
        }
        #endregion  Method
    }
}
