﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoheSpecMemsFz
	/// </summary>
	public partial class cm_KaoheSpecMemsFz
	{
		public cm_KaoheSpecMemsFz()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoheSpecMemsFz model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoheSpecMemsFz(");
			strSql.Append("ProId,ProKHId,ProKHNameId,KHTypeId,MemID,MemName,SpeName,SpeID,Jixiao)");
			strSql.Append(" values (");
			strSql.Append("@ProId,@ProKHId,@ProKHNameId,@KHTypeId,@MemID,@MemName,@SpeName,@SpeID,@Jixiao)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ProId", SqlDbType.Int,4),
					new SqlParameter("@ProKHId", SqlDbType.Int,4),
					new SqlParameter("@ProKHNameId", SqlDbType.Int,4),
					new SqlParameter("@KHTypeId", SqlDbType.Int,4),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@SpeName", SqlDbType.NVarChar,50),
					new SqlParameter("@SpeID", SqlDbType.Int,4),
					new SqlParameter("@Jixiao", SqlDbType.Decimal,9)};
			parameters[0].Value = model.ProId;
			parameters[1].Value = model.ProKHId;
			parameters[2].Value = model.ProKHNameId;
			parameters[3].Value = model.KHTypeId;
			parameters[4].Value = model.MemID;
			parameters[5].Value = model.MemName;
			parameters[6].Value = model.SpeName;
			parameters[7].Value = model.SpeID;
			parameters[8].Value = model.Jixiao;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoheSpecMemsFz model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoheSpecMemsFz set ");
			strSql.Append("ProId=@ProId,");
			strSql.Append("ProKHId=@ProKHId,");
			strSql.Append("ProKHNameId=@ProKHNameId,");
			strSql.Append("KHTypeId=@KHTypeId,");
			strSql.Append("MemID=@MemID,");
			strSql.Append("MemName=@MemName,");
			strSql.Append("SpeName=@SpeName,");
			strSql.Append("SpeID=@SpeID,");
			strSql.Append("Jixiao=@Jixiao");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ProId", SqlDbType.Int,4),
					new SqlParameter("@ProKHId", SqlDbType.Int,4),
					new SqlParameter("@ProKHNameId", SqlDbType.Int,4),
					new SqlParameter("@KHTypeId", SqlDbType.Int,4),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@SpeName", SqlDbType.NVarChar,50),
					new SqlParameter("@SpeID", SqlDbType.Int,4),
					new SqlParameter("@Jixiao", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.ProId;
			parameters[1].Value = model.ProKHId;
			parameters[2].Value = model.ProKHNameId;
			parameters[3].Value = model.KHTypeId;
			parameters[4].Value = model.MemID;
			parameters[5].Value = model.MemName;
			parameters[6].Value = model.SpeName;
			parameters[7].Value = model.SpeID;
			parameters[8].Value = model.Jixiao;
			parameters[9].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoheSpecMemsFz ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoheSpecMemsFz ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoheSpecMemsFz GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,ProId,ProKHId,ProKHNameId,KHTypeId,MemID,MemName,SpeName,SpeID,Jixiao from cm_KaoheSpecMemsFz ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoheSpecMemsFz model=new TG.Model.cm_KaoheSpecMemsFz();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoheSpecMemsFz DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoheSpecMemsFz model=new TG.Model.cm_KaoheSpecMemsFz();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["ProId"]!=null && row["ProId"].ToString()!="")
				{
					model.ProId=int.Parse(row["ProId"].ToString());
				}
				if(row["ProKHId"]!=null && row["ProKHId"].ToString()!="")
				{
					model.ProKHId=int.Parse(row["ProKHId"].ToString());
				}
				if(row["ProKHNameId"]!=null && row["ProKHNameId"].ToString()!="")
				{
					model.ProKHNameId=int.Parse(row["ProKHNameId"].ToString());
				}
				if(row["KHTypeId"]!=null && row["KHTypeId"].ToString()!="")
				{
					model.KHTypeId=int.Parse(row["KHTypeId"].ToString());
				}
				if(row["MemID"]!=null && row["MemID"].ToString()!="")
				{
					model.MemID=int.Parse(row["MemID"].ToString());
				}
				if(row["MemName"]!=null)
				{
					model.MemName=row["MemName"].ToString();
				}
				if(row["SpeName"]!=null)
				{
					model.SpeName=row["SpeName"].ToString();
				}
				if(row["SpeID"]!=null && row["SpeID"].ToString()!="")
				{
					model.SpeID=int.Parse(row["SpeID"].ToString());
				}
				if(row["Jixiao"]!=null && row["Jixiao"].ToString()!="")
				{
					model.Jixiao=decimal.Parse(row["Jixiao"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,ProId,ProKHId,ProKHNameId,KHTypeId,MemID,MemName,SpeName,SpeID,Jixiao ");
			strSql.Append(" FROM cm_KaoheSpecMemsFz ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,ProId,ProKHId,ProKHNameId,KHTypeId,MemID,MemName,SpeName,SpeID,Jixiao ");
			strSql.Append(" FROM cm_KaoheSpecMemsFz ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoheSpecMemsFz ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoheSpecMemsFz T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoheSpecMemsFz";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

