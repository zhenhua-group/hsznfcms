﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_ApplyStatisDetailHis
	/// </summary>
	public partial class cm_ApplyStatisDetailHis
	{
		public cm_ApplyStatisDetailHis()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("ID", "cm_ApplyStatisDetailHis"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_ApplyStatisDetailHis");
			strSql.Append(" where ID="+ID+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_ApplyStatisDetailHis model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.mem_id != null)
			{
				strSql1.Append("mem_id,");
				strSql2.Append(""+model.mem_id+",");
			}
			if (model.mem_name != null)
			{
				strSql1.Append("mem_name,");
				strSql2.Append("'"+model.mem_name+"',");
			}
			if (model.mem_unit_ID != null)
			{
				strSql1.Append("mem_unit_ID,");
				strSql2.Append(""+model.mem_unit_ID+",");
			}
			if (model.dataDate != null)
			{
				strSql1.Append("dataDate,");
				strSql2.Append("'"+model.dataDate+"',");
			}
			if (model.time_leave != null)
			{
				strSql1.Append("time_leave,");
				strSql2.Append("'"+model.time_leave+"',");
			}
			if (model.time_sick != null)
			{
				strSql1.Append("time_sick,");
				strSql2.Append("'"+model.time_sick+"',");
			}
			if (model.time_over != null)
			{
				strSql1.Append("time_over,");
				strSql2.Append("'"+model.time_over+"',");
			}
			if (model.time_marry != null)
			{
				strSql1.Append("time_marry,");
				strSql2.Append("'"+model.time_marry+"',");
			}
			if (model.time_mater != null)
			{
				strSql1.Append("time_mater,");
				strSql2.Append("'"+model.time_mater+"',");
			}
			if (model.time_die != null)
			{
				strSql1.Append("time_die,");
				strSql2.Append("'"+model.time_die+"',");
			}
			if (model.time_late != null)
			{
				strSql1.Append("time_late,");
				strSql2.Append("'"+model.time_late+"',");
			}
            if (model.isHoliday != null)
            {
                strSql1.Append("isHoliday,");
                strSql2.Append("'" + model.isHoliday + "',");
            }
			strSql.Append("insert into cm_ApplyStatisDetailHis(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ApplyStatisDetailHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_ApplyStatisDetailHis set ");
			if (model.mem_id != null)
			{
				strSql.Append("mem_id="+model.mem_id+",");
			}
			if (model.mem_name != null)
			{
				strSql.Append("mem_name='"+model.mem_name+"',");
			}
			if (model.mem_unit_ID != null)
			{
				strSql.Append("mem_unit_ID="+model.mem_unit_ID+",");
			}
			if (model.dataDate != null)
			{
				strSql.Append("dataDate='"+model.dataDate+"',");
			}
			if (model.time_leave != null)
			{
				strSql.Append("time_leave='"+model.time_leave+"',");
			}
			else
			{
				strSql.Append("time_leave= null ,");
			}
			if (model.time_sick != null)
			{
				strSql.Append("time_sick='"+model.time_sick+"',");
			}
			else
			{
				strSql.Append("time_sick= null ,");
			}
			if (model.time_over != null)
			{
				strSql.Append("time_over='"+model.time_over+"',");
			}
			else
			{
				strSql.Append("time_over= null ,");
			}
			if (model.time_marry != null)
			{
				strSql.Append("time_marry='"+model.time_marry+"',");
			}
			else
			{
				strSql.Append("time_marry= null ,");
			}
			if (model.time_mater != null)
			{
				strSql.Append("time_mater='"+model.time_mater+"',");
			}
			else
			{
				strSql.Append("time_mater= null ,");
			}
			if (model.time_die != null)
			{
				strSql.Append("time_die='"+model.time_die+"',");
			}
			else
			{
				strSql.Append("time_die= null ,");
			}
			if (model.time_late != null)
			{
				strSql.Append("time_late='"+model.time_late+"',");
			}
			else
			{
				strSql.Append("time_late= null ,");
			}
            if (model.isHoliday != null)
            {
                strSql.Append("isHoliday='" + model.isHoliday + "',");
            }
            else
            {
                strSql.Append("isHoliday= null ,");
            }
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ID="+ model.ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ApplyStatisDetailHis ");
			strSql.Append(" where ID="+ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ApplyStatisDetailHis ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ApplyStatisDetailHis GetModel(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
            strSql.Append(" ID,mem_id,mem_name,mem_unit_ID,dataDate,time_leave,time_sick,time_over,time_marry,time_mater,time_die,time_late,isHoliday ");
			strSql.Append(" from cm_ApplyStatisDetailHis ");
			strSql.Append(" where ID="+ID+"" );
			TG.Model.cm_ApplyStatisDetailHis model=new TG.Model.cm_ApplyStatisDetailHis();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ApplyStatisDetailHis DataRowToModel(DataRow row)
		{
			TG.Model.cm_ApplyStatisDetailHis model=new TG.Model.cm_ApplyStatisDetailHis();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["mem_id"]!=null && row["mem_id"].ToString()!="")
				{
					model.mem_id=int.Parse(row["mem_id"].ToString());
				}
				if(row["mem_name"]!=null)
				{
					model.mem_name=row["mem_name"].ToString();
				}
				if(row["mem_unit_ID"]!=null && row["mem_unit_ID"].ToString()!="")
				{
					model.mem_unit_ID=int.Parse(row["mem_unit_ID"].ToString());
				}
				if(row["dataDate"]!=null && row["dataDate"].ToString()!="")
				{
					model.dataDate=row["dataDate"].ToString();
				}
				if(row["time_leave"]!=null && row["time_leave"].ToString()!="")
				{
					model.time_leave=row["time_leave"].ToString();
				}
				if(row["time_sick"]!=null && row["time_sick"].ToString()!="")
				{
					model.time_sick=row["time_sick"].ToString();
				}
				if(row["time_over"]!=null && row["time_over"].ToString()!="")
				{
					model.time_over=row["time_over"].ToString();
				}
				if(row["time_marry"]!=null && row["time_marry"].ToString()!="")
				{
					model.time_marry=row["time_marry"].ToString();
				}
				if(row["time_mater"]!=null && row["time_mater"].ToString()!="")
				{
					model.time_mater=row["time_mater"].ToString();
				}
				if(row["time_die"]!=null && row["time_die"].ToString()!="")
				{
					model.time_die=row["time_die"].ToString();
				}
				if(row["time_late"]!=null && row["time_late"].ToString()!="")
				{
					model.time_late=row["time_late"].ToString();
				}
                if (row["isHoliday"] != null && row["isHoliday"].ToString() != "")
                {
                    model.isHoliday = row["isHoliday"].ToString();
                }
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select ID,mem_id,mem_name,mem_unit_ID,dataDate,time_leave,time_sick,time_over,time_marry,time_mater,time_die,time_late,isHoliday ");
			strSql.Append(" FROM cm_ApplyStatisDetailHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" ID,mem_id,mem_name,mem_unit_ID,dataDate,time_leave,time_sick,time_over,time_marry,time_mater,time_die,time_late,isHoliday ");
			strSql.Append(" FROM cm_ApplyStatisDetailHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_ApplyStatisDetailHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_ApplyStatisDetailHis T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
		#region  MethodEx

		#endregion  MethodEx
	}
}

