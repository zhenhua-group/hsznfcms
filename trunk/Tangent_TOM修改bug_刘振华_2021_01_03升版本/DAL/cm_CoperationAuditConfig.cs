﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DBUtility;
using System.Data.SqlClient;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class cm_CoperationAuditConfig
    {
        /// <summary>
        /// 添加一条审核配置记录
        /// </summary>
        /// <param name="coperationAuditConfig"></param>
        /// <returns></returns>
        public int InsertCoperationAuditConfig(TG.Model.cm_CoperationAuditConfig coperationAuditConfig)
        {
            string sql = "insert into cm_CoperationAuditConfig(ProcessDescription,RoleSysNo,ParentSysNo)values(";

            sql += coperationAuditConfig.Workflow + "," + coperationAuditConfig.RoleSysNo + "," + coperationAuditConfig.Position + ")";

            int count = DbHelperSQL.ExecuteSql(sql);

            return count;
        }

        /// <summary>
        /// 得到合同审核配置列表
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public List<TG.Model.cm_CoperationAuditConfig> GetCoperationAuditConfigList(string whereSql)
        {
            string sql = "select c.SysNo,c.ProcessDescription,c.RoleSysNo,c.Position,r.RoleName,r.Users from cm_CoperationAuditConfig c join cm_Role r on c.RoleSysNo = r.SysNo where 1=1";
            if (!string.IsNullOrEmpty(whereSql))
            {
                sql += whereSql;
            }
            sql += " order by c.Position ASC";
            List<TG.Model.cm_CoperationAuditConfig> resultList = new List<TG.Model.cm_CoperationAuditConfig>();

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            while (reader.Read())
            {
                TG.Model.cm_CoperationAuditConfig coperationAuditConfig = new TG.Model.cm_CoperationAuditConfig
                {
                    SysNo = reader["SysNo"] == null ? 0 : Convert.ToInt32(reader["SysNo"]),
                    Workflow = reader["ProcessDescription"] == DBNull.Value ? "" : reader["ProcessDescription"].ToString(),
                    RoleSysNo = reader["RoleSysNo"] == DBNull.Value ? 0 : Convert.ToInt32(reader["RoleSysNo"]),
                    Position = reader["Position"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Position"]),
                    UserSysNoArrayString = reader["Users"] == DBNull.Value ? "" : reader["Users"].ToString(),
                    RoleName = reader["RoleName"] == DBNull.Value ? "" : reader["RoleName"].ToString(),
                };
                resultList.Add(coperationAuditConfig);
            }

            reader.Close();
            return resultList;
        }

        /// <summary>
        /// 得到一个合同审核配置
        /// </summary>
        /// <param name="coperationAuditConfigSysNo"></param>
        /// <returns></returns>
        public TG.Model.cm_CoperationAuditConfig GetCoperationAuditConfig(int coperationAuditConfigSysNo)
        {
            string sql = "select c.SysNo,c.ProcessDescription,c.RoleSysNo,c.Position,r.RoleName,r.Users from cm_CoperationAuditConfig c join cm_Role r on c.RoleSysNo = r.SysNo where c.SysNo =" + coperationAuditConfigSysNo + " order by c.Position ASC";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            TG.Model.cm_CoperationAuditConfig coperationAuditConfig = new TG.Model.cm_CoperationAuditConfig
            {
                SysNo = reader["SysNo"] == null ? 0 : Convert.ToInt32(reader["SysNo"]),
                Workflow = reader["ProcessDescription"] == DBNull.Value ? "" : reader["ProcessDescription"].ToString(),
                RoleSysNo = reader["RoleSysNo"] == DBNull.Value ? 0 : Convert.ToInt32(reader["RoleSysNo"]),
                Position = reader["Position"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Position"]),
                UserSysNoArrayString = reader["Users"] == DBNull.Value ? "" : reader["Users"].ToString(),
                RoleName = reader["RoleName"] == DBNull.Value ? "" : reader["RoleName"].ToString(),
            };

            reader.Close();

            return coperationAuditConfig;
        }

        public TG.Model.cm_CoperationAuditConfig GetCoperationAuditConfigByPostion(int position)
        {
            string sql = "select c.SysNo,c.ProcessDescription,c.RoleSysNo,c.Position,r.RoleName,r.Users from cm_CoperationAuditConfig c join cm_Role r on c.RoleSysNo = r.SysNo where c.Position =" + position + "";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            TG.Model.cm_CoperationAuditConfig resultEntity = EntityBuilder<TG.Model.cm_CoperationAuditConfig>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }


        /// <summary>
        /// 修改合同审核配置状态
        /// </summary>
        /// <param name="coperationAuditConfig"></param>
        /// <returns></returns>
        public int UpdateCoperationAuditConfig(TG.Model.cm_CoperationAuditConfig coperationAuditConfig)
        {
            string sql = "update cm_CoperationAuditConfig set ProcessDescription=N'" + coperationAuditConfig.Workflow + "',RoleSysNo=" + coperationAuditConfig.RoleSysNo + ",Position=" + coperationAuditConfig.Position + " where SysNo=" + coperationAuditConfig.SysNo;

            int count = DbHelperSQL.ExecuteSql(sql);

            return count;
        }
    }
}
