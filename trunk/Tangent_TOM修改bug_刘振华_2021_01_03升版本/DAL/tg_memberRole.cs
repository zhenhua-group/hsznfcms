﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:tg_memberRole
	/// </summary>
	public partial class tg_memberRole
	{
		public tg_memberRole()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.tg_memberRole model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into tg_memberRole(");
			strSql.Append("mem_Id,RoleIdTec,RoleIdMag,Weights,mem_Unit_ID)");
			strSql.Append(" values (");
			strSql.Append("@mem_Id,@RoleIdTec,@RoleIdMag,@Weights,@mem_Unit_ID)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_Id", SqlDbType.Int,4),
					new SqlParameter("@RoleIdTec", SqlDbType.Int,4),
					new SqlParameter("@RoleIdMag", SqlDbType.Int,4),
					new SqlParameter("@Weights", SqlDbType.Decimal,9),
					new SqlParameter("@mem_Unit_ID", SqlDbType.Int,4)};
			parameters[0].Value = model.mem_Id;
			parameters[1].Value = model.RoleIdTec;
			parameters[2].Value = model.RoleIdMag;
			parameters[3].Value = model.Weights;
			parameters[4].Value = model.mem_Unit_ID;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_memberRole model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update tg_memberRole set ");
			strSql.Append("mem_Id=@mem_Id,");
			strSql.Append("RoleIdTec=@RoleIdTec,");
			strSql.Append("RoleIdMag=@RoleIdMag,");
			strSql.Append("Weights=@Weights,");
			strSql.Append("mem_Unit_ID=@mem_Unit_ID");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_Id", SqlDbType.Int,4),
					new SqlParameter("@RoleIdTec", SqlDbType.Int,4),
					new SqlParameter("@RoleIdMag", SqlDbType.Int,4),
					new SqlParameter("@Weights", SqlDbType.Decimal,9),
					new SqlParameter("@mem_Unit_ID", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.mem_Id;
			parameters[1].Value = model.RoleIdTec;
			parameters[2].Value = model.RoleIdMag;
			parameters[3].Value = model.Weights;
			parameters[4].Value = model.mem_Unit_ID;
			parameters[5].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_memberRole ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_memberRole ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_memberRole GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,mem_Id,RoleIdTec,RoleIdMag,Weights,mem_Unit_ID from tg_memberRole ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.tg_memberRole model=new TG.Model.tg_memberRole();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_memberRole DataRowToModel(DataRow row)
		{
			TG.Model.tg_memberRole model=new TG.Model.tg_memberRole();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["mem_Id"]!=null && row["mem_Id"].ToString()!="")
				{
					model.mem_Id=int.Parse(row["mem_Id"].ToString());
				}
				if(row["RoleIdTec"]!=null && row["RoleIdTec"].ToString()!="")
				{
					model.RoleIdTec=int.Parse(row["RoleIdTec"].ToString());
				}
				if(row["RoleIdMag"]!=null && row["RoleIdMag"].ToString()!="")
				{
					model.RoleIdMag=int.Parse(row["RoleIdMag"].ToString());
				}
				if(row["Weights"]!=null && row["Weights"].ToString()!="")
				{
					model.Weights=decimal.Parse(row["Weights"].ToString());
				}
				if(row["mem_Unit_ID"]!=null && row["mem_Unit_ID"].ToString()!="")
				{
					model.mem_Unit_ID=int.Parse(row["mem_Unit_ID"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,mem_Id,RoleIdTec,RoleIdMag,Weights,mem_Unit_ID ");
			strSql.Append(" FROM tg_memberRole ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,mem_Id,RoleIdTec,RoleIdMag,Weights,mem_Unit_ID ");
			strSql.Append(" FROM tg_memberRole ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM tg_memberRole ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from tg_memberRole T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "tg_memberRole";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

