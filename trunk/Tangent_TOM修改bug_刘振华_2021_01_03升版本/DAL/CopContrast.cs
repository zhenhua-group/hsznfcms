﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class CopContrast
    {
        /// <summary>
        /// 合同对比和同比
        /// </summary>
        /// <param name="unitStr">查询条件</param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public List<TG.Model.CopContrast> GetList(string sqlcheck, string unitStr, string year, string beginTime, string endTime)
        {
            string sql = "";
            if (sqlcheck == "unitallot")
            {
                sql = @"select unit_Name as Unit,SUM(ht) as AcountYearNow,SUM(mbz) as AllotUnitNow,
SUM(ht12) as AcountYear,SUM(mbz12) as AllotUnitLast,sum(by12) AS AcountMonth ,sum(by13)AS 'AcountMonthNow'
from (
SELECT *,
isnull
(
  (
   SELECT UnitAllot FROM cm_UnitCopAllot WHERE AllotYear='{0}' AND  UnitID=c.unit_ID
  ),0
) AS mbz,0 as ht12 ,0 as mbz12 FROM
(
SELECT a.unit_ID,a.unit_name ,
isnull(sum(b.cpr_Acount),0) AS ht,0 AS by12,0 AS by13
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
AND b.cpr_SignDate between'{0}-01-01'and'{0}-{4}' 
 WHERE {2}
GROUP BY a.unit_Name,a.unit_ID   
) AS c 
union all
SELECT *,
isnull
(
  (
   SELECT UnitAllot FROM cm_UnitCopAllot WHERE AllotYear='{1}'AND  UnitID=c.unit_ID
  ),0
) AS mbz12 FROM
(
SELECT a.unit_ID,a.unit_name,0 as ht,0 AS by12,0 AS by13,0 as mbz,
isnull(sum(b.cpr_Acount),0) AS ht12
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
AND b.cpr_SignDate between'{1}-01-01'and'{1}-{4}' 
 WHERE {2}
GROUP BY a.unit_Name,a.unit_ID    
) AS c 
UNION ALL
SELECT a.unit_ID,a.unit_name,0 AS ht,
isnull(sum(b.cpr_Acount),0) AS by12,0 AS by13,0 AS mbz,0 AS ht12,0 AS mbz12
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
AND b.cpr_SignDate between '{1}-{3}' and '{1}-{4}'
 WHERE {2}
GROUP BY a.unit_Name,a.unit_ID 
UNION ALL 
SELECT a.unit_ID,a.unit_name,0 AS ht,0 AS by12,
isnull(sum(b.cpr_Acount),0) AS by13,0 AS mbz,0 AS ht12,0 AS mbz12
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
AND b.cpr_SignDate between '{0}-{3}' and '{0}-{4}'
 WHERE {2}
GROUP BY a.unit_Name,a.unit_ID 
) d group by unit_Name ,unit_ID";
                sql = string.Format(sql, year, Convert.ToInt32(year) - 1, unitStr, beginTime, endTime);
            }
            else if (sqlcheck == "unitcharge")
            {
                sql = @"select unit_Name as Unit,SUM(ht) as AcountYearNow,SUM(mbz) as AllotUnitNow,
SUM(ht12) as AcountYear,SUM(mbz12) as AllotUnitLast,sum(by12) AS AcountMonth ,sum(by13)AS 'AcountMonthNow'
from (
SELECT *,
isnull
(
  (
   SELECT UnitAllot FROM cm_UnitCharge WHERE AllotYear='{0}' AND  UnitID=c.unit_ID
  ),0
) AS mbz,0 as ht12 ,0 as mbz12 FROM
(
SELECT a.unit_ID,a.unit_name ,
isnull(sum(c.Acount),0) AS ht,0 AS by12,0 AS by13
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
LEFT JOIN cm_ProjectCharge c ON b.cpr_Id =c.cprID AND  c.InAcountTime between '{0}-01-01'and'{0}-{4}' AND c.Status!='B'   WHERE {2}
GROUP BY a.unit_Name,a.unit_ID   
) AS c 
union all
SELECT *,
isnull
(
  (
   SELECT UnitAllot FROM cm_UnitCharge WHERE AllotYear='{1}'AND  UnitID=c.unit_ID
  ),0
) AS mbz12 FROM
(
SELECT a.unit_ID,a.unit_name,0 as ht,0 AS by12,0 AS by13,0 as mbz,
isnull(sum(c.Acount),0) AS ht12
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
LEFT JOIN cm_ProjectCharge c ON b.cpr_Id =c.cprID AND c.InAcountTime between '{1}-01-01'and'{1}-{4}' AND c.Status!='B'   WHERE {2}
GROUP BY a.unit_Name,a.unit_ID    
) AS c 
UNION ALL
SELECT a.unit_ID,a.unit_name,0 AS ht,
isnull(sum(c.Acount),0) AS by12,0 AS by13,0 AS mbz,0 AS ht12,0 AS mbz12
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
LEFT JOIN cm_ProjectCharge c ON b.cpr_Id =c.cprID AND c.InAcountTime between '{1}-{3}' and '{1}-{4}' AND c.Status!='B'   WHERE {2}
GROUP BY a.unit_Name,a.unit_ID 
UNION ALL 
SELECT a.unit_ID,a.unit_name,0 AS ht,0 AS by12,
isnull(sum(c.Acount),0) AS by13,0 AS mbz,0 AS ht12,0 AS mbz12
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
LEFT JOIN cm_ProjectCharge c ON b.cpr_Id =c.cprID AND c.InAcountTime between '{0}-{3}' and '{0}-{4}' AND c.Status!='B'   WHERE {2}
GROUP BY a.unit_Name,a.unit_ID 
) d group by unit_Name ,unit_ID";
                sql = string.Format(sql, year, Convert.ToInt32(year) - 1, unitStr, beginTime, endTime);
            }
            else
            {
                sql = @"select unit_Name as Unit,SUM(ht) as AcountYearNow,SUM(mbz) as AllotUnitNow,
SUM(ht12) as AcountYear,SUM(mbz12) as AllotUnitLast,sum(by12) AS AcountMonth ,sum(by13)AS 'AcountMonthNow'
from (
SELECT *,
isnull
(
  (
   SELECT UnitAllot FROM cm_UnitCopAllot WHERE AllotYear='{0}' AND  UnitID=c.unit_ID
  ),0
) AS mbz,0 as ht12 ,0 as mbz12 FROM
(
SELECT a.unit_ID,a.unit_name ,
isnull(sum(b.cpr_Acount),0) AS ht,0 AS by12,0 AS by13
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
AND b.cpr_SignDate between'{0}-01-01'and'{0}-{4}' 
 WHERE {2}
GROUP BY a.unit_Name,a.unit_ID   
) AS c 
union all
SELECT *,
isnull
(
  (
   SELECT UnitAllot FROM cm_UnitCopAllot WHERE AllotYear='{1}'AND  UnitID=c.unit_ID
  ),0
) AS mbz12 FROM
(
SELECT a.unit_ID,a.unit_name,0 as ht,0 AS by12,0 AS by13,0 as mbz,
isnull(sum(b.cpr_Acount),0) AS ht12
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
AND b.cpr_SignDate between'{1}-01-01'and'{1}-{4}' 
 WHERE {2}
GROUP BY a.unit_Name,a.unit_ID    
) AS c 
UNION ALL
SELECT a.unit_ID,a.unit_name,0 AS ht,
isnull(sum(b.cpr_Acount),0) AS by12,0 AS by13,0 AS mbz,0 AS ht12,0 AS mbz12
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
AND b.cpr_SignDate between '{1}-{3}' and '{1}-{4}'
 WHERE {2}
GROUP BY a.unit_Name,a.unit_ID 
UNION ALL 
SELECT a.unit_ID,a.unit_name,0 AS ht,0 AS by12,
isnull(sum(b.cpr_Acount),0) AS by13,0 AS mbz,0 AS ht12,0 AS mbz12
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
AND b.cpr_SignDate between '{0}-{3}' and '{0}-{4}'
WHERE {2}
GROUP BY a.unit_Name,a.unit_ID 
) d group by unit_Name ,unit_ID";
                sql = string.Format(sql, year, Convert.ToInt32(year) - 1, unitStr, beginTime, endTime);
            }
            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            List<TG.Model.CopContrast> list = new List<Model.CopContrast>();
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                TG.Model.CopContrast model = new Model.CopContrast();
                model.Unit = item["Unit"].ToString().Trim();
                model.AcountMonth = Convert.ToDecimal(item["AcountMonth"]);
                model.AcountMonthNow = Convert.ToDecimal(item["AcountMonthNow"].ToString().Trim());
                model.AcountYear = Convert.ToDecimal(item["AcountYear"].ToString().Trim());
                model.AcountYearNow = Convert.ToDecimal(item["AcountYearNow"].ToString().Trim());


                model.AllotUnitLast = Convert.ToInt32(Convert.ToDecimal(item["AllotUnitLast"].ToString().Trim()));
                model.AllotUnitNow = Convert.ToInt32(Convert.ToDecimal(item["AllotUnitNow"].ToString().Trim()));

                list.Add(model);
            }
            return list;
        }


    }
}
