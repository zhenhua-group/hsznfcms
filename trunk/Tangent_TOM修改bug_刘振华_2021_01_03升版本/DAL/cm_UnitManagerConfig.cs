﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_UnitManagerConfig
	/// </summary>
	public partial class cm_UnitManagerConfig
	{
		public cm_UnitManagerConfig()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("id", "cm_UnitManagerConfig"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_UnitManagerConfig");
			strSql.Append(" where id="+id+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public int ExistsID(int unitid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id from cm_UnitManagerConfig");
            strSql.Append(" where unit_id=" + unitid + " ");
            object obj=DbHelperSQL.GetSingle(strSql.ToString());
            if (obj==null)
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_UnitManagerConfig model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.unit_id != null)
			{
				strSql1.Append("unit_id,");
				strSql2.Append(""+model.unit_id+",");
			}
			if (model.unitusers != null)
			{
				strSql1.Append("unitusers,");
				strSql2.Append("'"+model.unitusers+"',");
			}
			if (model.managerusers != null)
			{
				strSql1.Append("managerusers,");
				strSql2.Append("'"+model.managerusers+"',");
			}
			strSql.Append("insert into cm_UnitManagerConfig(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_UnitManagerConfig model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_UnitManagerConfig set ");
			if (model.unit_id != null)
			{
				strSql.Append("unit_id="+model.unit_id+",");
			}
			if (model.unitusers != null)
			{
				strSql.Append("unitusers='"+model.unitusers+"',");
			}
			else
			{
				strSql.Append("unitusers= null ,");
			}
			if (model.managerusers != null)
			{
				strSql.Append("managerusers='"+model.managerusers+"',");
			}
			else
			{
				strSql.Append("managerusers= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where id="+ model.id+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_UnitManagerConfig ");
			strSql.Append(" where id="+id+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_UnitManagerConfig ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_UnitManagerConfig GetModel(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" id,unit_id,unitusers,managerusers ");
			strSql.Append(" from cm_UnitManagerConfig ");
			strSql.Append(" where id="+id+"" );
			TG.Model.cm_UnitManagerConfig model=new TG.Model.cm_UnitManagerConfig();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["id"]!=null && ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["unit_id"]!=null && ds.Tables[0].Rows[0]["unit_id"].ToString()!="")
				{
					model.unit_id=int.Parse(ds.Tables[0].Rows[0]["unit_id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["unitusers"]!=null && ds.Tables[0].Rows[0]["unitusers"].ToString()!="")
				{
					model.unitusers=ds.Tables[0].Rows[0]["unitusers"].ToString();
				}
				if(ds.Tables[0].Rows[0]["managerusers"]!=null && ds.Tables[0].Rows[0]["managerusers"].ToString()!="")
				{
					model.managerusers=ds.Tables[0].Rows[0]["managerusers"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,unit_id,unitusers,managerusers ");
			strSql.Append(" FROM cm_UnitManagerConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,unit_id,unitusers,managerusers ");
			strSql.Append(" FROM cm_UnitManagerConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_UnitManagerConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from cm_UnitManagerConfig T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

