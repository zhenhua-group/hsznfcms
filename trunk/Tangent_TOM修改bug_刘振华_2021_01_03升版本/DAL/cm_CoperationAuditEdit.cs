﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using System.Data;

namespace TG.DAL
{
    /// 数据访问类:cm_AuditRecordEdit
    /// </summary>
    public partial class cm_AuditRecordEdit
    {
        public cm_AuditRecordEdit()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int SysNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_AuditRecordEdit");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CoperationAuditEdit model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_AuditRecordEdit(");
            strSql.Append("CoperationSysNo,Options,ChangeDetails,InUser,InDate,Status,UndertakeProposal,OperateDepartmentProposal,ManageLevel,NeedLegalAdviser,LegalAdviserProposal,TechnologyDepartmentProposal,GeneralManagerProposal,BossProposal,AuditUser,AuditDate)");
            strSql.Append(" values (");
            strSql.Append("@CoperationSysNo,@Options,@ChangeDetails,@InUser,@InDate,@Status,@UndertakeProposal,@OperateDepartmentProposal,@ManageLevel,@NeedLegalAdviser,@LegalAdviserProposal,@TechnologyDepartmentProposal,@GeneralManagerProposal,@BossProposal,@AuditUser,@AuditDate)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@CoperationSysNo", SqlDbType.Int,4),
					new SqlParameter("@Options", SqlDbType.NVarChar,200),
					new SqlParameter("@InUser", SqlDbType.Int,4),
					new SqlParameter("@InDate", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.Char,1),
					new SqlParameter("@UndertakeProposal", SqlDbType.NVarChar,200),
					new SqlParameter("@OperateDepartmentProposal", SqlDbType.NVarChar,200),
					new SqlParameter("@ManageLevel", SqlDbType.Int,4),
					new SqlParameter("@NeedLegalAdviser", SqlDbType.Int,4),
					new SqlParameter("@LegalAdviserProposal", SqlDbType.NVarChar,2000),
					new SqlParameter("@TechnologyDepartmentProposal", SqlDbType.NVarChar,200),
					new SqlParameter("@GeneralManagerProposal", SqlDbType.NVarChar,200),
					new SqlParameter("@BossProposal", SqlDbType.NVarChar,200),
					new SqlParameter("@AuditUser", SqlDbType.NVarChar,200),
					new SqlParameter("@AuditDate", SqlDbType.NVarChar,300), 
                    new SqlParameter("@ChangeDetails", SqlDbType.NVarChar,200)};
            parameters[0].Value = model.CoperationSysNo;
            parameters[1].Value = model.Options;
            parameters[2].Value = model.InUser;
            parameters[3].Value = model.InDate;
            parameters[4].Value = model.Status;
            parameters[5].Value = model.UndertakeProposal;
            parameters[6].Value = model.OperateDepartmentProposal;
            parameters[7].Value = model.ManageLevel;
            parameters[8].Value = model.NeedLegalAdviser;
            parameters[9].Value = model.LegalAdviserProposal;
            parameters[10].Value = model.TechnologyDepartmentProposal;
            parameters[11].Value = model.GeneralManagerProposal;
            parameters[12].Value = model.BossProposal;
            parameters[13].Value = model.AuditUser;
            parameters[14].Value = model.AuditDate;
            parameters[15].Value = model.ChangeDetails;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CoperationAuditEdit model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_AuditRecordEdit set ");
            strSql.Append("CoperationSysNo=@CoperationSysNo,");
            strSql.Append("Options=@Options,");
            strSql.Append("ChangeDetails=@ChangeDetails,");
            strSql.Append("InUser=@InUser,");
            strSql.Append("InDate=@InDate,");
            strSql.Append("Status=@Status,");
            strSql.Append("UndertakeProposal=@UndertakeProposal,");
            strSql.Append("OperateDepartmentProposal=@OperateDepartmentProposal,");
            strSql.Append("ManageLevel=@ManageLevel,");
            strSql.Append("NeedLegalAdviser=@NeedLegalAdviser,");
            strSql.Append("LegalAdviserProposal=@LegalAdviserProposal,");
            strSql.Append("TechnologyDepartmentProposal=@TechnologyDepartmentProposal,");
            strSql.Append("GeneralManagerProposal=@GeneralManagerProposal,");
            strSql.Append("BossProposal=@BossProposal,");
            strSql.Append("AuditUser=@AuditUser,");
            strSql.Append("AuditDate=@AuditDate");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@CoperationSysNo", SqlDbType.Int,4),
					new SqlParameter("@Options", SqlDbType.NVarChar,200),
					new SqlParameter("@InUser", SqlDbType.Int,4),
					new SqlParameter("@InDate", SqlDbType.DateTime),
					new SqlParameter("@Status", SqlDbType.Char,1),
					new SqlParameter("@UndertakeProposal", SqlDbType.NVarChar,200),
					new SqlParameter("@OperateDepartmentProposal", SqlDbType.NVarChar,200),
					new SqlParameter("@ManageLevel", SqlDbType.Int,4),
					new SqlParameter("@NeedLegalAdviser", SqlDbType.Int,4),
					new SqlParameter("@LegalAdviserProposal", SqlDbType.NVarChar,2000),
					new SqlParameter("@TechnologyDepartmentProposal", SqlDbType.NVarChar,200),
					new SqlParameter("@GeneralManagerProposal", SqlDbType.NVarChar,200),
					new SqlParameter("@BossProposal", SqlDbType.NVarChar,200),
					new SqlParameter("@AuditUser", SqlDbType.NVarChar,200),
					new SqlParameter("@AuditDate", SqlDbType.NVarChar,300),
					new SqlParameter("@SysNo", SqlDbType.Int,4),
                    new SqlParameter("@ChangeDetails", SqlDbType.NVarChar,200)};
            parameters[0].Value = model.CoperationSysNo;
            parameters[1].Value = model.Options;
            parameters[2].Value = model.InUser;
            parameters[3].Value = model.InDate;
            parameters[4].Value = model.Status;
            parameters[5].Value = model.UndertakeProposal;
            parameters[6].Value = model.OperateDepartmentProposal;
            parameters[7].Value = model.ManageLevel;
            parameters[8].Value = model.NeedLegalAdviser;
            parameters[9].Value = model.LegalAdviserProposal;
            parameters[10].Value = model.TechnologyDepartmentProposal;
            parameters[11].Value = model.GeneralManagerProposal;
            parameters[12].Value = model.BossProposal;
            parameters[13].Value = model.AuditUser;
            parameters[14].Value = model.AuditDate;
            parameters[15].Value = model.SysNo;
            parameters[16].Value = model.ChangeDetails;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SysNo)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_AuditRecordEdit ");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SysNolist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_AuditRecordEdit ");
            strSql.Append(" where SysNo in (" + SysNolist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationAuditEdit GetModelByCoperationSysNo(int coperationSysNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" SysNo,CoperationSysNo,Options,ChangeDetails,InUser,InDate,Status,UndertakeProposal,OperateDepartmentProposal,TechnologyDepartmentProposal,GeneralManagerProposal,BossProposal,AuditUser,AuditDate,ManageLevel,NeedLegalAdviser,LegalAdviserProposal ");
            strSql.Append(" from cm_AuditRecordEdit ");
            strSql.Append(" where CoperationSysNo=" + coperationSysNo + " order by SysNo DESC");

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

            reader.Read();

            TG.Model.cm_CoperationAuditEdit coperationAuditEntity = TG.Common.EntityBuilder.EntityBuilder<TG.Model.cm_CoperationAuditEdit>.BuilderEntity(reader);

            reader.Close();

            return coperationAuditEntity;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationAuditEdit GetModel(int SysNo)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SysNo,CoperationSysNo,Options,ChangeDetails,InUser,InDate,Status,UndertakeProposal,OperateDepartmentProposal,ManageLevel,NeedLegalAdviser,LegalAdviserProposal,TechnologyDepartmentProposal,GeneralManagerProposal,BossProposal,AuditUser,AuditDate from cm_AuditRecordEdit ");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            TG.Model.cm_CoperationAuditEdit model = new TG.Model.cm_CoperationAuditEdit();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["SysNo"] != null && ds.Tables[0].Rows[0]["SysNo"].ToString() != "")
                {
                    model.SysNo = int.Parse(ds.Tables[0].Rows[0]["SysNo"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CoperationSysNo"] != null && ds.Tables[0].Rows[0]["CoperationSysNo"].ToString() != "")
                {
                    model.CoperationSysNo = int.Parse(ds.Tables[0].Rows[0]["CoperationSysNo"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Options"] != null && ds.Tables[0].Rows[0]["Options"].ToString() != "")
                {
                    model.Options = ds.Tables[0].Rows[0]["Options"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ChangeDetails"] != null && ds.Tables[0].Rows[0]["ChangeDetails"].ToString() != "")
                {
                    model.ChangeDetails = ds.Tables[0].Rows[0]["ChangeDetails"].ToString();
                }
                if (ds.Tables[0].Rows[0]["InUser"] != null && ds.Tables[0].Rows[0]["InUser"].ToString() != "")
                {
                    model.InUser = int.Parse(ds.Tables[0].Rows[0]["InUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InDate"] != null && ds.Tables[0].Rows[0]["InDate"].ToString() != "")
                {
                    model.InDate = DateTime.Parse(ds.Tables[0].Rows[0]["InDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UndertakeProposal"] != null && ds.Tables[0].Rows[0]["UndertakeProposal"].ToString() != "")
                {
                    model.UndertakeProposal = ds.Tables[0].Rows[0]["UndertakeProposal"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OperateDepartmentProposal"] != null && ds.Tables[0].Rows[0]["OperateDepartmentProposal"].ToString() != "")
                {
                    model.OperateDepartmentProposal = ds.Tables[0].Rows[0]["OperateDepartmentProposal"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ManageLevel"] != null && ds.Tables[0].Rows[0]["ManageLevel"].ToString() != "")
                {
                    model.ManageLevel = int.Parse(ds.Tables[0].Rows[0]["ManageLevel"].ToString());
                }
                if (ds.Tables[0].Rows[0]["NeedLegalAdviser"] != null && ds.Tables[0].Rows[0]["NeedLegalAdviser"].ToString() != "")
                {
                    model.NeedLegalAdviser = int.Parse(ds.Tables[0].Rows[0]["NeedLegalAdviser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LegalAdviserProposal"] != null && ds.Tables[0].Rows[0]["LegalAdviserProposal"].ToString() != "")
                {
                    model.LegalAdviserProposal = ds.Tables[0].Rows[0]["LegalAdviserProposal"].ToString();
                }
                if (ds.Tables[0].Rows[0]["TechnologyDepartmentProposal"] != null && ds.Tables[0].Rows[0]["TechnologyDepartmentProposal"].ToString() != "")
                {
                    model.TechnologyDepartmentProposal = ds.Tables[0].Rows[0]["TechnologyDepartmentProposal"].ToString();
                }
                if (ds.Tables[0].Rows[0]["GeneralManagerProposal"] != null && ds.Tables[0].Rows[0]["GeneralManagerProposal"].ToString() != "")
                {
                    model.GeneralManagerProposal = ds.Tables[0].Rows[0]["GeneralManagerProposal"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BossProposal"] != null && ds.Tables[0].Rows[0]["BossProposal"].ToString() != "")
                {
                    model.BossProposal = ds.Tables[0].Rows[0]["BossProposal"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditUser"] != null && ds.Tables[0].Rows[0]["AuditUser"].ToString() != "")
                {
                    model.AuditUser = ds.Tables[0].Rows[0]["AuditUser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditDate"] != null && ds.Tables[0].Rows[0]["AuditDate"].ToString() != "")
                {
                    model.AuditDate = ds.Tables[0].Rows[0]["AuditDate"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SysNo,CoperationSysNo,Options,ChangeDetails,InUser,InDate,Status,UndertakeProposal,OperateDepartmentProposal,ManageLevel,NeedLegalAdviser,LegalAdviserProposal,TechnologyDepartmentProposal,GeneralManagerProposal,BossProposal,AuditUser,AuditDate ");
            strSql.Append(" FROM cm_AuditRecordEdit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" SysNo,CoperationSysNo,Options,ChangeDetails,InUser,InDate,Status,UndertakeProposal,OperateDepartmentProposal,ManageLevel,NeedLegalAdviser,LegalAdviserProposal,TechnologyDepartmentProposal,GeneralManagerProposal,BossProposal,AuditUser,AuditDate ");
            strSql.Append(" FROM cm_AuditRecordEdit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_AuditRecordEdit ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.SysNo desc");
            }
            strSql.Append(")AS Row, T.*  from cm_AuditRecordEdit T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "cm_AuditRecordEdit";
            parameters[1].Value = "SysNo";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        /// <summary>
        /// 新规一条记录
        /// </summary>
        /// <param name="coperation"></param>
        /// <returns></returns>
        public int InsertCoperatioAuditRecord(TG.Model.cm_CoperationAuditEdit coperation)
        {
            string sql = "insert into cm_AuditRecordEdit(CoperationSysNo,Options,ChangeDetails,InUser,InDate,Status) values(";
            sql += "'" + coperation.CoperationSysNo + "',";
            sql += "'" + coperation.Options + "',";
            sql += "'" + coperation.ChangeDetails + "',";
            sql += "'" + coperation.InUser + "',";
            sql += "'" + DateTime.Now + "',";
            sql += "'A');select @@IDENTITY";
            int count = Convert.ToInt32(DbHelperSQL.GetSingle(sql));
            return count;
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="sysMsg"></param>
        /// <returns></returns>
        public int SendMsg(TG.Model.cm_SysMsg sysMsg)
        {
            string sql = "insert into cm_SysMsg(AuditRecordSysNo,FromUser,ToRole,Status,InDate)values(";
            sql += sysMsg.AuditRecordSysNo + ",";
            sql += sysMsg.FromUser + ",";
            sql += sysMsg.ToRole + ",";
            sql += sysMsg.MsgStatus + ",";
            sql += sysMsg.MsgInDate + ")";
            int count = DbHelperSQL.ExecuteSql(sql);
            return count;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="coperation"></param>
        /// <returns></returns>
        public int UpdateCoperatioAuditRecord(TG.Model.cm_CoperationAuditEdit coperation)
        {
            string sql = "update cm_AuditRecordEdit set Status='";
            sql += coperation.Status + "',";
            sql += "UndertakeProposal='" + coperation.UndertakeProposal + "',";
            sql += "OperateDepartmentProposal='" + coperation.OperateDepartmentProposal + "',";
            sql += "TechnologyDepartmentProposal='" + coperation.TechnologyDepartmentProposal + "',";
            sql += "GeneralManagerProposal = '" + coperation.GeneralManagerProposal + "',";
            sql += "BossProposal = '" + coperation.BossProposal + "',";
            sql += "AuditUser='" + coperation.AuditUser + "',";
            sql += "AuditDate='" + coperation.AuditDate + "', ";
            sql += "ManageLevel=" + coperation.ManageLevel + ",";
            sql += "NeedLegalAdviser=" + coperation.NeedLegalAdviser + ",";
            sql += "LegalAdviserProposal=N'" + coperation.LegalAdviserProposal + "'";
            sql += " where SysNo=" + coperation.SysNo;
            int count = DbHelperSQL.ExecuteSql(sql);
            return count;
        }

    }



}
