﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TG.DBUtility;
using TG.Model;
using TG.Common.EntityBuilder;
namespace TG.DAL
{
    public class cm_ProjectFileInfo
    {


        /// <summary>
        /// 工程设计归档
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_ProjectFileInfoNoByPager(int startIndex, int endIndex, string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", SqlDbType.Int,4),
					new SqlParameter("@endIndex", SqlDbType.Int,4),
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = strWhere;

            return DbHelperSQL.RunProcedure("P_cm_ProjectFileInfoNo", parameters, "ds");
        }
        /// <summary>
        /// 工程设计归档
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_ProjectFileInfoYesByPager(int startIndex, int endIndex, string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", SqlDbType.Int,4),
					new SqlParameter("@endIndex", SqlDbType.Int,4),
                    new SqlParameter("@query",SqlDbType.NVarChar,10000)
			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = strWhere;

            return DbHelperSQL.RunProcedure("P_cm_ProjectFileInfoYes", parameters, "ds");
        }

        /// <summary>
        /// 获取需要分页的数据总数--归档
        /// </summary>
        public object GetProjectFileInfoNoProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_ProjectFileInfoNo_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }

        /// <summary>
        /// 获取需要分页的数据总数--归档
        /// </summary>
        public object GetProjectFileInfoYesProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_ProjectFileInfoYes_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }

        /// <summary>
        /// 新增工程设计归档
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertProjectFileInfo(ProjectFileInfoEntity dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //删除
                command.CommandText = "delete from  cm_ProjectFileInfo where Project_ID=" + dataEntity.ProNo + "";
                command.ExecuteNonQuery();


                //新规子项
                dataEntity.File.ForEach((stage) =>
                {
                    if (stage.PageCount == null)
                    {
                        command.CommandText = "insert into cm_ProjectFileInfo(Project_ID,Num,name,PageCount,IsExist,Remark,InUser,InDate)values(" + dataEntity.ProNo + "," + stage.num + ",'" + stage.name + "','" + DBNull.Value + "'," + stage.IsExist + ",'" + stage.Remark + "'," + dataEntity.UserId + ",'" + DateTime.Now + "')";
                    }
                    else
                    {
                        command.CommandText = "insert into cm_ProjectFileInfo(Project_ID,Num,name,PageCount,IsExist,Remark,InUser,InDate)values(" + dataEntity.ProNo + "," + stage.num + ",'" + stage.name + "'," + stage.PageCount + "," + stage.IsExist + ",'" + stage.Remark + "'," + dataEntity.UserId + ",'" + DateTime.Now + "')";
                    }
                    command.ExecuteNonQuery();
                });


                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        /// 得到归档信息
        /// </summary>
        /// <param name="proId"></param>
        /// <returns></returns>
        public DataSet GetFileInfo(int proId)
        {
            string sql = "select * from cm_ProjectFileInfo where  Project_ID=" + proId + " order by num ";
            DataSet ds = DbHelperSQL.Query(sql);
            return ds;
        }

        /// <summary>
        /// 查询信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public List<ProjectFileAuditList> GetProjectFileAuditListView(string query, int startIndex, int endIndex)
        {
            List<ProjectFileAuditList> resultList = new List<ProjectFileAuditList>();

            SqlDataReader reader = DBUtility.DbHelperSQL.RunProcedure("P_cm_ProjectFileInfoYes", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });

            while (reader.Read())
            {
                ProjectFileAuditList auditListView = new ProjectFileAuditList
                {
                    Pro_ID = Convert.ToInt32(reader["pro_ID"]),
                    Pro_name = reader["pro_name"] == null ? string.Empty : reader["pro_name"].ToString(),
                    Pro_level = reader["pro_level"] == null ? string.Empty : reader["pro_level"].ToString(),
                    AuditLevel = reader["AuditLevel"] == null ? string.Empty : reader["AuditLevel"].ToString(),
                    Unit = reader["Unit"] == null ? string.Empty : reader["Unit"].ToString(),
                    Pro_number = reader["Pro_number"] == null ? string.Empty : reader["Pro_number"].ToString(),
                    PMName = reader["PMName"] == null ? string.Empty : reader["PMName"].ToString(),
                    Pro_status = reader["pro_status"] == null ? string.Empty : reader["pro_status"].ToString(),
                    InsertUserID = Convert.ToInt32(reader["InsertUserID"]),
                    Pro_startTime = reader["pro_startTime"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["pro_startTime"].ToString()),
                    Status = reader["Status"] == null ? string.Empty : reader["Status"].ToString()
                };
                resultList.Add(auditListView);
            }
            return resultList;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectFileInfoAudit GetModelByProIDSysNo(int? SysNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * ");
            strSql.Append(" from cm_ProjectFileInfoAudit ");
            strSql.Append(" where ProjectSysNo=" + SysNo + " order by SysNo DESC");

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

            reader.Read();

            TG.Model.cm_ProjectFileInfoAudit projectPlotInfoAudit = TG.Common.EntityBuilder.EntityBuilder<TG.Model.cm_ProjectFileInfoAudit>.BuilderEntity(reader);

            reader.Close();

            return projectPlotInfoAudit;
        }


        /// <summary>
        /// 新规一条审批记录
        /// </summary>
        /// <param name="coperation"></param>
        /// <returns></returns>
        public int InsertProjectFileAuditRecord(TG.Model.cm_ProjectFileInfoAudit model)
        {
            string sql = "insert into cm_ProjectFileInfoAudit(ProjectSysNo,InUser,InDate,Status) values(";
            sql += "'" + model.ProjectSysNo + "',";
            sql += "'" + model.InUser + "',";
            sql += "'" + DateTime.Now + "',";
            sql += "'A');select @@IDENTITY";
            int count = Convert.ToInt32(DbHelperSQL.GetSingle(sql));
            return count;
        }

        /// <summary>
        /// 更新审核信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateProjectFileAudit(cm_ProjectFileInfoAudit dataEntity)
        {
            string ChildrenSql = "";

            if (!string.IsNullOrEmpty(dataEntity.Suggestion))
            {

                ChildrenSql += " Suggestion = N'" + dataEntity.Suggestion + "|',";
            }
            if (!string.IsNullOrEmpty(dataEntity.AuditUser))
            {
                ChildrenSql += " AuditUser = N'" + dataEntity.AuditUser + ",',";
            }
            if (!string.IsNullOrEmpty(dataEntity.AuditDate))
            {
                ChildrenSql += " AuditDate = N'" + dataEntity.AuditDate + ",',";
            }
            if (!string.IsNullOrEmpty(dataEntity.Status))
            {
                ChildrenSql += " Status=N'" + dataEntity.Status + "',";
            }
            if (!string.IsNullOrEmpty(ChildrenSql))
            {
                string finalSql = "UPDATE cm_ProjectFileInfoAudit SET " + ChildrenSql.Substring(0, ChildrenSql.Length - 1) + " WHERE SysNo=" + dataEntity.SysNo;
                return DbHelperSQL.ExecuteSql(finalSql);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 得到审核信息
        /// </summary>
        /// <param name="queryEntity"></param>
        /// <returns></returns>
        public cm_ProjectFileInfoAudit GetProjectFileInfoAuditEntity(cm_ProjectFileAuditQueryEntity queryEntity)
        {
            string whereSql = " where 1=1 ";
            whereSql += " and SysNo =" + queryEntity.ProjectPlotAuditSysNo;

            if (queryEntity.ProjectSysNo != 0)
            {
                whereSql += " and ProjectSysNo=" + queryEntity.ProjectSysNo;
            }
            string sql = "select SysNo,ProjectSysNo,Status,Suggestion,AuditUser,AuditDate,InDate,InUser from cm_ProjectFileInfoAudit " + whereSql;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();
            cm_ProjectFileInfoAudit resultEntity = EntityBuilder<cm_ProjectFileInfoAudit>.BuilderEntity(reader);
            reader.Close();

            return resultEntity;
        }

        /// <summary>
        /// 得到审核配置信息
        /// </summary>
        /// <returns></returns>
        public List<string> GetAuditProcessDescription()
        {
            string sql = "select r.RoleName from cm_ProjectFileAuditConfig  ppc join cm_Role r on  r.SysNo = ppc.RoleSysNo order by ppc.position asc";

            List<string> resultString = new List<string>();

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    resultString.Add(reader["RoleName"].ToString());
                }
                reader.Close();
            }

            return resultString;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectFileInfoAudit GetProjectFileAuditModelSysNo(int? SysNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * ");
            strSql.Append(" from cm_ProjectFileInfoAudit ");
            strSql.Append(" where SysNo=" + SysNo + " order by SysNo DESC");

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

            reader.Read();

            TG.Model.cm_ProjectFileInfoAudit projectPlotInfoAudit = TG.Common.EntityBuilder.EntityBuilder<TG.Model.cm_ProjectFileInfoAudit>.BuilderEntity(reader);

            reader.Close();

            return projectPlotInfoAudit;
        }
    }
}
