﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using TG.DBUtility;//Please add references

namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_CompensationSet
    /// </summary>

    public partial class cm_CompensationSet
    {
        public cm_CompensationSet()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CompensationSet model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.MemberId != null)
            {
                strSql1.Append("UnitId,");
                strSql2.Append("" + model.UnitId + ",");
            }
            if (model.MemberId != null)
            {
                strSql1.Append("MemberId,");
                strSql2.Append("" + model.MemberId + ",");
            }
            if (model.ComValue != null)
            {
                strSql1.Append("ComValue,");
                strSql2.Append("" + model.ComValue + ",");
            }
            if (model.ComValue != null)
            {
                strSql1.Append("ValueYear,");
                strSql2.Append("'" + model.ValueYear + "',");
            }

            strSql.Append("insert into cm_CompensationSet(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CompensationSet model)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append("update cm_CompensationSet set ");

            if (model.UnitId != null)
            {
                strSql.Append("UnitId=" + model.UnitId + ",");
            }
            else
            {
                strSql.Append("MemberId= null ,");
            }
            if (model.ComValue != null)
            {
                strSql.Append("ComValue=" + model.ComValue + ",");
            }
            else
            {
                strSql.Append("ComValue= null ,");
            }
            if (model.ValueYear != null)
            {
                strSql.Append("ValueYear='" + model.ValueYear + "',");
            }
            else
            {
                strSql.Append("ValueYear= null ,");
            }

            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where ID=" + model.ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_CompensationSet ");
            strSql.Append(" where ID=" + ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            string tempIDList = IDlist;
            if (!string.IsNullOrEmpty(IDlist))
            {
                tempIDList = tempIDList.Substring(0, IDlist.Length - 1);
            }
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_CompensationSet ");
            strSql.Append(" where ID in (" + tempIDList + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CompensationSet GetModel(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" ID,UnitId,MemberId,ComValue,ValueYear ");
            strSql.Append(" from cm_CompensationSet ");
            strSql.Append(" where ID=" + ID + "");
            TG.Model.cm_CompensationSet model = new TG.Model.cm_CompensationSet();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UnitId"] != null && ds.Tables[0].Rows[0]["UnitId"].ToString() != "")
                {
                    model.UnitId = int.Parse(ds.Tables[0].Rows[0]["UnitId"].ToString());
                }
                if (ds.Tables[0].Rows[0]["MemberId"] != null && ds.Tables[0].Rows[0]["MemberId"].ToString() != "")
                {
                    model.MemberId = int.Parse(ds.Tables[0].Rows[0]["MemberId"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ComValue"] != null && ds.Tables[0].Rows[0]["ComValue"].ToString() != "")
                {
                    model.ComValue = decimal.Parse(ds.Tables[0].Rows[0]["ComValue"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ValueYear"] != null && ds.Tables[0].Rows[0]["ValueYear"].ToString() != "")
                {
                    model.ValueYear = ds.Tables[0].Rows[0]["ValueYear"].ToString();
                }

                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            //strSql.Append("select ID,UnitId,MemberId,ValueYear,ComValue");
            //strSql.Append(" FROM cm_CompensationSet ");
            strSql.Append(@"select * from 
				(
				select cm_CompensationSet.*,
					isnull((select unit_name from tg_unit where unit_id=cm_CompensationSet.UnitId),'无') as unit_Name,
					isnull((select mem_name FROM tg_member where mem_id=cm_CompensationSet.MemberId),'无') as mem_Name
				from cm_CompensationSet ) C where 1=1 ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(strWhere);
            }

            return DbHelperSQL.Query(strSql.ToString());
        }


        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,UnitId,MemberId,ComValue,ValueYear ");
            strSql.Append(" FROM cm_CompensationSet ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_CompensationSet ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_CompensationSet T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        */
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetUnitAllotListByPager(string strWhere, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,UnitId,MemberId,ComValue,ValueYear ");
            strSql.Append(" FROM cm_CompensationSet ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString(), startIndex, endIndex);
        }
        #endregion  Method
    }
}
