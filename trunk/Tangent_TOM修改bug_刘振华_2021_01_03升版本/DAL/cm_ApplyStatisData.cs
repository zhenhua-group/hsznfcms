﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_ApplyStatisData
	/// </summary>
	public partial class cm_ApplyStatisData
	{
		public cm_ApplyStatisData()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_ApplyStatisData model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.mem_id != null)
			{
				strSql1.Append("mem_id,");
				strSql2.Append(""+model.mem_id+",");
			}
			if (model.dataYear != null)
			{
				strSql1.Append("dataYear,");
				strSql2.Append(""+model.dataYear+",");
			}
			if (model.dataMonth != null)
			{
				strSql1.Append("dataMonth,");
				strSql2.Append(""+model.dataMonth+",");
			}
			if (model.dataDay != null)
			{
				strSql1.Append("dataDay,");
				strSql2.Append(""+model.dataDay+",");
			}
			if (model.dataValue != null)
			{
				strSql1.Append("dataValue,");
				strSql2.Append(""+model.dataValue+",");
			}
			if (model.dataType != null)
			{
				strSql1.Append("dataType,");
				strSql2.Append("'"+model.dataType+"',");
			}
			if (model.dataSource != null)
			{
				strSql1.Append("dataSource,");
				strSql2.Append("'"+model.dataSource+"',");
			}
			if (model.dataContent != null)
			{
				strSql1.Append("dataContent,");
				strSql2.Append("'"+model.dataContent+"',");
			}
			if (model.addUserId != null)
			{
				strSql1.Append("addUserId,");
				strSql2.Append(""+model.addUserId+",");
			}
			if (model.addDate != null)
			{
				strSql1.Append("addDate,");
				strSql2.Append("'"+model.addDate+"',");
			}
			strSql.Append("insert into cm_ApplyStatisData(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ApplyStatisData model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_ApplyStatisData set ");
			if (model.mem_id != null)
			{
				strSql.Append("mem_id="+model.mem_id+",");
			}
			if (model.dataYear != null)
			{
				strSql.Append("dataYear="+model.dataYear+",");
			}
			if (model.dataMonth != null)
			{
				strSql.Append("dataMonth="+model.dataMonth+",");
			}
			if (model.dataDay != null)
			{
				strSql.Append("dataDay="+model.dataDay+",");
			}
			if (model.dataValue != null)
			{
				strSql.Append("dataValue="+model.dataValue+",");
			}
			if (model.dataType != null)
			{
				strSql.Append("dataType='"+model.dataType+"',");
			}
			else
			{
				strSql.Append("dataType= null ,");
			}
			if (model.dataSource != null)
			{
				strSql.Append("dataSource='"+model.dataSource+"',");
			}
			else
			{
				strSql.Append("dataSource= null ,");
			}
			if (model.dataContent != null)
			{
				strSql.Append("dataContent='"+model.dataContent+"',");
			}
			else
			{
				strSql.Append("dataContent= null ,");
			}
			if (model.addUserId != null)
			{
				strSql.Append("addUserId="+model.addUserId+",");
			}
			if (model.addDate != null)
			{
				strSql.Append("addDate='"+model.addDate+"',");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where id="+ model.id+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ApplyStatisData ");
			strSql.Append(" where id="+id+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ApplyStatisData ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ApplyStatisData GetModel(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" id,mem_id,dataYear,dataMonth,dataDay,dataValue,dataType,dataSource,dataContent,addUserId,addDate ");
			strSql.Append(" from cm_ApplyStatisData ");
			strSql.Append(" where id="+id+"" );
			TG.Model.cm_ApplyStatisData model=new TG.Model.cm_ApplyStatisData();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["id"]!=null && ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_id"]!=null && ds.Tables[0].Rows[0]["mem_id"].ToString()!="")
				{
					model.mem_id=int.Parse(ds.Tables[0].Rows[0]["mem_id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["dataYear"]!=null && ds.Tables[0].Rows[0]["dataYear"].ToString()!="")
				{
					model.dataYear=int.Parse(ds.Tables[0].Rows[0]["dataYear"].ToString());
				}
				if(ds.Tables[0].Rows[0]["dataMonth"]!=null && ds.Tables[0].Rows[0]["dataMonth"].ToString()!="")
				{
					model.dataMonth=int.Parse(ds.Tables[0].Rows[0]["dataMonth"].ToString());
				}
				if(ds.Tables[0].Rows[0]["dataDay"]!=null && ds.Tables[0].Rows[0]["dataDay"].ToString()!="")
				{
					model.dataDay=int.Parse(ds.Tables[0].Rows[0]["dataDay"].ToString());
				}
				if(ds.Tables[0].Rows[0]["dataValue"]!=null && ds.Tables[0].Rows[0]["dataValue"].ToString()!="")
				{
					model.dataValue=decimal.Parse(ds.Tables[0].Rows[0]["dataValue"].ToString());
				}
				if(ds.Tables[0].Rows[0]["dataType"]!=null && ds.Tables[0].Rows[0]["dataType"].ToString()!="")
				{
					model.dataType=ds.Tables[0].Rows[0]["dataType"].ToString();
				}
				if(ds.Tables[0].Rows[0]["dataSource"]!=null && ds.Tables[0].Rows[0]["dataSource"].ToString()!="")
				{
					model.dataSource=ds.Tables[0].Rows[0]["dataSource"].ToString();
				}
				if(ds.Tables[0].Rows[0]["dataContent"]!=null && ds.Tables[0].Rows[0]["dataContent"].ToString()!="")
				{
					model.dataContent=ds.Tables[0].Rows[0]["dataContent"].ToString();
				}
				if(ds.Tables[0].Rows[0]["addUserId"]!=null && ds.Tables[0].Rows[0]["addUserId"].ToString()!="")
				{
					model.addUserId=int.Parse(ds.Tables[0].Rows[0]["addUserId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["addDate"]!=null && ds.Tables[0].Rows[0]["addDate"].ToString()!="")
				{
					model.addDate=DateTime.Parse(ds.Tables[0].Rows[0]["addDate"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,mem_id,dataYear,dataMonth,dataDay,dataValue,dataType,dataSource,dataContent,addUserId,addDate ");
			strSql.Append(" FROM cm_ApplyStatisData ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,mem_id,dataYear,dataMonth,dataDay,dataValue,dataType,dataSource,dataContent,addUserId,addDate ");
			strSql.Append(" FROM cm_ApplyStatisData ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_ApplyStatisData ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from cm_ApplyStatisData T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

