﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_TranjjsProjectValueAllot
    /// </summary>
    public partial class cm_TranjjsProjectValueAllot
    {
        public cm_TranjjsProjectValueAllot()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_TranjjsProjectValueAllot model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_TranjjsProjectValueAllot(");
            strSql.Append("Pro_ID,AllotID,TotalCount,AllotCount,ItemType,Status,Thedeptallotpercent,Thedeptallotcount,ProgramPercent,ProgramCount,DesignManagerPercent,DesignManagerCount,ShouldBeValuePercent,ShouldBeValueCount,AllotUser,AllotDate,ActualAllountTime)");
            strSql.Append(" values (");
            strSql.Append("@Pro_ID,@AllotID,@TotalCount,@AllotCount,@ItemType,@Status,@Thedeptallotpercent,@Thedeptallotcount,@ProgramPercent,@ProgramCount,@DesignManagerPercent,@DesignManagerCount,@ShouldBeValuePercent,@ShouldBeValueCount,@AllotUser,@AllotDate,@ActualAllountTime)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@Pro_ID", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@TotalCount", SqlDbType.Decimal,9),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,9),
					new SqlParameter("@ItemType", SqlDbType.NVarChar,10),
					new SqlParameter("@Status", SqlDbType.NChar,1),
					new SqlParameter("@Thedeptallotpercent", SqlDbType.Decimal,9),
					new SqlParameter("@Thedeptallotcount", SqlDbType.Decimal,9),
					new SqlParameter("@ProgramPercent", SqlDbType.Decimal,9),
					new SqlParameter("@ProgramCount", SqlDbType.Decimal,9),
					new SqlParameter("@DesignManagerPercent", SqlDbType.Decimal,9),
					new SqlParameter("@DesignManagerCount", SqlDbType.Decimal,9),
					new SqlParameter("@ShouldBeValuePercent", SqlDbType.Decimal,9),
					new SqlParameter("@ShouldBeValueCount", SqlDbType.Decimal,9),
					new SqlParameter("@AllotUser", SqlDbType.Int,4),
					new SqlParameter("@AllotDate", SqlDbType.DateTime),
                    new SqlParameter("@ActualAllountTime", SqlDbType.NChar,10)};
            parameters[0].Value = model.Pro_ID;
            parameters[1].Value = model.AllotID;
            parameters[2].Value = model.TotalCount;
            parameters[3].Value = model.AllotCount;
            parameters[4].Value = model.ItemType;
            parameters[5].Value = model.Status;
            parameters[6].Value = model.Thedeptallotpercent;
            parameters[7].Value = model.Thedeptallotcount;
            parameters[8].Value = model.ProgramPercent;
            parameters[9].Value = model.ProgramCount;
            parameters[10].Value = model.DesignManagerPercent;
            parameters[11].Value = model.DesignManagerCount;
            parameters[12].Value = model.ShouldBeValuePercent;
            parameters[13].Value = model.ShouldBeValueCount;
            parameters[14].Value = model.AllotUser;
            parameters[15].Value = model.AllotDate;
            parameters[16].Value = model.ActualAllountTime;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }


        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_TranjjsProjectValueAllot model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_TranjjsProjectValueAllot set ");
            strSql.Append("Pro_ID=@Pro_ID,");
            strSql.Append("AllotID=@AllotID,");
            strSql.Append("TotalCount=@TotalCount,");
            strSql.Append("AllotCount=@AllotCount,");
            strSql.Append("ItemType=@ItemType,");
            strSql.Append("Status=@Status,");
            strSql.Append("Thedeptallotpercent=@Thedeptallotpercent,");
            strSql.Append("Thedeptallotcount=@Thedeptallotcount,");
            strSql.Append("ProgramPercent=@ProgramPercent,");
            strSql.Append("ProgramCount=@ProgramCount,");
            strSql.Append("DesignManagerPercent=@DesignManagerPercent,");
            strSql.Append("DesignManagerCount=@DesignManagerCount,");
            strSql.Append("ShouldBeValuePercent=@ShouldBeValuePercent,");
            strSql.Append("ShouldBeValueCount=@ShouldBeValueCount,");
            strSql.Append("AllotUser=@AllotUser,");
            strSql.Append("AllotDate=@AllotDate,");
            strSql.Append("ActualAllountTime=@ActualAllountTime");            
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@Pro_ID", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@TotalCount", SqlDbType.Decimal,9),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,9),
					new SqlParameter("@ItemType", SqlDbType.NVarChar,10),
					new SqlParameter("@Status", SqlDbType.NChar,1),
					new SqlParameter("@Thedeptallotpercent", SqlDbType.Decimal,9),
					new SqlParameter("@Thedeptallotcount", SqlDbType.Decimal,9),
					new SqlParameter("@ProgramPercent", SqlDbType.Decimal,9),
					new SqlParameter("@ProgramCount", SqlDbType.Decimal,9),
					new SqlParameter("@DesignManagerPercent", SqlDbType.Decimal,9),
					new SqlParameter("@DesignManagerCount", SqlDbType.Decimal,9),
					new SqlParameter("@ShouldBeValuePercent", SqlDbType.Decimal,9),
					new SqlParameter("@ShouldBeValueCount", SqlDbType.Decimal,9),
					new SqlParameter("@AllotUser", SqlDbType.Int,4),
					new SqlParameter("@AllotDate", SqlDbType.DateTime),
                    new SqlParameter("@ActualAllountTime", SqlDbType.NChar,10),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.Pro_ID;
            parameters[1].Value = model.AllotID;
            parameters[2].Value = model.TotalCount;
            parameters[3].Value = model.AllotCount;
            parameters[4].Value = model.ItemType;
            parameters[5].Value = model.Status;
            parameters[6].Value = model.Thedeptallotpercent;
            parameters[7].Value = model.Thedeptallotcount;
            parameters[8].Value = model.ProgramPercent;
            parameters[9].Value = model.ProgramCount;
            parameters[10].Value = model.DesignManagerPercent;
            parameters[11].Value = model.DesignManagerCount;
            parameters[12].Value = model.ShouldBeValuePercent;
            parameters[13].Value = model.ShouldBeValueCount;
            parameters[14].Value = model.AllotUser;
            parameters[15].Value = model.AllotDate;
            parameters[16].Value = model.ActualAllountTime;
            parameters[17].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            return rows;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_TranjjsProjectValueAllot ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_TranjjsProjectValueAllot GetModelByProID(int proid,int allotID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 * from cm_TranjjsProjectValueAllot ");
            strSql.Append(" where Pro_ID=@Pro_ID and AllotID=@AllotID ");
            SqlParameter[] parameters = {
					new SqlParameter("@Pro_ID", SqlDbType.Int,4),
                    new SqlParameter("@AllotID", SqlDbType.Int,4)
			};
            parameters[0].Value = proid;
            parameters[1].Value = allotID ;

            TG.Model.cm_TranjjsProjectValueAllot model = new TG.Model.cm_TranjjsProjectValueAllot();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Pro_ID"] != null && ds.Tables[0].Rows[0]["Pro_ID"].ToString() != "")
                {
                    model.Pro_ID = int.Parse(ds.Tables[0].Rows[0]["Pro_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotID"] != null && ds.Tables[0].Rows[0]["AllotID"].ToString() != "")
                {
                    model.AllotID = int.Parse(ds.Tables[0].Rows[0]["AllotID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalCount"] != null && ds.Tables[0].Rows[0]["TotalCount"].ToString() != "")
                {
                    model.TotalCount = decimal.Parse(ds.Tables[0].Rows[0]["TotalCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotCount"] != null && ds.Tables[0].Rows[0]["AllotCount"].ToString() != "")
                {
                    model.AllotCount = decimal.Parse(ds.Tables[0].Rows[0]["AllotCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ItemType"] != null && ds.Tables[0].Rows[0]["ItemType"].ToString() != "")
                {
                    model.ItemType = int.Parse(ds.Tables[0].Rows[0]["ItemType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Thedeptallotpercent"] != null && ds.Tables[0].Rows[0]["Thedeptallotpercent"].ToString() != "")
                {
                    model.Thedeptallotpercent = decimal.Parse(ds.Tables[0].Rows[0]["Thedeptallotpercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Thedeptallotcount"] != null && ds.Tables[0].Rows[0]["Thedeptallotcount"].ToString() != "")
                {
                    model.Thedeptallotcount = decimal.Parse(ds.Tables[0].Rows[0]["Thedeptallotcount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProgramPercent"] != null && ds.Tables[0].Rows[0]["ProgramPercent"].ToString() != "")
                {
                    model.ProgramPercent = decimal.Parse(ds.Tables[0].Rows[0]["ProgramPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProgramCount"] != null && ds.Tables[0].Rows[0]["ProgramCount"].ToString() != "")
                {
                    model.ProgramCount = decimal.Parse(ds.Tables[0].Rows[0]["ProgramCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DesignManagerPercent"] != null && ds.Tables[0].Rows[0]["DesignManagerPercent"].ToString() != "")
                {
                    model.DesignManagerPercent = decimal.Parse(ds.Tables[0].Rows[0]["DesignManagerPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DesignManagerCount"] != null && ds.Tables[0].Rows[0]["DesignManagerCount"].ToString() != "")
                {
                    model.DesignManagerCount = decimal.Parse(ds.Tables[0].Rows[0]["DesignManagerCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ShouldBeValuePercent"] != null && ds.Tables[0].Rows[0]["ShouldBeValuePercent"].ToString() != "")
                {
                    model.ShouldBeValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["ShouldBeValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ShouldBeValueCount"] != null && ds.Tables[0].Rows[0]["ShouldBeValueCount"].ToString() != "")
                {
                    model.ShouldBeValueCount = decimal.Parse(ds.Tables[0].Rows[0]["ShouldBeValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotUser"] != null && ds.Tables[0].Rows[0]["AllotUser"].ToString() != "")
                {
                    model.AllotUser = int.Parse(ds.Tables[0].Rows[0]["AllotUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotDate"] != null && ds.Tables[0].Rows[0]["AllotDate"].ToString() != "")
                {
                    model.AllotDate = DateTime.Parse(ds.Tables[0].Rows[0]["AllotDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ActualAllountTime"] != null && ds.Tables[0].Rows[0]["ActualAllountTime"].ToString() != "")
                {
                    model.ActualAllountTime = ds.Tables[0].Rows[0]["ActualAllountTime"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_TranjjsProjectValueAllot GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,Pro_ID,AllotID,TotalCount,AllotCount,ItemType,Status,Thedeptallotpercent,Thedeptallotcount,ProgramPercent,ProgramCount,DesignManagerPercent,DesignManagerCount,ShouldBeValuePercent,ShouldBeValueCount,AuditUser,AuditDate from cm_TranjjsProjectValueAllot ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_TranjjsProjectValueAllot model = new TG.Model.cm_TranjjsProjectValueAllot();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Pro_ID"] != null && ds.Tables[0].Rows[0]["Pro_ID"].ToString() != "")
                {
                    model.Pro_ID = int.Parse(ds.Tables[0].Rows[0]["Pro_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotID"] != null && ds.Tables[0].Rows[0]["AllotID"].ToString() != "")
                {
                    model.AllotID = int.Parse(ds.Tables[0].Rows[0]["AllotID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalCount"] != null && ds.Tables[0].Rows[0]["TotalCount"].ToString() != "")
                {
                    model.TotalCount = decimal.Parse(ds.Tables[0].Rows[0]["TotalCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotCount"] != null && ds.Tables[0].Rows[0]["AllotCount"].ToString() != "")
                {
                    model.AllotCount = decimal.Parse(ds.Tables[0].Rows[0]["AllotCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ItemType"] != null && ds.Tables[0].Rows[0]["ItemType"].ToString() != "")
                {
                    model.ItemType =int.Parse( ds.Tables[0].Rows[0]["ItemType"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Thedeptallotpercent"] != null && ds.Tables[0].Rows[0]["Thedeptallotpercent"].ToString() != "")
                {
                    model.Thedeptallotpercent = decimal.Parse(ds.Tables[0].Rows[0]["Thedeptallotpercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Thedeptallotcount"] != null && ds.Tables[0].Rows[0]["Thedeptallotcount"].ToString() != "")
                {
                    model.Thedeptallotcount = decimal.Parse(ds.Tables[0].Rows[0]["Thedeptallotcount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProgramPercent"] != null && ds.Tables[0].Rows[0]["ProgramPercent"].ToString() != "")
                {
                    model.ProgramPercent = decimal.Parse(ds.Tables[0].Rows[0]["ProgramPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProgramCount"] != null && ds.Tables[0].Rows[0]["ProgramCount"].ToString() != "")
                {
                    model.ProgramCount = decimal.Parse(ds.Tables[0].Rows[0]["ProgramCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DesignManagerPercent"] != null && ds.Tables[0].Rows[0]["DesignManagerPercent"].ToString() != "")
                {
                    model.DesignManagerPercent = decimal.Parse(ds.Tables[0].Rows[0]["DesignManagerPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DesignManagerCount"] != null && ds.Tables[0].Rows[0]["DesignManagerCount"].ToString() != "")
                {
                    model.DesignManagerCount = decimal.Parse(ds.Tables[0].Rows[0]["DesignManagerCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ShouldBeValuePercent"] != null && ds.Tables[0].Rows[0]["ShouldBeValuePercent"].ToString() != "")
                {
                    model.ShouldBeValuePercent = decimal.Parse(ds.Tables[0].Rows[0]["ShouldBeValuePercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ShouldBeValueCount"] != null && ds.Tables[0].Rows[0]["ShouldBeValueCount"].ToString() != "")
                {
                    model.ShouldBeValueCount = decimal.Parse(ds.Tables[0].Rows[0]["ShouldBeValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotUser"] != null && ds.Tables[0].Rows[0]["AllotUser"].ToString() != "")
                {
                    model.AllotUser = int.Parse(ds.Tables[0].Rows[0]["AllotUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotDate"] != null && ds.Tables[0].Rows[0]["AllotDate"].ToString() != "")
                {
                    model.AllotDate = DateTime.Parse(ds.Tables[0].Rows[0]["AllotDate"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * ");
            strSql.Append(" FROM cm_TranjjsProjectValueAllot ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }


        #endregion  Method
    }
}

