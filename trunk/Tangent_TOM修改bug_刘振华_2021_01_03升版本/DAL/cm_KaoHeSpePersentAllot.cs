﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeSpePersentAllot
	/// </summary>
	public partial class cm_KaoHeSpePersentAllot
	{
		public cm_KaoHeSpePersentAllot()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeSpePersentAllot model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeSpePersentAllot(");
			strSql.Append("ProKHNameId,AllotID,SpeID,SpeName,Persentold,Persent1,Persent2,AllotCount,AllotCountEnd,ZcStr,XmjyStr)");
			strSql.Append(" values (");
			strSql.Append("@ProKHNameId,@AllotID,@SpeID,@SpeName,@Persentold,@Persent1,@Persent2,@AllotCount,@AllotCountEnd,@ZcStr,@XmjyStr)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ProKHNameId", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@SpeID", SqlDbType.Int,4),
					new SqlParameter("@SpeName", SqlDbType.NVarChar,50),
					new SqlParameter("@Persentold", SqlDbType.Decimal,9),
					new SqlParameter("@Persent1", SqlDbType.Decimal,9),
					new SqlParameter("@Persent2", SqlDbType.Decimal,9),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,9),
					new SqlParameter("@AllotCountEnd", SqlDbType.Decimal,9),
					new SqlParameter("@ZcStr", SqlDbType.NVarChar,50),
					new SqlParameter("@XmjyStr", SqlDbType.NVarChar,50)};
			parameters[0].Value = model.ProKHNameId;
			parameters[1].Value = model.AllotID;
			parameters[2].Value = model.SpeID;
			parameters[3].Value = model.SpeName;
			parameters[4].Value = model.Persentold;
			parameters[5].Value = model.Persent1;
			parameters[6].Value = model.Persent2;
			parameters[7].Value = model.AllotCount;
			parameters[8].Value = model.AllotCountEnd;
			parameters[9].Value = model.ZcStr;
			parameters[10].Value = model.XmjyStr;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeSpePersentAllot model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeSpePersentAllot set ");
			strSql.Append("ProKHNameId=@ProKHNameId,");
			strSql.Append("AllotID=@AllotID,");
			strSql.Append("SpeID=@SpeID,");
			strSql.Append("SpeName=@SpeName,");
			strSql.Append("Persentold=@Persentold,");
			strSql.Append("Persent1=@Persent1,");
			strSql.Append("Persent2=@Persent2,");
			strSql.Append("AllotCount=@AllotCount,");
			strSql.Append("AllotCountEnd=@AllotCountEnd,");
			strSql.Append("ZcStr=@ZcStr,");
			strSql.Append("XmjyStr=@XmjyStr");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ProKHNameId", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@SpeID", SqlDbType.Int,4),
					new SqlParameter("@SpeName", SqlDbType.NVarChar,50),
					new SqlParameter("@Persentold", SqlDbType.Decimal,9),
					new SqlParameter("@Persent1", SqlDbType.Decimal,9),
					new SqlParameter("@Persent2", SqlDbType.Decimal,9),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,9),
					new SqlParameter("@AllotCountEnd", SqlDbType.Decimal,9),
					new SqlParameter("@ZcStr", SqlDbType.NVarChar,50),
					new SqlParameter("@XmjyStr", SqlDbType.NVarChar,50),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.ProKHNameId;
			parameters[1].Value = model.AllotID;
			parameters[2].Value = model.SpeID;
			parameters[3].Value = model.SpeName;
			parameters[4].Value = model.Persentold;
			parameters[5].Value = model.Persent1;
			parameters[6].Value = model.Persent2;
			parameters[7].Value = model.AllotCount;
			parameters[8].Value = model.AllotCountEnd;
			parameters[9].Value = model.ZcStr;
			parameters[10].Value = model.XmjyStr;
			parameters[11].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeSpePersentAllot ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeSpePersentAllot ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeSpePersentAllot GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,ProKHNameId,AllotID,SpeID,SpeName,Persentold,Persent1,Persent2,AllotCount,AllotCountEnd,ZcStr,XmjyStr from cm_KaoHeSpePersentAllot ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeSpePersentAllot model=new TG.Model.cm_KaoHeSpePersentAllot();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeSpePersentAllot DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeSpePersentAllot model=new TG.Model.cm_KaoHeSpePersentAllot();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["ProKHNameId"]!=null && row["ProKHNameId"].ToString()!="")
				{
					model.ProKHNameId=int.Parse(row["ProKHNameId"].ToString());
				}
				if(row["AllotID"]!=null && row["AllotID"].ToString()!="")
				{
					model.AllotID=int.Parse(row["AllotID"].ToString());
				}
				if(row["SpeID"]!=null && row["SpeID"].ToString()!="")
				{
					model.SpeID=int.Parse(row["SpeID"].ToString());
				}
				if(row["SpeName"]!=null)
				{
					model.SpeName=row["SpeName"].ToString();
				}
				if(row["Persentold"]!=null && row["Persentold"].ToString()!="")
				{
					model.Persentold=decimal.Parse(row["Persentold"].ToString());
				}
				if(row["Persent1"]!=null && row["Persent1"].ToString()!="")
				{
					model.Persent1=decimal.Parse(row["Persent1"].ToString());
				}
				if(row["Persent2"]!=null && row["Persent2"].ToString()!="")
				{
					model.Persent2=decimal.Parse(row["Persent2"].ToString());
				}
				if(row["AllotCount"]!=null && row["AllotCount"].ToString()!="")
				{
					model.AllotCount=decimal.Parse(row["AllotCount"].ToString());
				}
				if(row["AllotCountEnd"]!=null && row["AllotCountEnd"].ToString()!="")
				{
					model.AllotCountEnd=decimal.Parse(row["AllotCountEnd"].ToString());
				}
				if(row["ZcStr"]!=null)
				{
					model.ZcStr=row["ZcStr"].ToString();
				}
				if(row["XmjyStr"]!=null)
				{
					model.XmjyStr=row["XmjyStr"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,ProKHNameId,AllotID,SpeID,SpeName,Persentold,Persent1,Persent2,AllotCount,AllotCountEnd,ZcStr,XmjyStr ");
			strSql.Append(" FROM cm_KaoHeSpePersentAllot ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,ProKHNameId,AllotID,SpeID,SpeName,Persentold,Persent1,Persent2,AllotCount,AllotCountEnd,ZcStr,XmjyStr ");
			strSql.Append(" FROM cm_KaoHeSpePersentAllot ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeSpePersentAllot ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeSpePersentAllot T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeSpePersentAllot";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

