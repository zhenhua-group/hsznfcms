﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_RelativeContractInfo
	/// </summary>
	public partial class cm_RelativeContractInfo
	{
		public cm_RelativeContractInfo()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("RC_Id", "cm_RelativeContractInfo"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int RC_Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_RelativeContractInfo");
			strSql.Append(" where RC_Id="+RC_Id+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_RelativeContractInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.Cst_Id != null)
			{
				strSql1.Append("Cst_Id,");
				strSql2.Append(""+model.Cst_Id+",");
			}
			if (model.ContractId != null)
			{
				strSql1.Append("ContractId,");
				strSql2.Append("'"+model.ContractId+"',");
			}
			if (model.ContractNo != null)
			{
				strSql1.Append("ContractNo,");
				strSql2.Append("'"+model.ContractNo+"',");
			}
			if (model.ContractName != null)
			{
				strSql1.Append("ContractName,");
				strSql2.Append("'"+model.ContractName+"',");
			}
			if (model.ContractAccount != null)
			{
				strSql1.Append("ContractAccount,");
				strSql2.Append(""+model.ContractAccount+",");
			}
			if (model.SigneDate != null)
			{
				strSql1.Append("SigneDate,");
				strSql2.Append("'"+model.SigneDate+"',");
			}
			if (model.CompleteDate != null)
			{
				strSql1.Append("CompleteDate,");
				strSql2.Append("'"+model.CompleteDate+"',");
			}
			if (model.Functionary != null)
			{
				strSql1.Append("Functionary,");
				strSql2.Append("'"+model.Functionary+"',");
			}
			if (model.UpdateBy != null)
			{
				strSql1.Append("UpdateBy,");
				strSql2.Append(""+model.UpdateBy+",");
			}
			if (model.LastUpdate != null)
			{
				strSql1.Append("LastUpdate,");
				strSql2.Append("'"+model.LastUpdate+"',");
			}
			strSql.Append("insert into cm_RelativeContractInfo(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_RelativeContractInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_RelativeContractInfo set ");
			if (model.Cst_Id != null)
			{
				strSql.Append("Cst_Id="+model.Cst_Id+",");
			}
			if (model.ContractId != null)
			{
				strSql.Append("ContractId='"+model.ContractId+"',");
			}
			else
			{
				strSql.Append("ContractId= null ,");
			}
			if (model.ContractNo != null)
			{
				strSql.Append("ContractNo='"+model.ContractNo+"',");
			}
			else
			{
				strSql.Append("ContractNo= null ,");
			}
			if (model.ContractName != null)
			{
				strSql.Append("ContractName='"+model.ContractName+"',");
			}
			else
			{
				strSql.Append("ContractName= null ,");
			}
			if (model.ContractAccount != null)
			{
				strSql.Append("ContractAccount="+model.ContractAccount+",");
			}
			else
			{
				strSql.Append("ContractAccount= null ,");
			}
			if (model.SigneDate != null)
			{
				strSql.Append("SigneDate='"+model.SigneDate+"',");
			}
			else
			{
				strSql.Append("SigneDate= null ,");
			}
			if (model.CompleteDate != null)
			{
				strSql.Append("CompleteDate='"+model.CompleteDate+"',");
			}
			else
			{
				strSql.Append("CompleteDate= null ,");
			}
			if (model.Functionary != null)
			{
				strSql.Append("Functionary='"+model.Functionary+"',");
			}
			else
			{
				strSql.Append("Functionary= null ,");
			}
			if (model.UpdateBy != null)
			{
				strSql.Append("UpdateBy="+model.UpdateBy+",");
			}
			else
			{
				strSql.Append("UpdateBy= null ,");
			}
			if (model.LastUpdate != null)
			{
				strSql.Append("LastUpdate='"+model.LastUpdate+"',");
			}
			else
			{
				strSql.Append("LastUpdate= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where RC_Id="+ model.RC_Id+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int RC_Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_RelativeContractInfo ");
			strSql.Append(" where RC_Id="+RC_Id+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string RC_Idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_RelativeContractInfo ");
			strSql.Append(" where RC_Id in ("+RC_Idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_RelativeContractInfo GetModel(int RC_Id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" RC_Id,Cst_Id,ContractId,ContractNo,ContractName,ContractAccount,SigneDate,CompleteDate,Functionary,UpdateBy,LastUpdate ");
			strSql.Append(" from cm_RelativeContractInfo ");
			strSql.Append(" where RC_Id="+RC_Id+"" );
			TG.Model.cm_RelativeContractInfo model=new TG.Model.cm_RelativeContractInfo();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["RC_Id"]!=null && ds.Tables[0].Rows[0]["RC_Id"].ToString()!="")
				{
					model.RC_Id=int.Parse(ds.Tables[0].Rows[0]["RC_Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Cst_Id"]!=null && ds.Tables[0].Rows[0]["Cst_Id"].ToString()!="")
				{
					model.Cst_Id=int.Parse(ds.Tables[0].Rows[0]["Cst_Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ContractId"]!=null && ds.Tables[0].Rows[0]["ContractId"].ToString()!="")
				{
					model.ContractId=ds.Tables[0].Rows[0]["ContractId"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ContractNo"]!=null && ds.Tables[0].Rows[0]["ContractNo"].ToString()!="")
				{
					model.ContractNo=ds.Tables[0].Rows[0]["ContractNo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ContractName"]!=null && ds.Tables[0].Rows[0]["ContractName"].ToString()!="")
				{
					model.ContractName=ds.Tables[0].Rows[0]["ContractName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ContractAccount"]!=null && ds.Tables[0].Rows[0]["ContractAccount"].ToString()!="")
				{
					model.ContractAccount=decimal.Parse(ds.Tables[0].Rows[0]["ContractAccount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SigneDate"]!=null && ds.Tables[0].Rows[0]["SigneDate"].ToString()!="")
				{
					model.SigneDate=DateTime.Parse(ds.Tables[0].Rows[0]["SigneDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CompleteDate"]!=null && ds.Tables[0].Rows[0]["CompleteDate"].ToString()!="")
				{
					model.CompleteDate=DateTime.Parse(ds.Tables[0].Rows[0]["CompleteDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Functionary"]!=null && ds.Tables[0].Rows[0]["Functionary"].ToString()!="")
				{
					model.Functionary=ds.Tables[0].Rows[0]["Functionary"].ToString();
				}
				if(ds.Tables[0].Rows[0]["UpdateBy"]!=null && ds.Tables[0].Rows[0]["UpdateBy"].ToString()!="")
				{
					model.UpdateBy=int.Parse(ds.Tables[0].Rows[0]["UpdateBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LastUpdate"]!=null && ds.Tables[0].Rows[0]["LastUpdate"].ToString()!="")
				{
					model.LastUpdate=DateTime.Parse(ds.Tables[0].Rows[0]["LastUpdate"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select RC_Id,Cst_Id,ContractId,ContractNo,ContractName,ContractAccount,SigneDate,CompleteDate,Functionary,UpdateBy,LastUpdate ");
			strSql.Append(" FROM cm_RelativeContractInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" RC_Id,Cst_Id,ContractId,ContractNo,ContractName,ContractAccount,SigneDate,CompleteDate,Functionary,UpdateBy,LastUpdate ");
			strSql.Append(" FROM cm_RelativeContractInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_RelativeContractInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.RC_Id desc");
			}
			strSql.Append(")AS Row, T.*  from cm_RelativeContractInfo T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

