﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.DBUtility;
using TG.Model;
using System.Data.SqlClient;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class ProjectStatisticsDA
    {
        public List<ProjectStatisticsQuarterEntity> GetProjectRecordByQuarter(string year, string departmentName)
        {
            string whereSql = " 1=1";
            if (!string.IsNullOrEmpty(departmentName))
            {
                whereSql += " and Unit=N'" + departmentName + "'";
            }

            string sql = string.Format(@"select *,1 as Quarter from cm_Project where {1} and pro_startTime between N'{0}-01-01' and N'{0}-03-31' union all
                                                        (select *,2 as Quarter from cm_Project where {1} and pro_startTime between N'{0}-04-01' and N'{0}-06-30')union all
                                                        (select *,3 as Quarter from cm_Project where {1} and pro_startTime between N'{0}-07-01' and N'{0}-09-30')union all
                                                        (select *,4 as Quarter from cm_Project where {1} and pro_startTime between N'{0}-10-01' and N'{0}-12-31')", year, whereSql);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectStatisticsQuarterEntity> resultList = EntityBuilder<ProjectStatisticsQuarterEntity>.BuilderEntityList(reader);

            return resultList;
        }

        public List<ProjectStatisticsMonthEntity> GetProjectRecordByMonth(string year, string departmentName)
        {
            string whereSql = " 1=1";
            if (!string.IsNullOrEmpty(departmentName))
            {
                whereSql += " and Unit =N'" + departmentName + "'";
            }

            string sql = string.Format(@"SELECT 1 as Month, * from [cm_Project] where {2} and pro_startTime between N'{0}-1-1' and N'{0}-1-31'
                                                union all
                                                (SELECT 2 as Month, * from [cm_Project] where {2} and pro_startTime between N'{0}-02-01' and N'{0}-02-{1}')
                                                union all
                                                (SELECT 3 as Month, * from [cm_Project] where {2} and pro_startTime between N'{0}-03-01' and N'{0}-03-31')
                                                union all
                                                (SELECT 4 as Month, * from [cm_Project] where {2} and pro_startTime between N'{0}-04-01' and N'{0}-04-30')
                                                union all
                                                (SELECT 5 as Month, * from [cm_Project] where {2} and pro_startTime between N'{0}-05-01' and N'{0}-05-31')
                                                union all
                                                (SELECT 6 as Month, * from [cm_Project] where {2} and pro_startTime between N'{0}-06-01' and N'{0}-06-30')
                                                union all
                                                (SELECT 7 as Month, * from [cm_Project] where {2} and pro_startTime between N'{0}-07-01' and N'{0}-07-31')
                                                union all
                                                (SELECT 8 as Month, * from [cm_Project] where {2} and pro_startTime between N'{0}-08-01' and N'{0}-08-31')
                                                union all
                                                (SELECT 9 as Month, * from [cm_Project] where {2} and pro_startTime between N'{0}-09-01' and N'{0}-09-30')
                                                union all
                                                (SELECT 10 as Month, * from [cm_Project] where {2} and pro_startTime between N'{0}-10-01' and N'{0}-10-31')
                                                union all
                                                (SELECT 11 as Month, * from [cm_Project] where {2} and pro_startTime between N'{0}-11-01' and N'{0}-11-30')
                                                union all
                                                (SELECT 12 as Month, * from [cm_Project] where {2} and pro_startTime between N'{0}-12-01' and N'{0}-12-31')", year, CalculationOfLeapYear(int.Parse(year)), whereSql);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectStatisticsMonthEntity> resultList = EntityBuilder<ProjectStatisticsMonthEntity>.BuilderEntityList(reader);

            return resultList;
        }

        public DataSet GetProjectRecordDetailByQuarter(string year, int quarter)
        {
            string quarterString = "";

            switch (quarter)
            {
                case 1:
                    quarterString = " between N'" + year + "-01-01' and N'" + year + "-03-31' ";
                    break;
                case 2:
                    quarterString = " between N'" + year + "-04-01' and N'" + year + "-06-30' ";
                    break;
                case 3:
                    quarterString = " between N'" + year + "-07-01' and N'" + year + "-09-30' ";
                    break;
                case 4:
                    quarterString = " between N'" + year + "-10-01' and N'" + year + "-12-31' ";
                    break;
            }

            string sql = "select Unit,count(*) from cm_Project where pro_startTime " + quarterString + " group by Unit";

            return DbHelperSQL.Query(sql);
        }

        public DataSet GetProjectRecordDetailByMonth(int year, int month)
        {
            string quarterString = "";

            switch (month)
            {
                case 1:
                    quarterString = " between N'" + year + "-01-01' and N'" + year + "-01-31' ";
                    break;
                case 2:
                    quarterString = " between N'" + year + "-02-01' and N'" + year + "-02-" + CalculationOfLeapYear(year) + "' ";
                    break;
                case 3:
                    quarterString = " between N'" + year + "-03-01' and N'" + year + "-03-31' ";
                    break;
                case 4:
                    quarterString = " between N'" + year + "-04-01' and N'" + year + "-04-30' ";
                    break;
                case 5:
                    quarterString = " between N'" + year + "-05-01' and N'" + year + "-05-31' ";
                    break;
                case 6:
                    quarterString = " between N'" + year + "-06-01' and N'" + year + "-06-30' ";
                    break;
                case 7:
                    quarterString = " between N'" + year + "-07-01' and N'" + year + "-07-31' ";
                    break;
                case 8:
                    quarterString = " between N'" + year + "-08-01' and N'" + year + "-08-31' ";
                    break;
                case 9:
                    quarterString = " between N'" + year + "-09-01' and N'" + year + "-09-30' ";
                    break;
                case 10:
                    quarterString = " between N'" + year + "-10-01' and N'" + year + "-10-31' ";
                    break;
                case 11:
                    quarterString = " between N'" + year + "-11-01' and N'" + year + "-11-30' ";
                    break;
                case 12:
                    quarterString = " between N'" + year + "-12-01' and N'" + year + "-12-31' ";
                    break;
            }

            string sql = "select Unit,count(*) from cm_Project where pro_startTime " + quarterString + " group by Unit";

            return DbHelperSQL.Query(sql);
        }

        public DataSet GetProjectCountByProjectTypeAndYear(int year, string departmentName)
        {
            string whereSql = " 1=1 ";
            if (!string.IsNullOrEmpty(departmentName))
            {
                whereSql += " and Unit =N'" + departmentName + "'";
            }
            //按照项目类型统计 by long 20130515
            string sql = string.Format(@"Select dic_Name AS ProjectType,
                                                        (Select Count(*) 
                                                        From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                        Where {1} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-01-01' and N'{0}-12-31') as ProjectCount
                                                        From dbo.cm_Dictionary where dic_type='cpr_fl'",year,whereSql);
            return DbHelperSQL.Query(sql);
        }

        public List<ProjectStatisticsType> GetProjectCountByProjectTypeAndMonth(int year, string departmentName)
        {
            #region sql

            string whereSql = " 1=1";
            if (!string.IsNullOrEmpty(departmentName))
            {
                whereSql += " and Unit =N'" + departmentName + "'";
            }
            //按照项目类型统计 by long 20130515
            string sql = string.Format(@"               Select dic_Name AS ProjectType,
                                                                          (Select Count(*) From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                                          Where {2} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-01-01' and N'{0}-1-31') as ProjectCount,
                                                                          1 as Month
                                                                          From dbo.cm_Dictionary where dic_type='cpr_fl'
                                                                          union all
                                                                          (
                                                                         Select dic_Name AS ProjectType,
                                                                          (Select Count(*) From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                                          Where {2} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-02-01' and N'{0}-02-{1}') as ProjectCount,
                                                                          2 as Month
                                                                          From dbo.cm_Dictionary where dic_type='cpr_fl'
                                                                          )
                                                                          union all
                                                                          (
                                                                          Select dic_Name AS ProjectType,
                                                                          (Select Count(*) From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                                          Where {2} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-03-01' and N'{0}-03-31') as ProjectCount,
                                                                          3 as Month
                                                                          From dbo.cm_Dictionary where dic_type='cpr_fl'
                                                                          )
                                                                          union all
                                                                          (
                                                                          Select dic_Name AS ProjectType,
                                                                          (Select Count(*) From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                                          Where {2} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-04-01' and N'{0}-04-30') as ProjectCount,
                                                                          4 as Month
                                                                          From dbo.cm_Dictionary where dic_type='cpr_fl'
                                                                          )
                                                                         union all
                                                                          (
                                                                         Select dic_Name AS ProjectType,
                                                                          (Select Count(*) From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                                          Where {2} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-05-01' and N'{0}-05-31') as ProjectCount,
                                                                          5 as Month
                                                                          From dbo.cm_Dictionary where dic_type='cpr_fl'
                                                                          ) 
                                                                          union all
                                                                          (
                                                                         Select dic_Name AS ProjectType,
                                                                          (Select Count(*) From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                                          Where {2} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-06-01' and N'{0}-06-30') as ProjectCount,
                                                                          6 as Month
                                                                          From dbo.cm_Dictionary where dic_type='cpr_fl'
                                                                          )
                                                                          union all
                                                                          (
                                                                          Select dic_Name AS ProjectType,
                                                                          (Select Count(*) From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                                          Where {2} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-07-01' and N'{0}-07-31') as ProjectCount,
                                                                         7 as Month
                                                                          From dbo.cm_Dictionary where dic_type='cpr_fl'
                                                                          )
                                                                          union all
                                                                          (
                                                                          Select dic_Name AS ProjectType,
                                                                          (Select Count(*) From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                                          Where {2} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-08-01' and N'{0}-08-31') as ProjectCount,
                                                                          8 as Month
                                                                          From dbo.cm_Dictionary where dic_type='cpr_fl'
                                                                          )
                                                                         union all
                                                                          (
                                                                          Select dic_Name AS ProjectType,
                                                                          (Select Count(*) From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                                          Where {2} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-09-01' and N'{0}-09-30') as ProjectCount,
                                                                          9 as Month
                                                                          From dbo.cm_Dictionary where dic_type='cpr_fl'
                                                                          )
                                                                          union all
                                                                          (
                                                                          Select dic_Name AS ProjectType,
                                                                          (Select Count(*) From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                                          Where {2} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-10-01' and N'{0}-10-31') as ProjectCount,
                                                                          10 as Month
                                                                          From dbo.cm_Dictionary where dic_type='cpr_fl'
                                                                          )
                                                                          union all
                                                                          (
                                                                          Select dic_Name AS ProjectType,
                                                                          (Select Count(*) From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                                          Where {2} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-11-01' and N'{0}-11-30') as ProjectCount,
                                                                          11 as Month
                                                                          From dbo.cm_Dictionary where dic_type='cpr_fl'
                                                                          )
                                                                          union all
                                                                          (
                                                                          Select dic_Name AS ProjectType,
                                                                          (Select Count(*) From cm_Project PJ inner join cm_Coperation CP on PJ.CoperationSysNo=CP.cpr_id 
                                                                          Where {2} AND cpr_type=dic_Name  AND pro_startTime between N'{0}-12-01' and N'{0}-12-31') as ProjectCount,
                                                                          12 as Month
                                                                          From dbo.cm_Dictionary where dic_type='cpr_fl'
                                                                          )", year, CalculationOfLeapYear(year), whereSql);
            #endregion

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectStatisticsType> projectStatistics = EntityBuilder<ProjectStatisticsType>.BuilderEntityList(reader);

            return projectStatistics;
        }

        public int GetProjectCountByYear(int year, string departmentName)
        {
            string whereSql = "1=1";

            if (!string.IsNullOrEmpty(departmentName))
            {
                whereSql += " and Unit =N'" + departmentName + "'";
            }

            string sql = string.Format("select count(*) from cm_Project where {1} and pro_startTime between N'{0}-01-01' and N'{0}-12-31'", year, whereSql);

            object objCount = DbHelperSQL.GetSingle(sql);

            return objCount == null ? 0 : Convert.ToInt32(objCount);
        }

        public DataSet GetProjectTypeDetailByYearAndType(int year, string projectType)
        {
            projectType = new TG.DAL.cm_Dictionary().GetModel(int.Parse(projectType)).dic_Name;

            string sql = "select Unit as Department,count(*) as ProjectCount from cm_Project where  pro_startTime between N'" + year + "-01-01' and N'" + year + "-12-31' and (Select cpr_Type From cm_Coperation Where cpr_ID = CoperationSysNo)  like N'%" + projectType + "%' group by Unit";

            return DbHelperSQL.Query(sql);
        }

        public int GetProjectTypeDetailByMonthAndYear(int year, int month, string projectType, string deparment)
        {
            string quarterString = "";

            switch (month)
            {
                case 1:
                    quarterString = " between N'" + year + "-01-01' and N'" + year + "-01-31' ";
                    break;
                case 2:
                    quarterString = " between N'" + year + "-02-01' and N'" + year + "-02-" + CalculationOfLeapYear(year) + "' ";
                    break;
                case 3:
                    quarterString = " between N'" + year + "-03-01' and N'" + year + "-03-31' ";
                    break;
                case 4:
                    quarterString = " between N'" + year + "-04-01' and N'" + year + "-04-30' ";
                    break;
                case 5:
                    quarterString = " between N'" + year + "-05-01' and N'" + year + "-05-31' ";
                    break;
                case 6:
                    quarterString = " between N'" + year + "-06-01' and N'" + year + "-06-30' ";
                    break;
                case 7:
                    quarterString = " between N'" + year + "-07-01' and N'" + year + "-07-31' ";
                    break;
                case 8:
                    quarterString = " between N'" + year + "-08-01' and N'" + year + "-08-31' ";
                    break;
                case 9:
                    quarterString = " between N'" + year + "-09-01' and N'" + year + "-09-30' ";
                    break;
                case 10:
                    quarterString = " between N'" + year + "-10-01' and N'" + year + "-10-31' ";
                    break;
                case 11:
                    quarterString = " between N'" + year + "-11-01' and N'" + year + "-11-30' ";
                    break;
                case 12:
                    quarterString = " between N'" + year + "-12-01' and N'" + year + "-12-31' ";
                    break;
            }
            string sql = "select Count(*) from cm_Project where pro_startTime " + quarterString + " and Unit =N'" + deparment + "' and pro_kinds like N'%" + projectType + "%'";
            object resultObjInt = DbHelperSQL.GetSingle(sql);
            return resultObjInt == null ? 0 : Convert.ToInt32(resultObjInt);
        }


        public List<ProjectStatisticeTypeByMonth> GetProjectGroupByDepartment()
        {
            string sql = @"select Unit  as Department
                        ,count(*) as ProjectCount
                        from cm_Project 
                        group by Unit";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectStatisticeTypeByMonth> resultList = EntityBuilder<ProjectStatisticeTypeByMonth>.BuilderEntityList(reader);

            return resultList;
        }

        public List<ProjectSquareMetersQuarter> GetProjectSquareMetersByQuarter(int year, string departmentName)
        {
            string whereSql = " 1=1 ";
            if (!string.IsNullOrEmpty(departmentName))
            {
                whereSql += " and Unit =N'" + departmentName + "'";
            }

            string sql = string.Format(@"select isnull(sum(projectscale),0) as SquareMeters,1 as Quarter from cm_Project where {1} and pro_startTime between N'{0}-01-01' and N'{0}-03-31' 
                                                                        union all(select isnull(sum(projectscale),0) as SquareMeters,2 as Quarter from cm_Project where {1} and pro_startTime between N'{0}-04-01' and N'{0}-06-30')
                                                                        union all(select isnull(sum(projectscale),0) as SquareMeters,3 as Quarter from cm_Project where {1} and pro_startTime between N'{0}-07-01' and N'{0}-09-30')
                                                                        union all(select isnull(sum(projectscale),0) as SquareMeters,4 as Quarter from cm_Project where {1} and pro_startTime between N'{0}-10-01' and N'{0}-12-31')", year, whereSql);
            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectSquareMetersQuarter> resultList = EntityBuilder<ProjectSquareMetersQuarter>.BuilderEntityList(reader);

            return resultList;
        }


        public DataSet GetProjectSquareMetersByMonth(int year)
        {
            string sql = string.Format(@"SELECT 1 as Month, isnull(sum(projectscale),0 ) as SquareMeters from [cm_Project] where pro_startTime between N'{0}-1-1' and N'{0}-1-31'
                                                union all
                                                (SELECT 2 as Month, isnull(sum(projectscale),0 ) as SquareMeters from [cm_Project] where pro_startTime between N'{0}-02-01' and N'{0}-02-{1}')
                                                union all
                                                (SELECT 3 as Month, isnull(sum(projectscale),0 ) as SquareMeters from [cm_Project] where pro_startTime between N'{0}-03-01' and N'{0}-03-31')
                                                union all
                                                (SELECT 4 as Month, isnull(sum(projectscale),0 ) as SquareMeters from [cm_Project] where pro_startTime between N'{0}-04-01' and N'{0}-04-30')
                                                union all
                                                (SELECT 5 as Month, isnull(sum(projectscale),0 ) as SquareMeters from [cm_Project] where pro_startTime between N'{0}-05-01' and N'{0}-05-31')
                                                union all
                                                (SELECT 6 as Month, isnull(sum(projectscale),0 ) as SquareMeters from [cm_Project] where pro_startTime between N'{0}-06-01' and N'{0}-06-30')
                                                union all
                                                (SELECT 7 as Month, isnull(sum(projectscale),0 ) as SquareMeters from [cm_Project] where pro_startTime between N'{0}-07-01' and N'{0}-07-31')
                                                union all
                                                (SELECT 8 as Month, isnull(sum(projectscale),0 ) as SquareMeters from [cm_Project] where pro_startTime between N'{0}-08-01' and N'{0}-08-31')
                                                union all
                                                (SELECT 9 as Month, isnull(sum(projectscale),0 ) as SquareMeters from [cm_Project] where pro_startTime between N'{0}-09-01' and N'{0}-09-30')
                                                union all
                                                (SELECT 10 as Month, isnull(sum(projectscale),0 ) as SquareMeters from [cm_Project] where pro_startTime between N'{0}-10-01' and N'{0}-10-31')
                                                union all
                                                (SELECT 11 as Month, isnull(sum(projectscale),0 ) as SquareMeters from [cm_Project] where pro_startTime between N'{0}-11-01' and N'{0}-11-30')
                                                union all
                                                (SELECT 12 as Month, isnull(sum(projectscale),0 ) as SquareMeters from [cm_Project] where pro_startTime between N'{0}-12-01' and N'{0}-12-31')", year, CalculationOfLeapYear(year));

            return DbHelperSQL.Query(sql);
        }

        public DataSet GetProjectDetailSquareMetersByQuarter(int year, int quarter)
        {
            string quarterString = "";

            switch (quarter)
            {
                case 1:
                    quarterString = " between N'" + year + "-01-01' and N'" + year + "-03-31' ";
                    break;
                case 2:
                    quarterString = " between N'" + year + "-04-01' and N'" + year + "-06-30' ";
                    break;
                case 3:
                    quarterString = " between N'" + year + "-07-01' and N'" + year + "-09-30' ";
                    break;
                case 4:
                    quarterString = " between N'" + year + "-10-01' and N'" + year + "-12-31' ";
                    break;

            }
            string sql = "SELECT pro_name,projectscale,Unit from [cm_Project] where pro_startTime " + quarterString;

            return DbHelperSQL.Query(sql);
        }

        public DataSet GetProjectDetailSquareMetersByMonth(int year, int month)
        {
            string quarterString = "";

            switch (month)
            {
                case 1:
                    quarterString = " between N'" + year + "-01-01' and N'" + year + "-01-31' ";
                    break;
                case 2:
                    quarterString = " between N'" + year + "-02-01' and N'" + year + "-02-" + CalculationOfLeapYear(year) + "' ";
                    break;
                case 3:
                    quarterString = " between N'" + year + "-03-01' and N'" + year + "-03-31' ";
                    break;
                case 4:
                    quarterString = " between N'" + year + "-04-01' and N'" + year + "-04-30' ";
                    break;
                case 5:
                    quarterString = " between N'" + year + "-05-01' and N'" + year + "-05-31' ";
                    break;
                case 6:
                    quarterString = " between N'" + year + "-06-01' and N'" + year + "-06-30' ";
                    break;
                case 7:
                    quarterString = " between N'" + year + "-07-01' and N'" + year + "-07-31' ";
                    break;
                case 8:
                    quarterString = " between N'" + year + "-08-01' and N'" + year + "-08-31' ";
                    break;
                case 9:
                    quarterString = " between N'" + year + "-09-01' and N'" + year + "-09-30' ";
                    break;
                case 10:
                    quarterString = " between N'" + year + "-10-01' and N'" + year + "-10-31' ";
                    break;
                case 11:
                    quarterString = " between N'" + year + "-11-01' and N'" + year + "-11-30' ";
                    break;
                case 12:
                    quarterString = " between N'" + year + "-12-01' and N'" + year + "-12-31' ";
                    break;
            }
            string sql = "SELECT pro_name,projectscale,Unit from [cm_Project] where pro_startTime " + quarterString;

            return DbHelperSQL.Query(sql);
        }

        public DataSet GetProjectCountByStatus(int year, string departmentName)
        {
            string whereSql = " 1=1 ";
            if (!string.IsNullOrEmpty(departmentName))
            {
                whereSql += " and Unit =N'" + departmentName + "'";
            }

            string sql = string.Format(@"select count(*) as ProjectCount,N'进行中' as ProjectStatus ,1 as ProjectStatusInt from cm_Project where {1} and pro_Status like N'%进行中%' and pro_StartTime >= N'{0}-01-01'
                                                                union all 
                                                                (
	                                                                select count(*) as ProjectCount,N'完成' as ProjectStatus ,3 as ProjectStatusInt from cm_Project where {1} and pro_Status like N'%完成%' and pro_StartTime >= N'{0}-01-01'
                                                                )
                                                                union all
                                                                (
	                                                                select count(*) as ProjectCount,N'暂停' as ProjectStatus ,2 as ProjectStatusInt from cm_Project where {1} and pro_Status like N'%暂停%' and pro_StartTime >= N'{0}-01-01'
                                                                )", year, whereSql);
            return DbHelperSQL.Query(sql);
        }


        public DataSet GetProjectDetailByStatus(int year, string status)
        {
            string sql = string.Format(@"SELECT 
                                                                     tp.[pro_Name]
                                                                    ,tp.Unit
                                                                    ,tp.pro_StartTime
                                                                    ,tp.pro_FinishTime
                                                                    ,tp.pro_ID
                                                                    ,cp.pro_ID as ProjectSysNo
                                                                    FROM [dbo].[cm_Project] tp join cm_Project cp on cp.ReferenceSysNo = tp.pro_ID where tp.pro_StartTime >=N'{0}-01-01' and tp.pro_Status like N'%{1}%'", year, status);
            return DbHelperSQL.Query(sql);
        }

        private string CalculationOfLeapYear(int year)
        {
            if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
                return "29";
            else
                return "28";
        }
    }
}
