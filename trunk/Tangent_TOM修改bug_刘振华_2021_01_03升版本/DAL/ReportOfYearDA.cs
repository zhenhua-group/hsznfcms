﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.DBUtility;
using TG.Model;
using System.Data.SqlClient;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class ReportOfYearDA
    {
        public List<ReportOfYearViewEntity> GetProjectCountBetweenYear(int startYear, int endYear, string unit)
        {
            //部门
            if (unit != "")
            {
                unit = " AND Unit='" + unit + "'";
            }
            string sql = string.Format(" select {0} as Year, count(*) as ProjectCount,(select COUNT(distinct ProjectSysNo) as ProjectAuditPassCount from cm_Project p join cm_ProjectAudit pa on pa.ProjectSysNo = p.pro_ID where Status =N'D' AND p.pro_StartTime between N'{0}-01-01' and N'{0}-12-31' ) as ProjectAuditPassCount from cm_Project where (pro_StartTime between N'{0}-01-01' and N'{0}-12-31') {1}", startYear, unit);

            for (int i = startYear + 1; i <= endYear; i++)
            {
                sql += string.Format(" union(select {0} as Year, count(*) as ProjectCount,(select COUNT(distinct ProjectSysNo) as ProjectAuditPassCount from cm_Project p join cm_ProjectAudit pa on pa.ProjectSysNo = p.pro_ID where Status =N'D' AND p.pro_StartTime between N'{0}-01-01' and N'{0}-12-31' ) as ProjectAuditPassCount from cm_Project where (pro_StartTime between N'{0}-01-01' and N'{0}-12-31') {1})", i, unit);
            }

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ReportOfYearViewEntity> resultList = EntityBuilder<ReportOfYearViewEntity>.BuilderEntityList(reader);

            return resultList;
        }

        public List<ProjectOfReportOfYearViewEntity> GetProjectInfoByWhereCondition(string whereCondition, int pageCurrent, int pageSize, out int dataCount)
        {
            string sql = string.Format(@" SELECT TOP {0}
	                                                             pro_ID
	                                                            ,pro_order
	                                                            ,pro_name
	                                                            ,Pro_number
	                                                            ,pro_StruType
	                                                            ,pro_kinds
	                                                            ,pro_buildUnit
	                                                            ,pro_status
	                                                            ,pro_level
	                                                            ,pro_startTime
	                                                            ,pro_finishTime
	                                                            ,Pro_src
	                                                            ,ChgJia
	                                                            ,Phone
	                                                            ,CoperationSysNo
	                                                            ,Project_reletive
	                                                            ,Cpr_Acount
	                                                            ,Unit
	                                                            ,ProjectScale
	                                                            ,BuildAddress
	                                                            ,pro_Intro 
	                                                            ,[Status]
	                                                            ,[Suggestion]
	                                                            ,[AuditUser]
	                                                            ,AuditDate
	                                                            ,N' ' as ProjectFrom
	                                                            from (select pa.ProjectSysNo
	                                                            ,MAX(pa.[Status]) as Status
	                                                            ,MAX(pa.[Suggestion]) as Suggestion
	                                                            ,MAX(pa.[AuditUser]) as AuditUser
	                                                            ,MAX(pa.[AudtiDate]) as AuditDate
	                                                            from cm_ProjectAudit pa WITH(NOLOCK) group by pa.ProjectSysNo) pa right join
	                                                            cm_Project p WITH(NOLOCK) on p.pro_ID = pa.ProjectSysNo 
	                                                            where 1=1 {2} and p.pro_id not in
	                                                            (
	                                                             select top {3} p.pro_id
	                                                             from (select pa.ProjectSysNo
	                                                            ,MAX(pa.[Status]) as Status
	                                                            ,MAX(pa.[Suggestion]) as Suggestion
	                                                            ,MAX(pa.[AuditUser]) as AuditUser
	                                                            ,MAX(pa.[AudtiDate]) as AuditDate
	                                                            from cm_ProjectAudit pa WITH(NOLOCK) group by pa.ProjectSysNo) pa right join
	                                                            cm_Project p WITH(NOLOCK) on p.pro_ID = pa.ProjectSysNo where 1=1  {2}
	                                                            order by p.pro_id desc
                                                             )order by p.pro_id desc", pageSize, pageCurrent, whereCondition, pageSize * pageCurrent);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectOfReportOfYearViewEntity> resultList = EntityBuilder<ProjectOfReportOfYearViewEntity>.BuilderEntityList(reader);

            string countSql = string.Format(@"select COUNT(*) as ProjectCount
                                                                              from (select pa.ProjectSysNo,MAX(pa.Status) as Status
                                                                              from cm_ProjectAudit pa WITH(NOLOCK) group by pa.ProjectSysNo) pa right join
                                                                              cm_Project p WITH(NOLOCK) on p.pro_ID = pa.ProjectSysNo where 1=1 {0}", whereCondition);

            object objResult = DbHelperSQL.GetSingle(countSql);

            dataCount = objResult == null ? 0 : Convert.ToInt32(objResult);

            return resultList;
        }

        public ProjectOfReportOfYearViewEntity GetProjectInfoByWhereCondition(string whereCondition)
        {
            string sql = string.Format(@"select    
                                                                       p.pro_ID
                                                                      ,p.pro_order
                                                                      ,p.pro_name
                                                                      ,p.Pro_number
                                                                      ,p.pro_StruType
                                                                      ,p.pro_kinds
                                                                      ,p.pro_buildUnit
                                                                      ,p.pro_status
                                                                      ,p.pro_level
                                                                      ,p.pro_startTime
                                                                      ,p.pro_finishTime
                                                                      ,p.Pro_src
                                                                      ,p.ChgJia
                                                                      ,p.Phone
                                                                      ,p.CoperationSysNo
                                                                      ,p.Project_reletive
                                                                      ,p.Cpr_Acount
                                                                      ,p.Unit
                                                                      ,p.ProjectScale
                                                                      ,p.BuildAddress
                                                                      ,p.pro_Intro 
                                                                      ,pa.[Status]
                                                                      ,pa.[Suggestion]
                                                                      ,pa.[AuditUser]
                                                                      ,pa.AuditDate
                                                                      ,d.dic_Name as ProjectFrom
                                                                      from (select pa.ProjectSysNo
                                                                      ,MAX(pa.[Status]) as Status
                                                                      ,MAX(pa.[Suggestion]) as Suggestion
                                                                      ,MAX(pa.[AuditUser]) as AuditUser
                                                                      ,MAX(pa.[AudtiDate]) as AuditDate
                                                                      from cm_ProjectAudit pa WITH(NOLOCK) group by pa.ProjectSysNo) pa right join
                                                                      cm_Project p WITH(NOLOCK) on p.pro_ID = pa.ProjectSysNo join cm_Dictionary d on d.ID = p.Pro_src where 1=1 {0}", whereCondition);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            ProjectOfReportOfYearViewEntity projectReportOfYearViewEntity = EntityBuilder<ProjectOfReportOfYearViewEntity>.BuilderEntity(reader);

            reader.Close();

            return projectReportOfYearViewEntity;
        }
        //项目类型统计
        public int GetProjectCountOfProjectTypeByYear(int year, string projectType, string unit)
        {
            //部门
            if (unit != "")
            {
                unit = " AND Unit='" + unit + "'";
            }
            string sql = string.Format(" select count(*) from cm_Project where pro_startTime between N'{0}-01-01' and N'{0}-12-31' and (Select cpr_Type From cm_Coperation Where cpr_ID=CoperationSysNo) like N'%{1}%' {2}", year, projectType, unit);

            object resultObj = DbHelperSQL.GetSingle(sql);

            return resultObj == null ? 0 : Convert.ToInt32(resultObj);
        }

        public List<ProjectCountOfProjectTypeViewEntity> GetProjectCountOfProjectTypeByYear(int year)
        {
            string sql = string.Format(@"select  (Select cpr_Type From cm_Coperation Where cpr_ID=CoperationSysNo) as ProjectType,
                                                                        Count(*) as ProjectCount 
                                                                        from [cm_Project] where pro_startTime between N'{0}-01-01' and N'{0}-12-31' group by 
                                                                       CoperationSysNo", year);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectCountOfProjectTypeViewEntity> resultList = EntityBuilder<ProjectCountOfProjectTypeViewEntity>.BuilderEntityList(reader);

            return resultList;
        }

        public ProjectScaleViewEntity GetProjectScaleByYear(int year, string unit)
        {
            //部门
            if (unit != "")
            {
                unit = " AND Unit='" + unit + "'";
            }
            string sql = string.Format("select sum(ProjectScale) as ProjectScale from cm_Project where pro_startTime between N'{0}-01-01' and N'{0}-12-31' {1}", year, unit);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            ProjectScaleViewEntity projectScaleViewEntity = EntityBuilder<ProjectScaleViewEntity>.BuilderEntity(reader);

            reader.Close();

            return projectScaleViewEntity;
        }

        public ProjectStatusViewEntity GetProjectStatusCountByByYear(int year, string unit)
        {
            if (unit != "")
            {
                unit = " AND pro_DesignUnit='" + unit + "'";
            }
            string sql = string.Format(@"select 
                                                                  COUNT(*) as RegisterProjectCount
                                                                 ,(select count(*) from tg_project where pro_status like N'%进行中%' and pro_StartTime between N'{0}-01-01' and N'{0}-12-31' {1}) as ProcessingProjectCount
                                                                 ,(select count(*) from tg_project where pro_status like N'%暂停%' and pro_StartTime between N'{0}-01-01' and N'{0}-12-31' {1}) as PauseProjectCount
                                                                 ,(select count(*) from tg_project where pro_status like N'%完成%' and pro_StartTime between N'{0}-01-01' and N'{0}-12-31' {1}) as FinishProjectCount
                                                                 from [cm_Project] where pro_StartTime between N'{0}-01-01' and N'{0}-12-31'", year, unit);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            ProjectStatusViewEntity projectStatusViewEntty = EntityBuilder<ProjectStatusViewEntity>.BuilderEntity(reader);

            reader.Close();

            return projectStatusViewEntty;
        }

        public List<string> GetProjectAuditConfigProcessDescription()
        {
            string sql = "SELECT r.RoleName FROM [dbo].[cm_ProjectAuditConfig] pc JOIN cm_Role r on r.SysNo = pc.[RoleSysNo]  ORDER BY pc.[Position] ASC";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<string> resultList = new List<string>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    resultList.Add(reader["RoleName"].ToString());
                }
                reader.Close();
            }

            return resultList;
        }

        public List<string> GetProjectAuditUserName(int[] userSysNoArray)
        {
            List<string> resultList = new List<string>();
            foreach (int userSysNo in userSysNoArray)
            {
                string sql = "SELECT [mem_Name] FROM [dbo].[tg_member] WITH(NOLOCK) WHERE mem_ID =" + userSysNo;

                resultList.Add(DbHelperSQL.GetSingle(sql).ToString());
            }
            return resultList;
        }
    }
}
