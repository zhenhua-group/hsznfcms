﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.DBUtility;
using System.Data.SqlClient;

namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:二次产值分配
    /// </summary>
    public partial class cm_ProjectSecondValueAllot
    {
        public cm_ProjectSecondValueAllot()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectSecondValueAllot model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_ProjectSecondValueAllot(");
            strSql.Append("ProID,AllotID,TheDeptValueCount,AuditCount,DesignCount,HavcCount,Status,SecondValue,ActualAllountTime)");
            strSql.Append(" values (");
            strSql.Append("@ProID,@AllotID,@TheDeptValueCount,@AuditCount,@DesignCount,@HavcCount,@Status,@SecondValue,@ActualAllountTime)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ProID", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@TheDeptValueCount", SqlDbType.Decimal,9),
					new SqlParameter("@AuditCount", SqlDbType.Decimal,9),
					new SqlParameter("@DesignCount", SqlDbType.Decimal,9),
					new SqlParameter("@HavcCount", SqlDbType.Decimal,9),
					new SqlParameter("@Status", SqlDbType.Char,1),
                    new SqlParameter("@SecondValue", SqlDbType.Char,1)  ,
                    new SqlParameter("@ActualAllountTime", SqlDbType.NChar,10)                  };
            parameters[0].Value = model.ProID;
            parameters[1].Value = model.AllotID;
            parameters[2].Value = model.TheDeptValueCount;
            parameters[3].Value = model.AuditCount;
            parameters[4].Value = model.DesignCount;
            parameters[5].Value = model.HavcCount;
            parameters[6].Value = model.Status;
            parameters[7].Value = model.SecondValue;
            parameters[8].Value = model.ActualAllountTime;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int UpdateAuditCount(int ID, decimal? auditCount, decimal? designCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ProjectSecondValueAllot set ");
            if (auditCount != null)
            {
                strSql.Append("AuditCount=" + auditCount + "");
            }
            if (designCount != null)
            {
                strSql.Append("DesignCount=" + designCount + "");
            }
            strSql.Append(" where ID=" + ID + "");

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            return rows;
        }

        /// <summary>
        /// 更新二次分配状态
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public int UpdateAuditStatus(int ID, string status)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ProjectSecondValueAllot set ");
            if (status != null)
            {
                strSql.Append("Status='" + status + "'");
            }

            strSql.Append(" where ID=" + ID + "");

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            return rows;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectSecondValueAllot GetModel(int proID, int? AllotID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,ProID,AllotID,TheDeptValueCount,AuditCount,DesignCount,HavcCount,Status from cm_ProjectSecondValueAllot ");
            strSql.Append(" where ProID=@proID AND AllotID=@AllotID ");
            SqlParameter[] parameters = {
					new SqlParameter("@proID", SqlDbType.Int,4),
                    new SqlParameter("@AllotID", SqlDbType.Int,4)
			};
            parameters[0].Value = proID;
            parameters[1].Value = AllotID;

            TG.Model.cm_ProjectSecondValueAllot model = new TG.Model.cm_ProjectSecondValueAllot();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProID"] != null && ds.Tables[0].Rows[0]["ProID"].ToString() != "")
                {
                    model.ProID = int.Parse(ds.Tables[0].Rows[0]["ProID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotID"] != null && ds.Tables[0].Rows[0]["AllotID"].ToString() != "")
                {
                    model.AllotID = int.Parse(ds.Tables[0].Rows[0]["AllotID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TheDeptValueCount"] != null && ds.Tables[0].Rows[0]["TheDeptValueCount"].ToString() != "")
                {
                    model.TheDeptValueCount = decimal.Parse(ds.Tables[0].Rows[0]["TheDeptValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AuditCount"] != null && ds.Tables[0].Rows[0]["AuditCount"].ToString() != "")
                {
                    model.AuditCount = decimal.Parse(ds.Tables[0].Rows[0]["AuditCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DesignCount"] != null && ds.Tables[0].Rows[0]["DesignCount"].ToString() != "")
                {
                    model.DesignCount = decimal.Parse(ds.Tables[0].Rows[0]["DesignCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HavcCount"] != null && ds.Tables[0].Rows[0]["HavcCount"].ToString() != "")
                {
                    model.HavcCount = decimal.Parse(ds.Tables[0].Rows[0]["HavcCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectSecondValueAllot GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,ProID,AllotID,TheDeptValueCount,AuditCount,DesignCount,HavcCount,Status from cm_ProjectSecondValueAllot ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_ProjectSecondValueAllot model = new TG.Model.cm_ProjectSecondValueAllot();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProID"] != null && ds.Tables[0].Rows[0]["ProID"].ToString() != "")
                {
                    model.ProID = int.Parse(ds.Tables[0].Rows[0]["ProID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotID"] != null && ds.Tables[0].Rows[0]["AllotID"].ToString() != "")
                {
                    model.AllotID = int.Parse(ds.Tables[0].Rows[0]["AllotID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TheDeptValueCount"] != null && ds.Tables[0].Rows[0]["TheDeptValueCount"].ToString() != "")
                {
                    model.TheDeptValueCount = decimal.Parse(ds.Tables[0].Rows[0]["TheDeptValueCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AuditCount"] != null && ds.Tables[0].Rows[0]["AuditCount"].ToString() != "")
                {
                    model.AuditCount = decimal.Parse(ds.Tables[0].Rows[0]["AuditCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["DesignCount"] != null && ds.Tables[0].Rows[0]["DesignCount"].ToString() != "")
                {
                    model.DesignCount = decimal.Parse(ds.Tables[0].Rows[0]["DesignCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["HavcCount"] != null && ds.Tables[0].Rows[0]["HavcCount"].ToString() != "")
                {
                    model.HavcCount = decimal.Parse(ds.Tables[0].Rows[0]["HavcCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,ProID,AllotID,TheDeptValueCount,AuditCount,DesignCount,HavcCount,Status ");
            strSql.Append(" FROM cm_ProjectSecondValueAllot ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }


        #endregion  Method
    }
}
