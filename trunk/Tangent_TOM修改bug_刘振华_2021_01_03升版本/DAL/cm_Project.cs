﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using System.Collections.Generic;//Please add references
using System.Collections;
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_Project
    /// </summary>
    public partial class cm_Project
    {
        public cm_Project()
        { }
        #region  Method

        public DataSet GetAllYearList()
        {
            string strSql = " SELECT DISTINCT DATEPART(year, pro_startTime) AS AllYear FROM cm_Project order by year(pro_startTime) desc";
            DataSet ds_year = TG.DBUtility.DbHelperSQL.Query(strSql);
            return ds_year;
        }
        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("pro_ID", "cm_Project");
        }
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int pro_ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_Project");
            strSql.Append(" where pro_ID=@pro_ID");
            SqlParameter[] parameters = {
					new SqlParameter("@pro_ID", SqlDbType.Int,4)
};
            parameters[0].Value = pro_ID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_Project model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_Project(");
            strSql.Append("pro_order,pro_name,pro_kinds,pro_buildUnit,pro_status,pro_level,pro_startTime,pro_finishTime,Pro_src,ChgJia,CoperationSysNo,Phone,Project_reletive,Cpr_Acount,Unit,ProjectScale,BuildAddress,pro_Intro,pro_StruType,UpdateBy,PMName,PMPhone,Industry,BuildType,ProjSub,InsertUserID,UpdateDate,PMUserID,ISTrunEconomy,ISHvac,AuditLevel,ISArch)");
            strSql.Append(" values (");
            strSql.Append("@pro_order,@pro_name,@pro_kinds,@pro_buildUnit,@pro_status,@pro_level,@pro_startTime,@pro_finishTime,@Pro_src,@ChgJia,@CoperationSysNo,@Phone,@Project_reletive,@Cpr_Acount,@Unit,@ProjectScale,@BuildAddress,@pro_Intro,@pro_StruType,@UpdateBy,@PMName,@PMPhone,@Industry,@BuildType,@ProjSub,@InsertUserID,@UpdateDate,@PMUserID,@ISTrunEconomy,@ISHvac,@AuditLevel,@ISArch)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@pro_order", SqlDbType.VarChar,100),
					new SqlParameter("@pro_name", SqlDbType.VarChar,500),
					new SqlParameter("@pro_kinds", SqlDbType.VarChar,500),
					new SqlParameter("@pro_buildUnit", SqlDbType.VarChar,500),
					new SqlParameter("@pro_status", SqlDbType.Char,60),
					new SqlParameter("@pro_level", SqlDbType.Int,4),
					new SqlParameter("@pro_startTime", SqlDbType.DateTime),
					new SqlParameter("@pro_finishTime", SqlDbType.DateTime),
					new SqlParameter("@Pro_src", SqlDbType.Int,4),
					new SqlParameter("@ChgJia", SqlDbType.VarChar,50),
                    new SqlParameter ("@CoperationSysNo",SqlDbType .Int,4),
					new SqlParameter("@Phone", SqlDbType.VarChar,30),
					new SqlParameter("@Project_reletive", SqlDbType.VarChar,500),
					new SqlParameter("@Cpr_Acount", SqlDbType.Decimal,9),
					new SqlParameter("@Unit", SqlDbType.VarChar,100),
					new SqlParameter("@ProjectScale", SqlDbType.Decimal,9),
					new SqlParameter("@BuildAddress", SqlDbType.VarChar,500),
					new SqlParameter("@pro_Intro", SqlDbType.VarChar,500),
                    new SqlParameter ("@pro_StruType",SqlDbType .VarChar ,500),
                    new SqlParameter("@UpdateBy",SqlDbType.Int,4),
                    new SqlParameter("@PMName",SqlDbType.VarChar,50),
                    new SqlParameter("@PMPhone",SqlDbType.VarChar,50),
                    new SqlParameter("@Industry",SqlDbType.VarChar,50),
                    new SqlParameter("@BuildType",SqlDbType.VarChar,50),
                    new SqlParameter("@ProjSub",SqlDbType.VarChar,1000),
                    new SqlParameter("@InsertUserID",SqlDbType.Int,4),
                    new SqlParameter("@InsertDate",SqlDbType.DateTime),
                    new SqlParameter("@UpdateDate",SqlDbType.DateTime),
                    new SqlParameter("@PMUserID",SqlDbType.Int,4),
                    new SqlParameter("@ISTrunEconomy",SqlDbType.VarChar,1),
                    new SqlParameter("@ISHvac",SqlDbType.VarChar,1),
                    new SqlParameter("@AuditLevel",SqlDbType.VarChar,10),
                    new SqlParameter("@ISArch",SqlDbType.Char,1)};
            parameters[0].Value = model.pro_order;
            parameters[1].Value = model.pro_name;
            parameters[2].Value = model.pro_kinds;
            parameters[3].Value = model.pro_buildUnit;
            parameters[4].Value = model.pro_status;
            parameters[5].Value = model.pro_level;
            parameters[6].Value = model.pro_startTime;
            parameters[7].Value = model.pro_finishTime;
            parameters[8].Value = model.Pro_src;
            parameters[9].Value = model.ChgJia;
            parameters[10].Value = model.cprID;
            parameters[11].Value = model.Phone;
            parameters[12].Value = model.Project_reletive;
            parameters[13].Value = model.Cpr_Acount;
            parameters[14].Value = model.Unit;
            parameters[15].Value = model.ProjectScale;
            parameters[16].Value = model.BuildAddress;
            parameters[17].Value = model.pro_Intro;
            parameters[18].Value = model.pro_StruType;
            parameters[19].Value = model.UpdateBy;
            parameters[20].Value = model.PMName;
            parameters[21].Value = model.PMPhone;
            parameters[22].Value = model.Industry;
            parameters[23].Value = model.BuildType;
            parameters[24].Value = model.ProjSub;
            parameters[25].Value = model.InsertUserID;
            parameters[26].Value = model.InsertDate;
            parameters[27].Value = model.UpdateDate;
            parameters[28].Value = model.PMUserID;
            parameters[29].Value = model.ISTrunEconomy;
            parameters[30].Value = model.ISHvac;
            parameters[31].Value = model.AuditLevel;
            parameters[32].Value = model.ISArch;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_Project model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_Project set ");
            strSql.Append("pro_order=@pro_order,");
            strSql.Append("pro_name=@pro_name,");
            strSql.Append("pro_kinds=@pro_kinds,");
            strSql.Append("pro_buildUnit=@pro_buildUnit,");
            strSql.Append("pro_status=@pro_status,");
            strSql.Append("pro_level=@pro_level,");
            strSql.Append("pro_startTime=@pro_startTime,");
            strSql.Append("pro_finishTime=@pro_finishTime,");
            strSql.Append("Pro_src=@Pro_src,");
            strSql.Append("ChgJia=@ChgJia,");
            strSql.Append("Phone=@Phone,");
            strSql.Append("Project_reletive=@Project_reletive,");
            strSql.Append("Cpr_Acount=@Cpr_Acount,");
            strSql.Append("Unit=@Unit,");
            strSql.Append("ProjectScale=@ProjectScale,");
            strSql.Append("BuildAddress=@BuildAddress,");
            strSql.Append("pro_Intro=@pro_Intro,");
            strSql.Append("CoperationSysNo=@CoperationSysNo,");
            strSql.Append("pro_StruType=@pro_StruType,");
            strSql.Append("Pro_number=@Pro_number,");
            strSql.Append("PMName=@PMName,");
            strSql.Append("PMPhone=@PMPhone,");
            strSql.Append("Industry=@Industry,");
            strSql.Append("BuildType=@BuildType,");
            strSql.Append("ProjSub=@ProjSub,");
            strSql.Append("InsertUserID=@InsertUserID,");
            strSql.Append("InsertDate=@InsertDate,");
            strSql.Append("UpdateDate=@UpdateDate,");
            strSql.Append("PMUserID=@PMUserID,");
            strSql.Append("ISTrunEconomy=@ISTrunEconomy,");
            strSql.Append("ISHvac=@ISHvac,");
            strSql.Append("AuditLevel=@AuditLevel,");
            strSql.Append("ISArch=@ISArch");
            strSql.Append(" where pro_ID=@pro_ID");
            SqlParameter[] parameters = {
					new SqlParameter("@pro_order", SqlDbType.VarChar,100),
					new SqlParameter("@pro_name", SqlDbType.VarChar,500),
					new SqlParameter("@pro_kinds", SqlDbType.Char,500),
					new SqlParameter("@pro_buildUnit", SqlDbType.VarChar,500),
					new SqlParameter("@pro_status", SqlDbType.Char,60),
					new SqlParameter("@pro_level", SqlDbType.Int,4),
					new SqlParameter("@pro_startTime", SqlDbType.DateTime),
					new SqlParameter("@pro_finishTime", SqlDbType.DateTime),
					new SqlParameter("@Pro_src", SqlDbType.Int,4),
					new SqlParameter("@ChgJia", SqlDbType.VarChar,50),
					new SqlParameter("@Phone", SqlDbType.VarChar,30),
					new SqlParameter("@Project_reletive", SqlDbType.VarChar,500),
					new SqlParameter("@Cpr_Acount", SqlDbType.Decimal,9),
					new SqlParameter("@Unit", SqlDbType.VarChar,100),
					new SqlParameter("@ProjectScale", SqlDbType.Decimal,9),
					new SqlParameter("@BuildAddress", SqlDbType.VarChar,500),
					new SqlParameter("@pro_Intro", SqlDbType.VarChar,500),
                    new SqlParameter ("@CoperationSysNo",SqlDbType .Int ,4),
					new SqlParameter("@pro_ID", SqlDbType.Int,4),
                    new SqlParameter ("@pro_StruType",SqlDbType .VarChar ,500),
                    new SqlParameter ("@Pro_number",SqlDbType .VarChar ,50),
                    new SqlParameter ("@PMName",SqlDbType .VarChar ,50),
                    new SqlParameter ("@PMPhone",SqlDbType .VarChar ,50),
                    new SqlParameter ("@Industry",SqlDbType .VarChar ,50),
                    new SqlParameter ("@BuildType",SqlDbType .VarChar ,50),
                    new SqlParameter ("@ProjSub",SqlDbType .VarChar ,1000),
                    new SqlParameter("@InsertUserID",SqlDbType.Int,4),
                    new SqlParameter("@InsertDate",SqlDbType.DateTime),
                    new SqlParameter("@UpdateDate",SqlDbType.DateTime),
                    new SqlParameter("@PMUserID",SqlDbType.Int,4),
                    new SqlParameter("@ISTrunEconomy",SqlDbType.VarChar,1),
                    new SqlParameter("@ISHvac",SqlDbType.VarChar,1),
                    new SqlParameter("@AuditLevel",SqlDbType.VarChar,10),
                    new SqlParameter("@ISArch",SqlDbType.Char,1)};
            parameters[0].Value = model.pro_order;
            parameters[1].Value = model.pro_name;
            parameters[2].Value = model.pro_kinds;
            parameters[3].Value = model.pro_buildUnit;
            parameters[4].Value = model.pro_status;
            parameters[5].Value = model.pro_level;
            parameters[6].Value = model.pro_startTime;
            parameters[7].Value = model.pro_finishTime;
            parameters[8].Value = model.Pro_src;
            parameters[9].Value = model.ChgJia;
            parameters[10].Value = model.Phone;
            parameters[11].Value = model.Project_reletive;
            parameters[12].Value = model.Cpr_Acount;
            parameters[13].Value = model.Unit;
            parameters[14].Value = model.ProjectScale;
            parameters[15].Value = model.BuildAddress;
            parameters[16].Value = model.pro_Intro;
            parameters[17].Value = model.cprID;
            parameters[18].Value = model.bs_project_Id;
            parameters[19].Value = model.pro_StruType;
            parameters[20].Value = model.Pro_number;
            parameters[21].Value = model.PMName;
            parameters[22].Value = model.PMPhone;
            parameters[23].Value = model.Industry;
            parameters[24].Value = model.BuildType;
            parameters[25].Value = model.ProjSub;
            parameters[26].Value = model.InsertUserID;
            parameters[27].Value = model.InsertDate;
            parameters[28].Value = model.UpdateDate;
            parameters[29].Value = model.PMUserID;
            parameters[30].Value = model.ISTrunEconomy;
            parameters[31].Value = model.ISHvac;
            parameters[32].Value = model.AuditLevel;
            parameters[33].Value = model.ISArch;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int pro_ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_Project ");
            strSql.Append(" where pro_ID=@pro_ID");
            SqlParameter[] parameters = {
					new SqlParameter("@pro_ID", SqlDbType.Int,4)
            };
            parameters[0].Value = pro_ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public ArrayList DeleteList(string pro_IDlist)
        {
            int count = 0;
            string[] idArray = pro_IDlist.Split(',');

            ArrayList listAudit = new ArrayList();

            foreach (string id in idArray)
            {
                if (!string.IsNullOrEmpty(id.Trim()) && id.Trim() != ",")
                {
                    //判断 是否在审核状态 中
                    if (!IsAudit(id))
                    {
                        count = DbHelperSQL.ExecuteSql("delete cm_Project where pro_ID=" + id);
                    }
                    else
                    {
                        listAudit.Add(id);
                    }
                }
            }

            return listAudit;
        }
        /// <summary>
        /// 是否在审核中
        /// </summary>
        /// <param name="cprID"></param>
        /// <returns></returns>
        public bool IsAudit(string cprID)
        {
            string sql = @"select  top 1 COUNT(SysNo) from dbo.cm_ProjectAudit 
                       where (Status='A' OR  Status='B' OR Status='D' )
                        and ProjectSysNo=" + cprID + "";

            DataSet ds = DbHelperSQL.Query(sql);

            if (int.Parse(ds.Tables[0].Rows[0][0].ToString()) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_Project GetModel(int pro_ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 pro_ID,ReferenceSysNo,pro_order,pro_number,pro_name,pro_StruType,pro_kinds,pro_buildUnit,pro_status,pro_level,pro_startTime,pro_finishTime,Pro_src,ChgJia,Phone,Project_reletive,Cpr_Acount,Unit,ProjectScale,BuildAddress,pro_Intro,pro_StruType,Pro_number,CoperationSysNo,UpdateBy,PMName,PMPhone,Industry,BuildType,ProjSub,InsertUserID,InsertDate,UpdateDate,PMUserID,ISTrunEconomy,ISHvac,AuditLevel,ISArch from cm_Project ");
            strSql.Append(" where pro_ID=@pro_ID");
            SqlParameter[] parameters = {
					new SqlParameter("@pro_ID", SqlDbType.Int,4)
            };
            parameters[0].Value = pro_ID;

            TG.Model.cm_Project model = new TG.Model.cm_Project();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["pro_ID"] != null && ds.Tables[0].Rows[0]["pro_ID"].ToString() != "")
                {
                    model.bs_project_Id = int.Parse(ds.Tables[0].Rows[0]["pro_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ReferenceSysNo"] != null && ds.Tables[0].Rows[0]["ReferenceSysNo"].ToString() != "")
                {
                    model.ReferenceSysNo = int.Parse(ds.Tables[0].Rows[0]["ReferenceSysNo"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_order"] != null && ds.Tables[0].Rows[0]["pro_order"].ToString() != "")
                {
                    model.pro_order = ds.Tables[0].Rows[0]["pro_order"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_number"] != null && ds.Tables[0].Rows[0]["pro_number"].ToString() != "")
                {
                    model.Pro_number = ds.Tables[0].Rows[0]["pro_number"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_name"] != null && ds.Tables[0].Rows[0]["pro_name"].ToString() != "")
                {
                    model.pro_name = ds.Tables[0].Rows[0]["pro_name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_StruType"] != null && ds.Tables[0].Rows[0]["pro_StruType"].ToString() != "")
                {
                    model.pro_StruType = ds.Tables[0].Rows[0]["pro_StruType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_kinds"] != null && ds.Tables[0].Rows[0]["pro_kinds"].ToString() != "")
                {
                    model.pro_kinds = ds.Tables[0].Rows[0]["pro_kinds"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_buildUnit"] != null && ds.Tables[0].Rows[0]["pro_buildUnit"].ToString() != "")
                {
                    model.pro_buildUnit = ds.Tables[0].Rows[0]["pro_buildUnit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_status"] != null && ds.Tables[0].Rows[0]["pro_status"].ToString() != "")
                {
                    model.pro_status = ds.Tables[0].Rows[0]["pro_status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_level"] != null && ds.Tables[0].Rows[0]["pro_level"].ToString() != "")
                {
                    model.pro_level = int.Parse(ds.Tables[0].Rows[0]["pro_level"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_startTime"] != null && ds.Tables[0].Rows[0]["pro_startTime"].ToString() != "")
                {
                    model.pro_startTime = DateTime.Parse(ds.Tables[0].Rows[0]["pro_startTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_finishTime"] != null && ds.Tables[0].Rows[0]["pro_finishTime"].ToString() != "")
                {
                    model.pro_finishTime = DateTime.Parse(ds.Tables[0].Rows[0]["pro_finishTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Pro_src"] != null && ds.Tables[0].Rows[0]["Pro_src"].ToString() != "")
                {
                    model.Pro_src = int.Parse(ds.Tables[0].Rows[0]["Pro_src"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ChgJia"] != null && ds.Tables[0].Rows[0]["ChgJia"].ToString() != "")
                {
                    model.ChgJia = ds.Tables[0].Rows[0]["ChgJia"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Phone"] != null && ds.Tables[0].Rows[0]["Phone"].ToString() != "")
                {
                    model.Phone = ds.Tables[0].Rows[0]["Phone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Project_reletive"] != null && ds.Tables[0].Rows[0]["Project_reletive"].ToString() != "")
                {
                    model.Project_reletive = ds.Tables[0].Rows[0]["Project_reletive"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Cpr_Acount"] != null && ds.Tables[0].Rows[0]["Cpr_Acount"].ToString() != "")
                {
                    model.Cpr_Acount = decimal.Parse(ds.Tables[0].Rows[0]["Cpr_Acount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Unit"] != null && ds.Tables[0].Rows[0]["Unit"].ToString() != "")
                {
                    model.Unit = ds.Tables[0].Rows[0]["Unit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProjectScale"] != null && ds.Tables[0].Rows[0]["ProjectScale"].ToString() != "")
                {
                    model.ProjectScale = decimal.Parse(ds.Tables[0].Rows[0]["ProjectScale"].ToString());
                }
                if (ds.Tables[0].Rows[0]["BuildAddress"] != null && ds.Tables[0].Rows[0]["BuildAddress"].ToString() != "")
                {
                    model.BuildAddress = ds.Tables[0].Rows[0]["BuildAddress"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_Intro"] != null && ds.Tables[0].Rows[0]["pro_Intro"].ToString() != "")
                {
                    model.pro_Intro = ds.Tables[0].Rows[0]["pro_Intro"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_StruType"] != null && ds.Tables[0].Rows[0]["pro_StruType"].ToString() != "")
                {
                    model.pro_StruType = ds.Tables[0].Rows[0]["pro_StruType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Pro_number"] != null && ds.Tables[0].Rows[0]["Pro_number"].ToString() != "")
                {
                    model.JobNum = ds.Tables[0].Rows[0]["Pro_number"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CoperationSysNo"] != null && ds.Tables[0].Rows[0]["CoperationSysNo"].ToString() != "")
                {
                    model.cprID = ds.Tables[0].Rows[0]["CoperationSysNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UpdateBy"] != null && ds.Tables[0].Rows[0]["UpdateBy"].ToString() != "")
                {
                    model.UpdateBy = int.Parse(ds.Tables[0].Rows[0]["UpdateBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["CoperationSysNo"] != null && ds.Tables[0].Rows[0]["CoperationSysNo"].ToString() != "")
                {
                    model.CoperationSysNo = int.Parse(ds.Tables[0].Rows[0]["CoperationSysNo"].ToString());
                }

                if (ds.Tables[0].Rows[0]["PMName"] != null && ds.Tables[0].Rows[0]["PMName"].ToString() != "")
                {
                    model.PMName = ds.Tables[0].Rows[0]["PMName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PMPhone"] != null && ds.Tables[0].Rows[0]["PMPhone"].ToString() != "")
                {
                    model.PMPhone = ds.Tables[0].Rows[0]["PMPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Industry"] != null && ds.Tables[0].Rows[0]["Industry"].ToString() != "")
                {
                    model.Industry = ds.Tables[0].Rows[0]["Industry"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BuildType"] != null && ds.Tables[0].Rows[0]["BuildType"].ToString() != "")
                {
                    model.BuildType = ds.Tables[0].Rows[0]["BuildType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProjSub"] != null && ds.Tables[0].Rows[0]["ProjSub"].ToString() != "")
                {
                    model.ProjSub = ds.Tables[0].Rows[0]["ProjSub"].ToString();
                }
                //by long 20130517
                if (ds.Tables[0].Rows[0]["InsertUserID"] != null && ds.Tables[0].Rows[0]["InsertUserID"].ToString() != "")
                {
                    model.InsertUserID = int.Parse(ds.Tables[0].Rows[0]["InsertUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InsertDate"] != null && ds.Tables[0].Rows[0]["InsertDate"].ToString() != "")
                {
                    model.InsertDate = DateTime.Parse(ds.Tables[0].Rows[0]["InsertDate"].ToString());
                }
                //by long 20130520
                if (ds.Tables[0].Rows[0]["UpdateDate"] != null && ds.Tables[0].Rows[0]["UpdateDate"].ToString() != "")
                {
                    model.UpdateDate = DateTime.Parse(ds.Tables[0].Rows[0]["UpdateDate"].ToString());
                }
                //by long 20130608 
                if (ds.Tables[0].Rows[0]["PMUserID"] != null && ds.Tables[0].Rows[0]["PMUserID"].ToString() != "")
                {
                    model.PMUserID = int.Parse(ds.Tables[0].Rows[0]["PMUserID"].ToString());
                }
                //2013-9-3
                if (ds.Tables[0].Rows[0]["ISTrunEconomy"] != null && ds.Tables[0].Rows[0]["ISTrunEconomy"].ToString() != "")
                {
                    model.ISTrunEconomy = ds.Tables[0].Rows[0]["ISTrunEconomy"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ISHvac"] != null && ds.Tables[0].Rows[0]["ISHvac"].ToString() != "")
                {
                    model.ISHvac = ds.Tables[0].Rows[0]["ISHvac"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditLevel"] != null && ds.Tables[0].Rows[0]["AuditLevel"].ToString() != "")
                {
                    model.AuditLevel = ds.Tables[0].Rows[0]["AuditLevel"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ISArch"] != null && ds.Tables[0].Rows[0]["ISArch"].ToString() != "")
                {
                    model.ISArch = ds.Tables[0].Rows[0]["ISArch"].ToString();
                }

                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select pro_ID,pro_order,pro_name,pro_StruType,pro_kinds,pro_buildUnit,pro_status,pro_level,pro_startTime,pro_finishTime,Pro_src,ChgJia,Phone,PMName,PMPhone,Project_reletive,Cpr_Acount,Unit,ProjectScale,BuildAddress,pro_Intro,CoperationSysNo,UpdateBy,UpdateDate,Industry,BuildType,ProjSub,(Select mem_Name From tg_member Where mem_ID=InsertUserID) AS InsertUser,InsertDate,PMUserID,ISHvac,AuditLevel,ISArch ");
            strSql.Append(" FROM cm_Project ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(@"                                                                                           
            t.pro_ID,pro_order,pro_name,pro_kinds,pro_buildUnit,pro_status,pro_level,pro_startTime,pro_finishTime,Pro_src,ChgJia,Phone,PMName,(SELECT ProNumber from cm_ProjectNumber where Pro_id=t.pro_ID) as gch,
            Project_reletive,Cpr_Acount,Unit,ProjectScale,BuildAddress,BuildType,pro_Intro,CoperationSysNo,isnull(pa1.SysNo,0) as AuditSysNo, isnull(pa1.Status,N'A') as AuditStatus,UpdateBy,UpdateDate,Industry,BuildType,ProjSub,(Select mem_Name From tg_member Where mem_ID=InsertUserID) AS InsertUser,InsertDate,PMUserID,ISTrunEconomy,ISHvac,AuditLevel,ISArch from(
	            select p.pro_id ,max(pa.SysNo) as ProjectAuditSysNo from cm_Project p left join cm_ProjectAudit pa on pa.ProjectSysNo = p.pro_ID group by p.pro_id
	        ) t join cm_Project p1 on t.pro_id= p1.pro_id left join cm_ProjectAudit pa1 on pa1.SysNo = t.ProjectAuditSysNo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 获取通过项目策划项目信息
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetListPassPlanProject(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" Select ");
            if (Top > 0)
            {
                strSql.Append(" TOP " + Top.ToString());
            }
            strSql.Append(@" pro_ID,P.ReferenceSysNo,pro_name,
                                        case when Ltrim(Project_reletive)=N'' then '无关联合同' else Ltrim(Project_reletive) end as Coperation ,
                                        pro_startTime,Cpr_Acount,Unit ");
            strSql.Append(" From cm_Project P  ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" Where " + strWhere);
            }
            strSql.Append(" Order By " + filedOrder);

            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "cm_Project";
            parameters[1].Value = "pro_ID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage", parameters, "ds");
        }
        public DataSet GetListByWhere(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select pro.* ");
            strSql.Append(" from cm_Project as pro");
            if (strWhere.Trim() != "")
            {
                strSql.Append("  where " + strWhere.Trim());
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        public DataSet GetListByWhere(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" Select ");
            if (Top > 0)
            {
                strSql.Append(" TOP " + Top.ToString());
            }
            strSql.Append(" pro.* ");
            strSql.Append(" from cm_Project as pro");
            if (strWhere.Trim() != "")
            {
                strSql.Append("  where " + strWhere.Trim());
            }
            strSql.Append(" Order By " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }
        /*
          edit bu Sago 2012.10.17
         */
        public int GetSetUpProjectCount(string whereSql)
        {
            string sql = "select count(*) from cm_Project where 1=1 " + whereSql;

            int count = Convert.ToInt32(DbHelperSQL.GetSingle(sql));

            return count;
        }
        public bool ExistsInTGProject(string projectName, string sysno)
        {
            bool flag = false;
            string sql = "select top 1 1 from tg_project where pro_Name=N'" + projectName.Trim() + "' and pro_ID not in (select ReferenceSysNo from cm_Project where pro_ID=" + sysno + ")";
            object objResultTG = DbHelperSQL.GetSingle(sql);

            flag = objResultTG == null ? false : true;

            if (flag == false)
            {
                flag = DbHelperSQL.GetSingle("select top 1 1 from cm_Project where pro_Name=N'" + projectName.Trim() + "' and pro_ID<>" + sysno + "") == null ? false : true;
            }
            return flag;
        }
        /// <summary>
        /// 获取所有合同年份
        /// </summary>
        public List<string> GetProjectYear()
        {
            List<string> list = new List<string>();
            string strSql = " Select Distinct year(pro_startTime) From cm_Project order by year(pro_startTime) desc";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i][0].ToString());
                }
            }
            return list;
        }
        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_Project", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListEditByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_Project_Edit", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_Project_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 申请修改获取需要分页的数据总数
        /// </summary>
        public object GetListPageEditProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_ProjectEdit_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 分页存储过程 审批通过的项目  20130808 long
        /// </summary>
        public SqlDataReader GetListByPageAuditProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_Project_Audit", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 获取需要分页的数据总数 审批通过的项目  20130808 long
        /// </summary>
        public object GetListPageAuditProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_Project_Audit_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountProjectByJD(string year, int? unitid, string type)
        {
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@unit_id",unitid),
                new SqlParameter("@type",type)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountProjByJD", paras, "Table1");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountProjectByMonth(string year, int? unitid, string type)
        {
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@unit_id",unitid),
                new SqlParameter("@type",type)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountProjByMonth", paras, "Table1");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountProjectByIndustry(string year, int? unitid, string type)
        {
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@unit_id",unitid),
                new SqlParameter("@type",type)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountProjByIndustry", paras, "Table1");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountProjectByStatus(string year, int? unitid, string type)
        {
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@unit_id",unitid),
                new SqlParameter("@type",type)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountProjByStatus", paras, "Table1");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountProjectByLevel(string year, int? unitid)
        {
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@unit_id",unitid)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountProjByLevel", paras, "Table1");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="year"></param>
        /// <param name="unitid"></param>
        /// <returns></returns>
        public DataSet CountProjectBySrc(string year, int? unitid)
        {
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@year",year),
                new SqlParameter("@unit_id",unitid)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountProjBySrc", paras, "Table1");
        }
        /// <summary>
        ///  自定义查询
        /// </summary>
        /// <param name="year"></param>
        /// <param name="type"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public DataSet CountProjectByCustom(string timewhere, string type, string where)
        {
            SqlParameter[] paras = new SqlParameter[] {
                new SqlParameter("@timewhere",timewhere), 
                new SqlParameter("@type",type),
                new SqlParameter("@where",where)
            };

            return DbHelperSQL.RunProcedure("P_cm_CountProjByCustom", paras, "Table1");
        }
        /// <summary>
        /// 得出导出的数据--项目信息
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectExportInfo(string strWhere)
        {
            string sql = @"select P.*,
                            isnull((select top 1 SysNo from cm_ProjectAudit where ProjectSysNo=P.pro_ID order by SysNo desc ),0) as AuditSysNo,
                            isnull((select top 1 Status from cm_ProjectAudit where ProjectSysNo=P.pro_ID order by SysNo desc ),N'A') as AuditStatus,
                            (case when P.Project_reletive=N'' or (P.Project_reletive is null) then '无' else P.Project_reletive end) as pro_reletive,
				            convert(varchar(10),pro_startTime,120) as qdrq,
				            convert(varchar(10),pro_finishTime,120) as wcrq,
				            (case when pro_level='0' then '院级' else '所级' end) as pro_jb,
				            (case when (P.PMName='0') then (select mem_name from tg_member where mem_id=P.PMUserID) else P.PMName end) as PMUserName,
                           (select mem_name from tg_member where mem_id=P.InsertUserID) as InsertUser, 
				CONVERT(nvarchar(100),p.InsertDate,23) as lrsj,
				            (select dic_Name from cm_Dictionary where dic_type='cpr_src' and ID=P.Pro_src) as pro_from
                            from cm_Project P where 1=1 " + strWhere + " order by P.pro_ID desc";
            return DbHelperSQL.Query(sql);
        }
        public DataSet GetScrProjectExportInfo(string strWhere)
        {
            string sql = @"select P.*,
                            isnull((select top 1 SysNo from cm_ProjectAudit where ProjectSysNo=P.pro_ID order by SysNo desc ),0) as AuditSysNo,
                            isnull((select top 1 Status from cm_ProjectAudit where ProjectSysNo=P.pro_ID order by SysNo desc ),N'A') as AuditStatus,
                            (case when P.Project_reletive=N'' or (P.Project_reletive is null) then '无' else P.Project_reletive end) as pro_reletive,
				            convert(varchar(10),pro_startTime,120) as qdrq,
				            convert(varchar(10),pro_finishTime,120) as wcrq,
				            (case when pro_level='0' then '院级' else '所级' end) as pro_jb,
				            (case when (P.PMName='0') then (select mem_name from tg_member where mem_id=P.PMUserID) else P.PMName end) as PMUserName,
				            (select dic_Name from cm_Dictionary where dic_type='cpr_src' and ID=P.Pro_src) as pro_from
                            from cm_Project P where 1=1 " + strWhere + " order by P.pro_ID desc";
            return DbHelperSQL.Query(sql);
        }
        /// <summary>
        /// 项目产值分配查看-导出
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="strWhere2"></param>
        /// <returns></returns>
        public DataSet GetProjectValueShowExportInfo(string strWhere, string strWhere2)
        {
            string sql2 = @"SELECT P.*, convert(varchar(10),pro_startTime,120) as qdrq,
				           convert(varchar(10),pro_finishTime,120) as wcrq from cm_Project P
                            inner JOIN 
                            (
                            SELECT DISTINCT pro_ID
                            FROM cm_ProjectValueAllot 
                            WHERE 1=1  " + strWhere2 + " " + @"
                            GROUP BY pro_ID 
                            ) ALLOT
                            ON P.pro_ID=ALLOT.pro_ID
                            where 1=1  " + strWhere + "";
            return DbHelperSQL.Query(sql2);
        }
        /// <summary>
        /// 得出导出的数据
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectEditExportInfo(string strWhere)
        {
            string sql = @"	select P.*,
                            isnull((select top 1 SysNo from cm_ProjectAuditRepeat where ProjectSysNo=P.pro_ID order by SysNo desc ),0) as AuditSysNo,
	                        isnull((select top 1 Status from cm_ProjectAuditRepeat where ProjectSysNo=P.pro_ID order by SysNo desc ),N'A') as AuditStatus
	                        from cm_Project P
	                        left join 
	                        cm_ProjectAudit u
	                        on p.pro_ID=u.ProjectSysNo  where  u.Status='D' " + strWhere + " order by P.pro_ID desc";
            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 得出导出的数据--项目工号
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectNumberExportInfo(string strWhere)
        {
            string sql = @"select PN.*,P.*,m.mem_Name
                            from cm_ProjectNumber PN inner join cm_Project P on PN.pro_id=P.pro_id
                            inner join tg_member m
                            on m.mem_ID=Pn.SubmintPerson where 1=1 " + strWhere + " order by P.pro_ID desc";
            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 得出导出的数据--未策划
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectNoPlanExportInfo(string strWhere)
        {
            string sql = @" select 
						 P.pro_id as ProjectSysNo
						,max(P.pro_name) as ProjectName
						,isnull(MAX(PA.SysNo),0) as ProjectPlanAuditSysNo
						,max(PA.Status) as ProjectPlanAuditStatus
						,max(PN.ProNumber) as ProjectJobNumber
						,max(PN.SubmitDate) as SubmitDate
						,case max(P.CoperationSysNo) when 0 then N'无' else N'有' end as IsBackup
						,max(C.cpr_Name) as CoperationName
						,max(RT.pro_ID) as IsHave
						,max(P.PMName) as PMName
						,max(P.PMUserID) as PMUserID
						from cm_Project P left join cm_ProjectNumber PN on PN.Pro_id = P.pro_id
						left join cm_Coperation C on C.cpr_Id = P.CoperationSysNo
						left join tg_relation RT on RT.pro_ID = P.ReferenceSysNo
						left join cm_ProjectPlanAudit PA on PA.ProjectSysNo = P.pro_id
						where isnull(PN.ProNumber,N'0')<>N'0' AND 1=1  " + strWhere + " group by P.pro_id having isnull(MAX(PA.SysNo),0)=0 and isnull(max(RT.pro_ID),0)=0 order by p.pro_ID desc";
            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 得出导出的数据--已策划
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectYesPlanExportInfo(string strWhere)
        {
            string sql = @"    select distinct
				                P.pro_ID as ProjectSysNo
				                ,P.pro_name as ProjectName
				                ,case P.CoperationSysNo when 0 then N'无' else N'有' end as IsBackup
				                ,C.cpr_Name as CoperationName
				                ,PN.SubmitDate as SubmitDate
				                ,PN.ProNumber as ProjectJobNumber
				                ,isnull((select top 1 SysNo from cm_ProjectPlanAudit where ProjectSysNo = P.pro_ID order by SysNo DESC),'0') as ProjectPlanAuditSysNo
				                ,(select top 1 Status from cm_ProjectPlanAudit where ProjectSysNo = P.pro_ID order by SysNo DESC) as ProjectPlanAuditStatus
				                ,RT.pro_ID as IsHave
				                ,P.PMName as PMName
				                ,P.PMUserID as PMUserID
				                ,isnull((select mem_Name from tg_member where mem_ID=(select top 1 InUser from cm_ProjectPlanAudit where ProjectSysNo = P.pro_ID order by SysNo DESC)),'') as ApplyAuditUserName
				                from cm_Project P 
				                join cm_ProjectNumber PN on PN.Pro_id = P.pro_ID 
				                left join cm_Coperation C on C.cpr_Id = P.CoperationSysNo 
				                left join tg_relation RT on RT.pro_ID = P.ReferenceSysNo
				                where isnull(PN.ProNumber,N'0') <> N'0' and isnull(RT.pro_ID ,0)<>0 " + strWhere + " order by p.pro_ID desc ";
            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 得出导出的数据--项目进度审批表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetProjectScheduleExportInfo(string strWhere)
        {
            string sql = @"select
					 a.pro_ID,
					 a.pro_Name,
					 a.Unit,
					 a.PMName,
					 case when b.pro_Status='' then '进行中' else  b.pro_Status end as pro_Status,
					 convert(nvarchar(10), a.pro_StartTime,120) pro_StartTime,
				 	convert(nvarchar(10), a.pro_FinishTime,120) pro_FinishTime,
				 	(SELECT  Isnull(purpose_Name,'') FROM tg_purpose a1
					 where purpose_ID in
					 ( 
					   SELECT  max(purpose_ID) FROM tg_package WHERE ver_date=
					  (SELECT max(ver_date) as MaxDate
						FROM tg_package 
						WHERE subpro_ID=c.pro_ID)		
					 ) 
                     AND a1.purpose_Name<>'往来公函' AND a1.purpose_Name<>'工程表单' AND a1.purpose_Name<>'变更' AND a1.purpose_Name<>'会议纪要' )
                     as ProjectCurrentStatus ,--当前项目状态  
				   (select  COUNT( package_ID) from tg_package  where subpro_ID = c.pro_ID
			         and filename  like '%.dwg' and delete_flag=0
				    ) as DrawingCount --图纸数量
			      
				  from cm_project a
				  inner join tg_project b
				  on a.ReferenceSysNo=b.pro_ID
				  left join tg_subproject c
				  on c.pro_parentID=b.pro_ID where 1=1 " + strWhere + "  order by a.pro_ID desc";
            return DbHelperSQL.Query(sql);
        }
        /// <summary>
        /// 更新阶段信息
        /// </summary>
        /// <param name="proid"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool UpdateTgProjectStatus(string proid, string status)
        {
            bool flag = false;
            //查询子项
            string strWhere = "pro_ParentID=" + proid;
            DataTable subProjct = new TG.DAL.tg_subproject().GetList(strWhere).Tables[0];
            //有子项
            if (subProjct.Rows.Count > 0)
            {
                int strSubProjectID = Convert.ToInt32(subProjct.Rows[0]["pro_ID"]);
                int purposeID = 0;
                switch (status)
                {
                    case "方案":
                    case "方案设计":
                        purposeID = 7;
                        break;
                    case "初设":
                    case "初步设计":
                        purposeID = 8;
                        break;
                    case "施工图":
                    case "施工图设计":
                        purposeID = 9;
                        break;
                }
                if (purposeID != 0)
                {
                    strWhere = "  purposeID=" + purposeID + " AND subproID=" + strSubProjectID;
                }
                TG.Model.tg_endSubItem list = new TG.DAL.tg_endSubItem().GetModel(strWhere);
                //不存在，添加
                if (list == null)
                {
                    TG.Model.tg_endSubItem model = new TG.Model.tg_endSubItem
                    {
                        purposeID = purposeID,
                        subproID = strSubProjectID
                    };
                    //添加
                    try
                    {
                        new TG.DAL.tg_endSubItem().Add(model);
                        flag = true;
                    }
                    catch
                    {
                        flag = false;
                    }
                }

                return flag;
            }
            else
            {
                return flag;
            }

        }


        /// <summary>
        ///  数据量小的查询分页
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * ");
            sb.Append(" ,(Select mem_Unit_ID From tg_member Where mem_ID=P.InsertUserID) As MemUnitID ");
            sb.Append(" From cm_Project P ");
            if (strWhere.Trim() != "")
            {
                sb.Append(" where " + strWhere);
            }
            if (orderby.Trim() != "")
            {
                sb.Append(" Order by " + orderby);
            }
            return DbHelperSQL.Query(sb.ToString(), startIndex, endIndex);
        }

        public DataSet GetListByPage(string strWhere, string strWhere2, string orderby, int startIndex, int endIndex)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * ");
            sb.Append(" ,(Select mem_Unit_ID From tg_member Where mem_ID=P.InsertUserID) As MemUnitID ");
            sb.AppendFormat(" ,(Select ID From dbo.cm_KaoHeRenwuProj Pj Where proID=P.pro_ID AND {0} ) As ID ", strWhere2);
            sb.AppendFormat(" ,(Select IsIn From dbo.cm_KaoHeRenwuProj Pj Where proID=P.pro_ID AND {0} ) As IsIn ", strWhere2);
            sb.AppendFormat(" ,(Select Statu From dbo.cm_KaoHeRenwuProj Pj Where proID=P.pro_ID AND {0} ) As Statu ", strWhere2);
            sb.AppendFormat(" ,(Select UnitName From dbo.cm_KaoHeRenwuProj Pj Where proID=P.pro_ID AND {0} ) As UnitName ", strWhere2);
            sb.AppendFormat(" ,(Select UnitID From dbo.cm_KaoHeRenwuProj Pj Where proID=P.pro_ID AND {0} ) As UnitID ", strWhere2);
            sb.AppendFormat(" From cm_Project P ");
            if (strWhere.Trim() != "")
            {
                sb.Append(" where " + strWhere);
            }
            if (orderby.Trim() != "")
            {
                sb.Append(" Order by " + orderby);
            }
            return DbHelperSQL.Query(sb.ToString(), startIndex, endIndex);
        }
        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_Project ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        #endregion  Method
    }
}

