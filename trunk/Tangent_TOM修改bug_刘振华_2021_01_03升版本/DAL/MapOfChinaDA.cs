﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DBUtility;
using TG.Model;
using System.Data.SqlClient;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class MapOfChinaDA
    {
        public List<MapOfChinaEntity> GetProjectCountByProvinceName()
        {
            string sql = @"select 
                                                case when BuildAddress like N'%北京%' then N'北京' 
                                                when BuildAddress like N'%安徽%' then N'安徽'
                                                 when BuildAddress like N'%福建%' then N'福建'
                                                 when BuildAddress like N'%甘肃%' then N'甘肃'
                                                 when BuildAddress like N'%广东%' then N'广东'
                                                 when BuildAddress like N'%广西%' then N'广西'
                                                 when BuildAddress like N'%贵州%' then N'贵州'
                                                 when BuildAddress like N'%海南%' then N'海南'
                                                 when BuildAddress like N'%河北%' then N'河北'
                                                 when BuildAddress like N'%河南%' then N'河南'
                                                 when BuildAddress like N'%湖北%' then N'湖北'
                                                 when BuildAddress like N'%湖南%' then N'湖南'
                                                 when BuildAddress like N'%吉林%' then N'吉林'
                                                 when BuildAddress like N'%江苏%' then N'江苏'
                                                 when BuildAddress like N'%江西%' then N'江西'
                                                 when BuildAddress like N'%辽宁%' then N'辽宁'
                                                 when BuildAddress like N'%宁夏%' then N'宁夏'
                                                 when BuildAddress like N'%青海%' then N'青海'
                                                 when BuildAddress like N'%山东%' then N'山东'
                                                 when BuildAddress like N'%山西%' then N'山西'
                                                 when BuildAddress like N'%陕西%' then N'陕西'
                                                 when BuildAddress like N'%上海%' then N'上海'
                                                 when BuildAddress like N'%四川%' then N'四川'
                                                 when BuildAddress like N'%天津%' then N'天津'
                                                 when BuildAddress like N'%西藏%' then N'西藏'
                                                 when BuildAddress like N'%新疆%' then N'新疆'
                                                 when BuildAddress like N'%云南%' then N'云南'
                                                 when BuildAddress like N'%浙江%' then N'浙江'
                                                 when BuildAddress like N'%重庆%' then N'重庆'
                                                 when BuildAddress like N'%黑龙江%' then N'黑龙江'
                                                 when BuildAddress like N'%内蒙古%' then N'内蒙古'
                                                else BuildAddress end as Province ,
                                                COUNT(*) as ProjectCount
                                                from 
                                                cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo
                                                group by 
                                                case when BuildAddress like N'%北京%' then N'北京' 
                                                when BuildAddress like N'%安徽%' then N'安徽'
                                                 when BuildAddress like N'%福建%' then N'福建'
                                                 when BuildAddress like N'%甘肃%' then N'甘肃'
                                                 when BuildAddress like N'%广东%' then N'广东'
                                                 when BuildAddress like N'%广西%' then N'广西'
                                                 when BuildAddress like N'%贵州%' then N'贵州'
                                                 when BuildAddress like N'%海南%' then N'海南'
                                                 when BuildAddress like N'%河北%' then N'河北'
                                                 when BuildAddress like N'%河南%' then N'河南'
                                                 when BuildAddress like N'%湖北%' then N'湖北'
                                                 when BuildAddress like N'%湖南%' then N'湖南'
                                                 when BuildAddress like N'%吉林%' then N'吉林'
                                                 when BuildAddress like N'%江苏%' then N'江苏'
                                                 when BuildAddress like N'%江西%' then N'江西'
                                                 when BuildAddress like N'%辽宁%' then N'辽宁'
                                                 when BuildAddress like N'%宁夏%' then N'宁夏'
                                                 when BuildAddress like N'%青海%' then N'青海'
                                                 when BuildAddress like N'%山东%' then N'山东'
                                                 when BuildAddress like N'%山西%' then N'山西'
                                                 when BuildAddress like N'%陕西%' then N'陕西'
                                                 when BuildAddress like N'%上海%' then N'上海'
                                                 when BuildAddress like N'%四川%' then N'四川'
                                                 when BuildAddress like N'%天津%' then N'天津'
                                                 when BuildAddress like N'%西藏%' then N'西藏'
                                                 when BuildAddress like N'%新疆%' then N'新疆'
                                                 when BuildAddress like N'%云南%' then N'云南'
                                                 when BuildAddress like N'%浙江%' then N'浙江'
                                                 when BuildAddress like N'%重庆%' then N'重庆'
                                                 when BuildAddress like N'%黑龙江%' then N'黑龙江'
                                                 when BuildAddress like N'%内蒙古%' then N'内蒙古'
                                                else BuildAddress end";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<MapOfChinaEntity> resultList = EntityBuilder<MapOfChinaEntity>.BuilderEntityList(reader);

            return resultList;
        }

        public List<ProjectForMap> GetProjectListByProvinceName(string provinceName, int pageSize, int pageCurrent)
        {
            string sql = string.Format(@"SELECT TOP {0} * 
                                                     from 
	                                                (cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo) 
	                                                left join cm_Coperation c on cp.CoperationSysNo = c.cpr_Id 
                                                    where cp.BuildAddress like N'%{2}%' and cp.pro_id not in
	                                                (
		                                                select TOP {3} cp.pro_id 
		                                                from
		                                                (cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo)
		                                                left join cm_Coperation c on cp.CoperationSysNo = c.cpr_Id
		                                                where cp.BuildAddress like N'%{2}%'
		                                                order by cp.pro_id desc
	                                                ) order by cp.pro_id desc", pageSize, pageCurrent, provinceName, pageSize * pageCurrent);

            //string sql = "SELECT TOP(" + pageSize + ") * FROM(select ROW_NUMBE() OVER(ORDER BY cp.pro_ID ASC) AS RowID, cp.* from   (cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo) left join cm_Coperation c on cp.CoperationSysNo = c.cpr_Id where cp.BuildAddress like N'%" + provinceName + "%' )TT WHERE TT.RowId > " + pageCurrent + " * " + pageSize + "";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectForMap> projectList = EntityBuilder<ProjectForMap>.BuilderEntityList(reader);

            return projectList;
        }

        public ProjectDetailForMap GetProjectInfoByProjectSysNo(int projectSysNo)
        {
            string sql = "select *  from  (cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo join [cm_Dictionary] cd on cd.ID = cp.Pro_src) left join cm_Coperation c on cp.CoperationSysNo = c.cpr_Id where cp.pro_ID =" + projectSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            ProjectDetailForMap project = EntityBuilder<ProjectDetailForMap>.BuilderEntity(reader);

            reader.Close();

            return project;
        }
    }
}
