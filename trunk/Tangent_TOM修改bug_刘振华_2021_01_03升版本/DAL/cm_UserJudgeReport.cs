﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_UserJudgeReport
	/// </summary>
	public partial class cm_UserJudgeReport
	{
		public cm_UserJudgeReport()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_UserJudgeReport model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_UserJudgeReport(");
			strSql.Append("MemID,MemName,Judge1,Judge2,Judge3,Judge4,Judge5,Judge6,Judge7,Judge8,Judge9,Judge10,Judge11,Judge12,SaveStatu,KaoHeUnitID,InsertUserId,InsertDate,JudgeCount)");
			strSql.Append(" values (");
			strSql.Append("@MemID,@MemName,@Judge1,@Judge2,@Judge3,@Judge4,@Judge5,@Judge6,@Judge7,@Judge8,@Judge9,@Judge10,@Judge11,@Judge12,@SaveStatu,@KaoHeUnitID,@InsertUserId,@InsertDate,@JudgeCount)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@Judge1", SqlDbType.Decimal,9),
					new SqlParameter("@Judge2", SqlDbType.Decimal,9),
					new SqlParameter("@Judge3", SqlDbType.Decimal,9),
					new SqlParameter("@Judge4", SqlDbType.Decimal,9),
					new SqlParameter("@Judge5", SqlDbType.Decimal,9),
					new SqlParameter("@Judge6", SqlDbType.Decimal,9),
					new SqlParameter("@Judge7", SqlDbType.Decimal,9),
					new SqlParameter("@Judge8", SqlDbType.Decimal,9),
					new SqlParameter("@Judge9", SqlDbType.Decimal,9),
					new SqlParameter("@Judge10", SqlDbType.Decimal,9),
					new SqlParameter("@Judge11", SqlDbType.Decimal,9),
					new SqlParameter("@Judge12", SqlDbType.Decimal,9),
					new SqlParameter("@SaveStatu", SqlDbType.Int,4),
					new SqlParameter("@KaoHeUnitID", SqlDbType.Int,4),
					new SqlParameter("@InsertUserId", SqlDbType.Int,4),
					new SqlParameter("@InsertDate", SqlDbType.DateTime),
					new SqlParameter("@JudgeCount", SqlDbType.Decimal,9)};
			parameters[0].Value = model.MemID;
			parameters[1].Value = model.MemName;
			parameters[2].Value = model.Judge1;
			parameters[3].Value = model.Judge2;
			parameters[4].Value = model.Judge3;
			parameters[5].Value = model.Judge4;
			parameters[6].Value = model.Judge5;
			parameters[7].Value = model.Judge6;
			parameters[8].Value = model.Judge7;
			parameters[9].Value = model.Judge8;
			parameters[10].Value = model.Judge9;
			parameters[11].Value = model.Judge10;
			parameters[12].Value = model.Judge11;
			parameters[13].Value = model.Judge12;
			parameters[14].Value = model.SaveStatu;
			parameters[15].Value = model.KaoHeUnitID;
			parameters[16].Value = model.InsertUserId;
			parameters[17].Value = model.InsertDate;
			parameters[18].Value = model.JudgeCount;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_UserJudgeReport model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_UserJudgeReport set ");
			strSql.Append("MemID=@MemID,");
			strSql.Append("MemName=@MemName,");
			strSql.Append("Judge1=@Judge1,");
			strSql.Append("Judge2=@Judge2,");
			strSql.Append("Judge3=@Judge3,");
			strSql.Append("Judge4=@Judge4,");
			strSql.Append("Judge5=@Judge5,");
			strSql.Append("Judge6=@Judge6,");
			strSql.Append("Judge7=@Judge7,");
			strSql.Append("Judge8=@Judge8,");
			strSql.Append("Judge9=@Judge9,");
			strSql.Append("Judge10=@Judge10,");
			strSql.Append("Judge11=@Judge11,");
			strSql.Append("Judge12=@Judge12,");
			strSql.Append("SaveStatu=@SaveStatu,");
			strSql.Append("KaoHeUnitID=@KaoHeUnitID,");
			strSql.Append("InsertUserId=@InsertUserId,");
			strSql.Append("InsertDate=@InsertDate,");
			strSql.Append("JudgeCount=@JudgeCount");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@Judge1", SqlDbType.Decimal,9),
					new SqlParameter("@Judge2", SqlDbType.Decimal,9),
					new SqlParameter("@Judge3", SqlDbType.Decimal,9),
					new SqlParameter("@Judge4", SqlDbType.Decimal,9),
					new SqlParameter("@Judge5", SqlDbType.Decimal,9),
					new SqlParameter("@Judge6", SqlDbType.Decimal,9),
					new SqlParameter("@Judge7", SqlDbType.Decimal,9),
					new SqlParameter("@Judge8", SqlDbType.Decimal,9),
					new SqlParameter("@Judge9", SqlDbType.Decimal,9),
					new SqlParameter("@Judge10", SqlDbType.Decimal,9),
					new SqlParameter("@Judge11", SqlDbType.Decimal,9),
					new SqlParameter("@Judge12", SqlDbType.Decimal,9),
					new SqlParameter("@SaveStatu", SqlDbType.Int,4),
					new SqlParameter("@KaoHeUnitID", SqlDbType.Int,4),
					new SqlParameter("@InsertUserId", SqlDbType.Int,4),
					new SqlParameter("@InsertDate", SqlDbType.DateTime),
					new SqlParameter("@JudgeCount", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.MemID;
			parameters[1].Value = model.MemName;
			parameters[2].Value = model.Judge1;
			parameters[3].Value = model.Judge2;
			parameters[4].Value = model.Judge3;
			parameters[5].Value = model.Judge4;
			parameters[6].Value = model.Judge5;
			parameters[7].Value = model.Judge6;
			parameters[8].Value = model.Judge7;
			parameters[9].Value = model.Judge8;
			parameters[10].Value = model.Judge9;
			parameters[11].Value = model.Judge10;
			parameters[12].Value = model.Judge11;
			parameters[13].Value = model.Judge12;
			parameters[14].Value = model.SaveStatu;
			parameters[15].Value = model.KaoHeUnitID;
			parameters[16].Value = model.InsertUserId;
			parameters[17].Value = model.InsertDate;
			parameters[18].Value = model.JudgeCount;
			parameters[19].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_UserJudgeReport ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_UserJudgeReport ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_UserJudgeReport GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,MemID,MemName,Judge1,Judge2,Judge3,Judge4,Judge5,Judge6,Judge7,Judge8,Judge9,Judge10,Judge11,Judge12,SaveStatu,KaoHeUnitID,InsertUserId,InsertDate,JudgeCount from cm_UserJudgeReport ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_UserJudgeReport model=new TG.Model.cm_UserJudgeReport();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_UserJudgeReport DataRowToModel(DataRow row)
		{
			TG.Model.cm_UserJudgeReport model=new TG.Model.cm_UserJudgeReport();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["MemID"]!=null && row["MemID"].ToString()!="")
				{
					model.MemID=int.Parse(row["MemID"].ToString());
				}
				if(row["MemName"]!=null)
				{
					model.MemName=row["MemName"].ToString();
				}
				if(row["Judge1"]!=null && row["Judge1"].ToString()!="")
				{
					model.Judge1=decimal.Parse(row["Judge1"].ToString());
				}
				if(row["Judge2"]!=null && row["Judge2"].ToString()!="")
				{
					model.Judge2=decimal.Parse(row["Judge2"].ToString());
				}
				if(row["Judge3"]!=null && row["Judge3"].ToString()!="")
				{
					model.Judge3=decimal.Parse(row["Judge3"].ToString());
				}
				if(row["Judge4"]!=null && row["Judge4"].ToString()!="")
				{
					model.Judge4=decimal.Parse(row["Judge4"].ToString());
				}
				if(row["Judge5"]!=null && row["Judge5"].ToString()!="")
				{
					model.Judge5=decimal.Parse(row["Judge5"].ToString());
				}
				if(row["Judge6"]!=null && row["Judge6"].ToString()!="")
				{
					model.Judge6=decimal.Parse(row["Judge6"].ToString());
				}
				if(row["Judge7"]!=null && row["Judge7"].ToString()!="")
				{
					model.Judge7=decimal.Parse(row["Judge7"].ToString());
				}
				if(row["Judge8"]!=null && row["Judge8"].ToString()!="")
				{
					model.Judge8=decimal.Parse(row["Judge8"].ToString());
				}
				if(row["Judge9"]!=null && row["Judge9"].ToString()!="")
				{
					model.Judge9=decimal.Parse(row["Judge9"].ToString());
				}
				if(row["Judge10"]!=null && row["Judge10"].ToString()!="")
				{
					model.Judge10=decimal.Parse(row["Judge10"].ToString());
				}
				if(row["Judge11"]!=null && row["Judge11"].ToString()!="")
				{
					model.Judge11=decimal.Parse(row["Judge11"].ToString());
				}
				if(row["Judge12"]!=null && row["Judge12"].ToString()!="")
				{
					model.Judge12=decimal.Parse(row["Judge12"].ToString());
				}
				if(row["SaveStatu"]!=null && row["SaveStatu"].ToString()!="")
				{
					model.SaveStatu=int.Parse(row["SaveStatu"].ToString());
				}
				if(row["KaoHeUnitID"]!=null && row["KaoHeUnitID"].ToString()!="")
				{
					model.KaoHeUnitID=int.Parse(row["KaoHeUnitID"].ToString());
				}
				if(row["InsertUserId"]!=null && row["InsertUserId"].ToString()!="")
				{
					model.InsertUserId=int.Parse(row["InsertUserId"].ToString());
				}
				if(row["InsertDate"]!=null && row["InsertDate"].ToString()!="")
				{
					model.InsertDate=DateTime.Parse(row["InsertDate"].ToString());
				}
				if(row["JudgeCount"]!=null && row["JudgeCount"].ToString()!="")
				{
					model.JudgeCount=decimal.Parse(row["JudgeCount"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,MemID,MemName,Judge1,Judge2,Judge3,Judge4,Judge5,Judge6,Judge7,Judge8,Judge9,Judge10,Judge11,Judge12,SaveStatu,KaoHeUnitID,InsertUserId,InsertDate,JudgeCount ");
			strSql.Append(" FROM cm_UserJudgeReport ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,MemID,MemName,Judge1,Judge2,Judge3,Judge4,Judge5,Judge6,Judge7,Judge8,Judge9,Judge10,Judge11,Judge12,SaveStatu,KaoHeUnitID,InsertUserId,InsertDate,JudgeCount ");
			strSql.Append(" FROM cm_UserJudgeReport ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_UserJudgeReport ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_UserJudgeReport T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_UserJudgeReport";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

