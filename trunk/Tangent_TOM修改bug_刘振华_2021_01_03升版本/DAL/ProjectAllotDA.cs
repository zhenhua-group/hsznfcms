﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using System.Data.SqlClient;
using TG.DBUtility;
using TG.Common.EntityBuilder;
using System.Data;

namespace TG.DAL
{
    public class ProjectAllotDA
    {

        public int SavePartOneRecord(PartOne partOneEntity)
        {
            //是否需要删除原始数据
            if (partOneEntity.SysNo != 0)
            {
                DbHelperSQL.ExecuteSql("delete cm_ProjectCost where ID=" + partOneEntity.SysNo);
            }

            #region 修改PartOne部分
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_ProjectCost (pro_ID,DesignNum,BuildArea,CprCharge,Cprunitprice,CurCharge,CurRate,TaxRate,LowDivUnitArea,LowDivUnitPrice,LowRateUnitArea,LowRateUnitPrice,InDate,StrucType) values(");
            strSql.Append(partOneEntity.ProjectSysNo + ",");
            strSql.Append("N'" + partOneEntity.DesignNum + "',");
            strSql.Append(partOneEntity.BuildArea + ",");
            strSql.Append(partOneEntity.CprCharge + ",");
            strSql.Append(partOneEntity.CprUnitprice + ",");
            strSql.Append(partOneEntity.CurChange + ",");
            strSql.Append(partOneEntity.CurRate + ",");
            strSql.Append(partOneEntity.TaxRate + ",");
            strSql.Append(partOneEntity.LowDivUnitArea + ",");
            strSql.Append(partOneEntity.LowDivUnitPrice + ",");
            strSql.Append(partOneEntity.LowRateUnitArea + ",");
            strSql.Append(partOneEntity.LowRateUntiPrice + ",");
            strSql.Append("N'" + partOneEntity.InDate + "',");
            strSql.Append("" + partOneEntity.StrucType + ")");
            strSql.Append(";select @@identity;");
            object objResult = DbHelperSQL.GetSingle(strSql.ToString());
            int costid = objResult == null ? 0 : Convert.ToInt32(objResult);

            //向项目分配次数表中插入数据
            if (costid != 0)
            {
                StringBuilder strsql = new StringBuilder();
                strsql.Append("insert into cm_ProjectAllot(pro_id,money,[percent]) values(");
                strsql.Append(partOneEntity.ProjectSysNo + ",");
                strsql.Append(partOneEntity.CurChange + ",");
                strsql.Append(partOneEntity.CurChange / partOneEntity.CprCharge + ")");
                strsql.Append(";select @@identity;");
                DbHelperSQL.ExecuteSql(strsql.ToString());
            }
            #endregion
            return costid;
        }

        public int SavePartThreeRecord(PartThree partThree)
        {
            string partThreeInsertSql = "insert into cm_CostUser(cost_ID,DesignPay,ABCDEPay,DesignUserPay,CheckUserPay,SpeRegPay,AuditUserPay,JuageUserPay,SpecialtyPayPercent,ProfessionalPercent) values(" + partThree.PartOneSysNo + ",N'" + partThree.DesignPay + "',N'" + partThree.ABCDPay + "',N'" + partThree.DesignUserPay + "',N'" + partThree.CheckUserPay + "',N'" + partThree.SpeRegPay + "',N'" + partThree.AuditUserPay + "',N'" + partThree.JuageUserPay + "',N'" + partThree.SpecialtyPayPercent + "',N'" + partThree.ProfessionalPercent + "')";
            int count = DbHelperSQL.ExecuteSql(partThreeInsertSql);
            if (count > 0)
            {
                if (count > 0)
                {
                    UpDatePartOneStatus('D', partThree.PartOneSysNo);
                }
            }
            return count;
        }

        public int UpDatePartOneStatus(char status, int partOneSysNo)
        {
            string sql = "update cm_ProjectCost Set Status=N'" + status + "' where ID=" + partOneSysNo;

            return DbHelperSQL.ExecuteSql(sql);
        }

        public List<AllotUserViewEntity> GetAllotUsers(int projectSysNo)
        {
            string sql = string.Format(@"SELECT 
                                                               tr.[mem_ID] as UserSysNo
                                                              ,[mem_RoleIDs] as UserRoleSysNo
                                                              ,tm.mem_Name as UserName
                                                              ,ts.spe_Name as Speciality
                                                               FROM [dbo].[tg_relation] tr join tg_member tm on.tm.mem_ID  = tr.mem_ID join tg_speciality ts on ts.spe_ID =  tm.mem_Speciality_ID where tr.pro_ID ={0}", projectSysNo);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<AllotUserViewEntity> resultList = EntityBuilder<AllotUserViewEntity>.BuilderEntityList(reader);

            return resultList;
        }
        public int SavePartTwoRecord(PartTwo partTwoEntity)
        {
            if (partTwoEntity.PartOneSysNo != 0)
            {
                //如果不为零的场合，删除该数据
                DbHelperSQL.ExecuteSql("delete cm_CostSignel where cost_ID=" + partTwoEntity.PartOneSysNo);
            }

            #region 新规PartTwo部分
            StringBuilder partTwostrSql = new StringBuilder();

            partTwostrSql.Append("insert into cm_CostSignel(");
            partTwostrSql.Append("cost_ID,TaxSignel,TaxSignelRate,TaxSignelUser,");
            partTwostrSql.Append("CollegeTax,CollegeRate,CollegeUser,");
            partTwostrSql.Append("ProjectTax,ProjectRate,ProjectUser,");
            partTwostrSql.Append("ProChgTax,ProChgRate,ProChgUser,");
            partTwostrSql.Append("RegisterTax,RegisterRate,RegisterUser,");
            partTwostrSql.Append("ShexTax,ShexRate,ShexUser,");
            partTwostrSql.Append("ShedTax,ShedRate,ShedUser,");
            partTwostrSql.Append("ZonggongTax,ZonggongRate,ZonggongUser,");
            partTwostrSql.Append("YuanzTax,YuanzRate,YuanzUser,");
            partTwostrSql.Append("SumTax,SumRate) ");
            partTwostrSql.Append(" values(");
            partTwostrSql.Append(partTwoEntity.PartOneSysNo + "," + partTwoEntity.TaxSignel + "," + partTwoEntity.TaxSignelRate + ",N'" + partTwoEntity.TaxSignelUser + "',");
            partTwostrSql.Append(partTwoEntity.CollegeTax + "," + partTwoEntity.CollegeRate + ",N'" + partTwoEntity.CollegeUser + "',");
            partTwostrSql.Append(partTwoEntity.ProjectTax + "," + partTwoEntity.ProjectRate + ",N'" + partTwoEntity.ProjectUser + "',");
            partTwostrSql.Append(partTwoEntity.ProChgTax + "," + partTwoEntity.ProChgRate + ",N'" + partTwoEntity.ProChgUser + "',");
            partTwostrSql.Append(partTwoEntity.RegisterTax + "," + partTwoEntity.RegisterRate + ",N'" + partTwoEntity.RegisterUser + "',");
            partTwostrSql.Append(partTwoEntity.ShexTax + "," + partTwoEntity.ShexRate + ",N'" + partTwoEntity.ShexUser + "',");
            partTwostrSql.Append(partTwoEntity.ShedTax + "," + partTwoEntity.ShedRate + ",N'" + partTwoEntity.ShedUser + "',");
            partTwostrSql.Append(partTwoEntity.ZonggongTax + "," + partTwoEntity.ZonggongRate + ",N'" + partTwoEntity.ZonggongUser + "',");
            partTwostrSql.Append(partTwoEntity.YuanzTax + "," + partTwoEntity.YuanzRate + ",N'" + partTwoEntity.YuanzUser + "',");
            partTwostrSql.Append(partTwoEntity.SumTax + "," + partTwoEntity.SumRate + ")");
            int count = DbHelperSQL.ExecuteSql(partTwostrSql.ToString());

            if (count > 0)
            {
                UpDatePartOneStatus('C', partTwoEntity.PartOneSysNo);
            }
            return count;
            #endregion
        }
        public PartOne GetPartOneRecordBySysNo(int partOneSysNo)
        {
            string sql = "select ID as SysNo,pro_ID as ProjectSysNo,DesignNum,BuildArea,CprCharge,Cprunitprice,CurCharge,CurRate,TaxRate,LowDivUnitArea,LowDivUnitPrice,LowRateUnitArea,LowRateUnitPrice,Status,InDate,StrucType,InUser From cm_ProjectCost where ID=" + partOneSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            PartOne partOneEntity = EntityBuilder<PartOne>.BuilderEntity(reader);

            reader.Close();

            return partOneEntity;
        }
        /// <summary>
        /// 项目ID 获取第一部分信息
        /// </summary>
        /// <param name="proid"></param>
        /// <returns></returns>
        public PartOne GetPartOneRecord(string proid)
        {
            string strSql = "SELECT TOP 1 ID as SysNo,pro_ID as ProjectSysNo,DesignNum,BuildArea,CprCharge,Cprunitprice,CurCharge,CurRate,TaxRate,LowDivUnitArea,LowDivUnitPrice,Status,LowRateUnitArea,LowRateUnitPrice,InDate,StrucType,InUser FROM dbo.cm_ProjectCost WHERE pro_ID=" + proid + " ORDER BY ID ";
            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql);

            reader.Read();

            PartOne partOneEntity = EntityBuilder<PartOne>.BuilderEntity(reader);

            reader.Close();

            return partOneEntity;
        }

        public PartOne GetPartOneRecordCprunitprice(string proid)
        {
            string strSql = "SELECT TOP 1 ID as SysNo,pro_ID as ProjectSysNo,DesignNum,BuildArea,CprCharge,Cprunitprice,CurCharge,CurRate,TaxRate,LowDivUnitArea,LowDivUnitPrice,Status,LowRateUnitArea,LowRateUnitPrice,InDate,StrucType,InUser FROM dbo.cm_ProjectCost WHERE pro_ID=" + proid + " and isnull(Cprunitprice,0) <>0 ORDER BY ID ";
            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql);

            reader.Read();

            PartOne partOneEntity = EntityBuilder<PartOne>.BuilderEntity(reader);

            reader.Close();

            return partOneEntity;
        }

        public PartOne GetPartOneRecord(int costSysNo)
        {
            string sql = "select ID as SysNo,pro_ID as ProjectSysNo,DesignNum,BuildArea,CprCharge,Cprunitprice,CurCharge,CurRate,TaxRate,LowDivUnitArea,LowDivUnitPrice,LowRateUnitArea,LowRateUnitPrice,StrucType from cm_ProjectCost where ID=" + costSysNo;
            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            PartOne partOneEntity = EntityBuilder<PartOne>.BuilderEntity(reader);

            reader.Close();

            return partOneEntity;
        }

        public PartTwo GetPartTwoRecord(int partOneSysNo)
        {
            string sql = @"SELECT cost_ID ,[TaxSignel],[TaxSignelRate],[TaxSignelUser] ,[CollegeTax] ,[CollegeRate] ,[CollegeUser] ,[ProjectTax] ,[ProjectRate] ,[ProjectUser]
                          ,[ProChgTax] ,[ProChgRate] ,[ProChgUser],[RegisterTax] ,[RegisterRate] ,[RegisterUser] ,[ShexTax] ,[ShexRate] ,[ShexUser],[ShedTax],[ShedRate] ,[ShedUser] ,[ZonggongTax] ,
                          [ZonggongRate],[ZonggongUser],[YuanzTax] ,[YuanzRate] ,[YuanzUser],[SumTax] ,[SumRate] FROM cm_CostSignel where cost_ID=" + partOneSysNo;
            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            PartTwo partTwoEntity = EntityBuilder<PartTwo>.BuilderEntity(reader);

            reader.Close();

            return partTwoEntity;
        }

        public PartThree GetPartThreeRecord(int partOneSysNo)
        {
            string sql = "select DesignPay,ABCDEPay,SpecialtyPayPercent,ProfessionalPercent,DesignUserPay,CheckUserPay,SpeRegPay,AuditUserPay,JuageUserPay from cm_CostUser where cost_ID=" + partOneSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            PartThree partThreeEntity = EntityBuilder<PartThree>.BuilderEntity(reader);

            reader.Close();

            return partThreeEntity;
        }

        public int SavePartFourRecord(PartFour partFour)
        {
            DbHelperSQL.ExecuteSql("delete cm_FinalValidation where cost_ID =" + partFour.PartOneSysNo);

            string partFourInsertSql = "insert into cm_FinalValidation(cost_ID,[Statistics],CourtyardKeep,ProjectKeep,DesignUserKeep,Total,Remark,Dean,Produce,ProduceManager,PM,TabulationUser) values(" + partFour.PartOneSysNo + ",N'" + partFour.Statistics + "'," + partFour.CourtyardKeep + "," + partFour.ProjectKeep + "," + partFour.DesignUserKeep + "," + partFour.Total + ",N'" + partFour.Remark + "',N'" + partFour.Dean + "',N'" + partFour.Produce + "',N'" + partFour.ProduceManager + "',N'" + partFour.PM + "',N'" + partFour.TabulationUser + "')";

            return DbHelperSQL.ExecuteSql(partFourInsertSql);
        }

        public PartFour GetPartFourRecord(int partOneSysNo)
        {
            string sql = "select [Statistics],CourtyardKeep,ProjectKeep,DesignUserKeep,Total,Remark,Dean,Produce,ProduceManager,PM,TabulationUser from cm_FinalValidation where cost_ID=" + partOneSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            PartFour partFourEntity = EntityBuilder<PartFour>.BuilderEntity(reader);

            reader.Close();

            return partFourEntity;
        }

        public List<cm_CostSpeConfig> GetCostSpeConfig()
        {
            string sql = "select c.*,sp.spe_Name as SpeName from cm_CostSpeConfig c join tg_speciality sp on sp.spe_ID =c.SpeID ORDER BY c.ID DESC";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);


            List<cm_CostSpeConfig> cost = EntityBuilder<cm_CostSpeConfig>.BuilderEntityList(reader);

            return cost;
        }
        /// <summary>
        /// 分配中的项目
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        public DataSet GetPassproject(string strWhere)
        {
            string strSql = "select * from cm_passProject";
            if (strWhere != "")
            {
                strSql += " where 1=1 " + strWhere + " ORDER BY ProjectSysNo DESC ";
            }
            else
            {
                strSql += " ORDER BY ProjectSysNo DESC ";
            }
            return DbHelperSQL.Query(strSql);
        }


        public string GetAllotPercent(string projectSysNo)
        {
            string strSql = "SELECT Sum(CurRate) FROM dbo.cm_ProjectCost WHERE pro_ID=" + projectSysNo;
            return Convert.ToString(DbHelperSQL.GetSingle(strSql));
        }
        /// <summary>
        /// 分配完成的项目
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        public DataSet GetList0(string strWhere)
        {
            string strSql = "select * from view_cm_projectAllotList where 1=1 " + strWhere + " Order By projectSysNo DESC ";
            return DbHelperSQL.Query(strSql);
        }
        /// <summary>
        /// 取得分配信息
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        public DataSet GetAllotInfo(string projectSysNo)
        {
            string strSql = " SELECT ID, pro_ID,CprCharge,CurCharge, CurRate,Status, InDate FROM dbo.cm_ProjectCost  WHERE Status <>N'A' AND pro_ID=" + projectSysNo;

            return DbHelperSQL.Query(strSql);
        }

        public int DeleteAllRecord(int costSysNo)
        {
            string deleteSql = string.Format("delete cm_ProjectCost where ID={0} ; delete cm_CostSignel where cost_ID ={0} ; delete cm_CostUser where cost_ID={0} ; delete cm_FinalValidation where cost_ID={0}", costSysNo);
            int count = DbHelperSQL.ExecuteSql(deleteSql);

            //删除消息
            count = DbHelperSQL.ExecuteSql(string.Format("delete cm_SysMsg where ReferenceSysNo like N'%{0}%'", "cost_id=" + costSysNo));
            return count;
        }

        public int GetRoleSysNoByPosition(int position)
        {
            string sql = "select RoleSysNo from cm_ProjectFeeAllotConfig where Position=" + position;

            object objResult = DbHelperSQL.GetSingle(sql);

            return objResult == null ? 0 : Convert.ToInt32(objResult);

        }

        #region 得到配置
        public CostCompanyConfig GetCostCompanyConfig()
        {
            string sql = "select top 1 * from cm_CostCompanyConfig ORDER BY ID DESC";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            CostCompanyConfig cost = EntityBuilder<CostCompanyConfig>.BuilderEntity(reader);

            reader.Close();

            return cost;
        }

        public CostUserConfig GetCostUserConfig()
        {
            string sql = "select top 1 * from cm_CostUserConfig ORDER BY ID DESC";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            CostUserConfig cost = EntityBuilder<CostUserConfig>.BuilderEntity(reader);

            reader.Close();

            return cost;
        }
        public List<string> GetAllSpeciality()
        {
            string sql = "SELECT [spe_Name] FROM [dbo].[tg_speciality]";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<string> resultList = new List<string>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    resultList.Add(reader["spe_Name"].ToString());
                }
                reader.Close();
            }
            return resultList;
        }
        #endregion
    }
}
