﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeUnitAllotReport
	/// </summary>
	public partial class cm_KaoHeUnitAllotReport
	{
		public cm_KaoHeUnitAllotReport()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeUnitAllotReport model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeUnitAllotReport(");
			strSql.Append("RenwuID,UnitID,UnitName,AvgGongzi,Unitjx,Unitjiangbi,Unitjiangall,MonthBei,ProjCount,ProjCountbi,Unitjiang,Stat)");
			strSql.Append(" values (");
			strSql.Append("@RenwuID,@UnitID,@UnitName,@AvgGongzi,@Unitjx,@Unitjiangbi,@Unitjiangall,@MonthBei,@ProjCount,@ProjCountbi,@Unitjiang,@Stat)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.NVarChar,50),
					new SqlParameter("@AvgGongzi", SqlDbType.Decimal,9),
					new SqlParameter("@Unitjx", SqlDbType.Decimal,9),
					new SqlParameter("@Unitjiangbi", SqlDbType.Decimal,9),
					new SqlParameter("@Unitjiangall", SqlDbType.Decimal,9),
					new SqlParameter("@MonthBei", SqlDbType.Decimal,9),
					new SqlParameter("@ProjCount", SqlDbType.Decimal,9),
					new SqlParameter("@ProjCountbi", SqlDbType.Decimal,9),
					new SqlParameter("@Unitjiang", SqlDbType.Decimal,9),
					new SqlParameter("@Stat", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.UnitID;
			parameters[2].Value = model.UnitName;
			parameters[3].Value = model.AvgGongzi;
			parameters[4].Value = model.Unitjx;
			parameters[5].Value = model.Unitjiangbi;
			parameters[6].Value = model.Unitjiangall;
			parameters[7].Value = model.MonthBei;
			parameters[8].Value = model.ProjCount;
			parameters[9].Value = model.ProjCountbi;
			parameters[10].Value = model.Unitjiang;
			parameters[11].Value = model.Stat;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeUnitAllotReport model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeUnitAllotReport set ");
			strSql.Append("RenwuID=@RenwuID,");
			strSql.Append("UnitID=@UnitID,");
			strSql.Append("UnitName=@UnitName,");
			strSql.Append("AvgGongzi=@AvgGongzi,");
			strSql.Append("Unitjx=@Unitjx,");
			strSql.Append("Unitjiangbi=@Unitjiangbi,");
			strSql.Append("Unitjiangall=@Unitjiangall,");
			strSql.Append("MonthBei=@MonthBei,");
			strSql.Append("ProjCount=@ProjCount,");
			strSql.Append("ProjCountbi=@ProjCountbi,");
			strSql.Append("Unitjiang=@Unitjiang,");
			strSql.Append("Stat=@Stat");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.NVarChar,50),
					new SqlParameter("@AvgGongzi", SqlDbType.Decimal,9),
					new SqlParameter("@Unitjx", SqlDbType.Decimal,9),
					new SqlParameter("@Unitjiangbi", SqlDbType.Decimal,9),
					new SqlParameter("@Unitjiangall", SqlDbType.Decimal,9),
					new SqlParameter("@MonthBei", SqlDbType.Decimal,9),
					new SqlParameter("@ProjCount", SqlDbType.Decimal,9),
					new SqlParameter("@ProjCountbi", SqlDbType.Decimal,9),
					new SqlParameter("@Unitjiang", SqlDbType.Decimal,9),
					new SqlParameter("@Stat", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.UnitID;
			parameters[2].Value = model.UnitName;
			parameters[3].Value = model.AvgGongzi;
			parameters[4].Value = model.Unitjx;
			parameters[5].Value = model.Unitjiangbi;
			parameters[6].Value = model.Unitjiangall;
			parameters[7].Value = model.MonthBei;
			parameters[8].Value = model.ProjCount;
			parameters[9].Value = model.ProjCountbi;
			parameters[10].Value = model.Unitjiang;
			parameters[11].Value = model.Stat;
			parameters[12].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeUnitAllotReport ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeUnitAllotReport ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeUnitAllotReport GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,RenwuID,UnitID,UnitName,AvgGongzi,Unitjx,Unitjiangbi,Unitjiangall,MonthBei,ProjCount,ProjCountbi,Unitjiang,Stat from cm_KaoHeUnitAllotReport ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeUnitAllotReport model=new TG.Model.cm_KaoHeUnitAllotReport();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeUnitAllotReport DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeUnitAllotReport model=new TG.Model.cm_KaoHeUnitAllotReport();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["RenwuID"]!=null && row["RenwuID"].ToString()!="")
				{
					model.RenwuID=int.Parse(row["RenwuID"].ToString());
				}
				if(row["UnitID"]!=null && row["UnitID"].ToString()!="")
				{
					model.UnitID=int.Parse(row["UnitID"].ToString());
				}
				if(row["UnitName"]!=null)
				{
					model.UnitName=row["UnitName"].ToString();
				}
				if(row["AvgGongzi"]!=null && row["AvgGongzi"].ToString()!="")
				{
					model.AvgGongzi=decimal.Parse(row["AvgGongzi"].ToString());
				}
				if(row["Unitjx"]!=null && row["Unitjx"].ToString()!="")
				{
					model.Unitjx=decimal.Parse(row["Unitjx"].ToString());
				}
				if(row["Unitjiangbi"]!=null && row["Unitjiangbi"].ToString()!="")
				{
					model.Unitjiangbi=decimal.Parse(row["Unitjiangbi"].ToString());
				}
				if(row["Unitjiangall"]!=null && row["Unitjiangall"].ToString()!="")
				{
					model.Unitjiangall=decimal.Parse(row["Unitjiangall"].ToString());
				}
				if(row["MonthBei"]!=null && row["MonthBei"].ToString()!="")
				{
					model.MonthBei=decimal.Parse(row["MonthBei"].ToString());
				}
				if(row["ProjCount"]!=null && row["ProjCount"].ToString()!="")
				{
					model.ProjCount=decimal.Parse(row["ProjCount"].ToString());
				}
				if(row["ProjCountbi"]!=null && row["ProjCountbi"].ToString()!="")
				{
					model.ProjCountbi=decimal.Parse(row["ProjCountbi"].ToString());
				}
				if(row["Unitjiang"]!=null && row["Unitjiang"].ToString()!="")
				{
					model.Unitjiang=decimal.Parse(row["Unitjiang"].ToString());
				}
				if(row["Stat"]!=null && row["Stat"].ToString()!="")
				{
					model.Stat=int.Parse(row["Stat"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,RenwuID,UnitID,UnitName,AvgGongzi,Unitjx,Unitjiangbi,Unitjiangall,MonthBei,ProjCount,ProjCountbi,Unitjiang,Stat ");
			strSql.Append(" FROM cm_KaoHeUnitAllotReport ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,RenwuID,UnitID,UnitName,AvgGongzi,Unitjx,Unitjiangbi,Unitjiangall,MonthBei,ProjCount,ProjCountbi,Unitjiang,Stat ");
			strSql.Append(" FROM cm_KaoHeUnitAllotReport ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeUnitAllotReport ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeUnitAllotReport T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeUnitAllotReport";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

