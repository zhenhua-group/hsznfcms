﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DBUtility;
using System.Data.SqlClient;
using TG.Model;
using TG.Common.EntityBuilder;
using System.Data;

namespace TG.DAL
{
    public class cm_SysMsg
    {
        public int InsertSysMsg(TG.Model.cm_SysMsg sysMsg)
        {
            StringBuilder sb = new StringBuilder("insert into cm_SysMsg(AuditRecordSysNo,FromUser,ToRole,Status,InDate) values(");
            sb.Append(sysMsg.AuditRecordSysNo + ",");
            sb.Append(sysMsg.FromUser + ",");
            sb.Append(sysMsg.ToRole + ",");
            sb.Append("N'A',");
            sb.Append("N'" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "')");
            int count = DbHelperSQL.ExecuteSql(sb.ToString());
            return count;
        }

        /// <summary>
        /// 查询消息列表
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="pageCurrent"></param>
        /// <returns></returns>
        public List<TG.Model.cm_SysMsg> GetSysMsgList(int pageSize, int pageCurrent, int userSysNo, string coperationName, char sysMsgStatus)
        {
            /*没有调用此方法，无需修改。Created by Sago*/
            string whereSql = " where 1=1 ";
            //合同名称不为空的场合
            if (!string.IsNullOrEmpty(coperationName))
            {
                whereSql += " and CoperationName =N'" + coperationName + "'";
            }

            if (sysMsgStatus == 'A' || sysMsgStatus == 'D')
            {
                whereSql += " and MsgStatus =N'" + sysMsgStatus + "'";
            }

            whereSql += "  and ((FromUser = " + userSysNo + " and ToRole = 0) ";
            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(userSysNo);
            //查询到该用户角色的场合
            if (userRoleList != null && userRoleList.Count > 0)
            {
                whereSql += " or (ToRole in (";
                foreach (var role in userRoleList)
                {
                    whereSql += role.SysNo + ",";
                }
                whereSql = whereSql.Substring(0, whereSql.Length - 1) + "))) ";
            }

            string sql = "";
            //sql += "   SELECT TOP(" + pageSize + ")* FROM ( SELECT ROW_NUMBE() OVER(ORDER BY c.MsgSysNo DESC) as RowID,[MsgSysNo],[AuditRecordSysNo],[FromUser],[ToRole],[MsgStatus],[MsgInDate],[CoperationSysNo],[AuditRecordInUser],[AuditRecordInDate],[AuditRecordStatus],[UndertakeProposal],[OperateDepartmentProposal],[TechnologyDepartmentProposal],[GeneralManagerProposal],[BossProposal],[AuditUser],[AuditDate],[CoperationName] FROM [View_CoperationAuditSysMsg] c " + whereSql + ") TT WHERE RowID > " + pageCurrent + "* " + pageSize + "";

            List<TG.Model.cm_SysMsg> result = new List<TG.Model.cm_SysMsg>();

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);
            while (reader.Read())
            {
                TG.Model.cm_SysMsg sysMsg = new TG.Model.cm_SysMsg
                {
                    MsgSysNo = reader["MsgSysNo"] == DBNull.Value ? 0 : (int)reader["MsgSysNo"],
                    AuditRecordSysNo = reader["AuditRecordSysNo"] == DBNull.Value ? 0 : (int)reader["AuditRecordSysNo"],
                    FromUser = reader["FromUser"] == DBNull.Value ? 0 : (int)reader["FromUser"],
                    ToRole = reader["ToRole"] == DBNull.Value ? 0 : (int)reader["ToRole"],
                    MsgStatus = reader["MsgStatus"] == DBNull.Value ? string.Empty : reader["MsgStatus"].ToString(),
                    MsgInDate = reader["MsgInDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["MsgInDate"]),
                    CoperationSysNo = reader["CoperationSysNo"] == DBNull.Value ? 0 : Convert.ToInt32(reader["CoperationSysNo"]),
                    AuditRecordInUser = reader["AuditRecordInUser"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AuditRecordInUser"]),
                    AuditRecordInDate = reader["AuditRecordInDate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["AuditRecordInDate"]),
                    AuditRecordStatus = reader["AuditRecordStatus"] == DBNull.Value ? string.Empty : reader["AuditRecordStatus"].ToString(),
                    UndertakeProposal = reader["UndertakeProposal"] == DBNull.Value ? string.Empty : reader["UndertakeProposal"].ToString(),
                    OperateDepartmentProposal = reader["OperateDepartmentProposal"] == DBNull.Value ? string.Empty : reader["OperateDepartmentProposal"].ToString(),
                    TechnologyDepartmentProposal = reader["TechnologyDepartmentProposal"] == DBNull.Value ? string.Empty : reader["TechnologyDepartmentProposal"].ToString(),
                    GeneralManagerProposal = reader["GeneralManagerProposal"] == DBNull.Value ? string.Empty : reader["GeneralManagerProposal"].ToString(),
                    AuditUser = reader["AuditUser"] == DBNull.Value ? string.Empty : reader["AuditUser"].ToString(),
                    AuditDate = reader["AuditDate"] == DBNull.Value ? string.Empty : reader["AuditDate"].ToString(),
                    CoperationName = reader["CoperationName"] == DBNull.Value ? string.Empty : reader["CoperationName"].ToString()
                };
                result.Add(sysMsg);
            }
            reader.Close();
            return result;
        }

        public List<TG.Model.cm_Role> GetRoleByUserSysNo(int userSysNo)
        {
            List<TG.Model.cm_Role> sourceRoleList = new TG.DAL.cm_Role().GetRoleList();

            sourceRoleList = sourceRoleList.Where(delegate(TG.Model.cm_Role role)
             {
                 string[] userArray = role.Users.Split(',');
                 if (userArray.Contains(userSysNo.ToString()))
                 {
                     return true;
                 }
                 else
                 {
                     return false;
                 }
             }).ToList<TG.Model.cm_Role>();

            return sourceRoleList;
        }

        //已读消息
        public int UpdateSysMsgStatus(int SysMsgSysNo)
        {
            string sql = "update cm_SysMsg set Status =N'D' Where SysNo=" + SysMsgSysNo;
            return DbHelperSQL.ExecuteSql(sql);
        }
        //待办事项  此方法会批量更新所有同一时间发送消息
        public int UpdateSysMsgDoneStatus(int SysMsgSysNo)
        {
            string sql = "update cm_SysMsg set Status=N'D', IsDone =N'D' Where InDate=(Select InDate From cm_SysMsg Where SysNo=" + SysMsgSysNo + ") and ReferenceSysNo=(Select ReferenceSysNo From cm_SysMsg Where SysNo=" + SysMsgSysNo + ") and MsgType=(Select MsgType From cm_SysMsg Where SysNo=" + SysMsgSysNo + ") ";
            return DbHelperSQL.ExecuteSql(sql);
        }
        //更改代办事项 根据ID
        public int UpdateSysMsgDoneStatusByID(int SysMsgSysNo)
        {
            string sql = "update cm_SysMsg set Status=N'D',IsDone =N'D' Where SysNo=" + SysMsgSysNo + "";
            return DbHelperSQL.ExecuteSql(sql);
        }
        //更新合同消息状态
        public int UpDateSysMsgStatusByCoperationSysNo(int coperationSysNo)
        {
            string sql = "update cm_SysMsg set Status =N'D' where MsgType =1 and ReferenceSysNo = '" + coperationSysNo + "'";
            return DbHelperSQL.ExecuteSql(sql);
        }
        //消息数量  未读/未办
        public int GetSysMsgCount(int userSysNo)
        {
            StringBuilder strWhere = new StringBuilder();

            strWhere.Append(" AND (S.IsDone='A' OR S.Status='A') ");
            //查询登陆人的消息条件
            string whereSql = " AND (";

            //查到个人条件
            whereSql += string.Format("(S.FromUser = {0} AND S.ToRole = 0 ) ", userSysNo);

            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(userSysNo);
            //查询到该用户角色的场合
            if (userRoleList != null && userRoleList.Count > 0)
            {
                whereSql += " OR  (Exists ( Select SysNo From dbo.cm_Role Where (";
                bool isfirst = true;
                foreach (var role in userRoleList)
                {
                    if (!isfirst)
                    {
                        whereSql += string.Format(" OR SysNo={0} ", role.SysNo);
                    }
                    else
                    {
                        whereSql += string.Format(" SysNo={0} ", role.SysNo);
                        isfirst = false;
                    }
                }
                whereSql = whereSql.Substring(0, whereSql.Length - 1) + ") AND SysNo=S.ToRole ))";
            }

            whereSql += ")";
            strWhere.Append(whereSql);
            //判断用户ID为空和消息对象为空
            strWhere.Append(" AND FromUser>0 AND InUser>0");

            string sql = "select COUNT(SysNo) from cm_SysMsg S Where 1=1 " + strWhere.ToString();
            object count = DbHelperSQL.GetSingle(sql);
            return count == null ? 0 : (int)count;
        }
        //待办事项个数  2017年4月10日 优化消息查询速度
        public int GetSysMsgDoneCount(int userSysNo)
        {
            StringBuilder strWhere = new StringBuilder();

            strWhere.Append(" AND S.IsDone='A' ");
            //查询登陆人的消息条件
            string whereSql = " AND (";

            //查到个人条件
            whereSql += string.Format("(S.FromUser = {0} AND S.ToRole = 0 ) ", userSysNo);

            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(userSysNo);
            //查询到该用户角色的场合
            if (userRoleList != null && userRoleList.Count > 0)
            {
                whereSql += " OR  (Exists ( Select SysNo From dbo.cm_Role Where (";
                bool isfirst = true;
                foreach (var role in userRoleList)
                {
                    if (!isfirst)
                    {
                        whereSql += string.Format(" OR SysNo={0} ", role.SysNo);
                    }
                    else
                    {
                        whereSql += string.Format(" SysNo={0} ", role.SysNo);
                        isfirst = false;
                    }
                }
                whereSql = whereSql.Substring(0, whereSql.Length - 1) + ") AND SysNo=S.ToRole ))";
            }

            whereSql += ")";
            strWhere.Append(whereSql);
            //判断用户ID为空和消息对象为空
            strWhere.Append(" AND FromUser>0 AND InUser>0");

            string sql = "select COUNT(SysNo) from cm_SysMsg S Where 1=1 " + strWhere.ToString();
            object count = DbHelperSQL.GetSingle(sql);
            return count == null ? 0 : (int)count;
        }

        //待办事项个数-产值
        public int GetSysValueMsgDoneCount(int userSysNo)
        {
            string whereSql = " where  ((FromUser = " + userSysNo + " and ToRole = 0 ) ";
            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(userSysNo);
            //查询到该用户角色的场合
            if (userRoleList != null && userRoleList.Count > 0)
            {
                whereSql += " or  (ToRole in (";
                foreach (var role in userRoleList)
                {
                    whereSql += role.SysNo + ",";
                }
                whereSql = whereSql.Substring(0, whereSql.Length - 1) + "))) ";
            }
            else
            {
                whereSql = whereSql + ")";
            }
            whereSql = whereSql + "  and IsDone =N'A' and InUser <> 0 and MsgType  in (5,10,14,15,16,17,18,19,20,25,26,27,28,29,30)";
            string sql = "select COUNT(*) from cm_SysMsg " + whereSql;
            object count = DbHelperSQL.GetSingle(sql);
            return count == null ? 0 : (int)count;
        }
        /// <summary>
        /// 得到消息列表
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public List<SysMsgEntity> GetMsgList(SysMsgQueryEntity queryEntity)
        {
            string whereSql = " where (FromUser = " + queryEntity.UserSysNo + " and ToRole = 0  and MsgType=" + queryEntity.MsgType + ") ";
            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(queryEntity.UserSysNo);
            //查询到该用户角色的场合
            if (userRoleList != null && userRoleList.Count > 0)
            {
                whereSql += " or  (ToRole in (";
                foreach (var role in userRoleList)
                {
                    whereSql += role.SysNo + ",";
                }
                whereSql = whereSql.Substring(0, whereSql.Length - 1) + ") and MsgType=" + queryEntity.MsgType + ")";
            }

            string sql = "SELECT s.SysNo,FromUser,s.InUser,ToRole,s.InDate,MsgType,ReferenceSysNo,s.Status,pa.Status as ProjectAuditStatus,s.IsDone FROM cm_SysMsg  s join cm_ProjectAudit pa on pa.SysNo =s.ReferenceSysNo" + whereSql + " ORDER BY s.SysNo DESC";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<SysMsgEntity> resultList = EntityBuilder<SysMsgEntity>.BuilderEntityList(reader);

            return resultList;
        }

        public List<ProjectAuditListViewDataEntity> GetProjectAuditListViewEntity(int userSysNo)
        {
            string whereSql = " where (FromUser = " + userSysNo + " and ToRole = 0  and MsgType=2 ) ";
            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(userSysNo);
            //查询到该用户角色的场合
            if (userRoleList != null && userRoleList.Count > 0)
            {
                whereSql += " or  (ToRole in (";
                foreach (var role in userRoleList)
                {
                    whereSql += role.SysNo + ",";
                }
                whereSql = whereSql.Substring(0, whereSql.Length - 1) + ") and MsgType=2)";
            }

            string sql = string.Format(@"select 
                                                     pa.SysNo
                                                    ,pa.ProjectSysNo
                                                    ,pa.Status
                                                    ,s.Status as MsgStatus
                                                    ,pa.Suggestion
                                                    ,pa.AuditUser as AuditUser
                                                    ,pa.AudtiDate
                                                    ,s.InDate
                                                    ,s.InUser
                                                    ,u.mem_Name as FromUserName
                                                    ,p.pro_name
                                                    ,s.ExtendField
                                                    ,s.IsDone
                                                    from cm_SysMsg s 
                                                    join cm_ProjectAudit pa on pa.SysNo = s.ReferenceSysNo 
                                                    join cm_Project p on p.pro_id = pa.ProjectSysNo 
                                                    join tg_member u on u.mem_ID = s.InUser
                                                    {0} ORDER BY s.SysNo DESC", whereSql);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectAuditListViewDataEntity> projectAuditList = EntityBuilder<ProjectAuditListViewDataEntity>.BuilderEntityList(reader);

            return projectAuditList;
        }

        /// <summary>
        /// 插入一条新消息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int CreateNewMsg(SysMsgEntity dataEntity)
        {
            StringBuilder sb = new StringBuilder("insert into cm_SysMsg(MsgType,FromUser,ToRole,Status,ReferenceSysNo,InUser,InDate) values(");
            sb.Append(dataEntity.MsgType + ",");
            sb.Append(dataEntity.FromUser + ",");
            sb.Append(dataEntity.ToRole + ",");
            sb.Append("N'A',");
            sb.Append(dataEntity.ReferenceSysNo + ",");
            sb.Append(dataEntity.InUser + ",");
            sb.Append("N'" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "')");
            int count = DbHelperSQL.ExecuteSql(sb.ToString());
            return count;
        }

        /// <summary>
        /// 取得系统消息情报
        /// </summary>
        /// <returns></returns>
        public List<SysMessageViewEntity> GetSysMessageRecord(SysMessageQueryEntity queryEntity)
        {
            string statusSql = " and 1 = 1";
            if (!string.IsNullOrEmpty(queryEntity.MessageDone))
            {
                if (!string.IsNullOrEmpty(queryEntity.MessageDoneStatus))
                {
                    statusSql += " and s.IsDone = N'" + queryEntity.MessageDoneStatus + "'";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(queryEntity.MessageStatus))
                {
                    statusSql += " and s.Status = N'" + queryEntity.MessageStatus + "'";
                }
            }

            string msgTypeSql = " and 1 = 1 ";
            if (!string.IsNullOrEmpty(queryEntity.MessageType))
            {
                msgTypeSql += " and s.MsgType=" + queryEntity.MessageType;
            }
            string msgQueryConditionSql = " and 1 = 1 ";
            if (!string.IsNullOrEmpty(queryEntity.QueryCondition))
            {
                msgQueryConditionSql += " and s.QueryCondition like N'%" + queryEntity.QueryCondition + "%' ";
            }

            string whereSql = " where (s.FromUser = " + queryEntity.UserSysNo + " and s.ToRole = 0 " + (statusSql + msgTypeSql + msgQueryConditionSql) + ") ";
            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(queryEntity.UserSysNo);
            //查询到该用户角色的场合
            if (userRoleList != null && userRoleList.Count > 0)
            {
                whereSql += " or  (s.ToRole in (";
                foreach (var role in userRoleList)
                {
                    whereSql += role.SysNo + ",";
                }
                whereSql = whereSql.Substring(0, whereSql.Length - 1) + ") " + (statusSql + msgTypeSql + msgQueryConditionSql) + ")";
            }
            string sql = "select s.SysNo,s.FromUser,s.ToRole,s.MsgType,s.ReferenceSysNo,s.Status,s.InUser,s.InDate,u.mem_Name as FromUserName,MessageContent,s.IsDone from cm_SysMsg s join tg_member u on u.mem_ID= s.InUser " + whereSql + " ORDER BY s.SysNo DESC";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<SysMessageViewEntity> resultList = EntityBuilder<SysMessageViewEntity>.BuilderEntityList(reader);

            return resultList;
        }
        /// <summary>
        /// 取得消息分页信息
        /// </summary>
        /// <returns></returns>
        public List<SysMessageViewEntity> GetSysMessageRecordProc(SysMessageQueryEntity queryEntity, int startIndex, int endIndex)
        {
            string statusSql = " and 1 = 1";
            //消息状态(已读，未读，已办，未办)
            if (!string.IsNullOrEmpty(queryEntity.MessageDone))
            {
                if (!string.IsNullOrEmpty(queryEntity.MessageDoneStatus))
                {
                    statusSql += " and s.IsDone = N'" + queryEntity.MessageDoneStatus + "'";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(queryEntity.MessageStatus))
                {
                    if (!string.IsNullOrEmpty(queryEntity.MessageDoneStatus))
                    {
                        statusSql += " and (s.Status = N'" + queryEntity.MessageStatus + "' or s.IsDone = N'" + queryEntity.MessageDoneStatus + "') ";
                    }
                    else
                    {
                        statusSql += " and s.Status = N'" + queryEntity.MessageStatus + "' ";
                    }

                }
            }

            string msgTypeSql = " and 1 = 1 ";
            //消息类型
            if (!string.IsNullOrEmpty(queryEntity.MessageType))
            {
                if (int.Parse(queryEntity.MessageType) == 0)
                {
                    msgTypeSql += " and s.MsgType IN (5,10,14,15,16,17,18,19) ";
                }
                else
                {
                    msgTypeSql += " and s.MsgType=" + queryEntity.MessageType;
                }
            }

            string msgQueryConditionSql = " and 1 = 1 ";
            //消息关键字
            if (!string.IsNullOrEmpty(queryEntity.QueryCondition))
            {
                msgQueryConditionSql += " and s.QueryCondition like N'%" + queryEntity.QueryCondition + "%' ";
            }

            string whereSql = "  (s.FromUser = " + queryEntity.UserSysNo + " and s.ToRole = 0 " + (statusSql + msgTypeSql + msgQueryConditionSql) + ") ";
            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(queryEntity.UserSysNo);
            //查询到该用户角色的场合
            if (userRoleList != null && userRoleList.Count > 0)
            {
                whereSql += " or  (s.ToRole in (";
                foreach (var role in userRoleList)
                {
                    whereSql += role.SysNo + ",";
                }
                whereSql = "  AND (" + whereSql.Substring(0, whereSql.Length - 1) + ") " + (statusSql + msgTypeSql + msgQueryConditionSql) + "))";
            }
            else
            {
                whereSql = " AND " + whereSql + " ";
            }

            whereSql += " ORDER BY s.SysNo DESC";

            SqlDataReader reader = DBUtility.DbHelperSQL.RunProcedure("P_cm_SysMsg", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", whereSql) });

            List<SysMessageViewEntity> resultList = EntityBuilder<SysMessageViewEntity>.BuilderEntityList(reader);

            return resultList;
        }
        /// <summary>
        /// 得到系统消息数
        /// </summary>
        /// <param name="queryEntity"></param>
        /// <returns></returns>
        public int GetSysMessageCount(SysMsgQueryEntity queryEntity)
        {
            string whereSql = " where (FromUser = " + queryEntity.UserSysNo + " and ToRole = 0 and s.Status =N'" + queryEntity.ProjectAuditStatus + "'  and MsgType=" + queryEntity.MsgType + ") ";
            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(queryEntity.UserSysNo);
            //查询到该用户角色的场合
            if (userRoleList != null && userRoleList.Count > 0)
            {
                whereSql += " or  (ToRole in (";
                foreach (var role in userRoleList)
                {
                    whereSql += role.SysNo + ",";
                }
                whereSql = whereSql.Substring(0, whereSql.Length - 1) + ") and s.Status =N'" + queryEntity.ProjectAuditStatus + "' and MsgType=" + queryEntity.MsgType + ")";
            }

            string sql = "SELECT COUNT(*) FROM cm_SysMsg s join tg_member u on u.mem_ID= s.InUser " + whereSql;

            int count = Convert.ToInt32(DbHelperSQL.GetSingle(sql));

            return count;
        }
        /// <summary>
        /// 得到消息分页数量
        /// </summary>
        /// <param name="queryEntity"></param>
        /// <returns></returns>
        public object GetSysMessageCountProc(SysMessageQueryEntity queryEntity)
        {
            string statusSql = " and 1 = 1";
            //消息状态(已读，未读)
            if (!string.IsNullOrEmpty(queryEntity.MessageDone))
            {
                if (!string.IsNullOrEmpty(queryEntity.MessageDoneStatus))
                {
                    statusSql += " and s.IsDone = N'" + queryEntity.MessageDoneStatus + "'";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(queryEntity.MessageStatus))
                {
                    if (!string.IsNullOrEmpty(queryEntity.MessageDoneStatus))
                    {
                        statusSql += " and (s.Status = N'" + queryEntity.MessageStatus + "' or s.IsDone = N'" + queryEntity.MessageDoneStatus + "') ";
                    }
                    else
                    {
                        statusSql += " and s.Status = N'" + queryEntity.MessageStatus + "' ";
                    }

                }
            }

            string msgTypeSql = " and 1 = 1 ";
            //消息类型
            if (!string.IsNullOrEmpty(queryEntity.MessageType))
            {
                if (int.Parse(queryEntity.MessageType) == 0)
                {
                    msgTypeSql += " and s.MsgType IN (5,10,14,15,16,17,18,19) ";
                }
                else
                {
                    msgTypeSql += " and s.MsgType=" + queryEntity.MessageType;
                }
            }

            string msgQueryConditionSql = " and 1 = 1 ";
            //消息关键字
            if (!string.IsNullOrEmpty(queryEntity.QueryCondition))
            {
                msgQueryConditionSql += " and s.QueryCondition like N'%" + queryEntity.QueryCondition + "%' ";
            }

            string whereSql = "  (s.FromUser = " + queryEntity.UserSysNo + " and s.ToRole = 0 " + (statusSql + msgTypeSql + msgQueryConditionSql) + ") ";
            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(queryEntity.UserSysNo);
            //查询到该用户角色的场合
            if (userRoleList != null && userRoleList.Count > 0)
            {
                whereSql += " or  (s.ToRole in (";
                foreach (var role in userRoleList)
                {
                    whereSql += role.SysNo + ",";
                }
                whereSql = " AND (" + whereSql.Substring(0, whereSql.Length - 1) + ") " + (statusSql + msgTypeSql + msgQueryConditionSql) + "))";
            }
            else
            {
                whereSql = " AND " + whereSql + " ";
            }

            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_SysMsg_Count", new SqlParameter[] { new SqlParameter("@query", whereSql) });
        }
        //新建一条消息
        public int InsertSysMessage(SysMessageViewEntity sysMessageDataEntity)
        {
            string sql = string.Format("insert into cm_SysMsg (FromUser,ToRole,MsgType,ReferenceSysNo,InUser,InDate,MessageContent,QueryCondition,ExtendField,IsDone)values({0},N'{1}',{2},N'{3}',{4},N'{5}',N'{6}',N'{7}',N'{8}',N'{9}');select @@IDENTITY"
                , sysMessageDataEntity.FromUser
                , sysMessageDataEntity.ToRole
                , sysMessageDataEntity.MsgType
                , sysMessageDataEntity.ReferenceSysNo
                , sysMessageDataEntity.InUser
                , DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"),
                sysMessageDataEntity.MessageContent
                , sysMessageDataEntity.QueryCondition
                , string.IsNullOrEmpty(sysMessageDataEntity.ExtendField) == true ? " " : sysMessageDataEntity.ExtendField
                , sysMessageDataEntity.IsDone);
            int identitySysNo = Convert.ToInt32(DbHelperSQL.GetSingle(sql));

            return identitySysNo;
        }

        /// <summary>
        /// 得到待办状态
        /// </summary>
        /// <param name="messageID"></param>
        /// <returns></returns>
        public string IsDone(int messageID)
        {
            string sql = "select  IsDone from  cm_SysMsg where SysNo=" + messageID + "";
            DataTable dt = DbHelperSQL.Query(sql).Tables[0];

            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0]["IsDone"].ToString();
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// new 20140317 史国庆
        /// </summary>
        /// <param name="usersysno"></param>
        /// <returns></returns>
        public List<SYSMsgQueryEntityByMain> GetListForMain(int usersysno, string wherestring)
        {
            string sql = string.Format(@"SELECT TOP 10 IsDone,Status,MessageContent,MsgType,FromUser FROM cm_SysMsg WHERE FromUser={0} and ToRole = 0  AND {1} and MsgType not in (5,10,14,15,16,17,18,19,20,25,26,27,28,29,30) ", usersysno, wherestring);

            string sqlAcount = string.Format(@"select MsgType as 'PrecentName',COUNT(*) as 'PrecentAcount' from cm_SysMsg WHERE FromUser={0} and ToRole = 0   and MsgType not in (5,10,14,15,16,17,18,19,20,25,26,27,28,29,30)", usersysno);
            string sqlAcountdone = string.Format(@"select MsgType as 'PrecentName',COUNT(*) as 'PrecentAcount' from cm_SysMsg WHERE FromUser={0} and ToRole = 0  AND IsDone='A' and MsgType not in (5,10,14,15,16,17,18,19,20,25,26,27,28,29,30)
 ", usersysno);
            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(usersysno);
            //查询到该用户角色的场合
            if (userRoleList != null && userRoleList.Count > 0)
            {
                sql += " or  (ToRole in (";
                sqlAcount += " or  (ToRole in (";
                sqlAcountdone += " or  (ToRole in (";
                foreach (var role in userRoleList)
                {
                    sql += role.SysNo + ",";
                    sqlAcount += role.SysNo + ",";
                    sqlAcountdone += role.SysNo + ",";
                }
                sql = sql.Substring(0, sql.Length - 1) + ") and " + wherestring + ")   ORDER BY SysNo Desc";
                sqlAcount = sqlAcount.Substring(0, sqlAcount.Length - 1) + ") and " + wherestring + ")  ";
                sqlAcountdone = sqlAcountdone.Substring(0, sqlAcountdone.Length - 1) + ") and " + wherestring + ") ";
            }

            sqlAcount += " group by MsgType";
            sqlAcountdone += " group by MsgType";
            SqlDataReader readeracountdone = DbHelperSQL.ExecuteReader(sqlAcountdone);
            SqlDataReader readeracount = DbHelperSQL.ExecuteReader(sqlAcount);
            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);
            List<SYSMsgQueryEntityByMain> MainList = EntityBuilder<SYSMsgQueryEntityByMain>.BuilderEntityList(reader);
            List<PrecentAcount> AcountList = EntityBuilder<PrecentAcount>.BuilderEntityList(readeracount);
            List<PrecentAcount> AcountListdone = EntityBuilder<PrecentAcount>.BuilderEntityList(readeracountdone);
            List<PrecentAcount> NewListAcount = new List<PrecentAcount>();

            foreach (PrecentAcount item in AcountList)
            {
                PrecentAcount modelAcount = new PrecentAcount();
                foreach (PrecentAcount itemchild in AcountListdone)
                {
                    if (item.PrecentName == itemchild.PrecentName)
                    {
                        modelAcount.PrecentName = itemchild.PrecentName;
                        if (item.PrecentAcountchild != 0)
                        {
                            modelAcount.PrecentAcountchild = itemchild.PrecentAcountchild * 100 / item.PrecentAcountchild;
                        }
                        else
                        {
                            modelAcount.PrecentAcountchild = 0;
                        }
                        NewListAcount.Add(modelAcount);
                    }
                }
            }
            foreach (SYSMsgQueryEntityByMain item in MainList)
            {
                foreach (PrecentAcount itemchild in NewListAcount)
                {
                    if (item.MsgType == itemchild.PrecentName)
                    {
                        item.PrecentAcountModel = itemchild.PrecentAcountchild;
                    }
                }
            }
            return MainList;
        }
        /// <summary>
        /// new 2014-3-17 16:19:42 史国庆
        /// </summary>
        /// <param name="usersysno"></param>
        /// <param name="wherestring"></param>
        /// <returns></returns>
        public List<SYSMsgQueryEntityByMain> GetListForMainAboutMsg(int usersysno, string wherestring)
        {

            //查询到该用户角色的场合

            string sql = string.Format(@"SELECT TOP 10 IsDone,Status,MessageContent,MsgType,FromUser FROM cm_SysMsg  where (FromUser ={0} and ToRole = 0 and (Status =N'A' or IsDone=N'A') and InUser <> 0 ) and MsgType not in (5,10,14,15,16,17,18,19,20,25,26,27,28,29,30)", usersysno);
            //查询用户角色
            List<TG.Model.cm_Role> userRoleList = GetRoleByUserSysNo(usersysno);
            if (userRoleList != null && userRoleList.Count > 0)
            {
                sql += " or  (ToRole in (";
                foreach (var role in userRoleList)
                {
                    sql += role.SysNo + ",";
                }
                //20131107
                //whereSql = whereSql.Substring(0, whereSql.Length - 1) + ") and Status =N'A' and InUser <> 0 and IsDone <>N'A') ";
                sql = sql.Substring(0, sql.Length - 1) + ") and (Status =N'A' or IsDone=N'A') and InUser <> 0 ) ORDER BY SysNo Desc";
            }
            //查询到该用户角色的场合

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);
            List<SYSMsgQueryEntityByMain> MainList = EntityBuilder<SYSMsgQueryEntityByMain>.BuilderEntityList(reader);
            return MainList;
        }
        /// <summary>
        /// 根据条件获取消息记录
        /// </summary>
        /// <param name="sqlwhere"></param>
        /// <returns></returns>
        public DataTable GetListWhere(string sqlwhere)
        {
            string sql = string.Format(@"SELECT *,(select mem_Name from tg_member where mem_ID=FromUser) UserName FROM cm_SysMsg where 1=1 {0}", sqlwhere);
            DataSet ds = DbHelperSQL.Query(sql);
            if (ds != null)
            {
                return ds.Tables[0];
            }
            return null;
        }
        //
    }
}
