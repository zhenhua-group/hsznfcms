﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_CostSpeConfig
	/// </summary>
	public partial class cm_CostSpeConfig
	{
		public cm_CostSpeConfig()
		{}
		#region  Method


        public bool Exsit(string speid,string strucid)
        {
            string strSql = " Select Count(*) From cm_CostSpeConfig Where SpeID=" + speid + " AND StrucType=" + strucid;
            return DbHelperSQL.Exists(strSql);
        }
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_CostSpeConfig model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.SpeID != null)
			{
				strSql1.Append("SpeID,");
				strSql2.Append(""+model.SpeID+",");
			}
			if (model.ListPrt != null)
			{
				strSql1.Append("ListPrt,");
				strSql2.Append(""+model.ListPrt+",");
			}
            if (model.StrucType!=null)
            {
                strSql1.Append("StrucType,");
                strSql2.Append("" + model.StrucType + ",");
            }
			strSql.Append("insert into cm_CostSpeConfig(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_CostSpeConfig model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_CostSpeConfig set ");
			if (model.SpeID != null)
			{
				strSql.Append("SpeID="+model.SpeID+",");
			}
			else
			{
				strSql.Append("SpeID= null ,");
			}
			if (model.ListPrt != null)
			{
				strSql.Append("ListPrt="+model.ListPrt+",");
			}
			else
			{
				strSql.Append("ListPrt= null ,");
			}
            if (model.StrucType != null)
            {
                strSql.Append("StrucType=" + model.StrucType + ",");
            }
            else
            {
                strSql.Append("StrucType= null ,");
            }
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ID="+ model.ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_CostSpeConfig ");
			strSql.Append(" where ID="+ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_CostSpeConfig ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_CostSpeConfig GetModel(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
            strSql.Append(" ID,SpeID,ListPrt,StrucType ");
			strSql.Append(" from cm_CostSpeConfig ");
			strSql.Append(" where ID="+ID+"" );
			TG.Model.cm_CostSpeConfig model=new TG.Model.cm_CostSpeConfig();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["ID"]!=null && ds.Tables[0].Rows[0]["ID"].ToString()!="")
				{
					model.ID=int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["SpeID"]!=null && ds.Tables[0].Rows[0]["SpeID"].ToString()!="")
				{
					model.SpeID=int.Parse(ds.Tables[0].Rows[0]["SpeID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ListPrt"]!=null && ds.Tables[0].Rows[0]["ListPrt"].ToString()!="")
				{
					model.ListPrt=decimal.Parse(ds.Tables[0].Rows[0]["ListPrt"].ToString());
				}
                if (ds.Tables[0].Rows[0]["StrucType"] != null && ds.Tables[0].Rows[0]["StrucType"].ToString() != "")
                {
                    model.StrucType = int.Parse(ds.Tables[0].Rows[0]["StrucType"].ToString());
                }
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select ID,SpeID,ListPrt,StrucType ");
			strSql.Append(" FROM cm_CostSpeConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" ID,SpeID,ListPrt,StrucType ");
			strSql.Append(" FROM cm_CostSpeConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_CostSpeConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_CostSpeConfig T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

