﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
using System.Collections.Generic;
using System.Collections;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_CustomerInfo
    /// </summary>
    public partial class cm_CustomerInfo
    {
        public cm_CustomerInfo()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("Cst_Id", "cm_CustomerInfo");
        }


        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int Cst_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_CustomerInfo");
            strSql.Append(" where Cst_Id=" + Cst_Id + " ");
            return DbHelperSQL.Exists(strSql.ToString());
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CustomerInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.Cst_No != null)
            {
                strSql1.Append("Cst_No,");
                strSql2.Append("'" + model.Cst_No + "',");
            }
            if (model.Cst_Brief != null)
            {
                strSql1.Append("Cst_Brief,");
                strSql2.Append("'" + model.Cst_Brief + "',");
            }
            if (model.Cst_Name != null)
            {
                strSql1.Append("Cst_Name,");
                strSql2.Append("'" + model.Cst_Name + "',");
            }
            if (model.Cpy_Address != null)
            {
                strSql1.Append("Cpy_Address,");
                strSql2.Append("'" + model.Cpy_Address + "',");
            }
            if (model.Code != null)
            {
                strSql1.Append("Code,");
                strSql2.Append("'" + model.Code + "',");
            }
            if (model.Linkman != null)
            {
                strSql1.Append("Linkman,");
                strSql2.Append("'" + model.Linkman + "',");
            }
            if (model.CallingCard != null)
            {
                strSql1.Append("CallingCard,");
                strSql2.Append("" + model.CallingCard + ",");
            }
            if (model.Cpy_Phone != null)
            {
                strSql1.Append("Cpy_Phone,");
                strSql2.Append("'" + model.Cpy_Phone + "',");
            }
            if (model.Cpy_Fax != null)
            {
                strSql1.Append("Cpy_Fax,");
                strSql2.Append("'" + model.Cpy_Fax + "',");
            }
            if (model.UpdateBy != null)
            {
                strSql1.Append("UpdateBy,");
                strSql2.Append("" + model.UpdateBy + ",");
            }
            if (model.LastUpdate != null)
            {
                strSql1.Append("LastUpdate,");
                strSql2.Append("'" + model.LastUpdate + "',");
            }
            //by long  20130517
            if (model.InsertUserID != null)
            {
                strSql1.Append("InsertUserID,");
                strSql2.Append("" + model.InsertUserID + ",");
            }
            if (model.InsertDate != null)
            {
                strSql1.Append("InsertDate,");
                strSql2.Append("'" + model.InsertDate + "',");
            }
            strSql.Append("insert into cm_CustomerInfo(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CustomerInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_CustomerInfo set ");
            if (model.Cst_No != null)
            {
                strSql.Append("Cst_No='" + model.Cst_No + "',");
            }
            if (model.Cst_Brief != null)
            {
                strSql.Append("Cst_Brief='" + model.Cst_Brief + "',");
            }
            else
            {
                strSql.Append("Cst_Brief= null ,");
            }
            if (model.Cst_Name != null)
            {
                strSql.Append("Cst_Name='" + model.Cst_Name + "',");
            }
            else
            {
                strSql.Append("Cst_Name= null ,");
            }
            if (model.Cpy_Address != null)
            {
                strSql.Append("Cpy_Address='" + model.Cpy_Address + "',");
            }
            else
            {
                strSql.Append("Cpy_Address= null ,");
            }
            if (model.Code != null)
            {
                strSql.Append("Code='" + model.Code + "',");
            }
            else
            {
                strSql.Append("Code= null ,");
            }
            if (model.Linkman != null)
            {
                strSql.Append("Linkman='" + model.Linkman + "',");
            }
            else
            {
                strSql.Append("Linkman= null ,");
            }
            if (model.CallingCard != null)
            {
                strSql.Append("CallingCard=" + model.CallingCard + ",");
            }
            else
            {
                strSql.Append("CallingCard= null ,");
            }
            if (model.Cpy_Phone != null)
            {
                strSql.Append("Cpy_Phone='" + model.Cpy_Phone + "',");
            }
            else
            {
                strSql.Append("Cpy_Phone= null ,");
            }
            if (model.Cpy_Fax != null)
            {
                strSql.Append("Cpy_Fax='" + model.Cpy_Fax + "',");
            }
            else
            {
                strSql.Append("Cpy_Fax= null ,");
            }
            if (model.UpdateBy != null)
            {
                strSql.Append("UpdateBy=" + model.UpdateBy + ",");
            }
            else
            {
                strSql.Append("UpdateBy= null ,");
            }
            if (model.LastUpdate != null)
            {
                strSql.Append("LastUpdate='" + model.LastUpdate + "',");
            }
            else
            {
                strSql.Append("LastUpdate= null ,");
            }
            //by long 20130517
            if (model.InsertUserID != null)
            {
                strSql.Append("InsertUserID=" + model.InsertUserID + ",");
            }
            else
            {
                strSql.Append("InsertUserID= null ,");
            }
            if (model.InsertDate != null)
            {
                strSql.Append("InsertDate='" + model.InsertDate + "',");
            }
            else
            {
                strSql.Append("InsertDate= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where Cst_Id=" + model.Cst_Id + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int Cst_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_CustomerInfo ");
            strSql.Append(" where Cst_Id=" + Cst_Id + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string Cst_Idlist)
        {
            bool flag = true;
            if (Cst_Idlist.IndexOf(',') > -1)
            {
                string[] ids = Cst_Idlist.Split(new char[] { ',' }, StringSplitOptions.None);
                for (int i = 0; i < ids.Length; i++)
                {
                    string cstid = ids[i].ToString();
                    if (cstid.Trim() != "")
                    {
                        string strSql = " Select Count(*) From cm_Coperation Where cst_id=" + cstid;
                        int count = Convert.ToInt32(DbHelperSQL.GetSingle(strSql));
                        if (count > 0)
                        {
                            flag = false;
                        }
                        else
                        {
                            //删除
                            StringBuilder strSql1 = new StringBuilder();
                            strSql1.Append("delete from cm_CustomerInfo ");
                            strSql1.Append(" where Cst_Id =" + cstid + " ");
                            int rows = DbHelperSQL.ExecuteSql(strSql1.ToString());
                        }
                    }
                }
            }
            return flag;
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CustomerInfo GetModel(int Cst_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" Cst_Id,Cst_No,Cst_Brief,Cst_Name,Cpy_Address,Code,Linkman,CallingCard,Cpy_Phone,Cpy_Fax,UpdateBy,LastUpdate,InsertUserID,InsertDate ");
            strSql.Append(" from cm_CustomerInfo ");
            strSql.Append(" where Cst_Id=" + Cst_Id + "");
            TG.Model.cm_CustomerInfo model = new TG.Model.cm_CustomerInfo();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["Cst_Id"] != null && ds.Tables[0].Rows[0]["Cst_Id"].ToString() != "")
                {
                    model.Cst_Id = int.Parse(ds.Tables[0].Rows[0]["Cst_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Cst_No"] != null && ds.Tables[0].Rows[0]["Cst_No"].ToString() != "")
                {
                    model.Cst_No = ds.Tables[0].Rows[0]["Cst_No"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Cst_Brief"] != null && ds.Tables[0].Rows[0]["Cst_Brief"].ToString() != "")
                {
                    model.Cst_Brief = ds.Tables[0].Rows[0]["Cst_Brief"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Cst_Name"] != null && ds.Tables[0].Rows[0]["Cst_Name"].ToString() != "")
                {
                    model.Cst_Name = ds.Tables[0].Rows[0]["Cst_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Cpy_Address"] != null && ds.Tables[0].Rows[0]["Cpy_Address"].ToString() != "")
                {
                    model.Cpy_Address = ds.Tables[0].Rows[0]["Cpy_Address"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Code"] != null && ds.Tables[0].Rows[0]["Code"].ToString() != "")
                {
                    model.Code = ds.Tables[0].Rows[0]["Code"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Linkman"] != null && ds.Tables[0].Rows[0]["Linkman"].ToString() != "")
                {
                    model.Linkman = ds.Tables[0].Rows[0]["Linkman"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CallingCard"] != null && ds.Tables[0].Rows[0]["CallingCard"].ToString() != "")
                {
                    model.CallingCard = (byte[])ds.Tables[0].Rows[0]["CallingCard"];
                }
                if (ds.Tables[0].Rows[0]["Cpy_Phone"] != null && ds.Tables[0].Rows[0]["Cpy_Phone"].ToString() != "")
                {
                    model.Cpy_Phone = ds.Tables[0].Rows[0]["Cpy_Phone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Cpy_Fax"] != null && ds.Tables[0].Rows[0]["Cpy_Fax"].ToString() != "")
                {
                    model.Cpy_Fax = ds.Tables[0].Rows[0]["Cpy_Fax"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UpdateBy"] != null && ds.Tables[0].Rows[0]["UpdateBy"].ToString() != "")
                {
                    model.UpdateBy = int.Parse(ds.Tables[0].Rows[0]["UpdateBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LastUpdate"] != null && ds.Tables[0].Rows[0]["LastUpdate"].ToString() != "")
                {
                    model.LastUpdate = DateTime.Parse(ds.Tables[0].Rows[0]["LastUpdate"].ToString());
                }
                //by long 20130517
                if (ds.Tables[0].Rows[0]["InsertUserID"] != null && ds.Tables[0].Rows[0]["InsertUserID"].ToString() != "")
                {
                    model.InsertUserID = int.Parse(ds.Tables[0].Rows[0]["InsertUserID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InsertDate"] != null && ds.Tables[0].Rows[0]["InsertDate"].ToString() != "")
                {
                    model.InsertDate = DateTime.Parse(ds.Tables[0].Rows[0]["InsertDate"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select Cst_Id,Cst_No,Cst_Brief,Cst_Name,Cpy_Address,Code,Linkman,CallingCard,Cpy_Phone,Cpy_Fax,UpdateBy,LastUpdate,(Select mem_Name From tg_member Where mem_ID=InsertUserID) AS InsertUser,InsertDate ");
            strSql.Append(" FROM cm_CustomerInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where 1=1 " + strWhere);
            }
            strSql.Append(" order by Cst_ID DESC");
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            //by long 20130517
            strSql.Append(" Cst_Id,Cst_No,Cst_Brief,Cst_Name,Cpy_Address,Code,Linkman,CallingCard,Cpy_Phone,Cpy_Fax,UpdateBy,LastUpdate,(Select mem_Name From tg_member Where mem_ID=InsertUserID) AS InsertUser,InsertDate ");
            strSql.Append(" FROM cm_CustomerInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_CustomerInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            /*没有调用此方法，无需修改。Created by Sago*/
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBE() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.Cst_Id desc");
            }
            strSql.Append(")AS Row, T.*  from cm_CustomerInfo T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_CustomerInfo", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_CustomerInfo_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// Where条件语句后半段。从AND 或者 OR 开始。
        /// </summary>
        /// <param name="whereSql"></param>
        /// <returns></returns>
        public DataSet GetCustomerRecord(string whereSql)
        {
            string sql = @"SELECT 
                                               [客户编号]
                                              ,[客户简称]
                                              ,[客户名称]
                                              ,[公司地址]
                                              ,[邮编]
                                              ,[联系人]
                                              ,[公司电话]
                                              ,[公司传真]
                                              ,[客户英文名称]
                                              ,[所在国家]
                                              ,[所在省份]
                                              ,[所在城市]
                                              ,[所在行业]
                                              ,[客户类别]
                                              ,[邮箱]
                                              ,[关系部门]
                                              ,[开户银行名称]
                                              ,[税务帐号]
                                              ,[银行帐号]
                                              ,[备注]
                                              ,[是否合作]
                                              ,[分支机构]
                                              ,[公司主页]
                                              ,[关系建立时间]
                                              ,[信用级别]
                                              ,[亲密度]
                                              ,[企业代码]
                                              ,[法定代表]
                                          FROM [dbo].[View_cm_CustomerRecord] WHERE 1=1 " + whereSql;
            DataSet set = DbHelperSQL.Query(sql);
            return set;
        }

        //获取所有客户年份
        //获取所有合同年份
        public List<string> GetCustomerYear()
        {
            List<string> list = new List<string>();
            string strSql = " Select Distinct year(LastUpdate)  From cm_CustomerInfo ";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i][0].ToString());
                }
            }
            return list;
        }

        /// <summary>
        /// 得到客户导出数据
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetCustomerExportInfo(string strWhere)
        {
            string sql = @"select C.*,CX.*,(Select mem_Name From tg_member Where mem_ID=InsertUserID) AS InsertUser,
                CONVERT(nvarchar(100),C.InsertDate,23) as lrsj
				,(case when IsPartner=1 then '是' else '否' end) as PartnerName
				,(select dic_name from cm_Dictionary where ID=CX.Type) as TypeName
				,(select dic_name from cm_Dictionary where ID=CX.Profession) as ProfessionName
				,(Convert(varchar(100),CX.CreateRelationTime,23)) as RelationTime
				,(select dic_name from cm_Dictionary where ID=CX.CreditLeve) as CreditLeveName
				,(select dic_name from cm_Dictionary where ID=CX.CloseLeve) as CloseLeveName
				from cm_CustomerInfo C left join cm_CustomerExtendInfo CX on C.cst_id=CX.cst_id where 1=1 " + strWhere + " order by  c.cst_id desc";
            return DbHelperSQL.Query(sql);
        }
        /// <summary>
        /// 得到客户导出数据-甲方负责人
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetContactPersionInfoExportInfo(string strWhere)
        {
            string sql = @"select CP.*
                           from cm_ContactPersionInfo CP inner join cm_CustomerInfo C on CP.cst_id=C.cst_id where 1=1 
                           " + strWhere + " order by CP_Id desc ";
            return DbHelperSQL.Query(sql);
        }
        /// <summary>
        /// 得到客户导出数据-客户前期资料
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet Get_AttachInfoExportInfo(string strWhere)
        {
            string sql = @"select AT.*,(cast(isnull(FileSize,0) as int)/1024) as FileSizes,(select mem_name from tg_member where mem_id=UploadUser) as UploadUserName
				from cm_AttachInfo AT inner join cm_CustomerInfo C on AT.Cst_Id=C.Cst_Id  " + strWhere + " order by AT.ID desc ";
            return DbHelperSQL.Query(sql);
        }
        #endregion  Method
    }
}

