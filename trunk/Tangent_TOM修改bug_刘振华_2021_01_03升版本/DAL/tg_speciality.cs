﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:tg_speciality
	/// </summary>
	public partial class tg_speciality
	{
		public tg_speciality()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.tg_speciality model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.spe_Name != null)
			{
				strSql1.Append("spe_Name,");
				strSql2.Append("'"+model.spe_Name+"',");
			}
			if (model.spe_Intro != null)
			{
				strSql1.Append("spe_Intro,");
				strSql2.Append("'"+model.spe_Intro+"',");
			}
			strSql.Append("insert into tg_speciality(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_speciality model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update tg_speciality set ");
			if (model.spe_Name != null)
			{
				strSql.Append("spe_Name='"+model.spe_Name+"',");
			}
			if (model.spe_Intro != null)
			{
				strSql.Append("spe_Intro='"+model.spe_Intro+"',");
			}
			else
			{
				strSql.Append("spe_Intro= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where spe_ID="+ model.spe_ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int spe_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_speciality ");
			strSql.Append(" where spe_ID="+spe_ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string spe_IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_speciality ");
			strSql.Append(" where spe_ID in ("+spe_IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_speciality GetModel(int spe_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" spe_ID,spe_Name,spe_Intro ");
			strSql.Append(" from tg_speciality ");
			strSql.Append(" where spe_ID="+spe_ID+"" );
			TG.Model.tg_speciality model=new TG.Model.tg_speciality();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["spe_ID"]!=null && ds.Tables[0].Rows[0]["spe_ID"].ToString()!="")
				{
					model.spe_ID=int.Parse(ds.Tables[0].Rows[0]["spe_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["spe_Name"]!=null && ds.Tables[0].Rows[0]["spe_Name"].ToString()!="")
				{
					model.spe_Name=ds.Tables[0].Rows[0]["spe_Name"].ToString();
				}
				if(ds.Tables[0].Rows[0]["spe_Intro"]!=null && ds.Tables[0].Rows[0]["spe_Intro"].ToString()!="")
				{
					model.spe_Intro=ds.Tables[0].Rows[0]["spe_Intro"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select spe_ID,spe_Name,spe_Intro ");
			strSql.Append(" FROM tg_speciality ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" spe_ID,spe_Name,spe_Intro ");
			strSql.Append(" FROM tg_speciality ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM tg_speciality ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.spe_ID desc");
			}
			strSql.Append(")AS Row, T.*  from tg_speciality T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

