﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.DBUtility;

namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_CoperationChargeType
    /// </summary>
    public partial class cm_CoperationChargeType
    {
        public cm_CoperationChargeType()
        { }
        #region  Method


        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_CoperationChargeType");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CoperationChargeType model)
        {

            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.cpr_ID != null)
            {
                strSql1.Append("cpr_ID,");
                strSql2.Append("" + model.cpr_ID + ",");
            }
            if (model.Times != null)
            {
                strSql1.Append("Times,");
                strSql2.Append("'" + model.Times + "',");
            }
            if (model.persent != null)
            {
                strSql1.Append("persent,");
                strSql2.Append("" + model.persent + ",");
            }
            if (model.payCount != null)
            {
                strSql1.Append("payCount,");
                strSql2.Append("" + model.payCount + ",");
            }
            if (model.paytime != null)
            {
                strSql1.Append("paytime,");
                strSql2.Append("'" + model.paytime + "',");
            }
            if (model.payShiCount != null)
            {
                strSql1.Append("payShiCount,");
                strSql2.Append("" + model.payShiCount + ",");
            }
            if (model.acceptuser != null)
            {
                strSql1.Append("acceptuser,");
                strSql2.Append("'" + model.acceptuser + "',");
            }
            if (model.mark != null)
            {
                strSql1.Append("mark,");
                strSql2.Append("'" + model.mark + "',");
            }
            //合同类型
            if (model.paytype != null)
            {
                strSql1.Append("paytype,");
                strSql2.Append("'" + model.paytype + "',");
            }
            strSql.Append("insert into cm_CoperationChargeType(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }


        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CoperationChargeType model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_CoperationChargeType set ");
            if (model.cpr_ID != null)
            {
                strSql.Append("cpr_ID=" + model.cpr_ID + ",");
            }
            else
            {
                strSql.Append("cpr_ID= null ,");
            }
            if (model.Times != null)
            {
                strSql.Append("Times='" + model.Times + "',");
            }
            else
            {
                strSql.Append("Times= null ,");
            }
            if (model.persent != null)
            {
                strSql.Append("persent=" + model.persent + ",");
            }
            else
            {
                strSql.Append("persent= null ,");
            }
            if (model.payCount != null)
            {
                strSql.Append("payCount=" + model.payCount + ",");
            }
            else
            {
                strSql.Append("payCount= null ,");
            }

            if (model.paytime != null)
            {
                strSql.Append("paytime='" + model.paytime + "',");
            }
            else
            {
                strSql.Append("paytime= null ,");
            }
            if (model.payShiCount != null)
            {
                strSql.Append("payShiCount=" + model.payShiCount + ",");
            }
            else
            {
                strSql.Append("payShiCount= null ,");
            }
            if (model.acceptuser != null)
            {
                strSql.Append("acceptuser='" + model.acceptuser + "',");
            }
            else
            {
                strSql.Append("acceptuser= null ,");
            }
            if (model.mark != null)
            {
                strSql.Append("mark='" + model.mark + "',");
            }
            else
            {
                strSql.Append("mark= null ,");
            }
            if (model.paytype != null)
            {
                strSql.Append("paytype='" + model.paytype + "',");
            }
            else
            {
                strSql.Append("paytype= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where ID=" + model.ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_CoperationChargeType ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID, string type)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_CoperationChargeType ");
            strSql.Append(" where ID=@ID AND paytype=@paytype");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4),
                    new SqlParameter("@paytype", SqlDbType.NVarChar,50)
			};
            parameters[0].Value = ID;
            parameters[1].Value = type;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_CoperationChargeType ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationChargeType GetModel(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,cpr_ID,Times,persent,payCount,paytime,payShiCount,acceptuser,mark,paytype from cm_CoperationChargeType ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_CoperationChargeType model = new TG.Model.cm_CoperationChargeType();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_ID"] != null && ds.Tables[0].Rows[0]["cpr_ID"].ToString() != "")
                {
                    model.cpr_ID = decimal.Parse(ds.Tables[0].Rows[0]["cpr_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Times"] != null && ds.Tables[0].Rows[0]["Times"].ToString() != "")
                {
                    model.Times = ds.Tables[0].Rows[0]["Times"].ToString();
                }
                if (ds.Tables[0].Rows[0]["persent"] != null && ds.Tables[0].Rows[0]["persent"].ToString() != "")
                {
                    model.persent = decimal.Parse(ds.Tables[0].Rows[0]["persent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["payCount"] != null && ds.Tables[0].Rows[0]["payCount"].ToString() != "")
                {
                    model.payCount = decimal.Parse(ds.Tables[0].Rows[0]["payCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["paytime"] != null && ds.Tables[0].Rows[0]["paytime"].ToString() != "")
                {
                    model.paytime = DateTime.Parse(ds.Tables[0].Rows[0]["paytime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["payShiCount"] != null && ds.Tables[0].Rows[0]["payShiCount"].ToString() != "")
                {
                    model.payShiCount = decimal.Parse(ds.Tables[0].Rows[0]["payShiCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["acceptuser"] != null && ds.Tables[0].Rows[0]["acceptuser"].ToString() != "")
                {
                    model.acceptuser = ds.Tables[0].Rows[0]["acceptuser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mark"] != null && ds.Tables[0].Rows[0]["mark"].ToString() != "")
                {
                    model.mark = ds.Tables[0].Rows[0]["mark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["paytype"] != null && ds.Tables[0].Rows[0]["paytype"].ToString() != "")
                {
                    model.paytype = ds.Tables[0].Rows[0]["paytype"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationChargeType GetModel(int ID, string cprType)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,cpr_ID,Times,persent,payCount,paytime,payShiCount,acceptuser,mark,paytype from cm_CoperationChargeType ");
            strSql.Append(" where ID=@ID AND paytype=@paytype ");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4),
                    new SqlParameter("@paytype", SqlDbType.NVarChar,50)
			};
            parameters[0].Value = ID;
            parameters[1].Value = cprType;
            TG.Model.cm_CoperationChargeType model = new TG.Model.cm_CoperationChargeType();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_ID"] != null && ds.Tables[0].Rows[0]["cpr_ID"].ToString() != "")
                {
                    model.cpr_ID = decimal.Parse(ds.Tables[0].Rows[0]["cpr_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Times"] != null && ds.Tables[0].Rows[0]["Times"].ToString() != "")
                {
                    model.Times = ds.Tables[0].Rows[0]["Times"].ToString();
                }
                if (ds.Tables[0].Rows[0]["persent"] != null && ds.Tables[0].Rows[0]["persent"].ToString() != "")
                {
                    model.persent = decimal.Parse(ds.Tables[0].Rows[0]["persent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["payCount"] != null && ds.Tables[0].Rows[0]["payCount"].ToString() != "")
                {
                    model.payCount = decimal.Parse(ds.Tables[0].Rows[0]["payCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["paytime"] != null && ds.Tables[0].Rows[0]["paytime"].ToString() != "")
                {
                    model.paytime = DateTime.Parse(ds.Tables[0].Rows[0]["paytime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["payShiCount"] != null && ds.Tables[0].Rows[0]["payShiCount"].ToString() != "")
                {
                    model.payShiCount = decimal.Parse(ds.Tables[0].Rows[0]["payShiCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["acceptuser"] != null && ds.Tables[0].Rows[0]["acceptuser"].ToString() != "")
                {
                    model.acceptuser = ds.Tables[0].Rows[0]["acceptuser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["mark"] != null && ds.Tables[0].Rows[0]["mark"].ToString() != "")
                {
                    model.mark = ds.Tables[0].Rows[0]["mark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["paytype"] != null && ds.Tables[0].Rows[0]["paytype"].ToString() != "")
                {
                    model.paytype = ds.Tables[0].Rows[0]["paytype"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,cpr_ID,Times,persent,payCount,Convert(char(10),paytime,120) as paytime,payShiCount,acceptuser,mark,paytype ");
            strSql.Append(" FROM cm_CoperationChargeType ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where 1=1 " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,cpr_ID,Times,persent,payCount,paytime,payShiCount,acceptuser,mark,paytype ");
            strSql.Append(" FROM cm_CoperationChargeType ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_CoperationChargeType ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_CoperationChargeType T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "cm_CoperationChargeType";
            parameters[1].Value = "ID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}
