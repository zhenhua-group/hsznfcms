﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeSpeMemsPersent
	/// </summary>
	public partial class cm_KaoHeSpeMemsPersent
	{
		public cm_KaoHeSpeMemsPersent()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeSpeMemsPersent model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeSpeMemsPersent(");
			strSql.Append("ProID,ProKHID,KaoHeNameID,MemID,MemName,SpeID,SpeName,Zt,Xd,Sh,Fa,Zc,InsertUser1,InsertDate1,InsertUser2,InsertDate2,InsertUser3,InsertDate3,Stat,Stat2,Stat3)");
			strSql.Append(" values (");
			strSql.Append("@ProID,@ProKHID,@KaoHeNameID,@MemID,@MemName,@SpeID,@SpeName,@Zt,@Xd,@Sh,@Fa,@Zc,@InsertUser1,@InsertDate1,@InsertUser2,@InsertDate2,@InsertUser3,@InsertDate3,@Stat,@Stat2,@Stat3)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ProID", SqlDbType.Int,4),
					new SqlParameter("@ProKHID", SqlDbType.Int,4),
					new SqlParameter("@KaoHeNameID", SqlDbType.Int,4),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@SpeID", SqlDbType.Int,4),
					new SqlParameter("@SpeName", SqlDbType.NVarChar,50),
					new SqlParameter("@Zt", SqlDbType.Decimal,9),
					new SqlParameter("@Xd", SqlDbType.Decimal,9),
					new SqlParameter("@Sh", SqlDbType.Decimal,9),
					new SqlParameter("@Fa", SqlDbType.Decimal,9),
					new SqlParameter("@Zc", SqlDbType.Decimal,9),
					new SqlParameter("@InsertUser1", SqlDbType.Int,4),
					new SqlParameter("@InsertDate1", SqlDbType.DateTime),
					new SqlParameter("@InsertUser2", SqlDbType.Int,4),
					new SqlParameter("@InsertDate2", SqlDbType.DateTime),
					new SqlParameter("@InsertUser3", SqlDbType.Int,4),
					new SqlParameter("@InsertDate3", SqlDbType.DateTime),
					new SqlParameter("@Stat", SqlDbType.Int,4),
					new SqlParameter("@Stat2", SqlDbType.Int,4),
					new SqlParameter("@Stat3", SqlDbType.Int,4)};
			parameters[0].Value = model.ProID;
			parameters[1].Value = model.ProKHID;
			parameters[2].Value = model.KaoHeNameID;
			parameters[3].Value = model.MemID;
			parameters[4].Value = model.MemName;
			parameters[5].Value = model.SpeID;
			parameters[6].Value = model.SpeName;
			parameters[7].Value = model.Zt;
			parameters[8].Value = model.Xd;
			parameters[9].Value = model.Sh;
			parameters[10].Value = model.Fa;
			parameters[11].Value = model.Zc;
			parameters[12].Value = model.InsertUser1;
			parameters[13].Value = model.InsertDate1;
			parameters[14].Value = model.InsertUser2;
			parameters[15].Value = model.InsertDate2;
			parameters[16].Value = model.InsertUser3;
			parameters[17].Value = model.InsertDate3;
			parameters[18].Value = model.Stat;
			parameters[19].Value = model.Stat2;
			parameters[20].Value = model.Stat3;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeSpeMemsPersent model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeSpeMemsPersent set ");
			strSql.Append("ProID=@ProID,");
			strSql.Append("ProKHID=@ProKHID,");
			strSql.Append("KaoHeNameID=@KaoHeNameID,");
			strSql.Append("MemID=@MemID,");
			strSql.Append("MemName=@MemName,");
			strSql.Append("SpeID=@SpeID,");
			strSql.Append("SpeName=@SpeName,");
			strSql.Append("Zt=@Zt,");
			strSql.Append("Xd=@Xd,");
			strSql.Append("Sh=@Sh,");
			strSql.Append("Fa=@Fa,");
			strSql.Append("Zc=@Zc,");
			strSql.Append("InsertUser1=@InsertUser1,");
			strSql.Append("InsertDate1=@InsertDate1,");
			strSql.Append("InsertUser2=@InsertUser2,");
			strSql.Append("InsertDate2=@InsertDate2,");
			strSql.Append("InsertUser3=@InsertUser3,");
			strSql.Append("InsertDate3=@InsertDate3,");
			strSql.Append("Stat=@Stat,");
			strSql.Append("Stat2=@Stat2,");
			strSql.Append("Stat3=@Stat3");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ProID", SqlDbType.Int,4),
					new SqlParameter("@ProKHID", SqlDbType.Int,4),
					new SqlParameter("@KaoHeNameID", SqlDbType.Int,4),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@SpeID", SqlDbType.Int,4),
					new SqlParameter("@SpeName", SqlDbType.NVarChar,50),
					new SqlParameter("@Zt", SqlDbType.Decimal,9),
					new SqlParameter("@Xd", SqlDbType.Decimal,9),
					new SqlParameter("@Sh", SqlDbType.Decimal,9),
					new SqlParameter("@Fa", SqlDbType.Decimal,9),
					new SqlParameter("@Zc", SqlDbType.Decimal,9),
					new SqlParameter("@InsertUser1", SqlDbType.Int,4),
					new SqlParameter("@InsertDate1", SqlDbType.DateTime),
					new SqlParameter("@InsertUser2", SqlDbType.Int,4),
					new SqlParameter("@InsertDate2", SqlDbType.DateTime),
					new SqlParameter("@InsertUser3", SqlDbType.Int,4),
					new SqlParameter("@InsertDate3", SqlDbType.DateTime),
					new SqlParameter("@Stat", SqlDbType.Int,4),
					new SqlParameter("@Stat2", SqlDbType.Int,4),
					new SqlParameter("@Stat3", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.ProID;
			parameters[1].Value = model.ProKHID;
			parameters[2].Value = model.KaoHeNameID;
			parameters[3].Value = model.MemID;
			parameters[4].Value = model.MemName;
			parameters[5].Value = model.SpeID;
			parameters[6].Value = model.SpeName;
			parameters[7].Value = model.Zt;
			parameters[8].Value = model.Xd;
			parameters[9].Value = model.Sh;
			parameters[10].Value = model.Fa;
			parameters[11].Value = model.Zc;
			parameters[12].Value = model.InsertUser1;
			parameters[13].Value = model.InsertDate1;
			parameters[14].Value = model.InsertUser2;
			parameters[15].Value = model.InsertDate2;
			parameters[16].Value = model.InsertUser3;
			parameters[17].Value = model.InsertDate3;
			parameters[18].Value = model.Stat;
			parameters[19].Value = model.Stat2;
			parameters[20].Value = model.Stat3;
			parameters[21].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeSpeMemsPersent ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeSpeMemsPersent ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeSpeMemsPersent GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,ProID,ProKHID,KaoHeNameID,MemID,MemName,SpeID,SpeName,Zt,Xd,Sh,Fa,Zc,InsertUser1,InsertDate1,InsertUser2,InsertDate2,InsertUser3,InsertDate3,Stat,Stat2,Stat3 from cm_KaoHeSpeMemsPersent ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeSpeMemsPersent model=new TG.Model.cm_KaoHeSpeMemsPersent();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeSpeMemsPersent DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeSpeMemsPersent model=new TG.Model.cm_KaoHeSpeMemsPersent();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["ProID"]!=null && row["ProID"].ToString()!="")
				{
					model.ProID=int.Parse(row["ProID"].ToString());
				}
				if(row["ProKHID"]!=null && row["ProKHID"].ToString()!="")
				{
					model.ProKHID=int.Parse(row["ProKHID"].ToString());
				}
				if(row["KaoHeNameID"]!=null && row["KaoHeNameID"].ToString()!="")
				{
					model.KaoHeNameID=int.Parse(row["KaoHeNameID"].ToString());
				}
				if(row["MemID"]!=null && row["MemID"].ToString()!="")
				{
					model.MemID=int.Parse(row["MemID"].ToString());
				}
				if(row["MemName"]!=null)
				{
					model.MemName=row["MemName"].ToString();
				}
				if(row["SpeID"]!=null && row["SpeID"].ToString()!="")
				{
					model.SpeID=int.Parse(row["SpeID"].ToString());
				}
				if(row["SpeName"]!=null)
				{
					model.SpeName=row["SpeName"].ToString();
				}
				if(row["Zt"]!=null && row["Zt"].ToString()!="")
				{
					model.Zt=decimal.Parse(row["Zt"].ToString());
				}
				if(row["Xd"]!=null && row["Xd"].ToString()!="")
				{
					model.Xd=decimal.Parse(row["Xd"].ToString());
				}
				if(row["Sh"]!=null && row["Sh"].ToString()!="")
				{
					model.Sh=decimal.Parse(row["Sh"].ToString());
				}
				if(row["Fa"]!=null && row["Fa"].ToString()!="")
				{
					model.Fa=decimal.Parse(row["Fa"].ToString());
				}
				if(row["Zc"]!=null && row["Zc"].ToString()!="")
				{
					model.Zc=decimal.Parse(row["Zc"].ToString());
				}
				if(row["InsertUser1"]!=null && row["InsertUser1"].ToString()!="")
				{
					model.InsertUser1=int.Parse(row["InsertUser1"].ToString());
				}
				if(row["InsertDate1"]!=null && row["InsertDate1"].ToString()!="")
				{
					model.InsertDate1=DateTime.Parse(row["InsertDate1"].ToString());
				}
				if(row["InsertUser2"]!=null && row["InsertUser2"].ToString()!="")
				{
					model.InsertUser2=int.Parse(row["InsertUser2"].ToString());
				}
				if(row["InsertDate2"]!=null && row["InsertDate2"].ToString()!="")
				{
					model.InsertDate2=DateTime.Parse(row["InsertDate2"].ToString());
				}
				if(row["InsertUser3"]!=null && row["InsertUser3"].ToString()!="")
				{
					model.InsertUser3=int.Parse(row["InsertUser3"].ToString());
				}
				if(row["InsertDate3"]!=null && row["InsertDate3"].ToString()!="")
				{
					model.InsertDate3=DateTime.Parse(row["InsertDate3"].ToString());
				}
				if(row["Stat"]!=null && row["Stat"].ToString()!="")
				{
					model.Stat=int.Parse(row["Stat"].ToString());
				}
				if(row["Stat2"]!=null && row["Stat2"].ToString()!="")
				{
					model.Stat2=int.Parse(row["Stat2"].ToString());
				}
				if(row["Stat3"]!=null && row["Stat3"].ToString()!="")
				{
					model.Stat3=int.Parse(row["Stat3"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,ProID,ProKHID,KaoHeNameID,MemID,MemName,SpeID,SpeName,Zt,Xd,Sh,Fa,Zc,InsertUser1,InsertDate1,InsertUser2,InsertDate2,InsertUser3,InsertDate3,Stat,Stat2,Stat3 ");
			strSql.Append(" FROM cm_KaoHeSpeMemsPersent ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,ProID,ProKHID,KaoHeNameID,MemID,MemName,SpeID,SpeName,Zt,Xd,Sh,Fa,Zc,InsertUser1,InsertDate1,InsertUser2,InsertDate2,InsertUser3,InsertDate3,Stat,Stat2,Stat3 ");
			strSql.Append(" FROM cm_KaoHeSpeMemsPersent ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeSpeMemsPersent ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeSpeMemsPersent T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeSpeMemsPersent";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

