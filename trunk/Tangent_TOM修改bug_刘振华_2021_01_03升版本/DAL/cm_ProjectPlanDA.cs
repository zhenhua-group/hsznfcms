﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using TG.DBUtility;
using TG.Model;
using System.Data.SqlClient;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class cm_ProjectPlanDA
    {
        //为策划项目
        public List<ProjectPlanListViewEntityNo> GetProjectPlanListNo(string whereSql)
        {
            string sql = string.Format(@"
                                                    select 
                                                    p.pro_id as ProjectSysNo
                                                    ,max(p.pro_name) as ProjectName
                                                    ,isnull(MAX(pa.SysNo),0) as ProjectPlanAuditSysNo
                                                    ,max(pa.Status) as ProjectPlanAuditStatus
                                                    ,max(pn.ProNumber) as ProjectJobNumber
                                                    ,max(pn.SubmitDate) as SubmitDate
                                                    ,case max(p.CoperationSysNo) when 0 then N'无' else N'有' end as IsBackup
                                                    ,max(c.cpr_Name) as CoperationName
                                                    ,max(rt.pro_ID) as IsHave,
                                                    max(p.PMName) as PMName,
                                                    max(p.PMUserID) as PMUserID
                                                    from cm_Project p 
                                                    left join cm_ProjectNumber pn on pn.Pro_id = p.pro_id 
                                                    left join cm_Coperation c on c.cpr_Id = p.CoperationSysNo
                                                    left join tg_relation rt on rt.pro_ID = p.ReferenceSysNo
                                                    left join cm_ProjectPlanAudit pa on pa.ProjectSysNo = p.pro_id
                                                    where 1=1 and isnull(pn.ProNumber,N'0') <>N'0' {0}
                                                    group by p.pro_id having isnull(MAX(pa.SysNo),0)=0  and isnull(max(rt.pro_ID),0)=0 order by p.pro_ID  ", whereSql);
            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectPlanListViewEntityNo> resultList = EntityBuilder<ProjectPlanListViewEntityNo>.BuilderEntityList(reader);

            return resultList;
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_ProjectPlanNo_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        //未策划项目信息列表分页带查询条件
        public List<ProjectPlanListViewEntityNo> GetProjectPlanListNo(string query, int startIndex, int endIndex)
        {
            SqlDataReader reader = DBUtility.DbHelperSQL.RunProcedure("P_cm_ProjectPlanNo", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });

            List<ProjectPlanListViewEntityNo> resultList = EntityBuilder<ProjectPlanListViewEntityNo>.BuilderEntityList(reader);

            return resultList;
        }

        //策划通过项目列表
        public List<ProjectPlanListViewEntityYes> GetProjectPlanListYes(string whereSql)
        {
            string sql = string.Format(@"
                                                    select distinct p.pro_ID as ProjectSysNo, 
                                                    p.pro_name as ProjectName,
                                                    case p.CoperationSysNo when 0 then N'无' else N'有' end as IsBackup,
                                                    c.cpr_Name as CoperationName,
                                                    pn.SubmitDate as SubmitDate,
                                                    pn.ProNumber as ProjectJobNumber,
                                                    isnull(ppa.SysNo,0) as ProjectPlanAuditSysNo ,
                                                    ppa.Status as ProjectPlanAuditStatus,
                                                    rt.pro_ID as IsHave,
                                                    p.PMName as PMName,
                                                    p.PMUserID as PMUserID,
                                                    isnull(u.mem_Name,'') as ApplyAuditUserName
                                                    from cm_Project p join cm_ProjectNumber pn on pn.Pro_id = p.pro_ID 
                                                    left join cm_Coperation c on c.cpr_Id = p.CoperationSysNo 
                                                    left join tg_relation rt on rt.pro_ID = p.ReferenceSysNo
                                                    left join cm_ProjectPlanAudit ppa on ppa.ProjectSysNo = p.pro_ID 
                                                    left join tg_member u on u.mem_ID = ppa.InUser
                                                    where 1=1  and isnull(pn.ProNumber,N'0') <> N'0' and isnull(rt.pro_ID ,0)<>0 {0} order by p.pro_ID desc", whereSql);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectPlanListViewEntityYes> resultList = EntityBuilder<ProjectPlanListViewEntityYes>.BuilderEntityList(reader);

            return resultList;
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcYesCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_ProjectPlanYes_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 存储过程消息列表
        /// </summary>
        public List<ProjectPlanListViewEntityYes> GetProjectPlanListYes(string query, int startIndex, int endIndex)
        {
            SqlDataReader reader = DbHelperSQL.RunProcedure("P_cm_ProjectPlanYes", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });

            List<ProjectPlanListViewEntityYes> resultList = EntityBuilder<ProjectPlanListViewEntityYes>.BuilderEntityList(reader);

            return resultList;
        }

        public List<TreeDepartmentEntity> GetTreeDepartmentList()
        {
            string sql = "select unit.[unit_ID] as DepartmentSysNo,unit.[unit_Name] as DepartmentName from [tg_unit] unit left join tg_unitExt ext on unit.unit_ID=ext.unit_ID Where unit_ParentID<>0 order by ext.unit_Order";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<TreeDepartmentEntity> treeDepartmentList = EntityBuilder<TreeDepartmentEntity>.BuilderEntityList(reader);

            return treeDepartmentList;
        }

        public List<TreeUserEntity> GetTreeUserListByDepartmentSysNo(int departmentSysNo)
        {
            string sql = "select u.mem_ID as UserSysNo,u.mem_Sex as UserSex,u.mem_Name as UserName, u.mem_Speciality_ID as SpecialtyNo,s.spe_Name as SpecialtyName from tg_member u join tg_speciality s on s.spe_ID =u.mem_Speciality_ID  where mem_Unit_ID =" + departmentSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<TreeUserEntity> treeUserList = EntityBuilder<TreeUserEntity>.BuilderEntityList(reader);

            return treeUserList;
        }

        public int InsertPlanUser(PlanProjectViewEntity dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //查询原始数据 cm_project
                TG.Model.cm_Project cmProject = new TG.DAL.cm_Project().GetModel(dataEntity.ProjectSysNo);
                if (cmProject != null)
                {
                    dataEntity.ProjectSysNo = cmProject.ReferenceSysNo;
                }
                //删除原用记录
                command.CommandText = "delete tg_relation where pro_ID =" + dataEntity.ProjectSysNo + ";delete cm_ProjectPlanSubItem where ProjectSysNo =" + dataEntity.ProjectSysNo;

                command.ExecuteNonQuery();

                //新规用户
                dataEntity.Member.ForEach((user) =>
                {
                    command.CommandText = "insert into [tg_relation] (pro_ID,mem_ID,mem_isManage,mem_isSpecLead,mem_RoleIDs,mem_isAssistant,mem_isAssessor ,mem_isCorrector,mem_isValidator,mem_isDirector,mem_bCanViewSpec,mem_bCanViewAllSpec,mem_isDesigner) values(" + dataEntity.ProjectSysNo + "," + user.UserSysNo + ",0,0,N'" + user.RoleSysNo + "',0,0,0,0,0,1,0," + user.IsDesigner + ");";

                    command.ExecuteNonQuery();
                });

                //新规子项
                dataEntity.Items.ForEach((item) =>
                {
                    command.CommandText = "insert into cm_ProjectPlanSubItem (ProjectSysNo,DesignLevel,StartDate,EndDate,BuildingCount,StructCount,ElectricCount,HVACCount,DrainCount,MunicipalAdministrationCount)values(" + dataEntity.ProjectSysNo + ",N'" + item.DesignLevel + "',N'" + item.StartDate + "',N'" + item.EndDate + "'," + item.BuildingCount + "," + item.StructCount + "," + item.ElectricCount + "," + item.HVACCount + "," + item.DrainCount + "," + item.MunicipalAdministrationCount + ")";
                    command.ExecuteNonQuery();
                });

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        public DataSet GetPlanUsers(int projectSysNo)
        {
            string sql = "select u.mem_ID as UserSysNo,u.mem_Sex as UserSex,u.mem_Name as UserName,s.spe_Name as SpecialtyName,un.unit_Name as DepartmentName  from tg_relation r join tg_member u on u.mem_ID = r.mem_ID join tg_unit un on un.unit_ID = u.mem_Unit_ID join tg_speciality s on s.spe_ID = u.mem_Speciality_ID where pro_ID = " + projectSysNo;

            return DbHelperSQL.Query(sql);
        }
        //获取通过审批的项目
        public DataSet GetPassAuditPro(string strWhere)
        {
            string strSql = "select DISTINCT Datename(year,InDate) AS InDate from cm_ProjectPlanAudit where status='H'" + strWhere;
            return DbHelperSQL.Query(strSql);
        }


        public List<ProjectPlanRole> GetProjectPlanRole()
        {
            string sql = "select ID,RoleName from tg_ProRole";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectPlanRole> result = EntityBuilder<ProjectPlanRole>.BuilderEntityList(reader);

            return result;
        }

        public List<ProjectPlanUser> GetProjectPlanUsers(int projectSysNo, int projectPlanRoleSysNo)
        {
            string sql = "select DISTINCT r.mem_ID,u.mem_Name as userName,r.mem_RoleIDs,s.spe_Name as SpecialtyName from tg_relation r join tg_member u on u.mem_ID = r.mem_ID join tg_speciality  s on s.spe_ID = u.mem_Speciality_ID where r.pro_ID = " + projectSysNo + " and mem_RoleIDs like N'%" + projectPlanRoleSysNo + "%'";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectPlanUser> resultList = EntityBuilder<ProjectPlanUser>.BuilderEntityList(reader);

            return resultList;
        }

        //获取项目调度设计人员
        public List<ProjectPlanUser> GetProjectPlanUsers(int projectSysNo)
        {
            string sql = "select DISTINCT r.mem_ID,u.mem_Name as userName,r.mem_RoleIDs,s.spe_Name as SpecialtyName from tg_relation r join tg_member u on u.mem_ID = r.mem_ID join tg_speciality  s on s.spe_ID = u.mem_Speciality_ID where r.pro_ID = " + projectSysNo + " and mem_IsDesigner=1 ";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectPlanUser> resultList = EntityBuilder<ProjectPlanUser>.BuilderEntityList(reader);

            return resultList;
        }

        public cm_ProjectPlanAuditConfigEntity GetProjectPlanAuditConfigEntity(int position)
        {
            string sql = "select SysNo,ProcessDescription,RoleSysNo,Position,InUser,InDate from cm_ProjectPlanAuditConfig where Position = " + position;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            cm_ProjectPlanAuditConfigEntity resultEntity = EntityBuilder<cm_ProjectPlanAuditConfigEntity>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }

        public List<string> GetAuditProcessDescription()
        {
            string sql = "select r.RoleName from cm_ProjectPlanAuditConfig  ppc join cm_Role r on  r.SysNo = ppc.RoleSysNo order by ppc.position asc";

            List<string> resultString = new List<string>();

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    resultString.Add(reader["RoleName"].ToString());
                }
                reader.Close();
            }

            return resultString;
        }

        public List<ProjectPlanSubItem> GetProjectPlanSubitemList(int projectSysNo)
        {
            string sql = "select SysNo,ProjectSysNo,DesignLevel,StartDate,EndDate,BuildingCount,StructCount,ElectricCount,HVACCount,DrainCount,MunicipalAdministrationCount from cm_ProjectPlanSubItem where ProjectSysNo=" + projectSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectPlanSubItem> resultList = EntityBuilder<ProjectPlanSubItem>.BuilderEntityList(reader);

            return resultList;
        }

        public int DeleteProjectPlanSubItem(int subItemSysNo)
        {
            string sql = "delete tg_subproject where pro_ID=" + subItemSysNo;
            return DbHelperSQL.ExecuteSql(sql);
        }

        public int GetProcessRoleSysNo(string auditStatus)
        {
            int position = 0;
            switch (auditStatus)
            {
                case "A":
                    position = 1;
                    break;
                case "B":
                    position = 2;
                    break;
                case "D":
                    position = 3;
                    break;
                case "F":
                    position = 4;
                    break;
            }
            string sql = "select RoleSysNo from cm_ProjectPlanAuditConfig WHERE Position=" + position;

            object obj = DbHelperSQL.GetSingle(sql);
            int roleSysNo = obj != null ? (int)obj : 0;
            return roleSysNo;
        }
        public int GetProjectPlanAuditMsgCount()
        {
            string sql = "select COUNT(*) from [cm_ProjectPlanAudit] pa join cm_SysMsg sm on sm.ReferenceSysNo = pa.SysNo";

            object objResult = DbHelperSQL.GetSingle(sql);

            return objResult == null ? 0 : Convert.ToInt32(objResult);
        }

        /// <summary>
        /// 新建一条策划审批
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertProjectPlanAudit(cm_ProjectPlanAuditEntity dataEntity)
        {
            string sql = "insert into cm_ProjectPlanAudit(ProjectSysNo,Status,InDate,InUser) values(" + dataEntity.ProjectSysNo + ",N'A',N'" + DateTime.Now + "'," + dataEntity.InUser + ");select @@IDENTITY";

            int sysNo = Convert.ToInt32(DbHelperSQL.GetSingle(sql));

            return sysNo;
        }

        public cm_ProjectPlanAuditEntity GetProjectPlanAuditEntity(cm_ProjectPlanAuditQueryEntity queryEntity)
        {
            string whereSql = " where 1=1 ";
            whereSql += " and SysNo =" + queryEntity.ProjectPlanAuditSysNo;

            if (queryEntity.ProjectSysNo != 0)
            {
                whereSql += " and ProjectSysNo=" + queryEntity.ProjectSysNo;
            }
            string sql = "select SysNo,ProjectSysNo,Status,Suggestion,AuditUser,AuditDate,InDate,InUser from cm_ProjectPlanAudit " + whereSql;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();
            cm_ProjectPlanAuditEntity resultEntity = EntityBuilder<cm_ProjectPlanAuditEntity>.BuilderEntity(reader);
            reader.Close();

            return resultEntity;
        }

        public int UpdateProjectPlanAudit(cm_ProjectPlanAuditEntity dataEntity)
        {
            string ChildrenSql = "";

            if (!string.IsNullOrEmpty(dataEntity.Suggestion))
            {

                ChildrenSql += " Suggestion = N'" + dataEntity.Suggestion + "|',";
            }
            if (!string.IsNullOrEmpty(dataEntity.AuditUser))
            {
                ChildrenSql += " AuditUser = N'" + dataEntity.AuditUser + ",',";
            }
            if (!string.IsNullOrEmpty(dataEntity.AuditDate))
            {
                ChildrenSql += " AuditDate = N'" + dataEntity.AuditDate + ",',";
            }
            if (!string.IsNullOrEmpty(dataEntity.Status))
            {
                ChildrenSql += " Status=N'" + dataEntity.Status + "',";
            }
            if (!string.IsNullOrEmpty(ChildrenSql))
            {
                string finalSql = "UPDATE [cm_ProjectPlanAudit] SET " + ChildrenSql.Substring(0, ChildrenSql.Length - 1) + " WHERE SysNo=" + dataEntity.SysNo;
                return DbHelperSQL.ExecuteSql(finalSql);
            }
            else
            {
                return 0;
            }
        }

        public bool IsExistsProjectPlanAudit(int projectSysNo)
        {
            string sql = "select top 1 1 from cm_ProjectPlanAudit where ProjectSysNo = " + projectSysNo + " and Status not in(N'C',N'E',N'G',N'I',N'R') ";

            object objExists = DbHelperSQL.GetSingle(sql);
            if (objExists == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 新建一条策划修改审批
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertProjectPlanEdit(cm_ProjectPlanEditRecordEntity dataEntity)
        {
            string sql = "insert into cm_ProjectPlanEditRecord(ProjectSysNo,optionsedit,Status,InDate,InUser) values(" + dataEntity.ProjectSysNo + ",'" + dataEntity.optionsedit + "',N'A',N'" + DateTime.Now + "'," + dataEntity.InUser + ");select @@IDENTITY";

            int sysNo = Convert.ToInt32(DbHelperSQL.GetSingle(sql));

            return sysNo;
        }
        //获取项目调度审批实体--修改时
        public cm_ProjectPlanEditRecordEntity GetProjectPlanEditRecordEntity(cm_ProjectPlanEditRecordEntityQueryEntity queryEntity)
        {
            string whereSql = " where 1=1 ";
            whereSql += " and SysNo =" + queryEntity.ProjectPlanEditRecordSysNo;

            if (queryEntity.ProjectSysNo != 0)
            {
                whereSql += " and ProjectSysNo=" + queryEntity.ProjectSysNo;
            }
            string sql = "select SysNo,ProjectSysNo,optionsedit,Status,Suggestion,AuditUser,AuditDate,InDate,InUser from cm_ProjectPlanEditRecord " + whereSql;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();
            cm_ProjectPlanEditRecordEntity resultEntity = EntityBuilder<cm_ProjectPlanEditRecordEntity>.BuilderEntity(reader);
            reader.Close();

            return resultEntity;
        }
        //更新项目调度审批实体---修改时
        public int UpdateProjectPlanEditRecord(cm_ProjectPlanEditRecordEntity dataEntity)
        {
            string ChildrenSql = "";

            if (!string.IsNullOrEmpty(dataEntity.Suggestion))
            {

                ChildrenSql += " Suggestion = N'" + dataEntity.Suggestion + "|',";
            }
            if (!string.IsNullOrEmpty(dataEntity.AuditUser))
            {
                ChildrenSql += " AuditUser = N'" + dataEntity.AuditUser + ",',";
            }
            if (!string.IsNullOrEmpty(dataEntity.AuditDate))
            {
                ChildrenSql += " AuditDate = N'" + dataEntity.AuditDate + ",',";
            }
            if (!string.IsNullOrEmpty(dataEntity.Status))
            {
                ChildrenSql += " Status=N'" + dataEntity.Status + "',";
            }
            if (!string.IsNullOrEmpty(ChildrenSql))
            {
                string finalSql = "UPDATE [cm_ProjectPlanEditRecord] SET " + ChildrenSql.Substring(0, ChildrenSql.Length - 1) + " WHERE SysNo=" + dataEntity.SysNo;
                return DbHelperSQL.ExecuteSql(finalSql);
            }
            else
            {
                return 0;
            }
        }
        //是否发起项目调度审批---修改时
        public bool IsExistsProjectPlanEditRecord(int projectSysNo)
        {
            string sql = "select top 1 1 from cm_ProjectPlanEditRecord where ProjectSysNo = " + projectSysNo + " and Status not in(N'C',N'E',N'G',N'I',N'R',N'F')";

            object objExists = DbHelperSQL.GetSingle(sql);

            if (objExists == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public ProjectPlanViewParameterEntity GetProjectPlanViewParameterEntity(int projectSysNo)
        {
            string sql = "select c.cpr_Name as CoperationName,pn.ProNumber as JobNumber from cm_Project p left join cm_Coperation c on c.cpr_Id = p.CoperationSysNo left join cm_ProjectNumber pn on pn.Pro_id = p.pro_ID where p.pro_ID=" + projectSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            ProjectPlanViewParameterEntity resultEntity = EntityBuilder<ProjectPlanViewParameterEntity>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }

        public string IsTruePack(int projectSysNo, int UserSysNo)
        {
            string sql = @"
SELECT pac.user_ID FROM 
cm_Project cmpro LEFT JOIN 
tg_project pro ON cmpro.ReferenceSysNo=pro.pro_ID LEFT JOIN	tg_subproject  sub
ON pro.pro_ID=sub.pro_parentID LEFT JOIN tg_package pac ON pac.subpro_ID=sub.pro_ID 
WHERE pac.user_ID={0} AND cmpro.pro_ID={1} GROUP BY pac.user_ID
";
            sql = string.Format(sql, UserSysNo, projectSysNo);
            return TG.DBUtility.DbHelperSQL.GetSingle(sql).ToString();
        }
    }
}
