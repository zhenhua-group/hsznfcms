﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using TG.DBUtility;

namespace TG.DAL
{
    public class cm_Qualification
    {
        /// <summary>
        /// 得到所对应的表attainfo的id。
        /// </summary>
        /// <param name="quaid"></param>
        /// <returns></returns>
        public string GetattInfoid(string quaid)
        {
            string sql = "select  cm_AttaInfoId from cm_qualification where qua_Id=@qua_Id";
            SqlParameter[] parameters = {
					new SqlParameter("@qua_Id", SqlDbType.Int,4)
			};
            parameters[0].Value = quaid;
            return DbHelperSQL.GetSingle(sql, parameters).ToString();
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_Qualification model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_qualification(");
            strSql.Append("cst_Id,qua_Name,qua_ZzTime,qua_Content,qua_ImageSrc,qua_SpTime,cm_AttaInfoId,qua_jb,qua_yeartime)");
            strSql.Append(" values (");
            strSql.Append("@cst_Id,@qua_Name,@qua_ZzTime,@qua_Content,@qua_ImageSrc,@qua_SpTime,@cm_AttaInfoId,@qua_jb,@qua_yeartime)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@cst_Id", SqlDbType.Int,4),
					new SqlParameter("@qua_Name", SqlDbType.NVarChar,50),
					new SqlParameter("@qua_ZzTime", SqlDbType.DateTime),
					new SqlParameter("@qua_Content", SqlDbType.NVarChar,500),
					new SqlParameter("@qua_ImageSrc", SqlDbType.NVarChar,500),
					new SqlParameter("@qua_SpTime", SqlDbType.DateTime),
                    new SqlParameter("@cm_AttaInfoId", SqlDbType.Int,4),
                    new SqlParameter("@qua_jb", SqlDbType.NVarChar,50),
					new SqlParameter("@qua_yeartime", SqlDbType.DateTime)
                                        };
            parameters[0].Value = model.cst_Id;
            parameters[1].Value = model.qua_Name;
            parameters[2].Value = model.qua_ZzTime;
            parameters[3].Value = model.qua_Content;
            parameters[4].Value = model.qua_ImageSrc;
            parameters[5].Value = model.qua_SpTime;
            parameters[6].Value = model.cm_AttaInfoId;
            parameters[7].Value = model.qua_jb;
            parameters[8].Value = model.qua_yeartime;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_Qualification model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_qualification set ");
            strSql.Append("cst_Id=@cst_Id,");
            strSql.Append("qua_Name=@qua_Name,");
            strSql.Append("qua_ZzTime=@qua_ZzTime,");
            strSql.Append("qua_Content=@qua_Content,");
            strSql.Append("qua_ImageSrc=@qua_ImageSrc,");
            strSql.Append("qua_SpTime=@qua_SpTime,");
            strSql.Append("cm_AttaInfoId=@cm_AttaInfoId,");
            strSql.Append("qua_jb=@qua_jb,");
            strSql.Append("qua_yeartime=@qua_yeartime");
            strSql.Append(" where qua_Id=@qua_Id");
            SqlParameter[] parameters = {
					new SqlParameter("@cst_Id", SqlDbType.Int,4),
					new SqlParameter("@qua_Name", SqlDbType.NVarChar,50),
					new SqlParameter("@qua_ZzTime", SqlDbType.DateTime),
					new SqlParameter("@qua_Content", SqlDbType.NVarChar,500),
					new SqlParameter("@qua_ImageSrc", SqlDbType.NVarChar,500),
					new SqlParameter("@qua_SpTime", SqlDbType.DateTime),
                    new SqlParameter("@cm_AttaInfoId", SqlDbType.Int,4),
                    new SqlParameter("@qua_jb", SqlDbType.NVarChar,50),
					new SqlParameter("@qua_yeartime", SqlDbType.DateTime),
					new SqlParameter("@qua_Id", SqlDbType.Int,4)};
            parameters[0].Value = model.cst_Id;
            parameters[1].Value = model.qua_Name;
            parameters[2].Value = model.qua_ZzTime;
            parameters[3].Value = model.qua_Content;
            parameters[4].Value = model.qua_ImageSrc;
            parameters[5].Value = model.qua_SpTime;
            parameters[6].Value = model.cm_AttaInfoId;
            parameters[7].Value = model.qua_jb;
            parameters[8].Value = model.qua_yeartime;
            parameters[9].Value = model.qua_Id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string cpr_Idlist)
        {
            int num1 = 0;
            string[] textArray1 = cpr_Idlist.Split(new char[] { ',' });
            string[] textArray2 = textArray1;
            for (int num2 = 0; num2 < textArray2.Length; num2++)
            {
                string text1 = textArray2[num2];
                if (!(string.IsNullOrEmpty(text1.Trim()) || (text1.Trim() == ",")))
                {
                    num1 = DbHelperSQL.ExecuteSql("delete cm_qualification where qua_Id=" + text1);
                }
            }
            if (num1 > 0)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string qua_Idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_qualification ");
            strSql.Append(" where qua_Id in (" + qua_Idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_Qualification GetModel(int qua_Id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 qua_Id,cst_Id,cm_AttaInfoId,qua_Name,qua_ZzTime,qua_Content,qua_ImageSrc,qua_SpTime,qua_jb,qua_yeartime from cm_qualification ");
            strSql.Append(" where qua_Id=@qua_Id");
            SqlParameter[] parameters = {
					new SqlParameter("@qua_Id", SqlDbType.Int,4)
			};
            parameters[0].Value = qua_Id;

            TG.Model.cm_Qualification model = new TG.Model.cm_Qualification();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["qua_Id"] != null && ds.Tables[0].Rows[0]["qua_Id"].ToString() != "")
                {
                    model.qua_Id = int.Parse(ds.Tables[0].Rows[0]["qua_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cst_Id"] != null && ds.Tables[0].Rows[0]["cst_Id"].ToString() != "")
                {
                    model.cst_Id = int.Parse(ds.Tables[0].Rows[0]["cst_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cm_AttaInfoId"] != null && ds.Tables[0].Rows[0]["cm_AttaInfoId"].ToString() != "")
                {
                    model.cm_AttaInfoId = int.Parse(ds.Tables[0].Rows[0]["cm_AttaInfoId"].ToString());
                }
                if (ds.Tables[0].Rows[0]["qua_Name"] != null && ds.Tables[0].Rows[0]["qua_Name"].ToString() != "")
                {
                    model.qua_Name = ds.Tables[0].Rows[0]["qua_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["qua_ZzTime"] != null && ds.Tables[0].Rows[0]["qua_ZzTime"].ToString() != "")
                {
                    model.qua_ZzTime = DateTime.Parse(ds.Tables[0].Rows[0]["qua_ZzTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["qua_Content"] != null && ds.Tables[0].Rows[0]["qua_Content"].ToString() != "")
                {
                    model.qua_Content = ds.Tables[0].Rows[0]["qua_Content"].ToString();
                }
                if (ds.Tables[0].Rows[0]["qua_ImageSrc"] != null && ds.Tables[0].Rows[0]["qua_ImageSrc"].ToString() != "")
                {
                    model.qua_ImageSrc = ds.Tables[0].Rows[0]["qua_ImageSrc"].ToString();
                }
                if (ds.Tables[0].Rows[0]["qua_SpTime"] != null && ds.Tables[0].Rows[0]["qua_SpTime"].ToString() != "")
                {
                    model.qua_SpTime = DateTime.Parse(ds.Tables[0].Rows[0]["qua_SpTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["qua_jb"] != null && ds.Tables[0].Rows[0]["qua_jb"].ToString() != "")
                {
                    model.qua_jb = ds.Tables[0].Rows[0]["qua_jb"].ToString();
                }
                if (ds.Tables[0].Rows[0]["qua_yeartime"] != null && ds.Tables[0].Rows[0]["qua_yeartime"].ToString() != "")
                {
                    model.qua_yeartime = DateTime.Parse(ds.Tables[0].Rows[0]["qua_yeartime"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select qua_Id,cst_Id,cm_AttaInfoId,qua_Name,qua_ZzTime,qua_Content,qua_ImageSrc,qua_SpTime,qua_jb,CONVERT(nvarchar(10),qua_yeartime,120) as qua_yeartime ");
            strSql.Append(" FROM cm_qualification ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where 1=1 " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 获得所有的数据
        /// </summary>
        /// <returns></returns>
        public DataSet GetList()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select qua_Id,cst_Id,cm_AttaInfoId,qua_Name,qua_ZzTime,qua_Content,qua_ImageSrc,qua_SpTime,qua_jb,CONVERT(nvarchar(10),qua_yeartime,120) as qua_yeartime ");
            strSql.Append(" FROM cm_qualification ");
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" qua_Id,cm_AttaInfoId,cst_Id,qua_Name,qua_ZzTime,qua_Content,qua_ImageSrc,qua_SpTime,qua_jb,CONVERT(nvarchar(10),qua_yeartime,120) as qua_yeartime ");
            strSql.Append(" FROM cm_qualification ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_qualification ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.qua_Id desc");
            }
            strSql.Append(")AS Row, T.*  from cm_qualification T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

    }
}
