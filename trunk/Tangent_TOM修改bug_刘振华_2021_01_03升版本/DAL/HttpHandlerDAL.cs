﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace TG.DAL
{
    public class HttpHandlerDAL
    {
        public HttpHandlerDAL()
        { }
        /// <summary>
        /// 返回执行的数据集
        /// </summary>
        /// <param name="procName"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public static DataSet ExecuteDataset(string procName, SqlParameter[] sqlParams)
        {
            return TG.Comm.DBUtility.SqlHelperComm.ExecuteDataset(procName, sqlParams);
        }
    }
}
