﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_SubCoperationType 
	/// </summary>
	public partial class cm_SubCoperationType
	{
		public cm_SubCoperationType()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("ID", "cm_SubCoperationType"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_SubCoperationType");
			strSql.Append(" where ID="+ID+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_SubCoperationType model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.cpr_Id != null)
			{
				strSql1.Append("cpr_Id,");
				strSql2.Append(""+model.cpr_Id+",");
			}
			if (model.cpr_No != null)
			{
				strSql1.Append("cpr_No,");
				strSql2.Append("'"+model.cpr_No+"',");
			}
			if (model.Item_Name != null)
			{
				strSql1.Append("Item_Name,");
				strSql2.Append("'"+model.Item_Name+"',");
			}
			if (model.Item_Area != null)
			{
				strSql1.Append("Item_Area,");
				strSql2.Append(""+model.Item_Area+",");
			}
			if (model.UpdateBy != null)
			{
				strSql1.Append("UpdateBy,");
				strSql2.Append("'"+model.UpdateBy+"',");
			}
			if (model.LastUpdate != null)
			{
				strSql1.Append("LastUpdate,");
				strSql2.Append("'"+model.LastUpdate+"',");
			}
			if (model.Money != null)
			{
				strSql1.Append("Money,");
				strSql2.Append(""+model.Money+",");
			}
			if (model.Remark != null)
			{
				strSql1.Append("Remark,");
				strSql2.Append("'"+model.Remark+"',");
			}
			if (model.SubType != null)
			{
				strSql1.Append("SubType,");
				strSql2.Append("'"+model.SubType+"',");
			}
			strSql.Append("insert into cm_SubCoperationType(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_SubCoperationType model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_SubCoperationType set ");
			if (model.cpr_Id != null)
			{
				strSql.Append("cpr_Id="+model.cpr_Id+",");
			}
			else
			{
				strSql.Append("cpr_Id= null ,");
			}
			if (model.cpr_No != null)
			{
				strSql.Append("cpr_No='"+model.cpr_No+"',");
			}
			else
			{
				strSql.Append("cpr_No= null ,");
			}
			if (model.Item_Name != null)
			{
				strSql.Append("Item_Name='"+model.Item_Name+"',");
			}
			else
			{
				strSql.Append("Item_Name= null ,");
			}
			if (model.Item_Area != null)
			{
				strSql.Append("Item_Area="+model.Item_Area+",");
			}
			else
			{
				strSql.Append("Item_Area= null ,");
			}
			if (model.UpdateBy != null)
			{
				strSql.Append("UpdateBy='"+model.UpdateBy+"',");
			}
			else
			{
				strSql.Append("UpdateBy= null ,");
			}
			if (model.LastUpdate != null)
			{
				strSql.Append("LastUpdate='"+model.LastUpdate+"',");
			}
			else
			{
				strSql.Append("LastUpdate= null ,");
			}
			if (model.Money != null)
			{
				strSql.Append("Money="+model.Money+",");
			}
			else
			{
				strSql.Append("Money= null ,");
			}
			if (model.Remark != null)
			{
				strSql.Append("Remark='"+model.Remark+"',");
			}
			else
			{
				strSql.Append("Remark= null ,");
			}
			if (model.SubType != null)
			{
				strSql.Append("SubType='"+model.SubType+"',");
			}
			else
			{
				strSql.Append("SubType= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ID="+ model.ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_SubCoperationType ");
			strSql.Append(" where ID="+ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_SubCoperationType ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_SubCoperationType GetModel(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" ID,cpr_Id,cpr_No,Item_Name,Item_Area,UpdateBy,LastUpdate,Money,Remark,SubType ");
			strSql.Append(" from cm_SubCoperationType ");
			strSql.Append(" where ID="+ID+"" );
			TG.Model.cm_SubCoperationType model=new TG.Model.cm_SubCoperationType();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["ID"]!=null && ds.Tables[0].Rows[0]["ID"].ToString()!="")
				{
					model.ID=int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["cpr_Id"]!=null && ds.Tables[0].Rows[0]["cpr_Id"].ToString()!="")
				{
					model.cpr_Id=int.Parse(ds.Tables[0].Rows[0]["cpr_Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["cpr_No"]!=null && ds.Tables[0].Rows[0]["cpr_No"].ToString()!="")
				{
					model.cpr_No=ds.Tables[0].Rows[0]["cpr_No"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Item_Name"]!=null && ds.Tables[0].Rows[0]["Item_Name"].ToString()!="")
				{
					model.Item_Name=ds.Tables[0].Rows[0]["Item_Name"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Item_Area"]!=null && ds.Tables[0].Rows[0]["Item_Area"].ToString()!="")
				{
					model.Item_Area=decimal.Parse(ds.Tables[0].Rows[0]["Item_Area"].ToString());
				}
				if(ds.Tables[0].Rows[0]["UpdateBy"]!=null && ds.Tables[0].Rows[0]["UpdateBy"].ToString()!="")
				{
					model.UpdateBy=ds.Tables[0].Rows[0]["UpdateBy"].ToString();
				}
				if(ds.Tables[0].Rows[0]["LastUpdate"]!=null && ds.Tables[0].Rows[0]["LastUpdate"].ToString()!="")
				{
					model.LastUpdate=DateTime.Parse(ds.Tables[0].Rows[0]["LastUpdate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Money"]!=null && ds.Tables[0].Rows[0]["Money"].ToString()!="")
				{
					model.Money=decimal.Parse(ds.Tables[0].Rows[0]["Money"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Remark"]!=null && ds.Tables[0].Rows[0]["Remark"].ToString()!="")
				{
					model.Remark=ds.Tables[0].Rows[0]["Remark"].ToString();
				}
				if(ds.Tables[0].Rows[0]["SubType"]!=null && ds.Tables[0].Rows[0]["SubType"].ToString()!="")
				{
					model.SubType=ds.Tables[0].Rows[0]["SubType"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,cpr_Id,cpr_No,Item_Name,Item_Area,UpdateBy,LastUpdate,Money,Remark,SubType ");
			strSql.Append(" FROM cm_SubCoperationType ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,cpr_Id,cpr_No,Item_Name,Item_Area,UpdateBy,LastUpdate,Money,Remark,SubType ");
			strSql.Append(" FROM cm_SubCoperationType ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_SubCoperationType ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_SubCoperationType T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

