﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.DBUtility;

namespace TG.DAL
{
    public class cm_SubConstruCoperartion
    {
        public cm_SubConstruCoperartion()
        { }
        #region  Method


        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_SubConstruCoperartion");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(Model.cm_SubConstruCoperartion model)
        {
            //StringBuilder strSql = new StringBuilder();
            //StringBuilder strSql1 = new StringBuilder();
            //StringBuilder strSql2 = new StringBuilder();
            //if (model.cpr_Id != null)
            //{
            //    strSql1.Append("cpr_Id,");
            //    strSql2.Append("" + model.cpr_Id + ",");
            //}
            //if (model.cpr_No != null)
            //{
            //    strSql1.Append("cpr_No,");
            //    strSql2.Append("'" + model.cpr_No + "',");
            //}
            //if (model.Construction_Name != null)
            //{
            //    strSql1.Append("Construction_Name,");
            //    strSql2.Append("'" + model.Construction_Name + "',");
            //}
            //if (model.Project_Diameter != null)
            //{
            //    strSql1.Append("Project_Diameter,");
            //    strSql2.Append("" + model.Project_Diameter + ",");
            //}
            //if (model.Quantity != null)
            //{
            //    strSql1.Append("Quantity,");
            //    strSql2.Append("" + model.Quantity + ",");
            //}
            //if (model.Pile_Length != null)
            //{
            //    strSql1.Append("Pile_Length,");
            //    strSql2.Append("" + model.Pile_Length + ",");
            //}
            //if (model.Pit_Area != null)
            //{
            //    strSql1.Append("Pit_Area,");
            //    strSql2.Append("" + model.Pit_Area + ",");
            //}
            //if (model.Depth != null)
            //{
            //    strSql1.Append("Depth,");
            //    strSql2.Append("" + model.Depth + ",");
            //}
            //if (model.Well_Amount != null)
            //{
            //    strSql1.Append("Well_Amount,");
            //    strSql2.Append("" + model.Well_Amount + ",");
            //}
            //if (model.Concrete_Amount != null)
            //{
            //    strSql1.Append("Concrete_Amount,");
            //    strSql2.Append("" + model.Concrete_Amount + ",");
            //}
            //if (model.Concrete_Amount != null)
            //{
            //    strSql1.Append("Concrete_Amount,");
            //    strSql2.Append("" + model.Concrete_Amount + ",");
            //}
            //strSql1.Append("Money,");
            //strSql2.Append("" + model.Money + ",");

            //if (model.UpdateBy != null)
            //{
            //    strSql1.Append("UpdateBy,");
            //    strSql2.Append("'" + model.UpdateBy + "',");
            //}
            //if (model.LastUpdate != null)
            //{
            //    strSql1.Append("LastUpdate,");
            //    strSql2.Append("'" + model.LastUpdate + "',");
            //}

            //strSql.Append("insert into cm_SubCoperation(");
            //strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            //strSql.Append(")");
            //strSql.Append(" values (");
            //strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            //strSql.Append(")");
            //strSql.Append(";select @@IDENTITY");
            //object obj = DbHelperSQL.GetSingle(strSql.ToString());
            //if (obj == null)
            //{
            //    return 0;
            //}
            //else
            //{
            //    return Convert.ToInt32(obj);
            //}

            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_SubConstruCoperartion(");
            strSql.Append("cpr_Id,cpr_No,Construction_Name,Project_Diameter,Quantity,Pile_Length,Pit_Area,Depth,Support_Form,Well_Amount,Concrete_Amount,Reinforced_Amount,UpdateBy,LastUpdate)");
            strSql.Append(" values (");
            strSql.Append("@cpr_Id,@cpr_No,@Construction_Name,@Project_Diameter,@Quantity,@Pile_Length,@Pit_Area,@Depth,@Support_Form,@Well_Amount,@Concrete_Amount,@Reinforced_Amount,@UpdateBy,@LastUpdate)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@cpr_Id", SqlDbType.Int,4),
					new SqlParameter("@cpr_No", SqlDbType.NVarChar,50),
					new SqlParameter("@Construction_Name", SqlDbType.NVarChar,1000),
					new SqlParameter("@Project_Diameter", SqlDbType.Decimal,9),
					new SqlParameter("@Quantity", SqlDbType.Int,4),
					new SqlParameter("@Pile_Length", SqlDbType.Decimal,9),
					new SqlParameter("@Pit_Area", SqlDbType.Decimal,9),
					new SqlParameter("@Depth", SqlDbType.Decimal,9),
					new SqlParameter("@Support_Form", SqlDbType.NVarChar,1000),
					new SqlParameter("@Well_Amount", SqlDbType.Decimal,9),
					new SqlParameter("@Concrete_Amount", SqlDbType.Decimal,9),
					new SqlParameter("@Reinforced_Amount", SqlDbType.Decimal,9),
					new SqlParameter("@UpdateBy", SqlDbType.Char,20),
					new SqlParameter("@LastUpdate", SqlDbType.DateTime)};
            parameters[0].Value = model.cpr_Id;
            parameters[1].Value = model.cpr_No;
            parameters[2].Value = model.Construction_Name;
            parameters[3].Value = model.Project_Diameter;
            parameters[4].Value = model.Quantity;
            parameters[5].Value = model.Pile_Length;
            parameters[6].Value = model.Pit_Area;
            parameters[7].Value = model.Depth;
            parameters[8].Value = model.Support_Form;
            parameters[9].Value = model.Well_Amount;
            parameters[10].Value = model.Concrete_Amount;
            parameters[11].Value = model.Reinforced_Amount;
            parameters[12].Value = model.UpdateBy;
            parameters[13].Value = model.LastUpdate;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(Model.cm_SubConstruCoperartion model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_SubConstruCoperartion set ");
            strSql.Append("cpr_Id=@cpr_Id,");
            strSql.Append("cpr_No=@cpr_No,");
            strSql.Append("Construction_Name=@Construction_Name,");
            strSql.Append("Project_Diameter=@Project_Diameter,");
            strSql.Append("Quantity=@Quantity,");
            strSql.Append("Pile_Length=@Pile_Length,");
            strSql.Append("Pit_Area=@Pit_Area,");
            strSql.Append("Depth=@Depth,");
            strSql.Append("Support_Form=@Support_Form,");
            strSql.Append("Well_Amount=@Well_Amount,");
            strSql.Append("Concrete_Amount=@Concrete_Amount,");
            strSql.Append("Reinforced_Amount=@Reinforced_Amount,");
            strSql.Append("UpdateBy=@UpdateBy,");
            strSql.Append("LastUpdate=@LastUpdate");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@cpr_Id", SqlDbType.Int,4),
					new SqlParameter("@cpr_No", SqlDbType.NVarChar,50),
					new SqlParameter("@Construction_Name", SqlDbType.NVarChar,1000),
					new SqlParameter("@Project_Diameter", SqlDbType.Decimal,9),
					new SqlParameter("@Quantity", SqlDbType.Int,4),
					new SqlParameter("@Pile_Length", SqlDbType.Decimal,9),
					new SqlParameter("@Pit_Area", SqlDbType.Decimal,9),
					new SqlParameter("@Depth", SqlDbType.Decimal,9),
					new SqlParameter("@Support_Form", SqlDbType.NVarChar,1000),
					new SqlParameter("@Well_Amount", SqlDbType.Decimal,9),
					new SqlParameter("@Concrete_Amount", SqlDbType.Decimal,9),
					new SqlParameter("@Reinforced_Amount", SqlDbType.Decimal,9),
					new SqlParameter("@UpdateBy", SqlDbType.Char,20),
					new SqlParameter("@LastUpdate", SqlDbType.DateTime),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.cpr_Id;
            parameters[1].Value = model.cpr_No;
            parameters[2].Value = model.Construction_Name;
            parameters[3].Value = model.Project_Diameter;
            parameters[4].Value = model.Quantity;
            parameters[5].Value = model.Pile_Length;
            parameters[6].Value = model.Pit_Area;
            parameters[7].Value = model.Depth;
            parameters[8].Value = model.Support_Form;
            parameters[9].Value = model.Well_Amount;
            parameters[10].Value = model.Concrete_Amount;
            parameters[11].Value = model.Reinforced_Amount;
            parameters[12].Value = model.UpdateBy;
            parameters[13].Value = model.LastUpdate;
            parameters[14].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_SubConstruCoperartion ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_SubConstruCoperartion ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public Model.cm_SubConstruCoperartion GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,cpr_Id,cpr_No,Construction_Name,Project_Diameter,Quantity,Pile_Length,Pit_Area,Depth,Support_Form,Well_Amount,Concrete_Amount,Reinforced_Amount,UpdateBy,LastUpdate from cm_SubConstruCoperartion ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            Model.cm_SubConstruCoperartion model = new Model.cm_SubConstruCoperartion();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_Id"] != null && ds.Tables[0].Rows[0]["cpr_Id"].ToString() != "")
                {
                    model.cpr_Id = int.Parse(ds.Tables[0].Rows[0]["cpr_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_No"] != null && ds.Tables[0].Rows[0]["cpr_No"].ToString() != "")
                {
                    model.cpr_No = ds.Tables[0].Rows[0]["cpr_No"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Construction_Name"] != null && ds.Tables[0].Rows[0]["Construction_Name"].ToString() != "")
                {
                    model.Construction_Name = ds.Tables[0].Rows[0]["Construction_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Project_Diameter"] != null && ds.Tables[0].Rows[0]["Project_Diameter"].ToString() != "")
                {
                    model.Project_Diameter = decimal.Parse(ds.Tables[0].Rows[0]["Project_Diameter"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Quantity"] != null && ds.Tables[0].Rows[0]["Quantity"].ToString() != "")
                {
                    model.Quantity = int.Parse(ds.Tables[0].Rows[0]["Quantity"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Pile_Length"] != null && ds.Tables[0].Rows[0]["Pile_Length"].ToString() != "")
                {
                    model.Pile_Length = decimal.Parse(ds.Tables[0].Rows[0]["Pile_Length"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Pit_Area"] != null && ds.Tables[0].Rows[0]["Pit_Area"].ToString() != "")
                {
                    model.Pit_Area = decimal.Parse(ds.Tables[0].Rows[0]["Pit_Area"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Depth"] != null && ds.Tables[0].Rows[0]["Depth"].ToString() != "")
                {
                    model.Depth = decimal.Parse(ds.Tables[0].Rows[0]["Depth"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Support_Form"] != null && ds.Tables[0].Rows[0]["Support_Form"].ToString() != "")
                {
                    model.Support_Form = ds.Tables[0].Rows[0]["Support_Form"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Well_Amount"] != null && ds.Tables[0].Rows[0]["Well_Amount"].ToString() != "")
                {
                    model.Well_Amount = decimal.Parse(ds.Tables[0].Rows[0]["Well_Amount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Concrete_Amount"] != null && ds.Tables[0].Rows[0]["Concrete_Amount"].ToString() != "")
                {
                    model.Concrete_Amount = decimal.Parse(ds.Tables[0].Rows[0]["Concrete_Amount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Reinforced_Amount"] != null && ds.Tables[0].Rows[0]["Reinforced_Amount"].ToString() != "")
                {
                    model.Reinforced_Amount = decimal.Parse(ds.Tables[0].Rows[0]["Reinforced_Amount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["UpdateBy"] != null && ds.Tables[0].Rows[0]["UpdateBy"].ToString() != "")
                {
                    model.UpdateBy = ds.Tables[0].Rows[0]["UpdateBy"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LastUpdate"] != null && ds.Tables[0].Rows[0]["LastUpdate"].ToString() != "")
                {
                    model.LastUpdate = DateTime.Parse(ds.Tables[0].Rows[0]["LastUpdate"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,cpr_Id,cpr_No,Construction_Name,Project_Diameter,Quantity,Pile_Length,Pit_Area,Depth,Support_Form,Well_Amount,Concrete_Amount,Reinforced_Amount,UpdateBy,LastUpdate ");
            strSql.Append(" FROM cm_SubConstruCoperartion ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,cpr_Id,cpr_No,Construction_Name,Project_Diameter,Quantity,Pile_Length,Pit_Area,Depth,Support_Form,Well_Amount,Concrete_Amount,Reinforced_Amount,UpdateBy,LastUpdate ");
            strSql.Append(" FROM cm_SubConstruCoperartion ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_SubConstruCoperartion ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_SubConstruCoperartion T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }


        #endregion  Method
    }
}
