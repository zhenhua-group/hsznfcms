﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_KaoHeRenWu
    /// </summary>
    public partial class cm_KaoHeRenWu
    {
        public cm_KaoHeRenWu()
        { }
        #region  BasicMethod


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_KaoHeRenWu model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_KaoHeRenWu(");
            strSql.Append("RenWuNo,StartDate,EndDate,Sub,ProjStatus,DeptStatus,InsertDate,InsertUser,JianAllCount)");
            strSql.Append(" values (");
            strSql.Append("@RenWuNo,@StartDate,@EndDate,@Sub,@ProjStatus,@DeptStatus,@InsertDate,@InsertUser,@JianAllCount)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@RenWuNo", SqlDbType.NVarChar,100),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Sub", SqlDbType.NVarChar,200),
					new SqlParameter("@ProjStatus", SqlDbType.NVarChar,10),
					new SqlParameter("@DeptStatus", SqlDbType.NVarChar,10),
					new SqlParameter("@InsertDate", SqlDbType.DateTime),
					new SqlParameter("@InsertUser", SqlDbType.Int,4),
					new SqlParameter("@JianAllCount", SqlDbType.Decimal,9)};
            parameters[0].Value = model.RenWuNo;
            parameters[1].Value = model.StartDate;
            parameters[2].Value = model.EndDate;
            parameters[3].Value = model.Sub;
            parameters[4].Value = model.ProjStatus;
            parameters[5].Value = model.DeptStatus;
            parameters[6].Value = model.InsertDate;
            parameters[7].Value = model.InsertUser;
            parameters[8].Value = model.JianAllCount;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_KaoHeRenWu model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_KaoHeRenWu set ");
            strSql.Append("RenWuNo=@RenWuNo,");
            strSql.Append("StartDate=@StartDate,");
            strSql.Append("EndDate=@EndDate,");
            strSql.Append("Sub=@Sub,");
            strSql.Append("ProjStatus=@ProjStatus,");
            strSql.Append("DeptStatus=@DeptStatus,");
            strSql.Append("InsertDate=@InsertDate,");
            strSql.Append("InsertUser=@InsertUser,");
            strSql.Append("JianAllCount=@JianAllCount");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@RenWuNo", SqlDbType.NVarChar,100),
					new SqlParameter("@StartDate", SqlDbType.DateTime),
					new SqlParameter("@EndDate", SqlDbType.DateTime),
					new SqlParameter("@Sub", SqlDbType.NVarChar,200),
					new SqlParameter("@ProjStatus", SqlDbType.NVarChar,10),
					new SqlParameter("@DeptStatus", SqlDbType.NVarChar,10),
					new SqlParameter("@InsertDate", SqlDbType.DateTime),
					new SqlParameter("@InsertUser", SqlDbType.Int,4),
					new SqlParameter("@JianAllCount", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.RenWuNo;
            parameters[1].Value = model.StartDate;
            parameters[2].Value = model.EndDate;
            parameters[3].Value = model.Sub;
            parameters[4].Value = model.ProjStatus;
            parameters[5].Value = model.DeptStatus;
            parameters[6].Value = model.InsertDate;
            parameters[7].Value = model.InsertUser;
            parameters[8].Value = model.JianAllCount;
            parameters[9].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_KaoHeRenWu ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_KaoHeRenWu ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_KaoHeRenWu GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,RenWuNo,StartDate,EndDate,Sub,ProjStatus,DeptStatus,InsertDate,InsertUser,JianAllCount from cm_KaoHeRenWu ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_KaoHeRenWu model = new TG.Model.cm_KaoHeRenWu();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_KaoHeRenWu DataRowToModel(DataRow row)
        {
            TG.Model.cm_KaoHeRenWu model = new TG.Model.cm_KaoHeRenWu();
            if (row != null)
            {
                if (row["ID"] != null && row["ID"].ToString() != "")
                {
                    model.ID = int.Parse(row["ID"].ToString());
                }
                if (row["RenWuNo"] != null)
                {
                    model.RenWuNo = row["RenWuNo"].ToString();
                }
                if (row["StartDate"] != null && row["StartDate"].ToString() != "")
                {
                    model.StartDate = DateTime.Parse(row["StartDate"].ToString());
                }
                if (row["EndDate"] != null && row["EndDate"].ToString() != "")
                {
                    model.EndDate = DateTime.Parse(row["EndDate"].ToString());
                }
                if (row["Sub"] != null)
                {
                    model.Sub = row["Sub"].ToString();
                }
                if (row["ProjStatus"] != null)
                {
                    model.ProjStatus = row["ProjStatus"].ToString();
                }
                if (row["DeptStatus"] != null)
                {
                    model.DeptStatus = row["DeptStatus"].ToString();
                }
                if (row["InsertDate"] != null && row["InsertDate"].ToString() != "")
                {
                    model.InsertDate = DateTime.Parse(row["InsertDate"].ToString());
                }
                if (row["InsertUser"] != null && row["InsertUser"].ToString() != "")
                {
                    model.InsertUser = int.Parse(row["InsertUser"].ToString());
                }
                if (row["JianAllCount"] != null && row["JianAllCount"].ToString() != "")
                {
                    model.JianAllCount = decimal.Parse(row["JianAllCount"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,RenWuNo,StartDate,EndDate,Sub,ProjStatus,DeptStatus,InsertDate,InsertUser,JianAllCount ");
            strSql.Append(" FROM cm_KaoHeRenWu ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,RenWuNo,StartDate,EndDate,Sub,ProjStatus,DeptStatus,InsertDate,InsertUser,JianAllCount ");
            strSql.Append(" FROM cm_KaoHeRenWu ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_KaoHeRenWu ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_KaoHeRenWu T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "cm_KaoHeRenWu";
            parameters[1].Value = "ID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

