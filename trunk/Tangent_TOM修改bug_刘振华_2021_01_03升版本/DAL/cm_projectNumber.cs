﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using System.Collections.Generic;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_projectNumber
    /// </summary>
    public partial class cm_projectNumber
    {
        public cm_projectNumber()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("id", "cm_projectNumber");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        public string GetProjectJobNumberByProjectSysNo(int projectSysNo)
        {
            string sql = "select ProNumber from cm_ProjectNumber where Pro_id=" + projectSysNo;

            object objResult = DbHelperSQL.GetSingle(sql);

            return objResult == null ? string.Empty : objResult.ToString();
        }
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int pro_id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_projectNumber");
            strSql.Append(" where pro_id=@proNum_id");
            SqlParameter[] parameters = {
					new SqlParameter("@proNum_id", SqlDbType.Int,4)
};
            parameters[0].Value = pro_id;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 是否存记录
        /// </summary>
        /// <param name="pro_id">项目id</param>
        /// <param name="pronum">项目工号</param>
        /// <returns></returns>
        public bool Exists(string pronum)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_projectNumber");
            strSql.Append(" where proNumber='" + pronum + "'");
            return DbHelperSQL.Exists(strSql.ToString());
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_projectNumber model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_projectNumber(");
            strSql.Append("pro_id,SubmitDate,SubmintPerson,ProNumber)");
            strSql.Append(" values (");
            strSql.Append("@pro_id,@SubmitDate,@SubmintPerson,@ProNumber)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@pro_id", SqlDbType.Int,4),
					new SqlParameter("@SubmitDate", SqlDbType.DateTime),
					new SqlParameter("@SubmintPerson", SqlDbType.VarChar,20),
					new SqlParameter("@ProNumber", SqlDbType.VarChar,50)};
            parameters[0].Value = model.pro_id;
            parameters[1].Value = model.SubmitDate;
            parameters[2].Value = model.SubmintPerson;
            parameters[3].Value = model.ProNumber;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_projectNumber model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_projectNumber set ");
            strSql.Append("ProNumber=@ProNumber");
            strSql.Append(" where id=@proNum_id");
            SqlParameter[] parameters = {
					new SqlParameter("@ProNumber", SqlDbType.VarChar,50),
					new SqlParameter("@proNum_id", SqlDbType.Int,4)};
            parameters[0].Value = model.ProNumber;
            parameters[1].Value = model.proNum_id;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int proNum_id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_projectNumber ");
            strSql.Append(" where id=@proNum_id");
            SqlParameter[] parameters = {
					new SqlParameter("@proNum_id", SqlDbType.Int,4)
};
            parameters[0].Value = proNum_id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string proNum_idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_projectNumber ");
            strSql.Append(" where id in (" + proNum_idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_projectNumber GetModel(int proNum_id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 id,pro_id,SubmitDate,SubmintPerson,ProNumber from cm_projectNumber ");
            strSql.Append(" where id=@proNum_id");
            SqlParameter[] parameters = {
					new SqlParameter("@proNum_id", SqlDbType.Int,4)
};
            parameters[0].Value = proNum_id;

            TG.Model.cm_projectNumber model = new TG.Model.cm_projectNumber();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["id"] != null && ds.Tables[0].Rows[0]["id"].ToString() != "")
                {
                    model.proNum_id = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_id"] != null && ds.Tables[0].Rows[0]["pro_id"].ToString() != "")
                {
                    model.pro_id = int.Parse(ds.Tables[0].Rows[0]["pro_id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SubmitDate"] != null && ds.Tables[0].Rows[0]["SubmitDate"].ToString() != "")
                {
                    model.SubmitDate = DateTime.Parse(ds.Tables[0].Rows[0]["SubmitDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["SubmintPerson"] != null && ds.Tables[0].Rows[0]["SubmintPerson"].ToString() != "")
                {
                    model.SubmintPerson = ds.Tables[0].Rows[0]["SubmintPerson"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProNumber"] != null && ds.Tables[0].Rows[0]["ProNumber"].ToString() != "")
                {
                    model.ProNumber = ds.Tables[0].Rows[0]["ProNumber"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,pro_id,SubmitDate,SubmintPerson,ProNumber ");
            strSql.Append(" FROM cm_projectNumber ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        /// 获取不同的pronumber
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet GetDistinctList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" Select Distinct proNumber ");
            strSql.Append(" FROM  cm_ProjectNumber ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" Where " + strWhere);
            }

            return DbHelperSQL.Query(strSql.ToString());
        }
        //获取已经使用工号列表
        public List<string> GetExistsCoperationNo()
        {
            string strSql = " SELECT DISTINCT ProNumber FROM cm_ProjectNumber WHERE (ISNULL(ProNumber, '') <> N'') ";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql);

            List<string> resultString = new List<string>();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    resultString.Add(reader["ProNumber"].ToString().Trim());
                }
                
                reader.Close();
            }

            return resultString;
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" id,PJ.pro_Name,PJ.pro_id,SubmitDate,SubmintPerson,ProNumber,pro_buildUnit,PJ.Unit");
            strSql.Append(" FROM cm_ProjectNumber PN inner join cm_Project PJ on PN.pro_id=PJ.pro_id  ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "cm_projectNumber";
            parameters[1].Value = "proNum_id";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        //工号分配的记录数
        public int ProNumcount()
        {
            string strSql = "select count(*) from cm_projectNumber";
            return int.Parse(DbHelperSQL.GetSingle(strSql).ToString());
        }

        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_ProjectNumber", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_ProjectNumber_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        #endregion  Method
    }
}

