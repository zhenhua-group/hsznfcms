﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_ProjectNumConfig
	/// </summary>
	public partial class cm_ProjectNumConfig
	{
		public cm_ProjectNumConfig()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_ProjectNumConfig model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.PreFix != null)
			{
				strSql1.Append("PreFix,");
				strSql2.Append("'"+model.PreFix+"',");
			}
			if (model.StartNum != null)
			{
				strSql1.Append("StartNum,");
				strSql2.Append("'"+model.StartNum+"',");
			}
			if (model.EndNum != null)
			{
				strSql1.Append("EndNum,");
				strSql2.Append("'"+model.EndNum+"',");
			}
			if (model.Mark != null)
			{
				strSql1.Append("Mark,");
				strSql2.Append("'"+model.Mark+"',");
			}
			if (model.ProType != null)
			{
				strSql1.Append("ProType,");
				strSql2.Append("'"+model.ProType+"',");
			}
			if (model.Flag != null)
			{
				strSql1.Append("Flag,");
				strSql2.Append(""+model.Flag+",");
			}
			strSql.Append("insert into cm_ProjectNumConfig(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ProjectNumConfig model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_ProjectNumConfig set ");
			if (model.PreFix != null)
			{
				strSql.Append("PreFix='"+model.PreFix+"',");
			}
			else
			{
				strSql.Append("PreFix= null ,");
			}
			if (model.StartNum != null)
			{
				strSql.Append("StartNum='"+model.StartNum+"',");
			}
			else
			{
				strSql.Append("StartNum= null ,");
			}
			if (model.EndNum != null)
			{
				strSql.Append("EndNum='"+model.EndNum+"',");
			}
			else
			{
				strSql.Append("EndNum= null ,");
			}
			if (model.Mark != null)
			{
				strSql.Append("Mark='"+model.Mark+"',");
			}
			else
			{
				strSql.Append("Mark= null ,");
			}
			if (model.ProType != null)
			{
				strSql.Append("ProType='"+model.ProType+"',");
			}
			else
			{
				strSql.Append("ProType= null ,");
			}
			if (model.Flag != null)
			{
				strSql.Append("Flag="+model.Flag+",");
			}
			else
			{
				strSql.Append("Flag= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ID="+ model.ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ProjectNumConfig ");
			strSql.Append(" where ID="+ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ProjectNumConfig ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ProjectNumConfig GetModel(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" ID,PreFix,StartNum,EndNum,Mark,ProType,Flag ");
			strSql.Append(" from cm_ProjectNumConfig ");
			strSql.Append(" where ID="+ID+"" );
			TG.Model.cm_ProjectNumConfig model=new TG.Model.cm_ProjectNumConfig();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["ID"]!=null && ds.Tables[0].Rows[0]["ID"].ToString()!="")
				{
					model.ID=int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["PreFix"]!=null && ds.Tables[0].Rows[0]["PreFix"].ToString()!="")
				{
					model.PreFix=ds.Tables[0].Rows[0]["PreFix"].ToString();
				}
				if(ds.Tables[0].Rows[0]["StartNum"]!=null && ds.Tables[0].Rows[0]["StartNum"].ToString()!="")
				{
					model.StartNum=ds.Tables[0].Rows[0]["StartNum"].ToString();
				}
				if(ds.Tables[0].Rows[0]["EndNum"]!=null && ds.Tables[0].Rows[0]["EndNum"].ToString()!="")
				{
					model.EndNum=ds.Tables[0].Rows[0]["EndNum"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Mark"]!=null && ds.Tables[0].Rows[0]["Mark"].ToString()!="")
				{
					model.Mark=ds.Tables[0].Rows[0]["Mark"].ToString();
				}
				if(ds.Tables[0].Rows[0]["ProType"]!=null && ds.Tables[0].Rows[0]["ProType"].ToString()!="")
				{
					model.ProType=ds.Tables[0].Rows[0]["ProType"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Flag"]!=null && ds.Tables[0].Rows[0]["Flag"].ToString()!="")
				{
					model.Flag=int.Parse(ds.Tables[0].Rows[0]["Flag"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,PreFix,StartNum,EndNum,Mark,ProType,Flag ");
			strSql.Append(" FROM cm_ProjectNumConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,PreFix,StartNum,EndNum,Mark,ProType,Flag ");
			strSql.Append(" FROM cm_ProjectNumConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_ProjectNumConfig ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_ProjectNumConfig T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

