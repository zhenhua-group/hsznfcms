﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;

namespace TG.DAL
{
	public class DropDownListCountryDA
	{
		/// <summary>
		/// 得到国家列表
		/// </summary>
		/// <param name="queryEntity"></param>
		/// <returns></returns>
		public List<TG.Model.Country> GetCountryList(TG.Model.DropDownListQueryEntity queryEntity)
		{
			string whereSql = " where 1=1";

			if (!string.IsNullOrEmpty(queryEntity.CountryID))
			{
				whereSql += " and CountryID=" + queryEntity.CountryID;
			}
			if (!string.IsNullOrEmpty(queryEntity.CountryName))
			{
				whereSql += " and CountryName like N'%" + queryEntity.CountryName + "%'";
			}

			string sql = "select ID,CountryID,CountryName from cm_Country" + whereSql;

			SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

			List<TG.Model.Country> resultList = new List<TG.Model.Country>();

			while (reader.Read())
			{
				TG.Model.Country country = new TG.Model.Country
				{
					ID = reader["ID"] == null ? 0 : Convert.ToInt32(reader["ID"]),
					CountryID = reader["CountryID"] == DBNull.Value ? "" : reader["CountryID"].ToString(),
					CountryName = reader["CountryName"] == DBNull.Value ? "" : reader["CountryName"].ToString()
				};
				resultList.Add(country);
			}
			reader.Close();
			return resultList;
		}

		/// <summary>
		/// 得到城市列表
		/// </summary>
		/// <param name="countryID">国家ID</param>
		/// <returns></returns>
		public List<TG.Model.Province> GetProvinceList(string countryID)
		{
			string sql = "select id,ProvinceID,ProvinceName,CountryID from cm_Province where CountryID=N'" + countryID + "'";

			SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

			List<TG.Model.Province> resultList = new List<TG.Model.Province>();

			while (reader.Read())
			{
				TG.Model.Province province = new TG.Model.Province
				{
					ID = reader["id"] == null ? 0 : Convert.ToInt32(reader["id"]),
					ProvinceID = reader["ProvinceID"] == null ? "" : reader["ProvinceID"].ToString(),
					ProvinceName = reader["ProvinceName"] == null ? "" : reader["ProvinceName"].ToString(),
					CountryID = reader["CountryID"] == null ? "" : reader["CountryID"].ToString()
				};
				resultList.Add(province);
			}
			reader.Close();
			return resultList;
		}

		/// <summary>
		/// 得到城市列表
		/// </summary>
		/// <param name="provinceID"></param>
		/// <returns></returns>
		public List<TG.Model.City> GetCitys(string provinceID)
		{
			string sql = "select ID,CityID,CityName,ProvinceID from cm_City where ProvinceID=" + provinceID;

			SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

			List<TG.Model.City> resultList = new List<TG.Model.City>();

			while (reader.Read())
			{
				TG.Model.City city = new TG.Model.City
				{
					ID = reader["ID"] == null ? 0 : Convert.ToInt32(reader["ID"]),
					CityID = reader["CityID"] == null ? "" : reader["CityID"].ToString(),
					CityName = reader["CityName"] == null ? "" : reader["CityName"].ToString(),
					ProvinceID = reader["ProvinceID"] == null ? "" : reader["ProvinceID"].ToString()
				};
				resultList.Add(city);
			}
			reader.Close();
			return resultList;
		}
	}
}
