﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeProjTypeEnum
	/// </summary>
	public partial class cm_KaoHeProjTypeEnum
	{
		public cm_KaoHeProjTypeEnum()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeProjTypeEnum model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeProjTypeEnum(");
			strSql.Append("TypeName,Type1,Type2,Type3,Type4,Type5,Type6,Type7,Type8,Type9,Type10,Type11,Jianzhu,Jiegou,Nuantong,Shui,Dianqi,Shinei,Jianzhu2,Shebei,Dianqi2)");
			strSql.Append(" values (");
			strSql.Append("@TypeName,@Type1,@Type2,@Type3,@Type4,@Type5,@Type6,@Type7,@Type8,@Type9,@Type10,@Type11,@Jianzhu,@Jiegou,@Nuantong,@Shui,@Dianqi,@Shinei,@Jianzhu2,@Shebei,@Dianqi2)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@TypeName", SqlDbType.NVarChar,50),
					new SqlParameter("@Type1", SqlDbType.Decimal,9),
					new SqlParameter("@Type2", SqlDbType.Decimal,9),
					new SqlParameter("@Type3", SqlDbType.Decimal,9),
					new SqlParameter("@Type4", SqlDbType.Decimal,9),
					new SqlParameter("@Type5", SqlDbType.Decimal,9),
					new SqlParameter("@Type6", SqlDbType.Decimal,9),
					new SqlParameter("@Type7", SqlDbType.Decimal,9),
					new SqlParameter("@Type8", SqlDbType.Decimal,9),
					new SqlParameter("@Type9", SqlDbType.Decimal,9),
					new SqlParameter("@Type10", SqlDbType.Decimal,9),
					new SqlParameter("@Type11", SqlDbType.Decimal,9),
					new SqlParameter("@Jianzhu", SqlDbType.Decimal,9),
					new SqlParameter("@Jiegou", SqlDbType.Decimal,9),
					new SqlParameter("@Nuantong", SqlDbType.Decimal,9),
					new SqlParameter("@Shui", SqlDbType.Decimal,9),
					new SqlParameter("@Dianqi", SqlDbType.Decimal,9),
					new SqlParameter("@Shinei", SqlDbType.Decimal,9),
					new SqlParameter("@Jianzhu2", SqlDbType.Decimal,9),
					new SqlParameter("@Shebei", SqlDbType.Decimal,9),
					new SqlParameter("@Dianqi2", SqlDbType.Decimal,9)};
			parameters[0].Value = model.TypeName;
			parameters[1].Value = model.Type1;
			parameters[2].Value = model.Type2;
			parameters[3].Value = model.Type3;
			parameters[4].Value = model.Type4;
			parameters[5].Value = model.Type5;
			parameters[6].Value = model.Type6;
			parameters[7].Value = model.Type7;
			parameters[8].Value = model.Type8;
			parameters[9].Value = model.Type9;
			parameters[10].Value = model.Type10;
			parameters[11].Value = model.Type11;
			parameters[12].Value = model.Jianzhu;
			parameters[13].Value = model.Jiegou;
			parameters[14].Value = model.Nuantong;
			parameters[15].Value = model.Shui;
			parameters[16].Value = model.Dianqi;
			parameters[17].Value = model.Shinei;
			parameters[18].Value = model.Jianzhu2;
			parameters[19].Value = model.Shebei;
			parameters[20].Value = model.Dianqi2;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeProjTypeEnum model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeProjTypeEnum set ");
			strSql.Append("TypeName=@TypeName,");
			strSql.Append("Type1=@Type1,");
			strSql.Append("Type2=@Type2,");
			strSql.Append("Type3=@Type3,");
			strSql.Append("Type4=@Type4,");
			strSql.Append("Type5=@Type5,");
			strSql.Append("Type6=@Type6,");
			strSql.Append("Type7=@Type7,");
			strSql.Append("Type8=@Type8,");
			strSql.Append("Type9=@Type9,");
			strSql.Append("Type10=@Type10,");
			strSql.Append("Type11=@Type11,");
			strSql.Append("Jianzhu=@Jianzhu,");
			strSql.Append("Jiegou=@Jiegou,");
			strSql.Append("Nuantong=@Nuantong,");
			strSql.Append("Shui=@Shui,");
			strSql.Append("Dianqi=@Dianqi,");
			strSql.Append("Shinei=@Shinei,");
			strSql.Append("Jianzhu2=@Jianzhu2,");
			strSql.Append("Shebei=@Shebei,");
			strSql.Append("Dianqi2=@Dianqi2");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@TypeName", SqlDbType.NVarChar,50),
					new SqlParameter("@Type1", SqlDbType.Decimal,9),
					new SqlParameter("@Type2", SqlDbType.Decimal,9),
					new SqlParameter("@Type3", SqlDbType.Decimal,9),
					new SqlParameter("@Type4", SqlDbType.Decimal,9),
					new SqlParameter("@Type5", SqlDbType.Decimal,9),
					new SqlParameter("@Type6", SqlDbType.Decimal,9),
					new SqlParameter("@Type7", SqlDbType.Decimal,9),
					new SqlParameter("@Type8", SqlDbType.Decimal,9),
					new SqlParameter("@Type9", SqlDbType.Decimal,9),
					new SqlParameter("@Type10", SqlDbType.Decimal,9),
					new SqlParameter("@Type11", SqlDbType.Decimal,9),
					new SqlParameter("@Jianzhu", SqlDbType.Decimal,9),
					new SqlParameter("@Jiegou", SqlDbType.Decimal,9),
					new SqlParameter("@Nuantong", SqlDbType.Decimal,9),
					new SqlParameter("@Shui", SqlDbType.Decimal,9),
					new SqlParameter("@Dianqi", SqlDbType.Decimal,9),
					new SqlParameter("@Shinei", SqlDbType.Decimal,9),
					new SqlParameter("@Jianzhu2", SqlDbType.Decimal,9),
					new SqlParameter("@Shebei", SqlDbType.Decimal,9),
					new SqlParameter("@Dianqi2", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.TypeName;
			parameters[1].Value = model.Type1;
			parameters[2].Value = model.Type2;
			parameters[3].Value = model.Type3;
			parameters[4].Value = model.Type4;
			parameters[5].Value = model.Type5;
			parameters[6].Value = model.Type6;
			parameters[7].Value = model.Type7;
			parameters[8].Value = model.Type8;
			parameters[9].Value = model.Type9;
			parameters[10].Value = model.Type10;
			parameters[11].Value = model.Type11;
			parameters[12].Value = model.Jianzhu;
			parameters[13].Value = model.Jiegou;
			parameters[14].Value = model.Nuantong;
			parameters[15].Value = model.Shui;
			parameters[16].Value = model.Dianqi;
			parameters[17].Value = model.Shinei;
			parameters[18].Value = model.Jianzhu2;
			parameters[19].Value = model.Shebei;
			parameters[20].Value = model.Dianqi2;
			parameters[21].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeProjTypeEnum ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeProjTypeEnum ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeProjTypeEnum GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,TypeName,Type1,Type2,Type3,Type4,Type5,Type6,Type7,Type8,Type9,Type10,Type11,Jianzhu,Jiegou,Nuantong,Shui,Dianqi,Shinei,Jianzhu2,Shebei,Dianqi2 from cm_KaoHeProjTypeEnum ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeProjTypeEnum model=new TG.Model.cm_KaoHeProjTypeEnum();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeProjTypeEnum DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeProjTypeEnum model=new TG.Model.cm_KaoHeProjTypeEnum();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["TypeName"]!=null)
				{
					model.TypeName=row["TypeName"].ToString();
				}
				if(row["Type1"]!=null && row["Type1"].ToString()!="")
				{
					model.Type1=decimal.Parse(row["Type1"].ToString());
				}
				if(row["Type2"]!=null && row["Type2"].ToString()!="")
				{
					model.Type2=decimal.Parse(row["Type2"].ToString());
				}
				if(row["Type3"]!=null && row["Type3"].ToString()!="")
				{
					model.Type3=decimal.Parse(row["Type3"].ToString());
				}
				if(row["Type4"]!=null && row["Type4"].ToString()!="")
				{
					model.Type4=decimal.Parse(row["Type4"].ToString());
				}
				if(row["Type5"]!=null && row["Type5"].ToString()!="")
				{
					model.Type5=decimal.Parse(row["Type5"].ToString());
				}
				if(row["Type6"]!=null && row["Type6"].ToString()!="")
				{
					model.Type6=decimal.Parse(row["Type6"].ToString());
				}
				if(row["Type7"]!=null && row["Type7"].ToString()!="")
				{
					model.Type7=decimal.Parse(row["Type7"].ToString());
				}
				if(row["Type8"]!=null && row["Type8"].ToString()!="")
				{
					model.Type8=decimal.Parse(row["Type8"].ToString());
				}
				if(row["Type9"]!=null && row["Type9"].ToString()!="")
				{
					model.Type9=decimal.Parse(row["Type9"].ToString());
				}
				if(row["Type10"]!=null && row["Type10"].ToString()!="")
				{
					model.Type10=decimal.Parse(row["Type10"].ToString());
				}
				if(row["Type11"]!=null && row["Type11"].ToString()!="")
				{
					model.Type11=decimal.Parse(row["Type11"].ToString());
				}
				if(row["Jianzhu"]!=null && row["Jianzhu"].ToString()!="")
				{
					model.Jianzhu=decimal.Parse(row["Jianzhu"].ToString());
				}
				if(row["Jiegou"]!=null && row["Jiegou"].ToString()!="")
				{
					model.Jiegou=decimal.Parse(row["Jiegou"].ToString());
				}
				if(row["Nuantong"]!=null && row["Nuantong"].ToString()!="")
				{
					model.Nuantong=decimal.Parse(row["Nuantong"].ToString());
				}
				if(row["Shui"]!=null && row["Shui"].ToString()!="")
				{
					model.Shui=decimal.Parse(row["Shui"].ToString());
				}
				if(row["Dianqi"]!=null && row["Dianqi"].ToString()!="")
				{
					model.Dianqi=decimal.Parse(row["Dianqi"].ToString());
				}
				if(row["Shinei"]!=null && row["Shinei"].ToString()!="")
				{
					model.Shinei=decimal.Parse(row["Shinei"].ToString());
				}
				if(row["Jianzhu2"]!=null && row["Jianzhu2"].ToString()!="")
				{
					model.Jianzhu2=decimal.Parse(row["Jianzhu2"].ToString());
				}
				if(row["Shebei"]!=null && row["Shebei"].ToString()!="")
				{
					model.Shebei=decimal.Parse(row["Shebei"].ToString());
				}
				if(row["Dianqi2"]!=null && row["Dianqi2"].ToString()!="")
				{
					model.Dianqi2=decimal.Parse(row["Dianqi2"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,TypeName,Type1,Type2,Type3,Type4,Type5,Type6,Type7,Type8,Type9,Type10,Type11,Jianzhu,Jiegou,Nuantong,Shui,Dianqi,Shinei,Jianzhu2,Shebei,Dianqi2 ");
			strSql.Append(" FROM cm_KaoHeProjTypeEnum ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,TypeName,Type1,Type2,Type3,Type4,Type5,Type6,Type7,Type8,Type9,Type10,Type11,Jianzhu,Jiegou,Nuantong,Shui,Dianqi,Shinei,Jianzhu2,Shebei,Dianqi2 ");
			strSql.Append(" FROM cm_KaoHeProjTypeEnum ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeProjTypeEnum ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeProjTypeEnum T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeProjTypeEnum";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

