﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:tg_principalship
	/// </summary>
	public partial class tg_principalship
	{
		public tg_principalship()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.tg_principalship model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.pri_Name != null)
			{
				strSql1.Append("pri_Name,");
				strSql2.Append("'"+model.pri_Name+"',");
			}
			if (model.pri_Intro != null)
			{
				strSql1.Append("pri_Intro,");
				strSql2.Append("'"+model.pri_Intro+"',");
			}
			strSql.Append("insert into tg_principalship(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_principalship model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update tg_principalship set ");
			if (model.pri_Name != null)
			{
				strSql.Append("pri_Name='"+model.pri_Name+"',");
			}
			else
			{
				strSql.Append("pri_Name= null ,");
			}
			if (model.pri_Intro != null)
			{
				strSql.Append("pri_Intro='"+model.pri_Intro+"',");
			}
			else
			{
				strSql.Append("pri_Intro= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where pri_ID="+ model.pri_ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int pri_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_principalship ");
			strSql.Append(" where pri_ID="+pri_ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string pri_IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_principalship ");
			strSql.Append(" where pri_ID in ("+pri_IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_principalship GetModel(int pri_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" pri_ID,pri_Name,pri_Intro ");
			strSql.Append(" from tg_principalship ");
			strSql.Append(" where pri_ID="+pri_ID+"" );
			TG.Model.tg_principalship model=new TG.Model.tg_principalship();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["pri_ID"]!=null && ds.Tables[0].Rows[0]["pri_ID"].ToString()!="")
				{
					model.pri_ID=int.Parse(ds.Tables[0].Rows[0]["pri_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["pri_Name"]!=null && ds.Tables[0].Rows[0]["pri_Name"].ToString()!="")
				{
					model.pri_Name=ds.Tables[0].Rows[0]["pri_Name"].ToString();
				}
				if(ds.Tables[0].Rows[0]["pri_Intro"]!=null && ds.Tables[0].Rows[0]["pri_Intro"].ToString()!="")
				{
					model.pri_Intro=ds.Tables[0].Rows[0]["pri_Intro"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select pri_ID,pri_Name,pri_Intro ");
			strSql.Append(" FROM tg_principalship ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" pri_ID,pri_Name,pri_Intro ");
			strSql.Append(" FROM tg_principalship ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM tg_principalship ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            StringBuilder sb = new StringBuilder();
            sb.Append("select * from tg_principalship ");
            if (strWhere.Trim() != "")
            {
                sb.Append(" where " + strWhere);
            }
            if (orderby.Trim() != "")
            {
                sb.Append(" Order by " + orderby);
            }
            return DbHelperSQL.Query(sb.ToString(), startIndex, endIndex);
		}

		/*
		*/

		#endregion  Method
	}
}

