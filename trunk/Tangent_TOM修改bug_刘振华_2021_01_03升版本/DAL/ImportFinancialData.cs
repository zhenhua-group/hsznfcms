﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TG.DBUtility;

namespace TG.DAL
{
    public class ImportFinancialData
    {

        /// <summary>
        /// 批量插入数据
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public int InsertFinancial(DataTable dt)
        {
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };

            SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);
            bulkCopy.BulkCopyTimeout = 1000;
            bulkCopy.DestinationTableName = "cm_FinancialReport";
            bulkCopy.BatchSize = dt.Rows.Count;

            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                //映射字段名 DataTable列名 ,数据库 对应的列名 
                bulkCopy.ColumnMappings.Add("type", "type");
                bulkCopy.ColumnMappings.Add("FinancialNo", "FinancialNo");
                bulkCopy.ColumnMappings.Add("Year", "Year");
                bulkCopy.ColumnMappings.Add("Month", "Month");
                bulkCopy.ColumnMappings.Add("VoucherNo", "VoucherNo");
                bulkCopy.ColumnMappings.Add("Name", "Name");
                bulkCopy.ColumnMappings.Add("Account", "Account");
                bulkCopy.ColumnMappings.Add("FieldOfB", "FieldOfB");
                bulkCopy.ColumnMappings.Add("FieldOfF", "FieldOfF");
                bulkCopy.ColumnMappings.Add("FieldOfH", "FieldOfH");
                bulkCopy.ColumnMappings.Add("FileOfI", "FileOfI");

                if (dt != null && dt.Rows.Count != 0)
                {
                    //批量数据插入
                    bulkCopy.WriteToServer(dt);
                }

                return 1;
            }
            catch (Exception ex)
            {
                if (bulkCopy != null)
                    bulkCopy.Close();
                return 0;
            }
            finally
            {
                if (bulkCopy != null)
                    bulkCopy.Close();
                conn.Close();
            }
        }

        /// <summary>
        /// 得到最大的ID
        /// </summary>
        /// <returns></returns>
        public int GetMaxFinancialID(string mounth, string type)
        {
            SqlParameter[] parameters =
               
                {  
                    new SqlParameter("@year", mounth), 
                    new SqlParameter("@Mount", type),
                     new SqlParameter("@type", type)
                };

            parameters[0].Value = DateTime.Now.Year.ToString();
            parameters[1].Value = mounth;
            parameters[2].Value = type;


            DataSet ds = DbHelperSQL.RunProcedure("cm_FinancialReport_NUM", parameters, "aa");

            return int.Parse(ds.Tables[0].Rows[0][0].ToString());

        }

        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_FinancialReport", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_FinancialReport_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }

        /// <summary>
        /// 得到凭证单号
        /// </summary>
        /// <param name="name"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public DataSet GetFinanalNum(string name, decimal account, string inTime,string unit)
        {
            StringBuilder sb = new StringBuilder();


            sb.Append("  AND ( name  like '%" + name + "%'");


            sb.Append(" OR Account='" + account + "' )");

            //增加年份和月份判断
            //if (!string.IsNullOrEmpty(inTime))
            //{
            //    if (inTime.Contains('/'))
            //    {
            //        string[] temp = inTime.Split('/');

            //        string year = temp[0];
            //        string mounth = temp[1];
            //        sb.Append("And Year='" + year + "'");
            //        sb.Append("And Month='" + mounth + "'");
            //    }
            //}

            sb.Append("  AND  name  like '%" + unit + "%'");

            sb.Append("And Status is null");


            string sql = @"SELECT ID, (type+ Year+Month+ CASE WHEN LEN(FinancialNo)=1 THEN '00'+CAST(FinancialNo as CHAR ) WHEN LEN(FinancialNo)=2 THEN '0'+CAST(FinancialNo as CHAR )  ELSE CAST( FinancialNo AS CHAR) END ) AS FinancialNum,Name,Account FROM cm_FinancialReport WHERE 1=1 " + sb.ToString();

            DataSet ds = DbHelperSQL.Query(sql);

            return ds;
        }

        /// <summary>
        /// 更新单据号状态
        /// </summary>
        /// <param name="Id"></param>
        public void UpdateFinanalStatus(int Id)
        {
            string sql = "update cm_FinancialReport set Status=1  where  ID =" + Id + "";
            DbHelperSQL.ExecuteSql(sql.ToString());
        }

        /// <summary>
        /// 查询凭单号
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public DataSet GetVoucherNo(string type)
        {
            string sql = string.Format("SELECT VoucherNo FROM cm_FinancialReport where  type='{0}'", type);

            DataSet ds = DbHelperSQL.Query(sql);

            return ds;
        }
        public List<string> GetCoperationYear()
        {
            List<string> list = new List<string>();
            string strSql = " Select DISTINCT YEAR AS SignDate From cm_FinancialReport order by SignDate DESC";
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    list.Add(dt.Rows[i][0].ToString());
                }
            }
            return list;
        }
    }
}
