﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.DBUtility;

namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_CostType
    /// </summary>
    public partial class cm_CostType
    {
        public cm_CostType()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CostType model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_CostType(");
            strSql.Append("costName,costGroup,isInput)");
            strSql.Append(" values (");
            strSql.Append("@costName,@costGroup,@isInput)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@costName", SqlDbType.NVarChar,100),
					new SqlParameter("@costGroup", SqlDbType.Int,4),
					new SqlParameter("@isInput", SqlDbType.Int,4)};
            parameters[0].Value = model.costName;
            parameters[1].Value = model.costGroup;
            parameters[2].Value = model.isInput;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CostType model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_CostType set ");
            strSql.Append("costName=@costName");
            // strSql.Append("costGroup=@costGroup,");
            // strSql.Append("isInput=@isInput");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@costName", SqlDbType.NVarChar,100),
					//new SqlParameter("@costGroup", SqlDbType.Int,4),
					//new SqlParameter("@isInput", SqlDbType.Int,4),
					new SqlParameter("@id", SqlDbType.Int,4)};
            parameters[0].Value = model.costName;
            // parameters[1].Value = model.costGroup;
            // parameters[2].Value = model.isInput;
            parameters[1].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_CostType ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CostType GetModel(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 costName,costGroup,isInput,id from cm_CostType ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
            parameters[0].Value = id;

            TG.Model.cm_CostType model = new TG.Model.cm_CostType();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["costName"] != null && ds.Tables[0].Rows[0]["costName"].ToString() != "")
                {
                    model.costName = ds.Tables[0].Rows[0]["costName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["costGroup"] != null && ds.Tables[0].Rows[0]["costGroup"].ToString() != "")
                {
                    model.costGroup = int.Parse(ds.Tables[0].Rows[0]["costGroup"].ToString());
                }
                if (ds.Tables[0].Rows[0]["isInput"] != null && ds.Tables[0].Rows[0]["isInput"].ToString() != "")
                {
                    model.isInput = int.Parse(ds.Tables[0].Rows[0]["isInput"].ToString());
                }
                if (ds.Tables[0].Rows[0]["id"] != null && ds.Tables[0].Rows[0]["id"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,costName,costGroup,isInput ");
            strSql.Append(" FROM cm_CostType ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,costName,costGroup,isInput ");
            strSql.Append(" FROM cm_CostType ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_CostType ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T. desc");
            }
            strSql.Append(")AS Row, T.*  from cm_CostType T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_CostType", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_CostType_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 判断是否重复
        /// </summary>
        /// <param name="cst_num"></param>
        /// <param name="cst_Name"></param>
        /// <returns></returns>
        public bool JudgeData(string cost_unit, string cost_Name, string cost_id, string isInput)
        {

            bool flag = false;

            string sql = @"SELECT count(*) FROM cm_CostType WHERE costName='" + cost_Name + "' and  costGroup=" + cost_unit + "  and isInput=" + isInput + "";

            if (!string.IsNullOrEmpty(cost_id))
            {
                sql = sql + " AND  id !=" + cost_id + "";
            }
            object result = TG.DBUtility.DbHelperSQL.GetSingle(sql);

            if (int.Parse(result.ToString()) > 0)
            {
                flag = true;
            }
            else
            {
                flag = false;
            }

            return flag;
        }

        #endregion  Method
    }
}


