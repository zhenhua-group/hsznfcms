﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:tg_subClass
	/// </summary>
	public partial class tg_subClass
	{
		public tg_subClass()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.tg_subClass model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.class_ID != null)
			{
				strSql1.Append("class_ID,");
				strSql2.Append(""+model.class_ID+",");
			}
			if (model.subproID != null)
			{
				strSql1.Append("subproID,");
				strSql2.Append(""+model.subproID+",");
			}
			if (model.purposeid != null)
			{
				strSql1.Append("purposeid,");
				strSql2.Append(""+model.purposeid+",");
			}
			if (model.name != null)
			{
				strSql1.Append("name,");
				strSql2.Append("'"+model.name+"',");
			}
			if (model.parentID != null)
			{
				strSql1.Append("parentID,");
				strSql2.Append(""+model.parentID+",");
			}
			if (model.descr != null)
			{
				strSql1.Append("descr,");
				strSql2.Append("'"+model.descr+"',");
			}
			strSql.Append("insert into tg_subClass(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_subClass model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update tg_subClass set ");
			if (model.class_ID != null)
			{
				strSql.Append("class_ID="+model.class_ID+",");
			}
			else
			{
				strSql.Append("class_ID= null ,");
			}
			if (model.subproID != null)
			{
				strSql.Append("subproID="+model.subproID+",");
			}
			else
			{
				strSql.Append("subproID= null ,");
			}
			if (model.purposeid != null)
			{
				strSql.Append("purposeid="+model.purposeid+",");
			}
			else
			{
				strSql.Append("purposeid= null ,");
			}
			if (model.name != null)
			{
				strSql.Append("name='"+model.name+"',");
			}
			if (model.parentID != null)
			{
				strSql.Append("parentID="+model.parentID+",");
			}
			else
			{
				strSql.Append("parentID= null ,");
			}
			if (model.descr != null)
			{
				strSql.Append("descr='"+model.descr+"',");
			}
			else
			{
				strSql.Append("descr= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where id="+ model.id+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_subClass ");
			strSql.Append(" where id="+id+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_subClass ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_subClass GetModel(int id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" id,class_ID,subproID,purposeid,name,parentID,descr ");
			strSql.Append(" from tg_subClass ");
			strSql.Append(" where id="+id+"" );
			TG.Model.tg_subClass model=new TG.Model.tg_subClass();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["id"]!=null && ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["class_ID"]!=null && ds.Tables[0].Rows[0]["class_ID"].ToString()!="")
				{
					model.class_ID=int.Parse(ds.Tables[0].Rows[0]["class_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["subproID"]!=null && ds.Tables[0].Rows[0]["subproID"].ToString()!="")
				{
					model.subproID=int.Parse(ds.Tables[0].Rows[0]["subproID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["purposeid"]!=null && ds.Tables[0].Rows[0]["purposeid"].ToString()!="")
				{
					model.purposeid=int.Parse(ds.Tables[0].Rows[0]["purposeid"].ToString());
				}
				if(ds.Tables[0].Rows[0]["name"]!=null && ds.Tables[0].Rows[0]["name"].ToString()!="")
				{
					model.name=ds.Tables[0].Rows[0]["name"].ToString();
				}
				if(ds.Tables[0].Rows[0]["parentID"]!=null && ds.Tables[0].Rows[0]["parentID"].ToString()!="")
				{
					model.parentID=int.Parse(ds.Tables[0].Rows[0]["parentID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["descr"]!=null && ds.Tables[0].Rows[0]["descr"].ToString()!="")
				{
					model.descr=ds.Tables[0].Rows[0]["descr"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,class_ID,subproID,purposeid,name,parentID,descr ");
			strSql.Append(" FROM tg_subClass ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,class_ID,subproID,purposeid,name,parentID,descr ");
			strSql.Append(" FROM tg_subClass ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM tg_subClass ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from tg_subClass T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

