﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_ProImage
    /// </summary>
    public partial class cm_ProImage
    {
        public cm_ProImage()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("ID", "cm_ProImage");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_ProImage");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 判断是否存在该记录，并得到id
        /// </summary>
        public int GetSingle(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select top 1 ID from cm_ProImage");
            strSql.Append(" where pro_id=" + ID + "");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());

             return obj==null?0:Convert.ToInt32(obj);
        }
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProImage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_ProImage(");
            strSql.Append("pro_id,pro_name,pro_number,pro_unit,pro_pmname,pro_status,jz_image,jz_cadimage,jg_image,jg_cadimage,jps_image,jps_cadimage,nt_image,nt_cadimage,dq_image,dq_cadimage,zh,image_type,guidang)");
            strSql.Append(" values (");
            strSql.Append("@pro_id,@pro_name,@pro_number,@pro_unit,@pro_pmname,@pro_status,@jz_image,@jz_cadimage,@jg_image,@jg_cadimage,@jps_image,@jps_cadimage,@nt_image,@nt_cadimage,@dq_image,@dq_cadimage,@zh,@image_type,@guidang)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@pro_id", SqlDbType.NVarChar,20),
					new SqlParameter("@pro_name", SqlDbType.NVarChar,50),
					new SqlParameter("@pro_number", SqlDbType.NVarChar,50),
					new SqlParameter("@pro_unit", SqlDbType.NVarChar,50),
					new SqlParameter("@pro_pmname", SqlDbType.NVarChar,50),
					new SqlParameter("@pro_status", SqlDbType.NVarChar,50),
					new SqlParameter("@jz_image", SqlDbType.NVarChar,50),
					new SqlParameter("@jz_cadimage", SqlDbType.NVarChar,50),
					new SqlParameter("@jg_image", SqlDbType.NVarChar,50),
					new SqlParameter("@jg_cadimage", SqlDbType.NVarChar,50),
					new SqlParameter("@jps_image", SqlDbType.NVarChar,50),
					new SqlParameter("@jps_cadimage", SqlDbType.NVarChar,50),
					new SqlParameter("@nt_image", SqlDbType.NVarChar,50),
					new SqlParameter("@nt_cadimage", SqlDbType.NVarChar,50),
					new SqlParameter("@dq_image", SqlDbType.NVarChar,50),
					new SqlParameter("@dq_cadimage", SqlDbType.NVarChar,50),
					new SqlParameter("@zh", SqlDbType.Int,4),
					new SqlParameter("@image_type", SqlDbType.NVarChar,50),
					new SqlParameter("@guidang", SqlDbType.Int,4)};
            parameters[0].Value = model.pro_id;
            parameters[1].Value = model.pro_name;
            parameters[2].Value = model.pro_number;
            parameters[3].Value = model.pro_unit;
            parameters[4].Value = model.pro_pmname;
            parameters[5].Value = model.pro_status;
            parameters[6].Value = model.jz_image;
            parameters[7].Value = model.jz_cadimage;
            parameters[8].Value = model.jg_image;
            parameters[9].Value = model.jg_cadimage;
            parameters[10].Value = model.jps_image;
            parameters[11].Value = model.jps_cadimage;
            parameters[12].Value = model.nt_image;
            parameters[13].Value = model.nt_cadimage;
            parameters[14].Value = model.dq_image;
            parameters[15].Value = model.dq_cadimage;
            parameters[16].Value = model.zh;
            parameters[17].Value = model.image_type;
            parameters[18].Value = model.guidang;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_ProImage model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ProImage set ");
            strSql.Append("pro_id=@pro_id,");
            strSql.Append("pro_name=@pro_name,");
            strSql.Append("pro_number=@pro_number,");
            strSql.Append("pro_unit=@pro_unit,");
            strSql.Append("pro_pmname=@pro_pmname,");
            strSql.Append("pro_status=@pro_status,");
            strSql.Append("jz_image=@jz_image,");
            strSql.Append("jz_cadimage=@jz_cadimage,");
            strSql.Append("jg_image=@jg_image,");
            strSql.Append("jg_cadimage=@jg_cadimage,");
            strSql.Append("jps_image=@jps_image,");
            strSql.Append("jps_cadimage=@jps_cadimage,");
            strSql.Append("nt_image=@nt_image,");
            strSql.Append("nt_cadimage=@nt_cadimage,");
            strSql.Append("dq_image=@dq_image,");
            strSql.Append("dq_cadimage=@dq_cadimage,");
            strSql.Append("zh=@zh,");
            strSql.Append("image_type=@image_type,");
            strSql.Append("guidang=@guidang");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@pro_id", SqlDbType.NVarChar,20),
					new SqlParameter("@pro_name", SqlDbType.NVarChar,50),
					new SqlParameter("@pro_number", SqlDbType.NVarChar,50),
					new SqlParameter("@pro_unit", SqlDbType.NVarChar,50),
					new SqlParameter("@pro_pmname", SqlDbType.NVarChar,50),
					new SqlParameter("@pro_status", SqlDbType.NVarChar,50),
					new SqlParameter("@jz_image", SqlDbType.NVarChar,50),
					new SqlParameter("@jz_cadimage", SqlDbType.NVarChar,50),
					new SqlParameter("@jg_image", SqlDbType.NVarChar,50),
					new SqlParameter("@jg_cadimage", SqlDbType.NVarChar,50),
					new SqlParameter("@jps_image", SqlDbType.NVarChar,50),
					new SqlParameter("@jps_cadimage", SqlDbType.NVarChar,50),
					new SqlParameter("@nt_image", SqlDbType.NVarChar,50),
					new SqlParameter("@nt_cadimage", SqlDbType.NVarChar,50),
					new SqlParameter("@dq_image", SqlDbType.NVarChar,50),
					new SqlParameter("@dq_cadimage", SqlDbType.NVarChar,50),
					new SqlParameter("@zh", SqlDbType.Int,4),
					new SqlParameter("@image_type", SqlDbType.NVarChar,50),
					new SqlParameter("@guidang", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.pro_id;
            parameters[1].Value = model.pro_name;
            parameters[2].Value = model.pro_number;
            parameters[3].Value = model.pro_unit;
            parameters[4].Value = model.pro_pmname;
            parameters[5].Value = model.pro_status;
            parameters[6].Value = model.jz_image;
            parameters[7].Value = model.jz_cadimage;
            parameters[8].Value = model.jg_image;
            parameters[9].Value = model.jg_cadimage;
            parameters[10].Value = model.jps_image;
            parameters[11].Value = model.jps_cadimage;
            parameters[12].Value = model.nt_image;
            parameters[13].Value = model.nt_cadimage;
            parameters[14].Value = model.dq_image;
            parameters[15].Value = model.dq_cadimage;
            parameters[16].Value = model.zh;
            parameters[17].Value = model.image_type;
            parameters[18].Value = model.guidang;
            parameters[19].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProImage ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProImage ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProImage GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,pro_id,pro_name,pro_number,pro_unit,pro_pmname,pro_status,jz_image,jz_cadimage,jg_image,jg_cadimage,jps_image,jps_cadimage,nt_image,nt_cadimage,dq_image,dq_cadimage,zh,image_type,guidang from cm_ProImage ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_ProImage model = new TG.Model.cm_ProImage();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_id"] != null && ds.Tables[0].Rows[0]["pro_id"].ToString() != "")
                {
                    model.pro_id = ds.Tables[0].Rows[0]["pro_id"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_name"] != null && ds.Tables[0].Rows[0]["pro_name"].ToString() != "")
                {
                    model.pro_name = ds.Tables[0].Rows[0]["pro_name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_number"] != null && ds.Tables[0].Rows[0]["pro_number"].ToString() != "")
                {
                    model.pro_number = ds.Tables[0].Rows[0]["pro_number"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_unit"] != null && ds.Tables[0].Rows[0]["pro_unit"].ToString() != "")
                {
                    model.pro_unit = ds.Tables[0].Rows[0]["pro_unit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_pmname"] != null && ds.Tables[0].Rows[0]["pro_pmname"].ToString() != "")
                {
                    model.pro_pmname = ds.Tables[0].Rows[0]["pro_pmname"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_status"] != null && ds.Tables[0].Rows[0]["pro_status"].ToString() != "")
                {
                    model.pro_status = ds.Tables[0].Rows[0]["pro_status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["jz_image"] != null && ds.Tables[0].Rows[0]["jz_image"].ToString() != "")
                {
                    model.jz_image = ds.Tables[0].Rows[0]["jz_image"].ToString();
                }
                if (ds.Tables[0].Rows[0]["jz_cadimage"] != null && ds.Tables[0].Rows[0]["jz_cadimage"].ToString() != "")
                {
                    model.jz_cadimage = ds.Tables[0].Rows[0]["jz_cadimage"].ToString();
                }
                if (ds.Tables[0].Rows[0]["jg_image"] != null && ds.Tables[0].Rows[0]["jg_image"].ToString() != "")
                {
                    model.jg_image = ds.Tables[0].Rows[0]["jg_image"].ToString();
                }
                if (ds.Tables[0].Rows[0]["jg_cadimage"] != null && ds.Tables[0].Rows[0]["jg_cadimage"].ToString() != "")
                {
                    model.jg_cadimage = ds.Tables[0].Rows[0]["jg_cadimage"].ToString();
                }
                if (ds.Tables[0].Rows[0]["jps_image"] != null && ds.Tables[0].Rows[0]["jps_image"].ToString() != "")
                {
                    model.jps_image = ds.Tables[0].Rows[0]["jps_image"].ToString();
                }
                if (ds.Tables[0].Rows[0]["jps_cadimage"] != null && ds.Tables[0].Rows[0]["jps_cadimage"].ToString() != "")
                {
                    model.jps_cadimage = ds.Tables[0].Rows[0]["jps_cadimage"].ToString();
                }
                if (ds.Tables[0].Rows[0]["nt_image"] != null && ds.Tables[0].Rows[0]["nt_image"].ToString() != "")
                {
                    model.nt_image = ds.Tables[0].Rows[0]["nt_image"].ToString();
                }
                if (ds.Tables[0].Rows[0]["nt_cadimage"] != null && ds.Tables[0].Rows[0]["nt_cadimage"].ToString() != "")
                {
                    model.nt_cadimage = ds.Tables[0].Rows[0]["nt_cadimage"].ToString();
                }
                if (ds.Tables[0].Rows[0]["dq_image"] != null && ds.Tables[0].Rows[0]["dq_image"].ToString() != "")
                {
                    model.dq_image = ds.Tables[0].Rows[0]["dq_image"].ToString();
                }
                if (ds.Tables[0].Rows[0]["dq_cadimage"] != null && ds.Tables[0].Rows[0]["dq_cadimage"].ToString() != "")
                {
                    model.dq_cadimage = ds.Tables[0].Rows[0]["dq_cadimage"].ToString();
                }
                if (ds.Tables[0].Rows[0]["zh"] != null && ds.Tables[0].Rows[0]["zh"].ToString() != "")
                {
                    model.zh = int.Parse(ds.Tables[0].Rows[0]["zh"].ToString());
                }
                if (ds.Tables[0].Rows[0]["image_type"] != null && ds.Tables[0].Rows[0]["image_type"].ToString() != "")
                {
                    model.image_type = ds.Tables[0].Rows[0]["image_type"].ToString();
                }
                if (ds.Tables[0].Rows[0]["guidang"] != null && ds.Tables[0].Rows[0]["guidang"].ToString() != "")
                {
                    model.guidang = int.Parse(ds.Tables[0].Rows[0]["guidang"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,pro_id,pro_name,pro_number,pro_unit,pro_pmname,pro_status,jz_image,jz_cadimage,jg_image,jg_cadimage,jps_image,jps_cadimage,nt_image,nt_cadimage,dq_image,dq_cadimage,zh,image_type,guidang ");
            strSql.Append(" FROM cm_ProImage ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,pro_id,pro_name,pro_number,pro_unit,pro_pmname,pro_status,jz_image,jz_cadimage,jg_image,jg_cadimage,jps_image,jps_cadimage,nt_image,nt_cadimage,dq_image,dq_cadimage,zh,image_type,guidang ");
            strSql.Append(" FROM cm_ProImage ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_ProImage ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_ProImage T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 得到项目信息
        /// </summary>
        /// <param name="proId"></param>
        /// <returns></returns>
        public DataSet GetProjectInfo(int proId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@" select Top 1 pj.pro_name,pj.Unit,pj.PMName,pj.pro_status ,
                          (SELECT pn.ProNumber from cm_ProjectNumber pn join cm_Project p on (pn.Pro_id=p.pro_ID) where p.pro_ID=@ID) as  gch,(SELECT dbo.fun_cm_ProImage(pj.CoperationSysNo)) as itemname from cm_Project  pj where pj.pro_ID=@ID");

            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = proId;

            return DbHelperSQL.Query(strSql.ToString(), parameters);

        }
        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "cm_ProImage";
            parameters[1].Value = "ID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}

