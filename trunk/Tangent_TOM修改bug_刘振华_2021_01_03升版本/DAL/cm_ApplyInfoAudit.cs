﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_ApplyInfoAudit
	/// </summary>
	public partial class cm_ApplyInfoAudit
	{
		public cm_ApplyInfoAudit()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("SysNo", "cm_ApplyInfoAudit"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SysNo)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_ApplyInfoAudit");
			strSql.Append(" where SysNo="+SysNo+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_ApplyInfoAudit model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.ApplyID != null)
			{
				strSql1.Append("ApplyID,");
				strSql2.Append(""+model.ApplyID+",");
			}
			if (model.Status != null)
			{
				strSql1.Append("Status,");
				strSql2.Append("'"+model.Status+"',");
			}
			if (model.Unitmanager != null)
			{
				strSql1.Append("Unitmanager,");
				strSql2.Append(""+model.Unitmanager+",");
			}
			if (model.Generalmanager != null)
			{
				strSql1.Append("Generalmanager,");
				strSql2.Append(""+model.Generalmanager+",");
			}
			if (model.AudtiDate != null)
			{
				strSql1.Append("AudtiDate,");
				strSql2.Append("'"+model.AudtiDate+"',");
			}
			if (model.Suggestion != null)
			{
				strSql1.Append("Suggestion,");
				strSql2.Append("'"+model.Suggestion+"',");
			}
			if (model.InDate != null)
			{
				strSql1.Append("InDate,");
				strSql2.Append("'"+model.InDate+"',");
			}
			if (model.InUser != null)
			{
				strSql1.Append("InUser,");
				strSql2.Append(""+model.InUser+",");
			}
			strSql.Append("insert into cm_ApplyInfoAudit(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ApplyInfoAudit model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_ApplyInfoAudit set ");
			if (model.ApplyID != null)
			{
				strSql.Append("ApplyID="+model.ApplyID+",");
			}
			if (model.Status != null)
			{
				strSql.Append("Status='"+model.Status+"',");
			}
			if (model.Unitmanager != null)
			{
				strSql.Append("Unitmanager="+model.Unitmanager+",");
			}
			else
			{
				strSql.Append("Unitmanager= null ,");
			}
			if (model.Generalmanager != null)
			{
				strSql.Append("Generalmanager="+model.Generalmanager+",");
			}
			else
			{
				strSql.Append("Generalmanager= null ,");
			}
			if (model.AudtiDate != null)
			{
				strSql.Append("AudtiDate='"+model.AudtiDate+"',");
			}
			else
			{
				strSql.Append("AudtiDate= null ,");
			}
			if (model.Suggestion != null)
			{
				strSql.Append("Suggestion='"+model.Suggestion+"',");
			}
			else
			{
				strSql.Append("Suggestion= null ,");
			}
			if (model.InDate != null)
			{
				strSql.Append("InDate='"+model.InDate+"',");
			}
			if (model.InUser != null)
			{
				strSql.Append("InUser="+model.InUser+",");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where SysNo="+ model.SysNo+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SysNo)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ApplyInfoAudit ");
			strSql.Append(" where SysNo="+SysNo+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SysNolist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ApplyInfoAudit ");
			strSql.Append(" where SysNo in ("+SysNolist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ApplyInfoAudit GetModel(int SysNo)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" SysNo,ApplyID,Status,Unitmanager,Generalmanager,AudtiDate,Suggestion,InDate,InUser ");
			strSql.Append(" from cm_ApplyInfoAudit ");
			strSql.Append(" where SysNo="+SysNo+"" );
			TG.Model.cm_ApplyInfoAudit model=new TG.Model.cm_ApplyInfoAudit();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["SysNo"]!=null && ds.Tables[0].Rows[0]["SysNo"].ToString()!="")
				{
					model.SysNo=int.Parse(ds.Tables[0].Rows[0]["SysNo"].ToString());
				}
				if(ds.Tables[0].Rows[0]["ApplyID"]!=null && ds.Tables[0].Rows[0]["ApplyID"].ToString()!="")
				{
					model.ApplyID=int.Parse(ds.Tables[0].Rows[0]["ApplyID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Status"]!=null && ds.Tables[0].Rows[0]["Status"].ToString()!="")
				{
					model.Status=ds.Tables[0].Rows[0]["Status"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Unitmanager"]!=null && ds.Tables[0].Rows[0]["Unitmanager"].ToString()!="")
				{
					model.Unitmanager=int.Parse(ds.Tables[0].Rows[0]["Unitmanager"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Generalmanager"]!=null && ds.Tables[0].Rows[0]["Generalmanager"].ToString()!="")
				{
					model.Generalmanager=int.Parse(ds.Tables[0].Rows[0]["Generalmanager"].ToString());
				}
				if(ds.Tables[0].Rows[0]["AudtiDate"]!=null && ds.Tables[0].Rows[0]["AudtiDate"].ToString()!="")
				{
					model.AudtiDate=ds.Tables[0].Rows[0]["AudtiDate"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Suggestion"]!=null && ds.Tables[0].Rows[0]["Suggestion"].ToString()!="")
				{
					model.Suggestion=ds.Tables[0].Rows[0]["Suggestion"].ToString();
				}
				if(ds.Tables[0].Rows[0]["InDate"]!=null && ds.Tables[0].Rows[0]["InDate"].ToString()!="")
				{
					model.InDate=DateTime.Parse(ds.Tables[0].Rows[0]["InDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["InUser"]!=null && ds.Tables[0].Rows[0]["InUser"].ToString()!="")
				{
					model.InUser=int.Parse(ds.Tables[0].Rows[0]["InUser"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SysNo,ApplyID,Status,Unitmanager,Generalmanager,AudtiDate,Suggestion,InDate,InUser ");
			strSql.Append(" FROM cm_ApplyInfoAudit ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SysNo,ApplyID,Status,Unitmanager,Generalmanager,AudtiDate,Suggestion,InDate,InUser ");
			strSql.Append(" FROM cm_ApplyInfoAudit ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_ApplyInfoAudit ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SysNo desc");
			}
			strSql.Append(")AS Row, T.*  from cm_ApplyInfoAudit T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

