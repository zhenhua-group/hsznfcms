﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_ContactPersionInfo
    /// </summary>
    public partial class cm_ContactPersionInfo
    {
        public cm_ContactPersionInfo()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("CP_Id", "cm_ContactPersionInfo");
        }


        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int CP_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_ContactPersionInfo");
            strSql.Append(" where CP_Id=" + CP_Id + " ");
            return DbHelperSQL.Exists(strSql.ToString());
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ContactPersionInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.Cst_Id != null)
            {
                strSql1.Append("Cst_Id,");
                strSql2.Append("" + model.Cst_Id + ",");
            }
            if (model.ContactNo != null)
            {
                strSql1.Append("ContactNo,");
                strSql2.Append("'" + model.ContactNo + "',");
            }
            if (model.Name != null)
            {
                strSql1.Append("Name,");
                strSql2.Append("'" + model.Name + "',");
            }
            if (model.Duties != null)
            {
                strSql1.Append("Duties,");
                strSql2.Append("'" + model.Duties + "',");
            }
            if (model.Department != null)
            {
                strSql1.Append("Department,");
                strSql2.Append("'" + model.Department + "',");
            }
            if (model.Phone != null)
            {
                strSql1.Append("Phone,");
                strSql2.Append("'" + model.Phone + "',");
            }
            if (model.BPhone != null)
            {
                strSql1.Append("BPhone,");
                strSql2.Append("'" + model.BPhone + "',");
            }
            if (model.FPhone != null)
            {
                strSql1.Append("FPhone,");
                strSql2.Append("'" + model.FPhone + "',");
            }
            if (model.FFax != null)
            {
                strSql1.Append("FFax,");
                strSql2.Append("'" + model.FFax + "',");
            }
            if (model.BFax != null)
            {
                strSql1.Append("BFax,");
                strSql2.Append("'" + model.BFax + "',");
            }
            if (model.Remark != null)
            {
                strSql1.Append("Remark,");
                strSql2.Append("'" + model.Remark + "',");
            }
            if (model.CallingCard != null)
            {
                strSql1.Append("CallingCard,");
                strSql2.Append("" + model.CallingCard + ",");
            }
            if (model.Email != null)
            {
                strSql1.Append("Email,");
                strSql2.Append("'" + model.Email + "',");
            }
            if (model.UpdateBy != null)
            {
                strSql1.Append("UpdateBy,");
                strSql2.Append("" + model.UpdateBy + ",");
            }
            if (model.LastUpdate != null)
            {
                strSql1.Append("LastUpdate,");
                strSql2.Append("'" + model.LastUpdate + "',");
            }
            strSql.Append("insert into cm_ContactPersionInfo(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_ContactPersionInfo model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ContactPersionInfo set ");
            if (model.Cst_Id != null)
            {
                strSql.Append("Cst_Id=" + model.Cst_Id + ",");
            }
            if (model.ContactNo != null)
            {
                strSql.Append("ContactNo='" + model.ContactNo + "',");
            }
            else
            {
                strSql.Append("ContactNo= null ,");
            }
            if (model.Name != null)
            {
                strSql.Append("Name='" + model.Name + "',");
            }
            else
            {
                strSql.Append("Name= null ,");
            }
            if (model.Duties != null)
            {
                strSql.Append("Duties='" + model.Duties + "',");
            }
            else
            {
                strSql.Append("Duties= null ,");
            }
            if (model.Department != null)
            {
                strSql.Append("Department='" + model.Department + "',");
            }
            else
            {
                strSql.Append("Department= null ,");
            }
            if (model.Phone != null)
            {
                strSql.Append("Phone='" + model.Phone + "',");
            }
            else
            {
                strSql.Append("Phone= null ,");
            }
            if (model.BPhone != null)
            {
                strSql.Append("BPhone='" + model.BPhone + "',");
            }
            else
            {
                strSql.Append("BPhone= null ,");
            }
            if (model.FPhone != null)
            {
                strSql.Append("FPhone='" + model.FPhone + "',");
            }
            else
            {
                strSql.Append("FPhone= null ,");
            }
            if (model.FFax != null)
            {
                strSql.Append("FFax='" + model.FFax + "',");
            }
            else
            {
                strSql.Append("FFax= null ,");
            }
            if (model.BFax != null)
            {
                strSql.Append("BFax='" + model.BFax + "',");
            }
            else
            {
                strSql.Append("BFax= null ,");
            }
            if (model.Remark != null)
            {
                strSql.Append("Remark='" + model.Remark + "',");
            }
            else
            {
                strSql.Append("Remark= null ,");
            }
            if (model.CallingCard != null)
            {
                strSql.Append("CallingCard=" + model.CallingCard + ",");
            }
            else
            {
                strSql.Append("CallingCard= null ,");
            }
            if (model.Email != null)
            {
                strSql.Append("Email='" + model.Email + "',");
            }
            else
            {
                strSql.Append("Email= null ,");
            }
            if (model.UpdateBy != null)
            {
                strSql.Append("UpdateBy=" + model.UpdateBy + ",");
            }
            else
            {
                strSql.Append("UpdateBy= null ,");
            }
            if (model.LastUpdate != null)
            {
                strSql.Append("LastUpdate='" + model.LastUpdate + "',");
            }
            else
            {
                strSql.Append("LastUpdate= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where CP_Id=" + model.CP_Id + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int CP_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ContactPersionInfo ");
            strSql.Append(" where CP_Id=" + CP_Id + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string CP_Idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ContactPersionInfo ");
            strSql.Append(" where CP_Id in (" + CP_Idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ContactPersionInfo GetModel(int CP_Id)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" CP_Id,Cst_Id,ContactNo,Name,Duties,Department,Phone,BPhone,FPhone,FFax,BFax,Remark,CallingCard,Email,UpdateBy,LastUpdate ");
            strSql.Append(" from cm_ContactPersionInfo ");
            strSql.Append(" where CP_Id=" + CP_Id + "");
            TG.Model.cm_ContactPersionInfo model = new TG.Model.cm_ContactPersionInfo();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["CP_Id"] != null && ds.Tables[0].Rows[0]["CP_Id"].ToString() != "")
                {
                    model.CP_Id = int.Parse(ds.Tables[0].Rows[0]["CP_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Cst_Id"] != null && ds.Tables[0].Rows[0]["Cst_Id"].ToString() != "")
                {
                    model.Cst_Id = Convert.ToDecimal(ds.Tables[0].Rows[0]["Cst_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ContactNo"] != null && ds.Tables[0].Rows[0]["ContactNo"].ToString() != "")
                {
                    model.ContactNo = ds.Tables[0].Rows[0]["ContactNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Name"] != null && ds.Tables[0].Rows[0]["Name"].ToString() != "")
                {
                    model.Name = ds.Tables[0].Rows[0]["Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Duties"] != null && ds.Tables[0].Rows[0]["Duties"].ToString() != "")
                {
                    model.Duties = ds.Tables[0].Rows[0]["Duties"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Department"] != null && ds.Tables[0].Rows[0]["Department"].ToString() != "")
                {
                    model.Department = ds.Tables[0].Rows[0]["Department"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Phone"] != null && ds.Tables[0].Rows[0]["Phone"].ToString() != "")
                {
                    model.Phone = ds.Tables[0].Rows[0]["Phone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BPhone"] != null && ds.Tables[0].Rows[0]["BPhone"].ToString() != "")
                {
                    model.BPhone = ds.Tables[0].Rows[0]["BPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FPhone"] != null && ds.Tables[0].Rows[0]["FPhone"].ToString() != "")
                {
                    model.FPhone = ds.Tables[0].Rows[0]["FPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FFax"] != null && ds.Tables[0].Rows[0]["FFax"].ToString() != "")
                {
                    model.FFax = ds.Tables[0].Rows[0]["FFax"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BFax"] != null && ds.Tables[0].Rows[0]["BFax"].ToString() != "")
                {
                    model.BFax = ds.Tables[0].Rows[0]["BFax"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Remark"] != null && ds.Tables[0].Rows[0]["Remark"].ToString() != "")
                {
                    model.Remark = ds.Tables[0].Rows[0]["Remark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CallingCard"] != null && ds.Tables[0].Rows[0]["CallingCard"].ToString() != "")
                {
                    model.CallingCard = (byte[])ds.Tables[0].Rows[0]["CallingCard"];
                }
                if (ds.Tables[0].Rows[0]["Email"] != null && ds.Tables[0].Rows[0]["Email"].ToString() != "")
                {
                    model.Email = ds.Tables[0].Rows[0]["Email"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UpdateBy"] != null && ds.Tables[0].Rows[0]["UpdateBy"].ToString() != "")
                {
                    model.UpdateBy = int.Parse(ds.Tables[0].Rows[0]["UpdateBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LastUpdate"] != null && ds.Tables[0].Rows[0]["LastUpdate"].ToString() != "")
                {
                    model.LastUpdate = DateTime.Parse(ds.Tables[0].Rows[0]["LastUpdate"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ContactPersionInfo GetModel(string CPWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" CP_Id,Cst_Id,ContactNo,Name,Duties,Department,Phone,BPhone,FPhone,FFax,BFax,Remark,CallingCard,Email,UpdateBy,LastUpdate ");
            strSql.Append(" from cm_ContactPersionInfo ");
            strSql.Append(" where  "+CPWhere);
            TG.Model.cm_ContactPersionInfo model = new TG.Model.cm_ContactPersionInfo();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["CP_Id"] != null && ds.Tables[0].Rows[0]["CP_Id"].ToString() != "")
                {
                    model.CP_Id = int.Parse(ds.Tables[0].Rows[0]["CP_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Cst_Id"] != null && ds.Tables[0].Rows[0]["Cst_Id"].ToString() != "")
                {
                    model.Cst_Id = Convert.ToDecimal(ds.Tables[0].Rows[0]["Cst_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ContactNo"] != null && ds.Tables[0].Rows[0]["ContactNo"].ToString() != "")
                {
                    model.ContactNo = ds.Tables[0].Rows[0]["ContactNo"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Name"] != null && ds.Tables[0].Rows[0]["Name"].ToString() != "")
                {
                    model.Name = ds.Tables[0].Rows[0]["Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Duties"] != null && ds.Tables[0].Rows[0]["Duties"].ToString() != "")
                {
                    model.Duties = ds.Tables[0].Rows[0]["Duties"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Department"] != null && ds.Tables[0].Rows[0]["Department"].ToString() != "")
                {
                    model.Department = ds.Tables[0].Rows[0]["Department"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Phone"] != null && ds.Tables[0].Rows[0]["Phone"].ToString() != "")
                {
                    model.Phone = ds.Tables[0].Rows[0]["Phone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BPhone"] != null && ds.Tables[0].Rows[0]["BPhone"].ToString() != "")
                {
                    model.BPhone = ds.Tables[0].Rows[0]["BPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FPhone"] != null && ds.Tables[0].Rows[0]["FPhone"].ToString() != "")
                {
                    model.FPhone = ds.Tables[0].Rows[0]["FPhone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["FFax"] != null && ds.Tables[0].Rows[0]["FFax"].ToString() != "")
                {
                    model.FFax = ds.Tables[0].Rows[0]["FFax"].ToString();
                }
                if (ds.Tables[0].Rows[0]["BFax"] != null && ds.Tables[0].Rows[0]["BFax"].ToString() != "")
                {
                    model.BFax = ds.Tables[0].Rows[0]["BFax"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Remark"] != null && ds.Tables[0].Rows[0]["Remark"].ToString() != "")
                {
                    model.Remark = ds.Tables[0].Rows[0]["Remark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CallingCard"] != null && ds.Tables[0].Rows[0]["CallingCard"].ToString() != "")
                {
                    model.CallingCard = (byte[])ds.Tables[0].Rows[0]["CallingCard"];
                }
                if (ds.Tables[0].Rows[0]["Email"] != null && ds.Tables[0].Rows[0]["Email"].ToString() != "")
                {
                    model.Email = ds.Tables[0].Rows[0]["Email"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UpdateBy"] != null && ds.Tables[0].Rows[0]["UpdateBy"].ToString() != "")
                {
                    model.UpdateBy = int.Parse(ds.Tables[0].Rows[0]["UpdateBy"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LastUpdate"] != null && ds.Tables[0].Rows[0]["LastUpdate"].ToString() != "")
                {
                    model.LastUpdate = DateTime.Parse(ds.Tables[0].Rows[0]["LastUpdate"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CP_Id,Cst_Id,ContactNo,Name,Duties,Department,Phone,BPhone,FPhone,FFax,BFax,Remark,CallingCard,Email,UpdateBy,LastUpdate,(Select InsertUserID From cm_CustomerInfo Where Cst_ID=CP.Cst_ID) AS InsertUserID ");
            strSql.Append(" FROM cm_ContactPersionInfo CP ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CP.CP_Id,CP.Cst_Id,CP.ContactNo,CP.Name,CP.Duties,CP.Department,CP.Phone,CP.BPhone,CP.FPhone,CP.FFax,CP.BFax,CP.Remark,CP.CallingCard,CP.Email,CP.UpdateBy,CP.LastUpdate ");
            strSql.Append(" FROM cm_ContactPersionInfo CP left join cm_CustomerInfo CT ON CP.CP_Id=CT.Cst_Id ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_ContactPersionInfo ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            /*没有调用此方法，无需修改。Created by Sago*/
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBE() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.CP_Id desc");
            }
            strSql.Append(")AS Row, T.*  from cm_ContactPersionInfo T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_ContactPersionInfo", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_ContactPersionInfo_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }



        /// <summary>
        /// 分阶段获得数据
        /// </summary>
        /// <param name="strWhere">where语句</param>
        /// <param name="pagesize">每页数量</param>
        /// <param name="pageIndex">要得到第几页的数据</param>
        /// <returns>数据集</returns>
        public DataSet GetListByPage(string strWhere, int pagesize, int pageIndex)
        {
            StringBuilder sqlStr = new StringBuilder();
            int notInTop = (pageIndex - 1) * pagesize;
            sqlStr.Append("select top " + pagesize + "* from cm_ContactPersionInfo where CP_Id not in ");
            sqlStr.Append(" (select TOP " + notInTop + " CP_Id from cm_ContactPersionInfo ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sqlStr.AppendFormat(" where ({0})", strWhere);
            }
            sqlStr.Append(")");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                sqlStr.AppendFormat(" and ({0})", strWhere);
            }
            return DbHelperSQL.Query(sqlStr.ToString());
        }
        /*
        */
        #endregion  Method
    }
}

