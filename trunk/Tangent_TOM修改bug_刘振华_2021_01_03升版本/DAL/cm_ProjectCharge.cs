﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_ProjectCharge
	/// </summary>
	public partial class cm_ProjectCharge
	{
		public cm_ProjectCharge()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_ProjectCharge model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.projID != null)
			{
				strSql1.Append("projID,");
				strSql2.Append(""+model.projID+",");
			}
			if (model.cprID != null)
			{
				strSql1.Append("cprID,");
				strSql2.Append(""+model.cprID+",");
			}
			if (model.Acount != null)
			{
				strSql1.Append("Acount,");
				strSql2.Append(""+model.Acount+",");
			}
			if (model.FromUser != null)
			{
				strSql1.Append("FromUser,");
				strSql2.Append("'"+model.FromUser+"',");
			}
			if (model.InAcountUser != null)
			{
				strSql1.Append("InAcountUser,");
				strSql2.Append("'"+model.InAcountUser+"',");
			}
			if (model.InAcountTime != null)
			{
				strSql1.Append("InAcountTime,");
				strSql2.Append("'"+model.InAcountTime+"',");
			}
			if (model.Status != null)
			{
				strSql1.Append("Status,");
				strSql2.Append("'"+model.Status+"',");
			}
            if (model.InAcountCode != null)
            {
                strSql1.Append("InAcountCode,");
                strSql2.Append("'" + model.InAcountCode + "',");
            }
            if (model.ProcessMark != null)
            {
                strSql1.Append("ProcessMark,");
                strSql2.Append("'" + model.ProcessMark + "',");
            }
            if (model.CaiwuMark != null)
            {
                strSql1.Append("CaiwuMark,");
                strSql2.Append("'" + model.CaiwuMark + "',");
            }
            if (model.SuoZhangMark != null)
            {
                strSql1.Append("SuoZhangMark,");
                strSql2.Append("'" + model.SuoZhangMark + "',");
            }
			strSql.Append("insert into cm_ProjectCharge(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ProjectCharge model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_ProjectCharge set ");
			if (model.projID != null)
			{
				strSql.Append("projID="+model.projID+",");
			}
			else
			{
				strSql.Append("projID= null ,");
			}
			if (model.cprID != null)
			{
				strSql.Append("cprID="+model.cprID+",");
			}
			else
			{
				strSql.Append("cprID= null ,");
			}
			if (model.Acount != null)
			{
				strSql.Append("Acount="+model.Acount+",");
			}
			else
			{
				strSql.Append("Acount= null ,");
			}
			if (model.FromUser != null)
			{
				strSql.Append("FromUser='"+model.FromUser+"',");
			}
			else
			{
				strSql.Append("FromUser= null ,");
			}
			if (model.InAcountUser != null)
			{
				strSql.Append("InAcountUser='"+model.InAcountUser+"',");
			}
			else
			{
				strSql.Append("InAcountUser= null ,");
			}
			if (model.InAcountTime != null)
			{
				strSql.Append("InAcountTime='"+model.InAcountTime+"',");
			}
			else
			{
				strSql.Append("InAcountTime= null ,");
			}
			if (model.Status != null)
			{
				strSql.Append("Status='"+model.Status+"',");
			}
			else
			{
				strSql.Append("Status= null ,");
			}
            if (model.InAcountCode != null)
            {
                strSql.Append("InAcountCode='" + model.InAcountCode + "',");
            }
            else
            {
                strSql.Append("InAcountCode= null ,");
            }
            if (model.ProcessMark != null)
            {
                strSql.Append("ProcessMark='" + model.ProcessMark + "',");
            }
            else
            {
                strSql.Append("ProcessMark= null ,");
            }
            if (model.CaiwuMark != null)
            {
                strSql.Append("CaiwuMark='" + model.CaiwuMark + "',");
            }
            else
            {
                strSql.Append("CaiwuMark= null ,");
            }
            if (model.SuoZhangMark != null)
            {
                strSql.Append("SuoZhangMark='" + model.SuoZhangMark + "',");
            }
            else
            {
                strSql.Append("SuoZhangMark= null ,");
            }
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ID="+ model.ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ProjectCharge ");
			strSql.Append(" where ID="+ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ProjectCharge ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ProjectCharge GetModel(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
            strSql.Append(" ID,projID,cprID,Acount,FromUser,InAcountUser,InAcountTime,Status,InAcountCode,ProcessMark,CaiwuMark,SuoZhangMark ");
			strSql.Append(" from cm_ProjectCharge ");
			strSql.Append(" where ID="+ID+"" );
			TG.Model.cm_ProjectCharge model=new TG.Model.cm_ProjectCharge();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["ID"]!=null && ds.Tables[0].Rows[0]["ID"].ToString()!="")
				{
					model.ID=int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["projID"]!=null && ds.Tables[0].Rows[0]["projID"].ToString()!="")
				{
					model.projID=int.Parse(ds.Tables[0].Rows[0]["projID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["cprID"]!=null && ds.Tables[0].Rows[0]["cprID"].ToString()!="")
				{
					model.cprID=int.Parse(ds.Tables[0].Rows[0]["cprID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Acount"]!=null && ds.Tables[0].Rows[0]["Acount"].ToString()!="")
				{
					model.Acount=decimal.Parse(ds.Tables[0].Rows[0]["Acount"].ToString());
				}
				if(ds.Tables[0].Rows[0]["FromUser"]!=null && ds.Tables[0].Rows[0]["FromUser"].ToString()!="")
				{
					model.FromUser=ds.Tables[0].Rows[0]["FromUser"].ToString();
				}
				if(ds.Tables[0].Rows[0]["InAcountUser"]!=null && ds.Tables[0].Rows[0]["InAcountUser"].ToString()!="")
				{
					model.InAcountUser=ds.Tables[0].Rows[0]["InAcountUser"].ToString();
				}
				if(ds.Tables[0].Rows[0]["InAcountTime"]!=null && ds.Tables[0].Rows[0]["InAcountTime"].ToString()!="")
				{
					model.InAcountTime=DateTime.Parse(ds.Tables[0].Rows[0]["InAcountTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Status"]!=null && ds.Tables[0].Rows[0]["Status"].ToString()!="")
				{
					model.Status=ds.Tables[0].Rows[0]["Status"].ToString();
				}
                if (ds.Tables[0].Rows[0]["InAcountCode"] != null && ds.Tables[0].Rows[0]["InAcountCode"].ToString() != "")
                {
                    model.InAcountCode = ds.Tables[0].Rows[0]["InAcountCode"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProcessMark"] != null && ds.Tables[0].Rows[0]["ProcessMark"].ToString() != "")
                {
                    model.ProcessMark = ds.Tables[0].Rows[0]["ProcessMark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["CaiwuMark"] != null && ds.Tables[0].Rows[0]["CaiwuMark"].ToString() != "")
                {
                    model.CaiwuMark = ds.Tables[0].Rows[0]["CaiwuMark"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SuoZhangMark"] != null && ds.Tables[0].Rows[0]["SuoZhangMark"].ToString() != "")
                {
                    model.SuoZhangMark = ds.Tables[0].Rows[0]["SuoZhangMark"].ToString();
                }
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select ID,projID,cprID,Acount,FromUser,InAcountUser,InAcountTime,Status,InAcountCode,ProcessMark,CaiwuMark,SuoZhangMark ");
			strSql.Append(" FROM cm_ProjectCharge ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" ID,projID,cprID,Acount,FromUser,InAcountUser,InAcountTime,Status,InAcountCode,ProcessMark,CaiwuMark,SuoZhangMark  ");
			strSql.Append(" FROM cm_ProjectCharge ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_ProjectCharge ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_ProjectCharge T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

        //根据条件获取项目合同总额
        public DataSet GetCoperationChargeCount(string query,string start,string end)
        {
            string strSql = " select SUM(Acount) AS CprChargeAcount From cm_ProjectCharge Where (Status<>'B') ";
            if (query != "")
            {
                strSql += " AND (cprID IN ( select cpr_ID From cm_Coperation Where 1=1 " + query + "))";
            }

            strSql += string.Format(" AND (InAcountTime between '{0}' and '{1}')", start, end);

            return DbHelperSQL.Query(strSql);
        }
        //获取项目合同收费
        public DataSet GetCoperationAcountCount(string query)
        {
            string strSql = " select SUM(cpr_Acount) AS CprAcount From cm_Coperation ";
            if (query != "")
            {
                strSql += " where 1=1 " + query;
            }

            return DbHelperSQL.Query(strSql);
        }
		#endregion  Method
	}
}

