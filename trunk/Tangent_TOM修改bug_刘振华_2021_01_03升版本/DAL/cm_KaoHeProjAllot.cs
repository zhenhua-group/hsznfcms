﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeProjAllot
	/// </summary>
	public partial class cm_KaoHeProjAllot
	{
		public cm_KaoHeProjAllot()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeProjAllot model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeProjAllot(");
			strSql.Append("ProId,ProKHId,ProKHNameId,Xnzcz,Xnzcz2,WcBl,Xmjjjs,Xmjjjs2,Jjbl,Jsjjzs,Jsjjzs2,Xmztjx,Sjjjzs,Sjjjzs2,Fashjtbl,Fashjj,Fashjj2,Zczjjtbl,Zcjx,Zcjj,Zcjj2,Xmjj,Xmjj2,Stat)");
			strSql.Append(" values (");
			strSql.Append("@ProId,@ProKHId,@ProKHNameId,@Xnzcz,@Xnzcz2,@WcBl,@Xmjjjs,@Xmjjjs2,@Jjbl,@Jsjjzs,@Jsjjzs2,@Xmztjx,@Sjjjzs,@Sjjjzs2,@Fashjtbl,@Fashjj,@Fashjj2,@Zczjjtbl,@Zcjx,@Zcjj,@Zcjj2,@Xmjj,@Xmjj2,@Stat)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ProId", SqlDbType.Int,4),
					new SqlParameter("@ProKHId", SqlDbType.Int,4),
					new SqlParameter("@ProKHNameId", SqlDbType.Int,4),
					new SqlParameter("@Xnzcz", SqlDbType.Decimal,9),
					new SqlParameter("@Xnzcz2", SqlDbType.Decimal,9),
					new SqlParameter("@WcBl", SqlDbType.Decimal,9),
					new SqlParameter("@Xmjjjs", SqlDbType.Decimal,9),
					new SqlParameter("@Xmjjjs2", SqlDbType.Decimal,9),
					new SqlParameter("@Jjbl", SqlDbType.Decimal,9),
					new SqlParameter("@Jsjjzs", SqlDbType.Decimal,9),
					new SqlParameter("@Jsjjzs2", SqlDbType.Decimal,9),
					new SqlParameter("@Xmztjx", SqlDbType.Decimal,9),
					new SqlParameter("@Sjjjzs", SqlDbType.Decimal,9),
					new SqlParameter("@Sjjjzs2", SqlDbType.Decimal,9),
					new SqlParameter("@Fashjtbl", SqlDbType.Decimal,9),
					new SqlParameter("@Fashjj", SqlDbType.Decimal,9),
					new SqlParameter("@Fashjj2", SqlDbType.Decimal,9),
					new SqlParameter("@Zczjjtbl", SqlDbType.Decimal,9),
					new SqlParameter("@Zcjx", SqlDbType.Decimal,9),
					new SqlParameter("@Zcjj", SqlDbType.Decimal,9),
					new SqlParameter("@Zcjj2", SqlDbType.Decimal,9),
					new SqlParameter("@Xmjj", SqlDbType.Decimal,9),
					new SqlParameter("@Xmjj2", SqlDbType.Decimal,9),
					new SqlParameter("@Stat", SqlDbType.Int,4)};
			parameters[0].Value = model.ProId;
			parameters[1].Value = model.ProKHId;
			parameters[2].Value = model.ProKHNameId;
			parameters[3].Value = model.Xnzcz;
			parameters[4].Value = model.Xnzcz2;
			parameters[5].Value = model.WcBl;
			parameters[6].Value = model.Xmjjjs;
			parameters[7].Value = model.Xmjjjs2;
			parameters[8].Value = model.Jjbl;
			parameters[9].Value = model.Jsjjzs;
			parameters[10].Value = model.Jsjjzs2;
			parameters[11].Value = model.Xmztjx;
			parameters[12].Value = model.Sjjjzs;
			parameters[13].Value = model.Sjjjzs2;
			parameters[14].Value = model.Fashjtbl;
			parameters[15].Value = model.Fashjj;
			parameters[16].Value = model.Fashjj2;
			parameters[17].Value = model.Zczjjtbl;
			parameters[18].Value = model.Zcjx;
			parameters[19].Value = model.Zcjj;
			parameters[20].Value = model.Zcjj2;
			parameters[21].Value = model.Xmjj;
			parameters[22].Value = model.Xmjj2;
			parameters[23].Value = model.Stat;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeProjAllot model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeProjAllot set ");
			strSql.Append("ProId=@ProId,");
			strSql.Append("ProKHId=@ProKHId,");
			strSql.Append("ProKHNameId=@ProKHNameId,");
			strSql.Append("Xnzcz=@Xnzcz,");
			strSql.Append("Xnzcz2=@Xnzcz2,");
			strSql.Append("WcBl=@WcBl,");
			strSql.Append("Xmjjjs=@Xmjjjs,");
			strSql.Append("Xmjjjs2=@Xmjjjs2,");
			strSql.Append("Jjbl=@Jjbl,");
			strSql.Append("Jsjjzs=@Jsjjzs,");
			strSql.Append("Jsjjzs2=@Jsjjzs2,");
			strSql.Append("Xmztjx=@Xmztjx,");
			strSql.Append("Sjjjzs=@Sjjjzs,");
			strSql.Append("Sjjjzs2=@Sjjjzs2,");
			strSql.Append("Fashjtbl=@Fashjtbl,");
			strSql.Append("Fashjj=@Fashjj,");
			strSql.Append("Fashjj2=@Fashjj2,");
			strSql.Append("Zczjjtbl=@Zczjjtbl,");
			strSql.Append("Zcjx=@Zcjx,");
			strSql.Append("Zcjj=@Zcjj,");
			strSql.Append("Zcjj2=@Zcjj2,");
			strSql.Append("Xmjj=@Xmjj,");
			strSql.Append("Xmjj2=@Xmjj2,");
			strSql.Append("Stat=@Stat");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ProId", SqlDbType.Int,4),
					new SqlParameter("@ProKHId", SqlDbType.Int,4),
					new SqlParameter("@ProKHNameId", SqlDbType.Int,4),
					new SqlParameter("@Xnzcz", SqlDbType.Decimal,9),
					new SqlParameter("@Xnzcz2", SqlDbType.Decimal,9),
					new SqlParameter("@WcBl", SqlDbType.Decimal,9),
					new SqlParameter("@Xmjjjs", SqlDbType.Decimal,9),
					new SqlParameter("@Xmjjjs2", SqlDbType.Decimal,9),
					new SqlParameter("@Jjbl", SqlDbType.Decimal,9),
					new SqlParameter("@Jsjjzs", SqlDbType.Decimal,9),
					new SqlParameter("@Jsjjzs2", SqlDbType.Decimal,9),
					new SqlParameter("@Xmztjx", SqlDbType.Decimal,9),
					new SqlParameter("@Sjjjzs", SqlDbType.Decimal,9),
					new SqlParameter("@Sjjjzs2", SqlDbType.Decimal,9),
					new SqlParameter("@Fashjtbl", SqlDbType.Decimal,9),
					new SqlParameter("@Fashjj", SqlDbType.Decimal,9),
					new SqlParameter("@Fashjj2", SqlDbType.Decimal,9),
					new SqlParameter("@Zczjjtbl", SqlDbType.Decimal,9),
					new SqlParameter("@Zcjx", SqlDbType.Decimal,9),
					new SqlParameter("@Zcjj", SqlDbType.Decimal,9),
					new SqlParameter("@Zcjj2", SqlDbType.Decimal,9),
					new SqlParameter("@Xmjj", SqlDbType.Decimal,9),
					new SqlParameter("@Xmjj2", SqlDbType.Decimal,9),
					new SqlParameter("@Stat", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.ProId;
			parameters[1].Value = model.ProKHId;
			parameters[2].Value = model.ProKHNameId;
			parameters[3].Value = model.Xnzcz;
			parameters[4].Value = model.Xnzcz2;
			parameters[5].Value = model.WcBl;
			parameters[6].Value = model.Xmjjjs;
			parameters[7].Value = model.Xmjjjs2;
			parameters[8].Value = model.Jjbl;
			parameters[9].Value = model.Jsjjzs;
			parameters[10].Value = model.Jsjjzs2;
			parameters[11].Value = model.Xmztjx;
			parameters[12].Value = model.Sjjjzs;
			parameters[13].Value = model.Sjjjzs2;
			parameters[14].Value = model.Fashjtbl;
			parameters[15].Value = model.Fashjj;
			parameters[16].Value = model.Fashjj2;
			parameters[17].Value = model.Zczjjtbl;
			parameters[18].Value = model.Zcjx;
			parameters[19].Value = model.Zcjj;
			parameters[20].Value = model.Zcjj2;
			parameters[21].Value = model.Xmjj;
			parameters[22].Value = model.Xmjj2;
			parameters[23].Value = model.Stat;
			parameters[24].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeProjAllot ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeProjAllot ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeProjAllot GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,ProId,ProKHId,ProKHNameId,Xnzcz,Xnzcz2,WcBl,Xmjjjs,Xmjjjs2,Jjbl,Jsjjzs,Jsjjzs2,Xmztjx,Sjjjzs,Sjjjzs2,Fashjtbl,Fashjj,Fashjj2,Zczjjtbl,Zcjx,Zcjj,Zcjj2,Xmjj,Xmjj2,Stat from cm_KaoHeProjAllot ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeProjAllot model=new TG.Model.cm_KaoHeProjAllot();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeProjAllot DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeProjAllot model=new TG.Model.cm_KaoHeProjAllot();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["ProId"]!=null && row["ProId"].ToString()!="")
				{
					model.ProId=int.Parse(row["ProId"].ToString());
				}
				if(row["ProKHId"]!=null && row["ProKHId"].ToString()!="")
				{
					model.ProKHId=int.Parse(row["ProKHId"].ToString());
				}
				if(row["ProKHNameId"]!=null && row["ProKHNameId"].ToString()!="")
				{
					model.ProKHNameId=int.Parse(row["ProKHNameId"].ToString());
				}
				if(row["Xnzcz"]!=null && row["Xnzcz"].ToString()!="")
				{
					model.Xnzcz=decimal.Parse(row["Xnzcz"].ToString());
				}
				if(row["Xnzcz2"]!=null && row["Xnzcz2"].ToString()!="")
				{
					model.Xnzcz2=decimal.Parse(row["Xnzcz2"].ToString());
				}
				if(row["WcBl"]!=null && row["WcBl"].ToString()!="")
				{
					model.WcBl=decimal.Parse(row["WcBl"].ToString());
				}
				if(row["Xmjjjs"]!=null && row["Xmjjjs"].ToString()!="")
				{
					model.Xmjjjs=decimal.Parse(row["Xmjjjs"].ToString());
				}
				if(row["Xmjjjs2"]!=null && row["Xmjjjs2"].ToString()!="")
				{
					model.Xmjjjs2=decimal.Parse(row["Xmjjjs2"].ToString());
				}
				if(row["Jjbl"]!=null && row["Jjbl"].ToString()!="")
				{
					model.Jjbl=decimal.Parse(row["Jjbl"].ToString());
				}
				if(row["Jsjjzs"]!=null && row["Jsjjzs"].ToString()!="")
				{
					model.Jsjjzs=decimal.Parse(row["Jsjjzs"].ToString());
				}
				if(row["Jsjjzs2"]!=null && row["Jsjjzs2"].ToString()!="")
				{
					model.Jsjjzs2=decimal.Parse(row["Jsjjzs2"].ToString());
				}
				if(row["Xmztjx"]!=null && row["Xmztjx"].ToString()!="")
				{
					model.Xmztjx=decimal.Parse(row["Xmztjx"].ToString());
				}
				if(row["Sjjjzs"]!=null && row["Sjjjzs"].ToString()!="")
				{
					model.Sjjjzs=decimal.Parse(row["Sjjjzs"].ToString());
				}
				if(row["Sjjjzs2"]!=null && row["Sjjjzs2"].ToString()!="")
				{
					model.Sjjjzs2=decimal.Parse(row["Sjjjzs2"].ToString());
				}
				if(row["Fashjtbl"]!=null && row["Fashjtbl"].ToString()!="")
				{
					model.Fashjtbl=decimal.Parse(row["Fashjtbl"].ToString());
				}
				if(row["Fashjj"]!=null && row["Fashjj"].ToString()!="")
				{
					model.Fashjj=decimal.Parse(row["Fashjj"].ToString());
				}
				if(row["Fashjj2"]!=null && row["Fashjj2"].ToString()!="")
				{
					model.Fashjj2=decimal.Parse(row["Fashjj2"].ToString());
				}
				if(row["Zczjjtbl"]!=null && row["Zczjjtbl"].ToString()!="")
				{
					model.Zczjjtbl=decimal.Parse(row["Zczjjtbl"].ToString());
				}
				if(row["Zcjx"]!=null && row["Zcjx"].ToString()!="")
				{
					model.Zcjx=decimal.Parse(row["Zcjx"].ToString());
				}
				if(row["Zcjj"]!=null && row["Zcjj"].ToString()!="")
				{
					model.Zcjj=decimal.Parse(row["Zcjj"].ToString());
				}
				if(row["Zcjj2"]!=null && row["Zcjj2"].ToString()!="")
				{
					model.Zcjj2=decimal.Parse(row["Zcjj2"].ToString());
				}
				if(row["Xmjj"]!=null && row["Xmjj"].ToString()!="")
				{
					model.Xmjj=decimal.Parse(row["Xmjj"].ToString());
				}
				if(row["Xmjj2"]!=null && row["Xmjj2"].ToString()!="")
				{
					model.Xmjj2=decimal.Parse(row["Xmjj2"].ToString());
				}
				if(row["Stat"]!=null && row["Stat"].ToString()!="")
				{
					model.Stat=int.Parse(row["Stat"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,ProId,ProKHId,ProKHNameId,Xnzcz,Xnzcz2,WcBl,Xmjjjs,Xmjjjs2,Jjbl,Jsjjzs,Jsjjzs2,Xmztjx,Sjjjzs,Sjjjzs2,Fashjtbl,Fashjj,Fashjj2,Zczjjtbl,Zcjx,Zcjj,Zcjj2,Xmjj,Xmjj2,Stat ");
			strSql.Append(" FROM cm_KaoHeProjAllot ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,ProId,ProKHId,ProKHNameId,Xnzcz,Xnzcz2,WcBl,Xmjjjs,Xmjjjs2,Jjbl,Jsjjzs,Jsjjzs2,Xmztjx,Sjjjzs,Sjjjzs2,Fashjtbl,Fashjj,Fashjj2,Zczjjtbl,Zcjx,Zcjj,Zcjj2,Xmjj,Xmjj2,Stat ");
			strSql.Append(" FROM cm_KaoHeProjAllot ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeProjAllot ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeProjAllot T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeProjAllot";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

