﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.DBUtility;

namespace TG.DAL
{
    public partial class CostImportBymaster
    {
        TG.DAL.cm_CostDetails dal = new TG.DAL.cm_CostDetails();
        public DataSet GetVoucherNo()
        {
            string sql = @"SELECT costNum FROM cm_CostDetails ";

            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);

            return ds;
        }
        /// <summary>
        /// 批量插入数据
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public int InsertFinancial(DataTable dt)
        {
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };

            SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);
            bulkCopy.BulkCopyTimeout = 1000;
            bulkCopy.DestinationTableName = "cm_CostDetails";
            bulkCopy.BatchSize = dt.Rows.Count;

            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                //映射字段名 DataTable列名 ,数据库 对应的列名 
                bulkCopy.ColumnMappings.Add("costTime", "costTime");
                bulkCopy.ColumnMappings.Add("costNum", "costNum");
                bulkCopy.ColumnMappings.Add("costSub", "costSub");
                bulkCopy.ColumnMappings.Add("costCharge", "costCharge");
                bulkCopy.ColumnMappings.Add("costOutCharge", "costOutCharge");
                bulkCopy.ColumnMappings.Add("costUnit", "costUnit");
                bulkCopy.ColumnMappings.Add("costUserId", "costUserId");
                bulkCopy.ColumnMappings.Add("costTypeID", "costTypeID");


                if (dt != null && dt.Rows.Count != 0)
                {
                    //批量数据插入
                    bulkCopy.WriteToServer(dt);
                }

                return dt.Rows.Count;
            }
            catch (Exception ex)
            {
                if (bulkCopy != null)
                    bulkCopy.Close();
                return 0;
            }
            finally
            {
                if (bulkCopy != null)
                    bulkCopy.Close();
                conn.Close();
            }
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_CostImportBymaster_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetListByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_CostImportBymaster", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        /// <summary>P_cm_ShowCostDetails
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetCostListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_ShowCostDetails_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 分页存储过程
        /// </summary>
        public SqlDataReader GetCostListByPageProc(string query, int startIndex, int endIndex)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_ShowCostDetails", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });
        }
        public object GetCostAllByProc(string query, string param1, string param2)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_CostAllDetails", new SqlParameter[] { new SqlParameter("@where1", param1), new SqlParameter("@where3", query), new SqlParameter("@where2", param2) });

        }
        public object GetAllCostByProc(string query, string param1, string param2,string costgroup)
        {
            return DBUtility.DbHelperSQL.RunProcedure("P_cm_AllCostDetails", new SqlParameter[] { new SqlParameter("@where1", param1), new SqlParameter("@costgroup", costgroup), new SqlParameter("@where3", query), new SqlParameter("@where2", param2) });
        }
    }
}
