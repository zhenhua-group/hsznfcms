﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:tg_unitExt
	/// </summary>
	public partial class tg_unitExt
	{
		public tg_unitExt()
		{}
		#region  BasicMethod

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("unit_ID", "tg_unitExt"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int unit_ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from tg_unitExt");
			strSql.Append(" where unit_ID=@unit_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@unit_ID", SqlDbType.Int,4)			};
			parameters[0].Value = unit_ID;

			return DbHelperSQL.Exists(strSql.ToString(),parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(TG.Model.tg_unitExt model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into tg_unitExt(");
			strSql.Append("unit_ID,unit_Name,unit_Type,unit_Order)");
			strSql.Append(" values (");
			strSql.Append("@unit_ID,@unit_Name,@unit_Type,@unit_Order)");
			SqlParameter[] parameters = {
					new SqlParameter("@unit_ID", SqlDbType.Int,4),
					new SqlParameter("@unit_Name", SqlDbType.Char,30),
					new SqlParameter("@unit_Type", SqlDbType.Int,4),
					new SqlParameter("@unit_Order", SqlDbType.Int,4)};
			parameters[0].Value = model.unit_ID;
			parameters[1].Value = model.unit_Name;
			parameters[2].Value = model.unit_Type;
			parameters[3].Value = model.unit_Order;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_unitExt model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update tg_unitExt set ");
			strSql.Append("unit_Name=@unit_Name,");
			strSql.Append("unit_Type=@unit_Type,");
			strSql.Append("unit_Order=@unit_Order");
			strSql.Append(" where unit_ID=@unit_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@unit_Name", SqlDbType.Char,30),
					new SqlParameter("@unit_Type", SqlDbType.Int,4),
					new SqlParameter("@unit_Order", SqlDbType.Int,4),
					new SqlParameter("@unit_ID", SqlDbType.Int,4)};
			parameters[0].Value = model.unit_Name;
			parameters[1].Value = model.unit_Type;
			parameters[2].Value = model.unit_Order;
			parameters[3].Value = model.unit_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int unit_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_unitExt ");
			strSql.Append(" where unit_ID=@unit_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@unit_ID", SqlDbType.Int,4)			};
			parameters[0].Value = unit_ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string unit_IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from tg_unitExt ");
			strSql.Append(" where unit_ID in ("+unit_IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_unitExt GetModel(int unit_ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 unit_ID,unit_Name,unit_Type,unit_Order from tg_unitExt ");
			strSql.Append(" where unit_ID=@unit_ID ");
			SqlParameter[] parameters = {
					new SqlParameter("@unit_ID", SqlDbType.Int,4)			};
			parameters[0].Value = unit_ID;

			TG.Model.tg_unitExt model=new TG.Model.tg_unitExt();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_unitExt DataRowToModel(DataRow row)
		{
			TG.Model.tg_unitExt model=new TG.Model.tg_unitExt();
			if (row != null)
			{
				if(row["unit_ID"]!=null && row["unit_ID"].ToString()!="")
				{
					model.unit_ID=int.Parse(row["unit_ID"].ToString());
				}
				if(row["unit_Name"]!=null)
				{
					model.unit_Name=row["unit_Name"].ToString();
				}
				if(row["unit_Type"]!=null && row["unit_Type"].ToString()!="")
				{
					model.unit_Type=int.Parse(row["unit_Type"].ToString());
				}
				if(row["unit_Order"]!=null && row["unit_Order"].ToString()!="")
				{
					model.unit_Order=int.Parse(row["unit_Order"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select unit_ID,unit_Name,unit_Type,unit_Order ");
			strSql.Append(" FROM tg_unitExt ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" unit_ID,unit_Name,unit_Type,unit_Order ");
			strSql.Append(" FROM tg_unitExt ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM tg_unitExt ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.unit_ID desc");
			}
			strSql.Append(")AS Row, T.*  from tg_unitExt T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "tg_unitExt";
			parameters[1].Value = "unit_ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

