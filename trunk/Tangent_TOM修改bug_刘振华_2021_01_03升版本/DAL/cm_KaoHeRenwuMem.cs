﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeRenwuMem
	/// </summary>
	public partial class cm_KaoHeRenwuMem
	{
		public cm_KaoHeRenwuMem()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeRenwuMem model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeRenwuMem(");
			strSql.Append("KaoHeUnitID,memID,memName,memUnitID,memUnitName,Statu,MagRole,NormalRole,Weights,IsOutUnitID)");
			strSql.Append(" values (");
			strSql.Append("@KaoHeUnitID,@memID,@memName,@memUnitID,@memUnitName,@Statu,@MagRole,@NormalRole,@Weights,@IsOutUnitID)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@KaoHeUnitID", SqlDbType.Int,4),
					new SqlParameter("@memID", SqlDbType.Int,4),
					new SqlParameter("@memName", SqlDbType.NVarChar,20),
					new SqlParameter("@memUnitID", SqlDbType.Int,4),
					new SqlParameter("@memUnitName", SqlDbType.NVarChar,20),
					new SqlParameter("@Statu", SqlDbType.Int,4),
					new SqlParameter("@MagRole", SqlDbType.Int,4),
					new SqlParameter("@NormalRole", SqlDbType.Int,4),
					new SqlParameter("@Weights", SqlDbType.Decimal,9),
					new SqlParameter("@IsOutUnitID", SqlDbType.Int,4)};
			parameters[0].Value = model.KaoHeUnitID;
			parameters[1].Value = model.memID;
			parameters[2].Value = model.memName;
			parameters[3].Value = model.memUnitID;
			parameters[4].Value = model.memUnitName;
			parameters[5].Value = model.Statu;
			parameters[6].Value = model.MagRole;
			parameters[7].Value = model.NormalRole;
			parameters[8].Value = model.Weights;
			parameters[9].Value = model.IsOutUnitID;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeRenwuMem model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeRenwuMem set ");
			strSql.Append("KaoHeUnitID=@KaoHeUnitID,");
			strSql.Append("memID=@memID,");
			strSql.Append("memName=@memName,");
			strSql.Append("memUnitID=@memUnitID,");
			strSql.Append("memUnitName=@memUnitName,");
			strSql.Append("Statu=@Statu,");
			strSql.Append("MagRole=@MagRole,");
			strSql.Append("NormalRole=@NormalRole,");
			strSql.Append("Weights=@Weights,");
			strSql.Append("IsOutUnitID=@IsOutUnitID");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@KaoHeUnitID", SqlDbType.Int,4),
					new SqlParameter("@memID", SqlDbType.Int,4),
					new SqlParameter("@memName", SqlDbType.NVarChar,20),
					new SqlParameter("@memUnitID", SqlDbType.Int,4),
					new SqlParameter("@memUnitName", SqlDbType.NVarChar,20),
					new SqlParameter("@Statu", SqlDbType.Int,4),
					new SqlParameter("@MagRole", SqlDbType.Int,4),
					new SqlParameter("@NormalRole", SqlDbType.Int,4),
					new SqlParameter("@Weights", SqlDbType.Decimal,9),
					new SqlParameter("@IsOutUnitID", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.KaoHeUnitID;
			parameters[1].Value = model.memID;
			parameters[2].Value = model.memName;
			parameters[3].Value = model.memUnitID;
			parameters[4].Value = model.memUnitName;
			parameters[5].Value = model.Statu;
			parameters[6].Value = model.MagRole;
			parameters[7].Value = model.NormalRole;
			parameters[8].Value = model.Weights;
			parameters[9].Value = model.IsOutUnitID;
			parameters[10].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeRenwuMem ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeRenwuMem ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeRenwuMem GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,KaoHeUnitID,memID,memName,memUnitID,memUnitName,Statu,MagRole,NormalRole,Weights,IsOutUnitID from cm_KaoHeRenwuMem ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeRenwuMem model=new TG.Model.cm_KaoHeRenwuMem();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeRenwuMem DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeRenwuMem model=new TG.Model.cm_KaoHeRenwuMem();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["KaoHeUnitID"]!=null && row["KaoHeUnitID"].ToString()!="")
				{
					model.KaoHeUnitID=int.Parse(row["KaoHeUnitID"].ToString());
				}
				if(row["memID"]!=null && row["memID"].ToString()!="")
				{
					model.memID=int.Parse(row["memID"].ToString());
				}
				if(row["memName"]!=null)
				{
					model.memName=row["memName"].ToString();
				}
				if(row["memUnitID"]!=null && row["memUnitID"].ToString()!="")
				{
					model.memUnitID=int.Parse(row["memUnitID"].ToString());
				}
				if(row["memUnitName"]!=null)
				{
					model.memUnitName=row["memUnitName"].ToString();
				}
				if(row["Statu"]!=null && row["Statu"].ToString()!="")
				{
					model.Statu=int.Parse(row["Statu"].ToString());
				}
				if(row["MagRole"]!=null && row["MagRole"].ToString()!="")
				{
					model.MagRole=int.Parse(row["MagRole"].ToString());
				}
				if(row["NormalRole"]!=null && row["NormalRole"].ToString()!="")
				{
					model.NormalRole=int.Parse(row["NormalRole"].ToString());
				}
				if(row["Weights"]!=null && row["Weights"].ToString()!="")
				{
					model.Weights=decimal.Parse(row["Weights"].ToString());
				}
				if(row["IsOutUnitID"]!=null && row["IsOutUnitID"].ToString()!="")
				{
					model.IsOutUnitID=int.Parse(row["IsOutUnitID"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,KaoHeUnitID,memID,memName,memUnitID,memUnitName,Statu,MagRole,NormalRole,Weights,IsOutUnitID ");
			strSql.Append(" FROM cm_KaoHeRenwuMem ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,KaoHeUnitID,memID,memName,memUnitID,memUnitName,Statu,MagRole,NormalRole,Weights,IsOutUnitID ");
			strSql.Append(" FROM cm_KaoHeRenwuMem ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeRenwuMem ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeRenwuMem T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeRenwuMem";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

