﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_YearAllStatisHis
	/// </summary>
	public partial class cm_YearAllStatisHis
	{
		public cm_YearAllStatisHis()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("ID", "cm_YearAllStatisHis"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_YearAllStatisHis");
			strSql.Append(" where ID="+ID+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_YearAllStatisHis model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.mem_id != null)
			{
				strSql1.Append("mem_id,");
				strSql2.Append(""+model.mem_id+",");
			}
			if (model.mem_name != null)
			{
				strSql1.Append("mem_name,");
				strSql2.Append("'"+model.mem_name+"',");
			}
			if (model.mem_unit_ID != null)
			{
				strSql1.Append("mem_unit_ID,");
				strSql2.Append(""+model.mem_unit_ID+",");
			}
			if (model.dataDate != null)
			{
				strSql1.Append("dataDate,");
				strSql2.Append("'"+model.dataDate+"',");
			}
			if (model.yearDay != null)
			{
				strSql1.Append("yearDay,");
				strSql2.Append(""+model.yearDay+",");
			}
			if (model.yearHour != null)
			{
				strSql1.Append("yearHour,");
				strSql2.Append(""+model.yearHour+",");
			}
			if (model.lastyearHour != null)
			{
				strSql1.Append("lastyearHour,");
				strSql2.Append(""+model.lastyearHour+",");
			}
			if (model.time_1231 != null)
			{
				strSql1.Append("time_1231,");
				strSql2.Append(""+model.time_1231+",");
			}
			if (model.time_115 != null)
			{
				strSql1.Append("time_115,");
				strSql2.Append(""+model.time_115+",");
			}
			if (model.time_215 != null)
			{
				strSql1.Append("time_215,");
				strSql2.Append(""+model.time_215+",");
			}
			if (model.time_228 != null)
			{
				strSql1.Append("time_228,");
				strSql2.Append(""+model.time_228+",");
			}
			if (model.time_315 != null)
			{
				strSql1.Append("time_315,");
				strSql2.Append(""+model.time_315+",");
			}
			if (model.time_415 != null)
			{
				strSql1.Append("time_415,");
				strSql2.Append(""+model.time_415+",");
			}
			if (model.time_515 != null)
			{
				strSql1.Append("time_515,");
				strSql2.Append(""+model.time_515+",");
			}
			if (model.time_615 != null)
			{
				strSql1.Append("time_615,");
				strSql2.Append(""+model.time_615+",");
			}
			if (model.time_715 != null)
			{
				strSql1.Append("time_715,");
				strSql2.Append(""+model.time_715+",");
			}
			if (model.time_815 != null)
			{
				strSql1.Append("time_815,");
				strSql2.Append(""+model.time_815+",");
			}
			if (model.time_915 != null)
			{
				strSql1.Append("time_915,");
				strSql2.Append(""+model.time_915+",");
			}
			if (model.time_1015 != null)
			{
				strSql1.Append("time_1015,");
				strSql2.Append(""+model.time_1015+",");
			}
			if (model.time_1115 != null)
			{
				strSql1.Append("time_1115,");
				strSql2.Append(""+model.time_1115+",");
			}
			if (model.time_1215 != null)
			{
				strSql1.Append("time_1215,");
				strSql2.Append(""+model.time_1215+",");
			}
			if (model.nextyearHour != null)
			{
				strSql1.Append("nextyearHour,");
				strSql2.Append(""+model.nextyearHour+",");
			}
			strSql.Append("insert into cm_YearAllStatisHis(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_YearAllStatisHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_YearAllStatisHis set ");
			if (model.mem_id != null)
			{
				strSql.Append("mem_id="+model.mem_id+",");
			}
			if (model.mem_name != null)
			{
				strSql.Append("mem_name='"+model.mem_name+"',");
			}
			if (model.mem_unit_ID != null)
			{
				strSql.Append("mem_unit_ID="+model.mem_unit_ID+",");
			}
			if (model.dataDate != null)
			{
				strSql.Append("dataDate='"+model.dataDate+"',");
			}
			if (model.yearDay != null)
			{
				strSql.Append("yearDay="+model.yearDay+",");
			}
			else
			{
				strSql.Append("yearDay= null ,");
			}
			if (model.yearHour != null)
			{
				strSql.Append("yearHour="+model.yearHour+",");
			}
			else
			{
				strSql.Append("yearHour= null ,");
			}
			if (model.lastyearHour != null)
			{
				strSql.Append("lastyearHour="+model.lastyearHour+",");
			}
			else
			{
				strSql.Append("lastyearHour= null ,");
			}
			if (model.time_1231 != null)
			{
				strSql.Append("time_1231="+model.time_1231+",");
			}
			else
			{
				strSql.Append("time_1231= null ,");
			}
			if (model.time_115 != null)
			{
				strSql.Append("time_115="+model.time_115+",");
			}
			else
			{
				strSql.Append("time_115= null ,");
			}
			if (model.time_215 != null)
			{
				strSql.Append("time_215="+model.time_215+",");
			}
			else
			{
				strSql.Append("time_215= null ,");
			}
			if (model.time_228 != null)
			{
				strSql.Append("time_228="+model.time_228+",");
			}
			else
			{
				strSql.Append("time_228= null ,");
			}
			if (model.time_315 != null)
			{
				strSql.Append("time_315="+model.time_315+",");
			}
			else
			{
				strSql.Append("time_315= null ,");
			}
			if (model.time_415 != null)
			{
				strSql.Append("time_415="+model.time_415+",");
			}
			else
			{
				strSql.Append("time_415= null ,");
			}
			if (model.time_515 != null)
			{
				strSql.Append("time_515="+model.time_515+",");
			}
			else
			{
				strSql.Append("time_515= null ,");
			}
			if (model.time_615 != null)
			{
				strSql.Append("time_615="+model.time_615+",");
			}
			else
			{
				strSql.Append("time_615= null ,");
			}
			if (model.time_715 != null)
			{
				strSql.Append("time_715="+model.time_715+",");
			}
			else
			{
				strSql.Append("time_715= null ,");
			}
			if (model.time_815 != null)
			{
				strSql.Append("time_815="+model.time_815+",");
			}
			else
			{
				strSql.Append("time_815= null ,");
			}
			if (model.time_915 != null)
			{
				strSql.Append("time_915="+model.time_915+",");
			}
			else
			{
				strSql.Append("time_915= null ,");
			}
			if (model.time_1015 != null)
			{
				strSql.Append("time_1015="+model.time_1015+",");
			}
			else
			{
				strSql.Append("time_1015= null ,");
			}
			if (model.time_1115 != null)
			{
				strSql.Append("time_1115="+model.time_1115+",");
			}
			else
			{
				strSql.Append("time_1115= null ,");
			}
			if (model.time_1215 != null)
			{
				strSql.Append("time_1215="+model.time_1215+",");
			}
			else
			{
				strSql.Append("time_1215= null ,");
			}
			if (model.nextyearHour != null)
			{
				strSql.Append("nextyearHour="+model.nextyearHour+",");
			}
			else
			{
				strSql.Append("nextyearHour= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ID="+ model.ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_YearAllStatisHis ");
			strSql.Append(" where ID="+ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_YearAllStatisHis ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_YearAllStatisHis GetModel(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" ID,mem_id,mem_name,mem_unit_ID,dataDate,yearDay,yearHour,lastyearHour,time_1231,time_115,time_215,time_228,time_315,time_415,time_515,time_615,time_715,time_815,time_915,time_1015,time_1115,time_1215,nextyearHour ");
			strSql.Append(" from cm_YearAllStatisHis ");
			strSql.Append(" where ID="+ID+"" );
			TG.Model.cm_YearAllStatisHis model=new TG.Model.cm_YearAllStatisHis();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_YearAllStatisHis DataRowToModel(DataRow row)
		{
			TG.Model.cm_YearAllStatisHis model=new TG.Model.cm_YearAllStatisHis();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["mem_id"]!=null && row["mem_id"].ToString()!="")
				{
					model.mem_id=int.Parse(row["mem_id"].ToString());
				}
				if(row["mem_name"]!=null)
				{
					model.mem_name=row["mem_name"].ToString();
				}
				if(row["mem_unit_ID"]!=null && row["mem_unit_ID"].ToString()!="")
				{
					model.mem_unit_ID=int.Parse(row["mem_unit_ID"].ToString());
				}
				if(row["dataDate"]!=null)
				{
					model.dataDate=row["dataDate"].ToString();
				}
				if(row["yearDay"]!=null && row["yearDay"].ToString()!="")
				{
					model.yearDay=int.Parse(row["yearDay"].ToString());
				}
				if(row["yearHour"]!=null && row["yearHour"].ToString()!="")
				{
					model.yearHour=decimal.Parse(row["yearHour"].ToString());
				}
				if(row["lastyearHour"]!=null && row["lastyearHour"].ToString()!="")
				{
					model.lastyearHour=decimal.Parse(row["lastyearHour"].ToString());
				}
				if(row["time_1231"]!=null && row["time_1231"].ToString()!="")
				{
					model.time_1231=decimal.Parse(row["time_1231"].ToString());
				}
				if(row["time_115"]!=null && row["time_115"].ToString()!="")
				{
					model.time_115=decimal.Parse(row["time_115"].ToString());
				}
				if(row["time_215"]!=null && row["time_215"].ToString()!="")
				{
					model.time_215=decimal.Parse(row["time_215"].ToString());
				}
				if(row["time_228"]!=null && row["time_228"].ToString()!="")
				{
					model.time_228=decimal.Parse(row["time_228"].ToString());
				}
				if(row["time_315"]!=null && row["time_315"].ToString()!="")
				{
					model.time_315=decimal.Parse(row["time_315"].ToString());
				}
				if(row["time_415"]!=null && row["time_415"].ToString()!="")
				{
					model.time_415=decimal.Parse(row["time_415"].ToString());
				}
				if(row["time_515"]!=null && row["time_515"].ToString()!="")
				{
					model.time_515=decimal.Parse(row["time_515"].ToString());
				}
				if(row["time_615"]!=null && row["time_615"].ToString()!="")
				{
					model.time_615=decimal.Parse(row["time_615"].ToString());
				}
				if(row["time_715"]!=null && row["time_715"].ToString()!="")
				{
					model.time_715=decimal.Parse(row["time_715"].ToString());
				}
				if(row["time_815"]!=null && row["time_815"].ToString()!="")
				{
					model.time_815=decimal.Parse(row["time_815"].ToString());
				}
				if(row["time_915"]!=null && row["time_915"].ToString()!="")
				{
					model.time_915=decimal.Parse(row["time_915"].ToString());
				}
				if(row["time_1015"]!=null && row["time_1015"].ToString()!="")
				{
					model.time_1015=decimal.Parse(row["time_1015"].ToString());
				}
				if(row["time_1115"]!=null && row["time_1115"].ToString()!="")
				{
					model.time_1115=decimal.Parse(row["time_1115"].ToString());
				}
				if(row["time_1215"]!=null && row["time_1215"].ToString()!="")
				{
					model.time_1215=decimal.Parse(row["time_1215"].ToString());
				}
				if(row["nextyearHour"]!=null && row["nextyearHour"].ToString()!="")
				{
					model.nextyearHour=decimal.Parse(row["nextyearHour"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,mem_id,mem_name,mem_unit_ID,dataDate,yearDay,yearHour,lastyearHour,time_1231,time_115,time_215,time_228,time_315,time_415,time_515,time_615,time_715,time_815,time_915,time_1015,time_1115,time_1215,nextyearHour ");
			strSql.Append(" FROM cm_YearAllStatisHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,mem_id,mem_name,mem_unit_ID,dataDate,yearDay,yearHour,lastyearHour,time_1231,time_115,time_215,time_228,time_315,time_415,time_515,time_615,time_715,time_815,time_915,time_1015,time_1115,time_1215,nextyearHour ");
			strSql.Append(" FROM cm_YearAllStatisHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_YearAllStatisHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_YearAllStatisHis T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
		#region  MethodEx

		#endregion  MethodEx
	}
}

