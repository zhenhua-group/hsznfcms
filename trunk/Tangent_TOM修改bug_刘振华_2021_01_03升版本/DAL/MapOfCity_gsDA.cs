﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DBUtility;
using TG.Model;
using System.Data.SqlClient;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class MapOfCity_gsDA
    {
        public List<MapOfChinaEntity> GetProjectCountByProvinceName()
        {
            //定西区，临夏自治州  ，兰州市，白银市，庆阳市，张掖市，酒泉市，武威市，金昌市，平凉市，甘南藏族自治区，陇南地区，天水地区
            string sql = @"select 
                                                case when BuildAddress like N'%定西%' then N'定西' 
                                                when BuildAddress like N'%临夏自治州%' then N'临夏自治州'
                                                 when BuildAddress like N'%兰州市%' then N'兰州市'
                                                 when BuildAddress like N'%白银市%' then N'白银市'
                                                 when BuildAddress like N'%庆阳市%' then N'庆阳市'
                                                 when BuildAddress like N'%张掖市%' then N'张掖市'
                                                 when BuildAddress like N'%酒泉市%' then N'酒泉市'
                                                 when BuildAddress like N'%武威市%' then N'武威市'
                                                 when BuildAddress like N'%金昌市%' then N'金昌市'
                                                 when BuildAddress like N'%平凉市%' then N'平凉市'
                                                 when BuildAddress like N'%甘南藏族自治区%' then N'甘南藏族自治区'
                                                 when BuildAddress like N'%陇南地区%' then N'陇南地区'
                                                 when BuildAddress like N'%天水地区%' then N'天水地区'
                                                when BuildAddress like N'%嘉峪关市%' then N'嘉峪关市'
                                                 when BuildAddress like N'%浙江%' then N'浙江'
                                                else BuildAddress end as Province ,
                                                COUNT(*) as ProjectCount
                                                from 
                                                cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo
                                                group by 
                                                 case when BuildAddress like N'%定西%' then N'定西' 
                                                when BuildAddress like N'%临夏自治州%' then N'临夏自治州'
                                                 when BuildAddress like N'%兰州市%' then N'兰州市'
                                                 when BuildAddress like N'%白银市%' then N'白银市'
                                                 when BuildAddress like N'%庆阳市%' then N'庆阳市'
                                                 when BuildAddress like N'%张掖市%' then N'张掖市'
                                                 when BuildAddress like N'%酒泉市%' then N'酒泉市'
                                                 when BuildAddress like N'%武威市%' then N'武威市'
                                                 when BuildAddress like N'%金昌市%' then N'金昌市'
                                                 when BuildAddress like N'%平凉市%' then N'平凉市'
                                                 when BuildAddress like N'%甘南藏族自治区%' then N'甘南藏族自治区'
                                                 when BuildAddress like N'%陇南地区%' then N'陇南地区'
                                                 when BuildAddress like N'%天水地区%' then N'天水地区'
                                                 when BuildAddress like N'%嘉峪关市%' then N'嘉峪关市'
                                                 when BuildAddress like N'%浙江%' then N'浙江'
                                                else BuildAddress end";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);
            //COUNT(*) as ProjectCount
            //                                 from 
            //                                 cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo
            //                                 group by 
            //                                 case when BuildAddress like N'%北京%' then N'北京' 
            //                                 when BuildAddress like N'%安徽%' then N'安徽'
            List<MapOfChinaEntity> resultList = EntityBuilder<MapOfChinaEntity>.BuilderEntityList(reader);

            return resultList;
        }

        public List<ProjectForMap> GetProjectListByProvinceName(string provinceName, int pageSize, int pageCurrent)
        {
            string sql = string.Format(@"SELECT TOP {0} * 
                                                     from 
	                                                (cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo) 
	                                                left join cm_Coperation c on cp.CoperationSysNo = c.cpr_Id 
                                                    where cp.BuildAddress like N'%{2}%' and cp.pro_id not in
	                                                (
		                                                select TOP {3} cp.pro_id 
		                                                from
		                                                (cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo)
		                                                left join cm_Coperation c on cp.CoperationSysNo = c.cpr_Id
		                                                where cp.BuildAddress like N'%{2}%'
		                                                order by cp.pro_id desc
	                                                ) order by cp.pro_id desc", pageSize, pageCurrent, provinceName, pageSize * pageCurrent);

            //string sql = "SELECT TOP(" + pageSize + ") * FROM(select ROW_NUMBE() OVER(ORDER BY cp.pro_ID ASC) AS RowID, cp.* from   (cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo) left join cm_Coperation c on cp.CoperationSysNo = c.cpr_Id where cp.BuildAddress like N'%" + provinceName + "%' )TT WHERE TT.RowId > " + pageCurrent + " * " + pageSize + "";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectForMap> projectList = EntityBuilder<ProjectForMap>.BuilderEntityList(reader);

            return projectList;
        }
        public List<ProjectForMap> GetProjectListByProvinceName(string provinceName)
        {
            string sql = string.Format(@"SELECT * FROM
cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo
where cp.BuildAddress like N'%{0}%'", provinceName);

            //string sql = "SELECT TOP(" + pageSize + ") * FROM(select ROW_NUMBE() OVER(ORDER BY cp.pro_ID ASC) AS RowID, cp.* from   (cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo) left join cm_Coperation c on cp.CoperationSysNo = c.cpr_Id where cp.BuildAddress like N'%" + provinceName + "%' )TT WHERE TT.RowId > " + pageCurrent + " * " + pageSize + "";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ProjectForMap> projectList = EntityBuilder<ProjectForMap>.BuilderEntityList(reader);

            return projectList;
        }
        public ProjectDetailForMap GetProjectInfoByProjectSysNo(int projectSysNo)
        {
            string sql = "select *  from  (cm_Project cp join tg_project tp on tp.pro_ID = cp.ReferenceSysNo join [cm_Dictionary] cd on cd.ID = cp.Pro_src) left join cm_Coperation c on cp.CoperationSysNo = c.cpr_Id where cp.pro_ID =" + projectSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            ProjectDetailForMap project = EntityBuilder<ProjectDetailForMap>.BuilderEntity(reader);

            reader.Close();

            return project;
        }
    }
}
