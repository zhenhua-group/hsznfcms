﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_ApplyUnitOvertime
	/// </summary>
	public partial class cm_ApplyUnitOvertime
	{
		public cm_ApplyUnitOvertime()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("ID", "cm_ApplyUnitOvertime"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_ApplyUnitOvertime");
			strSql.Append(" where ID="+ID+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_ApplyUnitOvertime model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.mem_ID != null)
			{
				strSql1.Append("mem_ID,");
				strSql2.Append(""+model.mem_ID+",");
			}
			if (model.OverYear != null)
			{
				strSql1.Append("OverYear,");
				strSql2.Append(""+model.OverYear+",");
			}
			if (model.OverMonth != null)
			{
				strSql1.Append("OverMonth,");
				strSql2.Append(""+model.OverMonth+",");
			}
			if (model.Quotiety != null)
			{
				strSql1.Append("Quotiety,");
				strSql2.Append(""+model.Quotiety+",");
			}
			if (model.QuotaMoney != null)
			{
				strSql1.Append("QuotaMoney,");
				strSql2.Append("'"+model.QuotaMoney+"',");
			}
			if (model.addDate != null)
			{
				strSql1.Append("addDate,");
				strSql2.Append("'"+model.addDate+"',");
			}
			if (model.addUserId != null)
			{
				strSql1.Append("addUserId,");
				strSql2.Append(""+model.addUserId+",");
			}
			strSql.Append("insert into cm_ApplyUnitOvertime(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_ApplyUnitOvertime model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_ApplyUnitOvertime set ");
			if (model.mem_ID != null)
			{
				strSql.Append("mem_ID="+model.mem_ID+",");
			}
			if (model.OverYear != null)
			{
				strSql.Append("OverYear="+model.OverYear+",");
			}
			if (model.OverMonth != null)
			{
				strSql.Append("OverMonth="+model.OverMonth+",");
			}
			if (model.Quotiety != null)
			{
				strSql.Append("Quotiety="+model.Quotiety+",");
			}
			if (model.QuotaMoney != null)
			{
				strSql.Append("QuotaMoney='"+model.QuotaMoney+"',");
			}
			else
			{
				strSql.Append("QuotaMoney= null ,");
			}
			if (model.addDate != null)
			{
				strSql.Append("addDate='"+model.addDate+"',");
			}
			else
			{
				strSql.Append("addDate= null ,");
			}
			if (model.addUserId != null)
			{
				strSql.Append("addUserId="+model.addUserId+",");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where ID="+ model.ID+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ApplyUnitOvertime ");
			strSql.Append(" where ID="+ID+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_ApplyUnitOvertime ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_ApplyUnitOvertime GetModel(int ID)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" ID,mem_ID,OverYear,OverMonth,Quotiety,QuotaMoney,addDate,addUserId ");
			strSql.Append(" from cm_ApplyUnitOvertime ");
			strSql.Append(" where ID="+ID+"" );
			TG.Model.cm_ApplyUnitOvertime model=new TG.Model.cm_ApplyUnitOvertime();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["ID"]!=null && ds.Tables[0].Rows[0]["ID"].ToString()!="")
				{
					model.ID=int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_ID"]!=null && ds.Tables[0].Rows[0]["mem_ID"].ToString()!="")
				{
					model.mem_ID=int.Parse(ds.Tables[0].Rows[0]["mem_ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["OverYear"]!=null && ds.Tables[0].Rows[0]["OverYear"].ToString()!="")
				{
					model.OverYear=int.Parse(ds.Tables[0].Rows[0]["OverYear"].ToString());
				}
				if(ds.Tables[0].Rows[0]["OverMonth"]!=null && ds.Tables[0].Rows[0]["OverMonth"].ToString()!="")
				{
					model.OverMonth=int.Parse(ds.Tables[0].Rows[0]["OverMonth"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Quotiety"]!=null && ds.Tables[0].Rows[0]["Quotiety"].ToString()!="")
				{
					model.Quotiety=decimal.Parse(ds.Tables[0].Rows[0]["Quotiety"].ToString());
				}
				if(ds.Tables[0].Rows[0]["QuotaMoney"]!=null && ds.Tables[0].Rows[0]["QuotaMoney"].ToString()!="")
				{
					model.QuotaMoney=ds.Tables[0].Rows[0]["QuotaMoney"].ToString();
				}
				if(ds.Tables[0].Rows[0]["addDate"]!=null && ds.Tables[0].Rows[0]["addDate"].ToString()!="")
				{
					model.addDate=DateTime.Parse(ds.Tables[0].Rows[0]["addDate"].ToString());
				}
				if(ds.Tables[0].Rows[0]["addUserId"]!=null && ds.Tables[0].Rows[0]["addUserId"].ToString()!="")
				{
					model.addUserId=int.Parse(ds.Tables[0].Rows[0]["addUserId"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,mem_ID,OverYear,OverMonth,Quotiety,QuotaMoney,addDate,addUserId ");
			strSql.Append(" FROM cm_ApplyUnitOvertime ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,mem_ID,OverYear,OverMonth,Quotiety,QuotaMoney,addDate,addUserId ");
			strSql.Append(" FROM cm_ApplyUnitOvertime ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_ApplyUnitOvertime ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_ApplyUnitOvertime T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

