﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_MemOutHis
	/// </summary>
	public partial class cm_MemOutHis
	{
		public cm_MemOutHis()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_MemOutHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_MemOutHis(");
			strSql.Append("Mem_ID,Mem_Name,Yue,SetCount,SetYear)");
			strSql.Append(" values (");
			strSql.Append("@Mem_ID,@Mem_Name,@Yue,@SetCount,@SetYear)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@Mem_ID", SqlDbType.Int,4),
					new SqlParameter("@Mem_Name", SqlDbType.VarChar,30),
					new SqlParameter("@Yue", SqlDbType.Decimal,9),
					new SqlParameter("@SetCount", SqlDbType.Decimal,9),
					new SqlParameter("@SetYear", SqlDbType.Int,4)};
			parameters[0].Value = model.Mem_ID;
			parameters[1].Value = model.Mem_Name;
			parameters[2].Value = model.Yue;
			parameters[3].Value = model.SetCount;
			parameters[4].Value = model.SetYear;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_MemOutHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_MemOutHis set ");
			strSql.Append("Mem_ID=@Mem_ID,");
			strSql.Append("Mem_Name=@Mem_Name,");
			strSql.Append("Yue=@Yue,");
			strSql.Append("SetCount=@SetCount,");
			strSql.Append("SetYear=@SetYear");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@Mem_ID", SqlDbType.Int,4),
					new SqlParameter("@Mem_Name", SqlDbType.VarChar,30),
					new SqlParameter("@Yue", SqlDbType.Decimal,9),
					new SqlParameter("@SetCount", SqlDbType.Decimal,9),
					new SqlParameter("@SetYear", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.Mem_ID;
			parameters[1].Value = model.Mem_Name;
			parameters[2].Value = model.Yue;
			parameters[3].Value = model.SetCount;
			parameters[4].Value = model.SetYear;
			parameters[5].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_MemOutHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_MemOutHis ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_MemOutHis GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,Mem_ID,Mem_Name,Yue,SetCount,SetYear from cm_MemOutHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_MemOutHis model=new TG.Model.cm_MemOutHis();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_MemOutHis DataRowToModel(DataRow row)
		{
			TG.Model.cm_MemOutHis model=new TG.Model.cm_MemOutHis();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["Mem_ID"]!=null && row["Mem_ID"].ToString()!="")
				{
					model.Mem_ID=int.Parse(row["Mem_ID"].ToString());
				}
				if(row["Mem_Name"]!=null)
				{
					model.Mem_Name=row["Mem_Name"].ToString();
				}
				if(row["Yue"]!=null && row["Yue"].ToString()!="")
				{
					model.Yue=decimal.Parse(row["Yue"].ToString());
				}
				if(row["SetCount"]!=null && row["SetCount"].ToString()!="")
				{
					model.SetCount=decimal.Parse(row["SetCount"].ToString());
				}
				if(row["SetYear"]!=null && row["SetYear"].ToString()!="")
				{
					model.SetYear=int.Parse(row["SetYear"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,Mem_ID,Mem_Name,Yue,SetCount,SetYear ");
			strSql.Append(" FROM cm_MemOutHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,Mem_ID,Mem_Name,Yue,SetCount,SetYear ");
			strSql.Append(" FROM cm_MemOutHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_MemOutHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_MemOutHis T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_MemOutHis";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

