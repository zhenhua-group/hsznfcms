﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_SheBaoChangeRecord
	/// </summary>
	public partial class cm_SheBaoChangeRecord
	{
		public cm_SheBaoChangeRecord()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_SheBaoChangeRecord model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_SheBaoChangeRecord(");
			strSql.Append("mem_ID,mem_SheBaobase,mem_SheBaobase2,mem_Gongjijin,mem_Gongjijin2,ChangeDate,InsertUserID)");
			strSql.Append(" values (");
			strSql.Append("@mem_ID,@mem_SheBaobase,@mem_SheBaobase2,@mem_Gongjijin,@mem_Gongjijin2,@ChangeDate,@InsertUserID)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_ID", SqlDbType.Int,4),
					new SqlParameter("@mem_SheBaobase", SqlDbType.Int,4),
					new SqlParameter("@mem_SheBaobase2", SqlDbType.Int,4),
					new SqlParameter("@mem_Gongjijin", SqlDbType.Int,4),
					new SqlParameter("@mem_Gongjijin2", SqlDbType.Int,4),
					new SqlParameter("@ChangeDate", SqlDbType.DateTime),
					new SqlParameter("@InsertUserID", SqlDbType.Int,4)};
			parameters[0].Value = model.mem_ID;
			parameters[1].Value = model.mem_SheBaobase;
			parameters[2].Value = model.mem_SheBaobase2;
			parameters[3].Value = model.mem_Gongjijin;
			parameters[4].Value = model.mem_Gongjijin2;
			parameters[5].Value = model.ChangeDate;
			parameters[6].Value = model.InsertUserID;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_SheBaoChangeRecord model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_SheBaoChangeRecord set ");
			strSql.Append("mem_ID=@mem_ID,");
			strSql.Append("mem_SheBaobase=@mem_SheBaobase,");
			strSql.Append("mem_SheBaobase2=@mem_SheBaobase2,");
			strSql.Append("mem_Gongjijin=@mem_Gongjijin,");
			strSql.Append("mem_Gongjijin2=@mem_Gongjijin2,");
			strSql.Append("ChangeDate=@ChangeDate,");
			strSql.Append("InsertUserID=@InsertUserID");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_ID", SqlDbType.Int,4),
					new SqlParameter("@mem_SheBaobase", SqlDbType.Int,4),
					new SqlParameter("@mem_SheBaobase2", SqlDbType.Int,4),
					new SqlParameter("@mem_Gongjijin", SqlDbType.Int,4),
					new SqlParameter("@mem_Gongjijin2", SqlDbType.Int,4),
					new SqlParameter("@ChangeDate", SqlDbType.DateTime),
					new SqlParameter("@InsertUserID", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.mem_ID;
			parameters[1].Value = model.mem_SheBaobase;
			parameters[2].Value = model.mem_SheBaobase2;
			parameters[3].Value = model.mem_Gongjijin;
			parameters[4].Value = model.mem_Gongjijin2;
			parameters[5].Value = model.ChangeDate;
			parameters[6].Value = model.InsertUserID;
			parameters[7].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_SheBaoChangeRecord ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_SheBaoChangeRecord ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_SheBaoChangeRecord GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,mem_ID,mem_SheBaobase,mem_SheBaobase2,mem_Gongjijin,mem_Gongjijin2,ChangeDate,InsertUserID from cm_SheBaoChangeRecord ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_SheBaoChangeRecord model=new TG.Model.cm_SheBaoChangeRecord();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_SheBaoChangeRecord DataRowToModel(DataRow row)
		{
			TG.Model.cm_SheBaoChangeRecord model=new TG.Model.cm_SheBaoChangeRecord();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["mem_ID"]!=null && row["mem_ID"].ToString()!="")
				{
					model.mem_ID=int.Parse(row["mem_ID"].ToString());
				}
				if(row["mem_SheBaobase"]!=null && row["mem_SheBaobase"].ToString()!="")
				{
					model.mem_SheBaobase=int.Parse(row["mem_SheBaobase"].ToString());
				}
				if(row["mem_SheBaobase2"]!=null && row["mem_SheBaobase2"].ToString()!="")
				{
					model.mem_SheBaobase2=int.Parse(row["mem_SheBaobase2"].ToString());
				}
				if(row["mem_Gongjijin"]!=null && row["mem_Gongjijin"].ToString()!="")
				{
					model.mem_Gongjijin=int.Parse(row["mem_Gongjijin"].ToString());
				}
				if(row["mem_Gongjijin2"]!=null && row["mem_Gongjijin2"].ToString()!="")
				{
					model.mem_Gongjijin2=int.Parse(row["mem_Gongjijin2"].ToString());
				}
				if(row["ChangeDate"]!=null && row["ChangeDate"].ToString()!="")
				{
					model.ChangeDate=DateTime.Parse(row["ChangeDate"].ToString());
				}
				if(row["InsertUserID"]!=null && row["InsertUserID"].ToString()!="")
				{
					model.InsertUserID=int.Parse(row["InsertUserID"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,mem_ID,mem_SheBaobase,mem_SheBaobase2,mem_Gongjijin,mem_Gongjijin2,ChangeDate,InsertUserID ");
			strSql.Append(" FROM cm_SheBaoChangeRecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,mem_ID,mem_SheBaobase,mem_SheBaobase2,mem_Gongjijin,mem_Gongjijin2,ChangeDate,InsertUserID ");
			strSql.Append(" FROM cm_SheBaoChangeRecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_SheBaoChangeRecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_SheBaoChangeRecord T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_SheBaoChangeRecord";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

