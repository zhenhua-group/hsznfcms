﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using TG.Common.EntityBuilder;
using System.Data.SqlClient;
using TG.DBUtility;

namespace TG.DAL
{
    public class LoadLeftMenuDA
    {
        public List<LeftMenuViewEntity> GetLeftMenuList(LeftMenuType leftMenuType)
        {
            string leftTypeString = " where 1=1 ";
            switch (leftMenuType)
            {
                case LeftMenuType.Coperation:
                    leftTypeString += " and Type=N'Coperation'";
                    break;
                case LeftMenuType.Customer:
                    leftTypeString += " and Type=N'Customer'";
                    break;
                case LeftMenuType.Default:
                    leftTypeString += "  and Type=N'Default'";
                    break;
                case LeftMenuType.LeaderShip:
                    leftTypeString += " and Type=N'LeaderShip'";
                    break;
                case LeftMenuType.Project:
                    leftTypeString += " and Type=N'Project'";
                    break;
                case LeftMenuType.ProjectShow:
                    leftTypeString += " and Type=N'ProjectShow'";
                    break;
                case LeftMenuType.SystemConfig:
                    leftTypeString += " and Type=N'SystemConfig'";
                    break;
                case LeftMenuType.ThechQu:
                    leftTypeString += " and Type=N'ThechQu'";
                    break;
                case LeftMenuType.JiXiao:
                    leftTypeString += " and Type=N'JiXiao'";
                    break;
                case LeftMenuType.KaoQin:
                    leftTypeString += " and Type=N'KaoQin'";
                    break;
                case LeftMenuType.All:
                    leftTypeString = "";
                    break;
            }
            string sql = string.Format(@" SELECT [SysNo]
                                                              ,[Description]
                                                              ,[ElementID]
                                                              ,[Role]
                                                              ,[Type]
                                                              ,[InUser]
                                                              ,[InDate],
                                                              tm.mem_Name as InUserName
                                                              FROM [dbo].[cm_LeftMenu] cl join tg_member tm on tm.mem_ID = cl.InUser {0}", leftTypeString);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<LeftMenuViewEntity> resultList = EntityBuilder<LeftMenuViewEntity>.BuilderEntityList(reader);

            return resultList;
        }

        public int SaveLeftMenu(LeftMenuViewEntity leftMenuViewEntity)
        {
            string sql = string.Format("insert into cm_LeftMenu(Description,ElementID,Role,Type,InUser)values(N'{0}',N'{1}',N'{2}',N'{3}',{4});select @@IDENTITY", leftMenuViewEntity.Description, leftMenuViewEntity.ElementID, leftMenuViewEntity.Role, leftMenuViewEntity.Type, leftMenuViewEntity.InUser);

            object objResult = DbHelperSQL.GetSingle(sql);

            return objResult == null ? 0 : Convert.ToInt32(objResult);
        }

        public LeftMenuViewEntity GetLeftMenuViewEntity(int sysNo)
        {
            string sql = string.Format(@" SELECT [SysNo]
                                                              ,[Description]
                                                              ,[ElementID]
                                                              ,[Role]
                                                              ,[Type]
                                                              ,[InUser]
                                                              ,[InDate],
                                                              tm.mem_Name as InUserName
                                                              FROM [dbo].[cm_LeftMenu] cl join tg_member tm on tm.mem_ID = cl.InUser where cl.SysNo ={0}", sysNo);
            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            LeftMenuViewEntity leftMenuViewEntity = EntityBuilder<LeftMenuViewEntity>.BuilderEntity(reader);

            reader.Close();

            return leftMenuViewEntity;
        }

        public int UpDateLeftMenu(LeftMenuViewEntity leftMenuViewEntity)
        {
            string sql = string.Format("update cm_LeftMenu set Description=N'{0}',ElementID=N'{1}',Role=N'{2}',Type=N'{3}' where SysNo={4}", leftMenuViewEntity.Description, leftMenuViewEntity.ElementID, leftMenuViewEntity.Role, leftMenuViewEntity.Type, leftMenuViewEntity.SysNo);

            int count = DbHelperSQL.ExecuteSql(sql);

            return count;
        }

        public int DeleteLeftMenu(int sysNo)
        {
            string sql = "delete cm_LeftMenu where SysNo= " + sysNo;
            return DbHelperSQL.ExecuteSql(sql);
        }
    }
}
