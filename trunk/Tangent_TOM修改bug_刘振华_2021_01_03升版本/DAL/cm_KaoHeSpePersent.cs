﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeSpePersent
	/// </summary>
	public partial class cm_KaoHeSpePersent
	{
		public cm_KaoHeSpePersent()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeSpePersent model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeSpePersent(");
			strSql.Append("ProKHNameID,SpeRoleID,SpeID,SpeName,Persentold,Persent1,Persent2,InserUserID1,InserDate1,InserUserID2,InserDate2,Stat,Stat2,Persent3,InserUserID3,InserDate3,Stat3)");
			strSql.Append(" values (");
			strSql.Append("@ProKHNameID,@SpeRoleID,@SpeID,@SpeName,@Persentold,@Persent1,@Persent2,@InserUserID1,@InserDate1,@InserUserID2,@InserDate2,@Stat,@Stat2,@Persent3,@InserUserID3,@InserDate3,@Stat3)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@ProKHNameID", SqlDbType.Int,4),
					new SqlParameter("@SpeRoleID", SqlDbType.Int,4),
					new SqlParameter("@SpeID", SqlDbType.Int,4),
					new SqlParameter("@SpeName", SqlDbType.NVarChar,50),
					new SqlParameter("@Persentold", SqlDbType.Decimal,9),
					new SqlParameter("@Persent1", SqlDbType.Decimal,9),
					new SqlParameter("@Persent2", SqlDbType.Decimal,9),
					new SqlParameter("@InserUserID1", SqlDbType.Int,4),
					new SqlParameter("@InserDate1", SqlDbType.DateTime),
					new SqlParameter("@InserUserID2", SqlDbType.Int,4),
					new SqlParameter("@InserDate2", SqlDbType.DateTime),
					new SqlParameter("@Stat", SqlDbType.Int,4),
					new SqlParameter("@Stat2", SqlDbType.Int,4),
					new SqlParameter("@Persent3", SqlDbType.Decimal,9),
					new SqlParameter("@InserUserID3", SqlDbType.Int,4),
					new SqlParameter("@InserDate3", SqlDbType.DateTime),
					new SqlParameter("@Stat3", SqlDbType.Int,4)};
			parameters[0].Value = model.ProKHNameID;
			parameters[1].Value = model.SpeRoleID;
			parameters[2].Value = model.SpeID;
			parameters[3].Value = model.SpeName;
			parameters[4].Value = model.Persentold;
			parameters[5].Value = model.Persent1;
			parameters[6].Value = model.Persent2;
			parameters[7].Value = model.InserUserID1;
			parameters[8].Value = model.InserDate1;
			parameters[9].Value = model.InserUserID2;
			parameters[10].Value = model.InserDate2;
			parameters[11].Value = model.Stat;
			parameters[12].Value = model.Stat2;
			parameters[13].Value = model.Persent3;
			parameters[14].Value = model.InserUserID3;
			parameters[15].Value = model.InserDate3;
			parameters[16].Value = model.Stat3;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeSpePersent model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeSpePersent set ");
			strSql.Append("ProKHNameID=@ProKHNameID,");
			strSql.Append("SpeRoleID=@SpeRoleID,");
			strSql.Append("SpeID=@SpeID,");
			strSql.Append("SpeName=@SpeName,");
			strSql.Append("Persentold=@Persentold,");
			strSql.Append("Persent1=@Persent1,");
			strSql.Append("Persent2=@Persent2,");
			strSql.Append("InserUserID1=@InserUserID1,");
			strSql.Append("InserDate1=@InserDate1,");
			strSql.Append("InserUserID2=@InserUserID2,");
			strSql.Append("InserDate2=@InserDate2,");
			strSql.Append("Stat=@Stat,");
			strSql.Append("Stat2=@Stat2,");
			strSql.Append("Persent3=@Persent3,");
			strSql.Append("InserUserID3=@InserUserID3,");
			strSql.Append("InserDate3=@InserDate3,");
			strSql.Append("Stat3=@Stat3");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ProKHNameID", SqlDbType.Int,4),
					new SqlParameter("@SpeRoleID", SqlDbType.Int,4),
					new SqlParameter("@SpeID", SqlDbType.Int,4),
					new SqlParameter("@SpeName", SqlDbType.NVarChar,50),
					new SqlParameter("@Persentold", SqlDbType.Decimal,9),
					new SqlParameter("@Persent1", SqlDbType.Decimal,9),
					new SqlParameter("@Persent2", SqlDbType.Decimal,9),
					new SqlParameter("@InserUserID1", SqlDbType.Int,4),
					new SqlParameter("@InserDate1", SqlDbType.DateTime),
					new SqlParameter("@InserUserID2", SqlDbType.Int,4),
					new SqlParameter("@InserDate2", SqlDbType.DateTime),
					new SqlParameter("@Stat", SqlDbType.Int,4),
					new SqlParameter("@Stat2", SqlDbType.Int,4),
					new SqlParameter("@Persent3", SqlDbType.Decimal,9),
					new SqlParameter("@InserUserID3", SqlDbType.Int,4),
					new SqlParameter("@InserDate3", SqlDbType.DateTime),
					new SqlParameter("@Stat3", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.ProKHNameID;
			parameters[1].Value = model.SpeRoleID;
			parameters[2].Value = model.SpeID;
			parameters[3].Value = model.SpeName;
			parameters[4].Value = model.Persentold;
			parameters[5].Value = model.Persent1;
			parameters[6].Value = model.Persent2;
			parameters[7].Value = model.InserUserID1;
			parameters[8].Value = model.InserDate1;
			parameters[9].Value = model.InserUserID2;
			parameters[10].Value = model.InserDate2;
			parameters[11].Value = model.Stat;
			parameters[12].Value = model.Stat2;
			parameters[13].Value = model.Persent3;
			parameters[14].Value = model.InserUserID3;
			parameters[15].Value = model.InserDate3;
			parameters[16].Value = model.Stat3;
			parameters[17].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeSpePersent ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeSpePersent ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeSpePersent GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,ProKHNameID,SpeRoleID,SpeID,SpeName,Persentold,Persent1,Persent2,InserUserID1,InserDate1,InserUserID2,InserDate2,Stat,Stat2,Persent3,InserUserID3,InserDate3,Stat3 from cm_KaoHeSpePersent ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeSpePersent model=new TG.Model.cm_KaoHeSpePersent();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeSpePersent DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeSpePersent model=new TG.Model.cm_KaoHeSpePersent();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["ProKHNameID"]!=null && row["ProKHNameID"].ToString()!="")
				{
					model.ProKHNameID=int.Parse(row["ProKHNameID"].ToString());
				}
				if(row["SpeRoleID"]!=null && row["SpeRoleID"].ToString()!="")
				{
					model.SpeRoleID=int.Parse(row["SpeRoleID"].ToString());
				}
				if(row["SpeID"]!=null && row["SpeID"].ToString()!="")
				{
					model.SpeID=int.Parse(row["SpeID"].ToString());
				}
				if(row["SpeName"]!=null)
				{
					model.SpeName=row["SpeName"].ToString();
				}
				if(row["Persentold"]!=null && row["Persentold"].ToString()!="")
				{
					model.Persentold=decimal.Parse(row["Persentold"].ToString());
				}
				if(row["Persent1"]!=null && row["Persent1"].ToString()!="")
				{
					model.Persent1=decimal.Parse(row["Persent1"].ToString());
				}
				if(row["Persent2"]!=null && row["Persent2"].ToString()!="")
				{
					model.Persent2=decimal.Parse(row["Persent2"].ToString());
				}
				if(row["InserUserID1"]!=null && row["InserUserID1"].ToString()!="")
				{
					model.InserUserID1=int.Parse(row["InserUserID1"].ToString());
				}
				if(row["InserDate1"]!=null && row["InserDate1"].ToString()!="")
				{
					model.InserDate1=DateTime.Parse(row["InserDate1"].ToString());
				}
				if(row["InserUserID2"]!=null && row["InserUserID2"].ToString()!="")
				{
					model.InserUserID2=int.Parse(row["InserUserID2"].ToString());
				}
				if(row["InserDate2"]!=null && row["InserDate2"].ToString()!="")
				{
					model.InserDate2=DateTime.Parse(row["InserDate2"].ToString());
				}
				if(row["Stat"]!=null && row["Stat"].ToString()!="")
				{
					model.Stat=int.Parse(row["Stat"].ToString());
				}
				if(row["Stat2"]!=null && row["Stat2"].ToString()!="")
				{
					model.Stat2=int.Parse(row["Stat2"].ToString());
				}
				if(row["Persent3"]!=null && row["Persent3"].ToString()!="")
				{
					model.Persent3=decimal.Parse(row["Persent3"].ToString());
				}
				if(row["InserUserID3"]!=null && row["InserUserID3"].ToString()!="")
				{
					model.InserUserID3=int.Parse(row["InserUserID3"].ToString());
				}
				if(row["InserDate3"]!=null && row["InserDate3"].ToString()!="")
				{
					model.InserDate3=DateTime.Parse(row["InserDate3"].ToString());
				}
				if(row["Stat3"]!=null && row["Stat3"].ToString()!="")
				{
					model.Stat3=int.Parse(row["Stat3"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,ProKHNameID,SpeRoleID,SpeID,SpeName,Persentold,Persent1,Persent2,InserUserID1,InserDate1,InserUserID2,InserDate2,Stat,Stat2,Persent3,InserUserID3,InserDate3,Stat3 ");
			strSql.Append(" FROM cm_KaoHeSpePersent ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,ProKHNameID,SpeRoleID,SpeID,SpeName,Persentold,Persent1,Persent2,InserUserID1,InserDate1,InserUserID2,InserDate2,Stat,Stat2,Persent3,InserUserID3,InserDate3,Stat3 ");
			strSql.Append(" FROM cm_KaoHeSpePersent ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeSpePersent ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeSpePersent T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeSpePersent";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

