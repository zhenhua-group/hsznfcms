﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using System.Data.SqlClient;
using TG.DBUtility;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
	public class ProjectAuditConfigDA
	{
		public List<ProjectAuditConfigViewEntity> GetProjectAuditConfigViewEntity()
		{
			string sql = "select pa.SysNo,pa.ProcessDescription,pa.RoleSysNo,pa.Position,pa.InUser,pa.InDate,r.Users,r.RoleName from cm_ProjectAuditConfig pa join cm_Role r on r.SysNo = pa.RoleSysNo";

			SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

			List<ProjectAuditConfigViewEntity> resultList = EntityBuilder<ProjectAuditConfigViewEntity>.BuilderEntityList(reader);

			return resultList;
		}

		public int UpdateProjectAuditConfigToRelationship(AuditConfigEntity auditConfigEntity)
		{
			string sql = "update cm_ProjectAuditConfig set RoleSysNo=" + auditConfigEntity.RoleSysNo + " where SysNo=" + auditConfigEntity.AuditConfigSysNo;

			return DbHelperSQL.ExecuteSql(sql);

		}

		/// <summary>
		/// 得到一个项目审核配置信息
		/// </summary>
		/// <param name="whereSql"></param>
		/// <returns></returns>
		public ProjectAuditConfigViewEntity GetProjectConfigViewEntity(string whereSql)
		{
			string sql = "select pa.SysNo,pa.ProcessDescription,pa.RoleSysNo,pa.Position,pa.InUser,pa.InDate,r.Users,r.RoleName from cm_ProjectAuditConfig pa join cm_Role r on r.SysNo = pa.RoleSysNo where 1=1 " + whereSql;

			SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

			reader.Read();

			ProjectAuditConfigViewEntity projectAuditConfigViewEntity = EntityBuilder<ProjectAuditConfigViewEntity>.BuilderEntity(reader);

			reader.Close();

			return projectAuditConfigViewEntity;
		}
	}
}
