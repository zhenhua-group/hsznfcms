﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Model;
using System.Data.SqlClient;
using TG.Common.EntityBuilder;
using TG.DBUtility;

namespace TG.DAL
{
	public class ProjectAuditListDA
	{
		public ProjectAuditListViewDataEntity GetProjectAuditListViewEntity(int projectAuditListViewSysNo)
		{
			string sql = "select pa.*,p.pro_Name,u.mem_Name from cm_ProjectAudit pa join cm_project p on pa.ProjectSysNo = p.pro_ID join tg_member u on u.mem_ID = pa.InUser  WHERE pa.SysNo = " + projectAuditListViewSysNo;

			SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

			reader.Read();

			ProjectAuditListViewDataEntity projectAuditListViewDataEntity = EntityBuilder<ProjectAuditListViewDataEntity>.BuilderEntity(reader);

			reader.Close();

			return projectAuditListViewDataEntity;
		}
	}
}
