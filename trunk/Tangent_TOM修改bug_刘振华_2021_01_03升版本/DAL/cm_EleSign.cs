﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_EleSign
	/// </summary>
	public partial class cm_EleSign
	{
		public cm_EleSign()
		{}
		#region  Method



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_EleSign model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_EleSign(");
            strSql.Append("mem_id,elesign_min,elesign)");
			strSql.Append(" values (");
            strSql.Append("@mem_id,@elesign_min,@elesign)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_id", SqlDbType.Int,4),
                    new SqlParameter("@elesign_min", SqlDbType.NVarChar,4000),
					new SqlParameter("@elesign", SqlDbType.NVarChar,4000)};
			parameters[0].Value = model.mem_id;
            parameters[1].Value = model.elesign_min;
			parameters[2].Value = model.elesign;


			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public int Update(TG.Model.cm_EleSign model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_EleSign set ");
			strSql.Append("mem_id=@mem_id,");
            strSql.Append("elesign_min=@elesign_min,");
			strSql.Append("elesign=@elesign");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_id", SqlDbType.Int,4),
                    new SqlParameter("@elesign_min", SqlDbType.NVarChar,4000),
					new SqlParameter("@elesign", SqlDbType.NVarChar,4000),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.mem_id;
            parameters[1].Value = model.elesign_min;
			parameters[2].Value = model.elesign;
			parameters[3].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
            return rows;
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_EleSign ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_EleSign ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteListMemID(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_EleSign ");
            strSql.Append(" where mem_id in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_EleSign GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,mem_id,elesign from cm_EleSign ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_EleSign model=new TG.Model.cm_EleSign();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["ID"]!=null && ds.Tables[0].Rows[0]["ID"].ToString()!="")
				{
					model.ID=int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
				}
				if(ds.Tables[0].Rows[0]["mem_id"]!=null && ds.Tables[0].Rows[0]["mem_id"].ToString()!="")
				{
					model.mem_id=int.Parse(ds.Tables[0].Rows[0]["mem_id"].ToString());
				}
                if (ds.Tables[0].Rows[0]["elesign_min"] != null && ds.Tables[0].Rows[0]["elesign_min"].ToString() != "")
                {
                    model.elesign_min = ds.Tables[0].Rows[0]["elesign_min"].ToString();
                }
                if(ds.Tables[0].Rows[0]["elesign"]!=null && ds.Tables[0].Rows[0]["elesign"].ToString()!="")
				{
					model.elesign=ds.Tables[0].Rows[0]["elesign"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}
        /// <summary>
        /// 根据会员id得到电子签名model
        /// </summary>
        /// <param name="memid"></param>
        /// <returns></returns>
        public TG.Model.cm_EleSign GetModel2(int memid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,mem_id,elesign_min,elesign from cm_EleSign ");
            strSql.Append(" where mem_id=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = memid;

            TG.Model.cm_EleSign model = new TG.Model.cm_EleSign();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_id"] != null && ds.Tables[0].Rows[0]["mem_id"].ToString() != "")
                {
                    model.mem_id = int.Parse(ds.Tables[0].Rows[0]["mem_id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["elesign_min"] != null && ds.Tables[0].Rows[0]["elesign_min"].ToString() != "")
                {
                    model.elesign_min = ds.Tables[0].Rows[0]["elesign_min"].ToString();
                }
                if (ds.Tables[0].Rows[0]["elesign"] != null && ds.Tables[0].Rows[0]["elesign"].ToString() != "")
                {
                    model.elesign = ds.Tables[0].Rows[0]["elesign"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }

        }
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select ID,mem_id,elesign_min,elesign ");
			strSql.Append(" FROM cm_EleSign ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
            strSql.Append(" ID,mem_id,elesign,elesign_min ");
			strSql.Append(" FROM cm_EleSign ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_EleSign ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_EleSign T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_EleSign";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
	}
}

