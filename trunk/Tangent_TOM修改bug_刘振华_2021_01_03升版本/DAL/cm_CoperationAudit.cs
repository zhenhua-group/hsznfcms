﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DBUtility;
using System.Data;
using TG.Model;
using System.Data.SqlClient;

namespace TG.DAL
{
    public class cm_CoperationAudit
    {
        /// <summary>
        /// 新规一条记录
        /// </summary>
        /// <param name="coperation"></param>
        /// <returns></returns>
        public int InsertCoperatioAuditRecord(TG.Model.cm_CoperationAudit coperation)
        {
            string sql = "insert into cm_AuditRecord(CoperationSysNo,InUser,InDate,Status) values(";
            sql += "'" + coperation.CoperationSysNo + "',";
            sql += "'" + coperation.InUser + "',";
            sql += "'" + DateTime.Now + "',";
            sql += "'A');select @@IDENTITY";
            int count = Convert.ToInt32(DbHelperSQL.GetSingle(sql));
            return count;
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="sysMsg"></param>
        /// <returns></returns>
        public int SendMsg(TG.Model.cm_SysMsg sysMsg)
        {
            string sql = "insert into cm_SysMsg(AuditRecordSysNo,FromUser,ToRole,Status,InDate)values(";
            sql += sysMsg.AuditRecordSysNo + ",";
            sql += sysMsg.FromUser + ",";
            sql += sysMsg.ToRole + ",";
            sql += sysMsg.MsgStatus + ",";
            sql += sysMsg.MsgInDate + ")";
            int count = DbHelperSQL.ExecuteSql(sql);
            return count;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="coperation"></param>
        /// <returns></returns>
        public int UpdateCoperatioAuditRecord(TG.Model.cm_CoperationAudit coperation)
        {
            string sql = "update cm_AuditRecord set Status='";
            sql += coperation.Status + "',";
            sql += "UndertakeProposal='" + coperation.UndertakeProposal + "',";
            sql += "OperateDepartmentProposal='" + coperation.OperateDepartmentProposal + "',";
            sql += "TechnologyDepartmentProposal='" + coperation.TechnologyDepartmentProposal + "',";
            sql += "GeneralManagerProposal = '" + coperation.GeneralManagerProposal + "',";
            sql += "BossProposal = '" + coperation.BossProposal + "',";
            sql += "AuditUser='" + coperation.AuditUser + "',";
            sql += "AuditDate='" + coperation.AuditDate + "', ";
            sql += "ManageLevel=" + coperation.ManageLevel + ",";
            sql += "NeedLegalAdviser=" + coperation.NeedLegalAdviser + ",";
            sql += "LegalAdviserProposal=N'" + coperation.LegalAdviserProposal + "'";
            sql += " where SysNo=" + coperation.SysNo;
            int count = DbHelperSQL.ExecuteSql(sql);
            return count;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationAudit GetModel(int SysNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" SysNo,CoperationSysNo,InUser,InDate,Status,UndertakeProposal,OperateDepartmentProposal,TechnologyDepartmentProposal,GeneralManagerProposal,BossProposal,AuditUser,AuditDate,ManageLevel,NeedLegalAdviser,LegalAdviserProposal ");
            strSql.Append(" from cm_AuditRecord ");
            strSql.Append(" where SysNo=" + SysNo + "");

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

            reader.Read();

            TG.Model.cm_CoperationAudit coperationAuditEntity = TG.Common.EntityBuilder.EntityBuilder<TG.Model.cm_CoperationAudit>.BuilderEntity(reader);

            reader.Close();

            return coperationAuditEntity;

        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationAudit GetModelByCoperationSysNo(int coperationSysNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" SysNo,CoperationSysNo,InUser,InDate,Status,UndertakeProposal,OperateDepartmentProposal,TechnologyDepartmentProposal,GeneralManagerProposal,BossProposal,AuditUser,AuditDate,ManageLevel,NeedLegalAdviser,LegalAdviserProposal ");
            strSql.Append(" from cm_AuditRecord ");
            strSql.Append(" where CoperationSysNo=" + coperationSysNo + " order by SysNo DESC");

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

            reader.Read();

            TG.Model.cm_CoperationAudit coperationAuditEntity = TG.Common.EntityBuilder.EntityBuilder<TG.Model.cm_CoperationAudit>.BuilderEntity(reader);

            reader.Close();

            return coperationAuditEntity;
        }
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CoperationAuditEdit GetEditModelByCoperationSysNo(int coperationSysNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" SysNo,CoperationSysNo,Options,InUser,InDate,Status,UndertakeProposal,OperateDepartmentProposal,TechnologyDepartmentProposal,GeneralManagerProposal,BossProposal,AuditUser,AuditDate,ManageLevel,NeedLegalAdviser,LegalAdviserProposal,ChangeDetails");
            strSql.Append(" from cm_AuditRecordEdit ");
            strSql.Append(" where CoperationSysNo=" + coperationSysNo + " order by SysNo DESC");

            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

            reader.Read();

            TG.Model.cm_CoperationAuditEdit coperationAuditEntity = TG.Common.EntityBuilder.EntityBuilder<TG.Model.cm_CoperationAuditEdit>.BuilderEntity(reader);

            reader.Close();

            return coperationAuditEntity;
        }
        /// <summary>
        ///  判断是否存在此条审核记录
        /// </summary>
        /// <param name="coperationSysNo"></param>
        /// <returns></returns>
        public int IsExist(int coperationSysNo)
        {
            bool flag = true;
            //查询合同信息
            string existsSql = string.Format("select TOP 1 SysNo from cm_AuditRecord ar where CoperationSysNo = {0} order by ar.SysNo DESC", coperationSysNo);

            string existsSql2 = string.Format("select TOP 1 ManageLevel from cm_AuditRecord ar where CoperationSysNo = {0} order by ar.SysNo DESC", coperationSysNo);

            object SysNo = DbHelperSQL.GetSingle(existsSql);
            object objManageLevel = DbHelperSQL.GetSingle(existsSql2);

            if (SysNo == null)
            {
                //没有查询到审核记录的场合
                flag = false;
            }
            else
            {
                //查询到审核记录的场合
                int manageLevel = Convert.ToInt32(objManageLevel);
                string stopChar = string.Empty;
                if (manageLevel == 0) stopChar = "N'K'"; else stopChar = "N'I'";

                string sql = string.Format("select top 1 1 from cm_AuditRecord where SysNo = {0} and Status in (N'C',N'E',N'G',{1}) order by SysNo DESC", SysNo.ToString(), stopChar);
                object obj = DbHelperSQL.GetSingle(sql);
                flag = obj == null ? true : false;
            }
            return flag == true ? 1 : 0;
        }

        /// <summary>
        /// 查询合同基本信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public List<cm_CoperationAuditListView> GetCoperationListView(string query, int startIndex, int endIndex)
        {
            List<cm_CoperationAuditListView> resultList = new List<cm_CoperationAuditListView>();

            SqlDataReader reader = DBUtility.DbHelperSQL.RunProcedure("P_cm_Coperation", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });

            while (reader.Read())
            {
                cm_CoperationAuditListView auditListView = new cm_CoperationAuditListView
                {
                    cpr_Id = Convert.ToInt32(reader["cpr_Id"]),
                    cst_Id = Convert.ToInt32(reader["cst_Id"]),
                    cpr_No = reader["cpr_No"] == null ? string.Empty : reader["cpr_No"].ToString(),
                    cpr_Type = reader["cpr_Type"] == null ? string.Empty : reader["cpr_Type"].ToString(),
                    cpr_Type2 = reader["cpr_Type2"] == null ? string.Empty : reader["cpr_Type2"].ToString(),
                    cpr_Name = reader["cpr_Name"] == null ? string.Empty : reader["cpr_Name"].ToString(),
                    cpr_Unit = reader["cpr_Unit"] == null ? string.Empty : reader["cpr_Unit"].ToString(),
                    cpr_Acount = reader["cpr_Acount"] == null ? 0 : Convert.ToDecimal(reader["cpr_Acount"]),
                    cpr_ShijiAcount = reader["cpr_ShijiAcount"] == null ? 0 : Convert.ToDecimal(reader["cpr_ShijiAcount"]),
                    cpr_Touzi = reader["cpr_Touzi"] == null ? 0 : Convert.ToDecimal(reader["cpr_Touzi"]),
                    cpr_ShijiTouzi = reader["cpr_ShijiTouzi"] == null ? 0 : Convert.ToDecimal(reader["cpr_ShijiTouzi"]),
                    cpr_Process = reader["cpr_Process"] == null ? string.Empty : reader["cpr_Process"].ToString(),
                    cpr_SignDate = reader["cpr_SignDate"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["cpr_SignDate"]),
                    cpr_DoneDate = reader["cpr_DoneDate"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["cpr_DoneDate"]),
                    cpr_Mark = reader["cpr_Mark"] == null ? string.Empty : reader["cpr_Mark"].ToString(),
                    BuildArea = reader["BuildArea"] == null ? string.Empty : reader["BuildArea"].ToString(),
                    ChgPeople = reader["ChgPeople"] == null ? string.Empty : reader["ChgPeople"].ToString(),
                    ChgPhone = reader["ChgPhone"] == null ? string.Empty : reader["ChgPhone"].ToString(),
                    ChgJia = reader["ChgJia"] == null ? string.Empty : reader["ChgJia"].ToString(),
                    ChgJiaPhone = reader["ChgJiaPhone"] == null ? string.Empty : reader["ChgJiaPhone"].ToString(),
                    BuildPosition = reader["BuildPosition"] == null ? string.Empty : reader["BuildPosition"].ToString(),
                    Industry = reader["Industry"] == null ? string.Empty : reader["Industry"].ToString(),
                    BuildUnit = reader["BuildUnit"] == null ? string.Empty : reader["BuildUnit"].ToString(),
                    BuildSrc = reader["BuildSrc"] == null ? string.Empty : reader["BuildSrc"].ToString(),
                    TableMaker = reader["TableMaker"] == null ? string.Empty : reader["TableMaker"].ToString(),
                    RegTime = reader["RegTime"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["RegTime"].ToString()),
                    BuildType = reader["BuildType"] == null ? string.Empty : reader["BuildType"].ToString(),
                    StructType = reader["StructType"] == null ? string.Empty : reader["StructType"].ToString(),
                    Floor = reader["Floor"] == null ? string.Empty : reader["Floor"].ToString(),
                    BuildStructType = reader["BuildStructType"] == null ? string.Empty : reader["BuildStructType"].ToString(),
                    MultiBuild = reader["MultiBuild"] == null ? string.Empty : reader["MultiBuild"].ToString()

                };
                resultList.Add(auditListView);
            }

            return resultList;
        }
        /// <summary>
        /// 查询合同基本信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public List<cm_CoperationAuditListView> GetCoperationListView(string whereSql)
        {
            string sql = "select cpr_Id,cst_Id,cpr_No,cpr_Type,cpr_Type2,cpr_Name,cpr_Unit,cpr_Acount,cpr_ShijiAcount,cpr_Touzi,cpr_ShijiTouzi,cpr_Process,cpr_SignDate,cpr_DoneDate,cpr_Mark,BuildArea,ChgPeople,ChgPhone,ChgJia,ChgJiaPhone,BuildPosition,Industry,BuildUnit,BuildSrc,TableMaker,RegTime,BuildType,StructType,Floor,BuildStructType,MultiBuild from cm_Coperation where  1=1 AND cpr_Type!='项目协议'" + whereSql + " ORDER BY cpr_Id DESC ";
            List<cm_CoperationAuditListView> resultList = new List<cm_CoperationAuditListView>();

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            while (reader.Read())
            {
                cm_CoperationAuditListView auditListView = new cm_CoperationAuditListView
                {
                    cpr_Id = Convert.ToInt32(reader["cpr_Id"]),
                    cst_Id = Convert.ToInt32(reader["cst_Id"]),
                    cpr_No = reader["cpr_No"] == null ? string.Empty : reader["cpr_No"].ToString(),
                    cpr_Type = reader["cpr_Type"] == null ? string.Empty : reader["cpr_Type"].ToString(),
                    cpr_Type2 = reader["cpr_Type2"] == null ? string.Empty : reader["cpr_Type2"].ToString(),
                    cpr_Name = reader["cpr_Name"] == null ? string.Empty : reader["cpr_Name"].ToString(),
                    cpr_Unit = reader["cpr_Unit"] == null ? string.Empty : reader["cpr_Unit"].ToString(),
                    cpr_Acount = reader["cpr_Acount"] == null ? 0 : Convert.ToDecimal(reader["cpr_Acount"]),
                    cpr_ShijiAcount = reader["cpr_ShijiAcount"] == null ? 0 : Convert.ToDecimal(reader["cpr_ShijiAcount"]),
                    cpr_Touzi = reader["cpr_Touzi"] == null ? 0 : Convert.ToDecimal(reader["cpr_Touzi"]),
                    cpr_ShijiTouzi = reader["cpr_ShijiTouzi"] == null ? 0 : Convert.ToDecimal(reader["cpr_ShijiTouzi"]),
                    cpr_Process = reader["cpr_Process"] == null ? string.Empty : reader["cpr_Process"].ToString(),
                    cpr_SignDate = reader["cpr_SignDate"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["cpr_SignDate"]),
                    cpr_DoneDate = reader["cpr_DoneDate"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["cpr_DoneDate"]),
                    cpr_Mark = reader["cpr_Mark"] == null ? string.Empty : reader["cpr_Mark"].ToString(),
                    BuildArea = reader["BuildArea"] == null ? string.Empty : reader["BuildArea"].ToString(),
                    ChgPeople = reader["ChgPeople"] == null ? string.Empty : reader["ChgPeople"].ToString(),
                    ChgPhone = reader["ChgPhone"] == null ? string.Empty : reader["ChgPhone"].ToString(),
                    ChgJia = reader["ChgJia"] == null ? string.Empty : reader["ChgJia"].ToString(),
                    ChgJiaPhone = reader["ChgJiaPhone"] == null ? string.Empty : reader["ChgJiaPhone"].ToString(),
                    BuildPosition = reader["BuildPosition"] == null ? string.Empty : reader["BuildPosition"].ToString(),
                    Industry = reader["Industry"] == null ? string.Empty : reader["Industry"].ToString(),
                    BuildUnit = reader["BuildUnit"] == null ? string.Empty : reader["BuildUnit"].ToString(),
                    BuildSrc = reader["BuildSrc"] == null ? string.Empty : reader["BuildSrc"].ToString(),
                    TableMaker = reader["TableMaker"] == null ? string.Empty : reader["TableMaker"].ToString(),
                    RegTime = reader["RegTime"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["RegTime"].ToString()),
                    BuildType = reader["BuildType"] == null ? string.Empty : reader["BuildType"].ToString(),
                    StructType = reader["StructType"] == null ? string.Empty : reader["StructType"].ToString(),
                    Floor = reader["Floor"] == null ? string.Empty : reader["Floor"].ToString(),
                    BuildStructType = reader["BuildStructType"] == null ? string.Empty : reader["BuildStructType"].ToString(),
                    MultiBuild = reader["MultiBuild"] == null ? string.Empty : reader["MultiBuild"].ToString()
                };
                resultList.Add(auditListView);
            }

            return resultList;
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_Coperation_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 获取需要分页的数据总数
        /// </summary>
        public object GetListPageProcEditCount(string query)
        {
            return DBUtility.DbHelperSQL.RunProcedureScalar("P_cm_CoperationEdit_Count", new SqlParameter[] { new SqlParameter("@query", query) });
        }
        /// <summary>
        /// 查询合同基本信息--修改使用
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public List<cm_CoperationAuditEditListView> GetCoperationEditListView(string query, int startIndex, int endIndex)
        {
            List<cm_CoperationAuditEditListView> resultList = new List<cm_CoperationAuditEditListView>();

            SqlDataReader reader = DBUtility.DbHelperSQL.RunProcedure("P_cm_CoperationEdit", new SqlParameter[] { new SqlParameter("@startIndex", startIndex), new SqlParameter("@endIndex", endIndex), new SqlParameter("@query", query) });

            while (reader.Read())
            {
                cm_CoperationAuditEditListView auditListView = new cm_CoperationAuditEditListView
                {
                    cpr_Id = Convert.ToInt32(reader["cpr_Id"]),
                    cst_Id = Convert.ToInt32(reader["cst_Id"]),
                    cpr_No = reader["cpr_No"] == null ? string.Empty : reader["cpr_No"].ToString(),
                    cpr_Type = reader["cpr_Type"] == null ? string.Empty : reader["cpr_Type"].ToString(),
                    cpr_Type2 = reader["cpr_Type2"] == null ? string.Empty : reader["cpr_Type2"].ToString(),
                    cpr_Name = reader["cpr_Name"] == null ? string.Empty : reader["cpr_Name"].ToString(),
                    cpr_Unit = reader["cpr_Unit"] == null ? string.Empty : reader["cpr_Unit"].ToString(),
                    cpr_Acount = reader["cpr_Acount"] == null ? 0 : Convert.ToDecimal(reader["cpr_Acount"]),
                    cpr_ShijiAcount = reader["cpr_ShijiAcount"] == null ? 0 : Convert.ToDecimal(reader["cpr_ShijiAcount"]),
                    cpr_Touzi = reader["cpr_Touzi"] == null ? 0 : Convert.ToDecimal(reader["cpr_Touzi"]),
                    cpr_ShijiTouzi = reader["cpr_ShijiTouzi"] == null ? 0 : Convert.ToDecimal(reader["cpr_ShijiTouzi"]),
                    cpr_Process = reader["cpr_Process"] == null ? string.Empty : reader["cpr_Process"].ToString(),
                    cpr_SignDate = reader["cpr_SignDate"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["cpr_SignDate"]),
                    cpr_DoneDate = reader["cpr_DoneDate"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["cpr_DoneDate"]),
                    cpr_Mark = reader["cpr_Mark"] == null ? string.Empty : reader["cpr_Mark"].ToString(),
                    BuildArea = reader["BuildArea"] == null ? string.Empty : reader["BuildArea"].ToString(),
                    ChgPeople = reader["ChgPeople"] == null ? string.Empty : reader["ChgPeople"].ToString(),
                    ChgPhone = reader["ChgPhone"] == null ? string.Empty : reader["ChgPhone"].ToString(),
                    ChgJia = reader["ChgJia"] == null ? string.Empty : reader["ChgJia"].ToString(),
                    ChgJiaPhone = reader["ChgJiaPhone"] == null ? string.Empty : reader["ChgJiaPhone"].ToString(),
                    BuildPosition = reader["BuildPosition"] == null ? string.Empty : reader["BuildPosition"].ToString(),
                    Industry = reader["Industry"] == null ? string.Empty : reader["Industry"].ToString(),
                    BuildUnit = reader["BuildUnit"] == null ? string.Empty : reader["BuildUnit"].ToString(),
                    BuildSrc = reader["BuildSrc"] == null ? string.Empty : reader["BuildSrc"].ToString(),
                    TableMaker = reader["TableMaker"] == null ? string.Empty : reader["TableMaker"].ToString(),
                    RegTime = reader["RegTime"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["RegTime"].ToString()),
                    BuildType = reader["BuildType"] == null ? string.Empty : reader["BuildType"].ToString(),
                    StructType = reader["StructType"] == null ? string.Empty : reader["StructType"].ToString(),
                    Floor = reader["Floor"] == null ? string.Empty : reader["Floor"].ToString(),
                    BuildStructType = reader["BuildStructType"] == null ? string.Empty : reader["BuildStructType"].ToString(),
                    MultiBuild = reader["MultiBuild"] == null ? string.Empty : reader["MultiBuild"].ToString(),


                };
                resultList.Add(auditListView);
            }

            return resultList;
        }
        /// <summary>
        /// 根据合同id查询修改状态
        /// </summary>
        /// <param name="SysNo"></param>
        /// <returns></returns>
        public TG.Model.cm_CoperationAuditEdit GetModelEditByCopno(int SysNo)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" SysNo,CoperationSysNo,Options,InUser,InDate,Status,UndertakeProposal,OperateDepartmentProposal,TechnologyDepartmentProposal,GeneralManagerProposal,BossProposal,AuditUser,AuditDate,ManageLevel,NeedLegalAdviser,LegalAdviserProposal,ChangeDetails ");
            strSql.Append(" from cm_AuditRecordEdit ");
            strSql.Append(" where CoperationSysNo=" + SysNo + "");
            strSql.Append("order by SysNo desc");
            SqlDataReader reader = DbHelperSQL.ExecuteReader(strSql.ToString());

            reader.Read();

            TG.Model.cm_CoperationAuditEdit coperationAuditEntity = TG.Common.EntityBuilder.EntityBuilder<TG.Model.cm_CoperationAuditEdit>.BuilderEntity(reader);

            reader.Close();

            return coperationAuditEntity;

        }
    }
}
