﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using TG.DBUtility;

namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_ProjectStageValueConfig
    /// </summary>
    public partial class cm_ProjectStageValueConfig
    {
        public cm_ProjectStageValueConfig()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectStageValueConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_ProjectStageValueConfig(");
            strSql.Append("ProjectStatues,ItemType,ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent)");
            strSql.Append(" values (");
            strSql.Append("@ProjectStatues,@ItemType,@ProgramPercent,@preliminaryPercent,@WorkDrawPercent,@LateStagePercent)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ProjectStatues", SqlDbType.Int,4),
					new SqlParameter("@ItemType", SqlDbType.NVarChar,50),
					new SqlParameter("@ProgramPercent", SqlDbType.Decimal,9),
					new SqlParameter("@preliminaryPercent", SqlDbType.Decimal,9),
					new SqlParameter("@WorkDrawPercent", SqlDbType.Decimal,9),
					new SqlParameter("@LateStagePercent", SqlDbType.Decimal,9)};
            parameters[0].Value = model.ProjectStatues;
            parameters[1].Value = model.ItemType;
            parameters[2].Value = model.ProgramPercent;
            parameters[3].Value = model.preliminaryPercent;
            parameters[4].Value = model.WorkDrawPercent;
            parameters[5].Value = model.LateStagePercent;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectStageValueConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ProjectStageValueConfig set ");
            strSql.Append("ProjectStatues=@ProjectStatues,");
            strSql.Append("ItemType=@ItemType,");
            strSql.Append("ProgramPercent=@ProgramPercent,");
            strSql.Append("preliminaryPercent=@preliminaryPercent,");
            strSql.Append("WorkDrawPercent=@WorkDrawPercent,");
            strSql.Append("LateStagePercent=@LateStagePercent");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ProjectStatues", SqlDbType.Int,4),
					new SqlParameter("@ItemType", SqlDbType.NVarChar,50),
					new SqlParameter("@ProgramPercent", SqlDbType.Decimal,9),
					new SqlParameter("@preliminaryPercent", SqlDbType.Decimal,9),
					new SqlParameter("@WorkDrawPercent", SqlDbType.Decimal,9),
					new SqlParameter("@LateStagePercent", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.ProjectStatues;
            parameters[1].Value = model.ItemType;
            parameters[2].Value = model.ProgramPercent;
            parameters[3].Value = model.preliminaryPercent;
            parameters[4].Value = model.WorkDrawPercent;
            parameters[5].Value = model.LateStagePercent;
            parameters[6].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            return rows;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProjectStageValueConfig ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_ProjectStageValueConfig ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectStageValueConfig GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,ProjectStatues,ItemType,ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent from cm_ProjectStageValueConfig ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_ProjectStageValueConfig model = new TG.Model.cm_ProjectStageValueConfig();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProjectStatues"] != null && ds.Tables[0].Rows[0]["ProjectStatues"].ToString() != "")
                {
                    model.ProjectStatues = int.Parse(ds.Tables[0].Rows[0]["ProjectStatues"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ItemType"] != null && ds.Tables[0].Rows[0]["ItemType"].ToString() != "")
                {
                    model.ItemType = ds.Tables[0].Rows[0]["ItemType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ProgramPercent"] != null && ds.Tables[0].Rows[0]["ProgramPercent"].ToString() != "")
                {
                    model.ProgramPercent = decimal.Parse(ds.Tables[0].Rows[0]["ProgramPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["preliminaryPercent"] != null && ds.Tables[0].Rows[0]["preliminaryPercent"].ToString() != "")
                {
                    model.preliminaryPercent = decimal.Parse(ds.Tables[0].Rows[0]["preliminaryPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["WorkDrawPercent"] != null && ds.Tables[0].Rows[0]["WorkDrawPercent"].ToString() != "")
                {
                    model.WorkDrawPercent = decimal.Parse(ds.Tables[0].Rows[0]["WorkDrawPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["LateStagePercent"] != null && ds.Tables[0].Rows[0]["LateStagePercent"].ToString() != "")
                {
                    model.LateStagePercent = decimal.Parse(ds.Tables[0].Rows[0]["LateStagePercent"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,ProjectStatues,ItemType,ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent ");
            strSql.Append(" FROM cm_ProjectStageValueConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetListAll()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"select ID,ProjectStatues,ItemType,ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent FROM cm_ProjectStageValueConfig where ProjectStatues=0
                            select ID,ProjectStatues,ItemType,ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent FROM cm_ProjectStageValueConfig where ProjectStatues=1
                            select ID,ProjectStatues,ItemType,ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent FROM cm_ProjectStageValueConfig where ProjectStatues=2
                            select ID,ProjectStatues,ItemType,ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent FROM cm_ProjectStageValueConfig where ProjectStatues=3
                            select * FROM cm_ProjectStageSpeValueConfig ORDER BY ID
                            select * FROM cm_ProjectDesignProcessValueConfig
                            select * FROM dbo.cm_ProjectOutDoorValueConfig  ORDER BY ID
                            select ID,ProjectStatues,ItemType,ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent FROM cm_ProjectStageValueConfig where ProjectStatues=10
                          ");
          
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,ProjectStatues,ItemType,ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent ");
            strSql.Append(" FROM cm_ProjectStageValueConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_ProjectStageValueConfig ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_ProjectStageValueConfig T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "cm_ProjectStageValueConfig";
            parameters[1].Value = "ID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}
