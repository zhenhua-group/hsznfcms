﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using TG.DBUtility;
using TG.Model;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class cm_TranProjectValueAuditRecord
    {
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_TranProjectValueAuditRecord model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_TranProjectValueAuditRecord(");
            strSql.Append("ProSysNo,AllotID,Status,OneSuggestion,AuditUser,AuditDate,InUser,InDate,ItemType)");
            strSql.Append(" values (");
            strSql.Append("@ProSysNo,@AllotID,@Status,@OneSuggestion,@AuditUser,@AuditDate,@InUser,@InDate,@ItemType)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ProSysNo", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Char,1),
					new SqlParameter("@OneSuggestion", SqlDbType.NVarChar,600),
					new SqlParameter("@AuditUser", SqlDbType.NVarChar,300),
					new SqlParameter("@AuditDate", SqlDbType.NVarChar,300),
					new SqlParameter("@InUser", SqlDbType.Int,4),
					new SqlParameter("@InDate", SqlDbType.DateTime),
                    new SqlParameter("@ItemType", SqlDbType.NVarChar)};
            parameters[0].Value = model.ProSysNo;
            parameters[1].Value = model.AllotID;
            parameters[2].Value = model.Status;
            parameters[3].Value = model.OneSuggestion;
            parameters[4].Value = model.AuditUser;
            parameters[5].Value = model.AuditDate;
            parameters[6].Value = model.InUser;
            parameters[7].Value = model.InDate;
            parameters[8].Value = model.ItemType;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_TranProjectValueAuditRecord model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_TranProjectValueAuditRecord set ");
            strSql.Append("ProSysNo=@ProSysNo,");
            strSql.Append("AllotID=@AllotID,");
            strSql.Append("Status=@Status,");
            strSql.Append("OneSuggestion=@OneSuggestion,");
            strSql.Append("AuditUser=@AuditUser,");
            strSql.Append("AuditDate=@AuditDate,");
            strSql.Append("InUser=@InUser,");
            strSql.Append("InDate=@InDate");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@ProSysNo", SqlDbType.Int,4),
					new SqlParameter("@AllotID", SqlDbType.Int,4),
					new SqlParameter("@Status", SqlDbType.Char,1),
					new SqlParameter("@OneSuggestion", SqlDbType.NVarChar,600),
					new SqlParameter("@AuditUser", SqlDbType.NVarChar,300),
					new SqlParameter("@AuditDate", SqlDbType.NVarChar,300),
					new SqlParameter("@InUser", SqlDbType.Int,4),
					new SqlParameter("@InDate", SqlDbType.DateTime),
					new SqlParameter("@SysNo", SqlDbType.Int,4)};
            parameters[0].Value = model.ProSysNo;
            parameters[1].Value = model.AllotID;
            parameters[2].Value = model.Status;
            parameters[3].Value = model.OneSuggestion;
            parameters[4].Value = model.AuditUser;
            parameters[5].Value = model.AuditDate;
            parameters[6].Value = model.InUser;
            parameters[7].Value = model.InDate;
            parameters[8].Value = model.SysNo;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SysNo)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_TranProjectValueAuditRecord ");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_TranProjectValueAuditRecord GetModel(int SysNo)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 SysNo,ProSysNo,AllotID,Status,OneSuggestion,AuditUser,AuditDate,InUser,InDate from cm_TranProjectValueAuditRecord ");
            strSql.Append(" where SysNo=@SysNo");
            SqlParameter[] parameters = {
					new SqlParameter("@SysNo", SqlDbType.Int,4)
			};
            parameters[0].Value = SysNo;

            TG.Model.cm_TranProjectValueAuditRecord model = new TG.Model.cm_TranProjectValueAuditRecord();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["SysNo"] != null && ds.Tables[0].Rows[0]["SysNo"].ToString() != "")
                {
                    model.SysNo = int.Parse(ds.Tables[0].Rows[0]["SysNo"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ProSysNo"] != null && ds.Tables[0].Rows[0]["ProSysNo"].ToString() != "")
                {
                    model.ProSysNo = int.Parse(ds.Tables[0].Rows[0]["ProSysNo"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotID"] != null && ds.Tables[0].Rows[0]["AllotID"].ToString() != "")
                {
                    model.AllotID = int.Parse(ds.Tables[0].Rows[0]["AllotID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Status"] != null && ds.Tables[0].Rows[0]["Status"].ToString() != "")
                {
                    model.Status = ds.Tables[0].Rows[0]["Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["OneSuggestion"] != null && ds.Tables[0].Rows[0]["OneSuggestion"].ToString() != "")
                {
                    model.OneSuggestion = ds.Tables[0].Rows[0]["OneSuggestion"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditUser"] != null && ds.Tables[0].Rows[0]["AuditUser"].ToString() != "")
                {
                    model.AuditUser = ds.Tables[0].Rows[0]["AuditUser"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AuditDate"] != null && ds.Tables[0].Rows[0]["AuditDate"].ToString() != "")
                {
                    model.AuditDate = ds.Tables[0].Rows[0]["AuditDate"].ToString();
                }
                if (ds.Tables[0].Rows[0]["InUser"] != null && ds.Tables[0].Rows[0]["InUser"].ToString() != "")
                {
                    model.InUser = int.Parse(ds.Tables[0].Rows[0]["InUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["InDate"] != null && ds.Tables[0].Rows[0]["InDate"].ToString() != "")
                {
                    model.InDate = DateTime.Parse(ds.Tables[0].Rows[0]["InDate"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 更新经济所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateTranJjsProjectAllotValue(TG.Model.cm_TranProjectValueAuditRecord dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;
                string status = "";
                if (dataEntity.Status.Trim() == "S")
                {
                    status = "D";
                }
                else
                {
                    status = "E";
                }
                //更新 分配表状态
                command.CommandText = "update cm_TranProjectValueAuditRecord set Status='" + status + "',OneSuggestion='" + dataEntity.OneSuggestion + "',AuditUser='" + dataEntity.AuditUser + "',AuditDate='" + DateTime.Now + "' where SysNo=" + dataEntity.SysNo + " ";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "update cm_TranjjsProjectValueAllot set Status='" + dataEntity.Status + "' where pro_ID=" + dataEntity.ProSysNo + " and AllotID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "update cm_jjsProjectValueAllotDetail set Status='" + dataEntity.Status + "' where proID=" + dataEntity.ProSysNo + " and AllotID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "update cm_ProjectValueByMemberDetails set Status='" + dataEntity.Status + "' where proID=" + dataEntity.ProSysNo + " and AllotID=" + dataEntity.AllotID + " and TranType='jjs'";
                command.ExecuteNonQuery();

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        /// 更新暖通所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateTranHavcProjectAllotValue(TG.Model.cm_TranProjectValueAuditRecord dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                string status = "";
                if (dataEntity.Status.Trim() == "S")
                {
                    status = "D";
                }
                else
                {
                    status = "E";
                }

                //更新 分配表状态
                command.CommandText = "update cm_TranProjectValueAuditRecord set Status='" + status + "',OneSuggestion='" + dataEntity.OneSuggestion + "',AuditUser='" + dataEntity.AuditUser + "',AuditDate='" + DateTime.Now + "' where SysNo=" + dataEntity.SysNo + " ";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "update cm_TranProjectValueAllot set Status='" + dataEntity.Status + "' where pro_ID=" + dataEntity.ProSysNo + " and AllotID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "update cm_ProjectValueByMemberDetails set Status='" + dataEntity.Status + "' where proID=" + dataEntity.ProSysNo + " and AllotID=" + dataEntity.AllotID + " and TranType='nts'";
                command.ExecuteNonQuery();

                if (dataEntity.Status == "S")
                {
                    command.CommandText = "update cm_ProjectSecondValueAllot set HavcCount=" + dataEntity.TranCount + " where ProID=" + dataEntity.ProSysNo + " and AllotID=" + dataEntity.AllotID + "";
                    command.ExecuteNonQuery();
                }

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int DeleteTranJjsProjectAllotValue(int proid, int allotID, int sysID)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //更新 分配表状态
                command.CommandText = "delete  from cm_TranProjectValueAuditRecord  where ProSysNo=" + proid + " and AllotID=" + allotID + " and ItemType='jjs'";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_TranjjsProjectValueAllot  where pro_ID=" + proid + " and AllotID=" + allotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_jjsProjectValueAllotDetail  where proID=" + proid + " and AllotID=" + allotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectValueByMemberDetails  where proID=" + proid + " and AllotID=" + allotID + " and TranType='jjs'";
                command.ExecuteNonQuery();

                //删除人员审批表
                command.CommandText = "delete from  cm_ProjectValueByMemberAuditStatus  where proID=" + proid + " and AllotID=" + allotID + " and TranType='jjs'";
                command.ExecuteNonQuery();

                command.CommandText = "  delete from cm_SysMsg where  ReferenceSysNo='allotID=" + allotID + "&proID=" + proid + "&ValueAllotAuditSysNo=" + sysID + "&proType=tranjjs&tranType=jjs' and  ( MsgType=25  or MsgType=28 )";
                command.ExecuteNonQuery();

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        /// 删除转暖通所
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int DeleteTranHavcProjectAllotValue(int proid, int allotID, string sysID)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //更新 分配表状态
                command.CommandText = "delete  from cm_TranProjectValueAuditRecord  where ProSysNo=" + proid + " and AllotID=" + allotID + " and ItemType='nts'";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_TranProjectValueAllot  where pro_ID=" + proid + " and AllotID=" + allotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectValueByMemberDetails  where proID=" + proid + " and AllotID=" + allotID + " and TranType='nts'";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectValueByMemberAuditStatus  where proID=" + proid + " and AllotID=" + allotID + " and TranType='nts'";
                command.ExecuteNonQuery();
                //删除消息
                command.CommandText = "  delete from cm_SysMsg where  ReferenceSysNo='allotID=" + allotID + "&proID=" + proid + "&ValueAllotAuditSysNo=" + sysID + "&proType=trannts&tranType=nts' and  ( MsgType=25  or MsgType=29 )";
                command.ExecuteNonQuery();


                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int DeleteJjsProjectAllotValue(int proid, int allotID)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //更新 分配表状态
                command.CommandText = "delete  from cm_ProjectValueAuditRecord  where ProSysNo=" + proid + " and AllotID=" + allotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectValueAllot  where pro_ID=" + proid + " and ID=" + allotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_jjsProjectValueAllotDetail  where proID=" + proid + " and AllotID=" + allotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectValueByMemberDetails  where proID=" + proid + " and AllotID=" + allotID + " ";
                command.ExecuteNonQuery();

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }


        /// <summary>
        /// 新增土建所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertTranBulidingProjectAllotValueByMemberDetail(TranBulidingProjectValueAllotByMemberEntity dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;


                //新规项目各设计阶段产值分配
                dataEntity.Stage.ForEach((stage) =>
                {
                    command.CommandText = "insert into cm_ProjectStageValueDetails(ALLOTID,[ItemType],[ProgramAmount],[preliminaryAmount],[WorkDrawAmount],[LateStageAmount],ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent)values(" + dataEntity.AllotID + ",N' " + stage.ItemType + "'," + stage.ProgramAmount + "," + stage.preliminaryAmount + "," + stage.WorkDrawAmount + "," + stage.LateStageAmount + "," + stage.ProgramPercent + "," + stage.preliminaryPercent + "," + stage.WorkDrawPercent + "," + stage.LateStagePercent + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段专业之间产值分配

                dataEntity.StageSpe.ForEach((stageSpe) =>
                {
                    command.CommandText = "insert into cm_ProjectStageSpeDetails(ALLOT,Specialty,ProgramCount,preliminaryCount,WorkDrawCount,LateStageCount,ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent)values(" + dataEntity.AllotID + ",N' " + stageSpe.Specialty + "'," + stageSpe.ProgramCount + "," + stageSpe.preliminaryCount + "," + stageSpe.WorkDrawCount + "," + stageSpe.LateStageCount + "," + stageSpe.ProgramPercent + "," + stageSpe.preliminaryPercent + "," + stageSpe.WorkDrawPercent + "," + stageSpe.LateStagePercent + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段专业之间产值分配
                dataEntity.DesignProcess.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + dataEntity.AllotID + "," + 0 + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段专业之间产值分配
                dataEntity.DesignProcessTwo.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + dataEntity.AllotID + "," + 1 + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段专业之间产值分配
                dataEntity.DesignProcessThree.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + dataEntity.AllotID + "," + 2 + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                dataEntity.DesignProcessFour.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + dataEntity.AllotID + "," + 3 + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                dataEntity.DesignProcessFive.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + dataEntity.AllotID + "," + dataEntity.ItemType + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                //室外工程
                dataEntity.OutDoor.ForEach((stageSpe) =>
                {
                    command.CommandText = "insert into cm_ProjectOutDoorValueDetails(ALLOT,Status,Specialty,SpecialtyPercent,SpecialtyCount)values(" + dataEntity.AllotID + "," + stageSpe.Status + ",'" + stageSpe.Specialty + "'," + stageSpe.SpecialtyPercent + "," + stageSpe.SpecialtyCount + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段产值分配
                dataEntity.ValueByMember.ForEach((stage) =>
                {
                    decimal designPercent = 0;
                    if (!string.IsNullOrEmpty(stage.DesignPercent))
                    {
                        designPercent = decimal.Parse(stage.DesignPercent);
                    }

                    decimal prooPercent = 0;
                    if (!string.IsNullOrEmpty(stage.ProofreadPercent))
                    {
                        prooPercent = decimal.Parse(stage.ProofreadPercent);
                    }

                    decimal headPercent = 0;
                    if (!string.IsNullOrEmpty(stage.SpecialtyHeadPercent))
                    {
                        headPercent = decimal.Parse(stage.SpecialtyHeadPercent);
                    }

                    decimal auditPercent = 0;
                    if (!string.IsNullOrEmpty(stage.AuditPercent))
                    {
                        auditPercent = decimal.Parse(stage.AuditPercent);
                    }
                    command.CommandText = "insert into cm_ProjectValueByMemberDetails(ProID,AllotID,ItemType,mem_ID,AuditUser,DesignPercent,DesignCount,SpecialtyHeadPercent,SpecialtyHeadCount,AuditPercent,AuditCount,ProofreadPercent,ProofreadCount,SecondValue,Status,IsExternal,TranType)values(" + dataEntity.ProNo + "," + dataEntity.AllotID + "," + stage.ItemType + "," + stage.mem_ID + "," + dataEntity.AuditUser + "," + designPercent + "," + stage.DesignCount + "," + headPercent + "," + stage.SpecialtyHeadCount + "," + auditPercent + "," + stage.AuditCount + "," + prooPercent + "," + stage.ProofreadCount + "," + 1 + ",'A','" + stage.IsExternal + "','tjs')";
                    command.ExecuteNonQuery();
                });

                //更新上一次状态
                command.CommandText = " update cm_TranBulidingProjectValueAllot set LastItemType=ItemType where Pro_ID=" + dataEntity.ProNo + " and AllotID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        ///转土建所
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateTranBulidingProjectAllotValue(TG.Model.cm_TranProjectValueAuditRecord dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                string status = "";
                if (dataEntity.Status.Trim() == "S")
                {
                    status = "D";
                }
                else
                {
                    status = "E";
                }
                //更新 分配表状态
                command.CommandText = "update cm_TranProjectValueAuditRecord set Status='" + status + "',OneSuggestion='" + dataEntity.OneSuggestion + "',AuditUser='" + dataEntity.AuditUser + "',AuditDate='" + DateTime.Now + "' where SysNo=" + dataEntity.SysNo + " ";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "update cm_TranBulidingProjectValueAllot set Status='" + dataEntity.Status + "' where pro_ID=" + dataEntity.ProSysNo + " and AllotID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "update cm_ProjectValueByMemberDetails set Status='" + dataEntity.Status + "' where proID=" + dataEntity.ProSysNo + " and AllotID=" + dataEntity.AllotID + " and TranType='tjs'";
                command.ExecuteNonQuery();

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }


        /// <summary>
        /// 删除转土建所
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int DeleteTranBudlingProjectAllotValue(int proid, int allotID, string sysID)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                //更新 分配表状态
                command.CommandText = "delete  from cm_TranProjectValueAuditRecord  where ProSysNo=" + proid + " and AllotID=" + allotID + " and  ItemType='tjs'";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_TranBulidingProjectValueAllot  where pro_ID=" + proid + " and AllotID=" + allotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectValueByMemberDetails  where proID=" + proid + " and AllotID=" + allotID + " and TranType='tjs'";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectStageValueDetails  where  ALLOTID=" + allotID + " ";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectStageSpeDetails  where  ALLOT=" + allotID + " ";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectOutDoorValueDetails  where  ALLOT=" + allotID + " ";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectDesignProcessValueDetails  where  AllotID=" + allotID + " ";
                command.ExecuteNonQuery();

                //删除人员审批表
                command.CommandText = "delete from  cm_ProjectValueByMemberAuditStatus  where proID=" + proid + " and AllotID=" + allotID + " and TranType='tjs'";
                command.ExecuteNonQuery();

                command.CommandText = "  delete from cm_SysMsg where  ReferenceSysNo='allotID=" + allotID + "&proID=" + proid + "&ValueAllotAuditSysNo=" + sysID + "&proType=trantjs&tranType=tjs' and  ( MsgType=25  or MsgType=30 )";
                command.ExecuteNonQuery();
                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        /// <summary>
        /// 更新状态
        /// </summary>
        /// <param name="proid"></param>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public int UpdateTranStatus(int proid, int allotID, string status, string itemType)
        {
            string sql = "update cm_TranProjectValueAuditRecord set Status='" + status + "' where ProSysNo=" + proid + " and AllotID=" + allotID + " and ItemType='" + itemType + "' ";

            return DbHelperSQL.ExecuteSql(sql);
        }

        /// <summary>
        /// 分配记录表
        /// </summary>
        /// <param name="allotAuditSysNo"></param>
        /// <returns></returns>
        public TG.Model.cm_TranProjectValueAuditRecord GetAllotAuditEntity(int allotAuditSysNo)
        {
            string sql = " SELECT * FROM dbo.cm_TranProjectValueAuditRecord WHERE SysNo=" + allotAuditSysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            TG.Model.cm_TranProjectValueAuditRecord resultEntity = EntityBuilder<TG.Model.cm_TranProjectValueAuditRecord>.BuilderEntity(reader);

            reader.Close();

            return resultEntity;
        }

        /// <summary>
        /// 更新土建所项目人员产值
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateTranBulidingProjectAllotValueByMemberDetail(TranBulidingProjectValueAllotByMemberEntity dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                command.CommandText = "delete from  cm_ProjectStageValueDetails  where ALLOTID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectStageSpeDetails  where  ALLOT=" + dataEntity.AllotID + " ";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectOutDoorValueDetails  where  ALLOT=" + dataEntity.AllotID + " ";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectDesignProcessValueDetails  where  AllotID=" + dataEntity.AllotID + " ";
                command.ExecuteNonQuery();

                //更新 分配表状态
                command.CommandText = "delete from  cm_ProjectValueByMemberDetails  where proID=" + dataEntity.ProNo + " and AllotID=" + dataEntity.AllotID + " and TranType='tjs'";
                command.ExecuteNonQuery();

                //新规项目各设计阶段产值分配
                dataEntity.Stage.ForEach((stage) =>
                {
                    command.CommandText = "insert into cm_ProjectStageValueDetails(ALLOTID,[ItemType],[ProgramAmount],[preliminaryAmount],[WorkDrawAmount],[LateStageAmount],ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent)values(" + dataEntity.AllotID + ",N' " + stage.ItemType + "'," + stage.ProgramAmount + "," + stage.preliminaryAmount + "," + stage.WorkDrawAmount + "," + stage.LateStageAmount + "," + stage.ProgramPercent + "," + stage.preliminaryPercent + "," + stage.WorkDrawPercent + "," + stage.LateStagePercent + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段专业之间产值分配

                dataEntity.StageSpe.ForEach((stageSpe) =>
                {
                    command.CommandText = "insert into cm_ProjectStageSpeDetails(ALLOT,Specialty,ProgramCount,preliminaryCount,WorkDrawCount,LateStageCount,ProgramPercent,preliminaryPercent,WorkDrawPercent,LateStagePercent)values(" + dataEntity.AllotID + ",N' " + stageSpe.Specialty + "'," + stageSpe.ProgramCount + "," + stageSpe.preliminaryCount + "," + stageSpe.WorkDrawCount + "," + stageSpe.LateStageCount + "," + stageSpe.ProgramPercent + "," + stageSpe.preliminaryPercent + "," + stageSpe.WorkDrawPercent + "," + stageSpe.LateStagePercent + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段专业之间产值分配
                dataEntity.DesignProcess.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + dataEntity.AllotID + "," + 0 + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段专业之间产值分配
                dataEntity.DesignProcessTwo.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + dataEntity.AllotID + "," + 1 + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段专业之间产值分配
                dataEntity.DesignProcessThree.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + dataEntity.AllotID + "," + 2 + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                dataEntity.DesignProcessFour.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + dataEntity.AllotID + "," + 3 + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                dataEntity.DesignProcessFive.ForEach((design) =>
                {
                    command.CommandText = "insert into cm_ProjectDesignProcessValueDetails(AllotID,type,[Specialty],[AuditAmount],[SpecialtyHeadAmount],[ProofreadAmount],[DesignAmount],AuditPercent,SpecialtyHeadPercent,ProofreadPercent,DesignPercent)values(" + dataEntity.AllotID + "," + dataEntity.ItemType + ", N'" + design.Specialty + "'," + design.AuditAmount + "," + design.SpecialtyHeadAmount + "," + design.ProofreadAmount + "," + design.DesignAmount + "," + design.AuditPercent + "," + design.SpecialtyHeadPercent + "," + design.ProofreadPercent + "," + design.DesignPercent + ")";
                    command.ExecuteNonQuery();
                });

                //室外工程
                dataEntity.OutDoor.ForEach((stageSpe) =>
                {
                    command.CommandText = "insert into cm_ProjectOutDoorValueDetails(ALLOT,Status,Specialty,SpecialtyPercent,SpecialtyCount)values(" + dataEntity.AllotID + "," + stageSpe.Status + ",'" + stageSpe.Specialty + "'," + stageSpe.SpecialtyPercent + "," + stageSpe.SpecialtyCount + ")";
                    command.ExecuteNonQuery();
                });

                //新规项目各设计阶段产值分配
                dataEntity.ValueByMember.ForEach((stage) =>
                {
                    decimal designPercent = 0;
                    if (!string.IsNullOrEmpty(stage.DesignPercent))
                    {
                        designPercent = decimal.Parse(stage.DesignPercent);
                    }

                    decimal prooPercent = 0;
                    if (!string.IsNullOrEmpty(stage.ProofreadPercent))
                    {
                        prooPercent = decimal.Parse(stage.ProofreadPercent);
                    }

                    decimal headPercent = 0;
                    if (!string.IsNullOrEmpty(stage.SpecialtyHeadPercent))
                    {
                        headPercent = decimal.Parse(stage.SpecialtyHeadPercent);
                    }

                    decimal auditPercent = 0;
                    if (!string.IsNullOrEmpty(stage.AuditPercent))
                    {
                        auditPercent = decimal.Parse(stage.AuditPercent);
                    }
                    command.CommandText = "insert into cm_ProjectValueByMemberDetails(ProID,AllotID,ItemType,mem_ID,AuditUser,DesignPercent,DesignCount,SpecialtyHeadPercent,SpecialtyHeadCount,AuditPercent,AuditCount,ProofreadPercent,ProofreadCount,SecondValue,Status,IsExternal,TranType)values(" + dataEntity.ProNo + "," + dataEntity.AllotID + "," + stage.ItemType + "," + stage.mem_ID + "," + dataEntity.AuditUser + "," + designPercent + "," + stage.DesignCount + "," + headPercent + "," + stage.SpecialtyHeadCount + "," + auditPercent + "," + stage.AuditCount + "," + prooPercent + "," + stage.ProofreadCount + "," + 1 + ",'A','" + stage.IsExternal + "','tjs')";
                    command.ExecuteNonQuery();
                });

                //更新上一次状态
                command.CommandText = " update cm_TranBulidingProjectValueAllot set LastItemType=ItemType where Pro_ID=" + dataEntity.ProNo + " and AllotID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        #endregion  Method
    } 
}
