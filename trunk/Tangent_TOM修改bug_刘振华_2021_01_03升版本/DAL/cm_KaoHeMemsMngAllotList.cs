﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeMemsMngAllotList
	/// </summary>
	public partial class cm_KaoHeMemsMngAllotList
	{
		public cm_KaoHeMemsMngAllotList()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeMemsMngAllotList model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeMemsMngAllotList(");
			strSql.Append("UnitID,UnitName,RenwuID,RenwuName,KaoHeUnitID,InsertUserID)");
			strSql.Append(" values (");
			strSql.Append("@UnitID,@UnitName,@RenwuID,@RenwuName,@KaoHeUnitID,@InsertUserID)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.NVarChar,50),
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@RenwuName", SqlDbType.NVarChar,50),
					new SqlParameter("@KaoHeUnitID", SqlDbType.Int,4),
					new SqlParameter("@InsertUserID", SqlDbType.Int,4)};
			parameters[0].Value = model.UnitID;
			parameters[1].Value = model.UnitName;
			parameters[2].Value = model.RenwuID;
			parameters[3].Value = model.RenwuName;
			parameters[4].Value = model.KaoHeUnitID;
			parameters[5].Value = model.InsertUserID;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeMemsMngAllotList model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeMemsMngAllotList set ");
			strSql.Append("UnitID=@UnitID,");
			strSql.Append("UnitName=@UnitName,");
			strSql.Append("RenwuID=@RenwuID,");
			strSql.Append("RenwuName=@RenwuName,");
			strSql.Append("KaoHeUnitID=@KaoHeUnitID,");
			strSql.Append("InsertUserID=@InsertUserID");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.NVarChar,50),
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@RenwuName", SqlDbType.NVarChar,50),
					new SqlParameter("@KaoHeUnitID", SqlDbType.Int,4),
					new SqlParameter("@InsertUserID", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.UnitID;
			parameters[1].Value = model.UnitName;
			parameters[2].Value = model.RenwuID;
			parameters[3].Value = model.RenwuName;
			parameters[4].Value = model.KaoHeUnitID;
			parameters[5].Value = model.InsertUserID;
			parameters[6].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsMngAllotList ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeMemsMngAllotList ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeMemsMngAllotList GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,UnitID,UnitName,RenwuID,RenwuName,KaoHeUnitID,InsertUserID from cm_KaoHeMemsMngAllotList ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeMemsMngAllotList model=new TG.Model.cm_KaoHeMemsMngAllotList();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeMemsMngAllotList DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeMemsMngAllotList model=new TG.Model.cm_KaoHeMemsMngAllotList();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["UnitID"]!=null && row["UnitID"].ToString()!="")
				{
					model.UnitID=int.Parse(row["UnitID"].ToString());
				}
				if(row["UnitName"]!=null)
				{
					model.UnitName=row["UnitName"].ToString();
				}
				if(row["RenwuID"]!=null && row["RenwuID"].ToString()!="")
				{
					model.RenwuID=int.Parse(row["RenwuID"].ToString());
				}
				if(row["RenwuName"]!=null)
				{
					model.RenwuName=row["RenwuName"].ToString();
				}
				if(row["KaoHeUnitID"]!=null && row["KaoHeUnitID"].ToString()!="")
				{
					model.KaoHeUnitID=int.Parse(row["KaoHeUnitID"].ToString());
				}
				if(row["InsertUserID"]!=null && row["InsertUserID"].ToString()!="")
				{
					model.InsertUserID=int.Parse(row["InsertUserID"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,UnitID,UnitName,RenwuID,RenwuName,KaoHeUnitID,InsertUserID ");
			strSql.Append(" FROM cm_KaoHeMemsMngAllotList ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,UnitID,UnitName,RenwuID,RenwuName,KaoHeUnitID,InsertUserID ");
			strSql.Append(" FROM cm_KaoHeMemsMngAllotList ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeMemsMngAllotList ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeMemsMngAllotList T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeMemsMngAllotList";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

