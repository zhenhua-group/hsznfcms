﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.DBUtility;

namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_CostDetails
    /// </summary>
    public partial class cm_CostDetails
    {
        public cm_CostDetails()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("ID", "cm_CostDetails");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from cm_CostDetails");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_CostDetails model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_CostDetails(");
            strSql.Append("costTime,costNum,costSub,costCharge,costOutCharge,costUnit,costUserId,costTypeID,isexpord)");
            strSql.Append(" values (");
            strSql.Append("@costTime,@costNum,@costSub,@costCharge,@costOutCharge,@costUnit,@costUserId,@costTypeID,@isexpord)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@costTime", SqlDbType.DateTime),
					new SqlParameter("@costNum", SqlDbType.NVarChar,50),
					new SqlParameter("@costSub", SqlDbType.NVarChar,500),
					new SqlParameter("@costCharge", SqlDbType.Decimal,9),
					new SqlParameter("@costOutCharge", SqlDbType.Decimal,9),
					new SqlParameter("@costUnit", SqlDbType.Int,4),
					new SqlParameter("@costUserId", SqlDbType.Int,4),
					new SqlParameter("@costTypeID", SqlDbType.Int,4),
					new SqlParameter("@isexpord", SqlDbType.Int,4)};
            parameters[0].Value = model.costTime;
            parameters[1].Value = model.costNum;
            parameters[2].Value = model.costSub;
            parameters[3].Value = model.costCharge;
            parameters[4].Value = model.costOutCharge;
            parameters[5].Value = model.costUnit;
            parameters[6].Value = model.costUserId;
            parameters[7].Value = model.costTypeID;
            parameters[8].Value = model.isexpord;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_CostDetails model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_CostDetails set ");
            strSql.Append("costTime=@costTime,");
            strSql.Append("costNum=@costNum,");
            strSql.Append("costSub=@costSub,");
            strSql.Append("costCharge=@costCharge,");
            strSql.Append("costOutCharge=@costOutCharge,");
            strSql.Append("costUnit=@costUnit,");
            strSql.Append("costUserId=@costUserId,");
            strSql.Append("costTypeID=@costTypeID,");
            strSql.Append("isexpord=@isexpord");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@costTime", SqlDbType.DateTime),
					new SqlParameter("@costNum", SqlDbType.NVarChar,50),
					new SqlParameter("@costSub", SqlDbType.NVarChar,500),
					new SqlParameter("@costCharge", SqlDbType.Decimal,9),
					new SqlParameter("@costOutCharge", SqlDbType.Decimal,9),
					new SqlParameter("@costUnit", SqlDbType.Int,4),
					new SqlParameter("@costUserId", SqlDbType.Int,4),
					new SqlParameter("@costTypeID", SqlDbType.Int,4),
					new SqlParameter("@isexpord", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.costTime;
            parameters[1].Value = model.costNum;
            parameters[2].Value = model.costSub;
            parameters[3].Value = model.costCharge;
            parameters[4].Value = model.costOutCharge;
            parameters[5].Value = model.costUnit;
            parameters[6].Value = model.costUserId;
            parameters[7].Value = model.costTypeID;
            parameters[8].Value = model.isexpord;
            parameters[9].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_CostDetails ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_CostDetails ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_CostDetails GetModel(int ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,costTime,costNum,costSub,costCharge,costOutCharge,costUnit,costUserId,costTypeID,isexpord from cm_CostDetails ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_CostDetails model = new TG.Model.cm_CostDetails();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["costTime"] != null && ds.Tables[0].Rows[0]["costTime"].ToString() != "")
                {
                    model.costTime = DateTime.Parse(ds.Tables[0].Rows[0]["costTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["costNum"] != null && ds.Tables[0].Rows[0]["costNum"].ToString() != "")
                {
                    model.costNum = ds.Tables[0].Rows[0]["costNum"].ToString();
                }
                if (ds.Tables[0].Rows[0]["costSub"] != null && ds.Tables[0].Rows[0]["costSub"].ToString() != "")
                {
                    model.costSub = ds.Tables[0].Rows[0]["costSub"].ToString();
                }
                if (ds.Tables[0].Rows[0]["costCharge"] != null && ds.Tables[0].Rows[0]["costCharge"].ToString() != "")
                {
                    model.costCharge = decimal.Parse(ds.Tables[0].Rows[0]["costCharge"].ToString());
                }
                if (ds.Tables[0].Rows[0]["costOutCharge"] != null && ds.Tables[0].Rows[0]["costOutCharge"].ToString() != "")
                {
                    model.costOutCharge = decimal.Parse(ds.Tables[0].Rows[0]["costOutCharge"].ToString());
                }
                if (ds.Tables[0].Rows[0]["costUnit"] != null && ds.Tables[0].Rows[0]["costUnit"].ToString() != "")
                {
                    model.costUnit = int.Parse(ds.Tables[0].Rows[0]["costUnit"].ToString());
                }
                if (ds.Tables[0].Rows[0]["costUserId"] != null && ds.Tables[0].Rows[0]["costUserId"].ToString() != "")
                {
                    model.costUserId = int.Parse(ds.Tables[0].Rows[0]["costUserId"].ToString());
                }
                if (ds.Tables[0].Rows[0]["costTypeID"] != null && ds.Tables[0].Rows[0]["costTypeID"].ToString() != "")
                {
                    model.costTypeID = int.Parse(ds.Tables[0].Rows[0]["costTypeID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["isexpord"] != null && ds.Tables[0].Rows[0]["isexpord"].ToString() != "")
                {
                    model.isexpord = int.Parse(ds.Tables[0].Rows[0]["isexpord"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,costTime,costNum,costSub,costCharge,costOutCharge,costUnit,costUserId,costTypeID,isexpord ");
            strSql.Append(" FROM cm_CostDetails ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,costTime,costNum,costSub,costCharge,costOutCharge,costUnit,costUserId,costTypeID,isexpord ");
            strSql.Append(" FROM cm_CostDetails ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_CostDetails ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_CostDetails T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "cm_CostDetails";
            parameters[1].Value = "ID";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
    }
}

