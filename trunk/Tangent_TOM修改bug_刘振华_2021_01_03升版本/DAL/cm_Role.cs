﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;
using TG.Model;
using TG.Common.EntityBuilder;

namespace TG.DAL
{
    public class cm_Role
    {
        /// <summary>
        /// 得到所有的角色集合
        /// </summary>
        /// <returns></returns>
        public List<TG.Model.cm_Role> GetRoleList()
        {
            string sql = "select SysNo,RoleName,Users from cm_Role ";

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<TG.Model.cm_Role> roleList = new List<TG.Model.cm_Role>();

            while (reader.Read())
            {
                TG.Model.cm_Role role = new TG.Model.cm_Role
                {
                    SysNo = reader["SysNo"] == null ? 0 : Convert.ToInt32(reader["SysNo"]),
                    RoleName = reader["RoleName"] == null ? "" : reader["RoleName"].ToString(),
                    Users = reader["Users"] == null ? "" : reader["Users"].ToString()
                };
                roleList.Add(role);
            }
            reader.Close();
            return roleList;
        }

        public List<TG.Model.cm_Role> GetRoleList(int userSysNo)
        {
            List<TG.Model.cm_Role> roleList = GetRoleList();

            var query = (from role in roleList
                         where
                         (
                                role.Users.Split(',').Contains(userSysNo.ToString())
                         )
                         select role).ToList<TG.Model.cm_Role>();
            return query;
        }

        /// <summary>
        /// 得到一个角色实体
        /// </summary>
        /// <param name="sysNo"></param>
        /// <returns></returns>
        public TG.Model.cm_Role GetRole(int sysNo)
        {
            string sql = "select SysNo,RoleName,Users from cm_Role  where SysNo=" + sysNo;

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();
            TG.Model.cm_Role role = null;
            if (reader.HasRows)
            {
                role = new TG.Model.cm_Role
                            {
                                SysNo = reader["SysNo"] == null ? 0 : Convert.ToInt32(reader["SysNo"]),
                                RoleName = reader["RoleName"] == null ? "" : reader["RoleName"].ToString(),
                                Users = reader["Users"] == null ? "" : reader["Users"].ToString()
                            };
            }

            reader.Close();

            return role;
        }

        public int InsertRole(TG.Model.cm_Role role)
        {
            string insertSql = string.Format("insert into cm_Role(RoleName,Users)values(N'{0}',N'{1}');select @@IDENTITY", role.RoleName, role.Users);

            object resultObj = DbHelperSQL.GetSingle(insertSql);

            return resultObj == null ? 0 : Convert.ToInt32(resultObj);
        }

        public int UpdateRole(TG.Model.cm_Role role)
        {
            string sql = "update cm_Role set RoleName=N'" + role.RoleName + "',Users=N'" + role.Users + "' where SysNo=" + role.SysNo;
            return DbHelperSQL.ExecuteSql(sql);
        }

        /// <summary>
        /// 检查用户权限
        /// </summary>
        /// <param name="roleSysNo"></param>
        /// <param name="userSysNo"></param>
        /// <returns></returns>
        public bool CheckPower(int roleSysNo, int userSysNo)
        {
            TG.Model.cm_Role role = GetRole(roleSysNo);

            string hasUser = "";
            if (role != null)
            {
                if (!string.IsNullOrEmpty(role.Users))
                {
                    List<string> userList = role.Users.Split(',').ToList<string>();

                    hasUser = userList.Find(delegate(string user) { return int.Parse(user) == userSysNo; });
                }
            }

            return string.IsNullOrEmpty(hasUser) == true ? false : true;
        }

        public List<RoleViewEntity> GetRoleViewEntityList(RoleQueryEntity queryEntity)
        {
            string whereSql = " where 1=1 ";
            if (queryEntity.RoleSysNo != 0)
            {
                whereSql += " and SysNo=" + queryEntity.RoleSysNo;
            }
            if (!string.IsNullOrEmpty(queryEntity.RoleName))
            {
                whereSql += " and RoleName like N'" + queryEntity.RoleName + "'";
            }

            string sql = string.Format("select SysNo,RoleName,Users from cm_Role {0}", whereSql);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<RoleViewEntity> roleViewEntityList = EntityBuilder<RoleViewEntity>.BuilderEntityList(reader);

            return roleViewEntityList;
        }

        public RoleViewEntity GetRoleViewEntity(RoleQueryEntity queryEntity)
        {
            string whereSql = " where 1=1 ";
            whereSql += " and SysNo=" + queryEntity.RoleSysNo;
            if (!string.IsNullOrEmpty(queryEntity.RoleName))
            {
                whereSql += " and RoleName like N'" + queryEntity.RoleName + "'";
            }
            string sql = string.Format("select SysNo,RoleName,Users from cm_Role {0}", whereSql);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            RoleViewEntity roleViewEntityList = EntityBuilder<RoleViewEntity>.BuilderEntity(reader);

            reader.Close();

            return roleViewEntityList;
        }
    }
}
