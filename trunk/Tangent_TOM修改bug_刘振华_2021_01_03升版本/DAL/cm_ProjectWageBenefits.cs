﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TG.DBUtility;

namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_ProjectWageBenefits
    /// </summary>
    public partial class cm_ProjectWageBenefits
    {
        public cm_ProjectWageBenefits()
        { }
        #region  Method


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectWageBenefits model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from  cm_ProjectWageBenefits where 1=1 and mem_id=@mem_id and yearWang=@yearWang;");
            strSql.Append("insert into cm_ProjectWageBenefits(");
            strSql.Append("mem_id,ValuePercent,ValueWage,PossessionMemWange,YearWange,RemunerationPercent,RemunerationWange,TaxPercent,YearTaxCount,ShouldWange,yearWang)");
            strSql.Append(" values (");
            strSql.Append("@mem_id,@ValuePercent,@ValueWage,@PossessionMemWange,@YearWange,@RemunerationPercent,@RemunerationWange,@TaxPercent,@YearTaxCount,@ShouldWange,@yearWang)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@mem_id", SqlDbType.Int,4),
					new SqlParameter("@ValuePercent", SqlDbType.NVarChar,50),
					new SqlParameter("@ValueWage", SqlDbType.Decimal,9),
					new SqlParameter("@PossessionMemWange", SqlDbType.Decimal,9),
					new SqlParameter("@YearWange", SqlDbType.Decimal,9),
					new SqlParameter("@RemunerationPercent", SqlDbType.NVarChar,50),
					new SqlParameter("@RemunerationWange", SqlDbType.Decimal,9),
					new SqlParameter("@TaxPercent", SqlDbType.NVarChar,50),
					new SqlParameter("@YearTaxCount", SqlDbType.Decimal,9),
					new SqlParameter("@ShouldWange", SqlDbType.Decimal,9),
					new SqlParameter("@yearWang", SqlDbType.NChar,10)};
            parameters[0].Value = model.mem_id;
            parameters[1].Value = model.ValuePercent;
            parameters[2].Value = model.ValueWage;
            parameters[3].Value = model.PossessionMemWange;
            parameters[4].Value = model.YearWange;
            parameters[5].Value = model.RemunerationPercent;
            parameters[6].Value = model.RemunerationWange;
            parameters[7].Value = model.TaxPercent;
            parameters[8].Value = model.YearTaxCount;
            parameters[9].Value = model.ShouldWange;
            parameters[10].Value = model.yearWang;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_ProjectWageBenefits model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ProjectWageBenefits set ");
            strSql.Append("mem_id=@mem_id,");
            strSql.Append("ValuePercent=@ValuePercent,");
            strSql.Append("ValueWage=@ValueWage,");
            strSql.Append("PossessionMemWange=@PossessionMemWange,");
            strSql.Append("YearWange=@YearWange,");
            strSql.Append("RemunerationPercent=@RemunerationPercent,");
            strSql.Append("RemunerationWange=@RemunerationWange,");
            strSql.Append("TaxPercent=@TaxPercent,");
            strSql.Append("YearTaxCount=@YearTaxCount,");
            strSql.Append("ShouldWange=@ShouldWange,");
            strSql.Append("yearWang=@yearWang");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@mem_id", SqlDbType.Int,4),
					new SqlParameter("@ValuePercent", SqlDbType.NVarChar,50),
					new SqlParameter("@ValueWage", SqlDbType.Decimal,9),
					new SqlParameter("@PossessionMemWange", SqlDbType.Decimal,9),
					new SqlParameter("@YearWange", SqlDbType.Decimal,9),
					new SqlParameter("@RemunerationPercent", SqlDbType.NVarChar,50),
					new SqlParameter("@RemunerationWange", SqlDbType.Decimal,9),
					new SqlParameter("@TaxPercent", SqlDbType.NVarChar,50),
					new SqlParameter("@YearTaxCount", SqlDbType.Decimal,9),
					new SqlParameter("@ShouldWange", SqlDbType.Decimal,9),
					new SqlParameter("@yearWang", SqlDbType.NChar,10),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.mem_id;
            parameters[1].Value = model.ValuePercent;
            parameters[2].Value = model.ValueWage;
            parameters[3].Value = model.PossessionMemWange;
            parameters[4].Value = model.YearWange;
            parameters[5].Value = model.RemunerationPercent;
            parameters[6].Value = model.RemunerationWange;
            parameters[7].Value = model.TaxPercent;
            parameters[8].Value = model.YearTaxCount;
            parameters[9].Value = model.ShouldWange;
            parameters[10].Value = model.yearWang;
            parameters[11].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectWageBenefits GetModel(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,mem_id,ValuePercent,ValueWage,PossessionMemWange,YearWange,RemunerationPercent,RemunerationWange,TaxPercent,YearTaxCount,ShouldWange,yearWang from cm_ProjectWageBenefits ");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
            parameters[0].Value = ID;

            TG.Model.cm_ProjectWageBenefits model = new TG.Model.cm_ProjectWageBenefits();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_id"] != null && ds.Tables[0].Rows[0]["mem_id"].ToString() != "")
                {
                    model.mem_id = int.Parse(ds.Tables[0].Rows[0]["mem_id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ValuePercent"] != null && ds.Tables[0].Rows[0]["ValuePercent"].ToString() != "")
                {
                    model.ValuePercent = ds.Tables[0].Rows[0]["ValuePercent"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ValueWage"] != null && ds.Tables[0].Rows[0]["ValueWage"].ToString() != "")
                {
                    model.ValueWage = decimal.Parse(ds.Tables[0].Rows[0]["ValueWage"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PossessionMemWange"] != null && ds.Tables[0].Rows[0]["PossessionMemWange"].ToString() != "")
                {
                    model.PossessionMemWange = decimal.Parse(ds.Tables[0].Rows[0]["PossessionMemWange"].ToString());
                }
                if (ds.Tables[0].Rows[0]["YearWange"] != null && ds.Tables[0].Rows[0]["YearWange"].ToString() != "")
                {
                    model.YearWange = decimal.Parse(ds.Tables[0].Rows[0]["YearWange"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RemunerationPercent"] != null && ds.Tables[0].Rows[0]["RemunerationPercent"].ToString() != "")
                {
                    model.RemunerationPercent = ds.Tables[0].Rows[0]["RemunerationPercent"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RemunerationWange"] != null && ds.Tables[0].Rows[0]["RemunerationWange"].ToString() != "")
                {
                    model.RemunerationWange = decimal.Parse(ds.Tables[0].Rows[0]["RemunerationWange"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TaxPercent"] != null && ds.Tables[0].Rows[0]["TaxPercent"].ToString() != "")
                {
                    model.TaxPercent = ds.Tables[0].Rows[0]["TaxPercent"].ToString();
                }
                if (ds.Tables[0].Rows[0]["YearTaxCount"] != null && ds.Tables[0].Rows[0]["YearTaxCount"].ToString() != "")
                {
                    model.YearTaxCount = decimal.Parse(ds.Tables[0].Rows[0]["YearTaxCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ShouldWange"] != null && ds.Tables[0].Rows[0]["ShouldWange"].ToString() != "")
                {
                    model.ShouldWange = decimal.Parse(ds.Tables[0].Rows[0]["ShouldWange"].ToString());
                }
                if (ds.Tables[0].Rows[0]["yearWang"] != null && ds.Tables[0].Rows[0]["yearWang"].ToString() != "")
                {
                    model.yearWang = ds.Tables[0].Rows[0]["yearWang"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_ProjectWageBenefits GetModelByYear(int ID, string year)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ID,mem_id,ValuePercent,ValueWage,PossessionMemWange,YearWange,RemunerationPercent,RemunerationWange,TaxPercent,YearTaxCount,ShouldWange,yearWang from cm_ProjectWageBenefits ");
            strSql.Append(" where mem_id=@ID and yearWang=@yearWang");
            SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4),
                    new SqlParameter("@yearWang", SqlDbType.NChar,10)
			};
            parameters[0].Value = ID;
            parameters[1].Value = year;

            TG.Model.cm_ProjectWageBenefits model = new TG.Model.cm_ProjectWageBenefits();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["mem_id"] != null && ds.Tables[0].Rows[0]["mem_id"].ToString() != "")
                {
                    model.mem_id = int.Parse(ds.Tables[0].Rows[0]["mem_id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ValuePercent"] != null && ds.Tables[0].Rows[0]["ValuePercent"].ToString() != "")
                {
                    model.ValuePercent = ds.Tables[0].Rows[0]["ValuePercent"].ToString();
                }
                if (ds.Tables[0].Rows[0]["ValueWage"] != null && ds.Tables[0].Rows[0]["ValueWage"].ToString() != "")
                {
                    model.ValueWage = decimal.Parse(ds.Tables[0].Rows[0]["ValueWage"].ToString());
                }
                if (ds.Tables[0].Rows[0]["PossessionMemWange"] != null && ds.Tables[0].Rows[0]["PossessionMemWange"].ToString() != "")
                {
                    model.PossessionMemWange = decimal.Parse(ds.Tables[0].Rows[0]["PossessionMemWange"].ToString());
                }
                if (ds.Tables[0].Rows[0]["YearWange"] != null && ds.Tables[0].Rows[0]["YearWange"].ToString() != "")
                {
                    model.YearWange = decimal.Parse(ds.Tables[0].Rows[0]["YearWange"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RemunerationPercent"] != null && ds.Tables[0].Rows[0]["RemunerationPercent"].ToString() != "")
                {
                    model.RemunerationPercent = ds.Tables[0].Rows[0]["RemunerationPercent"].ToString();
                }
                if (ds.Tables[0].Rows[0]["RemunerationWange"] != null && ds.Tables[0].Rows[0]["RemunerationWange"].ToString() != "")
                {
                    model.RemunerationWange = decimal.Parse(ds.Tables[0].Rows[0]["RemunerationWange"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TaxPercent"] != null && ds.Tables[0].Rows[0]["TaxPercent"].ToString() != "")
                {
                    model.TaxPercent = ds.Tables[0].Rows[0]["TaxPercent"].ToString();
                }
                if (ds.Tables[0].Rows[0]["YearTaxCount"] != null && ds.Tables[0].Rows[0]["YearTaxCount"].ToString() != "")
                {
                    model.YearTaxCount = decimal.Parse(ds.Tables[0].Rows[0]["YearTaxCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ShouldWange"] != null && ds.Tables[0].Rows[0]["ShouldWange"].ToString() != "")
                {
                    model.ShouldWange = decimal.Parse(ds.Tables[0].Rows[0]["ShouldWange"].ToString());
                }
                if (ds.Tables[0].Rows[0]["yearWang"] != null && ds.Tables[0].Rows[0]["yearWang"].ToString() != "")
                {
                    model.yearWang = ds.Tables[0].Rows[0]["yearWang"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,mem_id,ValuePercent,ValueWage,PossessionMemWange,YearWange,RemunerationPercent,RemunerationWange,TaxPercent,YearTaxCount,ShouldWange,yearWang ");
            strSql.Append(" FROM cm_ProjectWageBenefits ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetProjectWageBenefitsList(string query, string query1, string query2, string query3)
        {
            string sql = @"select me.mem_ID,
				           me.mem_Name,
				           isnull(( isnull(cast(round(cz.totalCount,0.00) as decimal(18,2)),0.00)+ isnull(cs.ComValue,0.00)),0.00) AS memTotalCount,
				           isnull(ValueWage,0.00) as ValueWage,--产值工资
				           isnull(PossessionMemWange,0.00) PossessionMemWange,--所管人员工资
				           isnull(YearWange,0.00) YearWange,--全年工资
				           isnull(RemunerationWange,0.00) RemunerationWange,--酬金工资
				           isnull(TaxPercent,0.00) TaxPercent,--税率
				           isnull(YearTaxCount,0.00) YearTaxCount,--全年实际应交税额
				           isnull(ShouldWange,0.00)ShouldWange,--应发工资
                            isnull(RemunerationPercent,0.00)RemunerationPercent 
				           from tg_member me 
				           left join (
					        select isnull(SUM(totalCount),0.00) as totalCount,mem_ID from 
					     ( 
						--查询产值--不包括设总
						 select a.mem_ID,
                         isnull(SUM(isnull(a.DesignManagerCount,0.00)+ isnull(isnull(a.DesignCount,0),0)+isnull(isnull(SpecialtyHeadCount,0),0)+isnull(isnull(AuditCount,0),0)+isnull(isnull(ProofreadCount,0),0)),0)  as totalCount
						 from cm_ProjectValueByMemberDetails a 
						 left join cm_ProjectValueAllot b on a.AllotID=b.ID 
						 where a.[Status]='S' and  (a.IsExternal='0'  or DesignType='designManager') " + query2 + " group by a.mem_ID " + @"
										        			         
						 
						 union all
												
						 --转土建所金额
						 select DesignUserID,SUM(DesignManagerCount) from cm_TranBulidingProjectValueAllot b 
						 where b.[Status]='S' " + query2 + " " + @"
						 group by b.Pro_ID,b.DesignUserID
					 ) ppj group by mem_ID
											
				     ) cz on me.mem_ID=cz.mem_ID 
				     left join 
				     (select UnitId,MemberId,isnull(sum(ComValue),0.00) as ComValue,
				      ValueYear from cm_CompensationSet 
				      group by MemberId,UnitId,ValueYear
				      ) cs 
				     on cs.MemberId=me.mem_ID
                     " + query1 + " " + @"
				     left join cm_ProjectWageBenefits wb
				     on wb.mem_id=me.mem_ID 
				      " + query3 + " " + @"
			     
			     where 1=1 " + query + " Order by me.mem_id asc";
            return DbHelperSQL.Query(sql);
        }
        #endregion  Method
    }
}