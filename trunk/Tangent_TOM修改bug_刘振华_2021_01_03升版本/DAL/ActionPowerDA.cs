﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.DBUtility;
using TG.Common.EntityBuilder;
using TG.Model;
using System.Data.SqlClient;

namespace TG.DAL
{
    public class ActionPowerDA
    {
        public List<ActionPowerViewEntity> GetActionPowerList(LeftMenuType leftMenuType)
        {
            string leftTypeString = " where 1=1 ";
            switch (leftMenuType)
            {
                case LeftMenuType.Coperation:
                    leftTypeString += " and Type=N'Coperation'";
                    break;
                case LeftMenuType.Customer:
                    leftTypeString += " and Type=N'Customer'";
                    break;
                case LeftMenuType.Default:
                    leftTypeString += "  and Type=N'Default'";
                    break;
                case LeftMenuType.LeaderShip:
                    leftTypeString += " and Type=N'LeaderShip'";
                    break;
                case LeftMenuType.Project:
                    leftTypeString += " and Type=N'Project'";
                    break;
                case LeftMenuType.ProjectShow:
                    leftTypeString += " and Type=N'ProjectShow'";
                    break;
                case LeftMenuType.SystemConfig:
                    leftTypeString += " and Type=N'SystemConfig'";
                    break;
                case LeftMenuType.JiXiao:
                    leftTypeString += " AND Type=N'JiXiao'";
                    break;
                case LeftMenuType.KaoQin:
                    leftTypeString += " AND Type=N'KaoQin'";
                    break;
                case LeftMenuType.All:
                    leftTypeString = "";
                    break;
            }
            string sql = string.Format(@" SELECT [SysNo]
                                                              ,[Description]
                                                              ,[PageName]
                                                              ,[Role]
                                                              ,[Type]
                                                              ,[Power]
                                                              ,[PreviewPower]
                                                              ,[InUser]
                                                              ,[InDate],
                                                              tm.mem_Name as InUserName
                                                              FROM [dbo].[cm_ActionPower] cl join tg_member tm on tm.mem_ID = cl.InUser {0}", leftTypeString);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<ActionPowerViewEntity> resultList = EntityBuilder<ActionPowerViewEntity>.BuilderEntityList(reader);

            return resultList;
        }

        public int InsertActionPower(ActionPowerViewEntity actionPowerViewEntity)
        {
            string insertSql = string.Format("insert into cm_ActionPower(Description,PageName,Role,Type,Power,InUser,InDate,PreviewPower)values(N'{0}',N'{1}',N'{2}',N'{3}',N'{4}',{5},N'{6}',{7});select @@IDENTITY"
                                        , actionPowerViewEntity.Description
                                        , actionPowerViewEntity.PageName
                                        , actionPowerViewEntity.Role
                                        , actionPowerViewEntity.Type
                                        , actionPowerViewEntity.Power
                                        , actionPowerViewEntity.InUser
                                        , DateTime.Now
                                        , actionPowerViewEntity.PreviewPower);

            object objResult = DbHelperSQL.GetSingle(insertSql);

            return objResult == null ? 0 : Convert.ToInt32(objResult);
        }

        public ActionPowerViewEntity GetActionPowerViewEntity(int actionPowerSysNo)
        {
            string sql = string.Format(@" SELECT [SysNo]
                                                              ,[Description]
                                                              ,[PageName]
                                                              ,[Role]
                                                              ,[Type]
                                                              ,[Power]
                                                              ,[PreviewPower]
                                                              ,[InUser]
                                                              ,[InDate],
                                                              tm.mem_Name as InUserName
                                                              FROM [dbo].[cm_ActionPower] cl join tg_member tm on tm.mem_ID = cl.InUser where cl.SysNo = {0}", actionPowerSysNo);


            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            ActionPowerViewEntity actionPowerViewEntity = EntityBuilder<ActionPowerViewEntity>.BuilderEntity(reader);

            reader.Close();

            return actionPowerViewEntity;
        }

        public ActionPowerViewEntity GetActionPowerViewEntity(ActionPowerQueryEntity queryEntity)
        {
            string whereSql = " where 1=1";

            if (!string.IsNullOrEmpty(queryEntity.PageName))
            {
                whereSql += " and PageName = N'" + queryEntity.PageName + "'";
            }
            if (queryEntity.Type != LeftMenuType.All)
            {
                switch (queryEntity.Type)
                {
                    case LeftMenuType.Coperation:
                        whereSql += " and Type=N'Coperation'";
                        break;
                    case LeftMenuType.Customer:
                        whereSql += " and Type=N'Customer'";
                        break;
                    case LeftMenuType.Default:
                        whereSql += "  and Type=N'Default'";
                        break;
                    case LeftMenuType.LeaderShip:
                        whereSql += " and Type=N'LeaderShip'";
                        break;
                    case LeftMenuType.Project:
                        whereSql += " and Type=N'Project'";
                        break;
                    case LeftMenuType.ProjectShow:
                        whereSql += " and Type=N'ProjectShow'";
                        break;
                    case LeftMenuType.SystemConfig:
                        whereSql += " and Type=N'SystemConfig'";
                        break;
                }
            }
            string sql = string.Format(@" SELECT [SysNo]
                                                              ,[Description]
                                                              ,[PageName]
                                                              ,[Role]
                                                              ,[Type]
                                                              ,[Power]
                                                              ,[PreviewPower]
                                                              ,[InUser]
                                                              ,[InDate],
                                                              tm.mem_Name as InUserName
                                                              FROM [dbo].[cm_ActionPower] cl join tg_member tm on tm.mem_ID = cl.InUser {0}", whereSql);

            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            reader.Read();

            ActionPowerViewEntity actionPowerViewEntity = EntityBuilder<ActionPowerViewEntity>.BuilderEntity(reader);

            reader.Close();

            return actionPowerViewEntity;
        }

        public int UpDateActionPower(ActionPowerViewEntity actionPower)
        {
            string sql = string.Format("update cm_ActionPower set Description=N'{0}',PageName=N'{1}',Role=N'{2}',Type=N'{3}',Power =N'{4}',PreviewPower={6} where SysNo={5}", actionPower.Description, actionPower.PageName, actionPower.Role, actionPower.Type, actionPower.Power, actionPower.SysNo, actionPower.PreviewPower);

            int count = DbHelperSQL.ExecuteSql(sql);

            return count;
        }

        public int DeleteActionPower(int sysNo)
        {
            string sql = "delete cm_ActionPower where SysNo= " + sysNo;
            return DbHelperSQL.ExecuteSql(sql);
        }
    }
}
