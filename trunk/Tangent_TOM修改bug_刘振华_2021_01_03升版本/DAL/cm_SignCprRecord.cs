﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_SignCprRecord
	/// </summary>
	public partial class cm_SignCprRecord
	{
		public cm_SignCprRecord()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_SignCprRecord model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_SignCprRecord(");
			strSql.Append("mem_ID,mem_CoperationDate,mem_CoperationType,mem_CoperationDateEnd,mem_CoperationSub,insertUserID)");
			strSql.Append(" values (");
			strSql.Append("@mem_ID,@mem_CoperationDate,@mem_CoperationType,@mem_CoperationDateEnd,@mem_CoperationSub,@insertUserID)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_ID", SqlDbType.Int,4),
					new SqlParameter("@mem_CoperationDate", SqlDbType.DateTime),
					new SqlParameter("@mem_CoperationType", SqlDbType.Int,4),
					new SqlParameter("@mem_CoperationDateEnd", SqlDbType.DateTime),
					new SqlParameter("@mem_CoperationSub", SqlDbType.NVarChar,200),
					new SqlParameter("@insertUserID", SqlDbType.Int,4)};
			parameters[0].Value = model.mem_ID;
			parameters[1].Value = model.mem_CoperationDate;
			parameters[2].Value = model.mem_CoperationType;
			parameters[3].Value = model.mem_CoperationDateEnd;
			parameters[4].Value = model.mem_CoperationSub;
			parameters[5].Value = model.insertUserID;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_SignCprRecord model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_SignCprRecord set ");
			strSql.Append("mem_ID=@mem_ID,");
			strSql.Append("mem_CoperationDate=@mem_CoperationDate,");
			strSql.Append("mem_CoperationType=@mem_CoperationType,");
			strSql.Append("mem_CoperationDateEnd=@mem_CoperationDateEnd,");
			strSql.Append("mem_CoperationSub=@mem_CoperationSub,");
			strSql.Append("insertUserID=@insertUserID");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@mem_ID", SqlDbType.Int,4),
					new SqlParameter("@mem_CoperationDate", SqlDbType.DateTime),
					new SqlParameter("@mem_CoperationType", SqlDbType.Int,4),
					new SqlParameter("@mem_CoperationDateEnd", SqlDbType.DateTime),
					new SqlParameter("@mem_CoperationSub", SqlDbType.NVarChar,200),
					new SqlParameter("@insertUserID", SqlDbType.Int,4),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.mem_ID;
			parameters[1].Value = model.mem_CoperationDate;
			parameters[2].Value = model.mem_CoperationType;
			parameters[3].Value = model.mem_CoperationDateEnd;
			parameters[4].Value = model.mem_CoperationSub;
			parameters[5].Value = model.insertUserID;
			parameters[6].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_SignCprRecord ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_SignCprRecord ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_SignCprRecord GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,mem_ID,mem_CoperationDate,mem_CoperationType,mem_CoperationDateEnd,mem_CoperationSub,insertUserID from cm_SignCprRecord ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_SignCprRecord model=new TG.Model.cm_SignCprRecord();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_SignCprRecord DataRowToModel(DataRow row)
		{
			TG.Model.cm_SignCprRecord model=new TG.Model.cm_SignCprRecord();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["mem_ID"]!=null && row["mem_ID"].ToString()!="")
				{
					model.mem_ID=int.Parse(row["mem_ID"].ToString());
				}
				if(row["mem_CoperationDate"]!=null && row["mem_CoperationDate"].ToString()!="")
				{
					model.mem_CoperationDate=DateTime.Parse(row["mem_CoperationDate"].ToString());
				}
				if(row["mem_CoperationType"]!=null && row["mem_CoperationType"].ToString()!="")
				{
					model.mem_CoperationType=int.Parse(row["mem_CoperationType"].ToString());
				}
				if(row["mem_CoperationDateEnd"]!=null && row["mem_CoperationDateEnd"].ToString()!="")
				{
					model.mem_CoperationDateEnd=DateTime.Parse(row["mem_CoperationDateEnd"].ToString());
				}
				if(row["mem_CoperationSub"]!=null)
				{
					model.mem_CoperationSub=row["mem_CoperationSub"].ToString();
				}
				if(row["insertUserID"]!=null && row["insertUserID"].ToString()!="")
				{
					model.insertUserID=int.Parse(row["insertUserID"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,mem_ID,mem_CoperationDate,mem_CoperationType,mem_CoperationDateEnd,mem_CoperationSub,insertUserID ");
			strSql.Append(" FROM cm_SignCprRecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,mem_ID,mem_CoperationDate,mem_CoperationType,mem_CoperationDateEnd,mem_CoperationSub,insertUserID ");
			strSql.Append(" FROM cm_SignCprRecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_SignCprRecord ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_SignCprRecord T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_SignCprRecord";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

