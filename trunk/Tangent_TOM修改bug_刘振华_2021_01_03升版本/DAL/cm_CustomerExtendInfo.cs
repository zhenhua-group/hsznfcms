﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_CustomerExtendInfo
	/// </summary>
	public partial class cm_CustomerExtendInfo
	{
		public cm_CustomerExtendInfo()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("Cst_ExtendInfoId", "cm_CustomerExtendInfo"); 
		}


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int Cst_ExtendInfoId)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from cm_CustomerExtendInfo");
			strSql.Append(" where Cst_ExtendInfoId="+Cst_ExtendInfoId+" ");
			return DbHelperSQL.Exists(strSql.ToString());
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_CustomerExtendInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			StringBuilder strSql1=new StringBuilder();
			StringBuilder strSql2=new StringBuilder();
			if (model.Cst_Id != null)
			{
				strSql1.Append("Cst_Id,");
				strSql2.Append(""+model.Cst_Id+",");
			}
			if (model.Cst_EnglishName != null)
			{
				strSql1.Append("Cst_EnglishName,");
				strSql2.Append("'"+model.Cst_EnglishName+"',");
			}
			if (model.Country != null)
			{
				strSql1.Append("Country,");
				strSql2.Append("'"+model.Country+"',");
			}
			if (model.Province != null)
			{
				strSql1.Append("Province,");
				strSql2.Append("'"+model.Province+"',");
			}
			if (model.City != null)
			{
				strSql1.Append("City,");
				strSql2.Append("'"+model.City+"',");
			}
			if (model.Profession != null)
			{
				strSql1.Append("Profession,");
				strSql2.Append(""+model.Profession+",");
			}
			if (model.Type != null)
			{
				strSql1.Append("Type,");
				strSql2.Append(""+model.Type+",");
			}
			if (model.Email != null)
			{
				strSql1.Append("Email,");
				strSql2.Append("'"+model.Email+"',");
			}
			if (model.RelationDepartment != null)
			{
				strSql1.Append("RelationDepartment,");
				strSql2.Append("'"+model.RelationDepartment+"',");
			}
			if (model.BankName != null)
			{
				strSql1.Append("BankName,");
				strSql2.Append("'"+model.BankName+"',");
			}
			if (model.TaxAccountNo != null)
			{
				strSql1.Append("TaxAccountNo,");
				strSql2.Append("'"+model.TaxAccountNo+"',");
			}
			if (model.BankAccountNo != null)
			{
				strSql1.Append("BankAccountNo,");
				strSql2.Append(""+model.BankAccountNo+",");
			}
			if (model.Remark != null)
			{
				strSql1.Append("Remark,");
				strSql2.Append("'"+model.Remark+"',");
			}
			if (model.IsPartner != null)
			{
				strSql1.Append("IsPartner,");
				strSql2.Append(""+model.IsPartner+",");
			}
			if (model.BranchPart != null)
			{
				strSql1.Append("BranchPart,");
				strSql2.Append("'"+model.BranchPart+"',");
			}
			if (model.Cpy_PrincipalSheet != null)
			{
				strSql1.Append("Cpy_PrincipalSheet,");
				strSql2.Append("'"+model.Cpy_PrincipalSheet+"',");
			}
			if (model.CreateRelationTime != null)
			{
				strSql1.Append("CreateRelationTime,");
				strSql2.Append("'"+model.CreateRelationTime+"',");
			}
			if (model.CreditLeve != null)
			{
				strSql1.Append("CreditLeve,");
				strSql2.Append(""+model.CreditLeve+",");
			}
			if (model.CloseLeve != null)
			{
				strSql1.Append("CloseLeve,");
				strSql2.Append(""+model.CloseLeve+",");
			}
			if (model.Cpy_Code != null)
			{
				strSql1.Append("Cpy_Code,");
				strSql2.Append("'"+model.Cpy_Code+"',");
			}
			if (model.LawPerson != null)
			{
				strSql1.Append("LawPerson,");
				strSql2.Append("'"+model.LawPerson+"',");
			}
			if (model.UpdateBy != null)
			{
				strSql1.Append("UpdateBy,");
				strSql2.Append(""+model.UpdateBy+",");
			}
			if (model.LastUpdate != null)
			{
				strSql1.Append("LastUpdate,");
				strSql2.Append("'"+model.LastUpdate+"',");
			}
			strSql.Append("insert into cm_CustomerExtendInfo(");
			strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
			strSql.Append(")");
			strSql.Append(" values (");
			strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
			strSql.Append(")");
			strSql.Append(";select @@IDENTITY");
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_CustomerExtendInfo model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_CustomerExtendInfo set ");
			if (model.Cst_Id != null)
			{
				strSql.Append("Cst_Id="+model.Cst_Id+",");
			}
			if (model.Cst_EnglishName != null)
			{
				strSql.Append("Cst_EnglishName='"+model.Cst_EnglishName+"',");
			}
			else
			{
				strSql.Append("Cst_EnglishName= null ,");
			}
			if (model.Country != null)
			{
				strSql.Append("Country='"+model.Country+"',");
			}
			else
			{
				strSql.Append("Country= null ,");
			}
			if (model.Province != null)
			{
				strSql.Append("Province='"+model.Province+"',");
			}
			else
			{
				strSql.Append("Province= null ,");
			}
			if (model.City != null)
			{
				strSql.Append("City='"+model.City+"',");
			}
			else
			{
				strSql.Append("City= null ,");
			}
			if (model.Profession != null)
			{
				strSql.Append("Profession="+model.Profession+",");
			}
			else
			{
				strSql.Append("Profession= null ,");
			}
			if (model.Type != null)
			{
				strSql.Append("Type="+model.Type+",");
			}
			else
			{
				strSql.Append("Type= null ,");
			}
			if (model.Email != null)
			{
				strSql.Append("Email='"+model.Email+"',");
			}
			else
			{
				strSql.Append("Email= null ,");
			}
			if (model.RelationDepartment != null)
			{
				strSql.Append("RelationDepartment='"+model.RelationDepartment+"',");
			}
			else
			{
				strSql.Append("RelationDepartment= null ,");
			}
			if (model.BankName != null)
			{
				strSql.Append("BankName='"+model.BankName+"',");
			}
			else
			{
				strSql.Append("BankName= null ,");
			}
			if (model.TaxAccountNo != null)
			{
				strSql.Append("TaxAccountNo='"+model.TaxAccountNo+"',");
			}
			else
			{
				strSql.Append("TaxAccountNo= null ,");
			}
			if (model.BankAccountNo != null)
			{
				strSql.Append("BankAccountNo="+model.BankAccountNo+",");
			}
			else
			{
				strSql.Append("BankAccountNo= null ,");
			}
			if (model.Remark != null)
			{
				strSql.Append("Remark='"+model.Remark+"',");
			}
			else
			{
				strSql.Append("Remark= null ,");
			}
			if (model.IsPartner != null)
			{
				strSql.Append("IsPartner="+model.IsPartner+",");
			}
			else
			{
				strSql.Append("IsPartner= null ,");
			}
			if (model.BranchPart != null)
			{
				strSql.Append("BranchPart='"+model.BranchPart+"',");
			}
			else
			{
				strSql.Append("BranchPart= null ,");
			}
			if (model.Cpy_PrincipalSheet != null)
			{
				strSql.Append("Cpy_PrincipalSheet='"+model.Cpy_PrincipalSheet+"',");
			}
			else
			{
				strSql.Append("Cpy_PrincipalSheet= null ,");
			}
			if (model.CreateRelationTime != null)
			{
				strSql.Append("CreateRelationTime='"+model.CreateRelationTime+"',");
			}
			else
			{
				strSql.Append("CreateRelationTime= null ,");
			}
			if (model.CreditLeve != null)
			{
				strSql.Append("CreditLeve="+model.CreditLeve+",");
			}
			else
			{
				strSql.Append("CreditLeve= null ,");
			}
			if (model.CloseLeve != null)
			{
				strSql.Append("CloseLeve="+model.CloseLeve+",");
			}
			else
			{
				strSql.Append("CloseLeve= null ,");
			}
			if (model.Cpy_Code != null)
			{
				strSql.Append("Cpy_Code='"+model.Cpy_Code+"',");
			}
			else
			{
				strSql.Append("Cpy_Code= null ,");
			}
			if (model.LawPerson != null)
			{
				strSql.Append("LawPerson='"+model.LawPerson+"',");
			}
			else
			{
				strSql.Append("LawPerson= null ,");
			}
			if (model.UpdateBy != null)
			{
				strSql.Append("UpdateBy="+model.UpdateBy+",");
			}
			else
			{
				strSql.Append("UpdateBy= null ,");
			}
			if (model.LastUpdate != null)
			{
				strSql.Append("LastUpdate='"+model.LastUpdate+"',");
			}
			else
			{
				strSql.Append("LastUpdate= null ,");
			}
			int n = strSql.ToString().LastIndexOf(",");
			strSql.Remove(n, 1);
			strSql.Append(" where Cst_ExtendInfoId="+ model.Cst_ExtendInfoId+"");
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int Cst_ExtendInfoId)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_CustomerExtendInfo ");
			strSql.Append(" where Cst_ExtendInfoId="+Cst_ExtendInfoId+"" );
			int rowsAffected=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string Cst_ExtendInfoIdlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_CustomerExtendInfo ");
			strSql.Append(" where Cst_ExtendInfoId in ("+Cst_ExtendInfoIdlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_CustomerExtendInfo GetModel(int Cst_ExtendInfoId)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1  ");
			strSql.Append(" Cst_ExtendInfoId,Cst_Id,Cst_EnglishName,Country,Province,City,Profession,Type,Email,RelationDepartment,BankName,TaxAccountNo,BankAccountNo,Remark,IsPartner,BranchPart,Cpy_PrincipalSheet,CreateRelationTime,CreditLeve,CloseLeve,Cpy_Code,LawPerson,UpdateBy,LastUpdate ");
			strSql.Append(" from cm_CustomerExtendInfo ");
			strSql.Append(" where Cst_ExtendInfoId="+Cst_ExtendInfoId+"" );
			TG.Model.cm_CustomerExtendInfo model=new TG.Model.cm_CustomerExtendInfo();
			DataSet ds=DbHelperSQL.Query(strSql.ToString());
			if(ds.Tables[0].Rows.Count>0)
			{
				if(ds.Tables[0].Rows[0]["Cst_ExtendInfoId"]!=null && ds.Tables[0].Rows[0]["Cst_ExtendInfoId"].ToString()!="")
				{
					model.Cst_ExtendInfoId=int.Parse(ds.Tables[0].Rows[0]["Cst_ExtendInfoId"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Cst_Id"]!=null && ds.Tables[0].Rows[0]["Cst_Id"].ToString()!="")
				{
					model.Cst_Id=int.Parse(ds.Tables[0].Rows[0]["Cst_Id"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Cst_EnglishName"]!=null && ds.Tables[0].Rows[0]["Cst_EnglishName"].ToString()!="")
				{
					model.Cst_EnglishName=ds.Tables[0].Rows[0]["Cst_EnglishName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Country"]!=null && ds.Tables[0].Rows[0]["Country"].ToString()!="")
				{
					model.Country=ds.Tables[0].Rows[0]["Country"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Province"]!=null && ds.Tables[0].Rows[0]["Province"].ToString()!="")
				{
					model.Province=ds.Tables[0].Rows[0]["Province"].ToString();
				}
				if(ds.Tables[0].Rows[0]["City"]!=null && ds.Tables[0].Rows[0]["City"].ToString()!="")
				{
					model.City=ds.Tables[0].Rows[0]["City"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Profession"]!=null && ds.Tables[0].Rows[0]["Profession"].ToString()!="")
				{
					model.Profession=int.Parse(ds.Tables[0].Rows[0]["Profession"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Type"]!=null && ds.Tables[0].Rows[0]["Type"].ToString()!="")
				{
					model.Type=int.Parse(ds.Tables[0].Rows[0]["Type"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Email"]!=null && ds.Tables[0].Rows[0]["Email"].ToString()!="")
				{
					model.Email=ds.Tables[0].Rows[0]["Email"].ToString();
				}
				if(ds.Tables[0].Rows[0]["RelationDepartment"]!=null && ds.Tables[0].Rows[0]["RelationDepartment"].ToString()!="")
				{
					model.RelationDepartment=ds.Tables[0].Rows[0]["RelationDepartment"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BankName"]!=null && ds.Tables[0].Rows[0]["BankName"].ToString()!="")
				{
					model.BankName=ds.Tables[0].Rows[0]["BankName"].ToString();
				}
				if(ds.Tables[0].Rows[0]["TaxAccountNo"]!=null && ds.Tables[0].Rows[0]["TaxAccountNo"].ToString()!="")
				{
					model.TaxAccountNo=ds.Tables[0].Rows[0]["TaxAccountNo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["BankAccountNo"]!=null && ds.Tables[0].Rows[0]["BankAccountNo"].ToString()!="")
				{
					model.BankAccountNo=ds.Tables[0].Rows[0]["BankAccountNo"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Remark"]!=null && ds.Tables[0].Rows[0]["Remark"].ToString()!="")
				{
					model.Remark=ds.Tables[0].Rows[0]["Remark"].ToString();
				}
				if(ds.Tables[0].Rows[0]["IsPartner"]!=null && ds.Tables[0].Rows[0]["IsPartner"].ToString()!="")
				{
					model.IsPartner=int.Parse(ds.Tables[0].Rows[0]["IsPartner"].ToString());
				}
				if(ds.Tables[0].Rows[0]["BranchPart"]!=null && ds.Tables[0].Rows[0]["BranchPart"].ToString()!="")
				{
					model.BranchPart=ds.Tables[0].Rows[0]["BranchPart"].ToString();
				}
				if(ds.Tables[0].Rows[0]["Cpy_PrincipalSheet"]!=null && ds.Tables[0].Rows[0]["Cpy_PrincipalSheet"].ToString()!="")
				{
					model.Cpy_PrincipalSheet=ds.Tables[0].Rows[0]["Cpy_PrincipalSheet"].ToString();
				}
				if(ds.Tables[0].Rows[0]["CreateRelationTime"]!=null && ds.Tables[0].Rows[0]["CreateRelationTime"].ToString()!="")
				{
					model.CreateRelationTime=DateTime.Parse(ds.Tables[0].Rows[0]["CreateRelationTime"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CreditLeve"]!=null && ds.Tables[0].Rows[0]["CreditLeve"].ToString()!="")
				{
					model.CreditLeve=int.Parse(ds.Tables[0].Rows[0]["CreditLeve"].ToString());
				}
				if(ds.Tables[0].Rows[0]["CloseLeve"]!=null && ds.Tables[0].Rows[0]["CloseLeve"].ToString()!="")
				{
					model.CloseLeve=int.Parse(ds.Tables[0].Rows[0]["CloseLeve"].ToString());
				}
				if(ds.Tables[0].Rows[0]["Cpy_Code"]!=null && ds.Tables[0].Rows[0]["Cpy_Code"].ToString()!="")
				{
					model.Cpy_Code=ds.Tables[0].Rows[0]["Cpy_Code"].ToString();
				}
				if(ds.Tables[0].Rows[0]["LawPerson"]!=null && ds.Tables[0].Rows[0]["LawPerson"].ToString()!="")
				{
					model.LawPerson=ds.Tables[0].Rows[0]["LawPerson"].ToString();
				}
				if(ds.Tables[0].Rows[0]["UpdateBy"]!=null && ds.Tables[0].Rows[0]["UpdateBy"].ToString()!="")
				{
					model.UpdateBy=int.Parse(ds.Tables[0].Rows[0]["UpdateBy"].ToString());
				}
				if(ds.Tables[0].Rows[0]["LastUpdate"]!=null && ds.Tables[0].Rows[0]["LastUpdate"].ToString()!="")
				{
					model.LastUpdate=DateTime.Parse(ds.Tables[0].Rows[0]["LastUpdate"].ToString());
				}
				return model;
			}
			else
			{
				return null;
			}
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select Cst_ExtendInfoId,Cst_Id,Cst_EnglishName,Country,Province,City,Profession,Type,Email,RelationDepartment,BankName,TaxAccountNo,BankAccountNo,Remark,IsPartner,BranchPart,Cpy_PrincipalSheet,CreateRelationTime,CreditLeve,CloseLeve,Cpy_Code,LawPerson,UpdateBy,LastUpdate ");
			strSql.Append(" FROM cm_CustomerExtendInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" Cst_ExtendInfoId,Cst_Id,Cst_EnglishName,Country,Province,City,Profession,Type,Email,RelationDepartment,BankName,TaxAccountNo,BankAccountNo,Remark,IsPartner,BranchPart,Cpy_PrincipalSheet,CreateRelationTime,CreditLeve,CloseLeve,Cpy_Code,LawPerson,UpdateBy,LastUpdate ");
			strSql.Append(" FROM cm_CustomerExtendInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_CustomerExtendInfo ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
            /*没有调用此方法，无需修改。Created by Sago*/
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBE() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.Cst_ExtendInfoId desc");
			}
			strSql.Append(")AS Row, T.*  from cm_CustomerExtendInfo T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		*/

		#endregion  Method
	}
}

