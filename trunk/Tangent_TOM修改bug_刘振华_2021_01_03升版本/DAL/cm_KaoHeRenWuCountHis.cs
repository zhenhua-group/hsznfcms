﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeRenWuCountHis
	/// </summary>
	public partial class cm_KaoHeRenWuCountHis
	{
		public cm_KaoHeRenWuCountHis()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeRenWuCountHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeRenWuCountHis(");
			strSql.Append("RenwuID,RenwuName,ShijiCount,InitCount,JishuCount,JiangjinCount)");
			strSql.Append(" values (");
			strSql.Append("@RenwuID,@RenwuName,@ShijiCount,@InitCount,@JishuCount,@JiangjinCount)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@RenwuName", SqlDbType.VarChar,50),
					new SqlParameter("@ShijiCount", SqlDbType.Decimal,9),
					new SqlParameter("@InitCount", SqlDbType.Decimal,9),
					new SqlParameter("@JishuCount", SqlDbType.Decimal,9),
					new SqlParameter("@JiangjinCount", SqlDbType.Decimal,9)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.RenwuName;
			parameters[2].Value = model.ShijiCount;
			parameters[3].Value = model.InitCount;
			parameters[4].Value = model.JishuCount;
			parameters[5].Value = model.JiangjinCount;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeRenWuCountHis model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeRenWuCountHis set ");
			strSql.Append("RenwuID=@RenwuID,");
			strSql.Append("RenwuName=@RenwuName,");
			strSql.Append("ShijiCount=@ShijiCount,");
			strSql.Append("InitCount=@InitCount,");
			strSql.Append("JishuCount=@JishuCount,");
			strSql.Append("JiangjinCount=@JiangjinCount");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@RenwuName", SqlDbType.VarChar,50),
					new SqlParameter("@ShijiCount", SqlDbType.Decimal,9),
					new SqlParameter("@InitCount", SqlDbType.Decimal,9),
					new SqlParameter("@JishuCount", SqlDbType.Decimal,9),
					new SqlParameter("@JiangjinCount", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.RenwuName;
			parameters[2].Value = model.ShijiCount;
			parameters[3].Value = model.InitCount;
			parameters[4].Value = model.JishuCount;
			parameters[5].Value = model.JiangjinCount;
			parameters[6].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeRenWuCountHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeRenWuCountHis ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeRenWuCountHis GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,RenwuID,RenwuName,ShijiCount,InitCount,JishuCount,JiangjinCount from cm_KaoHeRenWuCountHis ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeRenWuCountHis model=new TG.Model.cm_KaoHeRenWuCountHis();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeRenWuCountHis DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeRenWuCountHis model=new TG.Model.cm_KaoHeRenWuCountHis();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["RenwuID"]!=null && row["RenwuID"].ToString()!="")
				{
					model.RenwuID=int.Parse(row["RenwuID"].ToString());
				}
				if(row["RenwuName"]!=null)
				{
					model.RenwuName=row["RenwuName"].ToString();
				}
				if(row["ShijiCount"]!=null && row["ShijiCount"].ToString()!="")
				{
					model.ShijiCount=decimal.Parse(row["ShijiCount"].ToString());
				}
				if(row["InitCount"]!=null && row["InitCount"].ToString()!="")
				{
					model.InitCount=decimal.Parse(row["InitCount"].ToString());
				}
				if(row["JishuCount"]!=null && row["JishuCount"].ToString()!="")
				{
					model.JishuCount=decimal.Parse(row["JishuCount"].ToString());
				}
				if(row["JiangjinCount"]!=null && row["JiangjinCount"].ToString()!="")
				{
					model.JiangjinCount=decimal.Parse(row["JiangjinCount"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ID,RenwuID,RenwuName,ShijiCount,InitCount,JishuCount,JiangjinCount ");
			strSql.Append(" FROM cm_KaoHeRenWuCountHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,RenwuID,RenwuName,ShijiCount,InitCount,JishuCount,JiangjinCount ");
			strSql.Append(" FROM cm_KaoHeRenWuCountHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeRenWuCountHis ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeRenWuCountHis T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeRenWuCountHis";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

