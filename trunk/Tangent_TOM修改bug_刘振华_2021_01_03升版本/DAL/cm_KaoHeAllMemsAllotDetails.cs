﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:cm_KaoHeAllMemsAllotDetails
	/// </summary>
	public partial class cm_KaoHeAllMemsAllotDetails
	{
		public cm_KaoHeAllMemsAllotDetails()
		{}
		#region  BasicMethod



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.cm_KaoHeAllMemsAllotDetails model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into cm_KaoHeAllMemsAllotDetails(");
			strSql.Append("RenwuID,UnitID,UnitName,MemID,MemName,IsFired,bmjl,gstz,yfjj,xmjjjs,bmnjjjs,xmjj,bmnjj,xmglj,yxyg,tcgx,bimjj,ghbt,hj,insertUserID,insertDate,Stat,zcfy)");
			strSql.Append(" values (");
            strSql.Append("@RenwuID,@UnitID,@UnitName,@MemID,@MemName,@IsFired,@bmjl,@gstz,@yfjj,@xmjjjs,@bmnjjjs,@xmjj,@bmnjj,@xmglj,@yxyg,@tcgx,@bimjj,@ghbt,@hj,@insertUserID,@insertDate,@Stat,@zcfy)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.NVarChar,50),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@IsFired", SqlDbType.Int,4),
					new SqlParameter("@bmjl", SqlDbType.Decimal,9),
					new SqlParameter("@gstz", SqlDbType.Decimal,9),
					new SqlParameter("@yfjj", SqlDbType.Decimal,9),
					new SqlParameter("@xmjjjs", SqlDbType.Decimal,9),
					new SqlParameter("@bmnjjjs", SqlDbType.Decimal,9),
					new SqlParameter("@xmjj", SqlDbType.Decimal,9),
					new SqlParameter("@bmnjj", SqlDbType.Decimal,9),
					new SqlParameter("@xmglj", SqlDbType.Decimal,9),
					new SqlParameter("@yxyg", SqlDbType.Decimal,9),
					new SqlParameter("@tcgx", SqlDbType.Decimal,9),
					new SqlParameter("@bimjj", SqlDbType.Decimal,9),
					new SqlParameter("@ghbt", SqlDbType.Decimal,9),
					new SqlParameter("@hj", SqlDbType.Decimal,9),
					new SqlParameter("@insertUserID", SqlDbType.Int,4),
					new SqlParameter("@insertDate", SqlDbType.DateTime),
					new SqlParameter("@Stat", SqlDbType.Int,4),
                    new SqlParameter("@zcfy", SqlDbType.Decimal,9)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.UnitID;
			parameters[2].Value = model.UnitName;
			parameters[3].Value = model.MemID;
			parameters[4].Value = model.MemName;
			parameters[5].Value = model.IsFired;
			parameters[6].Value = model.bmjl;
			parameters[7].Value = model.gstz;
			parameters[8].Value = model.yfjj;
			parameters[9].Value = model.xmjjjs;
			parameters[10].Value = model.bmnjjjs;
			parameters[11].Value = model.xmjj;
			parameters[12].Value = model.bmnjj;
			parameters[13].Value = model.xmglj;
			parameters[14].Value = model.yxyg;
			parameters[15].Value = model.tcgx;
			parameters[16].Value = model.bimjj;
			parameters[17].Value = model.ghbt;
			parameters[18].Value = model.hj;
			parameters[19].Value = model.insertUserID;
			parameters[20].Value = model.insertDate;
			parameters[21].Value = model.Stat;
            parameters[22].Value = model.zcfy;
			object obj = DbHelperSQL.GetSingle(strSql.ToString(),parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.cm_KaoHeAllMemsAllotDetails model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update cm_KaoHeAllMemsAllotDetails set ");
			strSql.Append("RenwuID=@RenwuID,");
			strSql.Append("UnitID=@UnitID,");
			strSql.Append("UnitName=@UnitName,");
			strSql.Append("MemID=@MemID,");
			strSql.Append("MemName=@MemName,");
			strSql.Append("IsFired=@IsFired,");
			strSql.Append("bmjl=@bmjl,");
			strSql.Append("gstz=@gstz,");
			strSql.Append("yfjj=@yfjj,");
			strSql.Append("xmjjjs=@xmjjjs,");
			strSql.Append("bmnjjjs=@bmnjjjs,");
			strSql.Append("xmjj=@xmjj,");
			strSql.Append("bmnjj=@bmnjj,");
			strSql.Append("xmglj=@xmglj,");
			strSql.Append("yxyg=@yxyg,");
			strSql.Append("tcgx=@tcgx,");
			strSql.Append("bimjj=@bimjj,");
			strSql.Append("ghbt=@ghbt,");
			strSql.Append("hj=@hj,");
			strSql.Append("insertUserID=@insertUserID,");
			strSql.Append("insertDate=@insertDate,");
			strSql.Append("Stat=@Stat,");
            strSql.Append("zcfy=@zcfy");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@RenwuID", SqlDbType.Int,4),
					new SqlParameter("@UnitID", SqlDbType.Int,4),
					new SqlParameter("@UnitName", SqlDbType.NVarChar,50),
					new SqlParameter("@MemID", SqlDbType.Int,4),
					new SqlParameter("@MemName", SqlDbType.NVarChar,50),
					new SqlParameter("@IsFired", SqlDbType.Int,4),
					new SqlParameter("@bmjl", SqlDbType.Decimal,9),
					new SqlParameter("@gstz", SqlDbType.Decimal,9),
					new SqlParameter("@yfjj", SqlDbType.Decimal,9),
					new SqlParameter("@xmjjjs", SqlDbType.Decimal,9),
					new SqlParameter("@bmnjjjs", SqlDbType.Decimal,9),
					new SqlParameter("@xmjj", SqlDbType.Decimal,9),
					new SqlParameter("@bmnjj", SqlDbType.Decimal,9),
					new SqlParameter("@xmglj", SqlDbType.Decimal,9),
					new SqlParameter("@yxyg", SqlDbType.Decimal,9),
					new SqlParameter("@tcgx", SqlDbType.Decimal,9),
					new SqlParameter("@bimjj", SqlDbType.Decimal,9),
					new SqlParameter("@ghbt", SqlDbType.Decimal,9),
					new SqlParameter("@hj", SqlDbType.Decimal,9),
					new SqlParameter("@insertUserID", SqlDbType.Int,4),
					new SqlParameter("@insertDate", SqlDbType.DateTime),
					new SqlParameter("@Stat", SqlDbType.Int,4),
                    new SqlParameter("@zcfy", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
			parameters[0].Value = model.RenwuID;
			parameters[1].Value = model.UnitID;
			parameters[2].Value = model.UnitName;
			parameters[3].Value = model.MemID;
			parameters[4].Value = model.MemName;
			parameters[5].Value = model.IsFired;
			parameters[6].Value = model.bmjl;
			parameters[7].Value = model.gstz;
			parameters[8].Value = model.yfjj;
			parameters[9].Value = model.xmjjjs;
			parameters[10].Value = model.bmnjjjs;
			parameters[11].Value = model.xmjj;
			parameters[12].Value = model.bmnjj;
			parameters[13].Value = model.xmglj;
			parameters[14].Value = model.yxyg;
			parameters[15].Value = model.tcgx;
			parameters[16].Value = model.bimjj;
			parameters[17].Value = model.ghbt;
			parameters[18].Value = model.hj;
			parameters[19].Value = model.insertUserID;
			parameters[20].Value = model.insertDate;
			parameters[21].Value = model.Stat;
            parameters[22].Value = model.zcfy;
			parameters[23].Value = model.ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeAllMemsAllotDetails ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			int rows=DbHelperSQL.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string IDlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from cm_KaoHeAllMemsAllotDetails ");
			strSql.Append(" where ID in ("+IDlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeAllMemsAllotDetails GetModel(int ID)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select  top 1 ID,RenwuID,UnitID,UnitName,MemID,MemName,IsFired,bmjl,gstz,yfjj,xmjjjs,bmnjjjs,xmjj,bmnjj,xmglj,yxyg,tcgx,bimjj,ghbt,hj,insertUserID,insertDate,Stat,zcfy from cm_KaoHeAllMemsAllotDetails ");
			strSql.Append(" where ID=@ID");
			SqlParameter[] parameters = {
					new SqlParameter("@ID", SqlDbType.Int,4)
			};
			parameters[0].Value = ID;

			TG.Model.cm_KaoHeAllMemsAllotDetails model=new TG.Model.cm_KaoHeAllMemsAllotDetails();
			DataSet ds=DbHelperSQL.Query(strSql.ToString(),parameters);
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.cm_KaoHeAllMemsAllotDetails DataRowToModel(DataRow row)
		{
			TG.Model.cm_KaoHeAllMemsAllotDetails model=new TG.Model.cm_KaoHeAllMemsAllotDetails();
			if (row != null)
			{
				if(row["ID"]!=null && row["ID"].ToString()!="")
				{
					model.ID=int.Parse(row["ID"].ToString());
				}
				if(row["RenwuID"]!=null && row["RenwuID"].ToString()!="")
				{
					model.RenwuID=int.Parse(row["RenwuID"].ToString());
				}
				if(row["UnitID"]!=null && row["UnitID"].ToString()!="")
				{
					model.UnitID=int.Parse(row["UnitID"].ToString());
				}
				if(row["UnitName"]!=null)
				{
					model.UnitName=row["UnitName"].ToString();
				}
				if(row["MemID"]!=null && row["MemID"].ToString()!="")
				{
					model.MemID=int.Parse(row["MemID"].ToString());
				}
				if(row["MemName"]!=null)
				{
					model.MemName=row["MemName"].ToString();
				}
				if(row["IsFired"]!=null && row["IsFired"].ToString()!="")
				{
					model.IsFired=int.Parse(row["IsFired"].ToString());
				}
				if(row["bmjl"]!=null && row["bmjl"].ToString()!="")
				{
					model.bmjl=decimal.Parse(row["bmjl"].ToString());
				}
				if(row["gstz"]!=null && row["gstz"].ToString()!="")
				{
					model.gstz=decimal.Parse(row["gstz"].ToString());
				}
				if(row["yfjj"]!=null && row["yfjj"].ToString()!="")
				{
					model.yfjj=decimal.Parse(row["yfjj"].ToString());
				}
				if(row["xmjjjs"]!=null && row["xmjjjs"].ToString()!="")
				{
					model.xmjjjs=decimal.Parse(row["xmjjjs"].ToString());
				}
				if(row["bmnjjjs"]!=null && row["bmnjjjs"].ToString()!="")
				{
					model.bmnjjjs=decimal.Parse(row["bmnjjjs"].ToString());
				}
				if(row["xmjj"]!=null && row["xmjj"].ToString()!="")
				{
					model.xmjj=decimal.Parse(row["xmjj"].ToString());
				}
				if(row["bmnjj"]!=null && row["bmnjj"].ToString()!="")
				{
					model.bmnjj=decimal.Parse(row["bmnjj"].ToString());
				}
				if(row["xmglj"]!=null && row["xmglj"].ToString()!="")
				{
					model.xmglj=decimal.Parse(row["xmglj"].ToString());
				}
				if(row["yxyg"]!=null && row["yxyg"].ToString()!="")
				{
					model.yxyg=decimal.Parse(row["yxyg"].ToString());
				}
				if(row["tcgx"]!=null && row["tcgx"].ToString()!="")
				{
					model.tcgx=decimal.Parse(row["tcgx"].ToString());
				}
				if(row["bimjj"]!=null && row["bimjj"].ToString()!="")
				{
					model.bimjj=decimal.Parse(row["bimjj"].ToString());
				}
				if(row["ghbt"]!=null && row["ghbt"].ToString()!="")
				{
					model.ghbt=decimal.Parse(row["ghbt"].ToString());
				}
				if(row["hj"]!=null && row["hj"].ToString()!="")
				{
					model.hj=decimal.Parse(row["hj"].ToString());
				}
				if(row["insertUserID"]!=null && row["insertUserID"].ToString()!="")
				{
					model.insertUserID=int.Parse(row["insertUserID"].ToString());
				}
				if(row["insertDate"]!=null && row["insertDate"].ToString()!="")
				{
					model.insertDate=DateTime.Parse(row["insertDate"].ToString());
				}
				if(row["Stat"]!=null && row["Stat"].ToString()!="")
				{
					model.Stat=int.Parse(row["Stat"].ToString());
				}
                if (row["zcfy"] == null && row["zcfy"].ToString() != "")
                {
                   model.zcfy = decimal.Parse(row["zcfy"].ToString());
                 }
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
            strSql.Append("select ID,RenwuID,UnitID,UnitName,MemID,MemName,IsFired,bmjl,gstz,yfjj,xmjjjs,bmnjjjs,xmjj,bmnjj,xmglj,yxyg,tcgx,bimjj,ghbt,hj,insertUserID,insertDate,Stat,zcfy ");
			strSql.Append(" FROM cm_KaoHeAllMemsAllotDetails ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" ID,RenwuID,UnitID,UnitName,MemID,MemName,IsFired,bmjl,gstz,yfjj,xmjjjs,bmnjjjs,xmjj,bmnjj,xmglj,yxyg,tcgx,bimjj,ghbt,hj,insertUserID,insertDate,Stat,zcfy ");
			strSql.Append(" FROM cm_KaoHeAllMemsAllotDetails ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM cm_KaoHeAllMemsAllotDetails ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.ID desc");
			}
			strSql.Append(")AS Row, T.*  from cm_KaoHeAllMemsAllotDetails T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "cm_KaoHeAllMemsAllotDetails";
			parameters[1].Value = "ID";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

