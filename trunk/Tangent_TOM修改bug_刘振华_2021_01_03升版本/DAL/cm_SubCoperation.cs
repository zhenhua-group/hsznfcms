﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
using System.Collections.Generic;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:cm_SubCoperation
    /// </summary>
    public partial class cm_SubCoperation
    {
        public cm_SubCoperation()
        { }
        #region  Method



        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_SubCoperation model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.cpr_Id != null)
            {
                strSql1.Append("cpr_Id,");
                strSql2.Append("" + model.cpr_Id + ",");
            }
            if (model.cpr_No != null)
            {
                strSql1.Append("cpr_No,");
                strSql2.Append("'" + model.cpr_No + "',");
            }
            if (model.Item_Name != null)
            {
                strSql1.Append("Item_Name,");
                strSql2.Append("'" + model.Item_Name + "',");
            }
            if (model.Item_Area != null)
            {
                strSql1.Append("Item_Area,");
                strSql2.Append("'" + model.Item_Area + "',");
            }
            if (model.UpdateBy != null)
            {
                strSql1.Append("UpdateBy,");
                strSql2.Append("'" + model.UpdateBy + "',");
            }
            if (model.LastUpdate != null)
            {
                strSql1.Append("LastUpdate,");
                strSql2.Append("'" + model.LastUpdate + "',");
            }
            if (!string.IsNullOrEmpty(model.Remark))
            {
                strSql1.Append("Remark,");
                strSql2.Append("'" + model.Remark + "',");
            }
            if (!string.IsNullOrEmpty(model.MoneyStatus))
            {
                strSql1.Append("MoneyStatus,");
                strSql2.Append("'" + model.MoneyStatus + "',");
            }
            strSql1.Append("Money,");
            strSql2.Append("" + model.Money + ",");

            strSql.Append("insert into cm_SubCoperation(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_SubCoperation model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_SubCoperation set ");
            if (model.cpr_Id != null)
            {
                strSql.Append("cpr_Id=" + model.cpr_Id + ",");
            }
            else
            {
                strSql.Append("cpr_Id= null ,");
            }
            if (model.cpr_No != null)
            {
                strSql.Append("cpr_No='" + model.cpr_No + "',");
            }
            else
            {
                strSql.Append("cpr_No= null ,");
            }
            if (model.Item_Name != null)
            {
                strSql.Append("Item_Name='" + model.Item_Name + "',");
            }
            else
            {
                strSql.Append("Item_Name= null ,");
            }
            if (model.Item_Area != null)
            {
                strSql.Append("Item_Area='" + model.Item_Area + "',");
            }
            else
            {
                strSql.Append("Item_Area= null ,");
            }
            if (model.UpdateBy != null)
            {
                strSql.Append("UpdateBy='" + model.UpdateBy + "',");
            }
            else
            {
                strSql.Append("UpdateBy= null ,");
            }
            if (model.LastUpdate != null)
            {
                strSql.Append("LastUpdate='" + model.LastUpdate + "',");
            }
            else
            {
                strSql.Append("LastUpdate= null ,");
            }

            if (model.Money != decimal.Zero)
            {
                strSql.Append("Money=" + model.Money + ",");
            }
            if (model.MoneyStatus != null)
            {
                strSql.Append("MoneyStatus='" + model.MoneyStatus + "',");
            }
            else
            {
                strSql.Append("MoneyStatus= null ,");
            }
            strSql.Append("Remark =N'" + model.Remark + "',");

            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where ID=" + model.ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_SubCoperation ");
            strSql.Append(" where ID=" + ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from cm_SubCoperation ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.cm_SubCoperation GetModel(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" ID,cpr_Id,cpr_No,Item_Name,Item_Area,UpdateBy,LastUpdate,MoneyStatus ");
            strSql.Append(" from cm_SubCoperation ");
            strSql.Append(" where ID=" + ID + "");
            TG.Model.cm_SubCoperation model = new TG.Model.cm_SubCoperation();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_Id"] != null && ds.Tables[0].Rows[0]["cpr_Id"].ToString() != "")
                {
                    model.cpr_Id = int.Parse(ds.Tables[0].Rows[0]["cpr_Id"].ToString());
                }
                if (ds.Tables[0].Rows[0]["cpr_No"] != null && ds.Tables[0].Rows[0]["cpr_No"].ToString() != "")
                {
                    model.cpr_No = ds.Tables[0].Rows[0]["cpr_No"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Item_Name"] != null && ds.Tables[0].Rows[0]["Item_Name"].ToString() != "")
                {
                    model.Item_Name = ds.Tables[0].Rows[0]["Item_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Item_Area"] != null && ds.Tables[0].Rows[0]["Item_Area"].ToString() != "")
                {
                    model.Item_Area = ds.Tables[0].Rows[0]["Item_Area"].ToString();
                }
                if (ds.Tables[0].Rows[0]["UpdateBy"] != null && ds.Tables[0].Rows[0]["UpdateBy"].ToString() != "")
                {
                    model.UpdateBy = ds.Tables[0].Rows[0]["UpdateBy"].ToString();
                }
                if (ds.Tables[0].Rows[0]["LastUpdate"] != null && ds.Tables[0].Rows[0]["LastUpdate"].ToString() != "")
                {
                    model.LastUpdate = DateTime.Parse(ds.Tables[0].Rows[0]["LastUpdate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["MoneyStatus"] != null && ds.Tables[0].Rows[0]["MoneyStatus"].ToString() != "")
                {
                    model.MoneyStatus = ds.Tables[0].Rows[0]["MoneyStatus"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,cpr_Id,cpr_No,Item_Name,Item_Area,UpdateBy,LastUpdate,Money,Remark,MoneyStatus ");
            strSql.Append(" FROM cm_SubCoperation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,cpr_Id,cpr_No,Item_Name,Item_Area,UpdateBy,LastUpdate,MoneyStatus ");
            strSql.Append(" FROM cm_SubCoperation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM cm_SubCoperation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            /*没有调用此方法，无需修改。Created by Sago*/
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBE() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from cm_SubCoperation T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 根据合同系统自增号查询合同分项信息
        /// </summary>
        /// <param name="coperationSysNo"></param>
        /// <returns></returns>
        public List<TG.Model.cm_SubCoperation> GetSubCoperationByCoperationSysNo(int coperationSysNo)
        {
            string sql = "select ID,cpr_Id,cpr_No,Item_Name,Item_Area,UpdateBy,LastUpdate,MoneyStatus from cm_SubCoperation WHERE cpr_Id = " + coperationSysNo;
            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);
            List<TG.Model.cm_SubCoperation> resultList = new List<TG.Model.cm_SubCoperation>();

            while (reader.Read())
            {
                TG.Model.cm_SubCoperation subCoperation = new TG.Model.cm_SubCoperation
                {
                    ID = reader["ID"] == null ? 0 : Convert.ToInt32(reader["ID"]),
                    cpr_No = reader["cpr_No"] == null ? "" : reader["cpr_No"].ToString(),
                    Item_Name = reader["Item_Name"] == null ? "" : reader["Item_Name"].ToString(),
                    Item_Area = reader["Item_Area"] == null ? "" : reader["Item_Area"].ToString(),
                    UpdateBy = reader["UpdateBy"] == null ? "" : reader["UpdateBy"].ToString(),
                    LastUpdate = reader["LastUpdate"] == null ? DateTime.MinValue : Convert.ToDateTime(reader["LastUpdate"]),
                    MoneyStatus = reader["MoneyStatus"] == null ? "" : reader["MoneyStatus"].ToString()
                };
                resultList.Add(subCoperation);
            }
            reader.Close();
            return resultList;
        }
        /*
        */

        #endregion  Method
    }
}

