﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:tg_project
    /// </summary>
    public partial class tg_project
    {
        public tg_project()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("pro_ID", "tg_project");
        }


        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int pro_ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from tg_project");
            strSql.Append(" where pro_ID=" + pro_ID + " ");
            return DbHelperSQL.Exists(strSql.ToString());
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.tg_project model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.pro_Name != null)
            {
                strSql1.Append("pro_Name,");
                strSql2.Append("'" + model.pro_Name + "',");
            }
            if (model.pro_Order != null)
            {
                strSql1.Append("pro_Order,");
                strSql2.Append("'" + model.pro_Order + "',");
            }
            if (model.pro_Intro != null)
            {
                strSql1.Append("pro_Intro,");
                strSql2.Append("'" + model.pro_Intro + "',");
            }
            if (model.pro_count != null)
            {
                strSql1.Append("pro_count,");
                strSql2.Append("" + model.pro_count + ",");
            }
            if (model.pro_Kinds != null)
            {
                strSql1.Append("pro_Kinds,");
                strSql2.Append("'" + model.pro_Kinds + "',");
            }
            if (model.pro_Icon != null)
            {
                strSql1.Append("pro_Icon,");
                strSql2.Append("" + model.pro_Icon + ",");
            }
            if (model.pro_StartTime != null)
            {
                strSql1.Append("pro_StartTime,");
                strSql2.Append("'" + model.pro_StartTime + "',");
            }
            if (model.pro_FinishTime != null)
            {
                strSql1.Append("pro_FinishTime,");
                strSql2.Append("'" + model.pro_FinishTime + "',");
            }
            if (model.pro_Status != null)
            {
                strSql1.Append("pro_Status,");
                strSql2.Append("'" + model.pro_Status + "',");
            }
            if (model.pro_PostArticle != null)
            {
                strSql1.Append("pro_PostArticle,");
                strSql2.Append("" + model.pro_PostArticle + ",");
            }
            if (model.pro_PostComms != null)
            {
                strSql1.Append("pro_PostComms,");
                strSql2.Append("" + model.pro_PostComms + ",");
            }
            if (model.pro_BuildUnit != null)
            {
                strSql1.Append("pro_BuildUnit,");
                strSql2.Append("'" + model.pro_BuildUnit + "',");
            }
            if (model.pro_ArchArea != null)
            {
                strSql1.Append("pro_ArchArea,");
                strSql2.Append("" + model.pro_ArchArea + ",");
            }
            if (model.pro_FloorNum != null)
            {
                strSql1.Append("pro_FloorNum,");
                strSql2.Append("" + model.pro_FloorNum + ",");
            }
            if (model.pro_StruType != null)
            {
                strSql1.Append("pro_StruType,");
                strSql2.Append("'" + model.pro_StruType + "',");
            }
            if (model.pro_ArchHeight != null)
            {
                strSql1.Append("pro_ArchHeight,");
                strSql2.Append("" + model.pro_ArchHeight + ",");
            }
            if (model.pro_Serial != null)
            {
                strSql1.Append("pro_Serial,");
                strSql2.Append("'" + model.pro_Serial + "',");
            }
            if (model.pro_DesignUnit != null)
            {
                strSql1.Append("pro_DesignUnit,");
                strSql2.Append("'" + model.pro_DesignUnit + "',");
            }
            if (model.pro_Level != null)
            {
                strSql1.Append("pro_Level,");
                strSql2.Append("" + model.pro_Level + ",");
            }
            if (model.pro_UsedSpace != null)
            {
                strSql1.Append("pro_UsedSpace,");
                strSql2.Append("" + model.pro_UsedSpace + ",");
            }
            if (model.pro_AllSpace != null)
            {
                strSql1.Append("pro_AllSpace,");
                strSql2.Append("" + model.pro_AllSpace + ",");
            }
            if (model.pro_category != null)
            {
                strSql1.Append("pro_category,");
                strSql2.Append("" + model.pro_category + ",");
            }
            if (model.pro_Adress != null)
            {
                strSql1.Append("pro_Adress,");
                strSql2.Append("'" + model.pro_Adress + "',");
            }
            if (model.pro_Summary != null)
            {
                strSql1.Append("pro_Summary,");
                strSql2.Append("'" + model.pro_Summary + "',");
            }
            if (model.pro_FloorUp != null)
            {
                strSql1.Append("pro_FloorUp,");
                strSql2.Append("" + model.pro_FloorUp + ",");
            }
            if (model.pro_FloorDown != null)
            {
                strSql1.Append("pro_FloorDown,");
                strSql2.Append("" + model.pro_FloorDown + ",");
            }
            if (model.pro_flag != null)
            {
                strSql1.Append("pro_flag,");
                strSql2.Append("" + model.pro_flag + ",");
            }
            if (model.pro_ExInfo1 != null)
            {
                strSql1.Append("pro_ExInfo1,");
                strSql2.Append("" + model.pro_ExInfo1 + ",");
            }
            if (model.pro_ExInfo2 != null)
            {
                strSql1.Append("pro_ExInfo2,");
                strSql2.Append("'" + model.pro_ExInfo2 + "',");
            }
            if (model.pro_ExInfo3 != null)
            {
                strSql1.Append("pro_ExInfo3,");
                strSql2.Append("'" + model.pro_ExInfo3 + "',");
            }
            if (model.pro_Phone != null)
            {
                strSql1.Append("pro_Phone,");
                strSql2.Append("'" + model.pro_Phone + "',");
            }
            if (model.pro_ContractNo != null)
            {
                strSql1.Append("pro_ContractNo,");
                strSql2.Append("'" + model.pro_ContractNo + "',");
            }
            if (model.pro_DesignUnitID >= 0)
            {
                strSql1.Append("pro_DesignUnitID,");
                strSql2.Append("'" + model.pro_DesignUnitID + "',");
            }
            strSql.Append("insert into tg_project(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.tg_project model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update tg_project set ");
            if (model.pro_Name != null)
            {
                strSql.Append("pro_Name='" + model.pro_Name + "',");
            }
            if (model.pro_Order != null)
            {
                strSql.Append("pro_Order='" + model.pro_Order + "',");
            }
            else
            {
                strSql.Append("pro_Order= null ,");
            }
            if (model.pro_Intro != null)
            {
                strSql.Append("pro_Intro='" + model.pro_Intro + "',");
            }
            else
            {
                strSql.Append("pro_Intro= null ,");
            }
            if (model.pro_count != null)
            {
                strSql.Append("pro_count=" + model.pro_count + ",");
            }
            else
            {
                strSql.Append("pro_count= null ,");
            }
            if (model.pro_Kinds != null)
            {
                strSql.Append("pro_Kinds='" + model.pro_Kinds + "',");
            }
            else
            {
                strSql.Append("pro_Kinds= null ,");
            }
            if (model.pro_Icon != null)
            {
                strSql.Append("pro_Icon=" + model.pro_Icon + ",");
            }
            else
            {
                strSql.Append("pro_Icon= null ,");
            }
            if (model.pro_StartTime != null)
            {
                strSql.Append("pro_StartTime='" + model.pro_StartTime + "',");
            }
            else
            {
                strSql.Append("pro_StartTime= null ,");
            }
            if (model.pro_FinishTime != null)
            {
                strSql.Append("pro_FinishTime='" + model.pro_FinishTime + "',");
            }
            else
            {
                strSql.Append("pro_FinishTime= null ,");
            }
            if (model.pro_Status != null)
            {
                strSql.Append("pro_Status='" + model.pro_Status + "',");
            }
            else
            {
                strSql.Append("pro_Status= null ,");
            }
            if (model.pro_PostArticle != null)
            {
                strSql.Append("pro_PostArticle=" + model.pro_PostArticle + ",");
            }
            else
            {
                strSql.Append("pro_PostArticle= null ,");
            }
            if (model.pro_PostComms != null)
            {
                strSql.Append("pro_PostComms=" + model.pro_PostComms + ",");
            }
            else
            {
                strSql.Append("pro_PostComms= null ,");
            }
            if (model.pro_BuildUnit != null)
            {
                strSql.Append("pro_BuildUnit='" + model.pro_BuildUnit + "',");
            }
            else
            {
                strSql.Append("pro_BuildUnit= null ,");
            }
            if (model.pro_ArchArea != null)
            {
                strSql.Append("pro_ArchArea=" + model.pro_ArchArea + ",");
            }
            else
            {
                strSql.Append("pro_ArchArea= null ,");
            }
            if (model.pro_FloorNum != null)
            {
                strSql.Append("pro_FloorNum=" + model.pro_FloorNum + ",");
            }
            else
            {
                strSql.Append("pro_FloorNum= null ,");
            }
            if (model.pro_StruType != null)
            {
                strSql.Append("pro_StruType='" + model.pro_StruType + "',");
            }
            else
            {
                strSql.Append("pro_StruType= null ,");
            }
            if (model.pro_ArchHeight != null)
            {
                strSql.Append("pro_ArchHeight=" + model.pro_ArchHeight + ",");
            }
            else
            {
                strSql.Append("pro_ArchHeight= null ,");
            }
            if (model.pro_Serial != null)
            {
                strSql.Append("pro_Serial='" + model.pro_Serial + "',");
            }
            else
            {
                strSql.Append("pro_Serial= null ,");
            }
            if (model.pro_DesignUnit != null)
            {
                strSql.Append("pro_DesignUnit='" + model.pro_DesignUnit + "',");
            }
            else
            {
                strSql.Append("pro_DesignUnit= null ,");
            }
            if (model.pro_Level != null)
            {
                strSql.Append("pro_Level=" + model.pro_Level + ",");
            }
            else
            {
                strSql.Append("pro_Level= null ,");
            }
            if (model.pro_UsedSpace != null)
            {
                strSql.Append("pro_UsedSpace=" + model.pro_UsedSpace + ",");
            }
            else
            {
                strSql.Append("pro_UsedSpace= null ,");
            }
            if (model.pro_AllSpace != null)
            {
                strSql.Append("pro_AllSpace=" + model.pro_AllSpace + ",");
            }
            else
            {
                strSql.Append("pro_AllSpace= null ,");
            }
            if (model.pro_category != null)
            {
                strSql.Append("pro_category=" + model.pro_category + ",");
            }
            if (model.pro_Adress != null)
            {
                strSql.Append("pro_Adress='" + model.pro_Adress + "',");
            }
            else
            {
                strSql.Append("pro_Adress= null ,");
            }
            if (model.pro_Summary != null)
            {
                strSql.Append("pro_Summary='" + model.pro_Summary + "',");
            }
            else
            {
                strSql.Append("pro_Summary= null ,");
            }
            if (model.pro_FloorUp != null)
            {
                strSql.Append("pro_FloorUp=" + model.pro_FloorUp + ",");
            }
            else
            {
                strSql.Append("pro_FloorUp= null ,");
            }
            if (model.pro_FloorDown != null)
            {
                strSql.Append("pro_FloorDown=" + model.pro_FloorDown + ",");
            }
            else
            {
                strSql.Append("pro_FloorDown= null ,");
            }
            if (model.pro_ExInfo1 != null)
            {
                strSql.Append("pro_ExInfo1=" + model.pro_ExInfo1 + ",");
            }
            else
            {
                strSql.Append("pro_ExInfo1= null ,");
            }
            if (model.pro_ExInfo2 != null)
            {
                strSql.Append("pro_ExInfo2='" + model.pro_ExInfo2 + "',");
            }
            else
            {
                strSql.Append("pro_ExInfo2= null ,");
            }
            if (model.pro_ExInfo3 != null)
            {
                strSql.Append("pro_ExInfo3='" + model.pro_ExInfo3 + "',");
            }
            else
            {
                strSql.Append("pro_ExInfo3= null ,");
            }
            if (model.pro_Phone != null)
            {
                strSql.Append("pro_Phone='" + model.pro_Phone + "',");
            }
            else
            {
                strSql.Append("pro_Phone= null ,");
            }
            if (model.pro_ContractNo != null)
            {
                strSql.Append("pro_ContractNo='" + model.pro_ContractNo + "',");
            }
            else
            {
                strSql.Append("pro_ContractNo= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where pro_ID=" + model.pro_ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdateStatus(string projectStatus, int proid)
        {
            //删除以前的数据
            string sql2 = @"delete tg_endSubItem  where purposeID IN (7,8,9) AND subproID in (select pro_ID from tg_subproject where pro_parentID=" + proid + ")";
            string sql3 = @"select pro_ID from tg_subproject where pro_parentID=" + proid;
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql3);
            int proID = int.Parse(o.ToString());
            //插入阶段sql
            //insert into tg_endSubItem values(" + purposeID + "," + subProjectSysNo + ")";
            string sql = "";

            string[] arrayStatus = projectStatus.Split(',');
            for (int i = 0; i < arrayStatus.Length; i++)
            {
                if (arrayStatus[i].Contains("方"))
                {
                    sql += @"insert into tg_endSubItem values(7," + proID + ")";
                }
                if (arrayStatus[i].Contains("初"))
                {
                    sql += @"insert into tg_endSubItem values(8," + proID + ")";
                }
                if (arrayStatus[i].Contains("施"))
                {
                    sql += @"insert into tg_endSubItem values(9," + proID + ")";
                }
            }
            TG.DBUtility.DbHelperSQL.ExecuteSql(sql2);
            TG.DBUtility.DbHelperSQL.ExecuteSql(sql);

            return true;
        }


        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int pro_ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tg_project ");
            strSql.Append(" where pro_ID=" + pro_ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string pro_IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tg_project ");
            strSql.Append(" where pro_ID in (" + pro_IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.tg_project GetModel(int pro_ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" pro_ID,pro_Name,pro_Order,pro_Intro,pro_count,pro_Kinds,pro_Icon,pro_StartTime,pro_FinishTime,pro_Status,pro_PostArticle,pro_PostComms,pro_BuildUnit,pro_ArchArea,pro_FloorNum,pro_StruType,pro_ArchHeight,pro_Serial,pro_DesignUnit,pro_Level,pro_UsedSpace,pro_AllSpace,pro_category,pro_adress,pro_summary,pro_floorup,pro_floordown,pro_flag,pro_ExInfo1,pro_ExInfo2,pro_ExInfo3,pro_Phone,pro_ContractNo ");
            strSql.Append(" from tg_project ");
            strSql.Append(" where pro_ID=" + pro_ID + "");
            TG.Model.tg_project model = new TG.Model.tg_project();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["pro_ID"] != null && ds.Tables[0].Rows[0]["pro_ID"].ToString() != "")
                {
                    model.pro_ID = int.Parse(ds.Tables[0].Rows[0]["pro_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_Name"] != null && ds.Tables[0].Rows[0]["pro_Name"].ToString() != "")
                {
                    model.pro_Name = ds.Tables[0].Rows[0]["pro_Name"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_Order"] != null && ds.Tables[0].Rows[0]["pro_Order"].ToString() != "")
                {
                    model.pro_Order = ds.Tables[0].Rows[0]["pro_Order"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_Intro"] != null && ds.Tables[0].Rows[0]["pro_Intro"].ToString() != "")
                {
                    model.pro_Intro = ds.Tables[0].Rows[0]["pro_Intro"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_count"] != null && ds.Tables[0].Rows[0]["pro_count"].ToString() != "")
                {
                    model.pro_count = int.Parse(ds.Tables[0].Rows[0]["pro_count"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_Kinds"] != null && ds.Tables[0].Rows[0]["pro_Kinds"].ToString() != "")
                {
                    model.pro_Kinds = ds.Tables[0].Rows[0]["pro_Kinds"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_Icon"] != null && ds.Tables[0].Rows[0]["pro_Icon"].ToString() != "")
                {
                    model.pro_Icon = int.Parse(ds.Tables[0].Rows[0]["pro_Icon"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_StartTime"] != null && ds.Tables[0].Rows[0]["pro_StartTime"].ToString() != "")
                {
                    model.pro_StartTime = DateTime.Parse(ds.Tables[0].Rows[0]["pro_StartTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_FinishTime"] != null && ds.Tables[0].Rows[0]["pro_FinishTime"].ToString() != "")
                {
                    model.pro_FinishTime = DateTime.Parse(ds.Tables[0].Rows[0]["pro_FinishTime"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_Status"] != null && ds.Tables[0].Rows[0]["pro_Status"].ToString() != "")
                {
                    model.pro_Status = ds.Tables[0].Rows[0]["pro_Status"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_PostArticle"] != null && ds.Tables[0].Rows[0]["pro_PostArticle"].ToString() != "")
                {
                    model.pro_PostArticle = int.Parse(ds.Tables[0].Rows[0]["pro_PostArticle"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_PostComms"] != null && ds.Tables[0].Rows[0]["pro_PostComms"].ToString() != "")
                {
                    model.pro_PostComms = int.Parse(ds.Tables[0].Rows[0]["pro_PostComms"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_BuildUnit"] != null && ds.Tables[0].Rows[0]["pro_BuildUnit"].ToString() != "")
                {
                    model.pro_BuildUnit = ds.Tables[0].Rows[0]["pro_BuildUnit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_ArchArea"] != null && ds.Tables[0].Rows[0]["pro_ArchArea"].ToString() != "")
                {
                    model.pro_ArchArea = decimal.Parse(ds.Tables[0].Rows[0]["pro_ArchArea"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_FloorNum"] != null && ds.Tables[0].Rows[0]["pro_FloorNum"].ToString() != "")
                {
                    model.pro_FloorNum = int.Parse(ds.Tables[0].Rows[0]["pro_FloorNum"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_StruType"] != null && ds.Tables[0].Rows[0]["pro_StruType"].ToString() != "")
                {
                    model.pro_StruType = ds.Tables[0].Rows[0]["pro_StruType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_ArchHeight"] != null && ds.Tables[0].Rows[0]["pro_ArchHeight"].ToString() != "")
                {
                    model.pro_ArchHeight = decimal.Parse(ds.Tables[0].Rows[0]["pro_ArchHeight"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_Serial"] != null && ds.Tables[0].Rows[0]["pro_Serial"].ToString() != "")
                {
                    model.pro_Serial = ds.Tables[0].Rows[0]["pro_Serial"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_DesignUnit"] != null && ds.Tables[0].Rows[0]["pro_DesignUnit"].ToString() != "")
                {
                    model.pro_DesignUnit = ds.Tables[0].Rows[0]["pro_DesignUnit"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_Level"] != null && ds.Tables[0].Rows[0]["pro_Level"].ToString() != "")
                {
                    model.pro_Level = int.Parse(ds.Tables[0].Rows[0]["pro_Level"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_UsedSpace"] != null && ds.Tables[0].Rows[0]["pro_UsedSpace"].ToString() != "")
                {
                    model.pro_UsedSpace = decimal.Parse(ds.Tables[0].Rows[0]["pro_UsedSpace"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_AllSpace"] != null && ds.Tables[0].Rows[0]["pro_AllSpace"].ToString() != "")
                {
                    model.pro_AllSpace = decimal.Parse(ds.Tables[0].Rows[0]["pro_AllSpace"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_category"] != null && ds.Tables[0].Rows[0]["pro_category"].ToString() != "")
                {
                    model.pro_category = int.Parse(ds.Tables[0].Rows[0]["pro_category"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_Adress"] != null && ds.Tables[0].Rows[0]["pro_Adress"].ToString() != "")
                {
                    model.pro_Adress = ds.Tables[0].Rows[0]["pro_Adress"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_Summary"] != null && ds.Tables[0].Rows[0]["pro_Summary"].ToString() != "")
                {
                    model.pro_Summary = ds.Tables[0].Rows[0]["pro_Summary"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_ExInfo1"] != null && ds.Tables[0].Rows[0]["pro_ExInfo1"].ToString() != "")
                {
                    model.pro_ExInfo1 = int.Parse(ds.Tables[0].Rows[0]["pro_ExInfo1"].ToString());
                }
                if (ds.Tables[0].Rows[0]["pro_ExInfo2"] != null && ds.Tables[0].Rows[0]["pro_ExInfo2"].ToString() != "")
                {
                    model.pro_ExInfo2 = ds.Tables[0].Rows[0]["pro_ExInfo2"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_ExInfo3"] != null && ds.Tables[0].Rows[0]["pro_ExInfo3"].ToString() != "")
                {
                    model.pro_ExInfo3 = ds.Tables[0].Rows[0]["pro_ExInfo3"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_Phone"] != null && ds.Tables[0].Rows[0]["pro_Phone"].ToString() != "")
                {
                    model.pro_Phone = ds.Tables[0].Rows[0]["pro_Phone"].ToString();
                }
                if (ds.Tables[0].Rows[0]["pro_ContractNo"] != null && ds.Tables[0].Rows[0]["pro_ContractNo"].ToString() != "")
                {
                    model.pro_ContractNo = ds.Tables[0].Rows[0]["pro_ContractNo"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select pro_ID,pro_Name,pro_Order,pro_Intro,pro_count,pro_Kinds,pro_Icon,pro_StartTime,pro_FinishTime,pro_Status,pro_PostArticle,pro_PostComms,pro_BuildUnit,pro_ArchArea,pro_FloorNum,pro_StruType,pro_ArchHeight,pro_Serial,pro_DesignUnit,pro_Level,pro_UsedSpace,pro_AllSpace,pro_category,pro_adress,pro_summary,pro_floorup,pro_floordown,pro_flag,pro_ExInfo1,pro_ExInfo2,pro_ExInfo3,pro_Phone,pro_ContractNo ");
            strSql.Append(" FROM tg_project");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by pro_id desc");
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" pro_ID,pro_Name,pro_Order,pro_Intro,pro_count,pro_Kinds,pro_Icon,pro_StartTime,pro_FinishTime,pro_Status,pro_PostArticle,pro_PostComms,pro_BuildUnit,pro_ArchArea,pro_FloorNum,pro_StruType,pro_ArchHeight,pro_Serial,pro_DesignUnit,pro_Level,pro_UsedSpace,pro_AllSpace,pro_category,pro_adress,pro_summary,pro_floorup,pro_floordown,pro_flag,pro_ExInfo1,pro_ExInfo2,pro_ExInfo3,pro_Phone,pro_ContractNo ");
            strSql.Append(" FROM tg_project ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM tg_project ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where 1=1 " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCountNew(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM tg_project AT left join cm_project cm on cm.ReferenceSysNo=AT.pro_ID");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where 1=1 " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        ///  数据量小的查询分页
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("select * ");
            sb.Append(" FROM tg_project ");
            if (strWhere.Trim() != "")
            {
                sb.Append(" where 1=1 " + strWhere);
            }
            if (orderby.Trim() != "")
            {
                sb.Append(" Order by " + orderby);
            }
            return DbHelperSQL.Query(sb.ToString(), startIndex, endIndex);
        }

        public DataSet GetListByWhere(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select pro.* ");
            strSql.Append(" from tg_project as pro inner join tg_projectExtendInfo as e on pro.pro_id =e.project_id");
            if (strWhere.Trim() != "")
            {
                strSql.Append("  where " + strWhere.Trim());
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
        /// <summary>
        ///  导出
        /// </summary>
        public DataSet GetListExport(string strWhere)
        {
            string sql = @"select P.*,
				                convert(varchar(10),pro_startTime,120) as qdrq,
				                convert(varchar(10),pro_finishTime,120) as wcrq
				            from tg_project P where 1=1 " + strWhere;
            return DbHelperSQL.Query(sql);
        }
        #endregion  Method
    }
}

