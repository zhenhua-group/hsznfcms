﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references

namespace TG.DAL
{
    /// <summary>
    /// 数据访问类:tg_ProRole
    /// </summary>
    public partial class tg_ProRole
    {
        public tg_ProRole()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("ID", "tg_ProRole");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from tg_ProRole");
            strSql.Append(" where ID=" + ID + " ");
            return DbHelperSQL.Exists(strSql.ToString());
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.tg_ProRole model)
        {
            StringBuilder strSql = new StringBuilder();
            StringBuilder strSql1 = new StringBuilder();
            StringBuilder strSql2 = new StringBuilder();
            if (model.RoleName != null)
            {
                strSql1.Append("RoleName,");
                strSql2.Append("'" + model.RoleName + "',");
            }
            if (model.SubProLevel != null)
            {
                strSql1.Append("SubProLevel,");
                strSql2.Append("'" + model.SubProLevel + "',");
            }
            if (model.StageLevel != null)
            {
                strSql1.Append("StageLevel,");
                strSql2.Append("'" + model.StageLevel + "',");
            }
            if (model.PurposeLevel != null)
            {
                strSql1.Append("PurposeLevel,");
                strSql2.Append("'" + model.PurposeLevel + "',");
            }
            if (model.SpecLevel != null)
            {
                strSql1.Append("SpecLevel,");
                strSql2.Append("'" + model.SpecLevel + "',");
            }
            if (model.Designer != null)
            {
                strSql1.Append("Designer,");
                strSql2.Append("'" + model.Designer + "',");
            }
            if (model.Level3 != null)
            {
                strSql1.Append("Level3,");
                strSql2.Append("'" + model.Level3 + "',");
            }
            if (model.Level4 != null)
            {
                strSql1.Append("Level4,");
                strSql2.Append("'" + model.Level4 + "',");
            }
            if (model.Level5 != null)
            {
                strSql1.Append("Level5,");
                strSql2.Append("'" + model.Level5 + "',");
            }
            strSql.Append("insert into tg_ProRole(");
            strSql.Append(strSql1.ToString().Remove(strSql1.Length - 1));
            strSql.Append(")");
            strSql.Append(" values (");
            strSql.Append(strSql2.ToString().Remove(strSql2.Length - 1));
            strSql.Append(")");
            strSql.Append(";select @@IDENTITY");
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.tg_ProRole model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update tg_ProRole set ");
            if (model.RoleName != null)
            {
                strSql.Append("RoleName='" + model.RoleName + "',");
            }
            if (model.SubProLevel != null)
            {
                strSql.Append("SubProLevel='" + model.SubProLevel + "',");
            }
            else
            {
                strSql.Append("SubProLevel= null ,");
            }
            if (model.StageLevel != null)
            {
                strSql.Append("StageLevel='" + model.StageLevel + "',");
            }
            else
            {
                strSql.Append("StageLevel= null ,");
            }
            if (model.PurposeLevel != null)
            {
                strSql.Append("PurposeLevel='" + model.PurposeLevel + "',");
            }
            else
            {
                strSql.Append("PurposeLevel= null ,");
            }
            if (model.SpecLevel != null)
            {
                strSql.Append("SpecLevel='" + model.SpecLevel + "',");
            }
            else
            {
                strSql.Append("SpecLevel= null ,");
            }
            if (model.Designer != null)
            {
                strSql.Append("Designer='" + model.Designer + "',");
            }
            else
            {
                strSql.Append("Designer= null ,");
            }
            if (model.Level3 != null)
            {
                strSql.Append("Level3='" + model.Level3 + "',");
            }
            else
            {
                strSql.Append("Level3= null ,");
            }
            if (model.Level4 != null)
            {
                strSql.Append("Level4='" + model.Level4 + "',");
            }
            else
            {
                strSql.Append("Level4= null ,");
            }
            if (model.Level5 != null)
            {
                strSql.Append("Level5='" + model.Level5 + "',");
            }
            else
            {
                strSql.Append("Level5= null ,");
            }
            int n = strSql.ToString().LastIndexOf(",");
            strSql.Remove(n, 1);
            strSql.Append(" where ID=" + model.ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tg_ProRole ");
            strSql.Append(" where ID=" + ID + "");
            int rowsAffected = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }		/// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string IDlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from tg_ProRole ");
            strSql.Append(" where ID in (" + IDlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public TG.Model.tg_ProRole GetModel(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1  ");
            strSql.Append(" ID,RoleName,SubProLevel,StageLevel,PurposeLevel,SpecLevel,Designer,Level3,Level4,Level5 ");
            strSql.Append(" from tg_ProRole ");
            strSql.Append(" where ID=" + ID + "");
            TG.Model.tg_ProRole model = new TG.Model.tg_ProRole();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.ID = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["RoleName"] != null && ds.Tables[0].Rows[0]["RoleName"].ToString() != "")
                {
                    model.RoleName = ds.Tables[0].Rows[0]["RoleName"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SubProLevel"] != null && ds.Tables[0].Rows[0]["SubProLevel"].ToString() != "")
                {
                    model.SubProLevel = ds.Tables[0].Rows[0]["SubProLevel"].ToString();
                }
                if (ds.Tables[0].Rows[0]["StageLevel"] != null && ds.Tables[0].Rows[0]["StageLevel"].ToString() != "")
                {
                    model.StageLevel = ds.Tables[0].Rows[0]["StageLevel"].ToString();
                }
                if (ds.Tables[0].Rows[0]["PurposeLevel"] != null && ds.Tables[0].Rows[0]["PurposeLevel"].ToString() != "")
                {
                    model.PurposeLevel = ds.Tables[0].Rows[0]["PurposeLevel"].ToString();
                }
                if (ds.Tables[0].Rows[0]["SpecLevel"] != null && ds.Tables[0].Rows[0]["SpecLevel"].ToString() != "")
                {
                    model.SpecLevel = ds.Tables[0].Rows[0]["SpecLevel"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Designer"] != null && ds.Tables[0].Rows[0]["Designer"].ToString() != "")
                {
                    model.Designer = ds.Tables[0].Rows[0]["Designer"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Level3"] != null && ds.Tables[0].Rows[0]["Level3"].ToString() != "")
                {
                    model.Level3 = ds.Tables[0].Rows[0]["Level3"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Level4"] != null && ds.Tables[0].Rows[0]["Level4"].ToString() != "")
                {
                    model.Level4 = ds.Tables[0].Rows[0]["Level4"].ToString();
                }
                if (ds.Tables[0].Rows[0]["Level5"] != null && ds.Tables[0].Rows[0]["Level5"].ToString() != "")
                {
                    model.Level5 = ds.Tables[0].Rows[0]["Level5"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ID,RoleName,SubProLevel,StageLevel,PurposeLevel,SpecLevel,Designer,Level3,Level4,Level5 ");
            strSql.Append(" FROM tg_ProRole ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ID,RoleName,SubProLevel,StageLevel,PurposeLevel,SpecLevel,Designer,Level3,Level4,Level5 ");
            strSql.Append(" FROM tg_ProRole ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM tg_ProRole ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            /*没有调用此方法，无需修改。Created by Sago*/
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBE() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ID desc");
            }
            strSql.Append(")AS Row, T.*  from tg_ProRole T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        */

        #endregion  Method
    }
}

