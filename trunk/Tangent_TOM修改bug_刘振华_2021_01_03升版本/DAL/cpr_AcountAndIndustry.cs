﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TG.Common.EntityBuilder;
using System.Data.SqlClient;
using TG.DBUtility;

namespace TG.DAL
{
    public class cpr_AcountAndIndustry
    {
        public List<TG.Model.cpr_AcountAndIndustry> GetListAcountIndustry(string unitId, string year, string beginTime, string endTime)
        {
            string sql = @"select t.unit_Name AS 'Unit',isnull(gj,0) as 'LastPublicBuild',isnull(sz,0) as 'LastAffordHouse',
isnull(fdc,0) as 'LastEstate',
isnull(qt,0) as 'LastComprehensive',isnull(zh,0) as 'LastTotal',isnull(gj1,0) as 'NowPublicBuild',
isnull(sz1,0) as 'NowAffordHouse',
isnull(fdc1,0) as 'NowEstate',isnull(qt1,0) as 'NowComprehensive',isnull(zh1,0) as 'NowTotal' from
(select cpr_unit,SUM(gj) as gj,SUM(sz) as sz,SUM(fdc) as fdc,SUM(qt) as qt,SUM(zh) as zh,
SUM(gj1) as gj1,SUM(sz1) as sz1,SUM(fdc1) as fdc1,SUM(qt1) as qt1,SUM(zh1) as zh1 from(
select *,(gj1+sz1+fdc1+qt1) as zh1 from(
select cpr_unit,0 as gj,0 as sz,0 as fdc,0 as qt,0 zh,
isnull(sum(case industry when '公建' then  cpr_Acount end),0) as gj1 , 
isnull(sum(case  industry when '市政' then cpr_Acount end),0) as sz1,
isnull(sum(case 	industry when '房地产' then cpr_Acount end),0) as fdc1,
ISNULL(sum(case when industry<>'公建' and industry<>'市政' and industry<>'房地产' then cpr_Acount end),0) as qt1
 from cm_Coperation where cpr_SignDate between '{3}-01-01' and '{3}-{2}' group by cpr_unit
 ) a
 union all
select *,(gj+sz+fdc+qt) as zh,0,0,0,0,0 from(
select cpr_unit,
isnull(sum(case industry when '公建' then  cpr_Acount end),0) as gj , 
isnull(sum(case  industry when '市政' then cpr_Acount end),0) as sz,
isnull(sum(case 	industry when '房地产' then cpr_Acount end),0) as fdc,
ISNULL(sum(case when industry<>'公建' and industry<>'市政' and industry<>'房地产' then cpr_Acount end),0) as qt
 from cm_Coperation where cpr_SignDate between '{1}-01-01' and '{1}-{2}' group by cpr_unit
 ) b
 ) c group by cpr_unit
 ) d right join tg_unit t on t.unit_Name=d.cpr_Unit where {0} order by t.unit_ID asc
";
            sql = string.Format(sql, unitId, int.Parse(year) - 1, endTime, year);
            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<TG.Model.cpr_AcountAndIndustry> resultList = EntityBuilder<TG.Model.cpr_AcountAndIndustry>.BuilderEntityList(reader);

            return resultList;
        }
    }
}
