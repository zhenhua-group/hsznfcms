﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using TG.DBUtility;//Please add references
using TG.Common.EntityBuilder;
namespace TG.DAL
{
	/// <summary>
	/// 数据访问类:tg_ProjectExtendInfo
	/// </summary>
	public partial class tg_ProjectExtendInfo
	{
		public tg_ProjectExtendInfo()
		{ }
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return DbHelperSQL.GetMaxID("ProExtendInfoid", "tg_ProjectExtendInfo");
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ProExtendInfoid)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select count(1) from tg_ProjectExtendInfo");
			strSql.Append(" where ProExtendInfoid=@ProExtendInfoid");
			SqlParameter[] parameters = {
					new SqlParameter("@ProExtendInfoid", SqlDbType.Int,4)
};
			parameters[0].Value = ProExtendInfoid;

			return DbHelperSQL.Exists(strSql.ToString(), parameters);
		}


		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(TG.Model.tg_ProjectExtendInfo model)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("insert into tg_ProjectExtendInfo(");
			strSql.Append("project_Id,Pro_src,ChgJia,Phone,Project_reletive,Cpr_Acount,Unit,ProjectScale,BuildAddress)");
			strSql.Append(" values (");
			strSql.Append("@project_Id,@Pro_src,@ChgJia,@Phone,@Project_reletive,@Cpr_Acount,@Unit,@ProjectScale,@BuildAddress)");
			strSql.Append(";select @@IDENTITY");
			SqlParameter[] parameters = {
					new SqlParameter("@project_Id", SqlDbType.Int,4),
					new SqlParameter("@Pro_src", SqlDbType.Int,4),
					new SqlParameter("@ChgJia", SqlDbType.VarChar,50),
					new SqlParameter("@Phone", SqlDbType.VarChar,30),
					new SqlParameter("@Project_reletive", SqlDbType.VarChar,500),
					new SqlParameter("@Cpr_Acount", SqlDbType.Decimal,9),
					new SqlParameter("@Unit", SqlDbType.VarChar,100),
					new SqlParameter("@ProjectScale", SqlDbType.Decimal,9),
					new SqlParameter("@BuildAddress", SqlDbType.VarChar,500)};
			parameters[0].Value = model.project_Id;
			parameters[1].Value = model.Pro_src;
			parameters[2].Value = model.ChgJia;
			parameters[3].Value = model.Phone;
			parameters[4].Value = model.Project_reletive;
			parameters[5].Value = model.Cpr_Acount;
			parameters[6].Value = model.Unit;
			parameters[7].Value = model.ProjectScale;
			parameters[8].Value = model.BuildAddress;

			object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(TG.Model.tg_ProjectExtendInfo model)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("update tg_ProjectExtendInfo set ");
			strSql.Append("project_Id=@project_Id,");
			strSql.Append("Pro_src=@Pro_src,");
			strSql.Append("ChgJia=@ChgJia,");
			strSql.Append("Phone=@Phone,");
			strSql.Append("Project_reletive=@Project_reletive,");
			strSql.Append("Cpr_Acount=@Cpr_Acount,");
			strSql.Append("Unit=@Unit,");
			strSql.Append("ProjectScale=@ProjectScale,");
			strSql.Append("BuildAddress=@BuildAddress");
			strSql.Append(" where project_id=@project_Id");
			SqlParameter[] parameters = {
					new SqlParameter("@project_Id", SqlDbType.Int,4),
					new SqlParameter("@Pro_src", SqlDbType.Int,4),
					new SqlParameter("@ChgJia", SqlDbType.VarChar,50),
					new SqlParameter("@Phone", SqlDbType.VarChar,30),
					new SqlParameter("@Project_reletive", SqlDbType.VarChar,500),
					new SqlParameter("@Cpr_Acount", SqlDbType.Decimal,9),
					new SqlParameter("@Unit", SqlDbType.VarChar,100),
					new SqlParameter("@ProjectScale", SqlDbType.Decimal,9),
					new SqlParameter("@BuildAddress", SqlDbType.VarChar,500),
					new SqlParameter("@ProExtendInfoid", SqlDbType.Int,4)};
			parameters[0].Value = model.project_Id;
			parameters[1].Value = model.Pro_src;
			parameters[2].Value = model.ChgJia;
			parameters[3].Value = model.Phone;
			parameters[4].Value = model.Project_reletive;
			parameters[5].Value = model.Cpr_Acount;
			parameters[6].Value = model.Unit;
			parameters[7].Value = model.ProjectScale;
			parameters[8].Value = model.BuildAddress;
			parameters[9].Value = model.ProExtendInfoid;

			int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ProExtendInfoid)
		{

			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from tg_ProjectExtendInfo ");
			strSql.Append(" where ProExtendInfoid=@ProExtendInfoid");
			SqlParameter[] parameters = {
					new SqlParameter("@ProExtendInfoid", SqlDbType.Int,4)
};
			parameters[0].Value = ProExtendInfoid;

			int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string ProExtendInfoidlist)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("delete from tg_ProjectExtendInfo ");
			strSql.Append(" where project_id in (" + ProExtendInfoidlist + ")  ");
			int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public TG.Model.tg_ProjectExtendInfo GetModel(int ProExtendInfoid)
		{

			StringBuilder strSql = new StringBuilder();
			strSql.Append("select  top 1 ProExtendInfoid,project_Id,Pro_src,ChgJia,Phone,Project_reletive,Cpr_Acount,Unit,ProjectScale,BuildAddress from tg_ProjectExtendInfo ");
			strSql.Append(" where project_id=@project_id");
			SqlParameter[] parameters = {
					new SqlParameter("@project_id", SqlDbType.Int,4)
};
			parameters[0].Value = ProExtendInfoid;

			TG.Model.tg_ProjectExtendInfo model = new TG.Model.tg_ProjectExtendInfo();
			DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
			if (ds.Tables[0].Rows.Count > 0)
			{
				if (ds.Tables[0].Rows[0]["ProExtendInfoid"] != null && ds.Tables[0].Rows[0]["ProExtendInfoid"].ToString() != "")
				{
					model.ProExtendInfoid = int.Parse(ds.Tables[0].Rows[0]["ProExtendInfoid"].ToString());
				}
				if (ds.Tables[0].Rows[0]["project_Id"] != null && ds.Tables[0].Rows[0]["project_Id"].ToString() != "")
				{
					model.project_Id = int.Parse(ds.Tables[0].Rows[0]["project_Id"].ToString());
				}
				if (ds.Tables[0].Rows[0]["Pro_src"] != null && ds.Tables[0].Rows[0]["Pro_src"].ToString() != "")
				{
					model.Pro_src = int.Parse(ds.Tables[0].Rows[0]["Pro_src"].ToString());
				}
				if (ds.Tables[0].Rows[0]["ChgJia"] != null && ds.Tables[0].Rows[0]["ChgJia"].ToString() != "")
				{
					model.ChgJia = ds.Tables[0].Rows[0]["ChgJia"].ToString();
				}
				if (ds.Tables[0].Rows[0]["Phone"] != null && ds.Tables[0].Rows[0]["Phone"].ToString() != "")
				{
					model.Phone = ds.Tables[0].Rows[0]["Phone"].ToString();
				}
				if (ds.Tables[0].Rows[0]["Project_reletive"] != null && ds.Tables[0].Rows[0]["Project_reletive"].ToString() != "")
				{
					model.Project_reletive = ds.Tables[0].Rows[0]["Project_reletive"].ToString();
				}
				if (ds.Tables[0].Rows[0]["Cpr_Acount"] != null && ds.Tables[0].Rows[0]["Cpr_Acount"].ToString() != "")
				{
					model.Cpr_Acount = decimal.Parse(ds.Tables[0].Rows[0]["Cpr_Acount"].ToString());
				}
				if (ds.Tables[0].Rows[0]["Unit"] != null && ds.Tables[0].Rows[0]["Unit"].ToString() != "")
				{
					model.Unit = ds.Tables[0].Rows[0]["Unit"].ToString();
				}
				if (ds.Tables[0].Rows[0]["ProjectScale"] != null && ds.Tables[0].Rows[0]["ProjectScale"].ToString() != "")
				{
					model.ProjectScale = decimal.Parse(ds.Tables[0].Rows[0]["ProjectScale"].ToString());
				}
				if (ds.Tables[0].Rows[0]["BuildAddress"] != null && ds.Tables[0].Rows[0]["BuildAddress"].ToString() != "")
				{
					model.BuildAddress = ds.Tables[0].Rows[0]["BuildAddress"].ToString();
				}
				return model;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select ProExtendInfoid,project_Id,Pro_src,ChgJia,Phone,Project_reletive,Cpr_Acount,Unit,ProjectScale,BuildAddress ");
			strSql.Append(" FROM tg_ProjectExtendInfo ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top, string strWhere, string filedOrder)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("select ");
			if (Top > 0)
			{
				strSql.Append(" top " + Top.ToString());
			}
			strSql.Append(" ProExtendInfoid,project_Id,Pro_src,ChgJia,Phone,Project_reletive,Cpr_Acount,Unit,ProjectScale,BuildAddress ");
			strSql.Append(" FROM tg_ProjectExtendInfo ");
			if (strWhere.Trim() != "")
			{
				strSql.Append(" where " + strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "tg_ProjectExtendInfo";
			parameters[1].Value = "ProExtendInfoid";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		public TG.Model.tg_ProjectExtendInfo GetProjectExtendInfo(string whereSql)
		{
			string sql = "select ProExtendInfoid,project_Id,Pro_src,ChgJia,Phone,Project_reletive,Cpr_Acount,Unit,ProjectScale,BuildAddress from tg_ProjectExtendInfo where 1=1 ";
			if (!string.IsNullOrEmpty(whereSql))
			{
				sql += whereSql;
			}
			SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);
			reader.Read();

			TG.Model.tg_ProjectExtendInfo projectExtendInfo = EntityBuilder<TG.Model.tg_ProjectExtendInfo>.BuilderEntity(reader);

			reader.Close();

			return projectExtendInfo;
		}

		#endregion  Method
	}
}

