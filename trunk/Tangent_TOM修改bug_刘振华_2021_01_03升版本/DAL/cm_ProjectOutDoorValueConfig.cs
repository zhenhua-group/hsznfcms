﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using TG.DBUtility;
using TG.Model;

namespace TG.DAL
{
    public class cm_ProjectOutDoorValueConfig
    {
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_ProjectOutDoorValueConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_ProjectOutDoorValueConfig(");
            strSql.Append("type,bulidingPercent,structurePercent,drainPercent,hvacPercent,electricPercent)");
            strSql.Append(" values (");
            strSql.Append("@type,@bulidingPercent,@structurePercent,@drainPercent,@hvacPercent,@electricPercent)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@type", SqlDbType.NVarChar,50),
					new SqlParameter("@bulidingPercent", SqlDbType.Decimal,9),
					new SqlParameter("@structurePercent", SqlDbType.Decimal,9),
					new SqlParameter("@drainPercent", SqlDbType.Decimal,9),
					new SqlParameter("@hvacPercent", SqlDbType.Decimal,9),
					new SqlParameter("@electricPercent", SqlDbType.Decimal,9)};
            parameters[0].Value = model.type;
            parameters[1].Value = model.bulidingPercent;
            parameters[2].Value = model.structurePercent;
            parameters[3].Value = model.drainPercent;
            parameters[4].Value = model.hvacPercent;
            parameters[5].Value = model.electricPercent;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public int Update(TG.Model.cm_ProjectOutDoorValueConfig model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_ProjectOutDoorValueConfig set ");

            strSql.Append("bulidingPercent=@bulidingPercent,");
            strSql.Append("structurePercent=@structurePercent,");
            strSql.Append("drainPercent=@drainPercent,");
            strSql.Append("hvacPercent=@hvacPercent,");
            strSql.Append("electricPercent=@electricPercent");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@bulidingPercent", SqlDbType.Decimal,9),
					new SqlParameter("@structurePercent", SqlDbType.Decimal,9),
					new SqlParameter("@drainPercent", SqlDbType.Decimal,9),
					new SqlParameter("@hvacPercent", SqlDbType.Decimal,9),
					new SqlParameter("@electricPercent", SqlDbType.Decimal,9),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.bulidingPercent;
            parameters[1].Value = model.structurePercent;
            parameters[2].Value = model.drainPercent;
            parameters[3].Value = model.hvacPercent;
            parameters[4].Value = model.electricPercent;
            parameters[5].Value = model.ID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            return rows;
        }


        /// <summary>
        /// 批量更新数据
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateValueDetatil(cm_ProjectDesignProcessValueConfigEntity dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                int status = dataEntity.status;

                string type = dataEntity.type;

                dataEntity.Specialty.ForEach((spe) =>
                {
                    string specialtyName = spe.SpecialtyName;
                    if (spe.SpecialtyName.Contains("："))
                    {
                        specialtyName = spe.SpecialtyName.Replace("：", "");
                    }
                    if (status == 5)
                    {
                        command.CommandText = "update  cm_ProjectOutDoorValueConfig set type='" + type + "', SpecialtyPercent=" + decimal.Parse(spe.SpecialtyValue) + " where Specialty like '%" + specialtyName + "%' and status=" + status + " and IsHaved=" + dataEntity.IsHaved + " and type='"+dataEntity.type.Trim()+"'";
                    }
                    else
                    {
                        command.CommandText = "update  cm_ProjectOutDoorValueConfig set type='" + type + "', SpecialtyPercent=" + decimal.Parse(spe.SpecialtyValue) + " where Specialty like '%" + specialtyName + "%' and status=" + status + "and IsHaved=" + dataEntity.IsHaved + "";
                    }
                    command.ExecuteNonQuery();
                });

                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }
        /// <summary>
        #endregion  Method
    }
}
