﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace TG.DAL
{
    public class ChargeByYear
    {
        public List<TG.Model.ChargeByYear> ListGet(string yearnum, string unitid)
        {
            string sql = @"SELECT *,
isnull
(
  (
   SELECT UnitAllot FROM cm_UnitAllot WHERE AllotYear='{1}'AND UnitID IN 
    (
     SELECT unit_ID FROM tg_unit WHERE unit_Name=c.unit_Name
     )
  ),0
) AS allot FROM
(
SELECT a.unit_name,
isnull(sum(b.cpr_Acount),0) AS cpracount,
isnull(sum(c.Acount),0) AS acount,
isnull(sum(CAST(b.BuildArea AS decimal(18, 6))),0) AS sumarea,
count(b.cpr_Id) AS countid
FROM tg_unit AS a LEFT JOIN cm_Coperation AS b ON a.unit_Name=b.cpr_Unit 
AND b.cpr_SignDate BETWEEN '{1}-01-01' AND '{1}-12-31'
LEFT JOIN cm_ProjectCharge c ON b.cpr_Id =c.cprID {0}
GROUP BY a.unit_Name   
) AS c";
            sql = string.Format(sql, unitid, yearnum);
            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            List<TG.Model.ChargeByYear> list = new List<Model.ChargeByYear>();
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                TG.Model.ChargeByYear model = new Model.ChargeByYear();
                model.unit_Name = item["unit_name"].ToString().Trim();
                model.UnitAllot = Convert.ToInt32(item["allot"]);
                model.Count = Convert.ToDecimal(item["countid"].ToString().Trim());
                model.BuildArea = Convert.ToDecimal(item["sumarea"].ToString().Trim());
                model.HeTonge = Convert.ToDecimal(item["cpracount"].ToString().Trim());
                model.YiWanCHeTonge = Convert.ToDecimal(item["acount"].ToString().Trim());
                model.QianKuan = QianKuan(item["unit_name"].ToString().Trim(), yearnum);
                model.NDShouFei = ShouFei(item["unit_name"].ToString().Trim(), yearnum);
                list.Add(model);
            }
            return list;
        }

        /// <summary>
        ///年度欠款
        /// </summary>
        /// <param name="unitname">单位</param>
        /// <param name="year">年份</param>
        /// <returns></returns>
        private int QianKuan(string unitname, string year)
        {
            string sql = @"SELECT isnull(sum(cm_Coperation.cpr_Acount),0) FROM cm_Coperation WHERE cpr_Unit='{0}'AND cpr_SignDate<'{1}-01-01'";
            sql = string.Format(sql, unitname, year);
            return Convert.ToInt32(TG.DBUtility.DbHelperSQL.GetSingle(sql));
        }
        /// <summary>
        /// 本年度收费
        /// </summary>
        /// <param name="unitname">单位</param>
        /// <param name="year">年份</param>
        /// <returns></returns>
        public int ShouFei(string unitname, string year)
        {
            string sql = @"SELECT isnull(sum(Acount),0) FROM cm_ProjectCharge  WHERE  Status<>'B' AND cprID IN (SELECT cprID FROM cm_Coperation WHERE cpr_Unit='{0}'AND cpr_SignDate<'{1}-01-01')";

            sql = string.Format(sql, unitname, year);
            return Convert.ToInt32(TG.DBUtility.DbHelperSQL.GetSingle(sql));
        }
    }
}
