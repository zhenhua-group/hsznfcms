﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using TG.DBUtility;
using TG.Model;

namespace TG.DAL
{
    public class cm_TranProjectValueAllot
    {

        public cm_TranProjectValueAllot()
        { }
        #region  Method

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(TG.Model.cm_TranProjectValueAllot model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into cm_TranProjectValueAllot(");
            strSql.Append("Pro_ID,TotalCount,AllotCount,TranCount,ItemType,Status,Thedeptallotpercent,Thedeptallotcount,TranPercent,AllotPercent,AllotDate,AllotUser)");
            strSql.Append(" values (");
            strSql.Append("@Pro_ID,@TotalCount,@AllotCount,@TranCount,@ItemType,@Status,@Thedeptallotpercent,@Thedeptallotcount,@TranPercent,@AllotPercent,@AllotDate,@AllotUser)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@Pro_ID", SqlDbType.Int,4),
					new SqlParameter("@TotalCount", SqlDbType.Decimal,18),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,18),
                                        new SqlParameter("@TranCount", SqlDbType.Decimal,18),
                                        new SqlParameter("@ItemType", SqlDbType.NVarChar),
                                        new SqlParameter("@Status", SqlDbType.NVarChar),
                                        new SqlParameter("@Thedeptallotpercent", SqlDbType.Decimal,18),
                                        new SqlParameter("@Thedeptallotcount", SqlDbType.Decimal,18),
                                        new SqlParameter("@TranPercent", SqlDbType.Decimal,18),
                                        new SqlParameter("@AllotPercent", SqlDbType.Decimal,18),
                                        new SqlParameter("@AllotDate", SqlDbType.DateTime),
                                        new SqlParameter("@AllotUser", SqlDbType.Int,4)};
            parameters[0].Value = model.Pro_ID;
            parameters[1].Value = model.TotalCount;
            parameters[2].Value = model.AllotCount;
            parameters[3].Value = model.TranCount;
            parameters[4].Value = model.ItemType;
            parameters[5].Value = model.Status;
            parameters[6].Value = model.Thedeptallotpercent;
            parameters[7].Value = model.Thedeptallotcount;
            parameters[8].Value = model.Tranpercent;
            parameters[9].Value = model.AllotPercent;
            parameters[10].Value = DateTime.Now;
            parameters[11].Value = model.AllotUser;
            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(TG.Model.cm_TranProjectValueAllot model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update cm_TranProjectValueAllot set ");
            strSql.Append("Pro_ID=@Pro_ID,");
            strSql.Append("TotalCount=@TotalCount,");
            strSql.Append("AllotCount=@AllotCount");
            strSql.Append("TranCount=@TranCount");
            strSql.Append("ItemType=@ItemType");
            strSql.Append("Status=@Status");
            strSql.Append(" where ID=@ID");
            SqlParameter[] parameters = {
					new SqlParameter("@Pro_ID", SqlDbType.Int,4),
					new SqlParameter("@TotalCount", SqlDbType.Decimal,18),
					new SqlParameter("@AllotCount", SqlDbType.Decimal,18),
                                        new SqlParameter("@TranCount", SqlDbType.Decimal,18),
                                        new SqlParameter("@ItemType", SqlDbType.NVarChar),
                                        new SqlParameter("@Status", SqlDbType.NVarChar),
					new SqlParameter("@ID", SqlDbType.Int,4)};
            parameters[0].Value = model.Pro_ID;
            parameters[1].Value = model.TotalCount;
            parameters[2].Value = model.AllotCount;
            parameters[3].Value = model.TranCount;
            parameters[4].Value = model.ItemType;
            parameters[5].Value = model.Status;
            parameters[6].Value = model.Id;
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到对象
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public TG.Model.cm_TranProjectValueAllot GetModel(int ID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  *  ");
            strSql.Append(" from cm_TranProjectValueAllot ");
            strSql.Append(" where ID=" + ID + "");
            TG.Model.cm_TranProjectValueAllot model = new TG.Model.cm_TranProjectValueAllot();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.Id = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Pro_ID"] != null && ds.Tables[0].Rows[0]["Pro_ID"].ToString() != "")
                {
                    model.Pro_ID = int.Parse(ds.Tables[0].Rows[0]["Pro_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalCount"] != null && ds.Tables[0].Rows[0]["TotalCount"].ToString() != "")
                {
                    model.TotalCount = decimal.Parse(ds.Tables[0].Rows[0]["TotalCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotCount"] != null && ds.Tables[0].Rows[0]["AllotCount"].ToString() != "")
                {
                    model.AllotCount = decimal.Parse(ds.Tables[0].Rows[0]["AllotCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TranCount"] != null && ds.Tables[0].Rows[0]["TranCount"].ToString() != "")
                {
                    model.TranCount = decimal.Parse(ds.Tables[0].Rows[0]["TranCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ItemType"] != null && ds.Tables[0].Rows[0]["ItemType"].ToString() != "")
                {
                    model.ItemType = ds.Tables[0].Rows[0]["ItemType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AllotPercent"] != null && ds.Tables[0].Rows[0]["AllotPercent"].ToString() != "")
                {
                    model.AllotPercent = decimal.Parse(ds.Tables[0].Rows[0]["AllotPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TranPercent"] != null && ds.Tables[0].Rows[0]["TranPercent"].ToString() != "")
                {
                    model.Tranpercent = decimal.Parse(ds.Tables[0].Rows[0]["TranPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Thedeptallotpercent"] != null && ds.Tables[0].Rows[0]["Thedeptallotpercent"].ToString() != "")
                {
                    model.Thedeptallotpercent = decimal.Parse(ds.Tables[0].Rows[0]["Thedeptallotpercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Thedeptallotcount"] != null && ds.Tables[0].Rows[0]["Thedeptallotcount"].ToString() != "")
                {
                    model.Thedeptallotcount = decimal.Parse(ds.Tables[0].Rows[0]["Thedeptallotcount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotDate"] != null && ds.Tables[0].Rows[0]["AllotDate"].ToString() != "")
                {
                    model.AllotDate = DateTime.Parse(ds.Tables[0].Rows[0]["AllotDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotUser"] != null && ds.Tables[0].Rows[0]["AllotUser"].ToString() != "")
                {
                    model.AllotUser = int.Parse(ds.Tables[0].Rows[0]["AllotUser"].ToString());
                }
                return model;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到对象
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public TG.Model.cm_TranProjectValueAllot GetModelByProID(int proID, int AllotID)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  *  ");
            strSql.Append(" from cm_TranProjectValueAllot ");
            strSql.Append(" where Pro_ID=" + proID + " and AllotID=" + AllotID + "");

            TG.Model.cm_TranProjectValueAllot model = new TG.Model.cm_TranProjectValueAllot();
            DataSet ds = DbHelperSQL.Query(strSql.ToString());
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["ID"] != null && ds.Tables[0].Rows[0]["ID"].ToString() != "")
                {
                    model.Id = int.Parse(ds.Tables[0].Rows[0]["ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Pro_ID"] != null && ds.Tables[0].Rows[0]["Pro_ID"].ToString() != "")
                {
                    model.Pro_ID = int.Parse(ds.Tables[0].Rows[0]["Pro_ID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TotalCount"] != null && ds.Tables[0].Rows[0]["TotalCount"].ToString() != "")
                {
                    model.TotalCount = decimal.Parse(ds.Tables[0].Rows[0]["TotalCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotCount"] != null && ds.Tables[0].Rows[0]["AllotCount"].ToString() != "")
                {
                    model.AllotCount = decimal.Parse(ds.Tables[0].Rows[0]["AllotCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TranCount"] != null && ds.Tables[0].Rows[0]["TranCount"].ToString() != "")
                {
                    model.TranCount = decimal.Parse(ds.Tables[0].Rows[0]["TranCount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ItemType"] != null && ds.Tables[0].Rows[0]["ItemType"].ToString() != "")
                {
                    model.ItemType = ds.Tables[0].Rows[0]["ItemType"].ToString();
                }
                if (ds.Tables[0].Rows[0]["AllotPercent"] != null && ds.Tables[0].Rows[0]["AllotPercent"].ToString() != "")
                {
                    model.AllotPercent = decimal.Parse(ds.Tables[0].Rows[0]["AllotPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["TranPercent"] != null && ds.Tables[0].Rows[0]["TranPercent"].ToString() != "")
                {
                    model.Tranpercent = decimal.Parse(ds.Tables[0].Rows[0]["TranPercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Thedeptallotpercent"] != null && ds.Tables[0].Rows[0]["Thedeptallotpercent"].ToString() != "")
                {
                    model.Thedeptallotpercent = decimal.Parse(ds.Tables[0].Rows[0]["Thedeptallotpercent"].ToString());
                }
                if (ds.Tables[0].Rows[0]["Thedeptallotcount"] != null && ds.Tables[0].Rows[0]["Thedeptallotcount"].ToString() != "")
                {
                    model.Thedeptallotcount = decimal.Parse(ds.Tables[0].Rows[0]["Thedeptallotcount"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotDate"] != null && ds.Tables[0].Rows[0]["AllotDate"].ToString() != "")
                {
                    model.AllotDate = DateTime.Parse(ds.Tables[0].Rows[0]["AllotDate"].ToString());
                }
                if (ds.Tables[0].Rows[0]["AllotUser"] != null && ds.Tables[0].Rows[0]["AllotUser"].ToString() != "")
                {
                    model.AllotUser = int.Parse(ds.Tables[0].Rows[0]["AllotUser"].ToString());
                }
                if (ds.Tables[0].Rows[0]["ActualAllountTime"] != null && ds.Tables[0].Rows[0]["ActualAllountTime"].ToString() != "")
                {
                    model.ActualAllountTime = ds.Tables[0].Rows[0]["ActualAllountTime"].ToString();
                }
                return model;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 得到分页信息条数--查询转暖通-经济所信息
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public DataSet P_cm_TranProjectValuelNoListCount(string strWhere, string type)
        {

            SqlParameter[] parameters = {
                    new SqlParameter("@query",SqlDbType.NVarChar,10000),
                    new SqlParameter("@type",SqlDbType.NVarChar,10)
			};
            parameters[0].Value = strWhere;
            parameters[1].Value = type;
            return DbHelperSQL.RunProcedure("P_cm_TranProjectValuelNoList_Count", parameters, "ds");
        }


        /// <summary>
        /// 查询转暖通-经济所信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_TranProjectValuelNoByPager(int startIndex, int endIndex, string strWhere, string type)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", SqlDbType.Int,4),
					new SqlParameter("@endIndex", SqlDbType.Int,4),
                    new SqlParameter("@query",SqlDbType.NVarChar,10000),
                     new SqlParameter("@type",SqlDbType.NVarChar,10)
			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = strWhere;
            parameters[3].Value = type;
            return DbHelperSQL.RunProcedure("P_cm_TranProjectValuelNoList", parameters, "ds");
        }

        /// <summary>
        /// 查询转暖通-经济所信息
        /// </summary>
        /// <param name="whereSql">WhereSql语句，从 AND 开始。</param>
        /// <returns></returns>
        public DataSet P_cm_TranProjectValuelYesByPager(int startIndex, int endIndex, string strWhere, string type)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@startIndex", SqlDbType.Int,4),
					new SqlParameter("@endIndex", SqlDbType.Int,4),
                    new SqlParameter("@query",SqlDbType.NVarChar,10000),
                     new SqlParameter("@type",SqlDbType.NVarChar,10)
			};
            parameters[0].Value = startIndex;
            parameters[1].Value = endIndex;
            parameters[2].Value = strWhere;
            parameters[3].Value = type;
            return DbHelperSQL.RunProcedure("P_cm_TranProjectValuelYesList", parameters, "ds");
        }

        /// <summary>
        /// 得到分页信息条数--查询转暖通-经济所信息
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public DataSet P_cm_TranProjectValuelYesListCount(string strWhere, string type)
        {

            SqlParameter[] parameters = {
                    new SqlParameter("@query",SqlDbType.NVarChar,10000),
                    new SqlParameter("@type",SqlDbType.NVarChar,10)
			};
            parameters[0].Value = strWhere;
            parameters[1].Value = type;
            return DbHelperSQL.RunProcedure("P_cm_TranProjectValuelYesList_Count", parameters, "ds");
        }

        /// <summary>
        /// 审核人查看审核记录
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetTranProAuditedUser(int speID, string type, int allotID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                        select a.ItemType,a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID ,a.DesignPercent,AuditUser,
	                   cast(round(a.DesignCount,0) as int) DesignCount ,a.SpecialtyHeadPercent,cast(round(a.SpecialtyHeadCount,0) as int) SpecialtyHeadCount,
	                    a.AuditPercent,cast(round(a.AuditCount,0) as int)as AuditCount,a.ProofreadPercent,cast(round(a.ProofreadCount,0) as int) as ProofreadCount
	                   ,IsExternal
                         from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                    where ProID=@ProID
                       and TranType=@TranType
                       and AllotID=@AllotID and IsExternal='0'
	                    union all
                       select a.ItemType,a.mem_ID,b.mem_Name+'(外聘)' as mem_Name,c.spe_Name,c.spe_ID ,a.DesignPercent,AuditUser,
	                   cast(round(a.DesignCount,0) as int) DesignCount ,a.SpecialtyHeadPercent,cast(round(a.SpecialtyHeadCount,0) as int) SpecialtyHeadCount,
	                    a.AuditPercent,cast(round(a.AuditCount,0) as int)as AuditCount,a.ProofreadPercent,cast(round(a.ProofreadCount,0) as int) as ProofreadCount
	                    ,IsExternal
                       from cm_ProjectValueByMemberDetails a
	                    join cm_externalMember b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                    where ProID=@ProID
                       and TranType=@TranType
                       and AllotID=@AllotID and IsExternal='1'
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@ProID", SqlDbType.Int,4),
                     new SqlParameter("@TranType", SqlDbType.NVarChar),
                      new SqlParameter("@AllotID", SqlDbType.Int,4)
			};

            parameters[0].Value = speID;
            parameters[1].Value = type;
            parameters[2].Value = allotID;
            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }
        /// <summary>
        /// 审核人查看审核记录
        /// </summary>
        /// <param name="allotID"></param>
        /// <returns></returns>
        public DataSet GetDistinctTranProAuditedUser(int speID, string type, int allotID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@" select distinct * from (
                        select a.mem_ID,b.mem_Name,c.spe_Name,c.spe_ID 
	                    from cm_ProjectValueByMemberDetails a
	                    join tg_member b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                    where ProID=@ProID
                       and TranType=@TranType
                       and AllotID=@AllotID and IsExternal='0'
	                    union all
                       select a.mem_ID,b.mem_Name+'(外聘)' as mem_Name,c.spe_Name,c.spe_ID 
	                    from cm_ProjectValueByMemberDetails a
	                    join cm_externalMember b
	                    on a.mem_ID=b.mem_ID
	                    join tg_speciality c
	                    on b.mem_Speciality_ID=c.spe_ID
	                    where ProID=@ProID
                       and TranType=@TranType
                       and AllotID=@AllotID and IsExternal='1' ) temp
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@ProID", SqlDbType.Int,4),
                     new SqlParameter("@TranType", SqlDbType.NVarChar),
                      new SqlParameter("@AllotID", SqlDbType.Int,4)
			};

            parameters[0].Value = speID;
            parameters[1].Value = type;
            parameters[2].Value = allotID;
            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 得到人员信息
        /// </summary>
        /// <param name="speName"></param>
        /// <param name="pro_ID"></param>
        /// <returns></returns>
        public DataSet GetPlanMember(string speName, int pro_ID)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append(@"
                        SELECT M.mem_ID, mem_Name, mem_RoleIDs ,r.mem_isDesigner, s.spe_Name,s.spe_ID,m.mem_Principalship_ID
		              FROM   dbo.tg_relation R
		               INNER JOIN tg_member M
		               ON R.mem_ID=M.mem_ID
		               join tg_speciality  s on s.spe_ID = m.mem_Speciality_ID
		               WHERE   r.pro_ID= @pro_ID and  s.spe_Name=@speName
                        and  r.mem_RoleIDs <>'1'
                           and r.mem_RoleIDs <>'5'
                           and r.mem_RoleIDs <>'6' 
                        UNION     
                        --  审定 助理 设总 参与 设计人员            
                        SELECT DISTINCT M.mem_ID, mem_Name, mem_RoleIDs ,r.mem_isDesigner, s.spe_Name,s.spe_ID,m.mem_Principalship_ID
		                   FROM   dbo.tg_relation R
		                   INNER JOIN tg_member M
		                   ON R.mem_ID=M.mem_ID
		                   join tg_speciality  s on s.spe_ID = m.mem_Speciality_ID
		                   WHERE   r.pro_ID= @pro_ID and s.spe_Name=@speName
                           and( r.mem_RoleIDs <>'1'
                           or r.mem_RoleIDs <>'5'
                           or r.mem_RoleIDs <>'6'
                            ) and r.mem_isDesigner=1       
                       "
                        );
            SqlParameter[] parameters = {
                    new SqlParameter("@pro_ID", SqlDbType.Int,4),
					new SqlParameter("@speName", SqlDbType.NVarChar,50)
			};
            parameters[0].Value = pro_ID;
            parameters[1].Value = speName;

            return DbHelperSQL.Query(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 新增产值分配人员明细信息
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int InsertProjectAllotValueByMemberDetatil(TranProjectValueAllotByMemberEntity dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                if (dataEntity.ItemType == "nts")
                {
                    //新增
                    command.CommandText = "insert into cm_TranProjectValueAllot(Pro_ID,AllotID,TotalCount,AllotCount,TranCount,ItemType,Thedeptallotpercent,Thedeptallotcount,AllotPercent,TranPercent,ActualAllountTime)values(" + dataEntity.ProNo + "," + dataEntity.AllotID + "," + dataEntity.TotalCount + "," + dataEntity.AllotCount + "," + dataEntity.TranCount + ",'" + dataEntity.ItemType + "'," + dataEntity.Thedeptallotpercent + "," + dataEntity.Thedeptallotcount + "," + dataEntity.AllotPercent + "," + dataEntity.TranPercent + ",'" + dataEntity.ActualAllountTime + "')";
                    command.ExecuteNonQuery();
                }
                else
                {
                    //新增
                    command.CommandText = "insert into cm_TranjjsProjectValueAllot(Pro_ID,AllotID,TotalCount,AllotCount,TranCount,ItemType)values(" + dataEntity.ProNo + "," + dataEntity.AllotID + "," + dataEntity.TotalCount + "," + dataEntity.AllotCount + "," + dataEntity.TranCount + ",'" + dataEntity.ItemType + "')";
                    command.ExecuteNonQuery();
                }

                //新规项目各设计阶段产值分配
                dataEntity.ValueByMember.ForEach((stage) =>
                {
                    decimal programPercent = 0;
                    if (!string.IsNullOrEmpty(stage.DesignPercent))
                    {
                        programPercent = decimal.Parse(stage.DesignPercent);
                    }
                    decimal preliminaryPercent = 0;
                    if (!string.IsNullOrEmpty(stage.SpecialtyHeadPercent))
                    {
                        preliminaryPercent = decimal.Parse(stage.SpecialtyHeadPercent);
                    }

                    decimal WorkDrawPercent = 0;
                    if (!string.IsNullOrEmpty(stage.AuditPercent))
                    {
                        WorkDrawPercent = decimal.Parse(stage.AuditPercent);
                    }

                    decimal LateStagePercent = 0;
                    if (!string.IsNullOrEmpty(stage.ProofreadPercent))
                    {
                        LateStagePercent = decimal.Parse(stage.ProofreadPercent);
                    }
                    decimal designCount = 0;

                    designCount = stage.DesignCount;
                    command.CommandText = "insert into cm_ProjectValueByMemberDetails(ProID,AllotID,mem_ID,AuditUser,DesignPercent,DesignCount,SpecialtyHeadPercent,SpecialtyHeadCount,AuditPercent,AuditCount,ProofreadPercent,ProofreadCount,SecondValue,TranType,Status,IsExternal)values(" + dataEntity.ProNo + "," + dataEntity.AllotID + "," + stage.mem_ID + "," + dataEntity.AuditUser + "," + programPercent + "," + designCount + "," + preliminaryPercent + "," + stage.SpecialtyHeadCount + "," + WorkDrawPercent + "," + stage.AuditCount + "," + LateStagePercent + "," + stage.ProofreadCount + "," + 1 + ",'" + dataEntity.ItemType + "','A','" + stage.IsExternal + "')";
                    command.ExecuteNonQuery();
                });


                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        // <summary>
        /// 更新产值分配人员明细信息-转暖通所
        /// </summary>
        /// <param name="dataEntity"></param>
        /// <returns></returns>
        public int UpdateProjectAllotValueByMemberDetatil(TranProjectValueAllotByMemberEntity dataEntity)
        {
            SqlTransaction transaction = null;
            SqlConnection conn = new SqlConnection { ConnectionString = DbHelperSQL.connectionString };
            try
            {
                SqlCommand command = new SqlCommand();

                command.Connection = conn;

                conn.Open();

                transaction = command.Connection.BeginTransaction();

                command.Transaction = transaction;

                command.CommandText = "delete from  cm_TranProjectValueAllot where Pro_ID=" + dataEntity.ProNo + " and AllotID=" + dataEntity.AllotID + "";
                command.ExecuteNonQuery();

                command.CommandText = "delete from  cm_ProjectValueByMemberDetails where ProID=" + dataEntity.ProNo + " and AllotID=" + dataEntity.AllotID + " and TranType='nts'";
                command.ExecuteNonQuery();
                //新增
                command.CommandText = "insert into cm_TranProjectValueAllot(Pro_ID,AllotID,TotalCount,AllotCount,TranCount,ItemType,Thedeptallotpercent,Thedeptallotcount,AllotPercent,TranPercent,ActualAllountTime)values(" + dataEntity.ProNo + "," + dataEntity.AllotID + "," + dataEntity.TotalCount + "," + dataEntity.AllotCount + "," + dataEntity.TranCount + ",'" + dataEntity.ItemType + "'," + dataEntity.Thedeptallotpercent + "," + dataEntity.Thedeptallotcount + "," + dataEntity.AllotPercent + "," + dataEntity.TranPercent + ",'" + dataEntity.ActualAllountTime + "')";
                command.ExecuteNonQuery();

                //新规项目各设计阶段产值分配
                dataEntity.ValueByMember.ForEach((stage) =>
                {
                    decimal programPercent = 0;
                    if (!string.IsNullOrEmpty(stage.DesignPercent))
                    {
                        programPercent = decimal.Parse(stage.DesignPercent);
                    }
                    decimal preliminaryPercent = 0;
                    if (!string.IsNullOrEmpty(stage.SpecialtyHeadPercent))
                    {
                        preliminaryPercent = decimal.Parse(stage.SpecialtyHeadPercent);
                    }

                    decimal WorkDrawPercent = 0;
                    if (!string.IsNullOrEmpty(stage.AuditPercent))
                    {
                        WorkDrawPercent = decimal.Parse(stage.AuditPercent);
                    }

                    decimal LateStagePercent = 0;
                    if (!string.IsNullOrEmpty(stage.ProofreadPercent))
                    {
                        LateStagePercent = decimal.Parse(stage.ProofreadPercent);
                    }
                    decimal designCount = 0;

                    designCount = stage.DesignCount;
                    command.CommandText = "insert into cm_ProjectValueByMemberDetails(ProID,AllotID,mem_ID,AuditUser,DesignPercent,DesignCount,SpecialtyHeadPercent,SpecialtyHeadCount,AuditPercent,AuditCount,ProofreadPercent,ProofreadCount,SecondValue,TranType,Status,IsExternal)values(" + dataEntity.ProNo + "," + dataEntity.AllotID + "," + stage.mem_ID + "," + dataEntity.AuditUser + "," + programPercent + "," + designCount + "," + preliminaryPercent + "," + stage.SpecialtyHeadCount + "," + WorkDrawPercent + "," + stage.AuditCount + "," + LateStagePercent + "," + stage.ProofreadCount + "," + 1 + ",'" + dataEntity.ItemType + "','A','" + stage.IsExternal + "')";
                    command.ExecuteNonQuery();
                });


                transaction.Commit();
                return 1;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return 0;
            }
            finally
            {
                conn.Close();
                transaction.Dispose();
            }
        }

        #endregion
    }
}
