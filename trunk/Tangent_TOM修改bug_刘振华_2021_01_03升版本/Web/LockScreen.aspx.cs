﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web
{
    public partial class LockScreen : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Session["memid"] = null;
                //Session["memlogin"] = null;
                //Session["unitid"] = null;
                string sql = "SELECT mem_ID FROM tg_member WHERE mem_Login='" + UserSysLoginName + "'";
                object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
                int memId = 1;
                if (o != null)
                {
                    if (int.TryParse(o.ToString(), out memId))
                    {
                        TG.Model.cm_EleSign es = new TG.BLL.cm_EleSign().GetModel2(memId);
                        if (es != null)
                        {
                            this.lbl_img.ImageUrl = es.elesign;
                        }
                    }

                }

            }
        }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserSysName
        {
            get
            {
                return Convert.ToString(Request.QueryString["memname"] ?? "");
            }
        }
        /// <summary>
        /// 登录名
        /// </summary>
        public string UserSysLoginName
        {
            get
            {
                return Convert.ToString(Request.QueryString["memlogin"] ?? "");
            }
        }
        /// <summary>
        /// 返回Url
        /// </summary>
        public string BackUrl
        {
            get
            {
                return "mainpage/WelcomePage.aspx";
            }
        }
    }
}