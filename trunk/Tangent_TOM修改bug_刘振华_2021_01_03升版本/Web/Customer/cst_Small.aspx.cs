﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace TG.Web.Customer
{
    public partial class cst_Small : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }

        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //浏览权限控制
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND ( InsertUserID =" + UserSysNo + ")");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND ( InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + "))");
            }
        }
        //查询客户信息
        protected void btn_Search_Click(object sender, ImageClickEventArgs e)
        {
            BindData();
        }
        public void BindData()
        {
            string strWhere = "";
            TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();
            //高级查询
            if (this.txt_cstName.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_cstName.Text.Trim());
                strWhere += " AND C.Cst_Name LIKE '%" + keyname + "%' ";
            }
            if (this.drp_cstType.SelectedIndex != 0)
            {
                strWhere += " AND CX.Type =" + this.drp_cstType.SelectedItem.Value + " ";
            }
            if (this.txt_cstCity.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_cstCity.Text.Trim());
                strWhere += " AND CX.City LIKE '%" + keyname + "%' ";
            }
            if (this.txt_cstLian.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_cstLian.Text.Trim());
                strWhere += " AND C.LinkMan LIKE '%" + keyname + "%' ";
            }
            if (this.txt_cstprince.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_cstprince.Text.Trim());
                strWhere += " AND CX.Province LIKE '%" + keyname + "%' ";
            }

            if (strWhere != "")
            {
                StringBuilder sb = new StringBuilder(strWhere);
                //检查权限
                GetPreviewPowerSql(ref sb);
                //所有记录数
                this.AspNetPager1.RecordCount = int.Parse(bll.GetListPageProcCount(sb.ToString()).ToString());
                gridView.DataSource = bll.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
                gridView.DataBind();
            }
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindData();
        }
    }
}
