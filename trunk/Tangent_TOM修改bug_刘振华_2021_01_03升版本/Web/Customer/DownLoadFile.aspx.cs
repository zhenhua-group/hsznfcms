﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using TG.Common;

namespace TG.Web.Customer
{
    public partial class DownLoadFile : PageBase
    {
        /// <summary>
        /// 附件系统号
        /// </summary>
        public int SysNo
        {
            get
            {
                int sysNo = 0;
                int.TryParse(Request["SysNo"], out sysNo);
                return sysNo;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            TG.BLL.cm_AttachInfo acttachInfoBLL = new TG.BLL.cm_AttachInfo();
            TG.Model.cm_AttachInfo attachInfo = acttachInfoBLL.GetModel(SysNo);
            string fileName = attachInfo.FileName;
            string encodingFileName = System.Web.HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8);
            string filePath = HttpContext.Current.Server.MapPath(@"..\Attach_User\filedata\" + attachInfo.FileUrl);

            if (System.IO.File.Exists(filePath))
            {
                Response.ContentType = "application/x-zip-compressed";
                //by long 20130516
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Server.UrlEncode(attachInfo.FileName));
                Response.TransmitFile(filePath);
            }
            else
            {
                MessageBox.Show(this, "文件不存在或登录超时！");
            }
        }
    }
}
