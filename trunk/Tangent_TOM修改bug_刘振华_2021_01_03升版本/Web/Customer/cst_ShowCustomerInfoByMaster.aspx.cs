﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Words;
using TG.Common;
using System.Data;
using System.Text;

namespace TG.Web.Customer
{
    public partial class cst_ShowCustomerInfoByMaster : PageBase
    {
        protected TG.BLL.cm_CustomerExtendInfo cetbll = new TG.BLL.cm_CustomerExtendInfo();

        protected void Page_Load(object sender, EventArgs e)
        {
            //登录用户
            this.hid_parentid.Value = UserShortName;
            if (!IsPostBack)
            {
                string str_cstid = Request.QueryString["Cst_Id"] ?? "";
                if (str_cstid != "" && str_cstid.Length < 11)
                {
                    //保存全局
                    ViewState["Cst_ID"] = str_cstid;
                    //基本信息
                    ShowInfo(int.Parse(str_cstid));
                    //扩展信息
                    ShowExtendInfo(int.Parse(str_cstid));
                    //附件
                    BindAttachInfo();
                    //联系人
                    BindContactPerson();
                    //合同
                    BindContact(int.Parse(str_cstid));
                    //满意度
                    BindSatisfact();
                    this.hid_cstid.Value = str_cstid;
                }
            }
            else
            {
                //导出val
                OutPutValue();
            }
        }

        private void OutPutValue()
        {
            TG.BLL.cm_CustomerInfo bllinfo = new TG.BLL.cm_CustomerInfo();
            TG.BLL.cm_CustomerExtendInfo custbll = new TG.BLL.cm_CustomerExtendInfo();
            string custId = Request.QueryString["Cst_Id"];
            //得到所要导出的对象
            TG.Model.cm_CustomerInfo model = bllinfo.GetModel(Convert.ToInt32(custId));
            List<TG.Model.cm_CustomerExtendInfo> modelcustlist = custbll.GetModelList("Cst_Id=" + custId);
            TG.Model.cm_CustomerExtendInfo modelcust = null;
            foreach (var item in modelcustlist)
            {
                modelcust = item;
            }
            //公共类。给模板的书签增加值
            string tmppath = Server.MapPath("~/TemplateWord/ReportCustomer.doc");
            Document doc = new Document(tmppath); //载入模板

            if (doc.Range.Bookmarks["Cst_No"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Cst_No"];
                mark.Text = model.Cst_No == null ? "" : model.Cst_No;
            }

            if (doc.Range.Bookmarks["Cst_Name"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Cst_Name"];
                mark.Text = model.Cst_Name == null ? "" : model.Cst_Name;
            }

            if (doc.Range.Bookmarks["Cst_Brief"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Cst_Brief"];
                mark.Text = model.Cst_Brief == null ? "" : model.Cst_Brief;
            }

            if (doc.Range.Bookmarks["Cpy_Fax"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Cpy_Fax"];
                mark.Text = model.Cpy_Fax == null ? "" : model.Cpy_Fax;
            }

            if (doc.Range.Bookmarks["Cpy_Address"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Cpy_Address"];
                mark.Text = model.Cpy_Address == null ? "" : model.Cpy_Address;
            }

            if (doc.Range.Bookmarks["Code"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Code"];
                mark.Text = model.Code == null ? "" : model.Code.ToString();
            }

            if (doc.Range.Bookmarks["Cpy_Phone"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Cpy_Phone"];
                mark.Text = model.Cpy_Phone == null ? "" : model.Cpy_Phone;
            }

            if (doc.Range.Bookmarks["Linkman"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Linkman"];
                mark.Text = model.Linkman == null ? "" : model.Linkman;
            }


            if (doc.Range.Bookmarks["BankAccountNo"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BankAccountNo"];
                mark.Text = modelcust.BankAccountNo == null ? "" : modelcust.BankAccountNo;
            }

            if (doc.Range.Bookmarks["BankName"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BankName"];
                mark.Text = modelcust.BankName == null ? "" : modelcust.BankName;
            }

            if (doc.Range.Bookmarks["BranchPart"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BranchPart"];
                mark.Text = modelcust.BranchPart == null ? "" : modelcust.BranchPart;
            }

            if (doc.Range.Bookmarks["City"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["City"];
                mark.Text = modelcust.City == null ? "" : modelcust.City;
            }

            if (doc.Range.Bookmarks["Country"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Country"];
                mark.Text = modelcust.Country == null ? "" : modelcust.Country;
            }

            if (doc.Range.Bookmarks["Cpy_Email"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Cpy_Email"];
                mark.Text = modelcust.Email == null ? "" : modelcust.Email;
            }

            if (doc.Range.Bookmarks["Cst_EnglishName"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Cst_EnglishName"];
                mark.Text = modelcust.Cst_EnglishName == null ? "" : modelcust.Cst_EnglishName;
            }

            if (doc.Range.Bookmarks["FadingDaibiao"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["FadingDaibiao"];
                mark.Text = modelcust.LawPerson == null ? "" : modelcust.LawPerson;
            }

            if (doc.Range.Bookmarks["IsPartner"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["IsPartner"];
                mark.Text = modelcust.IsPartner.ToString() == "1" ? "合作" : "不合作";
            }

            if (doc.Range.Bookmarks["Profession"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Profession"];
                mark.Text = GetProfession(modelcust.Profession.ToString());
            }

            if (doc.Range.Bookmarks["Province"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Province"];
                mark.Text = modelcust.Province == null ? "" : modelcust.Province;
            }

            if (doc.Range.Bookmarks["Qinmidu"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Qinmidu"];
                mark.Text = GetProfession(modelcust.CloseLeve.ToString());
            }

            if (doc.Range.Bookmarks["QiyeDaima"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["QiyeDaima"];
                mark.Text = modelcust.Cpy_Code == null ? "" : modelcust.Cpy_Code;
            }

            if (doc.Range.Bookmarks["Remark"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Remark"];
                mark.Text = modelcust.Remark == null ? "" : modelcust.Remark;
            }

            if (doc.Range.Bookmarks["RelationDepartment"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["RelationDepartment"];
                mark.Text = modelcust.RelationDepartment == null ? "" : modelcust.RelationDepartment;
            }

            if (doc.Range.Bookmarks["TaxAccountNo"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TaxAccountNo"];
                mark.Text = modelcust.TaxAccountNo == null ? "" : modelcust.TaxAccountNo;
            }

            if (doc.Range.Bookmarks["Tyoe"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Tyoe"];
                mark.Text = GetProfession(modelcust.Type.ToString());
            }

            if (doc.Range.Bookmarks["Xinyongjibie"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Xinyongjibie"];
                mark.Text = GetProfession(modelcust.CreditLeve.ToString());
            }

            if (doc.Range.Bookmarks["UpdateBy"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["UpdateBy"];
                mark.Text = CommCoperation.GetEasyTime(Convert.ToDateTime(modelcust.LastUpdate));
            }


            doc.Save(model.Cst_No.Trim() + ".doc", SaveFormat.Doc, SaveType.OpenInWord, Response);  //保存为doc，并打开
        }
        //返回用户标示
        public string GetUserFlag()
        {
            return UserSysNo.ToString();
        }
        // 返回文件夹ID 
        public string GetParentID()
        {
            return this.hid_parentid.Value;
        }
        // 显示客户信息
        private void ShowInfo(int Cst_Id)
        {
            TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();
            TG.Model.cm_CustomerInfo model = bll.GetModel(Cst_Id);
            if (model != null)
            {
                //客户基本信息
                this.txtCst_No.Text = model.Cst_No == null ? "" : model.Cst_No.Trim();
                this.txtCst_Brief.Text = model.Cst_Brief == null ? "" : model.Cst_Brief.Trim();
                this.txtCst_Name.Text = model.Cst_Name == null ? "" : model.Cst_Name;
                this.txtCpy_Address.Text = model.Cpy_Address == null ? "" : model.Cpy_Address.Trim();
                this.txtCode.Text = model.Code ?? "";
                this.txtLinkman.Text = model.Linkman == null ? "" : model.Linkman;
                this.txtCpy_Phone.Text = model.Cpy_Phone == null ? "" : model.Cpy_Phone;
                this.txtCpy_Fax.Text = model.Cpy_Fax == null ? "" : model.Cpy_Fax;
            }
        }
        // 获取客户ID
        public string getCst_Id()
        {
            return Convert.ToString(ViewState["Cst_ID"] ?? "");
        }
        // 显示扩展信息
        protected void ShowExtendInfo(int Cst_Id)
        {
            List<TG.Model.cm_CustomerExtendInfo> models = cetbll.GetModelList("Cst_Id=" + Cst_Id);
            if (models.Count > 0)
            {
                TG.Model.cm_CustomerExtendInfo model = models[0];
                TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
                this.txtCst_EnglishName.Text = model.Cst_EnglishName;
                this.lbIsPartner.Text = model.IsPartner == 1 ? "是" : "否";
                this.txtCountry.Text = model.Country;
                this.txtProvince.Text = model.Province;
                this.txtCity.Text = model.City;
                DataSet type = dic.GetList("id=" + model.Type.ToString());
                if (type.Tables.Count > 0)
                {
                    if (type.Tables[0].Rows.Count > 0)
                    {
                        this.lbl_Type.Text = type.Tables[0].Rows[0]["dic_name"].ToString();
                    }
                }
                DataSet Profession = dic.GetList("id=" + model.Profession.ToString());
                if (Profession.Tables.Count > 0)
                {
                    if (Profession.Tables[0].Rows.Count > 0)
                    {
                        this.lbl_Profession.Text = Profession.Tables[0].Rows[0]["dic_name"].ToString();
                    }
                }

                this.txtBranchPart.Text = model.BranchPart;
                this.txtEmail.Text = model.Email;
                this.txtCpy_PrincipalSheet.Text = model.Cpy_PrincipalSheet;
                this.txtLawPerson.Text = model.LawPerson;
                this.txtRelationDepartment.Text = model.RelationDepartment;
                this.txtRemark.Text = model.Remark;
                this.txtTaxAccountNo.Text = model.TaxAccountNo;
                this.txtBankName.Text = model.BankName;
                this.txtBankAccountNo.Text = model.BankAccountNo == null ? "" : model.BankAccountNo.ToString();
                DataSet CloseLeve = dic.GetList("id=" + model.CloseLeve.ToString());
                if (CloseLeve.Tables.Count > 0)
                {
                    if (CloseLeve.Tables[0].Rows.Count > 0)
                    {
                        this.lbl_CloseLeve.Text = CloseLeve.Tables[0].Rows[0]["dic_name"].ToString();
                    }
                }
                this.txtCpy_Code.Text = model.Cpy_Code == null ? "" : model.Cpy_Code.Trim().ToString();
                this.txtCreateRelationTime.Text = model.CreateRelationTime == null ? "" : string.Format("{0:d}", model.CreateRelationTime);
                DataSet creditLeve = dic.GetList("id=" + model.CreditLeve.ToString());
                if (creditLeve.Tables.Count > 0)
                {
                    if (creditLeve.Tables[0].Rows.Count > 0)
                    {
                        this.lbl_CreditLeve.Text = creditLeve.Tables[0].Rows[0]["dic_name"].ToString();
                    }
                }
                //开户银行大额联行号
                // this.txtBankBigNo.Text = model.BankBigNo == null ? "" : model.BankBigNo.ToString();
                this.txtLawPerson.Text = model.LawPerson;
                ViewState["Cstext_ID"] = model.Cst_ExtendInfoId;
            }
            else
            {
                ViewState["Cstext_ID"] = null;
            }
        }
        //绑定附件
        public void BindAttachInfo()
        {
            string str_cstid = Convert.ToString(ViewState["Cst_ID"] ?? "0");
            BindAttachInfo(int.Parse(str_cstid));
        }
        //绑定附件查询ID
        public void BindAttachInfo(int cst_Id)
        {
            TG.BLL.cm_AttachInfo attachbll = new TG.BLL.cm_AttachInfo();
            StringBuilder strWhere = new StringBuilder();
            strWhere.AppendFormat("Cst_Id={0}", cst_Id);
            DataSet ds = attachbll.GetList(strWhere.ToString());
            this.gv_Attach.DataSource = ds;
            this.gv_Attach.DataBind();
        }
        // 删除附件
        protected bool DeleteAttachInfo(string filePath)
        {
            bool isDelete = false;
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
                isDelete = true;
            }
            return isDelete;
        }
        // 下载附件
        public bool DowloadFile(string fileName, string loginId)
        {
            string filePath = HttpContext.Current.Server.MapPath(@"..\Attach_User\filedata\" + loginId + "\\" + fileName);
            if (System.IO.File.Exists(filePath))
            {
                Response.ContentType = "application/x-zip-compressed";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);
                Response.TransmitFile(filePath);
                return true;
            }
            else
            {
                Response.Clear();
                return false;
            }
        }
        //绑定联系人
        public void BindContactPerson()
        {
            string str_cstid = Convert.ToString(ViewState["Cst_ID"] ?? "0");
            BindContactPerson(int.Parse(str_cstid));
        }
        public void BindContactPerson(int cst_Id)
        {
            TG.BLL.cm_ContactPersionInfo cpbll = new TG.BLL.cm_ContactPersionInfo();
            DataSet ds = new DataSet();
            StringBuilder strWhere = new StringBuilder();
            strWhere.AppendFormat("Cst_Id={0}", cst_Id);
            ds = cpbll.GetList(strWhere.ToString());
            this.gv_contactPerson.DataSource = ds;
            this.gv_contactPerson.DataBind();
        }
        // 合同
        public void BindContact()
        {
            string str_cstid = Convert.ToString(ViewState["Cst_ID"] ?? "0");
            BindContact(int.Parse(str_cstid));
        }
        public void BindContact(int cst_Id)
        {
            DataSet coperationSet = new TG.BLL.cm_Coperation().GetList(" cst_Id=" + cst_Id);
            this.gv_rContract.DataSource = coperationSet;
            this.gv_rContract.DataBind();

        }
        //绑定满意度
        public void BindSatisfact()
        {
            string str_cstid = Convert.ToString(ViewState["Cst_ID"] ?? "0");
            BindSatisfact(int.Parse(str_cstid));
        }
        public void BindSatisfact(int cst_Id)
        {
            TG.BLL.cm_SatisfactionInfo sfbll = new TG.BLL.cm_SatisfactionInfo();
            DataSet ds = new DataSet();
            StringBuilder strWhere = new StringBuilder();
            strWhere.AppendFormat("Cst_Id={0}", cst_Id);
            ds = sfbll.GetList(strWhere.ToString());
            this.gv_satisfaction.DataSource = ds;
            this.gv_satisfaction.DataBind();
        }
        protected void gv_satisfaction_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSat_Leve = (Label)e.Row.FindControl("lblSat_Leve");
                string levValue = lblSat_Leve.Text;
                Label schway = (Label)e.Row.FindControl("lblSch_Way");
                string wayValue = schway.Text;
                switch (levValue)
                {
                    case "1":
                        lblSat_Leve.Text = "满意";
                        break;
                    case "2":
                        lblSat_Leve.Text = "非常满意";
                        break;
                    case "3":
                        lblSat_Leve.Text = "不满意";
                        break;
                    case "4":
                        lblSat_Leve.Text = "非常不满意";
                        break;

                    default:
                        break;
                }
                switch (wayValue)
                {
                    case "1":
                        schway.Text = "问卷调查";
                        break;
                    case "2":
                        schway.Text = "统计调查";
                        break;
                    case "3":
                        schway.Text = "询问调查";
                        break;
                    case "4":
                        schway.Text = "电话调查";
                        break;
                    default:
                        break;
                }
            }
        }
        //格式化数据
        protected void gv_Attach_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //文件大小
                string fsize = e.Row.Cells[2].Text;
                if (fsize.Trim() != "&nbsp;" && fsize.Trim() != "")
                {
                    fsize = Convert.ToString(int.Parse(fsize) / 1024) + "KB";
                }
                e.Row.Cells[2].Text = fsize;
                //用户
                string fuserid = e.Row.Cells[4].Text;
                if (fuserid.Trim() != "" && fuserid.Trim() != "&nbsp;")
                {
                    TG.Model.tg_member mem_model = new TG.BLL.tg_member().GetModel(int.Parse(fuserid));
                    if (mem_model != null)
                    {
                        fuserid = mem_model.mem_Name;
                    }
                }
                e.Row.Cells[4].Text = fuserid;
            }
        }

        //protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        //{


        //    TG.BLL.cm_CustomerInfo bllinfo = new TG.BLL.cm_CustomerInfo();
        //    TG.BLL.cm_CustomerExtendInfo custbll = new TG.BLL.cm_CustomerExtendInfo();
        //    string custId = Request.QueryString["id"];
        //    //得到所要导出的对象
        //    TG.Model.cm_CustomerInfo model = bllinfo.GetModel(Convert.ToInt32(custId));
        //    List<TG.Model.cm_CustomerExtendInfo> modelcustlist = custbll.GetModelList("Cst_Id=" + custId);
        //    TG.Model.cm_CustomerExtendInfo modelcust = null;
        //    foreach (var item in modelcustlist)
        //    {
        //        modelcust = item;
        //    }
        //    //公共类。给模板的书签增加值
        //    string tmppath = Server.MapPath("~/TemplateWord/ReportCustomer.doc");
        //    Document doc = new Document(tmppath); //载入模板

        //    if (doc.Range.Bookmarks["Cst_No"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Cst_No"];
        //        mark.Text = model.Cst_No == null ? "" : model.Cst_No;
        //    }

        //    if (doc.Range.Bookmarks["Cst_Name"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Cst_Name"];
        //        mark.Text = model.Cst_Name == null ? "" : model.Cst_Name;
        //    }

        //    if (doc.Range.Bookmarks["Cst_Brief"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Cst_Brief"];
        //        mark.Text = model.Cst_Brief == null ? "" : model.Cst_Brief;
        //    }

        //    if (doc.Range.Bookmarks["Cpy_Fax"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Cpy_Fax"];
        //        mark.Text = model.Cpy_Fax == null ? "" : model.Cpy_Fax;
        //    }

        //    if (doc.Range.Bookmarks["Cpy_Address"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Cpy_Address"];
        //        mark.Text = model.Cpy_Address == null ? "" : model.Cpy_Address;
        //    }

        //    if (doc.Range.Bookmarks["Code"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Code"];
        //        mark.Text = model.Code == null ? "" : model.Code.ToString();
        //    }

        //    if (doc.Range.Bookmarks["Cpy_Phone"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Cpy_Phone"];
        //        mark.Text = model.Cpy_Phone == null ? "" : model.Cpy_Phone;
        //    }

        //    if (doc.Range.Bookmarks["Linkman"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Linkman"];
        //        mark.Text = model.Linkman == null ? "" : model.Linkman;
        //    }


        //    if (doc.Range.Bookmarks["BankAccountNo"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["BankAccountNo"];
        //        mark.Text = modelcust.BankAccountNo == null ? "" : modelcust.BankAccountNo;
        //    }

        //    if (doc.Range.Bookmarks["BankName"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["BankName"];
        //        mark.Text = modelcust.BankName == null ? "" : modelcust.BankName;
        //    }

        //    if (doc.Range.Bookmarks["BranchPart"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["BranchPart"];
        //        mark.Text = modelcust.BranchPart == null ? "" : modelcust.BranchPart;
        //    }

        //    if (doc.Range.Bookmarks["City"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["City"];
        //        mark.Text = modelcust.City == null ? "" : modelcust.City;
        //    }

        //    if (doc.Range.Bookmarks["Country"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Country"];
        //        mark.Text = modelcust.Country == null ? "" : modelcust.Country;
        //    }

        //    if (doc.Range.Bookmarks["Cpy_Email"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Cpy_Email"];
        //        mark.Text = modelcust.Email == null ? "" : modelcust.Email;
        //    }

        //    if (doc.Range.Bookmarks["Cst_EnglishName"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Cst_EnglishName"];
        //        mark.Text = modelcust.Cst_EnglishName == null ? "" : modelcust.Cst_EnglishName;
        //    }

        //    if (doc.Range.Bookmarks["FadingDaibiao"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["FadingDaibiao"];
        //        mark.Text = modelcust.LawPerson == null ? "" : modelcust.LawPerson;
        //    }

        //    if (doc.Range.Bookmarks["IsPartner"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["IsPartner"];
        //        mark.Text = modelcust.IsPartner.ToString() == "1" ? "合作" : "不合作";
        //    }

        //    if (doc.Range.Bookmarks["Profession"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Profession"];
        //        mark.Text = GetProfession(modelcust.Profession.ToString());
        //    }

        //    if (doc.Range.Bookmarks["Province"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Province"];
        //        mark.Text = modelcust.Province == null ? "" : model.Cst_No;
        //    }

        //    if (doc.Range.Bookmarks["Qinmidu"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Qinmidu"];
        //        mark.Text = GetProfession(modelcust.CloseLeve.ToString());
        //    }

        //    if (doc.Range.Bookmarks["QiyeDaima"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["QiyeDaima"];
        //        mark.Text = modelcust.Cpy_Code == null ? "" : modelcust.Cpy_Code;
        //    }

        //    if (doc.Range.Bookmarks["Remark"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Remark"];
        //        mark.Text = modelcust.Remark == null ? "" : modelcust.Remark;
        //    }

        //    if (doc.Range.Bookmarks["RelationDepartment"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["RelationDepartment"];
        //        mark.Text = modelcust.RelationDepartment == null ? "" : modelcust.RelationDepartment;
        //    }

        //    if (doc.Range.Bookmarks["TaxAccountNo"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["TaxAccountNo"];
        //        mark.Text = modelcust.TaxAccountNo == null ? "" : modelcust.TaxAccountNo;
        //    }

        //    if (doc.Range.Bookmarks["Tyoe"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Tyoe"];
        //        mark.Text = GetProfession(modelcust.Type.ToString());
        //    }

        //    if (doc.Range.Bookmarks["Xinyongjibie"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["Xinyongjibie"];
        //        mark.Text = GetProfession(modelcust.CreditLeve.ToString());
        //    }

        //    if (doc.Range.Bookmarks["UpdateBy"] != null)
        //    {
        //        Bookmark mark = doc.Range.Bookmarks["UpdateBy"];
        //        mark.Text = CommCoperation.GetEasyTime(Convert.ToDateTime(modelcust.LastUpdate));
        //    }


        //    doc.Save(model.Cst_No + ".doc", SaveFormat.Doc, SaveType.OpenInWord, Response);  //保存为doc，并打开
        //}
        //  转换地上和地下楼层数
        protected string GetFloor(string floor)
        {
            string[] floors = floor.Split('|');
            if (floors.Length > 0)
            {
                return "地上:" + floors[0] + "地下:" + floors[1];
            }
            else
            {
                return "";
            }
        }
        protected string GetProfession(string num)
        {
            string result = "";
            string[] str = num.Split(',');
            if (str.Length > 1)
            {
                for (int i = 0; i < str.Length; i++)
                {
                    if (str[i] == "27")
                    {
                        result += "方案，";
                    }
                    else if (str[i] == "28")
                    {
                        result += "初设，";
                    }
                    else if (str[i] == "29")
                    {
                        result += "施工图，";
                    }
                    else
                    {
                        result += "其他";
                    }
                }
            }
            switch (num.Trim())
            {
                case "-1":
                    result = "";
                    break;
                case "27":
                    result = "方案";
                    break;
                case "28":
                    result = "初设";
                    break;
                case "29":
                    result = "施工图";
                    break;
                case "30":
                    result = "其他";
                    break;
                case "31":
                    result = "公开招标";
                    break;
                case "32":
                    result = "邀请招标";
                    break;
                case "36":
                    result = "计算机行业";
                    break;
                case "33":
                    result = "自行委托";
                    break;
                case "37":
                    result = "教育行业";
                    break;
                case "38":
                    result = "建筑行业";
                    break;
                case "47":
                    result = "科教行业";
                    break;
                case "34":
                    result = "普通客户";
                    break;
                case "35":
                    result = "VIP客户";
                    break;
                case "39":
                    result = "一般";
                    break;
                case "40":
                    result = "密切";
                    break;
                case "41":
                    result = "很密切";
                    break;
                case "43":
                    result = "一级";
                    break;
                case "44":
                    result = "二级";
                    break;
                case "45":
                    result = "三级";
                    break;
                case "46":
                    result = "四级";
                    break;
                default:
                    result += "";
                    break;
            }
            return result;
        }
    }
}