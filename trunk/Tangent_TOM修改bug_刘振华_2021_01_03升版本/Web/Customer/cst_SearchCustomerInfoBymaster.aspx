﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cst_SearchCustomerInfoBymaster.aspx.cs" Inherits="TG.Web.Customer.cst_SearchCustomerInfoBymaster" %>

<%@ Register Src="~/UserControl/ChooseCustomer.ascx" TagName="ChooseCustomer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Customer/cst_SearchCustomerInfo_jq.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">客户信息管理 <small>高级查询</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>客户信息管理</a><i class="fa fa-angle-right"> </i><a>高级查询</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>客户高级查询
                    </div>
                    <div class="actions">
                        <asp:Button ID="btn_Output" runat="server" Text="导出" CssClass="btn red btn-sm" OnClick="btn_Output_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td>编号:
                                        </td>
                                        <td>
                                            <input type="text" id="txtCst_No" runat="server" class="form-control input-sm" maxlength="15" />
                                        </td>
                                        <td>客户简称:
                                        </td>
                                        <td>
                                            <input type="text" id="txtCst_Brief" runat="Server" class="form-control input-sm"
                                                maxlength="25" />
                                        </td>
                                        <td>客户名称:
                                        </td>
                                        <td>
                                            <input type="text" id="txtCst_Name" runat="Server" class="form-control input-sm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>客户类别:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddType" runat="Server" CssClass="form-control input-sm" AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>所在省份:
                                        </td>
                                        <td>
                                            <input type="text" id="txtProvince" runat="Server" class="form-control input-sm" />
                                        </td>
                                        <td>所属行业:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddProfession" runat="Server" CssClass="form-control input-sm"
                                                AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>所在城市:
                                        </td>
                                        <td>
                                            <input type="text" id="txtCity" runat="Server" class="form-control input-sm" />
                                        </td>
                                        <td>联系人:
                                        </td>
                                        <td>
                                            <input type="text" id="txtLinkman" runat="Server" class="form-control input-sm" />
                                        </td>
                                        <td>公司地址:
                                        </td>
                                        <td>
                                            <input type="text" id="txtCpy_Address" runat="Server" class="form-control input-sm" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="6">
                                            <input type="button" class="btn blue" id="btn_search" value="查询" />

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>客户信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body  form" style="display: block;">

                    <div class="row">
                        <div class="col-md-12">
                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                                没有符合条件数据！
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <input type="hidden" id="hiddenProvince" runat="server" />
    <input type="hidden" id="hiddenCity" runat="server" />
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
</asp:Content>
