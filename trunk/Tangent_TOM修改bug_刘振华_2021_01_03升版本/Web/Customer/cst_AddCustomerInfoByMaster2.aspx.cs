﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TG.BLL;

namespace TG.Web.Customer
{
    public partial class cst_AddCustomerInfoByMaster2 : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //城市
                GetCountryList();
                //绑定数据
                Bind_DdList();
                //初始化联系人满意度ID
                GetReserveMaxID();
            }
            
        }
        protected void GetReserveMaxID()
        {
            TG.BLL.cm_ContactPersionInfo bll_cst = new TG.BLL.cm_ContactPersionInfo();
            int tempSysNo = (bll_cst.GetMaxId() + 1);
            //this.HiddenCustomerSysNo.Value = tempSysNo.ToString();
            this.HiddenCustomerSysNo.Value = getCst_Id();
        }
        private void GetCountryList()
        {
            RepeaterCountry.DataSource = new TG.BLL.DropDownListCountryBP().GetCountryList(new TG.Model.DropDownListQueryEntity());
            RepeaterCountry.DataBind();
        }
        public void Bind_DdList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定客户类别
            string str_where = "dic_type='cst_type'";
            DataSet ds_cst = dic.GetList(str_where);
            ddType.DataValueField = "ID";
            ddType.DataTextField = "dic_name";
            ddType.DataSource = ds_cst;
            ddType.DataBind();
            //绑定所属行业
            DataSet ds_pro = dic.GetList("dic_type='cst_pro'");
            ddProfession.DataValueField = "ID";
            ddProfession.DataTextField = "dic_name";
            ddProfession.DataSource = ds_pro;
            ddProfession.DataBind();
            //绑定信用级别
            DataSet ds_xy = dic.GetList("dic_type='cst_credit'");
            this.ddCreditLeve.DataSource = ds_xy;
            this.ddCreditLeve.DataTextField = "dic_name";
            this.ddCreditLeve.DataValueField = "ID";
            this.ddCreditLeve.DataBind();
            //亲密度
            DataSet ds_qm = dic.GetList("dic_type='cst_close'");
            this.ddCloseLeve.DataSource = ds_qm;
            this.ddCloseLeve.DataTextField = "dic_name";
            this.ddCloseLeve.DataValueField = "ID";
            this.ddCloseLeve.DataBind();
        }
        /// <summary>
        /// 获取客户ID
        /// </summary>
        /// <returns></returns>
        public string getCst_Id()
        {
            DateTime dt = DateTime.Now;
            string tempid = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString();
            return tempid;
        }
        /// <summary>
        /// 获取登录ID(登录界面开始进行重新设置.....)
        /// </summary>
        /// <returns></returns>
        public string GetUserFlag()
        {
            return UserSysNo.ToString();
        }
        /// <summary>
        /// 返回文件夹ID (若是根据登录ID,此项可不要？待商讨中.......)
        /// </summary>
        /// <returns></returns>
        public string GetParentID()
        {
            return UserShortName;
        }
         protected void btn_Save_Click(object sender, EventArgs e)
        {
            string cur_userid = UserSysNo.ToString();
            if (cur_userid != "0")
            {
                string Cst_No = this.txtCst_No.Value.Trim();
                string Cst_Brief = this.txtCst_Brief.Value.Trim();
                string Cst_Name = this.txtCst_Name.Value.Trim();
                string Cpy_Address = this.txtCpy_Address.Value.Trim();
                string Linkman = this.txtLinkman.Value.Trim();
                string Cpy_Phone = this.txtCpy_Phone.Value.Trim();
                string Cpy_Fax = this.txtCpy_Fax.Value.Trim();

                TG.Model.cm_CustomerInfo model = new TG.Model.cm_CustomerInfo();
                model.Cst_No = Cst_No;
                model.Cst_Brief = Cst_Brief;
                model.Cst_Name = Cst_Name;
                model.Cpy_Address = Cpy_Address;
                if (this.txtCode.Value != "")
                {
                    model.Code = this.txtCode.Value;
                }
                model.Linkman = Linkman;
                model.Cpy_Phone = Cpy_Phone;
                model.Cpy_Fax = Cpy_Fax;
                model.UpdateBy = UserSysNo;
                DateTime LastUpdate = DateTime.Now;
                model.LastUpdate = LastUpdate;
                //by long 20130517
                model.InsertUserID = UserSysNo;
               
                TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();
                int rtn_id = -1;
                try
                {
                    rtn_id = bll.Add(model);
                }
                catch (System.Exception ex)
                { }

                //添加扩展信息
                if (rtn_id != -1)
                {
                    TG.Model.cm_CustomerExtendInfo model_ext = new TG.Model.cm_CustomerExtendInfo();
                    TG.BLL.cm_CustomerExtendInfo bll_ext = new TG.BLL.cm_CustomerExtendInfo();
                    model_ext.Cst_EnglishName = this.txtCst_EnglishName.Value.Trim();
                    model_ext.IsPartner = Convert.ToInt32(this.ddIsPartner.SelectedValue.Trim());
                    model_ext.Country = this.hiddenCountry.Value.Trim();
                    model_ext.City = this.hiddenCity.Value.Trim();
                    model_ext.Province = this.hiddenProvince.Value.Trim();
                    model_ext.Type = Convert.ToInt32(this.ddType.SelectedValue.Trim());
                    model_ext.Profession = Convert.ToInt32(this.ddProfession.SelectedValue.Trim());
                    model_ext.BranchPart = this.txtBranchPart.Value.Trim();
                    model_ext.Email = this.txtEmail.Value.Trim();
                    model_ext.Cpy_PrincipalSheet = this.txtCpy_PrincipalSheet.Value.Trim();
                    model_ext.Cst_Id = rtn_id;
                    model_ext.LawPerson = this.txtLawPerson.Value.Trim();
                    model_ext.RelationDepartment = this.txtRelationDepartment.Value.Trim();
                    model_ext.Remark = this.txtRemark.Value.Trim();
                    model_ext.TaxAccountNo = this.txtTaxAccountNo.Value.Trim();
                    model_ext.UpdateBy = UserSysNo;
                    model_ext.LastUpdate = DateTime.Now;
                    model_ext.BankName = this.txtBankName.Value.Trim();
                    if (this.txtBankAccountNo.Value.Trim() != "")
                    {
                        model_ext.BankAccountNo = this.txtBankAccountNo.Value.Trim();
                    }
                    else
                    {
                        model_ext.BankAccountNo = null;
                    }
                    model_ext.CloseLeve = Convert.ToInt32(this.ddCloseLeve.SelectedValue.Trim());
                    if (this.txtCpy_Code.Value.Trim() != "")
                    {
                        model_ext.Cpy_Code = Convert.ToString(this.txtCpy_Code.Value.Trim());
                    }
                    if (this.txtCreateRelationTime.Value.Trim() != "")
                    {
                        model_ext.CreateRelationTime = Convert.ToDateTime(this.txtCreateRelationTime.Value.Trim());
                    }
                    model_ext.CreditLeve = Convert.ToInt32(this.ddCreditLeve.SelectedValue.Trim());
                    //开户银行大额联行号
                    //model_ext.BankBigNo = this.txtBankBigNo.Value.Trim();
                    //保存扩展信息
                    try
                    {
                        bll_ext.Add(model_ext);
                        //TG.BLL.cm_OperationLog logBLL = new TG.BLL.cm_OperationLog();
                        //logBLL.OperationLog("客户管理", "添加客户信息--客户名称为'" + model.Cst_Name + "'的记录成功", "成功", UserSysNo.ToString());
                    }
                    catch (System.Exception ex)
                    { }
                }
                //添加联系人
                //添加满意度
                if (rtn_id != -1)
                {
                    //联系人  qpl  20140114
                    TG.BLL.CommDBHelper bll_com = new TG.BLL.CommDBHelper();
                    string strSql = " UPDATE cm_ContactPersionInfo SET Cst_Id =" + rtn_id + "  WHERE Cst_Id=" + this.HiddenCustomerSysNo.Value + " AND UpdateBy=" + UserSysNo;
                    bll_com.ExcuteBySql(strSql);
                    //满意度   qpl  20140114
                    strSql = " UPDATE cm_SatisfactionInfo SET Cst_Id = " + rtn_id + " WHERE Cst_Id=" + this.HiddenCustomerSysNo.Value + " AND UpdateBy=" + UserSysNo;
                    bll_com.ExcuteBySql(strSql);
                }

                //添加附件信息
                if (rtn_id != -1)
                {
                    //修改附件信息
                    if (attachHidden.Value.Length > 0)
                    {
                        string hiddenValue = attachHidden.Value.Substring(0, attachHidden.Value.Length - 1);
                        string[] hiddenArr = hiddenValue.Split(',');
                        TG.BLL.cm_AttachInfo attachInfoBLL = new TG.BLL.cm_AttachInfo();
                        foreach (var item in hiddenArr)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                TG.Model.cm_AttachInfo attachInfoModel = attachInfoBLL.GetModel(Convert.ToInt32(item));
                                model.Cst_Id = rtn_id;
                                //上传附件  qpl  20140114
                                string sql = "UPDATE cm_AttachInfo SET Cst_Id = " + rtn_id + "  WHERE [ID] = " + Convert.ToInt32(item) + " AND UploadUser='" + UserSysNo + "' AND OwnType='cst' ";
                                new CommDBHelper().ExcuteBySql(sql);
                            }
                           
                        }
                    }
                }
            }
            //弹出提示
            TG.Common.MessageBox.ShowAndRedirect(this, "客户信息保存成功！", "cst_CustomerInfoListByMaster.aspx");
        }
    }
}