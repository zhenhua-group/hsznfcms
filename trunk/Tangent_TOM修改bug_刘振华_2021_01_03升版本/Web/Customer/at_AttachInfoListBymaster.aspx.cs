﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;

namespace TG.Web.Customer
{
    public partial class at_AttachInfoListBymaster : PageBase
    {
        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定附件
                BindData();

                if (base.RolePowerParameterEntity != null)
                {
                    this.userShortName.Value = base.UserShortName;
                    this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                    this.userSysNum.Value = base.UserSysNo.ToString();
                    this.userUnitNum.Value = base.UserUnitNo.ToString();
                }
            }
        }
        //是否检查权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //检查权限
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND UploadUser =" + UserSysNo);
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND UploadUser IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
            }
            hid_where.Value = sb.ToString();
        }
        /// <summary>
        /// 数据绑定
        /// </summary>
        public void BindData()
        {
            TG.BLL.cm_AttachInfo bll = new TG.BLL.cm_AttachInfo();
            StringBuilder strWhere = new StringBuilder();

            //检查权限
            GetPreviewPowerSql(ref strWhere);
            this.hid_where.Value = strWhere.ToString();
            //所有记录数
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();

            string modelPath = " ~/TemplateXls/AttachInfo.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["FileName"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["UploadTime"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["FileSizes"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["FileType"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["UploadUserName"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["UploadTime"]).ToString("yyyy-MM-dd"));

            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("项目前期资料列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        /// <summary>
        /// 得到导出数据
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {

            TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();
            StringBuilder strWhere = new StringBuilder();

            if (txt_name.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_name.Value.Trim());
                strWhere.AppendFormat(" AND FileName like '%{0}%' AND AT.cst_Id<>-1", keyname);
            }
            else
            {
                strWhere.AppendFormat(" AND AT.cst_Id<>-1");
            }
            var startTime = txt_start.Value;
            var endTime = txt_end.Value;

            //录入时间  
            if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                strWhere.AppendFormat(" AND UploadTime>='{0}' ", startTime + "  00:00:00");
            }
            else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                strWhere.AppendFormat(" AND UploadTime<='{0}' ", endTime + " 23:59:59");
            }
            else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                strWhere.AppendFormat(" AND UploadTime between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
            }

            //检查权限
            GetPreviewPowerSql(ref strWhere);

            //所有记录数
            DataTable dt = bll.Get_AttachInfoExportInfo(strWhere.ToString()).Tables[0];
            return dt;
        }
    }
}