﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cst_AddCustomerInfoByMaster2.aspx.cs" Inherits="TG.Web.Customer.cst_AddCustomerInfoByMaster2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="../css/swfupload/default_cpr.css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/Global.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/jquery.cookie.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_green.js"></script>
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Customer/Contact.js"></script>
    <script type="text/javascript" src="../js/Customer/SatisfactionInfo.js"></script>
    <script src="../js/Customer/cst_AddCustomerInfoBymaster.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Common/DropDownListConutry.js"></script>
    <script type="text/javascript" src="../js/Common/CommonControl.js"></script>

    <script type="text/javascript">
        //登录过期
        var isLogin = '<%=GetUserFlag() %>';
        if (isLogin == "0") {
            window.parent.parent.document.location.href = "../index.html";
        }
        var cst_id = '<%=getCst_Id()%>';
        //获得ID
        var prtid = '<%=GetParentID() %>';
        //用户ID8
        var userid = '<%=GetUserFlag()%>';

        //上传
        if (prtid != "") {
            var swfu;
            window.onload = function () {
                swfu = new SWFUpload({

                    upload_url: "../ProcessUpload/upload.aspx?flag=file&prtid=" + prtid + "&cst_id=" + cst_id + "&userid=" + userid,
                    flash_url: "../js/swfupload/swfupload.swf",
                    post_params: {
                        "ASPSESSID": "<%=Session.SessionID %>"
                    },
                    file_size_limit: "10 MB",
                    file_types: "*.txt;*.jpg;*.dwg;*.wmf;*.doc;*.docx;*.ppt;*.pptx;*.xls;*.xlsx",
                    file_types_description: "上传",
                    file_upload_limit: "0",
                    file_queue_limit: "1",

                    //Events
                    file_queued_handler: fileQueued,
                    file_queue_error_handler: fileQueueError,
                    file_dialog_complete_handler: fileDialogComplete,
                    upload_progress_handler: uploadProgress,
                    upload_error_handler: uploadError,
                    upload_success_handler: uploadSuccessShowResult,
                    upload_complete_handler: uploadComplete,

                    // Button
                    button_placeholder_id: "spanButtonPlaceholder",
                    button_style: '{background-color:#d8d8d8 }',
                    button_width: 61,
                    button_height: 22, //<span class="btn default btn-file"><span class="fileupload-new" id="spanButtonPlaceholder"><i class="fa fa-paper-clip"></i>选择文件</span> </span>
                    button_text: '<span class="fileupload-new">选择文件</span>',
                    button_text_style: '.fileupload-new {background-color:#d8d8d8 ;} ',
                    button_text_top_padding: 1,
                    button_text_left_padding: 5,
                    custom_settings: {
                        upload_target: "divFileProgressContainer"
                    },
                    debug: false
                });
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">客户信息管理 <small>客户信息录入</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>客户信息管理</a><i class="fa fa-angle-right"> </i><a>客户信息录入</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>客户录入
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-10">
                                    基本信息
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered" align="center">
                                        <tr>
                                            <td colspan="4">
                                                <span style="color: red;">提示:黄色背景文本框为必填项！<i class="fa fa-warning "></i></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 15%;">编号:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCst_No" runat="server" class="form-control input-sm" maxlength="15"
                                                    style="background-color: #FFC;" value="编号规则：部门+年份+编号，例如：1S2015001" />
                                            </td>
                                            <td style="width: 15%;">客户简称:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCst_Brief" runat="Server" class="form-control input-sm"
                                                    maxlength="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>客户名称:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCst_Name" runat="Server" class="form-control input-sm"
                                                    maxlength="100" style="background-color: #FFC;" />
                                            </td>
                                            <td>公司地址:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_Address" runat="Server" class="form-control input-sm"
                                                    maxlength="100" style="background-color: #FFC;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>邮政编码:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCode" runat="Server" class="form-control input-sm" maxlength="6" />
                                            </td>
                                            <td>联系人:
                                            </td>
                                            <td>
                                                <input type="text" id="txtLinkman" runat="Server" class="form-control input-sm" maxlength="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>公司电话:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_Phone" runat="Server" class="form-control input-sm"
                                                    maxlength="15" />
                                            </td>
                                            <td>传真号:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_Fax" runat="Server" class="form-control input-sm" maxlength="20" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-10">
                                    甲方负责人
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn default" data-toggle="modal" href="#JFresponsive"
                                        id="addLinkManActionLink" url="ContactPersionInfo/cp_AddContactPersionInfo.aspx?Cst_Id=<%=getCst_Id()%>">
                                        添加</button>
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped table-condensed flip-contentr" id="cst_datas">
                                    <thead class="flip-content">
                                        <tr id="cst_row">
                                            <td align="center" id="id">序号
                                            </td>
                                            <td align="center" id="name">姓名
                                            </td>
                                            <td align="center" id="zhiwu">职务
                                            </td>
                                            <td align="center" id="bumen">部门
                                            </td>
                                            <td align="center" id="phone">电话
                                            </td>
                                            <td align="center" id="email">Email
                                            </td>
                                            <td align="center" id="remark">备注
                                            </td>
                                            <td align="center" id="oper">操作
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-10">
                                    扩展信息
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered" align="center">
                                        <tr>
                                            <td>客户英文名:
                                            </td>
                                            <td colspan="3">
                                                <input type="text" id="txtCst_EnglishName" runat="Server" class="form-control input-sm"
                                                    maxlength="350" />
                                            </td>
                                            <td>是否合作:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddIsPartner" runat="Server" CssClass="form-control">
                                                    <asp:ListItem Value="1">是</asp:ListItem>
                                                    <asp:ListItem Value="2">否</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>所在国家:
                                            </td>
                                            <td>
                                                <select class="form-control" id="DropDownListCountry" onchange="GetProvinces(this);">
                                                    <option value="请选择" countryid="0">----请选择国家----</option>
                                                    <asp:Repeater ID="RepeaterCountry" runat="server">
                                                        <ItemTemplate>
                                                            <option value="<%#Eval("CountryName") %>" countryid="<%#Eval("CountryID") %>">
                                                                <%#Eval("CountryName")%></option>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </select>
                                            </td>
                                            <td>所在省份:
                                            </td>
                                            <td>
                                                <select id="DropDownListProvince" class="form-control " onchange="GetCitys(this);">
                                                    <option value="请选择" provinceid="0">----请选择省份----</option>
                                                </select>
                                            </td>
                                            <td>所在城市:
                                            </td>
                                            <td>
                                                <select id="DropDownListCity" onchange="FillHidden(this);" class="form-control">
                                                    <option value="请选择" cityid="0">----请选择城市----</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>客户类型:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddType" runat="Server" CssClass="form-control" AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">------请选择------</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>所属行业:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddProfession" runat="Server" CssClass="form-control"
                                                    AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">------请选择------</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>分支机构:
                                            </td>
                                            <td>
                                                <input type="text" id="txtBranchPart" runat="Server" class="form-control input-sm"
                                                    maxlength="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Email:
                                            </td>
                                            <td>
                                                <input type="text" class="form-control input-sm" id="txtEmail" runat="Server" maxlength="50" />
                                            </td>
                                            <td>公司主页:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_PrincipalSheet" runat="Server" class="form-control input-sm"
                                                    maxlength="50" />
                                            </td>
                                            <td>关系建立时间:
                                            </td>
                                            <td>
                                                <input type="text" name="txt_date" id="txtCreateRelationTime" onclick="WdatePicker({ readOnly: true })"
                                                    class="Wdate" runat="Server" style="width: 100%; height: 22px; border: 1px solid #e5e5e5" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>关系部门:
                                            </td>
                                            <td>
                                                <input type="text" id="txtRelationDepartment" runat="Server" class="form-control input-sm"
                                                    maxlength="50" />
                                            </td>
                                            <td>信用级别:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddCreditLeve" runat="Server" CssClass="form-control"
                                                    AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">------请选择------</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>亲密度:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddCloseLeve" runat="Server" CssClass="form-control"
                                                    AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1">------请选择------</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>开户银行名称:
                                            </td>
                                            <td colspan="3">
                                                <input type="text" id="txtBankName" runat="Server" class="form-control input-sm"
                                                    maxlength="100" />
                                            </td>
                                            <td>企业代码:
                                            </td>
                                            <td>
                                                <input type="text" id="txtCpy_Code" runat="Server" class="form-control input-sm"
                                                    maxlength="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>开户银行账号:
                                            </td>
                                            <td colspan="3">
                                                <input type="text" id="txtBankAccountNo" runat="Server" class="form-control input-sm"
                                                    maxlength="20" />
                                            </td>
                                            <td>法定代表:
                                            </td>
                                            <td>
                                                <input type="text" id="txtLawPerson" runat="Server" class="form-control input-sm"
                                                    maxlength="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>纳税人识别号:
                                            </td>
                                            <td colspan="5">
                                                <input type="text" id="txtTaxAccountNo" runat="Server" class="form-control input-sm"
                                                    maxlength="20" style="width: 150px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>备注:
                                            </td>
                                            <td colspan="5">
                                                <textarea class="form-control input-sm" rows="3" id="txtRemark" runat="server"></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-1">
                                    </label>
                                    <div class="col-md-11">
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-10">
                                    满意度调查
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn default" data-toggle="modal" href="#MYresponsive"
                                        id="addSatisfactionActionLink" url="ContactPersionInfo/cp_AddContactPersionInfo.aspx?Cst_Id=<%=getCst_Id()%>">
                                        添加</button>
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped table-condensed flip-contentr" id="static_datas">
                                    <thead class="flip-content">
                                        <tr id="static_row">
                                            <td align="center" id="st_id">序号
                                            </td>
                                            <td align="center" id="st_content">调查内容
                                            </td>
                                            <td align="center" id="st_myd">满意度
                                            </td>
                                            <td align="center" id="st_time">调查时间
                                            </td>
                                            <td align="center" id="st_type">调查方式
                                            </td>
                                            <td align="center" id="st_remark">备注
                                            </td>
                                            <td align="center" id="st_oper">操作
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <h4 class="form-section">
                            <div class="row">
                                <div class="col-md-2">
                                    附件
                                </div>
                                <div class="col-md-8">
                                    <div id="divFileProgressContainer">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <span class="btn default btn-file"><span class="fileupload-new" id="spanButtonPlaceholder">
                                        <i class="fa fa-paper-clip"></i>选择文件</span> </span>
                                </div>
                            </div>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered table-striped table-condensed flip-contentr" id="gv_Attach">
                                    <thead class="flip-content">
                                        <tr>
                                            <td style="width: 40%;" class="hidden-xs">文件名
                                            </td>
                                            <td style="width: 15%;">上传时间
                                            </td>
                                            <td style="width: 15%;">文件大小
                                            </td>
                                            <td style="width: 10%;" class="hidden-xs">文件类型
                                            </td>
                                            <td style="width: 10%;">用户
                                            </td>
                                            <td colspan="2" style="width: 10%;" class="hidden-xs">操作
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-offset-12 col-md-12">
                                        <asp:Button Text="保存" runat="server" ID="btn_Save" CssClass="btn green" OnClick="btn_Save_Click" />
                                        <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                            返回</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="attachHidden" runat="server" />
    <!--HiddenArea-->
    <input type="hidden" id="hiddenCountry" runat="server" />
    <input type="hidden" id="hiddenProvince" runat="server" />
    <input type="hidden" id="hiddenCity" runat="server" />
    <asp:HiddenField ID="HiddenCustomerSysNo" runat="server" Value="<%=getCst_Id() %>" />
    <!-- 添加联系人 -->
    <div id="JFresponsive" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true"
        style="display: none; width: 600px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加甲方联系人</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 80px;">序号:
                                </td>
                                <td>
                                    <input type="text" id="txtContactNo" class="form-control input-sm" maxlength="350"
                                        style="background-color: #FFC;" />
                                </td>
                                <td style="width: 80px;">姓名:
                                </td>
                                <td>
                                    <input type="text" id="txtName" class="form-control input-sm" maxlength="350" style="background-color: #FFC;" />
                                </td>
                            </tr>
                            <tr>
                                <td>职务:
                                </td>
                                <td>
                                    <input type="text" id="txtDuties" class="form-control input-sm" maxlength="350" />
                                </td>
                                <td>电话:
                                </td>
                                <td>
                                    <input type="text" id="txtPhone" class="form-control input-sm" maxlength="350" style="background-color: #FFC;" />
                                </td>
                            </tr>
                            <tr>
                                <td>部门:
                                </td>
                                <td>
                                    <input type="text" id="txtDepartment" class="form-control input-sm" maxlength="350" />
                                </td>
                                <td>商务电话:
                                </td>
                                <td>
                                    <input type="text" id="txtBPhone" class="form-control input-sm" maxlength="350" />
                                </td>
                            </tr>
                            <tr>
                                <td>住宅电话:
                                </td>
                                <td>
                                    <input type="text" id="txtFPhone" class="form-control input-sm" maxlength="350" />
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>商务传真:
                                </td>
                                <td>
                                    <input type="text" id="txtBFax" class="form-control input-sm" maxlength="350" />
                                </td>
                                <td>住宅传真:
                                </td>
                                <td>
                                    <input type="text" id="txtFFax" class="form-control input-sm" maxlength="350" />
                                </td>
                            </tr>
                            <tr>
                                <td>Email:
                                </td>
                                <td>
                                    <input type="text" id="LinkManEmail" class="form-control input-sm" maxlength="350" />
                                </td>
                                <td>备注:
                                </td>
                                <td>
                                    <textarea class="form-control input-sm" rows="3" id="LinkManRemark"></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="jf_close" data-dismiss="modal" class="btn btn-default">
                取消</button>
            <button type="button" class="btn blue" id="btnJFAdd">
                保存</button>
        </div>
    </div>
    <!-- 满意度调查弹出层 -->
    <div id="MYresponsive" class="modal fade yellow" tabindex="-1" data-width="500" aria-hidden="true"
        style="display: none; width: 500px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加用户满意度</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 100px;">序号:
                                </td>
                                <td>
                                    <input type="text" id="txtSat_No" class="form-control input-sm" maxlength="350" style="background-color: #FFC;" />
                                </td>
                            </tr>
                            <tr>
                                <td>调查内容:
                                </td>
                                <td>
                                    <textarea class="form-control input-sm" rows="3" id="Sch_Context" style="background-color: #FFC;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>调查时间:
                                </td>
                                <td>
                                    <input type="text" name="txt_date" id="txtSch_Time" onclick="WdatePicker({ readOnly: true })"
                                        class="Wdate" style="width: 100%; height: 34px; border: 1px solid #e5e5e5; background-color: #FFC;" />
                                </td>
                            </tr>
                            <tr>
                                <td>满意度:
                                </td>
                                <td>
                                    <select id="ddSat_Leve" class="form-control input-sm">
                                        <option value="1">满意</option>
                                        <option value="2">非常满意</option>
                                        <option value="3">不满意</option>
                                        <option value="4">非常不满意</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>调查原因:
                                </td>
                                <td>
                                    <input type="text" id="txtSch_Reason" class="form-control input-sm" maxlength="350" />
                                </td>
                            </tr>
                            <tr>
                                <td>调查方式:
                                </td>
                                <td>
                                    <select id="ddSat_Way" class="form-control input-sm">
                                        <option value="1">问卷调查</option>
                                        <option value="2">统计调查</option>
                                        <option value="3">询问调查</option>
                                        <option value="4">电话调查</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>最满意方面:
                                </td>
                                <td>
                                    <input type="text" id="txtSat_Best" class="form-control input-sm" maxlength="350" />
                                </td>
                            </tr>
                            <tr>
                                <td>最不满意方面:
                                </td>
                                <td>
                                    <input type="text" id="txtSat_NotBest" class="form-control input-sm" maxlength="350" />
                                </td>
                            </tr>
                            <tr>
                                <td>备注:
                                </td>
                                <td>
                                    <textarea class="form-control input-sm" rows="3" id="Sat_Remark"></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" id="sat_close" class="btn btn-default">
                    取消</button>
                <button type="button" class="btn blue" id="btnSaveSatisfaction">
                    保存</button>
            </div>
        </div>
    </div>
</asp:Content>
