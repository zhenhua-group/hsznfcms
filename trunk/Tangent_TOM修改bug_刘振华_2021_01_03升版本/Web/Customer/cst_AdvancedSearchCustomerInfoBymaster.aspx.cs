﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.Customer
{
    public partial class cst_AdvancedSearchCustomerInfoBymaster : PageBase
    {
        public string ColumnsContent
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_DdList();
                BindData();
                //绑定字段
                BindColumns();
            }
        }
        protected void BindColumns()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("客户编号", "Cst_No");
            dic.Add("客户名称", "Cst_Name");
            dic.Add("客户简称", "Cst_Brief");
            dic.Add("公司地址", "Cpy_Address");
            dic.Add("联系人", "Linkman");
            dic.Add("联系电话", "Cpy_Phone");
            dic.Add("传真号", "Cpy_Fax");
            dic.Add("邮政编码", "Code");           
            dic.Add("录入人", "InsertUser");
            dic.Add("录入时间", "lrsj");
            dic.Add("客户英文名", "Cst_EnglishName");
            dic.Add("是否合作", "PartnerName");
            dic.Add("所在国家", "Country");
            dic.Add("所在省份", "Province");
            dic.Add("所在城市", "City");
            dic.Add("客户类型", "TypeName");
            dic.Add("所属行业", "ProfessionName");
            dic.Add("分支机构", "BranchPart");
            dic.Add("邮箱", "Email");
            dic.Add("公司主页", "Cpy_PrincipalSheet");
            dic.Add("关系建立时间", "RelationTime");
            dic.Add("关系部门", "RelationDepartment");
            dic.Add("信用级别", "CreditLeveName");
            dic.Add("亲密度", "CloseLeveName");
            dic.Add("开户银行名称", "BankName");
            dic.Add("企业代码", "Cpy_Code");
            dic.Add("开户银行账号", "BankAccountNo");
            dic.Add("法定代表", "LawPerson");
            dic.Add("纳税人识别号", "TaxAccountNo");
            dic.Add("备注", "Remark");

            foreach (KeyValuePair<string, string> pair in dic)
            {
                ColumnsContent += "<label><input type='checkbox' value='" + pair.Value + "' />" + pair.Key + "</label>";
            }             
        }

        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //浏览权限控制
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND ( InsertUserID =" + UserSysNo + ")");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND ( InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + "))");
            }
        }
        //绑定客户类型数据
        public void Bind_DdList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定客户类别
            string str_where = "dic_type='cst_type'";
            DataSet ds_cst = dic.GetList(str_where);
            ddType.DataValueField = "ID";
            ddType.DataTextField = "dic_name";
            ddType.DataSource = ds_cst;
            ddType.DataBind();

            //绑定所属行业
            DataSet ds_pro = dic.GetList("dic_type='cst_pro'");
            ddProfession.DataValueField = "ID";
            ddProfession.DataTextField = "dic_name";
            ddProfession.DataSource = ds_pro;
            ddProfession.DataBind();
        }
        public void BindData()
        {
            string strWhere = "";
            //限制浏览
            StringBuilder sb = new StringBuilder(strWhere);
            //检查权限
            GetPreviewPowerSql(ref sb);

            this.hid_where.Value = sb.ToString();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Export_Click(object sender, EventArgs e)
        {

            DataTable dt = getData();

            //创建workbook
            HSSFWorkbook workbook = new HSSFWorkbook();
            MemoryStream ms = new MemoryStream();
            ISheet sheet = workbook.CreateSheet("sheet1");
            IRow row = sheet.CreateRow(0);


            ICellStyle style = workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.LEFT;//左对齐
            //数据单元格的边框
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //数据的字体
            IFont datafontStyle = workbook.CreateFont();
            datafontStyle.FontHeightInPoints = 12;//字号
            datafontStyle.FontName = "宋体";
            style.SetFont(datafontStyle);
            style.WrapText = true;//自动换行

            ICellStyle datastyle = workbook.CreateCellStyle();
            datastyle.Alignment = HorizontalAlignment.LEFT;//左对齐
            //数据单元格的边框
            datastyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            datastyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            datastyle.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            datastyle.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //数据的字体
            IFont datafont = workbook.CreateFont();
            datafont.FontHeightInPoints = 9;//字号
            datafont.FontName = "宋体";
            datastyle.SetFont(datafont);
            datastyle.WrapText = true;//自动换行

            sheet.SetColumnWidth(0, 8 * 256);
            sheet.SetColumnWidth(1, 15 * 256);
            sheet.SetColumnWidth(2, 30 * 256);
            sheet.SetColumnWidth(3, 20 * 256);
            sheet.SetColumnWidth(4, 25 * 256);
            sheet.SetColumnWidth(5, 15 * 256);
            sheet.SetColumnWidth(6, 15 * 256);
            //第三行 从 0开始算
            IRow row2 = sheet.CreateRow(0);
            ICell cell0 = row2.CreateCell(0);
            cell0.SetCellValue("序号");
            cell0.CellStyle = style;

            ICell cell1 = row2.CreateCell(1);
            cell1.SetCellValue("客户编码");
            cell1.CellStyle = style;

            ICell cell2 = row2.CreateCell(2);
            cell2.SetCellValue("客户名称");
            cell2.CellStyle = style;

            ICell cell3 = row2.CreateCell(3);
            cell3.SetCellValue("客户简称");
            cell3.CellStyle = style;

            ICell cell4 = row2.CreateCell(4);
            cell4.SetCellValue("公司地址");
            cell4.CellStyle = style;

            ICell cell5 = row2.CreateCell(5);
            cell5.SetCellValue("联系人");
            cell5.CellStyle = style;

            ICell cell6 = row2.CreateCell(6);
            cell6.SetCellValue("公司电话");
            cell6.CellStyle = style;

            ICell cell8 = row2.CreateCell(7);
            cell8.SetCellValue("录入时间");
            cell8.CellStyle = style;

            //省份
            if (Province.Checked)
            {
                ICell cell7 = row2.CreateCell(8);
                cell7.SetCellValue("所在省份");
                cell7.CellStyle = style;
            }
            //城市
            if (City.Checked)
            {
                ICell cell7 = null;
                if (!Province.Checked)
                {
                    cell7 = row2.CreateCell(8);
                }
                else
                {
                    cell7 = row2.CreateCell(9);
                }
                cell7.SetCellValue("所在城市");
                cell7.CellStyle = style;
            }
            //客户类别
            if (Type.Checked)
            {
                ICell cell7 = null;
                if (!Province.Checked && !City.Checked)
                {
                    cell7 = row2.CreateCell(8);
                }
                else if (Province.Checked && City.Checked)
                {
                    cell7 = row2.CreateCell(10);
                }
                else
                {
                    cell7 = row2.CreateCell(9);
                }
                cell7.SetCellValue("客户类别");
                cell7.CellStyle = style;
            }
            //所属行业
            if (Profession.Checked)
            {
                ICell cell7 = null;
                if (!Province.Checked && !City.Checked && !Type.Checked)
                {
                    cell7 = row2.CreateCell(8);
                }
                else if (Province.Checked && City.Checked && Type.Checked)
                {
                    cell7 = row2.CreateCell(11);
                }
                else if ((Province.Checked && !City.Checked && !Type.Checked) || (!Province.Checked && City.Checked && !Type.Checked) || (!Province.Checked && !City.Checked && Type.Checked))
                {
                    cell7 = row2.CreateCell(9);
                }
                else
                {
                    cell7 = row2.CreateCell(10);
                }
                cell7.SetCellValue("所属行业");
                cell7.CellStyle = style;
            }

            int rowIndex = 1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                IRow rownew = sheet.CreateRow(i + rowIndex);

                ICell cell01 = rownew.CreateCell(0);
                cell01.SetCellValue(i + 1);
                cell01.CellStyle = datastyle;

                ICell cell02 = rownew.CreateCell(1);
                cell02.SetCellValue(dt.Rows[i]["Cst_No"].ToString());
                cell02.CellStyle = datastyle;

                ICell cell03 = rownew.CreateCell(2);
                cell03.SetCellValue(dt.Rows[i]["Cst_Name"].ToString());
                cell03.CellStyle = datastyle;

                ICell cell04 = rownew.CreateCell(3);
                cell04.SetCellValue(dt.Rows[i]["Cst_Brief"].ToString());
                cell04.CellStyle = datastyle;


                ICell cell05 = rownew.CreateCell(4);
                cell05.SetCellValue(dt.Rows[i]["Cpy_Address"].ToString());
                cell05.CellStyle = datastyle;


                ICell cell06 = rownew.CreateCell(5);
                cell06.SetCellValue(dt.Rows[i]["Linkman"].ToString());
                cell06.CellStyle = datastyle;


                ICell cell07 = rownew.CreateCell(6);
                cell07.SetCellValue(dt.Rows[i]["Cpy_Phone"].ToString());
                cell07.CellStyle = datastyle;

                ICell cell12 = rownew.CreateCell(7);
                cell12.SetCellValue(Convert.ToDateTime(dt.Rows[i]["InsertDate"]).ToString("yyyy-MM-dd"));
                cell12.CellStyle = datastyle;

                if (Province.Checked)
                {
                    ICell cell08 = rownew.CreateCell(8);
                    cell08.SetCellValue(dt.Rows[i]["Province"].ToString());
                    cell08.CellStyle = datastyle;
                }


                //城市
                if (City.Checked)
                {
                    ICell cell09 = null;
                    if (!Province.Checked)
                    {
                        cell09 = rownew.CreateCell(8);
                    }
                    else
                    {
                        cell09 = rownew.CreateCell(9);
                    }
                    cell09.SetCellValue(dt.Rows[i]["City"].ToString());
                    cell09.CellStyle = datastyle;
                }

                if (Type.Checked)
                {
                    ICell cell10 = null;
                    if (!Province.Checked && !City.Checked)
                    {
                        cell10 = rownew.CreateCell(8);
                    }
                    else if (Province.Checked && City.Checked)
                    {
                        cell10 = rownew.CreateCell(10);
                    }
                    else
                    {
                        cell10 = rownew.CreateCell(9);
                    }
                    cell10.SetCellValue(colTypeName(dt.Rows[i]["Type"].ToString()));
                    cell10.CellStyle = datastyle;
                }

                //所属行业
                if (Profession.Checked)
                {
                    ICell cell11 = null;
                    if (!Province.Checked && !City.Checked && !Type.Checked)
                    {
                        cell11 = rownew.CreateCell(8);
                    }
                    else if (Province.Checked && City.Checked && Type.Checked)
                    {
                        cell11 = rownew.CreateCell(11);
                    }
                    else if ((Province.Checked && !City.Checked && !Type.Checked) || (!Province.Checked && City.Checked && !Type.Checked) || (!Province.Checked && !City.Checked && Type.Checked))
                    {
                        cell11 = rownew.CreateCell(9);
                    }
                    else
                    {
                        cell11 = rownew.CreateCell(10);
                    }
                    cell11.SetCellValue(colSetProfessionName(dt.Rows[i]["Profession"].ToString()));
                    cell11.CellStyle = datastyle;
                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {
                workbook.Write(memoryStream);
                string name = System.Web.HttpContext.Current.Server.UrlEncode("客户信息列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                workbook = null;
                Response.End();
            }

        }

        /// <summary>
        /// 得到导出数据
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {
            string strWhere = "";
            TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();
            //高级查询AND
            if (this.txtCst_No.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtCst_No.Text.Trim());
                strWhere += " AND C.Cst_No LIKE '%" + keyname + "%' ";
            }
            if (this.txtCst_Brief.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtCst_Brief.Text.Trim());
                strWhere += " AND C.Cst_Brief LIKE '%" + keyname + "%' ";
            }
            if (this.txtCst_Name.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtCst_Name.Text.Trim());
                strWhere += " AND C.Cst_Name LIKE '%" + keyname + "%' ";
            }
            if (this.ddType.SelectedIndex != 0)
            {
                strWhere += " AND CX.Type =" + this.ddType.SelectedItem.Value + " ";
            }
            if (this.ddProfession.SelectedIndex != 0)
            {
                strWhere += " AND CX.Profession=" + this.ddProfession.SelectedItem.Value + " ";
            }
            if (this.txtCity.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtCity.Text.Trim());
                strWhere += " AND CX.City LIKE '%" + keyname + "%' ";
            }
            if (this.txtLinkman.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtLinkman.Text.Trim());
                strWhere += " AND C.LinkMan LIKE '%" + keyname + "%' ";
            }
            if (this.txtProvince.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtProvince.Text.Trim());
                strWhere += " AND CX.Province LIKE '%" + keyname + "%' ";
            }
            if (this.txtCpy_Address.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtCpy_Address.Text.Trim());
                strWhere += " AND C.Cpy_Address LIKE '%" + keyname + "%' ";
            }
            if (this.txtCpy_Phone.Text.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtCpy_Phone.Text.Trim());
                strWhere += " AND C.Cpy_Phone LIKE '%" + keyname + "%' ";
            }
            var startTime = txt_start.Value;
            var endTime = txt_end.Value;
            //录入时间
            if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                strWhere += " AND InsertDate>='" + startTime + "  00:00:00 '";
            }
            else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                strWhere += " AND InsertDate<='" + endTime + "  23:59:59 '";
            }
            else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                strWhere += " AND InsertDate between '" + startTime + "  00:00:00 ' and '" + endTime + "  23:59:59 '";
            }
            //限制浏览
            StringBuilder sb = new StringBuilder(strWhere);
            //检查权限
            GetPreviewPowerSql(ref sb);
            //所有记录数

            DataTable dt = bll.GetCustomerExportInfo(sb.ToString()).Tables[0];
            return dt;
        }


        //设置
        private string colTypeName(string celvalue)
        {
            var professionString = "";
            if (celvalue == "-1")
            {
                professionString = "";
            }
            else if (celvalue == "34")
            {
                professionString = "普通客户";
            }
            else if (celvalue == "35")
            {
                professionString = "VIP客户";
            }
            return professionString;
        }

        private string colSetProfessionName(string celvalue)
        {
            var professionString = "";
            if (celvalue == "-1")
            {
                professionString = "";
            }
            else if (celvalue == "36")
            {
                professionString = "计算机行业";
            }
            else if (celvalue == "37")
            {
                professionString = "教育行业";
            }
            else if (celvalue == "38")
            {
                professionString = "建筑行业";
            }
            else if (celvalue == "47")
            {
                professionString = "科教行业";
            }
            return professionString;
        }

    }
}