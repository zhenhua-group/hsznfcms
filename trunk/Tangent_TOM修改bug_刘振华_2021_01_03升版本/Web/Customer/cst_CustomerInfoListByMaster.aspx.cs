﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Data;
using System.IO;
using Geekees.Common.Controls;
using System.Xml;
using System.Web.UI.HtmlControls;

namespace TG.Web.Customer
{
    public partial class cst_CustomerInfoListByMaster : PageBase
    {
        public string ColumnsContent
        {
            get;
            set;
        }

        public string ColumnsContent_Sh
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //设置样式
                //  SetDropDownTreeThem();
                //绑定列字段样式
                BindColumns();
                Bind_DdList();
                BindPreviewPower();
                //初始化时间
                InitDate();
                //绑定生产部门
                BindUnit();
                //绑定年份
                BindYear();
                BindData();
                SelectCurrentYear();

            }
        }
        Dictionary<string, string> dic = null;
        //结构形式
        protected void BindColumns()
        {
            dic = new Dictionary<string, string>();
            dic.Add("客户编号", "Cst_No");
            dic.Add("客户名称", "Cst_Name");
            dic.Add("联系人", "Linkman");
            dic.Add("联系电话", "Cpy_Phone");
            dic.Add("公司地址", "Cpy_Address");
            dic.Add("邮政编码", "Code");
            dic.Add("传真号", "Cpy_Fax");
            dic.Add("录入人", "InsertUser");
            dic.Add("录入时间", "lrsj");
            dic.Add("客户简称", "Cst_Brief");
            dic.Add("客户英文名", "Cst_EnglishName");
            dic.Add("是否合作", "PartnerName");
            dic.Add("所在国家", "Country");
            dic.Add("所在省份", "Province");
            dic.Add("所在城市", "City");
            dic.Add("客户类型", "TypeName");
            dic.Add("所属行业", "ProfessionName");
            dic.Add("分支机构", "BranchPart");
            dic.Add("邮箱", "Email");
            dic.Add("公司主页", "Cpy_PrincipalSheet");
            dic.Add("关系建立时间", "RelationTime");
            dic.Add("关系部门", "RelationDepartment");
            dic.Add("信用级别", "CreditLeveName");
            dic.Add("亲密度", "CloseLeveName");
            dic.Add("开户银行名称", "BankName");
            dic.Add("企业代码", "Cpy_Code");
            dic.Add("开户银行账号", "BankAccountNo");
            dic.Add("法定代表", "LawPerson");
            dic.Add("纳税人识别号", "TaxAccountNo");
            dic.Add("备注", "Remark");
            foreach (KeyValuePair<string, string> pair in dic)
            {
                if (pair.Value != "Cst_Name" && pair.Value != "lrsj" && pair.Value != "Cst_No" && pair.Value != "Linkman")
                {
                    ColumnsContent_Sh += "<label><input type='checkbox' value='" + pair.Value + "' name='" + pair.Key + "' />" + pair.Key + "</label>";
                }
            }

            foreach (KeyValuePair<string, string> pair in dic)
            {
                ColumnsContent += "<label><input type='checkbox' value='" + pair.Value + "' name='" + pair.Key + "' />" + pair.Key + "</label>";
            }
            //  BindProInfoConfig(this.asTreeviewStruct.RootNode,dic);
            //this.asTreeviewStruct.CollapseAll();           
        }
        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(ASTreeViewNode rootnode, Dictionary<string, string> dic)
        {
            //声明根节点
            ASTreeViewNode root = rootnode;
            //初始化树控件
            foreach (KeyValuePair<string, string> pair in dic)
            {
                ASTreeViewNode linknode = new ASTreeViewNode(pair.Key, pair.Value);
                root.AppendChild(linknode);
            }

        }
        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            // this.asTreeviewStruct.Theme = macOS;

        }
        public void Bind_DdList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定客户类别
            string str_where = "dic_type='cst_type'";
            DataSet ds_cst = dic.GetList(str_where);
            ddType.DataValueField = "ID";
            ddType.DataTextField = "dic_name";
            ddType.DataSource = ds_cst;
            ddType.DataBind();
            //绑定所属行业
            DataSet ds_pro = dic.GetList("dic_type='cst_pro'");
            ddProfession.DataValueField = "ID";
            ddProfession.DataTextField = "dic_name";
            ddProfession.DataSource = ds_pro;
            ddProfession.DataBind();
            //绑定信用级别
            DataSet ds_xy = dic.GetList("dic_type='cst_credit'");
            this.ddCreditLeve.DataSource = ds_xy;
            this.ddCreditLeve.DataTextField = "dic_name";
            this.ddCreditLeve.DataValueField = "ID";
            this.ddCreditLeve.DataBind();
            //亲密度
            DataSet ds_qm = dic.GetList("dic_type='cst_close'");
            this.ddCloseLeve.DataSource = ds_qm;
            this.ddCloseLeve.DataTextField = "dic_name";
            this.ddCloseLeve.DataValueField = "ID";
            this.ddCloseLeve.DataBind();
        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
        /// <summary>
        /// 是否检查权限
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }

        //初始时间设置
        protected void InitDate()
        {
            string strDate = DateTime.Now.ToString("yyyy-MM-dd");
            //this.txt_start.Value = strDate;
            //this.txt_end.Value = strDate;
        }


        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_CustomerInfo().GetCustomerYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        /// <summary>
        /// 查看权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND InsertUserID =" + UserSysNo);
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 数据绑定
        /// </summary>
        public void BindData()
        {
            TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();
            StringBuilder strWhere = new StringBuilder("");

            //权限
            GetPreviewPowerSql(ref strWhere);
            this.hid_where.Value = strWhere.ToString();

        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果是部门和个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID=" + UserUnitNo + " ";
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        /// <summary>
        /// 输出流文件
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool DowloadTemplate(string path, string fileName)
        {
            if (System.IO.File.Exists(path))
            {
                System.IO.FileInfo file = new System.IO.FileInfo(path);
                Response.Clear();
                Response.Charset = "GB2312";
                Response.ContentEncoding = System.Text.Encoding.UTF8;
                // 添加头信息，为"文件下载/另存为"对话框指定默认文件名 
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode(file.Name));
                // 添加头信息，指定文件大小，让浏览器能够显示下载进度 
                Response.AddHeader("Content-Length", file.Length.ToString());
                // 指定返回的是一个不能被客户端读取的流，必须被下载 
                Response.ContentType = "application/ms-excel";
                // 把文件流发送到客户端 
                Response.WriteFile(path);
                Response.Flush();
                // 停止页面的执行 
                Response.End();
                return true;
            }
            else
            {
                Response.Clear();
                return false;
            }
        }


        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();
            string str_gcfl = hid_ColumnsID.Value;


            string modelPath = " ~/TemplateXls/CustomerListInfo.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cst_Name"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Linkman"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cpy_Phone"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cst_No"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cpy_Address"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Code"].ToString());

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cpy_Fax"].ToString());

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["InsertUser"].ToString());

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["InsertDate"]).ToString("yyyy-MM-dd"));
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("客户信息列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        /// <summary>
        /// 得到导出数据
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {

            TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();
            StringBuilder strWhere = new StringBuilder("");
            //按名称查询
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                strWhere.AppendFormat(" AND Cst_Name like '%{0}%'", keyname);
            }
            //按生产部门查询
            if (this.drp_unit.SelectedIndex != 0)
            {
                strWhere.AppendFormat(" AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID={0})", this.drp_unit.SelectedValue);
            }
            //年份
            if (this.drp_year.SelectedIndex != 0)
            {
                strWhere.AppendFormat(" AND year(InsertDate)={0} ", this.drp_year.SelectedValue);
            }

            //录入时间
            string startTime = txt_start.Value;//录入时间
            string endTime = txt_end.Value;//录入时间
            if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                strWhere.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
            }
            else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                strWhere.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
            }
            else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                strWhere.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
            }
            //权限
            GetPreviewPowerSql(ref strWhere);

            //所有记录数
            DataTable dt = bll.GetCustomerExportInfo(strWhere.ToString()).Tables[0];
            return dt;
        }
    }
}