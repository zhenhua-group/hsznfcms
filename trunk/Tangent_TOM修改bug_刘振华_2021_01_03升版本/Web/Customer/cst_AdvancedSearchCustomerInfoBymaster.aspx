﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="cst_AdvancedSearchCustomerInfoBymaster.aspx.cs" Inherits="TG.Web.Customer.cst_AdvancedSearchCustomerInfoBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Customer/cst_AdvancedSearchCustomerInfo_jq.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">客户信息管理 <small>客户综合查询</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>客户信息管理</a><i class="fa fa-angle-right"> </i><a>客户综合统计</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>请选择查询项
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tb_check">
                                <tr>
                                    <td align="left">
                                        <input id="Cst_No" name="bb" class="cls_audit" type="checkbox" /><span for="Cst_No">客户编号</span>
                                    </td>
                                    <td align="left">
                                        <input id="Cst_Name" name="bb" class="cls_audit" type="checkbox" /><span for="Cst_Name">客户名称</span>
                                    </td>
                                    <td align="left">
                                        <input id="Cst_Brief" name="bb" class="cls_audit" type="checkbox" /><span for="Cst_Brief">客户简称</span>
                                    </td>
                                    <td align="left">
                                        <input id="Cpy_Address" name="bb" class="cls_audit" type="checkbox" /><span for="Cpy_Address">公司地址</span>
                                    </td>
                                    <td align="left">
                                        <input id="Linkman" name="bb" class="cls_audit" type="checkbox" /><span for="Linkman">联系人</span>
                                    </td>
                                    <td align="left">
                                        <input id="Cpy_Phone" name="bb" class="cls_audit" type="checkbox" /><span for="Cpy_Phone">公司电话</span>
                                    </td>
                                    <td align="left">
                                        <input id="Province" name="bb" class="cls_audit" type="checkbox" runat="server" /><span for="Province">所在省份</span>
                                    </td>
                                    <td align="left">
                                        <input id="City" name="bb" class="cls_audit" type="checkbox" runat="server" /><span for="City">所在城市</span>
                                    </td>
                                    <td align="left">
                                        <input id="Type" name="bb" class="cls_audit" type="checkbox" runat="server" /><span for="Type">客户类别</span>
                                    </td>
                                    <td align="left">
                                        <input id="Profession" name="bb" class="cls_audit" type="checkbox" runat="server" /><span for="Profession">所属行业</span>
                                    </td>
                                    <td align="left">
                                        <input id="InsertTime" name="bb" class="cls_audit" type="checkbox" runat="server" /><span for="InsertTime">录入时间</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>客户查询条件
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <div class="btn-group" data-toggle="buttons">
                            <span class="btn blue">查询方式：</span>
                            <label rel="and" class="btn yellow btn-sm active" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="option1" value="and">必须
                            </label>
                            <label rel="or" class="btn default btn-sm" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="option2" value="or">或
                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                        </div>

                        <%--<div class="btn-group">
                            <a href="#" id="navbarDrop1" class="btn yellow dropdown-toggle" data-toggle="dropdown">查询方式<i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu pull-right" role="menu" aria-labelledby="navbarDrop1" style="min-width:100px;">
                                <li role="presentation"  class="active" rel="and"><a role="menuitem" href="#"  tabindex="-1">必须</a></li>
                                <li role="presentation" rel="or"><a role="menuitem" href="#"  tabindex="-1">或</a></li>
                            </ul>
                        </div>
                        <asp:Button ID="btn_Export" runat="server" Text="导出" CssClass="btn red btn-sm" OnClick="btn_Export_Click" />--%>
                    </div>

                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-hover" align="center" style="width: 100%; display: none;"
                        id="searchDetail">
                        <tr style="display: none;">
                            <td style="width: 20%;">客户编号：</td>
                            <td>
                                <asp:TextBox ID="txtCst_No" runat="server" CssClass="form-control input-sm" Style="width: 390px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>客户名称：</td>
                            <td>
                                <asp:TextBox ID="txtCst_Name" runat="server" CssClass="form-control input-sm" Style="width: 390px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>客户简称：</td>
                            <td>
                                <asp:TextBox ID="txtCst_Brief" runat="server" CssClass="form-control input-sm" Style="width: 390px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>公司地址：</td>
                            <td>
                                <asp:TextBox ID="txtCpy_Address" runat="server" CssClass="form-control input-sm" Style="width: 390px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>联系人：</td>
                            <td>
                                <asp:TextBox ID="txtLinkman" runat="server" CssClass="form-control input-sm" Style="width: 390px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>公司电话：</td>
                            <td>
                                <asp:TextBox ID="txtCpy_Phone" runat="server" CssClass="form-control input-sm" Style="width: 390px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>所在省份：</td>
                            <td>
                                <asp:TextBox ID="txtProvince" runat="server" CssClass="form-control input-sm" Style="width: 390px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>所在城市：</td>
                            <td>
                                <asp:TextBox ID="txtCity" runat="server" CssClass="form-control input-sm" Style="width: 390px;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>客户类别：</td>
                            <td>
                                <asp:DropDownList ID="ddType" runat="Server" CssClass="form-control" AppendDataBoundItems="True" Width="390">
                                    <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>所属行业：</td>
                            <td>
                                <asp:DropDownList ID="ddProfession" runat="Server" CssClass="form-control" Width="390"
                                    AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>录入时间：</td>
                            <td>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                                class="Wdate" runat="Server" style="width: 180px; height: 30px; border: 1px solid #e5e5e5" />
                                            &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 180px; height: 30px; border: 1px solid
                #e5e5e5" />
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table align="center" style="width: 60%; display: none;" id="tbSearch">
                        <tr>
                            <td align="center">
                                <input type="button" class="btn blue" id="btn_search" value="查询" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet box blue" id="divCusList">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-briefcase"></i>客户信息
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择显示列
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsid">
                                <label>
                                    <input type="checkbox" value="all" />全选
                                </label>
                                <%=ColumnsContent %>
                            </div>
                        </div>
                        <asp:Button ID="btn_Export" runat="server" Text="导出Excel" CssClass="btn red btn-sm" OnClick="btn_Export_Click" />
                    </div>
                </div>
                <div class="portlet-body  form">

                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>
                </div>


            </div>
        </div>
    </div>

    <asp:HiddenField ID="hid_where" runat="server" Value="" />
</asp:Content>
