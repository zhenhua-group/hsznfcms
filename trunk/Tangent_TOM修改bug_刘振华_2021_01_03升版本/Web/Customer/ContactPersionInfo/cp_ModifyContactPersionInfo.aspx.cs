﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using TG.Common;
using LTP.Accounts.Bus;
using Newtonsoft.Json;

namespace TG.Web.Customer.ContactPersionInfo
{
    public partial class cp_ModifyContactPersionInfo : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.HttpMethod == "GET")
            {
                ShowInfo();
            }
            else
            {
                btn_Save_Click();
            }
            Response.End();
        }
        //显示联系人信息
        private void ShowInfo()
        {
            TG.BLL.cm_ContactPersionInfo bll = new TG.BLL.cm_ContactPersionInfo();
            TG.Model.cm_ContactPersionInfo model = bll.GetModel(int.Parse(Request["cid"]));

            string jsonResult = Newtonsoft.Json.JsonConvert.SerializeObject(model);
            Response.Write(jsonResult);

        }
        //保存联系人信息
        protected void btn_Save_Click()
        {
            //当前用户
            int i_cur = UserSysNo;
            //更新类型 zxq 20140121
            decimal i_cstid = Convert.ToDecimal(Request["customerSysNo"] ?? "0");
            int i_cpid = Convert.ToInt32(Request["Cp_ID"] ?? "0");

            string ContactNo = Request["txtContactNo"] ?? "0";
            string Name = Request["txtName"] ?? "0";
            string Duties = Request["txtDuties"] ?? "0";
            string Department = Request["txtDepartment"] ?? "0";
            string Phone = Request["txtPhone"] ?? "0";
            string Email = Request["LinkManEmail"] ?? "0";
            int UpdateBy = i_cur;
            DateTime LastUpdate = DateTime.Now;

            TG.Model.cm_ContactPersionInfo model = new TG.Model.cm_ContactPersionInfo();
            model.CP_Id = i_cpid;
            model.Cst_Id = i_cstid;
            model.ContactNo = ContactNo;
            model.Name = Name;
            model.Duties = Duties;
            model.Department = Department;
            model.Phone = Phone;
            model.Email = Email;
            model.UpdateBy = UpdateBy;
            model.LastUpdate = LastUpdate;
            model.BFax = Request["txtBFax"] ?? "0";
            model.BPhone = Request["txtBPhone"] ?? "0";
            model.FFax = Request["txtFFax"] ?? "0";
            model.FPhone = Request["txtFPhone"] ?? "0";
            model.Remark = Request["LinkManRemark"] ?? "0";

            TG.BLL.cm_ContactPersionInfo bll = new TG.BLL.cm_ContactPersionInfo();
            int count = 0;
            try
            {
                bool flag = bll.Update(model);
                count = flag == true ? 1 : 0;
            }
            catch (System.Exception ex)
            { }

            Response.Write(count);
            //TG.Common.MessageBox.ShowAndRedirect(this, "保存成功！", "../cst_ModifyCustomerInfo.aspx?id=" + i_cstid);
        }
    }
}
