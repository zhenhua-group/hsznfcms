﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using TG.Common;
using LTP.Accounts.Bus;

namespace TG.Web.Customer.ContactPersionInfo
{
	public partial class cp_AddContactPersionInfo : PageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			btn_save_Click();
		}
        /// <summary>
        /// 保存联系人信息
        /// </summary>
		protected void btn_save_Click()
		{
			int count = 0;
			string str_cur = UserSysNo.ToString();
			string cst_id = Request["customerSysNo"];
			if (str_cur != "")
			{
				string Name = Request["txtName"].ToString();
				string Duties = Request["txtDuties"].ToString();
				string Department = Request["txtDepartment"].ToString();

				DateTime LastUpdate = DateTime.Now;
				TG.Model.cm_ContactPersionInfo model = new TG.Model.cm_ContactPersionInfo();
				model.ContactNo = Request["txtContactNo"].ToString();
				model.Cst_Id = decimal.Parse(cst_id); //更新类型 zxq 20140121
				model.Name = Name;
				model.Duties = Duties;
				model.Department = Department;
				model.Phone = Request["txtPhone"].ToString();
				model.Email = Request["LinkManEmail"].ToString();
				model.UpdateBy = Convert.ToInt32(str_cur);
				model.LastUpdate = LastUpdate;
				model.BFax = Request["txtBFax"].ToString();
				model.BPhone = Request["txtBPhone"].ToString();
				model.FFax = Request["txtFFax"].ToString().Trim();
				model.FPhone = Request["txtFPhone"].ToString().Trim();
				model.Remark = Request["LinkManRemark"].ToString();
				TG.BLL.cm_ContactPersionInfo bll = new TG.BLL.cm_ContactPersionInfo();
				try
				{
					count = bll.Add(model);
				}
				catch (System.Exception ex)
				{
				}
			}
			if (count > 0)
			{
				Response.Write("1");
			}
			else
			{
				Response.Write("0");
			}
			Response.End();
		}
	}
}
