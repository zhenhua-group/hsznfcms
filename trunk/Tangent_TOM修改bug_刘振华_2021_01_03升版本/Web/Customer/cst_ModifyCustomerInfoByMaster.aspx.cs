﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace TG.Web.Customer
{
    public partial class cst_ModifyCustomerInfoByMaster : PageBase
    {
        /// <summary>
        /// 客户系统自增号
        /// </summary>
        public int CustomerSysNo
        {
            get
            {
                int customerSysNo = 0;
                int.TryParse(Request["Cst_Id"], out customerSysNo);
                return customerSysNo;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            //显示基本数据
            if (!IsPostBack)
            {
                // this.hid_parentid.Value = UserShortName;
                string str_cstid = Request.QueryString["Cst_Id"] ?? "";
                this.HiddenCustomerSysNo.Value = str_cstid;
                //绑定下拉框
                Bind_DdList();
                //显示信息
                if (str_cstid != "")
                {
                    //客户ID
                    ViewState["Cst_ID"] = str_cstid;
                    //基本信息
                    ShowInfo(int.Parse(str_cstid));
                    //扩展信息
                    ShowExtendInfo(int.Parse(str_cstid));
                    //附件
                    BindAttachInfo();
                    //合同
                    BindContact();
                    //城市
                    GetCountryList();
                }
            }
        }
        protected void btn_Save_Click(object sender, EventArgs e)
        {
            BtnSave();
        }
        public void BtnSave()
        {
            string str_cur = UserSysNo.ToString();
            string str_cstid = Convert.ToString(ViewState["Cst_ID"] ?? "");
            string str_extid = Convert.ToString(ViewState["Cstext_ID"] ?? "");
            if (str_cur != "0")
            {
                string Cst_No = this.txtCst_No.Value.Trim();
                string Cst_Brief = this.txtCst_Brief.Value.Trim();
                string Cst_Name = this.txtCst_Name.Value.Trim();
                string Cpy_Address = this.txtCpy_Address.Value.Trim();
                string Linkman = this.txtLinkman.Value.Trim();
                string Cpy_Phone = this.txtCpy_Phone.Value.Trim();
                string Cpy_Fax = this.txtCpy_Fax.Value.Trim();

                //修改基本信息
                TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();
                TG.Model.cm_CustomerInfo model = bll.GetModel(int.Parse(str_cstid));
                model.Cst_Id = int.Parse(str_cstid);
                model.Cst_No = Cst_No;
                model.Cst_Brief = Cst_Brief;
                model.Cst_Name = Cst_Name;
                model.Cpy_Address = Cpy_Address;
                if (this.txtCode.Value.Trim() != "")
                {
                    model.Code = this.txtCode.Value.Trim();
                }
                model.Linkman = Linkman;
                model.Cpy_Phone = Cpy_Phone;
                model.Cpy_Fax = Cpy_Fax;
                model.UpdateBy = UserSysNo;
                model.LastUpdate = DateTime.Now;
                try
                {
                    bll.Update(model);


                }
                catch (System.Exception ex)
                {
                }
                //更新扩展信息
                TG.BLL.cm_CustomerExtendInfo bll_ext = new TG.BLL.cm_CustomerExtendInfo();
                TG.Model.cm_CustomerExtendInfo model_ext = ProcessExtendInfo();
                try
                {
                    if (str_extid == "")
                    {
                        model_ext.Cst_Id = int.Parse(str_cstid);
                        bll_ext.Add(model_ext);
                    }
                    else
                    {
                        model_ext.Cst_ExtendInfoId = int.Parse(str_extid);
                        bll_ext.Update(model_ext);

                    }
                }
                catch (System.Exception ex)
                { }
            }

            TG.Common.MessageBox.ResponseScriptBack(this, "保存成功！");
        }

        private Model.cm_CustomerExtendInfo ProcessExtendInfo()
        {
            // List<TG.Model.tg_member> sss = new List<TG.Model.tg_member>();

            string str_cur = UserSysNo.ToString();
            string str_cstid = Convert.ToString(ViewState["Cst_ID"] ?? "");
            TG.Model.cm_CustomerExtendInfo model = new TG.Model.cm_CustomerExtendInfo();
            if (str_cur != "")
            {
                model.Cst_EnglishName = this.txtCst_EnglishName.Value.Trim();
                model.IsPartner = Convert.ToInt32(this.ddIsPartner.SelectedValue.Trim());
                model.Country = this.hiddenCountry.Value.Trim();
                model.City = this.hiddenCity.Value.Trim();
                model.Province = this.hiddenProvince.Value.Trim();
                model.Type = Convert.ToInt32(this.ddType.SelectedValue.Trim());
                model.Profession = Convert.ToInt32(this.ddProfession.SelectedValue.Trim());
                model.BranchPart = this.txtBranchPart.Value.Trim();
                model.Email = this.txtEmail.Value.Trim();
                model.Cpy_PrincipalSheet = this.txtCpy_PrincipalSheet.Value.Trim();
                model.Cst_Id = int.Parse(str_cstid);
                model.LawPerson = this.txtLawPerson.Value.Trim();
                model.RelationDepartment = this.txtRelationDepartment.Value.Trim();
                model.Remark = this.txtRemark.InnerText.Trim();
                model.TaxAccountNo = this.txtTaxAccountNo.Value.Trim();
                model.UpdateBy = UserSysNo;
                model.LastUpdate = DateTime.Now;
                model.BankName = this.txtBankName.Value.Trim();
                if (this.txtBankAccountNo.Value.Trim() != "")
                {
                    model.BankAccountNo = this.txtBankAccountNo.Value.Trim();
                }
                else
                {
                    model.BankAccountNo = null;
                }
                model.CloseLeve = Convert.ToInt32(this.ddCloseLeve.SelectedValue.Trim());
                if (this.txtCpy_Code.Value.Trim() != "")
                {
                    model.Cpy_Code = Convert.ToString(this.txtCpy_Code.Value.Trim());
                } if (this.txtCreateRelationTime.Value.Trim() != "")
                {
                    model.CreateRelationTime = Convert.ToDateTime(this.txtCreateRelationTime.Value.Trim());
                }
                model.CreditLeve = Convert.ToInt32(this.ddCreditLeve.SelectedValue.Trim());
                ////开户银行大额联行号
                //model.BankBigNo = this.txtBankBigNo.Value.Trim();
            }
            return model;
        }
        //城市
        private void GetCountryList()
        {
            RepeaterCountry.DataSource = new TG.BLL.DropDownListCountryBP().GetCountryList(new TG.Model.DropDownListQueryEntity());
            RepeaterCountry.DataBind();
        }
        //绑定下拉项
        public void Bind_DdList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定客户类别
            string str_where = "dic_type='cst_type'";
            DataSet ds_cst = dic.GetList(str_where);
            ddType.DataValueField = "ID";
            ddType.DataTextField = "dic_name";
            ddType.DataSource = ds_cst;
            ddType.DataBind();
            // ddType.Items.Insert(0, new ListItem("----请选择----", "0"));
            //绑定所属行业
            DataSet ds_pro = dic.GetList("dic_type='cst_pro'");
            ddProfession.DataValueField = "ID";
            ddProfession.DataTextField = "dic_name";
            ddProfession.DataSource = ds_pro;
            ddProfession.DataBind();
            //ddProfession.Items.Insert(0, new ListItem("----请选择----", "0"));
            DataSet ds_credit = dic.GetList(" dic_type='cst_credit'");
            ddCreditLeve.DataValueField = "ID";
            ddCreditLeve.DataTextField = "dic_name";
            ddCreditLeve.DataSource = ds_credit;
            ddCreditLeve.DataBind();
            // ddCreditLeve.Items.Insert(0, new ListItem("----请选择----", "0"));
            //绑定亲密度
            DataSet ds_close = dic.GetList("dic_type='cst_close'");
            ddCloseLeve.DataValueField = "id";
            ddCloseLeve.DataTextField = "dic_name";
            ddCloseLeve.DataSource = ds_close;
            ddCloseLeve.DataBind();
            //  ddCloseLeve.Items.Insert(0, new ListItem("----请选择----", "0"));
        }

        /// <summary>
        /// 获取客户ID
        /// </summary>
        /// <returns></returns>
        public string getCst_Id()
        {
            return Convert.ToString(ViewState["Cst_ID"] ?? "");
        }
        /// <summary>
        /// 获取登录ID(登录界面开始进行重新设置.....)
        /// </summary>
        /// <returns></returns>
        public string GetUserFlag()
        {
            return UserSysNo.ToString();
        }
        /// <summary>
        /// 返回文件夹ID (若是根据登录ID,此项可不要？待商讨中.......)
        /// </summary>
        /// <returns></returns>
        public string GetParentID()
        {
            return UserShortName;
        }
        //绑定附件

        public void BindAttachInfo()
        {
            //string str_cstid = Convert.ToString(ViewState["Cst_ID"] ?? "");
            //TG.BLL.cm_AttachInfo attachbll = new TG.BLL.cm_AttachInfo();
            //StringBuilder strWhere = new StringBuilder();
            //strWhere.AppendFormat(" AND Cst_Id = {0}", str_cstid);
            ////数量
            //this.AspNetPager1.RecordCount = int.Parse(attachbll.GetListPageProcCount(strWhere.ToString()).ToString());
            ////分页
            //this.gv_Attach.DataSource = attachbll.GetListByPageProc(strWhere.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            //this.gv_Attach.DataBind();

        }
        //绑定合同
        public void BindContact()
        {
            TG.BLL.cm_Coperation bll_cpr = new TG.BLL.cm_Coperation();
            //合同ID
            string str_cstid = Convert.ToString(ViewState["Cst_ID"] ?? "0");
            string strWhere = " AND cst_Id=" + CustomerSysNo;
            //数量
            this.AspNetPager2.RecordCount = int.Parse(bll_cpr.GetListPageProcCount(strWhere).ToString());
            //分页
            this.gv_rContract.DataSource = bll_cpr.GetListByPageProc(strWhere, this.AspNetPager2.StartRecordIndex, this.AspNetPager2.EndRecordIndex);
            this.gv_rContract.DataBind();
        }
        /// <summary>
        /// 显示客户信息
        /// </summary>
        /// <param name="Cst_Id">客户ID</param>
        private void ShowInfo(int Cst_Id)
        {
            TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();
            TG.Model.cm_CustomerInfo model = bll.GetModel(Cst_Id);
            if (model != null)
            {

                this.txtCst_No.Value = model.Cst_No == null ? "" : model.Cst_No.Trim();
                this.txtCst_Brief.Value = model.Cst_Brief == null ? "" : model.Cst_Brief.Trim();
                this.txtCst_Name.Value = model.Cst_Name == null ? "" : model.Cst_Name.Trim();
                this.txtCpy_Address.Value = model.Cpy_Address == null ? "" : model.Cpy_Address.Trim();
                this.txtCode.Value = model.Code == null ? "" : model.Code.ToString().Trim();
                this.txtLinkman.Value = model.Linkman == null ? "" : model.Linkman.Trim();
                this.txtCpy_Phone.Value = model.Cpy_Phone == null ? "" : model.Cpy_Phone.Trim();
                this.txtCpy_Fax.Value = model.Cpy_Fax == null ? "" : model.Cpy_Fax.Trim();
            }
        }
        /// <summary>
        ///  显示客户的扩展信息
        /// </summary>
        /// <param name="Cst_Id">客户ID</param>
        protected void ShowExtendInfo(int Cst_Id)
        {
            TG.BLL.cm_CustomerExtendInfo cetbll = new TG.BLL.cm_CustomerExtendInfo();
            List<TG.Model.cm_CustomerExtendInfo> models = cetbll.GetModelList("Cst_Id=" + Cst_Id.ToString());
            if (models.Count > 0)
            {
                TG.Model.cm_CustomerExtendInfo model = models[0];
                this.txtCst_EnglishName.Value = model.Cst_EnglishName == null ? "" : model.Cst_EnglishName.ToString().Trim();
                this.ddIsPartner.SelectedValue = model.IsPartner.ToString();
                this.hiddenCountry.Value = Convert.ToString(model.Country ?? "").Trim();
                this.hiddenCity.Value = Convert.ToString(model.City ?? "").Trim();
                this.hiddenProvince.Value = Convert.ToString(model.Province ?? "").Trim();
                this.ddType.SelectedValue = model.Type.ToString();
                this.ddProfession.SelectedValue = model.Profession.ToString();
                this.txtBranchPart.Value = model.BranchPart == null ? "" : model.BranchPart.Trim();
                this.txtEmail.Value = model.Email == null ? "" : model.Email.Trim();
                this.txtCpy_PrincipalSheet.Value = model.Cpy_PrincipalSheet == null ? "" : model.Cpy_PrincipalSheet.Trim();
                this.txtLawPerson.Value = model.LawPerson == null ? "" : model.LawPerson.Trim();
                this.txtRelationDepartment.Value = model.RelationDepartment == null ? "" : model.RelationDepartment.Trim();
                this.txtRemark.Value = model.Remark == null ? "" : model.Remark.Trim();
                this.txtTaxAccountNo.Value = model.TaxAccountNo == null ? "" : model.TaxAccountNo.Trim();
                this.txtBankName.Value = model.BankName == null ? "" : model.BankName.Trim();
                this.txtBankAccountNo.Value = model.BankAccountNo == null ? "" : model.BankAccountNo.Trim();
                this.ddCloseLeve.SelectedValue = model.CloseLeve.ToString();
                this.txtCpy_Code.Value = model.Cpy_Code == null ? "" : model.Cpy_Code.Trim();
                this.txtCreateRelationTime.Value = model.CreateRelationTime == null ? "" : Convert.ToDateTime(model.CreateRelationTime).ToString("yyyy-MM-dd");
                this.ddCreditLeve.SelectedValue = model.CreditLeve.ToString();
                //开户银行大额联行号
                //this.txtBankBigNo.Value = model == null ? "" : model.BankBigNo.Trim();
                //扩展信息ID
                ViewState["Cstext_ID"] = model.Cst_ExtendInfoId;
            }
        }
        //行数据绑定判断
        protected void gv_Attach_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //文件大小
                string fsize = e.Row.Cells[2].Text;
                if (fsize.Trim() != "&nbsp;" && fsize.Trim() != "")
                {
                    fsize = Convert.ToString(int.Parse(fsize) / 1024) + "KB";
                }
                e.Row.Cells[2].Text = fsize;
                //用户
                string fuserid = e.Row.Cells[4].Text;
                if (fuserid.Trim() != "" && fuserid.Trim() != "&nbsp;")
                {
                    TG.Model.tg_member mem_model = new TG.BLL.tg_member().GetModel(int.Parse(fuserid));
                    if (mem_model != null)
                    {
                        fuserid = mem_model.mem_Name;
                    }
                }
                e.Row.Cells[4].Text = fuserid;
            }
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindAttachInfo();
        }

        protected void AspNetPager2_PageChanged(object src, EventArgs e)
        {
            BindContact();
        }
    }
}