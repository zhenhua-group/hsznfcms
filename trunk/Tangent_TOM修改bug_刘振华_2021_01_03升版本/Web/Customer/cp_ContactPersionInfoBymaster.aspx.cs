﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;

namespace TG.Web.Customer.ContactPersionInfo
{
    public partial class cp_ContactPersionInfoBymaster : PageBase
    {
        public string ColumnsContent
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定联系人
                BindContactPerson();
                //绑定字段
                BindColumns();
                if (base.RolePowerParameterEntity != null)
                {
                    this.userShortName.Value = base.UserShortName;
                    this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                    this.userSysNum.Value = base.UserSysNo.ToString();
                    this.userUnitNum.Value = base.UserUnitNo.ToString();
                }

            }
        }
        protected void BindColumns()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("姓名", "Name");
            dic.Add("职务", "Duties");
            dic.Add("部门", "Department");
            dic.Add("电话", "Phone");
            dic.Add("Email", "CP.Email");
            dic.Add("录入时间", "lrsj");
            dic.Add("编号", "Cst_No");
            dic.Add("客户名称", "Cst_Name");
            dic.Add("客户简称", "Cst_Brief");
            dic.Add("公司地址", "Cpy_Address");
            dic.Add("邮政编码", "Code");
            dic.Add("联系人", "Linkman");
            dic.Add("公司电话", "Cpy_Phone");
            dic.Add("传真号", "Cpy_Fax");
            dic.Add("客户英文名", "Cst_EnglishName");
            dic.Add("是否合作", "PartnerName");
            dic.Add("所在国家", "Country");
            dic.Add("所在省份", "Province");
            dic.Add("所在城市", "City");
            dic.Add("客户类型", "TypeName");
            dic.Add("所属行业", "ProfessionName");
            dic.Add("分支机构", "BranchPart");
            dic.Add("客户邮箱", "CX.Email");
            dic.Add("公司主页", "Cpy_PrincipalSheet");
            dic.Add("关系建立时间", "RelationTime");
            dic.Add("关系部门", "RelationDepartment");
            dic.Add("信用级别", "CreditLeveName");
            dic.Add("亲密度", "CloseLeveName");
            dic.Add("开户银行名称", "BankName");
            dic.Add("企业代码", "Cpy_Code");
            dic.Add("开户银行账号", "BankAccountNo");
            dic.Add("法定代表", "LawPerson");
            dic.Add("纳税人识别号", "TaxAccountNo");
            dic.Add("备注", "Remark");

            foreach (KeyValuePair<string, string> pair in dic)
            {
                ColumnsContent += "<label><input type='checkbox' value='" + pair.Value + "' />" + pair.Key + "</label>";
            }


            //  BindProInfoConfig(this.asTreeviewStruct.RootNode,dic);
            //this.asTreeviewStruct.CollapseAll();           
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //查看个人，部门权限判断
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND InsertUserID =" + UserSysNo);
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
            }
        }
        //绑定联系人
        public void BindContactPerson()
        {
            TG.BLL.cm_ContactPersionInfo cpbll = new TG.BLL.cm_ContactPersionInfo();
            DataSet ds = new DataSet();
            StringBuilder strWhere = new StringBuilder("");
            // if (this.txt_keyname.Value.Trim() != "")
            //   {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
            //    strWhere.AppendFormat(" and Name like '%{0}%'", keyname);
            // }
            //检查权限
            GetPreviewPowerSql(ref strWhere);

            this.hid_where.Value = strWhere.ToString();
            //所有记录数
            // this.AspNetPager1.RecordCount = int.Parse(cpbll.GetListPageProcCount(strWhere.ToString()).ToString());
            //排序
            // string orderString = " Order by " + OrderColumn + " " + Order;
            // strWhere.Append(orderString);
            //绑定数据
            //  this.gv_contactPerson.DataSource = cpbll.GetListByPageProc(strWhere.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            //  this.gv_contactPerson.DataBind();
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();

            string modelPath = " ~/TemplateXls/ContactPersionInfo.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Name"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Duties"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Department"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Phone"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Email"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["LastUpdate"]).ToString("yyyy-MM-dd"));

            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("联系人信息列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        /// <summary>
        /// 得到导出数据
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {

            TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();
            StringBuilder strWhere = new StringBuilder("");
            if (this.txt_customerNum.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_customerNum.Value.Trim());
                strWhere.AppendFormat(" and Name like '%{0}%'", keyname);
            }

            string startTime = txt_start.Value;//录入时间
            string endTime = txt_end.Value;//录入时间
            //录入时间  
            if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                strWhere.AppendFormat(" AND Cp.LastUpdate>='{0}' ", startTime + "  00:00:00");
            }
            else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                strWhere.AppendFormat(" AND Cp.LastUpdate<='{0}' ", endTime + " 23:59:59");
            }
            else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                strWhere.AppendFormat(" AND Cp.LastUpdate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
            }
           
            //检查权限
            GetPreviewPowerSql(ref strWhere);

            //所有记录数
            DataTable dt = bll.GetContactPersionInfoExportInfo(strWhere.ToString()).Tables[0];
            return dt;
        }
    }
}