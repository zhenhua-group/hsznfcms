﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="stf_ModifySatisfactionInfo.aspx.cs"
    Inherits="TG.Web.Customer.SatisfactionInfo.stf_ModifySatisfactionInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type="text/css" rel="Stylesheet" href="../../css/m_comm.css" />
    <link type="text/css" rel="Stylesheet" href="../../css/Customer.css" />
    <link type="text/css" rel="Stylesheet" href="../../css/jquery.alerts.css" />

    <script type="text/javascript" src="../../js/wdate/WdatePicker.js"></script>

    <script type="text/javascript" src="../../js/jquery-1.8.0.min.js"></script>

	<script type="text/javascript" src="../../js/jquery.alerts.js"></script>

	<script type="text/javascript" src="../../js/Customer/stf_ModifySatisfactionInfo.js"> </script>

</head>
<body style="background-color: #F0F0F0; font-size: 12px;">
	<form id="form1" runat="server">
	<%--<table class="cls_container">
		<tr>
			<td class="cls_head">
				&nbsp;&nbsp;当前位置：[客户管理]
			</td>
		</tr>
	</table>
	<table class="cls_show_cst_jiben">
		<tr>
			<td>
				序号：
			</td>
			<td>
				<asp:TextBox ID="txtSat_No" runat="server" Width="120px" Style="background: url(../images/bg_tdhead.gif) repeat-x;"
					CssClass="cls_input_text"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td>
				调查内容：
			</td>
			<td>
				<asp:TextBox ID="txtSch_Context" runat="server" Width="300px" Style="background: url(../images/bg_tdhead.gif) repeat-x;"
					CssClass="cls_input_text"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td>
				调查时间：
			</td>
			<td>
				<input type="text" name="txt_date" id="txtSch_Time" onclick="WdatePicker({readOnly:true})"
					class="Wdate" runat="Server" style="width: 120px;" />
			</td>
		</tr>
		<tr>
			<td>
				&nbsp; 满意度：
			</td>
			<td>
				<asp:DropDownList ID="ddSat_Leve" runat="Server" Width="60px">
					<asp:ListItem Value="1">满意</asp:ListItem>
					<asp:ListItem Value="2">非常满意</asp:ListItem>
					<asp:ListItem Value="3">不满意</asp:ListItem>
					<asp:ListItem Value="4">非常不满意</asp:ListItem>
				</asp:DropDownList>
			</td>
		</tr>
		<tr>
			<td>
				调查原因：
			</td>
			<td>
				<asp:TextBox ID="txtSch_Reason" runat="server" Width="300px" CssClass="cls_input_text"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td>
				调查方式：
			</td>
			<td>
				<asp:DropDownList ID="ddSat_Way" runat="Server" Width="80px">
					<asp:ListItem Value="1">问卷调查</asp:ListItem>
					<asp:ListItem Value="2">统计调查</asp:ListItem>
					<asp:ListItem Value="3">询问调查</asp:ListItem>
					<asp:ListItem Value="4">电话调查</asp:ListItem>
				</asp:DropDownList>
			</td>
		</tr>
		<tr>
			<td>
				&nbsp; 最满意方面：
			</td>
			<td>
				<asp:TextBox ID="txtSat_Best" runat="server" Width="300px" CssClass="cls_input_text"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td>
				最不满意方面：
			</td>
			<td>
				<asp:TextBox ID="txtSat_NotBest" runat="server" Width="300px" CssClass="cls_input_text"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td>
				备注：
			</td>
			<td>
				<asp:TextBox ID="txtRemark" runat="server" Height="50px" TextMode="MultiLine" CssClass="cls_input_text_w"></asp:TextBox>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<asp:ImageButton ID="btn_Save" runat="server" ImageUrl="~/Images/btn_save.gif" OnClick="btn_Save_Click" />
				&nbsp;<asp:ImageButton ID="btn_back" runat="server" ImageUrl="~/Images/btn_back.gif"
					OnClick="btn_back_Click" />
			</td>
		</tr>
	</table>--%>
	</form>
</body>
</html>
