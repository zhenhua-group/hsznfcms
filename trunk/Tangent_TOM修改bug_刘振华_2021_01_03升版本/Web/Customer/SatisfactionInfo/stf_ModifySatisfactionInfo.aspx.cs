﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;
using Newtonsoft.Json;


namespace TG.Web.Customer.SatisfactionInfo
{
    public partial class stf_ModifySatisfactionInfo : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.HttpMethod == "GET")
            {
                //请求原始数据
                ShowInfo();
            }
            else
            {
                //修改保存
                btn_Save_Click();
            }
            Response.End();
        }

        /// <summary>
        /// 显示满意度调查信息
        /// </summary>
        /// <param name="Sat_Id">满意度信息ID</param>
        /// <param name="cst_id">输出客户信息ID</param>
        private void ShowInfo()
        {
            TG.BLL.cm_SatisfactionInfo bll = new TG.BLL.cm_SatisfactionInfo();
            TG.Model.cm_SatisfactionInfo model = bll.GetModel(int.Parse(Request["id"]));

            string jsonResult = Newtonsoft.Json.JsonConvert.SerializeObject(model);

            Response.Write(jsonResult);
        }
        //添加
        protected void btn_Save_Click()
        {
            //当前用户
            int i_cur = UserSysNo;
            int i_satid = Convert.ToInt32(Request["Sat_Id"] ?? "0");
            //更新类型 zxq 20140121
            decimal i_cstid = Convert.ToDecimal(Request["customerSysNo"] ?? "0");

            string Sat_No = Request["txtSat_No"] ?? "0";
            string Sch_Context = Request["Sch_Context"] ?? "0";
            int Sat_Leve = Convert.ToInt32(Request["ddSat_Leve"]);
            DateTime Sch_Time = DateTime.Parse(Request["txtSch_Time"]);
            int Sch_Way = Convert.ToInt32(Request["ddSat_Way"]);
            string Sch_Reason = Request["txtSch_Reason"] ?? "0";
            string Sat_Best = Request["txtSat_Best"] ?? "0";
            string Sat_NotBest = Request["txtSat_NotBest"] ?? "0";
            string Remark = Request["Sat_Remark"] ?? "0";
            int UpdateBy = i_cur;
            DateTime LastUpdate = DateTime.Now;

            TG.Model.cm_SatisfactionInfo model = new TG.Model.cm_SatisfactionInfo();
            model.Sat_Id = i_satid;
            model.Cst_Id = i_cstid;
            model.Sat_No = Sat_No;
            model.Sch_Context = Sch_Context;
            model.Sat_Leve = Sat_Leve;
            model.Sch_Time = Sch_Time;
            model.Sch_Way = Sch_Way;
            model.Sch_Reason = Sch_Reason;
            model.Sat_Best = Sat_Best;
            model.Sat_NotBest = Sat_NotBest;
            model.Remark = Remark;
            model.UpdateBy = UpdateBy;
            model.LastUpdate = LastUpdate;

            TG.BLL.cm_SatisfactionInfo bll = new TG.BLL.cm_SatisfactionInfo();
            bll.Update(model);
            decimal id = i_cstid;
            Response.Write(id);

        }
    }
}
