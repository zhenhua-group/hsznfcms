﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;

namespace TG.Web.Customer.SatisfactionInfo
{
    public partial class stf_AddSatisfactionInfo : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //        ViewState["Cst_ID"] = Request.QueryString["Cst_Id"] ?? "0";
            //}
            btn_Save_Click();
        }


        protected void btn_Save_Click()
        {
            int count = 0;

            //获取当前用户
            int i_cur = UserSysNo;
            //更新类型 zxq20140121
            decimal i_cstid = Convert.ToDecimal(Request["customerSysNo"] ?? "0");

            string Sat_No = Request["txtSat_No"];
            string Sch_Context = Request["Sch_Context"];
            int Sat_Leve = int.Parse(Request["ddSat_Leve"].ToString());
            DateTime Sch_Time = DateTime.Parse(Request["txtSch_Time"].ToString());
            int Sch_Way = int.Parse(Request["ddSat_Way"].ToString());
            string Sch_Reason = Request["txtSch_Reason"].ToString();
            string Sat_Best = Request["txtSat_Best"].ToString();
            string Sat_NotBest = Request["txtSat_NotBest"].ToString();
            string Remark = Request["Sat_Remark"].ToString();
            int UpdateBy = i_cur;
            DateTime LastUpdate = DateTime.Now;

            TG.Model.cm_SatisfactionInfo model = new TG.Model.cm_SatisfactionInfo();
            model.Cst_Id = i_cstid;
            model.Sat_No = Sat_No;
            model.Sch_Context = Sch_Context;
            model.Sat_Leve = Sat_Leve;
            model.Sch_Time = Sch_Time;
            model.Sch_Way = Sch_Way;
            model.Sch_Reason = Sch_Reason;
            model.Sat_Best = Sat_Best;
            model.Sat_NotBest = Sat_NotBest;
            model.Remark = Remark;
            model.UpdateBy = UpdateBy;
            model.LastUpdate = LastUpdate;

            TG.BLL.cm_SatisfactionInfo bll = new TG.BLL.cm_SatisfactionInfo();
            try
            {
                count = bll.Add(model);
            }
            catch (System.Exception ex)
            {

            }
            if (count > 0)
            {
                Response.Write("1");
            }
            else
            {
                Response.Write("0");
            }
            Response.End();
            //TG.Common.MessageBox.ShowAndRedirect(this, "保存成功！", "../cst_ModifyCustomerInfo.aspx?id=" + i_cstid);
        }
    }
}
