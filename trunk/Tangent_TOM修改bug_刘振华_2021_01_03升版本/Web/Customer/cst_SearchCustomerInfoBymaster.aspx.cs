﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;

namespace TG.Web.Customer
{
    public partial class cst_SearchCustomerInfoBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_DdList();
                BindData();
                if (base.RolePowerParameterEntity != null)
                {
                    this.userShortName.Value = base.UserShortName;
                    this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                    this.userSysNum.Value = base.UserSysNo.ToString();
                    this.userUnitNum.Value = base.UserUnitNo.ToString();
                }
            }
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //浏览权限控制
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND ( InsertUserID =" + UserSysNo + ")");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND ( InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + "))");
            }
        }
        //绑定客户类型数据
        public void Bind_DdList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定客户类别
            string str_where = "dic_type='cst_type'";
            DataSet ds_cst = dic.GetList(str_where);
            ddType.DataValueField = "ID";
            ddType.DataTextField = "dic_name";
            ddType.DataSource = ds_cst;
            ddType.DataBind();

            //绑定所属行业
            DataSet ds_pro = dic.GetList("dic_type='cst_pro'");
            ddProfession.DataValueField = "ID";
            ddProfession.DataTextField = "dic_name";
            ddProfession.DataSource = ds_pro;
            ddProfession.DataBind();
        }
        public void BindData()
        {
            string strWhere = "";
            TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();

            //限制浏览
            StringBuilder sb = new StringBuilder(strWhere);
            //检查权限
            GetPreviewPowerSql(ref sb);

            this.hid_where.Value = strWhere.ToString();


        }


        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();

            string modelPath = " ~/TemplateXls/CustomerListInfo.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cst_Name"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Linkman"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cpy_Phone"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cst_No"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cpy_Address"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Code"].ToString());

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cpy_Fax"].ToString());

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["InsertUser"].ToString());
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.Write(memoryStream);
                string name = System.Web.HttpContext.Current.Server.UrlEncode("客户信息列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        /// <summary>
        /// 得到导出数据
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {

            string strWhere = "";
            TG.BLL.cm_CustomerInfo bll = new TG.BLL.cm_CustomerInfo();
            //高级查询AND
            if (this.txtCst_No.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtCst_No.Value.Trim());
                strWhere += " AND C.Cst_No LIKE '%" + keyname + "%' ";
            }
            if (this.txtCst_Brief.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtCst_Brief.Value.Trim());
                strWhere += " AND C.Cst_Brief LIKE '%" + keyname + "%' ";
            }
            if (this.txtCst_Name.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtCst_Name.Value.Trim());
                strWhere += " AND C.Cst_Name LIKE '%" + keyname + "%' ";
            }
            if (this.ddType.SelectedIndex != 0)
            {
                strWhere += " AND CX.Type =" + this.ddType.SelectedItem.Value + " ";
            }
            if (this.ddProfession.SelectedIndex != 0)
            {
                strWhere += " AND CX.Profession=" + this.ddProfession.SelectedItem.Value + " ";
            }
            if (this.txtCity.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtCity.Value.Trim());
                strWhere += " AND CX.City LIKE '%" + keyname + "%' ";
            }
            if (this.txtLinkman.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtLinkman.Value.Trim());
                strWhere += " AND C.LinkMan LIKE '%" + keyname + "%' ";
            }
            if (this.txtProvince.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtProvince.Value.Trim());
                strWhere += " AND CX.Province LIKE '%" + keyname + "%' ";
            }
            if (this.txtCpy_Address.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txtCpy_Address.Value.Trim());
                strWhere += " AND C.Cpy_Address LIKE '%" + keyname + "%' ";
            }
          
            //限制浏览
            StringBuilder sb = new StringBuilder(strWhere);
            //检查权限
            GetPreviewPowerSql(ref sb);
            //所有记录数

            //所有记录数
            DataTable dt = bll.GetCustomerExportInfo(strWhere.ToString()).Tables[0];
            return dt;
        }
    }
}