﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Text;

namespace TG.Web.Customer
{
    public partial class ExportToExcel : System.Web.UI.Page
    {
        /// <summary>
        /// 客户系统自增号String，以逗号分隔
        /// </summary>
        public string[] CustomerSysNoArr
        {
            get
            {
                object customerSysNoString = Request["CustomerSysNoString"];
                if (customerSysNoString == null)
                {
                    return null;
                }
                else
                {
                    string[] tempArr = customerSysNoString.ToString().Split(',');
                    return tempArr;
                }
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {

            //字段名称
            string str_columnsname = Request["str_columnsname"];
            //字段名称标题
            string str_columnschinaname = Request["str_columnschinaname"];

            string unit = Request.QueryString["unit"];
            string year = Request.QueryString["year"];
            string keyname = Request.QueryString["keyname"];
            string startTime = Request.QueryString["startTime"];//录入时间
            string endTime = Request.QueryString["endTime"];//录入时间
            string strwhere = System.Web.HttpUtility.UrlDecode(Request.QueryString["strwhere"]);
            //查询方式
            string andor = Request.QueryString["andor"] ?? "and";
            StringBuilder strsql = new StringBuilder("");
            //按名称查询
            if (keyname.Trim() != "")
            {
                keyname = TG.Common.StringPlus.SqlSplit(keyname);
                strsql.AppendFormat(" " + andor + " Cst_Name like '%{0}%'", keyname);
            }
            //按生产部门查询
            if (unit != "-1")
            {
                strsql.AppendFormat(" " + andor + " InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID={0})", unit);
            }
            //年份
            if (year != "-1")
            {
                strsql.AppendFormat(" " + andor + " year(InsertDate)={0} ", year);
            }

            //录入时间
            if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                strsql.AppendFormat(" " + andor + " InsertDate>='{0}' ", startTime + "  00:00:00");
            }
            else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                strsql.AppendFormat(" " + andor + " InsertDate<='{0}' ", endTime + " 23:59:59");
            }
            else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                strsql.AppendFormat(" " + andor + " InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
            }

            //客户编码
            string custNo = Request.QueryString["custNo"];
            if (!string.IsNullOrEmpty(custNo))
            {
                strsql.Append(" " + andor + " Cst_No like '%" + custNo.Trim() + "%'  ");
            }

            //联系人
            var linkMan = Request.QueryString["linkMan"].Trim();
            if (!string.IsNullOrEmpty(linkMan))
            {
                strsql.Append(" " + andor + " Linkman like '%" + linkMan.Trim() + "%'  ");
            }
            //联系电话
            var linkPhone = Request.QueryString["linkPhone"];
            if (!string.IsNullOrEmpty(linkPhone))
            {
                strsql.Append(" " + andor + " Cpy_Phone like '%" + linkPhone.Trim() + "%'  ");
            }

            //公司地址
            var address = Request.QueryString["address"];
            if (!string.IsNullOrEmpty(address))
            {
                strsql.Append(" " + andor + " Cpy_Address like '%" + address.Trim() + "%'  ");
            }

            //邮编
            var code = Request.QueryString["code"];
            if (!string.IsNullOrEmpty(code))
            {
                strsql.Append(" " + andor + " Code like '%" + code.Trim() + "%'  ");
            }
            //传真号
            var cpyFax = Request.QueryString["cpyFax"];
            if (!string.IsNullOrEmpty(cpyFax))
            {
                strsql.Append(" " + andor + " Cpy_Fax like '%" + cpyFax.Trim() + "%'  ");
            }

            //录入人
            var insertName = Request.QueryString["insertName"];
            if (!string.IsNullOrEmpty(insertName))
            {
                strsql.Append(" " + andor + "  InsertUserID in (select mem_id from tg_member where mem_Name like '%" + insertName + "%') ");
            }

            //客户简称
            var cst_Brief = Request.QueryString["cst_Brief"].Trim();
            if (!string.IsNullOrEmpty(cst_Brief))
            {
                strsql.Append(" " + andor + " Cst_Brief like '%" + cst_Brief.Trim() + "%'  ");
            }

            //客户英文名称
            var cstEnglishName = Request.QueryString["cstEnglishName"];
            if (!string.IsNullOrEmpty(cstEnglishName))
            {
                strsql.Append(" " + andor + " Cst_EnglishName like '%" + cstEnglishName.Trim() + "%'  ");
            }
            //是否合作
            var isPartner = Request.QueryString["isPartner"].Trim();
            if (!string.IsNullOrEmpty(isPartner) && isPartner != "-1")
            {
                strsql.Append(" " + andor + " IsPartner=" + isPartner + "  ");
            }

            //所在国家
            var country = Request.QueryString["country"];
            if (!string.IsNullOrEmpty(country))
            {
                strsql.Append(" " + andor + " Country like '%" + country.Trim() + "%'  ");
            }

            //所在省份
            var province = Request.QueryString["province"];
            if (!string.IsNullOrEmpty(province))
            {
                strsql.Append(" " + andor + " Province like '%" + province.Trim() + "%'  ");
            }
            //所在城市
            var city = Request.QueryString["city"];
            if (!string.IsNullOrEmpty(city))
            {
                strsql.Append(" " + andor + " City like '%" + city.Trim() + "%'  ");
            }

            //客户类型
            var cstType = Request.QueryString["cstType"].Trim();
            if (!string.IsNullOrEmpty(cstType) && cstType.Trim() != "-1")
            {
                strsql.Append(" " + andor + " Type =" + cstType + " ");
            }

            //所属行业
            var profession = Request.QueryString["profession"].Trim();
            if (!string.IsNullOrEmpty(profession) && profession != "-1")
            {
                strsql.Append(" " + andor + " Profession =" + profession + " ");
            }

            //分支机构
            var branchPart = Request.QueryString["branchPart"];
            if (!string.IsNullOrEmpty(branchPart))
            {
                strsql.Append(" " + andor + " BranchPart like '%" + branchPart.Trim() + "%'  ");
            }

            //邮箱
            var email = Request.QueryString["email"];
            if (!string.IsNullOrEmpty(email))
            {
                strsql.Append(" " + andor + " Email like '%" + email.Trim() + "%'  ");
            }
            //公司主页
            var principalSheet = Request.QueryString["principalSheet"];
            if (!string.IsNullOrEmpty(principalSheet))
            {
                strsql.Append(" " + andor + " Cpy_PrincipalSheet like '%" + principalSheet.Trim() + "%'  ");
            }

            //关系建立时间
            var date_Start = Request.QueryString["date_Start"].Trim();
            var date_End = Request.QueryString["date_End"].Trim();

            if (!string.IsNullOrEmpty(date_Start) && string.IsNullOrEmpty(date_End))
            {
                strsql.AppendFormat(" " + andor + " CreateRelationTime>='{0}' ", date_Start + "  00:00:00");
            }
            else if (string.IsNullOrEmpty(date_Start) && !string.IsNullOrEmpty(date_End))
            {
                strsql.AppendFormat(" " + andor + " CreateRelationTime<='{0}' ", date_End + " 23:59:59");
            }
            else if (!string.IsNullOrEmpty(date_Start) && !string.IsNullOrEmpty(date_End))
            {
                strsql.AppendFormat(" " + andor + " CreateRelationTime between '{0}' and '{1}' ", date_Start + "  00:00:00", date_End + " 23:59:59");
            }

            //关系部门
            var relationDepartment = Request.QueryString["relationDepartment"];
            if (!string.IsNullOrEmpty(relationDepartment))
            {
                strsql.Append(" " + andor + " RelationDepartment like '%" + relationDepartment.Trim() + "%'  ");
            }

            //信用级别
            var creditLeve = Request.QueryString["creditLeve"];
            if (!string.IsNullOrEmpty(creditLeve) && creditLeve != "-1")
            {
                strsql.Append(" " + andor + " CreditLeve =" + creditLeve + " ");
            }
            //亲密度
            var closeLeve = Request.QueryString["closeLeve"];
            if (!string.IsNullOrEmpty(closeLeve) && closeLeve != "-1")
            {
                strsql.Append(" " + andor + " CloseLeve =" + closeLeve + " ");
            }

            //开户银行
            var bankName = Request.QueryString["bankName"];
            if (!string.IsNullOrEmpty(bankName))
            {
                strsql.Append(" " + andor + " BankName like '%" + bankName.Trim() + "%'  ");
            }

            //企业代码
            var cpy_Code = Request.QueryString["cpy_Code"];
            if (!string.IsNullOrEmpty(cpy_Code))
            {
                strsql.Append(" " + andor + " Cpy_Code like '%" + cpy_Code.Trim() + "%'  ");
            }
            //开户银行账号
            var bankAccountNo = Request.QueryString["bankAccountNo"];
            if (!string.IsNullOrEmpty(bankAccountNo))
            {
                strsql.Append(" " + andor + " BankAccountNo like '%" + bankAccountNo.Trim() + "%'  ");
            }

            //法定代表
            var lawPerson = Request.QueryString["lawPerson"];
            if (!string.IsNullOrEmpty(lawPerson))
            {
                strsql.Append(" " + andor + " LawPerson like '%" + lawPerson.Trim() + "%'  ");
            }

            //纳税人识别号
            var taxAccountNo = Request.QueryString["taxAccountNo"];
            if (!string.IsNullOrEmpty(taxAccountNo))
            {
                strsql.Append(" " + andor + " TaxAccountNo like '%" + taxAccountNo.Trim() + "%'  ");
            }

            //备注
            var remark = Request.QueryString["remark"];
            if (!string.IsNullOrEmpty(remark))
            {
                strsql.Append(" " + andor + " Remark like '%" + remark.Trim() + "%'  ");
            }

            string whereTemp = strsql.ToString();

            if (andor.Contains("or"))
            {
                if (!string.IsNullOrEmpty(whereTemp))
                {
                    whereTemp = " and ( " + whereTemp.Trim().Substring(2) + ")";
                }
            }

            whereTemp = whereTemp + strwhere;

            ToExcelExport(whereTemp, str_columnsname, str_columnschinaname);
            // GetCustomerRecord();
        }

        private void ToExcelExport(string strWhere, string str_columnsname, string str_columnschinaname)
        {

            DataTable dt = new TG.BLL.cm_CustomerInfo().GetCustomerExportInfo(strWhere).Tables[0];

            string modelPath = " ~/TemplateXls/CustomerListInfo.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            ICellStyle style0 = wb.CreateCellStyle();
            style0.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style0.VerticalAlignment = VerticalAlignment.CENTER;
            style0.WrapText = true;

            IFont font0 = wb.CreateFont();
            font0.FontHeightInPoints = 12;//字号
            font0.FontName = "宋体";//字体
            font0.Boldweight = (short)700;
            style0.SetFont(font0);


            //标题样式
            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 12;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
           
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            //读取标题
            IRow dataRowTitle = ws.GetRow(1);
            dataRowTitle.GetCell(0).CellStyle = style1;
            dataRowTitle.GetCell(0).SetCellValue("序号");
            if (!string.IsNullOrEmpty(str_columnschinaname))
            {
                string[] columnsnamelist = str_columnschinaname.Split(',');
                for (int j = 0; j < columnsnamelist.Length; j++)
                {
                    ICell celltitle = dataRowTitle.CreateCell(j + 1);
                    celltitle.CellStyle = style1;          
                    celltitle.SetCellValue(columnsnamelist[j].Trim());
                }
            }
            int row = 2;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);


                if (!string.IsNullOrEmpty(str_columnsname))
                {
                    string[] columnslist = str_columnsname.Split(',');
                    for (int j = 0; j < columnslist.Length; j++)
                    {
                        cell = dataRow.CreateCell(j + 1);
                        cell.CellStyle = style2;
                        string cellvalue = "";
                        cellvalue = dt.Rows[i][columnslist[j].Trim()].ToString();
                        
                        cell.SetCellValue(cellvalue);
                    }
                }
            }

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("客户信息列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }


        }

        /// <summary>
        /// 查询客户信息
        /// </summary>
        private void GetCustomerRecord()
        {
            TG.BLL.cm_CustomerInfo customerInfoBLL = new TG.BLL.cm_CustomerInfo();
            TG.BLL.cm_CustomerExtendInfo customerExtendInfoBLL = new TG.BLL.cm_CustomerExtendInfo();

            //循环拼接WhereSql
            string wherreSql = " AND Cst_Id IN(";
            foreach (string customerSysNo in CustomerSysNoArr)
            {
                wherreSql += customerSysNo + ",";
            }
            wherreSql = wherreSql.Substring(0, wherreSql.Length - 1) + ")";
            //查询用户数据
            System.Data.DataSet recordTable = customerInfoBLL.GetCustomerRecord(wherreSql);
            if (recordTable.Tables.Count > 0)
                ExportDataToExcel(recordTable.Tables[0]);
        }

        private void ExportDataToExcel(DataTable table)
        {
            HSSFWorkbook excel = new HSSFWorkbook();
            ISheet sheet = excel.CreateSheet("sheet1");

            //生成数据列头
            IRow headRow = sheet.CreateRow(0);

            for (int i = 0; i < table.Rows.Count; i++)
            {
                IRow dataRow = sheet.CreateRow(i + 1);
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    if (i == 0)
                    {
                        headRow.CreateCell(j).SetCellValue(table.Columns[j].ColumnName);
                    }
                    dataRow.CreateCell(j).SetCellValue(table.Rows[i][j].ToString().Trim());
                }
            }

            using (MemoryStream memoryStream = new MemoryStream())
            {
                excel.Write(memoryStream);
                Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss")));
                Response.ContentType = "application/ms-excel";
                Response.BinaryWrite(memoryStream.ToArray());
                excel = null;
                Response.End();
            }
        }


        ///// 导出DataSet数据到Excel
        ///// </summary>
        ///// <param name="dataSet">数据源（要导出的数据源，类型应是DataSet）</param>
        ///// <param name="filePath">保存文件的路径 例：'C：\\Files\\'</param>
        ///// <param name="fileName">要保存的文件名，需要加扩展有.xls 例：'Test.xls'</param>
        //public void ExportDataToExcel(DataSet dataSet)
        //{
        //        string path = Server.MapPath("/") + "TemplateXls\\CustomerTempExcel.xls";
        //        if (dataSet.Tables.Count == 0)
        //        {
        //                throw new Exception("DataSet中没有任何可导出的表。");
        //        }

        //        //建立一个Excel进程 Application
        //        Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
        //        //默认值为 True。如果不想在宏运行时被无穷无尽的提示和警告消息所困扰，请将本属性设置为 False；这样每次出现需用户应答的消息时，Microsoft Excel
        //        // 将选择默认应答。 
        //        //如果将该属性设置为 False，则在代码运行结束后，Micorosoft Excel 将该属性设置为 True，除非正运行交叉处理代码。 
        //        //如果使用工作簿的 SaveAs 方法覆盖现有文件，“覆盖”警告默认为“No”，当 DisplayAlerts 属性值设置为 True 时，Excel 选择“Yes”。
        //        excelApplication.DisplayAlerts = false;
        //        //  建立或打开一个 Workbook对象生成新Workbook
        //        Microsoft.Office.Interop.Excel.Workbook workbook = excelApplication.Workbooks.Add(Missing.Value);

        //        foreach (System.Data.DataTable dt in dataSet.Tables)
        //        {
        //                Microsoft.Office.Interop.Excel.Worksheet lastWorksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets.get_Item(workbook.Worksheets.Count);
        //                Microsoft.Office.Interop.Excel.Worksheet newSheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets.Add(Type.Missing, lastWorksheet, Type.Missing, Type.Missing);

        //                newSheet.Name = dt.TableName;
        //                //获取DataSet的列名，并创建标题行
        //                for (int col = 0; col < dt.Columns.Count; col++)
        //                {
        //                        newSheet.Cells[1, col + 1] = dt.Columns[col].ColumnName;
        //                        newSheet.get_Range(newSheet.Cells[1, col + 1], newSheet.Cells[1, dt.Columns.Count]).Columns.AutoFit();
        //                }
        //                //通过循环把数据添加到Sheet里面
        //                for (int row = 0; row < dt.Rows.Count; row++)
        //                {

        //                        for (int col = 0; col < dt.Columns.Count; col++)
        //                        {
        //                                newSheet.Cells[row + 2, col + 1] = dt.Rows[row][col].ToString();

        //                        }
        //                }
        //        }

        //        //删除原来的空Sheet
        //        ((Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets.get_Item(1)).Delete();
        //        ((Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets.get_Item(1)).Delete();
        //        ((Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets.get_Item(1)).Delete();
        //        //设置默认选中是第一个Sheet 类似于Select();
        //        ((Microsoft.Office.Interop.Excel.Worksheet)workbook.Worksheets.get_Item(1)).Activate();
        //        try
        //        {
        //                workbook.Close(true, path, System.Reflection.Missing.Value);
        //        }
        //        catch (Exception e)
        //        {
        //                throw e;
        //        }
        //        UploadExcel(path);
        //        excelApplication.Quit();

        //        KillExcel();
        //}

        ///// <summary>
        ///// 用来结束所以的创建的Excel进程
        ///// </summary>
        //private void KillExcel()
        //{
        //        //Process 提供对本地和远程进程的访问并使您能够启动和停止本地系统进程。
        //        //Process.GetProcessesByName() 获取进程名为Excel的进程
        //        Process[] excelProcesses = Process.GetProcessesByName("EXCEL");
        //        DateTime startTime = new DateTime();

        //        int processId = 0;
        //        for (int i = 0; i < excelProcesses.Length; i++)
        //        {
        //                if (startTime < excelProcesses[i].StartTime)
        //                {
        //                        startTime = excelProcesses[i].StartTime;
        //                        processId = i;
        //                }
        //        }
        //        //杀掉进程
        //        if (excelProcesses[processId].HasExited == false)
        //        {
        //                excelProcesses[processId].Kill();
        //        }
        //}
        ///// <summary>
        ///// 提供下载
        ///// </summary>
        ///// <param name="path"></param>
        ///// <param name="page"></param>
        /////<param name="isDelete"></param>
        //private void UploadExcel(string path)
        //{
        //        System.IO.FileInfo file = new System.IO.FileInfo(path);
        //        Response.Clear();
        //        Response.Charset = "GB2312";
        //        Response.ContentEncoding = System.Text.Encoding.UTF8;
        //        // 添加头信息，为"文件下载/另存为"对话框指定默认文件名 
        //        Response.AddHeader("Content-Disposition", "attachment; filename=" + Server.UrlEncode(file.Name));
        //        // 添加头信息，指定文件大小，让浏览器能够显示下载进度 
        //        Response.AddHeader("Content-Length", file.Length.ToString());

        //        // 指定返回的是一个不能被客户端读取的流，必须被下载 
        //        Response.ContentType = "application/ms-excel";

        //        // 把文件流发送到客户端 
        //        Response.WriteFile(file.FullName);

        //        Response.Flush();
        //        System.IO.File.Delete(path);

        //        // 停止页面的执行 
        //        Response.End();
        //}
    }
}
