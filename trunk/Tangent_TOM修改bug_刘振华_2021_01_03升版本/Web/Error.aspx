﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="TG.Web.Error" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/m_comm.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#btn_back").click(function () {
                location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=A";
            })
        })
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;&nbsp;当前位置：[申请修改策划消息]
            </td>
        </tr>
    </table>
    <center style="color: Red">
        <asp:Label Text="text" runat="server" ID="Msg" /></center>
    <center>
        <input type="button" value="返回" id="btn_back" /></center>
    </form>
</body>
</html>
