﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjBpm
{
    public partial class SetMemsPersent : PageBase
    {
        public bool t = true;
        /// <summary>
        /// 项目ID
        /// </summary>
        public int ProID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["proid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 项目考核ID
        /// </summary>
        public int ProKHID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["prokhid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 考核名称ID
        /// </summary>
        public int KaoHeNameID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["khnameid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 考核类型ID
        /// </summary>
        public int KaoHeTypeID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["khtypeid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 负责人设置记录ID
        /// </summary>
        public int ID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["id"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 专业
        /// </summary>
        public int SpeID
        {
            get
            {
                int speid = 0;
                int.TryParse(Request["speid"] ?? "0", out speid);
                return speid;
            }
        }
        /// <summary>
        /// 专业名称
        /// </summary>
        public string SpeName
        {
            get
            {
                return new TG.BLL.tg_speciality().GetModel(this.SpeID).spe_Name;
            }
        }

        /// <summary>
        /// 考核项目
        /// </summary>
        public TG.Model.cm_Project ProjectModel
        {
            get
            {
                return new TG.BLL.cm_Project().GetModel(this.ProID);
            }
        }
        /// <summary>
        /// 考核名称
        /// </summary>
        public TG.Model.cm_KaoHeProjName KaoHeNameModel
        {
            get
            {
                var model = new TG.BLL.cm_KaoHeProjName().GetModel(this.KaoHeNameID);
                if (model == null)
                {
                    model = new TG.Model.cm_KaoHeProjName()
                    {
                        KName = "<此考核名称已经删除！>",
                        ID = 0
                    };
                }
                return model;
            }
        }
        /// <summary>
        /// 获取分配
        /// </summary>
        public TG.Model.cm_KaoheSpecMemsFz KaoHeSpeMem
        {
            get
            {
                string strWhere = string.Format(" ProKHNameId={0} AND MemID={1} AND SpeID={2}", this.KaoHeNameID, this.UserSysNo, this.SpeID);
                var modellist = new TG.BLL.cm_KaoheSpecMemsFz().GetModelList(strWhere);
                return modellist[0];
            }
        }
        /// <summary>
        /// 获取已经保存的数据
        /// </summary>
        public List<TG.Model.cm_KaoHeSpeMemsPersent> KaoHeMemsPrt
        {
            get
            {
                //专业负责人角色
                int speid = 0;
                if (Setup == 1)
                {
                    speid = KaoHeSpeMem.SpeID;
                }
                string strWhere = string.Format(" KaoHeNameID={0} AND InsertUser1={1} AND SpeID={2}", this.KaoHeNameID, this.UserSysNo, speid);
                if (Setup == 2 || Setup == 3)
                {
                    strWhere = string.Format(" KaoHeNameID={0} AND SpeID={1} ", this.KaoHeNameID, this.SpeID);
                }
                var list = new TG.BLL.cm_KaoHeSpeMemsPersent().GetModelList(strWhere).OrderBy(c => c.ID).ToList();
                return list;
            }
        }
        /// <summary>
        /// 是否已经保存有数据
        /// </summary>
        public bool IsSave
        {
            get
            {
                //负责人填写
                int speid = 0;
                if (Setup == 1)
                {
                    speid = KaoHeSpeMem.SpeID;
                }
                string strWhere = string.Format(" KaoHeNameID={0} AND InsertUser1={1} AND SpeID={2}", this.KaoHeNameID, this.UserSysNo, speid);
                if (Setup == 2)
                {
                    strWhere = string.Format(" KaoHeNameID={0} AND InsertUser2={1} AND SpeID={2}", this.KaoHeNameID, this.UserSysNo, this.SpeID);
                }
                if (Setup == 3)
                {
                    strWhere = string.Format(" KaoHeNameID={0} AND InsertUser3={1} AND SpeID={2}", this.KaoHeNameID, this.UserSysNo, this.SpeID);
                }
                var list = new TG.BLL.cm_KaoHeSpeMemsPersent().GetModelList(strWhere);
                if (list.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// 是否已经提交
        /// </summary>
        public bool IsSubmit
        {
            get
            {
                //负责人填写
                if (t)
                {
                    int speid = 0;
                    if (Setup == 1)
                    {
                        speid = KaoHeSpeMem.SpeID;
                    }
                    string strWhere = string.Format(" KaoHeNameID={0} AND InsertUser1={1}  AND SpeID={2} AND Stat=1", this.KaoHeNameID, this.UserSysNo, speid);
                    if (Setup == 2)
                    {
                        strWhere = string.Format(" KaoHeNameID={0} AND InsertUser2={1}  AND SpeID={2} AND Stat2=1", this.KaoHeNameID, this.UserSysNo, this.SpeID);
                    }
                    if (Setup == 3)
                    {
                        strWhere = string.Format(" KaoHeNameID={0} AND InsertUser3={1}  AND SpeID={2} AND Stat3=1", this.KaoHeNameID, this.UserSysNo, this.SpeID);
                    }
                    var list = new TG.BLL.cm_KaoHeSpeMemsPersent().GetModelList(strWhere);
                    if (list.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    int speid = 0;
                    if (Setup == 1)
                    {
                        speid = KaoHeSpeMem.SpeID;
                    }
                    string strWhere = string.Format(" KaoHeNameID={0} AND InsertUser1={1}  AND SpeID={2} AND Stat=1", this.KaoHeNameID, this.UserSysNo, speid);
                    if (Setup == 2)
                    {
                        strWhere = string.Format(" KaoHeNameID={0} AND InsertUser2={1}  AND SpeID={2} AND Stat2=1", this.KaoHeNameID, this.UserSysNo, this.SpeID);
                    }
                    if (Setup == 3)
                    {
                        strWhere = string.Format(" KaoHeNameID={0} AND InsertUser3={1}  AND SpeID={2} AND Stat3=1", this.KaoHeNameID, this.UserSysNo, this.SpeID);
                    }
                    var list = new TG.BLL.cm_KaoHeSpeMemsPersent().GetModelList(strWhere);
                    if (list.Count > 0)
                    {
                        return false;
                    }
                    else
                    {
                        return false;
                    }
                }
                
            }
        }
        /// <summary>
        /// 当前审批步骤
        /// </summary>
        public int CurSetUp
        {
            get
            {
                int setup = 0;
                int.TryParse(Request["setup"] ?? "0", out setup);
                return setup;
            }
        }
        /// <summary>
        /// 流程到哪一步了
        /// </summary>
        public int Setup
        {
            get
            {
                int setup = 0;
                int.TryParse(Request["setup"] ?? "0", out setup);
                return setup;
            }

            #region 更具状态判断阶段
            //get
            //{
            //    int setup = 1;
            //    //查询此名称下的具体专业
            //    string strWhere = string.Format(" KaoHeNameID={0} AND SpeID={1} ", this.KaoHeNameID, this.SpeID);
            //    var list = new TG.BLL.cm_KaoHeSpeMemsPersent().GetModelList(strWhere).OrderBy(c => c.ID).ToList();
            //    if (list.Count > 0)
            //    {
            //        if (list[0].Stat == 1)
            //        {
            //            setup = 2;
            //        }
            //        if (list[0].Stat2 == 1)
            //        {
            //            setup = 3;
            //            if (this.SpeID > 1)
            //            {
            //                setup = 2;
            //            }
            //        }
            //        if (list[0].Stat3 == 1)
            //        {
            //            setup = 3;
            //        }
            //    }

            //    return setup;
            //}
            #endregion
        }
        /// <summary>
        /// 获取专业主持人得分
        /// </summary>
        public decimal? SpeZhuChiCount
        {
            get
            {
                decimal? count = 0;

                //直接通过 KaoheSpecMems ID来查询主持人的评分
                //var mem = new TG.BLL.cm_KaoheSpecMems().GetModel(this.ID);
                //if (mem != null)
                //{
                //    count = mem.Jixiao;
                //}
                //else
                //{
                string strWhere = string.Format(" ProKHNameId={0} AND SpeID={1} ", this.KaoHeNameID, this.SpeID);
                var list = new TG.BLL.cm_KaoheSpecMems().GetModelList(strWhere);
                if (list.Count > 0)
                {
                    count = list[0].Jixiao;
                }
                //}
                return count;
            }
        }
        /// <summary>
        /// 消息ID
        /// </summary>
        protected int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["MsgNo"] ?? "0", out msgid);
                return msgid;
            }
        }

        /// <summary>
        /// 是否显示关闭自己
        /// </summary>
        public string IsClose
        {
            get
            {
                string isclose = Request["isclose"] ?? "";

                if (isclose == "true")
                {
                    return "yes";
                }
                else
                {
                    return "none";
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_Export_Clickcx(object sender, EventArgs e)
        {

            t = false;

        }
    }
}