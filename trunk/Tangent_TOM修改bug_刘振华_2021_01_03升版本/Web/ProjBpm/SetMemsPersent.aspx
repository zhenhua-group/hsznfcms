﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="SetMemsPersent.aspx.cs" Inherits="TG.Web.ProjBpm.SetMemsPersent" %>

<%@ Register Src="../UserControl/UserOfTheDepartmentTree.ascx" TagName="UserOfTheDepartmentTree"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/UserControl/UserOfTheDepartmentTree.js" type="text/javascript"></script>

    <script src="../js/ProjBpm/SetMemsPersent.js"></script>
    <script src="../js/MessageComm.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目考核 <small>设置人员比例</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目考核</a><i class="fa fa-angle-right"> </i><a>设置人员间比例</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>专业内个人比例划分
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <% if ((Setup == 1 && KaoHeSpeMem.SpeID == 1) || (Setup > 1 && SpeID == 1))//建筑专业审批
                           {%>

                        <table class="table table-bordered table-hover" id="tbData">
                            <thead>
                                <tr>
                                    <% if (Setup > 1)
                                       { %>
                                           <%
                                           if (KaoHeNameModel.KName.Contains("室外工程") || KaoHeNameModel.KName.Contains("室外工程(密云小镇)"))
                                        { %>
                                        <th colspan="7">
                                            <%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-info"><%= KaoHeNameModel.KName%></span>)<%= SpeName %>专业内比例划分</th>
                                          
                                        
                                      <%  }
                                          else
                                          {%>
                                          <th colspan="7"><%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)<%= SpeName %>专业内比例划分</th>
                                        <%}
                                     %>
                                          
                                 <%}
                                       else
                                       { %>


                                       <%
                                           if (KaoHeNameModel.KName.Contains("室外工程") || KaoHeNameModel.KName.Contains("室外工程(密云小镇)"))
                                        { %>
                                        <th colspan="7">
                                            <%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-info"><%= KaoHeNameModel.KName%></span>)<%= KaoHeSpeMem.SpeName %>专业内比例划分</th>
                                         
                                      <%  }
                                          else
                                          {%>
                                          <th colspan="7"><%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)<%= KaoHeSpeMem.SpeName %>专业内比例划分</th>
                                        <%}
                                     %>


                                   <%-- <th colspan="7"><%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)<%= KaoHeSpeMem.SpeName %>专业内比例划分</th>--%>
                                    <%} %>
                                </tr>
                                <tr>
                                    <th colspan="7">
                                        <% if (!IsSubmit)
                                           {
                                               if (Setup == 1 || Setup == 2 || Setup == 3)//增加经理审批时可以编辑功能 2016-8-24
                                               {%>
                                        <a class="btn btn-sm red" href="#UserOfTheDepartmentTreeDiv" data-toggle="modal" action="selectUser"><i class="fa fa-plus"></i>添加专业人员</a>
                                        <a class="btn btn-sm blue" href="#" id="btnClear">全部清空</a>
                                        <% }
                                               else
                                               {
                                        %>

                                        <a class="btn btn-sm red disabled" href="#UserOfTheDepartmentTreeDiv" data-toggle="modal" action="selectUser"><i class="fa fa-plus"></i>添加专业人员</a>
                                        <a class="btn btn-sm blue disabled" href="#" id="btnClear">全部清空</a>
                                        <%
                                               }
                                           }
                                           else
                                           {%>
                                        <a class="btn btn-sm red disabled" href="#UserOfTheDepartmentTreeDiv" data-toggle="modal" action="selectUser"><i class="fa fa-plus"></i>添加专业人员</a>
                                        <a class="btn btn-sm blue disabled" href="#" id="btnClear">全部清空</a>
                                        <%} %>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 100px;">姓名</th>
                                    <th style="width: 100px;">制图</th>
                                    <th style="width: 100px;">校对</th>
                                    <th style="width: 100px;">审核</th>
                                    <th style="width: 100px;">方案深化</th>
                                    <th style="width: 100px;">主持人</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                                <%
                                    
                                           decimal? hj = 0;
                                           decimal? hj2 = 0;
                                           decimal? hj3 = 0;
                                           decimal? hj4 = 0;
                                           decimal? hj5 = 0;
                                           if (Setup == 2 || Setup == 3)//部门经理与总建筑审批时
                                           {



                                               bool issubmit = IsSubmit;
                                               KaoHeMemsPrt.ForEach(c =>
                                               {
                                                   if (!issubmit)
                                                   {%>
                                <tr>
                                    <td><span class="badge badge-info" userspecialtyno="<%= c.SpeID%>" usersysno="<%=c.MemID%>" userspecialtyname="<%=c.SpeName%>"><%=c.MemName%></span></td>
                                    <td>
                                        <% hj += c.Zt; %>
                                        <input colindex="1" style="width: 80px;" name="name" type="text" value="<%=c.Zt%>" />%</td>
                                    <td>
                                        <% hj2 += c.Xd; %>
                                        <input colindex="2" style="width: 80px;" name="name" type="text" value="<%=c.Xd%>" />%</td>
                                    <td>
                                        <% hj3 += c.Sh; %>
                                        <input colindex="3" style="width: 80px;" name="name" type="text" value="<%=c.Sh%>" />%</td>
                                    <td>
                                        <% hj4 += c.Fa; %>
                                        <input colindex="4" style="width: 80px;" name="name" type="text" value="<%=c.Fa%>" />%</td>
                                    <td>
                                        <% hj5 += c.Zc; %>
                                        <input colindex="5" style="width: 80px;" name="name" type="text" value="<%=c.Zc%>" />%</td>
                                    <td><a href="#" class="btn default btn-xs red-stripe" action="del">删除</a></td>
                                </tr>
                                <%}
                                                   else
                                                   {%>
                                <tr>
                                    <td><span class="badge badge-info" userspecialtyno="<%= c.SpeID%>" usersysno="<%=c.MemID%>" userspecialtyname="<%=c.SpeName%>"><%=c.MemName%></span></td>
                                    <td>
                                        <% hj += c.Zt; %>
                                        <span class="badge badge-success" colindex="1"><%=c.Zt%></span>%</td>
                                    <td>
                                        <% hj2 += c.Xd; %>
                                        <span class="badge badge-success" colindex="2"><%=c.Xd%></span>%</td>
                                    <td>
                                        <% hj3 += c.Sh; %>
                                        <span class="badge badge-success" colindex="3"><%=c.Sh%></span>%</td>
                                    <td>
                                        <% hj4 += c.Fa; %>
                                        <span class="badge badge-success" colindex="4"><%=c.Fa%></span>%</td>
                                    <td>
                                        <% hj5 += c.Zc; %>
                                        <span class="badge badge-success" colindex="5"><%=c.Zc%></span>%</td>
                                    <td><a href="#" class="btn default btn-xs red-stripe disabled" action="del">删除</a></td>
                                </tr>
                                <%}
                                               });%>


                                <%
                                           }
                                           else
                                           {

                                               //专业负责人
                                               if (IsSave)
                                               {


                                                   bool issubmit = IsSubmit;
                                                   KaoHeMemsPrt.ForEach(c =>
                                                   {
                                                       if (!issubmit)
                                                       {%>
                                <tr>
                                    <td><span class="badge badge-info" userspecialtyno="<%= c.SpeID%>" usersysno="<%=c.MemID%>" userspecialtyname="<%=c.SpeName%>"><%=c.MemName%></span></td>
                                    <td>
                                        <% hj += c.Zt; %>
                                        <input colindex="1" style="width: 80px;" name="name" type="text" value="<%=c.Zt%>" />%</td>
                                    <td>
                                        <% hj2 += c.Xd; %>
                                        <input colindex="2" style="width: 80px;" name="name" type="text" value="<%=c.Xd%>" />%</td>
                                    <td>
                                        <% hj3 += c.Sh; %>
                                        <input colindex="3" style="width: 80px;" name="name" type="text" value="<%=c.Sh%>" />%</td>
                                    <td>
                                        <% hj4 += c.Fa; %>
                                        <input colindex="4" style="width: 80px;" name="name" type="text" value="<%=c.Fa%>" />%</td>
                                    <td>
                                        <% hj5 += c.Zc; %>
                                        <input colindex="5" style="width: 80px;" name="name" type="text" value="<%=c.Zc%>" />%</td>
                                    <td><a href="#" class="btn default btn-xs red-stripe" action="del">删除</a></td>
                                </tr>
                                <%}
                                                       else
                                                       {%>
                                <tr>
                                    <td><span class="badge badge-info" userspecialtyno="<%= c.SpeID%>" usersysno="<%=c.MemID%>" userspecialtyname="<%=c.SpeName%>"><%=c.MemName%></span></td>
                                    <td>
                                        <% hj += c.Zt; %>
                                        <span class="badge badge-success" colindex="1"><%=c.Zt%></span>%</td>
                                    <td>
                                        <% hj2 += c.Xd; %>
                                        <span class="badge badge-success" colindex="2"><%=c.Xd%></span>%</td>
                                    <td>
                                        <% hj3 += c.Sh; %>
                                        <span class="badge badge-success" colindex="3"><%=c.Sh%></span>%</td>
                                    <td>
                                        <% hj4 += c.Fa; %>
                                        <span class="badge badge-success" colindex="4"><%=c.Fa%></span>%</td>
                                    <td>
                                        <% hj5 += c.Zc; %>
                                        <span class="badge badge-success" colindex="5"><%=c.Zc%></span>%</td>
                                    <td><a href="#" class="btn default btn-xs red-stripe disabled" action="del">删除</a></td>
                                </tr>
                                <%}
                                                   });
                                               }
                                           }%>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>合计</td>
                                    <%if (IsSave)
                                      { %>
                                    <td><span id="spcount1"><%= Convert.ToDecimal(hj).ToString("f2") %></span>%</td>
                                    <td><span id="spcount2"><%= Convert.ToDecimal(hj2).ToString("f2") %></span>%</td>
                                    <td><span id="spcount3"><%= Convert.ToDecimal(hj3).ToString("f2") %></span>%</td>
                                    <td><span id="spcount4"><%= Convert.ToDecimal(hj4).ToString("f2") %></span>%</td>
                                    <td><span id="spcount5"><%= Convert.ToDecimal(hj5).ToString("f2") %></span>%</td>
                                    <%}
                                      else if (Setup > 1)
                                      { 
                                    %>
                                    <td><span id="spcount1"><%= Convert.ToDecimal(hj).ToString("f2") %></span>%</td>
                                    <td><span id="spcount2"><%= Convert.ToDecimal(hj2).ToString("f2") %></span>%</td>
                                    <td><span id="spcount3"><%= Convert.ToDecimal(hj3).ToString("f2") %></span>%</td>
                                    <td><span id="spcount4"><%= Convert.ToDecimal(hj4).ToString("f2") %></span>%</td>
                                    <td><span id="spcount5"><%= Convert.ToDecimal(hj5).ToString("f2") %></span>%</td>
                                    <%}
                                      else
                                      { %>
                                    <td><span id="spcount1">0</span>%</td>
                                    <td><span id="spcount2">0</span>%</td>
                                    <td><span id="spcount3">0</span>%</td>
                                    <td><span id="spcount4">0</span>%</td>
                                    <td><span id="spcount5">0</span>%</td>
                                    <%} %>
                                </tr>
                                <tr>
                                    <td colspan="7">
                                        <% if (!IsSubmit)
                                           { %>
                                        <a href="#" class="btn btn-sm red" id="btnSave">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmit">确认提交</a>
                                        <span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>
                                        <%}
                                      else{ %>
                                        <a href="/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=A" class="btn btn-sm default" id="btnCancel">取消</a>

                                        <a style="display: <%= IsClose%>;" href="javascript:this.close();" class="btn btn-sm red">关闭</a>

                                        <%} %>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                        <%}
                           else //其他下行专业的填写与审批
                           { %>

                        <table class="table table-bordered table-hover" id="tbData">
                            <thead>
                                <tr>
                                    <% if (Setup > 1)
                                       { %>

                                     <%
                                           if (KaoHeNameModel.KName.Contains("室外工程") || KaoHeNameModel.KName.Contains("室外工程(密云小镇)"))
                                        { %>
                                        <th colspan="5">
                                            <%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-info"><%= KaoHeNameModel.KName%></span>)<%= SpeName %>专业内比例划分</th>
                                         
                                      <%  }
                                          else
                                          {%>
                                          <th colspan="5"><%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)<%= SpeName %>专业内比例划分</th>
                                        <%}
                                     %>



                                    <%--<th colspan="5"><%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)<%= SpeName %>专业内比例划分</th>--%>
                                    <%}
                                       else
                                       { %>
                                    <%
                                           if (KaoHeNameModel.KName.Contains("室外工程") || KaoHeNameModel.KName.Contains("室外工程(密云小镇)"))
                                        { %>
                                        <th colspan="5">
                                            <%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-info"><%= KaoHeNameModel.KName%></span>)<%= KaoHeSpeMem.SpeName %>专业内比例划分</th>
                                         
                                      <%  }
                                          else
                                          {%>
                                          <th colspan="5"><%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)<%= KaoHeSpeMem.SpeName %>专业内比例划分</th>
                                        <%}
                                     %>

                                    <%--<th colspan="5"><%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)<%= KaoHeSpeMem.SpeName %>专业内比例划分</th>--%>
                                    <%} %>
                                </tr>
                                <tr>
                                    <th colspan="5">
                                        <% if (!IsSubmit)
                                           {
                                               if (Setup == 1 || Setup == 2)//增加经理审批时可以编辑功能 2016-8-24
                                               {%>
                                        <a class="btn btn-sm red" href="#UserOfTheDepartmentTreeDiv" data-toggle="modal" action="selectUser"><i class="fa fa-plus"></i>添加专业人员</a>
                                        <a class="btn btn-sm blue" href="#" id="btnClear">全部清空</a>
                                        <% }
                                               else
                                               {
                                        %>

                                        <a class="btn btn-sm red disabled" href="#UserOfTheDepartmentTreeDiv" data-toggle="modal"><i class="fa fa-plus"></i>添加专业人员</a>
                                        <a class="btn btn-sm blue disabled" href="#" id="btnClear">全部清空</a>
                                        <%
                                               }
                                           }
                                           else
                                           {%>
                                        <a class="btn btn-sm red disabled" href="#UserOfTheDepartmentTreeDiv" data-toggle="modal"><i class="fa fa-plus"></i>添加专业人员</a>
                                        <a class="btn btn-sm blue disabled" href="#" id="btnClear">全部清空</a>
                                        <%} %>
                                    </th>
                                </tr>
                                <tr>
                                    <th><span class="badge badge-info">主持人工作绩效:</span></th>
                                    <th>

                                        <%
                               if (!IsSubmit)
                               { %>
                                        <select id="selJixiao">
                                            <%if (SpeZhuChiCount > 0)
                                              { %>
                                            <option value="<%= SpeZhuChiCount %>" selected="selected"><%= SpeZhuChiCount %></option>
                                            <%} %>
                                            <option value="0.5">0.5</option>
                                            <option value="0.6">0.6</option>
                                            <option value="0.7">0.7</option>
                                            <option value="0.8">0.8</option>
                                            <option value="0.9">0.9</option>
                                            <%if (SpeZhuChiCount > 0)
                                              { %>
                                            <option value="1.0">1.0</option>
                                            <%}
                                              else
                                              { %>
                                            <option value="1.0" selected="selected">1.0</option>
                                            <%} %>
                                            <option value="1.1">1.1</option>
                                            <option value="1.2">1.2</option>
                                            <option value="1.3">1.3</option>
                                            <option value="1.4">1.4</option>
                                            <option value="1.5">1.5</option>
                                        </select>
                                        <%}
                               else
                               {%>
                                        <span class="badge badge-info"><%= SpeZhuChiCount %></span>
                                        <%} %>
                                    </th>
                                    <th colspan="3">1表示合格，区间0.5~1.5，请根据实际情况打分</th>

                                </tr>
                                <tr>
                                    <th style="width: 150px;">姓名</th>
                                    <th style="width: 150px;">制图</th>
                                    <th style="width: 150px;">校对</th>
                                    <th style="width: 150px;">审核</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                               decimal? hjt = 0;
                               decimal? hjt2 = 0;
                               decimal? hjt3 = 0;
                               if (Setup == 2 || Setup == 3)//部门经理与总建筑审批时
                               {
                                   bool issubmit = IsSubmit;
                                   KaoHeMemsPrt.ForEach(c =>
                                   {
                                       if (!issubmit)
                                       {%>
                                <tr>
                                    <td><span class="badge badge-info" userspecialtyno="<%= c.SpeID%>" usersysno="<%=c.MemID%>" userspecialtyname="<%=c.SpeName%>"><%=c.MemName%></span></td>
                                    <td>
                                        <% hjt += c.Zt; %>
                                        <input colindex="1" style="width: 80px;" name="name" type="text" value="<%=c.Zt%>" />%</td>
                                    <td>
                                        <% hjt2 += c.Xd; %>
                                        <input colindex="2" style="width: 80px;" name="name" type="text" value="<%=c.Xd%>" />%</td>
                                    <td>
                                        <% hjt3 += c.Sh; %>
                                        <input colindex="3" style="width: 80px;" name="name" type="text" value="<%=c.Sh%>" />%</td>
                                    <%--<td>
                                        <input colindex="4" style="width: 80px;" name="name" type="text" value="<%=c.Fa%>" />%</td>
                                    <td>
                                        <input colindex="5" style="width: 80px;" name="name" type="text" value="<%=c.Zc%>" />%</td>--%>
                                    <td><a href="#" class="btn default btn-xs red-stripe" action="del">删除</a></td>
                                </tr>
                                <%}
                                       else
                                       {%>
                                <tr>
                                    <td><span class="badge badge-info" userspecialtyno="<%= c.SpeID%>" usersysno="<%=c.MemID%>" userspecialtyname="<%=c.SpeName%>"><%=c.MemName%></span></td>
                                    <td>
                                        <% hjt += c.Zt; %>
                                        <span class="badge badge-success" colindex="1"><%=c.Zt%></span>%</td>
                                    <td>
                                        <% hjt2 += c.Xd; %>
                                        <span class="badge badge-success" colindex="2"><%=c.Xd%></span>%</td>
                                    <td>
                                        <% hjt3 += c.Sh; %>
                                        <span class="badge badge-success" colindex="3"><%=c.Sh%></span>%</td>
                                    <%--<td>
                                        <span class="badge badge-success" colindex="4"><%=c.Fa%></span>%</td>
                                    <td>
                                        <span class="badge badge-success" colindex="5"><%=c.Zc%></span>%</td>--%>
                                    <td><a href="#" class="btn default btn-xs red-stripe disabled" action="del">删除</a></td>
                                </tr>
                                <%}
                                   });%>


                                <%
                               }
                               else
                               {
                                   //专业负责人
                                   if (IsSave)
                                   {
                                       bool issubmit = IsSubmit;
                                       KaoHeMemsPrt.ForEach(c =>
                                       {
                                           if (!issubmit)
                                           {%>
                                <tr>
                                    <td><span class="badge badge-info" userspecialtyno="<%= c.SpeID%>" usersysno="<%=c.MemID%>" userspecialtyname="<%=c.SpeName%>"><%=c.MemName%></span></td>
                                    <td>
                                        <% hjt += c.Zt; %>
                                        <input colindex="1" style="width: 80px;" name="name" type="text" value="<%=c.Zt%>" />%</td>
                                    <td>
                                        <% hjt2 += c.Xd; %>
                                        <input colindex="2" style="width: 80px;" name="name" type="text" value="<%=c.Xd%>" />%</td>
                                    <td>
                                        <% hjt3 += c.Sh; %>
                                        <input colindex="3" style="width: 80px;" name="name" type="text" value="<%=c.Sh%>" />%</td>
                                    <%--<td>
                                        <input colindex="4" style="width: 80px;" name="name" type="text" value="<%=c.Fa%>" />%</td>
                                    <td>
                                        <input colindex="5" style="width: 80px;" name="name" type="text" value="<%=c.Zc%>" />%</td>--%>
                                    <td><a href="#" class="btn default btn-xs red-stripe" action="del">删除</a></td>
                                </tr>
                                <%}
                                           else
                                           {%>
                                <tr>
                                    <td><span class="badge badge-info" userspecialtyno="<%= c.SpeID%>" usersysno="<%=c.MemID%>" userspecialtyname="<%=c.SpeName%>"><%=c.MemName%></span></td>
                                    <td>
                                        <% hjt += c.Zt; %>
                                        <span class="badge badge-success" colindex="1"><%=c.Zt%></span>%</td>
                                    <td>
                                        <% hjt2 += c.Xd; %>
                                        <span class="badge badge-success" colindex="2"><%=c.Xd%></span>%</td>
                                    <td>
                                        <% hjt3 += c.Sh; %>
                                        <span class="badge badge-success" colindex="3"><%=c.Sh%></span>%</td>
                                    <%--<td>
                                        <span class="badge badge-success" colindex="4"><%=c.Fa%></span>%</td>
                                    <td>
                                        <span class="badge badge-success" colindex="5"><%=c.Zc%></span>%</td>--%>
                                    <td><a href="#" class="btn default btn-xs red-stripe disabled" action="del">删除</a></td>
                                </tr>
                                <%}
                                       });
                                   }
                               }%>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>合计</td>
                                    <%if (IsSave)
                                      { %>
                                    <td><span id="spcount1"><%= Convert.ToDecimal(hjt).ToString("f2") %></span>%</td>
                                    <td><span id="spcount2"><%= Convert.ToDecimal(hjt2).ToString("f2") %></span>%</td>
                                    <td><span id="spcount3"><%= Convert.ToDecimal(hjt3).ToString("f2") %></span>%</td>
                                    <td></td>
                                    <%}
                                      else if (Setup > 1)
                                      { 
                                    %>
                                    <td><span id="spcount1"><%= Convert.ToDecimal(hjt).ToString("f2") %></span>%</td>
                                    <td><span id="spcount2"><%= Convert.ToDecimal(hjt2).ToString("f2") %></span>%</td>
                                    <td><span id="spcount3"><%= Convert.ToDecimal(hjt3).ToString("f2") %></span>%</td>
                                    <td></td>

                                    <%}
                                      else
                                      { %>
                                    <td><span id="spcount1">0</span>%</td>
                                    <td><span id="spcount2">0</span>%</td>
                                    <td><span id="spcount3">0</span>%</td>
                                    <td></td>

                                    <%} %>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <% if (!IsSubmit)
                                           { %>
                                        <a href="#" class="btn btn-sm red" id="btnSave">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmit">确认提交</a>
                                        <span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>
                                        <%}else{ %>
                                        <a href="/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=A" class="btn btn-sm default" id="btnCancel">取消</a>
                                         <asp:Button  ID="Button1cx" CssClass="btn  red" runat="server" Text="撤回" OnClick="btn_Export_Clickcx" />
                                         
                                        <a style="display: <%= IsClose%>;" href="javascript:this.close();" class="btn btn-sm red">关闭</a>

                                        <%} %>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <%} %>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--弹出层-->
    <!--选择用户-->
    <div id="UserOfTheDepartmentTreeDiv" class="modal fade yellow" tabindex="-1" data-width="460"
        aria-hidden="true" style="display: none; width: 460px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择用户</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="ChooseUserOfTherDepartmentContainer">
                        <div style="overflow: auto; height: 300px;">
                            <uc1:UserOfTheDepartmentTree ID="UserOfTheDepartmentTree1" runat="server" IsRadio="false" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn
    green btn-default"
                id="btn_OK">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>


    <!--隐藏域-->
    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo" />
    <input type="hidden" name="name" value="<%= KaoHeNameID %>" id="hidKHNameID" />
    <input type="hidden" name="name" value="<%= ProID %>" id="hidProId" />
    <input type="hidden" name="name" value="<%= ProKHID %>" id="hidProKHID" />

    <%if (Setup > 1)
      { %>
    <input type="hidden" name="name" value="<%= SpeName %>" id="hidSpeName" />
    <input type="hidden" name="name" value="<%= SpeID %>" id="hidSpeID" />
    <%}
      else
      {%>
    <input type="hidden" name="name" value="<%= KaoHeSpeMem.SpeName %>" id="hidSpeName" />
    <input type="hidden" name="name" value="<%= KaoHeSpeMem.SpeID %>" id="hidSpeID" />
    <%} %>
    <input type="hidden" name="name" value="<%= Setup %>" id="hidSetup" />
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <!--当前用户部门ID-->
    <input type="hidden" name="name" value="<%= UserUnitNo %>" id="hidUnitSysNo" />

    <%--<input type="hidden" name="name" value="<%= CurSetUp %>" id="hidCurSetup" />--%>
    <input type="hidden" name="name" value="<%= KaoHeNameModel.ID %>" id="hidKaoHeNameID" />

    <input type="hidden" name="name" value="<%= IsSubmit %>" id="hidIsSubmit" />

    <script>
        SetMems.init();
    </script>

</asp:Content>
