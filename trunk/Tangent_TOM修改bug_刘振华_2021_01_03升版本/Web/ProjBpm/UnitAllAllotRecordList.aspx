﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UnitAllAllotRecordList.aspx.cs" Inherits="TG.Web.ProjBpm.UnitAllAllotRecordList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">部门考核 <small>年度总奖金分配</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>部门考核</a><i class="fa fa-angle-right"> </i><a>年度总奖金分配</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>部门奖金计算
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <%--<table class="table table-hover table-bordered table-full-width" >
                            <tr>
                                <td style="width: 80px;">考核任务:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drpRenwu" runat="server" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 150px;">
                                    <asp:Button CssClass="btn btn-sm red" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" />

                                </td>
                                <td></td>
                            </tr>
                        </table>--%>

                        <table class="table table-bordered table-hover" id="tbDataOne">
                            <thead>
                                <tr>
                                    <th style="width: 100px;">考核名称</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                                <% RenwuList.ForEach(c =>
                                   { 
                                %>

                                <tr>
                                    <td><%= c.RenWuNo %></td>
                                    <td>
                                        <% if (AllotRenwuIDs.Contains(Convert.ToInt32(c.ID)))
                                           { %>
                                        <a class="btn default btn-xs red-stripe" href="/DeptBpm/UnitAllAllotRecord.aspx?renwuno=<%= c.ID %>&action=4&check=chk">查看</a>
                                        <%}
                                           else
                                           { %>
                                        <a class="btn default btn-xs " href="#">无分配数据</a>
                                        <%} %>
                                    </td>
                                    <td></td>
                                </tr>
                                <%}); %>
                            </tbody>

                            <%--<tbody>
                                <% 
                                    RecordList.ForEach(c =>
                                    { %>


                                <tr>
                                    <td><%= c.RenwuName%></td>
                                    <td>
                                        <a class="btn default btn-xs red-stripe" href="/DeptBpm/UnitAllAllotRecord.aspx?renwuno=<%= c.RenwuID %>&action=4&check=chk">查看</a>
                                    </td>
                                    <td></td>
                                </tr>
                                <%});%>
                            </tbody>--%>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
