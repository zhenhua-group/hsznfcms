﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjBpm
{
    public partial class ProjDeptAllotList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRenwu();
            }
        }
        /// <summary>
        /// 考核任务
        /// </summary>
        public List<TG.Model.cm_KaoHeRenWu> RenwuList
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list;
            }
        }
        public List<TG.Model.cm_KaoHeRenWu> RenwuList2
        {
            get
            {
                string where = "";
                if(this.drpRenwu.SelectedIndex>0)
                {
                    where = " ID=" + this.drpRenwu.SelectedValue;
                }
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList(where).OrderByDescending(c => c.ID).ToList();
                return list;
            }
        }
        /// <summary>
        /// 绑定考核任务
        /// </summary>
        protected void BindRenwu()
        {
            if (RenwuList.Count > 0)
            {
                RenwuList.ForEach(c =>
                {
                    this.drpRenwu.Items.Add(new ListItem(c.RenWuNo, c.ID.ToString()));
                });
            }
        }
        public List<TG.Model.cm_KaoHeMemsAllotChangeList> AllotChangeList
        {
            get
            {
                string renwuid = this.drpRenwu.SelectedValue;
                string strWhere = " 1=1 ";
                strWhere += " AND RenwuID=" + renwuid;

                var list = new TG.BLL.cm_KaoHeMemsAllotChangeList().GetModelList(strWhere);

                return list;
            }
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {

        }
    }
}