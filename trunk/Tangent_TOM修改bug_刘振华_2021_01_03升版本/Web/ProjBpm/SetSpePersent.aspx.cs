﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjBpm
{
    public partial class SetSpePersent : PageBase
    {

        //proid=503&prokhid=62&khnameid=2&khtypeid=10&id=7
        /// <summary>
        /// 项目ID
        /// </summary>
        public int ProID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["proid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 项目考核ID
        /// </summary>
        public int ProKHID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["prokhid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 考核名称ID
        /// </summary>
        public int KaoHeNameID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["khnameid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 考核类型ID
        /// </summary>
        public int KaoHeTypeID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["khtypeid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 负责人设置记录ID
        /// </summary>
        public int ID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["id"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 考核项目
        /// </summary>
        public TG.Model.cm_Project ProjectModel
        {
            get
            {
                return new TG.BLL.cm_Project().GetModel(this.ProID);
            }
        }
        /// <summary>
        /// 考核名称
        /// </summary>
        public TG.Model.cm_KaoHeProjName KaoHeNameModel
        {
            get
            {
                var model = new TG.BLL.cm_KaoHeProjName().GetModel(this.KaoHeNameID);
                if (model == null)
                {
                    model = new TG.Model.cm_KaoHeProjName()
                    {
                        KName = "<此考核名称已经删除！>",
                        ID = 0
                    };
                }
                return model;
            }
        }
        /// <summary>
        /// 考核比例
        /// </summary>
        public TG.Model.cm_KaoHeProjTypeEnum KaoHeTypeEnum
        {
            get
            {
                TG.Model.cm_KaoHeProjTypeEnum result = new Model.cm_KaoHeProjTypeEnum();
                TG.BLL.cm_KaoheProjType blltype = new BLL.cm_KaoheProjType();
                TG.BLL.cm_KaoHeProjTypeEnum bllnum = new BLL.cm_KaoHeProjTypeEnum();
                //判断考核名称下有几个类型
                var typelist = new TG.BLL.cm_KaoheProjType().GetModelList(" KaoheNameID=" + this.KaoHeNameID + "");
                if (typelist.Count > 1)
                {
                    decimal? count1 = 0;
                    decimal? count2 = 0;
                    decimal? count3 = 0;
                    decimal? count4 = 0;
                    decimal? count5 = 0;
                    //虚拟合计
                    decimal? allCount = 0;

                    typelist.ForEach(c =>
                    {
                        //查询配置比例
                        var typeenum = bllnum.GetModel(c.KaoheTypeID);
                        //计算各个专业的比例 虚拟产值*比例
                        count1 += typeenum.Jianzhu * c.virallotCount;
                        count2 += typeenum.Jiegou * c.virallotCount;
                        count3 += typeenum.Nuantong * c.virallotCount;
                        count4 += typeenum.Shui * c.virallotCount;
                        count5 += typeenum.Dianqi * c.virallotCount;

                        allCount += c.virallotCount;
                    });
                    //计算比例
                    if (allCount != 0)
                    {
                        result.Jianzhu = Math.Round(Convert.ToDecimal(count1 / allCount), 2);
                        result.Jiegou = Math.Round(Convert.ToDecimal(count2 / allCount), 2);
                        result.Nuantong = Math.Round(Convert.ToDecimal(count3 / allCount), 2);
                        result.Shui = Math.Round(Convert.ToDecimal(count4 / allCount), 2);
                        result.Dianqi = Math.Round(Convert.ToDecimal(count5 / allCount), 2);
                    }
                    else
                    {
                        result.Jianzhu = 0;
                        result.Jiegou = 0;
                        result.Nuantong = 0;
                        result.Shui = 0;
                        result.Dianqi = 0;
                    }
                }
                else if (typelist.Count == 1)
                {
                    var typeModel = typelist[0];
                    if (typeModel != null)
                    {
                        result = new TG.BLL.cm_KaoHeProjTypeEnum().GetModel(typeModel.KaoheTypeID);
                    }
                    else
                    {
                        result.Jianzhu = 0;
                        result.Jiegou = 0;
                        result.Nuantong = 0;
                        result.Shui = 0;
                        result.Dianqi = 0;
                    }
                }
                else
                {
                    result.Jianzhu = 0;
                    result.Jiegou = 0;
                    result.Nuantong = 0;
                    result.Shui = 0;
                    result.Dianqi = 0;
                }

                return result;
            }
        }

        //查询是否已经保存或提交数据
        public bool IsSave
        {
            get
            {
                string strWhere = string.Format(" ProKHNameID={0} AND InserUserID1={1} ", this.KaoHeNameID, this.UserSysNo);
                //项目经理

                if (CurUserSperRole == -1)
                {
                    strWhere = string.Format(" ProKHNameID={0} AND InserUserID2={1} ", this.KaoHeNameID, this.UserSysNo);
                }
                else if (CurUserSperRole == -3)//2017年4月19日 部门经理审批
                {
                    strWhere = string.Format(" ProKHNameID={0} AND InserUserID3={1} ", this.KaoHeNameID, this.UserSysNo);
                }
                var list = new TG.BLL.cm_KaoHeSpePersent().GetModelList(strWhere);
                if (list.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// 是否已经提交
        /// </summary>
        public bool IsSubmit
        {
            get
            {
                string strWhere = string.Format(" ProKHNameID={0} AND InserUserID1={1} AND Stat=1 ", this.KaoHeNameID, this.UserSysNo);
                //项目经理
                if (CurUserSperRole == -1)
                {
                    strWhere = string.Format(" ProKHNameID={0} AND InserUserID2={1} AND Stat2=1 ", this.KaoHeNameID, this.UserSysNo);
                }
                else if (CurUserSperRole == -3) //2017年4月19日 项目经理审批
                {
                    strWhere = string.Format(" ProKHNameID={0} AND InserUserID3={1} AND Stat3=1 ", this.KaoHeNameID, this.UserSysNo);                
                }

                var list = new TG.BLL.cm_KaoHeSpePersent().GetModelList(strWhere);
                if (list.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// 返回指定的
        /// </summary>
        public List<TG.Model.cm_KaoHeSpePersent> SavePersentList
        {
            get
            {
                string strWhere = string.Format(" ProKHNameID={0} AND InserUserID1={1} ", this.KaoHeNameID, this.UserSysNo);
                //-3 为新加部门经理审批
                if (CurUserSperRole == -1 || CurUserSperRole == -3)
                {
                    strWhere = string.Format(" ProKHNameID={0} ", this.KaoHeNameID);
                }
                var list = new TG.BLL.cm_KaoHeSpePersent().GetModelList(strWhere);
                return list;
            }
        }

        /// <summary>
        /// 获取当前用户的专业觉色
        /// </summary>
        //public int CurUserSperRole
        //{
        //    get
        //    {
        //        int roleid = 0;
        //        string strWhere = string.Format(" ProKHNameId={0} AND MemID={1}", this.KaoHeNameID, this.UserSysNo);
        //        var list = new TG.BLL.cm_KaoheSpecMems().GetModelList(strWhere);
        //        if (list.Count == 0)
        //        {
        //            roleid = 6;
        //        }
        //        else
        //        {
        //            roleid = list[0].SpeID;
        //        }

        //        return roleid;
        //    }
        //}
        /// <summary>
        /// 获取当前消息得到的人员当前角色，修复同一人多角色的问题 2016年7月1日
        /// </summary>
        public int CurUserSperRole
        {
            get
            {
                return Convert.ToInt32(Request["speid"] ?? "0");
            }
        }
        /// <summary>
        /// 主持人绩效
        /// </summary>
        public decimal ZhuChiJiXiao
        {
            get
            {
                string where = string.Format(" ProKHNameID={0} AND SpeID=-2 ", this.KaoHeNameID);
                var memlist = new TG.BLL.cm_KaoheSpecMems().GetModelList(where);

                if (memlist.Count > 0)
                {
                    var model = memlist[0];
                    return model.Jixiao;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// 消息ID
        /// </summary>
        protected int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["MsgNo"] ?? "0", out msgid);
                return msgid;
            }
        }
        /// <summary>
        /// 跳转
        /// </summary>
        public int Action
        {
            get
            {
                int action = 0;
                int.TryParse(Request["action"] ?? "0", out action);
                return action;
            }
        }

        /// <summary>
        /// 是否显示关闭自己
        /// </summary>
        public string IsClose
        {
            get
            {
                string isclose = Request["isclose"] ?? "";

                if (isclose == "true")
                {
                    return "yes";
                }
                else
                {
                    return "none";
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //跳转到调整奖金系数
                if (this.Action == 1)
                {
                    string url = "ProjectBpmAllot.aspx" + HttpContext.Current.Request.Url.Query;
                    Response.Redirect(url);
                }
                if (this.Action == 2)
                {
                    string url = "ProjDeptAllot.aspx" + HttpContext.Current.Request.Url.Query;
                    Response.Redirect(url);
                }
                if (this.Action == 3)
                {
                    string url = "UnitMngAllot.aspx" + HttpContext.Current.Request.Url.Query;
                    Response.Redirect(url);
                }
                //年度奖金调整
                if (this.Action == 4)
                {
                    string url = "/DeptBpm/UnitAllAllotRecord.aspx" + HttpContext.Current.Request.Url.Query;
                    Response.Redirect(url);
                }
                //设置部门负责人
                if (this.Action == 6)
                {
                    string url = "SetSpeMemFz.aspx" + HttpContext.Current.Request.Url.Query;
                    Response.Redirect(url);
                }

                if (CurUserSperRole > 0)
                {
                    string url = "SetMemsPersent.aspx" + HttpContext.Current.Request.Url.Query;
                    Response.Redirect(url);
                }
                else
                {
                    //如果是专业需要部门经理确认
                    if (this.Action == 5)
                    {
                        string url = "SetMemsPersent.aspx" + HttpContext.Current.Request.Url.Query;
                        Response.Redirect(url);
                    }
                }
            }
        }
    }
}