﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Text;
using System.IO;

namespace TG.Web.ProjBpm
{
    public partial class UnitMngAllot : PageBase
    {
        /// <summary>
        /// 消息ID
        /// </summary>
        protected int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["MsgNo"] ?? "0", out msgid);
                return msgid;
            }
        }
        /// <summary>
        /// 考核部门ID
        /// </summary>
        protected int KaoHeUnitSysNo
        {
            get
            {
                int unitid = 0;
                int.TryParse(Request["uid"] ?? "0", out unitid);
                return unitid;
            }
        }

        protected TG.Model.tg_unit KaoHeUnit
        {
            get
            {
                return new TG.BLL.tg_unit().GetModel(this.KaoHeUnitSysNo);
            }
        }
        /// <summary>
        /// 部门考核ID
        /// </summary>
        protected int KaoHeUnitID
        {
            get
            {
                int ukaoheid = 0;
                int.TryParse(Request["id"] ?? "0", out ukaoheid);
                return ukaoheid;
            }
        }
        /// <summary>
        /// 任务ID
        /// </summary>
        protected int RenwuNo
        {
            get
            {
                int renwuid = 0;
                int.TryParse(Request["renwuno"] ?? "0", out renwuid);
                return renwuid;
            }
        }
        /// <summary>
        /// 部门奖金总额
        /// </summary>
        public decimal? UnitAllCount
        {
            get
            {
                decimal result = 0;

                var list = new TG.BLL.cm_KaoHeUnitAllotReport().GetModelList(" RenwuID=" + this.RenwuNo + " AND UnitID=" + this.KaoHeUnitSysNo + "");

                if (list.Count > 0)
                {
                    result = list[0].Unitjiang;
                }

                return result;
            }
        }

        public bool IsArch
        {
            get
            {
                //List<int> list = new List<int>() { 
                //    231, 232, 237, 238, 239
                //};

                // qpl 2016年11月23日
                List<int> list = new List<int>();
                // 0 为施工部门  
                string strWhere = string.Format(" unit_Type={0}", 0);
                var unitlist = new TG.BLL.tg_unitExt().GetModelList(strWhere);

                unitlist.ForEach(c =>
                {
                    list.Add(c.unit_ID);
                });

                if (list.Contains(this.KaoHeUnitSysNo))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsFangAn
        {
            get
            {
                //List<int> list = new List<int>() { 
                //    233, 234, 236
                //};

                // qpl 2016年11月23日
                List<int> list = new List<int>();
                // 1 为方案部门  
                string strWhere = string.Format(" unit_Type={0}", 1);
                var unitlist = new TG.BLL.tg_unitExt().GetModelList(strWhere);

                unitlist.ForEach(c =>
                {
                    list.Add(c.unit_ID);
                });

                if (list.Contains(this.KaoHeUnitSysNo))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsTeShu
        {
            get
            {
                //List<int> list = new List<int>() { 
                //    240, 241, 235
                //};

                // qpl 2016年11月23日
                List<int> list = new List<int>();
                // 0 为特殊部门  
                string strWhere = string.Format(" unit_Type={0}", 2);
                var unitlist = new TG.BLL.tg_unitExt().GetModelList(strWhere);

                unitlist.ForEach(c =>
                {
                    list.Add(c.unit_ID);
                });

                if (list.Contains(this.KaoHeUnitSysNo))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsTeShu2
        {
            get
            {
                //List<int> list = new List<int>() { 
                //    242, 243, 244
                //};

                // qpl 2016年11月23日
                List<int> list = new List<int>();
                // 3 为特殊部门  
                string strWhere = string.Format(" unit_Type={0}", 3);
                var unitlist = new TG.BLL.tg_unitExt().GetModelList(strWhere);

                unitlist.ForEach(c =>
                {
                    list.Add(c.unit_ID);
                });

                if (list.Contains(this.KaoHeUnitSysNo))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// 股东ID
        /// </summary>
        public string GuDongIdStr
        {
            get
            {
                return "1440,1441,1442,1443,1445,1446,1474,1493,1498,1519,1538,1554,1577,1447,1461,1501,2455";
            }
        }

        /// <summary>
        /// 上半年
        /// </summary>
        protected string SBNWhere
        {
            get
            {
                //上半年考核
                if (RenwuModel.StartDate.Month < 7)
                {
                    int year = RenwuModel.StartDate.Year - 1;

                    return string.Format("YEAR(StartDate)={0} AND Month(StartDate)<8", year);
                }
                else
                {
                    int year = RenwuModel.StartDate.Year - 1;
                    return string.Format("YEAR(StartDate)={0} AND Month(StartDate)>=8", year);
                }
            }
        }
        /// <summary>
        /// 下半年
        /// </summary>
        protected string XBNWhere
        {
            get
            {
                //上半年考核
                if (RenwuModel.StartDate.Month < 7)
                {
                    int year = RenwuModel.StartDate.Year - 1;
                    return string.Format("YEAR(StartDate)={0} AND Month(StartDate)>=8", year);
                }
                else
                {
                    int year = RenwuModel.StartDate.Year;
                    return string.Format("YEAR(StartDate)={0} AND Month(StartDate)<8", year);
                }
            }
        }
        /// <summary>
        /// 本部门所有在职人员
        /// </summary>
        protected DataTable UnitMemsList
        {
            get
            {
                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }

                string strSql = string.Format(@" select m.mem_ID
	                                            ,m.mem_Name
                                                ,ISNULL((Select top 1 jbgz+gwgz From dbo.cm_KaoHeMemsWages a Where MemID=m.mem_ID {3} AND SendMonth=Month('{5}') Order By a.SendMonth DESC ),0) AS jbgz1
                                                ----,ISNULL((Select top 1 gscb From dbo.cm_KaoHeMemsWages a Where MemID=m.mem_ID {3} AND SendMonth=Month('{5}') Order By a.SendMonth DESC ),0) AS jbgz
                                                ,ISNULL((select top 1 mem_GongZi From tg_memberExtInfo where mem_ID=m.mem_ID),0) AS jbgz
                                                ,(Select mem_InTime From tg_memberExt Where mem_ID=m.mem_ID) AS mem_InTime
                                                ,ISNULL((Select case SUM(b.Weights) when 0 then 0 else SUM(b.Weights*r.JudgeCount)/SUM(b.Weights) end From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID where R.MemID=m.mem_ID AND R.KaoHeUnitID={1} AND B.KaoHeUnitID={1}),0) AS bmpf
                                                ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a where MemID= m.mem_ID {3}),0)/6 AS pjgz
                                                ,isnull((select MAX(Val) AS A
                                                    From (
                                                    Select (case SUM(b.Weights) when 0 then 0 else SUM(b.Weights*r.JudgeCount)/SUM(b.Weights) end) AS Val
                                                    From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID 
                                                    where R.KaoHeUnitID={1} AND B.KaoHeUnitID={1}
                                                    group by R.MemID ) B),0) AS maxJudgeCount
                                                ,isnull((select MIN(Val) AS B
                                                    From (
                                                    Select (case SUM(b.Weights) when 0 then 0 else SUM(b.Weights*r.JudgeCount)/SUM(b.Weights) end) AS Val
                                                    From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID 
                                                    where R.KaoHeUnitID={1} AND B.KaoHeUnitID={1}
                                                    group by R.MemID ) B),0) AS minJudgeCount
                                                ,isnull((select top 1 B.OrderID
                                                From (
	                                                select MemID
		                                                    ,AllotCount
		                                                    ,RANK() over(order by AllotCount DESC) AS OrderID
	                                                From (
		                                                Select r.MemID AS MemID
			                                            ,(case SUM(b.Weights) when 0 then 0 else isnull(SUM(b.Weights*r.JudgeCount)/SUM(b.Weights),0) end ) AS AllotCount
			                                            From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID left join dbo.cm_KaoHeRenwuMem BB on r.MemID=BB.memID
			                                            Where B.memUnitID={0} AND B.KaoHeUnitID={1} AND R.KaoHeUnitID={1} AND BB.Statu=0 AND BB.KaoHeUnitID={1}
			                                            group by r.memID
	                                                ) A
                                                ) B 
                                                Where B.MemID=m.mem_ID),0) AS OrderID
                                                ,isnull((Select top 1 JudgeCount from dbo.cm_UserJudgeReport U left join tg_memberRole r on u.InsertUserId=r.mem_Id 
                                                 Where r.RoleIdMag=5 AND U.MemID=m.mem_ID AND U.KaoHeUnitID={1}
	                                             ),0) AS MngCount
                                                ,isnull((Select TOP 1 B.OrderID
                                                From 
	                                                (Select A.mem_ID
		                                                    ,A.MngCount
		                                                    ,RANK() over(order by A.MngCount DESC) As OrderID
	                                                From(
			                                                select m.mem_ID,
				                                                (
					                                                select top 1 JudgeCount 
					                                                From dbo.cm_UserJudgeReport U left join tg_memberRole r on u.InsertUserId=r.mem_Id 
					                                                Where r.RoleIdMag=5 AND U.MemID=m.mem_ID AND U.KaoHeUnitID={1}
				                                                ) AS MngCount
			                                                From tg_member m left join tg_memberRole r on m.mem_ID=r.mem_Id left join dbo.cm_KaoHeRenwuMem BB on m.mem_ID=BB.memID
			                                                Where r.RoleIdMag<>5 AND m.mem_isFired=0 AND m.mem_Unit_ID={0} AND BB.Statu=0 AND BB.KaoHeUnitID={1}
	                                                ) A
                                                )B
                                                Where B.mem_ID=m.mem_ID),0) AS OrderID2
                                                ,isnull((Select SUM(AllotCount) From dbo.cm_KaoHeMemsAllotChange Where MemID=m.mem_ID AND RenwuID={2}),0) AS AllotCount 
                                                ,isnull((Select SUM(AllotCountBefore) From dbo.cm_KaoHeMemsAllotChange Where MemID=m.mem_ID AND RenwuID={2}),0) AS AllotCountBefore 
                                                --,isnull((select SUM(hj) From dbo.cm_KaoHeMemsMngAllot Where MemID=m.mem_ID AND RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {7})),0) AS sbn
                                                --,isnull((select SUM(hj) From dbo.cm_KaoHeMemsMngAllot Where MemID=m.mem_ID AND RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {8})),0) AS xbn
                                                ,isnull((select SUM(hjyf) From dbo.cm_KaoHeMemsMngAllot Where MemID=m.mem_ID AND RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {7})),0) AS sbn
                                                ,isnull((select SUM(hjyf) From dbo.cm_KaoHeMemsMngAllot Where MemID=m.mem_ID AND RenwuID IN (Select top 1 ID From dbo.cm_KaoHeRenWu Where {8})),0) AS xbn
                                                ,isnull((Select top 1 AllotCount1 From dbo.cm_KaoHeHistoryBMN Where MemID=m.mem_ID AND AllotYear=YEAR('{4}')-1),0) AS sbn2
                                                ,isnull((Select top 1 AllotCount2 From dbo.cm_KaoHeHistoryBMN Where MemID=m.mem_ID AND AllotYear=YEAR('{4}')-1),0) AS xbn2
                                                ,isnull((select SUM(yfjj) from dbo.cm_KaoHeMemsWages a where MemID= m.mem_ID {3}),0) AS yfjj
                                                From tg_member m left join tg_memberRole r on m.mem_ID=r.mem_Id left join dbo.cm_KaoHeRenwuMem rm on m.mem_ID=rm.memID
                                                Where r.RoleIdMag<>5 AND m.mem_isFired=0 AND rm.memUnitID={0} AND rm.KaoHeUnitID={1} AND rm.Statu=0 AND RM.IsOutUnitID=0 AND (m.mem_ID NOT IN({6}))
                                                Order By m.mem_Order ", this.KaoHeUnitSysNo, this.KaoHeUnitID, this.RenwuNo, strWhere, RenwuModel.StartDate, RenwuModel.EndDate, GuDongIdStr, SBNWhere, XBNWhere);

                //Where r.RoleIdMag<>5 AND m.mem_isFired=0 AND m.mem_Unit_ID={0} AND rm.KaoHeUnitID={1} AND rm.Statu=0 ", this.KaoHeUnitSysNo, this.KaoHeUnitID, this.RenwuNo, strWhere, RenwuModel.StartDate);
                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
                return dt;
            }
        }

        /// <summary>
        /// 考核任务
        /// </summary>
        public TG.Model.cm_KaoHeRenWu RenwuModel
        {
            get
            {
                var model = new TG.BLL.cm_KaoHeRenWu().GetModel(this.RenwuNo);
                return model;
            }
        }
        /// <summary>
        /// 本部门所有离职人员
        /// </summary>
        protected DataTable UnitMemsOutList
        {
            get
            {

                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }

                string strSql = string.Format(@" select m.mem_ID
	                                            ,m.mem_Name
                                                ,(select top 1  mem_InTime from tg_memberExt Where mem_ID=m.mem_ID) AS mem_InTime
                                                ,(select top 1  mem_OutTime from tg_memberExt Where mem_ID=m.mem_ID) AS mem_OutTime
                                                ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {3}),0)/6 AS pjgz 
                                                ,ISNULL((Select top 1 jbgz+gwgz From dbo.cm_KaoHeMemsWages a Where MemID=m.mem_ID {3} AND SendMonth=Month('{4}') Order By a.SendMonth DESC ),0) AS jbgz1
                                                --,ISNULL((Select top 1 gscb From dbo.cm_KaoHeMemsWages a Where MemID=m.mem_ID {3} AND SendMonth=Month('{4}') Order By a.SendMonth DESC ),0) AS jbgz
                                                ,ISNULL((select top 1 mem_GongZi From tg_memberExtInfo where mem_ID=m.mem_ID),0) AS jbgz
                                                ,ISNULL((Select SUM(yfjj) From dbo.cm_KaoHeMemsWages a Where MemID=m.mem_ID {3}),0) AS yfjj
                                                From tg_member m left join tg_memberRole r on m.mem_ID=r.mem_Id left join dbo.tg_memberExt ex on m.mem_ID=ex.mem_ID
                                                where r.RoleIdMag<>5 AND m.mem_isFired=1 AND m.mem_Unit_ID={0} AND (ex.mem_OutTime BETWEEN '{5}' AND '{4}')
                                                Order By ex.mem_OutTime ", this.KaoHeUnitSysNo, this.KaoHeUnitID, this.RenwuNo, strWhere, RenwuModel.EndDate, RenwuModel.StartDate);
                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;
            }
        }
        /// <summary>
        /// 整体调整系数
        /// </summary>
        public decimal? ZhengTiXishu
        {
            get
            {
                decimal? result = 0;
                string strWhere = string.Format(" RenwuID={0} AND UnitID={1} ", this.RenwuNo, this.KaoHeUnitSysNo);
                var list = new TG.BLL.cm_KaoHeMemsMngAllot().GetModelList(strWhere);

                if (list.Count > 0)
                {
                    result = list[0].weights;
                }
                else
                {
                    result = (decimal)1.5;
                }

                return result;
            }
        }
        /// <summary>
        /// 本部门所有人员
        /// </summary>
        protected DataTable UnitMemsSaveList
        {
            get
            {
                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }

                string strSql = string.Format(@" select m.mem_ID
	                                            ,m.mem_Name
                                                ,ISNULL((Select top 1 jbgz+gwgz From dbo.cm_KaoHeMemsWages a Where MemID=m.mem_ID {3} AND SendMonth=Month('{4}') Order By a.SendMonth DESC ),0) AS jbgz1
                                                ----,ISNULL((Select top 1 gscb From dbo.cm_KaoHeMemsWages a Where MemID=m.mem_ID {3} AND SendMonth=Month('{4}') Order By a.SendMonth DESC ),0) AS jbgz
                                                ----,ISNULL((select top 1 mem_GongZi From tg_memberExtInfo where mem_ID=m.mem_ID),0) AS jbgz
                                                ,isnull((Select jbgz from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS jbgz
                                                ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where a.MemID= m.mem_ID {3}),0)/6 AS pjgz
                                                ,ISNULL((Select case SUM(b.Weights) when 0 then 0 else SUM(b.Weights*r.JudgeCount)/SUM(b.Weights) end From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID where R.MemID=m.mem_ID AND R.KaoHeUnitID={1} AND B.KaoHeUnitID={1}),0) AS bmpf
                                                ,isnull((select MAX(Val) AS A
                                                    From (
                                                    Select (case SUM(b.Weights) when 0 then 0 else SUM(b.Weights*r.JudgeCount)/SUM(b.Weights) end) AS Val
                                                    From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID 
                                                    where R.KaoHeUnitID={1} AND B.KaoHeUnitID={1}
                                                    group by R.MemID ) B),0) AS maxJudgeCount
                                                ,isnull((select MIN(Val) AS B
                                                    From (
                                                    Select (case SUM(b.Weights) when 0 then 0 else SUM(b.Weights*r.JudgeCount)/SUM(b.Weights) end) AS Val
                                                    From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID 
                                                    where R.KaoHeUnitID={1} AND B.KaoHeUnitID={1}
                                                    group by R.MemID ) B),0) AS minJudgeCount
                                                ,(select top 1 B.OrderID
                                                From (
	                                                select MemID
		                                                    ,AllotCount
		                                                    ,RANK() over(order by AllotCount DESC) AS OrderID
	                                                From (
		                                                Select r.MemID AS MemID
			                                            ,(case SUM(b.Weights) when 0 then 0 else isnull(SUM(b.Weights*r.JudgeCount)/SUM(b.Weights),0) end ) AS AllotCount
			                                            From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID left join dbo.cm_KaoHeRenwuMem BB on r.MemID=BB.memID
			                                            Where B.memUnitID={0} AND B.KaoHeUnitID={1} AND R.KaoHeUnitID={1} AND BB.Statu=0 AND BB.KaoHeUnitID={1}
			                                            group by r.memID
	                                                ) A
                                                ) B 
                                                Where B.MemID=m.mem_ID) AS OrderID
                                                ,isnull((Select top 1 JudgeCount from dbo.cm_UserJudgeReport U left join tg_memberRole r on u.InsertUserId=r.mem_Id 
                                                 Where r.RoleIdMag=5 AND U.MemID=m.mem_ID AND U.KaoHeUnitID={1}
	                                             ),0) AS MngCount
                                                ,(Select B.OrderID
                                                From 
	                                                (Select A.mem_ID
		                                                    ,A.MngCount
		                                                    ,RANK() over(order by A.MngCount DESC) As OrderID
	                                                From(
			                                                select m.mem_ID,
				                                                (
					                                                select top 1 JudgeCount 
					                                                From dbo.cm_UserJudgeReport U left join tg_memberRole r on u.InsertUserId=r.mem_Id 
					                                                Where r.RoleIdMag=5 AND U.MemID=m.mem_ID AND U.KaoHeUnitID={1}
				                                                ) AS MngCount
			                                                From tg_member m left join tg_memberRole r on m.mem_ID=r.mem_Id left join dbo.cm_KaoHeRenwuMem BB on m.mem_ID=BB.memID
			                                                Where r.RoleIdMag<>5 AND m.mem_isFired=0 AND m.mem_Unit_ID={0} AND BB.Statu=0 AND BB.KaoHeUnitID={1}
	                                                ) A
                                                )B
                                                Where B.mem_ID=m.mem_ID) AS OrderID2
                                                ,isnull((Select SUM(AllotCount) From dbo.cm_KaoHeMemsAllotChange Where MemID=m.mem_ID AND RenwuID={2}),0) AS AllotCount 
                                                ,isnull((Select SUM(AllotCountBefore) From dbo.cm_KaoHeMemsAllotChange Where MemID=m.mem_ID AND RenwuID={2}),0) AS AllotCountBefore 
                                                ,isnull((Select jqxs from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS jqxs
                                                ,isnull((Select xmjjjs from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS xmjjjs
                                                ,isnull((Select xmjjmng from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS xmjjmng
                                                ,isnull((Select xmjjtz from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS xmjjtz
                                                ,isnull((Select bmnjs from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS bmnjs
                                                ,isnull((Select yxyg from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS yxyg
                                                ,isnull((Select hj from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS hj
                                                ,isnull((Select hjyf from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS hjyf
                                                ,isnull((Select beizhu from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS beizhu
                                                ,isnull((Select gzsj from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS gzsj
                                                ,isnull((Select beforyear from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS beforyear
                                                ,isnull((Select chazhi from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS chazhi
                                                ,isnull((Select beforyear2 from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS beforyear2
                                                ,isnull((Select chazhi2 from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS chazhi2
                                                ,isnull((Select xmjjtz from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS xmjjtz
                                                ,isnull((Select Stat from cm_KaoHeMemsMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS Stat
                                                ,isnull((select SUM(yfjj) from dbo.cm_KaoHeMemsWages a where MemID= m.mem_ID {3}),0) AS yfjj
                                                ----From tg_member m left join tg_memberRole r on m.mem_ID=r.mem_Id left join dbo.cm_KaoHeRenwuMem rm on m.mem_ID=rm.memID
                                                ----Where r.RoleIdMag<>5 AND m.mem_isFired=0 AND m.mem_Unit_ID={0} AND rm.KaoHeUnitID={1} AND rm.Statu=0 AND RM.IsOutUnitID=0 AND (m.mem_ID NOT IN({5}))
                                                From cm_KaoHeMemsMngAllot KM left join tg_member M on KM.MemID=M.mem_ID left join tg_memberRole r on m.mem_ID=r.mem_Id left join dbo.cm_KaoHeRenwuMem rm on m.mem_ID=rm.memID
                                                Where RenwuID={2} AND r.RoleIdMag<>5 AND m.mem_isFired=0 AND KM.UnitID={0} AND (m.mem_ID NOT IN({5})) AND rm.memUnitID={0} AND rm.KaoHeUnitID={1} AND rm.Statu=0 AND RM.IsOutUnitID=0
                                                Order By M.mem_Order ", this.KaoHeUnitSysNo, this.KaoHeUnitID, this.RenwuNo, strWhere, RenwuModel.EndDate, GuDongIdStr);

                //如果已经归档就加载历史数据
                if (IsSubmit && IsBak)
                {
                    strSql = string.Format(@" SELECT [ID]
                                          ,[RenwuID]
                                          ,[RenwuName]
                                          ,[UnitID]
                                          ,[UnitName]
                                          ,[UnitAllCount]
                                          ,[ZTCJXS]
                                          ,0 AS mem_ID
                                          ,[UserName] AS mem_Name
                                          ,[JBGZ] AS jbgz 
                                          ,[GZSJ] AS gzsj 
                                          ,[PJYGZ] AS pjgz
                                          ,[YFJJ] AS yfjj
                                          ,[ZPHP] AS bmpf
                                          ,[PM] AS OrderID 
                                          ,[BMJLPJ] AS MngCount
                                          ,[PM2] AS OrderID2
                                          ,[JQXS] AS jqxs
                                          ,[BMNJJJS] AS bmnjs 
                                          ,[XMJJJS] AS xmjjjs 
                                          ,[XMJJJLTZ] AS xmjjmng
                                          ,[XMJJTZ] AS xmjjtz
                                          ,[YXYG] AS yxyg
                                          ,[HJ] 
                                          ,[HJYF]
                                          ,[GZBS] AS beizhu 
                                          ,[SBN] AS  beforyear2
                                          ,[SBN2] AS chazhi2
                                          ,[XBN] AS beforyear 
                                          ,[XBN2] AS chazhi
                                          ,0 AS maxJudgeCount
                                          ,0 AS minJudgeCount
                                          ,1 AS Stat
                                           FROM [dbo].[cm_KaoHeUnitDetailsHis] 
                                           WHERE RenwuID={0} AND UnitID={1}", this.RenwuNo, this.KaoHeUnitSysNo);
                }


                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
                // 2016年12月28日 修改了不显示部门人员调整后的记录
                return dt;
            }
        }
     
        /// <summary>
        /// 本部门所有离职人员
        /// </summary>
        protected DataTable UnitMemsSaveOutList
        {
            get
            {
                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }
                //这里的基本工资确定为公司成本
                string strSql = string.Format(@" select m.mem_ID
	                                            ,m.mem_Name
                                                ,(select top 1  mem_InTime from tg_memberExt Where mem_ID=m.mem_ID) AS mem_InTime
                                                ,(select top 1  mem_OutTime from tg_memberExt Where mem_ID=m.mem_ID) AS mem_OutTime
                                                ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a Where MemID= m.mem_ID {3}),0)/6 AS pjgz 
                                                ,ISNULL((Select top 1 jbgz+gwgz From dbo.cm_KaoHeMemsWages a Where MemID=m.mem_ID {3} AND SendMonth=Month('{4}') Order By a.SendMonth DESC ),0) AS jbgz1
                                                --,ISNULL((Select top 1 gscb From dbo.cm_KaoHeMemsWages a Where MemID=m.mem_ID {3} AND SendMonth=Month('{4}') Order By a.SendMonth DESC ),0) AS jbgz
                                                ,ISNULL((select top 1 mem_GongZi From tg_memberExtInfo where mem_ID=m.mem_ID),0) AS jbgz
                                                ,ISNULL((Select SUM(yfjj) From dbo.cm_KaoHeMemsWages a Where MemID=m.mem_ID {3}),0) AS yfjj
                                                ,ISNULL((Select bmjltz from cm_KaoHeMemsOutMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS bmjltz
                                                ,ISNULL((Select beizhu from cm_KaoHeMemsOutMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS beizhu
                                                ,ISNULL((Select Stat from cm_KaoHeMemsOutMngAllot where RenwuID={2} AND MemID=m.mem_ID),0) AS Stat
                                                From tg_member m left join tg_memberRole r on m.mem_ID=r.mem_Id left join dbo.tg_memberExt ex on m.mem_ID=ex.mem_ID
                                                where r.RoleIdMag<>5 AND m.mem_isFired=1 AND m.mem_Unit_ID={0} AND (ex.mem_OutTime BETWEEN '{5}' AND '{4}')
                                                Order By m.mem_OutTime ", this.KaoHeUnitSysNo, this.KaoHeUnitID, this.RenwuNo, strWhere, RenwuModel.EndDate, RenwuModel.StartDate);

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;
            }
        }

        public int InserUserSysNo
        {
            get
            {
                int userid = Convert.ToInt32(Request["chkuserid"] ?? "0");
                if (userid == 0)
                {
                    return this.UserSysNo;
                }
                else
                {
                    return userid;
                }
            }
        }
        /// <summary>
        /// 是否已经保存
        /// </summary>
        public bool IsSave
        {
            get
            {
                bool result = false;
                string strWhere = string.Format(" RenwuID={0} AND UnitID={1} AND insertUserID={2} ", this.RenwuNo, this.KaoHeUnitSysNo, this.InserUserSysNo);
                var list = new TG.BLL.cm_KaoHeMemsMngAllot().GetModelList(strWhere);

                if (list.Count > 0)
                {
                    result = true;
                }

                return result;
            }
        }
        /// <summary>
        /// 是否已经保存
        /// </summary>
        public bool IsSaveOut
        {
            get
            {
                bool result = false;
                string strWhere = string.Format(" RenwuID={0} AND UnitID={1} ", this.RenwuNo, this.KaoHeUnitSysNo, this.InserUserSysNo);
                var list = new TG.BLL.cm_KaoHeMemsOutMngAllot().GetModelList(strWhere);

                if (list.Count > 0)
                {
                    result = true;
                }

                return result;
            }
        }
        /// <summary>
        /// 是否提交
        /// </summary>
        public bool IsSubmit
        {
            get
            {
                bool result = false;
                string strWhere = string.Format(" RenwuID={0} AND UnitID={1} AND Stat=1 ", this.RenwuNo, this.KaoHeUnitSysNo, this.InserUserSysNo);
                var list = new TG.BLL.cm_KaoHeMemsMngAllot().GetModelList(strWhere);

                if (list.Count > 0)
                {
                    result = true;
                }

                return result;
            }
        }
        /// <summary>
        /// 离职分配
        /// </summary>
        public bool IsSubmitOut
        {
            get
            {
                bool result = false;
                string strWhere = string.Format(" RenwuID={0} AND UnitID={1} AND Stat=1 ", this.RenwuNo, this.KaoHeUnitSysNo, this.InserUserSysNo);
                var list = new TG.BLL.cm_KaoHeMemsOutMngAllot().GetModelList(strWhere);

                if (list.Count > 0)
                {
                    result = true;
                }

                return result;
            }
        }
        /// <summary>
        /// 是否已经归档
        /// </summary>
        public bool IsBak
        {
            get
            {
                string strWhere = string.Format(" RenwuID={0} AND UnitID={1}", this.RenwuNo, this.KaoHeUnitSysNo);

                int count = new TG.BLL.cm_KaoHeUnitDetailsHis().GetRecordCount(strWhere);
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_Export_Click(object sender, EventArgs e)
        {
            ExportShigong();
        }

        //特殊1
        protected void btn_Export2_Click(object sender, EventArgs e)
        {
            ExportTeshuOne();
        }

        protected void btn_Export3_Click(object sender, EventArgs e)
        {
            ExportOutMems();
        }

        protected void btn_Export4_Click(object sender, EventArgs e)
        {
            ExportFangAn();
        }

        public string IsCheck
        {
            get
            {
                return Request["check"] ?? "0";
            }
        }
        protected string SBNTitleStr
        {
            get
            {
                string title = "";
                if (RenwuModel.StartDate.Month < 7)
                {
                    title = (RenwuModel.StartDate.Year - 1) + "下半年";
                }
                else
                {
                    title = RenwuModel.StartDate.Year + "上半年";
                }

                return title;
            }
        }

        protected string XBNTitleStr
        {
            get
            {
                string title = "";
                if (RenwuModel.StartDate.Month < 7)
                {
                    title = (RenwuModel.StartDate.Year - 1) + "上半年";
                }
                else
                {
                    title = (RenwuModel.StartDate.Year - 1) + "下半年";
                }

                return title;
            }
        }
        /// <summary>
        /// 特殊2
        /// </summary>
        protected void ExportTeshu()
        {
            DataTable dt = this.UnitMemsSaveList;          
            string modelPath = " ~/TemplateXls/teshubumen.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            var rowOne = ws.GetRow(0);
            //rowOne.GetCell(8).SetCellValue((RenwuModel.StartDate.Year - 1) + "上半年");
            //rowOne.GetCell(10).SetCellValue((RenwuModel.StartDate.Year - 1) + "下半年");
            rowOne.GetCell(10).SetCellValue(SBNTitleStr);
            rowOne.GetCell(12).SetCellValue(XBNTitleStr);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)

                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["jbgz"].ToString()));

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["gzsj"].ToString()));

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["pjgz"].ToString()));

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["yfjj"].ToString()));

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["bmnjs"].ToString()));

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["yxyg"].ToString()));

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["hj"].ToString()));

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["hjyf"].ToString()));

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;

                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["beizhu"].ToString()));

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["beforyear2"].ToString()));

                cell = dataRow.CreateCell(11);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["chazhi2"].ToString()));

                cell = dataRow.CreateCell(12);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["beforyear"].ToString()));

                cell = dataRow.CreateCell(13);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["chazhi"].ToString()));
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(KaoHeUnit.unit_Name.Trim() + "奖金调整导出.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        //未做改动，导出数据
        protected void ExportTeshuYs()
        {
            DataTable dts = this.UnitMemsSaveList;
        
            if (dts.Rows.Count!=0)
            {
                ExportTeshu();
            }
            else
            {   
                DataTable dt = this.UnitMemsList;
                string modelPath = " ~/TemplateXls/teshubumen2.xls";

                HSSFWorkbook wb = null;

                //如果没有模板路径，则创建一个空的workbook和一个空的sheet
                if (string.IsNullOrEmpty(modelPath))
                {
                    wb = new HSSFWorkbook();
                    wb.CreateSheet();
                    wb.GetSheetAt(0).CreateRow(0);
                }
                else
                {
                    using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        wb = new HSSFWorkbook(fileStream);
                        fileStream.Close();
                    }
                }


                //内容样式
                ICellStyle style2 = wb.CreateCellStyle();
                style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
                style2.VerticalAlignment = VerticalAlignment.CENTER;
                style2.WrapText = true;
                style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                IFont font2 = wb.CreateFont();
                font2.FontHeightInPoints = 10;//字号
                font2.FontName = "宋体";//字体
                style2.SetFont(font2);

                //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
                var ws = wb.GetSheet("Sheet1");
                if (ws == null)
                    ws = wb.GetSheetAt(0);

                int row = 2;
                var rowOne = ws.GetRow(0);
                //rowOne.GetCell(8).SetCellValue((RenwuModel.StartDate.Year - 1) + "上半年");
                //rowOne.GetCell(10).SetCellValue((RenwuModel.StartDate.Year - 1) + "下半年");
                //rowOne.GetCell(10).SetCellValue(SBNTitleStr);
                //rowOne.GetCell(12).SetCellValue(XBNTitleStr);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(i + row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(i + row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)

                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["mem_Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["jbgz"].ToString()));


                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["pjgz"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["yfjj"].ToString()));


                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["xbn"].ToString()));

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["sbn"].ToString()));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    //cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["gzsj"].ToString()));
                    cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["mem_InTime"]).ToString("yyyy-MM-dd"));
                }
                using (MemoryStream memoryStream = new MemoryStream())
                {

                    wb.Write(memoryStream);

                    string name = System.Web.HttpContext.Current.Server.UrlEncode(KaoHeUnit.unit_Name.Trim() + "奖金调整导出.xls");
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                    Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                    Response.BinaryWrite(memoryStream.ToArray());
                    Response.ContentEncoding = Encoding.UTF8;
                    wb = null;
                    Response.End();
                }
            }
           
        }
        /// <summary>
        /// 特殊1
        /// </summary>
        protected void ExportTeshuOne()
        {
            DataTable dt = this.UnitMemsSaveList;

            string modelPath = " ~/TemplateXls/teshuone.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            var rowOne = ws.GetRow(0);
            //rowOne.GetCell(15).SetCellValue((RenwuModel.StartDate.Year - 1) + "上半年");
            //rowOne.GetCell(17).SetCellValue((RenwuModel.StartDate.Year - 1) + "下半年");

            rowOne.GetCell(14).SetCellValue(SBNTitleStr);
            rowOne.GetCell(16).SetCellValue(XBNTitleStr);
            //rowOne.GetCell(19).SetCellValue(Convert.ToDecimal(ZhengTiXishu).ToString("f2"));


            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)

                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["jbgz"].ToString()));

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["gzsj"].ToString()));

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["pjgz"].ToString()));

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["yfjj"].ToString()));

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["bmpf"].ToString()));

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["OrderID"].ToString()));

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["MngCount"].ToString()));

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["OrderID2"].ToString()));

                //cell = dataRow.CreateCell(9);
                //cell.CellStyle = style2;
                //cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["jqxs"].ToString()));

                //cell = dataRow.CreateCell(10);
                //cell.CellStyle = style2;
                //cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["bmnjs"].ToString()));

                //cell = dataRow.CreateCell(11);
                //cell.CellStyle = style2;
                //cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["xmjjmng"].ToString()));

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["xmjjtz"].ToString()));

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["yxyg"].ToString()));

                cell = dataRow.CreateCell(11);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["hj"].ToString()));

                cell = dataRow.CreateCell(12);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["hjyf"].ToString()));

                cell = dataRow.CreateCell(13);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["beizhu"].ToString()));

                cell = dataRow.CreateCell(14);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["beforyear2"].ToString()));

                cell = dataRow.CreateCell(15);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["chazhi2"].ToString()));

                cell = dataRow.CreateCell(16);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["beforyear"].ToString()));

                cell = dataRow.CreateCell(17);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["chazhi"].ToString()));
            }


            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(KaoHeUnit.unit_Name.Trim() + "奖金调整导出.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }


       /// <summary>
        /// 施工
        /// </summary>
        protected void ExportShigong()
        {
            DataTable dt = this.UnitMemsSaveList;

            string modelPath = " ~/TemplateXls/shigongbumen.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            var rowOne = ws.GetRow(0);
            //rowOne.GetCell(16).SetCellValue((RenwuModel.StartDate.Year - 1) + "上半年");
            //rowOne.GetCell(18).SetCellValue((RenwuModel.StartDate.Year - 1) + "下半年");

            rowOne.GetCell(18).SetCellValue(SBNTitleStr);
            rowOne.GetCell(20).SetCellValue(XBNTitleStr);
            rowOne.GetCell(23).SetCellValue(Convert.ToDecimal(ZhengTiXishu).ToString("f2"));

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)

                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["jbgz"].ToString()));

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["gzsj"].ToString()));

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["pjgz"].ToString()));

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["yfjj"].ToString()));

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["bmpf"].ToString()));

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["OrderID"].ToString()));

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["MngCount"].ToString()));

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["OrderID2"].ToString()));

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["jqxs"].ToString()));

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["bmnjs"].ToString()));

                cell = dataRow.CreateCell(11);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["xmjjjs"].ToString()));

                cell = dataRow.CreateCell(12);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["xmjjmng"].ToString()));

                cell = dataRow.CreateCell(13);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["xmjjtz"].ToString()));

                cell = dataRow.CreateCell(14);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["yxyg"].ToString()));

                cell = dataRow.CreateCell(15);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["hj"].ToString()));

                cell = dataRow.CreateCell(16);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["hjyf"].ToString()));

                cell = dataRow.CreateCell(17);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["beizhu"].ToString()));

                cell = dataRow.CreateCell(18);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["beforyear2"].ToString()));

                cell = dataRow.CreateCell(19);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["chazhi2"].ToString()));

                cell = dataRow.CreateCell(20);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["beforyear"].ToString()));

                cell = dataRow.CreateCell(21);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["chazhi"].ToString()));

            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(KaoHeUnit.unit_Name.Trim() + "奖金调整导出.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        /// <summary>
        /// 方案
        /// </summary>
        protected void ExportFangAn()
        {
            DataTable dt = this.UnitMemsSaveList;

            string modelPath = " ~/TemplateXls/fanganbumen.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            var rowOne = ws.GetRow(0);
            //rowOne.GetCell(15).SetCellValue((RenwuModel.StartDate.Year - 1) + "上半年");
            //rowOne.GetCell(17).SetCellValue((RenwuModel.StartDate.Year - 1) + "下半年");

            rowOne.GetCell(17).SetCellValue(SBNTitleStr);
            rowOne.GetCell(19).SetCellValue(XBNTitleStr);
            rowOne.GetCell(22).SetCellValue(Convert.ToDecimal(ZhengTiXishu).ToString("f2"));


            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)

                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["jbgz"].ToString()));

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["gzsj"].ToString()));

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["pjgz"].ToString()));

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["yfjj"].ToString()));

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["bmpf"].ToString()));

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["OrderID"].ToString()));

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["MngCount"].ToString()));

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["OrderID2"].ToString()));

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["jqxs"].ToString()));

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["bmnjs"].ToString()));

                cell = dataRow.CreateCell(11);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["xmjjmng"].ToString()));

                cell = dataRow.CreateCell(12);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["xmjjtz"].ToString()));

                cell = dataRow.CreateCell(13);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["yxyg"].ToString()));

                cell = dataRow.CreateCell(14);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["hj"].ToString()));

                cell = dataRow.CreateCell(15);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["hjyf"].ToString()));

                cell = dataRow.CreateCell(16);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["beizhu"].ToString()));

                cell = dataRow.CreateCell(17);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["beforyear2"].ToString()));

                cell = dataRow.CreateCell(18);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["chazhi2"].ToString()));

                cell = dataRow.CreateCell(19);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["beforyear"].ToString()));

                cell = dataRow.CreateCell(20);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["chazhi"].ToString()));

            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(KaoHeUnit.unit_Name.Trim() + "奖金调整导出.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        /// <summary>
        /// 离职人员
        /// </summary>
        protected void ExportOutMems()
        {
            DataTable dt = this.UnitMemsSaveOutList;

            string modelPath = " ~/TemplateXls/lizhirenyuan.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)

                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_InTime"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_OutTime"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["jbgz"].ToString()));

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["yfjj"].ToString()));

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["pjgz"].ToString()));

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["bmjltz"].ToString()));

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["beizhu"].ToString());



            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(KaoHeUnit.unit_Name.Trim() + "离职人员奖金调整导出.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ExportTeshu();
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            ExportTeshuYs();
        }
        /// <summary>
        /// 获取备份
        /// </summary>
        protected decimal? UnitAllCountBak
        {
            get {

                decimal? result = 0;

                string strWhere=string.Format(" RenwuID={0} AND UnitID={1} ",this.RenwuNo, this.KaoHeUnitSysNo);
                var list = new TG.BLL.cm_KaoHeUnitDetailsHis().GetModelList(strWhere);

                if(list.Count>0)
                {
                    result = list[0].UnitAllCount;
                }

                return result;
            }
        }
    }
}