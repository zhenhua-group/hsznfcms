﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjBpm
{
    public partial class ProjectBpmAllotList : PageBase
    {
        /// <summary>
        /// 任务列表
        /// </summary>
        public List<TG.Model.cm_KaoHeRenWu> RenwuList
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list;
            }
        }
        public List<TG.Model.cm_KaoHeRenWu> RenwuListOrder
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list.OrderByDescending(c => c.StartDate).ToList();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                SelectCurrentYear();
                BindUnit();
                BindRenWu();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void BindRenWu()
        {
            RenwuList.OrderByDescending(c => c.StartDate).ToList().ForEach(c =>
            {
                this.drprenwu.Items.Add(new ListItem(c.RenWuNo.ToString(), c.ID.ToString()));
            });
        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            this.drp_unit3.DataSource = bll_unit.GetList(" unit_ParentID<>0 AND unit_ID IN (231,232,233,234,236,237,238,239)");
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }
        //生产部门改变重新绑定数据
        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void drprenwu_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {

        }

        protected DataTable ProjList
        {
            get;
            set;
        }
        /// <summary>
        /// 分配类型
        /// </summary>
        protected List<TG.Model.cm_KaoHeProjTypeEnum> TypeEnumList
        {
            get
            {
                return new TG.BLL.cm_KaoHeProjTypeEnum().GetModelList("").OrderBy(c => c.ID).ToList();
            }
        }
        /// <summary>
        /// 专业列表
        /// </summary>
        protected List<TG.Model.tg_speciality> SpecialList
        {
            get
            {
                return new TG.BLL.tg_speciality().GetModelList(" Spe_ID<6 ");
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }


        public DataTable ProjNameList
        {
            get
            {
                string strSql = string.Format(@"Select P.ID,P.KaoHeProjId,
	                                    (Select proName From dbo.cm_KaoHeRenwuProj Where ID=P.KaoHeProjId) AS proName,
                                        (Select proID From dbo.cm_KaoHeRenwuProj Where ID=P.KaoHeProjId) AS proID,
	                                    P.KName,
	                                    P.DoneScale,
	                                    P.Sub,
                                        T.ID AS TID,
	                                    T.KaoheTypeName,
	                                    T.RealScale,
	                                    T.TypeXishu,
	                                    T.virallotCount,
	                                    P.AllScale,
	                                    (Select SUM(virallotCount) From dbo.cm_KaoheProjType  Where KaoheNameID=P.ID) AS AllAllotCount,
	                                    (Select Count(*) From dbo.cm_KaoheProjType  Where KaoheNameID=P.ID) AS ColCount
                                        From cm_KaoHeProjName P left join cm_KaoheProjType T on P.ID=T.KaoheNameID
                                        Where (T.ID IS NOT NULL) AND (P.KaoHeProjId IN (Select ID 
							            From dbo.cm_KaoHeRenwuProj 
							            Where YEAR(InsertDate)={0} AND RenWuId={1} )) 
                                        Order By P.ID", this.drp_year.SelectedValue, this.drprenwu.SelectedValue);

                if (this.drp_unit3.SelectedIndex > 0)
                {
                    strSql = string.Format(@"Select P.ID,P.KaoHeProjId,
	                                    (Select proName From dbo.cm_KaoHeRenwuProj Where ID=P.KaoHeProjId) AS proName,
                                        (Select proID From dbo.cm_KaoHeRenwuProj Where ID=P.KaoHeProjId) AS proID,
	                                    P.KName,
	                                    P.DoneScale,
	                                    P.Sub,
                                        T.ID AS TID,
	                                    T.KaoheTypeName,
	                                    T.RealScale,
	                                    T.TypeXishu,
	                                    T.virallotCount,
	                                    P.AllScale,
	                                    (Select SUM(virallotCount) From dbo.cm_KaoheProjType  Where KaoheNameID=P.ID) AS AllAllotCount,
	                                    (Select Count(*) From dbo.cm_KaoheProjType  Where KaoheNameID=P.ID) AS ColCount
                                        From cm_KaoHeProjName P left join cm_KaoheProjType T on P.ID=T.KaoheNameID
                                        Where (T.ID IS NOT NULL) AND (P.KaoHeProjId IN (Select ID 
							            From dbo.cm_KaoHeRenwuProj 
							            Where UnitID={0} AND YEAR(InsertDate)={1} AND RenWuId={2} ))  
                                        Order By P.ID", this.drp_unit3.SelectedValue, this.drp_year.SelectedValue, this.drprenwu.SelectedValue);

                }

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;
            }
        }
    }
}