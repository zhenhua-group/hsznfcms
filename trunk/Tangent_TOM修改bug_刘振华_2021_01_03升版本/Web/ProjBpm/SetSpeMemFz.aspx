﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="SetSpeMemFz.aspx.cs" Inherits="TG.Web.ProjBpm.SetSpeMemFz" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" />
    <!--CSS-->
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <!--JS-->
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/UserControl/ChooseCustomer.js" type="text/javascript"></script>
    <script src="../js/Common/ShowDivDialog.js" type="text/javascript"></script>

    <script src="../js/ProjBpm/SetSpeMemFz.js?v=20170117"></script>
    <script src="../js/MessageComm.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目考核 <small>设置专业负责人</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目考核</a><i class="fa fa-angle-right"> </i><a>设置专业负责人</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>专业负责人设置
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered " id="tbSetMems" style="width: 600px;">
                            <thead>
                                <tr>
                                    <%
                                        if (KaoHeNameModel.KName.Contains("室外工程") || KaoHeNameModel.KName.Contains("室外工程(密云小镇)"))
                                        { %>
                                        <th colspan="4"><%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-info"><%= KaoHeNameModel.KName%></span>)专业：<%=SpeName %>
                                            </th>
                                        
                                      <%  }
                                          else
                                          {%>
                                         <th colspan="4"><%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)专业：<%=SpeName %>
                                         </th>
                                        <%}
                                     %>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <% SpecialList.ForEach(c =>
                                   { %>
                                <tr>
                                    <td style="width: 150px;"><span class="bage bage-info"><%= c.spe_Name %></span>负责人</td>
                                    <td style="width: 150px;"></td>
                                    <td style="width: 150px;">
                                        <a spename="<%= c.spe_Name %>" speid="<%= c.spe_ID %>" href="#PMName" class="btn default btn-xs green-stripe" data-toggle="modal" action="adduser">添加</a>
                                     </td>
                                    <td></td>
                                </tr>
                                <%}); %>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="center">
                                        <% if (IsSendAudit)
                                           { %>
                                        <a href="#" class="btn btn-sm red disabled" id="btnSaveMems">保存</a>
                                  
                                         <%}
                                           else
                                           {%>
                                          <a href="#" class="btn btn-sm red " id="btnSaveMems">保存</a>
                                           <a href="#" class="btn btn-sm red" id="btnSaveMemsyy">撤回</a>
                                        <%} %>
                              
                                
                                        <a href="/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=A" class="btn btn-sm default" id="btnSaveMemsCancel">取消</a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo" />
    <!--项目ID-->
    <input type="hidden" name="name" value="<%= ProID %>" id="hidTempProid" />
    <!--项目考核ID-->
    <input type="hidden" name="name" value="<%= ProKHID %>" id="hidTempKaoHeProjId" />
    <!--考核名称与类型ID-->
    <input type="hidden" name="name" value="<%= KaoHeNameID %>" id="hidTempTypeProjNameid" />
    <!--考核类型ID-->
    <input type="hidden" name="name" value="<%= KaoHeTypeID %>" id="hidTempTypeId" />
    <!--专业-->
    <input type="hidden" name="name" value="<%= SpeID %>" id="hidSpeID" />
    <input type="hidden" id="msgno" value="<%= MessageID %>" />

    <!---标识部门专业--->
    <input type="hidden" name="name" value="0" id="hidFlagSpe" />

    <!--项目经理弹出层-->
    <div id="PMName" class="modal fade yellow" tabindex="-1" data-width="400" aria-hidden="true"
        style="display: none; width: 400px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择人员</h4>
        </div>
        <div class="modal-body">
            <div id="gcFzr_Dialog">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>生产部门:
                                    </td>
                                    <td>
                                        <select id="select_gcFzr_Unit" class="from-control">
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="gcFzr_MemTable" class="table table-bordered" style="text-align: center">
                                <tr class="trBackColor">
                                    <td style="width: 60px;">序号
                                    </td>
                                    <td style="width: 120px;">人员名称
                                    </td>
                                    <td style="width: 60px;">专业</td>
                                    <td align="center" style="width: 60px;">操作
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="gcFzr_ForPageDiv" class="divNavigation pageDivPosition">
                            总<label id="gcfzr_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;第<label id="gcfzr_nowPageIndex">0</label>/<label id="gcfzr_allPageCount">0</label>页&nbsp;&nbsp;<span id="gcfzr_firstPage" style="cursor: pointer;">首页</span>&nbsp; <span id="gcfzr_prevPage" style="cursor: pointer;"><<</span>&nbsp;&nbsp;<span id="gcfzr_nextPage" style="cursor: pointer;">>></span>&nbsp; <span id="gcfzr_lastPage" style="cursor: pointer;">末页</span>&nbsp;&nbsp;跳至<input type="text" id="gcfzr_pageIndex" style="width: 20px; border-style: none none solid none;" />页&nbsp;&nbsp; <span id="gcfzr_gotoPageIndex" style="cursor: pointer;">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>

    <script>
        SetSpeMemFz.init();
    </script>
</asp:Content>
