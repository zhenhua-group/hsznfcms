﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjBpm
{
    public partial class ProjectBpmAllot : PageBase
    {
        /// <summary>
        /// 项目ID
        /// </summary>
        public int ProID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["proid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 项目考核ID
        /// </summary>
        public int ProKHID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["prokhid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 考核名称ID
        /// </summary>
        public int KaoHeNameID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["khnameid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 考核类型ID
        /// </summary>
        public int KaoHeTypeID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["khtypeid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 负责人设置记录ID
        /// </summary>
        public int ID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["id"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 消息ID
        /// </summary>
        protected int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["MsgNo"] ?? "0", out msgid);
                return msgid;
            }
        }
        /// <summary>
        /// 虚拟总产值
        /// </summary>
        protected decimal? ProjVirAllCount
        {
            get
            {
                decimal? allcount = 0;
                var list = new TG.BLL.cm_KaoheProjType().GetModelList(" KaoheNameID=" + this.KaoHeNameID + "");

                if (list.Count > 0)
                {
                    list.ForEach(c =>
                    {
                        allcount += c.virallotCount;
                    });
                }

                return allcount;
            }
        }
        /// <summary>
        /// 项目完成比例
        /// </summary>
        protected decimal? DoneScale
        {
            get
            {
                decimal? scale = 0;
                var model = new TG.BLL.cm_KaoHeProjName().GetModel(this.KaoHeNameID);
                if (model != null)
                {
                    scale = model.DoneScale;
                }

                return scale;
            }
        }
        /// <summary>
        /// 奖金计提比例
        /// </summary>
        protected decimal? JiangJinJTBL
        {
            get
            {
                var list = new TG.BLL.cm_SysProjAllotXiShu().GetModelList(" jianc='ztjt' ");
                return list.Count > 0 ? list[0].bili : 0;
            }
        }
        /// <summary>
        /// 方案
        /// </summary>
        protected decimal? FAJiangJinJTBL
        {
            get
            {
                var list = new TG.BLL.cm_SysProjAllotXiShu().GetModelList(" jianc='fash' ");
                return list.Count > 0 ? list[0].bili : 0;
            }
        }
        /// <summary>
        /// 主持人
        /// </summary>
        protected decimal? ZCRJiangJinJTBL
        {
            get
            {
                var list = new TG.BLL.cm_SysProjAllotXiShu().GetModelList(" jianc='xmzcr' ");
                return list.Count > 0 ? list[0].bili : 0;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected decimal? ZCRJiXiao
        {
            get
            {
                decimal? result = 0;

                if (!IsSaveZcr)
                {
                    string strWhere = string.Format(" ProKHNameId={0} AND SpeID<>-2", this.KaoHeNameID);
                    var listmems = new TG.BLL.cm_KaoheSpecMems().GetModelList(strWhere);
                    strWhere = string.Format(" ProKHNameID={0} AND SpeRoleID=-2 AND Stat2=1 ", this.KaoHeNameID);
                    var listspes = new TG.BLL.cm_KaoHeSpePersent().GetModelList(strWhere);
                    //查询
                    var query = from m in listmems
                                join s in listspes on m.SpeID equals s.SpeID
                                select new
                                {
                                    m.Jixiao,
                                    s.Persent2
                                };

                    var list = query.ToList();

                    if (list.Count > 0)
                    {
                        list.ForEach(c =>
                        {
                            result += c.Jixiao * c.Persent2 * (decimal?)0.01;
                        });
                    }
                }
                else
                {
                    string strWhere = string.Format(" ProId={0} AND ProKHId={1} AND ProKHNameId={2} ", this.ProID, this.ProKHID, this.KaoHeNameID);

                    var projallotModel = new TG.BLL.cm_KaoHeProjAllot().GetModelList(strWhere);

                    if (projallotModel != null)
                    {
                        result = projallotModel[0].Zcjx;
                    }
                }

                return result;
            }
        }
        /// <summary>
        /// 考核名称
        /// </summary>
        protected TG.Model.cm_KaoHeProjName ProjNameModel
        {
            get
            {
                return new TG.BLL.cm_KaoHeProjName().GetModel(this.KaoHeNameID);
            }
        }
        /// <summary>
        /// 项目组奖金
        /// </summary>
        protected decimal? ProjCount
        { get; set; }
        /// <summary>
        /// 打开方式
        /// </summary>
        public string Action
        {
            get
            {
                return Request["action"] ?? "";
            }
        }

        public int Usermemid
        {
            get 
            {
                List<KaoHeSpePerentEntity> list = new List<KaoHeSpePerentEntity>();
                string strWhere = string.Format(" ProKHNameID={0} AND SpeRoleID=-2 AND Stat2=1 ", this.KaoHeNameID);
                var listspes = new TG.BLL.cm_KaoHeSpePersent().GetModelList(strWhere);
                var bllmem = new TG.BLL.tg_member();
                return Convert.ToInt32(listspes[0].InserUserID2);
            }
        
        }
        public string Usermemname
        {
            get
            {
                List<KaoHeSpePerentEntity> list = new List<KaoHeSpePerentEntity>();
                string strWhere = string.Format(" ProKHNameID={0} AND SpeRoleID=-2 AND Stat2=1 ", this.KaoHeNameID);
                var listspes = new TG.BLL.cm_KaoHeSpePersent().GetModelList(strWhere);
                var bllmem = new TG.BLL.tg_member();
                //查询结果
                if (listspes.Count > 0)
                {
                    //增加了部门经理审批界面
                    if (IsNewVersion)
                    {
                        var query = from p in listspes
                                    select new KaoHeSpePerentEntity
                                    {
                                        ProKHNameID = p.ProKHNameID,
                                        SpeID = p.SpeID,
                                        SpeName = p.SpeName,
                                        Persentold = p.Persentold,
                                        Persent1 = p.Persent1,
                                        Persent2 = p.Persent2,
                                        JiangJin = ProjCount * p.Persent2 * (decimal)0.01,
                                        JiangJinEnd = ProjCount * p.Persent2 * (decimal)0.01,
                                        InserUserID1 = bllmem.GetModel(p.InserUserID1).mem_Name,
                                        InserDate1 = p.InserDate1,
                                        InserUserID2 = bllmem.GetModel((int)p.InserUserID2).mem_Name,
                                        InserDate2 = p.InserDate2,
                                        Persent3 = p.Persent3,
                                        InserUserID3 = bllmem.GetModel((int)p.InserUserID3).mem_Name,
                                        InserDate3 = p.InserDate3

                                    };

                        list = query.ToList<KaoHeSpePerentEntity>().OrderBy(c => c.SpeID).ToList();

                    }
                    else//以前老数据
                    {
                        var query = from p in listspes
                                    select new KaoHeSpePerentEntity
                                    {
                                        ProKHNameID = p.ProKHNameID,
                                        SpeID = p.SpeID,
                                        SpeName = p.SpeName,
                                        Persentold = p.Persentold,
                                        Persent1 = p.Persent1,
                                        Persent2 = p.Persent2,
                                        JiangJin = ProjCount * p.Persent2 * (decimal)0.01,
                                        JiangJinEnd = ProjCount * p.Persent2 * (decimal)0.01,
                                        InserUserID1 = bllmem.GetModel(p.InserUserID1).mem_Name,
                                        InserDate1 = p.InserDate1,
                                        InserUserID2 = bllmem.GetModel((int)p.InserUserID2).mem_Name,
                                        InserDate2 = p.InserDate2
                                    };

                        list = query.ToList<KaoHeSpePerentEntity>().OrderBy(c => c.SpeID).ToList();
                    }

                }

                return list[0].InserUserID2;
            }

        }
        /// <summary>
        /// 专业间比例调整
        /// </summary>
        public List<KaoHeSpePerentEntity> KaoHeSpeList
        {
            get
            {
                List<KaoHeSpePerentEntity> list = new List<KaoHeSpePerentEntity>();
                string strWhere = string.Format(" ProKHNameID={0} AND SpeRoleID=-2 AND Stat2=1 ", this.KaoHeNameID);
                var listspes = new TG.BLL.cm_KaoHeSpePersent().GetModelList(strWhere);
                var bllmem = new TG.BLL.tg_member();
 
                //查询结果
                if (listspes.Count > 0)
                {
                    //增加了部门经理审批界面
                    if (IsNewVersion)
                    {
                        var query = from p in listspes
                                    select new KaoHeSpePerentEntity
                                    {
                                        ProKHNameID = p.ProKHNameID,
                                        SpeID = p.SpeID,
                                        SpeName = p.SpeName,
                                        Persentold = p.Persentold,
                                        Persent1 = p.Persent1,
                                        Persent2 = p.Persent2,
                                        JiangJin = ProjCount * p.Persent2 * (decimal)0.01,
                                        JiangJinEnd = ProjCount * p.Persent2 * (decimal)0.01,
                                        InserUserID1 = bllmem.GetModel(p.InserUserID1).mem_Name,
                                        InserDate1 = p.InserDate1,
                                        InserUserID2 = bllmem.GetModel((int)p.InserUserID2).mem_Name,
                                        InserDate2 = p.InserDate2,
                                        Persent3 = p.Persent3,
                                        InserUserID3 = bllmem.GetModel((int)p.InserUserID3).mem_Name,
                                        InserDate3 = p.InserDate3
                                        
                                    };

                        list = query.ToList<KaoHeSpePerentEntity>().OrderBy(c => c.SpeID).ToList();
               
                    }
                    else//以前老数据
                    {
                        var query = from p in listspes
                                    select new KaoHeSpePerentEntity
                                    {
                                        ProKHNameID = p.ProKHNameID,
                                        SpeID = p.SpeID,
                                        SpeName = p.SpeName,
                                        Persentold = p.Persentold,
                                        Persent1 = p.Persent1,
                                        Persent2 = p.Persent2,
                                        JiangJin = ProjCount * p.Persent2 * (decimal)0.01,
                                        JiangJinEnd = ProjCount * p.Persent2 * (decimal)0.01,
                                        InserUserID1 = bllmem.GetModel(p.InserUserID1).mem_Name,
                                        InserDate1 = p.InserDate1,
                                        InserUserID2 = bllmem.GetModel((int)p.InserUserID2).mem_Name,
                                        InserDate2 = p.InserDate2
                                    };

                        list = query.ToList<KaoHeSpePerentEntity>().OrderBy(c => c.SpeID).ToList();
                    }

                }

                return list;
            }
        }
        /// <summary>
        /// 人员最后奖金
        /// </summary>
        public List<KaoHeSpeMemsEntity> KaoHeSpeMemsList
        {
            get
            {
                var listzcr = new List<TG.Model.cm_KaoHeSpecMemsAllot>();
                if (IsSaveZcr)
                {
                    string where = string.Format(" ProKHNameId={0} ", this.KaoHeNameID);
                    listzcr = new TG.BLL.cm_KaoHeSpecMemsAllot().GetModelList(where);
                }

                List<KaoHeSpeMemsEntity> list = new List<KaoHeSpeMemsEntity>();
                string strWhere = string.Format(" ProKHNameID={0} AND SpeRoleID=-2 AND Stat2=1 ", this.KaoHeNameID);
                var listspes = new TG.BLL.cm_KaoHeSpePersent().GetModelList(strWhere);
                var bllmem = new TG.BLL.tg_member();

                //查询结果
                if (listspes.Count > 0)
                {
                    KaoHeSpeMemsEntity item;
                    //专业奖金
                    decimal? speTotalCount = 0;
                    //考核人
                    List<KaoHeMemEntity> kaohememList;
                    //项目经理
                    string roleFuze = "";
                    DateTime? roleFuzeTime;
                    string roleMng = "";
                    int memid = 0;
                    //string PKHID = "";
                    //string proID = "";
                    DateTime? roleMngTime;
                    //考核人
                    TG.Model.cm_KaoHeSpeMemsPersent memPrt;
                    //是否保存
                    bool issave = this.IsSaveZcr;
                    decimal? tempPersent2 = (decimal)0;
                    listspes.ForEach(p =>
                    {
                        speTotalCount = ProjCount * p.Persent2 * (decimal)0.01;
                        if (issave)
                        {
                            tempPersent2 = GetUpdatePersent(p.SpeID, p.ProKHNameID);
                            //更新后的
                            speTotalCount = ProjCount * tempPersent2 * (decimal)0.01;
                        }


                        kaohememList = GetMemList(p.SpeID, p.ProKHNameID, Convert.ToDecimal(speTotalCount));
                        memPrt = GetSpeMng(p.SpeID, p.ProKHNameID);
                        if (memPrt != null)
                        {
                            //负责人
                            if (memPrt.InsertUser1 > 0)
                            {
                                var user1 = bllmem.GetModel(memPrt.InsertUser1);
                                roleFuze = user1.mem_Name;
                            }
                            //部门经理
                            if (memPrt.InsertUser2 > 0)
                            {
                                var user2 = bllmem.GetModel(memPrt.InsertUser2);
                                roleMng = user2.mem_Name;
                                memid = user2.mem_ID;
                        
                            }

                            roleFuzeTime = memPrt.InsertDate1;
                            roleMngTime = memPrt.InsertDate2;
                        }
                        else
                        {
                            roleFuze = "未审批";
                            roleMng = "未审批";
                            roleFuzeTime = DateTime.Now;
                            roleMngTime = DateTime.Now;
                        }

                        item = new KaoHeSpeMemsEntity();
                        item.ProKHNameID = p.ProKHNameID;
                        item.SpeID = p.SpeID;
                        item.SpeName = p.SpeName;
                        item.Persentold = p.Persentold;
                        item.Persent1 = p.Persent1;
                        item.Persent2 = p.Persent2;
                        //保存会的数据
                        if (issave)
                        {
                            item.Persent2 = tempPersent2;
                        }
                        item.JiangJin = speTotalCount;
                        item.JiangJinEnd = speTotalCount;
                        item.KaoHeMemList = kaohememList;
                        item.InserUserID1 = roleFuze;
                        item.InserDate1 = roleFuzeTime;
                        item.InserUserID2 = roleMng;
                        item.InserDate2 = roleMngTime;
                        item.PKHID = memPrt.ProKHID;
                        item.proID = memPrt.ProID;
                        item.MemName=memPrt.MemName;
                        item.MemID = memid;
                        list.Add(item);
                    });
                }
                return list.OrderBy(c => c.SpeID).ToList();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public decimal? GetUpdatePersent(int speid, int nameid)
        {
            string where = string.Format(" ProKHNameId={0} AND SpeID={1} ", nameid, speid);
            var listzcr = new TG.BLL.cm_KaoHeSpecMemsAllot().GetModelList(where);
            if (listzcr.Count > 0)
            {
                return listzcr[0].PersentEnd;
            }
            else
            {
                return (decimal)0;
            }
        }
        /// <summary>
        /// 获取专业负责人和项目经理的审批时间
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        private TG.Model.cm_KaoHeSpeMemsPersent GetSpeMng(int p1, int p2)
        {
            string strWhere = string.Format(" KaoHeNameID={0} AND SpeID={1} AND Stat=1 ", p2, p1);

            var list = new TG.BLL.cm_KaoHeSpeMemsPersent().GetModelList(strWhere);
            if (list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 制图比例
        /// </summary>
        protected decimal? ZhiTuBl
        {
            get
            {
                var list = new TG.BLL.cm_SysProjAllotXiShu().GetModelList(" jianc='xmzt' ");
                return list.Count > 0 ? list[0].bili : 0;
            }
        }
        /// <summary>
        /// 校对
        /// </summary>
        protected decimal? XiaoDuiBl
        {
            get
            {
                var list = new TG.BLL.cm_SysProjAllotXiShu().GetModelList(" jianc='xmxd' ");
                return list.Count > 0 ? list[0].bili : 0;
            }
        }
        /// <summary>
        /// 审核
        /// </summary>
        protected decimal? ShenHeBl
        {
            get
            {
                var list = new TG.BLL.cm_SysProjAllotXiShu().GetModelList(" jianc='xmsh' ");
                return list.Count > 0 ? list[0].bili : 0;
            }
        }
        /// <summary>
        /// 方案深化奖金
        /// </summary>
        public decimal? FASHJiangJin
        {
            get;
            set;
        }
        /// <summary>
        /// 主持人奖金
        /// </summary>
        public decimal? ZCRenJiangJin
        {
            get;
            set;
        }
        /// <summary>
        /// 获取人员信息
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        private List<KaoHeMemEntity> GetMemList(int p1, int p2, decimal projcount)
        {
            string strWhere = string.Format(" KaoHeNameID={0} AND SpeID={1} AND Stat=1", p2, p1);

            var list = new TG.BLL.cm_KaoHeSpeMemsPersent().GetModelList(strWhere);
            var bllmem = new TG.BLL.tg_member();

            var resultlist = new List<KaoHeMemEntity>();
            //未保存项目参与人信息时
            if (!IsSaveZcr)
            {
                var query = from m in list
                            select new KaoHeMemEntity
                            {
                                ID = m.ID,
                                KaoHeNameID = m.KaoHeNameID,
                                MemID = m.MemID,
                                MemName = m.MemName,
                                SpeID = m.SpeID,
                                SpeName = m.SpeName,
                                Zt = m.Zt,
                                ZtEnd = projcount * m.Zt * (decimal)0.85 * (decimal)0.01,//这里有三者的比例
                                Xd = m.Xd,
                                XdEnd = projcount * m.Xd * (decimal)0.1 * (decimal)0.01,
                                Sh = m.Sh,
                                ShEnd = m.Sh * projcount * (decimal)0.05 * (decimal)0.01,
                                Fa = m.Fa,
                                FaEnd = m.Fa * FASHJiangJin * (decimal)0.01,
                                //FaEnd = m.Fa * ProjCount * FAJiangJinJTBL * (decimal)0.01,
                                Zc = m.Zc,
                                ZcEnd = m.Zc * ZCRenJiangJin * (decimal)0.01,
                                //ZcEnd = m.Zc * ZCRJiangJinJTBL * ZCRJiXiao * ProjCount * (decimal)0.01,
                                //AllCount = (projcount * m.Zt * (decimal)0.85 * (decimal)0.01) + (projcount * m.Xd * (decimal)0.1 * (decimal)0.01) + (m.Sh * projcount * (decimal)0.05 * (decimal)0.01) + (m.Fa * ProjCount * FAJiangJinJTBL * (decimal)0.01) + (m.Zc * ZCRJiangJinJTBL * ZCRJiXiao * ProjCount * (decimal)0.01),
                                AllCount = (projcount * m.Zt * (decimal)0.85 * (decimal)0.01) + (projcount * m.Xd * (decimal)0.1 * (decimal)0.01) + (m.Sh * projcount * (decimal)0.05 * (decimal)0.01) + (m.Fa * FASHJiangJin * (decimal)0.01) + (m.Zc * ZCRenJiangJin * (decimal)0.01),
                                InsertUser1 = m.InsertUser1,
                                InsertUserName1 = bllmem.GetModel(m.InsertUser2) == null ? "" : bllmem.GetModel(m.InsertUser1).mem_Name,
                                InsertDate1 = m.InsertDate1,
                                InsertUser2 = m.InsertUser2,
                                InsertUserName2 = bllmem.GetModel(m.InsertUser2) == null ? "" : bllmem.GetModel(m.InsertUser1).mem_Name,
                                InsertDate2 = m.InsertDate2
                            };

                resultlist = query.ToList<KaoHeMemEntity>();
            }
            else
            {
                strWhere = string.Format(" ProKHNameId={0} AND SpeID={1} ", p2, p1);
                var listmem = new TG.BLL.cm_KaoHeMemsAllot().GetModelList(strWhere);

                var query = from m in list
                            join p in listmem on m.MemID equals p.MemID
                            select new KaoHeMemEntity
                            {
                                ID = m.ID,
                                KaoHeNameID = m.KaoHeNameID,
                                MemID = m.MemID,
                                MemName = m.MemName,
                                SpeID = m.SpeID,
                                SpeName = m.SpeName,
                                Zt = m.Zt,
                                ZtEnd = p.Zt,//这里有三者的比例
                                Xd = m.Xd,
                                XdEnd = p.Xd,
                                Sh = m.Sh,
                                ShEnd = p.Sh,
                                Fa = m.Fa,
                                FaEnd = p.Fa,
                                Zc = m.Zc,
                                ZcEnd = p.Zc,
                                AllCount = p.AllotCount,
                                InsertUser1 = m.InsertUser1,
                                InsertUserName1 = bllmem.GetModel(m.InsertUser2) == null ? "" : bllmem.GetModel(m.InsertUser1).mem_Name,
                                InsertDate1 = m.InsertDate1,
                                InsertUser2 = m.InsertUser2,
                                InsertUserName2 = bllmem.GetModel(m.InsertUser2) == null ? "" : bllmem.GetModel(m.InsertUser1).mem_Name,
                                InsertDate2 = m.InsertDate2
                            };

                resultlist = query.ToList<KaoHeMemEntity>();
            }

            return resultlist;
        }
        /// <summary>
        /// 专业绩效
        /// </summary>
        public List<KaoHeSpeJixiaoEntity> KaoHeSpeJXList
        {
            get
            {
                string strWhere = string.Format(" ProKHNameId={0} AND SpeID<>-2", this.KaoHeNameID);
                var listmems = new TG.BLL.cm_KaoheSpecMems().GetModelList(strWhere);
                strWhere = string.Format(" ProKHNameID={0} AND SpeRoleID=-2 AND Stat2=1 ", this.KaoHeNameID);
                var listspes = new TG.BLL.cm_KaoHeSpePersent().GetModelList(strWhere);
                //查询
                var query = from m in listmems
                            join s in listspes on m.SpeID equals s.SpeID
                            select new KaoHeSpeJixiaoEntity
                            {
                                SpeID = m.SpeID,
                                SpeName = m.SpeName,
                                Persent2 = s.Persent2,
                                Jixiao = m.Jixiao
                            };

                var list = query.ToList<KaoHeSpeJixiaoEntity>().OrderBy(c => c.SpeID).ToList();

                return list;
            }
        }
        /// <summary>
        /// 调整单是否已经保存
        /// </summary>
        public bool IsSaveZcr
        {
            get
            {
                bool result = false;
                var bllzcr = new TG.BLL.cm_KaoHeSpecMemsAllot();
                string where = string.Format(" ProKHNameId={0} ", this.KaoHeNameID);
                var listzcr = bllzcr.GetModelList(where);

                if (listzcr.Count > 0)
                {
                    result = true;
                }
                return result;
            }
        }
        /// <summary>
        /// 是否已经提交
        /// </summary>
        public bool IsSubmit
        {
            get
            {
                bool result = false;
                var bllallot = new TG.BLL.cm_KaoHeProjAllot();
                string strWhere = string.Format(" ProKHNameId={0} AND Stat=1 ", this.KaoHeNameID);
                var listallot = bllallot.GetModelList(strWhere);

                if (listallot.Count > 0)
                {
                    if (listallot[0].Stat == 1)
                    {
                        result = true;
                    }
                }

                return result;
            }
        }
        /// <summary>
        /// 主持人
        /// </summary>
        public List<TG.Model.cm_KaoHeSpecMemsAllot> KaoHeZcrList
        {
            get
            {
                var bllzcr = new TG.BLL.cm_KaoHeSpecMemsAllot();
                string where = string.Format(" ProKHNameId={0} ", this.KaoHeNameID);
                var listzcr = bllzcr.GetModelList(where).OrderBy(c => c.SpeID).ToList();
                return listzcr;
            }
        }
        /// <summary>
        /// 获取分配实体
        /// </summary>
        public TG.Model.cm_KaoHeProjAllot KaoHeProjModel
        {
            get
            {
                var bllallot = new TG.BLL.cm_KaoHeProjAllot();
                string where = string.Format(" ProId={0} AND ProKHId={1} AND ProKHNameId={2}", this.ProID, this.ProKHID, this.KaoHeNameID);
                var list = bllallot.GetModelList(where);

                if (list.Count > 0)
                {
                    return list[0];
                }
                else
                {
                    return null;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 项目专业间比例实体
        /// </summary>
        public class KaoHeSpePerentEntity
        {
            public int ProKHNameID { get; set; }
            public int SpeID { get; set; }
            public string SpeName { get; set; }
            public decimal? Persentold { get; set; }
            public decimal? Persent1 { get; set; }
            public decimal? Persent2 { get; set; }
            public decimal? JiangJin { get; set; }
            public decimal? JiangJinEnd { get; set; }
            public string InserUserID1 { get; set; }
            public DateTime? InserDate1 { get; set; }
            public string InserUserID2 { get; set; }
            public DateTime? InserDate2 { get; set; }

            //添加部门经理审批阶段 2017年4月20日
            public decimal? Persent3 { get; set; }
            public string InserUserID3 { get; set; }
            public DateTime? InserDate3 { get; set; }
        }
        /// <summary>
        /// 专业个人奖金计算
        /// </summary>
        public class KaoHeSpeMemsEntity
        {
            public int ProKHNameID { get; set; }
            public int SpeID { get; set; }
            public string SpeName { get; set; }
            public decimal? Persentold { get; set; }
            public decimal? Persent1 { get; set; }
            public decimal? Persent2 { get; set; }
            public decimal? JiangJin { get; set; }
            public decimal? JiangJinEnd { get; set; }
            public string InserUserID1 { get; set; }
            public DateTime? InserDate1 { get; set; }
            public string InserUserID2 { get; set; }
            public DateTime? InserDate2 { get; set; }
            public List<KaoHeMemEntity> KaoHeMemList { get; set; }
            public int PKHID { get; set; }
            public int proID { get; set; }
          
            public string MemName{get;set;}
            public int MemID{get;set;}
         
        }
        public class KaoHeMemEntity
        {
            public int ID { get; set; }
            public int KaoHeNameID { get; set; }
            public int MemID { get; set; }
            public string MemName { get; set; }
            public int SpeID { get; set; }
            public string SpeName { get; set; }
            public decimal? Zt { get; set; }
            public decimal? ZtEnd { get; set; }
            public decimal? Xd { get; set; }
            public decimal? XdEnd { get; set; }
            public decimal? Sh { get; set; }
            public decimal? ShEnd { get; set; }
            public decimal? Fa { get; set; }
            public decimal? FaEnd { get; set; }
            public decimal? Zc { get; set; }
            public decimal? ZcEnd { get; set; }
            public int InsertUser1 { get; set; }
            public string InsertUserName1 { get; set; }
            public DateTime? InsertDate1 { get; set; }
            public int InsertUser2 { get; set; }
            public string InsertUserName2 { get; set; }
            public DateTime? InsertDate2 { get; set; }

            public decimal? AllCount { get; set; }
        }
        public class KaoHeSpeJixiaoEntity
        {
            public int SpeID { get; set; }
            public string SpeName { get; set; }
            public decimal? Persent2 { get; set; }

            public decimal? Jixiao { get; set; }
        }

        /// <summary>
        /// 考核项目
        /// </summary>
        public TG.Model.cm_Project ProjectModel
        {
            get
            {
                return new TG.BLL.cm_Project().GetModel(this.ProID);
            }
        }
        /// <summary>
        /// 考核名称
        /// </summary>
        public TG.Model.cm_KaoHeProjName KaoHeNameModel
        {
            get
            {
                return new TG.BLL.cm_KaoHeProjName().GetModel(this.KaoHeNameID);
            }
        }

        /// <summary>
        /// 是否是新版本
        /// </summary>
        public bool IsNewVersion
        {
            get
            {
                var bll = new TG.BLL.cm_KaoHeSpePersent();
                string strWhere = string.Format(" ProKHNameID={0} AND SpeRoleID=-2 AND Stat2=1 AND InserUserID3=0 AND Stat3=0 ", this.KaoHeNameID);
                int count = bll.GetRecordCount(strWhere);

                if (count > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }

        /// <summary>
        /// 获取已经保存的数据
        /// </summary>
        public List<TG.Model.cm_KaoHeSpeMemsPersent> KaoHeMemsPrt
        {
            get
            {
                string strWhere = string.Format(" ProID={0} AND ProKHID={1} AND KaoHeNameID={2} AND Stat2=1 ", this.ProID, this.ProKHID, this.KaoHeNameID);
                var list = new TG.BLL.cm_KaoHeSpeMemsPersent().GetModelList(strWhere).OrderBy(c => c.ID).ToList();
                return list;
            }
        }
    }

}