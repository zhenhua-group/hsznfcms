﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjectBpmListNew.aspx.cs" Inherits="TG.Web.ProjBpm.ProjectBpmListNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/fixtablehead/css/component.css" rel="stylesheet" />
    <%--<link rel="stylesheet" type="text/css" href="../js/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" href="../js/assets/plugins/data-tables/DT_bootstrap.css" />--%>

    <%--<script type="text/javascript" src="../js/assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/assets/plugins/data-tables/DT_bootstrap.js"></script>--%>

    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/UserControl/ChooseCustomer.js" type="text/javascript"></script>
    <script src="../js/Common/ShowDivDialog.js" type="text/javascript"></script>


    <script src="../js/ProjBpm/ProjectBpmListNew.js?v=20170119"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目考核 <small>设置考核项目</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目考核</a><i class="fa fa-angle-right"> </i><a>设置考核项目</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>设置考核项目
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 70px;">项目名称:</td>
                                <td style="width: 120px;">
                                    <asp:TextBox runat="server" ID="txtProjName" Width="150px"></asp:TextBox>
                                </td>
                                <td style="width: 70px;">项目年份:</td>
                                <td style="width: 90px;">
                                    <asp:DropDownList ID="drp_year" CssClass="form-control" Width="90px" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 70px;">项目部门:</td>
                                <td style="width: 95px;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server" Width="90px"
                                        AppendDataBoundItems="True" AutoPostBack="True">
                                        <asp:ListItem Value="-1">全部部门</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 70px;">
                                    <asp:Button CssClass="btn blue" ID="btnSearchProj" runat="server" Text="查询项目" OnClick="btnSearchProj_Click" />
                                </td>
                                <td style="width: 100px;">
                                    <a href="#" class="btn red" id="btnSaveProj"><i class="fa fa-save"></i>设置考核项目</a>
                                </td>
                                <td style="width: 80px;">考核任务:</td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drprenwu" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drprenwu_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 90px;">
                                    <% if (IsSetProj)
                                       { %>
                                    <span class="badge badge-success">已设置考核</span>
                                    <%}
                                       else
                                       { %>
                                    <span class="badge badge-danger">未设置考核</span>
                                    <%} %>
                                </td>
                                <td style="width: 40px;">
                                    <asp:Button CssClass="btn blue" ID="btn_Search" runat="server" Text="刷新" OnClick="btn_Search_Click" />
                                </td>
                                <td></td>
                            </tr>
                        </table>
                        <table>
                            <tr style="height:20px;"></tr>
                        </table>
                        <table class="table table-hover table-bordered table-full-width" id="tbData">
                            <thead>
                                <tr>
                                    <th style="width: 30px;">
                                        <input type="checkbox" name="name" id="chekALL" /></th>
                                    <th style="width: 200px;">项目名称</th>
                                    <th style="width: 100px;">考核名称</th>
                                    <th style="width: 80px;">完成比例</th>
                                    <th style="width: 80px;">比例备注</th>
                                    <th style="width: 90px;">建筑类型</th>
                                    <th style="width: 100px;">规模/万平米</th>
                                    <th style="width: 80px;">难度系数</th>
                                    <th style="width: 110px;">虚拟产值(万元)</th>
                                    <th style="width: 120px;">虚拟总产值(万元)</th>
                                    <th style="width: 80px;">整体绩效</th>
                                    <th style="width: 50px;"></th>
                                    <th style="width: 50px;"></th>
                                    <%--<th style="width: 50px;"></th>--%>
                                    <%--<th style="width: 50px;"></th>--%>
                                    <th style="width: 50px;"></th>
                                    <th style="width: 50px;">状态</th>

                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    if (ProjNameList.Rows.Count > 0)
                                    {
                                        int tempID = 0;
                                        int colcount = 0;
                                        bool iscol = false;
                                        foreach (System.Data.DataRow dr in ProjNameList.Rows)
                                        {
                                            if (tempID != Convert.ToInt32(dr["ID"].ToString()))
                                            {
                                                colcount = Convert.ToInt32(dr["ColCount"].ToString());
                                                iscol = colcount > 1 ? true : false;
                                            }
                                            else
                                            {
                                                iscol = false;
                                            }

                                            
                                %>
                                <tr>
                                    <!-- 选择考核项目 -->
                                    <%
                                            if (iscol)
                                            {   
                                    %>
                                    <td rowspan="<%= dr["ColCount"].ToString()%>">
                                        <% if (dr["IsSet"].ToString() == "0")
                                           { %>
                                        <input type="checkbox" name="name" proname="<%=dr["proName"].ToString() %>" unitid="<%=dr["UnitID"].ToString() %>" unitname="<%=dr["UnitName"].ToString() %>" proid="<%=dr["proID"].ToString() %>" />
                                        <%}
                                           else
                                           { %>
                                        <input type="checkbox" name="name" proname="<%=dr["proName"].ToString() %>" unitid="<%=dr["UnitID"].ToString() %>" unitname="<%=dr["UnitName"].ToString() %>" proid="<%=dr["proID"].ToString() %>" checked="checked" />
                                        <%} %>
                                    </td>
                                    <%}
                                            else
                                            {
                                                if (tempID != Convert.ToInt32(dr["ID"].ToString()))
                                                {%>
                                    <td style="text-align: right;">
                                        <% if (dr["IsSet"].ToString() == "0")
                                           { %>
                                        <input type="checkbox" name="name" proname="<%=dr["proName"].ToString() %>" unitid="<%=dr["UnitID"].ToString() %>" unitname="<%=dr["UnitName"].ToString() %>" proid="<%=dr["proID"].ToString() %>" />
                                        <%}
                                           else
                                           { %>
                                        <input type="checkbox" name="name" proname="<%=dr["proName"].ToString() %>" unitid="<%=dr["UnitID"].ToString() %>" unitname="<%=dr["UnitName"].ToString() %>" proid="<%=dr["proID"].ToString() %>" checked="checked" />
                                        <%} %>
                                    </td>
                                    <%
                                                }
                                            }
                                    %>


                                    <td><%= dr["proName"].ToString() %></td>
                                    <td style="text-align: right;"><%= dr["KName"].ToString() %></td>
                                    <td style="text-align: right;"><%= dr["DoneScale"].ToString() %>%</td>
                                    <td style="text-align: right;"><%= dr["Sub"].ToString() %></td>
                                    <td style="text-align: right;"><%= dr["KaoheTypeName"].ToString() %></td>
                                    <td style="text-align: right;"><%= dr["RealScale"].ToString() %></td>
                                    <td style="text-align: right;"><%= dr["TypeXishu"].ToString() %></td>
                                    <td style="text-align: right;"><%= dr["virallotCount"].ToString() %></td>
                                    <%
                                            if (iscol)
                                            { 
                                    %>
                                    <td style="text-align: right;" rowspan="<%= dr["ColCount"].ToString()%>"><%= dr["AllAllotCount"].ToString() %></td>
                                    <%}
                                            else
                                            {
                                                if (tempID != Convert.ToInt32(dr["ID"].ToString()))
                                                {%>
                                    <td style="text-align: right;"><%= dr["AllAllotCount"].ToString() %></td>
                                    <%}
                                            }

                                            tempID = Convert.ToInt32(dr["ID"].ToString()); %>

                                    <td style="text-align: right;"><%= dr["AllScale"].ToString() %></td>
                                    <td>
                                        <a href="#addKaoHeName" data-toggle="modal" class="btn default btn-xs green-stripe" asetmem="addname" proname="<%=dr["proName"].ToString() %>" unitid="<%=dr["UnitID"].ToString() %>" unitname="<%=dr["UnitName"].ToString() %>" proid="<%=dr["proID"].ToString() %>" kprojid="<%= dr["KaoHeProjId"].ToString() %>" title="添加考核名称">添加</a>
                                    </td>
                                    <td>
                                        <% if (dr["KName"].ToString() != "")
                                           { %>
                                        <a href="#addCharge" data-toggle="modal" class="btn default btn-xs blue-stripe" action="edit" pronameid="<%= dr["ID"].ToString() %>" proid="<%= dr["proID"].ToString()%>" prokhid="<%= dr["KaoHeProjId"].ToString()%>" typeid="<%= dr["TID"].ToString() %>" alt="编辑考核名称">编辑</a>
                                        <% }
                                           else
                                           { %>
                                        <a href="#" class="btn default btn-xs blue-stripe disabled" alt="编辑考核名称">编辑</a>
                                        <%} %>
                                    </td>
                                    <%--<td>
                                        <% if (dr["KName"].ToString() != "")
                                           { %>
                                        <a href="#" class="btn default btn-xs dark-stripe" proname="<%=dr["proName"].ToString() %>" unitid="<%=dr["UnitID"].ToString() %>" unitname="<%=dr["UnitName"].ToString() %>" proid="<%=dr["proID"].ToString() %>" kprojid="<%= dr["KaoHeProjId"].ToString() %>">删除</a>
                                        <% }
                                           else
                                           { %>
                                        <a href="#" class="btn default btn-xs dark-stripe disabled">删除</a>

                                        <%} %>
                                    </td>--%>
                                    <%--<td>
                                        <a class="btn default btn-xs purple-stripe " href="/ProjBpm/ProjectBpmAllot.aspx?proid=<%= dr["proID"].ToString()%>&prokhid=<%= dr["KaoHeProjId"].ToString()%>&khnameid=<%= dr["ID"].ToString()%>&action=check" title="人员设置">人员设置</a>
                                    </td>--%>
                                    <td>
                                        <% if (dr["KName"].ToString() != "")
                                           { %>
                                        <a target="_blank" class="btn default btn-xs red-stripe " href="/ProjBpm/ProjectBpmAllot.aspx?proid=<%= dr["proID"].ToString()%>&prokhid=<%= dr["KaoHeProjId"].ToString()%>&khnameid=<%= dr["ID"].ToString()%>&action=1" title="奖金调整">奖金调整</a>
                                        <% }
                                           else
                                           { %>
                                        <a href="#" class="btn default btn-xs red-stripe disabled">奖金调整</a>

                                        <%} %>
                                    </td>
                                    <td>
                                        <% if (Convert.ToInt32(dr["JJCount"].ToString()) > 0)
                                           {

                                               if (Convert.ToInt32(dr["JJCount2"].ToString()) > 0)
                                               { 
                                        %>
                                        <span class="badge badge-success">已提交</span>
                                        <%
                                               }
                                               else
                                               { %>
                                        <span class="badge badge-info">已保存</span>
                                        <%    }
                                           }
                                           else
                                           { %>
                                        <span class="badge badge-info">未调整</span>
                                        <%} %>
                                    </td>
                                    <td></td>
                                </tr>
                                <%}
                                    } %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- begin 弹出编辑考核名称设置界面 -->
    <div id="addCharge" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true" style="display: none; width: 900px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">编辑考核名称
            </h4>
        </div>
        <div class="modal-body">
            <div id="addChargeDialogDiv" style="color: #222222;">
                <table>
                    <tr>
                        <td style="width: 280px" valign="top">
                            <table class="table table-hover table-bordered table-full-width" id="tbNameAdd2">
                                <thead>
                                    <tr>
                                        <th colspan="4"><span id="spProjName"></span>(考核名称添加）
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="width: 100px;">考核名称</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtKName2" /></td>
                                    </tr>
                                    <tr>
                                        <td>整体绩效</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtAllScale2" /></td>
                                    </tr>
                                    <tr>
                                        <td>备注</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtSub2" /></td>
                                    </tr>
                                    <tr>
                                        <td>当期完成比例</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtDoneScale2" />%</td>
                                    </tr>
                                    <tr>
                                        <td>未完成比例</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtLeftScale2" />%</td>

                                    </tr>
                                    <tr>
                                        <td>历史完成比例</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtHisScale2" />%</td>
                                    </tr>
                                    <tr>
                                        <td>合计
                                        </td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtHeji2" />%</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td align="center" colspan="4">
                                            <a href="#" class="btn btn-sm red" id="btnaddprojName2">保存</a>
                                            <button type="button" data-dismiss="modal" class="btn btn-sm ">关闭</button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </td>
                        <td style="width: 280px" valign="top">
                            <table class="table table-hover table-bordered table-full-width" id="tbTypeAdd2">
                                <thead>
                                    <tr>
                                        <th colspan="2">编辑考核分类
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="width: 120px;">建筑类型:</td>
                                        <td>
                                            <select id="selType2">
                                                <option value="0">--请选择--</option>
                                                <% TypeEnumList.ForEach(c =>
                                               { %>
                                                <option value="<%=c.ID %>" type1="<%= c.Type1 %>" type5="<%= c.Type5 %>" type6="<%= c.Type6 %>" type10="<%= c.Type10 %>"><%= c.TypeName %></option>
                                                <%}); %>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>实际规模</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtRealScale2" />(万平米)</td>
                                    </tr>
                                    <tr>
                                        <td>难度系数</td>
                                        <td>
                                            <input type="text" name="name" value="" style="width: 120px;" id="txtTypeXishu2" /></td>
                                    </tr>
                                    <tr>
                                        <td>虚拟产值</td>
                                        <td><span id="spVirCount2">0</span></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <a href="#" class="btn btn-sm red" id="btnSaveType2">保存</a>
                                            <button type="button" data-dismiss="modal" class="btn btn-sm ">关闭</button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <!--考核名称ID-->
    <input type="hidden" name="name" value="0" id="hidTempProKHNameid2" />
    <!--项目ID-->
    <input type="hidden" name="name" value="0" id="hidTempProid2" />
    <!--项目考核ID-->
    <input type="hidden" name="name" value="0" id="hidTempKaoHeProjId2" />
    <!--考核名称与类型ID-->
    <input type="hidden" name="name" value="0" id="hidTempTypeProjNameid2" />
    <!--考核类型ID-->
    <input type="hidden" name="name" value="0" id="hidTempTypeId2" />
    <!--用户SysNo-->
    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo2" />

    <!-- end -->

    <!-- begin 弹出设计考核名称列表界面-->
    <div id="addKaoHeName" class="modal fade yellow" tabindex="-1" data-width="800" aria-hidden="true" style="display: none; width: 900px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">设置项目考核名称
            </h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">

                <table class="table table-hover table-bordered table-full-width" id="tbNameList">
                    <thead>
                        <tr>
                            <th colspan="10"><span id="spProjName2"></span>
                            </th>
                        </tr>
                        <tr>
                            <th style="width: 120px;">考核名称</th>
                            <th style="width: 90px;">考核分类</th>
                            <th style="width: 120px;">操作</th>
                            <th style="width: 120px;">虚拟总产值(万元)</th>
                            <th style="width: 60px;">已完成</th>
                            <th style="width: 60px;">未完成</th>
                            <th style="width: 90px;">任务名称</th>
                            <%--<th style="width: 70px;">继续考核</th>  --%>
                            <th style="width: 75px;">人员</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="10">无考核名称！
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table class="table table-hover table-bordered table-full-width" id="tbTypeList" style="display: none;">
                    <thead>
                        <tr>
                            <th style="width: 150px;">考核类型名称</th>
                            <th style="width: 100px;">规模(万平米)</th>
                            <th style="width: 40px;">系数</th>
                            <th style="width: 110px;">虚拟产值(万元)</th>
                            <th style="width: 150px;">操作</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="6">无考核分类！
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table class="table table-hover table-bordered table-full-width" id="tbNameAdd" style="display: none;">
                    <thead>
                        <tr>
                            <th colspan="4"><span id="spProjName"></span>(考核名称添加）
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 100px;">考核名称</td>
                            <td>
                                <input type="text" name="name" value="" style="width: 300px;" id="txtKName" /></td>
                        </tr>
                        <tr>
                            <td>整体绩效</td>
                            <td>
                                <input type="text" name="name" value="" style="width: 120px;" id="txtAllScale" /></td>
                        </tr>
                        <tr>
                            <td>备注</td>
                            <td>
                                <input type="text" name="name" value="" style="width: 120px;" id="txtSub" /></td>
                        </tr>
                        <tr>
                            <td>当期完成比例</td>
                            <td>
                                <input type="text" name="name" value="" style="width: 120px;" id="txtDoneScale" />%</td>
                        </tr>
                        <tr>
                            <td>未完成比例</td>
                            <td>
                                <input type="text" name="name" value="" style="width: 120px;" id="txtLeftScale" />%</td>

                        </tr>
                        <tr>
                            <td>历史完成比例</td>
                            <td>
                                <input type="text" name="name" value="" style="width: 120px;" id="txtHisScale" />%</td>
                        </tr>
                        <tr>
                            <td>合计
                            </td>
                            <td>
                                <input type="text" name="name" value="" style="width: 120px;" id="txtHeji" />%</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="center" colspan="4">
                                <a href="#" class="btn btn-sm red" id="btnaddprojName">保存</a>
                                <a href="#" class="btn btn-sm default" id="btnProNameCancel">取消</a>
                            </td>
                        </tr>
                    </tfoot>
                </table>

                <table class="table table-hover table-bordered table-full-width" id="tbNameDetails" style="display: none;">
                    <tbody>
                        <tr>
                            <td style="width: 100px;">考核名称</td>
                            <td>
                                <span id="spKName"></span></td>
                        </tr>
                        <tr>
                            <td>整体绩效</td>
                            <td>
                                <span id="spAllScale"></span></td>
                        </tr>
                        <tr>
                            <td>备注</td>
                            <td>
                                <span id="spSub"></span></td>
                        </tr>
                        <tr>
                            <td>完成比例</td>
                            <td>
                                <span id="spDoneScale"></span>%</td>
                        </tr>
                        <tr>
                            <td>未完成比例</td>
                            <td>
                                <span id="spLeftScale"></span>%</td>

                        </tr>
                        <tr>
                            <td>历史完成比例</td>
                            <td>
                                <span id="spHisScale"></span>%</td>
                        </tr>
                    </tbody>

                </table>

                <table class="table table-hover table-bordered table-full-width" id="tbTypeAdd" style="display: none;">
                    <thead>
                        <tr>
                            <th colspan="2">添加考核分类
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 120px;">建筑类型:</td>
                            <td>
                                <select id="selType">
                                    <option value="0">--请选择--</option>
                                    <% TypeEnumList.ForEach(c =>
                                               { %>
                                    <option value="<%=c.ID %>" type1="<%= c.Type1 %>" type5="<%= c.Type5 %>" type6="<%= c.Type6 %>" type10="<%= c.Type10 %>"><%= c.TypeName %></option>
                                    <%}); %>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>实际规模</td>
                            <td>
                                <input type="text" name="name" value="" style="width: 120px;" id="txtRealScale" />(万平米)</td>
                        </tr>
                        <tr>
                            <td>难度系数</td>
                            <td>
                                <input type="text" name="name" value="" style="width: 120px;" id="txtTypeXishu" /></td>
                        </tr>
                        <tr>
                            <td>虚拟产值</td>
                            <td><span id="spVirCount">0</span></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2" align="center">
                                <a href="#" class="btn btn-sm red" id="btnSaveType">保存</a>
                                <a href="#" class="btn btn-sm default" id="btnSaveTypeCancel">取消</a>
                            </td>
                        </tr>
                    </tfoot>
                </table>

                <table class="table table-hover table-bordered table-full-width" id="tbSetMems" style="display: none;">
                    <thead>
                        <tr>
                            <th colspan="4">项目考核人设置(<span id="roleTip" class="badge badge-info"></span>)[<span id="spSaveStat" class="badge badge-info">未保存</span>]
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 120px;"><span class="bage bage-info">总建筑师</span></td>
                            <td style="width: 200px;"></td>
                            <td style="width: 50px;"><a spename="项目经理" speid="-1" href="#PMName" class="btn default btn-xs green-stripe" data-toggle="modal" action="adduser">添加</a></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><span class="bage bage-info">主持人</span></td>
                            <td></td>
                            <td><a spename="主持人" speid="-2" href="#PMName" class="btn default btn-xs green-stripe" data-toggle="modal" action="adduser">添加</a></td>
                        </tr>
                        <% SpecialList.ForEach(c =>
                                   { %>
                        <tr>
                            <td><span class="bage bage-info"><%= c.spe_Name %></span>部门经理</td>
                            <td></td>
                            <td><a spename="<%= c.spe_Name %>" speid="<%= c.spe_ID %>" href="#PMName" class="btn default btn-xs green-stripe" data-toggle="modal" action="adduser">添加</a></td>
                            <td></td>
                        </tr>
                        <%}); %>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" align="center">
                                <a href="#" class="btn btn-sm red" id="btnSaveMems">保存</a>
                                <a href="#" class="btn btn-sm default" id="btnSaveMemsCancel">取消</a>
                            </td>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>

    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo" />
    <input type="hidden" name="name" value="<%= RenwuNo %>" id="hidRenwuId" />

    <input type="hidden" name="name" value="<%= ProcessRenwuID %>" id="hidProcessRenwuID" />
    <!-- 临时 -->
    <!--项目ID-->
    <input type="hidden" name="name" value="0" id="hidTempProid" />
    <!--部门ID-->
    <input type="hidden" name="name" value="0" id="hidTempUnitid" />
    <!--项目考核ID-->
    <input type="hidden" name="name" value="0" id="hidTempKaoHeProjId" />
    <!--考核名称ID-->
    <input type="hidden" name="name" value="0" id="hidTempProKHNameid" />
    <!--考核名称与类型ID-->
    <input type="hidden" name="name" value="0" id="hidTempTypeProjNameid" />
    <!--考核类型ID-->
    <input type="hidden" name="name" value="0" id="hidTempTypeId" />
    <!--考核人数-->
    <input type="hidden" name="name" value="0" id="hidIsSetMemsCount" />
    <!--编辑添加标示-->
    <input type="hidden" name="name" value="0" id="hidDoFlag" />
    <!--编辑历史考核名称时保留ID-->
    <input type="hidden" name="name" value="0" id="hidTempProKHNameidOld" />
    <!--标识人员设置时选择对象-->
    <input type="hidden" name="name" value="0" id="hidFlagZhuChi"/>

    <input type="hidden" name="name" value="<%= IsSetProj %>" id="hidIsSetProj" />

    <!--项目经理弹出层-->
    <div id="PMName" class="modal fade yellow" tabindex="-1" data-width="400" aria-hidden="true"
        style="display: none; width: 400px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择人员</h4>
        </div>
        <div class="modal-body">
            <div id="gcFzr_Dialog">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>生产部门:
                                    </td>
                                    <td>
                                        <select id="select_gcFzr_Unit" class="from-control">
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="gcFzr_MemTable" class="table table-bordered" style="text-align: center">
                                <tr class="trBackColor">
                                    <td style="width: 60px;">序号
                                    </td>
                                    <td style="width: 120px;">人员名称
                                    </td>
                                    <td style="width: 60px;">专业</td>
                                    <td align="center" style="width: 60px;">操作
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="gcFzr_ForPageDiv" class="divNavigation pageDivPosition">
                            总<label id="gcfzr_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;第<label id="gcfzr_nowPageIndex">0</label>/<label id="gcfzr_allPageCount">0</label>页&nbsp;&nbsp;<span id="gcfzr_firstPage" style="cursor: pointer;">首页</span>&nbsp; <span id="gcfzr_prevPage" style="cursor: pointer;"><<</span>&nbsp;&nbsp;<span id="gcfzr_nextPage" style="cursor: pointer;">>></span>&nbsp; <span id="gcfzr_lastPage" style="cursor: pointer;">末页</span>&nbsp;&nbsp;跳至<input type="text" id="gcfzr_pageIndex" style="width: 20px; border-style: none none solid none;" />页&nbsp;&nbsp; <span id="gcfzr_gotoPageIndex" style="cursor: pointer;">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
             关闭</button>
        </div>
    </div>
    <!-- end -->
    <script src="../js/fixtablehead/jquery.ba-throttle-debounce.min.js"></script>
    <script>
        ProjBpmNew.init();
    </script>

</asp:Content>
