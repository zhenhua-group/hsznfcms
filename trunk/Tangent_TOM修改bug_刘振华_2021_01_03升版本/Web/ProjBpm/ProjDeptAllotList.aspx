﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjDeptAllotList.aspx.cs" Inherits="TG.Web.ProjBpm.ProjDeptAllotList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目奖金 <small>项目奖金调整</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目奖金</a><i class="fa fa-angle-right"> </i><a>项目奖金调整</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>项目奖金调整
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">

                        <table class="table table-hover table-bordered table-full-width" style="display:none;">
                            <tr>
                                <td style="width: 80px; display: none;">考核任务:
                                </td>
                                <td style="width: 120px; display: none;">
                                    <asp:DropDownList CssClass="form-control" ID="drpRenwu" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 150px;">
                                    <asp:Button CssClass="btn btn-sm red" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" />

                                </td>
                                <td></td>
                            </tr>
                        </table>
                        <table class="table table-bordered table-hover" id="tbDataOne">
                            <thead>
                                <tr>
                                    <th style="width: 150px;">任务名称</th>
                                    <th style="width: 50px;"></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% RenwuList2.ForEach(c =>
                                   { 
                                %>
                                <tr>
                                    <td><%=c.RenWuNo %></td>

                                    <td>
                                        <a class="btn default btn-xs red-stripe" href="/ProjBpm/ProjDeptAllot.aspx?renwuid=<%= c.ID %>&proid=-1&prokhid=-1&khnameid=-1&action=2&check=chk">查看</a>
                                    </td>
                                    <td></td>
                                </tr>

                                <%}); %>
                            </tbody>
                        </table>



                        <%--<table class="table table-bordered table-hover" id="tbDataOne">
                            <thead>
                                <tr>
                                    <th style="width:300px;">项目名称</th>
                                    <th style="width:100px;">任务名称</th>
                                    <th style="width:80px;">考核名称</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    AllotChangeList.ForEach(c =>
                                    {%>

                                <tr>
                                    <td><%=c.proName %></td>
                                    <td><%= c.renwuName %></td>
                                    <td><%= c.KhName %></td>
                                    <td>
                                        <a class="btn default btn-xs red-stripe" href="/ProjBpm/ProjDeptAllot.aspx?proid=<%= c.proID %>&prokhid=<%= c.proKHid %>&khnameid=<%= c.KhNameId %>&action=2&check=chk">查看</a>
                                    </td>
                                    <td></td>
                                </tr>
                                <%
                                    });
                                %>
                            </tbody>
                        </table>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
