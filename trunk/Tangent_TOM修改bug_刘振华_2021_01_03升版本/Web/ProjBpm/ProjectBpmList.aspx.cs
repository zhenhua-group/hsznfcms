﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjBpm
{
    public partial class ProjectBpmList : PageBase
    {
        /// <summary>
        /// 考核任务ID
        /// </summary>
        public int RenwuId
        {
            get
            {
                int renwuid = 0;
                int.TryParse(Request["id"] ?? "0", out renwuid);
                return renwuid;
            }
        }

        public int RenwuNo
        {
            get
            {
                if (this.RenwuId == 0)
                {
                    return int.Parse(this.drprenwu.SelectedValue);
                }
                else
                {
                    return this.RenwuId;
                }
            }
        }
        /// <summary>
        /// 任务列表
        /// </summary>
        public List<TG.Model.cm_KaoHeRenWu> RenwuList
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list;
            }
        }
        public List<TG.Model.cm_KaoHeRenWu> RenwuListOrder
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list.OrderByDescending(c => c.StartDate).ToList();
            }
        }

        /// <summary>
        /// 进行中的任务ID
        /// </summary>
        public int ProcessRenwuID
        {
            get
            {
                int renwuid = 0;
                string strWhere = " ProjStatus='进行中' ";
                var bll = new BLL.cm_KaoHeRenWu();
                var list = bll.GetModelList(strWhere);

                if (list.Count > 0)
                {
                    list = list.OrderByDescending(c => c.ID).ToList();
                    renwuid = list[0].ID;
                }

                return renwuid;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                SelectCurrentYear();
                BindUnit();
                BindRenWu();
                BindProject();

                //选中当前的考核任务
                if (this.ProcessRenwuID != 0)
                {
                    this.drprenwu.ClearSelection();
                    if (this.drprenwu.Items.FindByValue(this.ProcessRenwuID.ToString()) != null)
                    {
                        this.drprenwu.Items.FindByValue(this.ProcessRenwuID.ToString()).Selected = true;
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void BindRenWu()
        {
            RenwuList.OrderByDescending(c => c.StartDate).ToList().ForEach(c =>
            {
                this.drprenwu.Items.Add(new ListItem(c.RenWuNo.ToString(), c.ID.ToString()));
            });
        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            this.drp_unit3.DataSource = bll_unit.GetList(" unit_ParentID<>0 AND unit_ID IN (231,232,233,234,236,237,238,239)");
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }
        //生产部门改变重新绑定数据
        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProject();
        }
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindProject();
        }

        protected DataTable ProjList
        { get; set; }
        /// <summary>
        /// 分配类型
        /// </summary>
        protected List<TG.Model.cm_KaoHeProjTypeEnum> TypeEnumList
        {
            get
            {
                return new TG.BLL.cm_KaoHeProjTypeEnum().GetModelList("").OrderBy(c => c.ID).ToList();
            }
        }
        /// <summary>
        /// 专业列表
        /// </summary>
        protected List<TG.Model.tg_speciality> SpecialList
        {
            get
            {
                return new TG.BLL.tg_speciality().GetModelList(" Spe_ID<6 ");
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
        protected void BindProject()
        {
            TG.BLL.cm_Project bll = new BLL.cm_Project();
            StringBuilder sb = new StringBuilder(" 1=1 ");
            StringBuilder sb2 = new StringBuilder("");
            //单位
            if (this.drp_unit3.SelectedIndex > 0)
            {
                sb.AppendFormat(" AND Unit='{0}'", this.drp_unit3.SelectedItem.Text);
            }
            //时间
            if (this.drp_year.SelectedIndex > 0)
            {
                sb.AppendFormat(" AND Year(pro_startTime)={0}", this.drp_year.SelectedItem.Text);
            }
            int renwuID = -1;
            if (this.drprenwu.SelectedIndex > 0)
            {
                renwuID = int.Parse(this.drprenwu.SelectedValue);
            }
            sb2.AppendFormat(" Pj.RenWuId={0}", renwuID);
            //查询总数
            this.AspNetPager1.RecordCount = bll.GetRecordCount(sb.ToString());
            //绑定
            ProjList = bll.GetListByPage(sb.ToString(), sb2.ToString(), "", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize - 1).Tables[0];
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindProject();
        }

        protected void drprenwu_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProject();
        }
    }
}