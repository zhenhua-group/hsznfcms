﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="SetSpePersent.aspx.cs" Inherits="TG.Web.ProjBpm.SetSpePersent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/ProjBpm/SetSpePersent.js?v=20170117"></script>
    <script src="../js/MessageComm.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目考核 <small>设置专业间比例</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目考核</a><i class="fa fa-angle-right"> </i><a>设置专业间比例</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>专业比例划分
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <% if (CurUserSperRole == -2)//主持人
                           { %>
                        <table class="table table-bordered table-hover" id="tbData">
                            <thead>
                                <tr>
                                     <%
                                        if (KaoHeNameModel.KName.Contains("室外工程") || KaoHeNameModel.KName.Contains("室外工程(密云小镇)"))
                                        { %>
                                        <th colspan="4">
                                            <%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-info"><%= KaoHeNameModel.KName%></span>)
                                            </th>
                                        
                                      <%  }
                                          else
                                          {%>
                                          <th colspan="4">
                                          <%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)
                                          </th>
                                        <%}
                                     %>
                                   
                                </tr>
                                <tr>
                                    <th style="width: 100px;">专业名称</th>
                                    <th style="width: 140px;">公司规定专业比例</th>
                                    <th style="width: 150px;">主持人调整专业比例</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% if (!IsSave)
                                   { %>
                                <tr>
                                    <td><span class="badge badge-warning">建筑</span></td>
                                    <td align="right"><span class="badge badge-danger" speid="1"><%= KaoHeTypeEnum.Jianzhu %></span>%</td>
                                    <td>
                                        <input speid="1" spename="建筑" type="text" name="name" value="<%= KaoHeTypeEnum.Jianzhu %>" style="width: 80px;" />%</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-warning">结构</span></td>
                                    <td align="right"><span class="badge badge-danger" speid="2"><%= KaoHeTypeEnum.Jiegou %></span>%</td>
                                    <td>
                                        <input speid="2" spename="结构" type="text" name="name" value="<%= KaoHeTypeEnum.Jiegou %>" style="width: 80px;" />%</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-warning">暖通</span></td>
                                    <td align="right"><span class="badge badge-danger" speid="3"><%= KaoHeTypeEnum.Nuantong %></span>%</td>
                                    <td>
                                        <input speid="3" spename="暖通" type="text" name="name" value="<%= KaoHeTypeEnum.Nuantong %>" style="width: 80px;" />%</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-warning">给排水</span></td>
                                    <td align="right"><span class="badge badge-danger" speid="4"><%= KaoHeTypeEnum.Shui %></span>%</td>
                                    <td>
                                        <input speid="4" spename="给排水" type="text" name="name" value="<%= KaoHeTypeEnum.Shui %>" style="width: 80px;" />%</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><span class="badge badge-warning">电气</span></td>
                                    <td align="right"><span class="badge badge-danger" speid="5"><%= KaoHeTypeEnum.Dianqi %></span>%</td>
                                    <td>
                                        <input speid="5" spename="电气" type="text" name="name" value="<%= KaoHeTypeEnum.Dianqi %>" style="width: 80px;" />%</td>
                                    <td></td>
                                </tr>
                            </tbody>
                            <%}
                                   else
                                   {
                                       bool issubmit = IsSubmit;
                                       SavePersentList.ForEach(c =>
                                       {
                                           if (issubmit)
                                           { 
                            %>
                            <tr>
                                <td><span class="badge badge-warning"><%= c.SpeName %></span></td>
                                <td align="right"><span class="badge badge-danger" speid="<%=c.SpeID %>"><%= c.Persentold %></span>%</td>
                                <td>
                                    <span speid="<%=c.SpeID %>" spename="<%= c.SpeName %>"><%= c.Persent1 %></span>%</td>
                                <td></td>
                            </tr>
                            <%}
                                           else
                                           {%>
                            <tr>
                                <td><span class="badge badge-warning"><%= c.SpeName %></span></td>
                                <td align="right"><span class="badge badge-danger" speid="<%=c.SpeID %>"><%= c.Persentold %></span>%</td>
                                <td>
                                    <input speid="<%=c.SpeID %>" spename="<%= c.SpeName %>" type="text" name="name" value="<%= c.Persent1 %>" style="width: 80px;" />%</td>
                                <td></td>
                            </tr>
                            <%}
                                       }); 
                            %>


                            <%} %>
                            <tfoot>
                                <tr>
                                    <td>合计</td>
                                    <td></td>
                                    <% if (IsSave)
                                       {%>
                                    <td><span style="font-weight: bold;" id="spAllCount">100</span>%</td>
                                    <% }
                                       else
                                       {%>
                                    <td><span style="font-weight: bold;" id="spAllCount">100</span>%</td>
                                    <%} %>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <% if (!IsSubmit)
                                           { %>
                                        <a href="#" class="btn btn-sm red" id="btnSave">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmit">确认提交</a>
                                        <span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>
                                        <% }
                                           else
                                           {%>
                                        <a href="/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=A" class="btn btn-sm default">返回</a>
                                        <%} %>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <%}
                           else if (CurUserSperRole == -1)//总建筑师
                           {
                        %>
                        <table class="table table-bordered table-hover" id="tbData">
                            <thead>
                                <tr>
                                    <%
                               if (KaoHeNameModel.KName.Contains("室外工程") || KaoHeNameModel.KName.Contains("室外工程(密云小镇)"))
                                        { %>
                                        <th colspan="6">
                                            <%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-info"><%= KaoHeNameModel.KName%></span>)
                                            </th>
                                        
                                      <%  }
                                          else
                                          {%>
                                           <th colspan="6">

                                        <%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)
                                         </th>
                                        <%}
                                     %>

                                  
                                </tr>
                                <tr>
                                    <th style="width: 100px;">专业名称</th>
                                    <th style="width: 140px;">公司规定专业比例</th>
                                    <th style="width: 150px;">主持人调整专业比例</th>
                                    <th style="width: 160px;">部门经理调整专业比例</th>
                                    <th style="width: 160px;">总建筑师调整专业比例</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                               bool issubmit = IsSubmit;
                               SavePersentList.ForEach(c =>
                               {
                                   if (issubmit)
                                   { 
                                %>
                                <tr>
                                    <td><span class="badge badge-warning"><%= c.SpeName %></span></td>
                                    <td align="right"><span class="badge badge-danger" speid="<%=c.SpeID %>"><%= c.Persentold %></span>%</td>
                                    <td align="right">
                                        <span class="badge badge-warning" speid="<%=c.SpeID %>" spename="<%= c.SpeName %>"><%= c.Persent1 %></span>%</td>
                                    <td align="right">
                                        <span class="badge badge-success" speid="<%=c.SpeID %>" spename="<%= c.SpeName %>"><%= c.Persent3 %></span>%
                                    </td>
                                    <td align="right">
                                        <span class="badge badge-success" speid="<%=c.SpeID %>" spename="<%= c.SpeName %>"><%= c.Persent2 %></span>%
                                    </td>
                                    <td></td>
                                </tr>
                                <%}
                                   else
                                   {%>
                                <tr>
                                    <td><span class="badge badge-warning"><%= c.SpeName %></span></td>
                                    <td align="right"><span class="badge badge-danger" speid="<%=c.SpeID %>"><%= c.Persentold %></span>%</td>
                                    <td align="right">
                                        <span class="badge badge-warning" speid="<%=c.SpeID %>" spename="<%= c.SpeName %>"><%= c.Persent1 %></span>%</td>
                                    <td align="right">
                                        <span class="badge badge-warning" speid="<%=c.SpeID %>" spename="<%= c.SpeName %>"><%= c.Persent3 %></span>%</td>

                                    <td>
                                        <% if (IsSave)
                                           { %>
                                        <input speid="<%=c.SpeID %>" spename="<%= c.SpeName %>" type="text" name="name" value="<%= c.Persent2 %>" style="width: 80px;" />
                                        <%}
                                           else
                                           { %>
                                        <input speid="<%=c.SpeID %>" spename="<%= c.SpeName %>" type="text" name="name" value="<%= c.Persent3 %>" style="width: 80px;" />
                                        <%} %>
                                        %</td>
                                    <td></td>

                                </tr>
                                <%}
                               }); 
                                %>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>合计</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <% if (IsSave)
                                       {%>
                                    <td><span style="font-weight: bold;" id="spAllCount">100</span>%</td>
                                    <% }
                                       else
                                       {%>
                                    <td><span style="font-weight: bold;" id="spAllCount">100</span>%</td>
                                    <%} %>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>主持人绩效</td>
                                    <td>
                                        <% if (!issubmit)
                                           { %>
                                        <select id="selJixiao">
                                            <option value="0.5">0.5</option>
                                            <option value="0.6">0.6</option>
                                            <option value="0.7">0.7</option>
                                            <option value="0.8">0.8</option>
                                            <option value="0.9">0.9</option>
                                            <option value="1.0">1.0</option>
                                            <option value="1.1">1.1</option>
                                            <option value="1.2">1.2</option>
                                            <option value="1.3">1.3</option>
                                            <option value="1.4">1.4</option>
                                            <option value="1.5">1.5</option>
                                        </select>
                                        <%}
                                           else
                                           { %>
                                        <span class="badge badge-success"><%= ZhuChiJiXiao %></span>
                                        <%} %>
                                    </td>
                                    <td colspan="4"></td>
                                </tr>

                                <tr>
                                    <td colspan="6">
                                        <% if (!IsSubmit)
                                           { %>
                                        <a href="#" class="btn btn-sm red" id="btnSave2">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmit2">确认提交</a>
                                        <span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>
                                        <% }
                                           else
                                           {%>
                                        <a href="/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=A" class="btn btn-sm default">返回</a>
                                        <%} %>

                                        <a style="display: <%= IsClose%>;" href="javascript:this.close();" class="btn btn-sm red">关闭</a>

                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <%
                           }
                           else if (CurUserSperRole == -3)//部门经理
                           {
                        %>

                        <table class="table table-bordered table-hover" id="tbData">
                            <thead>
                                <tr>
                                     <%
                               if (KaoHeNameModel.KName.Contains("室外工程") || KaoHeNameModel.KName.Contains("室外工程(密云小镇)"))
                                        { %>
                                        <th colspan="5">
                                            <%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-info"><%= KaoHeNameModel.KName%></span>)
                                            </th>
                                        
                                      <%  }
                                          else
                                          {%>
                                           <th colspan="5">

                                        <%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)
                                         </th>
                                        <%}
                                     %>

                                 <%--   <th colspan="5">
                                        <%= ProjectModel.pro_name.Trim() %>(<span class="badge badge-danger"><%= KaoHeNameModel.KName%></span>)
                                    </th>--%>
                                </tr>
                                <tr>
                                    <th style="width: 100px;">专业名称</th>
                                    <th style="width: 140px;">公司规定专业比例</th>
                                    <th style="width: 150px;">主持人调整专业比例</th>
                                    <th style="width: 160px;">部门经理调整专业比例</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                               bool issubmit = IsSubmit;
                               SavePersentList.ForEach(c =>
                               {
                                   if (issubmit)
                                   { 
                                %>
                                <tr>
                                    <td><span class="badge badge-warning"><%= c.SpeName %></span></td>
                                    <td align="right"><span class="badge badge-danger" speid="<%=c.SpeID %>"><%= c.Persentold %></span>%</td>
                                    <td align="right">
                                        <span class="badge badge-warning" speid="<%=c.SpeID %>" spename="<%= c.SpeName %>"><%= c.Persent1 %></span>%</td>
                                    <td align="right">
                                        <span class="badge badge-success" speid="<%=c.SpeID %>" spename="<%= c.SpeName %>"><%= c.Persent3 %></span>%
                                    </td>
                                    <td></td>
                                </tr>
                                <%}
                                   else
                                   {%>
                                <tr>
                                    <td><span class="badge badge-warning"><%= c.SpeName %></span></td>
                                    <td align="right"><span class="badge badge-danger" speid="<%=c.SpeID %>"><%= c.Persentold %></span>%</td>
                                    <td align="right">
                                        <span class="badge badge-warning" speid="<%=c.SpeID %>" spename="<%= c.SpeName %>"><%= c.Persent1 %></span>%</td>

                                    <td>
                                        <% if (IsSave)
                                           { %>
                                        <input speid="<%=c.SpeID %>" spename="<%= c.SpeName %>" type="text" name="name" value="<%= c.Persent3 %>" style="width: 80px;" />
                                        <%}
                                           else
                                           { %>
                                        <input speid="<%=c.SpeID %>" spename="<%= c.SpeName %>" type="text" name="name" value="<%= c.Persent1 %>" style="width: 80px;" />
                                        <%} %>
                                        %</td>
                                    <td></td>

                                </tr>
                                <%}
                               }); 
                                %>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>合计</td>
                                    <td></td>
                                    <td></td>
                                    <% if (IsSave)
                                       {%>
                                    <td><span style="font-weight: bold;" id="spAllCount">100</span>%</td>
                                    <% }
                                       else
                                       {%>
                                    <td><span style="font-weight: bold;" id="spAllCount">100</span>%</td>
                                    <%} %>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>主持人绩效</td>
                                    <td>
                                        <% if (!issubmit)
                                           { %>
                                        <select id="selJixiao">
                                            <option value="0.5">0.5</option>
                                            <option value="0.6">0.6</option>
                                            <option value="0.7">0.7</option>
                                            <option value="0.8">0.8</option>
                                            <option value="0.9">0.9</option>
                                            <option value="1.0">1.0</option>
                                            <option value="1.1">1.1</option>
                                            <option value="1.2">1.2</option>
                                            <option value="1.3">1.3</option>
                                            <option value="1.4">1.4</option>
                                            <option value="1.5">1.5</option>
                                        </select>
                                        <%}
                                           else
                                           { %>
                                        <span class="badge badge-success"><%= ZhuChiJiXiao %></span>
                                        <%} %>
                                    </td>
                                    <td colspan="3"></td>
                                </tr>

                                <tr>
                                    <td colspan="5">
                                        <% if (!IsSubmit)
                                           { %>
                                        <a href="#" class="btn btn-sm red" id="btnSave3">保存</a>
                                        <a href="#" class="btn btn-sm red" id="btnSubmit3">确认提交</a>
                                        <span style="font-weight: 600; color: #d84a38; margin-left: 10px;">确认提交后无法编辑<strong>!</strong></span>
                                        <% }
                                           else
                                           {%>
                                        <a href="/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=A" class="btn btn-sm default">返回</a>
                                        <%} %>

                                        <a style="display: <%= IsClose%>;" href="javascript:this.close();" class="btn btn-sm red">关闭</a>

                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                        <%}
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--隐藏域-->
    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo" />
    <input type="hidden" name="name" value="<%= KaoHeNameID %>" id="hidKHNameID" />
    <input type="hidden" name="name" value="<%= ZhuChiJiXiao %>" id="hidJixiao" />
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <input type="hidden" name="name" value="<%= CurUserSperRole %>" id="hidSpeRole" />

    <input type="hidden" name="name" value="<%= KaoHeNameModel.ID %>" id="hidKaoHeNameID" />
    <input type="hidden" name="name" value="<%= IsSubmit %>" id="hidIsSubmit" />

    <script>
        SetSpe.init();
    </script>
</asp:Content>
