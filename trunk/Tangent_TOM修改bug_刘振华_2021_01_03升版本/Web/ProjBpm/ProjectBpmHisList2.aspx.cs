﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.DBUtility;

namespace TG.Web.ProjBpm
{
    public partial class ProjectBpmHisList2 : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindKH();
                //选中当前的考核任务
                if (this.ProcessRenwuID != 0)
                {
                    this.drp_unit3.ClearSelection();
                    if (this.drp_unit3.Items.FindByValue(this.ProcessRenwuID.ToString()) != null)
                    {
                        this.drp_unit3.Items.FindByValue(this.ProcessRenwuID.ToString()).Selected = true;
                    }
                }
            }

            GetHisTable();
        }
        /// <summary>
        /// 审核记录
        /// </summary>
        public DataTable HisList
        {
            get;
            set;
        }
        /// <summary>
        /// 绑定历史列表
        /// </summary>
        protected void BindKH()
        {
            var list = new TG.BLL.cm_KaoHeRenWu().GetList(0, "", "ID DESC");

            this.drp_unit3.DataSource = list;
            this.drp_unit3.DataTextField = "RenWuNo";
            this.drp_unit3.DataValueField = "ID";
            this.DataBind();

        }
        /// <summary>
        /// 专业列表
        /// </summary>
        protected List<TG.Model.tg_speciality> SpecialList
        {
            get
            {
                return new TG.BLL.tg_speciality().GetModelList(" Spe_ID<6 ");
            }
        }
        /// <summary>
        /// 进行中的任务ID
        /// </summary>
        public int ProcessRenwuID
        {
            get
            {
                int renwuid = 0;
                string strWhere = " ProjStatus='进行中' ";
                var bll = new BLL.cm_KaoHeRenWu();
                var list = bll.GetModelList(strWhere);

                if (list.Count > 0)
                {
                    list = list.OrderByDescending(c => c.ID).ToList();
                    renwuid = list[0].ID;
                }

                return renwuid;
            }
        }
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            GetHisTable();
        }

        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetHisTable();
        }
        /// <summary>
        /// 历史表
        /// </summary>
        protected void GetHisTable()
        {
            #region 项目考核类型特殊类型的临时改动 2016年8月12日 qpl 后期得改回来

            //            string strSql = string.Format(@" select P.ID AS PKHID,P.proID,P.RenWuId,proName,KName,N.ID AS KID,N.Stat AS Stat
            //	            ,(select COUNT(*) From  cm_KaoheSpecMems where SpeID=-2 AND ProKHNameID=N.ID ) AS ZhuChi
            //	            ,(select COUNT(*) From  cm_KaoheSpecMems where SpeID=-1 AND ProKHNameID=N.ID ) AS XiangMu
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=1 AND KHTypeId=N.ID) end AS JianZhu
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0
            //	             then -1
            //	             else
            //	             (Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=1 AND KHTypeId=N.ID) end AS JianZhuBumen
            //	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=1 AND Stat3=1 AND KaoHeNameID=N.ID) AS JianZhuZong
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=2 AND KHTypeId=N.ID) end AS Jiegou
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=2 AND KHTypeId=N.ID) end AS JiegouBumen
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID IN (7,9))>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID NOT IN (7,9))=0
            //	             then -1
            //	             else
            //	             (Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=3 AND KHTypeId=N.ID) end AS Nuantong
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID IN (7,9))>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID NOT IN (7,9))=0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=3 AND KHTypeId=N.ID) end AS NuantongBumen
            //	            ,(Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=4 AND KHTypeId=N.ID) AS Shui
            //	            ,(Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=4 AND KHTypeId=N.ID) AS ShuiBumen
            //	            ,(Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=5 AND KHTypeId=N.ID) AS Dianqi
            //	            ,(Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=5 AND KHTypeId=N.ID) AS DianqiBumen
            //               ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0
            //				then 
            //               (select COUNT(*)+3 from cm_SysMsg Where ReferenceSysNo like 'proid='+convert(varchar,P.proID)+'&prokhid='+convert(varchar,P.ID)+'&khnameid='+convert(varchar,N.ID)+'&khtypeid='+convert(varchar,N.ID)+'&id=%&speid=_&setup=1')
            //               when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=9)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>9)=0
            //				then 
            //               (select COUNT(*)+1 from cm_SysMsg Where ReferenceSysNo like 'proid='+convert(varchar,P.proID)+'&prokhid='+convert(varchar,P.ID)+'&khnameid='+convert(varchar,N.ID)+'&khtypeid='+convert(varchar,N.ID)+'&id=%&speid=_&setup=1')
            //				else
            //               (select COUNT(*) from cm_SysMsg Where ReferenceSysNo like 'proid='+convert(varchar,P.proID)+'&prokhid='+convert(varchar,P.ID)+'&khnameid='+convert(varchar,N.ID)+'&khtypeid='+convert(varchar,N.ID)+'&id=%&speid=_&setup=1')
            //				
            //                end AS MsgCount 
            //            from dbo.cm_KaoHeRenwuProj P right join dbo.cm_KaoHeProjName N on P.ID=N.KaoHeProjId
            //            where p.RenWuId={0}
            //                ", this.drp_unit3.SelectedValue);

            #endregion

            #region 分页注销
            //            string strSql = string.Format(@" select P.ID AS PKHID,P.proID,P.RenWuId,proName,KName,N.ID AS KID,N.Stat AS Stat
            //	            ,(select COUNT(*) From  cm_KaoheSpecMems where SpeID=-2 AND ProKHNameID=N.ID ) AS ZhuChi
            //	            ,(select COUNT(*) From  cm_KaoheSpecMems where SpeID=-1 AND ProKHNameID=N.ID ) AS XiangMu
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=1 AND ProKHNameID=N.ID)=0
            //                          and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>1 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=1 AND KHTypeId=N.ID) end AS JianZhu
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=1 AND ProKHNameID=N.ID)=0
            //                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>1 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //	             (Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=1 AND KHTypeId=N.ID) end AS JianZhuBumen
            //	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=1 AND Stat3=1 AND KaoHeNameID=N.ID) AS JianZhuZong
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=2 AND ProKHNameID=N.ID)=0
            //                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>2 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=2 AND KHTypeId=N.ID) end AS Jiegou
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=2 AND ProKHNameID=N.ID)=0
            //                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>2 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=2 AND KHTypeId=N.ID) end AS JiegouBumen
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=3 AND ProKHNameID=N.ID)=0
            //                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>3 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //	             (Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=3 AND KHTypeId=N.ID) end AS Nuantong
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=3 AND ProKHNameID=N.ID)=0
            //                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>3 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=3 AND KHTypeId=N.ID) end AS NuantongBumen
            //	            ,(Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=4 AND KHTypeId=N.ID) AS Shui
            //	            ,(Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=4 AND KHTypeId=N.ID) AS ShuiBumen
            //	            ,(Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=5 AND KHTypeId=N.ID) AS Dianqi
            //	            ,(Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=5 AND KHTypeId=N.ID) AS DianqiBumen
            //               ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0
            //				then 
            //               (select COUNT(*)+3 from cm_SysMsg Where ReferenceSysNo like 'proid='+convert(varchar,P.proID)+'&prokhid='+convert(varchar,P.ID)+'&khnameid='+convert(varchar,N.ID)+'&khtypeid='+convert(varchar,N.ID)+'&id=%&speid=_&setup=1')
            //               when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=9)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>9)=0
            //				then 
            //               (select COUNT(*)+1 from cm_SysMsg Where ReferenceSysNo like 'proid='+convert(varchar,P.proID)+'&prokhid='+convert(varchar,P.ID)+'&khnameid='+convert(varchar,N.ID)+'&khtypeid='+convert(varchar,N.ID)+'&id=%&speid=_&setup=1')
            //				else
            //               (select COUNT(*) from cm_SysMsg Where ReferenceSysNo like 'proid='+convert(varchar,P.proID)+'&prokhid='+convert(varchar,P.ID)+'&khnameid='+convert(varchar,N.ID)+'&khtypeid='+convert(varchar,N.ID)+'&id=%&speid=_&setup=1')
            //				
            //                end AS MsgCount 
            //            from dbo.cm_KaoHeRenwuProj P right join dbo.cm_KaoHeProjName N on P.ID=N.KaoHeProjId
            //            where p.RenWuId={0} Order By proName
            //                ", this.drp_unit3.SelectedValue);
            #endregion
            string strWhere = "";
            //记录数
            this.AspNetPager1.RecordCount = this.GetRecordCount(strWhere);
            //获取分页数据
            DataTable dt = this.GetListByPage(strWhere, "", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize).Tables[0];
            this.HisList = dt;
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            GetHisTable();
        }
        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="orderby"></param>
        /// <param name="startIndex"></param>
        /// <param name="endIndex"></param>
        /// <returns></returns>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            string strSql = string.Format(@" select P.ID AS PKHID,P.proID,P.RenWuId,proName,KName,N.ID AS KID,N.Stat AS Stat
	            ,(select COUNT(*) From  cm_KaoheSpecMems where SpeID=-2 AND ProKHNameID=N.ID ) AS ZhuChi
	            ,(select COUNT(*) From  cm_KaoheSpecMems where SpeID=-1 AND ProKHNameID=N.ID ) AS XiangMu
	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=1 AND ProKHNameID=N.ID)=0
                          and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>1 AND ProKHNameID=N.ID)>0
	             then -1
	             else
	            (Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=1 AND KHTypeId=N.ID) end AS JianZhu
	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=1 AND ProKHNameID=N.ID)=0
                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>1 AND ProKHNameID=N.ID)>0
	             then -1
	             else
	             (Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=1 AND KHTypeId=N.ID) end AS JianZhuBumen
	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=1 AND Stat3=1 AND KaoHeNameID=N.ID) AS JianZhuZong
	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=2 AND ProKHNameID=N.ID)=0
                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>2 AND ProKHNameID=N.ID)>0
	             then -1
	             else
	            (Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=2 AND KHTypeId=N.ID) end AS Jiegou
	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=2 AND ProKHNameID=N.ID)=0
                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>2 AND ProKHNameID=N.ID)>0
	             then -1
	             else
	            (Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=2 AND KHTypeId=N.ID) end AS JiegouBumen
	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=3 AND ProKHNameID=N.ID)=0
                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>3 AND ProKHNameID=N.ID)>0
	             then -1
	             else
	             (Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=3 AND KHTypeId=N.ID) end AS Nuantong
	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=3 AND ProKHNameID=N.ID)=0
                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>3 AND ProKHNameID=N.ID)>0
	             then -1
	             else
	            (Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=3 AND KHTypeId=N.ID) end AS NuantongBumen
	            ,(Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=4 AND KHTypeId=N.ID) AS Shui
	            ,(Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=4 AND KHTypeId=N.ID) AS ShuiBumen
	            ,(Select COUNT(*) From  dbo.cm_KaoheSpecMemsFz Where SpeID=5 AND KHTypeId=N.ID) AS Dianqi
	            ,(Select COUNT(*) From  dbo.cm_KaoheSpecMems Where SpeID=5 AND KHTypeId=N.ID) AS DianqiBumen
               ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0
					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0
				then 
               (select COUNT(*)+3 from cm_SysMsg Where ReferenceSysNo like 'proid='+convert(varchar,P.proID)+'&prokhid='+convert(varchar,P.ID)+'&khnameid='+convert(varchar,N.ID)+'&khtypeid='+convert(varchar,N.ID)+'&id=%&speid=_&setup=1')
               when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=9)>0
					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>9)=0
				then 
               (select COUNT(*)+1 from cm_SysMsg Where ReferenceSysNo like 'proid='+convert(varchar,P.proID)+'&prokhid='+convert(varchar,P.ID)+'&khnameid='+convert(varchar,N.ID)+'&khtypeid='+convert(varchar,N.ID)+'&id=%&speid=_&setup=1')
				else
               (select COUNT(*) from cm_SysMsg Where ReferenceSysNo like 'proid='+convert(varchar,P.proID)+'&prokhid='+convert(varchar,P.ID)+'&khnameid='+convert(varchar,N.ID)+'&khtypeid='+convert(varchar,N.ID)+'&id=%&speid=_&setup=1')
				
                end AS MsgCount 
            from dbo.cm_KaoHeRenwuProj P right join dbo.cm_KaoHeProjName N on P.ID=N.KaoHeProjId
            where p.RenWuId={0} Order By proName
                ", this.drp_unit3.SelectedValue);

            return DbHelperSQL.Query(strSql, startIndex, endIndex);
        }

        public int GetRecordCount(string strWhere)
        {
            string strSql = string.Format(@" select Count(P.ID)
            from dbo.cm_KaoHeRenwuProj P right join dbo.cm_KaoHeProjName N on P.ID=N.KaoHeProjId
            where p.RenWuId={0}
            ", this.drp_unit3.SelectedValue);

            return Convert.ToInt32(DbHelperSQL.GetSingle(strSql));
        }
    }
}