﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjBpm
{
    public partial class SetSpeMemFz : PageBase
    {
        /// <summary>
        /// 消息ID
        /// </summary>
        protected int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["MsgNo"] ?? "0", out msgid);
                return msgid;
            }
        }
        /// <summary>
        /// 项目ID
        /// </summary>
        public int ProID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["proid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 项目考核ID
        /// </summary>
        public int ProKHID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["prokhid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 考核名称ID
        /// </summary>
        public int KaoHeNameID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["khnameid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 考核类型ID
        /// </summary>
        public int KaoHeTypeID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["khtypeid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 负责人设置记录ID
        /// </summary>
        public int ID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["id"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 专业ID
        /// </summary>
        public int SpeID
        {
            get
            {
                int speid = 0;
                int.TryParse(Request["speid"] ?? "0", out speid);
                return speid;
            }
        }
        /// <summary>
        /// 专业列表
        /// </summary>
        protected List<TG.Model.tg_speciality> SpecialList
        {
            get
            {
                return new TG.BLL.tg_speciality().GetModelList(" spe_ID=" + this.SpeID);
            }
        }
        /// <summary>
        /// 专业名称
        /// </summary>
        public string SpeName
        {
            get
            {
                return new TG.BLL.tg_speciality().GetModel(this.SpeID).spe_Name;
            }
        }
        public int RenwuID
        {
            get
            {
                int id = 0;
                var bllproj = new TG.BLL.cm_KaoHeRenwuProj();

                var model = bllproj.GetModel(this.ProKHID);
                if (model != null)
                {
                    id = model.RenWuId;
                }
                return id;
            }
        }
        /// <summary>
        /// 是否已经发起了审批
        /// </summary>
        public bool IsSendAudit
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                bool flag = false;
                sb.AppendFormat(" select case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0");
                sb.AppendFormat(" and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0");
                sb.AppendFormat(" then ");
                sb.AppendFormat(" (select COUNT(*)+3 from cm_SysMsg Where ReferenceSysNo like 'proid='+convert(varchar,P.proID)+'&prokhid='+convert(varchar,P.ID)+'&khnameid='+convert(varchar,N.ID)+'&khtypeid='+convert(varchar,N.ID)+'&id=%&speid=_&setup=1')");
                sb.AppendFormat(" when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=9)>0");
                sb.AppendFormat(" and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>9)=0");
                sb.AppendFormat(" then ");
                sb.AppendFormat(" (select COUNT(*)+1 from cm_SysMsg Where ReferenceSysNo like 'proid='+convert(varchar,P.proID)+'&prokhid='+convert(varchar,P.ID)+'&khnameid='+convert(varchar,N.ID)+'&khtypeid='+convert(varchar,N.ID)+'&id=%&speid=_&setup=1')");
                sb.AppendFormat(" else");
                sb.AppendFormat(" (select COUNT(*) from cm_SysMsg Where ReferenceSysNo like 'proid='+convert(varchar,P.proID)+'&prokhid='+convert(varchar,P.ID)+'&khnameid='+convert(varchar,N.ID)+'&khtypeid='+convert(varchar,N.ID)+'&id=%&speid=_&setup=1')");
                sb.AppendFormat(" end AS MsgCount ");
                sb.AppendFormat(" from dbo.cm_KaoHeRenwuProj P right join dbo.cm_KaoHeProjName N on P.ID=N.KaoHeProjId");
                sb.AppendFormat(" where p.RenWuId={0} AND N.ID={1}",RenwuID, KaoHeNameID);

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(sb.ToString()).Tables[0];

                if (dt.Rows.Count > 0)
                {
                    if (Convert.ToInt32(dt.Rows[0]["MsgCount"]) >= 5)
                    {
                        flag = true;
                    }
                }

                return flag;
            }
        }

        /// <summary>
        /// 考核项目
        /// </summary>
        public TG.Model.cm_Project ProjectModel
        {
            get
            {
                return new TG.BLL.cm_Project().GetModel(this.ProID);
            }
        }
        /// <summary>
        /// 考核名称
        /// </summary>
        public TG.Model.cm_KaoHeProjName KaoHeNameModel
        {
            get
            {
                return new TG.BLL.cm_KaoHeProjName().GetModel(this.KaoHeNameID);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}