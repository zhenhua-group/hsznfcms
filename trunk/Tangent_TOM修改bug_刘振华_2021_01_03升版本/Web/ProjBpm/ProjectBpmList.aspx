﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjectBpmList.aspx.cs" Inherits="TG.Web.ProjBpm.ProjectBpmList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" />
    <!--CSS-->
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <!--JS-->
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/UserControl/ChooseCustomer.js" type="text/javascript"></script>
    <script src="../js/Common/ShowDivDialog.js" type="text/javascript"></script>

    <script src="../js/ProjBpm/ProjectBpmList.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目考核 <small>设置考核项目</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目考核</a><i class="fa fa-angle-right"> </i><a>设置考核项目</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-7">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>设置考核项目
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 80px;">项目部门:
                                </td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server"
                                        AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="drp_unit3_SelectedIndexChanged">
                                        <asp:ListItem Value="-1">全部部门</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 80px;">项目年份:
                                </td>
                                <td style="width: 90px;">
                                    <asp:DropDownList ID="drp_year" CssClass="form-control" Width="90px" runat="server" AppendDataBoundItems="True">
                                        <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 80px;">考核任务:</td>
                                <td style="width: 120px;">
                                    <asp:DropDownList CssClass="form-control" ID="drprenwu" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drprenwu_SelectedIndexChanged">
                                        <asp:ListItem Value="-1">---请选择---</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 40px;">
                                    <asp:Button CssClass="btn btn-sm blue" ID="btn_Search" runat="server" Text="刷新" OnClick="btn_Search_Click" />
                                </td>
                                <td style="width: 40px;">
                                    <a href="../DeptBpm/DeptBpmList.aspx" class="btn btn-sm blue">返回</a>
                                </td>

                                <td></td>
                            </tr>
                        </table>

                        <table class="table table-hover table-bordered table-full-width" id="tbData">
                            <thead>
                                <tr>
                                    <th colspan="6">
                                        <span style="color: blue;">当前考核任务：<% RenwuListOrder.ForEach(c =>
                                           {
                                               if (c.ID == RenwuId && RenwuId.ToString() == this.drprenwu.SelectedValue)
                                               {%>
                                            <input type="checkbox" name="name" action="renwu" value="<%= c.ID%>" checked="checked" /><span style="font-weight: bold; color: blue;"><%=c.RenWuNo%></span>
                                            <%    }
                                               else
                                               {
                                                   if (RenwuId == 0)
                                                   {
                                                       if (c.ID.ToString() == this.drprenwu.SelectedValue)
                                                       {%>
                                            <input type="checkbox" name="name" action="renwu" value="<%= c.ID%>" checked="checked" /><span style="font-weight: bold; color: blue;"><%=c.RenWuNo%></span>

                                            <%}
                                                       else
                                                       { %>
                                            <%--<input type="checkbox" name="name" action="renwu" value="<%= c.ID%>" /><span style="font-weight: bold; color: blue;"><%=c.RenWuNo%></span>--%>

                                            <%       }
                                                   }
                                                   else
                                                   {
                                                       if (c.ID != RenwuId && RenwuId.ToString() != this.drprenwu.SelectedValue)
                                                       {%>
                                            <input type="checkbox" name="name" action="renwu" value="<%= c.ID%>" checked="checked" /><span style="font-weight: bold; color: blue;"><%=c.RenWuNo%></span>

                                            <% 
                                                       }
                                                       else
                                                       { %>

                                            <%--<input type="checkbox" name="name" action="renwu" value="<%= c.ID%>" /><span style="font-weight: bold; color: blue;"><%=c.RenWuNo%></span>--%>

                                            <% }
                                                   }
                                               }
                                           }); %></span>
                                        <a href="#" class="btn btn-xs red" id="btnSaveProj">保存设置考核项目&nbsp;<i class="fa fa-save"></i></a>
                                    </th>

                                </tr>
                                <tr>
                                    <th style="width: 30px;">
                                        <input type="checkbox" name="name" value="0" id="chkAll" /></th>
                                    <th style="width: 40px;">序号</th>
                                    <th>考核项目名称</th>
                                    <th style="width: 80px;">是否参与</th>
                                    <th style="width: 80px;">考核名称</th>
                                    <%--<th style="width: 80px;">分类设置</th>--%>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% 
                                    int index = 1;
                                    if (ProjList.Rows.Count > 0)
                                    {
                                        foreach (System.Data.DataRow dr in ProjList.Rows)
                                        {%>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="name" value="0" proid="<%= dr["pro_ID"].ToString() %>" /></td>
                                    <td><%=index++ %></td>
                                    <td><span proid="<%= dr["pro_ID"].ToString() %>" unitid="<%= dr["MemUnitID"].ToString() %>" unitname='<%=dr["Unit"].ToString()%>' style="font-weight: bold;"><%= dr["pro_name"].ToString() %></span></td>
                                    <td>
                                        <% if (dr["IsIn"].ToString() == "1")
                                           {%>
                                        <select>
                                            <option value="1" selected>否</option>
                                            <option value="0">是</option>
                                        </select>
                                        <%}
                                           else
                                           { %>
                                        <select>
                                            <option value="0" selected>是</option>
                                            <option value="1">否</option>
                                        </select>
                                        <%} %>
                                    </td>
                                    <td>
                                        <% if (dr["IsIn"].ToString() == "0")
                                           {%>
                                        <a href="#" class="btn default btn-xs green-stripe" asetmem="addname" proname="<%= dr["pro_Name"].ToString().Trim() %>" proid="<%= dr["pro_ID"].ToString() %>" unitid="<%= dr["UnitID"].ToString().Trim() %>" kprojid="<%= dr["ID"].ToString().Trim() %>">添加考核名称</a>
                                        <%}
                                           else
                                           { %>
                                        <a href="#" class="btn default btn-xs green-stripe disabled" asetmem="addname" proname="<%= dr["pro_Name"].ToString().Trim() %>" proid="<%= dr["pro_ID"].ToString() %>" unitid="<%= dr["UnitID"].ToString().Trim() %>" kprojid="<%= dr["ID"].ToString().Trim() %>">添加考核名称</a>
                                        <%} %>
                                    </td>
                                    <%--<td><a href="#" class="btn default btn-xs green-stripe disabled" asetmem="settype" proid="<%= dr["pro_ID"].ToString() %>" unitid="<%= dr["UnitID"].ToString() %>">添加分类</a></td>--%>
                                    <td></td>

                                </tr>
                                <%}
                                    } %>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                            CustomInfoSectionWidth="32%" CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                                            CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                            OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                                            PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                                            SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px; height:23px;"
                                            PageSize="30" SubmitButtonClass="btn green">
                                        </webdiyer:AspNetPager>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" align="center">
                                        <a href="#" class="btn btn-sm default">返回</a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i><span id="spTips">设置项目考核名称</span>
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">

                        <table class="table table-hover table-bordered table-full-width" id="tbNameList">
                            <thead>
                                <tr>
                                    <th colspan="10"><span id="spProjName2"></span>
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 120px;">考核名称</th>
                                    <th style="width: 90px;">考核分类</th>
                                    <th style="width: 120px;">操作</th>
                                    <th style="width: 120px;">虚拟总产值(万元)</th>
                                    <th style="width: 60px;">已完成</th>
                                    <th style="width: 60px;">未完成</th>
                                    <th style="width: 90px;">任务名称</th>
                                    <%--<th style="width: 70px;">继续考核</th>  --%>                                
                                    <th style="width: 75px;">人员</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="10">无考核名称！
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-hover table-bordered table-full-width" id="tbTypeList" style="display: none;">
                            <thead>
                                <tr>
                                    <th style="width: 150px;">考核类型名称</th>
                                    <th style="width: 100px;">规模(万平米)</th>
                                    <th style="width: 40px;">系数</th>
                                    <th style="width: 110px;">虚拟产值(万元)</th>
                                    <th style="width: 150px;">操作</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="6">无考核分类！
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-hover table-bordered table-full-width" id="tbNameAdd" style="display: none;">
                            <thead>
                                <tr>
                                    <th colspan="4"><span id="spProjName"></span>(考核名称添加）
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width: 100px;">考核名称</td>
                                    <td>
                                        <input type="text" name="name" value="" style="width: 300px;" id="txtKName" /></td>
                                </tr>
                                <tr>
                                    <td>整体绩效</td>
                                    <td>
                                        <input type="text" name="name" value="" style="width: 120px;" id="txtAllScale" /></td>
                                </tr>
                                <tr>
                                    <td>备注</td>
                                    <td>
                                        <input type="text" name="name" value="" style="width: 120px;" id="txtSub" /></td>
                                </tr>
                                <tr>
                                    <td>当期完成比例</td>
                                    <td>
                                        <input type="text" name="name" value="" style="width: 120px;" id="txtDoneScale" />%</td>
                                </tr>
                                <tr>
                                    <td>未完成比例</td>
                                    <td>
                                        <input type="text" name="name" value="" style="width: 120px; " id="txtLeftScale" />%</td>

                                </tr>
                                <tr>
                                    <td>历史完成比例</td>
                                    <td>
                                        <input type="text" name="name" value="" style="width: 120px; " id="txtHisScale" />%</td>
                                </tr>
                               <tr>
                                    <td>合计
                                    </td>
                                    <td>
                                        <input type="text" name="name" value="" style="width: 120px;" id="txtHeji" />%</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td align="center" colspan="4">
                                        <a href="#" class="btn btn-sm red" id="btnaddprojName">保存</a>
                                        <a href="#" class="btn btn-sm default" id="btnProNameCancel">取消</a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                        <table class="table table-hover table-bordered table-full-width" id="tbNameDetails" style="display: none;">
                            <tbody>
                                <tr>
                                    <td style="width: 100px;">考核名称</td>
                                    <td>
                                        <span id="spKName"></span></td>
                                </tr>
                                <tr>
                                    <td>整体绩效</td>
                                    <td>
                                        <span id="spAllScale"></span></td>
                                </tr>
                                <tr>
                                    <td>备注</td>
                                    <td>
                                        <span id="spSub"></span></td>
                                </tr>
                                <tr>
                                    <td>完成比例</td>
                                    <td>
                                        <span id="spDoneScale"></span>%</td>
                                </tr>
                                <tr>
                                    <td>未完成比例</td>
                                    <td>
                                        <span id="spLeftScale"></span>%</td>

                                </tr>
                                <tr>
                                    <td>历史完成比例</td>
                                    <td>
                                        <span id="spHisScale"></span>%</td>
                                </tr>
                            </tbody>

                        </table>

                        <table class="table table-hover table-bordered table-full-width" id="tbTypeAdd" style="display: none;">
                            <thead>
                                <tr>
                                    <th colspan="2">添加考核分类
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width: 120px;">建筑类型:</td>
                                    <td>
                                        <select id="selType">
                                            <option value="0">--请选择--</option>
                                            <% TypeEnumList.ForEach(c =>
                                               { %>
                                            <option value="<%=c.ID %>" type1="<%= c.Type1 %>" type5="<%= c.Type5 %>" type6="<%= c.Type6 %>" type10="<%= c.Type10 %>"><%= c.TypeName %></option>
                                            <%}); %>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>实际规模</td>
                                    <td>
                                        <input type="text" name="name" value="" style="width: 120px;" id="txtRealScale" />(万平米)</td>
                                </tr>
                                <tr>
                                    <td>难度系数</td>
                                    <td>
                                        <input type="text" name="name" value="" style="width: 120px;" id="txtTypeXishu" /></td>
                                </tr>
                                <tr>
                                    <td>虚拟产值</td>
                                    <td><span id="spVirCount">0</span></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2" align="center">
                                        <a href="#" class="btn btn-sm red" id="btnSaveType">保存</a>
                                        <a href="#" class="btn btn-sm default" id="btnSaveTypeCancel">取消</a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                        <table class="table table-hover table-bordered table-full-width" id="tbSetMems" style="display: none;">
                            <thead>
                                <tr>
                                    <th colspan="4">项目考核人设置(<span id="roleTip" class="badge badge-info"></span>)[<span id="spSaveStat" class="badge badge-info">未保存</span>]
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="width: 120px;"><span class="bage bage-info">总建筑师</span></td>
                                    <td style="width: 200px;"></td>
                                    <td style="width: 50px;"><a spename="项目经理" speid="-1" href="#PMName" class="btn default btn-xs green-stripe" data-toggle="modal" action="adduser">添加</a></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><span class="bage bage-info">主持人</span></td>
                                    <td></td>
                                    <td><a spename="主持人" speid="-2" href="#PMName" class="btn default btn-xs green-stripe" data-toggle="modal" action="adduser">添加</a></td>
                                </tr>
                                <% SpecialList.ForEach(c =>
                                   { %>
                                <tr>
                                    <td><span class="bage bage-info"><%= c.spe_Name %></span>部门经理</td>
                                    <td></td>
                                    <td><a spename="<%= c.spe_Name %>" speid="<%= c.spe_ID %>" href="#PMName" class="btn default btn-xs green-stripe" data-toggle="modal" action="adduser">添加</a></td>
                                    <td></td>
                                </tr>
                                <%}); %>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" align="center">
                                        <a href="#" class="btn btn-sm red" id="btnSaveMems">保存</a>
                                        <a href="#" class="btn btn-sm default" id="btnSaveMemsCancel">取消</a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo" />
    <input type="hidden" name="name" value="<%= RenwuNo %>" id="hidRenwuId" />

    <input type="hidden" name="name" value="<%= ProcessRenwuID %>" id="hidProcessRenwuID" />
    <!-- 临时 -->
    <!--项目ID-->
    <input type="hidden" name="name" value="0" id="hidTempProid" />
    <!--部门ID-->
    <input type="hidden" name="name" value="0" id="hidTempUnitid" />
    <!--项目考核ID-->
    <input type="hidden" name="name" value="0" id="hidTempKaoHeProjId" />
    <!--考核名称ID-->
    <input type="hidden" name="name" value="0" id="hidTempProKHNameid" />
    <!--考核名称与类型ID-->
    <input type="hidden" name="name" value="0" id="hidTempTypeProjNameid" />
    <!--考核类型ID-->
    <input type="hidden" name="name" value="0" id="hidTempTypeId" />
    <!--考核人数-->
    <input type="hidden" name="name" value="0" id="hidIsSetMemsCount" />
    <!--编辑添加标示-->
    <input type="hidden" name="name" value="0" id="hidDoFlag" />
    <!--编辑历史考核名称时保留ID-->
    <input type="hidden" name="name" value="0" id="hidTempProKHNameidOld" />
    <!--项目经理弹出层-->
    <div id="PMName" class="modal fade yellow" tabindex="-1" data-width="400" aria-hidden="true"
        style="display: none; width: 400px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择人员</h4>
        </div>
        <div class="modal-body">
            <div id="gcFzr_Dialog">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>生产部门:
                                    </td>
                                    <td>
                                        <select id="select_gcFzr_Unit" class="from-control">
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="gcFzr_MemTable" class="table table-bordered" style="text-align: center">
                                <tr class="trBackColor">
                                    <td style="width: 60px;">序号
                                    </td>
                                    <td style="width: 120px;">人员名称
                                    </td>
                                    <td style="width: 60px;">专业</td>
                                    <td align="center" style="width: 60px;">操作
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="gcFzr_ForPageDiv" class="divNavigation pageDivPosition">
                            总<label id="gcfzr_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;第<label id="gcfzr_nowPageIndex">0</label>/<label id="gcfzr_allPageCount">0</label>页&nbsp;&nbsp;<span id="gcfzr_firstPage" style="cursor: pointer;">首页</span>&nbsp; <span id="gcfzr_prevPage" style="cursor: pointer;"><<</span>&nbsp;&nbsp;<span id="gcfzr_nextPage" style="cursor: pointer;">>></span>&nbsp; <span id="gcfzr_lastPage" style="cursor: pointer;">末页</span>&nbsp;&nbsp;跳至<input type="text" id="gcfzr_pageIndex" style="width: 20px; border-style: none none solid none;" />页&nbsp;&nbsp; <span id="gcfzr_gotoPageIndex" style="cursor: pointer;">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <script>
        ProjBpm.init();
    </script>
</asp:Content>
