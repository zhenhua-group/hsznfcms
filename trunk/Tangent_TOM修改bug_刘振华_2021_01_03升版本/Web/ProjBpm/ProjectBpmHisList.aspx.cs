﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.DBUtility;

namespace TG.Web.ProjBpm
{
    public partial class ProjectBpmHisList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindKH();


                //选中当前的考核任务
                if (this.ProcessRenwuID != 0)
                {
                    this.drp_unit3.ClearSelection();
                    if (this.drp_unit3.Items.FindByValue(this.ProcessRenwuID.ToString()) != null)
                    {
                        this.drp_unit3.Items.FindByValue(this.ProcessRenwuID.ToString()).Selected = true;
                    }
                }
            }

            GetHisTable();
        }
        /// <summary>
        /// 审核记录
        /// </summary>
        public DataTable HisList
        {
            get;
            set;
        }
        /// <summary>
        /// 进行中的任务ID
        /// </summary>
        public int ProcessRenwuID
        {
            get
            {
                int renwuid = 0;
                string strWhere = " ProjStatus='进行中' ";
                var bll = new BLL.cm_KaoHeRenWu();
                var list = bll.GetModelList(strWhere);

                if (list.Count > 0)
                {
                    list = list.OrderByDescending(c => c.ID).ToList();
                    renwuid = list[0].ID;
                }

                return renwuid;
            }
        }
        /// <summary>
        /// 绑定历史列表
        /// </summary>
        protected void BindKH()
        {
            var list = new TG.BLL.cm_KaoHeRenWu().GetList(0, "", "ID DESC");

            this.drp_unit3.DataSource = list;
            this.drp_unit3.DataTextField = "RenWuNo";
            this.drp_unit3.DataValueField = "ID";
            this.DataBind();

        }
        /// <summary>
        /// 专业列表
        /// </summary>
        protected List<TG.Model.tg_speciality> SpecialList
        {
            get
            {
                return new TG.BLL.tg_speciality().GetModelList(" Spe_ID<6 ");
            }
        }
        /// <summary>
        /// 本次考核所有考核项目名称
        /// </summary>
        protected int KaoHeItemAllCount
        {
            get
            {
                string strSql = string.Format(@"select COUNT(*)
                                                From cm_KaoHeProjName N left join cm_KaoHeRenwuProj P on N.KaoHeProjId=P.ID
                                                Where P.RenWuId={0}", this.drp_unit3.SelectedValue);
                int count = Convert.ToInt32(TG.DBUtility.DbHelperSQL.GetSingle(strSql));
                return count;
            }
        }
        /// <summary>
        /// 本次考核已经调整提交的项目名称
        /// </summary>
        protected int KaoHeItemDoneCount
        {
            get
            {
                string strSql = string.Format(@"Select COUNT(*) 
                                                From dbo.cm_KaoHeProjAllot
                                                Where ProKHNameId IN (select distinct N.ID
                                                From cm_KaoHeProjName N left join cm_KaoHeRenwuProj P on N.KaoHeProjId=P.ID
                                                Where P.RenWuId={0}) AND Stat=1", this.drp_unit3.SelectedValue);
                int count = Convert.ToInt32(TG.DBUtility.DbHelperSQL.GetSingle(strSql));
                return count;
            }
        }
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            GetHisTable();
        }

        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetHisTable();
        }
        /// <summary>
        /// 历史表
        /// </summary>
        protected void GetHisTable()
        {
            #region 项目考核类型特殊类型的临时改动 2016年8月12日 qpl 后期得改回来

            //            string strSql = string.Format(@" select P.ID AS PKHID,P.proID,P.RenWuId,proName,KName,N.ID AS KID,N.Stat AS Stat
            //	            ,(select COUNT(*) From  cm_KaoHeSpePersent where Stat=1 AND ProKHNameID=N.ID ) AS ZhuChi
            //	            ,(select COUNT(*) From  cm_KaoHeSpePersent where Stat2=1 AND ProKHNameID=N.ID ) AS XiangMu
            //                ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=1 AND Stat=1 AND KaoHeNameID=N.ID) end AS JianZhu
            //                ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=1 AND Stat2=1 AND KaoHeNameID=N.ID) end AS JianZhuBumen
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0
            //	             then -1
            //	             else
            //                (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=1 AND Stat3=1 AND KaoHeNameID=N.ID) end AS JianZhuZong
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=2 AND Stat=1 AND KaoHeNameID=N.ID) end AS Jiegou
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID=7)>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID<>7)=0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=2 AND Stat2=1 AND KaoHeNameID=N.ID) end AS JiegouBumen
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID IN (7,9))>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID NOT IN (7,9))=0
            //	             then -1
            //	             else
            //	             (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=3 AND Stat=1 AND KaoHeNameID=N.ID) end AS Nuantong
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID IN (7,9))>0
            //					    and (select COUNT(*) From dbo.cm_KaoheProjType Where KaoheNameID=N.ID AND KaoheTypeID NOT IN (7,9))=0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=3 AND Stat2=1 AND KaoHeNameID=N.ID) end AS NuantongBumen
            //	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=4 AND Stat=1 AND KaoHeNameID=N.ID) AS Shui
            //	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=4 AND Stat2=1 AND KaoHeNameID=N.ID) AS ShuiBumen
            //	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=5 AND Stat=1 AND KaoHeNameID=N.ID) AS Dianqi
            //	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=5 AND Stat2=1 AND KaoHeNameID=N.ID) AS DianqiBumen
            //            from dbo.cm_KaoHeRenwuProj P right join dbo.cm_KaoHeProjName N on P.ID=N.KaoHeProjId
            //            where p.RenWuId={0}
            //                            ", this.drp_unit3.SelectedValue);

            #endregion

            #region 分页注销
            //            string strSql = string.Format(@" select P.ID AS PKHID,P.proID,P.RenWuId,proName,KName,N.ID AS KID,N.Stat AS Stat
            //	            ,(select COUNT(*) From  cm_KaoHeSpePersent where Stat=1 AND ProKHNameID=N.ID ) AS ZhuChi
            //	            ,(select COUNT(*) From  cm_KaoHeSpePersent where Stat2=1 AND ProKHNameID=N.ID ) AS XiangMu
            //                ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=1 AND ProKHNameID=N.ID)=0
            //                          and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>1 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=1 AND Stat=1 AND KaoHeNameID=N.ID) end AS JianZhu
            //                ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=1 AND ProKHNameID=N.ID)=0
            //                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>1 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=1 AND Stat2=1 AND KaoHeNameID=N.ID) end AS JianZhuBumen
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=1 AND ProKHNameID=N.ID)=0
            //                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>1 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //                (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=1 AND Stat3=1 AND KaoHeNameID=N.ID) end AS JianZhuZong
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=2 AND ProKHNameID=N.ID)=0
            //                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>2 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=2 AND Stat=1 AND KaoHeNameID=N.ID) end AS Jiegou
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=2 AND ProKHNameID=N.ID)=0
            //                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>2 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=2 AND Stat2=1 AND KaoHeNameID=N.ID) end AS JiegouBumen
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=3 AND ProKHNameID=N.ID)=0
            //                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>3 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //	             (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=3 AND Stat=1 AND KaoHeNameID=N.ID) end AS Nuantong
            //	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=3 AND ProKHNameID=N.ID)=0
            //                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>3 AND ProKHNameID=N.ID)>0
            //	             then -1
            //	             else
            //	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=3 AND Stat2=1 AND KaoHeNameID=N.ID) end AS NuantongBumen
            //	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=4 AND Stat=1 AND KaoHeNameID=N.ID) AS Shui
            //	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=4 AND Stat2=1 AND KaoHeNameID=N.ID) AS ShuiBumen
            //	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=5 AND Stat=1 AND KaoHeNameID=N.ID) AS Dianqi
            //	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=5 AND Stat2=1 AND KaoHeNameID=N.ID) AS DianqiBumen
            //                ,ISNULL((select MemID from cm_KaoheSpecMems where proID=p.proID AND ProKHId=P.ID AND ProKHNameId=N.ID AND SpeID=-2),0) AS ZhuChiMemID
            //                ,ISNULL((select MemID from cm_KaoheSpecMemsFz where proID=p.proID AND ProKHId=P.ID AND ProKHNameId=N.ID AND SpeID=1),0) AS JianZhuMemID
            //            from dbo.cm_KaoHeRenwuProj P right join dbo.cm_KaoHeProjName N on P.ID=N.KaoHeProjId
            //            where p.RenWuId={0} Order By proName
            //                            ", this.drp_unit3.SelectedValue);
            #endregion

            string strWhere = "";
            //记录数
            this.AspNetPager1.RecordCount = this.GetRecordCount(strWhere);
            //获取分页数据
            DataTable dt = this.GetListByPage(strWhere, "", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize).Tables[0];
            this.HisList = dt;
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            GetHisTable();
        }

        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            string strSql = string.Format(@" select P.ID AS PKHID,P.proID,P.RenWuId,proName,KName,N.ID AS KID,N.Stat AS Stat
	            ,(select COUNT(*) From  cm_KaoHeSpePersent where Stat=1 AND ProKHNameID=N.ID ) AS ZhuChi
	            ,(select COUNT(*) From  cm_KaoHeSpePersent where Stat3=1 AND ProKHNameID=N.ID ) AS BuMen
	            ,(select COUNT(*) From  cm_KaoHeSpePersent where Stat2=1 AND ProKHNameID=N.ID ) AS XiangMu
                ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=1 AND ProKHNameID=N.ID)=0
                          and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>1 AND ProKHNameID=N.ID)>0
	             then -1
	             else
	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=1 AND Stat=1 AND KaoHeNameID=N.ID) end AS JianZhu
                ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=1 AND ProKHNameID=N.ID)=0
                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>1 AND ProKHNameID=N.ID)>0
	             then -1
	             else
	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=1 AND Stat2=1 AND KaoHeNameID=N.ID) end AS JianZhuBumen
	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=1 AND ProKHNameID=N.ID)=0
                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>1 AND ProKHNameID=N.ID)>0
	             then -1
	             else
                (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=1 AND Stat3=1 AND KaoHeNameID=N.ID) end AS JianZhuZong
	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=2 AND ProKHNameID=N.ID)=0
                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>2 AND ProKHNameID=N.ID)>0
	             then -1
	             else
	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=2 AND Stat=1 AND KaoHeNameID=N.ID) end AS Jiegou
	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=2 AND ProKHNameID=N.ID)=0
                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>2 AND ProKHNameID=N.ID)>0
	             then -1
	             else
	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=2 AND Stat2=1 AND KaoHeNameID=N.ID) end AS JiegouBumen
	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=3 AND ProKHNameID=N.ID)=0
                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>3 AND ProKHNameID=N.ID)>0
	             then -1
	             else
	             (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=3 AND Stat=1 AND KaoHeNameID=N.ID) end AS Nuantong
	            ,case when (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID=3 AND ProKHNameID=N.ID)=0
                         and (select COUNT(*) From dbo.cm_KaoheSpecMems Where SpeID<>3 AND ProKHNameID=N.ID)>0
	             then -1
	             else
	            (Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=3 AND Stat2=1 AND KaoHeNameID=N.ID) end AS NuantongBumen
	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=4 AND Stat=1 AND KaoHeNameID=N.ID) AS Shui
	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=4 AND Stat2=1 AND KaoHeNameID=N.ID) AS ShuiBumen
	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=5 AND Stat=1 AND KaoHeNameID=N.ID) AS Dianqi
	            ,(Select COUNT(*) From  dbo.cm_KaoHeSpeMemsPersent Where SpeID=5 AND Stat2=1 AND KaoHeNameID=N.ID) AS DianqiBumen
                ,ISNULL((select MemID from cm_KaoheSpecMems where proID=p.proID AND ProKHId=P.ID AND ProKHNameId=N.ID AND SpeID=-2),0) AS ZhuChiMemID
                ,ISNULL((select MemID from cm_KaoheSpecMemsFz where proID=p.proID AND ProKHId=P.ID AND ProKHNameId=N.ID AND SpeID=1),0) AS JianZhuMemID
                ,ISNULL((select InserUserID3 From cm_KaoHeSpePersent where Stat3=1 AND ProKHNameID=N.ID AND SpeID=1 ),0) AS BuMenMemID
                ,(Select Count(ID) From cm_KaoHeSpePersent Where ProKHNameID=N.ID AND SpeRoleID=-2 AND Stat2=1 AND InserUserID3=0 AND Stat3=0) AS IsNew
            from dbo.cm_KaoHeRenwuProj P right join dbo.cm_KaoHeProjName N on P.ID=N.KaoHeProjId
            where p.RenWuId={0} Order By proName
                            ", this.drp_unit3.SelectedValue);

            return DbHelperSQL.Query(strSql, startIndex, endIndex);
        }

        public int GetRecordCount(string strWhere)
        {
            string strSql = string.Format(@" select Count(P.ID)
            from dbo.cm_KaoHeRenwuProj P right join dbo.cm_KaoHeProjName N on P.ID=N.KaoHeProjId
            where p.RenWuId={0}
            ", this.drp_unit3.SelectedValue);

            return Convert.ToInt32(DbHelperSQL.GetSingle(strSql));
        }
    }
}