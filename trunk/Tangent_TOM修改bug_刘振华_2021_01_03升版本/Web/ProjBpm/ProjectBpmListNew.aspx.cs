﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjBpm
{
    public partial class ProjectBpmListNew : PageBase
    {
        /// <summary>
        /// 任务列表
        /// </summary>
        public List<TG.Model.cm_KaoHeRenWu> RenwuList
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list;
            }
        }
        public List<TG.Model.cm_KaoHeRenWu> RenwuListOrder
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list.OrderByDescending(c => c.StartDate).ToList();
            }
        }
        /// <summary>
        /// 考核任务ID
        /// </summary>
        public int RenwuId
        {
            get
            {
                int renwuid = 0;
                int.TryParse(Request["id"] ?? "0", out renwuid);
                return renwuid;
            }
        }
        /// <summary>
        /// 当前任务ID
        /// </summary>
        public int RenwuNo
        {
            get
            {
                if (this.RenwuId == 0)
                {
                    return int.Parse(this.drprenwu.SelectedValue);
                }
                else
                {
                    return this.RenwuId;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                SelectCurrentYear();
                BindUnit();
                BindRenWu();

                this.IsSearchProj = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void BindRenWu()
        {
            RenwuList.OrderByDescending(c => c.StartDate).ToList().ForEach(c =>
            {
                this.drprenwu.Items.Add(new ListItem(c.RenWuNo.ToString(), c.ID.ToString()));
            });
        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            this.drp_unit3.DataSource = bll_unit.GetList(" unit_ParentID<>0 AND unit_ID IN (231,233,234,236,237,238,239,282)");
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }

        protected void drprenwu_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            this.IsSearchProj = false;
        }

        protected DataTable ProjList
        {
            get;
            set;
        }
        /// <summary>
        /// 分配类型
        /// </summary>
        protected List<TG.Model.cm_KaoHeProjTypeEnum> TypeEnumList
        {
            get
            {
                return new TG.BLL.cm_KaoHeProjTypeEnum().GetModelList("").OrderBy(c => c.ID).ToList();
            }
        }
        /// <summary>
        /// 专业列表
        /// </summary>
        protected List<TG.Model.tg_speciality> SpecialList
        {
            get
            {
                return new TG.BLL.tg_speciality().GetModelList(" Spe_ID<6 ");
            }
        }
        /// <summary>
        /// 进行中的任务ID
        /// </summary>
        public int ProcessRenwuID
        {
            get
            {
                int renwuid = 0;
                string strWhere = " ProjStatus='进行中' ";
                var bll = new BLL.cm_KaoHeRenWu();
                var list = bll.GetModelList(strWhere);

                if (list.Count > 0)
                {
                    list = list.OrderByDescending(c => c.ID).ToList();
                    renwuid = list[0].ID;
                }

                return renwuid;
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }

        /// <summary>
        /// 返回上一个考核
        /// </summary>
        public string PrevRenwuId
        {
            get
            {
                if (this.drprenwu.Items[this.drprenwu.SelectedIndex + 1] != null)
                {
                    return this.drprenwu.Items[this.drprenwu.SelectedIndex + 1].Value;
                }
                else
                {
                    return this.drprenwu.SelectedValue;
                }
            }
        }

        /// <summary>
        /// 判断考核任务下是否已经设置考核项目
        /// </summary>
        protected bool IsSetProj
        {
            get
            {
                string strWhere = string.Format(" RenWuId={0}", this.drprenwu.SelectedValue);

                var list = new TG.BLL.cm_KaoHeRenwuProj().GetModelList(strWhere);

                if (list.Count == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        /// <summary>
        /// 是否查询项目
        /// </summary>
        protected bool IsSearchProj
        {
            get;
            set;
        }
        /// <summary>
        /// 考核项目列表
        /// </summary>
        public DataTable ProjNameList
        {
            get
            {
                string strSql = "";
                if (!IsSearchProj)
                {
                    strSql = GetCurProjList();
                }
                else
                {
                    strSql = GetHisProjList();
                }

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];


                return dt;
            }

        }
        /// <summary>
        /// 查询未设置项目
        /// </summary>
        /// <returns></returns>
        public string GetCurProjList()
        {
            string strSql = "";
            //判断是否已经设置过
            if (IsSetProj)
            {
                //如果已经设置此考核任务的项目  默认加载已设置的考核与新建的项目列表
                strSql = string.Format(@"Select * From (Select top 10000
                                        isnull(P.ID,KP.proID) AS ID,
                                        isnull(P.KaoHeProjId,KP.ID) AS KaoHeProjId,
                                        KP.proName,
                                        KP.proID,
                                        isnull(P.KName,'') AS KName,
                                        isnull(P.DoneScale,0) AS DoneScale,
                                        isnull(P.Sub,'') AS Sub,
                                        isnull(T.ID,0) AS TID,
                                        isnull(T.KaoheTypeName,'') AS KaoheTypeName,
                                        isnull(T.RealScale,0) AS RealScale,
                                        isnull(T.TypeXishu,0) AS TypeXishu,
                                        isnull(T.virallotCount,0) AS virallotCount,
                                        isnull(P.AllScale,0) AS AllScale,
                                        isnull((Select SUM(virallotCount) From dbo.cm_KaoheProjType  Where KaoheNameID=P.ID),0) AS AllAllotCount,
                                        (Select Count(*) From dbo.cm_KaoheProjType  Where KaoheNameID=P.ID) AS ColCount,
                                        KP.UnitName,
                                        KP.UnitID,
                                        1 AS IsSet,
                                        (select COUNT(ProId) From dbo.cm_KaoHeProjAllot where ProId=KP.proID AND ProKHId=KP.ID AND ProKHNameId=P.ID) AS JJCount,
                                        (select COUNT(ProId) From dbo.cm_KaoHeProjAllot where ProId=KP.proID AND ProKHId=KP.ID AND ProKHNameId=P.ID AND Stat=1) AS JJCount2
                                        From  cm_KaoHeRenwuProj KP left join cm_KaoHeProjName P on KP.ID=P.KaoHeProjId left join cm_KaoheProjType T on P.ID=T.KaoheNameID
                                        Where RenWuId={0}
                                        Order By KP.proName,P.ID) AS ABC", this.drprenwu.SelectedValue);

                //新建项目
//                strSql += " UNION ALL ";
//                strSql += string.Format(@"Select * From (Select top 10000 pro_ID AS ID,0 AS KaoHeProjId,
//	                                    pro_name AS proName,
//                                        pro_ID AS proID,
//	                                    '' AS KName,
//	                                    0 AS DoneScale,
//	                                    '' AS Sub,
//                                        0 AS TID,
//	                                    '' AS KaoheTypeName,
//	                                    0 AS RealScale,
//	                                    0 AS TypeXishu,
//	                                    0 AS virallotCount,
//	                                    0 AS AllScale,
//	                                    0 AS AllAllotCount,
//	                                    1 AS ColCount,
//                                        Unit AS UnitName,
//                                        (Select unit_ID From tg_unit Where unit_name = Unit) AS UnitID,
//                                        0 AS IsSet,
//                                        0 AS JJCount,
//                                        0 AS JJCount2                                 
//                                        From dbo.cm_Project
//							            Where (pro_ID Not IN (select proID From cm_KaoHeRenwuProj)) Order By pro_name) AS ABD", this.drp_year.SelectedValue);
            }
            else //没有设置考核项目默认加载上一次的所有考核项目以及本次考核新建项目
            {
                strSql = string.Format(@"Select * From (Select TOP 10000 
                                        isnull(P.ID,KP.proID) AS ID,
                                        isnull(P.KaoHeProjId,KP.ID) AS KaoHeProjId,
                                        KP.proName,
                                        KP.proID,
                                        isnull(P.KName,'') AS KName,
                                        isnull(P.DoneScale,0) AS DoneScale,
                                        isnull(P.Sub,'') AS Sub,
                                        isnull(T.ID,0) AS TID,
                                        isnull(T.KaoheTypeName,'') AS KaoheTypeName,
                                        isnull(T.RealScale,0) AS RealScale,
                                        isnull(T.TypeXishu,0) AS TypeXishu,
                                        isnull(T.virallotCount,0) AS virallotCount,
                                        isnull(P.AllScale,0) AS AllScale,
                                        isnull((Select SUM(virallotCount) From dbo.cm_KaoheProjType  Where KaoheNameID=P.ID),0) AS AllAllotCount,
                                        (Select Count(*) From dbo.cm_KaoheProjType  Where KaoheNameID=P.ID) AS ColCount,
                                        KP.UnitName,
                                        KP.UnitID,
                                        0 AS IsSet,
                                        (select COUNT(ProId) From dbo.cm_KaoHeProjAllot where ProId=KP.proID AND ProKHId=KP.ID AND ProKHNameId=P.ID) AS JJCount,
                                        (select COUNT(ProId) From dbo.cm_KaoHeProjAllot where ProId=KP.proID AND ProKHId=KP.ID AND ProKHNameId=P.ID AND Stat=1) AS JJCount2
                                        From cm_KaoHeRenwuProj KP left join cm_KaoHeProjName P on KP.ID=P.KaoHeProjId left join cm_KaoheProjType T on P.ID=T.KaoheNameID
                                        Where RenWuId={0} 
                                        Order By KP.proName,P.ID) AS ABC ", PrevRenwuId);
                //本年度新建项目
                strSql += " UNION ALL ";
                strSql += string.Format(@"Select * From (Select top 10000  pro_ID AS ID,0 AS KaoHeProjId,
	                                    pro_name AS proName,
                                        pro_ID AS proID,
	                                    '' AS KName,
	                                    0 AS DoneScale,
	                                    '' AS Sub,
                                        0 AS TID,
	                                    '' AS KaoheTypeName,
	                                    0 AS RealScale,
	                                    0 AS TypeXishu,
	                                    0 AS virallotCount,
	                                    0 AS AllScale,
	                                    0 AS AllAllotCount,
	                                    1 AS ColCount,
                                        Unit AS UnitName,
                                        (Select unit_ID From tg_unit Where unit_name = Unit) AS UnitID,
                                        0 AS IsSet,
                                        0 AS JJCount,
                                        0 AS JJCount2                                
                                        From dbo.cm_Project
							            Where (pro_ID Not IN (select proID From cm_KaoHeRenwuProj)) Order By pro_name) AS ABD", this.drp_year.SelectedValue);
            }

            return strSql;
        }
        /// <summary>
        /// 查询历史项目
        /// </summary>
        public string GetHisProjList()
        {
            if (string.IsNullOrEmpty(this.txtProjName.Text.Trim()))
            {
                return "";
            }

            StringBuilder sb = new StringBuilder();

            sb.AppendFormat(" AND pro_name like '%{0}%' ", this.txtProjName.Text);

            if (this.drp_year.SelectedIndex > 0)
            {
                sb.AppendFormat(" AND Year(pro_startTime)={0} ", this.drp_year.SelectedValue);
            }

            if (this.drp_unit3.SelectedIndex > 0)
            {
                sb.AppendFormat(" AND Unit like '{0}%' ", this.drp_unit3.SelectedItem.Text);
            }

            string strSql = string.Format(@"Select  pro_ID AS ID,0 AS KaoHeProjId,
	                                    pro_name AS proName,
                                        pro_ID AS proID,
	                                    '' AS KName,
	                                    0 AS DoneScale,
	                                    '' AS Sub,
                                        0 AS TID,
	                                    '' AS KaoheTypeName,
	                                    0 AS RealScale,
	                                    0 AS TypeXishu,
	                                    0 AS virallotCount,
	                                    0 AS AllScale,
	                                    0 AS AllAllotCount,
	                                    1 AS ColCount,
                                        Unit AS UnitName,
                                        (Select unit_ID From tg_unit Where unit_name = Unit) AS UnitID,
                                        (Select COUNT(*) From cm_KaoHeRenwuProj Where RenwuID={1} AND proID=pro_ID) AS IsSet,
                                        0 AS JJCount,
                                        0 AS JJCount2
                                        From dbo.cm_Project
							            Where 1=1 {0} ", sb.ToString(), this.drprenwu.SelectedValue);


            return strSql;
        }

        protected void btnSearchProj_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtProjName.Text.Trim()))
            {
                this.IsSearchProj = true;
            }
        }
    }
}