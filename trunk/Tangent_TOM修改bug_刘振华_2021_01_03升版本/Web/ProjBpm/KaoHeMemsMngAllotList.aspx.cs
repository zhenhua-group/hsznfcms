﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjBpm
{
    public partial class KaoHeMemsMngAllotList : PageBase//System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindRenwu();


                //选中当前的考核任务
                if (this.ProcessRenwuID != 0)
                {
                    this.drpRenwu.ClearSelection();
                    if (this.drpRenwu.Items.FindByValue(this.ProcessRenwuID.ToString()) != null)
                    {
                        this.drpRenwu.Items.FindByValue(this.ProcessRenwuID.ToString()).Selected = true;
                    }
                }
            }
        }

        /// <summary>
        /// 考核任务
        /// </summary>
        public List<TG.Model.cm_KaoHeRenWu> RenwuList
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list;
            }
        }
        /// <summary>
        /// 进行中的任务ID
        /// </summary>
        public int ProcessRenwuID
        {
            get
            {
                int renwuid = 0;
                string strWhere = " ProjStatus='进行中' ";
                var bll = new BLL.cm_KaoHeRenWu();
                var list = bll.GetModelList(strWhere);

                if (list.Count > 0)
                {
                    list = list.OrderByDescending(c => c.ID).ToList();
                    renwuid = list[0].ID;
                }

                return renwuid;
            }
        }
        /// <summary>
        /// 绑定考核任务
        /// </summary>
        protected void BindRenwu()
        {
            if (RenwuList.Count > 0)
            {
                RenwuList.ForEach(c =>
                {
                    this.drpRenwu.Items.Add(new ListItem(c.RenWuNo, c.ID.ToString()));
                });
            }

        }
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            //this.drp_unit3.DataSource = bll_unit.GetList(" unit_ParentID<>0 AND unit_ID IN (231,232,237,238,239)");
            this.drp_unit3.DataSource = bll_unit.GetList(" unit_ParentID<>0 AND unit_ID<>230");
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }

        public List<TG.Model.cm_KaoHeMemsMngAllotList> MngAllotList
        {
            get
            {
                string unitid = this.drp_unit3.SelectedValue;
                string renwuid = this.drpRenwu.SelectedValue;
                string strWhere = " 1=1 ";
                if (unitid != "-1")
                {
                    strWhere += " AND UnitID=" + unitid;
                }

                strWhere += " AND RenwuID=" + renwuid;

                var list = new TG.BLL.cm_KaoHeMemsMngAllotList().GetModelList(strWhere).OrderBy(c => c.UnitID);

                var listExt = new TG.BLL.tg_unitExt().GetModelList("");
                //排序 2017年5月24日
                var query = from c in list
                            join p in listExt on c.UnitID equals p.unit_ID
                            orderby p.unit_Order ascending
                            select c;


                return query.ToList();
            }
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 是否已经备档
        /// </summary>
        protected bool IsBak
        {
            get
            {
                string strWhere = string.Format(" RenwuID={0}", this.drpRenwu.SelectedValue);
                int count = new TG.BLL.cm_KaoHeUnitHis().GetRecordCount(strWhere);

                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        protected List<TG.Model.cm_KaoHeUnitHis> UnitHisList
        {
            get
            {
                string strWhere = string.Format(" RenwuID={0}", this.drpRenwu.SelectedValue);

                return new TG.BLL.cm_KaoHeUnitHis().GetModelList(strWhere);
            }
        }

        protected void drpRenwu_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}