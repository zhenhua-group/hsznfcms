﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjBpm
{
    public partial class UnitAllAllotRecordList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BindRenwu();
            }
        }


        /// <summary>
        /// 考核任务
        /// </summary>
        public List<TG.Model.cm_KaoHeRenWu> RenwuList
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list;
            }
        }
        /// <summary>
        /// 绑定考核任务
        /// </summary>
        //protected void BindRenwu()
        //{
        //    if (RenwuList.Count > 0)
        //    {
        //        RenwuList.ForEach(c =>
        //        {
        //            this.drpRenwu.Items.Add(new ListItem(c.RenWuNo, c.ID.ToString()));
        //        });
        //    }
        //}


        //public List<TG.Model.cm_KaoHeAllMemsAllotRecord> RecordList
        //{
        //    get
        //    {
        //        string renwuid = this.drpRenwu.SelectedValue;
        //        string strWhere = " 1=1 ";
        //        strWhere += " AND RenwuID=" + renwuid;

        //        var list = new TG.BLL.cm_KaoHeAllMemsAllotRecord().GetModelList(strWhere);

        //        return list;
        //    }
        //}
        /// <summary>
        /// 
        /// </summary>
        public List<int> AllotRenwuIDs
        {
            get
            {
                string strWhere = "";

                var list = new TG.BLL.cm_KaoHeAllMemsAllotRecord().GetModelList(strWhere).Select(c => c.RenwuID).Distinct().ToList();

                return list;
            }
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {

        }
    }
}