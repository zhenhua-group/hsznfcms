﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjBpm
{
    public partial class ProjDeptAllot : PageBase//System.Web.UI.Page
    {
        /// <summary>
        /// 消息ID
        /// </summary>
        protected int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["MsgNo"] ?? "0", out msgid);
                return msgid;
            }
        }

        /// <summary>
        /// 项目ID
        /// </summary>
        public int ProID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["proid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 项目考核ID
        /// </summary>
        public int ProKHID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["prokhid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 考核名称ID
        /// </summary>
        public int KaoHeNameID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["khnameid"] ?? "0", out proid);
                return proid;
            }
        }
        /// <summary>
        /// 考核类型ID
        /// </summary>
        public int KaoHeTypeID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["khtypeid"] ?? "0", out proid);
                return proid;
            }
        }
        public int RenWuID
        {
            get
            {
                int renwuid = 0;
                int.TryParse(Request["renwuid"] ?? "0", out renwuid);
                return renwuid;
            }
        }
        /// <summary>
        /// 查询任务
        /// </summary>
        public TG.Model.cm_KaoHeRenWu KaoHeRenwu
        {
            get
            {
                TG.Model.cm_KaoHeRenWu model = new Model.cm_KaoHeRenWu();

                if (this.ProID != -1 && this.RenWuID == 0)
                {
                    //项目考核ID
                    var projKHModel = new TG.BLL.cm_KaoHeRenwuProj().GetModel(this.ProKHID);
                    if (projKHModel != null)
                    {
                        model = new TG.BLL.cm_KaoHeRenWu().GetModel(projKHModel.RenWuId);
                    }
                }
                else
                {
                    model = new TG.BLL.cm_KaoHeRenWu().GetModel(this.RenWuID);
                }

                return model;
            }
        }
        /// <summary>
        /// 考核任务
        /// </summary>
        public List<TG.Model.cm_KaoHeRenWu> RenwuList
        {
            get
            {
                var list = new TG.BLL.cm_KaoHeRenWu().GetModelList("").OrderByDescending(c => c.ID).ToList();
                return list;
            }
        }

        public string IsCheck
        {
            get
            {
                return Request["check"] ?? "0";
            }
        }
        /// <summary>
        /// 任务ID
        /// </summary>
        public string RenwuNo
        {
            get
            {
                return this.drpRenwu.SelectedValue;
            }
        }
        /// <summary>
        /// 考核任务
        /// </summary>
        public TG.Model.cm_KaoHeRenWu RenwuModel
        {
            get
            {
                var model = new TG.BLL.cm_KaoHeRenWu().GetModel(int.Parse(this.RenwuNo));
                return model;
            }
        }
        /// <summary>
        /// 项目奖金计算值
        /// </summary>
        public decimal? ProjAllDeptAllotCount
        {
            get
            {
                decimal? result = 0;
                string renwuid = this.drpRenwu.SelectedValue;
                string strSql = string.Format(@"Select SUM(AllotCount) AS AllotCount
                                         From cm_KaoHeMemsAllot K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id 
                                         Where m.mem_isFired=0 AND (m.mem_ID NOT IN ({1})) AND (ProKHNameId IN (Select A.ID From cm_KaoHeProjName A left join dbo.cm_KaoHeRenwuProj B on A.KaoHeProjId=B.ID  Where RenWuId={0})) AND (m.mem_Unit_ID IN (231,232,237,238,239,281,282))
                                         ", renwuid, GuDongIdStr);
                object obj = TG.DBUtility.DbHelperSQL.GetSingle(strSql);
                result = Convert.ToDecimal(obj);
                return result;
            }
        }
        /// <summary>
        /// 项目奖金调整值
        /// </summary>
        public decimal? ProjAllDeptAllotCountAfter
        {
            get
            {
                //2017年4月18日 加上西安部门 282,加上 特务三部  281 2017年5月24日
                decimal? result = 0;
                string renwuid = this.drpRenwu.SelectedValue;
                string strSql = string.Format(@"Select SUM(AllotCountBefore) AS AllotCountBefore
                                         From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id 
                                         Where m.mem_isFired=0 AND (m.mem_ID NOT IN ({1})) AND RenwuID={0} AND (m.mem_Unit_ID IN (231,232,237,238,239,281,282))
                                         ", renwuid, GuDongIdStr);
                object obj = TG.DBUtility.DbHelperSQL.GetSingle(strSql);
                result = Convert.ToDecimal(obj);
                return result;
            }
        }

        /// <summary>
        /// 比例
        /// </summary>
        public DataTable ProjAllotUnitListAfter
        {

            get
            {
                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }

                string renwuid = this.drpRenwu.SelectedValue;
                string strSql = string.Format(@"Select unit_ID,unit_Name
                                            ,isnull((
	                                            Select
	                                            SUM(AllotCount) AS AllotCount
	                                            From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
	                                            Where m.mem_Unit_ID=unit.unit_ID AND m.mem_isFired=0 AND R.RoleIdMag<>5 AND  RenWuId={0}
                                            ),0) AS AllotCount
                                            ,isnull((
	                                            Select
	                                            SUM(AllotCountBefore) AS AllotCountBefore
	                                            From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
	                                            Where m.mem_Unit_ID=unit.unit_ID AND m.mem_isFired=0 AND R.RoleIdMag<>5 AND  RenWuId={0}
                                            ),0) AS AllotCountBefore
                                            ,(select COUNT(*) From tg_member a left join tg_memberRole b on a.mem_ID=b.mem_Id where b.RoleIdMag<>5 AND m.mem_Unit_ID=unit.unit_ID AND mem_isFired=0) AS MemCount2
                                            ,(select COUNT(*) AS MemCount
                                                From (
                                                select distinct(k.MemID)
                                                from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                                where m.mem_Unit_ID=unit.unit_ID AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) 
                                            ) B) AS MemCount
                                            ,(select COUNT(*) From tg_member a left join tg_memberRole b on a.mem_ID=b.mem_Id where b.RoleIdMag<>5 AND mem_isFired=0) AS AllMemCount2
                                            ,(select COUNT(*) AS MemCount
                                                From (
                                                select distinct(k.MemID)
                                                from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                                where (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) 
                                            ) B) AS AllMemCount                                     
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join tg_member b on a.MemID= b.mem_ID left join tg_memberRole R on b.mem_ID=r.mem_Id where R.RoleIdMag<>5 AND mem_isFired=0 AND b.mem_Unit_ID=unit.unit_ID {1}),0) AS BumenGz
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join tg_member b on a.MemID= b.mem_ID left join tg_memberRole R on b.mem_ID=r.mem_Id where R.RoleIdMag<>5 AND mem_isFired=0 AND (b.mem_Unit_ID IN (231,232,237,238,239)) AND 1=1 {1}),0) AS AllBumemGz
                                            ,isnull((select top 1 weights from dbo.cm_KaoHeMemsAllotReport where UnitID=unit.unit_ID AND RenwuID={0}),0) AS Weights
                                            From tg_unit unit
                                            Where unit_ParentID<>0 AND unit_ID IN (231,232,237,238,239)", renwuid, strWhere);
                if (this.drp_unit3.SelectedValue != "-1")
                {
                    strSql = string.Format(@"Select unit_ID,unit_Name
                                            ,isnull((
	                                            Select
	                                            SUM(AllotCount) AS AllotCount
	                                            From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
	                                            Where m.mem_Unit_ID=unit.unit_ID AND m.mem_isFired=0 AND R.RoleIdMag<>5 AND RenWuId={0}
                                            ),0) AS AllotCount
                                            ,isnull((
	                                            Select
	                                            SUM(AllotCountBefore) AS AllotCountBefore
	                                            From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
	                                            Where m.mem_Unit_ID=unit.unit_ID AND m.mem_isFired=0 AND R.RoleIdMag<>5 AND  RenWuId={0}
                                            ),0) AS AllotCountBefore
                                            ,(select COUNT(*) From tg_member a left join tg_memberRole b on a.mem_ID=b.mem_Id where b.RoleIdMag<>5 AND m.mem_Unit_ID=unit.unit_ID AND mem_isFired=0) AS MemCount2
                                            ,(select COUNT(*) AS MemCount
                                                From (
                                                select distinct(k.MemID)
                                                from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                                where m.mem_Unit_ID=unit.unit_ID AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) 
                                            ) B) AS MemCount
                                            ,(select COUNT(*) From tg_member a left join tg_memberRole b on a.mem_ID=b.mem_Id where b.RoleIdMag<>5 AND mem_isFired=0) AS AllMemCount2
                                            ,(select COUNT(*) AS MemCount
                                                From (
                                                select distinct(k.MemID)
                                                from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                                where (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) 
                                            ) B) AS AllMemCount                                            
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join tg_member b on a.MemID= b.mem_ID left join tg_memberRole R on b.mem_ID=r.mem_Id where R.RoleIdMag<>5 AND mem_isFired=0 AND b.mem_Unit_ID=unit.unit_ID {2}),0) AS BumenGz
                                            ,isnull((select SUM(yfgz)-SUM(yfjj) from dbo.cm_KaoHeMemsWages a left join tg_member b on a.MemID= b.mem_ID left join tg_memberRole R on b.mem_ID=r.mem_Id where R.RoleIdMag<>5 AND mem_isFired=0 AND (b.mem_Unit_ID IN (231,232,237,238,239)) AND 1=1 {2}),0) AS AllBumemGz
                                            ,isnull((select top 1 weights from dbo.cm_KaoHeMemsAllotReport where UnitID=unit.unit_ID AND RenwuID={0}),0) AS Weights                             
                                            From tg_unit unit
                                            Where unit_ID={1} AND unit_ParentID<>0 ", renwuid, this.drp_unit3.SelectedValue, strWhere);
                }

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;
            }
        }

        public DataTable ProjAllotUnitColListAfter
        {
            get
            {
                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }


                string renwuid = this.drpRenwu.SelectedValue;
                //部门合计中新建西安分公司中的各专业人员
                string strSql = string.Format(@"Select 1 AS ID,'1.部门合计' AS Name,
                            isnull((
                                    Select
                                    SUM(AllotCountBefore) AS AllotCountBefore
                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                    Where (m.mem_Unit_ID IN (231,232,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID=1
                                ),0) AS JianZhu,
                                isnull((
                                    Select
                                    SUM(AllotCountBefore) AS AllotCountBefore
                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                    Where (m.mem_Unit_ID IN (237,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID=2
                                ),0) AS JieGou,
                                isnull((
                                    Select
                                    SUM(AllotCountBefore) AS AllotCountBefore
                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                    Where (m.mem_Unit_ID IN (238,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND (m.mem_Speciality_ID=3 OR m.mem_Speciality_ID=4)
                                ),0) AS SheBei,
                                isnull((
                                    Select
                                    SUM(AllotCountBefore) AS AllotCountBefore
                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                    Where (m.mem_Unit_ID IN (239,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID=5
                                ),0) AS Dianqi
                        From tg_unit unit
                        Where unit_ParentID<>0 AND unit_ID=231
                        union all

                        Select 2 AS ID,'2.奖金占比' AS Name,
                            isnull((
                                    Select
                                    SUM(AllotCountBefore) AS AllotCountBefore
                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                    Where (m.mem_Unit_ID IN (231,232,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID=1
                                ),0) AS JianZhu,
                                isnull((
                                    Select
                                    SUM(AllotCountBefore) AS AllotCountBefore
                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                    Where (m.mem_Unit_ID IN (237,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID=2
                                ),0) AS JieGou,
                                isnull((
                                    Select
                                    SUM(AllotCountBefore) AS AllotCountBefore
                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                    Where (m.mem_Unit_ID IN (238,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND (m.mem_Speciality_ID=3 OR m.mem_Speciality_ID=4)
                                ),0) AS SheBei,
                                isnull((
                                    Select
                                    SUM(AllotCountBefore) AS AllotCountBefore
                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                    Where (m.mem_Unit_ID IN (239,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID=5
                                ),0) AS Dianqi
                        From tg_unit unit
                        Where unit_ParentID<>0 AND unit_ID=232

                        union all

                        Select 3 AS ID,'3.奖金/人数' AS Name,
                        ((select CONVERT(decimal(18,2), COUNT(*)) AS MemCount
                        From (
                        select distinct(k.MemID)
                        from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                        where  m.mem_isFired=0 AND (m.mem_Unit_ID IN (231,232,282)) AND m.mem_Speciality_ID=1   AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                    ) B) ) AS JianZhu,
                    ((select CONVERT(decimal(18,2),COUNT(*)) AS MemCount
                        From (
                        select distinct(k.MemID)
                        from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                        where  m.mem_isFired=0 AND (m.mem_Unit_ID IN (237,282)) AND m.mem_Speciality_ID=2 AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                    ) B) ) AS JieGou,
                    ((select CONVERT(decimal(18,2),COUNT(*)) AS MemCount
                        From (
                        select distinct(k.MemID)
                        from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                        where m.mem_isFired=0 AND (m.mem_Unit_ID IN (238,282)) AND (m.mem_Speciality_ID=3 OR m.mem_Speciality_ID=4) AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                    ) B) ) AS SheBei,
                    ((select CONVERT(decimal(18,2),COUNT(*)) AS MemCount
                        From (
                        select distinct(k.MemID)
                        from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                        where  m.mem_isFired=0 AND (m.mem_Unit_ID IN (239,282)) AND m.mem_Speciality_ID=5 AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                    ) B) ) AS Dianqi
                        From tg_unit unit
                        Where unit_ParentID<>0 AND unit_ID=231

                        union all

                        Select 4 AS ID,'4.奖金/工资' AS Name,
                        (isnull((select SUM(yfgz)-SUM(yfjj)
                        from dbo.cm_KaoHeMemsWages a  
                        where a.MemID IN (
                            select MemID
                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                            Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND m.mem_Unit_ID IN (231,232,282) AND m.mem_Speciality_ID=1 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                        ) {1}),0)
	                    / isnull((select SUM(yfgz)-SUM(yfjj)
                        from dbo.cm_KaoHeMemsWages a  
                        where a.MemID IN (
	                        select MemID
	                        From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                        Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                        ) {1} AND a.MemID IN (Select mem_ID From tg_member where mem_Unit_ID IN (231,232,237,238,239,282))),1) 
                        )*100 AS JianZhu,
	                    (isnull((select SUM(yfgz)-SUM(yfjj)
                        from dbo.cm_KaoHeMemsWages a  
                        where a.MemID IN (
                            select MemID
                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                            Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND m.mem_Unit_ID IN (237,282) AND m.mem_Speciality_ID=2 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                        ) {1}),0)
	                    /  isnull((select SUM(yfgz)-SUM(yfjj)
                        from dbo.cm_KaoHeMemsWages a  
                        where a.MemID IN (
	                        select MemID
	                        From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                        Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                        ) {1} AND a.MemID IN (Select mem_ID From tg_member where mem_Unit_ID IN (231,232,237,238,239,282))),1)
                        )*100 AS JieGou,
	                    (isnull((select SUM(yfgz)-SUM(yfjj)
                        from dbo.cm_KaoHeMemsWages a  
                        where a.MemID IN (
                            select MemID
                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                            Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND  m.mem_Unit_ID IN (238,282) AND (m.mem_Speciality_ID=3 OR m.mem_Speciality_ID=4) AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                        ) {1}),0)
	                    /  isnull((select SUM(yfgz)-SUM(yfjj)
                        from dbo.cm_KaoHeMemsWages a  
                        where a.MemID IN (
	                        select MemID
	                        From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                        Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                        ) {1} AND a.MemID IN (Select mem_ID From tg_member where mem_Unit_ID IN (231,232,237,238,239,282))),1))*100 AS SheBei,
	                    (isnull((select SUM(yfgz)-SUM(yfjj)
                        from dbo.cm_KaoHeMemsWages a  
                        where a.MemID IN (
                            select MemID
                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                            Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND  m.mem_Unit_ID IN (239,282) AND m.mem_Speciality_ID=5 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                        ) {1}),0)
	                    /  isnull((select SUM(yfgz)-SUM(yfjj)
                        from dbo.cm_KaoHeMemsWages a  
                        where a.MemID IN (
	                        select MemID
	                        From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                        Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                        ) {1} AND a.MemID IN (Select mem_ID From tg_member where mem_Unit_ID IN (231,232,237,238,239,282))),1))*100 AS Dianqi
                    From tg_unit unit
                    Where unit_ParentID<>0 AND unit_ID=231
                    ", renwuid, strWhere, GuDongIdStr);


                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;
            }
        }


        public DataTable ProjAllotUnitColList
        {
            get
            {
                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }


                string renwuid = this.drpRenwu.SelectedValue;
                string strSql = string.Format(@"
                                        Select 1 AS ID,'1.部门合计' AS Name,
                                           isnull((
                                                    Select
                                                    SUM(AllotCount) AS AllotCount
                                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                                    Where (m.mem_Unit_ID IN (231,232,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID=1
                                                ),0) AS JianZhu,
                                                isnull((
                                                    Select
                                                    SUM(AllotCount) AS AllotCount
                                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                                    Where (m.mem_Unit_ID IN (237,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID=2
                                                ),0) AS JieGou,
                                                isnull((
                                                    Select
                                                    SUM(AllotCount) AS AllotCount
                                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                                    Where (m.mem_Unit_ID IN (238,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID IN (3,4)
                                                ),0) AS SheBei,
                                                isnull((
                                                    Select
                                                    SUM(AllotCount) AS AllotCount
                                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                                    Where (m.mem_Unit_ID IN (239,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID=5
                                                ),0) AS Dianqi
                                        From tg_unit unit
                                        Where unit_ParentID<>0 AND unit_ID=231
                                        union all

                                        Select 2 AS ID,'2.奖金占比' AS Name,
                                           isnull((
                                                    Select
                                                    SUM(AllotCount) AS AllotCount
                                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                                    Where (m.mem_Unit_ID IN (231,232,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID=1
                                                ),0) AS JianZhu,
                                                isnull((
                                                    Select
                                                    SUM(AllotCount) AS AllotCount
                                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                                    Where (m.mem_Unit_ID IN (237,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID=2
                                                ),0) AS JieGou,
                                                isnull((
                                                    Select
                                                    SUM(AllotCount) AS AllotCount
                                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                                    Where (m.mem_Unit_ID IN (238,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID IN (3,4)
                                                ),0) AS SheBei,
                                                isnull((
                                                    Select
                                                    SUM(AllotCount) AS AllotCount
                                                    From cm_KaoHeMemsAllotChange K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
                                                    Where (m.mem_Unit_ID IN (239,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND  RenWuId={0} AND m.mem_Speciality_ID=5
                                                ),0) AS Dianqi
                                        From tg_unit unit
                                        Where unit_ParentID<>0 AND unit_ID=232
                                        union all

                                        Select 3 AS ID,'3.部门人数' AS Name,
                                        (select CONVERT(decimal(18,2), COUNT(*)) AS MemCount
                                            From (
                                            select distinct(k.MemID)
                                            from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                            where m.mem_isFired=0 AND (m.mem_Unit_ID IN (231,232,282)) AND m.mem_Speciality_ID=1  AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                                        ) B)  AS JianZhu,
                                        (select CONVERT(decimal(18,2),COUNT(*)) AS MemCount
                                            From (
                                            select distinct(k.MemID)
                                            from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                            where m.mem_isFired=0 AND (m.mem_Unit_ID IN (237,282)) AND m.mem_Speciality_ID=2 AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                                        ) B) AS JieGou,
                                        (select CONVERT(decimal(18,2),COUNT(*)) AS MemCount
                                            From (
                                            select distinct(k.MemID)
                                            from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                            where m.mem_isFired=0 AND (m.mem_Unit_ID IN (238,282)) AND m.mem_Speciality_ID IN (3,4) AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                                        ) B) AS SheBei,
                                        (select CONVERT(decimal(18,2),COUNT(*)) AS MemCount
                                            From (
                                            select distinct(k.MemID)
                                            from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                            where m.mem_isFired=0 AND (m.mem_Unit_ID IN (239,282)) AND m.mem_Speciality_ID=5 AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                                        ) B) AS Dianqi
                                        From tg_unit unit
                                        Where unit_ParentID<>0 AND unit_ID=231

                                        union all

                                        Select 4 AS ID,'4.人数占比' AS Name,
                                        ((select CONVERT(decimal(18,2), COUNT(*)) AS MemCount
                                            From (
                                            select distinct(k.MemID)
                                            from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                            where  m.mem_isFired=0 AND (m.mem_Unit_ID IN (231,232,282)) AND m.mem_Speciality_ID=1  AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                                        ) B) ) AS JianZhu,
                                        ((select CONVERT(decimal(18,2),COUNT(*)) AS MemCount
                                            From (
                                            select distinct(k.MemID)
                                            from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                            where  m.mem_isFired=0 AND (m.mem_Unit_ID IN (237,282)) AND m.mem_Speciality_ID=2 AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                                        ) B) ) AS JieGou,
                                        ((select CONVERT(decimal(18,2),COUNT(*)) AS MemCount
                                            From (
                                            select distinct(k.MemID)
                                            from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                            where m.mem_isFired=0 AND (m.mem_Unit_ID IN (238,282)) AND m.mem_Speciality_ID IN (3,4) AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                                        ) B) ) AS SheBei,
                                        ((select CONVERT(decimal(18,2),COUNT(*)) AS MemCount
                                            From (
                                            select distinct(k.MemID)
                                            from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                            where  m.mem_isFired=0 AND (m.mem_Unit_ID IN (239,282)) AND m.mem_Speciality_ID=5 AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                                        ) B) ) AS Dianqi
                                        From tg_unit unit
                                        Where unit_ParentID<>0 AND unit_ID=231

                                        union all

                                        Select 5 AS ID,'5.奖金/人数' AS Name,
                                        ((select CONVERT(decimal(18,2), COUNT(*)) AS MemCount
                                            From (
                                            select distinct(k.MemID)
                                            from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                            where  m.mem_isFired=0 AND (m.mem_Unit_ID IN (231,232,282)) AND m.mem_Speciality_ID=1  AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                                        ) B) ) AS JianZhu,
                                        ((select CONVERT(decimal(18,2),COUNT(*)) AS MemCount
                                            From (
                                            select distinct(k.MemID)
                                            from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                            where  m.mem_isFired=0 AND (m.mem_Unit_ID IN (237,282)) AND m.mem_Speciality_ID=2 AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                                        ) B) ) AS JieGou,
                                        ((select CONVERT(decimal(18,2),COUNT(*)) AS MemCount
                                            From (
                                            select distinct(k.MemID)
                                            from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                            where m.mem_isFired=0 AND (m.mem_Unit_ID IN (238,282)) AND m.mem_Speciality_ID IN (3,4) AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                                        ) B) ) AS SheBei,
                                        ((select CONVERT(decimal(18,2),COUNT(*)) AS MemCount
                                            From (
                                            select distinct(k.MemID)
                                            from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                            where  m.mem_isFired=0 AND (m.mem_Unit_ID IN (239,282)) AND m.mem_Speciality_ID=5 AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) AND (m.mem_ID NOT IN ({2}))
                                        ) B) ) AS Dianqi
                                        From tg_unit unit
                                        Where unit_ParentID<>0 AND unit_ID=231

                                        union all

                                        Select 6 AS ID,'6.部门工资' AS Name,
	                                        isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
                                                select MemID
                                                From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                                                Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND m.mem_Unit_ID IN (231,232,282) AND m.mem_Speciality_ID=1 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1}),0) AS JianZhu,
	                                        isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
                                                select MemID
                                                From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                                                Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND m.mem_Unit_ID IN (237,282) AND m.mem_Speciality_ID=2 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1}),0) AS JieGou,
	                                        isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
                                                select MemID
                                                From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                                                Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND m.mem_Unit_ID IN (238,282) AND m.mem_Speciality_ID IN (3,4) AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1}),0) AS SheBei,
	                                        isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
                                                select MemID
                                                From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                                                Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND m.mem_Unit_ID IN (239,282) AND m.mem_Speciality_ID=5 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1}),0) AS Dianqi
                                        From tg_unit unit
                                        Where unit_ParentID<>0 AND unit_ID=231

                                        union all

                                        Select 7 AS ID,'7.工资占比' AS Name,
	                                        (isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
                                                select MemID
                                                From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                                                Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND m.mem_Unit_ID IN (231,232,282) AND m.mem_Speciality_ID=1 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1}),0)
	                                        / isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
	                                            select MemID
	                                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                                            Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1} AND a.MemID IN (Select mem_ID From tg_member where mem_Unit_ID IN (231,232,237,238,239,282))),1) 
                                            )*100 AS JianZhu,
	                                        (isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
                                                select MemID
                                                From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                                                Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND m.mem_Unit_ID IN (237,282) AND m.mem_Speciality_ID=2 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1}),0)
	                                        /  isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
	                                            select MemID
	                                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                                            Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1} AND a.MemID IN (Select mem_ID From tg_member where mem_Unit_ID IN (231,232,237,238,239,282))),1)
                                            )*100 AS JieGou,
	                                        (isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
                                                select MemID
                                                From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                                                Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND  m.mem_Unit_ID IN (238,282) AND m.mem_Speciality_ID IN (3,4) AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1}),0)
	                                        /  isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
	                                            select MemID
	                                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                                            Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1} AND a.MemID IN (Select mem_ID From tg_member where mem_Unit_ID IN (231,232,237,238,239,282))),1))*100 AS SheBei,
	                                        (isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
                                                select MemID
                                                From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                                                Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND  m.mem_Unit_ID IN (239,282) AND m.mem_Speciality_ID=5 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1}),0)
	                                        /  isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
	                                            select MemID
	                                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                                            Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1} AND a.MemID IN (Select mem_ID From tg_member where mem_Unit_ID IN (231,232,237,238,239,282))),1))*100 AS Dianqi
                                        From tg_unit unit
                                        Where unit_ParentID<>0 AND unit_ID=231

                                        union all

                                        Select 8 AS ID,'8.奖金/工资' AS Name,
	                                        (isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
                                                select MemID
                                                From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                                                Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND m.mem_Unit_ID IN (231,232,282) AND m.mem_Speciality_ID=1 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1}),0)
	                                        / isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
	                                            select MemID
	                                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                                            Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1} AND a.MemID IN (Select mem_ID From tg_member where mem_Unit_ID IN (231,232,237,238,239,282))),1) 
                                            )*100 AS JianZhu,
	                                        (isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
                                                select MemID
                                                From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                                                Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND m.mem_Unit_ID IN (237,282) AND m.mem_Speciality_ID=2 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1}),0)
	                                        /  isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
	                                            select MemID
	                                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                                            Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1} AND a.MemID IN (Select mem_ID From tg_member where mem_Unit_ID IN (231,232,237,238,239,282))),1)
                                            )*100 AS JieGou,
	                                        (isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
                                                select MemID
                                                From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                                                Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND  m.mem_Unit_ID IN (238,282) AND m.mem_Speciality_ID IN (3,4) AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1}),0)
	                                        /  isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
	                                            select MemID
	                                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                                            Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1} AND a.MemID IN (Select mem_ID From tg_member where mem_Unit_ID IN (231,232,237,238,239,282))),1))*100 AS SheBei,
	                                        (isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
                                                select MemID
                                                From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
                                                Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND  m.mem_Unit_ID IN (239,282) AND m.mem_Speciality_ID=5 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1}),0)
	                                        /  isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
	                                            select MemID
	                                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                                            Where (m.mem_ID NOT IN ({2})) AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) {1} AND a.MemID IN (Select mem_ID From tg_member where mem_Unit_ID IN (231,232,237,238,239,282))),1))*100 AS Dianqi
                                        From tg_unit unit
                                        Where unit_ParentID<>0 AND unit_ID=231", renwuid, strWhere, GuDongIdStr);

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;

            }
        }

        public decimal? GetWeightsByUnitID(int unitid)
        {

            string renwuid = this.drpRenwu.SelectedValue;
            string strWhere = string.Format(" RenwuID={0} AND UnitID={1} ", renwuid, unitid);

            var list = new TG.BLL.cm_KaoHeMemsAllotReport().GetModelList(strWhere);

            if (list.Count > 0)
            {
                return list[0].weights;
            }
            else
            {
                return (decimal)0;
            }
        }
        /// <summary>
        /// 比例
        /// </summary>
        public DataTable ProjAllotUnitList
        {

            get
            {
                string strWhere = "";
                if (RenwuModel != null)
                {
                    strWhere = string.Format(" AND a.SendYear=year('{0}') AND (a.SendMonth>= month('{0}') AND a.SendMonth<=month('{1}'))", RenwuModel.StartDate, RenwuModel.EndDate);

                    if (RenwuModel.StartDate.Year != RenwuModel.EndDate.Year)
                    {
                        strWhere = string.Format(" AND ((a.SendYear=year('{0}') AND a.SendMonth>=MONTH('{0}')) OR (a.SendYear=year('{1}') AND a.SendMonth=1))", RenwuModel.StartDate, RenwuModel.EndDate);
                    }
                }

                string renwuid = this.drpRenwu.SelectedValue;
                string strSql = string.Format(@"Select unit_ID,unit_Name
                                            ,isnull((
	                                            Select
	                                            SUM(AllotCount) AS AllotCount
	                                            From cm_KaoHeMemsAllot K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
	                                            Where m.mem_Unit_ID=unit.unit_ID AND m.mem_isFired=0 AND R.RoleIdMag<>5 AND ProKHNameId IN (Select A.ID From cm_KaoHeProjName A left join dbo.cm_KaoHeRenwuProj B on A.KaoHeProjId=B.ID  Where RenWuId={0})
                                            ),0) AS AllotCount
                                            ,(select COUNT(*) From tg_member a left join tg_memberRole b on a.mem_ID=b.mem_Id where b.RoleIdMag<>5 AND m.mem_Unit_ID=unit.unit_ID AND mem_isFired=0) AS MemCount2
                                            ,(select COUNT(*) AS MemCount
                                                From (
                                                select distinct(k.MemID)
                                                from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                                where m.mem_Unit_ID=unit.unit_ID AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) 
                                            ) B) AS MemCount
                                            ,(select COUNT(*) From tg_member a left join tg_memberRole b on a.mem_ID=b.mem_Id where b.RoleIdMag<>5 AND mem_isFired=0) AS AllMemCount2
                                            ,(select COUNT(*) AS MemCount
                                                From (
                                                select distinct(k.MemID)
                                                from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                                where (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) 
                                            ) B) AS AllMemCount                                              
                                            ,isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
	                                            select MemID
	                                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                                            Where R.RoleIdMag<>5 AND mem_isFired=0 AND m.mem_Unit_ID=unit.unit_ID AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) 
                                            {1}),0) AS BumenGz
                                            ,isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
	                                            select MemID
	                                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                                            Where R.RoleIdMag<>5 AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) 
                                            {1}),0) AS AllBumemGz
                                            From tg_unit unit 
                                            Where unit_ParentID<>0 AND unit_ID IN (231,232,237,238,239)", renwuid, strWhere);
                if (this.drp_unit3.SelectedValue != "-1")
                {
                    strSql = string.Format(@"Select unit_ID,unit_Name
                                            ,isnull((
	                                            Select
	                                            SUM(AllotCount) AS AllotCount
	                                            From cm_KaoHeMemsAllot K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id left join tg_unit u on u.unit_ID=m.mem_Unit_ID
	                                            Where m.mem_Unit_ID=unit.unit_ID AND m.mem_isFired=0 AND R.RoleIdMag<>5 AND ProKHNameId IN (Select A.ID From cm_KaoHeProjName A left join dbo.cm_KaoHeRenwuProj B on A.KaoHeProjId=B.ID  Where RenWuId={0})
                                            ),0) AS AllotCount
                                            ,(select COUNT(*) From tg_member a left join tg_memberRole b on a.mem_ID=b.mem_Id where b.RoleIdMag<>5 AND m.mem_Unit_ID=unit.unit_ID AND mem_isFired=0) AS MemCount2
                                            ,(select COUNT(*) AS MemCount
                                                From (
                                                select distinct(k.MemID)
                                                from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                                where m.mem_Unit_ID=unit.unit_ID AND (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) 
                                            ) B) AS MemCount
                                            ,(select COUNT(*) From tg_member a left join tg_memberRole b on a.mem_ID=b.mem_Id where b.RoleIdMag<>5 AND mem_isFired=0) AS AllMemCount2
                                            ,(select COUNT(*) AS MemCount
                                                From (
                                                select distinct(k.MemID)
                                                from dbo.cm_KaoHeSpeMemsPersent k left join tg_member m on k.MemID=m.mem_ID
                                                where (ProKHID IN (select ID from cm_KaoHeRenwuProj where RenWuId={0})) 
                                            ) B) AS AllMemCount  
                                            ,isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
	                                            select MemID
	                                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                                            Where R.RoleIdMag<>5 AND mem_isFired=0 AND m.mem_Unit_ID=unit.unit_ID AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) 
                                            {2}),0) AS BumenGz
                                            ,isnull((select SUM(yfgz)-SUM(yfjj)
                                            from dbo.cm_KaoHeMemsWages a  
                                            where a.MemID IN (
	                                            select MemID
	                                            From cm_KaoHeSpeMemsPersent KP left join tg_memberRole R on KP.MemID=R.mem_Id left join tg_member m on m.mem_ID=KP.MemID
	                                            Where R.RoleIdMag<>5 AND mem_isFired=0 AND (KP.ProKHID IN (Select ID From dbo.cm_KaoHeRenwuProj Where RenWuId={0}))
                                            ) 
                                            {2}),0) AS AllBumemGz
                                            From tg_unit unit
                                            Where unit_ID={1} AND unit_ParentID<>0", renwuid, this.drp_unit3.SelectedValue, strWhere);
                }

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                return dt;
            }
        }
        /// <summary>
        /// 股东ID
        /// </summary>
        public string GuDongIdStr
        {
            get
            {
                return "1440,1441,1442,1443,1445,1446,1474,1493,1498,1519,1538,1554,1577";
            }
        }
        /// <summary>
        /// 人员考核最终统计
        /// </summary>
        public List<KaoHeMemEntity> KaoHeMemAllotList
        {
            get
            {
                string unitid = this.drp_unit3.SelectedValue;
                string renwuid = this.drpRenwu.SelectedValue;
                string strSql = "";
                if (unitid != "-1")
                {
                    // AND (C.RenwuID={1} Or C.RenwuID is null)
                    strSql = string.Format(@"Select 
	                                          MAX(K.SpeID) AS SpeID
	                                          ,MAX(K.SpeName) AS SpeName
	                                          ,MAX(K.MemID) AS MemID
	                                          ,MAX(K.MemName) AS MemName
	                                          ,MAX(m.mem_Unit_ID) AS MemUnitID
	                                          ,SUM(K.AllotCount) AS AllotCount
                                              ,MAX(M.mem_isFired) AS IsFired
                                              ,isnull((Select SUM(AllotCountBefore) From  dbo.cm_KaoHeMemsAllotChange C Where RenwuID={1} AND C.MemID=MAX(m.mem_ID)),0) AS AllotCountBefore
                                              ,(select top 1 B.OrderID 
	                                             From ( 
		                                            select A.MemID
			                                              ,A.AllotCount
			                                              ,RANK() over(order by AllotCount DESC) AS OrderID
		                                            From (
                                                            select AA.AllotCount,AA.MemID
				                                            from (
				                                                select SUM(KA.AllotCount) AS AllotCount,KA.MemID
				                                                From cm_KaoHeMemsAllot KA inner join tg_member KM on KM.mem_ID=KA.MemID
				                                                Where KM.mem_Unit_ID=MAX(M.mem_Unit_ID) AND ProKHNameId IN (Select A.ID From cm_KaoHeProjName A left join dbo.cm_KaoHeRenwuProj B on A.KaoHeProjId=B.ID  Where RenWuId={1})
				                                                Group By KA.MemID,KM.mem_Unit_ID
                                                            ) AA left join tg_memberRole RR on RR.mem_Id=AA.MemID where RR.RoleIdMag<>5
		                                            ) A
	                                            )B
                                            Where B.MemID= M.mem_ID) AS OrderID
                                            ,isnull((select top 1 B.OrderID
                                                From (
                                                    select MemID
                                                            ,AllotCount
                                                            ,RANK() over(order by AllotCount DESC) AS OrderID
                                                    From (
                                                        Select r.MemID AS MemID
                                                        ,(case SUM(b.Weights) when 0 then 0 else isnull(SUM(b.Weights*r.JudgeCount)/SUM(b.Weights),0) end ) AS AllotCount
                                                        From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID left join dbo.cm_KaoHeRenwuMem BB on r.MemID=BB.memID
                                                        Where B.memUnitID=MAX(M.mem_Unit_ID) 
                                                        AND (R.KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{1}')) 
                                                        AND (B.KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{1}'))
                                                        AND BB.Statu=0 
                                                        AND (BB.KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{1}'))
                                                        group by r.memID
                                                    ) A
                                                ) B 
                                                Where B.MemID=m.mem_ID),0) AS UnitJudgeOrder
                                            From cm_KaoHeMemsAllot K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id
                                            Where m.mem_Unit_ID={0} AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({2})) AND ProKHNameId IN (Select A.ID From cm_KaoHeProjName A left join dbo.cm_KaoHeRenwuProj B on A.KaoHeProjId=B.ID  Where RenWuId={1})
                                            Group by m.mem_ID Order By AllotCount Desc", unitid, renwuid, GuDongIdStr);
                }
                else
                {
                    // AND (C.RenwuID={0} Or C.RenwuID is null)
                    strSql = string.Format(@"Select 
	                                          MAX(K.SpeID) AS SpeID
	                                          ,MAX(K.SpeName) AS SpeName
	                                          ,MAX(K.MemID) AS MemID
	                                          ,MAX(K.MemName) AS MemName
	                                          ,MAX(m.mem_Unit_ID) AS MemUnitID
	                                          ,SUM(K.AllotCount) AS AllotCount
                                              ,MAX(M.mem_isFired) AS IsFired
                                              ,isnull((Select SUM(AllotCountBefore) From  dbo.cm_KaoHeMemsAllotChange C Where RenwuID={0} AND C.MemID=MAX(m.mem_ID)),0) AS AllotCountBefore
                                            ,isnull((select top 1 B.OrderID 
	                                             From ( 
		                                            select A.MemID
			                                              ,A.AllotCount
			                                              ,RANK() over(order by AllotCount DESC) AS OrderID
		                                            From (
                                                            select AA.AllotCount,AA.MemID
				                                            from (
				                                                select SUM(KA.AllotCount) AS AllotCount,KA.MemID
				                                                From cm_KaoHeMemsAllot KA inner join tg_member KM on KM.mem_ID=KA.MemID
				                                                Where KM.mem_Unit_ID=MAX(M.mem_Unit_ID) AND ProKHNameId IN (Select A.ID From cm_KaoHeProjName A left join dbo.cm_KaoHeRenwuProj B on A.KaoHeProjId=B.ID  Where RenWuId={0})
				                                                Group By KA.MemID,KM.mem_Unit_ID
                                                            ) AA left join tg_memberRole RR on RR.mem_Id=AA.MemID where RR.RoleIdMag<>5
		                                            ) A
	                                            )B
                                            Where B.MemID= M.mem_ID),0) AS OrderID
                                            ,isnull((select top 1 B.OrderID
                                                From (
                                                    select MemID
                                                            ,AllotCount
                                                            ,RANK() over(order by AllotCount DESC) AS OrderID
                                                    From (
                                                        Select r.MemID AS MemID
                                                        ,(case SUM(b.Weights) when 0 then 0 else isnull(SUM(b.Weights*r.JudgeCount)/SUM(b.Weights),0) end ) AS AllotCount
                                                        From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID left join dbo.cm_KaoHeRenwuMem BB on r.MemID=BB.memID
                                                        Where B.memUnitID=MAX(M.mem_Unit_ID) 
                                                        AND (R.KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{0}')) 
                                                        AND (B.KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{0}'))
                                                        AND BB.Statu=0 
                                                        AND (BB.KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{0}'))
                                                        AND BB.memUnitID=MAX(M.mem_Unit_ID) 
                                                        group by r.memID
                                                    ) A
                                                ) B 
                                                Where B.MemID=m.mem_ID),0) AS UnitJudgeOrder
                                            From cm_KaoHeMemsAllot K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id
                                            Where (m.mem_Unit_ID IN (231,232,237,238,239,282)) AND m.mem_isFired=0 AND (m.mem_ID NOT IN ({1})) AND ProKHNameId IN (Select A.ID From cm_KaoHeProjName A left join dbo.cm_KaoHeRenwuProj B on A.KaoHeProjId=B.ID  Where RenWuId={0})
                                            Group by m.mem_ID Order By AllotCount Desc", renwuid, GuDongIdStr);
                }

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                string jsonStr = JsonConvert.SerializeObject(dt);

                var list = JsonConvert.DeserializeObject<List<KaoHeMemEntity>>(jsonStr);

                #region 注释部分
                //                //部门评分
                //                string strSql2 = "";
                //                if (unitid != "-1")
                //                {
                //                    strSql2 = string.Format(@"Select MAX(MemID) AS MemID
                //	                                           ,avg(JudgeCount) AS JudgeCount
                //                                                From dbo.cm_UserJudgeReport
                //                                                Where KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo={1} AND UnitID={0})
                //                                                Group By MemID
                //                                                order by JudgeCount Desc", unitid, renwuid);
                //                }
                //                else
                //                {
                //                    strSql2 = string.Format(@"Select MAX(MemID) AS MemID
                //	                                           ,avg(JudgeCount) AS JudgeCount
                //                                                From dbo.cm_UserJudgeReport
                //                                                Where KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo={0} )
                //                                                Group By MemID
                //                                                order by JudgeCount Desc", renwuid);
                //                }
                //                DataTable dtMems = TG.DBUtility.DbHelperSQL.Query(strSql2).Tables[0];

                //                int index = 1;
                //                foreach (DataRow dr in dtMems.Rows)
                //                {
                //                    int memid = Convert.ToInt32(dr["MemID"].ToString());

                //                    list.ForEach(c =>
                //                    {
                //                        if (memid == c.MemID)
                //                        {
                //                            //内部评分排名
                //                            c.UnitJudgeOrder = index++;
                //                        }
                //                    });
                //                }
                #endregion

                return list.OrderBy(c => c.MemID).ToList();
            }
        }
        /// <summary>
        /// 其他人员
        /// </summary>
        public List<KaoHeMemEntity> KaoHeMemAllotListOther
        {
            get
            {
                string unitid = this.drp_unit3.SelectedValue;
                string renwuid = this.drpRenwu.SelectedValue;
                string strSql = "";
                if (unitid != "-1")
                {
                    strSql = string.Format(@"Select 
	                                          MAX(K.SpeID) AS SpeID
	                                          ,MAX(K.SpeName) AS SpeName
	                                          ,MAX(K.MemID) AS MemID
	                                          ,MAX(K.MemName) AS MemName
	                                          ,MAX(m.mem_Unit_ID) AS MemUnitID
	                                          ,SUM(K.AllotCount) AS AllotCount
                                              ,MAX(M.mem_isFired) AS IsFired
                                              ,isnull((Select SUM(AllotCountBefore) From  dbo.cm_KaoHeMemsAllotChange C Where RenwuID={1} AND C.MemID=MAX(m.mem_ID)),0) AS AllotCountBefore
                                              ,(select top 1 B.OrderID 
	                                             From ( 
		                                            select A.MemID
			                                              ,A.AllotCount
			                                              ,RANK() over(order by AllotCount DESC) AS OrderID
		                                            From (
				                                            select SUM(KA.AllotCount) AS AllotCount,KA.MemID
				                                            From cm_KaoHeMemsAllot KA inner join tg_member KM on KM.mem_ID=KA.MemID
				                                            Where KM.mem_Unit_ID=MAX(M.mem_Unit_ID) AND ProKHNameId IN (Select A.ID From cm_KaoHeProjName A left join dbo.cm_KaoHeRenwuProj B on A.KaoHeProjId=B.ID  Where RenWuId={1})
				                                            Group By KA.MemID,KM.mem_Unit_ID
		                                            ) A
	                                            )B
                                            Where B.MemID= M.mem_ID) AS OrderID
                                            ,isnull((select top 1 B.OrderID
                                                From (
                                                    select MemID
                                                            ,AllotCount
                                                            ,RANK() over(order by AllotCount DESC) AS OrderID
                                                    From (
                                                        Select r.MemID AS MemID
                                                        ,(case SUM(b.Weights) when 0 then 0 else isnull(SUM(b.Weights*r.JudgeCount)/SUM(b.Weights),0) end ) AS AllotCount
                                                        From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID left join dbo.cm_KaoHeRenwuMem BB on r.MemID=BB.memID
                                                        Where B.memUnitID=MAX(M.mem_Unit_ID) 
                                                        AND (R.KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{1}')) 
                                                        AND (B.KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{1}'))
                                                        AND BB.Statu=0 
                                                        AND (BB.KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{1}'))
                                                        group by r.memID
                                                    ) A
                                                ) B 
                                                Where B.MemID=m.mem_ID),0) AS UnitJudgeOrder
                                                From cm_KaoHeMemsAllot K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id
                                                Where m.mem_Unit_ID={0} AND (m.mem_isFired=1 OR (m.mem_ID IN ({2})))  AND ProKHNameId IN (Select A.ID From cm_KaoHeProjName A left join dbo.cm_KaoHeRenwuProj B on A.KaoHeProjId=B.ID  Where RenWuId={1})
                                                Group by m.mem_ID Order By AllotCount Desc", unitid, renwuid, GuDongIdStr);
                }
                else
                {
                    strSql = string.Format(@"Select 
	                                          MAX(K.SpeID) AS SpeID
	                                          ,MAX(K.SpeName) AS SpeName
	                                          ,MAX(K.MemID) AS MemID
	                                          ,MAX(K.MemName) AS MemName
	                                          ,MAX(m.mem_Unit_ID) AS MemUnitID
	                                          ,SUM(K.AllotCount) AS AllotCount
                                              ,MAX(M.mem_isFired) AS IsFired
                                              ,isnull((Select SUM(AllotCountBefore) From  dbo.cm_KaoHeMemsAllotChange C Where RenwuID={0} AND C.MemID=MAX(m.mem_ID)),0) AS AllotCountBefore
                                            ,isnull((select top 1 B.OrderID 
	                                             From ( 
		                                            select A.MemID
			                                              ,A.AllotCount
			                                              ,RANK() over(order by AllotCount DESC) AS OrderID
		                                            From (
				                                            select SUM(KA.AllotCount) AS AllotCount,KA.MemID
				                                            From cm_KaoHeMemsAllot KA inner join tg_member KM on KM.mem_ID=KA.MemID
				                                            Where KM.mem_Unit_ID=MAX(M.mem_Unit_ID) AND ProKHNameId IN (Select A.ID From cm_KaoHeProjName A left join dbo.cm_KaoHeRenwuProj B on A.KaoHeProjId=B.ID  Where RenWuId={0})
				                                            Group By KA.MemID,KM.mem_Unit_ID
		                                            ) A
	                                            )B
                                            Where B.MemID= M.mem_ID),0) AS OrderID
                                            ,isnull((select top 1 B.OrderID
                                                From (
                                                    select MemID
                                                            ,AllotCount
                                                            ,RANK() over(order by AllotCount DESC) AS OrderID
                                                    From (
                                                        Select r.MemID AS MemID
                                                        ,(case SUM(b.Weights) when 0 then 0 else isnull(SUM(b.Weights*r.JudgeCount)/SUM(b.Weights),0) end ) AS AllotCount
                                                        From dbo.cm_UserJudgeReport R Left join dbo.cm_KaoHeRenwuMem B on R.InsertUserId=B.memID left join dbo.cm_KaoHeRenwuMem BB on r.MemID=BB.memID
                                                        Where B.memUnitID=MAX(M.mem_Unit_ID) 
                                                        AND (R.KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{0}')) 
                                                        AND (B.KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{0}'))
                                                        AND BB.Statu=0 
                                                        AND (BB.KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo='{0}'))
                                                        group by r.memID
                                                    ) A
                                                ) B 
                                                Where B.MemID=m.mem_ID),0) AS UnitJudgeOrder
                                                From cm_KaoHeMemsAllot K inner join tg_member M on K.MemID=m.mem_ID left join tg_memberRole R on m.mem_ID=r.mem_Id
                                                Where m.mem_ID<>1 AND ((m.mem_Unit_ID NOT IN (231,232,237,238,239,282)) OR m.mem_isFired=1 OR (m.mem_ID IN ({1}))) AND ProKHNameId IN (Select A.ID From cm_KaoHeProjName A left join dbo.cm_KaoHeRenwuProj B on A.KaoHeProjId=B.ID  Where RenWuId={0})
                                                Group by m.mem_ID Order By AllotCount Desc", renwuid, GuDongIdStr);
                }

                DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                string jsonStr = JsonConvert.SerializeObject(dt);

                var list = JsonConvert.DeserializeObject<List<KaoHeMemEntity>>(jsonStr);

                #region 注释部分
                //                //部门评分
                //                string strSql2 = "";
                //                if (unitid != "-1")
                //                {
                //                    strSql2 = string.Format(@"Select MAX(MemID) AS MemID
                //	                                           ,avg(JudgeCount) AS JudgeCount
                //                                                From dbo.cm_UserJudgeReport
                //                                                Where KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo={1} AND UnitID={0})
                //                                                Group By MemID
                //                                                order by JudgeCount Desc", unitid, renwuid);
                //                }
                //                else
                //                {
                //                    strSql2 = string.Format(@"Select MAX(MemID) AS MemID
                //	                                           ,avg(JudgeCount) AS JudgeCount
                //                                                From dbo.cm_UserJudgeReport
                //                                                Where KaoHeUnitID IN (Select ID From dbo.cm_KaoHeRenwuUnit Where RenWuNo={0} )
                //                                                Group By MemID
                //                                                order by JudgeCount Desc", renwuid);
                //                }
                //                DataTable dtMems = TG.DBUtility.DbHelperSQL.Query(strSql2).Tables[0];

                //                int index = 1;
                //                foreach (DataRow dr in dtMems.Rows)
                //                {
                //                    int memid = Convert.ToInt32(dr["MemID"].ToString());

                //                    list.ForEach(c =>
                //                    {
                //                        if (memid == c.MemID)
                //                        {
                //                            //内部评分排名
                //                            c.UnitJudgeOrder = index++;
                //                        }
                //                    });
                //                }
                #endregion

                return list.OrderBy(c => c.MemID).ToList();
            }
        }

        /// <summary>
        /// 普通人员
        /// </summary>
        public List<TG.Model.cm_KaoHeProjHis> KaoHeProjHis
        {
            get
            {
                string strWhere = string.Format(" IsOther=0 AND RenwuID={0}", this.RenWuID);
                var list = new TG.BLL.cm_KaoHeProjHis().GetModelList(strWhere);
                return list;
            }
        }
        /// <summary>
        /// 其他人员
        /// </summary>
        public List<TG.Model.cm_KaoHeProjHis> KaoHeProjHisOther
        {
            get
            {
                string strWhere = string.Format(" IsOther=1 AND RenwuID={0}", this.RenWuID);
                var list = new TG.BLL.cm_KaoHeProjHis().GetModelList(strWhere);
                return list;
            }
        }
        /// <summary>
        /// 项目奖金调整历史值
        /// </summary>
        public List<TG.Model.cm_KaoHeProjWeightHis> WeightListOne
        {
            get
            {
                string strWhere = string.Format(" Stat=0 AND RenwuID={0}", this.RenWuID);
                var list = new TG.BLL.cm_KaoHeProjWeightHis().GetModelList(strWhere);
                return list;
            }
        }
        /// <summary>
        /// 项目奖金计算值
        /// </summary>
        public List<TG.Model.cm_KaoHeProjWeightHis> WeightListTwo
        {
            get
            {
                string strWhere = string.Format(" Stat=1 AND RenwuID={0}", this.RenWuID);
                var list = new TG.BLL.cm_KaoHeProjWeightHis().GetModelList(strWhere);
                return list;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindRenwu();
            }
        }

        /// <summary>
        /// 绑定考核任务
        /// </summary>
        protected void BindRenwu()
        {
            if (RenwuList.Count > 0)
            {
                RenwuList.ForEach(c =>
                {
                    this.drpRenwu.Items.Add(new ListItem(c.RenWuNo, c.ID.ToString()));
                });
            }
            //选中当前任务
            if (this.drpRenwu.Items.FindByValue(KaoHeRenwu.ID.ToString()) != null)
            {
                this.drpRenwu.Items.FindByValue(KaoHeRenwu.ID.ToString()).Selected = true;
            }
        }
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            //部门列表显示设置显示部门与施工图部门 2017年4月18日
            string strWhere = string.Format(@" unit_ParentID<>0 
                                              AND Exists (select ID from cm_DropUnitList DU where flag=0 AND DU.unit_ID=tg_unit.unit_ID)
                                              AND Exists (select unit_ID from tg_unitExt UE Where unit_Type=0 AND UE.unit_ID=tg_unit.unit_ID)");

            this.drp_unit3.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }
        /// <summary>
        /// 查询单位
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetUnitName(int id)
        {
            return new TG.BLL.tg_unit().GetModel(id).unit_Name;
        }
        /// <summary>
        /// 用户考核数据
        /// </summary>
        public class KaoHeMemEntity
        {
            public KaoHeMemEntity() { }
            public int SpeID { get; set; }
            public string SpeName { get; set; }
            public int MemID { get; set; }
            public string MemName { get; set; }
            public int MemUnitID { get; set; }
            public string memUnitName { get; set; }
            public decimal? AllotCount { get; set; }
            public int IsFired { get; set; }
            public int OrderID { get; set; }
            public int UnitJudgeOrder { get; set; }

            public decimal? AllotCountBefore { get; set; }
        }
        /// <summary>
        /// 是否已经保存
        /// </summary>
        public bool IsSaved
        {
            get
            {
                bool issaved = false;
                var bll = new TG.BLL.cm_KaoHeMemsAllotChange();
                string strWhere = string.Format(" RenwuID={0} AND Stat=1 ", this.RenwuNo);

                if (bll.GetModelList(strWhere).Count > 0)
                {
                    issaved = true;
                }

                return issaved;
            }
        }
        /// <summary>
        /// 是否已经提交
        /// </summary>
        public bool IsSubmit
        {
            get
            {
                bool issaved = false;
                var bll = new TG.BLL.cm_KaoHeMemsAllotChange();
                string strWhere = string.Format(" RenwuID={0} AND Stat=1 ", this.RenwuNo);

                if (bll.GetModelList(strWhere).Count > 0)
                {
                    issaved = true;
                }

                return issaved;
            }
        }

        /// <summary>
        /// 判断是否已经归档
        /// </summary>
        public bool IsBak
        {
            get
            {
                string strWhere = string.Format(" RenwuID={0} ", this.RenWuID);

                var count = new TG.BLL.cm_KaoHeProjHis().GetRecordCount(strWhere);

                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        protected void btn_Search_Click(object sender, EventArgs e)
        {

        }
    }
}