﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Configuration;

namespace TG.Web.Project
{
    public partial class DwgFileList :PageBase
    {
        protected static ArgList arglist = new ArgList();
        //T-CD图纸路径
        protected string DwgPhysicsPath
        {
            get
            {
                //图纸路径
                return ConfigurationManager.AppSettings["FtpPysicsPath"].ToString();
            }
        }
        //网络下载路径
        protected string DwgVirsualPath
        {
            get
            {
                return ConfigurationManager.AppSettings["DwgVirsualPath"].ToString();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindTo_Grid();
            }
        } //绑定数据
        protected void BindTo_Grid()
        {
            //参数赋值
            string proid = Request.QueryString["proid"] ?? "0";
            string classid = Request.QueryString["classid"] ?? "0";
            string purposeid = Request.QueryString["purposeid"] ?? "";
            string itemid = Request.QueryString["id"] ?? "0";
            string ordermode = Request.QueryString["ordermode"] ?? "";
            string itemvalue = Request.QueryString["item"] ?? "";
            string speid = Request.QueryString["speid"] ?? "0";
            string parentid = Request.QueryString["parentid"] ?? "";
            //对结构体赋值
            arglist.proid = proid;
            arglist.classid = classid;
            arglist.purposeid = purposeid;
            arglist.itemid = itemid;
            arglist.ordermode = ordermode;
            arglist.itemvalue = itemvalue;
            arglist.speid = speid;
            arglist.parentid = parentid;

            BindTo_Grid(arglist, this.AspNetPager1.StartRecordIndex, this.AspNetPager1.PageSize);
        }
        //绑定文件列表
        protected void BindTo_Grid(ArgList arglist, int startIndex, int endIndex)
        {
            DataTable dt_pager = Init_DataTable(arglist);
            //记录数
            this.AspNetPager1.RecordCount = dt_pager.Rows.Count;
            //分页
            DataTable dt_source = GetPagedTable(dt_pager, this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            this.grid_list.DataSource = dt_source;
            this.grid_list.DataBind();
        }
        //初始化下载文件列表
        protected DataTable Init_DataTable(ArgList arglist)
        {
            string str_proid = "";
            string str_classid = "";
            string str_purposeid = "";
            string str_itemid = "";
            string str_itemvalue = "";
            string str_speid = "";
            string str_parentid = "";
            string str_cls_path = "";
            string str_cate = "";
            string str_strpath = "";

            str_proid = arglist.proid;
            str_classid = arglist.classid;
            str_purposeid = arglist.purposeid;
            str_itemid = arglist.itemid;
            str_itemvalue = arglist.itemvalue;
            str_speid = arglist.speid;
            str_parentid = arglist.purposeid;

            //查询数据
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            string str_sql = @"Select pro_Name From tg_project 
                                            Where pro_ID=(Select pro_parentID 
                                            From tg_subproject where pro_ID='" + str_proid + "')";
            DataTable dt_proj = bll_db.GetList(str_sql).Tables[0];
            //查询专业
            TG.BLL.tg_speciality bll_spec = new TG.BLL.tg_speciality();
            string str_w_spec = " spe_ID='" + str_classid + "'";
            DataTable dt_spec = bll_spec.GetList(str_w_spec).Tables[0];

            str_w_spec = " spe_ID='" + str_speid + "'";
            DataTable dt_spec2 = bll_spec.GetList(str_w_spec).Tables[0];

            //查询阶段
            TG.BLL.tg_purpose bll_purpose = new TG.BLL.tg_purpose();
            string str_pur = " purpose_ID=" + str_purposeid;
            DataTable dt_purpose = bll_purpose.GetList(str_pur).Tables[0];
            if (dt_purpose.Rows.Count > 0)
            {
                str_cate = dt_purpose.Rows[0]["purpose_Name"].ToString();
            }

            if (str_itemvalue == "1")
            {
                string str_s_name = "";
                if (dt_spec2.Rows.Count > 0)
                {
                    str_s_name = dt_spec2.Rows[0]["spe_Name"].ToString();
                }
                str_strpath = dt_proj.Rows[0]["pro_Name"].ToString().Trim();
                //拼接路径
                if (str_proid.Trim() != "")
                {
                    str_strpath += "/" + str_proid;
                }
                if (str_cate.Trim() != "")
                {
                    str_strpath += "/" + str_cate;
                }
                if (str_s_name.Trim() != "")
                {
                    str_strpath += "/" + str_s_name;
                }
                str_cls_path = GetPath(str_classid, "");
            }
            else
            {
                string str_s_name = "";
                if (dt_spec.Rows.Count > 0)
                {
                    str_s_name = dt_spec.Rows[0]["spe_Name"].ToString().Trim();
                }
                str_strpath = dt_proj.Rows[0]["pro_Name"].ToString().Trim();
                //拼接路径
                if (str_proid.Trim() != "")
                {
                    str_strpath += "/" + str_proid;
                }
                if (str_cate.Trim() != "")
                {
                    str_strpath += "/" + str_cate;
                }
                if (str_s_name.Trim() != "")
                {
                    str_strpath += "/" + str_s_name;
                }
            }
            //动态创建文件下载数据表
            DataTable rltTable = new DataTable();
            DataRow rltDr = null;
            rltTable.Columns.Add("dwgname");
            rltTable.Columns.Add("createuser");
            rltTable.Columns.Add("updateuser");
            rltTable.Columns.Add("filesize");
            rltTable.Columns.Add("createtime");
            rltTable.Columns.Add("version");
            rltTable.Columns.Add("sub");
            rltTable.Columns.Add("path");
            //声明查询字符串变量
            string str_pck_sql = "";
            if (str_purposeid != "")
            {
                //查询图纸
                str_pck_sql = @"Select package_ID,user_Id,filename,ver_date,file_size,remember,file_state,lastModifyUser_Id,version 
                            From tg_package where subpro_ID=" + str_proid + "and class_ID=" + str_classid + "and purpose_ID=" + str_purposeid + " order by package_ID DESC";
                DataTable dt_pack = bll_db.GetList(str_pck_sql).Tables[0];
                //动态生成数据表
                TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
                foreach (DataRow dr in dt_pack.Rows)
                {
                    string str_userid = dr["user_Id"].ToString();
                    string str_modifyid = dr["lastModifyUser_Id"].ToString();

                    string str_createuser = bll_mem.GetModel(int.Parse(str_userid)).mem_Name;
                    string str_modifyuser = "";
                    if (str_modifyid != "0")
                    {
                        str_modifyuser = bll_mem.GetModel(int.Parse(str_modifyid)).mem_Name;
                    }
                    if (UserSysNo.ToString() != "0")
                    {
                       //修改路径 by long 20130515
                        string downurl = DwgVirsualPath + str_strpath + str_cls_path + "/" + dr["filename"].ToString().Trim();
                        string serverpath = str_strpath + str_cls_path;
                        string path_dwg = DwgPhysicsPath + ReversolePath(serverpath) + "\\" + dr["filename"].ToString();
                        //是否存在文件
                        if (!ExsitFile(path_dwg))
                        {
                            path_dwg = "#";
                            downurl = path_dwg;
                        }
                        //图纸信息
                        string str_filename = dr["filename"].ToString();
                        string str_menname = str_createuser;
                        string str_filesize = Convert.ToString(Convert.ToInt32(dr["file_size"].ToString()) / 1024);
                        string str_verdate = dr["ver_date"].ToString();
                        string str_version = dr["version"].ToString();
                        string str_remember = dr["remember"].ToString();

                        rltDr = rltTable.NewRow();
                        rltDr[0] = str_filename;
                        rltDr[1] = str_menname;
                        rltDr[2] = str_modifyuser;
                        rltDr[3] = str_filesize;
                        rltDr[4] = str_verdate;
                        rltDr[5] = str_version;
                        rltDr[6] = str_remember;
                        rltDr[7] = downurl;

                        rltTable.Rows.Add(rltDr);
                    }
                }
            }
            else if (str_purposeid == "" && str_proid != "" && str_classid != "")
            {
                string str_sql2 = @"Select pro_Name from tg_project 
                                Where pro_ID=(select pro_parentID from tg_subproject where pro_ID='" + str_proid + "')";
                DataTable dt_proj2 = bll_db.GetList(str_sql2).Tables[0];
                //项目名称
                string str_proname = dt_proj2.Rows[0]["pro_Name"].ToString();
                //子目录
                TG.BLL.tg_subpurpose bll_subpur = new TG.BLL.tg_subpurpose();
                string str_w_subpur = " subproid=" + str_proid + "";
                DataTable dt_subpur = bll_subpur.GetList(str_w_subpur).Tables[0];
                //主目录
                TG.BLL.tg_purpose bll_pur = new TG.BLL.tg_purpose();
                string str_w_pur = " purpose_ID=" + str_itemid + "";
                DataTable dt_pur = bll_pur.GetList(str_w_pur).Tables[0];
                //目录名
                string str_purname = dt_pur.Rows[0]["purpose_Name"].ToString();
                //路径
                string str_tmp_path = str_proname ;
                //拼接路径
                if (str_proid.Trim()!="")
                {
                    str_tmp_path += "/" + str_proid;
                }
                if (str_purname.Trim() != "")
                {
                    str_tmp_path += "/" + str_purname;
                }
                //子专业路径
                if (str_itemvalue == "1")
                {
                    str_cls_path = GetPath(str_classid, "");
                }
                //查询图纸
                string str_pack_sql = @"Select user_Id,filename,ver_date,file_size,remember,file_state,lastModifyUser_Id,version
                                From tg_package where subpro_ID=" + str_proid + "and class_ID=" + str_classid + " and purpose_ID=" + str_itemid + "";
                DataTable dt_pack = bll_db.GetList(str_pack_sql).Tables[0];
                //声明用户变量
                string str_userid = "";
                string str_modifyid = "";
                if (dt_pack.Rows.Count > 0)
                {
                    str_userid = dt_pack.Rows[0]["user_Id"].ToString();
                    str_modifyid = dt_pack.Rows[0]["lastModifyUser_Id"].ToString();
                }
                //声明成员
                TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
                foreach (DataRow dr in dt_pack.Rows)
                {
                    //图纸用户
                    string str_user = bll_mem.GetModel(int.Parse(str_userid)).mem_Name;
                    string str_modifyuser = "";
                    if (str_modifyid != "0")
                    {
                        str_modifyuser = bll_mem.GetModel(int.Parse(str_modifyid)).mem_Name;
                    }
                    if (UserSysNo.ToString() != "0")
                    {
                        //修改路径 by long 20130515
                        string downurl = DwgVirsualPath + str_tmp_path + str_cls_path + "/" + dr["filename"].ToString().Trim();
                        string serverpath = str_tmp_path + str_cls_path;
                        string path_dwg = DwgPhysicsPath + ReversolePath(serverpath) + "\\" + dr["filename"].ToString();
                        //图纸是否存在
                        if (!ExsitFile(path_dwg))
                        {
                            path_dwg = "#";
                            downurl = path_dwg;
                        }

                        string str_filename = dr["filename"].ToString();
                        string str_menname = str_user;
                        string str_filesize = Convert.ToString(Convert.ToInt32(dr["file_size"].ToString()) / 1024);
                        string str_verdate = dr["ver_date"].ToString();
                        string str_version = dr["version"].ToString();
                        string str_remember = dr["remember"].ToString();

                        rltDr = rltTable.NewRow();
                        rltDr[0] = str_filename;
                        rltDr[1] = str_menname;
                        rltDr[2] = str_modifyuser;
                        rltDr[3] = str_filesize;
                        rltDr[4] = str_verdate;
                        rltDr[5] = str_version;
                        rltDr[6] = str_remember;
                        rltDr[7] = downurl;

                        rltTable.Rows.Add(rltDr);
                    }
                }
            }
            //返回数据表
            return rltTable;
        }
        //替换路径反斜杠
        protected string ReversolePath(string strpath)
        {
            return strpath.Replace(@"/", @"\");
        }
        //判断文件是否存在
        protected bool ExsitFile(string path)
        {
            if (File.Exists(path))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //获取路径
        private string GetPath(string id, string subpath)
        {
            string str_propath = "";
            string str_parentid = "0";
            string str_subname = "";
            TG.BLL.tg_subClass bll_subclass = new TG.BLL.tg_subClass();
            string str_where = " id=" + id;
            string strSql = "SELECT name,parentID FROM tg_subClass WHERE id=" + id + "";
            DataTable dt_subclass = bll_subclass.GetList(str_where).Tables[0];

            if (dt_subclass.Rows.Count > 0)
            {
                str_subname = dt_subclass.Rows[0]["name"].ToString().Trim();
                str_parentid = dt_subclass.Rows[0]["parentID"].ToString();
            }

            if (str_parentid != null && str_parentid != "0")
            {
                str_propath = GetPath(str_parentid, subpath);
            }

            subpath = str_propath + str_subname + "/";
            //返回子路径
            return subpath.ToString().Trim();
        }

        /// <summary>
        /// DataTable分页
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <param name="PageIndex">页索引,注意：从1开始</param>
        /// <param name="PageSize">每页大小</param>
        /// <returns>分好页的DataTable数据</returns>
        public DataTable GetPagedTable(DataTable dt, int startIndex, int endIndex)
        {
            if (startIndex == 0)
            {
                return dt;
            }
            DataTable newdt = dt.Copy();
            newdt.Clear();
            int rowbegin = startIndex - 1;
            int rowend = endIndex;
            if (rowbegin >= dt.Rows.Count)
            {
                return newdt;
            }
            if (rowend > dt.Rows.Count)
            {
                rowend = dt.Rows.Count;
            }
            for (int i = rowbegin; i <= rowend - 1; i++)
            {
                DataRow newdr = newdt.NewRow();
                DataRow dr = dt.Rows[i];
                foreach (DataColumn column in dt.Columns)
                {
                    newdr[column.ColumnName] = dr[column.ColumnName];
                }
                newdt.Rows.Add(newdr);
            }
            return newdt;
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindTo_Grid();
        }
    }
    //结构体
    public struct ArgList
    {
        public string proid;
        public string classid;
        public string purposeid;
        public string itemid;
        public string ordermode;
        public string itemvalue;
        public string speid;
        public string parentid;
    }
}
