﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.Project
{
    public partial class ShowISO : System.Web.UI.Page
    {
        /// <summary>
        /// 项目id
        /// </summary>
        public int ProjectID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["proid"], out proid);
                return proid;
            }

        }
        /// <summary>
        /// 人员id
        /// </summary>
        public int MemID
        {
            get
            {
                int memid = 0;
                int.TryParse(Request["memid"], out memid);
                return memid;
            }

        }
        public string projectName { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.hiddenSysno.Value = ProjectID.ToString();
            //Response.Write(MemID + "dd" + ProjectID);
            Getdesignphase();

        }

        /// <summary>
        /// 根据tg_project的id找到项目的设计阶段
        /// </summary>
        protected void Getdesignphase()
        {
            TG.BLL.cm_Project probll = new TG.BLL.cm_Project();
            string sql = @"SELECT pro_status  FROM cm_Project WHERE ReferenceSysNo=" + ProjectID;
            string sqlname = @"SELECT pro_name  FROM cm_Project WHERE ReferenceSysNo=" + ProjectID;
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            object name = TG.DBUtility.DbHelperSQL.GetSingle(sqlname);
            if (name != null)
            {
                projectName = name.ToString();
            }
            if (o != null)
            {
                string[] StatusArray = o.ToString().Split(',');
                if (StatusArray.Length > 0)
                {
                    for (int i = 0; i < StatusArray.Length; i++)
                    {
                        if (StatusArray[i] == "方案设计")
                        {
                            this.Frist.Value = "方案设计";
                        }
                        else if (StatusArray[i] == "初步设计")
                        {
                            this.Secend.Value = "初步设计";
                        }
                        else if (StatusArray[i] == "施工图设计")
                        {
                            this.Third.Value = "施工图设计";
                        }
                        else if (StatusArray[i] == "其他")
                        {
                            this.Fours.Value = "其他";
                        }
                    }

                }
                List<string> Statuslist = new List<string>();
                int index = 0;
                for (int i = 0; i < StatusArray.Length; i++)
                {
                    if (StatusArray[i].Trim() != "" && StatusArray[i].Trim() != "其他")
                    {
                        Statuslist.Add(StatusArray[i]);
                        index++;
                    }
                }
                this.drp_status.DataSource = Statuslist;
                this.drp_status.DataBind();
                this.drp_status2.DataSource = Statuslist;
                this.drp_status2.DataBind();
                this.drp_status3.DataSource = Statuslist;
                this.drp_status3.DataBind();
            }
        }

    }
}