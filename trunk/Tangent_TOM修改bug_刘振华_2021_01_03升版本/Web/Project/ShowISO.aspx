﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowISO.aspx.cs" Inherits="TG.Web.Project.ShowISO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/js/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
        type="text/css">
    <link href="/js/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"
        type="text/css">
    <link href="/js/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet"
        type="text/css">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Customer.css" rel="stylesheet" type="text/css" />
    <link href="../css/Project.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            //生产经营表单
            //            $("#scjyAll").click(function () {
            //                if ($(this).get(0).checked) {
            //                    $(":checkbox", $("#tb_list_head tr:eq(1)")).attr("checked", "true");
            //                }
            //                else {
            //                    $(":checkbox", $("#tb_list_head tr:eq(1)")).attr("checked", $(this).get(0).checked);
            //                }
            //            });
            //协同平台表单
            //            $("#xtptAll").click(function () {
            //                if ($(this).get(0).checked) {
            //                    $(":checkbox[name='xtpt']").attr("checked", "true");
            //                }
            //                else {
            //                    $(":checkbox[name='xtpt']").removeAttr("checked");
            //                }
            //            });
            //批量导出word
            //            $("#export").click(function () {

            //                if ($(":checked", $(".tb_row")).length == 0) {
            //                    alert("请选择一项要导出的项");
            //                    return false;
            //                }
            //                var optionAll = "";
            //                var proSysNo = $("#hiddenSysno").val();
            //                //获取客户信息SysNo
            //                $.each($("tr", $(".tb_row")), function (index, item) {
            //                    //判断是CheckBox是否被选中
            //                    if ($(":checkbox", $(item)).attr("checked") == "checked") {
            //                        //获取该行内隐藏的SysNo
            //                        var option = $("a", $(item)).attr("id");
            //                        if (option == "outAudit") {
            //                            window.open("ISOWord.aspx?option=" + option + "&sysno=" + proSysNo);
            //                        }
            //                        else if (option == "outKanCha") {
            //                            window.open("ISOWord.aspx?option=" + option + "&sysno=" + proSysNo);
            //                        }
            //                        else if (option == "GCJingGang") {
            //                            var status = $("#drp_status3").val();
            //                            if (status == "-1") {
            //                                alert("请选择设计阶段！");
            //                                return false;
            //                            }
            //                            else {

            //                                window.open("ISOWord.aspx?option=" + option + "&sysno=" + proSysNo + "&status=" + status);
            //                            }
            //                        }
            //                        else if (option == "XMJinDu") {
            //                            var status = $("#drp_status2").val();
            //                            if (status == "-1") {
            //                                alert("请选择设计阶段！");
            //                                return false;

            //                            }
            //                            else {
            //                                window.open("ISOWord.aspx?option=" + option + "&sysno=" + proSysNo + "&status=" + status);
            //                            }
            //                        }
            //                        else if (option == "GKZiLiao") {
            //                            window.open("ISOWord.aspx?option=" + option + "&sysno=" + proSysNo);
            //                        }
            //                        else if (option == "ShRPSh") {
            //                            var status = $("#drp_status").val();
            //                            if (status == "-1") {
            //                                alert("请选择设计阶段！");
            //                                return false;
            //                            }
            //                            else {
            //                                window.open("ISOWord.aspx?option=" + option + "&sysno=" + proSysNo + "&status=" + status);
            //                            }
            //                        }
            //                    }
            //                });

            //            })
            //单个表单的导出
            //隔行变色
            $("#outAudit").click(function () {
                var option = $(this).attr("id");
                var proSysNo = $("#hiddenSysno").val();
                window.open("ISOWord.aspx?option=" + option + "&sysno=" + proSysNo);
            })
            $("#outKanCha").click(function () {
                var option = $(this).attr("id");
                var proSysNo = $("#hiddenSysno").val();
                window.open("ISOWord.aspx?option=" + option + "&sysno=" + proSysNo);
            })

            ///tcd
            $("#GCJingGang").click(function () {
                var option = $(this).attr("id");
                var proSysNo = $("#hiddenSysno").val();
                var status = $("#drp_status3").val();
                if (status == "-1") {
                    alert("请选择设计阶段！");
                    return false;
                }
                window.open("ISOWord.aspx?option=" + option + "&sysno=" + proSysNo + "&status=" + status);
            })
            $("#XMJinDu").click(function () {
                var option = $(this).attr("id");
                var proSysNo = $("#hiddenSysno").val();
                var status = $("#drp_status2").val();
                if (status == "-1") {
                    alert("请选择设计阶段！");
                    return false;
                }
                window.open("ISOWord.aspx?option=" + option + "&sysno=" + proSysNo + "&status=" + status);
            })
            $("#GKZiLiao").click(function () {
                var option = $(this).attr("id");
                var proSysNo = $("#hiddenSysno").val();
                window.open("ISOWord.aspx?option=" + option + "&sysno=" + proSysNo);
            })
            $("#ShRPSh").click(function () {
                var option = $(this).attr("id");
                var proSysNo = $("#hiddenSysno").val();
                var status = $("#drp_status").val();
                if (status == "-1") {
                    alert("请选择设计阶段！");
                    return false;
                }
                window.open("ISOWord.aspx?option=" + option + "&sysno=" + proSysNo + "&status=" + status);
            })

        })
    </script>
    <style type="text/css">
        .tablemaind td
        {
            border: solid 1px #CCC;
            word-break: keep-all; /* 不换行 */
            white-space: nowrap; /* 不换行 */
            overflow: hidden; /* 内容超出宽度时隐藏超出部分的内容 */
            text-overflow: ellipsis; /* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用。*/
        }
        .tablemaind
        {
            border-collapse: collapse;
            border: solid 1px #CCC;
            width: 100%;
            table-layout: fixed;
            font-size: 12px;
            font-family: 微软雅黑;
        }
        .tablemaind tr:hover
        {
            background-color: #CCC;
        }
        a
        {
            font-size: 12px;
        }
        
        a:link
        {
            color: blue;
            text-decoration: none;
        }
        
        a:active:
        {
            color: red;
        }
        a:visited
        {
            color: blue;
            text-decoration: none;
        }
        
        a:hover
        {
            color: red;
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h3>
                生产经营表单库
            </h3>
            <table class="table table-bordered table-hover">
                <tr>
                    <th align="center" style="width: 16%;">
                        ISO 表 单 名 称
                    </th>
                    <th align="center" style="width: 4%;">
                        项 目 名 称
                    </th>
                    <th align="center" style="width: 4%;">
                        操 作
                    </th>
                </tr>
                <tr>
                    <td align="left" style="width: 16%;">
                        <img src="../Images/fileicon/doc.gif" />
                        13.QR-7.1-01《合同评审表》
                    </td>
                    <td title="<%=projectName %>" align="center" style="width: 4%;">
                        <%=projectName%>
                    </td>
                    <td align="center" style="width: 4%;">
                        <a href="###" id="outAudit">导出</a>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <img src="../Images/fileicon/doc.gif" />
                        16.QR-7.1-04《勘察设计任务通知单》
                    </td>
                    <td title="<%=projectName %>" align="center">
                        <%=projectName%>
                    </td>
                    <td align="center">
                        <a href="###" id="outKanCha">导出</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>
                协同平台表单库
            </h3>
            <table class="table table-bordered table-hover">
                <tr>
                    <td align="center" style="width: 16%">
                        ISO 表 单 名 称
                    </td>
                    <td align="center" style="width: 4%;">
                        设计阶段
                    </td>
                    <td align="center" style="width: 4%;">
                        操 作
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 16%">
                        <img src="../Images/fileicon/doc.gif" />2.QR-7.2-01《工程项目进岗人员审批表》
                    </td>
                    <td align="center" style="width: 4%">
                        <asp:DropDownList ID="drp_status3" runat="server" CssClass="form-control input-sm"
                            Width="135px" AppendDataBoundItems="true">
                            <asp:ListItem Value="-1">------设计阶段-------</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="center" style="width: 4%">
                        <a href="###" id="GCJingGang">导出</a>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <img src="../Images/fileicon/doc.gif" />3.QR-7.2-02 《项目进度计划表》
                    </td>
                    <td align="center">
                        <asp:DropDownList ID="drp_status2" runat="server" Width="135px" CssClass="form-control input-sm"
                            AppendDataBoundItems="true">
                            <asp:ListItem Value="-1">------设计阶段-------</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="center">
                        <a href="###" id="XMJinDu">导出</a>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <img src="../Images/fileicon/doc.gif" />20 QR-7.7-01《顾客提供资料记录卡》
                    </td>
                    <td align="center">
                    </td>
                    <td align="center">
                        <a href="###" id="GKZiLiao">导出</a>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <img src="../Images/fileicon/doc.gif" />6.QR-7.2-04《设计输入评审记录》
                    </td>
                    <td align="center">
                        <asp:DropDownList ID="drp_status" runat="server" Width="135px" CssClass="form-control input-sm"
                            AppendDataBoundItems="true">
                            <asp:ListItem Value="-1">------设计阶段-------</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="center">
                        <a href="###" id="ShRPSh">导出</a>
                    </td>
                </tr>
            </table>
            </fieldset>
        </div>
        <!--隐藏域-->
        <input type="hidden" runat="Server" id="Frist" value="" />
        <input type="hidden" runat="Server" id="Secend" value="" />
        <input type="hidden" runat="Server" id="Third" value="" />
        <input type="hidden" runat="Server" id="Fours" value="" />
        <input type="hidden" runat="Server" id="hiddenSysno" value="" />
    </form>
</body>
</html>
