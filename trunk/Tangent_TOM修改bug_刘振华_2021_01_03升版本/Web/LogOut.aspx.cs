﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web
{
    public partial class LoginOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ClearAllCookie();
            //然后跳转
            Response.Redirect("Login.aspx", true);
        }

        protected void ClearAllCookie()
        {
            HttpCookie cok = Request.Cookies["userinfo"];
            if (cok != null)
            {
                TimeSpan ts = new TimeSpan(-1, 0, 0, 0);
                cok.Expires = DateTime.Now.Add(ts);//删除整个Cookie，只要把过期时间设置为现在

                Response.AppendCookie(cok);
            }
        }
    }
}
