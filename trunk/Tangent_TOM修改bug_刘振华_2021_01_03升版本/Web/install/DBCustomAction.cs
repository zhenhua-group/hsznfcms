﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Xml;
using System.IO;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace TG.Web
{
    [RunInstaller(true)]
    public partial class DBCustomAction : Installer
    {
        public DBCustomAction()
        {
            InitializeComponent();
        }

        //重载
        public override void Install(System.Collections.IDictionary stateSaver)
        {
            base.Install(stateSaver);
            //服务器
            string server = this.Context.Parameters["server"];
            string dbname = this.Context.Parameters["dbname"];
            string user = this.Context.Parameters["user"];
            string pwd = this.Context.Parameters["pwd"];
            string targetdir = this.Context.Parameters["targetdir"];
            string college = this.Context.Parameters["college"];
            string collegeuser = this.Context.Parameters["collegeuser"];
            string day = this.Context.Parameters["day"];
            string useradmin = this.Context.Parameters["admin"];

            //执行Sql脚本
            InitDatabase(server, user, pwd, targetdir);
            //写入config
            WriteConfigConString(server, dbname, user, pwd, targetdir, college, collegeuser, day, useradmin);
        }

        //创建数据库
        private void ExcuteSql(string strcon, string targetdir)
        {
            string strpath = targetdir + "tangent_db.sql";
            if (!File.Exists(strpath))
            {
                throw new Exception("数据库初始化脚本文件不存在！");
            }
            else
            {
                //读取脚本信息
                string script = File.OpenText(strpath).ReadToEnd();
                //执行脚本
                SqlConnection conn = new SqlConnection(strcon);
                Server server = new Server(new ServerConnection(conn));
                try
                {
                    int i = server.ConnectionContext.ExecuteNonQuery(script);

                    if (i < 0)
                    {
                        throw new Exception("数据库脚本初始化失败！");
                    }
                }
                catch (System.Exception ex)
                {
                    throw new Exception("数据库连接错误,或数据表已存在！");
                }
                finally
                {
                    conn.Close();
                }
            }
        }
        //创建数据库并执行脚本
        private void InitDatabase(string server, string user, string pwd, string targetdir)
        {
            string strconn = string.Format("Data Source={0};User Id={1};Password={2};Persist Security Info=false;Packet Size=4096", server, user, pwd);
            ExcuteSql(strconn, targetdir);
        }
        //配置文件数据库
        private void WriteConfigConString(string server, string dbname, string user, string pwd, string targetdir, string college, string collegeuser, string day, string useradmin)
        {
            FileInfo filecfg = new FileInfo(targetdir + "web.config");
            if (!filecfg.Exists)
            {
                throw new Exception("配置文件不存在！");
            }
            else
            {
                XmlDocument xmlcfg = new XmlDocument();
                xmlcfg.Load(filecfg.FullName);
                XmlNodeList nodelist = xmlcfg.SelectNodes("//appSettings");
                //查到appsetting 节点
                foreach (XmlNode node in nodelist)
                {
                    if (node.Name == "appSettings")
                    {
                        foreach (XmlNode nodechild in node.ChildNodes)
                        {
                            if (nodechild.Attributes != null)
                            {
                                //数据库
                                if (nodechild.Attributes["key"].Value == "ConnectionString")
                                {
                                    nodechild.Attributes["value"].Value = string.Format("Persist Security Info=False;Data Source={0};Initial Catalog={1};User ID={2};Password={3};Packet Size=4096;Pooling=true;Max Pool Size=100;Min Pool Size=1", server, dbname, user, pwd);
                                }
                                //院名称
                                if (nodechild.Attributes["key"].Value == "CollegeName")
                                {
                                    nodechild.Attributes["value"].Value = college;
                                }
                                //院长
                                if (nodechild.Attributes["key"].Value == "ColleageUser")
                                {
                                    nodechild.Attributes["value"].Value = collegeuser;
                                }
                                //提醒天数
                                if (nodechild.Attributes["key"].Value == "WarningDay")
                                {
                                    nodechild.Attributes["value"].Value = day;
                                }
                                //超级管理员
                                if (nodechild.Attributes["key"].Value == "MenuControl")
                                {
                                    nodechild.Attributes["value"].Value = useradmin;
                                }
                            }
                        }
                    }
                }
                //保存xml
                xmlcfg.Save(filecfg.FullName);
            }
        }
    }
}
