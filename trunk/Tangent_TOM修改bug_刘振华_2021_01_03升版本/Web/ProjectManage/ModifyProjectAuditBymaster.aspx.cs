﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;
using TG.BLL;
using Geekees.Common.Controls;
using System.Xml;
using System.Text;
using System.Data;
using AjaxPro;

namespace TG.Web.ProjectManage
{
    public partial class ModifyProjectAuditBymaster : PageBase
    {
        public string ProjectAuditNo
        {
            get
            {
                return Request["projectAuditSysNo"];
            }
        }
        public string asTreeviewStructObjID
        {
            get
            {
                return this.asTreeviewStructEdit.GetClientTreeObjectId();
            }
        }
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructTypeEdit.GetClientTreeObjectId();
            }
        }

        public string ProjectSysno
        {
            get
            {

                return Request["projsysno"];
            }

        }
        public string ProName { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ModifyProjectAuditBymaster));

            //绑定权限
            BindPreviewPower();
            //绑定项目类型
            BindProjType();
            //行业性质
            BindCorpHyxz();
            //工程来源
            BindDdList();
            //合同阶段
            BindCorpProc();
            //设置样式
            SetDropDownTreeThem();
            //绑定建筑结构样式修改时
            BindStructTypeEdit();
            //绑定建筑分类修改时
            BindBuildStuctTypeEdit();
            //设计等级
            BindBuildType();
            GetProInfo(ProjectSysno);
        }
        //合同建筑类别
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
            this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "ID";
            this.drp_buildtype.DataBind();
        }
        //获取页面权限
        public int GetPreviewPower()
        {
            int UserSysNo = int.Parse(GetCurMemID());
            string PageName = "cpr_CorperationList.aspx";
            List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, PageName);

            //浏览权限
            int Power = 0;
            if ((from role in rolePowerParameterList where role.PreviewPattern == "2" select role).Count() > 0)
            {
                //部门
                Power = 2;
            }
            if ((from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0)
            {
                //全部
                Power = 1;
            }
            return Power;
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;
                //string NotShowUnitList = base.NotShowUnitList;

                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
                //this.NotShowUnitList.Value = NotShowUnitList;
            }
        }

        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz'";
            this.ddProfessionTypeEdit.DataSource = bll_dic.GetList(str_where);
            this.ddProfessionTypeEdit.DataTextField = "dic_Name";
            this.ddProfessionTypeEdit.DataValueField = "ID";
            this.ddProfessionTypeEdit.DataBind();
        }

        //绑定项目类型
        protected void BindProjType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_kind'";
            this.drp_projtype.DataSource = bll_dic.GetList(str_where);
            this.drp_projtype.DataTextField = "dic_Name";
            this.drp_projtype.DataValueField = "dic_Name";
            this.drp_projtype.DataBind();
        }

        //项目来源
        public void BindDdList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定项目来源
            DataSet dic_proly = dic.GetList(" dic_type='cpr_src'");
            ddsourceEdit.DataValueField = "id";
            ddsourceEdit.DataTextField = "dic_name";
            ddsourceEdit.DataSource = dic_proly;
            ddsourceEdit.DataBind();
            ddsourceEdit.Items.Insert(0, new ListItem("---请选择项目来源---", "0"));
        }
        //工程来源

        //合同阶段
        protected void BindCorpProc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_jd'";
            this.chk_cprjd.DataSource = bll_dic.GetList(str_where);
            this.chk_cprjd.DataTextField = "dic_Name";
            this.chk_cprjd.DataValueField = "ID";
            this.chk_cprjd.DataBind();
        }
        protected void IsEnabledCheckBox(CheckBoxList cheklist, string text)
        {
            if (cheklist != null)
            {
                if (cheklist.Items.FindByText(text) != null)
                {
                    ListItem chk = cheklist.Items.FindByText(text);
                    chk.Enabled = false;
                    chk.Selected = true;
                }

            }
        }
        public void GetProInfo(string strProId)
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(int.Parse(strProId));
            if (pro_model != null)
            {

                ProName = pro_model.pro_name == null ? "" : pro_model.pro_name.Trim();
                //项目名称
                this.txt_name.Text = pro_model.pro_name == null ? "" : pro_model.pro_name.Trim();
                //关联合同
                this.txt_reletiveB.Text = pro_model.Project_reletive == null ? "" : pro_model.Project_reletive.Trim();
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.radio_yuan.Checked = true;
                    this.radio_suo.Disabled = true;
                }
                else if (level.Trim() == "1")
                {
                    this.radio_suo.Checked = true;
                    this.radio_yuan.Disabled = true;
                }
                //建筑级别
                //string BuildType = pro_model.BuildType.Trim();
                //this.drp_buildtype.Text = BuildType;
                //建设单位
                this.txtbuildUnit.Text = pro_model.pro_buildUnit == null ? "" : pro_model.pro_buildUnit.Trim();
                //承接部门
                this.txt_unit.Text = pro_model.Unit == null ? "" : pro_model.Unit.Trim();
                //建设地点
                this.txtbuildAddress.Text = pro_model.BuildAddress == null ? "" : pro_model.BuildAddress.Trim();
                //建筑面积
                this.txt_scale.Text = pro_model.ProjectScale == null ? "" : pro_model.ProjectScale.ToString();
                //审核级别
                string sh = string.IsNullOrEmpty(pro_model.AuditLevel) == true ? "" : pro_model.AuditLevel.ToString().Trim();

                if (sh == "1,0")
                {
                    this.audit_yuan.Checked = true;
                    this.audit_suo.Checked = false;
                    this.audit_suo.Disabled = true;
                }
                else if (sh == "0,1")
                {
                    this.audit_suo.Checked = true;
                    this.audit_yuan.Checked = false;
                    this.audit_yuan.Disabled = true;
                }
                else if (sh == "1,1")
                {
                    this.audit_suo.Checked = true;
                    this.audit_yuan.Checked = true;
                }

                //项目结构形式
                string StrStruct = pro_model.pro_StruType;
                if (!string.IsNullOrEmpty(StrStruct))
                {
                    //LoadStructData(StrStruct, this.asTreeviewStruct);
                    this.lbl_StructType.Text = TG.Common.StringPlus.ResolveStructString(pro_model.pro_StruType.ToString());
                }
                //建筑分类
                string BuildStructType = pro_model.pro_kinds;
                if (!string.IsNullOrEmpty(BuildStructType))
                {
                    //LoadStructData(BuildStructType, this.asTreeviewStructType);
                    this.lbl_BuildStructType.Text = TG.Common.StringPlus.ResolveStructString(pro_model.pro_kinds.ToString());
                }
                string BuildType = pro_model.BuildType.ToString();
                if (!string.IsNullOrEmpty(BuildType))
                {
                    this.lbl_BuildTypeLevel.Text = pro_model.BuildType.ToString().Trim().Replace('+', ',');
                    //LoadStructData(BuildType, this.asTreeViewBuildType);
                    //this.buildStructType.Text = TG.Common.StringPlus.ResolveStructString(BuildStructType);
                }
                //显示项目阶段
                string str_jd = pro_model.pro_status;
                string[] prostatus = pro_model.pro_status.Split(new char[] { ',' }, StringSplitOptions.None);
                //阶段信息
                string str_status = "";
                //最后阶段
                string str_endstatus = "";
                for (int i = 0; i < prostatus.Length - 1; i++)
                {
                    str_status = prostatus[i].Trim();

                    if (str_status == "方案设计" || str_status == "方案")
                    {
                        CheckBox1.Checked = true;
                        str_endstatus = "方案";
                    }
                    if (str_status == "初步设计" || str_status == "初设")
                    {
                        CheckBox2.Checked = true;
                        str_endstatus = "初设";
                    }
                    if (str_status == "施工图设计" || str_status == "施工图")
                    {
                        CheckBox3.Checked = true;
                        str_endstatus = "施工图";
                    }
                    if (str_status == "其他" || str_status == "其他")
                    {
                        CheckBox4.Checked = true;
                        str_endstatus = "其他";
                    }
                    //让文本框可选    
                    IsEnabledCheckBox(this.chk_cprjd, str_endstatus);
                }

                CheckBox1.Enabled = false;
                CheckBox2.Enabled = false;
                CheckBox3.Enabled = false;
                CheckBox4.Enabled = false;
                //项目来源
                string pro_src = pro_model.Pro_src.ToString();
                this.ddsource.Text = GetProjectSrc(pro_src);
                //合同额
                this.txtproAcount.Text = pro_model.Cpr_Acount == null ? "" : pro_model.Cpr_Acount.ToString();
                //行业性质
                string industry = pro_model.Industry == null ? "" : pro_model.Industry.Trim();
                this.ddProfessionType.Text = industry;
                //甲方
                this.txt_Aperson.Text = pro_model.ChgJia == null ? "" : pro_model.ChgJia.Trim();
                this.txt_phone.Text = pro_model.Phone == null ? "" : pro_model.Phone.Trim();
                //项目经理
                this.txt_PMName.Text = pro_model.PMName;
                this.txt_PMPhone.Text = pro_model.PMPhone == null ? "" : pro_model.PMPhone.Trim();
                //开始结束日期
                this.txt_startdate.Text = Convert.ToDateTime(pro_model.pro_startTime).ToString("yyyy-MM-dd");
                this.txt_finishdate.Text = Convert.ToDateTime(pro_model.pro_finishTime).ToString("yyyy-MM-dd");
                //项目特征概况
                this.txt_sub.Text = pro_model.ProjSub == null ? "" : pro_model.ProjSub.Trim();
                this.txt_sub.Enabled = false;
                this.txt_remark.Text = pro_model.pro_Intro == null ? "" : pro_model.pro_Intro.Trim();
                this.txt_remark.Enabled = false;
                //项目类型
                this.txt_projtype.Text = pro_model.pro_StruType == null ? "" : pro_model.pro_StruType.Trim();
                //规划面积
                this.txt_area.Text = pro_model.ProjectScale == null ? "0.00" : pro_model.ProjectScale.ToString();
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }
            }
        }
        //获取项目来源
        protected string GetProjectSrc(string id)
        {
            TG.Model.cm_Dictionary model = new TG.BLL.cm_Dictionary().GetModel(int.Parse(id));
            if (model != null)
            {
                return model.dic_Name;
            }
            else
            {
                return "";
            }
        }
        //建筑分类
        protected void BindBuildStuctTypeEdit()
        {
            BindProInfoConfig("BuildType", this.asTreeviewStructTypeEdit.RootNode);
            this.asTreeviewStructTypeEdit.CollapseAll();
        }
        //结构形式
        protected void BindStructTypeEdit()
        {
            BindProInfoConfig("StructType", this.asTreeviewStructEdit.RootNode);
            this.asTreeviewStructEdit.CollapseAll();
        }
        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }
        /// <summary>
        /// 绑定树列表  qpl 20140108 
        /// </summary>
        /// <param name="infokey"></param>
        /// <param name="rootnode"></param>
        /// <param name="flagrootbox">根节点是否显示CheckBox</param>
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode, bool showsubbox)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root, showsubbox);
            }
        }
        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            //this.asTreeviewStruct.Theme = macOS;
            //this.asTreeviewStructType.Theme = macOS;
            //this.asTreeViewBuildType.Theme = macOS;
            this.asTreeviewStructEdit.Theme = macOS;
            this.asTreeviewStructTypeEdit.Theme = macOS;

        }
        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        /// <summary>
        /// 绑定树列表  qpl 20140108 
        /// </summary>
        /// <param name="xmlroot"></param>
        /// <param name="root"></param>
        /// <param name="flagrootbox">根节点是否显示CheckBox</param>
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root, bool showsubbox)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                //标示子节点是否显示CheckBox qpl 20140108
                switch (name)
                {
                    case "特级":
                    case "一级":
                    case "二级":
                    case "三级":
                        linknode.EnableCheckbox = !showsubbox;
                        break;
                    default:
                        linknode.EnableCheckbox = showsubbox;
                        break;
                }

                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);

                        //子节点的checkbox 不可用  qpl 20140108
                        sublinknode.EnableCheckbox = showsubbox;

                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode, showsubbox);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        //获取所有树文字数据
        protected void GetAllNodeTex(ASTreeViewNode nodes, ref List<string> alltext)
        {
            foreach (ASTreeViewNode node in nodes.ChildNodes)
            {
                alltext.Add(node.NodeText);
                if (node.ChildNodes.Count > 0)
                {
                    GetAllNodeTex(node, ref alltext);
                }
            }
        }
        [AjaxMethod]
        protected void output_Click(object sender, EventArgs e)
        {
            GetDropDownTreeCheckedValue();
        }
        [AjaxMethod]
        public string GetDropDownTreeCheckedValue()
        {
            string rootvalue = "";

            List<ASTreeViewNode> allnodes = this.asTreeviewStructEdit.RootNode.ChildNodes;
            //最终生成字符串

            foreach (ASTreeViewNode node in allnodes)
            {
                string secondvalue = "";
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                    GetSecondNodeList(node, ref secondvalue);
                }
                rootvalue += secondvalue;
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }
            return rootvalue;
        }
        protected void GetSecondNodeList(ASTreeViewNode node, ref string value)
        {
            if (node.ChildNodes.Count > 0)
            {
                //返回值
                StringBuilder sbresult = new StringBuilder();

                foreach (ASTreeViewNode snode in node.ChildNodes)
                {
                    if (snode.CheckedState == ASTreeViewCheckboxState.Checked || snode.CheckedState == ASTreeViewCheckboxState.HalfChecked)
                    {
                        //拼接第二级
                        value = "^" + snode.NodeValue;
                        string subvalue = "";
                        subvalue = value;
                        GetChildNodes(snode, ref subvalue);
                        foreach (string key in sblist)
                        {
                            sbresult.Append(key);
                        }
                        //清空当前列表
                        sblist.Clear();
                    }
                }
                value = sbresult.ToString();
            }
        }
        //查询数据
        List<string> sblist = new List<string>();
        protected void GetChildNodes(ASTreeViewNode node, ref string value)
        {
            StringBuilder sb = new StringBuilder();
            if (node.ChildNodes.Count > 0)
            {
                foreach (ASTreeViewNode childnode in node.ChildNodes)
                {
                    if ((childnode.CheckedState == ASTreeViewCheckboxState.Checked) || (childnode.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                    {
                        string tempvalue = value + "*" + childnode.NodeValue;

                        if (childnode.ChildNodes.Count > 0)
                        {
                            //如果还有子节点，继续遍历
                            GetChildNodes(childnode, ref tempvalue);
                        }
                        else
                        {
                            //添加末节点
                            sb.Append(tempvalue);
                        }
                    }
                }
            }
            else
            {
                //添加末节点
                sb.Append(value);
            }
            //赋值value
            sblist.Add(sb.ToString());
        }
        [AjaxPro.AjaxMethod]
        public string StartAppProject(string query, string flag)
        {
            string result = "0";
            if (flag == "0")
            {
                result = GetNextProcessRoleUser();
            }
            else
            {
                ProjectAduitEditParamEntity param = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectAduitEditParamEntity>(query);
                //实例一个审核实体
                ProjectAuditEditEntity dataEntity = new ProjectAuditEditEntity
                {
                    InUser = UserSysNo,
                    options = param.copoption,
                    ProjectSysNo = param.ProjectSysNo


                };
                //新建一条审批记录
                int identitySysNo = tg_ProjectAuditBP.InsertProjectAuditEdit(dataEntity);

                if (identitySysNo > 0)
                {
                    //获取项目配置审批实体
                    ProjectAuditConfigViewEntity projectAuditConfigViewEntity = new ProjectAuditConfigBP().GetProjectRoleSysNoByPoisition(1);

                    //声明消息实体
                    SysMessageViewEntity sysMessageDataEntity = new SysMessageViewEntity
                    {
                        //ReferenceSysNo = identitySysNo.ToString(),
                        ReferenceSysNo = string.Format("projectAuditSysNo={0}&MessageStatus={1}", identitySysNo.ToString(), "A"),
                        FromUser = UserSysNo,
                        MsgType = 11,
                        InUser = UserSysNo,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", param.ProjectName, "项目申请修改"),
                        QueryCondition = param.ProjectName,
                        IsDone = "A",
                        Status = "A"
                    };
                    //声明消息实体
                    string sysMsgString = string.Empty;
                    //第一审批阶段人员iD
                    sysMessageDataEntity.ToRole = projectAuditConfigViewEntity.RoleSysNo.ToString();
                    //获取审批人列表人名
                    sysMsgString = CommonAudit.GetMessagEntity(sysMessageDataEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        result = sysMsgString;
                    }
                }
                else
                {
                    result = "0";
                }
            }

            return result;
        }

        //返回下一阶段用户列表
        private string GetNextProcessRoleUser()
        {
            string result = "";
            //获取项目配置审批实体
            ProjectAuditConfigViewEntity projectAuditConfigViewEntity = new ProjectAuditConfigBP().GetProjectRoleSysNoByPoisition(1);
            //得到审批用户的实体
            string roleUserString = CommonAudit.GetRoleName(projectAuditConfigViewEntity.RoleSysNo);
            if (!string.IsNullOrEmpty(roleUserString))
            {
                result = roleUserString;
            }
            else
            {
                result = "0";
            }

            return result;
        }
    }
}