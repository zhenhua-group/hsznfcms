﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;
using Aspose.Words;
using Aspose.Words.Drawing;
using TG.Model;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace TG.Web.ProjectManage
{

    public partial class ShowProjectBymaster : System.Web.UI.Page
    {
        TG.BLL.cm_ProjectValueAllot bll = new TG.BLL.cm_ProjectValueAllot();

        TG.BLL.cm_Project proBll = new TG.BLL.cm_Project();
        public List<ProjectPlanRole> ProjectPlanRoleList { get; set; }

        public ProjectDesignPlanRole ProjectDesignPlanRoleList { get; set; }
        string str_proid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                str_proid = Request.QueryString["pro_id"].ToString();
            }
            catch (Exception)
            {
                Response.Redirect("ProjectList.aspx");
                throw;
            }
            if (!IsPostBack)
            {
                this.hid_projid.Value = str_proid;
                GetProInfo(str_proid);
                GetProPlanInfo(str_proid);
                GetProScheduleInfo(str_proid);
                GetProPagebackInfo(str_proid);
                GetProValueAndAllot(str_proid);
            }

        }
        //分配信息
        private void GetProValueAndAllot(string str_proid)
        {
            //项目信息
            cm_Project proModel = proBll.GetModel(int.Parse(str_proid));
            //判断经济所项目或暖通项目或生产部门项目
            if (proModel.Unit.Contains("经济"))
            {
                DataSet ds = bll.GetJjsProjectValueRecordViewByPro(str_proid.ToString(), "null");
                if (ds != null)
                {
                    this.Literal3.Text = "";
                    gv_ProjectValueJ.DataSource = ds.Tables[0];
                    gv_ProjectValueJ.DataBind();
                }

            }
            else if (proModel.Unit.Contains("暖通"))
            {
                DataSet ds = bll.GetHavcProjectValueRecordViewByPro(str_proid.ToString(), "null");
                if (ds != null)
                {
                    this.Literal3.Text = "";
                    gv_ProjectValueH.DataSource = ds.Tables[0];
                    gv_ProjectValueH.DataBind();
                }

            }
            else
            {
                DataSet ds = bll.GetProjectValueRecordViewByPro(str_proid.ToString(), "null");
                if (ds != null)
                {
                    this.Literal3.Text = "";
                    gv_ProjectValue.DataSource = ds.Tables[0];
                    gv_ProjectValue.DataBind();
                }

            }
            //throw new NotImplementedException();
        }
        //图纸信息
        private void GetProPagebackInfo(string str_proid)
        {
            cm_Project proModel = proBll.GetModel(int.Parse(str_proid));
            if (proModel.ReferenceSysNo != 0)
            {
                this.PagebackInfo.Text = "";
                this.grd_pack.DataSource = DBUtility.DbHelperSQL.RunProcedure("P_cm_Pageback", new SqlParameter[] { new SqlParameter("@query", proModel.ReferenceSysNo.ToString()), new SqlParameter("@queryName", proModel.pro_name.ToString().Trim()) });
                this.grd_pack.DataBind();
            }
        }
        //获取计划进度
        private void GetProScheduleInfo(string str_proid)
        {
            List<ProjectPlanSubItem> subList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanSubitemList(int.Parse(str_proid));
            if (subList.Count != 0)
            {
                this.ScheduleInfo.Text = "";
                this.grid_mem.DataSource = subList;
                this.grid_mem.DataBind();
            }
        }
        protected void grid_mem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //
        }
        // 策划信息
        private void GetProPlanInfo(string str_proid)
        {
            //策划人员
            ProjectPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanRoleAndUsers(int.Parse(str_proid));
            //设计人员
            ProjectDesignPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanDesignRoleAndUsers(int.Parse(str_proid));
            if (ProjectDesignPlanRoleList.Users.Count != 0)
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("<table class=\"table table-bordered table-hover\" style=\"width: 98%;\" align=\"center\">");
                builder.Append("<tr><th width=\"20%\">角色</th><th width=\"80%\">人员和专业</th></tr>");
                builder.Append("<tr><td>项目总负责:</td><td>");
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 1; }).Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("<tr><td>助理:</td><td>");
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 6; }).Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("<tr><td>专业负责人:</td><td>");
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 2; }).Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("<tr><td>校对人:</td><td>");
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 3; }).Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("<tr><td>审核人:</td><td>");
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 4; }).Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("<tr><td>审定人:</td><td>");
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 5; }).Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("<tr><td>设计人:</td><td>");
                ProjectDesignPlanRoleList.Users.ForEach((user) =>
                {
                    builder.Append("<span style=\"margin-left:5px;\" userSysNo=\"" + user.UserSysNo + "\" name=\"userSpan\">" + user.UserName + "[" + user.SpecialtyName + "]</span>");
                });
                builder.Append("</td></tr>");
                builder.Append("</table>");
                this.PlanInfo.Text = builder.ToString();
            }
            //throw new NotImplementedException();
        }

        protected void btn_Output_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_Project bll = new TG.BLL.cm_Project();
            string cprid = Request.QueryString["pro_id"];
            TG.Model.cm_Project model = bll.GetModel(Convert.ToInt32(cprid));
            string tmppath = Server.MapPath("~/TemplateWord/process.doc");
            Aspose.Words.Document doc = new Aspose.Words.Document(tmppath); //载入模板
            DocumentBuilder builder = new DocumentBuilder(doc);

            #region aspose.words使用模板导出word
            if (doc.Range.Bookmarks["Image"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Image"];


                string dataurl = GetThumdPicByProjid(cprid);

                if (dataurl.Length > 0 && dataurl.Contains("|"))
                {
                    for (int i = 0; i < dataurl.Split('|').Length; i++)
                    {
                        Shape shape = new Shape(doc, ShapeType.Image);
                        string url = "../Attach_User/filedata/projfile/" + dataurl.Split('|')[i].Split('/')[0] + "/min/" + dataurl.Split('|')[i].Split('/')[1];
                        url = Server.MapPath(url);
                        shape.ImageData.SetImage(url);
                        shape.Width = 70;
                        shape.Height = 70;
                        // shape.HorizontalAlignment = HorizontalAlignment.Center; //靠右对齐

                        CompositeNode node = shape.ParentNode;
                        //把此图片移动到那个单元格中
                        builder.MoveToCell(0, 14, 1, 0);
                        builder.InsertNode(shape);
                    }

                }
                else if (dataurl.Length > 0 && !dataurl.Contains("|"))
                {
                    Shape shape = new Shape(doc, ShapeType.Image);
                    string url = "../Attach_User/filedata/projfile/" + dataurl.Split('/')[0] + "/min/" + dataurl.Split('/')[1];
                    url = Server.MapPath(url);
                    shape.ImageData.SetImage(url);
                    shape.Width = 70;
                    shape.Height = 70;
                    //shape.HorizontalAlignment = HorizontalAlignment.Center; //靠右对齐
                    CompositeNode node = shape.ParentNode;
                    //把此图片移动到那个单元格中
                    builder.MoveToCell(0, 14, 1, 0);
                    builder.InsertNode(shape);
                }
                else
                {
                    Shape shape = new Shape(doc, ShapeType.Image);
                    string url = "../Attach_User/filedata/tempimg/tempimg.jpg";
                    url = Server.MapPath(url);
                    shape.ImageData.SetImage(url);
                    shape.Width = 100;
                    shape.Height = 100;
                    // shape.HorizontalAlignment = HorizontalAlignment.Center; //靠右对齐

                    CompositeNode node = shape.ParentNode;
                    //把此图片移动到那个单元格中
                    builder.MoveToCell(0, 14, 1, 0);
                    builder.InsertNode(shape);
                }

            }
            if (doc.Range.Bookmarks["BuildAddress"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildAddress"];
                mark.Text = model.BuildAddress == null ? "" : model.BuildAddress;
            }
            //经济所



            if (doc.Range.Bookmarks["JJS"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["JJS"];
                string jjs = string.IsNullOrEmpty(model.ISTrunEconomy) == true ? "" : model.ISTrunEconomy.Trim();
                if (jjs == "0")
                {
                    jjs = "否";
                }
                else if (jjs == "1")
                {
                    jjs = "是";
                }
                mark.Text = jjs;
            }
            if (doc.Range.Bookmarks["NTS"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["NTS"];
                //暖通
                string nt = string.IsNullOrEmpty(model.ISHvac) == true ? "" : model.ISHvac.Trim();
                if (nt == "0")
                {
                    nt = "否";
                }
                else if (nt == "1")
                {
                    nt = "是";
                }
                mark.Text = nt;
            }
            if (doc.Range.Bookmarks["LB"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["LB"];
                mark.Text = model.BuildType == null ? "" : model.BuildType;
            }

            if (doc.Range.Bookmarks["ChgJia"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgJia"];
                mark.Text = model.ChgJia == null ? "" : model.ChgJia;
            }

            if (doc.Range.Bookmarks["Cpr_Acount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Cpr_Acount"];
                mark.Text = model.Cpr_Acount.ToString() + "万元";
            }

            if (doc.Range.Bookmarks["Industry"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Industry"];
                mark.Text = model.Industry == null ? "" : model.Industry;
            }

            if (doc.Range.Bookmarks["Phone"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Phone"];
                mark.Text = model.Phone == null ? "" : model.Phone;
            }

            if (doc.Range.Bookmarks["PMName"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["PMName"];
                mark.Text = model.PMName == null ? "" : model.PMName;
            }

            if (doc.Range.Bookmarks["PMPhone"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["PMPhone"];
                mark.Text = model.PMPhone == null ? "" : model.PMPhone;
            }

            if (doc.Range.Bookmarks["pro_buildUnit"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["pro_buildUnit"];
                mark.Text = model.pro_buildUnit == null ? "" : model.pro_buildUnit;
            }

            if (doc.Range.Bookmarks["pro_finishTime"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["pro_finishTime"];
                mark.Text = CommCoperation.GetEasyTime(Convert.ToDateTime(model.pro_finishTime));
            }
            if (doc.Range.Bookmarks["pro_Intro"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["pro_Intro"];
                mark.Text = model.pro_Intro == null ? "" : model.pro_Intro;
            }
            if (doc.Range.Bookmarks["pro_kinds"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["pro_kinds"];
                mark.Text = model.pro_kinds == null ? "" : model.pro_kinds;
            }

            if (doc.Range.Bookmarks["pro_level"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["pro_level"];
                mark.Text = model.pro_level.ToString() == "0" ? "院管" : "所管";
            }

            if (doc.Range.Bookmarks["pro_name"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["pro_name"];
                mark.Text = model.pro_name == null ? "" : model.pro_name;
            }

            if (doc.Range.Bookmarks["Pro_src"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Pro_src"];
                mark.Text = GetProfession(model.Pro_src.ToString());
            }

            if (doc.Range.Bookmarks["pro_startTime"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["pro_startTime"];
                mark.Text = CommCoperation.GetEasyTime(Convert.ToDateTime(model.pro_startTime));
            }

            if (doc.Range.Bookmarks["pro_status"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["pro_status"];
                mark.Text = model.pro_status == null ? "" : model.pro_status;
            }

            if (doc.Range.Bookmarks["pro_StruType"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["pro_StruType"];
                mark.Text = model.pro_StruType == null ? "" : model.pro_StruType;
            }
            if (doc.Range.Bookmarks["Project_reletive"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Project_reletive"];
                mark.Text = model.Project_reletive == null ? "" : model.Project_reletive;
            }
            if (doc.Range.Bookmarks["ProjectScale"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ProjectScale"];
                mark.Text = model.ProjectScale.ToString() + "平米";
            }

            if (doc.Range.Bookmarks["ProjSub"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ProjSub"];
                mark.Text = model.ProjSub == null ? "" : model.ProjSub;
            }
            if (doc.Range.Bookmarks["CJBM"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["CJBM"];
                mark.Text = model.Unit == null ? "" : model.Unit;
            }
            if (doc.Range.Bookmarks["Audit"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Audit"];
                string sh = string.IsNullOrEmpty(model.AuditLevel) == true ? "" : model.AuditLevel.ToString().Trim();
                if (sh == "0")
                {
                    mark.Text = "院审";
                }
                else if (sh == "1")
                {
                    mark.Text = "所审";
                }

            }
            if (doc.Range.Bookmarks["Unit"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Unit"];
                mark.Text = model.pro_buildUnit == null ? "" : model.pro_buildUnit;
            }
            doc.Save(model.pro_name + ".doc", SaveFormat.Doc, SaveType.OpenInWord, Response);  //保存为doc，并打开
            #endregion
            //XWPFDocument doc = null;

            //string modelPath = "~/TemplateXls/33.doc";
            //using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            //{
            //    doc = new XWPFDocument(fileStream);
            //    fileStream.Close();
            //}

            //NPOI.OpenXmlFormats.Wordprocessing.CT_Tbl tbl = new NPOI.OpenXmlFormats.Wordprocessing.CT_Tbl();
            //XWPFTable modeltable = null;
            //IList<XWPFTable> table = doc.GetTables();
            //foreach (XWPFTable item in table)
            //{
            //    modeltable = item;
            //}
            //modeltable.GetRow(0).GetCell(0).SetText("第一行，第一列");
            //modeltable.GetRow(1).GetCell(1).SetText("EXAMPLE OF TABLE");
            //modeltable.GetRow(2).GetCell(2).SetText("选合适减得分");
            //string se = modeltable.GetRow(0).GetCell(0).GetText();
            //using (MemoryStream memoryStream = new MemoryStream())
            //{

            //    doc.Write(memoryStream);

            //    string name = System.Web.HttpContext.Current.Server.UrlEncode("年（月)【已签订合同额】与年同期对比表");
            //    Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
            //    Response.ContentType = "application/msword;charset=UTF-8";
            //    Response.BinaryWrite(memoryStream.ToArray());
            //    Response.ContentEncoding = Encoding.UTF8;
            //    doc = null;
            //    Response.End();
            //}
            //doc.Write(out1);
        }

        public void GetProInfo(string strProId)
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(int.Parse(strProId));
            //项目名称
            this.txt_name.Text = pro_model.pro_name == null ? "" : pro_model.pro_name.Trim();
            //关联合同
            this.txt_reletive.Text = pro_model.Project_reletive == null ? "" : pro_model.Project_reletive.Trim();
            //管理级别
            string level = pro_model.pro_level.ToString();
            if (level.Trim() == "0")
            {
                this.lbl_level.Text = "院管";
            }
            else
            {
                this.lbl_level.Text = "所管";
            }
            //建筑级别
            string BuildType = pro_model.BuildType.Trim();
            this.drp_buildtype.Text = BuildType;
            //审核级别
            string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
            if (auditlevel.Trim() == "1,0")
            {
                this.lbl_auditlevel.Text = "院审";
            }
            else if (auditlevel.Trim() == "0,1")
            {
                this.lbl_auditlevel.Text = "所审";
            }
            else if (auditlevel.Trim() == "1,1")
            {
                this.lbl_auditlevel.Text = "院审,所审";
            }
            //建设单位
            this.txtbuildUnit.Text = pro_model.pro_buildUnit == null ? "" : pro_model.pro_buildUnit.Trim();
            //承接部门
            this.txt_unit.Text = pro_model.Unit == null ? "" : pro_model.Unit.Trim();
            //建设地点
            this.txtbuildAddress.Text = pro_model.BuildAddress == null ? "" : pro_model.BuildAddress.Trim();
            //建设规模
            this.txt_scale.Text = pro_model.ProjectScale == null ? "" : pro_model.ProjectScale.ToString();
            //项目结构形式
            string StrStruct = pro_model.pro_StruType;
            if (!string.IsNullOrEmpty(StrStruct))
            {
                this.structType.Text = TG.Common.StringPlus.ResolveStructString(StrStruct);
            }
            //建筑分类
            string BuildStructType = pro_model.pro_kinds;
            if (!string.IsNullOrEmpty(BuildStructType))
            {
                this.buildStructType.Text = TG.Common.StringPlus.ResolveStructString(BuildStructType);
            }
            //显示项目阶段
            string str_jd = pro_model.pro_status;
            if (str_jd.IndexOf(',') > -1)
            {
                this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
            }
            //项目来源
            string pro_src = pro_model.Pro_src.ToString();
            this.ddsource.Text = GetProjectSrc(pro_src);
            //合同额
            this.txtproAcount.Text = pro_model.Cpr_Acount == null ? "" : pro_model.Cpr_Acount.ToString();
            //行业性质
            string industry = pro_model.Industry == null ? "" : pro_model.Industry.Trim();
            this.ddProfessionType.Text = industry;
            //甲方
            this.txt_Aperson.Text = pro_model.ChgJia == null ? "" : pro_model.ChgJia.Trim();
            this.txt_phone.Text = pro_model.Phone == null ? "" : pro_model.Phone.Trim();
            //项目总负责
            this.txt_PMName.Text = pro_model.PMName;
            this.txt_PMPhone.Text = pro_model.PMPhone == null ? "" : pro_model.PMPhone.Trim();
            //开始结束日期
            this.txt_startdate.Text = Convert.ToDateTime(pro_model.pro_startTime).ToString("yyyy-MM-dd");
            this.txt_finishdate.Text = Convert.ToDateTime(pro_model.pro_finishTime).ToString("yyyy-MM-dd");
            //经济所
            string isotherprt = "";
            if (pro_model.ISTrunEconomy == "1")
            {
                isotherprt += "经济所,";
            }
            //暖通
            if (pro_model.ISHvac == "1")
            {
                isotherprt += "暖通热力所,";
            }
            //土建所
            if (pro_model.ISArch == "1")
            {
                isotherprt += "土建所,";
            }
            if (isotherprt.IndexOf(',') > -1)
            {
                this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
            }
            else
            {
                this.lbl_isotherprt.Text = "无";
            }
            //项目特征概况
            string projSub = pro_model.ProjSub == null ? "" : pro_model.ProjSub.Trim();
            this.txt_sub.InnerHtml = TG.Common.StringPlus.HtmlEncode(projSub);
            //项目备注
            string projInfo = pro_model.pro_Intro == null ? "" : pro_model.pro_Intro.Trim();
            this.txt_remark.InnerHtml = TG.Common.StringPlus.HtmlEncode(projInfo);
        }
        //获取项目来源
        protected string GetProjectSrc(string id)
        {
            TG.Model.cm_Dictionary model = new TG.BLL.cm_Dictionary().GetModel(int.Parse(id));
            if (model != null)
            {
                return model.dic_Name;
            }
            else
            {
                return "";
            }
        }
        //通过项目ID 得到效果图
        protected string GetThumdPicByProjid(string projid)
        {
            TG.BLL.cm_AttachInfo bll = new TG.BLL.cm_AttachInfo();
            string strWhere = " Proj_id=" + projid + " AND OwnType='projt'";
            List<TG.Model.cm_AttachInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                return models[models.Count - 1].FileUrl.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        protected string GetProfession(string num)
        {
            string result = "";
            switch (num)
            {
                case "-1":
                    result = "";
                    break;
                case "27":
                    result = "方案";
                    break;
                case "28":
                    result = "初设";
                    break;
                case "29":
                    result = "施工图";
                    break;
                case "30":
                    result = "其他";
                    break;
                case "31":
                    result = "公开招标";
                    break;
                case "32":
                    result = "邀请招标";
                    break;
                case "36":
                    result = "计算机行业";
                    break;
                case "33":
                    result = "自行委托";
                    break;
                case "37":
                    result = "教育行业";
                    break;
                case "38":
                    result = "建筑行业";
                    break;
                case "47":
                    result = "科教行业";
                    break;
                case "34":
                    result = "普通客户";
                    break;
                case "35":
                    result = "VIP客户";
                    break;
                case "39":
                    result = "一般";
                    break;
                case "40":
                    result = "密切";
                    break;
                case "41":
                    result = "很密切";
                    break;
                case "43":
                    result = "一级";
                    break;
                case "44":
                    result = "二级";
                    break;
                case "45":
                    result = "三级";
                    break;
                case "46":
                    result = "四级";
                    break;
                default:
                    result = "";
                    break;
            }
            return result;
        }
    }
}