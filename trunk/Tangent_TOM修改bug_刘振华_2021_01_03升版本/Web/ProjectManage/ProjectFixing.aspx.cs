﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;
using Aspose.Words;
using Aspose.Words.Drawing;
using System.Configuration;

namespace TG.Web.ProjectManage
{
    public partial class ProjectFixing : PageBase
    {
        //项目编号
        protected string ProjectSysNo
        {
            get
            {
                return Request["pro_id"] ?? "0";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.hid_projid.Value = ProjectSysNo;
                GetProInfo(ProjectSysNo);
            }
        }
        //获取项目信息
        public void GetProInfo(string strProId)
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(int.Parse(strProId));
            //项目名称
            this.txt_name.Text = pro_model.pro_name == null ? "" : pro_model.pro_name.Trim();
            //关联合同
            this.txt_reletive.Text = pro_model.Project_reletive == null ? "" : pro_model.Project_reletive.Trim();
            //管理级别
            string level = pro_model.pro_level.ToString();
            if (level.Trim() == "0")
            {
                this.lbl_level.Text = "院管";
            }
            else
            {
                this.lbl_level.Text = "所管";
            }
            //建筑级别
            string BuildType = pro_model.BuildType.Trim();
            this.drp_buildtype.Text = BuildType;
            //审核级别
            string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
            if (auditlevel.Trim() == "1,0")
            {
                this.lbl_auditlevel.Text = "院审";
            }
            else if (auditlevel.Trim() == "0,1")
            {
                this.lbl_auditlevel.Text = "所审";
            }
            else if (auditlevel.Trim() == "1,1")
            {
                this.lbl_auditlevel.Text = "院审,所审";
            }
            //建设单位
            this.txtbuildUnit.Text = pro_model.pro_buildUnit == null ? "" : pro_model.pro_buildUnit.Trim();
            //承接部门
            this.txt_unit.Text = pro_model.Unit == null ? "" : pro_model.Unit.Trim();
            //建设地点
            this.txtbuildAddress.Text = pro_model.BuildAddress == null ? "" : pro_model.BuildAddress.Trim();
            //建设规模
            this.txt_scale.Text = pro_model.ProjectScale == null ? "" : pro_model.ProjectScale.ToString();
            //项目结构形式
            string StrStruct = pro_model.pro_StruType;
            if (!string.IsNullOrEmpty(StrStruct))
            {
                this.structType.Text = TG.Common.StringPlus.ResolveStructString(StrStruct);
            }
            //建筑分类
            string BuildStructType = pro_model.pro_kinds;
            if (!string.IsNullOrEmpty(BuildStructType))
            {
                this.buildStructType.Text = TG.Common.StringPlus.ResolveStructString(BuildStructType);
            }
            //显示项目阶段
            string str_jd = pro_model.pro_status;
            if (str_jd.IndexOf(',') > -1)
            {
                this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
            }
            //项目来源
            string pro_src = pro_model.Pro_src.ToString();
            this.ddsource.Text = GetProjectSrc(pro_src);
            //合同额
            this.txtproAcount.Text = pro_model.Cpr_Acount == null ? "" : pro_model.Cpr_Acount.ToString();
            //行业性质
            string industry = pro_model.Industry == null ? "" : pro_model.Industry.Trim();
            this.ddProfessionType.Text = industry;
            //甲方
            this.txt_Aperson.Text = pro_model.ChgJia == null ? "" : pro_model.ChgJia.Trim();
            this.txt_phone.Text = pro_model.Phone == null ? "" : pro_model.Phone.Trim();
            //项目总负责
            this.txt_PMName.Text = pro_model.PMName;
            this.txt_PMPhone.Text = pro_model.PMPhone == null ? "" : pro_model.PMPhone.Trim();
            //开始结束日期
            this.txt_startdate.Text = Convert.ToDateTime(pro_model.pro_startTime).ToString("yyyy-MM-dd");
            this.txt_finishdate.Text = Convert.ToDateTime(pro_model.pro_finishTime).ToString("yyyy-MM-dd");
            //经济所
            string isotherprt = "";
            if (pro_model.ISTrunEconomy == "1")
            {
                isotherprt += "经济所,";
            }
            //暖通
            if (pro_model.ISHvac == "1")
            {
                isotherprt += "暖通热力所,";
            }
            //土建所
            if (pro_model.ISArch == "1")
            {
                isotherprt += "土建所,";
            }
            if (isotherprt.IndexOf(',') > -1)
            {
                this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
            }
            else
            {
                this.lbl_isotherprt.Text = "无";
            }
            //项目特征概况
            string projSub = pro_model.ProjSub == null ? "" : pro_model.ProjSub.Trim();
            this.txt_sub.InnerHtml = TG.Common.StringPlus.HtmlEncode(projSub);
            //项目备注
            string projInfo = pro_model.pro_Intro == null ? "" : pro_model.pro_Intro.Trim();
            this.txt_remark.InnerHtml = TG.Common.StringPlus.HtmlEncode(projInfo);
        }
        //获取项目来源
        protected string GetProjectSrc(string id)
        {
            TG.Model.cm_Dictionary model = new TG.BLL.cm_Dictionary().GetModel(int.Parse(id));
            if (model != null)
            {
                return model.dic_Name;
            }
            else
            {
                return "";
            }
        }
        //通过项目ID 得到效果图
        protected string GetThumdPicByProjid(string projid)
        {
            TG.BLL.cm_AttachInfo bll = new TG.BLL.cm_AttachInfo();
            string strWhere = " Proj_id=" + projid + " AND OwnType='projt'";
            List<TG.Model.cm_AttachInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                return models[models.Count - 1].FileUrl.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        //获取阶段信息
        protected string GetProfession(string num)
        {
            string result = "";
            switch (num)
            {
                case "-1":
                    result = "";
                    break;
                case "27":
                    result = "方案";
                    break;
                case "28":
                    result = "初设";
                    break;
                case "29":
                    result = "施工图";
                    break;
                case "30":
                    result = "其他";
                    break;
                case "31":
                    result = "公开招标";
                    break;
                case "32":
                    result = "邀请招标";
                    break;
                case "36":
                    result = "计算机行业";
                    break;
                case "33":
                    result = "自行委托";
                    break;
                case "37":
                    result = "教育行业";
                    break;
                case "38":
                    result = "建筑行业";
                    break;
                case "47":
                    result = "科教行业";
                    break;
                case "34":
                    result = "普通客户";
                    break;
                case "35":
                    result = "VIP客户";
                    break;
                case "39":
                    result = "一般";
                    break;
                case "40":
                    result = "密切";
                    break;
                case "41":
                    result = "很密切";
                    break;
                case "43":
                    result = "一级";
                    break;
                case "44":
                    result = "二级";
                    break;
                case "45":
                    result = "三级";
                    break;
                case "46":
                    result = "四级";
                    break;
                default:
                    result = "";
                    break;
            }
            return result;
        }

        protected void Btn_Fixed_Click(object sender, EventArgs e)
        {
            //查询项目信息
            TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(int.Parse(ProjectSysNo));
            //导入数据到原始表
            string IsoPath = Server.MapPath("/ISO");
            string userid = UserSysNo.ToString();
            //工程表单FTP地址
            string ftpPath = ConfigurationManager.AppSettings["FtpPysicsPath"].ToString();
            int count=TG.BLL.tg_ProjectAuditBP.CreateProject(project, HttpContext.Current.Server.MapPath("/Template/tpmTemplate.XML"), IsoPath, userid, ftpPath, true);
            if (count==1)
            {
                MessageBox.ShowAndRedirect(this, "TCD项目已修复成功！", "/ProjectManage/ProjectFixed.aspx");
            }
            else
            {
                MessageBox.ShowAndRedirect(this, "修复失败，请检查项目名是否重复或包含特殊字符！", "/ProjectManage/ProjectFixed.aspx");
            }
        }
    }
}