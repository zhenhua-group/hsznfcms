﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using Geekees.Common.Controls;
using System.Xml;
using AjaxPro;
using TG.Model;
using TG.BLL;

namespace TG.Web.ProjectManage
{
    public partial class Sch_ProjectBymaster : PageBase
    {

        TG.BLL.cm_Project bll_pro = new TG.BLL.cm_Project();
        public string asTreeviewStructObjID
        {
            get
            {
                return this.asTreeviewStruct.GetClientTreeObjectId();
            }
        }
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructType.GetClientTreeObjectId();
            }
        }
        public string ColumnsContent
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ModifyProjectAuditBymaster));
            if (!IsPostBack)
            {
                //绑定字段
                BindColumns();
                //项目地点
                BindDdList();
                //项目阶段
                BindJDList();
                //行业性质
                BindXzList();
                //绑定承接部门
                BindUnit();
                //绑定权限
                BindPreviewPower();
                //设置样式
                SetDropDownTreeThem();
                //绑定建筑结构样式修改时
                BindStructTypeEdit();
                //绑定建筑分类修改时
                BindBuildStuctTypeEdit();
                //设计等级
                BindBuildType();
              
            }
        }
        protected void BindColumns()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("管理级别", "pro_jb");
            dic.Add("审核级别", "AuditLevel");
            dic.Add("建设规模", "ProjectScale");
            dic.Add("建设单位", "pro_buildUnit");
            dic.Add("结构形式", "pro_StruType");
            dic.Add("建筑分类", "pro_kinds");
            dic.Add("建筑类别", "BuildType");
            dic.Add("甲方负责人", "ChgJia");
            dic.Add("甲方负责人电话", "Phone");
            dic.Add("执行设总电话", "PMPhone");
            dic.Add("建设地点", "BuildAddress");
            dic.Add("行业性质", "Industry");
            dic.Add("项目来源", "pro_from");
            dic.Add("参与部门", "ISArch");
            dic.Add("项目概况", "ProjSub");
            dic.Add("项目备注", "pro_Intro");
            dic.Add("录入人", "InsertUser");

            foreach (KeyValuePair<string, string> pair in dic)
            {
                ColumnsContent += "<label><input type='checkbox' value='" + pair.Value + "' />" + pair.Key + "</label>";
            }


            //  BindProInfoConfig(this.asTreeviewStruct.RootNode,dic);
            //this.asTreeviewStruct.CollapseAll();           
        }
        //建筑类别
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
            this.ddrank.DataSource = bll_dic.GetList(str_where);
            this.ddrank.DataTextField = "dic_Name";
            this.ddrank.DataValueField = "ID";
            this.ddrank.DataBind();
        }
        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            //this.asTreeviewStruct.Theme = macOS;
            //this.asTreeviewStructType.Theme = macOS;
            //this.asTreeViewBuildType.Theme = macOS;
            this.asTreeviewStruct.Theme = macOS;
            this.asTreeviewStructType.Theme = macOS;

        }
       
        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        
        //获取所有树文字数据
        protected void GetAllNodeTex(ASTreeViewNode nodes, ref List<string> alltext)
        {
            foreach (ASTreeViewNode node in nodes.ChildNodes)
            {
                alltext.Add(node.NodeText);
                if (node.ChildNodes.Count > 0)
                {
                    GetAllNodeTex(node, ref alltext);
                }
            }
        }
        [AjaxMethod]
        protected void output_Click(object sender, EventArgs e)
        {
            GetDropDownTreeCheckedValue();
        }
        [AjaxMethod]
        public string GetDropDownTreeCheckedValue()
        {
            string rootvalue = "";

            List<ASTreeViewNode> allnodes = this.asTreeviewStruct.RootNode.ChildNodes;
            //最终生成字符串

            foreach (ASTreeViewNode node in allnodes)
            {
                string secondvalue = "";
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                    GetSecondNodeList(node, ref secondvalue);
                }
                rootvalue += secondvalue;
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }
            return rootvalue;
        }
        protected void GetSecondNodeList(ASTreeViewNode node, ref string value)
        {
            if (node.ChildNodes.Count > 0)
            {
                //返回值
                StringBuilder sbresult = new StringBuilder();

                foreach (ASTreeViewNode snode in node.ChildNodes)
                {
                    if (snode.CheckedState == ASTreeViewCheckboxState.Checked || snode.CheckedState == ASTreeViewCheckboxState.HalfChecked)
                    {
                        //拼接第二级
                        value = "^" + snode.NodeValue;
                        string subvalue = "";
                        subvalue = value;
                        GetChildNodes(snode, ref subvalue);
                        foreach (string key in sblist)
                        {
                            sbresult.Append(key);
                        }
                        //清空当前列表
                        sblist.Clear();
                    }
                }
                value = sbresult.ToString();
            }
        }
        //查询数据
        List<string> sblist = new List<string>();
        protected void GetChildNodes(ASTreeViewNode node, ref string value)
        {
            StringBuilder sb = new StringBuilder();
            if (node.ChildNodes.Count > 0)
            {
                foreach (ASTreeViewNode childnode in node.ChildNodes)
                {
                    if ((childnode.CheckedState == ASTreeViewCheckboxState.Checked) || (childnode.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                    {
                        string tempvalue = value + "*" + childnode.NodeValue;

                        if (childnode.ChildNodes.Count > 0)
                        {
                            //如果还有子节点，继续遍历
                            GetChildNodes(childnode, ref tempvalue);
                        }
                        else
                        {
                            //添加末节点
                            sb.Append(tempvalue);
                        }
                    }
                }
            }
            else
            {
                //添加末节点
                sb.Append(value);
            }
            //赋值value
            sblist.Add(sb.ToString());
        }
        //建筑分类
        protected void BindBuildStuctTypeEdit()
        {
            BindProInfoConfig("BuildType", this.asTreeviewStructType.RootNode);
            this.asTreeviewStructType.CollapseAll();
        }
        //结构形式
        protected void BindStructTypeEdit()
        {
            BindProInfoConfig("StructType", this.asTreeviewStruct.RootNode);
            this.asTreeviewStruct.CollapseAll();
        }
        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }
       
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //是否需要权限判断
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //绑定承接部门
        public void BindUnit()
        {
            //承接部门 
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            //不显示的单位
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                strWhere += " 1=1 AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            else
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }

        public void BindDdList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定项目来源
            DataSet dic_proly = dic.GetList(" dic_type='cpr_src'");
            ddsource.DataValueField = "id";
            ddsource.DataTextField = "dic_name";
            ddsource.DataSource = dic_proly;
            ddsource.DataBind();
        }
        //项目阶段
        protected void BindJDList()
        {
            //TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //DataSet dic_proly = dic.GetList(" dic_type='cpr_jd'");
            //chk_jd.DataValueField = "id";
            //chk_jd.DataTextField = "dic_name";
            //chk_jd.DataSource = dic_proly;
            //chk_jd.DataBind();
        }
        //绑定行业性质
        protected void BindXzList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            DataSet dic_proly = dic.GetList(" dic_type='cpr_hyxz'");
            ddType.DataValueField = "id";
            ddType.DataTextField = "dic_name";
            ddType.DataSource = dic_proly;
            ddType.DataBind();
        }
        public void GetList()
        {
            StringBuilder sb = new StringBuilder("");
            // //组合查询
            // if (this.txt_proName.Text.Trim() != "")
            // {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txt_proName.Text.Trim());
            //     sb.Append(" AND pro_name LIKE '%" + this.txt_proName.Text.Trim() + "%' ");
            // }
            // if (this.txt_buildUnit.Value.Trim() != "")
            // {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txt_buildUnit.Value.Trim());
            //     sb.Append(" AND pro_buildUnit LIKE '%" + this.txt_proName.Text.Trim() + "%'  ");
            // }
            // if (this.txtaddress.Text.Trim() != "")
            // {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txtaddress.Text.Trim());
            //     sb.Append(" AND BuildAddress LIKE '%" + this.txtaddress.Text.Trim() + "%'  ");
            // }
            // if (this.drp_unit.SelectedIndex != 0)
            // {
            //     sb.Append(" AND Unit  LIKE '%" + this.drp_unit.SelectedItem.Text.Trim() + "%'  ");
            // }
            // if (this.ddType.SelectedIndex != 0)
            // {
            //     sb.Append(" AND Industry LIKE '%" + this.ddType.SelectedItem.Text.Trim() + "%'  ");
            // }
            // if (this.ddsource.SelectedIndex != 0)
            // {
            //     sb.Append(" AND Pro_src=" + this.ddsource.SelectedItem.Value.Trim() + "  ");
            // }
            // if (this.txt_Aperson.Text.Trim() != "")
            // {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txt_Aperson.Text.Trim());
            //     sb.Append(" AND ChgJia LIKE '%" + this.txt_Aperson.Text.Trim() + "%'  ");
            // }
            // if (this.txt_phone.Text.Trim() != "")
            // {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txt_phone.Text.Trim());
            //     sb.Append(" AND Phone LIKE '%" + this.txt_phone.Text.Trim() + "%'  ");
            // }
            // if (this.txt_PMName.Text.Trim() != "")
            // {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txt_PMName.Text.Trim());
            //     sb.Append(" AND PMName LIKE '%" + this.txt_PMName.Text.Trim() + "%'  ");
            // }
            // //合同额
            // if (this.txtproAcount.Text != "" && this.txtproAcount2.Text == "")
            // {
            //     sb.Append(" AND cpr_Acount >= " + txtproAcount.Text + " ");
            // }
            // else if (this.txtproAcount.Text == "" && this.txtproAcount2.Text != "")
            // {
            //     sb.Append(" AND cpr_Acount <= " + txtproAcount2.Text + " ");
            // }
            // else if (this.txtproAcount.Text != "" && this.txtproAcount2.Text != "")
            // {
            //     sb.Append(" AND (cpr_Acount>=" + this.txtproAcount.Text + " AND cpr_Acount <= " + txtproAcount2.Text + ") ");
            // }
            // //建设规模
            // if (this.txt_buildArea.Text != "" && this.txt_buildArea2.Text == "")
            // {
            //     sb.Append(" AND ProjectScale >=" + txt_buildArea.Text + " ");
            // }
            // else if (this.txt_buildArea.Text == "" && this.txt_buildArea2.Text != "")
            // {
            //     sb.Append(" AND ProjectScale <=" + txt_buildArea2.Text + " ");
            // }
            // else if (this.txt_buildArea.Text != "" && this.txt_buildArea2.Text != "")
            // {
            //     sb.Append(" AND (ProjectScale >=" + txt_buildArea.Text + " AND ProjectScale <=" + this.txt_buildArea2.Text + ") ");
            // }
            // //时间
            // if (this.txt_signdate.Text != "")
            // {
            //     sb.Append(" AND (pro_startTime BETWEEN '" + this.txt_signdate.Text.Trim() + "' AND '" + this.txt_signdate2.Text + "') ");
            // }
            // if (this.ddrank.SelectedIndex != 0)
            // {
            //     sb.Append(" AND BuildType=N'" + this.ddrank.SelectedItem.Text.Trim() + "' ");
            // }
            // if (managerLevelDropDownList.SelectedValue.Trim() != "-1")
            // {
            //     sb.Append(" AND pro_level=" + managerLevelDropDownList.SelectedItem.Value.Trim() + "  ");
            // }
            // if (string.IsNullOrEmpty(txtproAcount.Text) == false && new Regex("^[0-9]+$").Match(txtproAcount.Text).Success && txtproAcount.Text.Trim() != "0")
            // {
            //     sb.Append(" AND Cpr_Acount = " + txtproAcount.Text.Trim() + " ");
            // }
            // //项目阶段信息
            //// string str_jd = "";

            // //bool flag_jd = false;
            // //for (int i = 0; i < this.chk_jd.Items.Count; i++)
            // //{
            // //    if (chk_jd.Items[i].Selected)
            // //    {
            // //        flag_jd = true;
            // //    }
            // //}
            // //if (flag_jd)
            // //{
            // //    foreach (ListItem jd in this.chk_jd.Items)
            // //    {
            // //        if (jd.Selected)
            // //        {
            // //            str_jd += jd.Text + ",";
            // //        }
            // //        //else
            // //        //{
            // //        //    str_jd += ",";
            // //        //}
            // //    }
            // //}

            // //项目阶段
            // string str_jd = "";
            // if (CheckBox1.Checked)
            // {
            //     str_jd += CheckBox1.Text + ",";
            // }
            // //else
            // //{
            // //    str_jd += " ,";
            // //}
            // if (CheckBox2.Checked)
            // {
            //     str_jd += CheckBox2.Text + ",";
            // }
            // //else
            // //{
            // //    str_jd += " ,";
            // //}
            // if (CheckBox3.Checked)
            // {
            //     str_jd += CheckBox3.Text + ",";
            // }
            // //else
            // //{
            // //    str_jd += " ,";
            // //}
            // if (CheckBox4.Checked)
            // {
            //     str_jd += CheckBox4.Text + ",";
            // }
            // //else
            // //{
            // //    str_jd += " ,";
            // //}

            // if (str_jd != "")
            // {
            //     sb.Append(" AND pro_status LIKE '%" + str_jd + "%' ");
            // }

            //  if (sb.ToString() != "")
            //  {
            //检查权限
            GetPreviewPowerSql(ref sb);
            this.hid_where.Value = sb.ToString();
            //所有记录数
            //  this.AspNetPager1.RecordCount = int.Parse(bll_pro.GetListPageProcCount(sb.ToString()).ToString());
            //排序
            // string orderString = " Order by " + OrderColumn + " " + Order;
            //  sb.Append(orderString);
            //绑定
            //this.gv_project.DataSource = bll_pro.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            // this.gv_project.DataBind();
            //  }
        }
    }
}