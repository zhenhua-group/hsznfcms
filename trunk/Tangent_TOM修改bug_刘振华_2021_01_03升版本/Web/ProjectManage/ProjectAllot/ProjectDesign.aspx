﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectDesign.aspx.cs" Inherits="TG.Web.ProjectManage.ProjectAllot.ProjectDesign" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>呼和浩特市建筑勘察设计研究院有限责任公司产值统计表</title>
              <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
  <table width="98%"  border="0"  align="center" style="font-size:12px; font-family:'微软雅黑';"><tr>
    <td colspan="5%" align="center"><strong>年度：XXX</strong></td>
    <td width="47%" align="center"><strong style="font-size:14px;"></strong><strong>呼和浩特市建筑勘察设计研究院有限责任公司产值统计表（一）</strong></td>
    <td width="26%" align="center">记录清单序号NO:17 &nbsp;&nbsp; 部门内容:&nbsp;&nbsp;</td>
    <td width="4%" align="center">设计</td>
    <td width="5%" align="center">钢构</td>
    <td width="4%" align="center">框架</td>
    <td width="4%" align="center">砖混</td>
  </tr>
  </table>
<table width="98%" cellpadding="1" cellspacing="1" style="background:#000;font-size:12px; font-family:'微软雅黑';" align="center">
      <tr>
        <td rowspan="3" align="center" bgcolor="#FFFFFF">序号</td>
        <td rowspan="3" align="center" bgcolor="#FFFFFF"> 项 目 名 称</td>
        <td rowspan="3" align="center" bgcolor="#FFFFFF">合同时间</td>
        <td rowspan="3" align="center" bgcolor="#FFFFFF">合同编号</td>
        <td rowspan="3" align="center" bgcolor="#FFFFFF">设计号</td>
        <td colspan="2" align="center" bgcolor="#FFFFFF">建筑面积</td>
        <td colspan="3" align="center" bgcolor="#FFFFFF">建筑特征</td>
        <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">设计费用（综合）</td>
        <td rowspan="3" align="center" bgcolor="#FFFFFF">出图时间</td>
        <td rowspan="3" align="center" bgcolor="#FFFFFF">归档时间</td>
        <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">设计费</td>
        <td rowspan="3" align="center" bgcolor="#FFFFFF">项目经理</td>
        <td colspan="3" align="center" bgcolor="#FFFFFF">划拨测算表</td>
      </tr>
      <tr>
        <td align="center" bgcolor="#FFFFFF">合同建筑面积</td>
        <td align="center" bgcolor="#FFFFFF">出图建筑面积</td>
        <td width="4%" rowspan="2" align="center" bgcolor="#FFFFFF">结构  形式</td>
        <td colspan="2" align="center" bgcolor="#FFFFFF">费率</td>
        <td height="30" align="center" bgcolor="#FFFFFF">合同设计费</td>
        <td height="30" align="center" bgcolor="#FFFFFF">出图设计费</td>
        <td height="30" align="center" bgcolor="#FFFFFF">已结算费用</td>
        <td align="center" bgcolor="#FFFFFF">未结算费用</td>
        <td align="center" bgcolor="#FFFFFF">比例划分</td>
        <td align="center" bgcolor="#FFFFFF">预分配</td>
        <td align="center" bgcolor="#FFFFFF">测算</td>
      </tr>
      <tr>
        <td align="center" bgcolor="#FFFFFF">(平米)</td>
        <td align="center" bgcolor="#FFFFFF">(平米)</td>
        <td width="4%" align="center" bgcolor="#FFFFFF">(元/㎡)</td>
        <td width="2%" align="center" bgcolor="#FFFFFF">(  %)</td>
        <td height="15" align="center" bgcolor="#FFFFFF">(万元)</td>
        <td height="15" align="center" bgcolor="#FFFFFF">(万元)</td>
        <td height="15" align="center" bgcolor="#FFFFFF">(万元)</td>
        <td align="center" bgcolor="#FFFFFF">(万元)</td>
        <td align="center" bgcolor="#FFFFFF">%</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
      <tr>
        <td height="30" align="center" bgcolor="#FFFFFF">1</td>
        <td height="30" align="center" bgcolor="#FFFFFF">XXXXXX</td>
        <td height="30" align="center" bgcolor="#FFFFFF">2013.XX</td>
        <td height="30" align="center" bgcolor="#FFFFFF">2013XX</td>
        <td align="center" bgcolor="#FFFFFF">2013-S-XX-YY</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">10000</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">10</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td height="30" align="center" bgcolor="#FFFFFF">10</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">1</td>
        <td align="center" bgcolor="#FFFFFF">9</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
      </tr>
      <tr>
        <td height="30" align="center" bgcolor="#FFFFFF">2</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
      </tr>
      <tr>
        <td height="30" align="center" bgcolor="#FFFFFF">3</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
      </tr>
      <tr>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF"><strong>小计</strong></td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
        <td align="center" bgcolor="#FFFFFF">0</td>
      </tr>
      <tr>
        <td width="2%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="7%" height="30" align="center" bgcolor="#FFFFFF">
 
        <strong>本页总计</strong></td>
        <td width="7%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="7%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="5%" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="7%" align="center" bgcolor="#FFFFFF">0</td>
        <td width="5%" align="center" bgcolor="#FFFFFF">10000</td>
        <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td height="30" align="center" bgcolor="#FFFFFF">10</td>
        <td height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td width="6%" height="30" align="center" bgcolor="#FFFFFF">0</td>
        <td width="6%" height="30" align="center" bgcolor="#FFFFFF">10</td>
        <td width="2%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="3%" height="30" align="center" bgcolor="#FFFFFF"></td>
        <td width="7%" height="30" align="center" bgcolor="#FFFFFF">1</td>
        <td width="7%" align="center" bgcolor="#FFFFFF">9</td>
        <td width="7%" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="6%" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="3%" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="3%" align="center" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        </table> 
  <table width="98%"  border="0"  align="center" style="font-size:12px; font-family:'微软雅黑';"><tr>
      <td width="47%" align="center" height="30">填报单位: 本表由生产部填写，相关人员填写、审核、签字</td>
      <td width="19%" align="center">生产院长：</td><td width="17%" align="center"> 生产部长: </td>
          <td width="17%" align="center">制表人: </td>
        </tr>
  </table>
        <table width="98%" cellpadding="1" cellspacing="1" style="background:#000;font-size:12px; font-family:'微软雅黑';" align="center">
          <tr>
            <td height="30" colspan="15" align="center" bgcolor="#FFFFFF"><strong>付款清单</strong></td>
          </tr>
          <tr>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">第1次</td>
            <td align="center" bgcolor="#FFFFFF">第2次</td>
            <td align="center" bgcolor="#FFFFFF">第3次</td>
            <td align="center" bgcolor="#FFFFFF">第4次</td>
            <td align="center" bgcolor="#FFFFFF">第5次</td>
            <td height="30" align="center" bgcolor="#FFFFFF">第6次</td>
            <td align="center" bgcolor="#FFFFFF">第7次</td>
            <td align="center" bgcolor="#FFFFFF">第8次</td>
            <td height="30" align="center" bgcolor="#FFFFFF">第9次</td>
            <td align="center" bgcolor="#FFFFFF">第10次</td>
            <td align="center" bgcolor="#FFFFFF">第11次</td>
            <td align="center" bgcolor="#FFFFFF">第12次</td>
            <td align="center" bgcolor="#FFFFFF">第13次</td>
            <td align="center" bgcolor="#FFFFFF">合计</td>
          </tr>
          <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">付款日期</td>
            <td height="30" align="center" bgcolor="#FFFFFF">2013.01.01</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="5%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">付款额（万）</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
          </tr>
          <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
          </tr>
          <tr>
            <td height="30" align="center" bgcolor="#FFFFFF"><strong>小计</strong></td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
            <td align="center" bgcolor="#FFFFFF">0</td>
          </tr>
          <tr>
            <td width="9%" height="30" align="center" bgcolor="#FFFFFF"><strong>本页总计</strong></td>
            <td width="6%" height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td width="5%" height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td width="6%" align="center" bgcolor="#FFFFFF">0</td>
            <td width="6%" align="center" bgcolor="#FFFFFF">0</td>
            <td height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td width="5%" height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td width="6%" height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td width="5%" height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td width="5%" height="30" align="center" bgcolor="#FFFFFF">0</td>
            <td width="7%" align="center" bgcolor="#FFFFFF">0</td>
            <td width="9%" align="center" bgcolor="#FFFFFF">0</td>
            <td width="7%" align="center" bgcolor="#FFFFFF">0</td>
            <td width="6%" align="center" bgcolor="#FFFFFF">0</td>
            <td width="8%" align="center" bgcolor="#FFFFFF">0</td>
          </tr>
        </table>
        <p>&nbsp;</p>
</form>
</body>
</html>
