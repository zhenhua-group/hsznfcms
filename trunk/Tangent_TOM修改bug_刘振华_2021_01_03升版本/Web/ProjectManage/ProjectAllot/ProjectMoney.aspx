﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectMoney.aspx.cs" Inherits="TG.Web.ProjectManage.ProjectAllot.ProjectMoney" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>设计项目收入分成划拨总表-A-20%(定金划拨)</title>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form id="form1" runat="server">
      <table width="98%"  border="0"  align="center" style="font-size:12px; font-family:'微软雅黑';"><tr><td colspan="10" align="center"><strong style="font-size:14px;">设计项目收入分成划拨总表-A-20%(定金划拨)</strong></td><td width="17%" align="center">记录清单编号NO：18</td><td width="20%" align="center"><strong>（项目经理留存）</strong></td></tr></table>
<table width="98%" cellpadding="1" cellspacing="1" style="background:#000;font-size:12px; font-family:'微软雅黑';" align="center">
            <tr>
                <td width="10%" height="30" align="center" bgcolor="#FFFFFF">项目经理人</td>
                <td width="8%" height="30" align="center" bgcolor="#FFFFFF">
               
                    <asp:Label ID="txt_username" runat="server"   ></asp:Label>
               
                </td>
                <td height="30" colspan="7" align="center" bgcolor="#FFFFFF">建设单位：
                <asp:Label ID="txt_company" runat="server"  ></asp:Label></td>
                <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">合同日期：<asp:Label ID="txt_cprdate" runat="server"></asp:Label></td>
            </tr>
                    <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">
                <strong>控制比例</strong></td>
            <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_kzbl" runat="server"    Text="20%" style=" font-weight:bold;"></asp:Label></td>
            <td height="30" colspan="7" align="center" bgcolor="#FFFFFF">
             <strong> 项目信息表(本表由经营部填写，并用于财务收入进账用）</strong>
                </td>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">收费日期：<asp:Label ID="txt_moenydate" runat="server"    Text="" ></asp:Label></td>
        </tr>
                    <tr>
            <td height="30" align="center" bgcolor="#FFFFFF">
                工程名称</td>
            <td height="30" colspan="6" align="center" bgcolor="#FFFFFF">
                <asp:Label ID="txt_projname" runat="server"   ></asp:Label></td>
            <td width="9%" height="30" align="center" bgcolor="#FFFFFF">定金划拨</td>
            <td width="9%" height="30" align="center" bgcolor="#FFFFFF">合同编号</td>
            <td height="30" colspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_cprno" runat="server"    Text="0"></asp:Label></td>
        </tr>
      <tr>
                <td height="30" align="center" bgcolor="#FFFFFF">楼号</td>
                <td height="30" align="center" bgcolor="#FFFFFF">
                     办公楼</td>
                <td width="10%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="6%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;
                    </td>
                <td width="10%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="6%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td width="13%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td height="30" align="center" bgcolor="#FFFFFF">合计</td>
                <td height="30" align="center" bgcolor="#FFFFFF">设计编号</td>
                <td height="30" colspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_projno" runat="server"    Text="0"></asp:Label></td>
            </tr>
               <tr>
                <td height="30" align="center" bgcolor="#FFFFFF">建筑面积</td>
                <td height="30" align="center" bgcolor="#FFFFFF">
                    <asp:Label ID="txt_mj" runat="server" Text="0"></asp:Label>
                </td>
                <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;
                    </td>
                <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
                <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_hj" runat="server"    Text="0"></asp:Label></td>
                <td height="30" align="center" bgcolor="#FFFFFF">税  率</td>
                <td width="10%" height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_rl" runat="server"    Text="90%"></asp:Label></td>
                <td width="9%" height="30" align="center" bgcolor="#FFFFFF">(万元)</td>
            </tr>
               
               <tr>
                <td height="30" align="center" bgcolor="#FFFFFF">建筑层数
                </td>
                <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="txt_floor" runat="server" Text="0" ></asp:Label>                    
                </td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF">合同单价<br />
                 (元/㎡)</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF">
                   
                     <asp:Label ID="txt_dj" runat="server"  style="width:50px;" Text="0"></asp:Label>
                  </td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF">本次收费<br />
                 (万元)</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"> <asp:Label ID="txt_money" runat="server"  style="width:50px;" Text="0"></asp:Label></td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF">最低标准    <br />
                 分成基数</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"> <asp:Label ID="txt_js" runat="server"  style="width:50px;" Text="0"></asp:Label>
                  <br />
                 (元/㎡)</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_jsmoney" runat="server"    Text="0"></asp:Label>
                  <br />
                 (万元)</td>
                <td height="30" align="center" bgcolor="#FFFFFF">院留管理费</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_glfmoney" runat="server"    Text="0"></asp:Label></td>
            </tr>
            <tr>
                <td height="30" align="center" bgcolor="#FFFFFF"  >建筑高度</td>
                <td height="30" align="center" bgcolor="#FFFFFF"  >
                    <asp:Label ID="txt_height" runat="server"  Text="0"></asp:Label></td>
                <td height="30" align="center" bgcolor="#FFFFFF"  ><asp:Label ID="txt_glf" runat="server"    Text="25%"></asp:Label></td>
            </tr>
          
         
            <tr>
                <td height="30" align="center" bgcolor="#FFFFFF"  >建筑规模</td>
                <td height="30" align="center" bgcolor="#FFFFFF"  >
                    <asp:Label ID="txt_gm" runat="server"  Text="0"></asp:Label></td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  >合同收费<br />
                (万元)</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  >
                   <asp:Label ID="txt_htsf" runat="server"    Text="0"></asp:Label>
                </td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  >本次收费<br />
比例</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  ><asp:Label ID="txt_bcsf" runat="server"    Text="0"></asp:Label></td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  >最低标准税后<br />
                分成基数</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  ><asp:Label ID="txt_shjs" runat="server"    Text="0"></asp:Label>
                  <br />
              (元/㎡)</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  ><asp:Label ID="txt_shjsmoney" runat="server"    Text="0"></asp:Label>
                  <br />
              (万元)</td>
                <td height="30" align="center" bgcolor="#FFFFFF"  >项目留费用</td>
                <td rowspan="2" align="center" bgcolor="#FFFFFF"  ><asp:Label ID="txt_xmmoney" runat="server"    Text="0"></asp:Label></td>
            </tr>
            <tr>
                <td height="30" align="center" bgcolor="#FFFFFF">结构形式</td>
                <td height="30" align="center" bgcolor="#FFFFFF">
                    <asp:Label ID="txt_jgxs" runat="server" Text="0" ></asp:Label></td>
                <td height="30" align="center" bgcolor="#FFFFFF" ><asp:Label ID="txt_xm" runat="server"    Text="75%"></asp:Label></td>
            </tr>
            <tr>
              <td rowspan="2" align="center" bgcolor="#FFFFFF">备注</td>
              <td height="30" colspan="8" align="left" bgcolor="#FFFFFF">1.本表部分由经营部根据项目特性填写，部分为计算公式结果(绝对禁止输入内容)</td>
              <td height="30" align="center" bgcolor="#FFFFFF">中标项目</td>
              <td height="30" align="center" bgcolor="#FFFFFF">一般项目</td>
  </tr>
            <tr>
              <td height="30" colspan="8" align="left" bgcolor="#FFFFFF">2.本表一式三份，院财务、项目经理、经营部各一份</td>
              <td height="30" align="center" bgcolor="#FFFFFF">咨询项目</td>
              <td height="30" align="center" bgcolor="#FFFFFF">冰山项目</td>
            </tr>
        </table>
    <table width="98%"  border="0"  align="center" style="font-size:12px; font-family:'微软雅黑';"><tr>
      <td width="12%" align="center" height="30">院长：</td>
      <td width="18%" align="center">经营院长：</td><td width="16%" align="center">经营部: </td>
          <td width="19%" align="center">财务部:</td>
          <td width="16%" align="center">项目经理:</td>
          <td width="19%" align="center">制表人: </td>
        </tr>
    </table>
  </form>
    
</body>
</html>
