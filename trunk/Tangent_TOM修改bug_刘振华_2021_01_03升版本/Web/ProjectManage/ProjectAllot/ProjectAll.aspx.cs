﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace TG.Web.ProjectManage.ProjectAllot
{
    public partial class ProjectAll : PageBase
    {
        public int pro_id = 0;
        public TG.Model.cm_Project pj = new Model.cm_Project();
        public TG.Model.cm_Coperation crp = new Model.cm_Coperation();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (int.TryParse(Request.QueryString["pro_id"], out pro_id))
            {
                pj = new BLL.cm_Project().GetModel(pro_id);
                if (pj != null)
                {
                    if(pj.CoperationSysNo>0)
                    {
                        crp=new BLL.cm_Coperation().GetModel(pj.CoperationSysNo);
                    }
                    
                    this.txt_username.Text=pj.PMName;
                    this.txt_projname.Text = pj.pro_name;
                    this.txt_company.Text = pj.pro_buildUnit;
                    if(crp!=null)
                    {
                        this.txt_cprdate.Text = Convert.ToDateTime(crp.cpr_SignDate).ToString("yyyy-MM-dd");
                        this.txt_cprno.Text = crp.cpr_No;
                    }
                    DataTable dt = new BLL.cm_projectNumber().GetList(" pro_id=" + pro_id + "").Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        this.txt_projno.Text = dt.Rows[0]["ProNumber"].ToString();
                    }
                }
            }
        }
    }
}