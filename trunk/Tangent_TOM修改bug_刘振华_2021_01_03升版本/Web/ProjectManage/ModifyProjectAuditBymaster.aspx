﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ModifyProjectAuditBymaster.aspx.cs" Inherits="TG.Web.ProjectManage.ModifyProjectAuditBymaster" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<%@ Register Src="../UserControl/ChooseUser.ascx" TagName="ChooseUser" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <link href="../css/swfupload/default_cpr.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_green.js"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="../js/UserControl/ChooseCustomer.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="../js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="../js/UserControl/ChooseUser.js" type="text/javascript"></script>
    <script src="../js/Common/ShowDivDialog.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/MessageComm.js"></script>
    <script src="../js/ProjectMamage/ModifyProjectAudit.js" type="text/javascript"></script>
    <script type="text/javascript">   
        //判断是否选中节点
  
        //判断是否选中节点
        var nodecount;
        var nodeLenght;
        function IsStructCheckNode(obj) {
            nodecount = 0;
            nodeLenght="";
           
            if (obj == 'struct') {
                <%= asTreeviewStructObjID %>.traverseTreeNode(displayNodeFun);
                <%= asTreeviewStructObjID %>.traverseTreeNode(displayNodeFuns);
            }
            else if (obj == 'structtype') {
                <%= asTreeviewStructTypeObjID %>.traverseTreeNode(displayNodeFun);
                <%= asTreeviewStructTypeObjID %>.traverseTreeNode(displayNodeFuns);
            }
            
       
        if (nodecount > 0 ) {
            if (nodeLenght.length<500) {
                return true;
            }else{
                return false;
            }            
        }
        else {
            return false;
        }
    }
    //选中与半选中  qpl 20140115
    function displayNodeFun(elem) {
        if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
            nodecount++;
        }
    }
    function displayNodeFuns(elem) {
        if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
            nodeLenght+=  elem.getElementsByTagName("checkedState");
        }
    }     
    function ChooseNodesValue()
    {
        var result="<%= GetDropDownTreeCheckedValue()%>"; 
        return result;
        alert(result);
    }
    function displayNodeFun(elem) {
        if (elem.getAttribute("checkedState") == "0") {
            nodecount++;
        }
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>项目修改申请</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>项目管理<i class="fa fa-angle-right"> </i>项目修改申请</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>请选择修改项
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td align="left">
                                            <input id="cprName" name="bbs" class="cls_audit" type="checkbox" /><span for="cprName">项目名称</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_coper" name="bbs" class="cls_audit" type="checkbox" /><span for="cpr_coper">关联合同</span>
                                        </td>
                                        <td align="left">
                                            <input id="Level" name="bbs" class="cls_audit" type="checkbox" /><span for="Level">管理级别</span>
                                        </td>
                                        <%-- <td align="left">
                    <input id="cpr_Type" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Type">项目类型</span>
                </td>--%>
                                        <td align="left">
                                            <input id="buildArea" name="bbs" class="cls_audit" type="checkbox" /><span for="buildArea">建设规模</span>
                                        </td>
                                        <td align="left">
                                            <input id="cprBuildUnit" name="bbs" class="cls_audit" type="checkbox" /><span for="cprBuildUnit">建设单位</span>
                                        </td>
                                        <%--  <td align="left">
                    <input id="area" name="bb" class="cls_audit" type="checkbox" /><span for="area">规划面积</span>
                </td>--%>
                                        <td align="left">
                                            <input id="StructType" name="bbs" class="cls_audit" type="checkbox" /><span for="StructType">结构形式</span>
                                        </td>
                                        <td align="left">
                                            <input id="BuildStructType" name="bbs" class="cls_audit" type="checkbox" /><span
                                                for="BuildStructType">建筑分类</span>
                                        </td>
                                        <td align="left">
                                            <input id="buildType" name="bbs" class="cls_audit" type="checkbox" /><span for="buildType">建筑类别</span>
                                        </td>
                                        <td align="left">
                                            <input id="proFuze" name="bbs" class="cls_audit" type="checkbox" /><span for="proFuze">项目经理</span>
                                        </td>
                                        <td align="left">
                                            <input id="FParty" name="bbs" class="cls_audit" type="checkbox" /><span for="FParty">甲方负责人</span>
                                        </td>
                                        <td align="left">
                                            <input id="cjbm" name="bbs" class="cls_audit" type="checkbox" /><span for="cjbm">承接部门</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <input id="cpr_Account" name="bbs" class="cls_audit" type="checkbox" /><span for="cpr_Account">合同额</span>
                                        </td>
                                        <td align="left">
                                            <input id="ddProjectPosition" name="bbs" class="cls_audit" type="checkbox" /><span
                                                for="ddProjectPosition">建设地点</span>
                                        </td>
                                        <td align="left">
                                            <input id="ddProfessionTypeCheck" name="bbs" class="cls_audit" type="checkbox" /><span
                                                for="ddProfessionTypeCheck">行业性质</span>
                                        </td>
                                        <td align="left">
                                            <input id="ddcpr_Stage" name="bbs" class="cls_audit" type="checkbox" /><span for="ddcpr_Stage">设计阶段</span>
                                        </td>
                                        <td align="left">
                                            <input id="ddSourceWay" name="bbs" class="cls_audit" type="checkbox" /><span for="ddSourceWay">工程来源</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_join" name="bbs" class="cls_audit" type="checkbox" /><span for="cpr_join">参与部门</span>
                                        </td>
                                        <td align="left">
                                            <input id="SingnDate" name="bbs" class="cls_audit" type="checkbox" /><span for="SingnDate">项目开始日期</span>
                                        </td>
                                        <td align="left">
                                            <input id="DoneDate" name="bbs" class="cls_audit" type="checkbox" /><span for="DoneDate">项目结束日期</span>
                                        </td>
                                        <td align="left">
                                            <input id="multibuild" name="bbs" class="cls_audit" type="checkbox" /><span for="multibuild">项目特征概况</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_Remark" name="bbs" class="cls_audit" type="checkbox" /><span for="cpr_Remark">项目备注</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>修改项目信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered table-hover" align="center" style="width: 100%;"
                                    id="EditDetail">
                                    <tr option="cpr_No" style="display: none;">
                                        <td style="width: 60px;">项目名称：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txt_name" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hid_projid" runat="server" Value="" />
                                            <asp:HiddenField ID="hid_cprid" runat="server" />
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txt_nameEdit" runat="server" CssClass="form-control input-sm" MaxLength="500"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">管理级别：
                                        </td>
                                        <td style="width: 300px;">
                                            <input id="radio_yuan" name="CC" type="radio" checked="false" runat="server" value="1"
                                                readyonly="true" />院管
                                            <input id="radio_suo" checked="false" readyonly="true" name="CC" type="radio" runat="server"
                                                value="2" />所管
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="radio-list">
                                                <label class="radio-inline">
                                                    <div class="radio" id="uniform-optionsRadios4">
                                                        <span class="">
                                                            <input type="radio" name="aa" id="radio_yuanEdit" runat="server" value="院管" />
                                                        </span>
                                                    </div>
                                                    院管
                                                </label>
                                                <label class="radio-inline">
                                                    <div class="radio" id="uniform-optionsRadios5">
                                                        <span class="checked">
                                                            <input type="radio" name="aa" id="radio_suoEdit" runat="server" value="所管" />
                                                        </span>
                                                    </div>
                                                    所管
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">审核级别：
                                        </td>
                                        <td style="width: 300px;">
                                            <span id="yg" runat="server">
                                                <input id="audit_yuan" name="DD" disabled="disabled" type="checkbox" checked="false"
                                                    runat="server" value="0" /><label for="audit_yuan">院审</label>
                                            </span><span id="sg" runat="server">
                                                <input id="audit_suo" checked="false" disabled="disabled" name="DD" type="checkbox"
                                                    runat="server" value="1" /><label for="audit_suo">所审</label>
                                            </span>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <span id="ygedit">
                                                <input id="audit_yuanEdit" name="bb" type="checkbox" checked="false" runat="server" /><label
                                                    for="audit_yuanEdit">院审</label>
                                            </span><span id="sgedit">
                                                <input id="audit_suoEdit" checked="false" name="bb" type="checkbox" runat="server" /><label
                                                    for="audit_suoEdit">所审</label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr option="ddcpr_Type" style="display: none;">
                                        <td style="width: 60px;">项目类型：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txt_projtype" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:DropDownList ID="drp_projtype" runat="server" AppendDataBoundItems="true" CssClass="form-control input-sm">
                                                    <asp:ListItem Value="-1">----选择项目类型----</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">建筑面积：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txt_scale" runat="server"></asp:Label>
                                            (<span style="font-size: 10pt">㎡</span>)
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txt_scaleEdit" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                            </div>
                                            (<span style="font-size: 10pt">㎡</span>)
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">建筑单位：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txtbuildUnit" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txtbuildUnitEdit" runat="server" CssClass="form-control input-sm"
                                                    MaxLength="250"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr option="cprName" style="display: none;">
                                        <td style="width: 60px;">规划面积：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txt_area" runat="server"></asp:Label>
                                            (<span style="font-size: 10pt">公顷</span>)
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txt_areaEdit" runat="server" CssClass="form-control input-sm" Width="150px"></asp:TextBox>
                                            </div>
                                            (<span style="font-size: 10pt">公顷</span>)
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">结构形式：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="struct_tree">
                                                <asp:Literal ID="lbl_StructType" runat="server"></asp:Literal>
                                            </div>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <cc1:ASDropDownTreeView ID="asTreeviewStructEdit" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="----------请选择结构形式----------"
                                                Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                Font-Size="12px" Font-Strikeout="False" EnableTheme="true" />
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">建筑分类：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="struct_tree">
                                                <asp:Literal ID="lbl_BuildStructType" runat="server"></asp:Literal>
                                            </div>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <cc1:ASDropDownTreeView ID="asTreeviewStructTypeEdit" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="----------请选择建筑分类----------"
                                                Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                Font-Size="12px" Font-Strikeout="False" EnableTheme="true" />
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">建筑类别：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="struct_tree">
                                                <asp:Literal ID="lbl_BuildTypeLevel" runat="server"></asp:Literal>
                                            </div>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-5">
                                                <asp:DropDownList ID="drp_buildtype" runat="server" AppendDataBoundItems="true" CssClass="form-control input-sm">
                                                    <asp:ListItem Value="-1">------选择类别------</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">项目经理：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txt_PMName" runat="server" Width="150px"></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txt_PMNameEdit" runat="server" CssClass="form-control input-sm"
                                                    MaxLength="25" Enabled="false" ReadOnly="true"></asp:TextBox>
                                                <asp:HiddenField ID="hid_pmname" runat="server" Value="0" />
                                                <asp:HiddenField ID="hid_pmuserid" runat="server" Value="0" />
                                            </div>
                                            <div class="col-md-2">
                                                <a href="#ZXSZ" id="sch_PM" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">电话：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txt_PMPhone" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txt_PMPhoneEdit" runat="server" CssClass="form-control input-sm"
                                                    MaxLength="25" Width="150px"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">甲方负责人：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txt_Aperson" runat="server" Style="width: 150px;"></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txt_ApersonEdit" runat="server" CssClass="form-control input-sm"
                                                    MaxLength="25" ReadOnly="true"></asp:TextBox>
                                            </div>
                                            <div class="col-md-2">
                                                <a id="sch_Aperson" href="#JFFZ" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">电话：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txt_phone" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txt_phoneEdit" runat="server" CssClass="form-control input-sm" MaxLength="15"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">承接部门：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txt_unit" runat="server" Width="150px"></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txt_unitEdit" runat="server" CssClass="form-control input-sm" MaxLength="50"
                                                    ReadOnly="true" Width="150px"></asp:TextBox>
                                                <asp:HiddenField ID="hid_unit" runat="server" Value="" />
                                            </div>
                                            <div class="col-md-2">
                                                <a id="sch_unit" href="#CJBMDiv" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">合同额：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txtproAcount" runat="server"></asp:Label>万元
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txtproAcountEdit" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                            </div>
                                            万元
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">建设地点：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txtbuildAddress" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txtbuildAddressEdit" runat="server" CssClass="form-control input-sm"
                                                    MaxLength="500"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">行业性质：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="ddProfessionType" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-5">
                                                <asp:DropDownList ID="ddProfessionTypeEdit" runat="Server" AppendDataBoundItems="True"
                                                    CssClass="form-control input-sm">
                                                    <asp:ListItem Value="0">--请选择--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">设计阶段：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:CheckBox ID="CheckBox1" runat="server" Text="方案设计" CssClass="cls_jd" />
                                            <asp:CheckBox ID="CheckBox2" runat="server" Text="初步设计" CssClass="cls_jd" />
                                            <asp:CheckBox ID="CheckBox3" runat="server" Text="施工图设计" CssClass="cls_jd" />
                                            <asp:CheckBox ID="CheckBox4" runat="server" Text="其他" CssClass="cls_jd" />
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-10">
                                                <asp:CheckBoxList ID="chk_cprjd" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                </asp:CheckBoxList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">项目来源：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="ddsource" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-5">
                                                <asp:DropDownList ID="ddsourceEdit" runat="server" CssClass="form-control input-sm">
                                                    <asp:ListItem Value="0">--请选择--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">参与部门：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="lbl_isotherprt" runat="server" Style="width: 150px;"></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-12">
                                                <div class="checkbox-list">
                                                    <label class="checkbox-inline">
                                                        <span class="">
                                                            <input type="checkbox" id="chk_ISTrunEconomy" class="can" runat="server" value="经济所" />
                                                        </span>经济所
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        <span class="">
                                                            <input type="checkbox" id="chk_ISHvac" runat="server" class="can" value="暖通热力所" />
                                                        </span>暖通热力所
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        <span>
                                                            <input type="checkbox" id="chk_ISArch" runat="server" class="can" value="土建所" />
                                                        </span>土建所
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">项目开始日期：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txt_startdate" runat="server" Style="width: 150px;"></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:TextBox ID="txt_startdateEdit" runat="server" onclick="WdatePicker({readOnly:true})"
                                                class="Wdate" size="20" Style="width: 150px; height: 30px; background-color: #FFC;"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">项目完成日期：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txt_finishdate" runat="server" Style="width: 150px;"></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:TextBox ID="txt_finishdateEdit" runat="server" onclick="WdatePicker({readOnly:true})"
                                                class="Wdate" runat="Server" size="20" Style="width: 150px; height: 30px; background-color: #FFC;"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">项目特征概况：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:TextBox ID="txt_sub" runat="server" Height="68px" Width="98%" TextMode="MultiLine"
                                                MaxLength="250" CssClass="cls_input_text_valid_w"></asp:TextBox>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txt_subEdit" runat="server" Height="68px" Width="98%" TextMode="MultiLine"
                                                    MaxLength="250" CssClass="form-control input-sm"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">项目备注：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:TextBox ID="txt_remark" runat="server" Height="68px" Width="98%" TextMode="MultiLine"
                                                MaxLength="250" CssClass="cls_input_text_w"></asp:TextBox>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txt_remarkEdit" runat="server" Height="68px" Width="99%" TextMode="MultiLine"
                                                    MaxLength="250" CssClass="form-control input-sm"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 60px;">关联合同：
                                        </td>
                                        <td style="width: 300px;">
                                            <asp:Label ID="txt_reletiveB" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 60px;">修改后：
                                        </td>
                                        <td style="width: 300px;">
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txt_reletive" runat="server" CssClass="form-control input-sm" MaxLength="500" ReadOnly="true"></asp:TextBox>
                                            </div>
                                            <div class="col-md-3">
                                                <a id="sch_reletive" href="#HTGL" class="btn
        blue btn-sm"
                                                    data-toggle="modal"><i class="fa fa-search"></i></a>
                                                <input id="txtcpr_id" type="hidden" runat="server" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <input type="button" value="提交申请" id="btnsub" href="#AuditUser" data-toggle="modal"
                                class="btn green" />
                            <input type="button" value="返回" id="btncancl" class="btn btn-default" />
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField runat="server" ID="NotShowUnitList" Value="" />
    <input type="hidden" id="HiddenProjectSysNo" value="<%=ProjectSysno %>" />
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenProjectName" value="<%=ProName %>" />
    <asp:LinkButton runat="server" ID="output" OnClick="output_Click"></asp:LinkButton>
    <!-- 弹出框 -->
    <div id="AuditUser" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!-- 4关联合同 -->
    <div id="HTGL" class="modal fade yellow" tabindex="-1" data-width="660" aria-hidden="true"
        style="display: none; width: 660px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">合同关联</h4>
        </div>
        <div class="modal-body">
            <div id="cprReletiveDiv">
                <div class="form-body">
                    <div class="row">
                        <label class="control-label col-md-2">
                            生产部门:</label>
                        <div class="col-md-3">
                            <select id="select_releCprUnit" class=" form-control input-sm">
                                <option value="-1" selected="selected">-----全院部门-----</option>
                            </select>
                        </div>
                        <label class="control-label col-md-2">
                            合同名称:</label>
                        <div class="col-md-3">
                            <input type="text" id="TxtCprName" value="" class=" form-control input-sm" />
                        </div>
                        <div class="col-md-2">
                            <input type="button" class="btn blue btn-block" id="btn_SearchByCprName" value="查询" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <table id="cprReletiveTable" class="table table-bordered table-striped table-condensed flip-content"
                                align="center">
                                <tr class="trBackColor">
                                    <td align="left">合同名称
                                    </td>
                                    <td align="center">签订日期
                                    </td>
                                    <td align="center">操作
                                    </td>
                                </tr>
                            </table>
                            <!--合同级别绑定-->
                            <input type="hidden" id="cprLevel" value="" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <div id="cprReletivePage" class="divNavigation pageDivPosition">
                                总<label id="reletiveCpr_totalCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                                第<label id="reletiveCpr_nowPageIndex">0</label>/<label id="reletiveCpr_PagesCount">0</label>页&nbsp;&nbsp;
                                <span id="reletiveCpr_firstPage">首页</span>&nbsp; <span id="reletiveCpr_prevPage">&lt;&lt;</span>&nbsp;
                                <span id="reletiveCpr_nextPage">&gt;&gt;</span>&nbsp; <span id="reletiveCpr_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp; 跳至<input type="text" id="reletiveCpr_gotoPageNum"
                                    maxlength="4" style="height: 3; width: 20px; border-style: none none solid none;" />
                                页&nbsp;&nbsp; <span id="reletiveCpr_gotoPage">GO</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--承接部门-->
    <div id="CJBMDiv" class="modal fade yellow" tabindex="-1" data-width="660" aria-hidden="true"
        style="display: none; width: 660px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">承接部门</h4>
        </div>
        <div class="modal-body">
            <div id="cpr_cjbmDiv">
                <div class="row">
                    <div class="col-md-12">
                        <table id="pro_cjbmTable" class="table table-bordered table-striped table-condensed flip-content"
                            style="text-align: center;">
                            <tr class="trBackColor">
                                <td style="width: 60px;">序号
                                </td>
                                <td style="width: 340px;">单位名称
                                </td>
                                <td style="width: 60px;">操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="cjbmByPageDiv" class="divNavigation pageDivPosition">
                            总<label id="cjbm_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="cjbm_nowPageIndex">0</label>/<label id="cjbm_allPageCount">0</label>页&nbsp;&nbsp;
                            <span id="cjbm_firstPage" style="color: Blue; cursor: pointer;">首页</span>&nbsp;
                            <span id="cjbm_prevPage" style="color: Blue; cursor: pointer;">&lt;&lt;</span>&nbsp;
                            <span id="cjbm_nextPage" style="color: Blue; cursor: pointer;">&gt;&gt;</span>&nbsp;
                            <span id="cjbm_lastPage" style="color: Blue; cursor: pointer;">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="cjbm_pageIndex" style="height: 3; width: 20px; border-style: hidden; border-bottom-style: solid;" />页&nbsp;&nbsp; <span id="cjbm_gotoPageIndex" style="color: Blue; cursor: pointer;">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--甲方负责人-->
    <div id="JFFZ" class="modal fade yellow" tabindex="-1" data-width="560" aria-hidden="true"
        style="display: none; width: 560px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">甲方负责人</h4>
        </div>
        <div class="modal-body">
            <div id="jffzr_DialogDiv">
                <div class="row">
                    <label class="control-label col-md-2">
                        姓名:</label><div class="col-md-4">
                            <input type="text" id="txtName" value="" class="form-control input-sm" maxlength="15" />
                        </div>
                    <label class="control-label col-md-2">
                        电话:</label>
                    <div class="col-md-4">
                        <input type="text" id="txtPhone" value="" class="form-control input-sm" maxlength="15" />
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-md-2">
                        部门:</label>
                    <div class="col-md-4">
                        <input type="text" id="txtCompName" value="" maxlength="25" class="form-control input-sm" />
                    </div>
                    <div class="col-md-2">
                        <input type="button" id="btn_serch" class="btn blue
    btn-default"
                            value="查询" />
                    </div>
                    <br />
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="jffzr_table" class="table table-bordered table-striped table-condensed flip-content"
                            style="text-align: center;">
                            <tr class="trBackColor">
                                <td>联系人
                                </td>
                                <td>电话
                                </td>
                                <td>部门
                                </td>
                                <td>操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="jffzr_forPageDiv" class="divNavigation pageDivPosition">
                            总<label id="jffzr_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="jffzr_nowPageIndex">0</label>/<label id="jffzr_allPageCount">0</label>页&nbsp;&nbsp;
                            <span id="jffzr_firstPage" style="color: Blue; cursor: pointer;">首页</span>&nbsp;
                            <span id="jffzr_prevPage" style="color: Blue; cursor: pointer;">&lt;&lt;</span>&nbsp;
                            <span id="jffzr_nextPage" style="color: Blue; cursor: pointer;">&gt;&gt;</span>&nbsp;
                            <span id="jffzr_lastPage" style="color: Blue; cursor: pointer;">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="jffzr_pageIndex" style="height: 3; width: 20px; border-style: hidden; border-bottom-style: solid;" />页&nbsp;&nbsp; <span style="color: Blue; cursor: pointer;"
                                id="jffzr_gotoPage">GO</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">
                        关闭</button>
                </div>
            </div>
        </div>
    </div>
    <!--执行设总-->
    <div id="ZXSZ" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择设总</h4>
        </div>
        <div class="modal-body">
            <div id="chooseUserMain">
                <uc1:ChooseUser ID="ChooseUser1" runat="server" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
