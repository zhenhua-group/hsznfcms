﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using System.Text;
using System.Data;
using System.Xml;
using TG.Common;

namespace TG.Web.ProjectManage
{
    public partial class ModifyProjectBymaster : PageBase
    {
        public string asTreeviewStructObjID
        {
            get
            {
                return this.asTreeviewStruct.GetClientTreeObjectId();
            }
        }
        //是否具有修改权限
        public string HasAudit { get; set; }
        //审批流程获取的修改权限
        public string HasEditAudit
        {
            get
            {
                return Request["audit"] ?? "0";
            }
        }
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructType.GetClientTreeObjectId();
            }
        }
        //是否需要检查权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //修改项目的ID
        string strProId = "";
        public string str_jd = "";
        public string strtype = "";
        TG.BLL.tg_project pro = new TG.BLL.tg_project();

        //建筑结构值  qpl 20140115
        public string StructString { get; set; }
        public string StructTypeString { get; set; }
        public string BuildTypeString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                strProId = Request.QueryString["bs_project_Id"];
            }
            catch (Exception)
            {
                Response.Redirect("ProjectListBymaster.aspx");
                throw;
            }
            if (!IsPostBack)
            {

                //项目是否在审批阶段
                if (!CheckAudit())
                {
                    if (HasEditAudit == "1")
                    {
                        HasAudit = "0";
                        ControlReadOnlyEdit(false);
                    }
                    else
                    {
                        Response.Write("<script language='javascript' >alert('该项目已提交审核，不能被修改！');window.history.back();</script>");
                        //TG.Common.MessageBox.Show(this, "该项目已提交审核，不能被修改！");
                        ControlReadOnly(false);

                    }
                }
                this.hidproId.Value = strProId;
                //行业性质
                BindCorpHyxz();
                //设置下拉树样式
                SetDropDownTreeThem();
                //建筑类别
                BindBuildType();
                //绑定建筑结构样式
                BindStructType();
                //绑定建筑分类
                BindBuildStuctType();
                //行业性质
                BindDdList();
                //项目信息
                GetProInfo();
                //加载用户权限
                BindPreviewPower();
            }
            else
            {
                EditSave();
            }
        }

        private void EditSave()
        {
            TG.BLL.cm_Project project = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_modol = project.GetModel(int.Parse(strProId));
            //项目ID
            pro_modol.bs_project_Id = int.Parse(strProId);
            //项目信息
            pro_modol.pro_name = this.txt_name.Value.Trim();
            //管理级别
            int level = 0;
            if (this.radio_yuan.Checked)
            {
                level = 0;
            }
            else if (this.radio_suo.Checked)
            {
                level = 1;
            }
            pro_modol.pro_level = level;
            //建设单位
            pro_modol.pro_buildUnit = this.txtbuildUnit.Value.Trim();
            //开始时间
            pro_modol.pro_startTime = Convert.ToDateTime(this.txt_startdate.Value);
            //结束时间
            pro_modol.pro_finishTime = Convert.ToDateTime(this.txt_finishdate.Value);
            //结构样式
            pro_modol.pro_StruType = GetDropDownTreeCheckedValue(this.asTreeviewStruct.RootNode.ChildNodes);
            //建筑分类
            pro_modol.pro_kinds = GetDropDownTreeCheckedValue(this.asTreeviewStructType.RootNode.ChildNodes);
            //项目备注
            pro_modol.pro_Intro = this.txt_remark.Value;
            //项目阶段
            string str_jd = "";
            if (CheckBox1.Checked)
            {
                str_jd += CheckBox1.Value + ",";
            }
            else
            {
                str_jd += " ,";
            }
            if (CheckBox2.Checked)
            {
                str_jd += CheckBox2.Value + ",";
            }
            else
            {
                str_jd += " ,";
            }
            if (CheckBox3.Checked)
            {
                str_jd += CheckBox3.Value + ",";
            }
            else
            {
                str_jd += " ,";
            }
            if (CheckBox4.Checked)
            {
                str_jd += CheckBox4.Value + ",";
            }
            else
            {
                str_jd += " ,";
            }
            pro_modol.pro_status = str_jd;
            //项目来源
            pro_modol.Pro_src = Convert.ToInt32(this.ddsource.SelectedValue);
            //甲方负责人
            pro_modol.ChgJia = this.txt_Aperson.Value.Trim();
            //合同额
            pro_modol.Cpr_Acount = Convert.ToDecimal(this.txtproAcount.Value.Trim());
            //甲方电话
            pro_modol.Phone = this.txt_phone.Value.Trim();
            //关联合同
            pro_modol.Project_reletive = this.txt_reletive.Value.Trim();
            //建筑规模
            pro_modol.ProjectScale = Convert.ToDecimal(this.txt_scale.Value.Trim());
            //承接部门
            pro_modol.Unit = this.hid_unit.Value.Trim();
            pro_modol.BuildAddress = this.txtbuildAddress.Value.Trim();
            //合同id
            pro_modol.cprID = this.txtcpr_id.Value.Trim();
            pro_modol.UpdateBy = UserSysNo;
            pro_modol.UpdateDate = DateTime.Now;
            //项目总负责
            pro_modol.PMName = this.txt_PMName.Value.Trim();
            pro_modol.PMName = this.hid_pmname.Value.Trim();
            pro_modol.PMPhone = this.txt_PMPhone.Value.Trim();
            //项目经理ID
            pro_modol.PMUserID = Convert.ToInt32(this.hid_pmuserid.Value);
            //行业性质
            if (this.ddProfessionType.SelectedIndex != 0)
            {
                pro_modol.Industry = this.ddProfessionType.SelectedItem.Text;
            }
            //建筑分类
            if (this.drp_buildtype.SelectedIndex != 0)
            {
                pro_modol.BuildType = this.drp_buildtype.SelectedItem.Text;
            }
            //项目概况
            pro_modol.ProjSub = this.txt_sub.Value;
            //update  2013-9-3
            string isjjs = "0";
            if (this.chk_ISTrunEconomy.Checked)
            {
                isjjs = "1";
            }
            pro_modol.ISTrunEconomy = isjjs;
            //审核级别
            string auditlevel = "0,0";
            if (this.audit_yuan.Checked && !this.audit_suo.Checked)
            {
                auditlevel = "1,0";
            }
            else if (!this.audit_yuan.Checked && this.audit_suo.Checked)
            {
                auditlevel = "0,1";
            }
            else if (this.audit_yuan.Checked && this.audit_suo.Checked)
            {
                auditlevel = "1,1";
            }

            pro_modol.AuditLevel = auditlevel;
            //暖通所
            string isnt = "0";
            if (this.chk_ISHvac.Checked)
            {
                isnt = "1";
            }
            pro_modol.ISHvac = isnt;
            //土建所
            string isarch = "0";
            if (this.chk_ISArch.Checked)
            {
                isarch = "1";
            }
            pro_modol.ISArch = isarch;

            //已审核修改在这里修改需要修改
            if (HasEditAudit == "1")
            {
                try
                {
                    project.Update(pro_modol);
                    TG.Model.tg_project tg_model = pro.GetModel(pro_modol.ReferenceSysNo);
                    tg_model.pro_Name = this.txt_name.Value.Trim();
                    tg_model.pro_DesignUnit = this.hid_unit.Value.Trim();
                    string unitsql = @" SELECT unit_ID FROM tg_unit WHERE unit_Name='" + this.hid_unit.Value.Trim() + "'";
                    tg_model.pro_DesignUnitID = int.Parse(TG.DBUtility.DbHelperSQL.GetSingle(unitsql).ToString());
                    pro.Update(tg_model);
                    TG.Common.MessageBox.ShowAndRedirect(this, "立项信息修改成功！", "ProMiddlePage.aspx?proid=" + strProId + "&proname=" + this.txt_name.Value.Trim());
                }
                catch (Exception)
                {
                    MessageBox.Show(this, "立项信息保存失败！");
                    throw;
                }
            }
            else//未审核修改
            {
                try
                {
                    project.Update(pro_modol);
                    ControlReadOnly(false);
                    this.hidproId.Value = strProId;
                    //MessageBox.ResponseScriptBack(this, "立项信息修改成功！");\?proid=" + result + "&proname=" + this.txt_name.Value.Trim()
                    TG.Common.MessageBox.ShowAndRedirect(this, "立项信息修改成功！", "ProMiddlePage.aspx?proid=" + strProId + "&proname=" + this.txt_name.Value.Trim());
                    GetProInfo();
                }
                catch (Exception)
                {
                    MessageBox.Show(this, "立项信息保存失败！");
                    throw;
                }
            }
        }
        //返回一个文件上传的随机ID
        public string GetProjectID()
        {
            return this.hidproId.Value;
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz'";
            this.ddProfessionType.DataSource = bll_dic.GetList(str_where);
            this.ddProfessionType.DataTextField = "dic_Name";
            this.ddProfessionType.DataValueField = "ID";
            this.ddProfessionType.DataBind();
        }
        //合同建筑类别
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
            this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "ID";
            this.drp_buildtype.DataBind();
        }
        //结构形式
        protected void BindStructType()
        {
            BindProInfoConfig("StructType", this.asTreeviewStruct.RootNode);
            this.asTreeviewStruct.CollapseAll();
        }
        //建筑分类
        protected void BindBuildStuctType()
        {
            BindProInfoConfig("BuildType", this.asTreeviewStructType.RootNode);
            this.asTreeviewStructType.CollapseAll();
        }
        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }
        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        //项目来源
        public void BindDdList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定项目来源
            DataSet dic_proly = dic.GetList(" dic_type='cpr_src'");
            ddsource.DataValueField = "id";
            ddsource.DataTextField = "dic_name";
            ddsource.DataSource = dic_proly;
            ddsource.DataBind();
           // ddsource.Items.Insert(0, new ListItem("---请选择项目来源---", "0"));
        }
        //显示项目信息
        public void GetProInfo()
        {
            //得到审批
            string sql = @"SELECT top 1 ChangeDetail FROM cm_ProjectAuditRepeat WHERE ProjectSysNo=" + int.Parse(strProId) + " ORDER BY SysNo desc";
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(int.Parse(strProId));
            //项目名称
            //if (o != null)
            //{
            //    if (o.ToString().Split(',').Length > 5)
            //    {
            //        if (o.ToString().Split(',')[5].Length > 0)
            //        {
            //            this.txt_name.Disabled = true;
            //        }
            //    }
            //}
            this.txt_name.Value = pro_model.pro_name == null ? "" : pro_model.pro_name.Trim();

            //关联合同
            this.txt_reletive.Value = pro_model.Project_reletive == null ? "" : pro_model.Project_reletive.Trim();
            //管理级别
            string level = pro_model.pro_level.ToString();
            //if (o != null)
            //{
            //    if (o.ToString().Split(',').Length > 9)
            //    {
            //        if (o.ToString().Split(',')[9].Trim() != level)
            //        {
            //            this.radio_suo.Disabled = false;
            //            this.radio_yuan.Disabled = false;
            //        }
            //    }

            //}
            if (level.Trim() == "0")
            {
                this.radio_yuan.Checked = true;
            }
            else if (level.Trim() == "1")
            {
                this.radio_suo.Checked = true;
            }
            //审核级别
            string[] auditlevel = pro_model.AuditLevel.Split(new char[] { ',' }, StringSplitOptions.None);
            //if (o != null)
            //{
            //    if (o.ToString().Split(',').Length > 11)
            //    {
            //        if (o.ToString().Split(',')[10].Trim() != auditlevel[0] && o.ToString().Split(',')[11].Trim() != auditlevel[1])
            //        {
            //            this.audit_suo.Disabled = true;
            //            this.audit_yuan.Disabled = true;
            //        }
            //    }

            //}
            if (auditlevel.Length > 0)
            {
                this.audit_yuan.Checked = auditlevel[0].Trim() == "1" ? true : false;
                if (auditlevel.Length > 1)
                {
                    this.audit_suo.Checked = auditlevel[1].Trim() == "1" ? true : false;
                }
            }


            //建筑级别
            string BuildType = pro_model.BuildType.Trim();
            //if (o != null)
            //{
            //    if (o.ToString().Split(',').Length > 8)
            //    {
            //        if (o.ToString().Split(',')[8].Trim() != BuildType)
            //        {
            //            this.drp_buildtype.Enabled = true;
            //        }
            //    }

            //}
            this.drp_buildtype.Items.FindByText(BuildType).Selected = true;
            //建设单位
            this.txtbuildUnit.Value = pro_model.pro_buildUnit == null ? "" : pro_model.pro_buildUnit.Trim();
            //承接部门
            //if (o != null)
            //{
            //    if (o.ToString().Split(',')[4].Length > 0)
            //    {
            //        //
            //    }
            //    else
            //    {
            //        this.sch_unit.InnerText = "";
            //    }
            //}
            this.txt_unit.Value = pro_model.Unit == null ? "" : pro_model.Unit.Trim();
            this.hid_unit.Value = pro_model.Unit == null ? "" : pro_model.Unit.Trim();
            //建设地点
            this.txtbuildAddress.Value = pro_model.BuildAddress == null ? "" : pro_model.BuildAddress.Trim();
            //建设规模
            this.txt_scale.Value = pro_model.ProjectScale == null ? "" : pro_model.ProjectScale.ToString();
            //项目结构形式
            string StrStruct = pro_model.pro_StruType == null ? "" : pro_model.pro_StruType.Trim();
            //update 20130530 qpl  注销  20140115
            //LoadStructData(StrStruct, this.asTreeviewStruct);
            //结构形式值 qpl  20140115
            StructString = StrStruct;
            //建筑分类
            string BuildStructType = pro_model.pro_kinds == null ? "" : pro_model.pro_kinds.Trim();
            //update 20130530 qpl  注销  20140115
            //LoadStructData(BuildStructType, this.asTreeviewStructType);
            //建筑分类值  qpl  20140115
            StructTypeString = BuildStructType;
            //显示项目阶段
            str_jd = pro_model.pro_status;
            //if (o != null)
            //{
            //    if (o.ToString().Split(',').Length > 7)
            //    {
            //        if (o.ToString().Split(',')[7].Trim().Length > 1)
            //        {
            //            this.CheckBox1.Disabled = true;
            //            this.CheckBox2.Disabled = true;
            //            this.CheckBox3.Disabled = true;
            //            this.CheckBox4.Disabled = true;
            //        }
            //    }
            //}
            string[] prostatus = pro_model.pro_status.Split(new char[] { ',' }, StringSplitOptions.None);

            if (prostatus[0].Trim() == "方案设计")
            {
                CheckBox1.Checked = true;
            }
            if (prostatus.Length > 1)
            {
                if (prostatus[1].Trim() == "初步设计")
                {
                    CheckBox2.Checked = true;
                }
            }
            if (prostatus.Length > 2)
            {
                if (prostatus[2].Trim() == "施工图设计")
                {
                    CheckBox3.Checked = true;
                }
            }
            if (prostatus.Length > 3)
            {
                if (prostatus[3].Trim() == "其他")
                {
                    CheckBox4.Checked = true;
                }
            }

            //项目来源
            string pro_src = pro_model.Pro_src.ToString();
            if (this.ddsource.Items.FindByValue(pro_src) != null)
            {
                this.ddsource.Items.FindByValue(pro_src).Selected = true;
            }
            //合同额
            //if (o != null)
            //{
            //    if (o.ToString().Split(',')[3].Length > 0)
            //    {
            //        this.txtproAcount.Disabled = true;
            //    }
            //}
            this.txtproAcount.Value = pro_model.Cpr_Acount == null ? "" : pro_model.Cpr_Acount.ToString();
            //行业性质
            if (!string.IsNullOrEmpty(pro_model.Industry))
            {
                this.ddProfessionType.Items.FindByText(pro_model.Industry.Trim()).Selected = true;
            }
            //甲方负责人
            this.txt_Aperson.Value = pro_model.ChgJia == null ? "" : pro_model.ChgJia.Trim();
            this.txt_phone.Value = pro_model.Phone == null ? "" : pro_model.Phone.Trim();
            //项目总负责
            this.txt_PMName.Value = pro_model.PMName;
            this.hid_pmname.Value = pro_model.PMName;
            this.txt_PMPhone.Value = pro_model.PMPhone == null ? "" : pro_model.PMPhone.Trim();
            //项目总负责ID
            this.hid_pmuserid.Value = pro_model.PMUserID.ToString();
            //开始结束日期
            this.txt_startdate.Value = Convert.ToDateTime(pro_model.pro_startTime).ToString("yyyy-MM-dd");
            this.txt_finishdate.Value = Convert.ToDateTime(pro_model.pro_finishTime).ToString("yyyy-MM-dd");
            //if (o != null)
            //{
            //    if (o.ToString().Split(',')[0].Length > 0)
            //    {
            //        this.chk_ISHvac.Disabled = true;
            //    }
            //}
            //if (o != null)
            //{
            //    if (o.ToString().Split(',')[1].Length > 0)
            //    {
            //        this.chk_ISTrunEconomy.Disabled = true;
            //    }
            //}
            //if (o != null)
            //{
            //    if (o.ToString().Split(',')[2].Length > 0)
            //    {
            //        this.chk_ISArch.Disabled = true;
            //    }
            //}
            //经济所
            this.chk_ISTrunEconomy.Checked = pro_model.ISTrunEconomy == "1" ? true : false;
            //暖通所
            this.chk_ISHvac.Checked = pro_model.ISHvac == "1" ? true : false;
            //土建所
            this.chk_ISArch.Checked = pro_model.ISArch == "1" ? true : false;

            //项目特征概况
            this.txt_sub.Value = pro_model.ProjSub == null ? "" : pro_model.ProjSub.Trim();
            this.txt_remark.Value = pro_model.pro_Intro == null ? "" : pro_model.pro_Intro.Trim();
            //合同ID
            this.txtcpr_id.Value = pro_model.cprID;

        }
        //通过项目ID 得到效果图
        protected string GetThumdPicByProjid(string projid)
        {
            TG.BLL.cm_AttachInfo bll = new TG.BLL.cm_AttachInfo();
            string strWhere = " Proj_id=" + projid + " AND OwnType='projt'";
            List<TG.Model.cm_AttachInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                return models[models.Count - 1].FileUrl.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// 控件是否可用
        /// </summary>
        /// <param name="result"></param>
        public void ControlReadOnly(bool result)
        {
            this.txt_name.Disabled = result;
            this.txt_phone.Disabled = result;
            this.txt_Aperson.Disabled = result;
            this.txt_finishdate.Disabled = result;
            this.txt_reletive.Disabled = result;
            this.txt_remark.Disabled = result;
            this.txt_scale.Disabled = result;
            this.txt_startdate.Disabled = result;
            this.txt_unit.Disabled = result;
            this.txtbuildAddress.Disabled = result;
            this.txtbuildUnit.Disabled = result;
            this.txtproAcount.Disabled = result;
            this.ddsource.Enabled = result;
            this.CheckBox1.Disabled = result;
            this.CheckBox2.Disabled = result;
            this.CheckBox3.Disabled = result;
            this.asTreeviewStruct.Enabled = result;
            this.asTreeviewStructType.Enabled = result;

            this.txt_PMName.Disabled = result;
            this.txt_PMPhone.Disabled = result;
            this.radio_suo.Disabled = !result;
            this.radio_yuan.Disabled = !result;
            this.drp_buildtype.Enabled = result;
            this.ddProfessionType.Enabled = result;
            this.audit_suo.Disabled = result;
            this.audit_yuan.Disabled = result;
            this.chk_ISTrunEconomy.Disabled = result;
            this.chk_ISHvac.Disabled = result;
            this.chk_ISArch.Disabled = result;
            this.isCanEdit.Value = "0";
        }
        /// <summary>
        /// 控件是否可用
        /// </summary>
        /// <param name="result"></param>
        public void ControlReadOnlyEdit(bool result)
        {
            this.txt_name.Disabled = result;
            this.txt_phone.Disabled = result;
            this.txt_Aperson.Disabled = result;
            this.txt_finishdate.Disabled = result;
            this.txt_reletive.Disabled = result;
            this.txt_remark.Disabled = result;
            this.txt_scale.Disabled = result;
            this.txt_startdate.Disabled = result;
            this.txt_unit.Disabled = result;
            this.txtbuildAddress.Disabled = result;
            this.txtbuildUnit.Disabled = result;
            this.txtproAcount.Disabled = result;
            this.ddsource.Enabled = result;
            this.CheckBox1.Disabled = result;
            this.CheckBox2.Disabled = result;
            this.CheckBox3.Disabled = result;
            this.asTreeviewStruct.Enabled = result;
            this.asTreeviewStructType.Enabled = result;
            this.txt_PMName.Disabled = result;
            this.txt_PMPhone.Disabled = result;
            this.radio_suo.Disabled = !result;
            this.radio_yuan.Disabled = !result;
            this.drp_buildtype.Enabled = result;
            this.ddProfessionType.Enabled = result;
            this.audit_suo.Disabled = result;
            this.audit_yuan.Disabled = result;
            this.chk_ISTrunEconomy.Disabled = result;
            this.chk_ISHvac.Disabled = result;
            this.chk_ISArch.Disabled = result;

            this.isCanEdit.Value = "1";
        }
        /// <summary>
        /// 判断是否已提交审核
        /// </summary>
        public bool CheckAudit()
        {
            bool flag = false;
            int result = TG.BLL.tg_ProjectAuditBP.CheckAudit(int.Parse(strProId));
            if (result > 0)
            {
                flag = false;
            }
            else
            {
                flag = true;
            }
            return flag;
        }

        //获取所有树文字数据
        protected void GetAllNodeTex(ASTreeViewNode nodes, ref List<string> alltext)
        {
            foreach (ASTreeViewNode node in nodes.ChildNodes)
            {
                alltext.Add(node.NodeText);
                if (node.ChildNodes.Count > 0)
                {
                    GetAllNodeTex(node, ref alltext);
                }
            }
        }

        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.asTreeviewStruct.Theme = macOS;
            this.asTreeviewStructType.Theme = macOS;
        }
        //update by 20130530 qpl
        //获取选中树所有节点值并返回值
        protected string GetDropDownTreeCheckedValue(List<ASTreeViewNode> allnodes)
        {
            //最终生成字符串
            string rootvalue = "";
            foreach (ASTreeViewNode node in allnodes)
            {
                string secondvalue = "";
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                    GetSecondNodeList(node, ref secondvalue);
                }
                rootvalue += secondvalue;
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }

            return rootvalue;
        }
        protected void GetSecondNodeList(ASTreeViewNode node, ref string value)
        {
            if (node.ChildNodes.Count > 0)
            {
                //返回值
                StringBuilder sbresult = new StringBuilder();

                foreach (ASTreeViewNode snode in node.ChildNodes)
                {
                    if (snode.CheckedState == ASTreeViewCheckboxState.Checked || snode.CheckedState == ASTreeViewCheckboxState.HalfChecked)
                    {
                        //拼接第二级
                        value = "^" + snode.NodeValue;
                        string subvalue = "";
                        subvalue = value;
                        GetChildNodes(snode, ref subvalue);
                        foreach (string key in sblist)
                        {
                            sbresult.Append(key);
                        }
                        //清空当前列表
                        sblist.Clear();
                    }
                }
                value = sbresult.ToString();
            }
        }
        //查询数据
        List<string> sblist = new List<string>();
        protected void GetChildNodes(ASTreeViewNode node, ref string value)
        {
            StringBuilder sb = new StringBuilder();
            if (node.ChildNodes.Count > 0)
            {
                foreach (ASTreeViewNode childnode in node.ChildNodes)
                {
                    if ((childnode.CheckedState == ASTreeViewCheckboxState.Checked) || (childnode.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                    {
                        string tempvalue = value + "*" + childnode.NodeValue;

                        if (childnode.ChildNodes.Count > 0)
                        {
                            //如果还有子节点，继续遍历
                            GetChildNodes(childnode, ref tempvalue);
                        }
                        else
                        {
                            //添加末节点
                            sb.Append(tempvalue);
                        }
                    }
                }
            }
            else
            {
                //添加末节点
                sb.Append(value);
            }
            //赋值value
            sblist.Add(sb.ToString());
        }
        //加载存储的树节点
        protected void LoadStructData(string strData, ASDropDownTreeView contrl)
        {
            if (!string.IsNullOrEmpty(strData))
            {
                //加载所有选中节点
                List<string> nodelist = new List<string>();
                //第一级
                string[] strarray = strData.Split('+');

                for (int i = 0; i < strarray.Length; i++)
                {
                    //特殊第二级
                    string[] strsubarray = strarray[i].Split('^');
                    for (int j = 0; j < strsubarray.Length; j++)
                    {
                        //递归的三级以后
                        string[] strendarray = strsubarray[j].Split('*');
                        nodelist.Add(strendarray[strendarray.Length - 1]);
                    }
                }
                //选中节点
                foreach (string nodevalue in nodelist)
                {
                    if (contrl.FindByValue(nodevalue) != null)
                    {
                        contrl.FindByValue(nodevalue).CheckedState = ASTreeViewCheckboxState.Checked;
                    }
                }
            }
        }
        //获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }
    }
}