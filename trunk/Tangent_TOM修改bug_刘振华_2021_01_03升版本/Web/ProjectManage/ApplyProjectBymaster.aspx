﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ApplyProjectBymaster.aspx.cs" Inherits="TG.Web.ProjectManage.ApplyProjectBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/tipsy/tipsy.css" rel="Stylesheet" type="text/css" />
    <link href="../css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
     <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Common/AutoComplete.js"></script>
    <script src="../js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jQuery-Plugs.js"></script>
    <script type="text/javascript" src="../js/Common/SendMessageCommon.js"></script>
    <script src="../js/ProjectMamage/ApplyProjectBymaster.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ProjectMamage/ApplyProject_jq.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>项目审批</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="#">项目信息管理</a><i class="fa fa-angle-right"> </i><a href="#">项目管理</a><i
        class="fa fa-angle-right"> </i><a href="#">项目审批</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询项目审批
                    </div>
                     
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>项目年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year" Width="90px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>项目名称:</td>
                            <td>
                                <input type="text" class="form-control input-sm" id="txt_keyname" runat="server" /></td>
                            <td>录入时间:<input type="text" name="txt_date" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" /></td>
                            <td>截止时间:<input type="text" name="txt_date" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" /></td>
                            <td>
                                <input type="button" class="btn blue " value="查询" id="btn_search" /></td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue  ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目审批列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                    <div class="btn-group">
                        <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择显示列
									<i class="fa fa-angle-down"></i>
                        </a>
                        <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsid">
                            <label>
                                <input type="checkbox" value="all" />全选
                            </label>
                            <%=ColumnsContent %>
                        </div>
                    </div>
                        <asp:Button ID="btn_Output" runat="server" Text="导出Excel" CssClass="btn red btn-sm" OnClick="btn_Output_Click" />

                </div>
                
                </div>
                <div class="portlet-body form" style="display: block;">
                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="HiddenField1" runat="server" Value="" />
    <!--HiddenArea-->
    <input type="hidden" id="HiddenUserSysNo" value="<%=UserSysNo %>" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <div id="AuditUser" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
