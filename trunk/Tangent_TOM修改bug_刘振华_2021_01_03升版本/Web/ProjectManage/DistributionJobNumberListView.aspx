﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DistributionJobNumberListView.aspx.cs"
	Inherits="TG.Web.ProjectManage.DistributionJobNumberListView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>项目工号分配列表</title>
	<link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
	<link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.title
		{
			width: 100px;
		}
		.content
		{
			width: 300px;
		}
		.aa
		{
			-moz-opacity: 0.5;
			filter: alpha(opacity=50);
		}
		
		body
		{
			font: 62.5% "Trebuchet MS" , sans-serif;
			color: Black;
		}
		#tabs
		{
			color: Black;
		}
	</style>
	<style type="text/css">
		.style1
		{
			width: 5%;
		}
		.style2
		{
			height: 33px;
			border-top: solid 1px #999;
			font-size: 12px;
			background: url(../images/bg_head.gif) repeat-x;
		}
		.btn
		{
			background-image: url(                                                 '../Images/bg.gif' );
			width: 80px;
			border: 1px solid #0066FF;
			text-align: center;
			height: 20px;
		}
	</style>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
	<script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(Function(){

     CommonControl.SetFormWidth();
    });
    </script>
</head>
<body bgcolor="f0f0f0">
	<form id="form1" runat="server">
	<div style="position: absolute;" id="mm">
		&nbsp;
	</div>
	<table class="cls_container">
		<tr>
			<td class="style2">
				&nbsp;&nbsp;当前位置：[项目工号分配列表]
			</td>
		</tr>
	</table>
	<div id="tabs-1">
		<div class="cls_data">
			<table class="cls_content_head">
				<tr>
					<td style="width: 15%;" align="center">
						项目名称
					</td>
					<td style="width: 8%" align="center">
						提交日期
					</td>
					<td style="width: 15%" align="center">
						项目工号
					</td>
					<td style="width: 15%" align="center">
						查看
					</td>
					<td style="width: 15%" align="center">
						提交申请
					</td>
				</tr>
			</table>
			<asp:GridView ID="DistributionJobNumberGridView" runat="server" Width="100%" ShowHeader="False"
				AutoGenerateColumns="False">
				<Columns>
					<asp:BoundField DataField="ProjectName" ShowHeader="False">
						<ItemStyle Width="15%" HorizontalAlign="Center" />
					</asp:BoundField>
					<asp:BoundField DataField="SubmitDate" ShowHeader="False">
						<ItemStyle Width="8%" />
					</asp:BoundField>
					<asp:BoundField DataField="ProNumber" ShowHeader="False">
						<ItemStyle Width="15%" />
					</asp:BoundField>
					<asp:TemplateField ShowHeader="False">
						<ItemStyle Width="15%" />
					</asp:TemplateField>
					<asp:TemplateField ShowHeader="False">
						<ItemTemplate>
							<a href="/ProjectManage/DistributionJobNumberListView.aspx?ProjectSysNo=<%#Eval("ProjectSysNo") %>&ProjectName=<%#Eval("ProjectName") %>">
								提交申请</a>
						</ItemTemplate>
						<ItemStyle Width="15%" HorizontalAlign="Center" />
					</asp:TemplateField>
				</Columns>
			</asp:GridView>
		</div>
		<div class="cls_data_bottom">
			<%--<cc1:pager id="Pager1" runat="server" controltopagination="gv_project" pagesize="15"
				height="24px" />--%>
		</div>
	</div>
	</form>
</body>
</html>
