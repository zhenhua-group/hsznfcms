﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using Geekees.Common.Controls;
using System.Xml;
using AjaxPro;

namespace TG.Web.ProjectManage
{
    public partial class ProjectListBymaster : PageBase
    {
        public string ColumnsContent
        {
            get;
            set;
        }
        public string asTreeviewdrpunitObjID
        {
            get
            {
                return this.drp_unit.GetClientTreeObjectId();
            }
        }
        public string asTreeviewStructObjID
        {
            get
            {
                return this.asTreeviewStruct.GetClientTreeObjectId();
            }
        }
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructType.GetClientTreeObjectId();
            }
        }

        //生产经营项目
        protected TG.BLL.cm_Project CProject
        {
            get
            {
                return new TG.BLL.cm_Project();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProjectListBymaster));
            if (!IsPostBack)
            {
                //绑定建筑类别
                BindBuildType();
                //绑定字段
                BindColumns();
                //绑定部门
                BindUnit();
                LoadUnitTree();
                //默认全选
                SelectedCurUnit();
                //绑定年份
                BindYear();
                //工程来源
                BindDdList();
                //行业性质
                BindXzList();
                //默认选中年份
                SelectCurrentYear();
                //绑定项目列表
                BindProject();
                //绑定权限
                BindPreviewPower();
                //绑定建筑结构样式修改时
                BindStructTypeEdit();
                //绑定建筑分类修改时
                BindBuildStuctTypeEdit();

            }
        }
        //合同建筑类别
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
            this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "dic_Name";
            this.drp_buildtype.DataBind();
            //this.ddrank.DataSource = bll_dic.GetList(str_where);
            //this.ddrank.DataTextField = "dic_Name";
            //this.ddrank.DataValueField = "dic_Name";
            //this.ddrank.DataBind();
        }
        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { "全部部门" };
            this.drp_unit.CheckNodes(curUnit, true);
        }
        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "全部部门");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString().Trim(), dr["unit_Name"].ToString().Trim());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
            else
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "全部部门");
                root.AppendChild(firstnode);
                Model.tg_unit unit = new TG.BLL.tg_unit().GetModel(UserUnitNo);
                if (unit != null)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(unit.unit_Name.Trim(), unit.unit_ID.ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }

            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drp_unit.Theme = macOS;
            this.asTreeviewStruct.Theme = macOS;
            this.asTreeviewStructType.Theme = macOS;
        }
        protected void BindColumns()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("甲方负责人", "ChgJia");
            dic.Add("项目名称", "pro_name");
            dic.Add("建筑类别", "BuildType");
            dic.Add("合同信息", "Project_reletive");
            dic.Add("合同额(万元)", "Cpr_Acount");
            dic.Add("开始日期", "qdrq");
            dic.Add("结束日期", "wcrq");
            dic.Add("执行设总", "PMUserName");
            dic.Add("录入时间", "lrsj");

            dic.Add("项目工程号", "Pro_number");
            dic.Add("管理级别", "pro_jb");
            dic.Add("审核级别", "AuditLevel");
            dic.Add("建设单位", "pro_buildUnit");
            dic.Add("建设地点", "BuildAddress");
            dic.Add("建设规模", "ProjectScale");
            dic.Add("承接部门", "Unit");
            dic.Add("结构形式", "pro_StruType");
            dic.Add("建筑分类", "pro_kinds");
            dic.Add("设计阶段", "pro_status");
            dic.Add("项目来源", "pro_from");
            dic.Add("行业性质", "Industry");
            dic.Add("甲方负责人电话", "Phone");
            dic.Add("执行设总电话", "PMPhone");
            dic.Add("参与部门", "ISTrunEconomy");
            dic.Add("项目概况", "ProjSub");
            dic.Add("项目备注", "pro_Intro");
            dic.Add("录入人", "InsertUser");

            foreach (KeyValuePair<string, string> pair in dic)
            {
                ColumnsContent += "<label><input type='checkbox' rel='" + pair.Key + "' value='" + pair.Value + "' />" + pair.Key + "</label>";
            }


            //  BindProInfoConfig(this.asTreeviewStruct.RootNode,dic);
            //this.asTreeviewStruct.CollapseAll();           
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = " 1=1 ";
            if (base.RolePowerParameterEntity != null)
            {

                //个人
                if (base.RolePowerParameterEntity.PreviewPattern == 0)
                {
                    strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
                }
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    strWhere = " unit_ID =" + UserUnitNo;
                }
                else
                {
                    strWhere = " 1=1 ";
                }
            }
            else
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }
        //根据条件查询
        public void BindProject()
        {
            StringBuilder sb = new StringBuilder("");

            //检查权限
            GetPreviewPowerSql(ref sb);

            this.hid_where.Value = sb.ToString();


        }
        //删除
        protected void btn_Delpro_Click(object sender, ImageClickEventArgs e)
        {
            TG.BLL.cm_Project proE = new TG.BLL.cm_Project();
            TG.Model.cm_Project model = new Model.cm_Project();
            string str_idList = Del_Id();
            if (str_idList.Trim().Length == 0)
                return;

            ArrayList listAudit = proE.DeleteList(str_idList);

            //存在审批中的合同
            if (listAudit.Count > 0)
            {
                string message = "";

                for (int i = 0; i < listAudit.Count; i++)
                {
                    model = proE.GetModel(int.Parse(listAudit[i].ToString()));
                    if (model != null)
                    {
                        message = message + model.pro_name + "项目在审批中，或者审批完毕\\n ";
                    }
                }
                message = message + "不能删除";

                //弹出提示
                TG.Common.MessageBox.Show(this, message);
            }

            BindProject();
        }
        /// <summary>
        /// 获取选中项的ID
        /// </summary>
        /// <returns></returns>
        public string Del_Id()
        {
            string strDelId = "";
            //foreach (GridViewRow gv in gv_project.Rows)
            //{
            //    CheckBox ck = (CheckBox)gv.FindControl("chk_Pro");
            //    if (ck.Checked)
            //    {
            //        HiddenField hfproId = (HiddenField)gv.FindControl("bs_project_Id");
            //        strDelId += hfproId.Value + ",";
            //    }
            //}
            //if (strDelId.IndexOf(",") > -1)
            //{
            //    strDelId = strDelId.Substring(0, strDelId.LastIndexOf(","));
            //}
            return strDelId;
        }
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Search_Click(object sender, ImageClickEventArgs e)
        {
            //查询项目信息
            BindDataSetPageIndex();
        }

        protected void gv_project_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //行绑定代码
            }
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindProject();
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }

        //生产部门改变重新绑定数据
        protected void drp_unit_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataSetPageIndex();
        }
        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>
        protected void BindDataSetPageIndex()
        {
            // this.AspNetPager1.CurrentPageIndex = 0;
            //绑定数据
            BindProject();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();

            string modelPath = " ~/TemplateXls/ProjectList.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);
            //读取标题
            string str_columnschinaname = this.HiddenColsName.Value;
            string str_columnsname = this.HiddenColsVal.Value;
            IRow dataRowTitle = ws.GetRow(1);
            if (!string.IsNullOrEmpty(str_columnschinaname))
            {
                string[] columnsnamelist = str_columnschinaname.Split(',');
                for (int j = 0; j < columnsnamelist.Length; j++)
                {
                    ICell celltitle = dataRowTitle.CreateCell(j + 10);
                    celltitle.CellStyle = style2;
                    celltitle.SetCellValue(columnsnamelist[j].Trim());
                }
            }
            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ChgJia"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_name"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["BuildType"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Project_reletive"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cpr_Acount"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["pro_startTime"]).ToString("yyyy_MM-dd"));

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["pro_finishTime"]).ToString("yyyy_MM-dd"));

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["PMName"].ToString());

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                if (!string.IsNullOrEmpty(dt.Rows[i]["InsertDate"].ToString()))
                {
                    cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["InsertDate"]).ToString("yyyy-MM-dd"));
                }
                if (!string.IsNullOrEmpty(str_columnsname))
                {
                    string[] columnslist = str_columnsname.Split(',');
                    for (int j = 0; j < columnslist.Length; j++)
                    {
                        if (columnslist[j] == "AuditLevel")
                        {
                            cell = dataRow.CreateCell(j + 10);
                            cell.CellStyle = style2;
                            string str = "";
                            if (dt.Rows[i][columnslist[j].Trim()].ToString().Contains("1,0"))
                            {
                                str = "所审";
                            }
                            else if (dt.Rows[i][columnslist[j].Trim()].ToString().Contains("0,1"))
                            {
                                str = "院审";
                            }
                            else if (dt.Rows[i][columnslist[j].Trim()].ToString().Contains("1,1"))
                            {
                                str = "院审,所审";
                            }
                            cell.SetCellValue(str);
                        }
                        else
                        {
                            cell = dataRow.CreateCell(j + 10);
                            cell.CellStyle = style2;
                            cell.SetCellValue(dt.Rows[i][columnslist[j].Trim()].ToString());
                        }

                    }
                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("项目信息列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {
            StringBuilder sb = new StringBuilder("");
            //名字不为空
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.Append(" AND pro_Name LIKE '%" + keyname + "%'  ");
            }
            //绑定单位
            List<ASTreeViewNode> checkedNods = this.drp_unit.GetCheckedNodes(false);
            //全没选
            string unitname = "";
            if (checkedNods.Count == 0)
            {
                //循环部门
                DataTable dtu = GetUnit();
                //部门名称

                foreach (DataRow dr in dtu.Rows)
                {
                    unitname += "'" + dr["unit_Name"].ToString().Trim() + "',";

                }
            }
            else
            {
                foreach (ASTreeViewNode node in checkedNods)
                {
                    if (node.NodeValue == "0")
                        continue;
                    //部门名称
                    unitname += "'" + node.NodeText.Trim() + "',";

                }
            }
            unitname = unitname.Substring(0, unitname.Length - 1);
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            sb.Append(" AND Unit in(" + unitname.Trim() + ") ");
            //}
            //按照年份
            if (this.drp_year.SelectedIndex != 0)
            {
                sb.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            }
            //检查权限
            //个人
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
            //建设类别
            string ddrank = this.drp_buildtype.SelectedItem.Text;
            //合同额
            //string txtproAcount = this.txtproAcount.Value;
            //string txtproAcount2 = this.txtproAcount2.Value;
            //建设规模
            string txt_buildArea = this.txt_buildArea.Value;
            string txt_buildArea2 = this.txt_buildArea2.Value;
            //合同额
            //if (!string.IsNullOrEmpty(txtproAcount) && string.IsNullOrEmpty(txtproAcount2))
            //{

            //    sb.Append("and ( cpr_Acount >= " + txtproAcount.Trim() + " )");
            //}
            //else if (string.IsNullOrEmpty(txtproAcount) && !string.IsNullOrEmpty(txtproAcount2))
            //{

            //    sb.Append(" and ( cpr_Acount <= " + txtproAcount2.Trim() + " )");
            //}
            //else if (!string.IsNullOrEmpty(txtproAcount) && !string.IsNullOrEmpty(txtproAcount2))
            //{

            //    sb.Append("and (cpr_Acount>=" + txtproAcount.Trim() + " AND cpr_Acount <= " + txtproAcount2.Trim() + ") ");
            //}
            //建设规模
            if (!string.IsNullOrEmpty(txt_buildArea) && string.IsNullOrEmpty(txt_buildArea2))
            {

                sb.Append(" and ( ProjectScale >=" + txt_buildArea.Trim() + " )");
            }
            else if (string.IsNullOrEmpty(txt_buildArea) && !string.IsNullOrEmpty(txt_buildArea2))
            {

                sb.Append(" and ( ProjectScale <=" + txt_buildArea2.Trim() + " )");
            }
            else if (!string.IsNullOrEmpty(txt_buildArea) && !string.IsNullOrEmpty(txt_buildArea2))
            {

                sb.Append(" and (ProjectScale >=" + txt_buildArea.Trim() + " AND ProjectScale <=" + txt_buildArea2.Trim() + ") ");
            }
            if (!string.IsNullOrEmpty(ddrank) && !ddrank.Contains("选择类别"))
            {
                sb.Append(" and ( BuildType=N'" + ddrank.Trim() + "' )");
            }
            //录入时间  
            string startTime = txt_start.Value;//录入时间
            string endTime = txt_end.Value;//录入时间
            if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                sb.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
            }
            else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                sb.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
            }
            else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                sb.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
            }
            TG.BLL.cm_Project bll = new TG.BLL.cm_Project();

            DataTable dt = bll.GetProjectExportInfo(sb.ToString()).Tables[0];

            return dt;
        }


        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }

        //获取所有树文字数据
        protected void GetAllNodeTex(ASTreeViewNode nodes, ref List<string> alltext)
        {
            foreach (ASTreeViewNode node in nodes.ChildNodes)
            {
                alltext.Add(node.NodeText);
                if (node.ChildNodes.Count > 0)
                {
                    GetAllNodeTex(node, ref alltext);
                }
            }
        }
        [AjaxMethod]
        protected void output_Click(object sender, EventArgs e)
        {
            GetDropDownTreeCheckedValue();
        }
        [AjaxMethod]
        public string GetDropDownTreeCheckedValue()
        {
            string rootvalue = "";

            List<ASTreeViewNode> allnodes = this.asTreeviewStruct.RootNode.ChildNodes;
            //最终生成字符串

            foreach (ASTreeViewNode node in allnodes)
            {
                string secondvalue = "";
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                    GetSecondNodeList(node, ref secondvalue);
                }
                rootvalue += secondvalue;
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }
            return rootvalue;
        }
        protected void GetSecondNodeList(ASTreeViewNode node, ref string value)
        {
            if (node.ChildNodes.Count > 0)
            {
                //返回值
                StringBuilder sbresult = new StringBuilder();

                foreach (ASTreeViewNode snode in node.ChildNodes)
                {
                    if (snode.CheckedState == ASTreeViewCheckboxState.Checked || snode.CheckedState == ASTreeViewCheckboxState.HalfChecked)
                    {
                        //拼接第二级
                        value = "^" + snode.NodeValue;
                        string subvalue = "";
                        subvalue = value;
                        GetChildNodes(snode, ref subvalue);
                        foreach (string key in sblist)
                        {
                            sbresult.Append(key);
                        }
                        //清空当前列表
                        sblist.Clear();
                    }
                }
                value = sbresult.ToString();
            }
        }
        //查询数据
        List<string> sblist = new List<string>();
        protected void GetChildNodes(ASTreeViewNode node, ref string value)
        {
            StringBuilder sb = new StringBuilder();
            if (node.ChildNodes.Count > 0)
            {
                foreach (ASTreeViewNode childnode in node.ChildNodes)
                {
                    if ((childnode.CheckedState == ASTreeViewCheckboxState.Checked) || (childnode.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                    {
                        string tempvalue = value + "*" + childnode.NodeValue;

                        if (childnode.ChildNodes.Count > 0)
                        {
                            //如果还有子节点，继续遍历
                            GetChildNodes(childnode, ref tempvalue);
                        }
                        else
                        {
                            //添加末节点
                            sb.Append(tempvalue);
                        }
                    }
                }
            }
            else
            {
                //添加末节点
                sb.Append(value);
            }
            //赋值value
            sblist.Add(sb.ToString());
        }
        //建筑分类
        protected void BindBuildStuctTypeEdit()
        {
            BindProInfoConfig("BuildType", this.asTreeviewStructType.RootNode);
            this.asTreeviewStructType.CollapseAll();
        }
        //结构形式
        protected void BindStructTypeEdit()
        {
            BindProInfoConfig("StructType", this.asTreeviewStruct.RootNode);
            this.asTreeviewStruct.CollapseAll();
        }
        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }

        //绑定承接部门
        public void BindUnit()
        {
            //承接部门 
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            //不显示的单位
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                strWhere += " 1=1 AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            else
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            //this.drp_unitS.DataSource = bll_unit.GetList(strWhere);
            //this.drp_unitS.DataTextField = "unit_Name";
            //this.drp_unitS.DataValueField = "unit_ID";
            //this.drp_unitS.DataBind();
        }

        public void BindDdList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定项目来源
            DataSet dic_proly = dic.GetList(" dic_type='cpr_src'");
            ddsource.DataValueField = "id";
            ddsource.DataTextField = "dic_name";
            ddsource.DataSource = dic_proly;
            ddsource.DataBind();
        }
        //项目阶段
        protected void BindJDList()
        {
            //TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //DataSet dic_proly = dic.GetList(" dic_type='cpr_jd'");
            //chk_jd.DataValueField = "id";
            //chk_jd.DataTextField = "dic_name";
            //chk_jd.DataSource = dic_proly;
            //chk_jd.DataBind();
        }
        //绑定行业性质
        protected void BindXzList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            DataSet dic_proly = dic.GetList(" dic_type='cpr_hyxz'");
            ddType.DataValueField = "id";
            ddType.DataTextField = "dic_name";
            ddType.DataSource = dic_proly;
            ddType.DataBind();
        }
        public void GetList()
        {
            StringBuilder sb = new StringBuilder("");
            // //组合查询
            // if (this.txt_proName.Text.Trim() != "")
            // {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txt_proName.Text.Trim());
            //     sb.Append(" AND pro_name LIKE '%" + this.txt_proName.Text.Trim() + "%' ");
            // }
            // if (this.txt_buildUnit.Value.Trim() != "")
            // {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txt_buildUnit.Value.Trim());
            //     sb.Append(" AND pro_buildUnit LIKE '%" + this.txt_proName.Text.Trim() + "%'  ");
            // }
            // if (this.txtaddress.Text.Trim() != "")
            // {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txtaddress.Text.Trim());
            //     sb.Append(" AND BuildAddress LIKE '%" + this.txtaddress.Text.Trim() + "%'  ");
            // }
            // if (this.drp_unit.SelectedIndex != 0)
            // {
            //     sb.Append(" AND Unit  LIKE '%" + this.drp_unit.SelectedItem.Text.Trim() + "%'  ");
            // }
            // if (this.ddType.SelectedIndex != 0)
            // {
            //     sb.Append(" AND Industry LIKE '%" + this.ddType.SelectedItem.Text.Trim() + "%'  ");
            // }
            // if (this.ddsource.SelectedIndex != 0)
            // {
            //     sb.Append(" AND Pro_src=" + this.ddsource.SelectedItem.Value.Trim() + "  ");
            // }
            // if (this.txt_Aperson.Text.Trim() != "")
            // {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txt_Aperson.Text.Trim());
            //     sb.Append(" AND ChgJia LIKE '%" + this.txt_Aperson.Text.Trim() + "%'  ");
            // }
            // if (this.txt_phone.Text.Trim() != "")
            // {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txt_phone.Text.Trim());
            //     sb.Append(" AND Phone LIKE '%" + this.txt_phone.Text.Trim() + "%'  ");
            // }
            // if (this.txt_PMName.Text.Trim() != "")
            // {
            //     string keyname = TG.Common.StringPlus.SqlSplit(this.txt_PMName.Text.Trim());
            //     sb.Append(" AND PMName LIKE '%" + this.txt_PMName.Text.Trim() + "%'  ");
            // }
            // //合同额
            // if (this.txtproAcount.Text != "" && this.txtproAcount2.Text == "")
            // {
            //     sb.Append(" AND cpr_Acount >= " + txtproAcount.Text + " ");
            // }
            // else if (this.txtproAcount.Text == "" && this.txtproAcount2.Text != "")
            // {
            //     sb.Append(" AND cpr_Acount <= " + txtproAcount2.Text + " ");
            // }
            // else if (this.txtproAcount.Text != "" && this.txtproAcount2.Text != "")
            // {
            //     sb.Append(" AND (cpr_Acount>=" + this.txtproAcount.Text + " AND cpr_Acount <= " + txtproAcount2.Text + ") ");
            // }
            // //建设规模
            // if (this.txt_buildArea.Text != "" && this.txt_buildArea2.Text == "")
            // {
            //     sb.Append(" AND ProjectScale >=" + txt_buildArea.Text + " ");
            // }
            // else if (this.txt_buildArea.Text == "" && this.txt_buildArea2.Text != "")
            // {
            //     sb.Append(" AND ProjectScale <=" + txt_buildArea2.Text + " ");
            // }
            // else if (this.txt_buildArea.Text != "" && this.txt_buildArea2.Text != "")
            // {
            //     sb.Append(" AND (ProjectScale >=" + txt_buildArea.Text + " AND ProjectScale <=" + this.txt_buildArea2.Text + ") ");
            // }
            // //时间
            // if (this.txt_signdate.Text != "")
            // {
            //     sb.Append(" AND (pro_startTime BETWEEN '" + this.txt_signdate.Text.Trim() + "' AND '" + this.txt_signdate2.Text + "') ");
            // }
            // if (this.ddrank.SelectedIndex != 0)
            // {
            //     sb.Append(" AND BuildType=N'" + this.ddrank.SelectedItem.Text.Trim() + "' ");
            // }
            // if (managerLevelDropDownList.SelectedValue.Trim() != "-1")
            // {
            //     sb.Append(" AND pro_level=" + managerLevelDropDownList.SelectedItem.Value.Trim() + "  ");
            // }
            // if (string.IsNullOrEmpty(txtproAcount.Text) == false && new Regex("^[0-9]+$").Match(txtproAcount.Text).Success && txtproAcount.Text.Trim() != "0")
            // {
            //     sb.Append(" AND Cpr_Acount = " + txtproAcount.Text.Trim() + " ");
            // }
            // //项目阶段信息
            //// string str_jd = "";

            // //bool flag_jd = false;
            // //for (int i = 0; i < this.chk_jd.Items.Count; i++)
            // //{
            // //    if (chk_jd.Items[i].Selected)
            // //    {
            // //        flag_jd = true;
            // //    }
            // //}
            // //if (flag_jd)
            // //{
            // //    foreach (ListItem jd in this.chk_jd.Items)
            // //    {
            // //        if (jd.Selected)
            // //        {
            // //            str_jd += jd.Text + ",";
            // //        }
            // //        //else
            // //        //{
            // //        //    str_jd += ",";
            // //        //}
            // //    }
            // //}

            // //项目阶段
            // string str_jd = "";
            // if (CheckBox1.Checked)
            // {
            //     str_jd += CheckBox1.Text + ",";
            // }
            // //else
            // //{
            // //    str_jd += " ,";
            // //}
            // if (CheckBox2.Checked)
            // {
            //     str_jd += CheckBox2.Text + ",";
            // }
            // //else
            // //{
            // //    str_jd += " ,";
            // //}
            // if (CheckBox3.Checked)
            // {
            //     str_jd += CheckBox3.Text + ",";
            // }
            // //else
            // //{
            // //    str_jd += " ,";
            // //}
            // if (CheckBox4.Checked)
            // {
            //     str_jd += CheckBox4.Text + ",";
            // }
            // //else
            // //{
            // //    str_jd += " ,";
            // //}

            // if (str_jd != "")
            // {
            //     sb.Append(" AND pro_status LIKE '%" + str_jd + "%' ");
            // }

            //  if (sb.ToString() != "")
            //  {
            //检查权限
            GetPreviewPowerSql(ref sb);
            this.hid_where.Value = sb.ToString();
            //所有记录数
            //  this.AspNetPager1.RecordCount = int.Parse(bll_pro.GetListPageProcCount(sb.ToString()).ToString());
            //排序
            // string orderString = " Order by " + OrderColumn + " " + Order;
            //  sb.Append(orderString);
            //绑定
            //this.gv_project.DataSource = bll_pro.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            // this.gv_project.DataBind();
            //  }
        }
    }
}