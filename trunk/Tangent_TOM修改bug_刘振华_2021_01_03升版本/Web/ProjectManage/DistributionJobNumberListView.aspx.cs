﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using TG.Model;

namespace TG.Web.ProjectManage
{
	public partial class DistributionJobNumberListView : PageBase
	{

		public int ProjectSysNo
		{
			get
			{
				int projectSysNo = 0;
				int.TryParse(Request["projectSysNo"], out projectSysNo);
				return projectSysNo;
			}
		}

		public string ProjectName
		{
			get
			{
				return Request["ProjectName"];
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (ProjectSysNo != 0)
			{
				SendRequest();
			}

			this.DistributionJobNumberGridView.DataSource = TG.BLL.tg_ProjectAuditBP.GetDistribuionJobNumberList();
			this.DistributionJobNumberGridView.DataBind();
		}

		private void SendRequest()
		{
			//TG.BLL.cm_projectNumber projectNumberBP = new TG.BLL.cm_projectNumber();

			new TG.BLL.cm_projectNumber().Add(new TG.Model.cm_projectNumber
			{
				pro_id = ProjectSysNo,
				SubmintPerson = UserSysNo.ToString(),
				SubmitDate = DateTime.Now
			});


			TG.Model.cm_ProjectDistributionJobNumberConfigEntity distributionJobNumberConfig = new TG.BLL.cm_ProjectDistributionJobNumberConfigBP().GetDistributionJobNumberConfig(1);

			SysMessageViewEntity sysMessage = new SysMessageViewEntity
			{
				MsgType = 3,
				ReferenceSysNo = ProjectSysNo.ToString(),
				InUser = UserSysNo,
				FromUser = UserSysNo,
				ToRole = distributionJobNumberConfig.RoleSysNo.ToString(),
				MessageContent = string.Format("关于 \"{0}\"的工号申请消息！", ProjectName)
			};
			new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessage);
		}
	}
}
