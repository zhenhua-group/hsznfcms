﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using TG.Model;
using AjaxPro;
using TG.BLL;
using System.Xml;
using TG.Web.ProjectManage.ProjectAudit;

namespace TG.Web.ProjectManage
{
    public partial class ProjectAuditEditBymaster : PageBase
    {
        /// <summary>
        /// 项目SysNo
        /// </summary>
        /// 
        public int ProjectSysNo { get; set; }
        public string Creattable { get; set; }
        TG.BLL.cm_Project proBll = new TG.BLL.cm_Project();
        TG.BLL.tg_project tgproj = new TG.BLL.tg_project();
        /// <summary>
        /// 项目审核SysNo
        /// </summary>
        public int ProjectAuditSysNo
        {
            get
            {
                int projectAuditSysNo = 0;
                int.TryParse(Request["projectAuditSysNo"], out projectAuditSysNo);
                return projectAuditSysNo;
            }
        }
        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        //类型
        public string MsgType
        {
            get
            {
                return Request["msyType"];
            }
        }
        /// <summary>
        /// 消息审批状态
        /// </summary>
        public string MessageStatus
        {
            get
            {
                return Request["MessageStatus"];
            }

        }

        public TG.Model.tg_project ProjectBasicInfo { get; set; }

        public TG.Model.tg_ProjectExtendInfo ProjectExtendInfo { get; set; }

        public TG.Model.cm_Project ProjectInfo { get; set; }

        protected override bool IsAuth
        {
            get
            {
                return false;
            }
        }
        //消息ID
        public int MessageID
        {
            get
            {
                int msgSysNo = 0;
                int.TryParse(Request["MsgNo"], out msgSysNo);
                return msgSysNo;
            }
        }
        TG.BLL.cm_Project bll_cpr = new TG.BLL.cm_Project();
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProjectAuditEditBymaster));
            string sql = @"SELECT ProjectSysNo FROM cm_ProjectAuditRepeat WHERE SysNo=" + ProjectAuditSysNo;
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            if (o != null)
            {
                //得到ProjectSysNo
                //得到ProjectSysNo
                ProjectAuditEditEntity proAuditDataEntity = tg_ProjectAuditBP.GetProjectAuditEditEntity(ProjectAuditSysNo);
                ProjectSysNo = tg_ProjectAuditBP.GetProjectAuditEditEntity(ProjectAuditSysNo).ProjectSysNo;
                string optionsValue = tg_ProjectAuditBP.GetProjectAuditEditEntity(ProjectAuditSysNo).options;
                TG.Model.cm_Project pro_model = bll_cpr.GetModel(ProjectSysNo);
                string ChangeDateil = tg_ProjectAuditBP.GetProjectAuditEditEntity(ProjectAuditSysNo).ChangeDetail;
                if (!IsPostBack)
                {
                    Creattable = CreatTableByContent(proAuditDataEntity.options, pro_model);
                    this.hid_projid.Value = ProjectSysNo.ToString();
                    GetProInfo(ProjectSysNo.ToString());
                }
            }
            else
            {
                Response.Redirect("../Error.aspx?msgtype=" + MsgType);
            }
        }
        TG.BLL.tg_project tg_probll = new TG.BLL.tg_project();
        //获取项目信息
        public void GetProInfo(string strProId)
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(int.Parse(strProId));
            //判断是否为空
            if (pro_model != null)
            {
                //项目名称
                this.txt_name.Text = pro_model.pro_name == null ? "" : pro_model.pro_name.Trim();
                //关联合同
                this.txt_reletive.Text = pro_model.Project_reletive == null ? "" : pro_model.Project_reletive.Trim();
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }
                //建筑级别
                string BuildType = pro_model.BuildType.Trim();
                this.drp_buildtype.Text = BuildType;
                //建设单位
                this.txtbuildUnit.Text = pro_model.pro_buildUnit == null ? "" : pro_model.pro_buildUnit.Trim();
                //承接部门
                this.txt_unit.Text = pro_model.Unit == null ? "" : pro_model.Unit.Trim();
                this.userUnitNum.Value = pro_model.Unit == null ? "" : pro_model.Unit.Trim();
                //建设地点
                this.txtbuildAddress.Text = pro_model.BuildAddress == null ? "" : pro_model.BuildAddress.Trim();
                //建设规模
                this.txt_scale.Text = pro_model.ProjectScale == null ? "" : pro_model.ProjectScale.ToString();
                //项目结构形式
                string StrStruct = pro_model.pro_StruType == null ? "" : pro_model.pro_StruType.Trim();
                this.structType.Text = TG.Common.StringPlus.ResolveStructString(StrStruct);
                //建筑分类
                string BuildStructType = pro_model.pro_kinds == null ? "" : pro_model.pro_kinds.Trim();
                this.buildStructType.Text = TG.Common.StringPlus.ResolveStructString(BuildStructType);
                //显示项目阶段
                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }
                //项目来源
                string pro_src = pro_model.Pro_src.ToString();
                if (pro_src.Trim() != "-1")
                {
                    this.ddsource.Text = new TG.BLL.cm_Dictionary().GetModel(int.Parse(pro_src)).dic_Name;
                }
                else
                {
                    this.ddsource.Text = "";
                }

                //合同额
                this.txtproAcount.Text = pro_model.Cpr_Acount == null ? "" : pro_model.Cpr_Acount.ToString();
                //行业性质
                string industry = pro_model.Industry == null ? "" : pro_model.Industry.Trim();
                this.ddProfessionType.Text = industry;
                //甲方
                this.txt_Aperson.Text = pro_model.ChgJia == null ? "" : pro_model.ChgJia.Trim();
                this.txt_phone.Text = pro_model.Phone == null ? "" : pro_model.Phone.Trim();
                //项目总负责
                this.txt_PMName.Text = pro_model.PMName;
                this.txt_PMPhone.Text = pro_model.PMPhone == null ? "" : pro_model.PMPhone.Trim();
                //开始结束日期
                this.txt_startdate.Text = Convert.ToDateTime(pro_model.pro_startTime).ToString("yyyy-MM-dd");
                this.txt_finishdate.Text = Convert.ToDateTime(pro_model.pro_finishTime).ToString("yyyy-MM-dd");
                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }

                //项目特征概况
                this.txt_sub.InnerText = pro_model.ProjSub == null ? "" : pro_model.ProjSub.Trim();
                //this.txt_sub.Enabled = false;
                //项目缩略图
                this.txt_remark.InnerText = pro_model.pro_Intro == null ? "" : pro_model.pro_Intro.Trim();
                //this.txt_remark.Enabled = false;
            }

        }
        //返回
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            string flag = Request.QueryString["flag"];
            if (flag == "list")
            {
                Response.Redirect("ProjectList.aspx");
            }
            else
            {
                Response.Redirect("Sch_Project.aspx");
            }
        }
        private string CreatTableByContent(string options, Model.cm_Project pro_model)
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder builder1 = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            if (options != null && options.Length > 0 && pro_model != null)
            {
                options = options.Replace("|*|", "\n");
                string[] arratList = options.Split('\n');
                for (int i = 0; i < arratList.Length; i++)
                {
                    string typestr = arratList[i].Split(':')[0];
                    switch (typestr)
                    {
                        case "pro_name":
                            builder.Append("<tr><td style=\" width:60px;\">项目名称：</td><td style=\" width:300px;\">" + pro_model.pro_name + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "cpr_copername":
                            builder.Append("<tr><td style=\" width:60px;\">关联合同：</td><td style=\" width:300px;\">" + (pro_model.Project_reletive == null ? "" : pro_model.Project_reletive.Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "pro_level":
                            builder.Append("<tr><td style=\" width:60px;\"> 管理级别：</td><td style=\" width:300px;\">" + (pro_model.pro_level.ToString() == "0" ? "院管" : "所管") + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + (arratList[i].Split(':')[1] == "0" ? "院管" : "所管") + "</td></tr>");
                            break;
                        case "AuditLevel":
                            builder.Append("<tr><td style=\" width:60px;\"> 审核级别：</td><td style=\" width:300px;\">" + GetAuditBefore(pro_model.AuditLevel.ToString().Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + GetAudit(arratList[i].Split(':')[1]) + "</td></tr>");
                            break;
                        case "ProjType":
                            builder.Append("<tr><td style=\" width:60px;\">项目类型：</td><td style=\" width:300px;\">" + pro_model.BuildType + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "ProjectScale": builder.Append("<tr><td style=\" width:60px;\">建筑面积：</td><td style=\" width:300px;\">" + (pro_model.ProjectScale == null ? "" : pro_model.ProjectScale.ToString()) + "(㎡)</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "(㎡)</td></tr>");
                            break;
                        case "pro_buildUnit": builder.Append("<tr><td style=\" width:60px;\">建筑单位：</td><td style=\" width:300px;\">" + (pro_model.pro_buildUnit == null ? "" : pro_model.pro_buildUnit.Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "ProjArea": builder.Append("<tr><td style=\" width:60px;\">规划面积：</td><td style=\" width:300px;\">" + (pro_model.BuildType == null ? "" : pro_model.BuildType.ToString()) + "(h㎡)</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "(h㎡)</td></tr>");
                            break;
                        case "pro_StruType": builder.Append("<tr><td style=\" width:60px;\">结构形式：</td><td style=\" width:300px;\">" + (pro_model.pro_StruType == null ? "" : pro_model.pro_StruType.Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + GetXML("StructType", arratList[i].Split(':')[1]) + "</td></tr>");
                            break;
                        case "pro_kinds": builder.Append("<tr><td style=\" width:60px;\">建筑分类：</td><td style=\" width:300px;\">" + (pro_model.pro_kinds == null ? "" : pro_model.pro_kinds.Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + GetXML("BuildType", arratList[i].Split(':')[1]) + "</td></tr>");
                            break;
                        case "BuildTypelevel": builder.Append("<tr><td style=\" width:60px;\">建筑类别：</td><td style=\" width:300px;\">" + pro_model.BuildType.Trim() + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + GetProfession(arratList[i].Split(':')[1]) + "</td></tr>");
                            break;
                        case "PMName": builder.Append("<tr><td style=\" width:60px;\">项目经理：</td><td style=\" width:300px;\">" + pro_model.PMName + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "PMPhone": builder.Append("<tr><td style=\" width:60px;\">电话：</td><td style=\" width:300px;\">" + (pro_model.PMPhone == null ? "" : pro_model.PMPhone.Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "ChgJia": builder.Append("<tr><td style=\" width:60px;\">甲方负责人：</td><td style=\" width:300px;\">" + (pro_model.ChgJia == null ? "" : pro_model.ChgJia.Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "Phone": builder.Append("<tr><td style=\" width:60px;\">电话：</td><td style=\" width:300px;\">" + (pro_model.Phone == null ? "" : pro_model.Phone.Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "Unit": builder.Append("<tr><td style=\" width:60px;\">承接部门：</td><td style=\" width:300px;\">" + (pro_model.Unit == null ? "" : pro_model.Unit.Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "cpr_Acount": builder.Append("<tr><td style=\" width:60px;\">合同额：</td><td style=\" width:300px;\">" + (pro_model.Cpr_Acount == null ? "" : pro_model.Cpr_Acount.ToString()) + "(万元)</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "(万元)</td></tr>");
                            break;
                        case "BuildAddress": builder.Append("<tr><td style=\" width:60px;\">建设地点：</td><td style=\" width:300px;\">" + (pro_model.BuildAddress == null ? "" : pro_model.BuildAddress.Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "Industry": builder.Append("<tr><td style=\" width:60px;\">行业性质：</td><td style=\" width:300px;\">" + (pro_model.Industry == null ? "" : pro_model.Industry.Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + GetProfession(arratList[i].Split(':')[1]) + "</td></tr>");
                            break;
                        case "pro_status": builder.Append("<tr><td style=\" width:60px;\">设计阶段：</td><td style=\" width:300px;\">" + pro_model.pro_status + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "Pro_src": builder.Append("<tr><td style=\" width:60px;\">项目来源：</td><td style=\" width:300px;\">" + new TG.BLL.cm_Dictionary().GetModel(int.Parse(pro_model.Pro_src.ToString())).dic_Name + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + GetProfession(arratList[i].Split(':')[1]) + "</td></tr>");
                            break;
                        case "cpr_join": builder.Append("<tr><td style=\" width:60px;\">参与部门：</td><td style=\" width:300px;\">" + Getshow(pro_model.ISTrunEconomy, pro_model.ISHvac, pro_model.ISArch) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + Getshow(arratList[i].Split(',')[0].Split(':')[2], arratList[i].Split(',')[1].Split(':')[1], arratList[i].Split(',')[2].Split(':')[1]) + "</td></tr>");
                            break;
                        case "pro_startTime": builder.Append("<tr><td style=\" width:60px;\">项目开始日期：</td><td style=\" width:300px;\">" + pro_model.pro_startTime + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "pro_finishTime": builder.Append("<tr><td style=\" width:60px;\">项目完成日期：</td><td style=\" width:300px;\">" + pro_model.pro_finishTime + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "ProjSub": builder.Append("<tr><td style=\" width:60px;\">项目特征概况：</td><td style=\" width:300px;\">" + (pro_model.ProjSub == null ? "" : pro_model.ProjSub.Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;
                        case "pro_Intro":
                            builder.Append("<tr><td style=\" width:60px;\">项目备注：</td><td style=\" width:300px;\">" + (pro_model.pro_Intro == null ? "" : pro_model.pro_Intro.Trim()) + "</td><td style=\" width:60px;\">修改后</td><td style=\" width:300px;\">" + arratList[i].Split(':')[1] + "</td></tr>");
                            break;

                    }
                }
            }
            return builder.ToString();
        }

        private string GetAuditBefore(string auditno)
        {
            string str = "";
            if (auditno == "1,0")
            {
                str = "院审";
            }
            else if (auditno == "0,1")
            {
                str = "所审";
            }
            else
            {
                str = "<span>院审</span><span>所审</span>";
            }
            return str;
        }
        protected string Getshow(string ISTrunEconomy, string ISHvac, string ISArch)
        {
            string isotherprt = "";
            if (ISTrunEconomy == "1")
            {
                isotherprt += "经济所,";
            }
            //暖通
            if (ISHvac == "1")
            {
                isotherprt += "暖通热力所,";
            }
            //土建所
            if (ISArch == "1")
            {
                isotherprt += "土建所,";
            }
            if (isotherprt.IndexOf(',') > -1)
            {
                isotherprt = isotherprt.Remove(isotherprt.LastIndexOf(','));
            }
            return isotherprt;
        }
        protected string GetProfession(string num)
        {
            string sql = "  SELECT dic_Name FROM cm_Dictionary WHERE id=" + num;
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            if (o != null)
            {
                return o.ToString().Trim();
            }
            else
            {
                return "";
            }

        }
        public string GetXML(string type, string p)
        {
            StringBuilder builder = new StringBuilder();
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + type + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                //一级
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                Readxml(nodes, out builder, p);
            }
            if (builder.ToString().Length > 0)
            {
                return builder.ToString().Substring(1);
            }
            else
            {
                return builder.ToString();
            }
            //公共建筑^办公+工业建筑^重工厂房^轻工厂房
        }
        //第一级别
        public void Readxml(XmlNodeList xmlnl, out StringBuilder sb_, string p)
        {
            sb_ = new StringBuilder();
            string[] arraylist = p.Split(',');
            foreach (XmlNode xl in xmlnl)
            {
                XmlNodeList nodes = xl.ChildNodes;
                for (int i = 0; i < arraylist.Length; i++)
                {
                    if (xl.Attributes["name"].Value == arraylist[i])
                    {
                        if (xl.ChildNodes.Count != 0)
                        {
                            sb_.Append("+" + xl.Attributes["name"].Value);
                            ReadxmlChild(nodes, sb_, p);
                        }
                        else
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                        }
                    }
                }
            }
        }
        //第二级别和一下的级别
        public void ReadxmlChild(XmlNodeList xmlnl, StringBuilder sb_, string p)
        {

            string[] arraylist = p.Split(',');
            foreach (XmlNode xl in xmlnl)
            {
                XmlNodeList nodes = xl.ChildNodes;
                for (int i = 0; i < arraylist.Length; i++)
                {
                    if (xl.Attributes["name"].Value == arraylist[i])
                    {
                        if (xl.ChildNodes.Count != 0)
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                            ReadxmlChild(nodes, sb_, p);
                        }
                        else
                        {
                            sb_.Append("^" + xl.Attributes["name"].Value);
                        }
                    }
                }
            }
        }
        protected string GetAudit(string auditno)
        {
            string str = "";
            if (auditno == "0")
            {
                str = "院审";
            }
            else if (auditno == "1")
            {
                str = "所审";
            }
            else
            {
                str = "<span>院审</span><span>所审</span>";
            }
            return str;
        }



        /// <summary>
        /// 得到项目审核信息
        /// </summary>
        /// <param name="projectAuditSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string GetProjectAuditInfo(string projectAuditSysNo)
        {
            ProjectAuditEditEntity projectAuditEntity = tg_ProjectAuditBP.GetProjectAuditEditEntity(int.Parse(projectAuditSysNo));
            return Newtonsoft.Json.JsonConvert.SerializeObject(projectAuditEntity);
        }

        /// <summary>
        /// 审核通过
        /// </summary>
        /// <param name="queryString"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string DoAgree(string queryString, string flag)
        {
            string result = "";
            ProjectAuditEditEntity dataEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectAuditEditEntity>(queryString);
            dataEntity.AuditUser = UserSysNo.ToString();
            dataEntity.AudtiDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            int asciiCode = (int)Convert.ToChar(dataEntity.Status);
            if (asciiCode % 2 != 0)
            {
                asciiCode++;
            }
            else
            {
                asciiCode = asciiCode + 2;
            }
            dataEntity.Status = ((char)asciiCode).ToString();
            //查询或审批
            if (flag == "0")
            {
                result = GetNextProcessRoleUser(dataEntity.Status);
            }
            else
            {
                //得到原始数据
                ProjectAuditEditEntity dataEntitySource = tg_ProjectAuditBP.GetProjectAuditEditEntity(dataEntity.SysNo);

                dataEntity.Suggestion = dataEntitySource.Suggestion + dataEntity.Suggestion;
                dataEntity.AuditUser = dataEntitySource.AuditUser + dataEntity.AuditUser;
                dataEntity.AudtiDate = dataEntitySource.AudtiDate + dataEntity.AudtiDate;

                int count = tg_ProjectAuditBP.UpdateProjectAuditEdit(dataEntity);

                //查询合同信息
                TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(dataEntitySource.ProjectSysNo);
                //得到修改前的project的name
                string projectNameB = project.pro_name.Trim();
                if (count > 0)
                {
                    if (dataEntity.Status == "D")
                    {
                        ////导入数据到原始表
                        //string IsoPath = Server.MapPath("/ISO");
                        //string userid = UserSysNo.ToString();
                        ////工程表单FTP地址
                        //string ftpPath = ConfigurationManager.AppSettings["FtpPysicsPath"].ToString();
                        //TG.BLL.tg_ProjectAuditBP.CreateProject(project, HttpContext.Current.Server.MapPath("/Template/tpmTemplate.XML"), IsoPath, userid, ftpPath);
                        //发送消息给审核发起人
                        SysMessageViewEntity sysMessageDataEntity1 = new SysMessageViewEntity
                        {
                            InUser = UserSysNo,
                            FromUser = dataEntitySource.InUser,
                            MsgType = 11,
                            //ReferenceSysNo = dataEntity.SysNo.ToString(),
                            ReferenceSysNo = string.Format("projectAuditSysNo={0}&MessageStatus={1}", dataEntity.SysNo.ToString(), dataEntity.Status),
                            ToRole = "0",
                            Status = "A",
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "项目申请修改通过，项目已经修改！"),
                            QueryCondition = project.pro_name,
                            ExtendField = dataEntity.Status,
                            IsDone = "B"
                        };
                        new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageDataEntity1);
                        //修改

                        if (tgproj.GetModel(project.ReferenceSysNo) == null)
                        {
                            if (project.pro_StruType.Length > 250)
                            {
                                project.pro_StruType = "公共建筑";
                            }
                            if (project.pro_kinds.Length > 250)
                            {
                                project.pro_kinds = "公共建筑";
                            }
                            ////导入数据到原始表
                            //string IsoPath = Server.MapPath("/ISO");
                            //string userid = UserSysNo.ToString();
                            ////工程表单FTP地址
                            //string ftpPath = System.Configuration.ConfigurationManager.AppSettings["FtpPysicsPath"].ToString();
                            //TG.BLL.tg_ProjectAuditBP.CreateProject(project, HttpContext.Current.Server.MapPath("/Template/tpmTemplate.XML"), IsoPath, userid, ftpPath);
                        }
                        tg_probll.UpdateByEdit(int.Parse(dataEntitySource.ProjectSysNo.ToString()), dataEntitySource.options);
                        proBll.UpdateByEdit(int.Parse(dataEntitySource.ProjectSysNo.ToString()), dataEntitySource.options);
                        project = new TG.BLL.cm_Project().GetModel(dataEntitySource.ProjectSysNo);
                        //修改ftp表单的名称---条件
                        //修改后的项目名称
                        string projectNameA = project.pro_name.Trim();
                        //获取ftp地址
                        string ftpPath = System.Configuration.ConfigurationManager.AppSettings["FtpPysicsPath"].ToString();
                        string IsoPath = Server.MapPath("/ISO");
                        string userid = UserSysNo.ToString();
                        //TG.BLL.tg_ProjectAuditBP.UpdateProject(project, HttpContext.Current.Server.MapPath("/Template/tpmTemplate.XML"), IsoPath, userid, ftpPath, true);
                        TG.BLL.tg_ProjectAuditBP.UpdateProjectName(projectNameB, projectNameA, ftpPath);
                        result = "1";
                    }
                    else
                    {
                        int roleSysNo = GetProcessRoleSysNo(dataEntity.Status);
                        SysMessageViewEntity sysMessageDataEntity = new SysMessageViewEntity
                        {
                            InUser = UserSysNo,
                            FromUser = UserSysNo,
                            MsgType = 11,
                            //ReferenceSysNo = dataEntity.SysNo.ToString(),
                            ReferenceSysNo = string.Format("projectAuditSysNo={0}&MessageStatus={1}", dataEntity.SysNo.ToString(), dataEntity.Status),
                            ToRole = roleSysNo.ToString(),
                            Status = "A",
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "项目申请修改"),
                            QueryCondition = project.pro_name,
                            ExtendField = dataEntity.Status,
                            IsDone = "A"
                        };
                        //消息实体
                        string sysMsgString = CommonAudit.GetMessagEntity(sysMessageDataEntity);
                        if (!string.IsNullOrEmpty(sysMsgString))
                        {
                            result = sysMsgString;
                        }
                        else
                        {
                            result = "0";
                        }
                    }
                }
                else
                {
                    return "0";
                }
            }

            return result;
        }

        /// <summary>
        /// 审核不通过
        /// </summary>
        /// <param name="queryString"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string DoDisAgree(string queryString)
        {
            ProjectAuditEditEntity dataEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectAuditEditEntity>(queryString);
            dataEntity.AuditUser = UserSysNo.ToString();
            dataEntity.AudtiDate = DateTime.Now.ToString("yyyy-MM-dd");

            int asciiCode = (int)Convert.ToChar(dataEntity.Status);
            if (asciiCode % 2 != 0)
            {
                asciiCode = asciiCode + 2;
            }
            else
            {
                asciiCode = asciiCode + 3;
            }
            dataEntity.Status = ((char)asciiCode).ToString();

            //得到原始数据
            ProjectAuditEditEntity dataEntitySource = tg_ProjectAuditBP.GetProjectAuditEditEntity(dataEntity.SysNo);

            dataEntity.Suggestion = dataEntitySource.Suggestion + dataEntity.Suggestion;
            dataEntity.AuditUser = dataEntitySource.AuditUser + dataEntity.AuditUser;
            dataEntity.AudtiDate = dataEntitySource.AudtiDate + dataEntity.AudtiDate;
            dataEntity.InUser = dataEntitySource.InUser;

            int count = tg_ProjectAuditBP.UpdateProjectAuditEdit(dataEntity);
            if (count > 0)
            {
                //查询合同信息
                TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(dataEntitySource.ProjectSysNo);

                SysMessageViewEntity sysMessageDataEntity = new SysMessageViewEntity
                {
                    InUser = UserSysNo,
                    FromUser = dataEntity.InUser,
                    MsgType = 11,
                    //ReferenceSysNo = dataEntity.SysNo.ToString(),
                    ReferenceSysNo = string.Format("projectAuditSysNo={0}&MessageStatus={1}", dataEntity.SysNo.ToString(), dataEntity.Status),
                    ToRole = "0",
                    Status = "A",
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "项目申请修改不通过！"),
                    QueryCondition = project.pro_name,
                    ExtendField = dataEntity.Status,
                    IsDone = "B"
                };

                new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageDataEntity);
            }
            return count.ToString();
        }

        /// <summary>
        /// 检查是否有权限进行操作
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <param name="ProjectAuditSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string CheckPower(string userSysNo, string projectAuditSysNo)
        {
            ProjectAuditEditEntity projectAuditDataEntity = tg_ProjectAuditBP.GetProjectAuditEditEntity(int.Parse(projectAuditSysNo));
            //查看当前审核流程需要哪个角色来操作
            int roleSysNo = GetProcessRoleSysNo(projectAuditDataEntity.Status);
            //检查登陆用户是否包含该权限。
            bool hasPower = new TG.BLL.cm_Role().CheckPower(roleSysNo, UserSysNo);
            return hasPower == true ? "1" : "0";
        }
        /// <summary>
        /// 得到审核流程顺序
        /// </summary>
        /// <returns></returns>
        [AjaxMethod]
        public string GetAuditProcessRoleName()
        {
            string[] roleNameArray = AuditLocusBP.GetProcessDescriptionArray("P");
            return Newtonsoft.Json.JsonConvert.SerializeObject(roleNameArray);
        }
        /// <summary>
        /// 得到审核用户
        /// </summary>
        /// <param name="userSysNoString"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string GetAuditUserNameArrayString(string userSysNoString)
        {
            string[] userSysNoArray = (userSysNoString.Substring(0, userSysNoString.Length - 1)).Split(',');

            return Newtonsoft.Json.JsonConvert.SerializeObject(TG.BLL.tg_ProjectAuditBP.GetUserNameArray(userSysNoArray));
        }

        //获取审批阶段角色
        private int GetProcessRoleSysNo(string auditStatus)
        {
            return tg_ProjectAuditBP.GetProcessRoleSysNo(auditStatus);
        }

        //更新待办状态
        protected void UpdateWorkDoneStatus()
        {
            //修改办公状态
            int count = new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatus(MessageID);
        }

        //返回下一阶段用户列表
        private string GetNextProcessRoleUser(string status)
        {
            string result = "";
            if (status == "D")
            {
                result = "1";
            }
            else
            {
                //获取项目配置审批实体
                int roleSysNo = GetProcessRoleSysNo(status);
                //得到审批用户的实体
                string roleUserString = CommonAudit.GetRoleName(roleSysNo);
                if (!string.IsNullOrEmpty(roleUserString))
                {
                    result = roleUserString;
                }
                else
                {
                    result = "0";
                }
            }
            return result;
        }

    }
}