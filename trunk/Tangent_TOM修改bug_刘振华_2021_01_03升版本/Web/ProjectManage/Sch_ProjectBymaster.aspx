﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="Sch_ProjectBymaster.aspx.cs" Inherits="TG.Web.ProjectManage.Sch_ProjectBymaster" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/ShowDivDialog.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ProjectMamage/SelectColumnsProject.js"></script>
    <script type="text/javascript" src="../js/ProjectMamage/SelectProject_jq.js"></script>
    <script type="text/javascript">
        //判断是否选中节点       
        var nodecount;
        function IsStructCheckNode(obj) {
            nodecount = 0;
            if (obj == 'struct') {
                <%= asTreeviewStructObjID %>.traverseTreeNode(displayNodeFun);
            }
            else if (obj == 'structtype') {
                <%= asTreeviewStructTypeObjID %>.traverseTreeNode(displayNodeFun);
            }

        if (nodecount > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    function ChooseNodesValue()
    {
        var result="<%= GetDropDownTreeCheckedValue()%>"; 
        return result;
        alert(result);
    }
    function displayNodeFun(elem) {
        if (elem.getAttribute("checkedState") == "0") {
            nodecount++;
        }
    }
    </script>
    <style type="text/css">
        .defaultDropdownIconTD {
            background-color: #FFF;
        }

        .defaultDropdownTextContainer {
            background-color: #FFF;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>高级查询</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目信息管理</a><i class="fa fa-angle-right"> </i><a>高级查询</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>请选择查询项
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table border="0" cellspacing="0" cellpadding="0" width="100%" id="tbl_id">
                                    <tr>
                                        <td align="left">
                                            <input id="cprName" name="bbs" class="cls_audit" type="checkbox" /><span rel="pro_name">项目名称</span>
                                        </td>
                                        <td align="left">
                                            <input id="Level" name="bbs" class="cls_audit" type="checkbox" /><span rel="pro_jb">管理级别</span>
                                        </td>
                                        <td align="left">
                                            <input id="buildArea" name="bbs" class="cls_audit" type="checkbox" /><span rel="ProjectScale">建设规模</span>
                                        </td>
                                        <td align="left">
                                            <input id="cprBuildUnit" name="bbs" class="cls_audit" type="checkbox" /><span rel="pro_buildUnit">建设单位</span>
                                        </td>
                                        <td align="left">
                                            <input id="StructType" name="bbs" class="cls_audit" type="checkbox" /><span rel="pro_StruType">结构形式</span>
                                        </td>
                                        <td align="left">
                                            <input id="BuildStructType" name="bbs" class="cls_audit" type="checkbox" /><span
                                                for="BuildStructType" rel="pro_kinds">建筑分类</span>
                                        </td>
                                        <td align="left">
                                            <input id="buildType" name="bbs" class="cls_audit" type="checkbox" /><span rel="BuildType">建筑类别</span>
                                        </td>
                                        <td align="left">
                                            <input id="proFuze" name="bbs" class="cls_audit" type="checkbox" /><span rel="PMUserName">执行设总</span>
                                        </td>
                                        <td align="left">
                                            <input id="FParty" name="bbs" class="cls_audit" type="checkbox" /><span rel="ChgJia">甲方负责人</span>
                                        </td>
                                        <td align="left">
                                            <input id="cjbm" name="bbs" class="cls_audit" type="checkbox" /><span rel="Unit">承接部门</span>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td align="left">
                                            <input id="cpr_Account" name="bbs" class="cls_audit" type="checkbox" /><span rel="Cpr_Acount">合同额</span>
                                        </td>
                                        <td align="left">
                                            <input id="ddProjectPosition" name="bbs" class="cls_audit" type="checkbox" /><span
                                                rel="BuildAddress">建设地点</span>
                                        </td>
                                        <td align="left">
                                            <input id="ddProfessionTypeCheck" name="bbs" class="cls_audit" type="checkbox" /><span
                                                rel="Industry">行业性质</span>
                                        </td>
                                        <td align="left">
                                            <input id="ddSourceWay" name="bbs" class="cls_audit" type="checkbox" /><span rel="pro_from">工程来源</span>
                                        </td>
                                        <td align="left">
                                            <input id="ddcpr_Stage" name="bbs" class="cls_audit" type="checkbox" /><span rel="pro_status">设计阶段</span>
                                        </td>
                                        <td align="left">
                                            <input id="cpr_join" name="bbs" class="cls_audit" type="checkbox" /><span rel="ISArch">参与部门</span>
                                        </td>
                                        <td align="left">
                                            <input id="SingnDate" name="bbs" class="cls_audit" type="checkbox" /><span rel="qdrq">项目开始日期</span>
                                        </td>
                                        <td align="left">
                                            <input id="DoneDate" name="bbs" class="cls_audit" type="checkbox" /><span rel="wcrq">项目完成日期</span>
                                        </td>
                                        <td align="left">
                                            <input id="InsertDate" name="bbs" class="cls_audit" type="checkbox" /><span rel="InsertDate">录入时间</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目高级查询
                    </div>
                    <div class="tools">
                         <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <div class="btn-group" data-toggle="buttons">
                            <span class="btn blue">查询方式：</span>
                            <label rel="and" class="btn yellow btn-sm active" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="option1" value="and">必须
                            </label>
                            <label rel="or" class="btn default btn-sm" style="width: 50px;">
                                <input type="radio" name="options" class="toggle" id="option2" value="or">或
                            </label>                           
                        </div>                    
                    </div>                    
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" id="tbl_id2">
                                    <tr for="cprName">
                                        <td>项目名称:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" id="txt_proName" style="width: 390px;border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr for="Level">
                                        <td>管理级别:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="managerLevelDropDownList" runat="server" CssClass="form-control input-sm"
                                                Width="180">
                                                <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                                                <asp:ListItem Value="0">院管</asp:ListItem>
                                                <asp:ListItem Value="1">所管</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="buildArea">
                                        <td>建筑规模:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="txt_buildArea" class="form-control input-sm " style="width: 180px;border: 1px solid #9BA0A6;" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_buildArea2" class="form-control input-sm" style="width: 180px;border: 1px solid #9BA0A6;" />
                                                        &nbsp; 平米
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="cprBuildUnit">
                                        <td>建设单位:
                                        </td>
                                        <td>
                                            <input type="text" id="txt_buildUnit" class="form-control input-sm" style="width: 390px;border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr for="StructType">
                                        <td>结构形式：
                                        </td>
                                        <td>
                                            <cc1:ASDropDownTreeView ID="asTreeviewStruct" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="--------请选择结构形式--------"
                                                Width="180px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                Font-Size="12px" Font-Strikeout="False" EnableTheme="true" />
                                        </td>
                                    </tr>
                                    <tr for="BuildStructType">
                                        <td>建筑分类：
                                        </td>
                                        <td>
                                            <cc1:ASDropDownTreeView ID="asTreeviewStructType" runat="server" BasePath="~/js/astreeview/astreeview/"
                                                DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                                EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                                EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="--------请选择建筑分类--------"
                                                Width="180px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                                RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                                DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                                Font-Size="12px" Font-Strikeout="False" EnableTheme="true" />
                                        </td>
                                    </tr>
                                    <tr for="buildType">
                                        <td>建筑类别:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddrank" runat="server" AppendDataBoundItems="true" CssClass="form-control input-sm"
                                                Width="180">
                                                <asp:ListItem Value="-1">------请选择------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="proFuze">
                                        <td>执行设总:
                                        </td>
                                        <td>
                                            <input type="text" id="txt_PMName" class="form-control input-sm " style="width: 390px;border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>电话：
                                        </td>
                                        <td>
                                            <input type="text" id="txt_PMPhone" class="form-control input-sm " style="width: 390px;border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr for="FParty">
                                        <td>甲方负责人:
                                        </td>
                                        <td>
                                            <input type="text" id="txt_Aperson" class="form-control input-sm " style="width: 390px;border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>电话：
                                        </td>
                                        <td>
                                            <input type="text" id="txt_phone" class="form-control input-sm " style="width: 390px;border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr for="cjbm">
                                        <td>承接部门:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drp_unit" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm"
                                                Width="180">
                                                <asp:ListItem Value="0">--------请选择--------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="cpr_Account">
                                        <td>合同额:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="txtproAcount" class="form-control input-sm" style="width: 180px;border: 1px solid #9BA0A6;" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="txtproAcount2" class="form-control input-sm" style="width: 180px;border: 1px solid #9BA0A6;" />
                                                        &nbsp;万元
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="ddProjectPosition">
                                        <td>建设地点:
                                        </td>
                                        <td>
                                            <input type="text" id="txtaddress" class="form-control input-sm " style="width: 390px;border: 1px solid #9BA0A6;" />
                                        </td>
                                    </tr>
                                    <tr for="ddProfessionTypeCheck">
                                        <td>行业性质:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddType" runat="server" CssClass="form-control input-sm" Width="180"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value="0">--------请选择--------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="ddSourceWay">
                                        <td>工程来源:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddsource" runat="server" AppendDataBoundItems="True" Width="180"
                                                CssClass="form-control input-sm">
                                                <asp:ListItem Value="-1">--------请选择--------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr for="ddcpr_Stage">
                                        <td>设计阶段:
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="CheckBox1" runat="server" Text="方案设计" CssClass="cls_jd" rel="sjjd" />
                                            <asp:CheckBox ID="CheckBox2" runat="server" Text="初步设计" CssClass="cls_jd" rel="sjjd" />
                                            <asp:CheckBox ID="CheckBox3" runat="server" Text="施工图设计" CssClass="cls_jd" rel="sjjd" />
                                            <asp:CheckBox ID="CheckBox4" runat="server" Text="其他" CssClass="cls_jd" rel="sjjd" />
                                        </td>
                                    </tr>
                                    <tr for="cpr_join">
                                        <td>参与部门:
                                        </td>
                                        <td>
                                            <div class="col-md-12">
                                                <div class="checkbox-list">
                                                    <label class="checkbox-inline">
                                                        <span class="">
                                                            <input type="checkbox" id="chk_ISTrunEconomy" class="can" runat="server" value="经济所" />
                                                        </span>经济所
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        <span class="">
                                                            <input type="checkbox" id="chk_ISHvac" runat="server" class="can" value="暖通热力所" />
                                                        </span>暖通热力所
                                                    </label>
                                                    <label class="checkbox-inline">
                                                        <span>
                                                            <input type="checkbox" id="chk_ISArch" runat="server" class="can" value="土建所" />
                                                        </span>土建所
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="SingnDate">
                                        <td>项目开始日期:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="txt_signdate" onclick="WdatePicker({ readOnly: true })" class="Wdate"
                                                            runat="Server" style="width: 180px; height: 30px; border: 1px solid #e5e5e5" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_signdate2" onclick="WdatePicker({ readOnly: true })" class="Wdate"
                                                            runat="Server" style="width: 180px; height: 30px; border: 1px solid
                #e5e5e5" />
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="DoneDate">
                                        <td>项目完成日期:
                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="txt_finishdate" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 180px; height: 30px; border: 1px solid #e5e5e5" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_finishdate2" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 180px; height: 30px; border: 1px solid
                #e5e5e5" />
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr for="InsertDate">
                                        <td>录入时间：</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <input type="text" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 180px; height: 30px; border: 1px solid #e5e5e5" />
                                                        &nbsp; 至 &nbsp;
                                                        <input type="text" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                                            class="Wdate" runat="Server" style="width: 180px; height: 30px; border: 1px solid
                #e5e5e5" />
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <input type="button" class="btn
                blue"
                                                id="btn_Search" value="查询" />&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>项目列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <div class="btn-group" id="choose">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择显示列
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsid">
                                 <label>
                                <input type="checkbox" value="all" />全选
                            </label>
                                <%=ColumnsContent %>
                            </div>
                        </div>
<input type="button" class="btn red btn-sm" id="btn_export" value="导出Excel" />
                    </div>

                </div>
                <div class="portlet-body form" style="display: block;">
                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
</asp:Content>
