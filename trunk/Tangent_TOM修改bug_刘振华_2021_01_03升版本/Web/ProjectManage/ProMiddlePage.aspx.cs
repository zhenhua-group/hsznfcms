﻿using AjaxPro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.BLL;
using TG.Model;

namespace TG.Web.ProjectManage
{
    public partial class ProMiddlePage : PageBase
    {
        protected string ProID
        {
            get
            {
                return Request.Params["proid"].ToString();
            }
        }
        protected string ProName
        {
            get
            {
                return Request.Params["proname"].ToString();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {   //注册页面
            Utility.RegisterTypeForAjax(typeof(ProMiddlePage));
            if (!IsPostBack)
            {
                this.hidproId.Value = ProID;
                this.hidproname.Value = ProName;
            }

        }
        protected void btn_startApply_Click(object sender, EventArgs e)
        {
        }
        protected void btn_ProList_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProjectListBymaster.aspx");
        }
        protected void btn_ShowPro_Click(object sender, EventArgs e)
        {
            Response.Redirect("ShowProjectBymaster.aspx?pro_id=" + ProID);
        }
        //发起审批
        [AjaxPro.AjaxMethod]
        public string StartAppProject(string query, string flag)
        {
            string result = "0";
            if (flag == "0")
            {
                result = GetNextProcessRoleUser();
            }
            else
            {
                ProjectAduitParamEntity param = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectAduitParamEntity>(query);
                //实例一个审核实体
                ProjectAuditDataEntity dataEntity = new ProjectAuditDataEntity
                {
                    InUser = UserSysNo,
                    ProjectSysNo = param.ProjectSysNo
                };
                //新建一条审批记录
                int identitySysNo = tg_ProjectAuditBP.InsertProjectAudit(dataEntity);

                if (identitySysNo > 0)
                {
                    //获取项目配置审批实体
                    ProjectAuditConfigViewEntity projectAuditConfigViewEntity = new ProjectAuditConfigBP().GetProjectRoleSysNoByPoisition(1);

                    //声明消息实体
                    SysMessageViewEntity sysMessageDataEntity = new SysMessageViewEntity
                    {
                        //ReferenceSysNo = identitySysNo.ToString(),
                        ReferenceSysNo = string.Format("projectAuditSysNo={0}&MessageStatus={1}", identitySysNo.ToString(), "A"),
                        FromUser = UserSysNo,
                        MsgType = 2,
                        InUser = UserSysNo,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", param.ProjectName, "项目立项评审"),
                        QueryCondition = param.ProjectName,
                        IsDone = "A",
                        Status = "A"
                    };
                    //声明消息实体
                    string sysMsgString = string.Empty;
                    //第一审批阶段人员iD
                    sysMessageDataEntity.ToRole = projectAuditConfigViewEntity.RoleSysNo.ToString();
                    //获取审批人列表人名
                    sysMsgString = CommonAudit.GetMessagEntity(sysMessageDataEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        result = sysMsgString;
                    }
                }
                else
                {
                    result = "0";
                }
            }

            return result;
        }
        //返回下一阶段用户列表
        private string GetNextProcessRoleUser()
        {
            string result = "";
            //获取项目配置审批实体
            ProjectAuditConfigViewEntity projectAuditConfigViewEntity = new ProjectAuditConfigBP().GetProjectRoleSysNoByPoisition(1);
            //得到审批用户的实体
            string roleUserString = CommonAudit.GetRoleName(projectAuditConfigViewEntity.RoleSysNo);
            if (!string.IsNullOrEmpty(roleUserString))
            {
                result = roleUserString;
            }
            else
            {
                result = "0";
            }

            return result;
        }

    }
}