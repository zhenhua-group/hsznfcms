﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Text.RegularExpressions;
using TG.Common;

namespace TG.Web.ProjectManage
{
    public partial class Export_SearchProject : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strWhere = System.Web.HttpUtility.UrlDecode(Request.QueryString["strwhere"]);
            string andor = Request["andor"] ?? "and";//查询方式 
            string txt_proName = Request.QueryString["txt_proName"] ?? "";
            string txt_buildUnit = Request.QueryString["txt_buildUnit"] ?? "";
            string year = Request.QueryString["year"] ?? "";
            string txtaddress = Request.QueryString["txtaddress"] ?? "";
            string drp_unit = Request.QueryString["drp_unit"] ?? "";
            string ddType = Request.QueryString["ddType"] ?? "";
            string ddsource = Request.QueryString["ddsource"] ?? "";
            string txt_Aperson = Request.QueryString["txt_Aperson"] ?? "";
            string txt_phone = Request.QueryString["txt_phone"] ?? "";
            string txt_PMName = Request.QueryString["txt_PMName"] ?? "";
            string txt_PMPhone = Request.QueryString["txt_PMPhone"] ?? "";
            string txtproAcount = Request.QueryString["txtproAcount"] ?? "";
            string txtproAcount2 = Request.QueryString["txtproAcount2"] ?? "";
            string txt_buildArea = Request.QueryString["txt_buildArea"] ?? "";
            string txt_buildArea2 = Request.QueryString["txt_buildArea2"] ?? "";
            string txt_signdate = Request.QueryString["txt_signdate"] ?? "";
            string txt_signdate2 = Request.QueryString["txt_signdate2"] ?? "";
            string txt_finishdate = Request.QueryString["txt_finishdate"] ?? "";
            string txt_finishdate2 = Request.QueryString["txt_finishdate2"] ?? "";
            string ddrank = Request.QueryString["ddrank"] ?? "";
            string managerLevelDropDownList = Request.QueryString["managerLevelDropDownList"] ?? "";
            string str_jd = Request.QueryString["str_jd"] ?? "";
            string cybm = Request.QueryString["cybm"] ?? "";
            string asTreeviewStruct = Request.QueryString["asTreeviewStruct"] ?? "";
            string asTreeviewStructType = Request.QueryString["asTreeviewStructType"] ?? "";
            string str_columnsname = Request.QueryString["str_columnsname"] ?? "";
            string str_columnschinaname = Request.QueryString["str_columnschinaname"] ?? "";
            string startTime = Request.QueryString["startTime"];//录入时间
            string endTime = Request.QueryString["endTime"];//录入时间  
            StringBuilder sb = new StringBuilder("");
            //组合查询
            if (!string.IsNullOrEmpty(txt_proName))
            {
                string keyname = TG.Common.StringPlus.SqlSplit(txt_proName.Trim());
                sb.Append(" AND ( pro_name LIKE '%" + keyname + "%' ");
            }
            if (!string.IsNullOrEmpty(txt_buildUnit))
            {
                string keyname = TG.Common.StringPlus.SqlSplit(txt_buildUnit.Trim());
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append("  pro_buildUnit LIKE '%" + keyname + "%'  ");
            }
            if (year != "-1" && !string.IsNullOrEmpty(year))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }

                sb.AppendFormat(" year(InsertDate)={0} ", year);
            }
            if (!string.IsNullOrEmpty(txtaddress))
            {
                string keyname = TG.Common.StringPlus.SqlSplit(txtaddress.Trim());
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append("  BuildAddress LIKE '%" + keyname + "%'  ");
            }
            if (!string.IsNullOrEmpty(drp_unit) && !drp_unit.Contains("请选择"))
            {
                drp_unit = drp_unit.Substring(0, drp_unit.Length - 1);
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" Unit  in (" + drp_unit.Trim() + ")");
            }
            if (!string.IsNullOrEmpty(ddType) && !ddType.Contains("请选择"))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append("  Industry LIKE '%" + ddType.Trim() + "%'  ");
            }
            if (!string.IsNullOrEmpty(ddsource) && ddsource != "-1")
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" Pro_src=" + ddsource.Trim() + "  ");
            }
            if (!string.IsNullOrEmpty(txt_Aperson))
            {
                string keyname = TG.Common.StringPlus.SqlSplit(txt_Aperson.Trim());
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" ChgJia LIKE '%" + keyname + "%'  ");
            }
            if (!string.IsNullOrEmpty(txt_phone))
            {
                string keyname = TG.Common.StringPlus.SqlSplit(txt_phone.Trim());
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" Phone LIKE '%" + keyname + "%'  ");
            }
            if (!string.IsNullOrEmpty(txt_PMName))
            {
                string keyname = TG.Common.StringPlus.SqlSplit(txt_PMName.Trim());
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" PMName LIKE '%" + keyname + "%'  ");
            }
            if (!string.IsNullOrEmpty(txt_PMPhone))
            {
                string keyname = TG.Common.StringPlus.SqlSplit(txt_PMPhone.Trim());
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" PMPhone LIKE '%" + keyname + "%'  ");
            }
            //合同额
            if (!string.IsNullOrEmpty(txtproAcount) && string.IsNullOrEmpty(txtproAcount2))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" cpr_Acount >= " + txtproAcount.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(txtproAcount) && !string.IsNullOrEmpty(txtproAcount2))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" cpr_Acount <= " + txtproAcount2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(txtproAcount) && !string.IsNullOrEmpty(txtproAcount2))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" (cpr_Acount>=" + txtproAcount.Trim() + " AND cpr_Acount <= " + txtproAcount2.Trim() + ") ");
            }
            //建设规模
            if (!string.IsNullOrEmpty(txt_buildArea) && string.IsNullOrEmpty(txt_buildArea2))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" ProjectScale >=" + txt_buildArea.Trim() + " ");
            }
            else if (string.IsNullOrEmpty(txt_buildArea) && !string.IsNullOrEmpty(txt_buildArea2))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" ProjectScale <=" + txt_buildArea2.Trim() + " ");
            }
            else if (!string.IsNullOrEmpty(txt_buildArea) && !string.IsNullOrEmpty(txt_buildArea2))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" (ProjectScale >=" + txt_buildArea.Trim() + " AND ProjectScale <=" + txt_buildArea2.Trim() + ") ");
            }
            //时间
            if (!string.IsNullOrEmpty(txt_signdate) && !string.IsNullOrEmpty(txt_signdate2))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" (pro_startTime BETWEEN '" + txt_signdate.Trim() + "' AND '" + txt_signdate2.Trim() + "') ");
            }
            if (!string.IsNullOrEmpty(txt_finishdate) && !string.IsNullOrEmpty(txt_finishdate2))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" (pro_finishTime BETWEEN '" + txt_finishdate.Trim() + "' AND '" + txt_finishdate2.Trim() + "') ");
            }
            if (!string.IsNullOrEmpty(ddrank) && !ddrank.Contains("选择类别"))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" BuildType=N'" + ddrank.Trim() + "' ");
            }
            if (!string.IsNullOrEmpty(managerLevelDropDownList) && managerLevelDropDownList.Trim() != "-1")
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" pro_level=" + managerLevelDropDownList.Trim() + "  ");
            }
            if (string.IsNullOrEmpty(txtproAcount) == false && new Regex("^[0-9]+$").Match(txtproAcount.Trim()).Success && txtproAcount.Trim() != "0")
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" Cpr_Acount = " + txtproAcount.Trim() + " ");
            }

            //项目阶段  
            if (!string.IsNullOrEmpty(str_jd))
            {
                str_jd = str_jd.IndexOf(',') > -1 ? str_jd.Remove(str_jd.Length - 1) : "";
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" pro_status LIKE '%" + str_jd + "%' ");
            }
            //参与部门
            if (!string.IsNullOrEmpty(cybm))
            {
                string[] arr = cybm.Split(',');
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" ISTrunEconomy='" + arr[0].Trim() + "'");
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" ISHvac='" + arr[1].Trim() + "'");
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" ISArch='" + arr[2].Trim() + "'");
            }
            if (!string.IsNullOrEmpty(asTreeviewStruct) && !asTreeviewStruct.Contains("请选择结构形式"))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" replace(replace(replace(pro_StruType,'+',','),'*',','),'^',',')  LIKE '%" + asTreeviewStruct + "%' ");
            }
            if (!string.IsNullOrEmpty(asTreeviewStructType) && !asTreeviewStructType.Contains("请选择建筑分类"))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.Append(" replace(replace(replace(pro_kinds,'+',','),'*',','),'^',',') LIKE '%" + asTreeviewStructType + "%' ");
            }


            //录入时间  

            if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.AppendFormat(" InsertDate>='{0}' ", startTime + "  00:00:00");
            }
            else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.AppendFormat(" InsertDate<='{0}' ", endTime + " 23:59:59");
            }
            else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                if (sb.ToString() != "")
                {
                    sb.Append(" " + andor + " ");
                }
                else
                {
                    sb.Append(" and ( ");
                }
                sb.AppendFormat("  (InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59)");
            }

            if (sb.ToString() != "")
            {
                sb.Append(")");
            }
            sb.Append(strWhere);

            DataTable dt = new TG.BLL.cm_Project().GetProjectExportInfo(sb.ToString()).Tables[0];
            if (dt.Rows.Count > 0)
            {

                string modelPath = " ~/TemplateXls/Sch_Project.xls";

                HSSFWorkbook wb = null;

                //如果没有模板路径，则创建一个空的workbook和一个空的sheet
                if (string.IsNullOrEmpty(modelPath))
                {
                    wb = new HSSFWorkbook();
                    wb.CreateSheet();
                    wb.GetSheetAt(0).CreateRow(0);
                }
                else
                {
                    using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        wb = new HSSFWorkbook(fileStream);
                        fileStream.Close();
                    }
                }

                //标题样式
                ICellStyle style1 = wb.CreateCellStyle();
                style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
                style1.VerticalAlignment = VerticalAlignment.CENTER;
                style1.WrapText = true;
                style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                IFont font1 = wb.CreateFont();
                font1.FontHeightInPoints = 12;//字号
                font1.FontName = "宋体";//字体
                font1.Boldweight = (short)700;
                style1.SetFont(font1);


                //内容样式
                ICellStyle style2 = wb.CreateCellStyle();
                style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
                style2.VerticalAlignment = VerticalAlignment.CENTER;
                style2.WrapText = true;
                style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                IFont font2 = wb.CreateFont();
                font2.FontHeightInPoints = 10;//字号
                font2.FontName = "宋体";//字体
                style2.SetFont(font2);

                //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
                var ws = wb.GetSheet("Sheet1");
                if (ws == null)
                    ws = wb.GetSheetAt(0);

                //读取标题
                IRow dataRowTitle = ws.GetRow(0);
                if (!string.IsNullOrEmpty(str_columnschinaname))
                {
                    string[] columnsnamelist = str_columnschinaname.Split(',');
                    for (int j = 0; j < columnsnamelist.Length; j++)
                    {
                        ICell celltitle = dataRowTitle.CreateCell(j + 9);
                        celltitle.CellStyle = style1;
                        celltitle.SetCellValue(columnsnamelist[j].Trim());
                    }
                }
                int row = 1;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(i + row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(i + row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style2;
                    cell.SetCellValue(i + 1);

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["pro_name"].ToString());

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["Project_reletive"].ToString());

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["PMUserName"].ToString());

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["pro_status"].ToString());

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["Unit"].ToString());

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["Cpr_Acount"].ToString());

                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["qdrq"].ToString());

                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["wcrq"].ToString());

                    if (!string.IsNullOrEmpty(str_columnsname))
                    {
                        string[] columnslist = str_columnsname.Split(',');
                        for (int j = 0; j < columnslist.Length; j++)
                        {
                            if (columnslist[j] == "AuditLevel")
                            {
                                cell = dataRow.CreateCell(j + 9);
                                cell.CellStyle = style2;
                                string str = "";
                                if (dt.Rows[i][columnslist[j].Trim()].ToString().Contains("1,0"))
                                {
                                    str = "所审";
                                }
                                else if (dt.Rows[i][columnslist[j].Trim()].ToString().Contains("0,1"))
                                {
                                    str = "院审";
                                }
                                else if (dt.Rows[i][columnslist[j].Trim()].ToString().Contains("1,1"))
                                {
                                    str = "院审,所审";
                                }
                                cell.SetCellValue(str);
                            }
                            else
                            {
                                cell = dataRow.CreateCell(j + 9);
                                cell.CellStyle = style2;
                                cell.SetCellValue(dt.Rows[i][columnslist[j].Trim()].ToString());
                            }
                        }
                    }
                }
                using (MemoryStream memoryStream = new MemoryStream())
                {

                    wb.Write(memoryStream);

                    string name = System.Web.HttpContext.Current.Server.UrlEncode("项目高级查询列表.xls");
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                    Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                    Response.BinaryWrite(memoryStream.ToArray());
                    Response.ContentEncoding = Encoding.UTF8;
                    wb = null;
                    Response.End();
                }
            }
            else
            {
                MessageBox.Show(this, "导出没有数据！"); 
            }
        }
    }
}