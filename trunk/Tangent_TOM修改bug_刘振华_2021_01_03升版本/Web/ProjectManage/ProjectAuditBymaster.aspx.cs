﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using TG.BLL;
using TG.Model;
using System.Configuration;

namespace TG.Web.ProjectManage
{
    public partial class ProjectAuditBymaster : PageBase
    {
        /// <summary>
        /// 项目SysNo
        /// </summary>
        public int ProjectSysNo { get; set; }

        /// <summary>
        /// 项目审核SysNo
        /// </summary>
        public int ProjectAuditSysNo
        {
            get
            {
                int projectAuditSysNo = 0;
                int.TryParse(Request["projectAuditSysNo"], out projectAuditSysNo);
                return projectAuditSysNo;
            }
        }

        /// <summary>
        /// 消息审批状态
        /// </summary>
        public string MessageStatus
        {
            get
            {
                return Request["MessageStatus"];
            }

        }
        public string HiddenAuditStatus { get; set; }
        public string HiddenCoperationName { get; set; }


        public int HiddenProjectPlanAuditSysNo { get; set; }

        public int HiddenManageLevel { get; set; }

        public TG.Model.tg_project ProjectBasicInfo { get; set; }

        public TG.Model.tg_ProjectExtendInfo ProjectExtendInfo { get; set; }

        public TG.Model.cm_Project ProjectInfo { get; set; }

        protected override bool IsAuth
        {
            get
            {
                return false;
            }
        }
        //消息ID
        public int MessageID
        {
            get
            {
                int msgSysNo = 0;
                int.TryParse(Request["MsgNo"], out msgSysNo);
                return msgSysNo;
            }
        }
        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProjectAuditBymaster));
            TG.BLL.cm_Project bllpro = new BLL.cm_Project();

            //得到ProjectSysNo
            ProjectSysNo = tg_ProjectAuditBP.GetProjectAuditEntity(ProjectAuditSysNo).ProjectSysNo;
            HiddenAuditStatus = tg_ProjectAuditBP.GetProjectAuditEntity(ProjectAuditSysNo).Status;
            HiddenProjectPlanAuditSysNo = tg_ProjectAuditBP.GetProjectAuditEntity(ProjectAuditSysNo).SysNo;
            HiddenCoperationName = bllpro.GetModel(ProjectSysNo).pro_name;
            HiddenManageLevel = Convert.ToInt32(bllpro.GetModel(ProjectSysNo).pro_level);
            if (!IsPostBack)
            {
                this.hid_projid.Value = ProjectSysNo.ToString();
                GetProInfo(ProjectSysNo.ToString());
                BindPreviewPower();
                BindBuildType();
            }
        }
        //合同建筑类别
        protected void BindBuildType()
        {
            //             TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            //             string str_where = " dic_Type='cpr_buildtype'";
            //             this.DropDownList1.DataSource = bll_dic.GetList(str_where);
            //             this.DropDownList1.DataTextField = "dic_Name";
            //             this.DropDownList1.DataValueField = "dic_Name";
            //             this.DropDownList1.DataBind();
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;


                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }
        //获取项目信息
        public void GetProInfo(string strProId)
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(int.Parse(strProId));
            //判断是否为空
            if (pro_model != null)
            {
                //项目名称
                this.txt_name.Text = pro_model.pro_name == null ? "" : pro_model.pro_name.Trim();
                //关联合同
                this.txt_reletive.Text = pro_model.Project_reletive == null ? "" : pro_model.Project_reletive.Trim();
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel;
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }
                //建筑级别
                string BuildType = pro_model.BuildType.Trim();
                this.drp_buildtype.Text = BuildType;
                //建设单位
                this.txtbuildUnit.Text = pro_model.pro_buildUnit == null ? "" : pro_model.pro_buildUnit.Trim();
                //承接部门
                this.txt_unit.Text = pro_model.Unit == null ? "" : pro_model.Unit.Trim();
                //建设地点
                this.txtbuildAddress.Text = pro_model.BuildAddress == null ? "" : pro_model.BuildAddress.Trim();
                //建设规模
                this.txt_scale.Text = pro_model.ProjectScale == null ? "" : pro_model.ProjectScale.ToString();
                //项目结构形式
                string StrStruct = pro_model.pro_StruType == null ? "" : pro_model.pro_StruType.Trim();
                this.structType.Text = TG.Common.StringPlus.ResolveStructString(StrStruct);
                //建筑分类
                string BuildStructType = pro_model.pro_kinds == null ? "" : pro_model.pro_kinds.Trim();
                this.buildStructType.Text = TG.Common.StringPlus.ResolveStructString(BuildStructType);
                //显示项目阶段
                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }
                //项目来源
                string pro_src = pro_model.Pro_src.ToString();
                if (pro_src.Trim() != "-1")
                {
                    this.ddsource.Text = new TG.BLL.cm_Dictionary().GetModel(int.Parse(pro_src)).dic_Name;
                }
                else
                {
                    this.ddsource.Text = "";
                }

                //合同额
                this.txtproAcount.Text = pro_model.Cpr_Acount == null ? "" : pro_model.Cpr_Acount.ToString();
                //行业性质
                string industry = pro_model.Industry == null ? "" : pro_model.Industry.Trim();
                this.ddProfessionType.Text = industry;
                //甲方
                this.txt_Aperson.Text = pro_model.ChgJia == null ? "" : pro_model.ChgJia.Trim();
                this.txt_phone.Text = pro_model.Phone == null ? "" : pro_model.Phone.Trim();
                //项目总负责
                this.txt_PMName.Text = pro_model.PMName;
                this.txt_PMPhone.Text = pro_model.PMPhone == null ? "" : pro_model.PMPhone.Trim();
                //开始结束日期
                this.txt_startdate.Text = Convert.ToDateTime(pro_model.pro_startTime).ToString("yyyy-MM-dd");
                this.txt_finishdate.Text = Convert.ToDateTime(pro_model.pro_finishTime).ToString("yyyy-MM-dd");
                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }
                //项目特征概况
                string projSub = pro_model.ProjSub == null ? "" : pro_model.ProjSub.Trim();
                this.txt_sub.InnerHtml = TG.Common.StringPlus.HtmlEncode(projSub);
                //项目缩略图
                this.txt_remark.InnerText = pro_model.pro_Intro == null ? "" : pro_model.pro_Intro.Trim();
                this.txt_remark.Disabled = false;
            }

        }
        /// <summary>
        /// 得到项目审核信息
        /// </summary>
        /// <param name="projectAuditSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string GetProjectAuditInfo(string projectAuditSysNo)
        {
            ProjectAuditDataEntity projectAuditEntity = tg_ProjectAuditBP.GetProjectAuditEntity(int.Parse(projectAuditSysNo));
            return Newtonsoft.Json.JsonConvert.SerializeObject(projectAuditEntity);
        }

        /// <summary>
        /// 审核通过
        /// </summary>
        /// <param name="queryString"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string DoAgree(string queryString, string flag)
        {
            string result = "";
            ProjectAuditDataEntity dataEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectAuditDataEntity>(queryString);
            dataEntity.AuditUser = UserSysNo.ToString();
            dataEntity.AudtiDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            int asciiCode = (int)Convert.ToChar(dataEntity.Status);
            if (asciiCode % 2 != 0)
            {
                asciiCode++;
            }
            else
            {
                asciiCode = asciiCode + 2;
            }
            dataEntity.Status = ((char)asciiCode).ToString();
            //查询或审批
            if (flag == "0")
            {
                result = GetNextProcessRoleUser(dataEntity.Status);
            }
            else
            {
                //得到原始数据
                ProjectAuditDataEntity dataEntitySource = tg_ProjectAuditBP.GetProjectAuditEntity(dataEntity.SysNo);

                dataEntity.Suggestion = dataEntitySource.Suggestion + dataEntity.Suggestion;
                dataEntity.AuditUser = dataEntitySource.AuditUser + dataEntity.AuditUser;
                dataEntity.AudtiDate = dataEntitySource.AudtiDate + dataEntity.AudtiDate;

                int count = tg_ProjectAuditBP.UpdateProjectAudit(dataEntity);

                //查询合同信息
                TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(dataEntitySource.ProjectSysNo);

                if (count > 0)
                {
                    if (dataEntity.Status == "D")
                    {
                        //导入数据到原始表
                        string IsoPath = Server.MapPath("/ISO");
                        string userid = UserSysNo.ToString();
                        //工程表单FTP地址
                        string ftpPath = ConfigurationManager.AppSettings["FtpPysicsPath"].ToString();
                        TG.BLL.tg_ProjectAuditBP.CreateProject(project, HttpContext.Current.Server.MapPath("/Template/tpmTemplate.XML"), IsoPath, userid, ftpPath);
                        //发送消息给审核发起人
                        SysMessageViewEntity sysMessageDataEntity1 = new SysMessageViewEntity
                        {
                            InUser = UserSysNo,
                            FromUser = dataEntitySource.InUser,
                            MsgType = 2,
                            //ReferenceSysNo = dataEntity.SysNo.ToString(),
                            ReferenceSysNo = string.Format("projectAuditSysNo={0}&MessageStatus={1}", dataEntity.SysNo.ToString(), dataEntity.Status),
                            ToRole = "0",
                            Status = "A",
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "项目评审通过，请申请工号！"),
                            QueryCondition = project.pro_name,
                            ExtendField = dataEntity.Status,
                            IsDone = "B"
                        };
                        new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageDataEntity1);

                        result = "1";
                    }
                    else
                    {
                        int roleSysNo = GetProcessRoleSysNo(dataEntity.Status);
                        SysMessageViewEntity sysMessageDataEntity = new SysMessageViewEntity
                        {
                            InUser = UserSysNo,
                            FromUser = UserSysNo,
                            MsgType = 2,
                            //ReferenceSysNo = dataEntity.SysNo.ToString(),
                            ReferenceSysNo = string.Format("projectAuditSysNo={0}&MessageStatus={1}", dataEntity.SysNo.ToString(), dataEntity.Status),
                            ToRole = roleSysNo.ToString(),
                            Status = "A",
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "项目评审"),
                            QueryCondition = project.pro_name,
                            ExtendField = dataEntity.Status,
                            IsDone = "A"
                        };
                        //消息实体
                        string sysMsgString = CommonAudit.GetMessagEntity(sysMessageDataEntity);
                        if (!string.IsNullOrEmpty(sysMsgString))
                        {
                            result = sysMsgString;
                        }
                        else
                        {
                            result = "0";
                        }
                    }
                }
                else
                {
                    return "0";
                }
            }

            return result;
        }

        /// <summary>
        /// 审核不通过
        /// </summary>
        /// <param name="queryString"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string DoDisAgree(string queryString)
        {
            ProjectAuditDataEntity dataEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectAuditDataEntity>(queryString);
            dataEntity.AuditUser = UserSysNo.ToString();
            dataEntity.AudtiDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            int asciiCode = (int)Convert.ToChar(dataEntity.Status);
            if (asciiCode % 2 != 0)
            {
                asciiCode = asciiCode + 2;
            }
            else
            {
                asciiCode = asciiCode + 3;
            }
            dataEntity.Status = ((char)asciiCode).ToString();

            //得到原始数据
            ProjectAuditDataEntity dataEntitySource = tg_ProjectAuditBP.GetProjectAuditEntity(dataEntity.SysNo);

            dataEntity.Suggestion = dataEntitySource.Suggestion + dataEntity.Suggestion;
            dataEntity.AuditUser = dataEntitySource.AuditUser + dataEntity.AuditUser;
            dataEntity.AudtiDate = dataEntitySource.AudtiDate + dataEntity.AudtiDate;
            dataEntity.InUser = dataEntitySource.InUser;

            int count = tg_ProjectAuditBP.UpdateProjectAudit(dataEntity);
            if (count > 0)
            {
                //查询合同信息
                TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(dataEntitySource.ProjectSysNo);

                SysMessageViewEntity sysMessageDataEntity = new SysMessageViewEntity
                {
                    InUser = UserSysNo,
                    FromUser = dataEntity.InUser,
                    MsgType = 2,
                    //ReferenceSysNo = dataEntity.SysNo.ToString(),
                    ReferenceSysNo = string.Format("projectAuditSysNo={0}&MessageStatus={1}", dataEntity.SysNo.ToString(), dataEntity.Status),
                    ToRole = "0",
                    Status = "A",
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project.pro_name, "项目评审不通过！"),
                    QueryCondition = project.pro_name,
                    ExtendField = dataEntity.Status,
                    IsDone = "B"
                };

                new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageDataEntity);
            }
            return count.ToString();
        }

        /// <summary>
        /// 检查是否有权限进行操作
        /// </summary>
        /// <param name="userSysNo"></param>
        /// <param name="ProjectAuditSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string CheckPower(string userSysNo, string projectAuditSysNo)
        {
            ProjectAuditDataEntity projectAuditDataEntity = tg_ProjectAuditBP.GetProjectAuditEntity(int.Parse(projectAuditSysNo));
            //查看当前审核流程需要哪个角色来操作
            int roleSysNo = GetProcessRoleSysNo(projectAuditDataEntity.Status);
            //检查登陆用户是否包含该权限。
            bool hasPower = new TG.BLL.cm_Role().CheckPower(roleSysNo, UserSysNo);
            return hasPower == true ? "1" : "0";
        }
        /// <summary>
        /// 得到审核流程顺序
        /// </summary>
        /// <returns></returns>
        [AjaxMethod]
        public string GetAuditProcessRoleName()
        {
            string[] roleNameArray = AuditLocusBP.GetProcessDescriptionArray("P");
            return Newtonsoft.Json.JsonConvert.SerializeObject(roleNameArray);
        }
        /// <summary>
        /// 得到审核用户
        /// </summary>
        /// <param name="userSysNoString"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string GetAuditUserNameArrayString(string userSysNoString)
        {
            string[] userSysNoArray = (userSysNoString.Substring(0, userSysNoString.Length - 1)).Split(',');

            return Newtonsoft.Json.JsonConvert.SerializeObject(TG.BLL.tg_ProjectAuditBP.GetUserNameArray(userSysNoArray));
        }
        ///ADD
        [AjaxMethod]
        public string StartAppProject(string query, string flag)
        {
            string result = "0";
            if (flag == "0")
            {
                result = GetNextProcessRoleUser();
            }
            else
            {
                ProjectAduitEditParamEntity param = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectAduitEditParamEntity>(query);
                //实例一个审核实体
                ProjectAuditEditEntity dataEntity = new ProjectAuditEditEntity
                {
                    InUser = UserSysNo,
                    ProjectSysNo = param.ProjectSysNo,
                    options = param.copoption,
                    ChangeDetail = param.ChangeDetail
                };
                int sysNo = 0;

                TG.BLL.cm_ProjectPlanBP projectPlanBP = new TG.BLL.cm_ProjectPlanBP();
                //是否存在策划审批

                string sql = @"SELECT Status FROM cm_ProjectAuditRepeat WHERE ProjectSysNo=" + param.ProjectSysNo;
                object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
                //项目实体
                TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(param.ProjectSysNo);
                //如果已有申请
                if (o != null)
                {
                    if (o.ToString() == "D" || o.ToString() == "C" || o.ToString() == "E" || o.ToString() == "G")
                    {
                        string sql2 = @"DELETE cm_ProjectAuditRepeat WHERE ProjectSysNo=" + param.ProjectSysNo;
                        //TG.DBUtility.DbHelperSQL.GetSingle(sql2);
                    }
                }
                string sql24 = "select count(*) from cm_ProjectAuditRepeat where status NOT IN ('C','E','D')  AND ProjectSysNo=" + param.ProjectSysNo;
                object so = TG.DBUtility.DbHelperSQL.GetSingle(sql24);

                if (int.Parse(so.ToString()) < 1)
                {
                    //新建一条审批记录
                    int identitySysNo = tg_ProjectAuditBP.InsertProjectAuditEdit(dataEntity);

                    if (identitySysNo > 0)
                    {
                        //获取项目配置审批实体
                        ProjectAuditConfigViewEntity projectAuditConfigViewEntity = new ProjectAuditConfigBP().GetProjectRoleSysNoByPoisition(1);

                        //声明消息实体
                        SysMessageViewEntity sysMessageDataEntity = new SysMessageViewEntity
                        {
                            //ReferenceSysNo = identitySysNo.ToString(),
                            ReferenceSysNo = string.Format("projectAuditSysNo={0}&MessageStatus={1}", identitySysNo.ToString(), "A"),
                            FromUser = UserSysNo,
                            MsgType = 11,
                            InUser = UserSysNo,
                            ToRole = "0",
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", param.ProjectName, "项目修改申请评审"),
                            QueryCondition = param.ProjectName,
                            IsDone = "A",
                            Status = "A"
                        };
                        //声明消息实体
                        string sysMsgString = string.Empty;
                        //第一审批阶段人员iD
                        sysMessageDataEntity.ToRole = projectAuditConfigViewEntity.RoleSysNo.ToString();
                        //获取审批人列表人名
                        sysMsgString = CommonAudit.GetMessagEntity(sysMessageDataEntity);
                        if (!string.IsNullOrEmpty(sysMsgString))
                        {
                            result = sysMsgString;
                        }
                    }
                }
                else
                {
                    result = "1";
                }
            }

            return result;
        }

        //返回下一阶段用户列表
        private string GetNextProcessRoleUser()
        {
            string result = "";
            //获取项目配置审批实体
            ProjectAuditConfigViewEntity projectAuditConfigViewEntity = new ProjectAuditConfigBP().GetProjectRoleSysNoByPoisition(1);
            //得到审批用户的实体
            string roleUserString = CommonAudit.GetRoleName(projectAuditConfigViewEntity.RoleSysNo);
            if (!string.IsNullOrEmpty(roleUserString))
            {
                result = roleUserString;
            }
            else
            {
                result = "0";
            }

            return result;
        }
        ///
        //获取审批阶段角色
        private int GetProcessRoleSysNo(string auditStatus)
        {
            return tg_ProjectAuditBP.GetProcessRoleSysNo(auditStatus);
        }

        //更新待办状态
        protected void UpdateWorkDoneStatus()
        {
            //修改办公状态
            int count = new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatus(MessageID);
        }

        //返回下一阶段用户列表
        private string GetNextProcessRoleUser(string status)
        {
            string result = "";
            if (status == "D")
            {
                result = "1";
            }
            else
            {
                //获取项目配置审批实体
                int roleSysNo = GetProcessRoleSysNo(status);
                //得到审批用户的实体
                string roleUserString = CommonAudit.GetRoleName(roleSysNo);
                if (!string.IsNullOrEmpty(roleUserString))
                {
                    result = roleUserString;
                }
                else
                {
                    result = "0";
                }
            }
            return result;
        }
    }
}