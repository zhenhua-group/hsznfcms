﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;


namespace TG.Web.ProjectManage
{

    public partial class ProjectFixed : PageBase
    {
        public string ColumnsContent
        {
            get;
            set;
        }
        //生产经营项目
        protected TG.BLL.cm_Project CProject
        {
            get
            {
                return new TG.BLL.cm_Project();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定字段
                BindColumns();
                //绑定部门
                BindUnit();
                //绑定年份
                BindYear();
                //默认选中年份
                SelectCurrentYear();
                //绑定项目列表
                BindProject();
                //绑定权限
                BindPreviewPower();

            }
        }
        protected void BindColumns()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("管理级别", "pro_jb");
            dic.Add("审核级别", "AuditLevel");
            dic.Add("建设单位", "pro_buildUnit");
            dic.Add("建设地点", "BuildAddress");
            dic.Add("建设规模", "ProjectScale");
            dic.Add("承接部门", "Unit");
            dic.Add("结构形式", "pro_StruType");
            dic.Add("建筑分类", "pro_kinds");
            dic.Add("设计阶段", "pro_status");
            dic.Add("项目来源", "pro_from");
            dic.Add("行业性质", "Industry");
            dic.Add("甲方负责人电话", "Phone");
            dic.Add("执行设总电话", "PMPhone");
            dic.Add("参与部门", "ISTrunEconomy");
            dic.Add("项目概况", "ProjSub");
            dic.Add("项目备注", "pro_Intro");
            dic.Add("录入人", "InsertUser");
            dic.Add("录入时间", "lrsj");

            foreach (KeyValuePair<string, string> pair in dic)
            {
                ColumnsContent += "<label><input type='checkbox' value='" + pair.Value + "' />" + pair.Key + "</label>";
            }


            //  BindProInfoConfig(this.asTreeviewStruct.RootNode,dic);
            //this.asTreeviewStruct.CollapseAll();           
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //根据条件查询
        public void BindProject()
        {
            StringBuilder sb = new StringBuilder("");
            //名字不为空
            //if (this.txt_keyname.Value.Trim() != "")
            //{
            //    string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
            //    sb.Append(" AND pro_Name LIKE '%" + keyname + "%'  ");
            //}
            ////绑定单位
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    sb.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            //}
            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    sb.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            //}
            //检查权限
            GetPreviewPowerSql(ref sb);

            this.hid_where.Value = sb.ToString();

            //所有记录数
            //this.AspNetPager1.RecordCount = int.Parse(CProject.GetListPageProcCount(sb.ToString()).ToString());
            //排序
            // string orderString = " Order by " + OrderColumn + " " + Order;
            // sb.Append(orderString);

            // gv_project.DataSource = CProject.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex); ;
            // gv_project.DataBind();
        }
        //删除
        protected void btn_Delpro_Click(object sender, ImageClickEventArgs e)
        {
            TG.BLL.cm_Project proE = new TG.BLL.cm_Project();
            TG.Model.cm_Project model = new Model.cm_Project();
            string str_idList = Del_Id();
            if (str_idList.Trim().Length == 0)
                return;

            ArrayList listAudit = proE.DeleteList(str_idList);

            //存在审批中的合同
            if (listAudit.Count > 0)
            {
                string message = "";

                for (int i = 0; i < listAudit.Count; i++)
                {
                    model = proE.GetModel(int.Parse(listAudit[i].ToString()));
                    if (model != null)
                    {
                        message = message + model.pro_name + "项目在审批中，或者审批完毕\\n ";
                    }
                }
                message = message + "不能删除";

                //弹出提示
                TG.Common.MessageBox.Show(this, message);
            }

            BindProject();
        }
        /// <summary>
        /// 获取选中项的ID
        /// </summary>
        /// <returns></returns>
        public string Del_Id()
        {
            string strDelId = "";
            //foreach (GridViewRow gv in gv_project.Rows)
            //{
            //    CheckBox ck = (CheckBox)gv.FindControl("chk_Pro");
            //    if (ck.Checked)
            //    {
            //        HiddenField hfproId = (HiddenField)gv.FindControl("bs_project_Id");
            //        strDelId += hfproId.Value + ",";
            //    }
            //}
            //if (strDelId.IndexOf(",") > -1)
            //{
            //    strDelId = strDelId.Substring(0, strDelId.LastIndexOf(","));
            //}
            return strDelId;
        }
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Search_Click(object sender, ImageClickEventArgs e)
        {
            //查询项目信息
            BindDataSetPageIndex();
        }

        protected void gv_project_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //行绑定代码
            }
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindProject();
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }

        //生产部门改变重新绑定数据
        protected void drp_unit_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataSetPageIndex();
        }
        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>
        protected void BindDataSetPageIndex()
        {
            // this.AspNetPager1.CurrentPageIndex = 0;
            //绑定数据
            BindProject();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();

            string modelPath = " ~/TemplateXls/ProjectList.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ChgJia"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_name"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["BuildType"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Project_reletive"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cpr_Acount"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["pro_startTime"]).ToString("yyyy_MM-dd"));

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["pro_finishTime"]).ToString("yyyy_MM-dd"));

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["PMName"].ToString());
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("项目信息列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {
            StringBuilder sb = new StringBuilder("");
            //名字不为空
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.Append(" AND pro_Name LIKE '%" + keyname + "%'  ");
            }
            //绑定单位
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            }
            //按照年份
            if (this.drp_year.SelectedIndex != 0)
            {
                sb.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            }
            //检查权限
            //个人
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }

            TG.BLL.cm_Project bll = new TG.BLL.cm_Project();

            DataTable dt = bll.GetProjectExportInfo(sb.ToString()).Tables[0];

            return dt;
        }
    }
}