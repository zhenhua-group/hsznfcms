﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddProjectFileInfo.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.AddProjectFileInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>新增工程设计出图卡</title>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="/css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery.alerts.css" rel="Stylesheet" type="text/css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
    <link href="/css/swfupload/default_cpr.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
    <script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.alerts.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.chromatable.js"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script src="../../js/ProjectMamage/ProImageAudit/AddProjectFileInfo.js" type="text/javascript"></script>
    <style type="text/css">
        .input
        {
            width: 98%;
            height: 99%;
            font-size: 12px;
            font-style: normal;
            font-family: 宋体;
            white-space: nowrap;
            border: 1px solid #CCC;
        }
    </style>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                当前位置：[项目信息-新增工程设计归档资料]
            </td>
        </tr>
        <tr>
            <td class="cls_content" align="center" valign="top">
                <table class="cls_content_2">
                    <tr>
                        <td class="cls_head_2">
                            <div class="cls_1">
                                项目信息
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben" id="projectInfo">
        <tr>
            <td style="width: 15%">
                工程名称：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_proname" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                工程号:
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_pronumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                设计部门：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_prounit" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                设计阶段：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_prostatus" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                管理级别：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_level" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                审核级别：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                设&nbsp;&nbsp;总：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_PMName" runat="server" Width="150px"></asp:Label>
            </td>
            <td style="width: 15%">
            </td>
            <td style="width: 35%">
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    工程设计归档资料
                </div>
            </td>
        </tr>
    </table>
    <table class="cls_content_head" style="width: 100%;">
        <tr id="Tr1">
            <td style="width: 10%;" align="center">
                序号
            </td>
            <td style="width: 25%;" align="center">
                名称
            </td>
            <td style="width: 15%" align="center">
                页数
            </td>
            <td style="width: 15%;" align="center">
                有无
            </td>
            <td style="width: 35%" align="center">
                备注
            </td>
        </tr>
    </table>
    <table class="show_projectPlot" style="width: 100%;" id="tbFile">
        <tr>
            <td style="width: 10%;">
                1
            </td>
            <td style="width: 25%;" align="left">
                《工程地质勘查报告》
            </td>
            <td style="width: 15%">
                <input id="Text209" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio29" type="radio" class="yes" name="1" value="1" />有<input id="Radio30"
                    type="radio" class="no" name="1" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text1" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                2
            </td>
            <td align="left">
                《地质测量、拨地和建设位置红线图》
            </td>
            <td style="width: 15%">
                <input id="Text2" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio1" type="radio" class="yes" name="2" value="1" />有<input id="Radio2"
                    type="radio" class="no" name="2" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text3" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                3
            </td>
            <td align="left">
                合同及《合同评审表》
            </td>
            <td style="width: 15%">
                <input id="Text4" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio3" type="radio" class="yes" name="3" value="1" />有<input id="Radio4"
                    type="radio" class="no" name="3" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text5" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                4
            </td>
            <td align="left">
                有关部门立项报告、计划批文
            </td>
            <td style="width: 15%">
                <input id="Text6" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio5" type="radio" class="yes" name="4" value="1" />有<input id="Radio6"
                    type="radio" class="no" name="4" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text7" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                5
            </td>
            <td align="left">
                《设计图纸校对、审核、审定卡》<br />
                （按 建、结、水、暖、电、概算单独装订)
            </td>
            <td style="width: 15%">
                <input id="Text8" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio7" type="radio" class="yes" name="5" value="1" />有<input id="Radio8"
                    type="radio" class="no" name="5" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text9" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                6
            </td>
            <td align="left">
                顾客提供的设计要求
            </td>
            <td style="width: 15%">
                <input id="Text10" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio9" type="radio" class="yes" name="6" value="1" />有<input id="Radio10"
                    type="radio" class="no" name="6" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text11" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                7
            </td>
            <td align="left">
                《顾客提供资料记录卡》
            </td>
            <td style="width: 15%">
                <input id="Text12" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio11" type="radio" class="yes" name="7" value="1" />有<input id="Radio12"
                    type="radio" class="no" name="7" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text13" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                8
            </td>
            <td align="left">
                各专业计算书(按建、结、水、暖、电、概算单独装订)
            </td>
            <td style="width: 15%">
                <input id="Text14" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio13" type="radio" class="yes" name="8" value="1" />有<input id="Radio14"
                    type="radio" class="no" name="8" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text15" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                9
            </td>
            <td align="left">
                其他相关资料
            </td>
            <td style="width: 15%">
                <input id="Text16" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio15" type="radio" class="yes" name="9" value="1" />有<input id="Radio16"
                    type="radio" class="no" name="9" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text17" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                10
            </td>
            <td align="left">
                <input id="Text50" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%">
                <input id="Text46" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio47" type="radio" class="yes" name="10" value="1" />有<input id="Radio48"
                    type="radio" class="no" name="10" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text47" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                11
            </td>
            <td align="left">
                <input id="Text51" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%">
                <input id="Text48" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio49" type="radio" class="yes" name="11" value="1" />有<input id="Radio50"
                    type="radio" class="no" name="11" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text49" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                12
            </td>
            <td align="left">
                《勘查设计任务通知单》
            </td>
            <td style="width: 15%">
                <input id="Text18" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio17" type="radio" class="yes" name="12" value="1" />有<input id="Radio18"
                    type="radio" class="no" name="12" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text19" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                13
            </td>
            <td align="left">
                《工程项目进岗人员审批表》
            </td>
            <td style="width: 15%">
                <input id="Text20" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio19" type="radio" class="yes" name="13" value="1" />有<input id="Radio20"
                    type="radio" class="no" name="13" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text21" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                14
            </td>
            <td align="left">
                《项目进度计划表》
            </td>
            <td style="width: 15%">
                <input id="Text22" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio21" type="radio" class="yes" name="14" value="1" />有<input id="Radio22"
                    type="radio" class="no" name="14" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text23" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                15
            </td>
            <td align="left">
                《专业间设计互提资料单》
            </td>
            <td style="width: 15%">
                <input id="Text24" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio23" type="radio" class="yes" name="15" value="1" />有<input id="Radio24"
                    type="radio" class="no" name="15" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text25" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                16
            </td>
            <td align="left">
                《设计输入评审记录》
            </td>
            <td style="width: 15%">
                <input id="Text26" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio25" type="radio" class="yes" name="16" value="1" />有<input id="Radio26"
                    type="radio" class="no" name="16" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text27" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                17
            </td>
            <td align="left">
                《设计评审记录》
            </td>
            <td style="width: 15%">
                <input id="Text28" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio27" type="radio" class="yes" name="17" value="1" />有<input id="Radio28"
                    type="radio" class="no" name="17" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text29" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                18
            </td>
            <td align="left">
                《设计综合质量评定表》
            </td>
            <td style="width: 15%">
                <input id="Text30" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio31" type="radio" class="yes" name="18" value="1" />有<input id="Radio32"
                    type="radio" class="no" name="18" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text31" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                19
            </td>
            <td align="left">
                《工程设计出图卡》
            </td>
            <td style="width: 15%">
                <input id="Text32" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio33" type="radio" class="yes" name="19" value="1" />有<input id="Radio34"
                    type="radio" class="no" name="19" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text33" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                20
            </td>
            <td align="left">
                《设计更改通知单》
            </td>
            <td style="width: 15%">
                <input id="Text34" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio35" type="radio" class="yes" name="20" value="1" />有<input id="Radio36"
                    type="radio" class="no" name="20" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text35" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                21
            </td>
            <td align="left">
                《现场服务记录》
            </td>
            <td style="width: 15%">
                <input id="Text36" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio37" type="radio" class="yes" name="21" value="1" />有<input id="Radio38"
                    type="radio" class="no" name="21" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text37" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                22
            </td>
            <td align="left">
                《工程回访记录单》
            </td>
            <td style="width: 15%">
                <input id="Text38" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio39" type="radio" class="yes" name="22" value="1" />有<input id="Radio40"
                    type="radio" class="no" name="22" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text39" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                23
            </td>
            <td align="left">
                其他需要加盖院出图章的质量记录
            </td>
            <td style="width: 15%">
                <input id="Text40" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio41" type="radio" class="yes" name="23" value="1" />有<input id="Radio42"
                    type="radio" class="no" name="23" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text41" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                24
            </td>
            <td align="left">
                《建设工程施工图审查批准书》
            </td>
            <td style="width: 15%">
                <input id="Text42" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio43" type="radio" class="yes" name="24" value="1" />有<input id="Radio44"
                    type="radio" class="no" name="24" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text43" type="text" runat="server" class="input" />
            </td>
        </tr>
        <tr>
            <td>
                25
            </td>
            <td align="left">
                《施工图审查报告》及《施工图审查报告答复》
            </td>
            <td style="width: 15%">
                <input id="Text44" type="text" runat="server" class="input" />
            </td>
            <td style="width: 15%;">
                <input id="Radio45" type="radio" class="yes" name="25" value="1" />有<input id="Radio46"
                    type="radio" class="no" name="25" value="0" checked="checked" />无
            </td>
            <td style="width: 35%" align="left">
                <input id="Text45" type="text" runat="server" class="input" />
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben">
        <tr>
            <td style="height: 23px; background-color: #f0f0f0" colspan="4" align="center">
                <a href="###" id="btn_save">
                    <img src="/Images/buttons/btn_save2.gif" style="width: 64px; height: 23px; border: none;" /></a>
                &nbsp;<a href="ProjectFileList.aspx" target="_self"><img src="/Images/buttons/btn_back2.gif"
                    style="width: 65px; height: 23px; border: none;" /></a>
            </td>
        </tr>
    </table>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <input id="HidProID" type="hidden" value="<%=ProjectSysNo %>" />
    <input id="HidUserID" type="hidden" value="<%=UserSysNo %>" />
    </form>
</body>
</html>
