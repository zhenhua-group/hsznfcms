﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProPrint.aspx.cs" Inherits="TG.Web.ProjectManage.ProImageAudit.ProPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link type="text/css" rel="Stylesheet" href="/css/m_comm.css" />
    <link type="text/css" rel="Stylesheet" href="~/css/Customer.css" />
    <link type="text/css" href="~/css/jquery.alerts.css" rel="Stylesheet" />
    <link type="text/css" rel="stylesheet" href="/css/swfupload/default_cpr.css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <title>工程设计出图卡</title>
</head>
<script>
    function preview(oper) {
        if (oper < 10) {
            bdhtml = window.document.body.innerHTML; //获取当前页的html代码
            sprnstr = "<!--startprint" + oper + "-->"; //设置打印开始区域
            eprnstr = "<!--endprint" + oper + "-->"; //设置打印结束区域
            prnhtml = bdhtml.substring(bdhtml.indexOf(sprnstr) + 18); //从开始代码向后取html

            prnhtml = prnhtml.substring(0, prnhtml.indexOf(eprnstr)); //从结束代码向前取html
            window.document.body.innerHTML = prnhtml;
            window.print();
            window.document.body.innerHTML = bdhtml;


        } else {
            window.print();
        }

    }
</script>
<body style="font-size: 12px;font-family: ""微软雅黑"";">
    <form id="form1" runat="server">
<!--startprint1-->
  <table width="95%" align="center" border=0><tr><td align=center height="30"><strong style="font-size:14px;">工程设计出图卡</strong></td></tr></table>
               <table width="95%" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#000000">
  <tr>
    <td width="16%" height="30" align="center" bgcolor="#FFFFFF">工程名称</td>
    <td height="30" colspan="5" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_proname" runat="server"></asp:Label></td>
    <td width="13%" height="30" align="center" bgcolor="#FFFFFF">工程号</td>
    <td height="30" colspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_pronumber" runat="server"></asp:Label></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">子项名称</td>
    <td height="30" colspan="5" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_proitemname" runat="server"></asp:Label></td>
    <td height="30" align="center" bgcolor="#FFFFFF">承担部门</td>
    <td height="30" colspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_prounit" runat="server"></asp:Label></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">设&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;总</td>
    <td height="30" colspan="5" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_pm_name" runat="server" ></asp:Label></td>
    <td height="30" align="center" bgcolor="#FFFFFF">设计阶段</td>
    <td height="30" colspan="2" align="center" bgcolor="#FFFFFF"><asp:Label ID="txt_prostatus" runat="server" ></asp:Label></td>
  </tr>
  <tr>
    <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">专&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;业</td>
    <td width="10%" height="30" align="center" bgcolor="#FFFFFF">0#图</td>
    <td width="9%" height="30" align="center" bgcolor="#FFFFFF">1#图</td>
    <td width="9%" height="30" align="center" bgcolor="#FFFFFF">2#图</td>
    <td width="9%" height="30" align="center" bgcolor="#FFFFFF">2#加长</td>
    <td height="30" align="center" bgcolor="#FFFFFF">3#图</td>
    <td width="10%" height="30" align="center" bgcolor="#FFFFFF">4#图</td>
    <td width="12%" height="30" align="center" bgcolor="#FFFFFF">折合1#图</td>
  </tr>
  <tr>
    <td rowspan="2" align="center" bgcolor="#FFFFFF">建筑</td>
    <td  width="12%" height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="jz0" runat="server"  Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="jz1" runat="server"  Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="jz2" runat="server"  Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="jz2_1" runat="server"  Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="jz3" runat="server"  Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="jz4" runat="server"  Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="jz1_1" runat="server"  Text="0" /></td>
   
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">CAD</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadjz0" runat="server" Text="0"   /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadjz1" runat="server" Text="0"   /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="cadjz2" runat="server" Text="0"   /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="cadjz2_1" runat="server" Text="0"   /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="cadjz3" runat="server" Text="0"   /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadjz4" runat="server" Text="0"   /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="cadjz1_1" runat="server" Text="0"   /></td>
  </tr>
  <tr>
    <td rowspan="2" align="center" bgcolor="#FFFFFF">结构</td>
    <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="jg0" runat="server"   Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="jg1" runat="server"   Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="jg2" runat="server"   Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="jg2_1" runat="server"   Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="jg3" runat="server"   Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="jg4" runat="server"   Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="jg1_1" runat="server"   Text="0" /></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">CAD</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadjg0" runat="server" Text="0"   /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="cadjg1" runat="server" Text="0"   /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadjg2" runat="server" Text="0"   /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="cadjg2_1" runat="server" Text="0"   /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="cadjg3" runat="server" Text="0"   /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadjg4" runat="server" Text="0"   /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="cadjg1_1" runat="server" Text="0"   /></td>
  </tr>
  <tr>
    <td rowspan="2" align="center" bgcolor="#FFFFFF">给排水</td>
    <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="gps0" runat="server"   Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="gps1" runat="server"   Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="gps2" runat="server"   Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="gps2_1" runat="server"   Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="gps3" runat="server"   Text="0" /></td>
   <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="gps4" runat="server"   Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="gps1_1" runat="server"   Text="0" /></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">CAD</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadgps0" runat="server" Text="0" /></td>
   <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadgps1" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadgps2" runat="server" Text="0" /></td>
   <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadgps2_1" runat="server" Text="0" /></td>
   <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadgps3" runat="server" Text="0" /></td>
   <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadgps4" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadgps1_1" runat="server" Text="0" /></td>
  </tr>
  <tr>
    <td rowspan="2" align="center" bgcolor="#FFFFFF">暖通</td>
    <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="nt0" runat="server" Text="0" /></td>
   <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="nt1" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="nt2" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="nt2_1" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="nt3" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="nt4" runat="server" Text="0" /></td>
   <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="nt1_1" runat="server" Text="0" /></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">CAD</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadnt0" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadnt1" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadnt2" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadnt2_1" runat="server" Text="0" /></td>
   <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadnt3" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadnt4" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadnt1_1" runat="server" Text="0" /></td>
  </tr>
  <tr>
    <td rowspan="2" align="center" bgcolor="#FFFFFF">电气</td>
    <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="dq0" runat="server"  Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="dq1" runat="server"  Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="dq2" runat="server"  Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="dq2_1" runat="server"  Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="dq3" runat="server"  Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="dq4" runat="server"  Text="0" /></td>
   <td height="30" align="center" bgcolor="#FFFFFF"> <asp:Label ID="dq1_1" runat="server"  Text="0" /></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">CAD</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="caddq0" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="caddq1" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="caddq2" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="caddq2_1" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="caddq3" runat="server" Text="0" /></td>
   <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="caddq4" runat="server" Text="0" /></td>
   <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="caddq1_1" runat="server" Text="0" /></td>
  </tr>
  <tr>
    <td rowspan="2" align="center" bgcolor="#FFFFFF">合计</td>
    <td height="30" align="center" bgcolor="#FFFFFF">&nbsp;</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj0" runat="server"  Text="0" /></td>
     <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj1" runat="server"  Text="0" /></td>
      <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj2" runat="server"  Text="0" /></td>
     <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj2_1" runat="server"  Text="0" /></td>
     <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj3" runat="server"  Text="0" /></td>
      <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj4" runat="server"  Text="0" /></td>
      <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="hj1_1" runat="server"  Text="0" /></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">CAD</td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadhj0" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadhj1" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadhj2" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadhj2_1" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadhj3" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadhj4" runat="server" Text="0" /></td>
    <td height="30" align="center" bgcolor="#FFFFFF"><asp:Label ID="cadhj1_1" runat="server" Text="0" /></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">图纸类别</td>
    <td height="30" colspan="8" align="center" bgcolor="#FFFFFF">新出图（<%=ct=="新出图"?"V":"" %>）&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  补充图（<%=ct=="补充图"?"V":"" %>） &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;变更图（<%=ct=="变更图"?"V":"" %>）</td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">晒图份数</td>
    <td height="30" colspan="3" align="center" bgcolor="#FFFFFF"><asp:Label ID="Sum_image" runat="server" Text="0"  /></td>
    <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">是否归档</td>
    <td height="30" colspan="3" align="center" bgcolor="#FFFFFF">  <asp:Label ID="check_Gd" runat="server" Text="0"  /></td>
  </tr>
  <tr>
    <td height="30" align="center" bgcolor="#FFFFFF">技术质量部</td>
    <td height="30" colspan="3" align="center" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="35%" height="50">盖章：</td>
        <td width="33%">&nbsp;</td>
        <td width="32%">&nbsp;</td>
      </tr>
      <tr>
        <td height="30">&nbsp;</td>
        <td>签字：</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="right">年</td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;月 &nbsp;&nbsp;&nbsp;日</td>
      </tr>
    </table></td>
    <td height="30" colspan="2" align="center" bgcolor="#FFFFFF">生成经营部</td>
    <td height="30" colspan="3" align="center" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="35%" height="50">盖章：</td>
        <td width="39%">&nbsp;</td>
        <td width="26%">&nbsp;</td>
      </tr>
      <tr>
        <td height="30">&nbsp;</td>
        <td>签字：</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="right">年</td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;月&nbsp;&nbsp;&nbsp;日</td>
      </tr>
    </table></td>
  </tr>
</table>
<!--endprint1-->
<table width="95%" align="center"><tr><td align=center><input type="button" onclick="preview(1);" value=" " style="background:url(../../images/buttons/btn_printgif.gif); width:65px; height:23px; border:0; cursor:pointer;" /></td></tr></table>
    </form>
</body>
</html>
