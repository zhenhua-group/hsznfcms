﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace TG.Web.ProjectManage.ProImageAudit
{
    public partial class PrintFileInfo : System.Web.UI.Page
    {

        /// <summary>
        /// 项目SysNo
        /// </summary>
        public int ProjectSysNo
        {
            get
            {
                int projectSysNo = 0;
                int.TryParse(Request["ProjectSysNo"], out projectSysNo);
                return projectSysNo;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindProjectInfo();
                bindFileInfo();
            }
        }

        /// <summary>
        /// 绑定项目信息
        /// </summary>
        private void bindProjectInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(ProjectSysNo);
            if (pro_model != null)
            {
                txt_proname.Text = pro_model.pro_name;
                txt_pronumber.Text = pro_model.Pro_number;
                lblProcess.Text = pro_model.BuildType;
                lbl_PMName.Text = pro_model.PMName;
                txt_prounit.Text = pro_model.Unit;
            }
        }

        /// <summary>
        /// 取得信息
        /// </summary>
        private void bindFileInfo()
        {
            TG.BLL.cm_ProjectFileInfo bll = new TG.BLL.cm_ProjectFileInfo();
            DataTable dt = bll.GetFileInfo(ProjectSysNo).Tables[0];

            StringBuilder sb = new StringBuilder();

            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("<tr>");
                    sb.Append(" <td style=\"width: 10%;\" align=\"center\">" + dt.Rows[i]["num"] + "</td>");
                    sb.Append("<td style=\"width: 35%;\" align=\"left\" colspan=\"3\">" + dt.Rows[i]["name"] + "</td>");

                    sb.Append(" <td style=\"width: 15%\"  align=\"center\">" + dt.Rows[i]["PageCount"] + "  </td>");
                    string isExist = dt.Rows[i]["IsExist"].ToString();
                    if (isExist == "0")
                    {
                        sb.Append("<td style=\"width: 15%;\" align=\"center\"><input id=\"Radio29\" type=\"radio\" class=\"yes\" name=" + i + " value=\"1\" />有<input id=\"Radio30\" type=\"radio\" class=\"no\" name=" + i + " value=\"0\" checked=\"checked\" />无 </td>");

                    }
                    else
                    {
                        sb.Append("<td style=\"width: 15%;\" align=\"center\"><input id=\"Radio29\" type=\"radio\" class=\"yes\" name=" + i + " value=\"1\" checked=\"checked\"/>有<input id=\"Radio30\" type=\"radio\" class=\"no\" name=" + i + " value=\"0\"  />无 </td>");
                    }
                    sb.Append("<td style=\"width: 25%\" align=\"left\"> " + dt.Rows[i]["Remark"] + " </td>");
                    sb.Append("</tr>");
                }

            }

            lblFileInfo.Text = sb.ToString();
        }
    }
}