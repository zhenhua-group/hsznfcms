﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace TG.Web.ProjectManage.ProImageAudit
{
    public partial class ModifyProjectDesignCards : PageBase
    {
        /// <summary>
        /// 项目SysNo
        /// </summary>
        public int ProjectSysNo
        {
            get
            {
                int projectSysNo = 0;
                int.TryParse(Request["project_Id"], out projectSysNo);
                return projectSysNo;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //出图类型
                BindPlotTypeList();

                //图幅
                BindMapsheetList();

                //加长倍数
                BindLengthenTypeList();

                //绑定项目信息
                bindProjectInfo();

                //绑定出图类别
                bindGetPlotInfo();

                //绑定子项目信息
                bindSubPlotInfo();

                //获得并绑定权限
                BindPreviewPower();
            }
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;

                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }

        /// <summary>
        /// 绑定项目信息
        /// </summary>
        private void bindProjectInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(ProjectSysNo);
            if (pro_model != null)
            {
                txt_proname.Text = pro_model.pro_name;
                txt_pronumber.Text = pro_model.Pro_number;
                txt_proitemname.Text = pro_model.pro_name;
                txt_prostatus.Text = pro_model.pro_status;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }
                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }
                lbl_PMName.Text = pro_model.PMName;
                txt_prounit.Text = pro_model.Unit;
            }
        }

        /// <summary>
        /// 绑定信息
        /// </summary>
        private void bindGetPlotInfo()
        {
            TG.BLL.cm_ProjectPlotInfo bll = new TG.BLL.cm_ProjectPlotInfo();
            TG.Model.cm_ProjectPlotInfo model = bll.GetPlotInfo(ProjectSysNo);
            if (model != null)
            {
                string plotType = model.PlotType;
                if (this.rbtlist.Items.FindByText(plotType) != null)
                {
                    this.rbtlist.Items.FindByText(plotType).Selected = true;
                }
                txt_BluePrintCounts.Value = model.BlueprintCounts.ToString();
                this.txt_PMName.Value = model.PlotUserName;
                this.hid_pmname.Value = model.PlotUserName;
                this.hid_pmuserid.Value = model.PlotUserID.ToString();
            }
        }
        /// <summary>
        /// 出图类型
        /// </summary>
        public void BindPlotTypeList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定出图类型
            DataSet dic_proly = dic.GetList(" dic_type='plot_type'");
            rbtlist.DataValueField = "id";
            rbtlist.DataTextField = "dic_name";
            rbtlist.DataSource = dic_proly;
            rbtlist.DataBind();
        }

        /// <summary>
        /// 绑定图幅
        /// </summary>
        private void BindMapsheetList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定项目来源
            DataSet dic_proly = dic.GetList(" dic_type='map_sheet'");
            drop_Mapsheet.DataValueField = "id";
            drop_Mapsheet.DataTextField = "dic_name";
            drop_Mapsheet.DataSource = dic_proly;
            drop_Mapsheet.DataBind();
        }
        /// <summary>
        /// 绑定加长倍数
        /// </summary>
        private void BindLengthenTypeList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定项目来源
            DataSet dic_proly = dic.GetList(" dic_type='double_type'");
            drop_LengthenType.DataValueField = "id";
            drop_LengthenType.DataTextField = "dic_name";
            drop_LengthenType.DataSource = dic_proly;
            drop_LengthenType.DataBind();
        }

        private void bindSubPlotInfo()
        {
            //取得子项信息
            TG.BLL.cm_ProjectPlotInfo bll = new TG.BLL.cm_ProjectPlotInfo();
            DataTable dt = bll.GetSubPlotInfo(ProjectSysNo).Tables[0];

            StringBuilder sb = new StringBuilder();

            sb.Append("<table class=\"show_projectPlot show_projectPlot_Input\" style=\"width: 100%;\" id=\"tbSunPlotInfo\">");

            //建筑 标准长       
            int jzA0CountOne = 0;
            //建筑 1/4倍长            
            int jzA0CountTwo = 0;
            //建筑 2/4倍长
            int jzA0CountThree = 0;
            //建筑 3/4倍长
            int jzA0CountFour = 0;
            //建筑 A0倍长
            int jzA0CountFive = 0;
            //折合1#图
            decimal jzA0CountSix = 0;
            getPlotSuInfo(dt, "建筑", "A0", out jzA0CountOne, out jzA0CountTwo, out jzA0CountThree, out jzA0CountFour, out jzA0CountFive, out jzA0CountSix);

            sb.Append("<tr> <td rowspan=\"5\" style=\"width: 10%;\">建筑</td>");

            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\">  <span>A0</span> <input id=\"Text208\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A0bz\" value=" + jzA0CountOne + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\">  <span>A0</span> <input id=\"Text207\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A0fzy\" value=" + jzA0CountTwo + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\">  <span>A0</span> <input id=\"Text209\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A0fze\" value=" + jzA0CountThree + " />(张) </td>");
            sb.Append(" <td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text210\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A0fzs\" value=" + jzA0CountFour + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\">  <span>A0</span> <input id=\"Text211\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A0lb\" value=" + jzA0CountFive + " />(张) </td>");
            sb.Append("<td style=\"width: 15%;\" rowspan=\"5\">  <input id=\"totalJz\" type=\"text\" runat=\"server\" value=" + jzA0CountSix + " disabled=\"disabled\" />(张)  </td>");
            sb.Append("</tr>");

            //建筑 标准长       
            int jzA1CountOne = 0;
            //建筑 1/4倍长            
            int jzA1CountTwo = 0;
            //建筑 2/4倍长
            int jzA1CountThree = 0;
            //建筑 3/4倍长
            int jzA1CountFour = 0;
            //建筑 A0倍长
            int jzA1CountFive = 0;
            //折合1#图
            decimal jzA1CountSix = 0;
            getPlotSuInfo(dt, "建筑", "A1", out jzA1CountOne, out jzA1CountTwo, out jzA1CountThree, out jzA1CountFour, out jzA1CountFive, out jzA1CountSix);


            sb.Append(" <tr><td class=\"display\"> 建筑  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A1bz\" value=" + jzA1CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A1fzy\" value=" + jzA1CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A1fze\" value=" + jzA1CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A1fzs\" value=" + jzA1CountFour + "  />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A1lb\" value=" + jzA1CountFive + "  />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");


            //建筑 标准长       
            int jzA2CountOne = 0;
            //建筑 1/4倍长            
            int jzA2CountTwo = 0;
            //建筑 2/4倍长
            int jzA2CountThree = 0;
            //建筑 3/4倍长
            int jzA2CountFour = 0;
            //建筑 A0倍长
            int jzA2CountFive = 0;
            //折合1#图
            decimal jzA2CountSix = 0;
            getPlotSuInfo(dt, "建筑", "A2", out jzA2CountOne, out jzA2CountTwo, out jzA2CountThree, out jzA2CountFour, out jzA2CountFive, out jzA2CountSix);


            sb.Append(" <tr><td class=\"display\"> 建筑  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A2bz\" value=" + jzA2CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A2fzy\" value=" + jzA2CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A2fze\" value=" + jzA2CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A2fzs\" value=" + jzA2CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A2lb\" value=" + jzA2CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");


            //建筑 标准长       
            int jzA3CountOne = 0;
            //建筑 1/4倍长            
            int jzA3CountTwo = 0;
            //建筑 2/4倍长
            int jzA3CountThree = 0;
            //建筑 3/4倍长
            int jzA3CountFour = 0;
            //建筑 A0倍长
            int jzA3CountFive = 0;
            //折合1#图
            decimal jzA3CountSix = 0;
            getPlotSuInfo(dt, "建筑", "A3", out jzA3CountOne, out jzA3CountTwo, out jzA3CountThree, out jzA3CountFour, out jzA3CountFive, out jzA3CountSix);


            sb.Append(" <tr><td class=\"display\"> 建筑  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A3bz\" value=" + jzA3CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A3fzy\" value=" + jzA3CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A3fze\" value=" + jzA3CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A3fzs\" value=" + jzA3CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A3lb\" value=" + jzA3CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");

            //建筑 标准长       
            int jzA4CountOne = 0;
            //建筑 1/4倍长            
            int jzA4CountTwo = 0;
            //建筑 2/4倍长
            int jzA4CountThree = 0;
            //建筑 3/4倍长
            int jzA4CountFour = 0;
            //建筑 A0倍长
            int jzA4CountFive = 0;
            //折合1#图
            decimal jzA4CountSix = 0;
            getPlotSuInfo(dt, "建筑", "A4", out jzA4CountOne, out jzA4CountTwo, out jzA4CountThree, out jzA4CountFour, out jzA4CountFive, out jzA4CountSix);

            sb.Append(" <tr><td class=\"display\"> 建筑  </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A4bz\" value=" + jzA4CountOne + " />(张)  </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A4fzy\" value=" + jzA4CountTwo + " />(张) </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A4fze\" value=" + jzA4CountThree + " />(张) </td>");
            sb.Append("<td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A4fzs\" value=" + jzA4CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-top: 0px;\"> <span>A4</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"建筑\" lenghts=\"A4lb\" value=" + jzA4CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");



            //结构 标准长       
            int jgA0CountOne = 0;
            //结构 1/4倍长            
            int jgA0CountTwo = 0;
            //结构 2/4倍长
            int jgA0CountThree = 0;
            //结构 3/4倍长
            int jgA0CountFour = 0;
            //结构 A0倍长
            int jgA0CountFive = 0;
            //折合1#图
            decimal jgA0CountSix = 0;
            getPlotSuInfo(dt, "结构", "A0", out jgA0CountOne, out jgA0CountTwo, out jgA0CountThree, out jgA0CountFour, out jgA0CountFive, out jgA0CountSix);

            sb.Append("<tr> <td rowspan=\"5\" style=\"width: 10%;\">结构</td>");

            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text208\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A0bz\" value=" + jgA0CountOne + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text207\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A0fzy\" value=" + jgA0CountTwo + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text209\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A0fze\" value=" + jgA0CountThree + " />(张) </td>");
            sb.Append(" <td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text210\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A0fzs\" value=" + jgA0CountFour + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text211\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A0lb\" value=" + jgA0CountFive + " />(张) </td>");
            sb.Append("<td style=\"width: 15%;\" rowspan=\"5\">  <input id=\"totalJg\" type=\"text\" runat=\"server\" value=" + jgA0CountSix + " disabled=\"disabled\" />(张)  </td>");
            sb.Append("</tr>");

            //结构 标准长       
            int jgA1CountOne = 0;
            //结构 1/4倍长            
            int jgA1CountTwo = 0;
            //结构 2/4倍长
            int jgA1CountThree = 0;
            //结构 3/4倍长
            int jgA1CountFour = 0;
            //结构 A0倍长
            int jgA1CountFive = 0;
            //折合1#图
            decimal jgA1CountSix = 0;
            getPlotSuInfo(dt, "结构", "A1", out jgA1CountOne, out jgA1CountTwo, out jgA1CountThree, out jgA1CountFour, out jgA1CountFive, out jgA1CountSix);


            sb.Append(" <tr><td class=\"display\"> 结构  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A1bz\" value=" + jgA1CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A1fzy\" value=" + jgA1CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A1fze\" value=" + jgA1CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A1fzs\" value=" + jgA1CountFour + "  />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A1lb\" value=" + jgA1CountFive + "  />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");


            //结构 标准长       
            int jgA2CountOne = 0;
            //结构 1/4倍长            
            int jgA2CountTwo = 0;
            //结构 2/4倍长
            int jgA2CountThree = 0;
            //结构 3/4倍长
            int jgA2CountFour = 0;
            //结构 A0倍长
            int jgA2CountFive = 0;
            //折合1#图
            decimal jgA2CountSix = 0;
            getPlotSuInfo(dt, "结构", "A2", out jgA2CountOne, out jgA2CountTwo, out jgA2CountThree, out jgA2CountFour, out jgA2CountFive, out jgA2CountSix);


            sb.Append(" <tr><td class=\"display\"> 结构  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A2bz\" value=" + jgA2CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A2fzy\" value=" + jgA2CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A2fze\" value=" + jgA2CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A2fzs\" value=" + jgA2CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A2lb\" value=" + jgA2CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");


            //结构 标准长       
            int jgA3CountOne = 0;
            //结构 1/4倍长            
            int jgA3CountTwo = 0;
            //结构 2/4倍长
            int jgA3CountThree = 0;
            //结构 3/4倍长
            int jgA3CountFour = 0;
            //结构 A0倍长
            int jgA3CountFive = 0;
            //折合1#图
            decimal jgA3CountSix = 0;
            getPlotSuInfo(dt, "结构", "A3", out jgA3CountOne, out jgA3CountTwo, out jgA3CountThree, out jgA3CountFour, out jgA3CountFive, out jgA3CountSix);


            sb.Append(" <tr><td class=\"display\"> 结构  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A3bz\" value=" + jgA3CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A3fzy\" value=" + jgA3CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A3fze\" value=" + jgA3CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A3fzs\" value=" + jgA3CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A3lb\" value=" + jgA3CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");

            //结构 标准长       
            int jgA4CountOne = 0;
            //结构 1/4倍长            
            int jgA4CountTwo = 0;
            //结构 2/4倍长
            int jgA4CountThree = 0;
            //结构 3/4倍长
            int jgA4CountFour = 0;
            //结构 A0倍长
            int jgA4CountFive = 0;
            //折合1#图
            decimal jgA4CountSix = 0;
            getPlotSuInfo(dt, "结构", "A4", out jgA4CountOne, out jgA4CountTwo, out jgA4CountThree, out jgA4CountFour, out jgA4CountFive, out jgA4CountSix);

            sb.Append(" <tr><td class=\"display\"> 结构  </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A4bz\" value=" + jgA4CountOne + " />(张)  </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A4fzy\" value=" + jgA4CountTwo + " />(张) </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A4fze\" value=" + jgA4CountThree + " />(张) </td>");
            sb.Append("<td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A4fzs\" value=" + jgA4CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-top: 0px;\"> <span>A4</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"结构\" lenghts=\"A4lb\" value=" + jgA4CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");

            //给排水 标准长       
            int gpsA0CountOne = 0;
            //给排水 1/4倍长            
            int gpsA0CountTwo = 0;
            //给排水 2/4倍长
            int gpsA0CountThree = 0;
            //给排水 3/4倍长
            int gpsA0CountFour = 0;
            //给排水 A0倍长
            int gpsA0CountFive = 0;
            //折合1#图
            decimal gpsA0CountSix = 0;
            getPlotSuInfo(dt, "给排水", "A0", out gpsA0CountOne, out gpsA0CountTwo, out gpsA0CountThree, out gpsA0CountFour, out gpsA0CountFive, out gpsA0CountSix);

            sb.Append("<tr> <td rowspan=\"5\" style=\"width: 10%;\">给排水</td>");

            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text208\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A0bz\" value=" + gpsA0CountOne + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text207\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A0fzy\" value=" + gpsA0CountTwo + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text209\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A0fze\" value=" + gpsA0CountThree + " />(张) </td>");
            sb.Append(" <td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text210\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A0fzs\" value=" + gpsA0CountFour + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\">  <span>A0</span> <input id=\"Text211\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A0lb\" value=" + gpsA0CountFive + " />(张) </td>");
            sb.Append("<td style=\"width: 15%;\" rowspan=\"5\">  <input id=\"totalGps\" type=\"text\" runat=\"server\" value=" + gpsA0CountSix + " disabled=\"disabled\" />(张)  </td>");
            sb.Append("</tr>");

            //给排水 标准长       
            int gpsA1CountOne = 0;
            //给排水 1/4倍长            
            int gpsA1CountTwo = 0;
            //给排水 2/4倍长
            int gpsA1CountThree = 0;
            //给排水 3/4倍长
            int gpsA1CountFour = 0;
            //给排水 A0倍长
            int gpsA1CountFive = 0;
            //折合1#图
            decimal gpsA1CountSix = 0;
            getPlotSuInfo(dt, "给排水", "A1", out gpsA1CountOne, out gpsA1CountTwo, out gpsA1CountThree, out gpsA1CountFour, out gpsA1CountFive, out gpsA1CountSix);


            sb.Append(" <tr><td class=\"display\"> 给排水  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A1bz\" value=" + gpsA1CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A1fzy\" value=" + gpsA1CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A1fze\" value=" + gpsA1CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A1fzs\" value=" + gpsA1CountFour + "  />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A1lb\" value=" + gpsA1CountFive + "  />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");


            //给排水 标准长       
            int gpsA2CountOne = 0;
            //给排水 1/4倍长            
            int gpsA2CountTwo = 0;
            //给排水 2/4倍长
            int gpsA2CountThree = 0;
            //给排水 3/4倍长
            int gpsA2CountFour = 0;
            //给排水 A0倍长
            int gpsA2CountFive = 0;
            //折合1#图
            decimal gpsA2CountSix = 0;
            getPlotSuInfo(dt, "给排水", "A2", out gpsA2CountOne, out gpsA2CountTwo, out gpsA2CountThree, out gpsA2CountFour, out gpsA2CountFive, out gpsA2CountSix);


            sb.Append(" <tr><td class=\"display\"> 给排水  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A2bz\" value=" + gpsA2CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A2fzy\" value=" + gpsA2CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A2fze\" value=" + gpsA2CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A2fzs\" value=" + gpsA2CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A2lb\" value=" + gpsA2CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");


            //给排水 标准长       
            int gpsA3CountOne = 0;
            //给排水 1/4倍长            
            int gpsA3CountTwo = 0;
            //给排水 2/4倍长
            int gpsA3CountThree = 0;
            //给排水 3/4倍长
            int gpsA3CountFour = 0;
            //给排水 A0倍长
            int gpsA3CountFive = 0;
            //折合1#图
            decimal gpsA3CountSix = 0;
            getPlotSuInfo(dt, "给排水", "A3", out gpsA3CountOne, out gpsA3CountTwo, out gpsA3CountThree, out gpsA3CountFour, out gpsA3CountFive, out gpsA3CountSix);


            sb.Append(" <tr><td class=\"display\"> 给排水  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A3bz\" value=" + gpsA3CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A3fzy\" value=" + gpsA3CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A3fze\" value=" + gpsA3CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A3fzs\" value=" + gpsA3CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A3lb\" value=" + gpsA3CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");

            //给排水 标准长       
            int gpsA4CountOne = 0;
            //给排水 1/4倍长            
            int gpsA4CountTwo = 0;
            //给排水 2/4倍长
            int gpsA4CountThree = 0;
            //给排水 3/4倍长
            int gpsA4CountFour = 0;
            //给排水 A0倍长
            int gpsA4CountFive = 0;
            //折合1#图
            decimal gpsA4CountSix = 0;
            getPlotSuInfo(dt, "给排水", "A4", out gpsA4CountOne, out gpsA4CountTwo, out gpsA4CountThree, out gpsA4CountFour, out gpsA4CountFive, out gpsA4CountSix);

            sb.Append(" <tr><td class=\"display\"> 给排水  </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A4bz\" value=" + gpsA4CountOne + " />(张)  </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A4fzy\" value=" + gpsA4CountTwo + " />(张) </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A4fze\" value=" + gpsA4CountThree + " />(张) </td>");
            sb.Append("<td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A4fzs\" value=" + gpsA4CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-top: 0px;\"> <span>A4</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"给排水\" lenghts=\"A4lb\" value=" + gpsA4CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");

            //暖通 标准长       
            int ntA0CountOne = 0;
            //暖通 1/4倍长            
            int ntA0CountTwo = 0;
            //暖通 2/4倍长
            int ntA0CountThree = 0;
            //暖通 3/4倍长
            int ntA0CountFour = 0;
            //暖通 A0倍长
            int ntA0CountFive = 0;
            //折合1#图
            decimal ntA0CountSix = 0;
            getPlotSuInfo(dt, "暖通", "A0", out ntA0CountOne, out ntA0CountTwo, out ntA0CountThree, out ntA0CountFour, out ntA0CountFive, out ntA0CountSix);

            sb.Append("<tr> <td rowspan=\"5\" style=\"width: 10%;\">暖通</td>");

            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text208\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A0bz\" value=" + ntA0CountOne + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text207\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A0fzy\" value=" + ntA0CountTwo + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text209\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A0fze\" value=" + ntA0CountThree + " />(张) </td>");
            sb.Append(" <td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text210\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A0fzs\" value=" + ntA0CountFour + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\">  <span>A0</span> <input id=\"Text211\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A0lb\" value=" + ntA0CountFive + " />(张) </td>");
            sb.Append("<td style=\"width: 15%;\" rowspan=\"5\">  <input id=\"totalNt\" type=\"text\" runat=\"server\" value=" + ntA0CountSix + " disabled=\"disabled\" />(张)  </td>");
            sb.Append("</tr>");

            //暖通 标准长       
            int ntA1CountOne = 0;
            //暖通 1/4倍长            
            int ntA1CountTwo = 0;
            //暖通 2/4倍长
            int ntA1CountThree = 0;
            //暖通 3/4倍长
            int ntA1CountFour = 0;
            //暖通 A0倍长
            int ntA1CountFive = 0;
            //折合1#图
            decimal ntA1CountSix = 0;
            getPlotSuInfo(dt, "暖通", "A1", out ntA1CountOne, out ntA1CountTwo, out ntA1CountThree, out ntA1CountFour, out ntA1CountFive, out ntA1CountSix);


            sb.Append(" <tr><td class=\"display\"> 暖通  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A1bz\" value=" + ntA1CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A1fzy\" value=" + ntA1CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A1fze\" value=" + ntA1CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A1fzs\" value=" + ntA1CountFour + "  />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A1lb\" value=" + ntA1CountFive + "  />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");


            //暖通 标准长       
            int ntA2CountOne = 0;
            //暖通 1/4倍长            
            int ntA2CountTwo = 0;
            //暖通 2/4倍长
            int ntA2CountThree = 0;
            //暖通 3/4倍长
            int ntA2CountFour = 0;
            //暖通 A0倍长
            int ntA2CountFive = 0;
            //折合1#图
            decimal ntA2CountSix = 0;
            getPlotSuInfo(dt, "暖通", "A2", out ntA2CountOne, out ntA2CountTwo, out ntA2CountThree, out ntA2CountFour, out ntA2CountFive, out ntA2CountSix);


            sb.Append(" <tr><td class=\"display\"> 暖通  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A2bz\" value=" + ntA2CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A2fzy\" value=" + ntA2CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A2fze\" value=" + ntA2CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A2fzs\" value=" + ntA2CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A2lb\" value=" + ntA2CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");


            //暖通 标准长       
            int ntA3CountOne = 0;
            //暖通 1/4倍长            
            int ntA3CountTwo = 0;
            //暖通 2/4倍长
            int ntA3CountThree = 0;
            //暖通 3/4倍长
            int ntA3CountFour = 0;
            //暖通 A0倍长
            int ntA3CountFive = 0;
            //折合1#图
            decimal ntA3CountSix = 0;
            getPlotSuInfo(dt, "暖通", "A3", out ntA3CountOne, out ntA3CountTwo, out ntA3CountThree, out ntA3CountFour, out ntA3CountFive, out ntA3CountSix);


            sb.Append(" <tr><td class=\"display\"> 暖通  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A3bz\" value=" + ntA3CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A3fzy\" value=" + ntA3CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A3fze\" value=" + ntA3CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A3fzs\" value=" + ntA3CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A3lb\" value=" + ntA3CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");

            //暖通 标准长       
            int ntA4CountOne = 0;
            //暖通 1/4倍长            
            int ntA4CountTwo = 0;
            //暖通 2/4倍长
            int ntA4CountThree = 0;
            //暖通 3/4倍长
            int ntA4CountFour = 0;
            //暖通 A0倍长
            int ntA4CountFive = 0;
            //折合1#图
            decimal ntA4CountSix = 0;
            getPlotSuInfo(dt, "暖通", "A4", out ntA4CountOne, out ntA4CountTwo, out ntA4CountThree, out ntA4CountFour, out ntA4CountFive, out ntA4CountSix);

            sb.Append(" <tr><td class=\"display\"> 暖通  </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A4bz\" value=" + ntA4CountOne + " />(张)  </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A4fzy\" value=" + ntA4CountTwo + " />(张) </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A4fze\" value=" + ntA4CountThree + " />(张) </td>");
            sb.Append("<td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A4fzs\" value=" + ntA4CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-top: 0px;\"> <span>A4</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"暖通\" lenghts=\"A4lb\" value=" + ntA4CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");

            //电气 标准长       
            int dqA0CountOne = 0;
            //电气 1/4倍长            
            int dqA0CountTwo = 0;
            //电气 2/4倍长
            int dqA0CountThree = 0;
            //电气 3/4倍长
            int dqA0CountFour = 0;
            //电气 A0倍长
            int dqA0CountFive = 0;
            //折合1#图
            decimal dqA0CountSix = 0;
            getPlotSuInfo(dt, "电气", "A0", out dqA0CountOne, out dqA0CountTwo, out dqA0CountThree, out dqA0CountFour, out dqA0CountFive, out dqA0CountSix);

            sb.Append("<tr> <td rowspan=\"5\" style=\"width: 10%;\">电气</td>");

            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text208\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A0bz\" value=" + dqA0CountOne + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text207\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A0fzy\" value=" + dqA0CountTwo + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text209\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A0fze\" value=" + dqA0CountThree + " />(张) </td>");
            sb.Append(" <td style=\"width: 15%; border-bottom: 0px;\"> <span>A0</span> <input id=\"Text210\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A0fzs\" value=" + dqA0CountFour + " />(张) </td>");
            sb.Append("<td style=\"width: 15%; border-bottom: 0px;\">  <span>A0</span> <input id=\"Text211\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A0lb\" value=" + dqA0CountFive + " />(张) </td>");
            sb.Append("<td style=\"width: 15%;\" rowspan=\"5\">  <input id=\"totalDq\" type=\"text\" runat=\"server\" value=" + dqA0CountSix + " disabled=\"disabled\" />(张)  </td>");
            sb.Append("</tr>");

            //电气 标准长       
            int dqA1CountOne = 0;
            //电气 1/4倍长            
            int dqA1CountTwo = 0;
            //电气 2/4倍长
            int dqA1CountThree = 0;
            //电气 3/4倍长
            int dqA1CountFour = 0;
            //电气 A0倍长
            int dqA1CountFive = 0;
            //折合1#图
            decimal dqA1CountSix = 0;
            getPlotSuInfo(dt, "电气", "A1", out dqA1CountOne, out dqA1CountTwo, out dqA1CountThree, out dqA1CountFour, out dqA1CountFive, out dqA1CountSix);


            sb.Append(" <tr><td class=\"display\"> 电气  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A1bz\" value=" + dqA1CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A1fzy\" value=" + dqA1CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A1fze\" value=" + dqA1CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A1fzs\" value=" + dqA1CountFour + "  />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A1</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A1lb\" value=" + dqA1CountFive + "  />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");


            //电气 标准长       
            int dqA2CountOne = 0;
            //电气 1/4倍长            
            int dqA2CountTwo = 0;
            //电气 2/4倍长
            int dqA2CountThree = 0;
            //电气 3/4倍长
            int dqA2CountFour = 0;
            //电气 A0倍长
            int dqA2CountFive = 0;
            //折合1#图
            decimal dqA2CountSix = 0;
            getPlotSuInfo(dt, "电气", "A2", out dqA2CountOne, out dqA2CountTwo, out dqA2CountThree, out dqA2CountFour, out dqA2CountFive, out dqA2CountSix);


            sb.Append(" <tr><td class=\"display\"> 电气  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A2bz\" value=" + dqA2CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A2fzy\" value=" + dqA2CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A2fze\" value=" + dqA2CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A2fzs\" value=" + dqA2CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A2</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A2lb\" value=" + dqA2CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");


            //电气 标准长       
            int dqA3CountOne = 0;
            //电气 1/4倍长            
            int dqA3CountTwo = 0;
            //电气 2/4倍长
            int dqA3CountThree = 0;
            //电气 3/4倍长
            int dqA3CountFour = 0;
            //电气 A0倍长
            int dqA3CountFive = 0;
            //折合1#图
            decimal dqA3CountSix = 0;
            getPlotSuInfo(dt, "电气", "A3", out dqA3CountOne, out dqA3CountTwo, out dqA3CountThree, out dqA3CountFour, out dqA3CountFive, out dqA3CountSix);


            sb.Append(" <tr><td class=\"display\"> 电气  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A3bz\" value=" + dqA3CountOne + " />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A3fzy\" value=" + dqA3CountTwo + " />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A3fze\" value=" + dqA3CountThree + " />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A3fzs\" value=" + dqA3CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px; border-top: 0px;\"> <span>A3</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A3lb\" value=" + dqA3CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");

            //电气 标准长       
            int dqA4CountOne = 0;
            //电气 1/4倍长            
            int dqA4CountTwo = 0;
            //电气 2/4倍长
            int dqA4CountThree = 0;
            //电气 3/4倍长
            int dqA4CountFour = 0;
            //电气 A0倍长
            int dqA4CountFive = 0;
            //折合1#图
            decimal dqA4CountSix = 0;
            getPlotSuInfo(dt, "电气", "A4", out dqA4CountOne, out dqA4CountTwo, out dqA4CountThree, out dqA4CountFour, out dqA4CountFive, out dqA4CountSix);

            sb.Append(" <tr><td class=\"display\"> 电气  </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text1\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A4bz\" value=" + dqA4CountOne + " />(张)  </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text2\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A4fzy\" value=" + dqA4CountTwo + " />(张) </td>");
            sb.Append(" <td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text3\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A4fze\" value=" + dqA4CountThree + " />(张) </td>");
            sb.Append("<td style=\" border-top: 0px;\"> <span>A4</span> <input id=\"Text4\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A4fzs\" value=" + dqA4CountFour + " />(张)  </td>");
            sb.Append("<td style=\"border-top: 0px;\"> <span>A4</span> <input id=\"Text5\" type=\"text\" runat=\"server\" spe=\"电气\" lenghts=\"A4lb\" value=" + dqA4CountFive + " />(张) </td>");
            sb.Append(" <td class=\"display\"></td>  </tr>");


            // 标准长       
            int totalA0CountOne = 0;
            // 1/4倍长            
            int totalA0CountTwo = 0;
            // 2/4倍长
            int totalA0CountThree = 0;
            // 3/4倍长
            int totalA0CountFour = 0;
            // A0倍长
            int totalA0CountFive = 0;
            //折合1#图
            decimal totalA0CountSix = 0;

            totalA0CountOne = jzA0CountOne + jgA0CountOne + gpsA0CountOne + ntA0CountOne + dqA0CountOne;
            totalA0CountTwo = jzA0CountTwo + jgA0CountTwo + gpsA0CountTwo + ntA0CountTwo + dqA0CountTwo;
            totalA0CountThree = jzA0CountThree + jgA0CountThree + gpsA0CountThree + ntA0CountThree + dqA0CountThree;
            totalA0CountFour = jzA0CountFour + jgA0CountFour + gpsA0CountFour + ntA0CountFour + dqA0CountFour;
            totalA0CountFive = jzA0CountFive + jgA0CountFive + gpsA0CountFive + ntA0CountFive + dqA0CountFive;
            totalA0CountSix = jzA0CountSix + jgA0CountSix + gpsA0CountSix + ntA0CountSix + dqA0CountSix;

            sb.Append(" <tr> <td rowspan=\"5\"> 合计 </td>");
            sb.Append(" <td style=\"border-bottom: 0px;\"> <span>A0</span> <input id=\"Text47\" type=\"text\" runat=\"server\" lenghts=\"TotalA0bz\" value=" + totalA0CountOne + " disabled=\"disabled\" />(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px;\">  <span>A0</span> <input id=\"Text48\" type=\"text\" runat=\"server\" lenghts=\"TotalA0fzy\" value=" + totalA0CountTwo + " disabled=\"disabled\" />(张)</td>");
            sb.Append(" <td style=\"border-bottom: 0px;\"> <span>A0</span> <input id=\"Text49\" type=\"text\" runat=\"server\" lenghts=\"TotalA0fze\" value=" + totalA0CountThree + " disabled=\"disabled\" />(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px;\">  <span>A0</span> <input id=\"Text50\" type=\"text\" runat=\"server\" lenghts=\"TotalA0fzs\" value=" + totalA0CountFour + " disabled=\"disabled\" />(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px;\"> <span>A0</span> <input id=\"Text51\" type=\"text\" runat=\"server\" lenghts=\"TotalA0lb\" value=" + totalA0CountFive + " disabled=\"disabled\" />(张)  </td>");
            sb.Append(" <td rowspan=\"5\"> <input id=\"totalZh\" type=\"text\" runat=\"server\" lenghts=\"Totalzhlj\" value=" + totalA0CountSix + " disabled=\"disabled\" />(张)  </td>");
            sb.Append("</tr>");


            // 标准长       
            int totalA1CountOne = 0;
            // 1/4倍长            
            int totalA1CountTwo = 0;
            // 2/4倍长
            int totalA1CountThree = 0;
            // 3/4倍长
            int totalA1CountFour = 0;
            // A1倍长
            int totalA1CountFive = 0;

            totalA1CountOne = jzA1CountOne + jgA1CountOne + gpsA1CountOne + ntA1CountOne + dqA1CountOne;
            totalA1CountTwo = jzA1CountTwo + jgA1CountTwo + gpsA1CountTwo + ntA1CountTwo + dqA1CountTwo;
            totalA1CountThree = jzA1CountThree + jgA1CountThree + gpsA1CountThree + ntA1CountThree + dqA1CountThree;
            totalA1CountFour = jzA1CountFour + jgA1CountFour + gpsA1CountFour + ntA1CountFour + dqA1CountFour;
            totalA1CountFive = jzA1CountFive + jgA1CountFive + gpsA1CountFive + ntA1CountFive + dqA1CountFive;

            sb.Append(" <tr><td class=\"display\"></td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A1</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA1bz\" value=" + totalA1CountOne + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A1</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA1fzy\" value=" + totalA1CountTwo + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A1</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA1fze\" value=" + totalA1CountThree + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A1</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA1fzs\" value=" + totalA1CountFour + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A1</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA1lb\" value=" + totalA1CountFive + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td class=\"display\"> </td> </tr>");

            // 标准长       
            int totalA2CountOne = 0;
            // 1/4倍长            
            int totalA2CountTwo = 0;
            // 2/4倍长
            int totalA2CountThree = 0;
            // 3/4倍长
            int totalA2CountFour = 0;
            // A2倍长
            int totalA2CountFive = 0;

            totalA2CountOne = jzA2CountOne + jgA2CountOne + gpsA2CountOne + ntA2CountOne + dqA2CountOne;
            totalA2CountTwo = jzA2CountTwo + jgA2CountTwo + gpsA2CountTwo + ntA2CountTwo + dqA2CountTwo;
            totalA2CountThree = jzA2CountThree + jgA2CountThree + gpsA2CountThree + ntA2CountThree + dqA2CountThree;
            totalA2CountFour = jzA2CountFour + jgA2CountFour + gpsA2CountFour + ntA2CountFour + dqA2CountFour;
            totalA2CountFive = jzA2CountFive + jgA2CountFive + gpsA2CountFive + ntA2CountFive + dqA2CountFive;

            sb.Append(" <tr><td class=\"display\"></td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A2</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA2bz\" value=" + totalA2CountOne + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A2</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA2fzy\" value=" + totalA2CountTwo + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A2</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA2fze\" value=" + totalA2CountThree + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A2</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA2fzs\" value=" + totalA2CountFour + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A2</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA2lb\" value=" + totalA2CountFive + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td class=\"display\"> </td> </tr>");

            // 标准长       
            int totalA3CountOne = 0;
            // 1/4倍长            
            int totalA3CountTwo = 0;
            // 2/4倍长
            int totalA3CountThree = 0;
            // 3/4倍长
            int totalA3CountFour = 0;
            // A3倍长
            int totalA3CountFive = 0;

            totalA3CountOne = jzA3CountOne + jgA3CountOne + gpsA3CountOne + ntA3CountOne + dqA3CountOne;
            totalA3CountTwo = jzA3CountTwo + jgA3CountTwo + gpsA3CountTwo + ntA3CountTwo + dqA3CountTwo;
            totalA3CountThree = jzA3CountThree + jgA3CountThree + gpsA3CountThree + ntA3CountThree + dqA3CountThree;
            totalA3CountFour = jzA3CountFour + jgA3CountFour + gpsA3CountFour + ntA3CountFour + dqA3CountFour;
            totalA3CountFive = jzA3CountFive + jgA3CountFive + gpsA3CountFive + ntA3CountFive + dqA3CountFive;

            sb.Append(" <tr><td class=\"display\"></td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A3</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA3bz\" value=" + totalA3CountOne + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A3</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA3fzy\" value=" + totalA3CountTwo + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A3</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA3fze\" value=" + totalA3CountThree + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A3</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA3fzs\" value=" + totalA3CountFour + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\"border-bottom: 0px; border-top: 0px;\">  <span>A3</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA3lb\" value=" + totalA3CountFive + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td class=\"display\"> </td> </tr>");

            // 标准长       
            int totalA4CountOne = 0;
            // 1/4倍长            
            int totalA4CountTwo = 0;
            // 2/4倍长
            int totalA4CountThree = 0;
            // 3/4倍长
            int totalA4CountFour = 0;
            // A4倍长
            int totalA4CountFive = 0;

            totalA4CountOne = jzA4CountOne + jgA4CountOne + gpsA4CountOne + ntA4CountOne + dqA4CountOne;
            totalA4CountTwo = jzA4CountTwo + jgA4CountTwo + gpsA4CountTwo + ntA4CountTwo + dqA4CountTwo;
            totalA4CountThree = jzA4CountThree + jgA4CountThree + gpsA4CountThree + ntA4CountThree + dqA4CountThree;
            totalA4CountFour = jzA4CountFour + jgA4CountFour + gpsA4CountFour + ntA4CountFour + dqA4CountFour;
            totalA4CountFive = jzA4CountFive + jgA4CountFive + gpsA4CountFive + ntA4CountFive + dqA4CountFive;
            sb.Append(" <tr><td class=\"display\"></td>");
            sb.Append(" <td style=\" border-top: 0px;\">  <span>A4</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA4bz\" value=" + totalA4CountOne + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\" border-top: 0px;\">  <span>A4</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA4fzy\" value=" + totalA4CountTwo + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\" border-top: 0px;\">  <span>A4</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA4fze\" value=" + totalA4CountThree + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\" border-top: 0px;\">  <span>A4</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA4fzs\" value=" + totalA4CountFour + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td style=\" border-top: 0px;\">  <span>A4</span> <input id=\"Text53\" type=\"text\" runat=\"server\" lenghts=\"TotalA4lb\" value=" + totalA4CountFive + " disabled=\"disabled\" />(张) </td>");
            sb.Append(" <td class=\"display\"> </td> </tr>");
            sb.Append("</table>");
            lblSubPlot.Text = sb.ToString();

        }

        private void getPlotSuInfo(DataTable dt, string sepName, string mapSheet, out int jzCountOne, out int jzCountTwo, out int jzCountThree, out int jzCountFour, out int jzCountFive, out decimal jzCountSix)
        {

            DataTable dtJZOne = new DataView(dt) { RowFilter = "  Specialty  like '%" + sepName + "%' And  Mapsheet like '%" + mapSheet + "%'" }.ToTable();

            if (dtJZOne != null && dtJZOne.Rows.Count > 0)
            {
                jzCountOne = dtJZOne.AsEnumerable().Sum(s => s.Field<int>("StandardCount"));
                jzCountTwo = dtJZOne.AsEnumerable().Sum(s => s.Field<int>("QuarterCount"));
                jzCountThree = dtJZOne.AsEnumerable().Sum(s => s.Field<int>("SecondQuarterCount"));
                jzCountFour = dtJZOne.AsEnumerable().Sum(s => s.Field<int>("ThreeQuarterCount"));
                jzCountFive = dtJZOne.AsEnumerable().Sum(s => s.Field<int>("FourQuarterCount"));
                jzCountSix = dtJZOne.AsEnumerable().Max(s => s.Field<decimal>("TotalCount"));
            }
            else
            {
                jzCountOne = 0;
                jzCountTwo = 0;
                jzCountThree = 0;
                jzCountFour = 0;
                jzCountFive = 0;
                jzCountSix = 0;
            }

        }
    }
}