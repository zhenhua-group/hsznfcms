﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace TG.Web.ProjectManage.ProImageAudit
{
    public partial class ProjectDesignCardsList : PageBase
    {

        private TG.BLL.cm_ProjectPlotInfo plot = new TG.BLL.cm_ProjectPlotInfo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定部门
                BindUnit();
                //绑定年份
                BindYear();
                //选中当年年份
                SelectCurrentYear();

                //绑定出图卡信息
                bindProjectPlotNo();
                bindProjectPlotYes();

                //绑定权限
                BindPreviewPower();
            }
        }
        //排序字段
        protected string OrderColumn
        {
            get
            {
                return this.hid_column.Value;
            }
        }
        //排序
        protected string Order
        {
            get
            {
                return this.hid_order.Value;
            }
        }

        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //检查是否需要权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }

        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        /// <summary>
        /// 选择当年年份
        /// </summary>
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND InsertUserID =" + UserSysNo + "");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
            }
        }

        /// <summary>
        /// 未填写出图卡信息
        /// </summary>
        private void bindProjectPlotNo()
        {
            StringBuilder sb = new StringBuilder("");
            //if (this.txt_keyname.Value.Trim() != "")
            //{
            //    string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
            //    sb.Append(" AND pro_Name LIKE '%" + keyname + "%'  ");
            //}
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    sb.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            //}
            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    sb.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            //}
            //检查权限
            GetPreviewPowerSql(ref sb);

            this.hid_where.Value = sb.ToString();

            //所有记录数
           // this.AspNetPager1.RecordCount = int.Parse(plot.GetProjectPlotInfoNoProcCount(sb.ToString()).ToString());
            //排序
          //  string orderString = " Order by " + OrderColumn + " " + Order;
           // sb.Append(orderString);

           // gvProjectPlotNo.DataSource = plot.P_cm_ProjectPlotInfoNoByPager(this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex, sb.ToString()); ;
           // gvProjectPlotNo.DataBind();
        }

        /// <summary>
        /// 已完成的出图卡信息
        /// </summary>
        private void bindProjectPlotYes()
        {
            TG.BLL.cm_ProjectPlotInfo plot = new TG.BLL.cm_ProjectPlotInfo();
            StringBuilder sb = new StringBuilder("");
            //if (this.txt_keyname.Value.Trim() != "")
            //{
            //    string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
            //    sb.Append(" AND pro_Name LIKE '%" + keyname + "%'  ");
            //}
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    sb.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            //}
            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    sb.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            //}
            //检查权限
            GetPreviewPowerSql(ref sb);

            this.hid_where2.Value = sb.ToString();
            //所有记录数
          //  this.AspNetPager2.RecordCount = int.Parse(plot.GetProjectPlotInfoYesProcCount(sb.ToString()).ToString());
            //排序
          //  string orderString = " Order by P.pro_ID DESC";
           // sb.Append(orderString);

          //  List<TG.Model.ProjectPlotAuditList> resultList = plot.GetProjectPlotListView(sb.ToString(), this.AspNetPager2.StartRecordIndex, this.AspNetPager2.EndRecordIndex);
           // this.gvProjectPlotYes.DataSource = TG.BLL.TransformSource.ConvertToDataSet<TG.Model.ProjectPlotAuditList>(resultList);
           // this.gvProjectPlotYes.DataBind();

        }

        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Search_Click(object sender, ImageClickEventArgs e)
        {
            //this.AspNetPager1.CurrentPageIndex = 0;
           // this.AspNetPager2.CurrentPageIndex = 0;
            bindProjectPlotNo();
            bindProjectPlotYes();
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            bindProjectPlotNo();
        }

        protected void AspNetPager2_PageChanged(object src, EventArgs e)
        {
            bindProjectPlotYes();
        }

        //生产部门改变重新绑定数据
        protected void drp_unit_SelectedIndexChanged(object sender, EventArgs e)
        {
           // this.AspNetPager1.CurrentPageIndex = 0;
            //this.AspNetPager2.CurrentPageIndex = 0;
            bindProjectPlotNo();
            bindProjectPlotYes();
        }

    }
}