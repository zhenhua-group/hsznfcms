﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TG.DBUtility;
using TG.Common;

namespace TG.Web.ProjectManage.ProImageAudit
{
    public partial class ProForeignPrint : PageBase
    {
        public string Str_Prid { get; set; }
        public int id = 0, pid = 0;
        TG.BLL.cm_ProimageSend pis = new BLL.cm_ProimageSend();
        TG.BLL.cm_ProImage pi = new BLL.cm_ProImage();
        protected void Page_Load(object sender, EventArgs e)
        {
            Str_Prid = Request.QueryString["ProjectSysNo"].ToString();
            pid = pi.GetSingle(Convert.ToInt32(Str_Prid));
            id = pis.GetSingle(Convert.ToInt32(Str_Prid));
            if (!IsPostBack)
            {
                GetProInfo(Str_Prid);
            }
        }
        #region 绑定项目信息
        //定义分类图纸张数
        int a0 = 0, a1 = 0, a2 = 0, a3 = 0, a4 = 0, a5 = 0, a6 = 0;
        public void GetProInfo(string strProId)
        {

            //显示项目信息
            SelectItemName(strProId);
            if (pid > 0)
            {
                TG.Model.cm_ProImage proimage = pi.GetModel(pid);
                if (proimage != null)
                {
                    for (int i = 0; i < 7; i++)
                    {
                        FuZhi(proimage.jz_image, i);
                        FuZhi(proimage.jz_cadimage, i);
                        FuZhi(proimage.jg_image, i);
                        FuZhi(proimage.jg_cadimage, i);
                        FuZhi(proimage.jps_image, i);
                        FuZhi(proimage.jps_cadimage, i);
                        FuZhi(proimage.nt_image, i);
                        FuZhi(proimage.nt_cadimage, i);
                        FuZhi(proimage.dq_image, i);
                        FuZhi(proimage.dq_cadimage, i);
                    }
                    this.txt_tznumber.Text = Convert.ToString(a0 + a1 + a2 + a3 + a4 + a5);
                    this.txt_ztnumber.Text = Convert.ToString(a6);
                    this.hj0.Text = Convert.ToString(a0);
                    this.hj1.Text = Convert.ToString(a1);
                    this.hj2.Text = Convert.ToString(a2);
                    this.hj2_1.Text = Convert.ToString(a3);
                    this.hj3.Text = Convert.ToString(a4);
                    this.hj4.Text = Convert.ToString(a5);
                    this.Sum_image.Text = Convert.ToString(a0 + a1 + a2 + a3 + a4 + a5 + a6);
                }
            }
            if (id > 0)
            {
                TG.Model.cm_ProimageSend proimagesend = pis.GetModel(id);
                if (proimagesend != null)
                {
                    this.txt_send.Text = Convert.ToString(proimagesend.Send_number);
                    this.txt_company.Text = proimagesend.Send_company;
                    this.price0.Text = Convert.ToString(proimagesend.Send_A0);
                    this.price1.Text = Convert.ToString(proimagesend.Send_A1);
                    this.price2.Text = Convert.ToString(proimagesend.Send_A2);
                    this.price2_1.Text = Convert.ToString(proimagesend.Send_A2_1);
                    this.price3.Text = Convert.ToString(proimagesend.Send_A3);
                    this.price4.Text = Convert.ToString(proimagesend.Send_A4);
                    this.Sum_money.Text = (proimagesend.Send_A0 + proimagesend.Send_A1 + proimagesend.Send_A2 + proimagesend.Send_A2_1 + proimagesend.Send_A3 + proimagesend.Send_A4).ToString();
                }
            }

        }

        //获取项目信息
        protected void SelectItemName(string strproID)
        {
            DataSet ds = pi.GetProjectInfo(int.Parse(strproID));
            this.txt_proname.Text = ds.Tables[0].Rows[0]["pro_name"] == null ? "" : ds.Tables[0].Rows[0]["pro_name"].ToString();
            this.txt_pronumber.Text = ds.Tables[0].Rows[0]["gch"] == null ? "" : ds.Tables[0].Rows[0]["gch"].ToString();
        }

        /// <summary>
        /// 为文本框赋值
        /// </summary>
        /// <param name="strs"></param>
        /// <param name="type"></param>
        public void FuZhi(string strs, int type)
        {
            string[] vals = strs.Split('|');
            if (type == 0)
            {
                a0 = a0 + Convert.ToInt32(vals[type].ToString());
            }
            if (type == 1)
            {
                a1 = a1 + Convert.ToInt32(vals[type].ToString());
            }
            if (type == 2)
            {
                a2 = a2 + Convert.ToInt32(vals[type].ToString());
            }
            if (type == 3)
            {
                a3 = a3 + Convert.ToInt32(vals[type].ToString());
            }
            if (type == 4)
            {
                a4 = a4 + Convert.ToInt32(vals[type].ToString());
            }
            if (type == 5)
            {
                a5 = a5 + Convert.ToInt32(vals[type].ToString());
            }
            if (type == 6)
            {
                a6 = a6 + Convert.ToInt32(vals[type].ToString());
            }
        }

        #endregion
    }
}