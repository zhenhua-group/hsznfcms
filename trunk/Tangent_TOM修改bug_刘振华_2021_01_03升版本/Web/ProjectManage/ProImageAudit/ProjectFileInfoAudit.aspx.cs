﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;
using System.Text;
using System.Data;

namespace TG.Web.ProjectManage.ProImageAudit
{
    public partial class ProjectFileInfoAudit : PageBase
    {
        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        #region 传入参数
        //审批消息ID
        public int MessageID
        {
            get
            {
                int msgSysNo = 0;
                int.TryParse(Request["MsgNo"], out msgSysNo);
                return msgSysNo;
            }
        }
        /// <summary>
        /// 项目审核SysNo
        /// </summary>
        public int ProjectFileAuditSysNo
        {
            get
            {
                int projectPlanAuditSysNo = 0;
                int.TryParse(Request["ProjectFileAuditSysNo"], out projectPlanAuditSysNo);
                return projectPlanAuditSysNo;
            }
        }
        protected override bool IsAuth
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// 消息状态
        /// </summary>
        public string MessageStatus
        {
            get
            {
                return Request["MessageStatus"];
            }
        }
        #endregion

        #region 得到信息
        /// <summary>
        /// 项目ID
        /// </summary>
        public int ProSysNo { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public string AuditStatus { get; set; }


        /// <summary>
        /// 审核信息
        /// </summary>
        public string AuditHTML { get; set; }


        #endregion

        private TG.BLL.cm_ProjectFileInfo bll = new TG.BLL.cm_ProjectFileInfo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetProjectPlanAudit();
            }
        }

        /// <summary>
        /// 绑定项目信息
        /// </summary>
        private void bindProjectInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(ProSysNo);
            if (pro_model != null)
            {
                txt_proname.Text = pro_model.pro_name;
                txt_pronumber.Text = pro_model.Pro_number;
                txt_prostatus.Text = pro_model.pro_status;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }
                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }
                lbl_PMName.Text = pro_model.PMName;
                txt_prounit.Text = pro_model.Unit;
            }
        }


        /// <summary>
        /// 获取审批信息
        /// </summary>
        private void GetProjectPlanAudit()
        {

            TG.Model.cm_ProjectFileInfoAudit projectFileAudit = bll.GetProjectFileInfoAuditEntity(new cm_ProjectFileAuditQueryEntity { ProjectPlotAuditSysNo = ProjectFileAuditSysNo });

            //获取审批时的项目ID
            if (projectFileAudit != null)
            {
                ProSysNo = projectFileAudit.ProjectSysNo;
                AuditStatus = projectFileAudit.Status;
            }
            else
            {
                ProSysNo = 0;
            }

            bindProjectInfo();

            bindFileInfo();

            if (projectFileAudit != null)
            {
                GetProjectPlanAuditEntity(projectFileAudit);
            }
        }

        /// <summary>
        /// 取得信息
        /// </summary>
        private void bindFileInfo()
        {
            TG.BLL.cm_ProjectFileInfo bll = new TG.BLL.cm_ProjectFileInfo();
            DataTable dt = bll.GetFileInfo(ProSysNo).Tables[0];

            StringBuilder sb = new StringBuilder();

            sb.Append("<table class=\"show_projectPlot\" style=\"width: 100%;\" id=\"tbFile\">");

            if (dt != null && dt.Rows.Count > 0)
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("<tr>");
                    sb.Append(" <td style=\"width: 10%;\">" + dt.Rows[i]["num"] + "</td>");
                    sb.Append("<td style=\"width: 25%;\" align=\"left\">" + dt.Rows[i]["name"] + "</td>");

                    sb.Append(" <td style=\"width: 15%\">" + dt.Rows[i]["PageCount"] + "  </td>");
                    string isExist = dt.Rows[i]["IsExist"].ToString();
                    if (isExist == "0")
                    {
                        sb.Append("<td style=\"width: 15%;\"><input id=\"Radio29\" type=\"radio\" class=\"yes\" name=" + i + " value=\"1\" />有<input id=\"Radio30\" type=\"radio\" class=\"no\" name=" + i + " value=\"0\" checked=\"checked\" />无 </td>");
                    }
                    else
                    {
                        sb.Append("<td style=\"width: 15%;\"><input id=\"Radio29\" type=\"radio\" class=\"yes\" name=" + i + " value=\"1\" checked=\"checked\"/>有<input id=\"Radio30\" type=\"radio\" class=\"no\" name=" + i + " value=\"0\"  />无 </td>");
                    }
                    sb.Append("<td style=\"width: 35%\" align=\"left\"> " + dt.Rows[i]["Remark"] + " </td>");
                    sb.Append("</tr>");
                }

            }
            sb.Append("</table>");
            lblFileInfo.Text = sb.ToString();
        }

        /// <summary>
        /// 查询审核实体的方法
        /// </summary>
        public void GetProjectPlanAuditEntity(cm_ProjectFileInfoAudit projectFileAudit)
        {

            //查询项目审核策划的流程信息
            List<string> processDescription = bll.GetAuditProcessDescription();
            //审批用户
            string[] auditUserArray = projectFileAudit.AuditUserArray;
            //审批时间
            string[] auditUserDate = projectFileAudit.AuditDateArray;

            string html = "";
            html = "<fieldset style=\"width:98%;font-size:12px;\"><legend>审批意见</legend><table style=\"width: 100%; font-size: 12px\">";


            if (projectFileAudit != null && projectFileAudit.SuggestionArray != null)
            {
                TG.BLL.tg_member uBp = new BLL.tg_member();
                int i = 0;
                //查询流程信息
                List<string> temp = processDescription;

                if ((AuditStatus != MessageStatus) && (MessageStatus == "A" || MessageStatus == "C"))
                {
                    TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[0]));
                    string usreName = user == null ? "" : user.mem_Name;
                    string auditDate = auditUserDate[0];
                    html += JoinAuditHTML(projectFileAudit.SuggestionArray[0], usreName, temp[0], auditDate);
                }
                else if ((AuditStatus != MessageStatus) && (MessageStatus == "B" || MessageStatus == "E"))
                {
                    TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[1]));
                    string usreName = user == null ? "" : user.mem_Name;
                    string auditDate = auditUserDate[1].ToString();
                    html += JoinAuditHTML(projectFileAudit.SuggestionArray[1], usreName, temp[1], auditDate);
                }
                else
                {
                    int length = projectFileAudit.SuggestionArray.Length;
                    foreach (string suggestion in projectFileAudit.SuggestionArray)
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[i]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[i].ToString();
                        html += JoinAuditHTML(suggestion, usreName, processDescription[i], auditDate);
                        i++;
                    }

                    if (AuditStatus == "D")
                    {
                        html += "<tr><td align=\"center\" colspan=\"2\"><input type=\"button\" id=\"Button1\" style=\"background-color: #E8E8E8\" name=\"controlBtn\" class=\"cls_btn_comm_w\" value=\"打印\" onclick=\"window.open('PrintFileInfo.aspx?ProjectSysNo=" + ProSysNo + "','打印','width=800,height=500,top=50,left=150,scrollbars=yes,resizable=yes,location=no');\" />&nbsp;<input type=\"button\" id=\"Button1\" style=\"background-color: #E8E8E8\" name=\"controlBtn\" class=\"cls_btn_comm_w\" value=\"返回\" onclick=\"javascript:history.back();\" /></td></tr>";
                    }
                }

            }
            if (AuditStatus == MessageStatus)
            {
                if (AuditStatus != "C" && AuditStatus != "E" && AuditStatus != "D")
                {
                    html += JoinAuditHTML("", "", "", "");
                }
            }


            html += "  </table></fieldset>";
            AuditHTML = html;
        }

        public bool CheckPower()
        {
            int roleSysNo = new TG.BLL.cm_ProjectPlanBP().GetProcessRoleSysNo(AuditStatus);

            bool hasPower = new TG.BLL.cm_Role().CheckPower(roleSysNo, UserSysNo);

            return hasPower;
        }

        private string JoinAuditHTML(string suggestion, string userName, string roleName, string auditDate)
        {
            string disableFlag = "";
            string idFlag = "";
            if (!string.IsNullOrEmpty(suggestion))
            {
                disableFlag = "disabled=disabled";
            }
            else
            {
                idFlag = "suggestionTextArea";
            }

            string html = "";
            html += "<tr>";
            html += "<td style=\"width: 20%\" align=\"center\">";
            string sugesstionString = "意见";
            if (!string.IsNullOrEmpty(roleName) && !string.IsNullOrEmpty(userName))
            {
                sugesstionString = string.Format("{0} : {1}的意见", roleName, userName);
            }
            html += sugesstionString;
            html += "</td>";

            html += "<td style=\"width: 70%\">";
            html += "&nbsp;";
            html += " <textarea style=\"height:60px;width:98%;border:solid 1px gray;\" id=\"" + idFlag + "\" " + disableFlag + ">" + suggestion + "</textarea>";
            html += "</td>";

            html += "<td style=\"width:15%;\">";
            html += auditDate;
            html += "</td>";
            html += "</tr>";
            //审批
            if (string.IsNullOrEmpty(suggestion))
            {
                html += "<tr><td align=\"center\" colspan=\"2\">";
                html += "<input type=\"button\" id=\"AgreeButton\" style=\"background-color: #E8E8E8\" value=\"通过\" class=\"cls_btn_comm_w\"/>&nbsp;";
                html += "<input type=\"button\" id=\"DisAgreeButton\" style=\"background-color: #E8E8E8\" value=\"不通过\" class=\"cls_btn_comm_w\"/>&nbsp;";
                html += "<input type=\"button\" id=\"Button1\" style=\"background-color: #E8E8E8\" name=\"controlBtn\" class=\"cls_btn_comm_w\" value=\"返回\" onclick=\"javascript:history.back();\" />";
                html += " </td></tr>";
            }
            return html;
        }

    }
}