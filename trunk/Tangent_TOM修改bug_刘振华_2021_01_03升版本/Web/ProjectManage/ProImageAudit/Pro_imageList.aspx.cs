﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;


namespace TG.Web.ProjectManage.ProImageAudit
{
    public partial class Pro_imageList : PageBase
    {

        TG.BLL.cm_Project project = new TG.BLL.cm_Project();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定项目列表
                BindProject();
                //绑定部门
                BindUnit();
            }
        }

        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            //不显示的单位
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                strWhere += " 1=1 AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            else
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }


        //根据条件查询
        public void BindProject()
        {
            StringBuilder sb = new StringBuilder("");
            //合同名称
            if (this.txt_keyname.Value != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.AppendFormat(" AND pro_Name LIKE '%{0}%'", keyname);
            }
            //按照部门
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.AppendFormat(" AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID={0})", this.drp_unit.SelectedValue);
            }
            //检查权限
            GetPreviewPowerSql(ref sb);
            //数量
            this.AspNetPager1.RecordCount = int.Parse(project.GetListPageProcCount(sb.ToString()).ToString());
            //排序
            string orderString = " order by pro_ID DESC ";
            sb.Append(orderString);

            //分页
            gv_project.DataSource = project.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            gv_project.DataBind();
        }

        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Search_Click(object sender, ImageClickEventArgs e)
        {
            //查询项目信息
            BindProject();
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindProject();
        }
    }
}