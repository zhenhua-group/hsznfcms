﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModifyProjectDesignCards.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.ModifyProjectDesignCards" %>

<%@ Register Src="/UserControl/ChooseUser.ascx" TagName="ChooseUser" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>修改工程设计出图卡</title>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="/css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery.alerts.css" rel="Stylesheet" type="text/css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
    <link href="/css/swfupload/default_cpr.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
    <script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.alerts.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.chromatable.js"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script src="/js/UserControl/ChooseUser.js" type="text/javascript"></script>
    <script src="../../js/ProjectMamage/ProImageAudit/AddProjectDesignCards.js" type="text/javascript"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                当前位置：[项目信息-修改工程设计出图卡]
            </td>
        </tr>
        <tr>
            <td class="cls_content" align="center" valign="top">
                <table class="cls_content_2">
                    <tr>
                        <td class="cls_head_2">
                            <div class="cls_1">
                                项目信息
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben" id="projectInfo">
        <tr>
            <td style="width: 15%">
                工程名称：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_proname" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                工程号:
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_pronumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                子项名称：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_proitemname" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                设计阶段：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_prostatus" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                管理级别：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_level" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                设计部门：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_prounit" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                审核级别：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                设&nbsp;&nbsp;总：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_PMName" runat="server" Width="150px"></asp:Label>
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    工程设计出图卡
                </div>
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben">
        <tr>
            <td style="width: 15%">
                出图类别：
            </td>
            <td style="width: 35%">
                <asp:RadioButtonList ID="rbtlist" runat="server" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
            <td style="width: 15%">
                晒图份数：
            </td>
            <td style="width: 35%">
                <input id="txt_BluePrintCounts" type="text" name="number" class="cls_input_text_valid_w"
                    runat="Server" style="width: 120px;" />(份)
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                出图人：
            </td>
            <td style="width: 35%">
                <input id="txt_PMName" type="text" name="number" disabled="disabled" runat="Server"
                    class="cls_input_text_valid_w" style="width: 120px;" />
                <asp:HiddenField ID="hid_pmname" runat="server" Value="0" />
                <asp:HiddenField ID="hid_pmuserid" runat="server" Value="0" />
                <span style="color: blue; cursor: pointer" id="sch_PM">查询</span>
            </td>
            <td style="width: 15%">
            </td>
            <td style="width: 35%">
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                详细信息：
            </td>
            <td colspan="3">
                <table class="cls_content_head" style="width: 100%;">
                    <tr id="Tr1">
                        <td style="width: 10%;" align="center">
                            专业
                        </td>
                        <td style="width: 15%;" align="center">
                            标准长
                        </td>
                        <td style="width: 15%" align="center">
                            加1/4倍长
                        </td>
                        <td style="width: 15%;" align="center">
                            加2/4倍长
                        </td>
                        <td style="width: 15%" align="center">
                            加3/4倍长
                        </td>
                        <td style="width: 15%;" align="center">
                            加1倍长
                        </td>
                        <td style="width: 15%;" align="center">
                            折合1#图
                        </td>
                    </tr>
                </table>
                <asp:Literal ID="lblSubPlot" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td style="height: 23px; background-color: #f0f0f0" colspan="4" align="center">
                <a href="###" id="btn_save">
                    <img src="/Images/buttons/btn_save2.gif" style="width: 64px; height: 23px; border: none;" /></a>
                &nbsp;<a href="ProjectDesignCardsList.aspx" target="_self"><img src="/Images/buttons/btn_back2.gif"
                    style="width: 65px; height: 23px; border: none;" /></a>
            </td>
        </tr>
    </table>
    <!--添加详细信息-->
    <div id="addsubPlotInfo" style="display: none;">
        <table class="cls_show_cst_jiben" style="width: 400px;" align="center" id="addsubDataTable">
            <tr>
                <td style="text-align: right;">
                    专业:
                </td>
                <td>
                    <select style="width: 120px;" id="drop_Specialty" class="cls_input_text_valid">
                        <option value="-1">---请选择---</option>
                        <option value="建筑">建筑</option>
                        <option value="结构">结构</option>
                        <option value="给排水">给排水</option>
                        <option value="暖通">暖通</option>
                        <option value="电气">电气</option>
                    </select>
                    <span id="spanSpecialty" class="valide">请选择专业!</span>
                </td>
            </tr>
            <tr>
                <td style="width: 20%; text-align: right;">
                    图幅:
                </td>
                <td>
                    <asp:DropDownList ID="drop_Mapsheet" runat="Server" Width="120px" AppendDataBoundItems="True"
                        CssClass="cls_input_text_valid">
                        <asp:ListItem Value="-1">---请选择---</asp:ListItem>
                    </asp:DropDownList>
                    <span id="spanSubMapsheet" class="valide">请选择图幅!</span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    加长倍数:
                </td>
                <td>
                    <asp:DropDownList ID="drop_LengthenType" runat="Server" Width="120px" AppendDataBoundItems="True"
                        CssClass="cls_input_text_valid">
                        <asp:ListItem Value="-1">---请选择---</asp:ListItem>
                    </asp:DropDownList>
                    <span id="spanLengthenType" class="valide">请选择加长倍数!</span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">
                    数量:
                </td>
                <td>
                    <input id="txt_Count" type="text" name="number" class="cls_input_text_valid_w" style="width: 120px;" />
                    <span id="countsNull" class="valide">请填写出图卡数量!</span> <span id="countsNoInt" class="valide">
                        请填写整数!</span>
                </td>
            </tr>
        </table>
    </div>
    <div id="chooseUserMain" style="display: none;">
        <uc1:ChooseUser ID="ChooseUser1" runat="server" />
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <input id="HidProID" type="hidden" value="<%=ProjectSysNo %>" />
    </form>
</body>
</html>
