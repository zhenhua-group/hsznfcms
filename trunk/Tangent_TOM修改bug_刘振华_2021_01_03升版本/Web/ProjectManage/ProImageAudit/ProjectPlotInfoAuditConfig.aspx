﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectPlotInfoAuditConfig.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.ProjectPlotInfoAuditConfig" %>

<%@ Register Src="../../UserControl/AuditConfigBase.ascx" TagName="AuditConfigBase"
    TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>项目出图卡申请流程配置</title>
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="/js/UserControl/AuditConfigCommon.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <div>
        <uc2:AuditConfigBase ID="AuditConfigBase1" runat="server" Title="工程设计出图申请" TableName="cm_ProjectPlotInfoConfig" />
    </div>
    </form>
    <script type="text/javascript">
        $(function () {
            CommonControl.SetFormWidth();
            var auditConfigCommon = new AuditConfigCommon($("#form1"));
        });
    </script>
</body>
</html>
