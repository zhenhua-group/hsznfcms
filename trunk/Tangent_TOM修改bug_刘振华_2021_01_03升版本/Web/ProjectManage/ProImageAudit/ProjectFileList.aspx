﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectFileList.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.ProjectFileList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <script src="/js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery.tablesort.css" rel="stylesheet" type="text/css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="/css/ProjectPlan.css" rel="stylesheet" type="text/css" />
    <link href="/css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="/css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" type="text/css" />
    <link href="/css/tipsy/tipsy.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="../../js/Global.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/SendMessageCommon.js"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.tablesort.js"></script>
    <script type="text/javascript" src="/js/jQuery-Plugs.js"></script>
    <script type="text/javascript" src="/js/jquery.tipsy.js"></script>
    <script src="../../js/ProjectMamage/ProImageAudit/ProjectFileAuditList.js" type="text/javascript"></script>

     <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/ProjectMamage/ProImageAudit/ProjectFileAuditList_jq.js"></script>
    <!--[if IE 9]>
    <link rel="stylesheet" type="text/css" href="/css/cssie9.css" />
    <![endif]-->
    <style type="text/css">
        .ui-pg-div
        {
            display: none;
        }
    </style>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            AllotList();
            $("#wfp").click(function () {
                AllotList();
            });
            $("#fpwb").click(function () {
                AllotList2();
            });
        });
    </script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="style2">
                当前位置：[工程设计归档资料申请列表]
            </td>
        </tr>
        <tr>
            <td class="cls_head_bar">
                <table class="cls_head_div">
                    <tr>
                        <td>
                            生产部门：
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_unit" runat="server" Width="120px" AppendDataBoundItems="true">
                                <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            年份：<asp:DropDownList ID="drp_year" runat="server" AppendDataBoundItems="true" Width="95px">
                                <asp:ListItem Value="-1">--选择年份--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            项目名称：<input id="txt_keyname" name="txt_keyname" type="text" runat="Server" />
                        </td>
                        <td>
                         <input type="button" id="btn_Search" style="background:url(/Images/buttons/btn_search.gif) no-repeat; height:22px; width:64px; border:0; cursor:pointer;" value=" " />
                            <%--<asp:ImageButton ID="btn_Search" runat="server" ImageUrl="~/Images/buttons/btn_search.gif"
                                Width="64px" Height="22px" OnClick="btn_Search_Click" />--%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="tabs">
        <ul>
            <li>
                <div class="cls_1">
                    <a href="#tabs-1" style="color: Black; font-size: 12px; font-weight: normal;" id="wfp">归档资料填写</a>
                </div>
            </li>
            <li>
                <div class="cls_1">
                    <a href="#tabs-2" style="color: Black; font-size: 12px; font-weight: normal;" id="fpwb">归档资料申请</a>
                </div>
            </li>
        </ul>
        <div id="tabs-1">
            <%--<div class="cls_data">
                <table class="cls_content_head" id="sorttable">
                    <tr>
                        <td style="width: 4%;" align="center">
                            序号
                        </td>
                        <td style="width: 35%;" align="center" sortc="P.pro_Name">
                            项目名称
                        </td>
                        <td style="width: 8%" align="center" sortc="P.Pro_number">
                            工程号
                        </td>
                        <td style="width: 8%" align="center" sortc="P.Unit">
                            承接部门
                        </td>
                        <td style="width: 8%;" align="center">
                            执行设总
                        </td>
                        <td style="width: 8%;" align="center">
                            管理级别
                        </td>
                        <td style="width: 9%" align="center">
                            审核级别
                        </td>
                        <td style="width: 12%" align="center">
                            备案号
                        </td>
                        <td style="width: 8%" align="center">
                            操作
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="gvProjectPlotNo" runat="server" CellPadding="0" AutoGenerateColumns="False"
                    ShowHeader="False" CssClass="gridView_comm" RowStyle-HorizontalAlign="Center"
                    Font-Size="12px" RowStyle-Height="22px" Width="100%">
                    <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# (Container.DataItemIndex+1).ToString() %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="4%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="项目名称">
                            <ItemTemplate>
                                <a href="/ProjectManage/ShowProject.aspx?flag=list&pro_id=<%#Eval("pro_ID") %>" rel='<%# Eval("pro_ID") %>'
                                    class="chk">
                                    <div style="width: 31em;" class="cls_column" title='<%# Eval("pro_name").ToString()%>'>
                                        <img src="/Images/proj.png" style="width: 16px; height: 16px; margin-left: 1px; border: none;" />
                                        <%# Eval("pro_name").ToString()%></div>
                                </a>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="35%" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Pro_number">
                            <ItemStyle HorizontalAlign="Center" Width="8%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Unit">
                            <ItemStyle HorizontalAlign="Center" Width="8%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PMName">
                            <ItemStyle HorizontalAlign="Center" Width="8%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="pro_level">
                            <ItemStyle HorizontalAlign="Center" Width="8%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="AuditLevel">
                            <ItemStyle HorizontalAlign="Center" Width="9%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FileNum">
                            <ItemStyle HorizontalAlign="Center" Width="12%" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="添加出图卡">
                            <ItemTemplate>
                                <a href='AddProjectFileInfo.aspx?project_Id=<%# Eval("pro_ID") %>' class='allowEdit'
                                    target="_self">归档申请</a>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="8%" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <div style="padding-top: 3px; height: 23px; width: 100%;">
                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                        CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                        CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                        OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                        PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                        SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                        PageSize="25" SubmitButtonClass="submitbtn">
                    </webdiyer:AspNetPager>
                </div>
            </div>--%>
             <table id="jqGrid">
            </table>
            <div id="gridpager">
            </div>
            <div id="nodata" class="norecords"></div>
        </div>
        <div id="tabs-2">
            <%--<div class="cls_data">
                <table class="cls_content_head" id="Table1">
                    <tr>
                        <td style="width: 4%;" align="center">
                            序号
                        </td>
                        <td style="width: 35%;" align="center" sortc="P.pro_Name">
                            项目名称
                        </td>
                        <td style="width: 8%" align="center" sortc="P.Pro_number">
                            工程号
                        </td>
                        <td style="width: 8%" align="center" sortc="P.Unit">
                            承接部门
                        </td>
                        <td style="width: 6%;" align="center">
                            执行设总
                        </td>
                        <td style="width: 6%;" align="center">
                            管理级别
                        </td>
                        <td style="width: 10%" align="center">
                            审核级别
                        </td>
                        <td style="width: 13%" align="center">
                            审批进度
                        </td>
                        <td style="width: 10%" align="center">
                            操作
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="gvProjectPlotYes" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                    CssClass="gridView_comm" Width="100%" Font-Size="12px">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# (Container.DataItemIndex+1).ToString() %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="4%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="项目名称">
                            <ItemTemplate>
                                <a href="/ProjectManage/ShowProject.aspx?flag=list&pro_id=<%#Eval("pro_ID") %>" rel='<%# Eval("pro_ID") %>'
                                    class="chk">
                                    <div style="width: 31em;" class="cls_column" title='<%# Eval("Pro_name").ToString()%>'>
                                        <img src="/Images/proj.png" style="width: 16px; height: 16px; margin-left: 1px; border: none;" />
                                        <%# Eval("Pro_name").ToString()%></div>
                                </a>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="35%" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Pro_number">
                            <ItemStyle HorizontalAlign="Center" Width="8%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Unit">
                            <ItemStyle HorizontalAlign="Center" Width="8%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PMName">
                            <ItemStyle HorizontalAlign="Center" Width="6%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Pro_level">
                            <ItemStyle HorizontalAlign="Center" Width="6%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="AuditLevel">
                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="审核状态">
                            <ItemTemplate>
                                <div class="progressbar" percent="<%#Eval("Percent") %> " id="auditLocusContainer"
                                    title='<%#Eval("Percent") %>%' action="File" referencesysno="<%#Eval("AuditRecordSysNo") %>"
                                    style="cursor: pointer; margin: 1px 1px 1px 1px;">
                                </div>
                            </ItemTemplate>
                            <ItemStyle Width="13%" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <%-- <a href='ModifyProjectFileInfo.aspx?project_Id=<%# Eval("pro_ID") %>' class='allowEdit'
                                    target="_self">编辑</a>|
                                <%#Eval("ActionLink") %>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <div style="padding-top: 3px; height: 23px; width: 100%;">
                    <webdiyer:AspNetPager ID="AspNetPager2" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                        CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                        CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                        OnPageChanged="AspNetPager2_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                        PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                        SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                        PageSize="25" SubmitButtonClass="submitbtn">
                    </webdiyer:AspNetPager>
                </div>
            </div>--%>
             <table id="jqGrid2">
            </table>
            <div id="gridpager2">
            </div>
            <div id="nodata2" class="norecords"></div>
           
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <!-- 排序指定 -->
    <asp:HiddenField ID="hid_column" runat="server" Value="P.pro_ID" />
    <asp:HiddenField ID="hid_order" runat="server" Value="DESC" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="hid_where2" runat="server" Value="" />
    <!--HiddenArea-->
    <input type="hidden" id="HiddenUserSysNo" value="<%=UserSysNo %>" />
    <!--选择消息接收着-->
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
    </form>
</body>
</html>
