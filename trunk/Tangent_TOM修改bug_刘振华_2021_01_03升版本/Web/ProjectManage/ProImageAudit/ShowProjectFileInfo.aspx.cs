﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace TG.Web.ProjectManage.ProImageAudit
{
    public partial class ShowProjectFileInfo : System.Web.UI.Page
    {
        /// <summary>
        /// 项目SysNo
        /// </summary>
        public int ProjectSysNo
        {
            get
            {
                int projectSysNo = 0;
                int.TryParse(Request["project_Id"], out projectSysNo);
                return projectSysNo;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindProjectInfo();
                bindFileInfo();
            }
        }

        /// <summary>
        /// 绑定项目信息
        /// </summary>
        private void bindProjectInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(ProjectSysNo);
            if (pro_model != null)
            {
                txt_proname.Text = pro_model.pro_name;
                txt_pronumber.Text = pro_model.Pro_number;
                txt_prostatus.Text = pro_model.pro_status;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }
                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }
                lbl_PMName.Text = pro_model.PMName;
                txt_prounit.Text = pro_model.Unit;
            }
        }

        /// <summary>
        /// 取得信息
        /// </summary>
        private void bindFileInfo()
        {
            TG.BLL.cm_ProjectFileInfo bll = new TG.BLL.cm_ProjectFileInfo();
            DataTable dt = bll.GetFileInfo(ProjectSysNo).Tables[0];

            StringBuilder sb = new StringBuilder();

            sb.Append("<table class=\"show_projectPlot\" style=\"width: 100%;\" id=\"tbFile\">");

            if (dt != null && dt.Rows.Count > 0)
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("<tr>");
                    sb.Append(" <td style=\"width: 10%;\">" + dt.Rows[i]["num"] + "</td>");
                    sb.Append("<td style=\"width: 25%;\" align=\"left\">" + dt.Rows[i]["name"] + "</td>");

                    sb.Append(" <td style=\"width: 15%\">" + dt.Rows[i]["PageCount"] + "  </td>");
                    string isExist = dt.Rows[i]["IsExist"].ToString();
                    if (isExist == "0")
                    {
                        sb.Append("<td style=\"width: 15%;\"><input id=\"Radio29\" type=\"radio\" class=\"yes\" name=" + i + " value=\"1\" />有<input id=\"Radio30\" type=\"radio\" class=\"no\" name=" + i + " value=\"0\" checked=\"checked\" />无 </td>");

                    }
                    else
                    {
                        sb.Append("<td style=\"width: 15%;\"><input id=\"Radio29\" type=\"radio\" class=\"yes\" name=" + i + " value=\"1\" checked=\"checked\"/>有<input id=\"Radio30\" type=\"radio\" class=\"no\" name=" + i + " value=\"0\"  />无 </td>");
                    }
                    sb.Append("<td style=\"width: 35%\" align=\"left\"> " + dt.Rows[i]["Remark"] + " </td>");
                    sb.Append("</tr>");
                }

            }
            sb.Append("</table>");
            lblFileInfo.Text = sb.ToString();
        }
    }
}