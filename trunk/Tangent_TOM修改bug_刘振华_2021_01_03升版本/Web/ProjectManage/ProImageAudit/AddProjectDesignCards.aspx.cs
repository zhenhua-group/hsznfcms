﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace TG.Web.ProjectManage.ProImageAudit
{
    public partial class AddProjectDesignCards : PageBase
    {
        /// <summary>
        /// 项目SysNo
        /// </summary>
        public int ProjectSysNo
        {
            get
            {
                int projectSysNo = 0;
                int.TryParse(Request["project_Id"], out projectSysNo);
                return projectSysNo;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //出图类型
                BindPlotTypeList();

                //图幅
                BindMapsheetList();

                //加长倍数
                BindLengthenTypeList();

                //绑定项目信息
                bindProjectInfo();

                //获得并绑定权限
                BindPreviewPower();
            }
        }

        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;

                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }

        /// <summary>
        /// 绑定项目信息
        /// </summary>
        private void bindProjectInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(ProjectSysNo);
            if (pro_model != null)
            {
                txt_proname.Text = pro_model.pro_name;
                txt_pronumber.Text = pro_model.Pro_number;
                txt_proitemname.Text = pro_model.pro_name;
                txt_prostatus.Text = pro_model.pro_status;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }
                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }
                lbl_PMName.Text = pro_model.PMName;
                txt_prounit.Text = pro_model.Unit;
            }
        }
        /// <summary>
        /// 出图类型
        /// </summary>
        public void BindPlotTypeList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定出图类型
            DataSet dic_proly = dic.GetList(" dic_type='plot_type'");
            rbtlist.DataSource = dic_proly;
            rbtlist.DataTextField = "dic_name";
            rbtlist.DataValueField = "id";
            rbtlist.DataBind();
        }

        /// <summary>
        /// 绑定图幅
        /// </summary>
        private void BindMapsheetList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定项目来源
            DataSet dic_proly = dic.GetList(" dic_type='map_sheet'");
            drop_Mapsheet.DataValueField = "id";
            drop_Mapsheet.DataTextField = "dic_name";
            drop_Mapsheet.DataSource = dic_proly;
            drop_Mapsheet.DataBind();
        }
        /// <summary>
        /// 绑定加长倍数
        /// </summary>
        private void BindLengthenTypeList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定项目来源
            DataSet dic_proly = dic.GetList(" dic_type='double_type'");
            drop_LengthenType.DataValueField = "id";
            drop_LengthenType.DataTextField = "dic_name";
            drop_LengthenType.DataSource = dic_proly;
            drop_LengthenType.DataBind();
        }
    }
}