﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;
using System.Data;
using System.Text;
using TG.BLL;
using TG.Common;


namespace TG.Web.ProjectManage.ProImageAudit
{
    public partial class ApplyProImage : PageBase
    {

        TG.BLL.cm_ProImaAudit bll = new TG.BLL.cm_ProImaAudit();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Project();
            }
        }

        //获取用户权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }

        //绑定项目列表
        public void Bind_Project()
        {
            StringBuilder sb = new StringBuilder();
            //权限
            GetPreviewPowerSql(ref sb);
            ////数量
            TG.BLL.cm_Project project = new TG.BLL.cm_Project();
           
            this.AspNetPager1.RecordCount = int.Parse(project.GetListPageProcCount(sb.ToString()).ToString());
            //分页
            List<TG.Model.cm_ProImaAuditListView> resultList =bll.GetProjectImgAuditView(sb.ToString(),this.AspNetPager1.StartRecordIndex,this.AspNetPager1.EndRecordIndex);
            this.gv_project.DataSource = TG.BLL.TransformSource.ConvertToDataSet<TG.Model.cm_ProImaAuditListView>(resultList);
            this.gv_project.DataBind();
        }
        //得到项目工程
        public string getNumber(object proid)
        {
            return new BLL.cm_projectNumber().GetProjectJobNumberByProjectSysNo(int.Parse(proid.ToString()));
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            Bind_Project();
        }
    }
}