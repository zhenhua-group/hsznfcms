﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using Newtonsoft.Json;
using TG.Model;
using TG.BLL;
using TG.DBUtility;
using System.Configuration;
using System.Data;
using System.Linq;

namespace TG.Web.ProjectManage.ProImageAudit
{
    public partial class ProImageAudit : PageBase
    {
        #region QueryString
        /// <summary>
        /// 项目SysNo
        /// </summary>
        public int ProjectSysNo { get; set; }

        public string AuditStatus { get; set; }
        public string ct = "";

        /// <summary>
        /// 项目审核SysNo
        /// </summary>
        public int ProjectImaAuditSysNo
        {
            get
            {
                int projectAuditSysNo = 0;
                int.TryParse(Request["projectImaAuditSysNo"], out projectAuditSysNo);
                return projectAuditSysNo;
            }
        }
        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }
        /// <summary>
        /// 消息审批状态
        /// </summary>
        public string MessageStatus
        {
            get
            {
                return Request["MessageStatus"];
            }

        }
        #endregion

        #region Property

        /// <summary>
        /// 审核记录实体
        /// </summary>
        public TG.Model.cm_ProImaAudit proImaAuditRecordEntity { get; set; }
        TG.BLL.cm_ProImaAudit bll = new BLL.cm_ProImaAudit();
        BLL.cm_ProImage bllImage = new BLL.cm_ProImage();
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProImageAudit));

            if (!IsPostBack)
            {
                ProjectSysNo = bll.GetModel(ProjectImaAuditSysNo).ProjectSysNo;
                GetProImaAuditRecord();
                GetProInfo(ProjectSysNo);
            }
        }

        /// <summary>
        /// 查询消息审核记录
        /// </summary>
        private void GetProImaAuditRecord()
        {
            TG.BLL.cm_ProImaAudit proImaBLL = new TG.BLL.cm_ProImaAudit();
            proImaAuditRecordEntity = proImaBLL.GetModel(ProjectImaAuditSysNo);
            AuditStatus = proImaAuditRecordEntity == null ? "" : proImaAuditRecordEntity.Status;
            if (!string.IsNullOrEmpty(proImaAuditRecordEntity.AuditUser))
            {
                string[] tempArr = proImaAuditRecordEntity.AuditUser.Split(',');
                if (tempArr != null && tempArr.Length > 0)
                {
                    foreach (var item in tempArr)
                    {
                        //查询联系人姓名
                        TG.Model.tg_member user = new TG.BLL.tg_member().GetModel(int.Parse(item));
                        proImaAuditRecordEntity.AuditUserString = string.IsNullOrEmpty(proImaAuditRecordEntity.AuditUser) ? user.mem_Name : proImaAuditRecordEntity.AuditUserString + user.mem_Name + ",";
                    }
                    proImaAuditRecordEntity.AuditUserString = proImaAuditRecordEntity.AuditUserString.Substring(0, proImaAuditRecordEntity.AuditUserString.Length - 1);
                }
            }
        }

        [AjaxMethod]
        public string CheckPower(string auditStatus, int userSysNo)
        {
            //得到用户所有的角色集合
            List<TG.Model.cm_Role> roleList = new TG.BLL.cm_Role().GetRoleList(userSysNo);

            int position = 0;
            switch (auditStatus)
            {
                case "A":
                    position = 1;
                    break;
                case "B":
                    position = 2;
                    break;

            }
            //得到该审核记录当前的审核角色
            TG.Model.cm_ProImageAuditConfig coperationAuditConfig = bll.GetCoperationAuditConfigByPostion(position);

            string hasPower = (from role in roleList where role.SysNo == coperationAuditConfig.RoleSysNo select role).Count() > 0 ? "1" : "0";

            return hasPower;
        }

        #region 绑定数据
        public void GetProInfo(int strProId)
        {
            //显示项目信息
            SelectItemName(strProId);

            TG.Model.cm_ProImage proimage = bllImage.GetModel(strProId);
            if (proimage != null)
            {
                FuZhi(proimage.jz_image, "jz");
                FuZhi(proimage.jz_cadimage, "cadjz");
                FuZhi(proimage.jg_image, "jg");
                FuZhi(proimage.jg_cadimage, "cadjg");
                FuZhi(proimage.jps_image, "gps");
                FuZhi(proimage.jps_cadimage, "cadgps");
                FuZhi(proimage.nt_image, "nt");
                FuZhi(proimage.nt_cadimage, "cadnt");
                FuZhi(proimage.dq_image, "dq");
                FuZhi(proimage.dq_cadimage, "dq");

                SumFuZhi();
                if (!string.IsNullOrEmpty(proimage.image_type))
                {
                    ct = proimage.image_type.Trim();
                }
                FenLeiPanDuan(proimage.image_type, proimage.guidang.ToString());
            }
        }

        protected void SelectItemName(int strproID)
        {

            DataSet ds = bllImage.GetProjectInfo(strproID);
            this.txt_proname.Text = ds.Tables[0].Rows[0]["pro_name"] == null ? "" : ds.Tables[0].Rows[0]["pro_name"].ToString();
            this.txt_prounit.Text = ds.Tables[0].Rows[0]["Unit"] == null ? "" : ds.Tables[0].Rows[0]["Unit"].ToString();
            this.txt_pm_name.Text = ds.Tables[0].Rows[0]["PMName"] == null ? "" : ds.Tables[0].Rows[0]["PMName"].ToString();
            this.txt_pronumber.Text = ds.Tables[0].Rows[0]["gch"] == null ? "" : ds.Tables[0].Rows[0]["gch"].ToString();
            this.txt_prostatus.Text = ds.Tables[0].Rows[0]["pro_status"] == null ? "" : ds.Tables[0].Rows[0]["pro_status"].ToString();
            this.txt_proitemname.Text = ds.Tables[0].Rows[0]["itemname"] == null ? "" : ds.Tables[0].Rows[0]["itemname"].ToString();
        }


        public void FuZhi(string strs, string type)
        {
            string[] vals = strs.Split('|');
            if (type == "jz")
            {
                jz0.Text = vals[0].ToString();
                jz1.Text = vals[1].ToString();
                jz2.Text = vals[2].ToString();
                jz2_1.Text = vals[3].ToString();
                jz3.Text = vals[4].ToString();
                jz4.Text = vals[5].ToString();
                jz1_1.Text = vals[6].ToString();
            }
            else if (type == "cadjz")
            {
                cadjz0.Text = vals[0].ToString();
                cadjz1.Text = vals[1].ToString();
                cadjz2.Text = vals[2].ToString();
                cadjz2_1.Text = vals[3].ToString();
                cadjz3.Text = vals[4].ToString();
                cadjz4.Text = vals[5].ToString();
                cadjz1_1.Text = vals[6].ToString();
            }
            else if (type == "jg")
            {
                jg0.Text = vals[0].ToString();
                jg1.Text = vals[1].ToString();
                jg2.Text = vals[2].ToString();
                jg2_1.Text = vals[3].ToString();
                jg3.Text = vals[4].ToString();
                jg4.Text = vals[5].ToString();
                jg1_1.Text = vals[6].ToString();

            }
            else if (type == "cadjg")
            {
                cadjg0.Text = vals[0].ToString();
                cadjg1.Text = vals[1].ToString();
                cadjg2.Text = vals[2].ToString();
                cadjg2_1.Text = vals[3].ToString();
                cadjg3.Text = vals[4].ToString();
                cadjg4.Text = vals[5].ToString();
                cadjg1_1.Text = vals[6].ToString();
            }
            else if (type == "cadgps")
            {
                cadgps0.Text = vals[0].ToString();
                cadgps1.Text = vals[1].ToString();
                cadgps2.Text = vals[2].ToString();
                cadgps2_1.Text = vals[3].ToString();
                cadgps3.Text = vals[4].ToString();
                cadgps4.Text = vals[5].ToString();
                cadgps1_1.Text = vals[6].ToString();
            }
            else if (type == "gps")
            {
                gps0.Text = vals[0].ToString();
                gps1.Text = vals[1].ToString();
                gps2.Text = vals[2].ToString();
                gps2_1.Text = vals[3].ToString();
                gps3.Text = vals[4].ToString();
                gps4.Text = vals[5].ToString();
                gps1_1.Text = vals[6].ToString();

            }
            else if (type == "nt")
            {
                nt0.Text = vals[0].ToString();
                nt1.Text = vals[1].ToString();
                nt2.Text = vals[2].ToString();
                nt2_1.Text = vals[3].ToString();
                nt3.Text = vals[4].ToString();
                nt4.Text = vals[5].ToString();
                nt1_1.Text = vals[6].ToString();

            }
            else if (type == "cadnt")
            {
                cadnt0.Text = vals[0].ToString();
                cadnt1.Text = vals[1].ToString();
                cadnt2.Text = vals[2].ToString();
                cadnt2_1.Text = vals[3].ToString();
                cadnt3.Text = vals[4].ToString();
                cadnt4.Text = vals[5].ToString();
                cadnt1_1.Text = vals[6].ToString();

            }
            else if (type == "dq")
            {
                dq0.Text = vals[0].ToString();
                dq1.Text = vals[1].ToString();
                dq2.Text = vals[2].ToString();
                dq2_1.Text = vals[3].ToString();
                dq3.Text = vals[4].ToString();
                dq4.Text = vals[5].ToString();
                dq1_1.Text = vals[6].ToString();

            }
            else if (type == "caddq")
            {
                caddq0.Text = vals[0].ToString();
                caddq1.Text = vals[1].ToString();
                caddq2.Text = vals[2].ToString();
                caddq2_1.Text = vals[3].ToString();
                caddq3.Text = vals[4].ToString();
                caddq4.Text = vals[5].ToString();
                caddq1_1.Text = vals[6].ToString();

            }

        }
        public void SumFuZhi()
        {
            this.hj0.Text = (int.Parse(jz0.Text) + int.Parse(jg0.Text) + int.Parse(gps0.Text) + int.Parse(nt0.Text) + int.Parse(dq0.Text)).ToString();
            this.hj1.Text = (int.Parse(jz1.Text) + int.Parse(jg1.Text) + int.Parse(gps1.Text) + int.Parse(nt1.Text) + int.Parse(dq1.Text)).ToString();
            this.hj2.Text = (int.Parse(jz2.Text) + int.Parse(jg2.Text) + int.Parse(gps2.Text) + int.Parse(nt2.Text) + int.Parse(dq2.Text)).ToString();
            this.hj2_1.Text = (int.Parse(jz2_1.Text) + int.Parse(jg2_1.Text) + int.Parse(gps2_1.Text) + int.Parse(nt2_1.Text) + int.Parse(dq2_1.Text)).ToString();
            this.hj3.Text = (int.Parse(jz3.Text) + int.Parse(jg3.Text) + int.Parse(gps3.Text) + int.Parse(nt3.Text) + int.Parse(dq3.Text)).ToString();
            this.hj4.Text = (int.Parse(jz4.Text) + int.Parse(jg4.Text) + int.Parse(gps4.Text) + int.Parse(nt4.Text) + int.Parse(dq4.Text)).ToString();
            this.hj1_1.Text = (int.Parse(jz1_1.Text) + int.Parse(jg1_1.Text) + int.Parse(gps1_1.Text) + int.Parse(nt1_1.Text) + int.Parse(dq1_1.Text)).ToString();


            this.cadhj0.Text = (int.Parse(cadjz0.Text) + int.Parse(cadjg0.Text) + int.Parse(cadgps0.Text) + int.Parse(cadnt0.Text) + int.Parse(caddq0.Text)).ToString();
            this.caddq1.Text = (int.Parse(cadjz1.Text) + int.Parse(cadjg1.Text) + int.Parse(cadgps1.Text) + int.Parse(cadnt1.Text) + int.Parse(caddq1.Text)).ToString();
            this.cadhj2.Text = (int.Parse(cadjz2.Text) + int.Parse(cadjg2.Text) + int.Parse(cadgps2.Text) + int.Parse(cadnt2.Text) + int.Parse(caddq2.Text)).ToString();
            this.cadhj2_1.Text = (int.Parse(cadjz2_1.Text) + int.Parse(cadjg2_1.Text) + int.Parse(cadgps2_1.Text) + int.Parse(cadnt2_1.Text) + int.Parse(caddq2_1.Text)).ToString();
            this.cadhj3.Text = (int.Parse(cadjz3.Text) + int.Parse(cadjg3.Text) + int.Parse(cadgps3.Text) + int.Parse(cadnt3.Text) + int.Parse(caddq3.Text)).ToString();
            this.cadhj4.Text = (int.Parse(cadjz4.Text) + int.Parse(cadjg4.Text) + int.Parse(cadgps4.Text) + int.Parse(cadnt4.Text) + int.Parse(caddq4.Text)).ToString();
            this.cadhj1_1.Text = (int.Parse(cadjz1_1.Text) + int.Parse(cadjg1_1.Text) + int.Parse(cadgps1_1.Text) + int.Parse(cadnt1_1.Text) + int.Parse(caddq1_1.Text)).ToString();

            Sum_image.Text = (int.Parse(hj0.Text) + int.Parse(hj1.Text) + int.Parse(hj2.Text) + int.Parse(hj2_1.Text) + int.Parse(hj3.Text) + int.Parse(hj4.Text) + int.Parse(hj1_1.Text) + int.Parse(cadhj0.Text) + int.Parse(cadhj1.Text) + int.Parse(cadhj2.Text) + int.Parse(cadhj2_1.Text) + int.Parse(cadhj3.Text) + int.Parse(cadhj4.Text) + int.Parse(cadhj1_1.Text)).ToString();

        }
        public void FenLeiPanDuan(string type, string guidang)
        {
            
            if (guidang == "0")
            {
                check_Gd.Checked = true;
            }
        }
        #endregion

    }


}

