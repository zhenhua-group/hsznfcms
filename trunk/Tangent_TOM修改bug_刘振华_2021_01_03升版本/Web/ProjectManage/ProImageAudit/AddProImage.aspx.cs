﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using System.Data;
using TG.DBUtility;
using TG.Common;

namespace TG.Web.ProjectManage.ProImageAudit
{
    public partial class AddProImage : PageBase
    {

        public string Str_Prid { get; set; }
        public int id = 0;
        public string ct = "";
        TG.BLL.cm_ProImage pi = new BLL.cm_ProImage();

        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(AddProImage));
            Str_Prid = Request.QueryString["bs_project_Id"].ToString();
            id=pi.GetSingle(Convert.ToInt32(Str_Prid));
            if (!IsPostBack)
            {               
                GetProInfo(Str_Prid);
               
            }
        }

        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }


        #region 绑定项目信息


        public void GetProInfo(string strProId)
        {

            //显示项目信息
            SelectItemName(strProId);
            if (id > 0)
            {
                TG.Model.cm_ProImage proimage = pi.GetModel(id);
                if (proimage != null)
                {
                    FuZhi(proimage.jz_image, "jz");
                    FuZhi(proimage.jz_cadimage, "cadjz");
                    FuZhi(proimage.jg_image, "jg");
                    FuZhi(proimage.jg_cadimage, "cadjg");
                    FuZhi(proimage.jps_image, "gps");
                    FuZhi(proimage.jps_cadimage, "cadgps");
                    FuZhi(proimage.nt_image, "nt");
                    FuZhi(proimage.nt_cadimage, "cadnt");
                    FuZhi(proimage.dq_image, "dq");
                    FuZhi(proimage.dq_cadimage, "caddq");

                    SumFuZhi();
                    if (!string.IsNullOrEmpty(proimage.image_type))
                    {
                        ct = proimage.image_type.Trim();
                    }
                    FenLeiPanDuan(proimage.guidang.ToString());
                }
            }
           

        }

        //获取项目信息
        protected void SelectItemName(string strproID)
        {
            DataSet ds = pi.GetProjectInfo(int.Parse(strproID));
            this.txt_proname.Text = ds.Tables[0].Rows[0]["pro_name"] == null ? "" : ds.Tables[0].Rows[0]["pro_name"].ToString();
            this.txt_prounit.Text = ds.Tables[0].Rows[0]["Unit"] == null ? "" : ds.Tables[0].Rows[0]["Unit"].ToString();
            this.txt_pm_name.Text = ds.Tables[0].Rows[0]["PMName"] == null ? "" : ds.Tables[0].Rows[0]["PMName"].ToString();
            this.txt_pronumber.Text = ds.Tables[0].Rows[0]["gch"] == null ? "" : ds.Tables[0].Rows[0]["gch"].ToString();
            this.txt_prostatus.Text = ds.Tables[0].Rows[0]["pro_status"] == null ? "" : ds.Tables[0].Rows[0]["pro_status"].ToString();
            this.txt_proitemname.Text = ds.Tables[0].Rows[0]["itemname"] == null ? "" : ds.Tables[0].Rows[0]["itemname"].ToString();
        }

        /// <summary>
        /// 为文本框赋值
        /// </summary>
        /// <param name="strs"></param>
        /// <param name="type"></param>
        public void FuZhi(string strs, string type)
        {
            string[] vals = strs.Split('|');
            if (type == "jz")
            {
                jz0.Text = vals[0].ToString();
                jz1.Text = vals[1].ToString();
                jz2.Text = vals[2].ToString();
                jz2_1.Text = vals[3].ToString();
                jz3.Text = vals[4].ToString();
                jz4.Text = vals[5].ToString();
                jz1_1.Text = vals[6].ToString();
            }
            else if (type == "cadjz")
            {
                cadjz0.Text = vals[0].ToString();
                cadjz1.Text = vals[1].ToString();
                cadjz2.Text = vals[2].ToString();
                cadjz2_1.Text = vals[3].ToString();
                cadjz3.Text = vals[4].ToString();
                cadjz4.Text = vals[5].ToString();
                cadjz1_1.Text = vals[6].ToString();
            }
            else if (type == "jg")
            {
                jg0.Text = vals[0].ToString();
                jg1.Text = vals[1].ToString();
                jg2.Text = vals[2].ToString();
                jg2_1.Text = vals[3].ToString();
                jg3.Text = vals[4].ToString();
                jg4.Text = vals[5].ToString();
                jg1_1.Text = vals[6].ToString();

            }
            else if (type == "cadjg")
            {
                cadjg0.Text = vals[0].ToString();
                cadjg1.Text = vals[1].ToString();
                cadjg2.Text = vals[2].ToString();
                cadjg2_1.Text = vals[3].ToString();
                cadjg3.Text = vals[4].ToString();
                cadjg4.Text = vals[5].ToString();
                cadjg1_1.Text = vals[6].ToString();
            }
            else if (type == "cadgps")
            {
                cadgps0.Text = vals[0].ToString();
                cadgps1.Text = vals[1].ToString();
                cadgps2.Text = vals[2].ToString();
                cadgps2_1.Text = vals[3].ToString();
                cadgps3.Text = vals[4].ToString();
                cadgps4.Text = vals[5].ToString();
                cadgps1_1.Text = vals[6].ToString();
            }
            else if (type == "gps")
            {
                gps0.Text = vals[0].ToString();
                gps1.Text = vals[1].ToString();
                gps2.Text = vals[2].ToString();
                gps2_1.Text = vals[3].ToString();
                gps3.Text = vals[4].ToString();
                gps4.Text = vals[5].ToString();
                gps1_1.Text = vals[6].ToString();

            }
            else if (type == "nt")
            {
                nt0.Text = vals[0].ToString();
                nt1.Text = vals[1].ToString();
                nt2.Text = vals[2].ToString();
                nt2_1.Text = vals[3].ToString();
                nt3.Text = vals[4].ToString();
                nt4.Text = vals[5].ToString();
                nt1_1.Text = vals[6].ToString();

            }
            else if (type == "cadnt")
            {
                cadnt0.Text = vals[0].ToString();
                cadnt1.Text = vals[1].ToString();
                cadnt2.Text = vals[2].ToString();
                cadnt2_1.Text = vals[3].ToString();
                cadnt3.Text = vals[4].ToString();
                cadnt4.Text = vals[5].ToString();
                cadnt1_1.Text = vals[6].ToString();

            }
            else if (type == "dq")
            {
                dq0.Text = vals[0].ToString();
                dq1.Text = vals[1].ToString();
                dq2.Text = vals[2].ToString();
                dq2_1.Text = vals[3].ToString();
                dq3.Text = vals[4].ToString();
                dq4.Text = vals[5].ToString();
                dq1_1.Text = vals[6].ToString();

            }
            else if (type == "caddq")
            {
                caddq0.Text = vals[0].ToString();
                caddq1.Text = vals[1].ToString();
                caddq2.Text = vals[2].ToString();
                caddq2_1.Text = vals[3].ToString();
                caddq3.Text = vals[4].ToString();
                caddq4.Text = vals[5].ToString();
                caddq1_1.Text = vals[6].ToString();
            }

        }
        /// <summary>
        /// 计算图纸总数
        /// </summary>
        /// <returns></returns>
        public string[] SumImage()
        {
            string[] strs = new string[15];
            strs[0] = (int.Parse(jz0.Text) + int.Parse(jg0.Text) + int.Parse(gps0.Text) + int.Parse(nt0.Text) + int.Parse(dq0.Text)).ToString();
            strs[1] = (int.Parse(jz1.Text) + int.Parse(jg1.Text) + int.Parse(gps1.Text) + int.Parse(nt1.Text) + int.Parse(dq1.Text)).ToString();
            strs[2] = (int.Parse(jz2.Text) + int.Parse(jg2.Text) + int.Parse(gps2.Text) + int.Parse(nt2.Text) + int.Parse(dq2.Text)).ToString();
            strs[3] = (int.Parse(jz2_1.Text) + int.Parse(jg2_1.Text) + int.Parse(gps2_1.Text) + int.Parse(nt2_1.Text) + int.Parse(dq2_1.Text)).ToString();
            strs[4] = (int.Parse(jz3.Text) + int.Parse(jg3.Text) + int.Parse(gps3.Text) + int.Parse(nt3.Text) + int.Parse(dq3.Text)).ToString();
            strs[5] = (int.Parse(jz4.Text) + int.Parse(jg4.Text) + int.Parse(gps4.Text) + int.Parse(nt4.Text) + int.Parse(dq4.Text)).ToString();
            strs[6] = (int.Parse(jz1_1.Text) + int.Parse(jg1_1.Text) + int.Parse(gps1_1.Text) + int.Parse(nt1_1.Text) + int.Parse(dq1_1.Text)).ToString();


            strs[7] = (int.Parse(cadjz0.Text) + int.Parse(cadjg0.Text) + int.Parse(cadgps0.Text) + int.Parse(cadnt0.Text) + int.Parse(caddq0.Text)).ToString();
            strs[8] = (int.Parse(cadjz1.Text) + int.Parse(cadjg1.Text) + int.Parse(cadgps1.Text) + int.Parse(cadnt1.Text) + int.Parse(caddq1.Text)).ToString();
            strs[9] = (int.Parse(cadjz2.Text) + int.Parse(cadjg2.Text) + int.Parse(cadgps2.Text) + int.Parse(cadnt2.Text) + int.Parse(caddq2.Text)).ToString();
            strs[10] = (int.Parse(cadjz2_1.Text) + int.Parse(cadjg2_1.Text) + int.Parse(cadgps2_1.Text) + int.Parse(cadnt2_1.Text) + int.Parse(caddq2_1.Text)).ToString();
            strs[11] = (int.Parse(cadjz3.Text) + int.Parse(cadjg3.Text) + int.Parse(cadgps3.Text) + int.Parse(cadnt3.Text) + int.Parse(caddq3.Text)).ToString();
            strs[12] = (int.Parse(cadjz4.Text) + int.Parse(cadjg4.Text) + int.Parse(cadgps4.Text) + int.Parse(cadnt4.Text) + int.Parse(caddq4.Text)).ToString();
            strs[13] = (int.Parse(cadjz1_1.Text) + int.Parse(cadjg1_1.Text) + int.Parse(cadgps1_1.Text) + int.Parse(cadnt1_1.Text) + int.Parse(caddq1_1.Text)).ToString();

            strs[14] = (int.Parse(strs[0]) + int.Parse(strs[1]) + int.Parse(strs[2]) + int.Parse(strs[3]) + int.Parse(strs[4]) + int.Parse(strs[5]) + int.Parse(strs[6]) + int.Parse(strs[7]) + int.Parse(strs[8]) + int.Parse(strs[9]) + int.Parse(strs[10]) + int.Parse(strs[11]) + int.Parse(strs[12]) + int.Parse(strs[13])).ToString();
            return strs;
        }
        /// <summary>
        /// 为图纸总数赋值
        /// </summary>
        public void SumFuZhi()
        {
            string[] strs = SumImage();
            this.hj0.Text = strs[0];
            this.hj1.Text = strs[1];
            this.hj2.Text = strs[2];
            this.hj2_1.Text = strs[3];
            this.hj3.Text = strs[4];
            this.hj4.Text = strs[5];
            this.hj1_1.Text = strs[6];
            this.cadhj0.Text = strs[7];
            this.caddq1.Text = strs[8];
            this.cadhj2.Text = strs[9];
            this.cadhj2_1.Text = strs[10];
            this.cadhj3.Text = strs[11];
            this.cadhj4.Text = strs[12];
            this.cadhj1_1.Text = strs[13];
            Sum_image.Text = strs[14];
        }
        /// <summary>
        /// 判断图纸类型
        /// </summary>

        #endregion
        
        public void FenLeiPanDuan(string guidang)
        {
            if (guidang == "0")
            {
                check_Gd.Checked = true;
            }
        }
        /// <summary>
        /// 连接字符
        /// </summary>
        protected string[] Joinstr()
        {
            string[] strs = new string[10];
            //按照，建筑\cad\结构\cad\给排水\cad\暖通\cad\电气\cad
            strs[0] = jz0.Text + "|" + jz1.Text + "|" + jz2.Text + "|" + jz2_1.Text + "|" + jz3.Text + "|" + jz4.Text + "|" + jz1_1.Text;
            strs[1] = cadjz0.Text + "|" + cadjz1.Text + "|" + cadjz2.Text + "|" + cadjz2_1.Text + "|" + cadjz3.Text + "|" + cadjz4.Text + "|" + cadjz1_1.Text;
            strs[2] = jg0.Text + "|" + jg1.Text + "|" + jg2.Text + "|" + jg2_1.Text + "|" + jg3.Text + "|" + jg4.Text + "|" + jg1_1.Text;
            strs[3] = cadjg0.Text + "|" + cadjg1.Text + "|" + cadjg2.Text + "|" + cadjg2_1.Text + "|" + cadjg3.Text + "|" + cadjg4.Text + "|" + cadjg1_1.Text;
            strs[4] = gps0.Text + "|" + gps1.Text + "|" + gps2.Text + "|" + gps2_1.Text + "|" + gps3.Text + "|" + gps4.Text + "|" + gps1_1.Text;
            strs[5] = cadgps0.Text + "|" + cadgps1.Text + "|" + cadgps2.Text + "|" + cadgps2_1.Text + "|" + cadgps3.Text + "|" + cadgps4.Text + "|" + cadgps1_1.Text;
            strs[6] = nt0.Text + "|" + nt1.Text + "|" + nt2.Text + "|" + nt2_1.Text + "|" + nt3.Text + "|" + nt4.Text + "|" + nt1_1.Text;
            strs[7] = cadnt0.Text + "|" + cadnt1.Text + "|" + cadnt2.Text + "|" + cadnt2_1.Text + "|" + cadnt3.Text + "|" + cadnt4.Text + "|" + cadnt1_1.Text;
            strs[8] = dq0.Text + "|" + dq1.Text + "|" + dq2.Text + "|" + dq2_1.Text + "|" + dq3.Text + "|" + dq4.Text + "|" + dq1_1.Text;
            strs[9] = caddq0.Text + "|" + caddq1.Text + "|" + caddq2.Text + "|" + caddq2_1.Text + "|" + caddq3.Text + "|" + caddq4.Text + "|" + caddq1_1.Text;
            return strs;
        }


        protected void btn_Save_Click(object sender, ImageClickEventArgs e)
        {

            Model.cm_ProImage model = new Model.cm_ProImage();
            model.pro_id = Str_Prid;
            model.pro_name = txt_proname.Text;
            model.pro_number = txt_pronumber.Text;
            model.pro_unit = txt_prounit.Text;
            model.pro_pmname = txt_pm_name.Text;
            model.pro_status = txt_prostatus.Text;
            string[] strs1 = Joinstr();
            model.jz_image = strs1[0];
            model.jz_cadimage = strs1[1];
            model.jg_image = strs1[2];
            model.jg_cadimage = strs1[3];
            model.jps_image = strs1[4];
            model.jps_cadimage = strs1[5];
            model.nt_image = strs1[6];
            model.nt_cadimage = strs1[7];
            model.dq_image = strs1[8];
            model.dq_cadimage = strs1[9];
            string[] strs2 = SumImage();
            model.zh = int.Parse(strs2[14]);
            model.image_type = Request.Form["ct"];
            if (check_Gd.Checked)
            {
                model.guidang = 0;
            }

            int count = 0;
            bool flag = false;
            
            if (id>0)
            {
                model.ID = id;
                flag = pi.Update(model);                
             }
            else
            {
                 count = pi.Add(model);                
            }

            if (count > 0 || flag==true)
            {
                MessageBox.Show(this, "保存成功");
            }
            else
            {
                MessageBox.Show(this, "保存失败");
            }
        }
    }
}