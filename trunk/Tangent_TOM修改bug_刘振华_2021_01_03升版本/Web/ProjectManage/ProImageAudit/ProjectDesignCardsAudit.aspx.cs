﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;
using System.Data;
using System.Text;
using AjaxPro;

namespace TG.Web.ProjectManage.ProImageAudit
{
    public partial class ProjectDesignCardsAudit : PageBase
    {
        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        #region 传入参数
        //审批消息ID
        public int MessageID
        {
            get
            {
                int msgSysNo = 0;
                int.TryParse(Request["MsgNo"], out msgSysNo);
                return msgSysNo;
            }
        }
        /// <summary>
        /// 项目审核SysNo
        /// </summary>
        public int ProjectPlotAuditSysNo
        {
            get
            {
                int projectPlanAuditSysNo = 0;
                int.TryParse(Request["ProjectPlotAuditSysNo"], out projectPlanAuditSysNo);
                return projectPlanAuditSysNo;
            }
        }
        protected override bool IsAuth
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// 消息状态
        /// </summary>
        public string MessageStatus
        {
            get
            {
                return Request["MessageStatus"];
            }
        }
        #endregion

        #region 得到信息
        /// <summary>
        /// 项目ID
        /// </summary>
        public int ProSysNo { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public string AuditStatus { get; set; }

        /// <summary>
        /// 出图类型
        /// </summary>
        public string PlotType { get; set; }

        /// <summary>
        /// 审核信息
        /// </summary>
        public string AuditHTML { get; set; }

        /// <summary>
        /// 档案号
        /// </summary>
        public string FileNum { get; set; }
        #endregion

        private TG.BLL.cm_ProjectPlotInfo bll = new TG.BLL.cm_ProjectPlotInfo();
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProjectDesignCardsAudit));
            if (!IsPostBack)
            {
                //绑定出图类型
                BindPlotTypeList();

                GetProjectPlanAudit();

                //得到项目信息
                bindProjectInfo();

                //绑定子项
                bindSubPlotInfo();
            }
        }

        /// <summary>
        /// 出图类型
        /// </summary>
        public void BindPlotTypeList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定出图类型
            DataSet dic_proly = dic.GetList(" dic_type='plot_type'");
            rbtlist.DataValueField = "id";
            rbtlist.DataTextField = "dic_name";
            rbtlist.DataSource = dic_proly;
            rbtlist.DataBind();
        }

        /// <summary>
        /// 绑定项目信息
        /// </summary>
        private void bindProjectInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(ProSysNo);
            if (pro_model != null)
            {
                txt_proname.Text = pro_model.pro_name;
                txt_pronumber.Text = pro_model.Pro_number;
                txt_proitemname.Text = pro_model.pro_name;
                txt_prostatus.Text = pro_model.pro_status;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }
                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }
                lbl_PMName.Text = pro_model.PMName;
                txt_prounit.Text = pro_model.Unit;
            }
        }
        /// <summary>
        /// 绑定信息
        /// </summary>
        private void bindGetPlotInfo()
        {
            TG.BLL.cm_ProjectPlotInfo bll = new TG.BLL.cm_ProjectPlotInfo();
            TG.Model.cm_ProjectPlotInfo model = bll.GetPlotInfo(ProSysNo);
            if (model != null)
            {
                string plotType = model.PlotType;
                if (this.rbtlist.Items.FindByText(plotType) != null)
                {
                    this.rbtlist.Items.FindByText(plotType).Selected = true;
                }
                PlotType = plotType;
                lbl_BlueprintCounts.Text = model.BlueprintCounts.ToString();
                lbl_PlotUserName.Text = model.PlotUserName;
                FileNum = model.FileNum;
            }
        }
        /// <summary>
        /// 获取审批信息
        /// </summary>
        private void GetProjectPlanAudit()
        {

            TG.Model.cm_ProjectPlotInfoAudit projectPlotAudit = bll.GetProjectPlotInfoAuditEntity(new cm_ProjectPlotAuditQueryEntity { ProjectPlotAuditSysNo = ProjectPlotAuditSysNo });

            //获取审批时的项目ID
            if (projectPlotAudit != null)
            {
                ProSysNo = projectPlotAudit.ProjectSysNo;
                AuditStatus = projectPlotAudit.Status;
            }
            else
            {
                ProSysNo = 0;
            }

            //绑定出图信息
            bindGetPlotInfo();

            if (projectPlotAudit != null)
            {
                GetProjectPlanAuditEntity(projectPlotAudit);
            }
        }

        /// <summary>
        /// 查询审核实体的方法
        /// </summary>
        public void GetProjectPlanAuditEntity(cm_ProjectPlotInfoAudit projectPlotAudit)
        {

            //查询项目审核策划的流程信息
            List<string> processDescription = bll.GetAuditProcessDescription();
            //审批用户
            string[] auditUserArray = projectPlotAudit.AuditUserArray;
            //审批时间
            string[] auditUserDate = projectPlotAudit.AuditDateArray;

            string html = "";
            html = "<fieldset style=\"width:98%;font-size:12px;\"><legend>审批意见</legend><table style=\"width: 100%; font-size: 12px\">";

            if (PlotType.Trim().Equals("特殊输出") || PlotType.Trim().Equals("结构延迟校审"))
            {
                if (projectPlotAudit != null && projectPlotAudit.SuggestionArray != null)
                {
                    TG.BLL.tg_member uBp = new BLL.tg_member();
                    int i = 0;

                    if ((AuditStatus != MessageStatus) && (MessageStatus == "A" || MessageStatus == "C"))
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[0]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[0];
                        html += JoinAuditHTML(projectPlotAudit.SuggestionArray[0], usreName, processDescription[0], auditDate, false);
                    }
                    else if ((AuditStatus != MessageStatus) && (MessageStatus == "B" || MessageStatus == "E"))
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[1]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[1].ToString();
                        html += JoinAuditHTML(projectPlotAudit.SuggestionArray[1], usreName, processDescription[1], auditDate, false);
                    }
                    else if ((AuditStatus != MessageStatus) && (MessageStatus == "D" || MessageStatus == "G"))
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[2]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[2].ToString();
                        html += JoinAuditHTML(projectPlotAudit.SuggestionArray[2], usreName, processDescription[2], auditDate, false);
                    }
                    else if ((AuditStatus != MessageStatus) && (MessageStatus == "F" || MessageStatus == "I"))
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[3]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[3].ToString();
                        html += JoinAuditHTML(projectPlotAudit.SuggestionArray[3], usreName, processDescription[3], auditDate, false);
                    }
                    else if ((AuditStatus != MessageStatus) && (MessageStatus == "H" || MessageStatus == "K"))
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[4]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[4].ToString();
                        html += JoinAuditHTML(projectPlotAudit.SuggestionArray[4], usreName, processDescription[4], auditDate, false);
                    }
                    else if ((AuditStatus != MessageStatus) && (MessageStatus == "J" || MessageStatus == "M"))
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[5]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[5].ToString();
                        html += JoinAuditHTML(projectPlotAudit.SuggestionArray[5], usreName, processDescription[5], auditDate, false);
                    }
                    else if ((AuditStatus != MessageStatus) && (MessageStatus == "L" || MessageStatus == "O"))
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[6]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[6].ToString();
                        html += JoinAuditHTML(projectPlotAudit.SuggestionArray[6], usreName, processDescription[6], auditDate, false);
                    }
                    else if ((AuditStatus != MessageStatus) && (MessageStatus == "N" || MessageStatus == "Q"))
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[7]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[7].ToString();
                        html += JoinAuditHTML(projectPlotAudit.SuggestionArray[7], usreName, processDescription[7], auditDate, true);
                    }
                    else
                    {
                        int length = projectPlotAudit.SuggestionArray.Length;
                        foreach (string suggestion in projectPlotAudit.SuggestionArray)
                        {
                            TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[i]));
                            string usreName = user == null ? "" : user.mem_Name;
                            string auditDate = auditUserDate[i].ToString();
                            if (AuditStatus == "P")
                            {
                                if (i == (length - 1))
                                {
                                    html += JoinAuditHTML(suggestion, usreName, processDescription[i], auditDate, true);
                                }
                                else
                                {
                                    html += JoinAuditHTML(suggestion, usreName, processDescription[i], auditDate, false);
                                }
                            }
                            else
                            {
                                html += JoinAuditHTML(suggestion, usreName, processDescription[i], auditDate, false);
                            }
                            i++;
                        }

                        if (AuditStatus == "P")
                        {
                            html += "<tr><td align=\"center\" colspan=\"2\"><input type=\"button\" id=\"Button1\" style=\"background-color: #E8E8E8\" name=\"controlBtn\" class=\"cls_btn_comm_w\" value=\"打印\" onclick=\"window.open('PrintProjectPlotInfo.aspx?ProjectSysNo=" + ProSysNo + "','打印','width=800,height=500,top=50,left=150,scrollbars=yes,resizable=yes,location=no');\" />&nbsp;<input type=\"button\" id=\"Button1\" style=\"background-color: #E8E8E8\" name=\"controlBtn\" class=\"cls_btn_comm_w\" value=\"返回\" onclick=\"javascript:history.back();\" /></td></tr>";
                        }
                    }

                }
                if (AuditStatus == MessageStatus)
                {
                    if (AuditStatus != "C" && AuditStatus != "E" && AuditStatus != "G" && AuditStatus != "I" && AuditStatus != "K" && AuditStatus != "M" && AuditStatus != "O" && AuditStatus != "Q" && AuditStatus != "P")
                    {
                        if (AuditStatus == "N")
                        {
                            html += JoinAuditHTML("", "", "", "", true);
                        }
                        else
                        {
                            html += JoinAuditHTML("", "", "", "", false);
                        }
                    }
                }
            }
            else
            {
                if (projectPlotAudit != null && projectPlotAudit.SuggestionArray != null)
                {
                    TG.BLL.tg_member uBp = new BLL.tg_member();
                    int i = 0;
                    //查询流程信息
                    List<string> temp = processDescription;
                    temp.RemoveAt(0);
                    temp.RemoveAt(0);
                    temp.RemoveAt(0);
                    temp.RemoveAt(0);
                    temp.RemoveAt(0);
                    if ((AuditStatus != MessageStatus) && (MessageStatus == "A" || MessageStatus == "C"))
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[0]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[0];
                        html += JoinAuditHTML(projectPlotAudit.SuggestionArray[0], usreName, temp[0], auditDate, false);
                    }
                    else if ((AuditStatus != MessageStatus) && (MessageStatus == "B" || MessageStatus == "E"))
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[1]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[1].ToString();
                        html += JoinAuditHTML(projectPlotAudit.SuggestionArray[1], usreName, temp[1], auditDate, false);
                    }
                    else if ((AuditStatus != MessageStatus) && (MessageStatus == "D" || MessageStatus == "G"))
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[2]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[2].ToString();
                        html += JoinAuditHTML(projectPlotAudit.SuggestionArray[2], usreName, temp[2], auditDate, true);
                    }
                    else
                    {
                        int length = projectPlotAudit.SuggestionArray.Length;
                        foreach (string suggestion in projectPlotAudit.SuggestionArray)
                        {
                            TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[i]));
                            string usreName = user == null ? "" : user.mem_Name;
                            string auditDate = auditUserDate[i].ToString();
                            if (AuditStatus == "F")
                            {
                                if (i == (length - 1))
                                {
                                    html += JoinAuditHTML(suggestion, usreName, processDescription[i], auditDate, true);
                                }
                                else
                                {
                                    html += JoinAuditHTML(suggestion, usreName, processDescription[i], auditDate, false);
                                }
                            }
                            else
                            {
                                html += JoinAuditHTML(suggestion, usreName, processDescription[i], auditDate, false);
                            }
                            i++;
                        }

                        if (AuditStatus == "F")
                        {
                            html += "<tr><td align=\"center\" colspan=\"2\"><input type=\"button\" id=\"Button1\" style=\"background-color: #E8E8E8\" name=\"controlBtn\" class=\"cls_btn_comm_w\" value=\"打印\" onclick=\"window.open('PrintProjectPlotInfo.aspx?ProjectSysNo=" + ProSysNo + "','打印','width=800,height=500,top=50,left=150,scrollbars=yes,resizable=yes,location=no');\" />&nbsp;<input type=\"button\" id=\"Button1\" style=\"background-color: #E8E8E8\" name=\"controlBtn\" class=\"cls_btn_comm_w\" value=\"返回\" onclick=\"javascript:history.back();\" /></td></tr>";
                        }
                    }

                }
                if (AuditStatus == MessageStatus)
                {
                    if (AuditStatus != "C" && AuditStatus != "E" && AuditStatus != "G" && AuditStatus != "F")
                    {
                        if (AuditStatus == "D")
                        {
                            html += JoinAuditHTML("", "", "", "", true);
                        }
                        else
                        {
                            html += JoinAuditHTML("", "", "", "", false);
                        }
                    }
                }
            }

            html += "  </table></fieldset>";
            AuditHTML = html;
        }

        public bool CheckPower()
        {
            int roleSysNo = new TG.BLL.cm_ProjectPlanBP().GetProcessRoleSysNo(AuditStatus);

            bool hasPower = new TG.BLL.cm_Role().CheckPower(roleSysNo, UserSysNo);

            return hasPower;
        }

        private string JoinAuditHTML(string suggestion, string userName, string roleName, string auditDate, bool status)
        {
            string disableFlag = "";
            string idFlag = "";
            if (!string.IsNullOrEmpty(suggestion))
            {
                disableFlag = "disabled=disabled";
            }
            else
            {
                idFlag = "suggestionTextArea";
            }

            string html = "";
            html += "<tr>";
            html += "<td style=\"width: 20%\" align=\"center\">";
            string sugesstionString = "意见";
            if (!string.IsNullOrEmpty(roleName) && !string.IsNullOrEmpty(userName))
            {
                sugesstionString = string.Format("{0} : {1}的意见", roleName, userName);
            }
            html += sugesstionString;
            html += "</td>";
            if (status)
            {
                string disableFileNum = "";
                if (!string.IsNullOrEmpty(FileNum))
                {
                    disableFileNum = "disabled=disabled";
                }

                html += "<td style=\"width: 70%\">";
                html += "<table style=\"width: 100%\"><tr>";
                html += "<td style=\"width: 10%\">备案号:</td><td ><input type=\"text\" id=\"FileNum\" value=" + FileNum + " " + disableFileNum + "></input></td></tr>";
                html += "<tr><td colspan=\"2\">";
                html += " <textarea style=\"height:60px;width:98%;border:solid 1px gray;\" id=\"" + idFlag + "\" " + disableFlag + ">" + suggestion + "</textarea>";
                html += "</td>";
                html += "</tr></table></td>";

            }
            else
            {
                html += "<td style=\"width: 70%\">";
                html += "&nbsp;";
                html += " <textarea style=\"height:60px;width:98%;border:solid 1px gray;\" id=\"" + idFlag + "\" " + disableFlag + ">" + suggestion + "</textarea>";
                html += "</td>";
            }
            html += "<td style=\"width:15%;\">";
            html += auditDate;
            html += "</td>";
            html += "</tr>";
            //审批
            if (string.IsNullOrEmpty(suggestion))
            {
                html += "<tr><td align=\"center\" colspan=\"2\">";
                html += "<input type=\"button\" id=\"AgreeButton\" style=\"background-color: #E8E8E8\" value=\"通过\" class=\"cls_btn_comm_w\"/>&nbsp;";
                html += "<input type=\"button\" id=\"DisAgreeButton\" style=\"background-color: #E8E8E8\" value=\"不通过\" class=\"cls_btn_comm_w\"/>&nbsp;";
                html += "<input type=\"button\" id=\"Button1\" style=\"background-color: #E8E8E8\" name=\"controlBtn\" class=\"cls_btn_comm_w\" value=\"返回\" onclick=\"javascript:history.back();\" />";
                html += " </td></tr>";
            }
            return html;
        }


        #region 绑定子项信息
        private void bindSubPlotInfo()
        {
            //取得子项信息
            TG.BLL.cm_ProjectPlotInfo bll = new TG.BLL.cm_ProjectPlotInfo();
            DataTable dt = bll.GetSubPlotInfo(ProSysNo).Tables[0];

            StringBuilder sb = new StringBuilder();

            sb.Append("<table class=\"show_projectPlot show_projectPlot_Input\" style=\"width: 100%;\" id=\"tbSunPlotInfo\">");

            //建筑 标准长       
            int jzA0CountOne = 0;
            //建筑 1/4倍长            
            int jzA0CountTwo = 0;
            //建筑 2/4倍长
            int jzA0CountThree = 0;
            //建筑 3/4倍长
            int jzA0CountFour = 0;
            //建筑 A0倍长
            int jzA0CountFive = 0;
            //折合1#图
            decimal jzA0CountSix = 0;
            getPlotSuInfo(dt, "建筑", "A0", out jzA0CountOne, out jzA0CountTwo, out jzA0CountThree, out jzA0CountFour, out jzA0CountFive, out jzA0CountSix);


            int jzA0Count = jzA0CountOne + jzA0CountTwo + jzA0CountThree + jzA0CountFour + jzA0CountFive;


            //建筑 标准长       
            int jzA1CountOne = 0;
            //建筑 1/4倍长            
            int jzA1CountTwo = 0;
            //建筑 2/4倍长
            int jzA1CountThree = 0;
            //建筑 3/4倍长
            int jzA1CountFour = 0;
            //建筑 A0倍长
            int jzA1CountFive = 0;
            //折合1#图
            decimal jzA1CountSix = 0;
            getPlotSuInfo(dt, "建筑", "A1", out jzA1CountOne, out jzA1CountTwo, out jzA1CountThree, out jzA1CountFour, out jzA1CountFive, out jzA1CountSix);

            int jzA1Count = jzA1CountOne + jzA1CountTwo + jzA1CountThree + jzA1CountFour + jzA1CountFive;

            //建筑 标准长       
            int jzA2CountOne = 0;
            //建筑 1/4倍长            
            int jzA2CountTwo = 0;
            //建筑 2/4倍长
            int jzA2CountThree = 0;
            //建筑 3/4倍长
            int jzA2CountFour = 0;
            //建筑 A0倍长
            int jzA2CountFive = 0;
            //折合1#图
            decimal jzA2CountSix = 0;
            getPlotSuInfo(dt, "建筑", "A2", out jzA2CountOne, out jzA2CountTwo, out jzA2CountThree, out jzA2CountFour, out jzA2CountFive, out jzA2CountSix);

            int jzA2Count = jzA2CountOne + jzA2CountTwo + jzA2CountThree + jzA2CountFour + jzA2CountFive;


            //建筑 标准长       
            int jzA3CountOne = 0;
            //建筑 1/4倍长            
            int jzA3CountTwo = 0;
            //建筑 2/4倍长
            int jzA3CountThree = 0;
            //建筑 3/4倍长
            int jzA3CountFour = 0;
            //建筑 A0倍长
            int jzA3CountFive = 0;
            //折合1#图
            decimal jzA3CountSix = 0;
            getPlotSuInfo(dt, "建筑", "A3", out jzA3CountOne, out jzA3CountTwo, out jzA3CountThree, out jzA3CountFour, out jzA3CountFive, out jzA3CountSix);

            int jzA3Count = jzA3CountOne + jzA3CountTwo + jzA3CountThree + jzA3CountFour + jzA3CountFive;

            //建筑 标准长       
            int jzA4CountOne = 0;
            //建筑 1/4倍长            
            int jzA4CountTwo = 0;
            //建筑 2/4倍长
            int jzA4CountThree = 0;
            //建筑 3/4倍长
            int jzA4CountFour = 0;
            //建筑 A0倍长
            int jzA4CountFive = 0;
            //折合1#图
            decimal jzA4CountSix = 0;
            getPlotSuInfo(dt, "建筑", "A4", out jzA4CountOne, out jzA4CountTwo, out jzA4CountThree, out jzA4CountFour, out jzA4CountFive, out jzA4CountSix);

            int jzA4Count = jzA4CountOne + jzA4CountTwo + jzA4CountThree + jzA4CountFour + jzA4CountFive;

            int rowspanCount = 0;
            if (jzA0Count != 0)
            {
                rowspanCount = rowspanCount + 1;
            }
            if (jzA1Count != 0)
            {
                rowspanCount = rowspanCount + 1;
            }
            if (jzA2Count != 0)
            {
                rowspanCount = rowspanCount + 1;
            }
            if (jzA3Count != 0)
            {
                rowspanCount = rowspanCount + 1;
            }
            if (jzA4Count != 0)
            {
                rowspanCount = rowspanCount + 1;
            }
            if (jzA0Count == 0 && jzA1Count == 0 && jzA2Count == 0 && jzA3Count == 0 && jzA4Count == 0)
            {
                rowspanCount = 1;
            }



            //建筑没有图纸
            if ((jzA0Count + jzA1Count + jzA2Count + jzA3Count + jzA4Count) == 0)
            {
                sb.Append("<tr> <td rowspan=" + rowspanCount + " style=\"width: 10%;\">建筑</td>");
                sb.Append("<td style=\"width: 15%;\"> <span style=\"font-weight: bold;\">0</span> (张)</td>");
                sb.Append("<td style=\"width: 15%;\"> <span style=\"font-weight: bold;\">0</span> (张)</td>");
                sb.Append("<td style=\"width: 15%;\"> <span style=\"font-weight: bold;\"> 0</span> (张)</td>");
                sb.Append(" <td style=\"width: 15%;\"> <span style=\"font-weight: bold;\">0</span> (张)</td>");
                sb.Append("<td style=\"width: 15%;\">  <span style=\"font-weight: bold;\">0</span> (张)</td>");
                sb.Append("<td style=\"width: 15%;\" rowspan=" + rowspanCount + "><span style=\"font-weight: bold;\"> 0</span>(张) </td>");
                sb.Append("</tr>");

            }

            //建筑A0有图纸
            if (jzA0Count != 0)
            {
                sb.Append("<tr> <td rowspan=" + rowspanCount + " style=\"width: 10%;\">建筑</td>");
                sb.Append("<td style=\"width: 15%;\"> <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA0CountOne + " </span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\"> <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA0CountTwo + " </span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\"> <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA0CountThree + " </span>(张) </td>");
                sb.Append(" <td style=\"width: 15%;\"> <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA0CountFour + "</span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\">  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA0CountFive + " </span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\" rowspan=" + rowspanCount + ">  <span style=\"font-weight: bold;\">" + jzA0CountSix + " </span>(张)  </td>");
                sb.Append("</tr>");
            }
            //建筑A1有图纸
            if (jzA1Count != 0)
            {
                if (jzA0Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + rowspanCount + " style=\"width: 10%;\">建筑</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }
                sb.Append(" <td style=\"width: 15%;\"> <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA1CountOne + "</span>(张)  </td>");
                sb.Append(" <td style=\"width: 15%;\"> <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA1CountTwo + " </span>(张) </td>");
                sb.Append(" <td style=\"width: 15%;\"> <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA1CountThree + "</span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\"> <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA1CountFour + " </span>(张)  </td>");
                sb.Append("<td style=\"width: 15%;\"> <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\"> " + jzA1CountFive + "  </span>(张) </td>");

                if (jzA0Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + rowspanCount + ">  <span style=\"font-weight: bold;\">" + jzA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td> ");
                }
                sb.Append("  </tr>");
            }



            if (jzA2Count != 0)
            {
                if (jzA0Count == 0 && jzA1Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + rowspanCount + " style=\"width: 10%;\">建筑</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }
                sb.Append(" <td style=\"width: 15%;\"> <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA2CountOne + " </span>(张)  </td>");
                sb.Append(" <td style=\"width: 15%;\"> <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA2CountTwo + " </span>(张) </td>");
                sb.Append(" <td style=\"width: 15%;\"> <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA2CountThree + " </span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\"> <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA2CountFour + " </span>(张)  </td>");
                sb.Append("<td style=\"width: 15%;\"> <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA2CountFive + " </span>(张) </td>");
                if (jzA0Count == 0 && jzA1Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + rowspanCount + ">  <span style=\"font-weight: bold;\">" + jzA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td> ");
                }
                sb.Append("  </tr>");
            }



            if (jzA3Count != 0)
            {
                if (jzA0Count == 0 && jzA1Count == 0 && jzA2Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + rowspanCount + " style=\"width: 10%;\">建筑</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }
                sb.Append(" <td style=\"width: 15%;\"> <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA3CountOne + " </span>(张)  </td>");
                sb.Append(" <td style=\"width: 15%;\"> <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA3CountTwo + " </span>(张) </td>");
                sb.Append(" <td style=\"width: 15%;\"> <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA3CountThree + "</span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\"> <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA3CountFour + "</span>(张)  </td>");
                sb.Append("<td style=\"width: 15%;\"> <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA3CountFive + " </span>(张) </td>");
                if (jzA0Count == 0 && jzA1Count == 0 && jzA2Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + rowspanCount + ">  <span style=\"font-weight: bold;\">" + jzA0CountSix + " </span>(张)  </td>");

                }
                else
                {
                    sb.Append(" <td class=\"display\"></td> ");
                }
                sb.Append("  </tr>");

            }
            if (jzA4Count != 0)
            {
                if (jzA0Count == 0 && jzA1Count == 0 && jzA2Count == 0 && jzA3Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + rowspanCount + " style=\"width: 10%;\">建筑</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }

                sb.Append(" <td style=\"width: 15%;\"> <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA4CountOne + " </span>(张)  </td>");
                sb.Append(" <td style=\"width: 15%;\"> <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA4CountTwo + " </span>(张) </td>");
                sb.Append(" <td style=\"width: 15%;\"> <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA4CountThree + " </span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\"> <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA4CountFour + " </span>(张)  </td>");
                sb.Append("<td style=\"width: 15%;\"> <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + jzA4CountFive + " </span>(张) </td>");

                if (jzA0Count == 0 && jzA1Count == 0 && jzA2Count == 0 && jzA3Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + rowspanCount + ">  <span style=\"font-weight: bold;\">" + jzA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append("<td class=\"display\"></td> ");
                }
                sb.Append("  </tr>");

            }


            //结构 标准长       
            int jgA0CountOne = 0;
            //结构 1/4倍长            
            int jgA0CountTwo = 0;
            //结构 2/4倍长
            int jgA0CountThree = 0;
            //结构 3/4倍长
            int jgA0CountFour = 0;
            //结构 A0倍长
            int jgA0CountFive = 0;
            //折合1#图
            decimal jgA0CountSix = 0;
            getPlotSuInfo(dt, "结构", "A0", out jgA0CountOne, out jgA0CountTwo, out jgA0CountThree, out jgA0CountFour, out jgA0CountFive, out jgA0CountSix);


            //结构 标准长       
            int jgA1CountOne = 0;
            //结构 1/4倍长            
            int jgA1CountTwo = 0;
            //结构 2/4倍长
            int jgA1CountThree = 0;
            //结构 3/4倍长
            int jgA1CountFour = 0;
            //结构 A0倍长
            int jgA1CountFive = 0;
            //折合1#图
            decimal jgA1CountSix = 0;
            getPlotSuInfo(dt, "结构", "A1", out jgA1CountOne, out jgA1CountTwo, out jgA1CountThree, out jgA1CountFour, out jgA1CountFive, out jgA1CountSix);


            //结构 标准长       
            int jgA2CountOne = 0;
            //结构 1/4倍长            
            int jgA2CountTwo = 0;
            //结构 2/4倍长
            int jgA2CountThree = 0;
            //结构 3/4倍长
            int jgA2CountFour = 0;
            //结构 A0倍长
            int jgA2CountFive = 0;
            //折合1#图
            decimal jgA2CountSix = 0;
            getPlotSuInfo(dt, "结构", "A2", out jgA2CountOne, out jgA2CountTwo, out jgA2CountThree, out jgA2CountFour, out jgA2CountFive, out jgA2CountSix);



            //结构 标准长       
            int jgA3CountOne = 0;
            //结构 1/4倍长            
            int jgA3CountTwo = 0;
            //结构 2/4倍长
            int jgA3CountThree = 0;
            //结构 3/4倍长
            int jgA3CountFour = 0;
            //结构 A0倍长
            int jgA3CountFive = 0;
            //折合1#图
            decimal jgA3CountSix = 0;
            getPlotSuInfo(dt, "结构", "A3", out jgA3CountOne, out jgA3CountTwo, out jgA3CountThree, out jgA3CountFour, out jgA3CountFive, out jgA3CountSix);

            //结构 标准长       
            int jgA4CountOne = 0;
            //结构 1/4倍长            
            int jgA4CountTwo = 0;
            //结构 2/4倍长
            int jgA4CountThree = 0;
            //结构 3/4倍长
            int jgA4CountFour = 0;
            //结构 A0倍长
            int jgA4CountFive = 0;
            //折合1#图
            decimal jgA4CountSix = 0;
            getPlotSuInfo(dt, "结构", "A4", out jgA4CountOne, out jgA4CountTwo, out jgA4CountThree, out jgA4CountFour, out jgA4CountFive, out jgA4CountSix);

            int jgA0Count = jgA0CountOne + jgA0CountTwo + jgA0CountThree + jgA0CountFour + jgA0CountFive;
            int jgA1Count = jgA1CountOne + jgA1CountTwo + jgA1CountThree + jgA1CountFour + jgA1CountFive;
            int jgA2Count = jgA2CountOne + jgA2CountTwo + jgA2CountThree + jgA2CountFour + jgA2CountFive;
            int jgA3Count = jgA3CountOne + jgA3CountTwo + jgA3CountThree + jgA3CountFour + jgA3CountFive;
            int jgA4Count = jgA4CountOne + jgA4CountTwo + jgA4CountThree + jgA4CountFour + jgA4CountFive;

            int jgrowspanCount = 0;
            if (jgA0Count != 0)
            {
                jgrowspanCount = jgrowspanCount + 1;
            }
            if (jgA1Count != 0)
            {
                jgrowspanCount = jgrowspanCount + 1;
            }
            if (jgA2Count != 0)
            {
                jgrowspanCount = jgrowspanCount + 1;
            }
            if (jgA3Count != 0)
            {
                jgrowspanCount = jgrowspanCount + 1;
            }
            if (jgA4Count != 0)
            {
                jgrowspanCount = jgrowspanCount + 1;
            }
            if (jgA0Count == 0 && jgA1Count == 0 && jgA2Count == 0 && jgA3Count == 0 && jgA4Count == 0)
            {
                jgrowspanCount = 1;
            }


            //结构 没有图纸
            if ((jgA0Count + jgA1Count + jgA2Count + jgA3Count + jgA4Count) == 0)
            {
                sb.Append("<tr> <td rowspan=" + jgrowspanCount + " style=\"width: 10%;\">结构</td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张)</td>");
                sb.Append(" <td > <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张)</td>");
                sb.Append("<td style=\"width: 15%;\" rowspan=" + jgrowspanCount + "> <span style=\"font-weight: bold;\">0</span>(张)  </td>");
                sb.Append("</tr>");

            }

            if (jgA0Count != 0)
            {
                sb.Append("<tr> <td rowspan=" + jgrowspanCount + " style=\"width: 10%;\">结构</td>");
                sb.Append("<td > <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA0CountOne + " </span>(张) </td>");
                sb.Append("<td > <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA0CountTwo + " </span>(张) </td>");
                sb.Append("<td > <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA0CountThree + "</span>(张) </td>");
                sb.Append(" <td > <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA0CountFour + " </span>(张) </td>");
                sb.Append("<td >  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA0CountFive + "</span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\" rowspan=" + jgrowspanCount + ">  <span style=\"font-weight: bold;\">" + jgA0CountSix + " </span>(张)  </td>");
                sb.Append("</tr>");
            }

            if (jgA1Count != 0)
            {
                if (jgA0Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + jgrowspanCount + " style=\"width: 10%;\">结构</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }

                sb.Append(" <td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA1CountOne + " </span>(张)  </td>");
                sb.Append(" <td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA1CountTwo + " </span>(张) </td>");
                sb.Append(" <td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA1CountThree + " </span>(张) </td>");
                sb.Append("<td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA1CountFour + "  </span>(张)  </td>");
                sb.Append("<td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA1CountFive + "  </span>(张) </td>");
                if (jgA0Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + jgrowspanCount + ">  <span style=\"font-weight: bold;\">" + jgA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td>  ");
                }
                sb.Append("  </tr>");
            }


            if (jgA2Count != 0)
            {
                if (jgA0Count == 0 && jgA1Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + jgrowspanCount + " style=\"width: 10%;\">结构</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }
                sb.Append(" <td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA2CountOne + " </span>(张)  </td>");
                sb.Append(" <td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA2CountTwo + "</span>(张) </td>");
                sb.Append(" <td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA2CountThree + " </span>(张) </td>");
                sb.Append("<td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA2CountFour + " </span>(张)  </td>");
                sb.Append("<td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA2CountFive + " </span>(张) </td>");
                if (jgA0Count == 0 && jgA1Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + jgrowspanCount + ">  <span style=\"font-weight: bold;\">" + jgA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td>");
                }
                sb.Append(" </tr>");

            }

            if (jgA3Count != 0)
            {
                if (jgA0Count == 0 && jgA1Count == 0 && jgA2Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + jgrowspanCount + " style=\"width: 10%;\">结构</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }
                sb.Append(" <td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA3CountOne + " </span>(张)  </td>");
                sb.Append(" <td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA3CountTwo + " </span>(张) </td>");
                sb.Append(" <td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA3CountThree + " </span>(张) </td>");
                sb.Append("<td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA3CountFour + " </span>(张)  </td>");
                sb.Append("<td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA3CountFive + "</span>(张) </td>");
                if (jgA0Count == 0 && jgA1Count == 0 && jgA2Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + jgrowspanCount + ">  <span style=\"font-weight: bold;\">" + jgA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td> ");
                }
                sb.Append("  </tr>");
            }

            if (jgA4Count != 0)
            {
                if (jgA0Count == 0 && jgA1Count == 0 && jgA2Count == 0 && jgA4Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + jgrowspanCount + " style=\"width: 10%;\">结构</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }
                sb.Append(" <td > <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA4CountOne + " </span>(张)  </td>");
                sb.Append(" <td > <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA4CountTwo + " </span>(张) </td>");
                sb.Append(" <td > <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA4CountThree + "</span>(张) </td>");
                sb.Append("<td > <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + jgA4CountFour + " </span>(张)  </td>");
                sb.Append("<td style=\"border-top: 0px;\"> <span>A4</span>  &nbsp;<span style=\"font-weight: bold;\">" + jgA4CountFive + " </span>(张) </td>");
                if (jgA0Count == 0 && jgA1Count == 0 && jgA2Count == 0 && jgA4Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + jgrowspanCount + ">  <span style=\"font-weight: bold;\">" + jgA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td>  ");
                }
                sb.Append("  </tr>");
            }

            //给排水 标准长       
            int gpsA0CountOne = 0;
            //给排水 1/4倍长            
            int gpsA0CountTwo = 0;
            //给排水 2/4倍长
            int gpsA0CountThree = 0;
            //给排水 3/4倍长
            int gpsA0CountFour = 0;
            //给排水 A0倍长
            int gpsA0CountFive = 0;
            //折合1#图
            decimal gpsA0CountSix = 0;
            getPlotSuInfo(dt, "给排水", "A0", out gpsA0CountOne, out gpsA0CountTwo, out gpsA0CountThree, out gpsA0CountFour, out gpsA0CountFive, out gpsA0CountSix);

            //给排水 标准长       
            int gpsA1CountOne = 0;
            //给排水 1/4倍长            
            int gpsA1CountTwo = 0;
            //给排水 2/4倍长
            int gpsA1CountThree = 0;
            //给排水 3/4倍长
            int gpsA1CountFour = 0;
            //给排水 A0倍长
            int gpsA1CountFive = 0;
            //折合1#图
            decimal gpsA1CountSix = 0;
            getPlotSuInfo(dt, "给排水", "A1", out gpsA1CountOne, out gpsA1CountTwo, out gpsA1CountThree, out gpsA1CountFour, out gpsA1CountFive, out gpsA1CountSix);


            //给排水 标准长       
            int gpsA2CountOne = 0;
            //给排水 1/4倍长            
            int gpsA2CountTwo = 0;
            //给排水 2/4倍长
            int gpsA2CountThree = 0;
            //给排水 3/4倍长
            int gpsA2CountFour = 0;
            //给排水 A0倍长
            int gpsA2CountFive = 0;
            //折合1#图
            decimal gpsA2CountSix = 0;
            getPlotSuInfo(dt, "给排水", "A2", out gpsA2CountOne, out gpsA2CountTwo, out gpsA2CountThree, out gpsA2CountFour, out gpsA2CountFive, out gpsA2CountSix);


            //给排水 标准长       
            int gpsA3CountOne = 0;
            //给排水 1/4倍长            
            int gpsA3CountTwo = 0;
            //给排水 2/4倍长
            int gpsA3CountThree = 0;
            //给排水 3/4倍长
            int gpsA3CountFour = 0;
            //给排水 A0倍长
            int gpsA3CountFive = 0;
            //折合1#图
            decimal gpsA3CountSix = 0;
            getPlotSuInfo(dt, "给排水", "A3", out gpsA3CountOne, out gpsA3CountTwo, out gpsA3CountThree, out gpsA3CountFour, out gpsA3CountFive, out gpsA3CountSix);


            //给排水 标准长       
            int gpsA4CountOne = 0;
            //给排水 1/4倍长            
            int gpsA4CountTwo = 0;
            //给排水 2/4倍长
            int gpsA4CountThree = 0;
            //给排水 3/4倍长
            int gpsA4CountFour = 0;
            //给排水 A0倍长
            int gpsA4CountFive = 0;
            //折合1#图
            decimal gpsA4CountSix = 0;
            getPlotSuInfo(dt, "给排水", "A4", out gpsA4CountOne, out gpsA4CountTwo, out gpsA4CountThree, out gpsA4CountFour, out gpsA4CountFive, out gpsA4CountSix);

            int gpsA0Count = gpsA0CountOne + gpsA0CountTwo + gpsA0CountThree + gpsA0CountFour + gpsA0CountFive;
            int gpsA1Count = gpsA1CountOne + gpsA1CountTwo + gpsA1CountThree + gpsA1CountFour + gpsA1CountFive;
            int gpsA2Count = gpsA2CountOne + gpsA2CountTwo + gpsA2CountThree + gpsA2CountFour + gpsA2CountFive;
            int gpsA3Count = gpsA3CountOne + gpsA3CountTwo + gpsA3CountThree + gpsA3CountFour + gpsA3CountFive;
            int gpsA4Count = gpsA4CountOne + gpsA4CountTwo + gpsA4CountThree + gpsA4CountFour + gpsA4CountFive;

            int gpsrowspanCount = 0;
            if (gpsA0Count != 0)
            {
                gpsrowspanCount = gpsrowspanCount + 1;
            }

            if (gpsA1Count != 0)
            {
                gpsrowspanCount = gpsrowspanCount + 1;
            }

            if (gpsA2Count != 0)
            {
                gpsrowspanCount = gpsrowspanCount + 1;
            }

            if (gpsA3Count != 0)
            {
                gpsrowspanCount = gpsrowspanCount + 1;
            }

            if (gpsA4Count != 0)
            {
                gpsrowspanCount = gpsrowspanCount + 1;
            }
            else if (gpsA0Count == 0 && gpsA1Count == 0 && gpsA2Count == 0 && gpsA3Count == 0 && gpsA4Count == 0)
            {
                gpsrowspanCount = 1;
            }


            //给排水 没有图纸
            if ((gpsA0Count + gpsA1Count + gpsA2Count + gpsA3Count + gpsA4Count) == 0)
            {
                sb.Append("<tr> <td rowspan=" + gpsrowspanCount + " style=\"width: 10%;\">给排水</td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张)  </td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append(" <td > <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append("<td >  <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\" rowspan=" + gpsrowspanCount + "> <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append("</tr>");

            }

            if (gpsA0Count != 0)
            {
                sb.Append("<tr> <td rowspan=" + gpsrowspanCount + " style=\"width: 10%;\">给排水</td>");
                sb.Append("<td> <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA0CountOne + " </span>(张) </td>");
                sb.Append("<td> <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA0CountTwo + "</span>(张) </td>");
                sb.Append("<td> <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA0CountThree + " </span>(张) </td>");
                sb.Append("<td> <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA0CountFour + "</span>(张) </td>");
                sb.Append("<td> <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA0CountFive + "</span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\" rowspan=" + gpsrowspanCount + ">  <span style=\"font-weight: bold;\">" + gpsA0CountSix + " </span>(张)  </td>");
                sb.Append("</tr>");

            }
            if (gpsA1Count != 0)
            {
                if (gpsA0Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + gpsrowspanCount + " style=\"width: 10%;\">给排水</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }
                sb.Append(" <td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA1CountOne + "</span>(张)  </td>");
                sb.Append(" <td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA1CountTwo + "</span>(张) </td>");
                sb.Append(" <td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA1CountThree + " </span>(张) </td>");
                sb.Append("<td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA1CountFour + "  </span>(张)  </td>");
                sb.Append("<td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA1CountFive + "  </span>(张) </td>");
                if (gpsA0Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + gpsrowspanCount + ">  <span style=\"font-weight: bold;\">" + gpsA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td> ");
                }
                sb.Append(" </tr>");
            }


            if (gpsA2Count != 0)
            {
                if (gpsA0Count == 0 && gpsA1Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + gpsrowspanCount + " style=\"width: 10%;\">给排水</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }
                sb.Append(" <td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA2CountOne + "</span>(张)  </td>");
                sb.Append(" <td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA2CountTwo + " </span>(张) </td>");
                sb.Append(" <td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA2CountThree + " </span>(张) </td>");
                sb.Append("<td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA2CountFour + " </span>(张)  </td>");
                sb.Append("<td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA2CountFive + " </span>(张) </td>");
                if (gpsA0Count == 0 && gpsA1Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + gpsrowspanCount + ">  <span style=\"font-weight: bold;\">" + gpsA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td> ");
                }
                sb.Append(" </tr>");
            }


            if (gpsA3Count != 0)
            {
                if (gpsA0Count == 0 && gpsA1Count == 0 && gpsA2Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + gpsrowspanCount + " style=\"width: 10%;\">给排水</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }

                sb.Append(" <td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA3CountOne + " </span>(张)  </td>");
                sb.Append(" <td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA3CountTwo + " </span>(张) </td>");
                sb.Append(" <td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA3CountThree + " </span>(张) </td>");
                sb.Append("<td >  <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA3CountFour + " </span>(张)  </td>");
                sb.Append("<td >  <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA3CountFive + " </span>(张) </td>");
                if (gpsA0Count == 0 && gpsA1Count == 0 && gpsA2Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + gpsrowspanCount + ">  <span style=\"font-weight: bold;\">" + gpsA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td> ");
                }
                sb.Append(" </tr>");
            }

            if (gpsA4Count != 0)
            {
                if (gpsA0Count == 0 && gpsA1Count == 0 && gpsA2Count == 0 && gpsA3Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + gpsrowspanCount + " style=\"width: 10%;\">给排水</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }
                sb.Append(" <td > <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA4CountOne + " </span>(张)  </td>");
                sb.Append(" <td > <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA4CountTwo + " </span>(张) </td>");
                sb.Append(" <td > <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA4CountThree + " </span>(张) </td>");
                sb.Append("<td >  <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA4CountFour + " </span>(张)  </td>");
                sb.Append("<td style=\"border-top: 0px;\"><span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + gpsA4CountFive + " </span>(张) </td>");
                if (gpsA0Count == 0 && gpsA1Count == 0 && gpsA2Count == 0 && gpsA3Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + gpsrowspanCount + ">  <span style=\"font-weight: bold;\">" + gpsA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td> ");
                }
                sb.Append(" </tr>");
            }

            //暖通 标准长       
            int ntA0CountOne = 0;
            //暖通 1/4倍长            
            int ntA0CountTwo = 0;
            //暖通 2/4倍长
            int ntA0CountThree = 0;
            //暖通 3/4倍长
            int ntA0CountFour = 0;
            //暖通 A0倍长
            int ntA0CountFive = 0;
            //折合1#图
            decimal ntA0CountSix = 0;
            getPlotSuInfo(dt, "暖通", "A0", out ntA0CountOne, out ntA0CountTwo, out ntA0CountThree, out ntA0CountFour, out ntA0CountFive, out ntA0CountSix);


            //暖通 标准长       
            int ntA1CountOne = 0;
            //暖通 1/4倍长            
            int ntA1CountTwo = 0;
            //暖通 2/4倍长
            int ntA1CountThree = 0;
            //暖通 3/4倍长
            int ntA1CountFour = 0;
            //暖通 A0倍长
            int ntA1CountFive = 0;
            //折合1#图
            decimal ntA1CountSix = 0;
            getPlotSuInfo(dt, "暖通", "A1", out ntA1CountOne, out ntA1CountTwo, out ntA1CountThree, out ntA1CountFour, out ntA1CountFive, out ntA1CountSix);

            //暖通 标准长       
            int ntA2CountOne = 0;
            //暖通 1/4倍长            
            int ntA2CountTwo = 0;
            //暖通 2/4倍长
            int ntA2CountThree = 0;
            //暖通 3/4倍长
            int ntA2CountFour = 0;
            //暖通 A0倍长
            int ntA2CountFive = 0;
            //折合1#图
            decimal ntA2CountSix = 0;
            getPlotSuInfo(dt, "暖通", "A2", out ntA2CountOne, out ntA2CountTwo, out ntA2CountThree, out ntA2CountFour, out ntA2CountFive, out ntA2CountSix);

            //暖通 标准长       
            int ntA3CountOne = 0;
            //暖通 1/4倍长            
            int ntA3CountTwo = 0;
            //暖通 2/4倍长
            int ntA3CountThree = 0;
            //暖通 3/4倍长
            int ntA3CountFour = 0;
            //暖通 A0倍长
            int ntA3CountFive = 0;
            //折合1#图
            decimal ntA3CountSix = 0;
            getPlotSuInfo(dt, "暖通", "A3", out ntA3CountOne, out ntA3CountTwo, out ntA3CountThree, out ntA3CountFour, out ntA3CountFive, out ntA3CountSix);

            //暖通 标准长       
            int ntA4CountOne = 0;
            //暖通 1/4倍长            
            int ntA4CountTwo = 0;
            //暖通 2/4倍长
            int ntA4CountThree = 0;
            //暖通 3/4倍长
            int ntA4CountFour = 0;
            //暖通 A0倍长
            int ntA4CountFive = 0;
            //折合1#图
            decimal ntA4CountSix = 0;
            getPlotSuInfo(dt, "暖通", "A4", out ntA4CountOne, out ntA4CountTwo, out ntA4CountThree, out ntA4CountFour, out ntA4CountFive, out ntA4CountSix);


            int ntA0Count = ntA0CountOne + ntA0CountTwo + ntA0CountThree + ntA0CountFour + ntA0CountFive;
            int ntA1Count = ntA1CountOne + ntA1CountTwo + ntA1CountThree + ntA1CountFour + ntA1CountFive;
            int ntA2Count = ntA2CountOne + ntA2CountTwo + ntA2CountThree + ntA2CountFour + ntA2CountFive;
            int ntA3Count = ntA3CountOne + ntA3CountTwo + ntA3CountThree + ntA3CountFour + ntA3CountFive;
            int ntA4Count = ntA4CountOne + ntA4CountTwo + ntA4CountThree + ntA4CountFour + ntA4CountFive;

            int ntrowspanCount = 0;
            if (ntA0Count != 0)
            {
                ntrowspanCount = ntrowspanCount + 1;
            }
            if (ntA1Count != 0)
            {
                ntrowspanCount = ntrowspanCount + 1;
            }
            if (ntA2Count != 0)
            {
                ntrowspanCount = ntrowspanCount + 1;
            }
            if (ntA3Count != 0)
            {
                ntrowspanCount = ntrowspanCount + 1;
            }
            if (ntA4Count != 0)
            {
                ntrowspanCount = ntrowspanCount + 1;
            }
            else if (ntA0Count == 0 && ntA1Count == 0 && ntA2Count == 0 && ntA3Count == 0 && ntA4Count == 0)
            {
                ntrowspanCount = 1;
            }


            //暖通 没有图纸
            if ((ntA0Count + ntA1Count + ntA2Count + ntA3Count + ntA4Count) == 0)
            {
                sb.Append("<tr> <td rowspan=" + ntrowspanCount + " style=\"width: 10%;\">暖通</td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张)</td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append(" <td > <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append("<td >  <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\" rowspan=" + ntrowspanCount + "> <span style=\"font-weight: bold;\">0</span>(张)  </td>");
                sb.Append("</tr>");

            }

            if (ntA0Count != 0)
            {
                sb.Append("<tr> <td rowspan=" + ntrowspanCount + " style=\"width: 10%;\">暖通</td>");
                sb.Append("<td >  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA0CountOne + " </span>(张) </td>");
                sb.Append("<td >  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA0CountTwo + "</span>(张) </td>");
                sb.Append("<td >  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA0CountThree + " </span>(张) </td>");
                sb.Append(" <td>  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA0CountFour + " </span>(张) </td>");
                sb.Append("<td >  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA0CountFive + " </span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\" rowspan=" + ntrowspanCount + ">  <span style=\"font-weight: bold;\">" + ntA0CountSix + " </span>(张)  </td>");
                sb.Append("</tr>");
            }

            if (ntA1Count != 0)
            {
                if (ntA0Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + ntrowspanCount + " style=\"width: 10%;\">暖通</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }
                sb.Append(" <td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA1CountOne + " </span>(张)  </td>");
                sb.Append(" <td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA1CountTwo + "</span>(张) </td>");
                sb.Append(" <td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA1CountThree + " </span>(张) </td>");
                sb.Append("<td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA1CountFour + "  </span>(张)  </td>");
                sb.Append("<td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA1CountFive + " </span>(张) </td>");
                if (ntA0Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + ntrowspanCount + ">  <span style=\"font-weight: bold;\">" + ntA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td>  ");
                }
                sb.Append("</tr>");
            }

            if (ntA2Count != 0)
            {
                if (ntA0Count == 0 && ntA1Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + ntrowspanCount + " style=\"width: 10%;\">暖通</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }

                sb.Append(" <td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA2CountOne + "  </span>(张)  </td>");
                sb.Append(" <td > <span>A2</span>  &nbsp;  <span style=\"font-weight: bold;\">" + ntA2CountTwo + "  </span>(张) </td>");
                sb.Append(" <td > <span>A2</span>  &nbsp;  <span style=\"font-weight: bold;\">" + ntA2CountThree + "  </span>(张) </td>");
                sb.Append("<td>  <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA2CountFour + "  </span>(张)  </td>");
                sb.Append("<td>  <span>A2</span>  &nbsp;  <span style=\"font-weight: bold;\">" + ntA2CountFive + "  </span>(张) </td>");
                if (ntA0Count == 0 && ntA1Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + ntrowspanCount + ">  <span style=\"font-weight: bold;\">" + ntA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td>  ");
                }
                sb.Append("</tr>");
            }

            if (ntA3Count != 0)
            {
                if (ntA0Count == 0 && ntA1Count == 0 && ntA2Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + ntrowspanCount + " style=\"width: 10%;\">暖通</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }

                sb.Append(" <td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA3CountOne + "</span>(张)  </td>");
                sb.Append(" <td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA3CountTwo + " </span>(张) </td>");
                sb.Append(" <td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA3CountThree + " </span>(张) </td>");
                sb.Append("<td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA3CountFour + " </span>(张)  </td>");
                sb.Append("<td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA3CountFive + "</span>(张) </td>");
                if (ntA0Count == 0 && ntA1Count == 0 && ntA2Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + ntrowspanCount + ">  <span style=\"font-weight: bold;\">" + ntA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td>  ");
                }
                sb.Append("</tr>");
            }

            if (ntA4Count != 0)
            {
                if (ntA0Count == 0 && ntA1Count == 0 && ntA2Count == 0 && ntA3Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + ntrowspanCount + " style=\"width: 10%;\">暖通</td>");
                }
                else
                {
                    sb.Append(" <tr>");
                }

                sb.Append(" <td > <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA4CountOne + " </span>(张)  </td>");
                sb.Append(" <td > <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA4CountTwo + " </span>(张) </td>");
                sb.Append(" <td > <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA4CountThree + " </span>(张) </td>");
                sb.Append("<td >  <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + ntA4CountFour + " </span>(张)  </td>");
                sb.Append("<td style=\"border-top: 0px;\"><span>A4</span>  &nbsp;<span style=\"font-weight: bold;\">" + ntA4CountFive + " </span>(张) </td>");
                if (ntA0Count == 0 && ntA1Count == 0 && ntA2Count == 0 && ntA3Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + ntrowspanCount + ">  <span style=\"font-weight: bold;\">" + ntA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td>  ");
                }
                sb.Append("</tr>");
            }

            //电气 标准长       
            int dqA0CountOne = 0;
            //电气 1/4倍长            
            int dqA0CountTwo = 0;
            //电气 2/4倍长
            int dqA0CountThree = 0;
            //电气 3/4倍长
            int dqA0CountFour = 0;
            //电气 A0倍长
            int dqA0CountFive = 0;
            //折合1#图
            decimal dqA0CountSix = 0;
            getPlotSuInfo(dt, "电气", "A0", out dqA0CountOne, out dqA0CountTwo, out dqA0CountThree, out dqA0CountFour, out dqA0CountFive, out dqA0CountSix);

            //电气 标准长       
            int dqA1CountOne = 0;
            //电气 1/4倍长            
            int dqA1CountTwo = 0;
            //电气 2/4倍长
            int dqA1CountThree = 0;
            //电气 3/4倍长
            int dqA1CountFour = 0;
            //电气 A0倍长
            int dqA1CountFive = 0;
            //折合1#图
            decimal dqA1CountSix = 0;
            getPlotSuInfo(dt, "电气", "A1", out dqA1CountOne, out dqA1CountTwo, out dqA1CountThree, out dqA1CountFour, out dqA1CountFive, out dqA1CountSix);

            //电气 标准长       
            int dqA2CountOne = 0;
            //电气 1/4倍长            
            int dqA2CountTwo = 0;
            //电气 2/4倍长
            int dqA2CountThree = 0;
            //电气 3/4倍长
            int dqA2CountFour = 0;
            //电气 A0倍长
            int dqA2CountFive = 0;
            //折合1#图
            decimal dqA2CountSix = 0;
            getPlotSuInfo(dt, "电气", "A2", out dqA2CountOne, out dqA2CountTwo, out dqA2CountThree, out dqA2CountFour, out dqA2CountFive, out dqA2CountSix);

            //电气 标准长       
            int dqA3CountOne = 0;
            //电气 1/4倍长            
            int dqA3CountTwo = 0;
            //电气 2/4倍长
            int dqA3CountThree = 0;
            //电气 3/4倍长
            int dqA3CountFour = 0;
            //电气 A0倍长
            int dqA3CountFive = 0;
            //折合1#图
            decimal dqA3CountSix = 0;
            getPlotSuInfo(dt, "电气", "A3", out dqA3CountOne, out dqA3CountTwo, out dqA3CountThree, out dqA3CountFour, out dqA3CountFive, out dqA3CountSix);

            //电气 标准长       
            int dqA4CountOne = 0;
            //电气 1/4倍长            
            int dqA4CountTwo = 0;
            //电气 2/4倍长
            int dqA4CountThree = 0;
            //电气 3/4倍长
            int dqA4CountFour = 0;
            //电气 A0倍长
            int dqA4CountFive = 0;
            //折合1#图
            decimal dqA4CountSix = 0;
            getPlotSuInfo(dt, "电气", "A4", out dqA4CountOne, out dqA4CountTwo, out dqA4CountThree, out dqA4CountFour, out dqA4CountFive, out dqA4CountSix);


            int dqA0Count = dqA0CountOne + dqA0CountTwo + dqA0CountThree + dqA0CountFour + dqA0CountFive;
            int dqA1Count = dqA1CountOne + dqA1CountTwo + dqA1CountThree + dqA1CountFour + dqA1CountFive;
            int dqA2Count = dqA2CountOne + dqA2CountTwo + dqA2CountThree + dqA2CountFour + dqA2CountFive;
            int dqA3Count = dqA3CountOne + dqA3CountTwo + dqA3CountThree + dqA3CountFour + dqA3CountFive;
            int dqA4Count = dqA4CountOne + dqA4CountTwo + dqA4CountThree + dqA4CountFour + dqA4CountFive;

            int dqrowspanCount = 0;
            if (dqA0Count != 0)
            {
                dqrowspanCount = dqrowspanCount + 1;
            }
            if (dqA1Count != 0)
            {
                dqrowspanCount = dqrowspanCount + 1;
            }
            if (dqA2Count != 0)
            {
                dqrowspanCount = dqrowspanCount + 1;
            }
            if (dqA3Count != 0)
            {
                dqrowspanCount = dqrowspanCount + 1;
            }
            if (dqA4Count != 0)
            {
                dqrowspanCount = dqrowspanCount + 1;
            }
            if (dqA0Count == 0 && dqA1Count == 0 && dqA2Count == 0 && dqA3Count == 0 && dqA4Count == 0)
            {
                dqrowspanCount = 1;
            }


            //电气 没有图纸
            if ((dqA0Count + dqA1Count + dqA2Count + dqA3Count + dqA4Count) == 0)
            {
                sb.Append("<tr> <td rowspan=" + dqrowspanCount + " style=\"width: 10%;\">电气</td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张)  </td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append("<td > <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append(" <td > <span style=\"font-weight: bold;\">0</span>(张)</td>");
                sb.Append("<td >  <span style=\"font-weight: bold;\">0</span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\" rowspan=" + dqrowspanCount + "> <span style=\"font-weight: bold;\">0</span>(张)  </td>");
                sb.Append("</tr>");

            }

            if (dqA0Count != 0)
            {
                sb.Append("<tr> <td rowspan=" + dqrowspanCount + " style=\"width: 10%;\">电气</td>");
                sb.Append("<td >  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA0CountOne + " </span>(张) </td>");
                sb.Append("<td >  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA0CountTwo + "</span>(张) </td>");
                sb.Append("<td >  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA0CountThree + "</span>(张) </td>");
                sb.Append(" <td > <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA0CountFour + " </span>(张) </td>");
                sb.Append("<td >  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA0CountFive + " </span>(张) </td>");
                sb.Append("<td style=\"width: 15%;\" rowspan=" + dqrowspanCount + ">  <span style=\"font-weight: bold;\">" + dqA0CountSix + " </span>(张)  </td>");
                sb.Append("</tr>");
            }

            if (dqA1Count != 0)
            {
                if (dqA0Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + dqrowspanCount + " style=\"width: 10%;\">电气</td>");
                }
                else
                {
                    sb.Append("<tr> ");
                }
                sb.Append(" <td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA1CountOne + " </span>(张)  </td>");
                sb.Append(" <td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA1CountTwo + " </span>(张) </td>");
                sb.Append(" <td > <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA1CountThree + "</span>(张) </td>");
                sb.Append("<td >  <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA1CountFour + "  </span>(张)  </td>");
                sb.Append("<td >  <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA1CountFive + " </span>(张) </td>");
                if (dqA0Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + dqrowspanCount + ">  <span style=\"font-weight: bold;\">" + dqA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td> ");
                }
                sb.Append("  </tr>");
            }

            if (dqA2Count != 0)
            {
                if (dqA0Count == 0 && dqA1Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + dqrowspanCount + " style=\"width: 10%;\">电气</td>");
                }
                else
                {
                    sb.Append("<tr> ");
                }

                sb.Append(" <td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA2CountOne + " </span>(张)  </td>");
                sb.Append(" <td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA2CountTwo + "</span>(张) </td>");
                sb.Append(" <td > <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA2CountThree + "</span>(张) </td>");
                sb.Append("<td >  <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA2CountFour + " </span>(张)  </td>");
                sb.Append("<td >  <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA2CountFive + " </span>(张) </td>");
                if (dqA0Count == 0 && dqA1Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + dqrowspanCount + ">  <span style=\"font-weight: bold;\">" + dqA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td> ");
                }
                sb.Append("  </tr>");
            }


            if (dqA3Count != 0)
            {
                if (dqA0Count == 0 && dqA1Count == 0 && dqA2Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + dqrowspanCount + " style=\"width: 10%;\">电气</td>");
                }
                else
                {
                    sb.Append("<tr> ");
                }
                sb.Append(" <td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA3CountOne + " </span>(张)  </td>");
                sb.Append(" <td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA3CountTwo + "</span>(张) </td>");
                sb.Append(" <td > <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA3CountThree + " </span>(张) </td>");
                sb.Append("<td >  <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA3CountFour + " </span>(张)  </td>");
                sb.Append("<td >  <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA3CountFive + "</span>(张) </td>");
                if (dqA0Count == 0 && dqA1Count == 0 && dqA2Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + dqrowspanCount + ">  <span style=\"font-weight: bold;\">" + dqA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td> ");
                }
                sb.Append("  </tr>");
            }

            if (dqA4Count != 0)
            {
                if (dqA0Count == 0 && dqA1Count == 0 && dqA2Count == 0 && dqA3Count == 0)
                {
                    sb.Append("<tr> <td rowspan=" + dqrowspanCount + " style=\"width: 10%;\">电气</td>");
                }
                else
                {
                    sb.Append("<tr> ");
                }

                sb.Append(" <td> <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA4CountOne + "</span>(张)  </td>");
                sb.Append(" <td> <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA4CountTwo + "</span>(张) </td>");
                sb.Append(" <td> <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA4CountThree + " </span>(张) </td>");
                sb.Append("<td>  <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + dqA4CountFour + "</span>(张)  </td>");
                sb.Append("<td style=\"border-top: 0px;\"><span>A4</span>  &nbsp;<span style=\"font-weight: bold;\">" + dqA4CountFive + " </span>(张) </td>");
                if (dqA0Count == 0 && dqA1Count == 0 && dqA2Count == 0 && dqA3Count == 0)
                {
                    sb.Append("<td style=\"width: 15%;\" rowspan=" + dqrowspanCount + ">  <span style=\"font-weight: bold;\">" + dqA0CountSix + " </span>(张)  </td>");
                }
                else
                {
                    sb.Append(" <td class=\"display\"></td> ");
                }
                sb.Append("  </tr>");
            }

            // 标准长       
            int totalA0CountOne = 0;
            // 1/4倍长            
            int totalA0CountTwo = 0;
            // 2/4倍长
            int totalA0CountThree = 0;
            // 3/4倍长
            int totalA0CountFour = 0;
            // A0倍长
            int totalA0CountFive = 0;
            //折合1#图
            decimal totalA0CountSix = 0;

            totalA0CountOne = jzA0CountOne + jgA0CountOne + gpsA0CountOne + ntA0CountOne + dqA0CountOne;
            totalA0CountTwo = jzA0CountTwo + jgA0CountTwo + gpsA0CountTwo + ntA0CountTwo + dqA0CountTwo;
            totalA0CountThree = jzA0CountThree + jgA0CountThree + gpsA0CountThree + ntA0CountThree + dqA0CountThree;
            totalA0CountFour = jzA0CountFour + jgA0CountFour + gpsA0CountFour + ntA0CountFour + dqA0CountFour;
            totalA0CountFive = jzA0CountFive + jgA0CountFive + gpsA0CountFive + ntA0CountFive + dqA0CountFive;
            totalA0CountSix = jzA0CountSix + jgA0CountSix + gpsA0CountSix + ntA0CountSix + dqA0CountSix;

            sb.Append(" <tr> <td rowspan=\"5\"> 合计 </td>");
            sb.Append(" <td style=\"border-bottom: 0px;\"> <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA0CountOne + " </span>(张)  </td>");
            sb.Append("<td style=\"border-bottom: 0px;\">  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA0CountTwo + " </span>(张)</td>");
            sb.Append(" <td style=\"border-bottom: 0px;\"> <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA0CountThree + " </span>(张) </td>");
            sb.Append("<td style=\"border-bottom: 0px;\">  <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA0CountFour + " </span>(张)  </td>");
            sb.Append(" <td style=\"border-bottom: 0px;\"> <span>A0</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA0CountFive + " </span>(张)  </td>");
            sb.Append(" <td rowspan=\"5\"> <span style=\"font-weight: bold;\">" + totalA0CountSix + " </span>(张)  </td>");
            sb.Append("</tr>");


            // 标准长       
            int totalA1CountOne = 0;
            // 1/4倍长            
            int totalA1CountTwo = 0;
            // 2/4倍长
            int totalA1CountThree = 0;
            // 3/4倍长
            int totalA1CountFour = 0;
            // A1倍长
            int totalA1CountFive = 0;

            totalA1CountOne = jzA1CountOne + jgA1CountOne + gpsA1CountOne + ntA1CountOne + dqA1CountOne;
            totalA1CountTwo = jzA1CountTwo + jgA1CountTwo + gpsA1CountTwo + ntA1CountTwo + dqA1CountTwo;
            totalA1CountThree = jzA1CountThree + jgA1CountThree + gpsA1CountThree + ntA1CountThree + dqA1CountThree;
            totalA1CountFour = jzA1CountFour + jgA1CountFour + gpsA1CountFour + ntA1CountFour + dqA1CountFour;
            totalA1CountFive = jzA1CountFive + jgA1CountFive + gpsA1CountFive + ntA1CountFive + dqA1CountFive;

            sb.Append(" <tr><td class=\"display\"></td>");
            sb.Append(" <td >  <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA1CountOne + " </span>(张) </td>");
            sb.Append(" <td >  <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA1CountTwo + " </span>(张) </td>");
            sb.Append(" <td >  <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA1CountThree + " </span>(张) </td>");
            sb.Append(" <td >  <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA1CountFour + " </span>(张) </td>");
            sb.Append(" <td >  <span>A1</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA1CountFive + " </span>(张) </td>");
            sb.Append(" <td class=\"display\"> </td> </tr>");

            // 标准长       
            int totalA2CountOne = 0;
            // 1/4倍长            
            int totalA2CountTwo = 0;
            // 2/4倍长
            int totalA2CountThree = 0;
            // 3/4倍长
            int totalA2CountFour = 0;
            // A2倍长
            int totalA2CountFive = 0;

            totalA2CountOne = jzA2CountOne + jgA2CountOne + gpsA2CountOne + ntA2CountOne + dqA2CountOne;
            totalA2CountTwo = jzA2CountTwo + jgA2CountTwo + gpsA2CountTwo + ntA2CountTwo + dqA2CountTwo;
            totalA2CountThree = jzA2CountThree + jgA2CountThree + gpsA2CountThree + ntA2CountThree + dqA2CountThree;
            totalA2CountFour = jzA2CountFour + jgA2CountFour + gpsA2CountFour + ntA2CountFour + dqA2CountFour;
            totalA2CountFive = jzA2CountFive + jgA2CountFive + gpsA2CountFive + ntA2CountFive + dqA2CountFive;

            sb.Append(" <tr><td class=\"display\"></td>");
            sb.Append(" <td >  <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA2CountOne + " </span>(张) </td>");
            sb.Append(" <td >  <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA2CountTwo + " </span>(张) </td>");
            sb.Append(" <td >  <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA2CountThree + " </span>(张) </td>");
            sb.Append(" <td >  <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA2CountFour + " </span>(张) </td>");
            sb.Append(" <td >  <span>A2</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA2CountFive + " </span>(张) </td>");
            sb.Append(" <td class=\"display\"> </td> </tr>");

            // 标准长       
            int totalA3CountOne = 0;
            // 1/4倍长            
            int totalA3CountTwo = 0;
            // 2/4倍长
            int totalA3CountThree = 0;
            // 3/4倍长
            int totalA3CountFour = 0;
            // A3倍长
            int totalA3CountFive = 0;

            totalA3CountOne = jzA3CountOne + jgA3CountOne + gpsA3CountOne + ntA3CountOne + dqA3CountOne;
            totalA3CountTwo = jzA3CountTwo + jgA3CountTwo + gpsA3CountTwo + ntA3CountTwo + dqA3CountTwo;
            totalA3CountThree = jzA3CountThree + jgA3CountThree + gpsA3CountThree + ntA3CountThree + dqA3CountThree;
            totalA3CountFour = jzA3CountFour + jgA3CountFour + gpsA3CountFour + ntA3CountFour + dqA3CountFour;
            totalA3CountFive = jzA3CountFive + jgA3CountFive + gpsA3CountFive + ntA3CountFive + dqA3CountFive;

            sb.Append(" <tr><td class=\"display\"></td>");
            sb.Append(" <td >  <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA3CountOne + " </span>(张) </td>");
            sb.Append(" <td >  <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA3CountTwo + " </span>(张) </td>");
            sb.Append(" <td >  <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA3CountThree + " </span>(张) </td>");
            sb.Append(" <td >  <span>A3</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA3CountFour + " </span>(张) </td>");
            sb.Append(" <td >  <span>A3</span>  &nbsp;<span style=\"font-weight: bold;\">" + totalA3CountFive + " </span>(张) </td>");
            sb.Append(" <td class=\"display\"> </td> </tr>");

            // 标准长       
            int totalA4CountOne = 0;
            // 1/4倍长            
            int totalA4CountTwo = 0;
            // 2/4倍长
            int totalA4CountThree = 0;
            // 3/4倍长
            int totalA4CountFour = 0;
            // A4倍长
            int totalA4CountFive = 0;

            totalA4CountOne = jzA4CountOne + jgA4CountOne + gpsA4CountOne + ntA4CountOne + dqA4CountOne;
            totalA4CountTwo = jzA4CountTwo + jgA4CountTwo + gpsA4CountTwo + ntA4CountTwo + dqA4CountTwo;
            totalA4CountThree = jzA4CountThree + jgA4CountThree + gpsA4CountThree + ntA4CountThree + dqA4CountThree;
            totalA4CountFour = jzA4CountFour + jgA4CountFour + gpsA4CountFour + ntA4CountFour + dqA4CountFour;
            totalA4CountFive = jzA4CountFive + jgA4CountFive + gpsA4CountFive + ntA4CountFive + dqA4CountFive;
            sb.Append(" <tr><td class=\"display\"></td>");
            sb.Append(" <td >  <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA4CountOne + " </span>(张) </td>");
            sb.Append(" <td >  <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA4CountTwo + " </span>(张) </td>");
            sb.Append(" <td >  <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA4CountThree + " </span>(张) </td>");
            sb.Append(" <td >  <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA4CountFour + " </span>(张) </td>");
            sb.Append(" <td >  <span>A4</span>  &nbsp; <span style=\"font-weight: bold;\">" + totalA4CountFive + " </span>(张) </td>");
            sb.Append(" <td class=\"display\"> </td> </tr>");
            sb.Append("</table>");
            lblSubPlot.Text = sb.ToString();

        }

        private void getPlotSuInfo(DataTable dt, string sepName, string mapSheet, out int jzCountOne, out int jzCountTwo, out int jzCountThree, out int jzCountFour, out int jzCountFive, out decimal jzCountSix)
        {

            DataTable dtJZOne = new DataView(dt) { RowFilter = "  Specialty  like '%" + sepName + "%' And  Mapsheet like '%" + mapSheet + "%'" }.ToTable();

            if (dtJZOne != null && dtJZOne.Rows.Count > 0)
            {
                jzCountOne = dtJZOne.AsEnumerable().Sum(s => s.Field<int>("StandardCount"));
                jzCountTwo = dtJZOne.AsEnumerable().Sum(s => s.Field<int>("QuarterCount"));
                jzCountThree = dtJZOne.AsEnumerable().Sum(s => s.Field<int>("SecondQuarterCount"));
                jzCountFour = dtJZOne.AsEnumerable().Sum(s => s.Field<int>("ThreeQuarterCount"));
                jzCountFive = dtJZOne.AsEnumerable().Sum(s => s.Field<int>("FourQuarterCount"));
                jzCountSix = dtJZOne.AsEnumerable().Max(s => s.Field<decimal>("TotalCount"));
            }
            else
            {
                jzCountOne = 0;
                jzCountTwo = 0;
                jzCountThree = 0;
                jzCountFour = 0;
                jzCountFive = 0;
                jzCountSix = 0;
            }

        }

        #endregion
    }
}