﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintProjectPlotInfo.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.PrintProjectPlotInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        .cls_content_head
        {
            width: 100%;
            height: 25px; /*margin: 0 auto;
            border: solid 1px black;*/
            border-collapse: collapse;
            font-size: 12px;
            font-family: "微软雅黑";
        }
        
        .cls_content_head td
        {
            border: solid 1px black;
            font-size: 12px;
            font-family: "微软雅黑";
        }
        
        .cls_content_head tr
        {
            height: 25px;
        }
        
        #rbtlist td
        {
            border: 0px;
        }
        .display
        {
            display: none;
        }
    </style>
    <style type="text/css" media="print">
        .NoPrint
        {
            display: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin: 0 auto; width: 90%;">
        <table style="width: 100%;">
            <tr>
                <td align="center" style="font-family: 微软雅黑; font-size: 16px;">
                    甘&nbsp;肃&nbsp;省&nbsp;建&nbsp;筑&nbsp;设&nbsp;计&nbsp;研&nbsp;究&nbsp;院
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td align="center" style="font-family: 微软雅黑; font-size: 16px;">
                    工程设计出图卡
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td align="center" style="font-family: 微软雅黑; font-size: 16px;">
                    QR-7.2-05
                </td>
            </tr>
        </table>
        <table class="cls_content_head" align="center">
            <tr>
                <td style="width: 10%;">
                    项目名称
                </td>
                <td colspan="3" style="width: 51%;">
                    <asp:Label ID="txt_proname" runat="server"></asp:Label>
                </td>
                <td style="width: 10%;">
                    工程号
                </td>
                <td style="width: 29%;">
                    <asp:Label ID="txt_pronumber" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    子项名称
                </td>
                <td colspan="3">
                    <asp:Label ID="txt_proitemname" runat="server"></asp:Label>
                </td>
                <td>
                    设计阶段
                </td>
                <td>
                    <asp:Label ID="txt_prostatus" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    管理级别
                </td>
                <td colspan="3">
                    <asp:Label ID="lbl_level" runat="server"></asp:Label>
                </td>
                <td>
                    设计部门
                </td>
                <td>
                    <asp:Label ID="txt_prounit" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    审核级别
                </td>
                <td colspan="3">
                    <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                </td>
                <td>
                    设&nbsp;&nbsp;总
                </td>
                <td>
                    <asp:Label ID="lbl_PMName" runat="server" Width="150px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="font-weight: bold;">
                    出图类别
                </td>
                <td colspan="5" style="font-weight: bold;">
                    <asp:RadioButtonList ID="rbtlist" runat="server" RepeatDirection="Horizontal" Enabled="true">
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    生产经营部<br />
                    ( 签字、盖章)
                </td>
                <td style="width: 17%">
                </td>
                <td style="width: 17%">
                    技术质量部<br />
                    ( 签字、盖章)
                </td>
                <td style="width: 17%">
                </td>
                <td>
                    技术质量部<br />
                    备&nbsp;案&nbsp;号
                </td>
                <td>
                    <asp:Label ID="lblNum" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td style="height: 10px;">
                </td>
            </tr>
        </table>
        <table class="cls_content_head" align="center">
            <tr>
                <td style="width: 10%;">
                    晒图份数
                </td>
                <td colspan="6" style="width: 90%;" align="left">
                    <asp:Label ID="lbl_BlueprintCounts" runat="server" Width="150px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 10%;" align="center">
                    专业
                </td>
                <td style="width: 15%;" align="center">
                    标准长
                </td>
                <td style="width: 15%" align="center">
                    加1/4倍长
                </td>
                <td style="width: 15%;" align="center">
                    加2/4倍长
                </td>
                <td style="width: 15%" align="center">
                    加3/4倍长
                </td>
                <td style="width: 15%;" align="center">
                    加1倍长
                </td>
                <td style="width: 15%;" align="center">
                    折合1#图
                </td>
            </tr>
            <asp:Label ID="lblSubPlot" runat="server" Text="Label"></asp:Label>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td style="height: 10px;">
                </td>
            </tr>
        </table>
        <table class="cls_content_head" align="center">
            <tr>
                <td colspan="2" align="center" style="width: 50%;">
                    设计特殊输出批准
                </td>
                <td colspan="2" align="center" style="width: 50%;">
                    结构专业施工图延迟校审批准
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    原因：
                    <br />
                    <br />
                    <br />
                    所长：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;日期：
                </td>
                <td colspan="2">
                    原因：
                    <br />
                    <br />
                    <br />
                    所长：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;日期：
                </td>
            </tr>
            <tr>
                <td style="width: 25%">
                    所总建筑师：
                    <br />
                    <br />
                    <br />
                    日期:
                </td>
                <td style="width: 25%">
                    所总工程师：
                    <br />
                    <br />
                    <br />
                    日期：
                </td>
                <td style="width: 25%">
                    所总建筑师：
                    <br />
                    <br />
                    <br />
                    日期:
                </td>
                <td style="width: 25%">
                    所总工程师：
                    <br />
                    <br />
                    <br />
                    日期：
                </td>
            </tr>
            <tr>
                <td style="width: 25%">
                    主管生产经营院领导<br />
                    审批意见
                    <br />
                    <br />
                    签字:
                    <br />
                    <br />
                    日期:
                </td>
                <td style="width: 25%">
                    主管技术质量部院领导<br />
                    审批意见
                    <br />
                    <br />
                    签字:
                    <br />
                    <br />
                    日期:
                </td>
                <td style="width: 25%">
                    主管生产经营院领导<br />
                    审批意见
                    <br />
                    <br />
                    签字:
                    <br />
                    <br />
                    日期:
                </td>
                <td style="width: 25%">
                    主管技术质量部院领导<br />
                    审批意见
                    <br />
                    <br />
                    签字:
                    <br />
                    <br />
                    日期:
                </td>
            </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td style="font-size: 12px; font-family: 微软雅黑;">
                    注：所有项目均填写上表和中表，特殊输出的项目在下表左侧审批，延迟校审的项目在下表右侧审批。
                </td>
            </tr>
        </table>
        <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="Table1">
            <tr>
                <td style="width: 100%; text-align: center;">
                    <input id="print" type="button" onclick="window.print()" value="打印" class="NoPrint"
                        style="width: 75px; height: 23px; cursor: pointer;" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
