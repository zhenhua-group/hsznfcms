﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModifyProjectFileInfo.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.ModifyProjectFileInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="/css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="/css/jquery.alerts.css" rel="Stylesheet" type="text/css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
    <link href="/css/swfupload/default_cpr.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
    <script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.alerts.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery.chromatable.js"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script src="/js/UserControl/ChooseUser.js" type="text/javascript"></script>
    <script src="../../js/ProjectMamage/ProImageAudit/AddProjectFileInfo.js" type="text/javascript"></script>
    <style type="text/css">
        .input
        {
            width: 98%;
            height: 98%;
            font-size: 12px;
            font-style: normal;
            font-family: 宋体;
            white-space: nowrap;
            border: 1px solid #CCC;
        }
    </style>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                当前位置：[项目信息-修改工程设计归档资料]
            </td>
        </tr>
        <tr>
            <td class="cls_content" align="center" valign="top">
                <table class="cls_content_2">
                    <tr>
                        <td class="cls_head_2">
                            <div class="cls_1">
                                项目信息
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben" id="projectInfo">
        <tr>
            <td style="width: 15%">
                工程名称：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_proname" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                工程号:
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_pronumber" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                设计部门：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_prounit" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                设计阶段：
            </td>
            <td style="width: 35%">
                <asp:Label ID="txt_prostatus" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                管理级别：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_level" runat="server"></asp:Label>
            </td>
            <td style="width: 15%">
                审核级别：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                设&nbsp;&nbsp;总：
            </td>
            <td style="width: 35%">
                <asp:Label ID="lbl_PMName" runat="server" Width="150px"></asp:Label>
            </td>
            <td style="width: 15%">
            </td>
            <td style="width: 35%">
            </td>
        </tr>
    </table>
    <table class="cls_show_cst_jiben">
        <tr>
            <td class="cls_head_2">
                <div class="cls_1">
                    工程设计归档资料
                </div>
            </td>
        </tr>
    </table>
    <table class="cls_content_head" style="width: 100%;">
        <tr id="Tr1">
            <td style="width: 10%;" align="center">
                序号
            </td>
            <td style="width: 25%;" align="center">
                名称
            </td>
            <td style="width: 15%" align="center">
                页数
            </td>
            <td style="width: 15%;" align="center">
                有无
            </td>
            <td style="width: 35%" align="center">
                备注
            </td>
        </tr>
    </table>
    <asp:Literal ID="lblFileInfo" runat="server"></asp:Literal>
    <table class="cls_show_cst_jiben">
        <tr>
            <td style="height: 23px; background-color: #f0f0f0" colspan="4" align="center">
                <a href="###" id="btn_save">
                    <img src="/Images/buttons/btn_save2.gif" style="width: 64px; height: 23px; border: none;" /></a>
                &nbsp;
                <img src="/Images/buttons/btn_back2.gif" onclick="javascript:history.back();" style="width: 65px;
                    height: 23px; border: none;" />
            </td>
        </tr>
    </table>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <input id="HidProID" type="hidden" value="<%=ProjectSysNo %>" />
    <input id="HidUserID" type="hidden" value="<%=UserSysNo %>" />
    </form>
</body>
</html>
