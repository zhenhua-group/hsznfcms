﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApplyProImage.aspx.cs"
    Inherits="TG.Web.ProjectManage.ProImageAudit.ApplyProImage" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="/css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="/css/ui-lightness/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css" />
    <link href="/css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" type="text/css" />
    <link href="/css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="/css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="/css/ui-lightness/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/ProjectMamage/ProImaAuditListView.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/jQuery-Plugs.js"></script>
    <script type="text/javascript" src="/js/Common/SendMessageCommon.js"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;当前位置：[图纸审批]
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head">
            <tr>
                <td style="width: 23%;" align="center">
                    工程名称
                </td>
                <td style="width: 8%" align="center">
                    工程号
                </td>
                <td style="width: 8%" align="center">
                    承担部门
                </td>
                <td style="width: 9%" align="center">
                    设计总负责
                </td>
                <td style="width: 10%" align="center">
                    设计阶段
                </td>
                <td style="width: 9%" align="center">
                    开始时间
                </td>
                <td style="width: 9%" align="center">
                    结束时间
                </td>
                <td style="width: 16%" align="center">
                    审批进度
                </td>
                <td style="width: 8%" align="center">
                    操作
                </td>
            </tr>
        </table>
        <asp:GridView ID="gv_project" runat="server" CellPadding="0" DataKeyNames="pro_ID"  CssClass="gridView_comm"
            AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center" ShowHeader="False"
            Font-Size="12px" RowStyle-Height="22px" Width="100%">
            <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
            <Columns>
                <asp:TemplateField HeaderText="项目名称">
                    <ItemTemplate>
                        <img src="/Images/proj.png" style="width: 16px; height: 16px; margin-left: 1px;" />
                        <a href="/ProjectManage/ShowProject.aspx?flag=image&pro_id=<%#Eval("pro_ID") %>"
                            title='<%# Eval("pro_name") %>' rel='<%# Eval("pro_ID") %>' class="chk">
                            <%# Eval("pro_name").ToString().Length > 15 ? Eval("pro_name").ToString().Substring(0, 15) + "......" : Eval("pro_name").ToString()%></a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="23%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="工程号">
                    <ItemTemplate>
                        <%#getNumber(Eval("pro_ID"))%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                </asp:TemplateField>
                <asp:BoundField DataField="Unit" HeaderText="承担部门" ItemStyle-HorizontalAlign="Center">
                    <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="PMName" HeaderText="项目负责人">
                    <ItemStyle Width="9%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="设计阶段">
                    <ItemTemplate>
                        <%#Eval("pro_status")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                </asp:TemplateField>
                <asp:BoundField DataField="pro_startTime" HeaderText="项目开始时间" DataFormatString="{0:yyyy-MM-dd}">
                    <ItemStyle Width="9%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="pro_finishTime" HeaderText="项目结束时间" DataFormatString="{0:yyyy-MM-dd}">
                    <ItemStyle Width="9%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="审核状态">
                    <ItemTemplate>
                        <div class="progressbar" percent="<%#Eval("Percent") %> " id="auditLocusContainer"
                            title='<%#Eval("Percent") %>%' action="M" referencesysno="<%#Eval("AuditRecordSysNo") %>"
                            style="cursor: pointer; margin: 1px 1px 1px 1px;">
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="16%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="发起审批">
                    <ItemTemplate>
                        <%#Eval("ActionLink")%>
                    </ItemTemplate>
                    <ItemStyle Width="8%" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
            CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条" CustomInfoTextAlign="Left"
            FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" OnPageChanged="AspNetPager1_PageChanged"
            PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox" PrevPageText="上一页" ShowBoxThreshold="10"
            ShowCustomInfoSection="Left" ShowPageIndexBox="Auto" SubmitButtonText="Go" TextAfterPageIndexBox="页"
            TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;" PageSize="30" SubmitButtonClass="submitbtn">
        </webdiyer:AspNetPager>
    </div>
    <!--HiddenArea-->
    <input type="hidden" id="HiddenUserSysNo" value="<%=UserSysNo %>" />
    <!--选择消息接收着-->
    <div id="msgReceiverContainer" style="width: 400px; height: 200px;">
    </div>
    </form>
</body>
</html>
