﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace TG.Web.ProjectManage
{
    public partial class SelectProjectNum : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定配置
                BingConfig();
            }
        }
        //绑定项目工号配置文件
        protected void BingConfig()
        {
            TG.BLL.cm_ProjectNumConfig bll = new TG.BLL.cm_ProjectNumConfig();
            DataSet ds_type = bll.GetList("");

            this.drp_protype.DataSource = ds_type;
            this.drp_protype.DataTextField = "ProType";
            this.drp_protype.DataValueField = "ID";
            this.drp_protype.DataBind();
        }

        protected void drp_protype_SelectedIndexChanged(object sender, EventArgs e)
        {
            TG.BLL.cm_ProjectNumConfig bll = new TG.BLL.cm_ProjectNumConfig();
            TG.Model.cm_ProjectNumConfig model = bll.GetModel(int.Parse(this.drp_protype.SelectedItem.Value));
            int istart = 0;
            int iend = 0;
            if (model != null)
            {
                //前缀
                string prefix = "";
                string strstart = "0";
                if (model.StartNum != "" && model.StartNum.IndexOf("-") > -1)
                {
                    prefix = model.StartNum.Split(new char[] { '-' }, StringSplitOptions.None)[0].ToString();
                    strstart = model.StartNum.Split(new char[] { '-' }, StringSplitOptions.None)[1].ToString();
                    //获取开始日期
                    int.TryParse(strstart, out istart);
                }
                string strend = "0";
                if (model.EndNum != "" && model.EndNum.IndexOf("-") > -1)
                {
                    strend = model.EndNum.Split(new char[] { '-' }, StringSplitOptions.None)[1].ToString();
                    int.TryParse(strend, out iend);
                }
                //添加工号列表
                this.drp_protypenum.Items.Add(new ListItem("---自定义工号---", "-1"));
                for (int i = istart; i < iend; i++)
                {
                    string strkey = prefix + "-" + Prefixf(i.ToString());
                    ListItem item = new ListItem(strkey, i.ToString());
                    this.drp_protypenum.Items.Add(item);
                }

            }
            //去除
            RemoveItems();
        }
        //去除使用的工号
        protected void RemoveItems()
        {
            TG.BLL.cm_projectNumber bll = new TG.BLL.cm_projectNumber();
            string strwhere = " ProNumber IS NOT NULL";
            List<TG.Model.cm_projectNumber> models = bll.GetModelList(strwhere);
            if (models.Count > 0)
            {
                for (int i = 0; i < this.drp_protypenum.Items.Count; i++)
                {
                    for (int j = 0; j < models.Count; j++)
                    {
                        if (models[j].ProNumber.Trim() == this.drp_protypenum.Items[i].Text.Trim())
                        {
                            this.drp_protypenum.Items.Remove(this.drp_protypenum.Items[i]);
                        }
                    }
                }
            }
        }
        protected string Prefixf(string str)
        {
            string rlt = "";
            if (str.Length == 1)
            {
                rlt = "00" + str;
            }
            else if (str.Length == 2)
            {
                rlt = "0" + str;
            }
            return rlt;
        }
    }
}
