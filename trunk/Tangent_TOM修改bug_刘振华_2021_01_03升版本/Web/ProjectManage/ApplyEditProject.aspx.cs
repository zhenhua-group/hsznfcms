﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TG.Model;
using TG.BLL;
using TG.Common;
using System.Text;
using AjaxPro;
using Newtonsoft.Json;

namespace TG.Web.ProjectManage
{
    public partial class ApplyEditProject : PageBase
    {
        protected TG.BLL.tg_project TGProject
        {
            get
            {
                return new TG.BLL.tg_project();
            }
        }
        protected TG.BLL.cm_Project CMProject
        {
            get
            {
                return new TG.BLL.cm_Project();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //注册页面
            Utility.RegisterTypeForAjax(typeof(ApplyEditProject));
            if (!IsPostBack)
            {
                //绑定单位
                BindUnit();
                //绑定年份
                BindYear();
                //绑定项目
                Bind_Project();
                //绑定权限
                BindPreviewPower();
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
            this.drp_year.SelectedValue = DateTime.Now.Year.ToString();
        }
        //获取用户权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位

            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //绑定项目列表
        public void Bind_Project()
        {
            StringBuilder sb = new StringBuilder("");
            ////名字不为空
            //if (this.txt_keyname.Value.Trim() != "")
            //{
            //    string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
            //    sb.Append(" AND pro_name LIKE '%" + keyname + "%'  ");
            //}
            ////绑定单位
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    sb.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            //}
            ////时间
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    sb.AppendFormat(" AND year(pro_startTime)={0} ", this.drp_year.SelectedValue);
            //}
            //检查权限
            GetPreviewPowerSql(ref sb);

            this.hid_where.Value = sb.ToString();
            //所有记录数
           // this.AspNetPager1.RecordCount = int.Parse(CMProject.GetListPageEditProcCount(sb.ToString()).ToString());
            //排序
           // string orderString = " Order by P.pro_ID DESC ";
           // sb.Append(orderString);

           // gv_project.DataSource = CMProject.GetListEditByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
           // gv_project.DataBind();
        }

        /// <summary>
        /// 获取选中项的ID
        /// </summary>
        /// <returns></returns>
        public string Del_Id()
        {
            string strDelId = "";
            //foreach (GridViewRow gv in gv_project.Rows)
            //{
            //    CheckBox ck = (CheckBox)gv.FindControl("chk_Pro");
            //    if (ck.Checked)
            //    {
            //        HiddenField hfproId = (HiddenField)gv.FindControl("bs_project_Id");
            //        strDelId += hfproId.Value + ",";
            //    }
            //}
            //if (strDelId.IndexOf(",") > -1)
            //{
            //    strDelId = strDelId.Substring(0, strDelId.LastIndexOf(","));
            //}
            return strDelId;
        }
        //绑定行信息
        //绑定行信息
        protected void gv_project_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string proid = ((HiddenField)e.Row.Cells[7].FindControl("hid_proid")).Value;
                if (!CheckAudit(proid))
                {
                    ((HiddenField)e.Row.Cells[7].FindControl("hid_exsit")).Value = "1";
                    if (IsCompleteAppProj(proid))
                    {
                        ((HiddenField)e.Row.Cells[7].FindControl("hid_exsit")).Value = "2";
                    }
                }

                HiddenField hiddenPercent = e.Row.Cells[5].FindControl("HiddenFieldPercent") as HiddenField;
                HiddenField hiddenAuditStatus = e.Row.Cells[5].FindControl("HiddenFieldAuditStatus") as HiddenField;

                hiddenPercent.Value = XMLHelper.AccountPercent(2, hiddenAuditStatus.Value).ToString("f2");
            }
        }
        // 项目立项审批完成判断
        protected bool IsCompleteAppProj(string proid)
        {
            bool flag = false;
            string strSql = " SELECT TOP 1 Status FROM dbo.cm_ProjectAudit Where ProjectSysNo=" + proid + " Order By SysNo DESC ";
            CommDBHelper bll = new CommDBHelper();
            DataSet ds_result = bll.GetList(strSql);
            if (ds_result.Tables.Count > 0)
            {
                if (ds_result.Tables[0].Rows.Count > 0)
                {
                    string process = ds_result.Tables[0].Rows[0][0].ToString().Trim();
                    if (process == "D")
                    {
                        flag = true;
                    }
                }
            }
            return flag;
        }
        //发起审批
        [AjaxPro.AjaxMethod]
        public string StartAppProject(string query, string flag)
        {
            string result = "0";
            if (flag == "0")
            {
                result = GetNextProcessRoleUser();
            }
            else
            {
                ProjectAduitParamEntity param = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectAduitParamEntity>(query);
                //实例一个审核实体
                ProjectAuditDataEntity dataEntity = new ProjectAuditDataEntity
                {
                    InUser = UserSysNo,
                    ProjectSysNo = param.ProjectSysNo
                };
                //新建一条审批记录
                int identitySysNo = tg_ProjectAuditBP.InsertProjectAudit(dataEntity);

                if (identitySysNo > 0)
                {
                    //获取项目配置审批实体
                    ProjectAuditConfigViewEntity projectAuditConfigViewEntity = new ProjectAuditConfigBP().GetProjectRoleSysNoByPoisition(1);

                    //声明消息实体
                    SysMessageViewEntity sysMessageDataEntity = new SysMessageViewEntity
                    {
                        //ReferenceSysNo = identitySysNo.ToString(),
                        ReferenceSysNo = string.Format("projectAuditSysNo={0}&MessageStatus={1}", identitySysNo.ToString(), "A"),
                        FromUser = UserSysNo,
                        MsgType = 2,
                        InUser = UserSysNo,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", param.ProjectName, "项目立项审核"),
                        QueryCondition = param.ProjectName,
                        IsDone = "A",
                        Status = "A"
                    };
                    //声明消息实体
                    string sysMsgString = string.Empty;
                    //第一审批阶段人员iD
                    sysMessageDataEntity.ToRole = projectAuditConfigViewEntity.RoleSysNo.ToString();
                    //获取审批人列表人名
                    sysMsgString = CommonAudit.GetMessagEntity(sysMessageDataEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        result = sysMsgString;
                    }
                }
                else
                {
                    result = "0";
                }
            }

            return result;
        }

        /// <summary>
        /// 判断是否已提交审核
        /// </summary>
        public bool CheckAudit(string proid)
        {
            int result = TG.BLL.tg_ProjectAuditBP.CheckAudit(int.Parse(proid));
            if (result > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //查询
        protected void btn_Search_Click(object sender, EventArgs args)
        {
            Bind_Project();
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            Bind_Project();
        }

        //返回下一阶段用户列表
        private string GetNextProcessRoleUser()
        {
            string result = "";
            //获取项目配置审批实体
            ProjectAuditConfigViewEntity projectAuditConfigViewEntity = new ProjectAuditConfigBP().GetProjectRoleSysNoByPoisition(1);
            //得到审批用户的实体
            string roleUserString = CommonAudit.GetRoleName(projectAuditConfigViewEntity.RoleSysNo);
            if (!string.IsNullOrEmpty(roleUserString))
            {
                result = roleUserString;
            }
            else
            {
                result = "0";
            }

            return result;
        }
        //生产部门改变重新绑定数据
        protected void drp_unit_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_Project();
        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
    }
}