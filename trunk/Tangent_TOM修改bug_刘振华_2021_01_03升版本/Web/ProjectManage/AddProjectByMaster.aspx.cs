﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using System.Text;
using System.Xml;
using System.Data;
using TG.Common;

namespace TG.Web.ProjectManage
{
    public partial class AddProjectByMaster : PageBase
    {
        TG.BLL.cm_Project project = new TG.BLL.cm_Project();
        public string asTreeviewStructObjID
        {
            get
            {
                return this.asTreeviewStruct.GetClientTreeObjectId();
            }
        }
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructType.GetClientTreeObjectId();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定项目来源
                BindDdList();
                //项目来源添加新的属性
                DdListAddAttr();
                //保存合同ID
                ViewState["cprID"] = this.txtcpr_id.Value;
                //设置下拉树样式
                SetDropDownTreeThem();
                //建筑类别
                BindBuildType();
                //绑定建筑结构样式
                BindStructType();
                //绑定建筑分类
                BindBuildStuctType();
                //行业性质
                BindCorpHyxz();
                //获得并绑定权限
                BindPreviewPower();

            }
            else
            {
                SaveProj();
            }
        }

        private void SaveProj()
        {
            TG.Model.cm_Project pro_modol = new TG.Model.cm_Project();
            //项目信息
            pro_modol.pro_name = this.txt_name.Value.Trim();
            //管理级别
            int level = 0;
            if (this.radio_yuan.Checked)
            {
                level = 0;
            }
            else if (this.radio_suo.Checked)
            {
                level = 1;
            }
            pro_modol.pro_level = level;
            //建设单位
            pro_modol.pro_buildUnit = this.txtbuildUnit.Value.Trim();
            pro_modol.pro_startTime = Convert.ToDateTime(this.txt_startdate.Value);
            pro_modol.pro_finishTime = Convert.ToDateTime(this.txt_finishdate.Value);
            //建筑分类
            //update by 20130530 qpl
            pro_modol.pro_kinds = GetDropDownTreeCheckedValue(this.asTreeviewStructType.RootNode.ChildNodes);
            //项目备注
            pro_modol.pro_Intro = this.txt_remark.Value;
            //项目阶段
            string str_jd = "";
            if (CheckBox1.Checked)
            {
                str_jd += CheckBox1.Value + ",";
            }
            else
            {
                str_jd += " ,";
            }
            if (CheckBox2.Checked)
            {
                str_jd += CheckBox2.Value + ",";
            }
            else
            {
                str_jd += " ,";
            }
            if (CheckBox3.Checked)
            {
                str_jd += CheckBox3.Value + ",";
            }
            else
            {
                str_jd += " ,";
            }
            if (CheckBox4.Checked)
            {
                str_jd += CheckBox4.Value + ",";
            }
            else
            {
                str_jd += " ,";
            }
            pro_modol.pro_status = str_jd;
            pro_modol.Pro_src = Convert.ToInt32(this.ddsource.SelectedValue);
            pro_modol.ChgJia = this.txt_Aperson.Value.Trim();
            pro_modol.Cpr_Acount = Convert.ToDecimal(this.txtproAcount.Value.Trim());
            pro_modol.Phone = this.txt_phone.Value.Trim();
            pro_modol.Project_reletive = this.txt_reletive.Value.Trim();
            pro_modol.ProjectScale = Convert.ToDecimal(this.txt_scale.Value.Trim());
            //承接部门
            pro_modol.Unit = this.hid_unit.Value.Trim();
            pro_modol.BuildAddress = this.txtbuildAddress.Value.Trim();
            //合同id
            pro_modol.cprID = this.txtcpr_id.Value.Trim();
            pro_modol.UpdateBy = UserSysNo;
            //更新时间
            pro_modol.UpdateDate = DateTime.Now;
            //项目总负责
            pro_modol.PMName = this.txt_PMName.Value.Trim();
            pro_modol.PMName = this.hid_pmname.Value.Trim();
            pro_modol.PMPhone = this.txt_PMPhone.Value.Trim();
            //行业性质
            pro_modol.BuildType = this.drp_buildtype.SelectedItem.Text;
            pro_modol.Industry = this.ddProfessionType.SelectedItem.Text;
            //项目概况
            pro_modol.ProjSub = this.txt_sub.Value;
            if (this.txtcpr_id.Value == "")
            {
                pro_modol.cprID = ViewState["cprID"].ToString();
                if (string.IsNullOrEmpty(pro_modol.cprID))
                {
                    pro_modol.cprID = "0";
                }
            }
            //录入人
            pro_modol.InsertUserID = UserSysNo;
            //update 20130530 qpl
            pro_modol.pro_StruType = GetDropDownTreeCheckedValue(this.asTreeviewStruct.RootNode.ChildNodes);
            //update 20130608 qpl
            pro_modol.PMUserID = int.Parse(this.hid_pmuserid.Value);
            //update 20130715
            //经济所
            string isjjs = "0";
            if (this.chk_ISTrunEconomy.Checked)
            {
                isjjs = "1";
            }
            pro_modol.ISTrunEconomy = isjjs;
            //update  2013-9-3
            string auditlevel = "0,0";
            if (this.audit_yuan.Checked && !this.audit_suo.Checked)
            {
                auditlevel = "1,0";
            }
            else if (!this.audit_yuan.Checked && this.audit_suo.Checked)
            {
                auditlevel = "0,1";
            }
            else if (this.audit_yuan.Checked && this.audit_suo.Checked)
            {
                auditlevel = "1,1";
            }
            pro_modol.AuditLevel = auditlevel;
            //暖通所
            string isnt = "0";
            if (this.chk_ISHvac.Checked)
            {
                isnt = "1";
            }
            pro_modol.ISHvac = isnt;
            //土建所
            string isarch = "0";
            if (this.chk_ISArch.Checked)
            {
                isarch = "1";
            }
            pro_modol.ISArch = isarch;
            pro_modol.InsertDate = DateTime.Now;
            int result = 0;
            //是否进行到审批界面
            string isApply = this.hidIsAppay.Value;
            try
            {
                result = project.Add(pro_modol);
                if (result > 0)
                {
                    //添加合同附件
                    AddProjectAttach(result.ToString());
                    //添加合同效果图
                    AddProjectAttachThumd(result.ToString());
                    this.hidproId.Value = result.ToString();
                }

                //成功提示信息
                if (isApply == "1")
                {
                    MessageBox.ShowAndRedirect(this, "立项信息保存成功！", "ProMiddlePage.aspx?proid=" + result + "&proname=" + this.txt_name.Value.Trim());
                }
                else
                {
                    MessageBox.ShowAndRedirect(this, "立项信息保存成功！", "ProjectListBymaster.aspx?proid=" + result + "&proname=" + this.txt_name.Value.Trim());
                }

            }
            catch (Exception)
            {
                MessageBox.Show(this, "立项信息保存失败！");
                throw;
            }

        }
        //返回一个文件上传的随机ID zxq 20140121
        public string GetProjectID()
        {
            DateTime dt = DateTime.Now;
            string tempid = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString();
            return tempid;
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz'";
            this.ddProfessionType.DataSource = bll_dic.GetList(str_where);
            this.ddProfessionType.DataTextField = "dic_Name";
            this.ddProfessionType.DataValueField = "dic_Name";
            this.ddProfessionType.DataBind();
        }
        //合同建筑类别
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
            this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "dic_Name";
            this.drp_buildtype.DataBind();
        }
        //项目来源
        public void BindDdList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            //绑定项目来源
            DataSet dic_proly = dic.GetList(" dic_type='cpr_src'");
            ddsource.DataValueField = "id";
            ddsource.DataTextField = "dic_name";
            ddsource.DataSource = dic_proly;
            ddsource.DataBind();
        }
        //项目地区
        public void DdListAddAttr()
        {
            for (int i = 0; i < this.ddsource.Items.Count; i++)
            {
                this.ddsource.Items[i].Attributes.Add("name", this.ddsource.Items[i].Text);
            }
        }
        //建筑分类
        protected void BindBuildStuctType()
        {
            BindProInfoConfig("BuildType", this.asTreeviewStructType.RootNode);
            this.asTreeviewStructType.CollapseAll();
        }
        //结构形式
        protected void BindStructType()
        {
            BindProInfoConfig("StructType", this.asTreeviewStruct.RootNode);
            this.asTreeviewStruct.CollapseAll();
        }
        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }

        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        //添加附件
        protected void AddProjectAttach(string projid)
        {
            string strSql = " UPDATE [cm_AttachInfo] SET [Proj_Id] =" + projid + " WHERE [Temp_No] = '" + this.hid_projid.Value + "' AND OwnType='proj' AND UploadUser='" + UserSysNo + "'";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            bll_db.ExcuteBySql(strSql);
        }
        //保存缩略图
        protected void AddProjectAttachThumd(string projid)
        {
            string strSql = " UPDATE [cm_AttachInfo] SET [Proj_Id] =" + projid + " WHERE [Temp_No] = '" + this.hid_projid.Value + "'  AND OwnType='projt' AND UploadUser='" + UserSysNo + "'";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            bll_db.ExcuteBySql(strSql);
        }
        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.asTreeviewStruct.Theme = macOS;
            this.asTreeviewStructType.Theme = macOS;
        }
        // update by 20130530 qpl
        //获取选中树所有节点值并返回值
        protected string GetDropDownTreeCheckedValue(List<ASTreeViewNode> allnodes)
        {
            //最终生成字符串
            string rootvalue = "";
            foreach (ASTreeViewNode node in allnodes)
            {
                string secondvalue = "";
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                    GetSecondNodeList(node, ref secondvalue);
                }
                rootvalue += secondvalue;
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }

            return rootvalue;
        }
        protected void GetSecondNodeList(ASTreeViewNode node, ref string value)
        {
            if (node.ChildNodes.Count > 0)
            {
                //返回值
                StringBuilder sbresult = new StringBuilder();

                foreach (ASTreeViewNode snode in node.ChildNodes)
                {
                    if (snode.CheckedState == ASTreeViewCheckboxState.Checked || snode.CheckedState == ASTreeViewCheckboxState.HalfChecked)
                    {
                        //拼接第二级
                        value = "^" + snode.NodeValue;
                        string subvalue = "";
                        subvalue = value;
                        GetChildNodes(snode, ref subvalue);
                        foreach (string key in sblist)
                        {
                            sbresult.Append(key);
                        }
                        //清空当前列表
                        sblist.Clear();
                    }
                }
                value = sbresult.ToString();
            }
        }
        //查询数据
        List<string> sblist = new List<string>();
        protected void GetChildNodes(ASTreeViewNode node, ref string value)
        {
            StringBuilder sb = new StringBuilder();
            if (node.ChildNodes.Count > 0)
            {
                foreach (ASTreeViewNode childnode in node.ChildNodes)
                {
                    if ((childnode.CheckedState == ASTreeViewCheckboxState.Checked) || (childnode.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                    {
                        string tempvalue = value + "*" + childnode.NodeValue;

                        if (childnode.ChildNodes.Count > 0)
                        {
                            //如果还有子节点，继续遍历
                            GetChildNodes(childnode, ref tempvalue);
                        }
                        else
                        {
                            //添加末节点
                            sb.Append(tempvalue);
                        }
                    }
                }
            }
            else
            {
                //添加末节点
                sb.Append(value);
            }
            //赋值value
            sblist.Add(sb.ToString());
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;

                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }
    }
}