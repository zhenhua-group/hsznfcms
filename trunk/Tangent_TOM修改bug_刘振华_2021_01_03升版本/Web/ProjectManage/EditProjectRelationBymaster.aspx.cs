﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectManage
{
    public partial class EditProjectRelationBymaster : PageBase
    {
        //项目ID
        public int ProjectID
        {
            get
            {
                int proid = 0;
                int.TryParse(Request["pro_id"], out proid);
                return proid;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetProInfo();
                //获得并绑定权限
                BindPreviewPower();
            }
            else
            {
                SaveCopAndProj();
            }
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get { return true; }
        }
        //获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }
        private void SaveCopAndProj()
        {
            string strSql = " UPDATE cm_Project Set CoperationSysNo=" + this.hid_cprid.Value + ",Project_reletive='" + this.txt_coperation.Value + "' Where pro_ID=" + ProjectID;
            int AffectRow = TG.DBUtility.DbHelperSQL.ExecuteSql(strSql);
            if (AffectRow > 0)
            {
                this.txt_coperation.Disabled = false;
                TG.Common.MessageBox.ResponseScriptBack(this, "关联合同更新成功！");
            }
        }
        public void GetProInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(ProjectID);
            //项目名称
            this.txt_name.Text = pro_model.pro_name == null ? "" : pro_model.pro_name.Trim();

            //管理级别
            string level = pro_model.pro_level.ToString();
            if (level.Trim() == "0")
            {
                this.lbl_level.Text = "院管";
            }
            else
            {
                this.lbl_level.Text = "所管";
            }
            //建筑级别
            string BuildType = pro_model.BuildType.Trim();
            this.drp_buildtype.Text = BuildType;

            //审核级别
            string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
            if (auditlevel.Trim() == "1,0")
            {
                this.lbl_auditlevel.Text = "院审";
            }
            else if (auditlevel.Trim() == "0,1")
            {
                this.lbl_auditlevel.Text = "所审";
            }
            else if (auditlevel.Trim() == "1,1")
            {
                this.lbl_auditlevel.Text = "院审,所审";
            }
            //建设单位
            this.txtbuildUnit.Text = pro_model.pro_buildUnit == null ? "" : pro_model.pro_buildUnit.Trim();
            //承接部门
            this.txt_unit.Text = pro_model.Unit == null ? "" : pro_model.Unit.Trim();
            //建设地点
            this.txtbuildAddress.Text = pro_model.BuildAddress == null ? "" : pro_model.BuildAddress.Trim();
            //建设规模
            this.txt_scale.Text = pro_model.ProjectScale == null ? "" : pro_model.ProjectScale.ToString();
            //项目结构形式
            string StrStruct = pro_model.pro_StruType;
            if (!string.IsNullOrEmpty(StrStruct))
            {
                this.structType.Text = TG.Common.StringPlus.ResolveStructString(StrStruct);
            }
            //建筑分类
            string BuildStructType = pro_model.pro_kinds;
            if (!string.IsNullOrEmpty(BuildStructType))
            {
                this.buildStructType.Text = TG.Common.StringPlus.ResolveStructString(BuildStructType);
            }
            //显示项目阶段
            string str_jd = pro_model.pro_status;
            if (str_jd.IndexOf(',') > -1)
            {
                this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
            }
            //经济所
            string isotherprt = "";
            if (pro_model.ISTrunEconomy == "1")
            {
                isotherprt += "经济所,";
            }
            //暖通
            if (pro_model.ISHvac == "1")
            {
                isotherprt += "暖通热力所,";
            }
            //土建所
            if (pro_model.ISArch == "1")
            {
                isotherprt += "土建所,";
            }
            if (isotherprt.IndexOf(',') > -1)
            {
                this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
            }
            else
            {
                this.lbl_isotherprt.Text = "无";
            }
            //项目来源
            string pro_src = pro_model.Pro_src.ToString();
            this.ddsource.Text = GetProjectSrc(pro_src);
            //合同额
            this.txtproAcount.Text = pro_model.Cpr_Acount == null ? "" : pro_model.Cpr_Acount.ToString();
            //行业性质
            string industry = pro_model.Industry == null ? "" : pro_model.Industry.Trim();
            this.ddProfessionType.Text = industry;
            //甲方
            this.txt_Aperson.Text = pro_model.ChgJia == null ? "" : pro_model.ChgJia.Trim();
            this.txt_phone.Text = pro_model.Phone == null ? "" : pro_model.Phone.Trim();
            //项目总负责
            this.txt_PMName.Text = pro_model.PMName;
            this.txt_PMPhone.Text = pro_model.PMPhone == null ? "" : pro_model.PMPhone.Trim();
            //开始结束日期
            this.txt_startdate.Text = Convert.ToDateTime(pro_model.pro_startTime).ToString("yyyy-MM-dd");
            this.txt_finishdate.Text = Convert.ToDateTime(pro_model.pro_finishTime).ToString("yyyy-MM-dd");

            //项目特征概况
            string projSub = pro_model.ProjSub == null ? "" : pro_model.ProjSub.Trim();
            this.txt_sub.InnerHtml = TG.Common.StringPlus.HtmlEncode(projSub);
            //项目备注
            this.txt_remark.InnerText = pro_model.pro_Intro == null ? "" : pro_model.pro_Intro.Trim();
            this.txt_remark.Disabled = false;
        }
        //获取项目来源
        protected string GetProjectSrc(string id)
        {
            TG.Model.cm_Dictionary model = new TG.BLL.cm_Dictionary().GetModel(int.Parse(id));
            if (model != null)
            {
                return model.dic_Name;
            }
            else
            {
                return "";
            }
        }
        //通过项目ID 得到效果图
        protected string GetThumdPicByProjid(string projid)
        {
            TG.BLL.cm_AttachInfo bll = new TG.BLL.cm_AttachInfo();
            string strWhere = " Proj_id=" + projid + " AND OwnType='projt'";
            List<TG.Model.cm_AttachInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                return models[models.Count - 1].FileUrl.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

    }
}