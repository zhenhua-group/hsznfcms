﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using System.Text;
using TG.BLL;
using TG.Model;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;

namespace TG.Web.ProjectManage
{

    public partial class ApplyProjectBymaster : PageBase
    {
        public string ColumnsContent
        {
            get;
            set;
        }
        protected TG.BLL.tg_project TGProject
        {
            get
            {
                return new TG.BLL.tg_project();
            }
        }
        protected TG.BLL.cm_Project CMProject
        {
            get
            {
                return new TG.BLL.cm_Project();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //注册页面
            Utility.RegisterTypeForAjax(typeof(ApplyProjectBymaster));

            if (!IsPostBack)
            {
                //绑定字段
                BindColumns();
                //绑定单位
                BindUnit();
                //绑定年份
                BindYear();
                //绑定项目
                Bind_Project();
                //绑定权限
                BindPreviewPower();
            }
        }
        protected void BindColumns()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();

            dic.Add("管理级别", "pro_jb");
            dic.Add("审核级别", "AuditLevel");
            dic.Add("建设单位", "pro_buildUnit");
            dic.Add("建设地点", "BuildAddress");
            dic.Add("建设规模", "ProjectScale");
            dic.Add("承接部门", "Unit");
            dic.Add("合同额", "Cpr_Acount");
            dic.Add("结构形式", "pro_StruType");
            dic.Add("建筑分类", "pro_kinds");
            dic.Add("设计阶段", "pro_status");
            dic.Add("项目来源", "pro_from");
            dic.Add("行业性质", "Industry");
            dic.Add("甲方负责人电话", "Phone");
            dic.Add("执行设总", "PMUserName");   
            dic.Add("执行设总电话", "PMPhone");                  
            dic.Add("参与部门", "ISTrunEconomy");
            dic.Add("项目概况", "ProjSub");
            dic.Add("项目备注", "pro_Intro");
            dic.Add("录入人", "InsertUser");

            foreach (KeyValuePair<string, string> pair in dic)
            {
                ColumnsContent += "<label><input type='checkbox' value='" + pair.Value + "' />" + pair.Key + "</label>";
            }


            //  BindProInfoConfig(this.asTreeviewStruct.RootNode,dic);
            //this.asTreeviewStruct.CollapseAll();           
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
            this.drp_year.SelectedValue = DateTime.Now.Year.ToString();
        }
        //获取用户权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位

            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //绑定项目列表
        public void Bind_Project()
        {
            StringBuilder sb = new StringBuilder("");
            ////名字不为空
            //if (this.txt_keyname.Value.Trim() != "")
            //{
            //    string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
            //    sb.Append(" AND pro_name LIKE '%" + keyname + "%'  ");
            //}
            ////绑定单位
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    sb.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            //}
            ////时间
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    sb.AppendFormat(" AND year(pro_startTime)={0} ", this.drp_year.SelectedValue);
            //}
            //检查权限
            GetPreviewPowerSql(ref sb);

            this.hid_where.Value = sb.ToString();

            //所有记录数
            // this.AspNetPager1.RecordCount = int.Parse(CMProject.GetListPageProcCount(sb.ToString()).ToString());
            //排序
            // string orderString = " Order by P.pro_ID DESC ";
            // sb.Append(orderString);

            //  gv_project.DataSource = CMProject.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            // gv_project.DataBind();
        }
        //绑定行信息
        protected void gv_project_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string proid = ((HiddenField)e.Row.Cells[7].FindControl("hid_proid")).Value;
                if (!CheckAudit(proid))
                {
                    ((HiddenField)e.Row.Cells[7].FindControl("hid_exsit")).Value = "1";
                    if (IsCompleteAppProj(proid))
                    {
                        ((HiddenField)e.Row.Cells[7].FindControl("hid_exsit")).Value = "2";
                    }
                }

                HiddenField hiddenPercent = e.Row.Cells[5].FindControl("HiddenFieldPercent") as HiddenField;
                HiddenField hiddenAuditStatus = e.Row.Cells[5].FindControl("HiddenFieldAuditStatus") as HiddenField;

                hiddenPercent.Value = XMLHelper.AccountPercent(2, hiddenAuditStatus.Value).ToString("f2");
            }
        }
        // 项目立项审批完成判断
        protected bool IsCompleteAppProj(string proid)
        {
            bool flag = false;
            string strSql = " SELECT TOP 1 Status FROM dbo.cm_ProjectAudit Where ProjectSysNo=" + proid + " Order By SysNo DESC ";
            CommDBHelper bll = new CommDBHelper();
            DataSet ds_result = bll.GetList(strSql);
            if (ds_result.Tables.Count > 0)
            {
                if (ds_result.Tables[0].Rows.Count > 0)
                {
                    string process = ds_result.Tables[0].Rows[0][0].ToString().Trim();
                    if (process == "D")
                    {
                        flag = true;
                    }
                }
            }
            return flag;
        }
        //发起审批
        [AjaxPro.AjaxMethod]
        public string StartAppProject(string query, string flag)
        {
            string result = "0";
            if (flag == "0")
            {
                result = GetNextProcessRoleUser();
            }
            else
            {
                ProjectAduitParamEntity param = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectAduitParamEntity>(query);
                //实例一个审核实体
                ProjectAuditDataEntity dataEntity = new ProjectAuditDataEntity
                {
                    InUser = UserSysNo,
                    ProjectSysNo = param.ProjectSysNo
                };
                //新建一条审批记录
                int identitySysNo = tg_ProjectAuditBP.InsertProjectAudit(dataEntity);

                if (identitySysNo > 0)
                {
                    //获取项目配置审批实体
                    ProjectAuditConfigViewEntity projectAuditConfigViewEntity = new ProjectAuditConfigBP().GetProjectRoleSysNoByPoisition(1);

                    //声明消息实体
                    SysMessageViewEntity sysMessageDataEntity = new SysMessageViewEntity
                    {
                        //ReferenceSysNo = identitySysNo.ToString(),
                        ReferenceSysNo = string.Format("projectAuditSysNo={0}&MessageStatus={1}", identitySysNo.ToString(), "A"),
                        FromUser = UserSysNo,
                        MsgType = 2,
                        InUser = UserSysNo,
                        MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", param.ProjectName, "项目立项评审"),
                        QueryCondition = param.ProjectName,
                        IsDone = "A",
                        Status = "A"
                    };
                    //声明消息实体
                    string sysMsgString = string.Empty;
                    //第一审批阶段人员iD
                    sysMessageDataEntity.ToRole = projectAuditConfigViewEntity.RoleSysNo.ToString();
                    //获取审批人列表人名
                    sysMsgString = CommonAudit.GetMessagEntity(sysMessageDataEntity);
                    if (!string.IsNullOrEmpty(sysMsgString))
                    {
                        result = sysMsgString;
                    }
                }
                else
                {
                    result = "0";
                }
            }

            return result;
        }

        /// <summary>
        /// 判断是否已提交审核
        /// </summary>
        public bool CheckAudit(string proid)
        {
            int result = TG.BLL.tg_ProjectAuditBP.CheckAudit(int.Parse(proid));
            if (result > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //查询
        protected void btn_Search_Click(object sender, EventArgs args)
        {
            BindDataSetPageIndex();
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            Bind_Project();
        }

        //返回下一阶段用户列表
        private string GetNextProcessRoleUser()
        {
            string result = "";
            //获取项目配置审批实体
            ProjectAuditConfigViewEntity projectAuditConfigViewEntity = new ProjectAuditConfigBP().GetProjectRoleSysNoByPoisition(1);
            //得到审批用户的实体
            string roleUserString = CommonAudit.GetRoleName(projectAuditConfigViewEntity.RoleSysNo);
            if (!string.IsNullOrEmpty(roleUserString))
            {
                result = roleUserString;
            }
            else
            {
                result = "0";
            }

            return result;
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }

        //生产部门改变重新绑定数据
        protected void drp_unit_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataSetPageIndex();
        }
        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>
        protected void BindDataSetPageIndex()
        {
            //this.AspNetPager1.CurrentPageIndex = 0;
            //绑定数据
            Bind_Project();
        }


        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();

            string modelPath = " ~/TemplateXls/ProjectAuditList.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ChgJia"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_name"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["BuildType"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Project_reletive"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cpr_Acount"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["pro_startTime"]).ToString("yyyy_MM-dd"));

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["pro_finishTime"]).ToString("yyyy_MM-dd"));

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;

                cell.SetCellValue(setAuditStatus(dt.Rows[i]["AuditSysNo"].ToString(), dt.Rows[i]["AuditStatus"].ToString()));

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                if (!string.IsNullOrEmpty(dt.Rows[i]["InsertDate"].ToString()))
                {
                    cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["InsertDate"]).ToString("yyyy-MM-dd"));
                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("项目审批列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        /// <summary>
        /// 得到数据信息
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {
            StringBuilder sb = new StringBuilder("");
            //名字不为空
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.Append(" AND pro_name LIKE '%" + keyname + "%'  ");
            }
            //绑定单位
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            }
            //时间
            if (this.drp_year.SelectedIndex != 0)
            {
                sb.AppendFormat(" AND year(pro_startTime)={0} ", this.drp_year.SelectedValue);
            }
            //录入时间  
            string startTime = txt_start.Value;//录入时间
            string endTime = txt_end.Value;//录入时间
            if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                sb.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
            }
            else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                sb.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
            }
            else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                sb.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
            }
            //检查权限
            GetPreviewPowerSql(ref sb);

            TG.BLL.cm_Project bll = new TG.BLL.cm_Project();

            DataTable dt = bll.GetProjectExportInfo(sb.ToString()).Tables[0];

            return dt;
        }

        /// <summary>
        /// 设置审批状态
        /// </summary>
        /// <param name="auditid"></param>
        /// <param name="aduitStaues"></param>
        /// <returns></returns>
        private string setAuditStatus(string auditid, string aduitStaues)
        {
            string strStatus = "";
            if (auditid == "0" && aduitStaues == "A")
            {
                strStatus = "未发起评审";
            }
            else if (auditid != "0" && aduitStaues == "A")
            {
                strStatus = "发起评审";
            }
            else if (auditid != "0" && aduitStaues == "B")
            {
                strStatus = "承接部门负责人评审完毕";
            }
            else if (auditid != "0" && aduitStaues == "C")
            {
                strStatus = "承接部门负责人评审不通过";
            }
            else if (auditid != "0" && aduitStaues == "D")
            {
                strStatus = "全部审批通过";
            }
            else if (auditid != "0" && aduitStaues == "E")
            {
                strStatus = "生产经营部审批不通过";
            }
            return strStatus;
        }
    }

    //定义一个参数累
    public class ProjectAduitParamEntity
    {
        public int ProjectSysNo { get; set; }

        public string ProjectName { get; set; }
    }

    public class ProjectAduitEditParamEntity
    {
        public int ProjectSysNo { get; set; }

        public string ProjectName { get; set; }
        public string copoption { get; set; }
        public string ChangeDetail { get; set; }
    }


}