﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProImageAuditConfigBymaster.aspx.cs" Inherits="TG.Web.ProjectManage.ProImageAuditConfigBymaster" %>

<%@ Register Src="../UserControl/AuditConfigBase.ascx" TagName="AuditConfigBase"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        ul
        {
            list-style-type: none;
        }
        
        .containerli
        {
            width: 100%;
            float: right;
            margin-top: 2px;
            margin-bottom: 2px;
            cursor: all-scroll;
        }
        
        .sortli
        {
            line-height: 20px;
            border-right: dotted 1px;
            padding-left: 5px;
        }
        
        .inputTextBox
        {
            height: 12px;
            width: 100%;
        }
        
        .RoleSelectShow
        {
            display: block;
        }
        
        body
        {
            font-size: 12px;
        }
        /*jQuery UI fakes 默认字体大小  */
        .ui-widget
        {
            font-size: 1em;
        }
        
        .ui-dialog .ui-dialog-buttonpane
        {
            padding-top: .1em;
            padding-bottom: .1em;
        }
    </style>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/UserControl/AuditConfigCommon.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            CommonControl.SetFormWidth();
            var auditConfigCommon = new AuditConfigCommon($("#form1"));
            $("#show8_5_6").parent().attr("class", "active");
            $("#show8_5").attr("class", "arrow open");
            $("#show8").parent().attr("class", "open");
            $("#arrow8").attr("class", "arrow open");
            $("#show8_5_6").parent().parent().css("display", "block");
            $("#show8_5_6").parent().parent().parent().parent().css("display", "block");
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        配置管理 <small>项目出图流程</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i>系统设置<i class="fa fa-angle-right"> </i>配置管理<i class="fa fa-angle-right"> </i>项目出图流程</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>产值分配审核配置</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12" id="AddCog">
                                <uc2:AuditConfigBase ID="AuditConfigBase1" runat="server" Title="项目出图流程" TableName="cm_ProImageAuditConfig" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
