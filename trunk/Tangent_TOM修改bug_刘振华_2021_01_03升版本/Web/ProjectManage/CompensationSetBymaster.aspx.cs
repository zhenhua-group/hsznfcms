﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
using NPOI.HSSF.Util;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using TG.Common;

namespace TG.Web.ProjectManage
{
    public partial class CompensationSetBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //初始分配年
                InitAllotYear();
                //绑定生产部门
                BindUnit();
                //绑定
                BindUnitAllot();
                //初始部门
                InitUnit();

            }
        }

        protected void out_Click(object sender, EventArgs e)
        {
            Output();
        }
        private void Output()
        {
            TG.BLL.cm_CompensationSet bll = new TG.BLL.cm_CompensationSet();
            string strWhere = " ";
            if (this.drp_year.SelectedIndex != 0)
            {
                strWhere = strWhere + " and ValueYear='" + this.drp_year.SelectedItem.Text + "'";
            }
            if (this.drp_unit.SelectedIndex != 0)
            {
                strWhere = strWhere + " and UnitId=" + this.drp_unit.SelectedItem.Value + "";
            }

            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = strWhere + "  and UnitId IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ") and MemberId=" + UserSysNo + "";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = strWhere + " and UnitId= " + UserUnitNo;
            }
            //姓名
            if (!string.IsNullOrEmpty(txt_cprName.Value))
            {
                strWhere = strWhere + " and mem_Name like '%" + txt_cprName.Value + "%'";
            }
            DataTable dt = bll.GetList(strWhere).Tables[0];
            ExportDataToExcel(dt);
        }
        //是否需要检查权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //绑定目标值
        protected void BindUnitAllot()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.hid_PreviewPattern.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.hid_UserSysNo.Value = UserSysNo.ToString();
                this.hid_UserUnitNo.Value = UserUnitNo.ToString();
            }


            TG.BLL.cm_CompensationSet bll = new TG.BLL.cm_CompensationSet();
            string strWhere = "";
            //   if (this.drp_year.SelectedIndex != 0)
            //  {
            //     strWhere=strWhere+ " and ValueYear='" + this.drp_year.SelectedItem.Text + "'";
            // }
            // if (this.drp_unit.SelectedIndex != 0)
            //{
            //    strWhere =strWhere+ " and UnitId=" + this.drp_unit.SelectedItem.Value + "";
            // }

            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = strWhere + "  and UnitId IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ") and MemberId=" + UserSysNo + "";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = strWhere + " and UnitId= " + UserUnitNo;
            }

            this.hid_where.Value = strWhere;
            //绑定
            //this.grid_mem.DataSource = bll.GetList(strWhere);
            //this.grid_mem.DataBind();
        }
        //初始当前年份
        protected void InitAllotYear()
        {
            string curYear = DateTime.Now.Year.ToString();
            this.drp_allotyear0.Items.FindByText(curYear).Selected = true;
            this.drp_year.Items.FindByText(curYear).Selected = true;
        }
        //初始部门
        protected void InitUnit()
        {
            //添加
            if (this.drp_unit0.Items.FindByValue(UserUnitNo.ToString()) != null)
            {
                this.drp_unit0.Items.FindByValue(UserUnitNo.ToString()).Selected = true;
            }
            //  if (this.drp_unit.Items.FindByValue(UserUnitNo.ToString()) != null)
            //  {
            //     this.drp_unit.Items.FindByValue(UserUnitNo.ToString()).Selected = true;
            // }
        }
        //保存
        protected void btn_save_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_CompensationSet bll = new TG.BLL.cm_CompensationSet();
            TG.Model.cm_CompensationSet model = new TG.Model.cm_CompensationSet();
            //检查当前年份是否已经添加了目标值
            //  string strWhere = " UnitID=" + this.drp_unit0.SelectedValue + " and MemberId=" + this.drp_memid.Value + " AND ValueYear='" + this.drp_allotyear0.SelectedItem.Text.Trim() + "'";
            //    List<TG.Model.cm_CompensationSet> list = bll.GetModelList(strWhere);
            //   if (list.Count == 0)
            //   {
            //赋值
            model.UnitId = int.Parse(this.drp_unit0.SelectedValue);
            model.MemberId = int.Parse(this.drp_memid.Value);
            model.ComValue = Convert.ToDecimal(this.txt_allot0.Text);
            model.ValueYear = this.drp_allotyear0.SelectedItem.Text;


            if (bll.Add(model) > 0)
            {
                MessageBox.ShowAndRedirect(this, "所补产值添加成功！", "CompensationSetBymaster.aspx");

            }

            //   }
            // else
            //  {
            //     MessageBox.Show(this, "所补产值已存在不能重复添加！");
            //  }
        }
        //编辑
        protected void btn_edit_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_CompensationSet bll = new TG.BLL.cm_CompensationSet();
            TG.Model.cm_CompensationSet model = new TG.Model.cm_CompensationSet();
            //赋值
            model.ID = int.Parse(this.hid_id.Value);
            model.UnitId = int.Parse(this.hid_unitid.Value);
            model.MemberId = int.Parse(this.hid_memid.Value);
            model.ComValue = Convert.ToDecimal(this.txt_allot1.Text);
            model.ValueYear = this.hid_allotyear.Value;

            if (bll.Update(model))
            {
                MessageBox.ShowAndRedirect(this, "所补产值修改成功！", "CompensationSetBymaster.aspx");
            }

            //绑定产值目标
            BindUnitAllot();
        }
        //  绑定生产部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";

             DataSet ds1 = bll_unit.GetList(strWhere);

            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            DataSet ds = bll_unit.GetList(strWhere);
            this.drp_unit.DataSource = ds;
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();

            this.drp_unit0.DataSource = ds;
            this.drp_unit0.DataTextField = "unit_Name";
            this.drp_unit0.DataValueField = "unit_ID";
            this.drp_unit0.DataBind();

            this.drp_unit1.DataSource = ds;
            this.drp_unit1.DataTextField = "unit_Name";
            this.drp_unit1.DataValueField = "unit_ID";
            this.drp_unit1.DataBind();

            this.drp_unit0_1.DataSource = ds1;
            this.drp_unit0_1.DataTextField = "unit_Name";
            this.drp_unit0_1.DataValueField = "unit_ID";
            this.drp_unit0_1.DataBind();

        }
        //导出Excel
        private void ExportDataToExcel(DataTable table)
        {
            string title = "";
            if (this.drp_year.SelectedIndex != 0)
            {
                title = this.drp_year.SelectedValue.Trim() + "年";
            }
            if (this.drp_unit.SelectedIndex != 0)
            {
                title += this.drp_unit.SelectedItem.Text.Trim();
            }


            FileStream fileOne = new FileStream(Server.MapPath("/TemplateXls/CompensationSet.xls"), FileMode.Open, FileAccess.ReadWrite);


            HSSFWorkbook excel = new HSSFWorkbook(fileOne);
            HSSFSheet sheet = (HSSFSheet)excel.GetSheetAt(0);
            //单元格格式
            HSSFDataFormat format = (HSSFDataFormat)excel.CreateDataFormat();
            HSSFCellStyle cs = (HSSFCellStyle)excel.CreateCellStyle();

            //文字置中 
            cs.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.CENTER;
            cs.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;
            //边框及颜色 
            cs.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            cs.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            cs.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //cs.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            cs.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.GREY_50_PERCENT.BLACK.index; cs.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.GREY_50_PERCENT.BLACK.index;
            cs.RightBorderColor = NPOI.HSSF.Util.HSSFColor.GREY_50_PERCENT.BLACK.index; cs.TopBorderColor = NPOI.HSSF.Util.HSSFColor.GREY_50_PERCENT.BLACK.index;
            // 设置字体   
            IFont font1 = excel.CreateFont();
            font1.FontName = "宋体";
            //字体大小 
            font1.FontHeightInPoints = 9;
            cs.SetFont(font1);

            //顶头设置
            HSSFCellStyle csHeader = (HSSFCellStyle)excel.CreateCellStyle();
            //文字置中 
            csHeader.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;
            csHeader.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.CENTER;
            csHeader.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            csHeader.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            csHeader.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont fontHeader = excel.CreateFont();
            fontHeader.FontName = "宋体";
            //字体大小 
            fontHeader.FontHeightInPoints = 9;
            //字体颜色
            fontHeader.Color = HSSFColor.GREY_50_PERCENT.BLACK.index;
            //字体加粗
            fontHeader.Boldweight = (short)2000;
            csHeader.SetFont(fontHeader);
            //csHeader
            IRow dataRowHeader = sheet.GetRow(0);

            dataRowHeader.GetCell(0).SetCellValue(title + "方案补贴表");
            //dataRowHeader.GetCell(0).CellStyle = csHeader;            


            for (int i = 0; i < table.Rows.Count; i++)
            {
                IRow dataRow = sheet.CreateRow(i + 2);

                for (int j = 0; j < table.Columns.Count-2; j++)
                {
                    object cellvalue = "";
                    if (j == 0)
                    {
                        cellvalue = (i + 1).ToString();
                    }
                    else
                    {
                        cellvalue = table.Rows[i][j];
                        if (j == 1)
                        {
                            TG.Model.tg_unit tgunit = new TG.BLL.tg_unit().GetModel(Convert.ToInt32(table.Rows[i][j]));
                            if (tgunit != null)
                            {
                                cellvalue = tgunit.unit_Name.Trim();
                            }

                        }
                        if (j == 2)
                        {
                            TG.Model.tg_member tgmember = new TG.BLL.tg_member().GetModel(Convert.ToInt32(table.Rows[i][j]));
                            if (tgmember != null)
                            {
                                cellvalue = tgmember.mem_Name;
                            }
                        }


                    }
                    if (j == (table.Columns.Count - 1))
                    {
                        dataRow.CreateCell(j).SetCellValue(Convert.ToDouble(cellvalue));
                    }
                    else
                    {
                        WriteExcelValue(dataRow.CreateCell(j), cellvalue);
                    }

                    //cs.WrapText = true;
                    dataRow.GetCell(j).CellStyle = cs;
                }

            }
            //横向总计
            int zj = table.Rows.Count + 2;
            IRow dataRowSum = sheet.CreateRow(zj);
            dataRowSum.Height = 25;
            dataRowSum.HeightInPoints = 25;
            for (int x = 0; x < 5; x++)
            {
                if (x == 0)
                {
                    dataRowSum.CreateCell(x).SetCellValue("总计：");
                }
                else if (x == 4)
                {
                    if (zj > 2)
                    {
                        dataRowSum.CreateCell(x).SetCellFormula("SUM(E3:E" + zj + ")");
                    }
                    else
                    {
                        dataRowSum.CreateCell(x).SetCellValue("0");
                    }

                }
                else
                {
                    dataRowSum.CreateCell(x).SetCellValue("");
                }
                dataRowSum.GetCell(x).CellStyle = csHeader;
            }



            using (MemoryStream memoryStream = new MemoryStream())
            {

                excel.Write(memoryStream);
                Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.xls", HttpUtility.UrlEncode(title + "方案补贴表", Encoding.UTF8)));
                Response.ContentType = "application/ms-excel";
                Response.BinaryWrite(memoryStream.ToArray());
                excel = null;
                fileOne.Close();
                Response.End();
            }
        }
        //判断数据类型
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };

                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());
                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));
                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);
                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);
                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}