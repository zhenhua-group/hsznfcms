﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Words;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjectJjsValueAllotExportBymaster : PageBase
    {
        /// <summary>
        /// 合同系统号
        /// </summary>
        public int ProSysNo
        {
            get
            {
                int CoperationSysNo = 0;
                int.TryParse(Request["pro_id"], out CoperationSysNo);
                return CoperationSysNo;
            }
        }

        /// <summary>
        /// 合同系统号
        /// </summary>
        public int Allot_Id
        {
            get
            {
                int allot_id = 0;
                int.TryParse(Request["allot_id"], out allot_id);
                return allot_id;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindProject();
                bindAllot();
            }
        }

        /// <summary>
        /// 项目信息
        /// </summary>
        private void bindProject()
        {
            TG.Model.cm_Project model = new TG.BLL.cm_Project().GetModel(ProSysNo);
            if (model != null)
            {
                lbl_UnitName.Text = model.Unit;
                lblproName.Text = model.pro_name;
            }
        }

        /// <summary>
        /// 产值信息
        /// </summary>
        private void bindAllot()
        {

            TG.BLL.cm_ProjectValueAllot bll = new BLL.cm_ProjectValueAllot();

            TG.Model.cm_ProjectValueAllot ProjectValueAllot = bll.GetModel(Allot_Id);
            if (ProjectValueAllot != null)
            {

                lbl_PaidValueCount.Text = ProjectValueAllot.PaidValueCount.ToString();
                txtTheDeptValuePercent.Text = ProjectValueAllot.Thedeptallotpercent.ToString();
                txtTheDeptValueCount.Text = ProjectValueAllot.Thedeptallotcount.ToString();
                txt_ProgramPercent.Text = ProjectValueAllot.ProgramPercent.ToString();
                txt_ProgramCount.Text = ProjectValueAllot.ProgramCount.ToString();
                txt_DesignManagerPercent.Text = ProjectValueAllot.DesignManagerPercent.ToString();
                txt_DesignManagerCount.Text = ProjectValueAllot.DesignManagerCount.ToString();
                txt_ShouldBeValuePercent.Text = ProjectValueAllot.ShouldBeValuePercent.ToString();
                txt_ShouldBeValueCount.Text = ProjectValueAllot.ShouldBeValueCount.ToString();
                lblTranBulidingPercent.Text = ProjectValueAllot.TranBulidingPercent.ToString();
                lblTranBulidingCount.Text = ProjectValueAllot.TranBulidingCount.ToString();
                lblFinanceValuePercent.Text = ProjectValueAllot.FinanceValuePercent.ToString();
                lblFinanceValueCount.Text = ProjectValueAllot.FinanceValueCount.ToString();
                lblTheDeptShouldValuePercent.Text = ProjectValueAllot.TheDeptShouldValuePercent.ToString();
                lblTheDeptShouldValueCount.Text = ProjectValueAllot.TheDeptShouldValueCount.ToString();
                lbl_AllotYear.Text = ProjectValueAllot.ActualAllountTime;
                //查询
                if (ProjectValueAllot.Itemtype == 25)
                {
                    int unitId = ProjectValueAllot.UnitId == null ? 0 : int.Parse(ProjectValueAllot.UnitId.ToString());
                    TG.Model.tg_unit unit = new TG.BLL.tg_unit().GetModel(unitId);
                    if (unit != null)
                    {
                        lblName.Text = unit.unit_Name;
                    }
                }

            }
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Export_Click(object sender, EventArgs e)
        {
            OutputInfo();
        }

        private void OutputInfo()
        {

            string tmppath = Server.MapPath("~/TemplateWord/ProjectJjsValue.doc");
            Document doc = new Document(tmppath); //载入模板
            if (doc.Range.Bookmarks["UnitName"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["UnitName"];
                mark.Text = lbl_UnitName.Text;
            }
            if (doc.Range.Bookmarks["ProjectName"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ProjectName"];
                mark.Text = lblproName.Text;
            }
            if (doc.Range.Bookmarks["PaidValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["PaidValueCount"];
                mark.Text = lbl_PaidValueCount.Text + "(元)";
            }

            if (doc.Range.Bookmarks["BulidingUnitName"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BulidingUnitName"];
                mark.Text = "转土建产值:" + lblName.Text;
            }
            if (doc.Range.Bookmarks["BulidingPercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BulidingPercent"];
                mark.Text = lblTranBulidingPercent.Text + "%";
            }
            if (doc.Range.Bookmarks["BulidingCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BulidingCount"];
                mark.Text = lblTranBulidingCount.Text + "(元)";
            }

            if (doc.Range.Bookmarks["DesignManagerPercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["DesignManagerPercent"];
                mark.Text = txt_DesignManagerPercent.Text + "%";
            }
            if (doc.Range.Bookmarks["DesignManagerCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["DesignManagerCount"];
                mark.Text = txt_DesignManagerCount.Text + "(元)";
            }


            if (doc.Range.Bookmarks["TheDeptValuePercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TheDeptValuePercent"];
                mark.Text = txtTheDeptValuePercent.Text + "%";
            }
            if (doc.Range.Bookmarks["TheDeptValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TheDeptValueCount"];
                mark.Text = txtTheDeptValueCount.Text + "(元)";
            }

            if (doc.Range.Bookmarks["ProgramPercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ProgramPercent"];
                mark.Text = txt_ProgramPercent.Text + "%";
            }
            if (doc.Range.Bookmarks["ProgramCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ProgramCount"];
                mark.Text = txt_ProgramCount.Text + "(元)";
            }

            if (doc.Range.Bookmarks["ShouldBeValuePercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ShouldBeValuePercent"];
                mark.Text = txt_ShouldBeValuePercent.Text + "%";
            }
            if (doc.Range.Bookmarks["ShouldBeValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ShouldBeValueCount"];
                mark.Text = txt_ShouldBeValueCount.Text + "(元)";
            }
            if (doc.Range.Bookmarks["FinanceValuePercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["FinanceValuePercent"];
                mark.Text = lblFinanceValuePercent.Text + "%";
            }
            if (doc.Range.Bookmarks["FinanceValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["FinanceValueCount"];
                mark.Text = lblFinanceValueCount.Text + "(元)";
            }
            if (doc.Range.Bookmarks["TheDeptShouldValuePercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TheDeptShouldValuePercent"];
                mark.Text = lblTheDeptShouldValuePercent.Text + "%";
            }
            if (doc.Range.Bookmarks["TheDeptShouldValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TheDeptShouldValueCount"];
                mark.Text = lblTheDeptShouldValueCount.Text + "(元)";
            }


            doc.Save(System.Web.HttpContext.Current.Server.UrlEncode(lblproName.Text + "产值分配表") + ".doc", SaveFormat.Doc, SaveType.OpenInWord, Response);
            //保存为doc，并打开
        }
    }
}