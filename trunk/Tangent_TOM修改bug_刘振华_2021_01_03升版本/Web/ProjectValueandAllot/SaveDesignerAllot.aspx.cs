﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using TG.Common;
using TG.Model;

namespace TG.Web.ProjectValueandAllot
{
    public partial class SaveDesignerAllot : PageBase
    {
        public List<ProjectPlanRole> ProjectPlanRoleList { get; set; }
        public ProjectDesignPlanRole ProjectDesignPlanRoleList { get; set; }
        /// <summary>
        /// 项目id
        /// </summary>
        public int ProjectSysNo
        {
            get
            {
                string obj = Request["proid"] ?? "0";
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分配年份
        /// </summary>
        public string AllotYear
        {
            get
            {

                return Request["year"] ?? "";
            }
        }
        /// <summary>
        /// 编辑或查看
        /// </summary>
        public string Action
        {
            get
            {
                return Request["pagetype"] ?? "";
            }
        }
        /// <summary>
        /// 当前登录用户
        /// </summary>
        public string UserID
        {

            get
            {
                return this.UserInfo["memid"].ToString();
            }
        }
        /// <summary>
        /// 专业ID
        /// </summary>
        public string SpeID
        {
            get
            {
                return Request["speid"] ?? "0";
            }
        }
        /// <summary>
        /// 专业名称
        /// </summary>
        public string SpeName
        {
            get
            {
                if (this.SpeID == "1")
                {
                    return "建筑";
                }
                else if (this.SpeID == "2")
                {
                    return "结构";
                }
                return "";
            }
        }
        /// <summary>
        /// 审核金额
        /// </summary>
        public decimal ProjAuditAllotCount
        {
            get
            {
                decimal result = 0m;
                if (!string.IsNullOrEmpty(AllotYear))
                {
                    string strSql = string.Format(@"Select ISNULL( SUM(AuditAmount),0) AS AuditCount
                                                    From cm_ProjectDesignProcessValueDetails 
                                                    Where Specialty='{0}' AND allotid in (Select  id From cm_ProjectValueAllot Where  pro_id={1} AND status='S'
					 AND (('{2}' is null)  OR (ActualAllountTime ='{2}')))", this.SpeName, ProjectSysNo, this.AllotYear);

                    DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        result = Convert.ToDecimal(dt.Rows[0]["AuditCount"].ToString());
                    }
                }

                return result;
            }
        }
        /// <summary>
        /// 设计金额
        /// </summary>
        public decimal ProjDesignAllotCount
        {
            get
            {
                decimal result = 0m;
                if (!string.IsNullOrEmpty(AllotYear))
                {
                    string strSql = string.Format(@"Select isnull(SUM(DesignCount),0) AS DesignCount 
                                                    From cm_ProjectValueByMemberDetails a left join tg_member b 
                                                    on a.mem_id =b.mem_id
                                                    inner join tg_speciality s
                                                    on b.mem_Speciality_ID=s.spe_ID
                                                    where  1=1
                                                    and ItemType=2 
                                                    and Status='S'
                                                    and  b.mem_Principalship_ID in (1,2,3)
                                                    and s.spe_Name='{0}'
                                                    and a.AllotID  in
                                                     (
                                                       select  id from cm_ProjectValueAllot where pro_ID={1}  and status='S'
                                                       and (('{2}' is null)  OR (ActualAllountTime ='{2}'  ) )
                                                      )
                                                    and a.ProID={1}", this.SpeName, ProjectSysNo, AllotYear);

                    DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        result = Convert.ToDecimal(dt.Rows[0]["DesignCount"].ToString());
                    }
                }

                return result;
            }
        }
        /// <summary>
        /// 已分配的审核金额
        /// </summary>
        public decimal AuditAllotDone
        {
            get
            {
                decimal result = 0m;
                string strSql = string.Format(@"Select isnull(SUM(AllotMoney),0.00) as AllotMoney
                                            From dbo.cm_AuditDesignCoefficient
                                            Where ProID={0} AND allotYear={1} AND allotType='1' AND SpeID={2}", this.ProjectSysNo, this.AllotYear, this.SpeID);

                result = Convert.ToDecimal(TG.DBUtility.DbHelperSQL.GetSingle(strSql));

                return result;
            }
        }
        /// <summary>
        /// 未分配的审核金额
        /// </summary>
        public decimal AuditAllotHav
        {
            get
            {
                return this.ProjAuditAllotCount - this.AuditAllotDone;
            }
        }
        /// <summary>
        /// 已分配设计金额
        /// </summary>
        public decimal DesignerAllotDone
        {
            get
            {
                decimal result = 0m;
                string strSql = string.Format(@"Select isnull(SUM(AllotMoney),0.00) as AllotMoney
                                            From dbo.cm_AuditDesignCoefficient
                                            Where ProID={0} AND allotYear={1} AND allotType='2' AND SpeID={2}", this.ProjectSysNo, this.AllotYear, this.SpeID);

                result = Convert.ToDecimal(TG.DBUtility.DbHelperSQL.GetSingle(strSql));

                return result;
            }
        }
        /// <summary>
        /// 已分配的设计金额
        /// </summary>
        public decimal DesignerAllotHav
        {
            get
            {
                return this.ProjDesignAllotCount - this.DesignerAllotDone;
            }
        }
        /// <summary>
        /// 获取审核人
        /// </summary>
        public void GetAuditUserHtml()
        {

            StringBuilder html = new StringBuilder();
            //策划人员
            ProjectPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanRoleAndUsers(this.ProjectSysNo);

            if (ProjectPlanRoleList.Count != 0)
            {
                ProjectPlanRoleList.Find(delegate(ProjectPlanRole plan) { return plan.SysNo == 4; }).Users.ForEach((user) =>
                {
                    if (this.SpeName == user.SpecialtyName)
                    {
                        DataTable dt = GetUserAllot(user.UserSysNo.ToString(), "1");
                        if (dt.Rows.Count > 0)
                        {
                            string allotmoney = dt.Rows[0]["AllotMoney"].ToString();
                            string allotprt = dt.Rows[0]["AllotPrt"].ToString();

                            html.Append("<tr>");
                            html.AppendFormat("<td><span class=\"clsUserName\" usersysno=\"{1}\">{0}</span></td>", user.UserName, user.UserSysNo);
                            html.AppendFormat("<td><input type=\"text\" allottype=\"1\" class=\"clsAllotCount\" value=\"{0}\" style=\"width:60px;\"/></td>", allotmoney);
                            html.AppendFormat("<td><span style=\"color:red;\" class=\"clsAllotPrt\">{0}</span>%</td>", allotprt);
                            html.Append("</tr>");
                        }
                        else
                        {
                            html.Append("<tr>");
                            html.AppendFormat("<td><span class=\"clsUserName\" usersysno=\"{1}\">{0}</span></td>", user.UserName, user.UserSysNo);
                            html.Append("<td><input type=\"text\" allottype=\"1\" class=\"clsAllotCount\" value=\"0\" style=\"width:60px;\"/></td>");
                            html.Append("<td><span style=\"color:red;\" class=\"clsAllotPrt\">0</span>%</td>");
                            html.Append("</tr>");
                        }
                    }
                });

                this.ltlAuditBody.Text = html.ToString();
            }
        }
        /// <summary>
        /// 获取已经分配额
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="allottyepe"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public DataTable GetUserAllot(string userid, string allottyepe)
        {
            string strSql = string.Format(@" SELECT AllotPrt,AllotMoney FROM [cm_AuditDesignCoefficient] 
                                            Where ProID={0} AND UserID={1} AND AllotYear={2} AND AllotType='{3}' AND SpeID={4} ", this.ProjectSysNo, userid, this.AllotYear, allottyepe, this.SpeID);

            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
            return dt;
        }
        /// <summary>
        /// 获取设计人员
        /// </summary>
        public void GetDesignUserHtml()
        {

            StringBuilder html = new StringBuilder();
            //设计人员
            ProjectDesignPlanRoleList = new TG.BLL.cm_ProjectPlanBP().GetProjectPlanDesignRoleAndUsers(this.ProjectSysNo);

            if (ProjectDesignPlanRoleList.Users.Count != 0)
            {
                ProjectDesignPlanRoleList.Users.ForEach((user) =>
                {
                    if (user.SpecialtyName == this.SpeName)
                    {
                        DataTable dt = GetUserAllot(user.UserSysNo.ToString(), "2");
                        if (dt.Rows.Count > 0)
                        {
                            string allotmoney = dt.Rows[0]["AllotMoney"].ToString();
                            string allotprt = dt.Rows[0]["AllotPrt"].ToString();

                            html.Append("<tr>");
                            html.AppendFormat("<td><span class=\"clsUserName\" usersysno=\"{1}\">{0}</span></td>", user.UserName, user.UserSysNo);
                            html.AppendFormat("<td><input type=\"text\" allottype=\"2\" class=\"clsAllotCount\" value=\"{0}\" style=\"width:60px;\"/></td>", allotmoney);
                            html.AppendFormat("<td><span style=\"color:red;\" class=\"clsAllotPrt\">{0}</span>%</td>", allotprt);
                            html.Append("</tr>");
                        }
                        else
                        {
                            html.Append("<tr>");
                            html.AppendFormat("<td><span class=\"clsUserName\" usersysno=\"{1}\">{0}</span></td>", user.UserName, user.UserSysNo);
                            html.Append("<td><input type=\"text\" allottype=\"2\" class=\"clsAllotCount\" value=\"0\" style=\"width:60px;\"/></td>");
                            html.Append("<td><span style=\"color:red;\" class=\"clsAllotPrt\">0</span>%</td>");
                            html.Append("</tr>");
                        }
                    }
                });

                this.ltlDesignBody.Text = html.ToString();
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetAuditUserHtml();
                GetDesignUserHtml();
            }
        }
    }
}