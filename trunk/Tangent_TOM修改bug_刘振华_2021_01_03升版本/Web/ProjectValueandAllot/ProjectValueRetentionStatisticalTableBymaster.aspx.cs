﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjectValueRetentionStatisticalTableBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //绑定年份
                BindYear();
                //选中当前年份
                SelectCurrentYear();
                //获取项目信息
                bindProjectValueNoList();
                //绑定
                BindUnit();
         
            }
        }

        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }

        //获取用户权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (p.InsertUserID =" + UserSysNo + " OR p.PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }

        
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += "  AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }


        private void bindProjectValueNoList()
        {
            StringBuilder strWhere = new StringBuilder();

            GetPreviewPowerSql(ref strWhere);
            this.hid_where.Value = strWhere.ToString();

        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {

            string modelPath = " ~/TemplateXls/ProjectValueRetentionStatisticalList.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);


            int row = 2;
            DataTable dt = getdata();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["pro_name"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Unit"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["Cpr_Acount"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["PayShiCount"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["AllotCount"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["TheDeptValueCount"].ToString());

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["AuditCount"].ToString());

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["DesignCount"].ToString());

            }

            row = row + dt.Rows.Count;

            IRow row2 = ws.CreateRow(row);
            ICell cellm1 = row2.CreateCell(0);
            cellm1.SetCellValue("");
            cellm1.CellStyle = style2;

            cellm1 = row2.CreateCell(1);
            cellm1.SetCellValue("本页合计");
            cellm1.CellStyle = style2;

            cellm1 = row2.CreateCell(2);
            cellm1.SetCellValue("");
            cellm1.CellStyle = style2;

            cellm1 = row2.CreateCell(3);
            cellm1.SetCellFormula("sum(D3:D" + row + ")");
            cellm1.CellStyle = style2;

            cellm1 = row2.CreateCell(4);
            cellm1.SetCellFormula("sum(E3:E" + row + ")");
            cellm1.CellStyle = style2;

            cellm1 = row2.CreateCell(5);
            cellm1.SetCellFormula("sum(F3:F" + row + ")");
            cellm1.CellStyle = style2;

            cellm1 = row2.CreateCell(6);
            cellm1.SetCellFormula("sum(G3:G" + row + ")");
            cellm1.CellStyle = style2;

            cellm1 = row2.CreateCell(7);
            cellm1.SetCellFormula("sum(H3:H" + row + ")");
            cellm1.CellStyle = style2;

            cellm1 = row2.CreateCell(8);
            cellm1.SetCellFormula("sum(I3:I" + row + ")");
            cellm1.CellStyle = style2;

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("项目所留产值统计表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }


        private DataTable getdata()
        {
            StringBuilder sb = new StringBuilder();
            string keyname = txt_cprName.Value;
            //按名称查询
            if (!string.IsNullOrEmpty(keyname))
            {
                keyname = TG.Common.StringPlus.SqlSplit(keyname);
                sb.AppendFormat(" AND pro_Name like '%{0}%'", keyname);
            }
            //绑定单位
            string unit = drp_unit.SelectedItem.Text;
            if (!string.IsNullOrEmpty(unit) && !unit.Contains("全院部门"))
            {
                sb.AppendFormat(" AND Unit='" + unit + "' ");
            }

            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (p.InsertUserID =" + UserSysNo + " OR p.PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }

            //年份
            string year = drp_year.SelectedValue;
            if (!string.IsNullOrEmpty(year) && year != "-1")
            {
                year = year;
            }
            else
            {
                year = "null";
            }

            return new TG.BLL.cm_ProjectValueAllot().GetProjectValueRetentionStatisticalInfo(sb.ToString(), year).Tables[0];
        }

       
    }
}