﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;

namespace TG.Web.ProjectValueandAllot
{
    public partial class AddjjsProjectValueAllotBymaster : PageBase
    {
        #region QueryString
        /// <summary>
        /// 项目系统编号
        /// </summary>
        public int proSysNo
        {
            get
            {
                int proSysNo = 0;
                int.TryParse(Request["proid"], out proSysNo);
                return proSysNo;
            }
        }


        public TG.Model.cm_ProjectValueAllot ProjectValueAllot { get; set; }

        /// <summary>
        /// 是否四舍五入
        /// </summary>
        public string IsRounding = ConfigurationManager.AppSettings["IsRounding"] ?? "0";

    /// <summary>
        /// 年份
        /// </summary>
        public string year
        {
            get
            {
                return Request["year"];
            }
        }
        public int CprID { get; set; }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定年份
                BindYear();
                //获取项目信息
                getProInfo();
                GetConperationBaseInfo();
                BindUnit();
//设置当年分配年份
                SelectCurrentYear();
            }
        }
        /// <summary>
        /// 项目信息
        /// </summary>
        private void getProInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(proSysNo);

            if (pro_model != null)
            {
                //项目名称
                lblProjectName.Text = pro_model.pro_name;
                //关联合同
                this.lblReletive.Text = pro_model.Project_reletive;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }

                //建设规模
                this.lblScale.Text = pro_model.ProjectScale.ToString();
                //建筑类别
                lblBuildType.Text = pro_model.BuildType;
                //项目总负责
                this.lblPMName.Text = pro_model.PMName;
                //承接部门
                lblcpr_Unit.Text = pro_model.Unit;

                //项目阶段

                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }

                //合同额

                lblCoperationAmount.Text = pro_model.Cpr_Acount.ToString();
                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }
                CprID = pro_model.CoperationSysNo;
            }

        }

        /// <summary>
        /// 得到项目基本信息
        /// </summary>
        private void GetConperationBaseInfo()
        {
            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
            string tempyear = year == "-1" ? "" : year;
            DataTable dt = BLL.GetComList(proSysNo.ToString(), tempyear).Tables[0];

            if (dt != null && dt.Rows.Count > 0)
            {

                //实收金额
                if (dt.Rows[0]["PayShiCount"] != null)
                {
                    lblPayShiCount.Text = dt.Rows[0]["PayShiCount"].ToString();
                }

                //分配金额
                if (dt.Rows[0]["AllotCount"] != null)
                {
                    lblAllotAccount.Text = dt.Rows[0]["AllotCount"].ToString();
                }
                if (dt.Rows[0]["PayShiCount"] != null && dt.Rows[0]["AllotCount"] != null)
                {
                    lblNotAllotAccount.Text = (decimal.Parse(dt.Rows[0]["PayShiCount"].ToString()) - decimal.Parse(dt.Rows[0]["AllotCount"].ToString())).ToString("f2");

                }
            }

        }

        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            this.RepeaterDepartment.DataSource = new TG.BLL.tg_unit().GetList("");
            this.RepeaterDepartment.DataBind();
        }

        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                    this.drp_year1.Items.Add(list[i]);
                }
            }
        }
/// <summary>
        /// 设置当年分配年份
        /// </summary>
        protected void SelectCurrentYear()
        {
            string curyear = year;
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            if (this.drp_year1.Items.FindByText(curyear) != null)
            {
                this.drp_year1.Items.FindByText(curyear).Selected = true;
            }
        }
    }
}