﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace TG.Web.ProjectValueandAllot
{
    public partial class TranjjsProjectValueAllotAuditBymaster : PageBase
    {
        /// <summary>
        /// 项目系统编号
        /// </summary>
        public int proSysNo
        {
            get
            {
                int proSysNo = 0;
                int.TryParse(Request["proid"], out proSysNo);
                return proSysNo;
            }
        }

        public decimal TotalCount
        {
            get
            {
                decimal totalCount = 0;
                decimal.TryParse(Request["totalCount"], out totalCount);
                return totalCount;
            }
        }

        /// <summary>
        /// 审核记录系统编号
        /// </summary>
        public int ValueAllotAuditSysNo
        {
            get
            {
                int valueAllotAuditSysNo = 0;
                int.TryParse(Request["auditSysID"], out valueAllotAuditSysNo);
                return valueAllotAuditSysNo;
            }
        }

        public string Type
        {
            get
            {
                return Request["type"];
            }
        }

        public TG.Model.cm_TranjjsProjectValueAllot ProjectValueAllot { get; set; }


        /// <summary>
        /// 审核记录实体
        /// </summary>
        public TG.Model.cm_TranProjectValueAuditRecord projectAuditRecordEntity { get; set; }

        public int allotID
        {
            get
            {
                int allotID = 0;
                int.TryParse(Request["allotID"], out allotID);
                return allotID;
            }
        }

        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }

        public string AuditUser { get; set; }
        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getProInfo();
                GetProjectValueAllotInfo();
                getProcessDetail();
                GetAuditRecord();
            }
        }
        /// <summary>
        /// 项目信息
        /// </summary>
        private void getProInfo()
        {

            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(proSysNo);

            if (pro_model != null)
            {
                //项目名称
                lblProjectName.Text = pro_model.pro_name;
                //关联合同
                this.lblReletive.Text = pro_model.Project_reletive;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }

                //建设规模
                this.lblScale.Text = pro_model.ProjectScale.ToString();
                //建筑类别
                lblBuildType.Text = pro_model.BuildType;
                //项目总负责
                this.lblPMName.Text = pro_model.PMName;
                //承接部门
                lblcpr_Unit.Text = pro_model.Unit;

                //项目阶段

                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }
                //合同额
                lblCoperationAmount.Text = pro_model.Cpr_Acount.ToString();
                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }
                lblTotalCount.Text = TotalCount.ToString();
                lblAllotAmount.Text = TotalCount.ToString();
                lbl_PaidValueCount.Text = TotalCount.ToString();
            }

        }

        /// <summary>
        /// 得到分配信息
        /// </summary>
        private void GetProjectValueAllotInfo()
        {
            TG.BLL.cm_TranjjsProjectValueAllot bll = new BLL.cm_TranjjsProjectValueAllot();
            TG.Model.cm_TranjjsProjectValueAllot ProjectValueAllot = bll.GetModelByProID(proSysNo, allotID);
            if (ProjectValueAllot != null)
            {
                lblStage.Text = getTypeDetail(ProjectValueAllot.ItemType.ToString());
                lbl_PaidValueCount.Text = ProjectValueAllot.PaidValueCount.ToString();
                lblAllotAmount.Text = ProjectValueAllot.AllotCount.ToString();
                txtTheDeptValuePercent.Text = ProjectValueAllot.Thedeptallotpercent.ToString();
                txtTheDeptValueCount.Text = ProjectValueAllot.Thedeptallotcount.ToString();
                txt_ProgramPercent.Text = ProjectValueAllot.ProgramPercent.ToString();
                txt_ProgramCount.Text = ProjectValueAllot.ProgramCount.ToString();
                txt_DesignManagerPercent.Text = ProjectValueAllot.DesignManagerPercent.ToString();
                txt_DesignManagerCount.Text = ProjectValueAllot.DesignManagerCount.ToString();
                txt_ShouldBeValuePercent.Text = ProjectValueAllot.ShouldBeValuePercent.ToString();
                txt_ShouldBeValueCount.Text = ProjectValueAllot.ShouldBeValueCount.ToString();
                lbl_PaidValueCount.Text = ProjectValueAllot.TotalCount.ToString();
                lblTotalCount.Text = ProjectValueAllot.TotalCount.ToString();
                lblYear.Text = ProjectValueAllot.ActualAllountTime;
            }
        }

        /// <summary>
        /// 得到人员信息
        /// </summary>
        private void getProcessDetail()
        {

            TG.BLL.cm_jjsProjectValueAllotConfig bll = new TG.BLL.cm_jjsProjectValueAllotConfig();
            TG.Model.cm_jjsProjectValueAllotDetail model = bll.GetDetailModel(proSysNo, allotID);

            //专业 表头
            StringBuilder sbHead = new StringBuilder();
            if (model != null)
            {
                sbHead.Append(" <div id=\"tbProjectValueProcess\" class=\"cls_Container_Report\" >");
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  " + getTypeDetail(model == null ? "" : model.typeStatus.ToString()) + "-配置比例% </td> </tr> </table>");
                sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                sbHead.Append(" <tr><td rowspan=\"3\" width=\"18%\">项目分类 </td> <td colspan=\"8\" width=\"72%\">编制 </td> <td width=\"10%\" rowspan=\"3\"> 校对  </td> </tr>");
                sbHead.Append("  <tr>  <td rowspan=\"2\" width=\"9%\">编制 <br/>合计 </td> <td colspan=\"3\" width=\"27%\"> 建筑</td> <td colspan=\"4\" width=\"36%\">  安装 </td> </tr>");
                sbHead.Append("  <tr> <td width=\"9%\"> 建筑<br />  合计  </td><td width=\"9%\">土建 </td> <td width=\"9%\">  结构</td>");
                sbHead.Append("<td width=\"9%\">  安装<br /> 合计 </td><td width=\"9%\"> 给排水 </td> <td width=\"9%\">  采暖</td> <td width=\"9%\"> 电气 </td> </tr>");
                sbHead.Append("<table id=\"gvProjectValueProcess\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

                string itemStatus = model.typeStatus.ToString();
                if (itemStatus == "13" || itemStatus == "14" || itemStatus == "15" || itemStatus == "16" || itemStatus == "17" || itemStatus == "19" || itemStatus == "20" || itemStatus == "21" || itemStatus == "22")
                {
                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（%）</td>");
                    sbHead.Append("<td width=\"9%\">" + (100 - decimal.Parse(model.ProofreadPercent.ToString())).ToString("f2") + " %</td>");
                    sbHead.Append("<td width=\"27%\" colspan=\"3\">" + model.Totalbuildingpercent + "%</td>");
                    sbHead.Append("<td width=\"36%\" colspan=\"4\">" + model.TotalInstallationpercent + "%</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadPercent + "%</td>");
                    sbHead.Append("</tr>");

                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（元）</td>");
                    sbHead.Append("<td width=\"9%\">" + (decimal.Parse(model.TotalInstallationcount.ToString()) + decimal.Parse(model.Totalbuildingcount.ToString())).ToString("f2") + "</td>");
                    sbHead.Append("<td width=\"27%\" colspan=\"3\">" + model.Totalbuildingcount + "</td>");
                    sbHead.Append("<td width=\"36%\" colspan=\"4\">" + model.TotalInstallationcount + "</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadPercent + "</td>");
                    sbHead.Append("</tr>");
                }
                else
                {
                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（%）</td>");
                    sbHead.Append("<td width=\"9%\">" + (100 - decimal.Parse(model.ProofreadPercent.ToString())).ToString("f2") + " %</td>");
                    sbHead.Append("<td width=\"9%\">" + model.Totalbuildingpercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.BuildingPercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.StructurePercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.TotalInstallationpercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.DrainPercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.HavcPercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.ElectricPercent + "%</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadPercent + "%</td>");
                    sbHead.Append("</tr>");

                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（元）</td>");
                    sbHead.Append("<td width=\"9%\">" + (decimal.Parse(model.TotalInstallationcount.ToString()) + decimal.Parse(model.Totalbuildingcount.ToString())).ToString("f2") + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.Totalbuildingcount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.BuildingCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.StructureCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.TotalInstallationcount + "</td>");

                    sbHead.Append("<td width=\"9%\">" + model.DrainCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.HavcCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.ElectricCount + "</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadCount + "</td>");
                    sbHead.Append("</tr>");
                }
                sbHead.Append("</table></div>");
                sbHead.Append(getMember());
                lblMember.Text = sbHead.ToString();
            }
            else
            {
                lblNotData.Text = "<table id=\"tbdelete\" align=\"center\"><tr><td style=\" font-size: 12px;font-weight: bold; color: Red;\"  >此信息已被删除!</td></tr></table>";
            }
        }

        private string getMember()
        {

            TG.BLL.cm_TranProjectValueAllot BLL = new TG.BLL.cm_TranProjectValueAllot();

            DataTable dtData = BLL.GetTranProAuditedUser(proSysNo, "jjs", allotID).Tables[0];

            //专业 表头
            StringBuilder sbHead = new StringBuilder();
            sbHead.Append(" <div id=\"tbProjectValueBymemberAmount\" class=\"cls_Container_Report\" >");
            sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td> 项目策划人员产值分配比例% </td> </tr> </table>");
            sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbHead.Append("<tr ><td style=\"width: 15%; text-align: center;\">  名称</td><td style=\"width: 15%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 35%; text-align: center;\"  > 编制 </td> <td style=\"width: 35%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sbHead.Append(" <tr > <td style=\"width: 15%; text-align: center;\" > 专业 </td><td style=\"width: 15%; text-align: center;\">   姓名 </td> <td style=\"width: 17.5%; text-align: center;\"> 比例 </td> <td style=\"width: 17.5%; text-align: center;\"> 金额(元) </td> <td style=\"width: 17.5%; text-align: center;\"> 比例 </td> <td style=\"width: 17.5%; text-align: center;\">金额(元) </td>  </tr>");
            sbHead.Append(" </table>");

            sbHead.Append("<table id=\"gvProjectValueBymemberAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

            StringBuilder sbHtml = new StringBuilder();

            //取得专业
            var dtSpecialtyList = new DataView(dtData).ToTable("Specialty", true, "spe_Name");

            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                var dv = new DataView(dtData) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();

                sbHtml.Append("<tr>");
                sbHtml.Append("<td  width= \"15%\" class=\"cls_Column\" rowspan=\"" + dtMemberBySpecialty.Rows.Count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sbHtml.Append("<tr>");
                    }

                    sbHtml.Append("<td width= \"15%\">" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"17.5%\"> " + dtMemberBySpecialty.Rows[j]["DesignPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"17.5%\">" + dtMemberBySpecialty.Rows[j]["DesignCount"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"17.5%\"> " + dtMemberBySpecialty.Rows[j]["ProofreadPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"17.5%\">" + dtMemberBySpecialty.Rows[j]["ProofreadCount"].ToString() + "</td>");
                    sbHtml.Append("</tr>");
                }
            }
            sbHead.Append(sbHtml.ToString());
            sbHead.Append(" </table></div>");

            return sbHead.ToString();
        }
        private void GetAuditRecord()
        {
            TG.BLL.cm_TranProjectValueAuditRecord BLL = new TG.BLL.cm_TranProjectValueAuditRecord();

            projectAuditRecordEntity = BLL.GetModel(ValueAllotAuditSysNo);
            if (projectAuditRecordEntity != null)
            {
                TG.Model.tg_member user = new TG.Model.tg_member();
                //查询联系人姓名
                if (!string.IsNullOrEmpty(projectAuditRecordEntity.AuditUser))
                {
                    user = new TG.BLL.tg_member().GetModel(int.Parse(projectAuditRecordEntity.AuditUser));
                    AuditUser = user == null ? "" : user.mem_Name;
                }
            }
        }

        /// <summary>
        /// 得到信息
        /// </summary>
        /// <param name="itemType"></param>
        /// <returns></returns>
        private string getTypeDetail(string itemType)
        {
            string typeDetail = "";
            if (itemType == "11")
            {
                typeDetail = "概算（室外不单算）";
            }
            else if (itemType == "12")
            {
                typeDetail = "概算（室外单算）";
            }
            else if (itemType == "13")
            {
                typeDetail = "概算 (锅炉房)";
            }
            else if (itemType == "14")
            {
                typeDetail = "概算(变电所)";
            }
            else if (itemType == "15")
            {
                typeDetail = "概算(水泵房)";
            }
            else if (itemType == "16")
            {
                typeDetail = "概算(空调机房)";
            }
            else if (itemType == "17")
            {
                typeDetail = "概算（单独室外工程）";
            }
            else if (itemType == "18")
            {
                typeDetail = "预算";
            }
            else if (itemType == "19")
            {
                typeDetail = "预算 (锅炉房)";
            }
            else if (itemType == "20")
            {
                typeDetail = "预算(变电所)";
            }
            else if (itemType == "21")
            {
                typeDetail = "预算(水泵房)";
            }
            else if (itemType == "22")
            {
                typeDetail = "预算(空调机房)";
            }
            else if (itemType == "23")
            {
                typeDetail = "结算审核";
            }
            else if (itemType == "24")
            {
                typeDetail = "调整概算、造价鉴定";
            }
            else if (itemType == "25")
            {
                typeDetail = "建议书、可研";
            }
            return typeDetail;
        }
    }
}