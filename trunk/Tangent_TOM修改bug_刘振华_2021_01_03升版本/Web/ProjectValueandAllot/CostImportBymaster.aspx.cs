﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class CostImportBymaster : PageBase
    {
        private TG.BLL.CostImportBymaster bll = new BLL.CostImportBymaster();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitDropDownByYear();
                BindType();
                bindData();
                this.radiolist.Items[0].Selected = true;
                // 勘察分院生产部门 施工图审查室       承包公司    
            }

        }

        //初始年份
        protected void InitDropDownByYear()
        {
            this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
        }
        TG.BLL.cm_CostType costbll = new TG.BLL.cm_CostType();
        private void BindType()
        {
            //throw new NotImplementedException();

            string strwhere = "isInput=1 and costGroup in(0,4)";
            this.drp_costType.DataSource = costbll.GetList(strwhere);
            this.drp_costType.DataTextField = "costName";
            this.drp_costType.DataValueField = "ID";
            this.drp_costType.DataBind();
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string year = this.drp_year.SelectedItem.Text;
            string type = this.drp_costType.SelectedItem.Value;
            if (FileUpload.PostedFile != null && FileUpload.PostedFile.ContentLength > 0)
            {

                string FileImportDirectory = Server.MapPath("~/Template/"); ;

                //取得上传文件名称
                string uplodeName = FileUpload.PostedFile.FileName;

                //保存文件
                string extensionName = FileUpload.PostedFile.FileName.Substring(FileUpload.PostedFile.FileName.LastIndexOf("."));

                //生成的文件名称
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + (new Random()).Next(999).ToString("D3") + extensionName;


                if (!Directory.Exists(FileImportDirectory))
                {
                    Directory.CreateDirectory(FileImportDirectory);
                }

                //保存文件

                FileUpload.SaveAs(FileImportDirectory + "\\" + fileName);

                //读取到DataSet
                DataSet ds = new DataSet();

                try
                {
                    ds = ReaderExcelToDataSet(FileImportDirectory + "\\" + fileName, true);
                }
                catch (Exception ex)
                {
                    //把07 .xlsx格式 强制改成xls格式 抛出异常
                    if (ex.Message.Trim().Equals("外部表不是预期的格式。"))
                    {
                        TG.Common.MessageBox.Show(this.Page, "请上传标准版本(2003或者2007）格式的excel！");
                        return;
                    }
                    else
                    {
                        TG.Common.MessageBox.Show(this.Page, "请选择正确的模板！");
                        return;
                    }
                }

                DataTable dt = new DataTable();

                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                }
                else
                {
                    TG.Common.MessageBox.Show(this.Page, "上传文件无数据！");
                    return;
                }

                //模板 默认是9列
                if (dt.Columns.Count != 9)
                {
                    TG.Common.MessageBox.Show(this.Page, "请选择正确的模板！");
                    return;
                }

                //删除 空行
                List<DataRow> removelist = new List<DataRow>();
                // 删除 本月合计 和本月累计
                List<DataRow> removeHJlist = new List<DataRow>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bool rowdataisnull = true;

                    if (dt.Rows[i][4].ToString().Trim().Equals("本月合计") || dt.Rows[i][4].ToString().Trim().Equals("本年累计") || dt.Rows[i][4].ToString().Trim().Equals("月初余额"))
                    {
                        rowdataisnull = false;
                    }

                    if (!rowdataisnull)
                    {
                        removeHJlist.Add(dt.Rows[i]);
                    }
                }
                for (int i = 0; i < removeHJlist.Count; i++)
                {
                    dt.Rows.Remove(removeHJlist[i]);
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    bool rowdataisnull = true;
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {

                        if (!string.IsNullOrEmpty(dt.Rows[i][j].ToString().Trim()))
                        {
                            rowdataisnull = false;
                        }

                    }
                    if (rowdataisnull)
                    {
                        removelist.Add(dt.Rows[i]);
                    }

                }
                for (int i = 0; i < removelist.Count; i++)
                {
                    dt.Rows.Remove(removelist[i]);
                }
                //删除 C列
                dt.Columns.Remove(dt.Columns[2]);
                //判断是否有数据
                if (dt.Rows.Count < 0)
                {
                    TG.Common.MessageBox.Show(this.Page, "上传文件无数据！");
                    return;
                }
                try
                {

                    int count = bll.InsertFinancial(dt, year, type, UserSysNo.ToString());

                    if (count > 0)
                    {
                        TG.Common.MessageBox.Show(this.Page, "数据导入成功");


                        bindData();

                    }
                    else if (count == -1)
                    {
                        TG.Common.MessageBox.Show(this.Page, "上传数据已存在数据库中，请修改");
                    }
                    else
                    {

                        TG.Common.MessageBox.Show(this.Page, "数据导入失败");
                    }
                }
                catch (Exception ex)
                {
                    TG.Common.MessageBox.Show(this.Page, "数据导入失败");
                }
            }
        }

        private void bindData()
        {

            StringBuilder sb = new StringBuilder();
            //throw new NotImplementedException();

            //记录数
            this.AspNetPager1.RecordCount = int.Parse(bll.GetListPageProcCount("").ToString());
            //分页
            grid_Financial.DataSource = bll.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex); ;
            grid_Financial.DataBind();
        }
        /// <summary>
        /// 分页绑定数据
        /// </summary>
        /// <param name="src"></param>
        /// <param name="e"></param>
        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            bindData();
        }
        #region 读取Excel的内容到DataSet--第一行为标题行
        /// <summary>
        /// 读取Excel的内容到DataSet
        /// </summary>
        /// <param name="fileName">Excel全路径</param>
        /// <returns>读取到的DataSet</returns>
        public static DataSet ReaderExcelToDataSet(string fileName, bool isDeleteFile)
        {
            //声明保存excel数据的DataSet
            DataSet ds = new DataSet();

            //读取方式
            // string xlsDriver = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel {1};HDR=YES; IMEX=1;\"";
            string xlsDriver = "";
            string excelVersion = "";
            if (fileName.ToLower().EndsWith("xls"))
            {
                xlsDriver = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel {1};HDR=YES; IMEX=1;\"";
                excelVersion = "8.0";
            }
            else if (fileName.ToLower().EndsWith("xlsx"))
            {
                xlsDriver = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel {1};HDR=YES; IMEX=1;\"";
                excelVersion = "12.0";
            }
            else
            {
                throw new Exception("文件不是Excel格式");
            }


            OleDbConnection conn = new OleDbConnection(string.Format(xlsDriver, new string[] { fileName, excelVersion }));
            conn.Open();

            try
            {
                //excel中的所有sheet和视图
                DataTable schema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                OleDbDataAdapter da = new OleDbDataAdapter();
                OleDbCommand dc = new OleDbCommand();
                dc.Connection = conn;

                //读取第一个Sheet
                string tableName = string.Empty;
                foreach (DataRow row in schema.Rows)
                {
                    tableName = row["TABLE_NAME"].ToString();

                    if (tableName.Replace("'", string.Empty).Replace("\"", string.Empty).EndsWith("$"))
                    {
                        break;
                    }
                }


                if (!tableName.Equals(string.Empty))
                {
                    dc.CommandText = "select * from [" + tableName + "] ";
                    da.SelectCommand = dc;
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    ds.Tables.Add(dt);
                }
                else
                {
                    ds.Tables.Add(new DataTable());
                }

                da.Dispose();
                conn.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
            }

            //删除上传文件
            if (isDeleteFile && File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            return ds;

        }
        #endregion

        protected void radiolist_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.drp_costType.Items.Clear();
            string indexValue = this.radiolist.SelectedItem.Value;
            string strwhere = "";
            if (indexValue == "4")
            {
                strwhere = "isInput=1 and costGroup in(0,4)";

            }
            else if (indexValue == "1")
            {
                strwhere = "isInput=1 and costGroup in(0,1)";
            }
            else if (indexValue == "2")
            {
                strwhere = "isInput=1 and costGroup in(0,2)";
            }
            else if (indexValue == "3")
            {
                strwhere = "isInput=1 and costGroup in(0,3)";
            }
            this.drp_costType.DataSource = costbll.GetList(strwhere);
            this.drp_costType.DataTextField = "costName";
            this.drp_costType.DataValueField = "ID";
            this.drp_costType.DataBind();
            this.drp_costType.Items.Insert(0, new ListItem("-----选择类型-----", "-1"));
        }

       
    }
}