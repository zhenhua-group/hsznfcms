﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectSecondValueAllotDetailsBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.ProjectSecondValueAllotDetailsBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <%--    <link href="../css/ProjectValueandAllot.css" rel="stylesheet" type="text/css" />--%>
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <link href="../css/ui-lightness/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () { 
            CommonControl.SetFormWidth();
            ChangedBackgroundColor();
            //行背景
            $("#ctl00_ContentPlaceHolder1_gv_ProjectValue tr").hover(function () {
                $(this).addClass("tr_in");
            }, function () {
                $(this).removeClass("tr_in");
            });
            //隔行变色
            $("#ctl00_ContentPlaceHolder1_gv_ProjectValue tr:even").css({ background: "White" });

            $.each($(".progressbar"), function (index, item) {
                var percent = parseFloat($(item).attr("percent"));
                $(item).progressbar({
                    value: percent
                });
            });

            $("#ctl00_ContentPlaceHolder1_gv_ProjectValue tr").each(function () {
                if ($.trim($(this).children(":eq(6)").text()) == "分配被驳回") {
                    $(this).children(":eq(6)").find("a").css({ color: "Red" });
                    $(this).children().children(".progressbar").children("div:first").css({ background: "Red" });
                } else if ($.trim($(this).children(":eq(6)").text()) == "分配结果") {
                    $(this).children(":eq(7)").text("---");
                }
            });

            //删除
            $("a[class=deleteAuditValue]").live("click", function () {

                if (confirm("是否要删除此条产值分配信息，删除之后不再恢复?")) {

                    var pro_id = $(this).attr("pro_id");
                    var allot_id = $(this).attr("allot_id");
                    var audit_id = $(this).attr("audit_id");

                    $.post("/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx", { "action": "deleteAuditValue", "pro_id": pro_id, "allot_id": allot_id, "audit_id": audit_id, "type": "sec" }, function (result) {

                        if (result == 1) {
                            alert("删除成功");
                            window.location.reload();
                        }
                        else {
                            alert("删除失败");
                        }
                    });
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>二次产值分配明细</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>领导驾驶舱<i class="fa fa-angle-right"> </i>数据统计<i class="fa fa-angle-right"> </i>二次产值分配明细</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>二次产值分配明细
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered" style="width: 95%;" align="center">
                                <tr>
                                    <td style="width: 150px;">项目名称:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCprName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView ID="gv_ProjectValue" runat="server" CellPadding="0" AutoGenerateColumns="False"
                                CssClass="table table-bordered table-hover" RowStyle-HorizontalAlign="Left" ShowHeader="true"
                                Font-Size="12px" RowStyle-Height="22px" Width="100%">
                                <RowStyle HorizontalAlign="Left" Height="22px"></RowStyle>
                                <Columns>
                                    <asp:TemplateField HeaderText="分配次数" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            第 <%# Container.DataItemIndex+1 %>产值分配
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="AllotCount" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:F}"
                                        HeaderText="分配金额(元)">
                                        <ItemStyle HorizontalAlign="Right" Width="12%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AuditCount" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:F}"
                                        HeaderText="此次分配的审核金额(元)">
                                        <ItemStyle HorizontalAlign="Right" Width="12%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DesignCount" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:F}"
                                        HeaderText="此次分配的设计金额(元)">
                                        <ItemStyle HorizontalAlign="Right" Width="12%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AllotDate" ItemStyle-HorizontalAlign="Left" HeaderText="分配时间">
                                        <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="状态">
                                        <ItemTemplate>
                                            <div class="progressbar" id="auditLocusContainer" action="S" percent="<%#Eval("Percent") %>"
                                                referencesysno="<%# Eval("AuditSysNo") %>" style="cursor: pointer;">
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="16%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <%#Eval("LinkAction")%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="12%" />
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="删除" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <a class="deleteAuditValue" pro_id="<%#Eval("pro_ID") %>" allot_id="<%#Eval("ID")%>" audit_id="<%# Eval("AuditSysNo") %>" style="cursor: pointer;">删除</a>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="6%" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    无分配记录！
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
