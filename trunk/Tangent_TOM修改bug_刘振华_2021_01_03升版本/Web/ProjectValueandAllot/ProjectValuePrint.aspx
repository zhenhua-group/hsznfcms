﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectValuePrint.aspx.cs"
    Inherits="TG.Web.ProjectValueandAllot.ProjectValuePrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>项目产值分配打印</title>
    <style type="text/css">
        .cls_content_head
        {
            width: 90%;
            height: 25px;
            margin: 0 auto;
            border: solid 1px #CCC;
            border-collapse: collapse;
            font-size: 12px;
            font-family: "微软雅黑";
            margin-top: 2px;
        }
        
        .cls_content_head td
        {
            border: solid 1px #CCC;
            font-size: 12px;
            font-family: "微软雅黑";
        }
        .cls_content_head tr
        {
            height: 25px;
        }
        .cls_head
        {
            height: 25px;
            font-size: 15px;
            text-align: center;
        }
        .cls_container
        {
            width: 90%;
            border-collapse: collapse;
            margin: 0 auto;
        }
    </style>
    <style type="text/css" media="print">
        .NoPrint
        {
            display: none;
        }
    </style>
    <script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        $(function () {

            var process = $("#HiddenCoperationProcess").val();
            if (process == 0) {
                $("#stageamountOne").show();
                $("#processAmountOneTable").show();
                $("#processAmountTwoTable").show();
                $("#processAmountThreeTable").show();
                $("#processAmountFourTable").show();
                $("#spetamountable").show();
            }
            else if (process == 1) {
                $("#stageamountTwo").show();
                $("#processAmountOneTable").show();
                $("#processAmountTwoTable").show();
                $("#spetamountable").show();
            }
            else if (process == 2) {
                $("#stageamountThree").show();
                $("#processAmountThreeTable").show();
                $("#processAmountFourTable").show();
                $("#spetamountable").show();
            }
            else if (process == 3) {
                $("#stageamountFour").show();
                $("#processAmountTwoTable").show();
                $("#processAmountThreeTable").show();
                $("#processAmountFourTable").show();
                $("#spetamountable").show();
            } else if (process == 4) {
                $("#processAmountFiveTable").show();
                $("#outdooramounttable").show();
            }

        });
    </script>
</head>
<body >
    <form id="form1" runat="server">
    <div>
        <table class="cls_container">
            <tr>
                <td class="cls_head">
                    项目产值分配详细信息
                </td>
            </tr>
        </table>
        <table class="cls_content_head" align="center" id="CoperationBaseInfo">
            <tr>
                <td style="width: 10%;">
                    项目名称：
                </td>
                <td colspan="3" style="width: 90%;">
                    <asp:Label ID="lblProjectName" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    部门：
                </td>
                <td>
                    <asp:Label ID="lblcpr_Unit" runat="server" Text=""></asp:Label>
                </td>
                <td>
                    项目类别：
                </td>
                <td>
                    <asp:Label ID="lblBuildType" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    项目阶段：
                </td>
                <td colspan="3">
                    <asp:Label ID="lblProcess" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    合同额：
                </td>
                <td>
                    <asp:Label ID="lblCoperationAmount" runat="server" Text=""></asp:Label>(万元)
                </td>
                <td>
                    实收金额：
                </td>
                <td>
                    <asp:Label ID="lblPayShiCount" runat="server" Text=""></asp:Label>(万元)
                </td>
            </tr>
            <tr>
                <td>
                    未分配金额：
                </td>
                <td>
                    <asp:Label ID="lblNotAllotAccount" runat="server" Text=""></asp:Label>
                    (万元)
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <div id="TableContainer">
            <!--生产经营部Table-->
            <table class="cls_content_head">
                <tr>
                    <td style="width: 15%">
                        项目分配阶段:
                    </td>
                    <td style="width: 20%">
                        <select id="stage" runat="server" disabled="disabled">
                            <option value="-1">--请选择--</option>
                            <option value="0">方案+初设+施工图+后期</option>
                            <option value="1">方案+初设</option>
                            <option value="2">施工图+后期</option>
                            <option value="3">初设+施工图+后期</option>
                            <option value="4">室外工程</option>
                        </select>
                    </td>
                    <td style="width: 10%">
                        分配金额：
                    </td>
                    <td style="width: 55%">
                        <asp:Label ID="txt_AllotAccount" runat="server" Text=""></asp:Label>
                        <%-- <input id="txt_AllotAccount" maxlength="15" type="text" class="TextBoxStyle"
                            disabled="disabled" value="100" />--%>
                        (万元)
                    </td>
                </tr>
            </table>
            <table id="alloutCount" class="cls_content_head">
                <tr>
                    <td style="width: 10%" align="center">
                        序号
                    </td>
                    <td style="width: 15%" align="center">
                        内容
                    </td>
                    <td style="width: 25%" align="center">
                        比例
                    </td>
                    <td style="width: 25%" align="center">
                        产值(元)
                    </td>
                    <td style="width: 25%" align="center">
                        备注
                    </td>
                </tr>
                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        实收产值
                    </td>
                    <td>
                        <asp:Label ID="txt_PaidValuePercent" runat="server" Text="100"></asp:Label>
                        (%)
                    </td>
                    <td>
                        <asp:Label ID="txt_PaidValueCount" runat="server" Text=""></asp:Label>
                       (元)
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        2
                    </td>
                    <td>
                        设&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 总
                    </td>
                    <td>
                        <asp:Label ID="txt_DesignManagerPercent" runat="server" Text=""></asp:Label>
                       (%)
                    </td>
                    <td>
                        <asp:Label ID="txt_DesignManagerCount" runat="server" Text=""></asp:Label>
                       (元)
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        3
                    </td>
                    <td>
                        分配产值
                    </td>
                    <td>
                        <asp:Label ID="txtAllotValuePercent" runat="server" Text="100%-2"></asp:Label>
                       
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        4
                    </td>
                    <td>
                        转经济所产值
                    </td>
                    <td>
                        <asp:Label ID="txt_EconomyValuePercent" runat="server" Text=""></asp:Label>
                        (%)
                    </td>
                    <td>
                        <asp:Label ID="txt_EconomyValueCount" runat="server" Text=""></asp:Label>
                       (元)
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        5
                    </td>
                    <td>
                        转出其他部门产值
                    </td>
                    <td>
                        <asp:Label ID="txtOtherDeptValuePercent" runat="server" Text=""></asp:Label>
                        (%)
                    </td>
                    <td>
                        <asp:Label ID="txtOtherDeptValueCount" runat="server" Text=""></asp:Label>
                      (元)
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        6
                    </td>
                    <td>
                        本部门自留产值
                    </td>
                    <td>
                        <asp:Label ID="txtTheDeptValuePercent" runat="server" Text=""></asp:Label>
                        (%)
                    </td>
                    <td>
                        <asp:Label ID="txtTheDeptValueCount" runat="server" Text=""></asp:Label>
                        (元)
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        7
                    </td>
                    <td>
                        本部门产值合计
                    </td>
                    <td>
                        <asp:Label ID="txt_UnitValueCountPercent" runat="server" Text=""></asp:Label>
                       (%)
                    </td>
                    <td>
                        <asp:Label ID="txtUnitValueCount" runat="server" Text=""></asp:Label>
                        (元)
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <!--生产经营部Table-->
            <table class="cls_content_head" id="stageamountOne" style="display: none;">
                <tr>
                    <td colspan="5" style="text-align: center;">
                        项目各设计阶段产值分配比例(%)
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%; text-align: center;">
                        项目类别
                    </td>
                    <td style="width: 20%; text-align: center;">
                        方案金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        初设金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        施工图金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        后期金额(元)
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:GridView ID="gvStageOne" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            Width="100%">
                            <Columns>
                                <asp:BoundField DataField="ItemType">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ProgramAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="preliminaryAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="WorkDrawAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="LateStageAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有信息!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table class="cls_content_head" id="stageamountTwo" style="display: none;">
                <tr>
                    <td colspan="3" style="text-align: center;">
                        项目(方案+初步设计)二阶段产值分配比例(%)
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; width: 40%;">
                        项目类别
                    </td>
                    <td style="text-align: center; width: 30%;">
                        方案金额(元)
                    </td>
                    <td style="text-align: center; width: 30%;">
                        初设金额(元)
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:GridView ID="gvStageTwo" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            Width="100%">
                            <Columns>
                                <asp:BoundField DataField="ItemType">
                                    <ItemStyle Width="40%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ProgramAmount">
                                    <ItemStyle Width="30%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="preliminaryAmount">
                                    <ItemStyle Width="30%" HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有信息!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table class="cls_content_head" id="stageamountThree" style="display: none;">
                <tr>
                    <td colspan="3" style="text-align: center;">
                        项目(施工图设计+后期服务)二阶段产值分配比例(%)
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; width: 40%;">
                        项目类别
                    </td>
                    <td style="text-align: center; width: 30%;">
                        施工图设计金额(元)
                    </td>
                    <td style="text-align: center; width: 30%;">
                        后期服务金额(元)
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:GridView ID="gvStageThree" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            Width="100%">
                            <Columns>
                                <asp:BoundField DataField="ItemType">
                                    <ItemStyle Width="40%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="WorkDrawAmount">
                                    <ItemStyle Width="30%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="LateStageAmount">
                                    <ItemStyle Width="30%" HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有信息!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table class="cls_content_head" id="stageamountFour" style="display: none;">
                <tr>
                    <td colspan="4" style="text-align: center;">
                        项目(初步设计+施工图设计+后期服务)二阶段产值分配比例(%)
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; width: 25%;">
                        项目类别
                    </td>
                    <td style="text-align: center; width: 25%;">
                        初步设计金额(元)
                    </td>
                    <td style="text-align: center; width: 25%;">
                        施工图设计金额(元)
                    </td>
                    <td style="text-align: center; width: 25%;">
                        后期服务金额(元)
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:GridView ID="gvStageFour" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            Width="100%">
                            <Columns>
                                <asp:BoundField DataField="ItemType">
                                    <ItemStyle Width="25%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="preliminaryAmount">
                                    <ItemStyle Width="25%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="WorkDrawAmount">
                                    <ItemStyle Width="25%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="LateStageAmount">
                                    <ItemStyle Width="25%" HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有信息!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table class="cls_content_head" id="spetamountable" style="display: none;">
                <tr>
                    <td colspan="6" style="text-align: center;">
                        项目各设计阶段专业之间产值分配比例(%)
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%; text-align: center;">
                        设计阶段
                    </td>
                    <td style="width: 16%; text-align: center;">
                        建筑金额(元)
                    </td>
                    <td style="width: 16%; text-align: center;">
                        结构金额(元)
                    </td>
                    <td style="width: 16%; text-align: center;">
                        给排水金额(元)
                    </td>
                    <td style="width: 16%; text-align: center;">
                        暖通金额(元)
                    </td>
                    <td style="width: 16%; text-align: center;">
                        电气金额(元)
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:GridView ID="gvSpeAmount" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            Width="100%">
                            <Columns>
                                <asp:BoundField DataField="ProjectStage">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="bulidingAmount">
                                    <ItemStyle Width="16%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="structureAmount">
                                    <ItemStyle Width="16%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="drainAmount">
                                    <ItemStyle Width="16%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="hvacAmount">
                                    <ItemStyle Width="16%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="electricAmount">
                                    <ItemStyle Width="16%" HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有信息!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table class="cls_content_head" id="outdooramounttable" style="display: none;">
                <tr>
                    <td colspan="6" style="text-align: center;">
                        室外工程产值分配比例(%)
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%; text-align: center;">
                        建筑金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        结构金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        给排水金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        暖通金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        电气金额(元)
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:GridView ID="gvOutDoorAmount" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            Width="100%">
                            <Columns>
                                <asp:BoundField DataField="bulidingAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="structureAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="drainAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="hvacAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="electricAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有信息!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table class="cls_content_head" id="processAmountOneTable" style="display: none;">
                <tr>
                    <td colspan="5" style="width: 100%; text-align: center;">
                        方案设计工序产值分配比例(%)
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%; text-align: center;">
                        专业
                    </td>
                    <td style="width: 20%; text-align: center;">
                        审核金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        专业负责金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        校对金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        设计金额(元)
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:GridView ID="gvProcessAmountOne" runat="server" AutoGenerateColumns="False"
                            ShowHeader="False" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="Specialty">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AuditAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="SpecialtyHeadAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ProofreadAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DesignAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有信息!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table class="cls_content_head" id="processAmountTwoTable" style="display: none;">
                <tr>
                    <td colspan="5" style="width: 100%; text-align: center;">
                        初步设计工序产值分配比例(%)
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%; text-align: center;">
                        专业
                    </td>
                    <td style="width: 20%; text-align: center;">
                        审核金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        专业负责金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        校对金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        设计金额(元)
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:GridView ID="gvProcessAmountTwo" runat="server" AutoGenerateColumns="False"
                            ShowHeader="False" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="Specialty">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AuditAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="SpecialtyHeadAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ProofreadAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DesignAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有信息!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table class="cls_content_head" id="processAmountThreeTable" style="display: none;">
                <tr>
                    <td colspan="5" style="width: 100%; text-align: center;">
                        施工图工序产值分配比例(%)
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%; text-align: center;">
                        专业
                    </td>
                    <td style="width: 20%; text-align: center;">
                        审核金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        专业负责金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        校对金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        设计金额(元)
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:GridView ID="gvProcessAmountThree" runat="server" AutoGenerateColumns="False"
                            ShowHeader="False" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="Specialty">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AuditAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="SpecialtyHeadAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ProofreadAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DesignAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有信息!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table class="cls_content_head" id="processAmountFourTable" style="display: none;">
                <tr>
                    <td colspan="5" style="width: 100%; text-align: center;">
                        后期服务工序产值分配比例(%)
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%; text-align: center;">
                        专业
                    </td>
                    <td style="width: 20%; text-align: center;">
                        审核金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        专业负责金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        校对金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        设计金额(元)
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:GridView ID="gvProcessAmountFour" runat="server" AutoGenerateColumns="False"
                            ShowHeader="False" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="Specialty">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AuditAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="SpecialtyHeadAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ProofreadAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DesignAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有信息!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table class="cls_content_head" id="processAmountFiveTable" style="display: none;">
                <tr>
                    <td colspan="5" style="width: 100%; text-align: center;">
                        室外工程工序产值分配比例(%)
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%; text-align: center;">
                        专业
                    </td>
                    <td style="width: 20%; text-align: center;">
                        审核金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        专业负责金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        校对金额(元)
                    </td>
                    <td style="width: 20%; text-align: center;">
                        设计金额(元)
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <asp:GridView ID="gvProcessAmountFive" runat="server" AutoGenerateColumns="False"
                            ShowHeader="False" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="Specialty">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AuditAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="SpecialtyHeadAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ProofreadAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DesignAmount">
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有信息!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <table class="cls_content_head" id="tbProjectValueBymemberAmount">
                <tr>
                    <td colspan="6" style="width: 100%; text-align: center;">
                        项目设计人员产值分配比例(%)
                    </td>
                </tr>
                <tr>
                    <td style="width: 16%; text-align: center;">
                        姓名
                    </td>
                    <td style="width: 10%; text-align: center;">
                        性别
                    </td>
                    <td style="width: 16%; text-align: center;">
                        专业
                    </td>
                    <td style="width: 16%; text-align: center;">
                        部门
                    </td>
                    <td style="width: 16%; text-align: center;">
                        职责
                    </td>
                    <td style="width: 16%; text-align: center;">
                        金额(元)
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:GridView ID="gvProjectValueBymemberAmount" runat="server" AutoGenerateColumns="False"
                            ShowHeader="False" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="UserName">
                                    <ItemStyle Width="16%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="UserSex">
                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="SpecialtyName">
                                    <ItemStyle Width="16%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DepartmentName">
                                    <ItemStyle Width="16%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ItemType">
                                    <ItemStyle Width="16%" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Amount">
                                    <ItemStyle Width="16%" HorizontalAlign="Center" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有信息!
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <!--保存按钮-->
        <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="Table1">
            <tr>
                <td style="width: 100%; text-align: center;">
                    <input id="print" type="button" onclick="window.print()" value="打印"  class="NoPrint" style="width: 75px;
                        height: 23px; cursor: pointer;" />
                </td>
            </tr>
        </table>
    </div>
    <input type="hidden" id="HiddenCoperationProcess" value="<%=CoperationProcess %>" />
    </form>
</body>
</html>
