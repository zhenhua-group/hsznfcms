﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using AjaxPro;
using TG.Model;
using Newtonsoft.Json;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjAllotDetailsTable : System.Web.UI.Page
    {
        TG.BLL.CommClassOperator bll_comm = new TG.BLL.CommClassOperator();

        public string HasDone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProjAllotDetailsTable));

            if (!IsPostBack)
            {
                GetPartOneAndPartTwoInfo();
                if (HasDone == "D")
                {
                    Response.Write("<script type=\"text/javascript\">alert(\"该项已经分配完成！\");window.location.reload();</script>");
                    Response.End();
                    return;
                }
                //生成数据表
                this.Literal1.Text = CreatTable();
            }
        }
        /// <summary>
        /// 分配起始用户ID
        /// </summary>
        protected static int partOneRecordInUserSysNo
        {
            get
            {
                return Convert.ToInt32(HttpContext.Current.Request.QueryString["PartOneRecordInUserSysNo"] ?? "0");
            }
        }
        /// <summary>
        /// 当前用户ID
        /// </summary>
        protected static int UserSysNo
        {
            get
            {
                return Convert.ToInt32(new ProjAllotDetailsTable().UserInfo["memid"] ?? "0");
            }
        }
        /// <summary>
        /// 项目名称
        /// </summary>
        protected static string projectName
        {
            get
            {
                TG.Model.cm_Project model_proj = new TG.BLL.cm_Project().GetModel(int.Parse(HttpContext.Current.Request.QueryString["proid"] ?? "0"));
                if (model_proj != null)
                {
                    return model_proj.pro_name;
                }
                else
                {
                    return "";
                }
            }
        }
        /// <summary>
        /// 合同id
        /// </summary>
        protected static string coperationSysNo
        {
            get
            {
                return HttpContext.Current.Request.QueryString["cprid"] ?? "0";
            }
        }
        /// <summary>
        /// 创建表格
        /// </summary>
        /// <returns></returns>
        protected string CreatTable()
        {
            //声明创建Table
            StringBuilder sb_tb = new StringBuilder();
            sb_tb.Append("<table id=\"UserAllotDetails\" class=\"cls_UserAllot\">");
            //创建表头
            sb_tb.Append(CreateAllotHeader());
            //创建专业人员
            sb_tb.Append(CreateNumalRowCollection(this.hid_proid.Value));
            //创建表底
            sb_tb.Append(CreateAllotBottom());
            sb_tb.Append("</table>");
            return sb_tb.ToString();
        }
        /// <summary>
        /// 穿件表单头
        /// </summary>
        /// <returns></returns>
        protected string CreateAllotHeader()
        {
            StringBuilder sb_row = new StringBuilder();
            int icolum = 11;
            string[] configs_header = { "1|11" };
            string[] rowconfigs_header = { };
            string[] htmls_header = { "<div class=\"cls_tb_header\">设计、校核及专业注册、审核、审定人员设计报酬分配表(本表由生产部填写）</div>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs_header, rowconfigs_header, htmls_header));

            string[] configs = { "1|2", "3|4", "7|2" };
            string[] rowconfigs = { };
            string[] htmls = { "<span class=\"cls_cellheader\"></span>", "设计人员B", "<div style=\"width:120px;\">校核人员C</div>", "专业注册人员A", "审核人员D", "审定人员E" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));
            ////two
            TG.Model.cm_CostUserConfig cfg_cost = GetAllotUserConfig();
            string[] htmls2 = { "设计校审分成比例", "<span ids=\"partThreeDesignPercentTextBox\" class=\"cls_cell_label\" id=\"sp_disgnuser\" rel=\"" + cfg_cost.DisgnUserPrt + "\">" + cfg_cost.DisgnUserPrt + "%</span>", "<span style=\"\" ids=\"partThreeDesignPercentTextBox\" class=\"cls_cell_label\" id=\"sp_xiaodui\" rel=\"" + cfg_cost.ExamPrt + "\">" + cfg_cost.ExamPrt + "%</span>", "<span style=\"\" ids=\"partThreeDesignPercentTextBox\" class=\"cls_cell_label\" id=\"sp_zhuce\" rel=\"" + cfg_cost.SpeRegPrt + "\">" + cfg_cost.SpeRegPrt + "%</span>", "<span ids=\"partThreeDesignPercentTextBox\" style=\"\" class=\"cls_cell_label\" id=\"sp_shenh\" rel=\"" + cfg_cost.AuditPrt + "\">" + cfg_cost.AuditPrt + "%</span>", "<span style=\"\" ids=\"partThreeDesignPercentTextBox\" class=\"cls_cell_label\" id=\"sp_shend\" rel=\"" + cfg_cost.AuditDPrt + "\">" + cfg_cost.AuditDPrt + "%</span>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls2));
            ////three
            //百分比
            decimal persent = 0.01m;
            decimal allcount = Convert.ToDecimal(this.hid_allcount.Value);
            //设计人员总费用
            decimal ddesign = persent * allcount * Convert.ToDecimal(cfg_cost.DisgnUserPrt);
            //校核人员总费用
            decimal dxiaohe = persent * allcount * Convert.ToDecimal(cfg_cost.ExamPrt);
            //专业注册
            decimal dreg = persent * allcount * Convert.ToDecimal(cfg_cost.SpeRegPrt);
            //审核人员
            decimal dshenhe = persent * allcount * Convert.ToDecimal(cfg_cost.AuditPrt);
            //审定人员
            decimal dshending = persent * allcount * Convert.ToDecimal(cfg_cost.AuditDPrt);
            string[] configs2 = { "3|4", "7|2" };
            string[] htmls3 = { "设计报酬<br/>折合元/㎡", "ABCDE费用", "<span ids=\"partThreeABCDEPayTextBox\" class=\"cls_cell_label\" id=\"sp_disgnuserc\" rel=\"" + ddesign + "\">" + ddesign.ToString("f2") + "</span>", "<span  ids=\"partThreeABCDEPayTextBox\" style=\"\" class=\"cls_cell_label\" id=\"sp_xiaoduic\" rel=\"" + dxiaohe + "\">" + dxiaohe.ToString("f2") + "</span>", "<span style=\"\"  ids=\"partThreeABCDEPayTextBox\" class=\"cls_cell_label\" id=\"sp_zhucec\" rel=\"" + dreg + "\">" + dreg.ToString("f2") + "</span>", "<span  ids=\"partThreeABCDEPayTextBox\" style=\"\" class=\"cls_cell_label\" id=\"sp_shenhc\" rel=\"" + dshenhe + "\">" + dshenhe.ToString("f2") + "</span>", "<span  ids=\"partThreeABCDEPayTextBox\" style=\"\" class=\"cls_cell_label\" id=\"sp_shendc\" rel=\"" + dshending + "\">" + dshending.ToString("f2") + "</span>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs2, rowconfigs, htmls3));
            return sb_row.ToString();
        }
        /// <summary>
        /// 获取用户配置
        /// </summary>
        /// <returns></returns>
        protected TG.Model.cm_CostUserConfig GetAllotUserConfig()
        {
            TG.Model.cm_CostUserConfig model = null;
            TG.BLL.cm_CostUserConfig bll_cfg = new TG.BLL.cm_CostUserConfig();
            string strWhere = " Used=1";
            List<TG.Model.cm_CostUserConfig> models = bll_cfg.GetModelList(strWhere);
            if (models.Count > 0)
            {
                model = models[0];
            }
            return model;
        }
        /// <summary>
        /// 根据角色名返回用户列表
        /// </summary>
        /// <param name="rolename"></param>
        /// <param name="speid"></param>
        /// <param name="proid"></param>
        /// <returns></returns>
        public List<TG.Model.tg_member> GetMemsByProRoleName(string rolename, string speid, string proid)
        {
            string strWhere = "";
            string mem_IDs = "";
            //查询专业下校对人
            TG.BLL.tg_relation bll_rel = new TG.BLL.tg_relation();
            List<TG.Model.tg_relation> models_rel = new List<TG.Model.tg_relation>();
            TG.BLL.tg_ProRole bll_role = new TG.BLL.tg_ProRole();
            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();

            strWhere = " RoleName='" + rolename + "'";
            List<TG.Model.tg_ProRole> model_role = bll_role.GetModelList(strWhere);
            string str_role = model_role[0].ID.ToString();
            string tgProjectID = new TG.BLL.cm_Project().GetModel(int.Parse(proid)).ReferenceSysNo.ToString();
            //校对人员
            strWhere = " Pro_ID=" + tgProjectID + " AND Charindex('" + str_role + "',mem_RoleIDs,0)>0";
            models_rel = bll_rel.GetModelList(strWhere);
            foreach (TG.Model.tg_relation rel in models_rel)
            {
                mem_IDs += rel.mem_ID + ",";
            }

            mem_IDs = mem_IDs.Remove(mem_IDs.Length - 1);
            strWhere = " mem_ID IN (" + mem_IDs + ") AND mem_Speciality_ID=" + speid;

            //返回用户列表
            return bll_mem.GetModelList(strWhere);
        }
        /// <summary>
        /// 生成专业设计人员分配行
        /// </summary>
        /// <param name="proid"></param>
        /// <returns></returns>
        protected string CreateNumalRowCollection(string proid)
        {
            StringBuilder sb_row = new StringBuilder();

            //获取项目专业列表
            TG.BLL.tg_speciality bll_spe = new TG.BLL.tg_speciality();
            TG.BLL.tg_relation bll_rel = new TG.BLL.tg_relation();
            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
            TG.Model.cm_CostUserConfig cfg_cost = GetAllotUserConfig();
            TG.BLL.cm_CostSpeConfig bll_cfg = new TG.BLL.cm_CostSpeConfig();
            List<TG.Model.tg_speciality> models = bll_spe.GetModelList("");
            foreach (TG.Model.tg_speciality spe in models)
            {
                //对象索引变量
                int memID = 0;
                int mem_xdID = 0;
                int mem_shID = 0;
                int mem_sdID = 0;
                int itemp = 0;
                string tgProjectID = new TG.BLL.cm_Project().GetModel(int.Parse(proid)).ReferenceSysNo.ToString();
                //查询人员
                string strWhere = " Pro_ID=" + tgProjectID;
                //查询关联集合
                List<TG.Model.tg_relation> models_rel = bll_rel.GetModelList(strWhere);
                string mem_IDs = "";
                foreach (TG.Model.tg_relation rel in models_rel)
                {
                    mem_IDs += rel.mem_ID + ",";
                }
                mem_IDs = mem_IDs.Remove(mem_IDs.Length - 1);
                //查询专业下员工列表
                strWhere = " mem_ID IN (" + mem_IDs + ") AND mem_Speciality_ID=" + spe.spe_ID;
                List<TG.Model.tg_member> models_mem = bll_mem.GetModelList(strWhere);
                List<TG.Model.tg_member> models_mem_xd = GetMemsByProRoleName("校对人", spe.spe_ID.ToString(), proid);
                List<TG.Model.tg_member> models_mem_sh = GetMemsByProRoleName("审核人", spe.spe_ID.ToString(), proid);
                List<TG.Model.tg_member> models_mem_sd = GetMemsByProRoleName("审定人", spe.spe_ID.ToString(), proid);
                //计算行数
                int iresult = models_mem.Count / 4;
                int iresult_xd = models_mem_xd.Count / 2;
                int iresult_sh = models_mem_sh.Count;
                int iresult_sd = models_mem_sd.Count;
                int rows = models_mem.Count % 4 == 0 ? iresult : iresult + 1;
                int rows_xd = models_mem_xd.Count % 2 == 0 ? iresult_xd : iresult_xd + 1;
                int rows_sh = iresult_sh;
                int rows_sd = iresult_sd;
                int[] iarray = new int[4];
                iarray[0] = rows;
                iarray[1] = rows_xd;
                iarray[2] = rows_sh;
                iarray[3] = rows_sd;
                rows = Sort(iarray)[0];
                //根据专业生成行
                if (rows > 0)
                {
                    //特定结构类型专业的分成比例
                    string strsql = " SpeID=" + spe.spe_ID + " AND StrucType=" + this.hid_structype.Value;
                    List<TG.Model.cm_CostSpeConfig> model = bll_cfg.GetModelList(strsql);
                    decimal spe_prt = 0.0m;
                    if (model.Count > 0)
                    {
                        if (model[0] != null)
                        {
                            spe_prt = Convert.ToDecimal(model[0].ListPrt);
                        }
                    }
                    //计算不同专业每平米的钱数
                    decimal dlow = Convert.ToDecimal(this.hid_lowerallot.Value);
                    decimal dsign = Convert.ToDecimal(cfg_cost.DisgnUserPrt) * 0.01m;
                    decimal ddanwei = GetLowAllotMoney(dlow, dsign, spe_prt);
                    //各个专业开始
                    string[] configs = { };
                    string[] rowconfigs = { };
                    string[] htmls = { "<div ids=\"partThreeDesignPayTextBox\" id=\"cfg_danw_" + spe.spe_ID + "\" class=\"cls_danwei\" rel=\"rel_danwei\">" + decimal.Round(ddanwei, 1) + "</div>", "<span ids=\"partThreeSpecialtyPayPercentTextBox\" id=\"cfg_danwp_" + spe.spe_ID + "\" class=\"cls_danweip\" rel=\"" + spe_prt + "\">" + spe_prt.ToString("f2") + "%</span>", "<div class=\"cls_cell_tip\">B1</div>", "<div class=\"cls_cell_tip\">B2</div>", "<div class=\"cls_cell_tip\">B3</div>", "<div class=\"cls_cell_tip\">B4</div>", "<div class=\"cls_cell_tip\">C1</div>", "<div class=\"cls_cell_tip\">C2</div>", "<div class=\"cls_cell_tip\">A</div>", "<div class=\"cls_cell_tip\">D</div>", "<div class=\"cls_cell_tip\">E</div>" };
                    int icolum = 11;
                    sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));
                    //创建行
                    for (int i = 0; i < rows; i++)
                    {
                        string emptcell = "";
                        string[] htmls2 = new string[11];
                        string[] htmls3 = new string[11];
                        string[] htmls4 = new string[11];
                        htmls2[0] = spe.spe_Name.Trim() + "专业";
                        htmls3[0] = "NULL";
                        htmls4[0] = "NULL";
                        htmls2[1] = "设计比例";
                        htmls3[1] = "金额";
                        htmls4[1] = "人员";
                        htmls4[2] = memID < models_mem.Count ? "<span bindattribute=\"employee_B\" containername=\"" + spe.spe_Name + "\" >" + models_mem[memID++].mem_Name + "</span>" : emptcell;
                        itemp = memID - 1;
                        if (itemp >= 0 && memID <= models_mem.Count)
                        {
                            if (memID == models_mem.Count)
                            {
                                memID++;
                            }
                            htmls2[2] = itemp < models_mem.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"designperenct_B\" class=\"cls_cell_dsign_bl\" id=\"dsuser_" + models_mem[itemp].mem_ID + "_" + spe.spe_ID + "\"/>%" : emptcell;
                            htmls3[2] = itemp < models_mem.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"money_B\" vname=\"B1\" class=\"cls_cell_dsign\" id=\"dsusercount_" + models_mem[itemp].mem_ID + "_" + spe.spe_ID + "\" readonly/>" : emptcell;
                        }
                        else
                        {
                            htmls2[2] = "";
                            htmls3[2] = "";
                        }
                        htmls4[3] = memID < models_mem.Count ? "<span bindattribute=\"employee_B\" containername=\"" + spe.spe_Name + "\" >" + models_mem[memID++].mem_Name + "</span>" : emptcell;
                        itemp = memID - 1;
                        if (itemp >= 0 && memID <= models_mem.Count)
                        {
                            if (memID == models_mem.Count)
                            {
                                memID++;
                            }
                            htmls2[3] = itemp < models_mem.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"designperenct_B\" class=\"cls_cell_dsign_bl\" id=\"dsuser_" + models_mem[itemp].mem_ID + "_" + spe.spe_ID + "\"/>%" : emptcell;
                            htmls3[3] = itemp < models_mem.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"money_B\" vname=\"B2\" class=\"cls_cell_dsign\" id=\"dsusercount_" + models_mem[itemp].mem_ID + "_" + spe.spe_ID + "\"  readonly/>" : emptcell;
                        }
                        else
                        {
                            htmls2[3] = "";
                            htmls3[3] = "";
                        }
                        htmls4[4] = memID < models_mem.Count ? "<span bindattribute=\"employee_B\" containername=\"" + spe.spe_Name + "\" >" + models_mem[memID++].mem_Name + "</span>" : emptcell;
                        itemp = memID - 1;
                        if (itemp >= 0 && memID <= models_mem.Count)
                        {
                            if (memID == models_mem.Count)
                            {
                                memID++;
                            }
                            htmls2[4] = itemp < models_mem.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"designperenct_B\" class=\"cls_cell_dsign_bl\" id=\"dsuser_" + models_mem[itemp].mem_ID + "_" + spe.spe_ID + "\"/>%" : emptcell;
                            htmls3[4] = itemp < models_mem.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"money_B\" vname=\"B3\" class=\"cls_cell_dsign\" id=\"dsusercount_" + models_mem[itemp].mem_ID + "_" + spe.spe_ID + "\"  readonly/>" : emptcell;
                        }
                        else
                        {
                            htmls2[4] = "";
                            htmls3[4] = "";
                        }
                        htmls4[5] = memID < models_mem.Count ? "<span bindattribute=\"employee_B\" containername=\"" + spe.spe_Name + "\" >" + models_mem[memID++].mem_Name + "</span>" : emptcell;
                        itemp = memID - 1;
                        if (itemp >= 0 && memID <= models_mem.Count)
                        {
                            if (memID == models_mem.Count)
                            {
                                memID++;
                            }
                            htmls2[5] = itemp < models_mem.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"designperenct_B\" class=\"cls_cell_dsign_bl\" id=\"dsuser_" + models_mem[itemp].mem_ID + "_" + spe.spe_ID + "\"/>%" : emptcell;
                            htmls3[5] = itemp < models_mem.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"money_B\" vname=\"B4\" class=\"cls_cell_dsign\" id=\"dsusercount_" + models_mem[itemp].mem_ID + "_" + spe.spe_ID + "\"  readonly/>" : emptcell;
                        }
                        else
                        {
                            htmls2[5] = "";
                            htmls3[5] = "";
                        }
                        htmls4[6] = mem_xdID < models_mem_xd.Count ? "<span bindattribute=\"employee_C\" containername=\"" + spe.spe_Name + "\" >" + models_mem_xd[mem_xdID++].mem_Name + "</span>" : emptcell;
                        itemp = mem_xdID - 1;
                        if (itemp >= 0)
                        {
                            if (mem_xdID == models_mem_xd.Count)
                            {
                                mem_xdID++;
                            }
                            htmls2[6] = itemp < models_mem_xd.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"designperenct_C\" class=\"cls_cell_xh_bl\" id=\"xiaohe_" + models_mem_xd[itemp].mem_ID + "_" + spe.spe_ID + "\"/>%" : emptcell;
                            htmls3[6] = itemp < models_mem_xd.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"money_C\" vname=\"C1\" class=\"cls_cell_xh\" id=\"xiaohecount_" + models_mem_xd[itemp].mem_ID + "_" + spe.spe_ID + "\"  readonly/>" : emptcell;
                        }
                        else
                        {
                            htmls2[6] = "";
                            htmls3[6] = "";
                        }
                        htmls4[7] = mem_xdID < models_mem_xd.Count ? "<span bindattribute=\"employee_C\" containername=\"" + spe.spe_Name + "\" >" + models_mem_xd[mem_xdID++].mem_Name + "</span>" : emptcell;
                        itemp = mem_xdID - 1;
                        if (itemp >= 0)
                        {
                            if (mem_xdID == models_mem_xd.Count)
                            {
                                mem_xdID++;
                            }
                            htmls2[7] = itemp < models_mem_xd.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"designperenct_C\"  class=\"cls_cell_xh_bl\" id=\"xiaohe_" + models_mem_xd[itemp].mem_ID + "_" + spe.spe_ID + "\"/>%" : emptcell;
                            htmls3[7] = itemp < models_mem_xd.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"money_C\" vname=\"C2\" class=\"cls_cell_xh\" id=\"xiaohecount_" + models_mem_xd[itemp].mem_ID + "_" + spe.spe_ID + "\"  readonly/>" : emptcell;
                        }
                        else
                        {
                            htmls2[7] = "";
                            htmls3[7] = "";
                        }
                        //专业注册人员名称
                        htmls4[8] = "<input bindattribute=\"employee_S\" value=\"\" containername=\"" + spe.spe_Name + "\" class=\"cls_cell\"/>";
                        //比例
                        htmls2[8] = "<input type=\"text\" value=\"0\" class=\"cls_cell_reg_bl\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"designperenct_S\" id=\"reguser_" + i + "_" + spe.spe_ID + "\"/>%";
                        //钱数
                        htmls3[8] = "<input type=\"text\" value=\"0\" class=\"cls_cell_reg\" vname=\"A\"  containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"money_S\" id=\"regusercount_" + i + "_" + spe.spe_ID + "\"/>";

                        htmls4[9] = mem_shID < models_mem_sh.Count ? "<span bindattribute=\"employee_A\" containername=\"" + spe.spe_Name + "\" >" + models_mem_sh[mem_shID++].mem_Name + "</span>" : emptcell;
                        itemp = mem_shID - 1;
                        if (itemp >= 0)
                        {
                            if (mem_shID == models_mem_sh.Count)
                            {
                                mem_shID++;
                            }
                            htmls2[9] = itemp < models_mem_sh.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"money_A\"  class=\"cls_cell_sh_bl\" id=\"shenhe_" + models_mem_sh[itemp].mem_ID + "_" + spe.spe_ID + "\"/>%" : emptcell;
                            htmls3[9] = itemp < models_mem_sh.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\"  bindattribute=\"designperenct_A\" vname=\"D\" class=\"cls_cell_sh\" id=\"shenhecount_" + models_mem_sh[itemp].mem_ID + "_" + spe.spe_ID + "\"  readonly/>" : emptcell;
                        }
                        else
                        {
                            htmls3[9] = "";
                            htmls2[9] = "";
                        }
                        htmls4[10] = mem_sdID < models_mem_sd.Count ? "<span bindattribute=\"employee_J\" containername=\"" + spe.spe_Name + "\" >" + models_mem_sd[mem_sdID++].mem_Name + "</span>" : emptcell;
                        itemp = mem_sdID - 1;
                        if (itemp >= 0)
                        {
                            if (mem_sdID == models_mem_sd.Count)
                            {
                                mem_sdID++;
                            }
                            htmls2[10] = itemp < models_mem_sd.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"designperenct_J\"  class=\"cls_cell_sd_bl\" id=\"shending_" + models_mem_sd[itemp].mem_ID + "_" + spe.spe_ID + "\"/>%" : emptcell;
                            htmls3[10] = itemp < models_mem_sd.Count ? "<input type=\"text\" value=\"0\" containername=\"" + spe.spe_Name.Trim() + "\" bindattribute=\"money_J\" vname=\"E\"  class=\"cls_cell_sd\" id=\"shendingcount_" + models_mem_sd[itemp].mem_ID + "_" + spe.spe_ID + "\"  readonly/>" : emptcell;
                        }
                        else
                        {
                            htmls2[10] = "";
                            htmls3[10] = "";
                        }
                        string[] rowconfigs2 = { "1|4" };
                        sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs2, htmls2));
                        string[] rowconfigs3 = { };
                        sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs3, htmls3));
                        string[] rowconfigs4 = { };
                        sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs4, htmls4));
                        string[] htmls5 = { "NULL", "签字", "", "", "", "", "", "", "", "", "" };
                        string[] rowconfigs5 = { };
                        sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs5, htmls5));
                    }
                }
            }
            return sb_row.ToString();
        }
        //计算最低平米钱数
        private decimal GetLowAllotMoney(decimal dlow, decimal allprt, decimal prsprt)
        {
            return dlow * allprt * prsprt * 0.01m;
        }
        /// <summary>
        /// 获取那种角色的行数最多
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public int[] Sort(int[] array)
        {
            int i, j;  // 循环变量
            int temp;  // 临时变量
            for (i = 0; i < array.Length - 1; i++)
            {
                for (j = 0; j < array.Length - 1 - i; j++)
                {
                    if (array[j] < array[j + 1])
                    {
                        // 交换元素
                        temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }
            return array;
        }
        /// <summary>
        /// 生成分配底部汇总表
        /// </summary>
        /// <returns></returns>
        protected string CreateAllotBottom()
        {
            StringBuilder sb_btm = new StringBuilder();
            //列数量
            int icolumn = 11;
            //合并统计列
            string[] configs_all = { };
            string[] rowconfigs_all = { };
            string[] htmls_all = { "<span ids=\"partFourStatisticsSpan\" id=\"bt_danwei_all\"  class=\"cls_cell_allcount\">0</span>", "<span ids=\"partFourStatisticsSpan\" id=\"bt_danweip_all\" class=\"cls_cell_allcount\">0</span>", "<span ids=\"partFourStatisticsSpan\" id=\"bt_b1_all\" class=\"cls_cell_allcount\">0</span>", "<span ids=\"partFourStatisticsSpan\" id=\"bt_b2_all\" class=\"cls_cell_allcount\">0</span>", "<span ids=\"partFourStatisticsSpan\" id=\"bt_b3_all\" class=\"cls_cell_allcount\">0</span>", "<span ids=\"partFourStatisticsSpan\" id=\"bt_b4_all\" class=\"cls_cell_allcount\">0</span>", "<span ids=\"partFourStatisticsSpan\" id=\"bt_c1_all\" class=\"cls_cell_allcount\">0</span>", "<span ids=\"partFourStatisticsSpan\" id=\"bt_c2_all\" class=\"cls_cell_allcount\">0</span>", "<span ids=\"partFourStatisticsSpan\" id=\"bt_a_all\" class=\"cls_cell_allcount\">0</span>", "<span ids=\"partFourStatisticsSpan\" id=\"bt_d_all\" class=\"cls_cell_allcount\">0</span>", "<span ids=\"partFourStatisticsSpan\" id=\"bt_e_all\" class=\"cls_cell_allcount\">0</span>" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs_all, rowconfigs_all, htmls_all));
            //列配置
            string[] configs = { "2|2", "4|2", "7|5" };
            //行配置
            string[] rowconfigs = { "1|4", "6|4", "7|4" };
            //单元格控件
            string strallcount = this.hid_collegemng.Value;
            string[] htmls = { "总体核验", "院留费用合计", "<span id=\"allcount_hy\" class=\"cls_allcount\">" + strallcount + "</span>", "备注", "<textarea id=\"allsub\" class=\"cls_mark\" ></textarea>" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs, rowconfigs, htmls));

            string[] rowconfigs2 = { };
            string allcount_pro = (Convert.ToDecimal(this.hid_projallot.Value) + Convert.ToDecimal(this.hid_projchrg.Value)).ToString();
            string[] htmls2 = { "NULL", "项目费用合计", "<span id=\"allcount_pro\" class=\"cls_allcount\">" + allcount_pro + "</span>", "NULL", "NULL" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs, rowconfigs2, htmls2));

            string[] rowconfigs3 = { };
            string allcount_user = this.hid_dusercount.Value;
            string[] htmls3 = { "NULL", "设计人员合计", "<span id=\"allcount_user\" class=\"cls_allcount\">" + allcount_user + "</span>", "NULL", "NULL" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs, rowconfigs3, htmls3));

            string[] rowconfigs4 = { };
            string allcount_all = this.hid_projallcount.Value;
            string[] htmls4 = { "NULL", "总计", "<span id=\"allcount_all\" class=\"cls_allcount\">" + allcount_all + "</span>", "NULL", "NULL" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs, rowconfigs4, htmls4));

            //底角签名
            string[] configs_bt = { };
            string[] rowconfigs5 = { };
            string[] htmls5 = { "院长", "<input rel=\"valide\" class=\"cls_cell\" id=\"collegeuser\" value=\"\"/>", "生产院长", "<input rel=\"valide\"  class=\"cls_cell\" id=\"collegeuser2\" value=\"\"/>", "生产经营", "<input rel=\"valide\"  class=\"cls_cell\" id=\"collegeuser3\" value=\"\"/>", "项目经理", "<input rel=\"valide\"  class=\"cls_cell\" id=\"collegeuser4\" value=\"\"/>", "制表人", "<input rel=\"valide\"  class=\"cls_cell\" id=\"collegeuser5\" value=\"\"/>", "" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs_bt, rowconfigs5, htmls5));
            //操作
            string[] configs_oper = { "1|11" };
            string[] rowconfigs_oper = { };
            string[] htmls_oper = { "<input type=\"button\" value=\"保存\" class=\"cls_oper\" id=\"partThreeSaveButton\"/>&nbsp;<input type=\"button\" value=\"返回\" class=\"cls_oper\" id=\"btn_back\"/>" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs_oper, rowconfigs_oper, htmls_oper));

            return sb_btm.ToString();
        }
        //保存第三和第四部分
        [AjaxMethod]
        public string SavePartThreeAndPartFourRecord(string partThreeJsonData, string partFourJsonData, string costid, string cprid, string proid, string proname, int userno)
        {
            PartThree partThreeEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<PartThree>(partThreeJsonData);

            PartFour partFourEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<PartFour>(partFourJsonData);

            int count = new TG.BLL.ProjectAllotBP().SavePartThreeAndPartFourRecord(partThreeEntity, partFourEntity);

            if (count > 0)
            {
                SysMessageViewEntity msg = new SysMessageViewEntity
                {
                    ReferenceSysNo = "cost_id=" + costid + "&projectSysNo=" + proid + "&cprid=" + cprid + "&ProcessType=1",
                    FromUser = userno,
                    InUser = UserSysNo,
                    MsgType = 5,
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", proname, "项目产值分配第三部分通过消息"),
                    QueryCondition = proname,
                    Status = "A",
                    ToRole = "0"
                };
                new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
            }

            return count + "";
        }
        //获取专业ID
        /// <summary>
        /// 查询所有专业
        /// </summary>
        /// <returns></returns>
        [AjaxMethod]
        public string GetAllSpeciality()
        {
            List<string> specialityList = new TG.BLL.ProjectAllotBP().GetAllSpeciality();

            return Newtonsoft.Json.JsonConvert.SerializeObject(specialityList);
        }
        /// <summary>
        /// 获取第一和第二部分
        /// </summary>
        /// <param name="costid"></param>
        protected void GetPartOneAndPartTwoInfo()
        {
            //分配ID
            string costid = Request.QueryString["costid"] ?? "0";
            //项目ID
            string proid = Request.QueryString["proid"] ?? "0";
            //第一部分用户ID
            //string oneuserid = Request.QueryString["PartOneRecordInUserSysNo"] ?? "0";
            //this.HiddenPartOneRecordInUserSysNo.Value = oneuserid;
            //合同ID
            string cprid = Request.QueryString["cprid"] ?? "0";
            this.hid_cprid.Value = cprid;
            //项目名
            this.hid_proname.Value = projectName;

            TG.Model.PartOne model_one = new TG.BLL.ProjectAllotBP().GetPartOneRecord(int.Parse(costid));
            HasDone = model_one.Status;
            this.HiddenPartOneRecordInUserSysNo.Value = model_one.InUser.ToString();
            TG.Model.PartTwo model_two = new TG.BLL.ProjectAllotBP().GetPartTwoRecord(int.Parse(costid));

            this.hid_costid.Value = costid;
            this.hid_proid.Value = proid;
            //分成税后金额
            this.hid_allcount.Value = model_two.TaxSignelRate.ToString();
            //最低收费税率
            this.hid_lowerallot.Value = model_one.LowRateUnitArea.ToString();
            //院务管理
            this.hid_collegemng.Value = model_two.CollegeRate.ToString();
            //项目费用
            this.hid_projallot.Value = model_two.ProjectRate.ToString();
            //项目负责
            this.hid_projchrg.Value = model_two.ProChgRate.ToString();
            //结构类型
            this.hid_structype.Value = model_one.StrucType.ToString();

            decimal D42, D43, D44;
            D42 = model_two.CollegeRate;
            D43 = model_two.ProjectRate + model_two.ProChgRate;
            //设计人员费用
            decimal F13, G13, H13, I13, J13;
            F13 = model_two.RegisterRate;
            G13 = model_two.ShexRate;
            H13 = model_two.ShedRate;
            I13 = model_two.ZonggongRate;
            J13 = model_two.YuanzRate;
            this.hid_dusercount.Value = Convert.ToString(F13 + G13 + H13 + I13 + J13);
            //D44
            D44 = F13 + G13 + H13 + I13 + J13;
            //所有统计费用
            this.hid_projallcount.Value = Convert.ToString(D42 + D43 + D44);
        }
    }
}
