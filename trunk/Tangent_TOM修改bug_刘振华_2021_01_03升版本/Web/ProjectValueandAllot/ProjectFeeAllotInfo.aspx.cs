﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using TG.Model;
using Newtonsoft.Json;
using System.Data;

namespace TG.Web
{
    public partial class ProjectFeeAllot : PageBase
    {
        protected override bool IsAuth
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 项目SysNo
        /// </summary>
        public int ProjectSysNo
        {
            get
            {
                int projectSysNo = 0;
                int.TryParse(Request["projectSysNo"], out projectSysNo);
                return projectSysNo;
            }
        }

        /// <summary>
        /// 产值分配ID
        /// </summary>
        public int PartOneSysNo
        {
            get
            {
                int partOneSysNo = 0;
                int.TryParse(Request["cost_id"], out partOneSysNo);
                return partOneSysNo;
            }
        }
        /// <summary>
        /// 合同ID
        /// </summary>
        public int CoperationSysNo
        {
            get;
            set;
        }

        /// <summary>
        ///操作类型
        /// </summary>
        public string ProcessType
        {
            get
            {
                return Request["ProcessType"];
            }
        }
        /// <summary>
        /// 项目
        /// </summary>
        public TG.Model.cm_Project Project { get; set; }
        /// <summary>
        /// 合同
        /// </summary>
        public TG.Model.cm_Coperation Coperation { get; set; }
        /// <summary>
        /// 权限
        /// </summary>
        public string HasPower { get; set; }
        /// <summary>
        /// 是否分配
        /// </summary>
        public string IsAllot
        {
            get;
            set;
        }

        public string AllotStatus { get; set; }

        #region 1-4 部分对象
        public PartOne PartOneEntity { get; set; }
        public PartTwo PartTwoEntity { get; set; }
        #endregion

        #region 配置
        public CostCompanyConfig CostCompanyConfig { get; set; }
        public CostUserConfig CostUserConfig { get; set; }
        public List<cm_CostSpeConfig> cm_CostSpeConfig { get; set; }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProjectFeeAllot));

            IsAllot = DBUtility.DbHelperSQL.GetSingle("select pro_ID from cm_ProjectCost where pro_ID=" + ProjectSysNo + " and isnull(Cprunitprice,0)<>0") == null ? "0" : "1";

            if (!IsPostBack)
            {
                GetProInfo();
                GetConfigRecord();
                //多次分配
                GetPartOneRecord();
                if (IsAllot == "1")
                {
                    GetPartOneCount();
                }
                //绑定结构类型
                BindStructType();
            }
            int position = 0;
            switch (PartOneEntity.Status)
            {
                case "A":
                    position = 1;
                    break;
                case "B":
                    position = 2;
                    //GetPartTwoRecord();
                    break;
                case "C":
                    GetPartTwoRecord();
                    position = 3;
                    break;
            }
            int roleSysNo = new TG.BLL.ProjectAllotBP().GetRoleSysNoByPosition(position);

            //编辑的场合
            HasPower = new TG.BLL.cm_Role().CheckPower(roleSysNo, UserSysNo) == true ? "1" : "0";
            //HasPower = "1";
            //显示结构
            if (PartOneEntity.StrucType != 0)
            {
                this.lbl_structype.Visible = true;
                this.lbl_structype.Text = this.drp_strutype.Items.FindByValue(PartOneEntity.StrucType.ToString()).Text;
                this.drp_strutype.Visible = false;
            }
            //判断时间
            if (PartOneEntity.InDate == DateTime.Parse("0001/1/1 0:00:00"))
            {
                PartOneEntity.InDate = DateTime.Now;
            }
        }

        public void BindStructType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            this.drp_strutype.DataSource = bll_dic.GetList(" dic_Type='proj_fp'");
            this.drp_strutype.DataTextField = "dic_Name";
            this.drp_strutype.DataValueField = "ID";
            this.drp_strutype.DataBind();
        }
        /// <summary>
        /// 第一部分的信息
        /// </summary>
        public void GetPartOneCount()
        {
            //第一部分的信息
            PartOne partOneTemp = new TG.BLL.ProjectAllotBP().GetPartOneRecordCprunitprice(ProjectSysNo.ToString());

            PartOneEntity.CprUnitprice = partOneTemp.CprUnitprice;
            PartOneEntity.CprCharge = partOneTemp.CprCharge;
            //当前合同额
            decimal allcount = PartOneEntity.CprCharge;
            Page.ClientScript.RegisterStartupScript(typeof(Page), "", "<script type=\"text/javascript\">$(function(){$(\"#partOnecprunitpricetext\").attr(\"disabled\",\"disabled\") });</script>");
            this.HiddenSubAllot.Value = Convert.ToString(allcount - GetReadyAllot());
        }
        //获取已分配合同额
        public decimal GetReadyAllot()
        {
            decimal result = 0m;
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
            string strSql = " SELECT Sum(CurCharge) FROM dbo.cm_ProjectCost WHERE pro_ID=" + ProjectSysNo;
            DataSet ds = bll.GetList(strSql);
            if (ds.Tables.Count > 0)
            {
                result = Convert.ToDecimal(ds.Tables[0].Rows[0][0].ToString());
            }

            //返回结果
            return result;
        }

        /// <summary>
        /// 第一部分信息
        /// </summary>
        public void GetPartOneRecord()
        {
            PartOneEntity = new TG.BLL.ProjectAllotBP().GetPartOneRecord(PartOneSysNo);
            AllotStatus = PartOneEntity.Status;
        }
        /// <summary>
        /// 第二部分信息
        /// </summary>
        private void GetPartTwoRecord()
        {
            PartTwoEntity = new TG.BLL.ProjectAllotBP().GetPartTwoRecord(PartOneSysNo);
        }
        /// <summary>
        /// 合同信息
        /// </summary>
        public void GetCoperationInfo()
        {
            Coperation = new TG.BLL.cm_Coperation().GetModel(CoperationSysNo);
        }
        /// <summary>
        /// 配置分成比例
        /// </summary>
        private void GetConfigRecord()
        {
            TG.BLL.ProjectAllotBP bp = new TG.BLL.ProjectAllotBP();
            CostCompanyConfig = bp.GetCostCompanyConfig();
            CostUserConfig = bp.GetCostUserConfig();
        }
        #region AjaxMethod
        [AjaxMethod]
        public string SavePartOneRecord(string partOneJsonData)
        {
            PartOne allotDetailEntity = null;
            try
            {
                allotDetailEntity = JavaScriptConvert.DeserializeObject<PartOne>(partOneJsonData);
                allotDetailEntity.Status = "B";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            string sql = string.Format(@" update cm_ProjectCost set
                                                DesignNum =N'{0}',
                                                BuildArea = {1},
                                                CprCharge ={2},
                                                Cprunitprice={3},
                                                CurCharge={4},
                                                CurRate={5},
                                                TaxRate={6},
                                                LowDivUnitArea={7},
                                                LowDivUnitPrice={8},
                                                LowRateUnitArea={9},
                                                LowRateUnitPrice={10},
                                                Status = N'{11}',
                                                StrucType ={12},
                                                InDate=N'{14}' 
                                                where ID={13}", allotDetailEntity.DesignNum, allotDetailEntity.BuildArea, allotDetailEntity.CprCharge, allotDetailEntity.CprUnitprice, allotDetailEntity.CurChange, allotDetailEntity.CurRate, allotDetailEntity.TaxRate, allotDetailEntity.LowDivUnitArea, allotDetailEntity.LowDivUnitPrice, allotDetailEntity.LowRateUnitArea, allotDetailEntity.LowRateUntiPrice, allotDetailEntity.Status, allotDetailEntity.StrucType, allotDetailEntity.SysNo, allotDetailEntity.InDate);

            int count = TG.DBUtility.DbHelperSQL.ExecuteSql(sql);

            if (count > 0)
            {
                //得到项目信息
                TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(allotDetailEntity.ProjectSysNo);

                SysMessageViewEntity msg = new SysMessageViewEntity
                {
                    ReferenceSysNo = "cost_id=" + allotDetailEntity.SysNo + "&projectSysNo=" + allotDetailEntity.ProjectSysNo,
                    FromUser = UserSysNo,
                    InUser = UserSysNo,
                    MsgType = 5,
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project == null ? "" : project.pro_name, "项目产值分配审核"),
                    QueryCondition = project == null ? "" : project.pro_name,
                    Status = "A",
                    ToRole = new TG.BLL.ProjectAllotBP().GetRoleSysNoByPosition(2) + ""
                };
                new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
            }
            return count + "";
        }

        [AjaxMethod]
        public string SavePartTwoRecord(string partTwoJsonData)
        {
            PartTwo partTwoEntity = null;
            try
            {
                partTwoEntity = JavaScriptConvert.DeserializeObject<PartTwo>(partTwoJsonData);
            }
            catch (System.Exception ex)
            {

            }

            int count = new TG.BLL.ProjectAllotBP().SavePartTwoRecord(partTwoEntity);

            if (count > 0)
            {
                PartOne partOneViewEntity = new TG.BLL.ProjectAllotBP().GetPartOneRecord(partTwoEntity.PartOneSysNo);
                //得到项目信息
                TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(partOneViewEntity.ProjectSysNo);

                SysMessageViewEntity msg = new SysMessageViewEntity
                {
                    ReferenceSysNo = "cost_id=" + partTwoEntity.PartOneSysNo + "&projectSysNo=" + partOneViewEntity.ProjectSysNo,
                    FromUser = UserSysNo,
                    InUser = UserSysNo,
                    MsgType = 5,
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project == null ? "" : project.pro_name, "项目产值分配审核"),
                    QueryCondition = project == null ? "" : project.pro_name,
                    Status = "A",
                    ToRole = new TG.BLL.ProjectAllotBP().GetRoleSysNoByPosition(3) + ""
                };
                new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
            }
            return count + "";
        }

        /// <summary>
        /// 查询所有专业
        /// </summary>
        /// <returns></returns>
        [AjaxMethod]
        public string GetAllSpeciality()
        {
            List<string> specialityList = new TG.BLL.ProjectAllotBP().GetAllSpeciality();

            return JavaScriptConvert.SerializeObject(specialityList);
        }
        /// <summary>
        /// 单独得到第三部分数据
        /// </summary>
        /// <param name="partOneSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string GetPartThreeRecord(string partOneSysNo)
        {
            PartThree partThree = new TG.BLL.ProjectAllotBP().GetPartThreeRecord(int.Parse(partOneSysNo));

            Dictionary<string, List<PartThreeTableViewEntity>> drc = partThree.ResolveJSON(partThree.DesignUserPay);

            return JavaScriptConvert.SerializeObject(partThree);
        }

        /// <summary>
        /// 得到用户配置
        /// </summary>
        /// <param name="projectSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string GetAllotUsers(int projectSysNo)
        {
            Dictionary<string, List<AllotUserViewEntity>> allotUserList = new TG.BLL.ProjectAllotBP().GetAllotUsers(projectSysNo);

            return JavaScriptConvert.SerializeObject(allotUserList);
        }

        [AjaxMethod]
        public string DeleteAllRecord(string costSysNo)
        {
            return new TG.BLL.ProjectAllotBP().DeleteAllRecord(int.Parse(costSysNo)) + "";
        }

        //显示项目,合同信息
        public void GetProInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(ProjectSysNo);
            Project = pro_model;
            //项目名称
            txtprojectname.Text = Convert.ToString(pro_model.pro_name ?? "").Trim();
            txtdesignNo.Text = new TG.BLL.cm_projectNumber().GetProjectJobNumberByProjectSysNo(ProjectSysNo);
            //工程等级
            string dj = pro_model.pro_level.ToString();
            if (dj == "1")
                txtprorate.Text = "一级";
            if (dj == "2")
                txtprorate.Text = "二级";
            if (dj == "3")
                txtprorate.Text = "三级";
            //建筑面积
            partOneBuildAreatext.Text = Convert.ToString(pro_model.ProjectScale ?? 0);
            this.partOneSumtext.Text = this.partOneBuildAreatext.Text;
            //显示合同信息
            int strcprid = pro_model.CoperationSysNo;
            CoperationSysNo = pro_model.CoperationSysNo;
            TG.BLL.cm_Coperation cpr = new TG.BLL.cm_Coperation();
            TG.Model.cm_Coperation cpr_model = cpr.GetModel(strcprid);
            //项目经理
            txtProPerson.Text = Convert.ToString(cpr_model.ChgJia ?? "").Trim();
            //建设单位
            txtBuildUnit.Text = Convert.ToString(cpr_model.BuildUnit ?? "").Trim();
            //合同编号
            txtcprNo.Text = Convert.ToString(cpr_model.cpr_No ?? "").Trim();
        }
        #endregion


    }
}
