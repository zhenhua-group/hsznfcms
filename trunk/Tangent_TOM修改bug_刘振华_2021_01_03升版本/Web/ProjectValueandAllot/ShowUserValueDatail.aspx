﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowUserValueDatail.aspx.cs"
    Inherits="TG.Web.ProjectValueandAllot.ShowUserValueDatail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectValueandAllot.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/MessageComm.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            CommonControl.SetFormWidth();
            //Tab页
            $("#tabsMemAmount").tabs();

        });
    </script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                当前位置：[个人产值分配明细]
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="cls_head_bar">
                <table class="cls_head_div">
                    <tr>
                        <td>
                            项目名称：
                            <asp:Label ID="lblCprName" runat="server"></asp:Label>：个人产值分配明细如下
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:Literal ID="lbl_Detail" runat="server"></asp:Literal>
    <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="Table1">
        <tr>
            <td style="width: 100%; text-align: center;">
                <input type="button" id="Button1" name="controlBtn" class="cls_btn_comm_w" value="返回"
                    onclick="javascript:history.back();" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
