﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="CostDetailsBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.CostDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/ProjectValueandAllot/CostDetailsBymaster_jq.js" type="text/javascript"></script>
    <script src="../js/ProjectValueandAllot/CostDetailsBymaster.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>设计所成本明细</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>财务管理 </a><i class="fa fa-angle-right"></i><a>设计所成本明细</a> </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>查询成本明细
                    </div>

                </div>
                <div class="portlet-body" style="display: block;">
                    <table id="tbl_id" class="table-responsive">
                        <tr>
                            <td>部门:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" Width="120px" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>成本类型:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_costTypeAll" Width="120px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----选择类型-----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>日期:
                            </td>
                            <td>
                                <input id="txt_costdate1" type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" runat="server" />至
                                         <input id="txt_costdate2" type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>明细:</td>
                            <td>
                                <input type="text" class="form-control input-sm" id="txt_keyname" runat="server" />
                            </td>
                            <td>
                                <input type="button" class="btn blue" value="查询" id="btn_search" />
                            </td>
                            <td colspan="3">
                                <a class="btn blue" id="btn_createCost" href="#DivAddCost" data-toggle="modal">添加成本</a>
                                <a class="btn blue" id="btn_Import" href="CostImportBymaster.aspx">导入成本</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>设计所成本明细列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <%--OnClick="btn_export_Click"--%>
                        <asp:Button Text="导出Excel" runat="server" CssClass="btn red btn-sm" ID="btn_export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">

                    <table id="jqGrid">
                    </table>
                    <div id="gridpager">
                    </div>
                    <div id="nodata" class="norecords">
                        没有符合条件数据！
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--弹出层-->
    <!--增加-->
    <div id="DivAddCost" class="modal fade yellow"
        tabindex="-1" data-width="600" aria-hidden="true" style="display: none; width: 600px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">增加成本明细
            </h4>
        </div>
        <div class="modal-body">

            <table class="table table-bordered">
                <tr>
                    <td>部门：</td>
                    <td>
                        <asp:DropDownList ID="drp_unitAdd" CssClass="form-control input-sm" runat="server" AppendDataBoundItems="True" Width="167px">
                            <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                        </asp:DropDownList>
                        <span style="color: red; display: none;" id="drp_unitSpan">请输入汇款人!</span></td>
                    <td>类型：</td>
                    <td>
                        <asp:DropDownList ID="drp_costType" CssClass="form-control input-sm" runat="server" AppendDataBoundItems="True" Width="167px">
                            <asp:ListItem Value="-1">-----选择类型-----</asp:ListItem>
                        </asp:DropDownList>
                        <span style="color: red; display: none;" id="drp_costTypeSpan">请输入汇款人!</span></td>
                </tr>
                <tr>
                    <td>日期：</td>
                    <td>
                        <input id="txt_costdate" type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" style="height: 30px; border: 1px solid #e5e5e5;" />
                        <span style="color: red; display: none;" id="txt_costdateSpan">请输入日期时间!</span></td>
                    <td>凭证号：</td>
                    <td>
                        <input type="text" class="form-control input-sm" id="costNum" maxlength="10" />
                        <span style="color: red; display: none;" id="costNumSpan">请输入汇款人!</span>
                    </td>
                </tr>
                <tr>
                    <td>金额：</td>
                    <td>
                        <input type="text" class="form-control input-sm" style="width: 80%;" id="costcharge" placeholder="元" />
                        <span style="color: red; display: none;" id="costchargeSpan">请输入汇款人!</span>
                    </td>
                    <td>借出金额：</td>
                    <td>
                        <input type="text" class="form-control input-sm" style="width: 80%;" id="costOutcharge" placeholder="元" />
                        <span style="color: red; display: none;" id="costoutchargeSpan">请输入汇款人!</span>
                    </td>
                </tr>
                <tr>
                    <td>备注：</td>
                    <td colspan="3">
                        <textarea id="remark" style="width: 400px; height: 50px"></textarea>

                        <span style="color: red; display: none;" id="remarkSpan">请输入汇款人!</span>
                    </td>

                </tr>
            </table>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn red btn-default" id="btn_addcount">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn
    btn-default">
                取消</button>
        </div>
    </div>
    <div id="DivShowCost" class="modal fade yellow"
        tabindex="-1" data-width="600" aria-hidden="true" style="display: none; width: 600px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">查看成本明细
            </h4>
        </div>
        <div class="modal-body">

            <table class="table table-bordered">
                <tr>
                    <td>部门：</td>
                    <td>
                        <label id="labUnitCost"></label>
                        <td>类型：</td>
                    <td>
                        <label id="labTypeCost"></label>
                </tr>
                <tr>
                    <td>日期：</td>
                    <td>
                        <label id="labTime"></label>
                    </td>
                    <td>凭证号：</td>
                    <td>
                        <label id="labNum"></label>
                    </td>
                </tr>
                <tr>
                    <td>金额：</td>
                    <td>
                        <label id="labCharge"></label>
                    </td>
                    <td>借出金额：</td>
                    <td>
                        <label id="labOutCharge"></label>
                    </td>
                </tr>
                <tr>
                    <td>备注：</td>
                    <td colspan="3">
                        <label id="labSub" style="height: 50px; width: 400px;"></label>
                    </td>

                </tr>
            </table>

        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn
    btn-default">
                关闭</button>
        </div>
    </div>
    <!--选择导出类型-->
    <%--    <div id="DivReportType" class="modal fade yellow"
        tabindex="-1" data-width="600" aria-hidden="true" style="display: none; width: 600px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择导出类型
            </h4>
        </div>
        <div class="modal-body">
            

        </div>
        <div class="modal-footer">
           <button type="button" data-dismiss="modal" id="btn_report_close" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn
    btn-default">
                取消</button>
        </div>
    </div>--%>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField runat="server" ID="hidUserId" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="HiddenISImport" runat="server" Value="4" />
</asp:Content>
