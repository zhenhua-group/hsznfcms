﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using System.Linq;
using Newtonsoft.Json;
using TG.Model;
using System.Text;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System.IO;
using System.Data;
using System.Configuration;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProSecondValueAllotAuditBymaster : PageBase
    {
        #region QueryString

        /// <summary>
        /// 项目系统号
        /// </summary>
        public int ProSysNo { get; set; }

        /// <summary>
        /// 实收金额
        /// </summary>
        public decimal PayShiAccount { get; set; }

        /// <summary>
        /// 分配金额
        /// </summary>
        public decimal TotalAllotCount { get; set; }


        /// <summary>
        /// tg中项目
        /// </summary>
        public int ProReferenceSysNo { get; set; }

        /// <summary>
        /// 是否可以编辑
        /// </summary>
        public int IsParamterEdit { get; set; }

        /// <summary>
        /// 专业负责人
        /// </summary>
        public string SpetailtyHeadUser { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string SpeName { get; set; }

        /// <summary>
        /// 审核记录系统编号
        /// </summary>
        public int ValueAllotAuditSysNo
        {
            get
            {
                int valueAllotAuditSysNo = 0;
                int.TryParse(Request["ValueAllotAuditSysNo"], out valueAllotAuditSysNo);
                return valueAllotAuditSysNo;
            }
        }
        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }
        /// <summary>
        /// 消息状态
        /// </summary>
        public string MessageStatus
        {
            get
            {
                return Request["MessageStatus"];
            }
        }
        /// <summary>
        /// 合同系统阶段
        /// </summary>
        public int CoperationProcess { get; set; }
        public string TypeMark { get; set; }
        /// <summary>
        /// 是否四舍五入
        /// </summary>
        public string IsRounding = ConfigurationManager.AppSettings["IsRounding"] ?? "0";

        /// <summary>
        /// 专业审核人员
        /// </summary>
        public int SpecialityAuditUser { get; set; }

        /// <summary>
        /// 部门信息
        /// </summary>
        public string UnitName { get; set; }

        public string LikeType
        {
            get
            {
                return Request["likeType"];
            }
        }
        #endregion

        #region Property
        /// <summary>
        /// 合同基本信息实体
        /// </summary>
        public TG.Model.cm_Coperation Coperation { get; set; }

        /// <summary>
        /// 合同审核状态
        /// </summary>
        public string AuditStatus { get; set; }


        /// <summary>
        /// 是否有权限
        /// </summary>
        public string HasPower { get; set; }

        public string CoperationTypeHTMLString { get; set; }

        /// <summary>
        /// 审核记录实体
        /// </summary>
        public TG.Model.cm_ProjectValueAuditRecord projectAuditRecordEntity { get; set; }

        TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();

        public string year = "";
        #endregion

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(TG.Web.ProjectValueandAllot.ProValueAllotAuditBymaster));
            if (!IsPostBack)
            {

                //得到审核信息
                GetAuditInfo();

                //得到项目名称
                getProInfo();

                //得到基本信息
                GetConperationBaseInfo();

                //绑定产值分配
                if (UnitName != null)
                {
                    if (UnitName.Contains("暖通"))
                    {
                        bindHaveValue();
                    }
                    else
                    {
                        bindValue();
                    }

                    //绑定分配之后的信息
                    if (UnitName.Contains("暖通"))
                    {
                        bindHaveValuedData();
                    }
                    else
                    {
                        bindValuedData();
                    }
                }
                //绑定人员产值
                bindProjectValueByMember();

                //绑定人员产值
                bindProjectValueByMemberAmount();
            }
        }
        /// <summary>
        /// 项目信息
        /// </summary>
        private void getProInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(ProSysNo);

            if (pro_model != null)
            {
                //项目名称
                lblProjectName.Text = pro_model.pro_name;
                //关联合同
                this.lblReletive.Text = pro_model.Project_reletive;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }

                //建设规模
                this.lblScale.Text = pro_model.ProjectScale.ToString();
                //建筑类别
                lblBuildType.Text = pro_model.BuildType;
                //项目总负责
                this.lblPMName.Text = pro_model.PMName;
                //承接部门
                lblcpr_Unit.Text = pro_model.Unit;

                UnitName = pro_model.Unit;

                //项目阶段

                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }

                //tg中项目编号
                ProReferenceSysNo = pro_model.ReferenceSysNo;

                //合同额
                lblCoperationAmount.Text = pro_model.Cpr_Acount.ToString();

                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }

                int cprID = pro_model.CoperationSysNo;
                //显示项目信息
                TG.BLL.cm_Coperation cop = new TG.BLL.cm_Coperation();
                TG.Model.cm_Coperation cop_model = cop.GetModel(cprID);

                //是否可以编辑
                if (cop_model != null)
                {
                    IsParamterEdit = int.Parse(cop_model.IsEdit);
                }
                else
                {
                    IsParamterEdit = 0;
                }
            }

        }

        /// <summary>
        /// 得到基本信息
        /// </summary>
        private void GetConperationBaseInfo()
        {
            TG.BLL.cm_ProjectValueAllot BLL = new TG.BLL.cm_ProjectValueAllot();

            DataTable dt = BLL.GetSecondValueCurrent(year, UnitName).Tables[0];

            if (dt != null && dt.Rows.Count > 0)
            {
                //总金额金额
                lblTotalCount.Text = dt.Rows[0]["totalCount"].ToString();
                //分配金额
                lblAllotAccount.Text = dt.Rows[0]["allountCount"].ToString();

                var programCount = dt.Rows[0]["ProgramCount"].ToString();
                var tranAllotCount = dt.Rows[0]["TranAllotCount"].ToString();
                // 未分配金额
                lblNotAllotAccount.Text = (decimal.Parse(dt.Rows[0]["totalCount"].ToString()) + decimal.Parse(tranAllotCount) - decimal.Parse(dt.Rows[0]["allountCount"].ToString()) - decimal.Parse(programCount)).ToString("f2");

            }
        }

        /// <summary>
        /// 得到审核信息
        /// </summary>
        private void GetAuditInfo()
        {
            projectAuditRecordEntity = BLL.GetModel(ValueAllotAuditSysNo);
            if (projectAuditRecordEntity != null)
            {
                //查询合同系统号
                ProSysNo = projectAuditRecordEntity.ProSysNo;

                //查询消息审核记录
                GetCoperationAuditRecord();

                //分配详细信息
                GetAllotInfo();
            }
            else
            {
                lblNotData.Text = "<table id=\"tbdelete\" align=\"center\"><tr><td style=\" font-size: 12px;font-weight: bold; color: Red;\"  >此信息已被删除!</td></tr></table>";
            }
        }
        /// <summary>
        /// 查询消息审核记录
        /// </summary>
        private void GetCoperationAuditRecord()
        {

            AuditStatus = projectAuditRecordEntity == null ? "" : projectAuditRecordEntity.Status;
            if (!string.IsNullOrEmpty(projectAuditRecordEntity.AuditUser))
            {
                string[] tempArr = projectAuditRecordEntity.AuditUser.Split(',');
                if (tempArr != null && tempArr.Length > 0)
                {
                    foreach (var item in tempArr)
                    {
                        TG.Model.tg_member user = new TG.Model.tg_member();
                        //查询联系人姓名
                        if (!item.Contains('{'))
                        {
                            user = new TG.BLL.tg_member().GetModel(int.Parse(item));
                        }

                        projectAuditRecordEntity.AuditUserString = string.IsNullOrEmpty(projectAuditRecordEntity.AuditUser) ? user.mem_Name : projectAuditRecordEntity.AuditUserString + user.mem_Name + ",";
                    }
                    projectAuditRecordEntity.AuditUserString = projectAuditRecordEntity.AuditUserString.Substring(0, projectAuditRecordEntity.AuditUserString.Length - 1);
                }
            }

            if (AuditStatus != "J")
            {

                SpetailtyHeadUser = GetSpetailtyHeadUser();
            }
        }

        /// <summary>
        /// 得到专业负责人
        /// </summary>
        /// <returns></returns>
        public string GetSpetailtyHeadUser()
        {
            string userName = "";
            TG.Model.tg_member user = new TG.Model.tg_member();
            if (!string.IsNullOrEmpty(projectAuditRecordEntity.AuditUser))
            {
                string[] tempArr = projectAuditRecordEntity.AuditUser.Split(',');
                if (tempArr != null && tempArr.Length > 0)
                {
                    foreach (var item in tempArr)
                    {
                        if (item.Contains('{'))
                        {
                            string[] tempUser = item.Replace("{", "").Replace("}", "").ToString().Split(';');
                            foreach (var temp in tempUser)
                            {
                                user = new TG.BLL.tg_member().GetModel(int.Parse(temp));
                                userName = string.IsNullOrEmpty(userName) ? user.mem_Name : userName + ";" + user.mem_Name;
                            }
                            break;
                        }
                    }
                }
            }

            return userName;
        }

        /// <summary>
        /// 得到分配信息
        /// </summary>
        private void GetAllotInfo()
        {

            if (projectAuditRecordEntity.AllotID != null)
            {
                DataTable dt = BLL.GetAllotDataList(projectAuditRecordEntity.AllotID.ToString()).Tables[0];
                TG.Model.cm_ProjectValueAllot allot = new TG.Model.cm_ProjectValueAllot();
                TG.BLL.cm_ProjectValueAllot bllAllot = new BLL.cm_ProjectValueAllot();
                allot = bllAllot.GetModel(projectAuditRecordEntity.AllotID);

                txt_ActulCount.Text = allot == null ? "0" : allot.AllotCount.ToString();
                string stage = allot == null ? "0" : allot.Itemtype.ToString();
                lblStage.Text = setProcessText(stage);
                CoperationProcess = int.Parse(stage);
                //备注

                TypeMark = allot == null ? "0" : allot.mark;

                lblYear.Text = allot.ActualAllountTime;
                year = allot.ActualAllountTime;
                lblType.Text = allot.BulidType;
            }

        }

        /// <summary>
        /// 绑定产值分配
        /// </summary>
        private void bindValue()
        {
            if (AuditStatus == "F" || AuditStatus == "I")
            {
                string itemType = lblType.Text.Trim();

                if (!string.IsNullOrEmpty(itemType))
                {

                    decimal allotCount = decimal.Parse(txt_ActulCount.Text);

                //参与策划人员的专业
                DataTable dtPlanSpecialty = BLL.GetPlanUserSpecialty(ProSysNo).Tables[0];

                string specialty = "";
                foreach (DataRow row in dtPlanSpecialty.Rows)
                {
                    specialty += "'" + row["spe_Name"].ToString() + "'" + ",";
                }

                if (!string.IsNullOrEmpty(specialty))
                {
                    specialty = specialty.Remove(specialty.Length - 1);


                    DataSet dsAllot = BLL.GetCprProcessValueDataList(itemType, CoperationProcess, allotCount, specialty, IsRounding);

                    //设计阶段
                    DataTable dtStageValue = dsAllot.Tables[0];

                    //工序
                    DataTable dtProcessValue = dsAllot.Tables[2];

                    //设计阶段与专业
                    DataTable dtStageSpeValue = new DataView(dsAllot.Tables[1]) { RowFilter = "IsHaved=0" }.ToTable();

                    //生成方案设计 初步设计 施工图 后期服务表单
                    if (CoperationProcess < 4 || CoperationProcess == 10)
                    {
                        CreateTable(dtStageValue, dtStageSpeValue, dtProcessValue);
                    }
                    else
                    {
                        DataTable dt3 = new DataView(dsAllot.Tables[3]) { RowFilter = "IsHaved=0" }.ToTable();
                        if (CoperationProcess == 5)
                        {
                            dt3 = new DataView(dt3) { RowFilter = "type like '" + TypeMark + "'" }.ToTable();
                        }
                        CreateOtherTable(dt3, dtProcessValue, allotCount);
                    }

                    gvOne.DataSource = dtStageValue;
                    gvOne.DataBind();

                    gvTwo.DataSource = dtStageValue;
                    gvTwo.DataBind();

                    gvThree.DataSource = dtStageValue;
                    gvThree.DataBind();

                    gvFour.DataSource = dtStageValue;
                    gvFour.DataBind();

                    //方案+施工图+后期
                    gvTen.DataSource = dtStageValue;
                    gvTen.DataBind();
                }
                else
                {
                    StringBuilder sbSpeHead = new StringBuilder();
                    sbSpeHead.Append(" <table class=\"cls_table_head\" id=\"tbNoData\" style=\"display: none;\">");
                    sbSpeHead.Append("<tr>");
                    sbSpeHead.Append("<td style=\"color: Red;\">没有查询到信息。请检查项目策划是否分配正确、或者产值分配参数是否设置正确</td>");
                    sbSpeHead.Append("</tr></table>");
                    lbl_NoData.Text = sbSpeHead.ToString();
                    }
                }
            }
        }


        /// <summary>
        /// 绑定分配后的产值
        /// </summary>
        private void bindValuedData()
        {
            if (AuditStatus == "H" || AuditStatus == "J" || AuditStatus == "L" || AuditStatus == "N" || AuditStatus == "M" || AuditStatus == "O" || AuditStatus == "P" || AuditStatus == "Q" || AuditStatus == "K")
            {

                DataSet ds = BLL.GetValuedDataList((int)projectAuditRecordEntity.AllotID);

                gvStageOne.DataSource = ds.Tables[0];
                gvStageOne.DataBind();

                gvStageTwo.DataSource = ds.Tables[0];
                gvStageTwo.DataBind();

                gvStageThree.DataSource = ds.Tables[0];
                gvStageThree.DataBind();

                gvStageFour.DataSource = ds.Tables[0];
                gvStageFour.DataBind();

                gvStageTen.DataSource = ds.Tables[0];
                gvStageTen.DataBind();

                DataTable dtProcess = ds.Tables[2];

                //生成方案设计 初步设计 施工图 后期服务表单
                if (CoperationProcess < 4 || CoperationProcess == 10)
                {
                    CreateSpeAmountTable(ds.Tables[1]);
                    CreateProcessTable(dtProcess);
                }
                else
                {
                    CreateOtherAmountTable(ds.Tables[3], dtProcess);
                }
            }
        }

        /// <summary>
        /// 绑定项目分配人员信息
        /// </summary>
        private void bindProjectValueByMember()
        {
            if (AuditStatus == "H")
            {
                TG.Model.cm_SysMsg model = new cm_SysMsg();
                TG.BLL.cm_SysMsg bll = new TG.BLL.cm_SysMsg();

                //判断该审批是否完成
                string isDone = bll.IsDone(MessageID);

                TG.Model.tg_member memberModel = new TG.BLL.tg_member().GetModel(UserSysNo);

                //该专业已审批完毕
                if (BLL.UserValueIsSaved(memberModel.mem_Speciality_ID, (int)projectAuditRecordEntity.AllotID))
                {
                    //完成 审批 查看自己审批的信息
                    CreateDataTableIsAuditUser(memberModel.mem_Speciality_ID);

                    //把消息修改成已办
                    new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatusByID(MessageID);
                }
                else
                {
                    //生成人员表单
                    CreateDataTableByMember();
                }

            }

            if (AuditStatus == "K")
            {
                CreateNoDataTableByMember();
            }
        }

        /// <summary>
        /// 绑定人员产值分配之后信息
        /// </summary>
        private void bindProjectValueByMemberAmount()
        {
            if (AuditStatus == "J" || AuditStatus == "L" || AuditStatus == "N" || AuditStatus == "O" || AuditStatus == "P" || AuditStatus == "Q")
            {
                if (AuditStatus != MessageStatus && MessageStatus == "H")
                {
                    TG.Model.tg_member memberModel = new TG.BLL.tg_member().GetModel(UserSysNo);
                    //完成 审批 查看自己审批的信息
                    CreateDataTableIsAuditUser(memberModel.mem_Speciality_ID);
                }
                else
                {
                    //生成人员信息
                    CreateTableByMemberCount();
                }
            }

        }

        /// <summary>
        /// 生成表单
        /// </summary>
        /// <param name="dtStageValue">设计阶段</param>
        /// <param name="dtStageSpeValue">专业</param>
        /// <param name="dtProcessValue">工序</param>
        private void CreateTable(DataTable dtStageValue, DataTable dtStageSpeValue, DataTable dtProcessValue)
        {
            //取得专业
            var dtSpecialtyList = new DataView(dtStageSpeValue).ToTable("Specialty", true, "Specialty");

            int count = dtSpecialtyList.Rows.Count;

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();
            sbSpeHead.Append(" <div id=\"stagespetable\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  项目各设计阶段产值分配比例% </td> </tr> </table>");
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 设计阶段</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">合计</td>");

            //比例

            sbSpeHead.Append(" </tr> </table>");

            sbSpeHead.Append("<table id=\"gvProjectStageSpe\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\"  >");


            #region  方案设计
            //方案设计工序
            StringBuilder sb_ProcessOneTable = new StringBuilder();
            sb_ProcessOneTable.Append("<div id=\"designProcessOneTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  方案设计工序产值分配比例%<span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span> <br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td> </tr> </table>");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllotHead\" style=\"width: 98%;\">");
            sb_ProcessOneTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
            sb_ProcessOneTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
            sb_ProcessOneTable.Append("</table>");
            sb_ProcessOneTable.Append("<table id=\"gvdesignProcessOne\" width=\"98%\"  class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\" >");

            //方案设计
            var dataProgram = new DataView(dtStageSpeValue).ToTable("ProgramPercent", false, "ProgramPercent", "Specialty");


            //第一行循环方案设计专业
            StringBuilder sb_one = new StringBuilder();

            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                sb_one.Append("<tr>");
                sb_one.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">方案设计</td>");

                //方案设计金额
                decimal ProgramCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["ProgramCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["ProgramCOUNT"].ToString());

                //计算方案设计总百分比
                var sumProgramPercent = dataProgram.AsEnumerable().Sum(s => s.Field<decimal>("ProgramPercent"));

                decimal sumOther = 100 - sumProgramPercent;

                #region 建筑

                //取得建筑
                decimal bulidingOnePercent = 0;
                //看是否有建筑人员参与
                var bulidingOneData = new DataView(dataProgram) { RowFilter = "Specialty='建筑'" }.ToTable();

                if (bulidingOneData != null && bulidingOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingOnePercent = decimal.Parse(bulidingOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }

                decimal bulidingOneCount = 0;

                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingOneCount = Math.Round(bulidingOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingOneCount = Math.Floor(bulidingOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"9%\">" + bulidingOneCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureOnePercent = 0;
                //看是否有结构人员参与
                var structureOneData = new DataView(dataProgram) { RowFilter = "Specialty='结构'" }.ToTable();

                if (structureOneData != null && structureOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureOnePercent = decimal.Parse(structureOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal structureOneCount = 0;

                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureOneCount = Math.Round(structureOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureOneCount = Math.Floor(structureOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"9%\">" + structureOneCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainOnePercent = 0;
                //看是否有给排水人员参与
                var drainOneData = new DataView(dataProgram) { RowFilter = "Specialty='给排水'" }.ToTable();

                if (drainOneData != null && drainOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainOnePercent = decimal.Parse(drainOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal drainOneCount = 0;

                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainOneCount = Math.Round(drainOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainOneCount = Math.Floor(drainOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"9%\">" + drainOneCount + "</td>");

                #endregion

                #region 电气

                //取得电气
                decimal electricOnePercent = 0;
                //看是否有电气人员参与
                var electricOneData = new DataView(dataProgram) { RowFilter = "Specialty='电气'" }.ToTable();

                if (electricOneData != null && electricOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricOnePercent = decimal.Parse(electricOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal electricOneCount = 0;

                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricOneCount = Math.Round(electricOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricOneCount = Math.Floor(electricOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"9%\">" + electricOneCount + "</td>");

                #endregion

                #region 合计
                decimal programCount = bulidingOneCount + structureOneCount + drainOneCount + electricOneCount;

                decimal programPercent = bulidingOnePercent + structureOnePercent + drainOnePercent + electricOnePercent;

                sb_one.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(programPercent, 0) + "</span>%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_one.Append("<td width=\"9%\">" + Math.Round(programCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_one.Append("<td width=\"9%\">" + Math.Floor(programCount) + "</td>");
                }

                sb_one.Append("</tr>");
                #endregion

                //方案设计工序 --方案为0
                var dtProgramProcessValue = new DataView(dtProcessValue) { RowFilter = "ItemType=0" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("建筑", dtProgramProcessValue, bulidingOneCount, 0));

                //取得结构工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("结构", dtProgramProcessValue, structureOneCount, 0));

                //取得给排水工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("给排水", dtProgramProcessValue, drainOneCount, 0));

                //取得电气工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("电气", dtProgramProcessValue, electricOneCount, 0));

                sb_ProcessOneTable.Append("</table></div>");

            }
            #endregion

            #region 初步设计
            //第一行循环方案设计专业
            StringBuilder sb_two = new StringBuilder();
            //初步设计工序
            StringBuilder sb_ProcessTwoTable = new StringBuilder();
            // 0个阶段 1 方案+初设 二阶段 3 三阶段
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                sb_two.Append("<tr>");
                sb_two.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">初步设计</td>");


                //方案设计
                var dataPreliminary = new DataView(dtStageSpeValue).ToTable("preliminaryPercent", false, "preliminaryPercent", "Specialty");

                //初步设计金额
                decimal preliminaryCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["preliminaryCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["preliminaryCOUNT"].ToString());

                //计算初步设计总百分比
                var sumPreliminaryPercent = dataPreliminary.AsEnumerable().Sum(s => s.Field<decimal>("preliminaryPercent"));

                sb_ProcessTwoTable.Append("<div id=\"designProcessTwoTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb_ProcessTwoTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  初步设计工序产值分配比例%<span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span> <br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td> </tr> </table>");
                sb_ProcessTwoTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");

                sb_ProcessTwoTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%; height:30px text-align: center;\" colspan=\"2\">    审核   </td> ");
                sb_ProcessTwoTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
                sb_ProcessTwoTable.Append("</tr></table>");
                sb_ProcessTwoTable.Append("<table id=\"gvdesignProcessTwo\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

                var sumTwo = 100 - sumPreliminaryPercent;
                //初步设计
                #region 建筑

                //取得建筑
                decimal bulidingTwoPercent = 0;
                //看是否有建筑人员参与
                var bulidingTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='建筑'" }.ToTable();

                if (bulidingTwoData != null && bulidingTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingTwoPercent = decimal.Parse(bulidingTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }

                decimal bulidingTwoCount = 0;

                sb_two.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingTwoCount = Math.Round(bulidingTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingTwoCount = Math.Floor(bulidingTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"9%\">" + bulidingTwoCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureTwoPercent = 0;
                //看是否有结构人员参与
                var structureTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='结构'" }.ToTable();

                if (structureTwoData != null && structureTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureTwoPercent = decimal.Parse(structureTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal structureTwoCount = 0;

                sb_two.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureTwoCount = Math.Round(structureTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureTwoCount = Math.Floor(structureTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"9%\">" + structureTwoCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainTwoPercent = 0;
                //看是否有给排水人员参与
                var drainTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='给排水'" }.ToTable();

                if (drainTwoData != null && drainTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainTwoPercent = decimal.Parse(drainTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal drainTwoCount = 0;

                sb_two.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainTwoCount = Math.Round(drainTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainTwoCount = Math.Floor(drainTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"9%\">" + drainTwoCount + "</td>");

                #endregion

                #region 电气

                //取得电气
                decimal electricTwoPercent = 0;
                //看是否有电气人员参与
                var electricTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='电气'" }.ToTable();

                if (electricTwoData != null && electricTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricTwoPercent = decimal.Parse(electricTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal electricTwoCount = 0;

                sb_two.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricTwoCount = Math.Round(electricTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricTwoCount = Math.Floor(electricTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"9%\">" + electricTwoCount + "</td>");

                #endregion

                #region 合计
                decimal preliminarySumCount = bulidingTwoCount + structureTwoCount + drainTwoCount + electricTwoCount;

                decimal preliminarySumPercent = bulidingTwoPercent + structureTwoPercent + drainTwoPercent + electricTwoPercent;

                sb_two.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(preliminarySumPercent, 0) + "</span>%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_two.Append("<td width=\"9%\">" + Math.Round(preliminarySumCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_two.Append("<td width=\"9%\">" + Math.Floor(preliminarySumCount) + "</td>");
                }

                sb_two.Append("</tr>");
                #endregion

                //初步设计工序 --初步为1
                var dtPreliminaryProcessValue = new DataView(dtProcessValue) { RowFilter = "ItemType=1" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("建筑", dtPreliminaryProcessValue, bulidingTwoCount, 1));

                //取得结构工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("结构", dtPreliminaryProcessValue, structureTwoCount, 1));

                //取得给排水工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("给排水", dtPreliminaryProcessValue, drainTwoCount, 1));

                //取得电气工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("电气", dtPreliminaryProcessValue, electricTwoCount, 1));

                sb_ProcessTwoTable.Append("</table></div>");

            }
            #endregion

            #region 施工图设计
            //第一行循环方案设计专业
            StringBuilder sb_three = new StringBuilder();

            StringBuilder sb_ProcessThreeTable = new StringBuilder();

            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                sb_three.Append("<tr>");
                sb_three.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">施工图设计</td>");


                //施工图设计
                var dataWorkDraw = new DataView(dtStageSpeValue).ToTable("WorkDrawPercent", false, "WorkDrawPercent", "Specialty");

                //施工图设计金额
                decimal WorkDrawPercentCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["WorkDrawCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["WorkDrawCOUNT"].ToString());

                //计算施工图总百分比
                var sumWorkDrawPercent = dataWorkDraw.AsEnumerable().Sum(s => s.Field<decimal>("WorkDrawPercent"));

                //初步设计工序
                sb_ProcessThreeTable.Append("<div id=\"designProcessThreeTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb_ProcessThreeTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  施工图工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td> </tr> </table>");
                sb_ProcessThreeTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");

                sb_ProcessThreeTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
                sb_ProcessThreeTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center; \" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
                //sb_ProcessThreeTable.Append(" </tr>  <tr><td colspan=11>");
                sb_ProcessThreeTable.Append("</tr></table>");
                sb_ProcessThreeTable.Append("<table id=\"gvdesignProcessThree\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

                var sumThree = 100 - sumWorkDrawPercent;
                #region 建筑

                //取得建筑
                decimal bulidingThreePercent = 0;
                //看是否有建筑人员参与
                var bulidingThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='建筑'" }.ToTable();

                if (bulidingThreeData != null && bulidingThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingThreePercent = decimal.Parse(bulidingThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }

                decimal bulidingThreeCount = 0;

                sb_three.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingThreeCount = Math.Round(bulidingThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingThreeCount = Math.Floor(bulidingThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"9%\">" + bulidingThreeCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureThreePercent = 0;
                //看是否有结构人员参与
                var structureThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='结构'" }.ToTable();

                if (structureThreeData != null && structureThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureThreePercent = decimal.Parse(structureThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal structureThreeCount = 0;

                sb_three.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureThreeCount = Math.Round(structureThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureThreeCount = Math.Floor(structureThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"9%\">" + structureThreeCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainThreePercent = 0;
                //看是否有给排水人员参与
                var drainThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='给排水'" }.ToTable();

                if (drainThreeData != null && drainThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainThreePercent = decimal.Parse(drainThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal drainThreeCount = 0;

                sb_three.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainThreeCount = Math.Round(drainThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainThreeCount = Math.Floor(drainThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"9%\">" + drainThreeCount + "</td>");

                #endregion

                #region 电气

                //取得电气
                decimal electricThreePercent = 0;
                //看是否有电气人员参与
                var electricThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='电气'" }.ToTable();

                if (electricThreeData != null && electricThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricThreePercent = decimal.Parse(electricThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal electricThreeCount = 0;

                sb_three.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricThreeCount = Math.Round(electricThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricThreeCount = Math.Floor(electricThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"9%\">" + electricThreeCount + "</td>");

                #endregion

                #region 合计
                decimal workDrawSumCount = bulidingThreeCount + structureThreeCount + drainThreeCount + electricThreeCount;

                decimal workDrawSumPercent = bulidingThreePercent + structureThreePercent + drainThreePercent + electricThreePercent;

                sb_three.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(workDrawSumPercent, 0) + "</span>%</td>");


                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_three.Append("<td width=\"9%\">" + Math.Round(workDrawSumCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_three.Append("<td width=\"9%\">" + Math.Floor(workDrawSumCount) + "</td>");
                }

                sb_three.Append("</tr>");
                #endregion

                //施工图 --施工图为2
                var dtWorkingProcessValue = new DataView(dtProcessValue) { RowFilter = "ItemType=2" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("建筑", dtWorkingProcessValue, bulidingThreeCount, 2));

                //取得结构工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("结构", dtWorkingProcessValue, structureThreeCount, 2));

                //取得给排水工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("给排水", dtWorkingProcessValue, drainThreeCount, 2));

                //取得电气工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("电气", dtWorkingProcessValue, electricThreeCount, 2));

                sb_ProcessThreeTable.Append("</table></div>");
            }
            #endregion

            #region 后期服务
            //第一行循环方案设计专业
            StringBuilder sb_four = new StringBuilder();
            StringBuilder sb_ProcessFourTable = new StringBuilder();
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                sb_four.Append("<tr>");
                sb_four.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">后期服务</td>");

                //后期服务
                var dataLateStage = new DataView(dtStageSpeValue).ToTable("LateStagePercent", false, "LateStagePercent", "Specialty");

                //后期服务金额
                decimal lastStageCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["LateStageCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["LateStageCOUNT"].ToString());

                //后期服务总百分比
                var sumLateStagePercent = dataLateStage.AsEnumerable().Sum(s => s.Field<decimal>("LateStagePercent"));


                //后期服务工序

                sb_ProcessFourTable.Append("<div id=\"designProcessFourTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb_ProcessFourTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  后期服务工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td> </tr> </table>");
                sb_ProcessFourTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                sb_ProcessFourTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%; text-align: center;\" colspan=\"2\">    审核   </td> ");
                sb_ProcessFourTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
                sb_ProcessFourTable.Append("</tr></table>");
                sb_ProcessFourTable.Append("<table id=\"gvdesignProcessFour\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

                var sumFour = 100 - sumLateStagePercent;

                #region 建筑

                //取得建筑
                decimal bulidingFourPercent = 0;
                //看是否有建筑人员参与
                var bulidingFourData = new DataView(dataLateStage) { RowFilter = "Specialty='建筑'" }.ToTable();

                if (bulidingFourData != null && bulidingFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingFourPercent = decimal.Parse(bulidingFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }

                decimal bulidingFourCount = 0;

                sb_four.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingFourCount = Math.Round(bulidingFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingFourCount = Math.Floor(bulidingFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"9%\">" + bulidingFourCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureFourPercent = 0;
                //看是否有结构人员参与
                var structureFourData = new DataView(dataLateStage) { RowFilter = "Specialty='结构'" }.ToTable();

                if (structureFourData != null && structureFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureFourPercent = decimal.Parse(structureFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal structureFourCount = 0;

                sb_four.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureFourCount = Math.Round(structureFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureFourCount = Math.Floor(structureFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"9%\">" + structureFourCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainFourPercent = 0;
                //看是否有给排水人员参与
                var drainFourData = new DataView(dataLateStage) { RowFilter = "Specialty='给排水'" }.ToTable();

                if (drainFourData != null && drainFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainFourPercent = decimal.Parse(drainFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal drainFourCount = 0;

                sb_four.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainFourCount = Math.Round(drainFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainFourCount = Math.Floor(drainFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"9%\">" + drainFourCount + "</td>");

                #endregion

                #region 电气

                //取得电气
                decimal electricFourPercent = 0;
                //看是否有电气人员参与
                var electricFourData = new DataView(dataLateStage) { RowFilter = "Specialty='电气'" }.ToTable();

                if (electricFourData != null && electricFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricFourPercent = decimal.Parse(electricFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal electricFourCount = 0;

                sb_four.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricFourCount = Math.Round(electricFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricFourCount = Math.Floor(electricFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"9%\">" + electricFourCount + "</td>");

                #endregion

                #region 合计
                decimal lastSumCount = bulidingFourCount + structureFourCount + drainFourCount + electricFourCount;

                decimal lastSumPercent = bulidingFourPercent + structureFourPercent + drainFourPercent + electricFourPercent;

                sb_four.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(lastSumPercent, 0) + "</span>%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_four.Append("<td width=\"9%\">" + Math.Round(lastSumCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_four.Append("<td width=\"9%\">" + Math.Floor(lastSumCount) + "</td>");
                }

                sb_four.Append("</tr>");
                #endregion

                //后期--后期为3
                var dtLateStageProcessValue = new DataView(dtProcessValue) { RowFilter = "ItemType=3" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("建筑", dtLateStageProcessValue, bulidingFourCount, 3));

                //取得结构工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("结构", dtLateStageProcessValue, structureFourCount, 3));

                //取得给排水工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("给排水", dtLateStageProcessValue, drainFourCount, 3));

                //取得电气工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("电气", dtLateStageProcessValue, electricFourCount, 3));
                sb_ProcessFourTable.Append("</table></div>");
            }
            #endregion

            if (CoperationProcess == 0)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 1)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
            }

            else if (CoperationProcess == 2)
            {
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 3)
            {
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 10)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            sbSpeHead.Append("</table></div>");

            lbl_stagespetable.Text = sbSpeHead.ToString();

            StringBuilder strTab = new StringBuilder();
            strTab.Append(" <div id=\"tabs\" >  <ul style=\"width:98%; margin:auto;\"> ");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabs-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabs-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");
            }

            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabs-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabs-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
            }

            strTab.Append("</ul>");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append(" <div id=\"tabs-1\">");
                strTab.Append(sb_ProcessOneTable.ToString());
                strTab.Append(" </div>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append(" <div id=\"tabs-2\">");
                strTab.Append(sb_ProcessTwoTable.ToString());
                strTab.Append(" </div>");
            }

        
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                strTab.Append(" <div id=\"tabs-3\">");
                strTab.Append(sb_ProcessThreeTable.ToString());
                strTab.Append(" </div>");
                strTab.Append(" <div id=\"tabs-4\">");
                strTab.Append(sb_ProcessFourTable.ToString());
                strTab.Append(" </div>");
            }
            strTab.Append(" </div>");
            lbl_Tab.Text = strTab.ToString();

        }

        /// <summary>
        /// 得到各专业工序产值
        /// </summary>
        /// <param name="speName">专业名称</param>
        /// <param name="dtProcessValue">工序</param>
        /// <param name="speCount">专业产值</param>
        /// <returns>各专业工序产值</returns>
        private string getSpeProcessValue(string speName, DataTable dtProcessValue, decimal speCount, int processID)
        {

            StringBuilder sb_ProcessOneTable = new StringBuilder();

            //计算 该专业下 该审核角色是否有人员参与

            //看该专业审核 是否有人参与
            int countAudit = BLL.IsRolePlay(4, speName, ProSysNo);

            //该专业 专业负责 是否有人参与
            int countSpecialtyHead = BLL.IsRolePlay(2, speName, ProSysNo);

            // 该专业 校对 是否有人参与
            int countProofread = BLL.IsRolePlay(3, speName, ProSysNo);

            //该专业 审核 是否有人参与
            int countDesign = BLL.IsDesignerPlay(ProSysNo, speName);

            var programBulidingData = new DataView(dtProcessValue) { RowFilter = "Specialty='" + speName + "'" }.ToTable();
            // 审核比例
            decimal auditpercent = 0;
            decimal specialtyHeadPercent = 0;
            decimal proofreadPercent = 0;
            decimal designPercent = 0;
            if (programBulidingData != null && programBulidingData.Rows.Count > 0)
            {
                //审核比例
                auditpercent = string.IsNullOrEmpty(programBulidingData.Rows[0]["AuditPercent"].ToString()) ? 0 : decimal.Parse(programBulidingData.Rows[0]["AuditPercent"].ToString());

                //专业负责比例
                specialtyHeadPercent = string.IsNullOrEmpty(programBulidingData.Rows[0]["SpecialtyHeadPercent"].ToString()) ? 0 : decimal.Parse(programBulidingData.Rows[0]["SpecialtyHeadPercent"].ToString());

                //校对比例
                proofreadPercent = string.IsNullOrEmpty(programBulidingData.Rows[0]["ProofreadPercent"].ToString()) ? 0 : decimal.Parse(programBulidingData.Rows[0]["ProofreadPercent"].ToString());

                //设计比例
                designPercent = string.IsNullOrEmpty(programBulidingData.Rows[0]["DesignPercent"].ToString()) ? 0 : decimal.Parse(programBulidingData.Rows[0]["DesignPercent"].ToString());

            }


            decimal totalRoleCount = 0;
            int count = 0;
            if (countAudit > 0)
            {
                totalRoleCount = totalRoleCount + auditpercent;
                count = count + 1;
            }
            if (countSpecialtyHead > 0)
            {
                totalRoleCount = totalRoleCount + specialtyHeadPercent;
                count = count + 1;
            }

            if (countProofread > 0)
            {
                totalRoleCount = totalRoleCount + proofreadPercent;
                count = count + 1;
            }
            if (countDesign > 0)
            {
                totalRoleCount = totalRoleCount + designPercent;
                count = count + 1;
            }


            decimal otherTotal = 100 - totalRoleCount;

            sb_ProcessOneTable.Append("<tr>");
            sb_ProcessOneTable.Append("<td width= 10% class=\"cls_Column\"> " + speName + "</td>");

            //审核
            if (countAudit > 0)
            {
                if (totalRoleCount != 0 && count != 0)
                {
                    auditpercent = auditpercent + Convert.ToDecimal(otherTotal / count);
                }
            }
            else
            {
                auditpercent = 0;
            }


            decimal auditCount = auditpercent / 100 * speCount;
            //0 表示方案设计 3表示后期服务
            if (processID == 0 || processID == 3)
            {
                if (auditpercent == 0)
                {
                    sb_ProcessOneTable.Append("<td width= 9%> 0%</td>");
                    sb_ProcessOneTable.Append("<td width= 9%> 0</td>");
                }
                else
                {
                    auditpercent = Math.Round(auditpercent, 2);
                    sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"6\" type=\"text\"  value=" + auditpercent + " />%</td>");

                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(auditCount) + "</td>");
                    }
                    else
                    {
                        sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(auditCount) + "</td>");
                    }
                }
            }
            else
            {
                auditpercent = Math.Round(auditpercent, 2);
                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"6\" type=\"text\"  value=" + auditpercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(auditCount) + "</td>");
                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(auditCount) + "</td>");
                }
            }

            //专业负责
            if (countSpecialtyHead > 0)
            {
                if (totalRoleCount != 0 && count != 0)
                {
                    // specialtyHeadPercent = specialtyHeadPercent + specialtyHeadPercent * otherTotal / totalRoleCount;
                    specialtyHeadPercent = specialtyHeadPercent + Convert.ToDecimal(otherTotal / count);
                }
            }
            else
            {
                specialtyHeadPercent = 0;
            }


            decimal specialtyHeadCount = specialtyHeadPercent / 100 * speCount;

            //该专业没有专业负责人员参与
            if (processID == 0 || processID == 3)
            {
                if (specialtyHeadPercent == 0)
                {
                    sb_ProcessOneTable.Append("<td width= 9%> 0%</td>");
                    sb_ProcessOneTable.Append("<td width= 9%> 0</td>");
                }
                else
                {
                    specialtyHeadPercent = Math.Round(specialtyHeadPercent, 2);

                    sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + specialtyHeadPercent + " />%</td>");

                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(specialtyHeadCount, 0) + "</td>");

                    }
                    else
                    {
                        sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(specialtyHeadCount) + "</td>");
                    }
                }
            }
            else
            {
                specialtyHeadPercent = Math.Round(specialtyHeadPercent, 2);

                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + specialtyHeadPercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(specialtyHeadCount, 0) + "</td>");

                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(specialtyHeadCount) + "</td>");
                }
            }

            //校对
            if (countProofread > 0)
            {
                if (totalRoleCount != 0 && count != 0)
                {
                    proofreadPercent = proofreadPercent + Convert.ToDecimal(otherTotal / count);
                }
            }
            else
            {
                proofreadPercent = 0;
            }

            decimal proofreadCount = proofreadPercent / 100 * speCount;
            //没有校对人员参与
            if (processID == 0 || processID == 3)
            {
                if (proofreadPercent == 0)
                {
                    sb_ProcessOneTable.Append("<td width= 9%> 0%</td>");
                    sb_ProcessOneTable.Append("<td width= 9%> 0</td>");
                }
                else
                {
                    proofreadPercent = Math.Round(proofreadPercent, 2);

                    sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + proofreadPercent + " />%</td>");

                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        sb_ProcessOneTable.Append("<td width= 9%> " + Math.Round(proofreadCount, 0) + "</td>");
                    }
                    else
                    {
                        sb_ProcessOneTable.Append("<td width= 9%> " + Math.Floor(proofreadCount) + "</td>");
                    }
                }
            }
            else
            {
                proofreadPercent = Math.Round(proofreadPercent, 2);

                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + proofreadPercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%> " + Math.Round(proofreadCount, 0) + "</td>");
                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%> " + Math.Floor(proofreadCount) + "</td>");
                }
            }


            //设计
            if (countDesign > 0)
            {
                if (totalRoleCount != 0 && count != 0)
                {
                    // designPercent = designPercent + designPercent * otherTotal / totalRoleCount;
                    designPercent = designPercent + Convert.ToDecimal(otherTotal / count);
                }
            }
            else
            {
                designPercent = 0;
            }


            decimal designCount = designPercent / 100 * speCount;
            //没有设计人员参与
            if (processID == 0 || processID == 3)
            {
                //if (designPercent == 0)
                //{
                //    sb_ProcessOneTable.Append("<td width= 9%> 0%</td>");
                //    sb_ProcessOneTable.Append("<td width= 9%> 0</td>");
                //}
                //else
                //{
                designPercent = Math.Round(designPercent, 2);

                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + designPercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(designCount, 0) + "</td>");
                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(designCount) + "</td>");
                }
                //}
            }
            else
            {
                designPercent = Math.Round(designPercent, 2);

                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + designPercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(designCount, 0) + "</td>");
                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(designCount) + "</td>");
                }
            }

            decimal totalPercent = auditpercent + specialtyHeadPercent + proofreadPercent + designPercent;
            //decimal 
            if (totalPercent == 0)
            {
                sb_ProcessOneTable.Append("<td width= 9%> <span>0</span>%</td>");
            }
            else
            {
                sb_ProcessOneTable.Append("<td width= 9%  style=\"background-color:#D2F3CB\"> <span>" + Math.Round(totalPercent, 0) + "</span>%</td>");
            }

            decimal countTotal = specialtyHeadCount + auditCount + proofreadCount + designCount;

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                sb_ProcessOneTable.Append("<td width= 9%> " + Math.Round(countTotal, 0) + "</td>");
            }
            else
            {
                sb_ProcessOneTable.Append("<td width= 9%> " + Math.Floor(countTotal) + "</td>");

            }
            sb_ProcessOneTable.Append("</tr>");

            return sb_ProcessOneTable.ToString();

        }


        /// <summary>
        /// 生成表四之后的表单
        /// </summary>
        /// <param name="dtSpe">专业</param>
        /// <param name="dtProcess">工序</param>
        private void CreateOtherTable(DataTable dtSpe, DataTable dtProcess, decimal allotCount)
        {
            //取得专业
            var dtSpecialtyList = new DataView(dtSpe).ToTable("Specialty", true, "Specialty");

            int count = dtSpecialtyList.Rows.Count;

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();
            sbSpeHead.Append(" <div id=\"stagetabletfive\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> ");

            if (CoperationProcess == 4)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  室外工程产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 5)
            {
                sbSpeHead.Append("<tr><td align=\"center\">  锅炉房产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 6)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  地上单建水泵房产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 7)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  地上单建变配所(室)产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 8)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  单建地下室（车库）产值分配比例%</td></tr>");
            }
            else if (CoperationProcess == 9)
            {
                sbSpeHead.Append("<tr><td align=\"center\">  市政道路工程产值分配比例% </td></tr>");
            }

            //比例
            decimal percent = decimal.Divide(92, count * 2);
            percent = decimal.Round(percent, 2);
            sbSpeHead.Append("</table>");
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 专业</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">合计</td>");

            sbSpeHead.Append("</tr></table>");

            sbSpeHead.Append("<table id=\"gvFive\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\"  >");


            //
            StringBuilder sb_one = new StringBuilder();
            sb_one.Append("<tr>");
            sb_one.Append("<td class=\"cls_Column\" style=\"width: 10%\">比例</td>");

            //计算方案设计总百分比
            var sumProgramPercent = dtSpe.AsEnumerable().Sum(s => s.Field<decimal>("SpecialtyPercent"));


            decimal sumOther = 100 - sumProgramPercent;

            #region 建筑

            //取得建筑
            decimal bulidingOnePercent = 0;
            //看是否有建筑人员参与
            var bulidingOneData = new DataView(dtSpe) { RowFilter = "Specialty='建筑'" }.ToTable();

            if (bulidingOneData != null && bulidingOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    bulidingOnePercent = decimal.Parse(bulidingOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }

            decimal bulidingOneCount = 0;

            sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingOnePercent, 2).ToString() + " />%</td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                bulidingOneCount = Math.Round(bulidingOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
            }
            else //去整
            {
                bulidingOneCount = Math.Floor(bulidingOnePercent / 100 * allotCount);
            }

            sb_one.Append("<td width=\"9%\">" + bulidingOneCount + "</td>");

            #endregion

            #region 结构

            //取得结构
            decimal structureOnePercent = 0;
            //看是否有结构人员参与
            var structureOneData = new DataView(dtSpe) { RowFilter = "Specialty='结构'" }.ToTable();

            if (structureOneData != null && structureOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    structureOnePercent = decimal.Parse(structureOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal structureOneCount = 0;

            sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureOnePercent, 2).ToString() + " />%</td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                structureOneCount = Math.Round(structureOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
            }
            else //去整
            {
                structureOneCount = Math.Floor(structureOnePercent / 100 * allotCount);
            }

            sb_one.Append("<td width=\"9%\">" + structureOneCount + "</td>");

            #endregion

            #region 给排水

            //取得给排水
            decimal drainOnePercent = 0;
            //看是否有给排水人员参与
            var drainOneData = new DataView(dtSpe) { RowFilter = "Specialty='给排水'" }.ToTable();

            if (drainOneData != null && drainOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    drainOnePercent = decimal.Parse(drainOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal drainOneCount = 0;

            sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainOnePercent, 2).ToString() + " />%</td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                drainOneCount = Math.Round(drainOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
            }
            else //去整
            {
                drainOneCount = Math.Floor(drainOnePercent / 100 * allotCount);
            }

            sb_one.Append("<td width=\"9%\">" + drainOneCount + "</td>");

            #endregion

            #region 电气

            //取得电气
            decimal electricOnePercent = 0;
            //看是否有电气人员参与
            var electricOneData = new DataView(dtSpe) { RowFilter = "Specialty='电气'" }.ToTable();

            if (electricOneData != null && electricOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    electricOnePercent = decimal.Parse(electricOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal electricOneCount = 0;

            sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricOnePercent, 2).ToString() + " />%</td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                electricOneCount = Math.Round(electricOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
            }
            else //去整
            {
                electricOneCount = Math.Floor(electricOnePercent / 100 * allotCount);
            }

            sb_one.Append("<td width=\"9%\">" + electricOneCount + "</td>");

            #endregion

            #region 合计
            decimal programCount = bulidingOneCount + structureOneCount + drainOneCount + electricOneCount;

            decimal programPercent = bulidingOnePercent + structureOnePercent + drainOnePercent + electricOnePercent;

            sb_one.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(programPercent, 0) + "</span>%</td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                sb_one.Append("<td width=\"9%\">" + Math.Round(programCount, MidpointRounding.AwayFromZero) + "</td>");
            }
            else //去整
            {
                sb_one.Append("<td width=\"9%\">" + Math.Floor(programCount) + "</td>");
            }

            sb_one.Append("</tr>");
            #endregion


            StringBuilder sb_ProcessOneTable = new StringBuilder();
            sb_ProcessOneTable.Append(" <div id=\"tbOutDoor\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> ");


            if (CoperationProcess == 4)
            {
                sb_ProcessOneTable.Append("<tr>  <td  align=\"center\" >    室外工程工序产值分配比例%<span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span> <br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
            }
            else if (CoperationProcess == 5)
            {
                sb_ProcessOneTable.Append("<tr>  <td   align=\"center\" >   锅炉房工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
            }
            else if (CoperationProcess == 6)
            {
                sb_ProcessOneTable.Append("<tr>  <td  align=\"center\" >    地上单建水泵房工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
            }
            else if (CoperationProcess == 7)
            {
                sb_ProcessOneTable.Append("<tr>   <td   align=\"center\" >    地上单建变配所(室)工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
            }
            else if (CoperationProcess == 8)
            {
                sb_ProcessOneTable.Append("<tr>   <td   align=\"center\" >    单建地下室（车库）工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
            }
            else if (CoperationProcess == 9)
            {
                sb_ProcessOneTable.Append("<tr>   <td  align=\"center\" >  市政道路工程工序产值分配比例%<span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span> <br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
            }
            sb_ProcessOneTable.Append("</table>");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sb_ProcessOneTable.Append(" <tr> <td style=\"width: 10%; text-align: center\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
            sb_ProcessOneTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
            sb_ProcessOneTable.Append("</table>");

            sb_ProcessOneTable.Append("<table id=\"gvdesignProcessFive\" width=\"98%\"  class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\" >");

            //取得室外工程的工序
            var dtProcessValue = new DataView(dtProcess) { RowFilter = "ItemType=4" }.ToTable();

            //取得建筑工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("建筑", dtProcessValue, bulidingOneCount, CoperationProcess));

            //取得结构工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("结构", dtProcessValue, structureOneCount, CoperationProcess));

            //取得给排水工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("给排水", dtProcessValue, drainOneCount, CoperationProcess));

            //取得电气工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("电气", dtProcessValue, electricOneCount, CoperationProcess));

            sb_ProcessOneTable.Append("</table></div>");

            sbSpeHead.Append(sb_one.ToString());
            sbSpeHead.Append("</table></div>");
            lbl_StageFive.Text = sbSpeHead.ToString();
            lbl_out.Text = sb_ProcessOneTable.ToString();
        }


        /// 生成阶段专业产值表单--保存之后数据
        /// </summary>
        /// <param name="dtSpeAmount"></param>
        private void CreateSpeAmountTable(DataTable dtSpeAmount)
        {

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();
            sbSpeHead.Append(" <div id=\"spetamountable\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  项目各设计阶段专业之间产值分配比例% </td> </tr> </table>");
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;height: 30px; \"> 设计阶段</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">合计</td>");
            sbSpeHead.Append("</tr>");
            sbSpeHead.Append(" </table>");

            sbSpeHead.Append("<table id=\"gvSpeAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

            //方案设计
            var dataProgram = new DataView(dtSpeAmount).ToTable("ProgramCount", false, "ProgramCount", "Specialty", "ProgramPercent");

            //第一行循环方案设计专业
            StringBuilder sb_one = new StringBuilder();
            sb_one.Append("<tr>");
            sb_one.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">方案设计</td>");

            //方案的金额
            for (int j = 0; j < dataProgram.Rows.Count; j++)
            {
                sb_one.Append("<td width=\"9%\">" + dataProgram.Rows[j]["ProgramPercent"].ToString() + "%</td>");
                sb_one.Append("<td width=\"9%\">" + dataProgram.Rows[j]["ProgramCount"].ToString() + "</td>");
            }
            sb_one.Append("<td width=\"9%\">100%</td>");
            //取得总和
            var sumProgramCount = dataProgram.AsEnumerable().Sum(s => s.Field<decimal>("ProgramCount"));
            sb_one.Append("<td width=\"9%\">" + sumProgramCount + "</td>");
            sb_one.Append("</tr>");

            //初步设计
            var dataPreliminary = new DataView(dtSpeAmount).ToTable("preliminaryCount", false, "preliminaryCount", "Specialty", "preliminaryPercent");

            //第一行循环方案设计专业
            StringBuilder sb_two = new StringBuilder();
            sb_two.Append("<tr>");
            sb_two.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">初步设计</td>");


            for (int j = 0; j < dataPreliminary.Rows.Count; j++)
            {
                sb_two.Append("<td width=\"9%\">" + dataPreliminary.Rows[j]["preliminaryPercent"].ToString() + "%</td>");
                sb_two.Append("<td  width=\"9%\">" + dataPreliminary.Rows[j]["preliminaryCount"].ToString() + "</td>");
            }
            sb_two.Append("<td width=\"9%\">100%</td>");
            //取得总和
            var sumPreliminaryCount = dataPreliminary.AsEnumerable().Sum(s => s.Field<decimal>("preliminaryCount"));
            sb_two.Append("<td width=\"9%\">" + sumPreliminaryCount + "</td>");

            sb_two.Append("</tr>");

            //施工图设计
            var dataWorkDraw = new DataView(dtSpeAmount).ToTable("WorkDrawCount", false, "WorkDrawCount", "Specialty", "WorkDrawPercent");

            //第一行循环方案设计专业
            StringBuilder sb_three = new StringBuilder();
            sb_three.Append("<tr>");
            sb_three.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">施工图设计</td>");

            for (int j = 0; j < dataWorkDraw.Rows.Count; j++)
            {
                sb_three.Append("<td width=\"9%\">" + dataWorkDraw.Rows[j]["WorkDrawPercent"].ToString() + "%</td>");
                sb_three.Append("<td width=\"9%\">" + dataWorkDraw.Rows[j]["WorkDrawCount"].ToString() + "</td>");
            }
            sb_three.Append("<td width=\"9%\">100%</td>");
            //取得总和
            var sumWorkDrawCount = dataWorkDraw.AsEnumerable().Sum(s => s.Field<decimal>("WorkDrawCount"));
            sb_three.Append("<td width=\"9%\">" + sumWorkDrawCount + "</td>");
            sb_three.Append("</tr>");

            //后期服务
            var dataLateStage = new DataView(dtSpeAmount).ToTable("LateStageCount", false, "LateStageCount", "Specialty", "LateStagePercent");

            //第一行循环方案设计专业
            StringBuilder sb_four = new StringBuilder();
            sb_four.Append("<tr>");
            sb_four.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">后期服务</td>");

            for (int j = 0; j < dataLateStage.Rows.Count; j++)
            {
                sb_four.Append("<td width=\"9%\">" + dataLateStage.Rows[j]["LateStagePercent"].ToString() + "%</td>");
                sb_four.Append("<td width=\"9%\">" + dataLateStage.Rows[j]["LateStageCount"].ToString() + "</td>");
            }
            sb_four.Append("<td width=\"9%\">100%</td>");
            //取得总和
            var sumLateStageCount = dataLateStage.AsEnumerable().Sum(s => s.Field<decimal>("LateStageCount"));
            sb_four.Append("<td width=\"9%\">" + sumLateStageCount + "</td>");
            sb_four.Append("</tr>");


            if (CoperationProcess == 0)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 1)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
            }

            else if (CoperationProcess == 2)
            {
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 3)
            {
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 10)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }

            sbSpeHead.Append(" </table></div> ");
            lbl_SpeAmount.Text = sbSpeHead.ToString();


        }

        /// <summary>
        /// 生成工序--保存之后数据
        /// </summary>
        /// <param name="dtProcessAmount">工序金额</param>
        private void CreateProcessTable(DataTable dtProcessAmount)
        {

            //方案工序
            DataTable dtProgram = new DataView(dtProcessAmount) { RowFilter = "type=0" }.ToTable();
            string program = getProcessDeatil(dtProgram, 0);

            //初步工序
            DataTable dtPreliminary = new DataView(dtProcessAmount) { RowFilter = "type=1" }.ToTable();
            string preliminary = getProcessDeatil(dtPreliminary, 1);

            //施工图
            DataTable dtWorkDraw = new DataView(dtProcessAmount) { RowFilter = "type=2" }.ToTable();
            string workDraw = getProcessDeatil(dtWorkDraw, 2);

            //后期服务
            DataTable dtLateStage = new DataView(dtProcessAmount) { RowFilter = "type=3" }.ToTable();
            string lateStage = getProcessDeatil(dtLateStage, 3);

            StringBuilder strTab = new StringBuilder();
            strTab.Append(" <div id=\"tabsProcess\">  <ul style=\"width:98%; margin:auto;\"> ");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabsProcess-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");

            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsProcess-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");

            }
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsProcess-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsProcess-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
            }
            strTab.Append(" </ul>");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append("<div id=\"tabsProcess-1\">");
                strTab.Append(program);
                strTab.Append(" </div>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append(" <div id=\"tabsProcess-2\">");
                strTab.Append(preliminary);
                strTab.Append(" </div>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10) 
            {
                strTab.Append(" <div id=\"tabsProcess-3\">");
                strTab.Append(workDraw);
                strTab.Append(" </div>");
                strTab.Append(" <div id=\"tabsProcess-4\">");
                strTab.Append(lateStage);
                strTab.Append(" </div>");
            }
            strTab.Append(" </div>");
            lbl_ProcessAmount.Text = strTab.ToString();

        }

        /// <summary>
        /// 得到工序分配明细
        /// </summary>
        /// <param name="dtProcess"></param>
        /// <param name="process">哪个阶段</param>
        /// <returns></returns>
        private string getProcessDeatil(DataTable dtProcess, int process)
        {
            StringBuilder sb = new StringBuilder();

            if (process == 0)
            {
                sb.Append(" <div id=\"processAmountOneTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  方案设计工序产值分配比例% </td> </tr> </table>");
            }
            else if (process == 1)
            {
                sb.Append(" <div id=\"processAmountTwoTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  初步设计工序产值分配比例% </td> </tr> </table>");
            }
            else if (process == 2)
            {
                sb.Append(" <div id=\"processAmountThreeTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  施工图工序产值分配比例% </td> </tr> </table>");
            }
            else if (process == 3)
            {
                sb.Append(" <div id=\"processAmountFourTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  后期服务工序产值分配比例% </td> </tr> </table>");

            }
            else
            {
                sb.Append(" <div id=\"processAmountFiveTable\" class=\"cls_Container_Report\" style=\"display: none;\">");


                if (CoperationProcess == 4)
                {
                    sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  室外工程工序产值分配比例% </td> </tr> </table>");

                }
                else if (CoperationProcess == 5)
                {
                    sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  锅炉房工序产值分配比例% </td> </tr> </table>");

                }
                else if (CoperationProcess == 6)
                {
                    sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建水泵房工序产值分配比例% </td> </tr> </table>");

                }
                else if (CoperationProcess == 7)
                {
                    sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建变配所(室)工序产值分配比例% </td> </tr> </table>");

                }
                else if (CoperationProcess == 8)
                {
                    sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  单建地下室（车库）工序产值分配比例% </td> </tr> </table>");

                }
                else if (CoperationProcess == 9)
                {
                    sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  市政道路工程工序产值分配比例% </td> </tr> </table>");

                }

            }
            sb.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sb.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
            sb.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
            sb.Append(" </table>");
            sb.Append("<table id=\"gvProcessAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\"  >");

            for (int j = 0; j < dtProcess.Rows.Count; j++)
            {
                sb.Append("<tr>");
                sb.Append("<td width=10% class=\"cls_Column\">" + dtProcess.Rows[j]["Specialty"].ToString() + "</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["AuditPercent"].ToString() + "%</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["AuditAmount"].ToString() + "</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["SpecialtyHeadPercent"].ToString() + "%</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["SpecialtyHeadAmount"].ToString() + "</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["ProofreadPercent"].ToString() + "%</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["ProofreadAmount"].ToString() + "</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["DesignPercent"].ToString() + "%</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["DesignAmount"].ToString() + "</td>");
                sb.Append("<td width=9%>100%</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["TotalAmount"].ToString() + "</td>");
                sb.Append("</tr>");
            }
            sb.Append("</table></div>");
            return sb.ToString();

        }

        /// <summary>
        /// 生成表四之后的表单--保存之后数据
        /// </summary>
        /// <param name="dtOutAmount"></param>
        /// <param name="dtProcess"></param>
        private void CreateOtherAmountTable(DataTable dtOutAmount, DataTable dtProcess)
        {

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();

            sbSpeHead.Append(" <div id=\"outdooramounttable\" class=\"cls_Container_Report\" style=\"display: none;\">");

            if (CoperationProcess == 4)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  室外工程产值分配比例% </td> </tr> </table>");
                //sbSpeHead.Append("<tr><td colspan=\"11\">  室外工程产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 5)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  锅炉房产值分配比例% </td> </tr> </table>");
                //sbSpeHead.Append("<tr><td colspan=\"11\">   锅炉房产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 6)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建水泵房产值分配比例% </td> </tr> </table>");

                //sbSpeHead.Append("<tr><td colspan=\"11\">   地上单建水泵房产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 7)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建变配所(室)产值分配比例% </td> </tr> </table>");

                // sbSpeHead.Append("<tr><td colspan=\"11\"> 地上单建变配所(室)产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 8)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  单建地下室（车库）产值分配比例% </td> </tr> </table>");

                // sbSpeHead.Append("<tr><td colspan=\"11\">  单建地下室（车库）产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 9)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  市政道路工程产值分配比例% </td> </tr> </table>");

                // sbSpeHead.Append("<tr><td colspan=\"11\">  市政道路工程产值分配比例% </td></tr>");
            }
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 专业</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">合计</td>");

            sbSpeHead.Append("</tr> </table>");

            sbSpeHead.Append("<table id=\"gvOutDoorAmount\" width=\"98%\"  class=\"cls_ProjAllot_Table cls_ProjAllot_label2\"  >");
            if (dtProcess.Rows.Count > 0)
            {
                sbSpeHead.Append("<tr><td width=\"10%\" class=\"cls_Column\">比例</td>");
                //建筑
                var bulidingData = new DataView(dtOutAmount) { RowFilter = "Specialty='建筑'" }.ToTable();
                if (bulidingData != null && bulidingData.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td width=\"9%\">" + bulidingData.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td width=\"9%\">" + bulidingData.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                }

                //结构
                var structureCount = new DataView(dtOutAmount) { RowFilter = "Specialty='结构'" }.ToTable();
                if (structureCount != null && structureCount.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td  width=\"9%\">" + structureCount.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td  width=\"9%\">" + structureCount.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                }

                //给排水
                var drainCount = new DataView(dtOutAmount) { RowFilter = "Specialty='给排水'" }.ToTable();
                if (drainCount != null && drainCount.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td  width=\"9%\">" + drainCount.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td  width=\"9%\">" + drainCount.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                }

                //电气
                var electricCount = new DataView(dtOutAmount) { RowFilter = "Specialty='电气'" }.ToTable();
                if (electricCount != null && electricCount.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td  width=\"9%\">" + electricCount.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td  width=\"9%\">" + electricCount.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                }

                var sumTotal = dtOutAmount.AsEnumerable().Sum(s => s.Field<decimal>("SpecialtyCount"));
                //合计
                sbSpeHead.Append("<td  width=\"9%\">100%</td>");
                sbSpeHead.Append("<td  width=\"9%\">" + sumTotal + "</td>");
                sbSpeHead.Append("</tr>");
            }
            else
            {
                sbSpeHead.Append("<tr><td colspan=\"11\">没有数据</td></tr>");
            }

            sbSpeHead.Append(" </table></div>");

            lbl_outAmount.Text = sbSpeHead.ToString();

            lbl_processAmountFiveTable.Text = getProcessDeatil(dtProcess, 4);
        }

        /// <summary>
        /// 创建人员的表单
        /// </summary>
        private void CreateDataTableByMember()
        {
            //取得该用户信息
            TG.BLL.tg_member bll = new TG.BLL.tg_member();
            TG.Model.tg_member model = new TG.Model.tg_member();
            model = bll.GetModel(UserSysNo);

            decimal allotCount = decimal.Parse(txt_ActulCount.Text);
            DataSet ds = BLL.GetProjectValueByMemberDataList(ProReferenceSysNo, (int)projectAuditRecordEntity.AllotID, model.mem_Speciality_ID);

            DataTable dtValue = ds.Tables[0];
            DataTable dtmember = ds.Tables[1];


            //取得设计人员的专业
            var dtSpecialtyList = new DataView(dtmember).ToTable("Specialty", true, "spe_Name");


            //判断改专业是否保存完毕
            DataTable dtMemberBySpecialty = new DataTable();
            DataTable dtProcess = new DataTable();
            if (dtmember.Rows.Count > 0)
            {

                string speName = dtSpecialtyList.Rows[0]["spe_Name"].ToString();

                SpeName = speName;

                //取得方案工序金额
                DataTable dtProgramData = new DataView(dtValue) { RowFilter = "Specialty='" + speName + "'AND type=0" }.ToTable();

                //取得初步工序金额
                DataTable dtPreliminaryData = new DataView(dtValue) { RowFilter = "Specialty='" + speName + "'AND type=1" }.ToTable();

                //取得施工图工序金额
                DataTable dtWorkDrawData = new DataView(dtValue) { RowFilter = "Specialty='" + speName + "'AND type=2" }.ToTable();

                //取得后期工序金额
                DataTable dtLateStageData = new DataView(dtValue) { RowFilter = "Specialty='" + speName + "'AND type=3" }.ToTable();

                //显示工序
                StringBuilder sbTotal = new StringBuilder();
                sbTotal.Append(" <div id=\"memberProcessTotal\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sbTotal.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> ");

                if (CoperationProcess >= 4 && CoperationProcess != 10)
                {
                    if (CoperationProcess == 4)
                    {
                        sbTotal.Append("<tr><td > " + speName + "专业-室外工程工序产值合计<br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
                    }
                    else if (CoperationProcess == 5)
                    {
                        sbTotal.Append("<tr><td  > " + speName + "专业-锅炉房工序产值合计 <br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span></td></tr>");
                    }
                    else if (CoperationProcess == 6)
                    {
                        sbTotal.Append("<tr><td  > " + speName + "专业-地上单建水泵房工序产值合计 <br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span></td></tr>");
                    }
                    else if (CoperationProcess == 7)
                    {
                        sbTotal.Append("<tr><td > " + speName + "专业-地上单建变配所(室)工序产值合计<br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
                    }
                    else if (CoperationProcess == 8)
                    {
                        sbTotal.Append("<tr><td  > " + speName + "专业-单建地下室（车库）工序产值合计<br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
                    }
                }
                else
                {
                    sbTotal.Append("<tr><td > " + speName + "专业各工序产值合计 </td></tr>");
                }

                sbTotal.Append("</table>");
                sbTotal.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                sbTotal.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 专业</td>");
                sbTotal.Append("<td  style=\"width:15%;text-align: center;\">设计阶段</td>");
                sbTotal.Append("<td  style=\"width:15%;text-align: center;\">审核金额(元)</td>");
                sbTotal.Append("<td  style=\"width:15%;text-align: center;\">专业负责金额(元)</td>");
                sbTotal.Append("<td  style=\"width:15%;text-align: center;\">校对金额(元)</td>");
                sbTotal.Append("<td  style=\"width:15%;text-align: center;\">设计金额(元)</td>");
                sbTotal.Append("<td  style=\"width:15%;text-align: center;\">合计(元)</td>");
                sbTotal.Append("</tr></table>");
                sbTotal.Append(" <table class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\"  width=\"98%\">");
                if (CoperationProcess == 0 || CoperationProcess == 10)
                {
                    sbTotal.Append("<tr><td align=\"center\" rowspan=\"4\" class=\"cls_Column\" style=\"width: 10%;\">" + speName + "</td>");
                }
                else if (CoperationProcess == 1 || CoperationProcess == 2)
                {
                    sbTotal.Append("<tr><td align=\"center\" rowspan=\"2\" class=\"cls_Column\" style=\"width: 10%;\">" + speName + "</td>");
                }
                else if (CoperationProcess == 3 || CoperationProcess == 10)
                {
                    sbTotal.Append("<tr><td align=\"center\" rowspan=\"3\" class=\"cls_Column\" style=\"width: 10%;\">" + speName + "</td>");
                }
                else
                {
                    sbTotal.Append("<tr><td align=\"center\" rowspan=\"1\" class=\"cls_Column\" style=\"width: 10%;\">" + speName + "</td>");
                }

                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                {
                    sbTotal.Append("<td style=\"width:15%;\">方案设计</td>");
                    if (dtProgramData.Rows.Count > 0)
                    {
                        sbTotal.Append("<td style=\"width:15%;\">" + dtProgramData.Rows[0]["AuditAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtProgramData.Rows[0]["SpecialtyHeadAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtProgramData.Rows[0]["ProofreadAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtProgramData.Rows[0]["DesignAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtProgramData.Rows[0]["TotalCount"].ToString() + "</td>");
                    }
                    else
                    {
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                    }
                    sbTotal.Append("</tr>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                {
                    if (CoperationProcess != 3)
                    {
                        sbTotal.Append("<tr>");
                    }

                    sbTotal.Append("<td style=\"width:15%;\">初步设计</td>");
                    if (dtPreliminaryData.Rows.Count > 0)
                    {
                        sbTotal.Append("<td style=\"width:15%;\">" + dtPreliminaryData.Rows[0]["AuditAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtPreliminaryData.Rows[0]["SpecialtyHeadAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\"> " + dtPreliminaryData.Rows[0]["ProofreadAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtPreliminaryData.Rows[0]["DesignAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtPreliminaryData.Rows[0]["TotalCount"].ToString() + "</td>");
                    }
                    else
                    {
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                    }
                    sbTotal.Append("</tr>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                {
                    if (CoperationProcess != 2)
                    {
                        sbTotal.Append("<tr>");
                    }
                    sbTotal.Append("<td style=\"width:15%;\">施工图设计</td>");
                    if (dtWorkDrawData.Rows.Count > 0)
                    {
                        sbTotal.Append("<td style=\"width:15%;\">" + dtWorkDrawData.Rows[0]["AuditAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtWorkDrawData.Rows[0]["SpecialtyHeadAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtWorkDrawData.Rows[0]["ProofreadAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtWorkDrawData.Rows[0]["DesignAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtWorkDrawData.Rows[0]["TotalCount"].ToString() + "</td>");
                    }
                    else
                    {
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                    }
                    sbTotal.Append("</tr>");
                    sbTotal.Append("<tr><td style=\"width:15%;\">后期服务</td>");
                    if (dtLateStageData.Rows.Count > 0)
                    {
                        sbTotal.Append("<td style=\"width:15%;\">" + dtLateStageData.Rows[0]["AuditAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtLateStageData.Rows[0]["SpecialtyHeadAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtLateStageData.Rows[0]["ProofreadAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtLateStageData.Rows[0]["DesignAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtLateStageData.Rows[0]["TotalCount"].ToString() + "</td>");
                    }
                    else
                    {
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                    }
                    sbTotal.Append("</tr>");
                }
                //室外工程
                if (CoperationProcess >= 4 && CoperationProcess != 10)
                {
                    if (CoperationProcess == 4)
                    {
                        sbTotal.Append("<td  style=\"width:15%;\">室外工程</td>");
                    }
                    else if (CoperationProcess == 5)
                    {
                        sbTotal.Append("<td style=\"width:15%;\"> 锅炉房 </td>");
                    }
                    else if (CoperationProcess == 6)
                    {
                        sbTotal.Append("<td style=\"width:15%;\"> 地上单建水泵房 </td>");

                    }
                    else if (CoperationProcess == 7)
                    {
                        sbTotal.Append("<td  style=\"width:15%;\"> 地上单建变配所(室) </td>");

                    }
                    else if (CoperationProcess == 8)
                    {
                        sbTotal.Append("<td  style=\"width:15%;\"> 单建地下室（车库） </td>");
                    }
                    DataTable dtOut = new DataView(dtValue) { RowFilter = "Specialty='" + speName + "'" }.ToTable();
                    if (dtOut.Rows.Count > 0)
                    {
                        sbTotal.Append("<td style=\"width:15%;\">" + dtOut.Rows[0]["AuditAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtOut.Rows[0]["SpecialtyHeadAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtOut.Rows[0]["ProofreadAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtOut.Rows[0]["DesignAmount"].ToString() + "</td>");
                        sbTotal.Append("<td style=\"width:15%;\">" + dtOut.Rows[0]["TotalCount"].ToString() + "</td>");
                    }
                    else
                    {
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        sbTotal.Append("<td style=\"width:15%;\">0</td>");
                    }

                    sbTotal.Append("</tr>");
                }

                sbTotal.Append("</table></div>");
                //方案
                StringBuilder sbOne = new StringBuilder();
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                {
                    sbOne.Append(getMemberValue(dtProgramData, speName, dtmember, 0));
                }
                //初步
                StringBuilder sbTwo = new StringBuilder();
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                {
                    sbTwo.Append(getMemberValue(dtPreliminaryData, speName, dtmember, 1));
                }

                //施工图
                StringBuilder sbThree = new StringBuilder();
                StringBuilder sbFour = new StringBuilder();
                if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                {
                    sbThree.Append(getMemberValue(dtWorkDrawData, speName, dtmember, 2));
                    //后期                 
                    sbFour.Append(getMemberValue(dtLateStageData, speName, dtmember, 3));
                }
                StringBuilder strTab = new StringBuilder();
                StringBuilder sbOut = new StringBuilder();
                //室外工程
                if (CoperationProcess >= 4 &&  CoperationProcess != 10)
                {
                    sbOut.Append(sbTotal.ToString());
                    DataTable dtOutProcess = new DataView(dtValue) { RowFilter = "Specialty='" + speName + "'" }.ToTable();
                    sbOut.Append(getMemberValue(dtOutProcess, speName, dtmember, 4));
                }
                else
                {
                    strTab.Append(sbTotal.ToString());
                }


                strTab.Append(" <div id=\"tabsMem\">  <ul style=\"width:98%; margin:auto;\"> ");
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                {
                    strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabsMem-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                {
                    strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMem-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");

                }
                if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                {
                    strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMem-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                    strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMem-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
                }
                strTab.Append(" </ul>");
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                {
                    strTab.Append("<div id=\"tabsMem-1\">");
                    strTab.Append(sbOne.ToString());
                    strTab.Append(" </div>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                {
                    strTab.Append(" <div id=\"tabsMem-2\">");
                    strTab.Append(sbTwo.ToString());
                    strTab.Append(" </div>");
                }

               
                if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                {
                    strTab.Append(" <div id=\"tabsMem-3\">");
                    strTab.Append(sbThree.ToString());
                    strTab.Append(" </div>");
                    strTab.Append(" <div id=\"tabsMem-4\">");
                    strTab.Append(sbFour.ToString());
                    strTab.Append(" </div>");
                }
                strTab.Append(" </div>");
                if (CoperationProcess >= 4 && CoperationProcess != 10)
                {
                    lbl_Member.Text = sbOut.ToString();
                }
                else
                {
                    lbl_Member.Text = strTab.ToString();
                }
            }
            else
            {
                string noData = "<table style=\"100%\"><tr><td colspan=\"10\" style=\"color: Red; \">没有人员信息</td></tr></table>";
                lbl_Member.Text = noData;
            }
        }


        /// <summary>
        /// 得到人员产值
        /// </summary>
        /// <param name="dtProcess">阶段金额</param>
        /// <param name="speName">专业</param>
        /// <param name="dtMember">人员</param>
        /// <param name="process">阶段</param>
        /// <returns></returns>
        private string getMemberValue(DataTable dtProcess, string speName, DataTable dtMember, int process)
        {

            StringBuilder sb = new StringBuilder();
            if (dtProcess.Rows.Count > 0)
            {
                sb.Append(" <div id=\"tbProjectValueBymember\" class=\"cls_Container_Report\" >");
                sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"><tr> ");

                // sb.Append("  <table class=\"cls_Tbhead\" id=\"tbProjectValueBymember\"  width=\"100%\"> <tr> ");
                if (process == 0)
                {
                    sb.Append("<td style=\"text-align: center;\"> 方案设计-策划人员产值分配比例% </td> </tr>");
                }
                else if (process == 1)
                {
                    sb.Append("<td  style=\"text-align: center;\"> 初步设计-策划人员产值分配比例% </td> </tr>");
                }
                else if (process == 2)
                {
                    sb.Append("<td  style=\"text-align: center;\"> 施工图设计-策划人员产值分配比例% </td> </tr>");
                }
                else if (process == 3)
                {
                    sb.Append("<td  style=\"text-align: center;\"> 后期服务-策划人员产值分配比例% </td> </tr>");
                }
                else if (process == 4)
                {
                    sb.Append("<td  style=\"text-align: center;\"> 策划人员产值分配比例% </td> </tr>");
                }
                sb.Append("</table>");
                sb.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                sb.Append("<tr ><td style=\"width: 10%; text-align: center;height:15px;\">  名称</td><td style=\"width: 10%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
                sb.Append(" <tr> <td style=\"width: 10%; text-align: center;\" > 专业 </td><td style=\"width: 10%; text-align: center;\">   姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
                sb.Append(" </table>");

                if (process == 0)
                {
                    sb.Append("<table id=\"gvProjectValueOneBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\"  class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
                }
                else if (process == 1)
                {
                    sb.Append("<table id=\"gvProjectValueTwoBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
                }
                else if (process == 2)
                {
                    sb.Append("<table id=\"gvProjectValueThreeBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
                }
                else if (process == 3)
                {
                    sb.Append("<table id=\"gvProjectValueFourBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
                }
                else
                {
                    sb.Append("<table id=\"gvProjectValueBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
                }

                sb.Append("<tr>");
                sb.Append("<td  width= \"10%\" class=\"cls_Column\" rowspan=\"" + dtMember.Rows.Count + "\">" + speName + "</td>");


                for (int j = 0; j < dtMember.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td class=\"display\"></td>");
                    }
                    string name = dtMember.Rows[j]["mem_Name"].ToString();

                    sb.Append("<td class=\"display\" wp=\"0\">" + dtMember.Rows[j]["mem_ID"].ToString() + "</td>");
                    sb.Append("<td width= \"10%\">" + dtMember.Rows[j]["mem_Name"].ToString() + "</td>");

                    //该人员是否是设计人员
                    int designCount = new DataView(dtMember) { RowFilter = "spe_Name='" + speName + "' AND  mem_Name='" + name + "' AND mem_isDesigner='1'" }.ToTable().Rows.Count;

                    if (designCount <= 0)
                    {
                        sb.Append("<td width= \"10%\"> 0%</td>");
                        sb.Append("<td width= \"10%\">0</td>");
                    }
                    else
                    {
                        sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" runat=\"server\" zy=" + speName + " sz=" + dtMember.Rows[j]["mem_Principalship_ID"].ToString() + " />%</td>");
                        sb.Append("<td width= \"10%\"></td>");
                    }

                    sb.Append("<td class=\"display\" >" + dtProcess.Rows[0]["DesignAmount"] + "</td>");

                    //该人员是否是专业负责
                    int headCount = new DataView(dtMember) { RowFilter = "spe_Name='" + speName + "' AND  mem_Name='" + name + "' AND mem_RoleIDs like '%2%'" }.ToTable().Rows.Count;
                    if (headCount <= 0)
                    {
                        sb.Append("<td width= \"10%\"> 0%</td>");
                        sb.Append("<td width= \"10%\">0</td>");
                    }
                    else
                    {
                        sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" />%</td>");
                        sb.Append("<td width= \"10%\"></td>");
                    }

                    sb.Append("<td class=\"display\" >" + dtProcess.Rows[0]["SpecialtyHeadAmount"] + "</td>");

                    //该人员是否是审核
                    if (speName == "建筑" || speName == "结构")
                    {
                        sb.Append("<td width= \"10%\">0% </td>");
                        sb.Append("<td width= \"10%\">0</td>");
                        sb.Append("<td class=\"display\" >0</td>");
                    }
                    else
                    {
                        int auditCount = new DataView(dtMember) { RowFilter = "spe_Name='" + speName + "' AND  mem_Name='" + name + "' AND mem_RoleIDs like '%4%'" }.ToTable().Rows.Count;
                        if (auditCount <= 0)
                        {
                            sb.Append("<td width= \"10%\">0% </td>");
                            sb.Append("<td width= \"10%\">0</td>");
                        }
                        else
                        {
                            sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" />%</td>");
                            sb.Append("<td width= \"10%\"></td>");
                        }
                        sb.Append("<td class=\"display\" >" + dtProcess.Rows[0]["AuditAmount"].ToString() + "</td>");
                    }


                    //该人员是否是校对
                    int proofreadCount = new DataView(dtMember) { RowFilter = "spe_Name='" + speName + "' AND  mem_Name='" + name + "' AND mem_RoleIDs like '%3%'" }.ToTable().Rows.Count;
                    if (proofreadCount <= 0)
                    {
                        sb.Append("<td width= \"10%\"> 0%</td>");
                        sb.Append("<td width= \"10%\">0</td>");
                    }
                    else
                    {
                        sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" />%</td>");
                        sb.Append("<td width= \"10%\"></td>");
                    }
                    sb.Append("<td class=\"display\" >" + dtProcess.Rows[0]["ProofreadAmount"] + "</td>");
                    sb.Append("<td class=\"display\" >" + process + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
                sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\" id=\"tbChooseUser\"><tr> ");
                if (process == 0)
                {
                    sb.Append("<td><span id=\"chooseOneUser\" style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseExternalOneUser\"    style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                }
                else if (process == 1)
                {
                    sb.Append("<td><span id=\"chooseTwoUser\"  style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseTwoExternalUser\"   style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                }
                else if (process == 2)
                {
                    sb.Append("<td><span id=\"chooseThreeUser\"  style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseThreeExternalUser\"   style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                }
                else if (process == 3)
                {
                    sb.Append("<td><span id=\"chooseFourUser\"  style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseFourExternalUser\"   style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                }
                else
                {
                    sb.Append("<td><span id=\"chooseUser\" style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseExternalUser\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                }
                sb.Append("</tr></table>");
                sb.Append(" </div>");
            }
            return sb.ToString();
        }

        /// <summary>
        /// 查询该专业负责人审核过的用户
        /// </summary>
        private void CreateDataTableIsAuditUser(int sepID)
        {
            DataTable dtData = BLL.GetIsAuditedUserAcount((int)projectAuditRecordEntity.AllotID, sepID).Tables[0];

            lbl_MemberCount.Text = CreateTableByMemberCount(dtData, true);
        }


        /// <summary>
        /// 分配不通过的人员信息
        /// </summary>
        private void CreateNoDataTableByMember()
        {
            //取得该用户信息

            TG.BLL.tg_member bll = new TG.BLL.tg_member();
            TG.Model.tg_member model = new TG.Model.tg_member();
            DataTable dtDataAudit = new DataTable();
            //审核人 查看自己
            if (AuditStatus != MessageStatus)
            {
                model = bll.GetModel(UserSysNo);
            }
            else //申请看到所有不通过审核信息
            {
                //取得最后一个审核人
                int lastAuditUser = getLastAudit(projectAuditRecordEntity.AuditUser);

                model = bll.GetModel(lastAuditUser);

                //取得审核通过信息
                dtDataAudit = BLL.GetProjectValueByMemberAcount((int)projectAuditRecordEntity.AllotID).Tables[0];
            }

            DataSet ds = BLL.GetProjectValueByMemberNoData(ProReferenceSysNo, model.mem_Speciality_ID);

            DataTable dtmember = ds.Tables[0];

            //专业 表头

            StringBuilder sbHtml = new StringBuilder();

            StringBuilder program = new StringBuilder();
            StringBuilder preliminary = new StringBuilder();
            StringBuilder workDraw = new StringBuilder();
            StringBuilder lateStage = new StringBuilder();
            StringBuilder sbOut = new StringBuilder();

            if (MessageStatus == AuditStatus)
            {
                if (dtDataAudit.Rows.Count > 0)
                {
                    //方案工序
                    DataTable dtProgram = new DataView(dtDataAudit) { RowFilter = "ItemType=0" }.ToTable();

                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                    {
                        program.Append(createMemberCountDetail(dtProgram, 0, false));
                    }
                    //初步工序
                    DataTable dtPreliminary = new DataView(dtDataAudit) { RowFilter = "ItemType=1" }.ToTable();
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                    {
                        preliminary.Append(createMemberCountDetail(dtPreliminary, 1, false));
                    }

                    if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        //施工图
                        DataTable dtWorkDraw = new DataView(dtDataAudit) { RowFilter = "ItemType=2" }.ToTable();
                        workDraw.Append(createMemberCountDetail(dtWorkDraw, 2, false));

                        //后期服务
                        DataTable dtLateStage = new DataView(dtDataAudit) { RowFilter = "ItemType=3" }.ToTable();
                        lateStage.Append(createMemberCountDetail(dtLateStage, 3, false));
                    }

                    if (CoperationProcess >= 4 && CoperationProcess != 10)
                    {
                        sbOut.Append(createMemberCountDetail(dtDataAudit, CoperationProcess, false));
                    }
                }
                else
                {
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                    {
                        program.Append(createNotAuditMemberCountDetail(dtmember, 0));
                    }
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                    {
                        preliminary.Append(createNotAuditMemberCountDetail(dtmember, 1));
                    }
                    if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        workDraw.Append(createNotAuditMemberCountDetail(dtmember, 2));
                        lateStage.Append(createNotAuditMemberCountDetail(dtmember, 3));
                    }

                    if (CoperationProcess >= 4 && CoperationProcess != 10)
                    {
                        sbOut.Append(createNotAuditMemberCountDetail(dtmember, 4));
                    }
                }
            }
            else
            {
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                {
                    program.Append(createNotAuditMemberCountDetail(dtmember, 0));
                }
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                {
                    preliminary.Append(createNotAuditMemberCountDetail(dtmember, 1));
                }
                if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                {
                    workDraw.Append(createNotAuditMemberCountDetail(dtmember, 2));
                    lateStage.Append(createNotAuditMemberCountDetail(dtmember, 3));
                }

                if (CoperationProcess >= 4 &&  CoperationProcess != 10)
                {
                    sbOut.Append(createNotAuditMemberCountDetail(dtmember, 4));
                }
            }

            StringBuilder strTab = new StringBuilder();
            strTab.Append(" <div id=\"tabsMemAmount\">  <ul style=\"width:98%; margin:auto;\"> ");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabsMemAmount-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
            }
            strTab.Append(" </ul><div id=\"tabsMemAmount-1\">");
            strTab.Append(program);
            strTab.Append(" </div>");
            strTab.Append(" <div id=\"tabsMemAmount-2\">");
            strTab.Append(preliminary);
            strTab.Append(" </div>");
            strTab.Append(" <div id=\"tabsMemAmount-3\">");
            strTab.Append(workDraw);
            strTab.Append(" </div>");
            strTab.Append(" <div id=\"tabsMemAmount-4\">");
            strTab.Append(lateStage);
            strTab.Append(" </div>");
            strTab.Append(" </div>");
            if (CoperationProcess >= 4 &&  CoperationProcess != 10)
            {
                lbl_Member.Text = sbOut.ToString();
            }
            else
            {
                lbl_Member.Text = strTab.ToString();
            }
        }


        /// <summary>
        /// 生成分配之后的产值人员
        /// </summary>
        private void CreateTableByMemberCount()
        {

            DataTable dtData = BLL.GetProjectValueByMemberAcount((int)projectAuditRecordEntity.AllotID).Tables[0];

            lbl_MemberCount.Text = CreateTableByMemberCount(dtData, false);
        }

        /// <summary>
        /// 生成分配之后的产值人员
        /// </summary>
        /// <param name="dtData"></param>
        /// <returns></returns>
        private string CreateTableByMemberCount(DataTable dtData, bool isAudited)
        {

            //方案工序
            string program = "";
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                DataTable dtProgram = new DataView(dtData) { RowFilter = "ItemType=0" }.ToTable();
                program = createMemberCountDetail(dtProgram, 0, isAudited);
            }
            //初步工序
            string preliminary = "";
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                DataTable dtPreliminary = new DataView(dtData) { RowFilter = "ItemType=1" }.ToTable();
                preliminary = createMemberCountDetail(dtPreliminary, 1, isAudited);
            }
            //施工图
            string workDraw = "";
            string lateStage = "";
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                DataTable dtWorkDraw = new DataView(dtData) { RowFilter = "ItemType=2" }.ToTable();
                workDraw = createMemberCountDetail(dtWorkDraw, 2, isAudited);

                //后期服务
                DataTable dtLateStage = new DataView(dtData) { RowFilter = "ItemType=3" }.ToTable();
                lateStage = createMemberCountDetail(dtLateStage, 3, isAudited);
            }

            if (CoperationProcess >= 4 && CoperationProcess != 10)
            {
                return createMemberCountDetail(dtData, CoperationProcess, isAudited);
            }
            else
            {
                StringBuilder strTab = new StringBuilder();
                strTab.Append(" <div id=\"tabsMemAmount\">  <ul style=\"width:98%; margin:auto;\"> ");
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                {
                    strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabsMemAmount-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                {
                    strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");

                }
                if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                {
                    strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                    strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");

                }
                strTab.Append(" </ul>");
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                {
                    strTab.Append("<div id=\"tabsMemAmount-1\">");
                    strTab.Append(program);
                    strTab.Append(" </div>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                {
                    strTab.Append(" <div id=\"tabsMemAmount-2\">");
                    strTab.Append(preliminary);
                    strTab.Append(" </div>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                {
                    strTab.Append(" <div id=\"tabsMemAmount-3\">");
                    strTab.Append(workDraw);
                    strTab.Append(" </div>");
                    strTab.Append(" <div id=\"tabsMemAmount-4\">");
                    strTab.Append(lateStage);
                    strTab.Append(" </div>");
                }
                strTab.Append(" </div>");

                return strTab.ToString();
            }
        }

        /// <summary>
        /// 生成人员产值明细
        /// </summary>
        /// <param name="dtMemberCount"></param>
        /// <param name="process"></param>
        /// <returns></returns>
        private string createMemberCountDetail(DataTable dtData, int process, bool isAudited)
        {
            //专业 表头
            StringBuilder sbHead = new StringBuilder();

            if (isAudited)
            {
                sbHead.Append(" <div id=\"tbIsAuditedBymemberAmount\" class=\"cls_Container_Report\" >");
                //取得该专业的审核人
                if (dtData.Rows.Count > 0)
                {
                    SpecialityAuditUser = int.Parse(dtData.Rows[0]["AuditUser"].ToString());
                }
            }
            else
            {
                sbHead.Append(" <div id=\"tbProjectValueBymemberAmount\" class=\"cls_Container_Report\" >");
            }
            if (process == 0)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  方案设计-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            else if (process == 1)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  初步设计-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 2)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  施工图设计-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 3)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  后期服务-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 4)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  室外工程-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 5)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  锅炉房-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            else if (process == 6)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建水泵房-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            else if (process == 7)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建变配所(室)-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 8)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  单建地下室（车库）-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbHead.Append("<tr ><td style=\"width: 10%; text-align: center;\">  名称</td><td style=\"width: 10%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sbHead.Append(" <tr > <td style=\"width: 10%; text-align: center;\" > 专业 </td><td style=\"width: 10%; text-align: center;\">   姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
            sbHead.Append(" </table>");

            sbHead.Append("<table id=\"gvProjectValueBymemberAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

            StringBuilder sbHtml = new StringBuilder();

            //取得专业
            var dtSpecialtyList = new DataView(dtData).ToTable("Specialty", true, "spe_Name");

            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                var dv = new DataView(dtData) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();

                sbHtml.Append("<tr>");
                sbHtml.Append("<td  width= \"10%\" class=\"cls_Column\" rowspan=\"" + dtMemberBySpecialty.Rows.Count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sbHtml.Append("<tr>");
                    }

                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["DesignPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["DesignCount"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["SpecialtyHeadPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["SpecialtyHeadCount"].ToString() + "</td>");

                    sbHtml.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["AuditPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["AuditCount"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["ProofreadPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["ProofreadCount"].ToString() + "</td>");
                    sbHtml.Append("</tr>");
                }


            }
            sbHead.Append(sbHtml.ToString());
            sbHead.Append(" </table></div>");

            return sbHead.ToString();
        }

        /// <summary>
        /// 生成未分配的人员信息
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="process"></param>
        /// <returns></returns>
        private string createNotAuditMemberCountDetail(DataTable dt, int process)
        {
            //专业 表头
            StringBuilder sbHead = new StringBuilder();
            sbHead.Append(" <div id=\"tbProjectValueBymemberNOAmount\" class=\"cls_Container_Report\" >");

            if (process == 0)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  方案设计-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            else if (process == 1)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  初步设计-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 2)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  施工图设计-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 3)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  后期服务-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  项目策划人员产值分配比例% </td> </tr> </table>");

            }
            sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbHead.Append("<tr ><td style=\"width: 10%; text-align: center;\">  名称</td><td style=\"width: 10%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sbHead.Append(" <tr > <td style=\"width: 10%; text-align: center;\" > 专业 </td><td style=\"width: 10%; text-align: center;\">   姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
            sbHead.Append(" </table>");

            sbHead.Append("<table id=\"gvProjectValueBymemberAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\" >");

            StringBuilder sbHtml = new StringBuilder();



            //取得设计人员的专业
            var dtSpecialtyList = new DataView(dt).ToTable("Specialty", true, "spe_Name");


            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                var dv = new DataView(dt) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();


                sbHtml.Append("<tr>");
                sbHtml.Append("<td  width= \"10%\" class=\"cls_Column\" rowspan=\"" + dtMemberBySpecialty.Rows.Count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sbHtml.Append("<tr>");
                        sbHtml.Append("<td class=\"display\"></td>");
                    }
                    string name = dtMemberBySpecialty.Rows[j]["mem_Name"].ToString();

                    sbHtml.Append("<td class=\"display\">" + dtMemberBySpecialty.Rows[j]["mem_ID"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");

                    //该人员是否是设计人员
                    int designCount = new DataView(dt) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "' AND  mem_Name='" + name + "' AND mem_isDesigner='1'" }.ToTable().Rows.Count;

                    if (designCount <= 0)
                    {
                        sbHtml.Append("<td width= \"10%\"> 0%</td>");
                        sbHtml.Append("<td width= \"10%\">0</td>");
                    }
                    else
                    {
                        sbHtml.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" />%</td>");
                        sbHtml.Append("<td width= \"10%\"></td>");
                    }


                    //该人员是否是专业负责
                    int headCount = new DataView(dt) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "' AND  mem_Name='" + name + "' AND mem_RoleIDs like '%2%'" }.ToTable().Rows.Count;
                    if (headCount <= 0)
                    {
                        sbHtml.Append("<td width= \"10%\"> 0%</td>");
                        sbHtml.Append("<td width= \"10%\">0</td>");
                    }
                    else
                    {
                        sbHtml.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" />%</td>");
                        sbHtml.Append("<td width= \"10%\"></td>");
                    }

                    //该人员是否是审核
                    int auditCount = new DataView(dt) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "' AND  mem_Name='" + name + "' AND mem_RoleIDs like '%4%'" }.ToTable().Rows.Count;
                    if (auditCount <= 0)
                    {
                        sbHtml.Append("<td width= \"10%\">0% </td>");
                        sbHtml.Append("<td width= \"10%\">0</td>");
                    }
                    else
                    {
                        sbHtml.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" />%</td>");
                        sbHtml.Append("<td width= \"10%\"></td>");
                    }

                    //该人员是否是校对
                    int proofreadCount = new DataView(dt) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "' AND  mem_Name='" + name + "' AND mem_RoleIDs like '%3%'" }.ToTable().Rows.Count;
                    if (proofreadCount <= 0)
                    {
                        sbHtml.Append("<td width= \"10%\"> 0%</td>");
                        sbHtml.Append("<td width= \"10%\">0</td>");
                    }
                    else
                    {
                        sbHtml.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" />%</td>");
                        sbHtml.Append("<td width= \"10%\"></td>");
                    }
                    sbHtml.Append("</tr>");
                }
            }
            sbHead.Append(sbHtml.ToString());
            sbHead.Append(" </table></div>");

            return sbHead.ToString();
        }

        #region 暖通所二次分配

        /// <summary>
        /// 绑定暖通所的项目产值分配信息
        /// </summary>
        private void bindHaveValue()
        {
            if (AuditStatus == "F" || AuditStatus == "I")
            {
                string itemType = lblBuildType.Text.Trim();

                if (!string.IsNullOrEmpty(itemType))
                {
                    decimal allotCount = decimal.Parse(txt_ActulCount.Text);

                    //参与策划人员的专业
                    DataTable dtPlanSpecialty = BLL.GetPlanUserSpecialtyByHaveProject(ProSysNo).Tables[0];

                    string specialty = "";
                    foreach (DataRow row in dtPlanSpecialty.Rows)
                    {
                        specialty += "'" + row["spe_Name"].ToString() + "'" + ",";
                    }

                    if (!string.IsNullOrEmpty(specialty))
                    {
                        specialty = specialty.Remove(specialty.Length - 1);


                        DataSet dsAllot = BLL.GetCprProcessValueDataList(itemType, CoperationProcess, allotCount, specialty, IsRounding);

                        //设计阶段
                        DataTable dtStageValue = dsAllot.Tables[0];

                        //设计阶段与专业
                        DataTable dtStageSpeValue = new DataView(dsAllot.Tables[1]) { RowFilter = "IsHaved=1" }.ToTable();

                        //工序
                        DataTable dtProcessValue = dsAllot.Tables[2];

                        //生成方案设计 初步设计 施工图 后期服务表单
                        if (CoperationProcess < 4 || CoperationProcess == 10)
                        {
                            CreateHaveTable(dtStageValue, dtStageSpeValue, dtProcessValue);
                        }
                        else
                        {
                            DataTable dtSpeOutdt = new DataView(dsAllot.Tables[3]) { RowFilter = "IsHaved=1" }.ToTable();
                            if (CoperationProcess == 5)
                            {
                                dtSpeOutdt = new DataView(dtSpeOutdt) { RowFilter = "type like '" + TypeMark + "'" }.ToTable();
                            }
                            CreateHavcOtherTable(dtSpeOutdt, dtProcessValue, allotCount);
                        }

                        gvOne.DataSource = dtStageValue;
                        gvOne.DataBind();

                        gvTwo.DataSource = dtStageValue;
                        gvTwo.DataBind();

                        gvThree.DataSource = dtStageValue;
                        gvThree.DataBind();

                        gvFour.DataSource = dtStageValue;
                        gvFour.DataBind();

                        //方案+施工图+后期
                        gvTen.DataSource = dtStageValue;
                        gvTen.DataBind();
                    }
                    else
                    {
                        StringBuilder sbSpeHead = new StringBuilder();
                        sbSpeHead.Append(" <table class=\"cls_table_head\" id=\"tbNoData\" style=\"display: none;\">");
                        sbSpeHead.Append("<tr>");
                        sbSpeHead.Append("<td style=\"color: Red;\">没有查询到信息。请检查项目策划是否分配正确、或者产值分配参数是否设置正确</td>");
                        sbSpeHead.Append("</tr></table>");
                        lbl_NoData.Text = sbSpeHead.ToString();
                    }
                }
            }

        }

        /// <summary>
        /// 生成表单
        /// </summary>
        /// <param name="dtStageValue">设计阶段</param>
        /// <param name="dtStageSpeValue">专业</param>
        /// <param name="dtProcessValue">工序</param>
        private void CreateHaveTable(DataTable dtStageValue, DataTable dtStageSpeValue, DataTable dtProcessValue)
        {
            //取得专业
            var dtSpecialtyList = new DataView(dtStageSpeValue).ToTable("Specialty", true, "Specialty");

            int count = dtSpecialtyList.Rows.Count;

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();
            sbSpeHead.Append(" <div id=\"stagespetable\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  项目各设计阶段产值分配比例% </td> </tr> </table>");
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 设计阶段</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">暖通</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">合计</td>");

            //比例

            sbSpeHead.Append(" </tr> </table>");

            sbSpeHead.Append("<table id=\"gvProjectStageSpe\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\"  >");


            #region  方案设计
            //方案设计工序
            StringBuilder sb_ProcessOneTable = new StringBuilder();
            sb_ProcessOneTable.Append("<div id=\"designProcessOneTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  方案设计工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td> </tr> </table>");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllotHead\" style=\"width: 98%;\">");
            sb_ProcessOneTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
            sb_ProcessOneTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
            sb_ProcessOneTable.Append("</table>");
            sb_ProcessOneTable.Append("<table id=\"gvdesignProcessOne\" width=\"98%\"  class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\" >");

            //方案设计
            var dataProgram = new DataView(dtStageSpeValue).ToTable("ProgramPercent", false, "ProgramPercent", "Specialty");


            //第一行循环方案设计专业
            StringBuilder sb_one = new StringBuilder();

            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                sb_one.Append("<tr>");
                sb_one.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">方案设计</td>");

                //方案设计金额
                decimal ProgramCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["ProgramCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["ProgramCOUNT"].ToString());

                //计算方案设计总百分比
                var sumProgramPercent = dataProgram.AsEnumerable().Sum(s => s.Field<decimal>("ProgramPercent"));

                decimal sumOther = 100 - sumProgramPercent;

                #region 建筑

                //取得建筑
                decimal bulidingOnePercent = 0;
                //看是否有建筑人员参与
                var bulidingOneData = new DataView(dataProgram) { RowFilter = "Specialty='建筑'" }.ToTable();

                if (bulidingOneData != null && bulidingOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingOnePercent = decimal.Parse(bulidingOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }

                decimal bulidingOneCount = 0;

                sb_one.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingOneCount = Math.Round(bulidingOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingOneCount = Math.Floor(bulidingOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"7.5%\">" + bulidingOneCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureOnePercent = 0;
                //看是否有结构人员参与
                var structureOneData = new DataView(dataProgram) { RowFilter = "Specialty='结构'" }.ToTable();

                if (structureOneData != null && structureOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureOnePercent = decimal.Parse(structureOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal structureOneCount = 0;

                sb_one.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureOneCount = Math.Round(structureOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureOneCount = Math.Floor(structureOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"7.5%\">" + structureOneCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainOnePercent = 0;
                //看是否有给排水人员参与
                var drainOneData = new DataView(dataProgram) { RowFilter = "Specialty='给排水'" }.ToTable();

                if (drainOneData != null && drainOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainOnePercent = decimal.Parse(drainOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal drainOneCount = 0;

                sb_one.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainOneCount = Math.Round(drainOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainOneCount = Math.Floor(drainOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"7.5%\">" + drainOneCount + "</td>");

                #endregion

                #region 暖通

                //取得给排水
                decimal havcOnePercent = 0;
                //看是否有给排水人员参与
                var haveOneData = new DataView(dataProgram) { RowFilter = "Specialty='暖通'" }.ToTable();

                if (haveOneData != null && haveOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        havcOnePercent = decimal.Parse(haveOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal havcOneCount = 0;

                sb_one.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(havcOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    havcOneCount = Math.Round(havcOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    havcOneCount = Math.Floor(havcOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"7.5%\">" + havcOneCount + "</td>");

                #endregion
                #region 电气

                //取得电气
                decimal electricOnePercent = 0;
                //看是否有电气人员参与
                var electricOneData = new DataView(dataProgram) { RowFilter = "Specialty='电气'" }.ToTable();

                if (electricOneData != null && electricOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricOnePercent = decimal.Parse(electricOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal electricOneCount = 0;

                sb_one.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricOneCount = Math.Round(electricOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricOneCount = Math.Floor(electricOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"7.5%\">" + electricOneCount + "</td>");

                #endregion

                #region 合计
                decimal programCount = bulidingOneCount + structureOneCount + drainOneCount + electricOneCount + havcOneCount;

                decimal programPercent = bulidingOnePercent + structureOnePercent + drainOnePercent + electricOnePercent + havcOnePercent;

                sb_one.Append("<td width=\"7.5%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(programPercent, 0) + "</span>%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_one.Append("<td width=\"7.5%\">" + Math.Round(programCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_one.Append("<td width=\"7.5%\">" + Math.Floor(programCount) + "</td>");
                }

                sb_one.Append("</tr>");
                #endregion

                //方案设计工序 --方案为0
                var dtProgramProcessValue = new DataView(dtProcessValue) { RowFilter = "ItemType=0" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("建筑", dtProgramProcessValue, bulidingOneCount, 0));

                //取得结构工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("结构", dtProgramProcessValue, structureOneCount, 0));

                //取得给排水工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("给排水", dtProgramProcessValue, drainOneCount, 0));

                //取得暖通工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("暖通", dtProgramProcessValue, havcOneCount, 0));

                //取得电气工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("电气", dtProgramProcessValue, electricOneCount, 0));

                sb_ProcessOneTable.Append("</table></div>");

            }
            #endregion

            #region 初步设计
            //第一行循环方案设计专业
            StringBuilder sb_two = new StringBuilder();
            //初步设计工序
            StringBuilder sb_ProcessTwoTable = new StringBuilder();
            // 0个阶段 1 方案+初设 二阶段 3 三阶段
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3 )
            {
                sb_two.Append("<tr>");
                sb_two.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">初步设计</td>");


                //方案设计
                var dataPreliminary = new DataView(dtStageSpeValue).ToTable("preliminaryPercent", false, "preliminaryPercent", "Specialty");

                //初步设计金额
                decimal preliminaryCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["preliminaryCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["preliminaryCOUNT"].ToString());

                //计算初步设计总百分比
                var sumPreliminaryPercent = dataPreliminary.AsEnumerable().Sum(s => s.Field<decimal>("preliminaryPercent"));

                sb_ProcessTwoTable.Append("<div id=\"designProcessTwoTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb_ProcessTwoTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  初步设计工序产值分配比例%<span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span>  </td> </tr> </table>");
                sb_ProcessTwoTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                sb_ProcessTwoTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%; height:30px text-align: center;\" colspan=\"2\">    审核   </td> ");
                sb_ProcessTwoTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
                sb_ProcessTwoTable.Append("</table>");
                sb_ProcessTwoTable.Append("<table id=\"gvdesignProcessTwo\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

                var sumTwo = 100 - sumPreliminaryPercent;
                //初步设计
                #region 建筑

                //取得建筑
                decimal bulidingTwoPercent = 0;
                //看是否有建筑人员参与
                var bulidingTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='建筑'" }.ToTable();

                if (bulidingTwoData != null && bulidingTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingTwoPercent = decimal.Parse(bulidingTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }

                decimal bulidingTwoCount = 0;

                sb_two.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingTwoCount = Math.Round(bulidingTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingTwoCount = Math.Floor(bulidingTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"7.5%\">" + bulidingTwoCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureTwoPercent = 0;
                //看是否有结构人员参与
                var structureTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='结构'" }.ToTable();

                if (structureTwoData != null && structureTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureTwoPercent = decimal.Parse(structureTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal structureTwoCount = 0;


                sb_two.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureTwoCount = Math.Round(structureTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureTwoCount = Math.Floor(structureTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"7.5%\">" + structureTwoCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainTwoPercent = 0;
                //看是否有给排水人员参与
                var drainTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='给排水'" }.ToTable();

                if (drainTwoData != null && drainTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainTwoPercent = decimal.Parse(drainTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal drainTwoCount = 0;

                sb_two.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainTwoCount = Math.Round(drainTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainTwoCount = Math.Floor(drainTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"7.5%\">" + drainTwoCount + "</td>");

                #endregion

                #region 暖通

                //取得给排水
                decimal havcTwoPercent = 0;
                //看是否有给排水人员参与
                var havcTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='暖通'" }.ToTable();

                if (havcTwoData != null && havcTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        havcTwoPercent = decimal.Parse(havcTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal havcTwoCount = 0;

                sb_two.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(havcTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    havcTwoCount = Math.Round(havcTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    havcTwoCount = Math.Floor(havcTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"7.5%\">" + havcTwoCount + "</td>");

                #endregion
                #region 电气

                //取得电气
                decimal electricTwoPercent = 0;
                //看是否有电气人员参与
                var electricTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='电气'" }.ToTable();

                if (electricTwoData != null && electricTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricTwoPercent = decimal.Parse(electricTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal electricTwoCount = 0;

                sb_two.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricTwoCount = Math.Round(electricTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricTwoCount = Math.Floor(electricTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"7.5%\">" + electricTwoCount + "</td>");

                #endregion

                #region 合计
                decimal preliminarySumCount = bulidingTwoCount + structureTwoCount + drainTwoCount + electricTwoCount + havcTwoCount;

                decimal preliminarySumPercent = bulidingTwoPercent + structureTwoPercent + drainTwoPercent + electricTwoPercent + havcTwoPercent;

                sb_two.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(preliminarySumPercent, 0) + "</span>%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_two.Append("<td width=\"7.5%\">" + Math.Round(preliminarySumCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_two.Append("<td width=\"7.5%\">" + Math.Floor(preliminarySumCount) + "</td>");
                }

                sb_two.Append("</tr>");
                #endregion

                //初步设计工序 --初步为1
                var dtPreliminaryProcessValue = new DataView(dtProcessValue) { RowFilter = "ItemType=1" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("建筑", dtPreliminaryProcessValue, bulidingTwoCount, 1));

                //取得结构工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("结构", dtPreliminaryProcessValue, structureTwoCount, 1));

                //取得给排水工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("给排水", dtPreliminaryProcessValue, drainTwoCount, 1));

                //取得暖通工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("暖通", dtPreliminaryProcessValue, havcTwoCount, 1));

                //取得电气工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("电气", dtPreliminaryProcessValue, electricTwoCount, 1));

                sb_ProcessTwoTable.Append("</table></div>");

            }
            #endregion

            #region 施工图设计
            //第一行循环方案设计专业
            StringBuilder sb_three = new StringBuilder();

            StringBuilder sb_ProcessThreeTable = new StringBuilder();

            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                sb_three.Append("<tr>");
                sb_three.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">施工图设计</td>");


                //施工图设计
                var dataWorkDraw = new DataView(dtStageSpeValue).ToTable("WorkDrawPercent", false, "WorkDrawPercent", "Specialty");

                //施工图设计金额
                decimal WorkDrawPercentCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["WorkDrawCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["WorkDrawCOUNT"].ToString());

                //计算施工图总百分比
                var sumWorkDrawPercent = dataWorkDraw.AsEnumerable().Sum(s => s.Field<decimal>("WorkDrawPercent"));

                //初步设计工序
                sb_ProcessThreeTable.Append("<div id=\"designProcessThreeTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb_ProcessThreeTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  施工图工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td> </tr> </table>");
                sb_ProcessThreeTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");

                sb_ProcessThreeTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
                sb_ProcessThreeTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center; \" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  ");
                sb_ProcessThreeTable.Append("</tr></table>");
                sb_ProcessThreeTable.Append("<table id=\"gvdesignProcessThree\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

                var sumThree = 100 - sumWorkDrawPercent;
                #region 建筑

                //取得建筑
                decimal bulidingThreePercent = 0;
                //看是否有建筑人员参与
                var bulidingThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='建筑'" }.ToTable();

                if (bulidingThreeData != null && bulidingThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        bulidingThreePercent = decimal.Parse(bulidingThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }

                decimal bulidingThreeCount = 0;

                sb_three.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingThreeCount = Math.Round(bulidingThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingThreeCount = Math.Floor(bulidingThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"7.5%\">" + bulidingThreeCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureThreePercent = 0;
                //看是否有结构人员参与
                var structureThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='结构'" }.ToTable();

                if (structureThreeData != null && structureThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureThreePercent = decimal.Parse(structureThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal structureThreeCount = 0;

                sb_three.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureThreeCount = Math.Round(structureThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureThreeCount = Math.Floor(structureThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"7.5%\">" + structureThreeCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainThreePercent = 0;
                //看是否有给排水人员参与
                var drainThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='给排水'" }.ToTable();

                if (drainThreeData != null && drainThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainThreePercent = decimal.Parse(drainThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal drainThreeCount = 0;

                sb_three.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainThreeCount = Math.Round(drainThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainThreeCount = Math.Floor(drainThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"7.5%\">" + drainThreeCount + "</td>");

                #endregion
                #region 暖通

                //取得给排水
                decimal havcThreePercent = 0;
                //看是否有给排水人员参与
                var havcThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='暖通'" }.ToTable();

                if (havcThreeData != null && havcThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        havcThreePercent = decimal.Parse(havcThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal havcThreeCount = 0;

                sb_three.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(havcThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    havcThreeCount = Math.Round(havcThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    havcThreeCount = Math.Floor(havcThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"7.5%\">" + havcThreeCount + "</td>");

                #endregion
                #region 电气

                //取得电气
                decimal electricThreePercent = 0;
                //看是否有电气人员参与
                var electricThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='电气'" }.ToTable();

                if (electricThreeData != null && electricThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricThreePercent = decimal.Parse(electricThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal electricThreeCount = 0;

                sb_three.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricThreeCount = Math.Round(electricThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricThreeCount = Math.Floor(electricThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"7.5%\">" + electricThreeCount + "</td>");

                #endregion

                #region 合计
                decimal workDrawSumCount = bulidingThreeCount + structureThreeCount + drainThreeCount + electricThreeCount + havcThreeCount;

                decimal workDrawSumPercent = bulidingThreePercent + structureThreePercent + drainThreePercent + electricThreePercent + havcThreePercent;

                sb_three.Append("<td width=\"7.5%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(workDrawSumPercent, 0) + "</span>%</td>");


                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_three.Append("<td width=\"7.5%\">" + Math.Round(workDrawSumCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_three.Append("<td width=\"7.5%\">" + Math.Floor(workDrawSumCount) + "</td>");
                }

                sb_three.Append("</tr>");
                #endregion

                //施工图 --施工图为2
                var dtWorkingProcessValue = new DataView(dtProcessValue) { RowFilter = "ItemType=2" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("建筑", dtWorkingProcessValue, bulidingThreeCount, 2));

                //取得结构工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("结构", dtWorkingProcessValue, structureThreeCount, 2));

                //取得给排水工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("给排水", dtWorkingProcessValue, drainThreeCount, 2));

                //取得暖通工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("暖通", dtWorkingProcessValue, havcThreeCount, 2));

                //取得电气工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("电气", dtWorkingProcessValue, electricThreeCount, 2));

                sb_ProcessThreeTable.Append("</table></div>");
            }
            #endregion

            #region 后期服务
            //第一行循环方案设计专业
            StringBuilder sb_four = new StringBuilder();
            StringBuilder sb_ProcessFourTable = new StringBuilder();
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                sb_four.Append("<tr>");
                sb_four.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">后期服务</td>");

                //后期服务
                var dataLateStage = new DataView(dtStageSpeValue).ToTable("LateStagePercent", false, "LateStagePercent", "Specialty");

                //后期服务金额
                decimal lastStageCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["LateStageCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["LateStageCOUNT"].ToString());

                //后期服务总百分比
                var sumLateStagePercent = dataLateStage.AsEnumerable().Sum(s => s.Field<decimal>("LateStagePercent"));


                //后期服务工序

                sb_ProcessFourTable.Append("<div id=\"designProcessFourTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb_ProcessFourTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  后期服务工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td> </tr> </table>");
                sb_ProcessFourTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");

                sb_ProcessFourTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%; text-align: center;\" colspan=\"2\">    审核   </td> ");
                sb_ProcessFourTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td> ");
                sb_ProcessFourTable.Append("</tr></table>");
                sb_ProcessFourTable.Append("<table id=\"gvdesignProcessFour\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

                var sumFour = 100 - sumLateStagePercent;

                #region 建筑

                //取得建筑
                decimal bulidingFourPercent = 0;
                //看是否有建筑人员参与
                var bulidingFourData = new DataView(dataLateStage) { RowFilter = "Specialty='建筑'" }.ToTable();

                if (bulidingFourData != null && bulidingFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingFourPercent = decimal.Parse(bulidingFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }

                decimal bulidingFourCount = 0;

                sb_four.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingFourCount = Math.Round(bulidingFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingFourCount = Math.Floor(bulidingFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"7.5%\">" + bulidingFourCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureFourPercent = 0;
                //看是否有结构人员参与
                var structureFourData = new DataView(dataLateStage) { RowFilter = "Specialty='结构'" }.ToTable();

                if (structureFourData != null && structureFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureFourPercent = decimal.Parse(structureFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal structureFourCount = 0;

                sb_four.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureFourCount = Math.Round(structureFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureFourCount = Math.Floor(structureFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"7.5%\">" + structureFourCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainFourPercent = 0;
                //看是否有给排水人员参与
                var drainFourData = new DataView(dataLateStage) { RowFilter = "Specialty='给排水'" }.ToTable();

                if (drainFourData != null && drainFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainFourPercent = decimal.Parse(drainFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal drainFourCount = 0;

                sb_four.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainFourCount = Math.Round(drainFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainFourCount = Math.Floor(drainFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"7.5%\">" + drainFourCount + "</td>");

                #endregion
                #region 暖通

                //取得给排水
                decimal havcFourPercent = 0;
                //看是否有给排水人员参与
                var havcFourData = new DataView(dataLateStage) { RowFilter = "Specialty='暖通'" }.ToTable();

                if (havcFourData != null && havcFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        havcFourPercent = decimal.Parse(havcFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal havcFourCount = 0;

                sb_four.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(havcFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    havcFourCount = Math.Round(havcFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    havcFourCount = Math.Floor(havcFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"7.5%\">" + havcFourCount + "</td>");

                #endregion
                #region 电气

                //取得电气
                decimal electricFourPercent = 0;
                //看是否有电气人员参与
                var electricFourData = new DataView(dataLateStage) { RowFilter = "Specialty='电气'" }.ToTable();

                if (electricFourData != null && electricFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricFourPercent = decimal.Parse(electricFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal electricFourCount = 0;

                sb_four.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricFourCount = Math.Round(electricFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricFourCount = Math.Floor(electricFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"7.5%\">" + electricFourCount + "</td>");

                #endregion

                #region 合计
                decimal lastSumCount = bulidingFourCount + structureFourCount + drainFourCount + electricFourCount + havcFourCount;

                decimal lastSumPercent = bulidingFourPercent + structureFourPercent + drainFourPercent + electricFourPercent + havcFourPercent;

                sb_four.Append("<td width=\"7.5%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(lastSumPercent, 0) + "</span>%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_four.Append("<td width=\"7.5%\">" + Math.Round(lastSumCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_four.Append("<td width=\"7.5%\">" + Math.Floor(lastSumCount) + "</td>");
                }

                sb_four.Append("</tr>");
                #endregion

                //后期--后期为3
                var dtLateStageProcessValue = new DataView(dtProcessValue) { RowFilter = "ItemType=3" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("建筑", dtLateStageProcessValue, bulidingFourCount, 3));

                //取得结构工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("结构", dtLateStageProcessValue, structureFourCount, 3));

                //取得给排水工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("给排水", dtLateStageProcessValue, drainFourCount, 3));

                //取得给排水工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("暖通", dtLateStageProcessValue, havcFourCount, 3));

                //取得电气工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("电气", dtLateStageProcessValue, electricFourCount, 3));
                sb_ProcessFourTable.Append("</table></div>");
            }
            #endregion

            if (CoperationProcess == 0)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 1)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
            }

            else if (CoperationProcess == 2)
            {
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 3)
            {
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 10)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            sbSpeHead.Append("</table></div>");

            lbl_stagespetable.Text = sbSpeHead.ToString();

            StringBuilder strTab = new StringBuilder();
            strTab.Append(" <div id=\"tabs\" >  <ul style=\"width:98%; margin:auto;\"> ");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabs-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabs-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");
            }

            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabs-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabs-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
            }

            strTab.Append("</ul>");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append(" <div id=\"tabs-1\">");
                strTab.Append(sb_ProcessOneTable.ToString());
                strTab.Append(" </div>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append(" <div id=\"tabs-2\">");
                strTab.Append(sb_ProcessTwoTable.ToString());
                strTab.Append(" </div>");
            }

           
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                strTab.Append(" <div id=\"tabs-3\">");
                strTab.Append(sb_ProcessThreeTable.ToString());
                strTab.Append(" </div>");
                strTab.Append(" <div id=\"tabs-4\">");
                strTab.Append(sb_ProcessFourTable.ToString());
                strTab.Append(" </div>");
            }
            strTab.Append(" </div>");
            lbl_Tab.Text = strTab.ToString();

        }


        /// <summary>
        /// 生成表四之后的表单
        /// </summary>
        /// <param name="dtSpe">专业</param>
        /// <param name="dtProcess">工序</param>
        private void CreateHavcOtherTable(DataTable dtSpe, DataTable dtProcess, decimal allotCount)
        {
            //取得专业
            var dtSpecialtyList = new DataView(dtSpe).ToTable("Specialty", true, "Specialty");

            int count = dtSpecialtyList.Rows.Count;

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();
            sbSpeHead.Append(" <div id=\"stagetabletfive\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> ");

            if (CoperationProcess == 4)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  室外工程产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 5)
            {
                sbSpeHead.Append("<tr><td align=\"center\">  锅炉房产值分配比例%</td></tr>");
            }
            else if (CoperationProcess == 6)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  地上单建水泵房产值分配比例%</td></tr>");
            }
            else if (CoperationProcess == 7)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  地上单建变配所(室)产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 8)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  单建地下室（车库）产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 9)
            {
                sbSpeHead.Append("<tr><td align=\"center\">  市政道路工程产值分配比例% </td></tr>");
            }

            //比例
            decimal percent = decimal.Divide(92, count * 2);
            percent = decimal.Round(percent, 2);
            sbSpeHead.Append("</table>");
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 专业</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">暖通</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">合计</td>");

            sbSpeHead.Append("</tr></table>");

            sbSpeHead.Append("<table id=\"gvFive\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\"  >");


            //
            StringBuilder sb_one = new StringBuilder();
            sb_one.Append("<tr>");
            sb_one.Append("<td class=\"cls_Column\" style=\"width:10%\">比例</td>");

            //计算方案设计总百分比
            var sumProgramPercent = dtSpe.AsEnumerable().Sum(s => s.Field<decimal>("SpecialtyPercent"));


            decimal sumOther = 100 - sumProgramPercent;

            #region 建筑

            //取得建筑
            decimal bulidingOnePercent = 0;
            //看是否有建筑人员参与
            var bulidingOneData = new DataView(dtSpe) { RowFilter = "Specialty='建筑'" }.ToTable();

            if (bulidingOneData != null && bulidingOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    bulidingOnePercent = decimal.Parse(bulidingOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }

            decimal bulidingOneCount = 0;

            sb_one.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingOnePercent, 2).ToString() + " />%</td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                bulidingOneCount = Math.Round(bulidingOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
            }
            else //去整
            {
                bulidingOneCount = Math.Floor(bulidingOnePercent / 100 * allotCount);
            }

            sb_one.Append("<td width=\"7.5%\">" + bulidingOneCount + "</td>");

            #endregion

            #region 结构

            //取得结构
            decimal structureOnePercent = 0;
            //看是否有结构人员参与
            var structureOneData = new DataView(dtSpe) { RowFilter = "Specialty='结构'" }.ToTable();

            if (structureOneData != null && structureOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    structureOnePercent = decimal.Parse(structureOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal structureOneCount = 0;

            sb_one.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureOnePercent, 2).ToString() + " />%</td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                structureOneCount = Math.Round(structureOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
            }
            else //去整
            {
                structureOneCount = Math.Floor(structureOnePercent / 100 * allotCount);
            }

            sb_one.Append("<td width=\"7.5%\">" + structureOneCount + "</td>");

            #endregion

            #region 给排水

            //取得给排水
            decimal drainOnePercent = 0;
            //看是否有给排水人员参与
            var drainOneData = new DataView(dtSpe) { RowFilter = "Specialty='给排水'" }.ToTable();

            if (drainOneData != null && drainOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    drainOnePercent = decimal.Parse(drainOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal drainOneCount = 0;

            sb_one.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainOnePercent, 2).ToString() + " />%</td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                drainOneCount = Math.Round(drainOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
            }
            else //去整
            {
                drainOneCount = Math.Floor(drainOnePercent / 100 * allotCount);
            }

            sb_one.Append("<td width=\"7.5%\">" + drainOneCount + "</td>");

            #endregion

            #region 暖通

            //取得给排水
            decimal havcOnePercent = 0;
            //看是否有给排水人员参与
            var havcOneData = new DataView(dtSpe) { RowFilter = "Specialty='暖通'" }.ToTable();

            if (havcOneData != null && havcOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    havcOnePercent = decimal.Parse(havcOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal havcOneCount = 0;

            sb_one.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(havcOnePercent, 2).ToString() + " />%</td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                havcOneCount = Math.Round(havcOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
            }
            else //去整
            {
                havcOneCount = Math.Floor(havcOnePercent / 100 * allotCount);
            }

            sb_one.Append("<td width=\"7.5%\">" + havcOneCount + "</td>");

            #endregion
            #region 电气

            //取得电气
            decimal electricOnePercent = 0;
            //看是否有电气人员参与
            var electricOneData = new DataView(dtSpe) { RowFilter = "Specialty='电气'" }.ToTable();

            if (electricOneData != null && electricOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    electricOnePercent = decimal.Parse(electricOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal electricOneCount = 0;

            sb_one.Append("<td width=\"7.5%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricOnePercent, 2).ToString() + " />%</td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                electricOneCount = Math.Round(electricOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
            }
            else //去整
            {
                electricOneCount = Math.Floor(electricOnePercent / 100 * allotCount);
            }

            sb_one.Append("<td width=\"7.5%\">" + electricOneCount + "</td>");

            #endregion

            #region 合计
            decimal programCount = bulidingOneCount + structureOneCount + drainOneCount + electricOneCount + havcOneCount;

            decimal programPercent = bulidingOnePercent + structureOnePercent + drainOnePercent + electricOnePercent + havcOnePercent;

            sb_one.Append("<td width=\"7.5%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(programPercent, 0) + "</span>%</td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                sb_one.Append("<td width=\"7.5%\">" + Math.Round(programCount, MidpointRounding.AwayFromZero) + "</td>");
            }
            else //去整
            {
                sb_one.Append("<td width=\"7.5%\">" + Math.Floor(programCount) + "</td>");
            }

            sb_one.Append("</tr>");
            #endregion


            StringBuilder sb_ProcessOneTable = new StringBuilder();
            sb_ProcessOneTable.Append(" <div id=\"tbOutDoor\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> ");

            //sb_ProcessOneTable.Append("<table class=\"cls_Tbhead\" id=\"tbOutDoor\" style=\"display: none;\" width=\"100%\">");

            if (CoperationProcess == 4)
            {
                sb_ProcessOneTable.Append("<tr>  <td  align=\"center\" >    室外工程工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
            }
            else if (CoperationProcess == 5)
            {
                sb_ProcessOneTable.Append("<tr>  <td   align=\"center\" >   锅炉房工序产值分配比例%<span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span> <br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
            }
            else if (CoperationProcess == 6)
            {
                sb_ProcessOneTable.Append("<tr>  <td  align=\"center\">    地上单建水泵房工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
            }
            else if (CoperationProcess == 7)
            {
                sb_ProcessOneTable.Append("<tr>   <td   align=\"center\" >    地上单建变配所(室)工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
            }
            else if (CoperationProcess == 8)
            {
                sb_ProcessOneTable.Append("<tr>   <td   align=\"center\" >    单建地下室（车库）工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
            }
            else if (CoperationProcess == 9)
            {
                sb_ProcessOneTable.Append("<tr>   <td  align=\"center\">  市政道路工程工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span><br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
            }

            sb_ProcessOneTable.Append("</table>");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sb_ProcessOneTable.Append(" <tr> <td style=\"width: 10%; text-align: center\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
            sb_ProcessOneTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
            sb_ProcessOneTable.Append("</table>");

            sb_ProcessOneTable.Append("<table id=\"gvdesignProcessFive\" width=\"98%\"  class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\" >");

            //后期--后期为3
            var dtProcessValue = new DataView(dtProcess) { RowFilter = "ItemType=4" }.ToTable();

            //取得建筑工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("建筑", dtProcessValue, bulidingOneCount, CoperationProcess));

            //取得结构工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("结构", dtProcessValue, structureOneCount, CoperationProcess));

            //取得给排水工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("给排水", dtProcessValue, drainOneCount, CoperationProcess));

            //取得给排水工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("暖通", dtProcessValue, havcOneCount, CoperationProcess));

            //取得电气工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("电气", dtProcessValue, electricOneCount, CoperationProcess));

            sb_ProcessOneTable.Append("</table></div>");

            sbSpeHead.Append(sb_one.ToString());
            sbSpeHead.Append("</table></div>");
            lbl_StageFive.Text = sbSpeHead.ToString();
            lbl_out.Text = sb_ProcessOneTable.ToString();
        }


        /// <summary>
        /// 绑定分配后的产值
        /// </summary>
        private void bindHaveValuedData()
        {
            if (AuditStatus == "H" || AuditStatus == "J" || AuditStatus == "L" || AuditStatus == "N" || AuditStatus == "M" || AuditStatus == "O" || AuditStatus == "P" || AuditStatus == "Q" || AuditStatus == "K")
            {

                DataSet ds = BLL.GetValuedDataList((int)projectAuditRecordEntity.AllotID);

                gvStageOne.DataSource = ds.Tables[0];
                gvStageOne.DataBind();

                gvStageTwo.DataSource = ds.Tables[0];
                gvStageTwo.DataBind();

                gvStageThree.DataSource = ds.Tables[0];
                gvStageThree.DataBind();

                gvStageFour.DataSource = ds.Tables[0];
                gvStageFour.DataBind();

                gvStageTen.DataSource = ds.Tables[0];
                gvStageTen.DataBind();

                DataTable dtProcess = ds.Tables[2];

                //生成方案设计 初步设计 施工图 后期服务表单
                if (CoperationProcess < 4 || CoperationProcess == 10)
                {
                    CreateHavcSpeAmountTable(ds.Tables[1]);
                    CreateProcessTable(dtProcess);
                }
                else
                {
                    CreateHavcOtherAmountTable(ds.Tables[3], dtProcess);
                }
            }
        }
        /// 生成阶段专业产值表单--保存之后数据
        /// </summary>
        /// <param name="dtSpeAmount"></param>
        private void CreateHavcSpeAmountTable(DataTable dtSpeAmount)
        {

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();
            sbSpeHead.Append(" <div id=\"spetamountable\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  项目各设计阶段专业之间产值分配比例% </td> </tr> </table>");
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;height: 30px; \"> 设计阶段</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">暖通</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">合计</td>");
            sbSpeHead.Append(" </table>");

            sbSpeHead.Append("<table id=\"gvSpeAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

            //方案设计
            var dataProgram = new DataView(dtSpeAmount).ToTable("ProgramCount", false, "ProgramCount", "Specialty", "ProgramPercent");

            //第一行循环方案设计专业
            StringBuilder sb_one = new StringBuilder();
            sb_one.Append("<tr>");
            sb_one.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">方案设计</td>");

            //方案的金额
            for (int j = 0; j < dataProgram.Rows.Count; j++)
            {
                sb_one.Append("<td width=\"7.5%\">" + dataProgram.Rows[j]["ProgramPercent"].ToString() + "%</td>");
                sb_one.Append("<td width=\"7.5%\">" + dataProgram.Rows[j]["ProgramCount"].ToString() + "</td>");
            }
            sb_one.Append("<td width=\"7.5%\">100%</td>");
            //取得总和
            var sumProgramCount = dataProgram.AsEnumerable().Sum(s => s.Field<decimal>("ProgramCount"));
            sb_one.Append("<td width=\"7.5%\">" + sumProgramCount + "</td>");
            sb_one.Append("</tr>");

            //初步设计
            var dataPreliminary = new DataView(dtSpeAmount).ToTable("preliminaryCount", false, "preliminaryCount", "Specialty", "preliminaryPercent");

            //第一行循环方案设计专业
            StringBuilder sb_two = new StringBuilder();
            sb_two.Append("<tr>");
            sb_two.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">初步设计</td>");


            for (int j = 0; j < dataPreliminary.Rows.Count; j++)
            {
                sb_two.Append("<td width=\"7.5%\">" + dataPreliminary.Rows[j]["preliminaryPercent"].ToString() + "%</td>");
                sb_two.Append("<td  width=\"7.5%\">" + dataPreliminary.Rows[j]["preliminaryCount"].ToString() + "</td>");
            }
            sb_two.Append("<td width=\"7.5%\">100%</td>");
            //取得总和
            var sumPreliminaryCount = dataPreliminary.AsEnumerable().Sum(s => s.Field<decimal>("preliminaryCount"));
            sb_two.Append("<td width=\"7.5%\">" + sumPreliminaryCount + "</td>");

            sb_two.Append("</tr>");

            //施工图设计
            var dataWorkDraw = new DataView(dtSpeAmount).ToTable("WorkDrawCount", false, "WorkDrawCount", "Specialty", "WorkDrawPercent");

            //第一行循环方案设计专业
            StringBuilder sb_three = new StringBuilder();
            sb_three.Append("<tr>");
            sb_three.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">施工图设计</td>");

            for (int j = 0; j < dataWorkDraw.Rows.Count; j++)
            {
                sb_three.Append("<td width=\"7.5%\">" + dataWorkDraw.Rows[j]["WorkDrawPercent"].ToString() + "%</td>");
                sb_three.Append("<td width=\"7.5%\">" + dataWorkDraw.Rows[j]["WorkDrawCount"].ToString() + "</td>");
            }
            sb_three.Append("<td width=\"7.5%\">100%</td>");
            //取得总和
            var sumWorkDrawCount = dataWorkDraw.AsEnumerable().Sum(s => s.Field<decimal>("WorkDrawCount"));
            sb_three.Append("<td width=\"7.5%\">" + sumWorkDrawCount + "</td>");
            sb_three.Append("</tr>");

            //后期服务
            var dataLateStage = new DataView(dtSpeAmount).ToTable("LateStageCount", false, "LateStageCount", "Specialty", "LateStagePercent");

            //第一行循环方案设计专业
            StringBuilder sb_four = new StringBuilder();
            sb_four.Append("<tr>");
            sb_four.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">后期服务</td>");

            for (int j = 0; j < dataLateStage.Rows.Count; j++)
            {
                sb_four.Append("<td width=\"7.5%\">" + dataLateStage.Rows[j]["LateStagePercent"].ToString() + "%</td>");
                sb_four.Append("<td width=\"7.5%\">" + dataLateStage.Rows[j]["LateStageCount"].ToString() + "</td>");
            }
            sb_four.Append("<td width=\"7.5%\">100%</td>");
            //取得总和
            var sumLateStageCount = dataLateStage.AsEnumerable().Sum(s => s.Field<decimal>("LateStageCount"));
            sb_four.Append("<td width=\"7.5%\">" + sumLateStageCount + "</td>");
            sb_four.Append("</tr>");


            if (CoperationProcess == 0)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 1)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
            }

            else if (CoperationProcess == 2)
            {
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 3)
            {
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 10)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            sbSpeHead.Append(" </table></div> ");
            lbl_SpeAmount.Text = sbSpeHead.ToString();


        }

        /// <summary>
        /// 生成表四之后的表单--保存之后数据
        /// </summary>
        /// <param name="dtOutAmount"></param>
        /// <param name="dtProcess"></param>
        private void CreateHavcOtherAmountTable(DataTable dtOutAmount, DataTable dtProcess)
        {

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();

            sbSpeHead.Append(" <div id=\"outdooramounttable\" class=\"cls_Container_Report\" style=\"display: none;\">");

            if (CoperationProcess == 4)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  室外工程产值分配比例% </td> </tr> </table>");
            }
            else if (CoperationProcess == 5)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  锅炉房产值分配比例% </td> </tr> </table>");
            }
            else if (CoperationProcess == 6)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建水泵房产值分配比例% </td> </tr> </table>");

            }
            else if (CoperationProcess == 7)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建变配所(室)产值分配比例% </td> </tr> </table>");

            }
            else if (CoperationProcess == 8)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  单建地下室（车库）产值分配比例% </td> </tr> </table>");

            }
            else if (CoperationProcess == 9)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  市政道路工程产值分配比例% </td> </tr> </table>");

            }
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 专业</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">暖通</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:15%;text-align: center;\">合计</td>");

            sbSpeHead.Append(" </tr></table>");

            sbSpeHead.Append("<table id=\"gvOutDoorAmount\" width=\"98%\"  class=\"cls_ProjAllot_Table cls_ProjAllot_label2\"  >");
            if (dtProcess.Rows.Count > 0)
            {
                sbSpeHead.Append("<tr><td width=\"10%\" class=\"cls_Column\">比例</td>");
                //建筑
                var bulidingData = new DataView(dtOutAmount) { RowFilter = "Specialty='建筑'" }.ToTable();
                if (bulidingData != null && bulidingData.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td width=\"7.5%\">" + bulidingData.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td width=\"7.5%\">" + bulidingData.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"7.5%\">0</td>");
                    sbSpeHead.Append("<td width=\"7.5%\">0</td>");
                }

                //结构
                var structureCount = new DataView(dtOutAmount) { RowFilter = "Specialty='结构'" }.ToTable();
                if (structureCount != null && structureCount.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td  width=\"7.5%\">" + structureCount.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td  width=\"7.5%\">" + structureCount.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"7.5%\">0</td>");
                    sbSpeHead.Append("<td width=\"7.5%\">0</td>");
                }

                //给排水
                var drainCount = new DataView(dtOutAmount) { RowFilter = "Specialty='给排水'" }.ToTable();
                if (drainCount != null && drainCount.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td  width=\"7.5%\">" + drainCount.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td  width=\"7.5%\">" + drainCount.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"7.5%\">0</td>");
                    sbSpeHead.Append("<td width=\"7.5%\">0</td>");
                }
                //暖通
                var havcCount = new DataView(dtOutAmount) { RowFilter = "Specialty='暖通'" }.ToTable();
                if (havcCount != null && havcCount.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td  width=\"7.5%\">" + havcCount.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td  width=\"7.5%\">" + havcCount.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"7.5%\">0</td>");
                    sbSpeHead.Append("<td width=\"7.5%\">0</td>");
                }
                //电气
                var electricCount = new DataView(dtOutAmount) { RowFilter = "Specialty='电气'" }.ToTable();
                if (electricCount != null && electricCount.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td  width=\"7.5%\">" + electricCount.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td  width=\"7.5%\">" + electricCount.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"7.5%\">0</td>");
                    sbSpeHead.Append("<td width=\"7.5%\">0</td>");
                }

                var sumTotal = dtOutAmount.AsEnumerable().Sum(s => s.Field<decimal>("SpecialtyCount"));
                //合计
                sbSpeHead.Append("<td  width=\"7.5%\">100%</td>");
                sbSpeHead.Append("<td  width=\"7.5%\">" + sumTotal + "</td>");
                sbSpeHead.Append("</tr>");
            }
            else
            {
                sbSpeHead.Append("<tr><td colspan=\"11\">没有数据</td></tr>");
            }

            sbSpeHead.Append(" </table></div>");

            lbl_outAmount.Text = sbSpeHead.ToString();

            lbl_processAmountFiveTable.Text = getProcessDeatil(dtProcess, 4);
        }

        #endregion

        /// <summary>
        /// 设置状态文本
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        private string setProcessText(string process)
        {
            string processText = "";

            if (process == "0")
            {
                processText = "方案+初设+施工图+后期";
            }
            else if (process == "1")
            {
                processText = "方案+初设";
            }
            else if (process == "2")
            {
                processText = "施工图+后期";
            }
            else if (process == "3")
            {
                processText = "初设+施工图+后期";
            }
            else if (process == "4")
            {
                processText = "室外工程";
            }
            else if (process == "5")
            {
                processText = "锅炉房";
            }
            else if (process == "6")
            {
                processText = "地上单建水泵房";
            }
            else if (process == "7")
            {
                processText = "地上单建变配电所(室)";
            }
            else if (process == "8")
            {
                processText = "单建地下室（车库）";
            }
            else if (process == "10")
            {
                processText = "方案+施工图+后期";
            }
            return processText;
        }
        /// <summary>
        /// 取得最后一个专业负责审核人
        /// </summary>
        /// <param name="auditUser"></param>
        /// <returns></returns>
        public int getLastAudit(string auditUser)
        {
            int lastAuditUser = 0;
            string[] tempUser = null;
            // 取得专业负责人
            if (!string.IsNullOrEmpty(auditUser))
            {
                string[] tempArr = auditUser.Split(',');
                if (tempArr != null && tempArr.Length > 0)
                {
                    string temp;
                    foreach (var item in tempArr)
                    {
                        if (item.Contains('{'))
                        {
                            temp = item.Replace("{", "");
                            if (item.Contains('}'))
                            {
                                temp = temp.Replace("}", "");
                            }
                            tempUser = temp.Split(';');
                            break;
                        }
                    }
                }
            }

            if (tempUser != null)
            {
                int length = tempUser.Length;
                // 取得最后一个审核人
                lastAuditUser = int.Parse(tempUser[length - 1].ToString());
            }

            return lastAuditUser;
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_click(object sender, EventArgs e)
        {

            TG.Model.cm_ProjectValueAuditRecord record = BLL.GetModel(ValueAllotAuditSysNo);
            DataSet dt = new DataSet();
            DataTable dtSecondData = new DataTable();
            if (record != null)
            {
                //显示项目信息
                TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
                TG.Model.cm_Project pro_model = pro.GetModel(record.ProSysNo);

                dtSecondData = BLL.GetSecondAuditDesignAmount(record.ProSysNo, (int)record.AllotID).Tables[0];
                if (pro_model != null)
                {
                    dt = BLL.GetProjectValueByMemberTotalAcount((int)record.AllotID, pro_model.Unit);
                }

            }

            string modelPath = " ~/TemplateXls/1.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);


            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);


            ws.GetRow(0).GetCell(0).SetCellValue(lblcpr_Unit.Text + year + "年产值分配单");
            ws.GetRow(1).GetCell(1).SetCellValue(lblProjectName.Text);
            ws.GetRow(12).GetCell(3).SetCellValue(txt_ActulCount.Text);

            CellRangeAddress range = new CellRangeAddress(0, 0, 1, 6);
            ws.AddMergedRegion(range);

            ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);


            int row = 16;
            //实分产值
            decimal payShiAllotCount = 0;
            var theUnitMember = dt.Tables[0];
            #region 本所人员
            if (theUnitMember.Rows.Count > 0)
            {
                var dataRowTop = ws.GetRow(row);//生成行
                if (dataRowTop == null)
                    dataRowTop = ws.CreateRow(row);
                var cellTop = dataRowTop.GetCell(0);
                if (cellTop == null)
                    cellTop = dataRowTop.CreateCell(0);
                cellTop.SetCellValue("本所人员产值分配明细");
                cellTop.CellStyle = style2;

                range = new CellRangeAddress(row, row, 0, 8);
                ws.AddMergedRegion(range);
                ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

                //取得专业
                var dtSpecialtyList = new DataView(theUnitMember).ToTable("Specialty", true, "spe_Name");
                row = row + 1;
                for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
                {
                    // 查找该专业下面的所有人员
                    var dv = new DataView(theUnitMember) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                    var dtMemberBySpecialty = dv.ToTable();

                    int count = 0;
                    if (dtMemberBySpecialty.Rows.Count % 2 == 0)
                    {
                        count = dtMemberBySpecialty.Rows.Count / 2;
                    }
                    else
                    {
                        count = dtMemberBySpecialty.Rows.Count / 2 + 1;
                    }

                    var dataRow = ws.GetRow(row);//生成行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);


                    range = new CellRangeAddress(row, (row + count - 1), 0, 0);
                    ws.AddMergedRegion(range);
                    ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtSpecialtyList.Rows[i][0].ToString());

                    int row1 = 0;
                    for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j = j + 2)
                    {

                        var dataRow1 = ws.GetRow(row + row1);//生成行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row + row1);
                        var cell1 = dataRow1.CreateCell(1);
                        cell1.SetCellValue(dtMemberBySpecialty.Rows[j]["mem_Name"].ToString());
                        cell1.CellStyle = style2;

                        cell1 = dataRow1.CreateCell(2);
                        cell1.SetCellValue("");
                        cell1.CellStyle = style2;

                        cell1 = dataRow1.CreateCell(3);
                        cell1.SetCellValue(dtMemberBySpecialty.Rows[j]["totalCount"].ToString());
                        cell1.CellStyle = style2;

                        cell1 = dataRow1.CreateCell(4);
                        cell1.SetCellValue("");
                        cell1.CellStyle = style2;

                        if (j + 1 >= dtMemberBySpecialty.Rows.Count)
                        {
                            cell1 = dataRow1.CreateCell(5);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(6);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(7);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(8);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;

                        }
                        else
                        {
                            cell1 = dataRow1.CreateCell(5);
                            cell1.SetCellValue(dtMemberBySpecialty.Rows[j + 1]["mem_Name"].ToString());
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(6);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(7);
                            cell1.SetCellValue(dtMemberBySpecialty.Rows[j + 1]["totalCount"].ToString());
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(8);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                        }

                        row1 = row1 + 1;
                    }


                    row = row + count;
                }
                payShiAllotCount = theUnitMember.AsEnumerable().Sum(s => s.Field<int>("totalCount"));
                var dataRowBottom = ws.GetRow(row);//生成行
                if (dataRowBottom == null)
                    dataRowBottom = ws.CreateRow(row);
                var cellBottom = dataRowBottom.GetCell(0);
                if (cellBottom == null)
                    cellBottom = dataRowBottom.CreateCell(0);
                cellBottom.SetCellValue("本所合计");
                cellBottom.CellStyle = style2;
                cellBottom = dataRowBottom.CreateCell(1);
                cellBottom.SetCellValue("本所分配产值");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(2);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(3);
                cellBottom.SetCellValue(payShiAllotCount.ToString());
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(4);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(5);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(6);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(7);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(8);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;
                row = row + 1;
            }
            #endregion

            var otherUnitMember = dt.Tables[1];
            decimal payOtherAllotCount = 0;
            #region 外所人员
            if (otherUnitMember != null && otherUnitMember.Rows.Count > 0)
            {
                var dataRowTop = ws.GetRow(row);//生成行
                if (dataRowTop == null)
                    dataRowTop = ws.CreateRow(row);
                var cellTop = dataRowTop.GetCell(0);
                if (cellTop == null)
                    cellTop = dataRowTop.CreateCell(0);
                cellTop.SetCellValue("外所人员产值分配明细");
                cellTop.CellStyle = style2;
                range = new CellRangeAddress(row, row, 0, 8);
                ws.AddMergedRegion(range);
                ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

                //取得专业
                var dtSpecialtyList = new DataView(otherUnitMember).ToTable("Specialty", true, "spe_Name");
                row = row + 1;
                for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
                {
                    // 查找该专业下面的所有人员
                    var dv = new DataView(otherUnitMember) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                    var dtMemberBySpecialty = dv.ToTable();

                    int count = 0;
                    if (dtMemberBySpecialty.Rows.Count % 2 == 0)
                    {
                        count = dtMemberBySpecialty.Rows.Count / 2;
                    }
                    else
                    {
                        count = dtMemberBySpecialty.Rows.Count / 2 + 1;
                    }

                    var dataRow = ws.GetRow(row);//生成行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);


                    range = new CellRangeAddress(row, (row + count - 1), 0, 0);
                    ws.AddMergedRegion(range);
                    ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtSpecialtyList.Rows[i][0].ToString());

                    int row1 = 0;
                    for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j = j + 2)
                    {

                        var dataRow1 = ws.GetRow(row + row1);//生成行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row + row1);
                        var cell1 = dataRow1.CreateCell(1);
                        cell1.SetCellValue(dtMemberBySpecialty.Rows[j]["mem_Name"].ToString());
                        cell1.CellStyle = style2;

                        cell1 = dataRow1.CreateCell(2);
                        cell1.SetCellValue("");
                        cell1.CellStyle = style2;

                        cell1 = dataRow1.CreateCell(3);
                        cell1.SetCellValue(dtMemberBySpecialty.Rows[j]["totalCount"].ToString());
                        cell1.CellStyle = style2;

                        cell1 = dataRow1.CreateCell(4);
                        cell1.SetCellValue("");
                        cell1.CellStyle = style2;

                        if (j + 1 >= dtMemberBySpecialty.Rows.Count)
                        {
                            cell1 = dataRow1.CreateCell(5);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(6);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(7);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(8);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;

                        }
                        else
                        {
                            cell1 = dataRow1.CreateCell(5);
                            cell1.SetCellValue(dtMemberBySpecialty.Rows[j + 1]["mem_Name"].ToString());
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(6);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(7);
                            cell1.SetCellValue(dtMemberBySpecialty.Rows[j + 1]["totalCount"].ToString());
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(8);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                        }

                        row1 = row1 + 1;
                    }


                    row = row + count;
                }

                payOtherAllotCount = otherUnitMember.AsEnumerable().Sum(s => s.Field<int>("totalCount"));
                var dataRowBottom = ws.GetRow(row);//生成行
                if (dataRowBottom == null)
                    dataRowBottom = ws.CreateRow(row);
                var cellBottom = dataRowBottom.GetCell(0);
                if (cellBottom == null)
                    cellBottom = dataRowBottom.CreateCell(0);
                cellBottom.SetCellValue("外所合计");
                cellBottom.CellStyle = style2;
                cellBottom = dataRowBottom.CreateCell(1);
                cellBottom.SetCellValue("外所分配产值");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(2);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(3);
                cellBottom.SetCellValue(payOtherAllotCount.ToString());
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(4);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(5);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(6);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(7);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(8);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;
                row = row + 1;

            }

            #endregion

            #region 外聘人员
            var wpUnitMember = dt.Tables[2];
            decimal wpAllotCount = 0;
            if (wpUnitMember != null && wpUnitMember.Rows.Count > 0)
            {
                var dataRowTop = ws.GetRow(row);//生成行
                if (dataRowTop == null)
                    dataRowTop = ws.CreateRow(row);
                var cellTop = dataRowTop.GetCell(0);
                if (cellTop == null)
                    cellTop = dataRowTop.CreateCell(0);
                cellTop.SetCellValue("外聘人员产值分配明细");
                cellTop.CellStyle = style2;
                range = new CellRangeAddress(row, row, 0, 8);
                ws.AddMergedRegion(range);
                ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

                //取得专业
                var dtSpecialtyList = new DataView(wpUnitMember).ToTable("Specialty", true, "spe_Name");
                row = row + 1;
                for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
                {
                    // 查找该专业下面的所有人员
                    var dv = new DataView(wpUnitMember) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                    var dtMemberBySpecialty = dv.ToTable();

                    int count = 0;
                    if (dtMemberBySpecialty.Rows.Count % 2 == 0)
                    {
                        count = dtMemberBySpecialty.Rows.Count / 2;
                    }
                    else
                    {
                        count = dtMemberBySpecialty.Rows.Count / 2 + 1;
                    }

                    var dataRow = ws.GetRow(row);//生成行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);


                    range = new CellRangeAddress(row, (row + count - 1), 0, 0);
                    ws.AddMergedRegion(range);
                    ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtSpecialtyList.Rows[i][0].ToString());

                    int row1 = 0;
                    for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j = j + 2)
                    {

                        var dataRow1 = ws.GetRow(row + row1);//生成行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row + row1);
                        var cell1 = dataRow1.CreateCell(1);
                        cell1.SetCellValue(dtMemberBySpecialty.Rows[j]["mem_Name"].ToString());
                        cell1.CellStyle = style2;

                        cell1 = dataRow1.CreateCell(2);
                        cell1.SetCellValue("");
                        cell1.CellStyle = style2;

                        cell1 = dataRow1.CreateCell(3);
                        cell1.SetCellValue(dtMemberBySpecialty.Rows[j]["totalCount"].ToString());
                        cell1.CellStyle = style2;

                        cell1 = dataRow1.CreateCell(4);
                        cell1.SetCellValue("");
                        cell1.CellStyle = style2;

                        if (j + 1 >= dtMemberBySpecialty.Rows.Count)
                        {
                            cell1 = dataRow1.CreateCell(5);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(6);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(7);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(8);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;

                        }
                        else
                        {
                            cell1 = dataRow1.CreateCell(5);
                            cell1.SetCellValue(dtMemberBySpecialty.Rows[j + 1]["mem_Name"].ToString());
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(6);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(7);
                            cell1.SetCellValue(dtMemberBySpecialty.Rows[j + 1]["totalCount"].ToString());
                            cell1.CellStyle = style2;
                            cell1 = dataRow1.CreateCell(8);
                            cell1.SetCellValue("");
                            cell1.CellStyle = style2;
                        }

                        row1 = row1 + 1;
                    }


                    row = row + count;
                }

                wpAllotCount = wpUnitMember.AsEnumerable().Sum(s => s.Field<int>("totalCount"));
                var dataRowBottom = ws.GetRow(row);//生成行
                if (dataRowBottom == null)
                    dataRowBottom = ws.CreateRow(row);
                var cellBottom = dataRowBottom.GetCell(0);
                if (cellBottom == null)
                    cellBottom = dataRowBottom.CreateCell(0);
                cellBottom.SetCellValue("本所合计");
                cellBottom.CellStyle = style2;
                cellBottom = dataRowBottom.CreateCell(1);
                cellBottom.SetCellValue("本所分配产值");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(2);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(3);
                cellBottom.SetCellValue(wpAllotCount.ToString());
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(4);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(5);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(6);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(7);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;

                cellBottom = dataRowBottom.CreateCell(8);
                cellBottom.SetCellValue("");
                cellBottom.CellStyle = style2;
                row = row + 1;
            }

            #endregion

            //差额
            decimal differenceAllotCount = 0;
            if (!string.IsNullOrEmpty(txt_ActulCount.Text))
            {
                differenceAllotCount = decimal.Parse(txt_ActulCount.Text) - payShiAllotCount - payOtherAllotCount - wpAllotCount;
            }

            decimal totalAllotCount = payShiAllotCount + payOtherAllotCount + wpAllotCount;

            var dataRowTotal = ws.GetRow(row);//生成行
            if (dataRowTotal == null)
                dataRowTotal = ws.CreateRow(row);
            var cellTotal = dataRowTotal.GetCell(0);
            if (cellTotal == null)
                cellTotal = dataRowTotal.CreateCell(0);
            cellTotal.SetCellValue("总体合计");
            cellTotal.CellStyle = style2;
            cellTotal = dataRowTotal.CreateCell(1);
            cellTotal.SetCellValue("实分产值");
            cellTotal.CellStyle = style2;

            cellTotal = dataRowTotal.CreateCell(2);
            cellTotal.SetCellValue("");
            cellTotal.CellStyle = style2;

            cellTotal = dataRowTotal.CreateCell(3);
            cellTotal.SetCellValue(totalAllotCount.ToString());
            cellTotal.CellStyle = style2;

            cellTotal = dataRowTotal.CreateCell(4);
            cellTotal.SetCellValue("");
            cellTotal.CellStyle = style2;

            cellTotal = dataRowTotal.CreateCell(5);
            cellTotal.SetCellValue("");
            cellTotal.CellStyle = style2;

            cellTotal = dataRowTotal.CreateCell(6);
            cellTotal.SetCellValue("");
            cellTotal.CellStyle = style2;

            cellTotal = dataRowTotal.CreateCell(7);
            cellTotal.SetCellValue("");
            cellTotal.CellStyle = style2;

            cellTotal = dataRowTotal.CreateCell(8);
            cellTotal.SetCellValue("");
            cellTotal.CellStyle = style2;
            row = row + 1;


            //第二分配金额
            if (dtSecondData != null && dtSecondData.Rows.Count > 0)
            {
                var dataRowTotal1 = ws.GetRow(row);//生成行
                if (dataRowTotal1 == null)
                    dataRowTotal1 = ws.CreateRow(row);
                var cellTotal1 = dataRowTotal1.GetCell(0);
                if (cellTotal1 == null)
                    cellTotal1 = dataRowTotal1.CreateCell(0);
                cellTotal1.SetCellValue("剩余二次分配金额");
                cellTotal1.CellStyle = style2;

                cellTotal1 = dataRowTotal1.CreateCell(1);
                cellTotal1.SetCellValue("建筑结构审核金额");
                cellTotal1.CellStyle = style2;

                cellTotal1 = dataRowTotal1.CreateCell(2);
                cellTotal1.SetCellValue("");
                cellTotal1.CellStyle = style2;

                cellTotal1 = dataRowTotal1.CreateCell(3);
                cellTotal1.SetCellValue(dtSecondData.Rows[0]["AuditCount"].ToString() + "元");
                cellTotal1.CellStyle = style2;

                cellTotal1 = dataRowTotal1.CreateCell(4);
                cellTotal1.SetCellValue("");
                cellTotal1.CellStyle = style2;

                cellTotal1 = dataRowTotal1.CreateCell(5);
                cellTotal1.SetCellValue("所领导班子所剩金额");
                cellTotal1.CellStyle = style2;

                cellTotal1 = dataRowTotal1.CreateCell(6);
                cellTotal1.SetCellValue("");
                cellTotal1.CellStyle = style2;

                cellTotal1 = dataRowTotal1.CreateCell(7);
                cellTotal1.SetCellValue(dtSecondData.Rows[0]["DesignCount"].ToString());
                cellTotal1.CellStyle = style2;

                cellTotal1 = dataRowTotal1.CreateCell(8);
                cellTotal1.SetCellValue("");
                cellTotal1.CellStyle = style2;
                row = row + 1;

            }
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.Write(memoryStream);
                string name = System.Web.HttpContext.Current.Server.UrlEncode(string.Format("{0}{1}年二次产值分配单{2}.xls", lblcpr_Unit.Text, DateTime.Now.Year, DateTime.Now.ToString("yyyyMMddHHmmss")));
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.Charset = "UTF-8";
                Response.ContentType = "application/ms-excel";
                Response.BinaryWrite(memoryStream.ToArray());
                wb = null;
                Response.End();
            }

        }
    }
}