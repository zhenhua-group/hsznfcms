﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjectValueGeneralStatisticsBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindYear();
                SelectCurrentYear();
                bindText();
                bindDataDetail();
            }
        }

        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }

        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
        private void bindText()
        {
            if (drp_unit.SelectedValue != "-1")
            {
                this.lblUnit.Text = drp_unit.SelectedItem.Text.Trim();
            }
            else
            {
                this.lblUnit.Text = "生产部门";
            }
            if (drp_year.SelectedValue != "-1")
            {
                this.lblYear.Text = drp_year.SelectedItem.Text.Trim() + "年";
            }
            else
            {
                this.lblYear.Text = "全部年份";
            }
        }

        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND a.unit_Name= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND a.unit_Name= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            bindText();
            bindDataDetail();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();

            string modelPath = " ~/TemplateXls/ProjectValueComprehensiveReport.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            NPOI.SS.UserModel.IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);


            //设置样式
            ICellStyle styleTotal = wb.CreateCellStyle();
            //设置字体
            IFont fontTotal = wb.CreateFont();
            fontTotal.FontHeightInPoints = 9;//字号
            fontTotal.FontName = "宋体";
            //字体加粗
            fontTotal.Boldweight = (short)2000;
            styleTotal.SetFont(fontTotal);

            //设置边框
            styleTotal.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            styleTotal.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleTotal.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleTotal.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            ws.GetRow(0).GetCell(0).SetCellValue(lblYear.Text.Trim() + lblUnit.Text.Trim() + "酬金工资统计表");
            CellRangeAddress range = new CellRangeAddress(0, 0, 0, 7);
            ws.AddMergedRegion(range);
            ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

            //取得专业
            var dtUnit = new DataView(dt).ToTable("unit_Name", true, "unit_Name");

            int row = 2;

            for (int i = 0; i < dt.Rows.Count; i++)
            {


                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);

                cell.CellStyle = style2;
                cell.SetCellValue((i + 1));

                cell = dataRow.GetCell(1);
                if (cell == null)
                    cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dtUnit.Rows[i]["unit_Name"].ToString());


                cell = dataRow.CreateCell(2);
                decimal allotCount = decimal.Parse(dt.Rows[i]["AllotCount"].ToString()) * 10000;
                WriteExcelValue(cell, decimal.Parse(allotCount.ToString("f2")));
                cell.CellStyle = style2;

                //发展基金
                decimal fundpercent = dt.Rows[i]["fund_percent"].ToString() == "0.00" ? decimal.Parse("0.03") : decimal.Parse(dt.Rows[i]["fund_percent"].ToString())/100;
                cell = dataRow.CreateCell(3);
                cell.SetCellFormula("C" + (row + 1) + "*" + fundpercent + "");
                cell.CellStyle = style2;

                //固定成本             
                cell = dataRow.CreateCell(4);
                decimal costCharge = decimal.Parse(dt.Rows[i]["costCharge"].ToString());
                WriteExcelValue(cell, decimal.Parse(costCharge.ToString("f2")));
                cell.CellStyle = style2;

                //年度工资
                cell = dataRow.CreateCell(5);
                cell.SetCellFormula("C" + (row + 1) + "-D" + (row + 1) + "-E" + (row + 1) + "");
                cell.CellStyle = style2;

                //技术酬金
                decimal remunerationPercent = decimal.Parse(dt.Rows[i]["Remuneration_percent"].ToString());
                cell = dataRow.CreateCell(6);
                cell.SetCellFormula("F" + (row + 1) + "*" + remunerationPercent + "");
                cell.CellStyle = style2;

                cell = dataRow.CreateCell(7);
                cell.SetCellFormula("F" + (row + 1) + "-G" + (row + 1) + "");
                cell.CellStyle = style2;

                row = row + 1;
            }

            //总合计
            var dataTotal = ws.GetRow(row);//生成行
            if (dataTotal == null)
                dataTotal = ws.CreateRow(row);
            var total = dataTotal.GetCell(0);
            if (total == null)
                total = dataTotal.CreateCell(0);
            total.SetCellValue("");
            total.CellStyle = styleTotal;

            total = dataTotal.GetCell(1);
            if (total == null)
                total = dataTotal.CreateCell(1);
            total.SetCellValue("总计");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(2);
            total.SetCellFormula("SUM(C3:C" + (row) + ")");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(3);
            total.SetCellFormula("SUM(D3:D" + (row) + ")");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(4);
            total.SetCellFormula("SUM(E3:E" + (row) + ")");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(5);
            total.SetCellFormula("SUM(F3:F" + (row) + ")");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(6);
            total.SetCellFormula("SUM(G3:G" + (row) + ")");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(7);
            total.SetCellFormula("SUM(H3:H" + (row) + ")");
            total.CellStyle = styleTotal;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(lblYear.Text.Trim() + lblUnit.Text.Trim() + "酬金工资统计表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }

        /// <summary>
        /// 得到数据
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {
            StringBuilder sb = new StringBuilder();
            //绑定单位
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND a.unit_ID='" + this.drp_unit.SelectedValue.Trim() + "' ");
            }

            //按照年份
            string year = null;
            if (this.drp_year.SelectedIndex != 0)
            {
                //sb.Append(" AND  b.DivideYear='" + this.drp_year.SelectedItem.Text.Trim() + "' ");
                year = this.drp_year.SelectedValue;
            }
            //检查权限
            GetPreviewPowerSql(ref sb);
            TG.BLL.cm_FundPercentSet bll = new TG.BLL.cm_FundPercentSet();
            DataTable dt = bll.GetUnitRevenueCostBalance(sb.ToString(), year).Tables[0];
            return dt;
        }

        /// <summary>
        /// 明细
        /// </summary>
        private void bindDataDetail()
        {

            DataTable dt = getData();

            string year = null;
            if (this.drp_year.SelectedIndex != 0)
            {
                year = this.drp_year.SelectedValue;
            }

            StringBuilder sbHtml = new StringBuilder();
            if (dt != null && dt.Rows.Count > 0)
            {
                decimal totalAllotCount = 0;//收入
                decimal totaFundpercent = 0;//发展基金
                decimal totalcostCharge = 0;//成本
                decimal totalYearCount = 0;//年度工资
                decimal totalRemunerationCount = 0;//技术酬金
                decimal totalXiaoyiCount = 0;//效益

                int rowNum = 1;
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    sbHtml.Append("<tr>");

                    sbHtml.Append("<td>" + rowNum + "</td>");
                    sbHtml.Append("<td>" + dt.Rows[i]["unit_Name"] + "</td>");
                    decimal allotCount = decimal.Parse(dt.Rows[i]["AllotCount"].ToString()) * 10000;//万元
                    sbHtml.Append("<td>" + allotCount.ToString("f2") + "</td>");
                    //发展基金
                    //本所收入 =所留*0.97
                    decimal fundpercent = dt.Rows[i]["fund_percent"].ToString() == "0.00" ? decimal.Parse("0.03") : decimal.Parse(dt.Rows[i]["fund_percent"].ToString()) / 100;
                    decimal fundCount = allotCount * fundpercent;
                    sbHtml.Append("<td>" + fundCount.ToString("f2") + "</td>");
                    //固定成本
                    decimal costCharge = decimal.Parse(dt.Rows[i]["costCharge"].ToString());
                    sbHtml.Append("<td>" + costCharge.ToString("f2") + "</td>");

                    //年度工资
                    decimal yearCount = allotCount - fundCount - costCharge;
                    sbHtml.Append("<td>" + yearCount.ToString("f2") + "</td>");

                    //技术酬金系数
                    decimal remunerationPercent = decimal.Parse(dt.Rows[i]["Remuneration_percent"].ToString());
                    decimal remunerationCount = yearCount * remunerationPercent;
                    sbHtml.Append("<td>" + remunerationCount.ToString("f2") + "</td>");

                    //效益工资
                    decimal xiaoyiCount = yearCount - remunerationCount;
                    sbHtml.Append("<td>" + xiaoyiCount.ToString("f2") + "</td>");

                    rowNum = rowNum + 1;

                    totalAllotCount = totalAllotCount + allotCount;//收入
                    totaFundpercent = totaFundpercent + fundCount;//发展基金
                    totalcostCharge = totalcostCharge + costCharge;//成本
                    totalYearCount = totalYearCount + yearCount;//年度工资
                    totalRemunerationCount = totalRemunerationCount + remunerationCount;//技术酬金
                    totalXiaoyiCount = totalXiaoyiCount + xiaoyiCount;//效益
                }
                sbHtml.Append("<tr>");
                sbHtml.Append("<td></td>");
                sbHtml.Append("<td class=\"fontBold\">总合计</td>");
                sbHtml.Append("<td class=\"fontBold\">" + totalAllotCount.ToString("f2") + "</td>");
                sbHtml.Append("<td class=\"fontBold\">" + totaFundpercent.ToString("f2") + "</td>");
                sbHtml.Append("<td class=\"fontBold\">" + totalcostCharge.ToString("f2") + "</td>");
                sbHtml.Append("<td class=\"fontBold\">" + totalYearCount.ToString("f2") + "</td>");
                sbHtml.Append("<td class=\"fontBold\">" + totalRemunerationCount.ToString("f2") + "</td>");
                sbHtml.Append("<td class=\"fontBold\">" + totalXiaoyiCount.ToString("f2") + "</td>");
                sbHtml.Append("</tr>");
                lblHtml.Text = sbHtml.ToString();
            }
            else
            {
                sbHtml.Append("<tr><td colspan=8 style=\"color:red;\">没有数据</td><tr>");
                lblHtml.Text = sbHtml.ToString();
            }
        }

        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}