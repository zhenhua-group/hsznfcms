﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class HavcProjectValueAllotDetailsBymaster :PageBase
    {
        private readonly TG.BLL.cm_ProjectValueAllot bll = new TG.BLL.cm_ProjectValueAllot();

        #region QueryString

        /// <summary>
        /// 合同系统号
        /// </summary>
        public int ProSysNo
        {
            get
            {
                int CoperationSysNo = 0;
                int.TryParse(Request["proid"], out CoperationSysNo);
                return CoperationSysNo;
            }
        }
        public string year
        {
            get
            {
                return Request["year"];
            }
        }

        //是否生产经营部
        public string isFlag;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定合同名称
                bindCprName();

                // 绑定列表数据
                bindGridData();

                List<TG.Model.cm_Role> roleList = new TG.BLL.cm_Role().GetRoleList(UserSysNo);
                isFlag = "false";
                if ((from role in roleList where role.RoleName == "生产经营部" select role).Count() > 0)
                {
                    isFlag = "true";
                }
            }
        }
        //绑定列表信息
        private void bindGridData()
        {
            string tempYear = year == "-1" ? "null" : year;
            gv_ProjectValue.DataSource = bll.GetHavcProjectValueRecordView(ProSysNo.ToString(), "null");
            gv_ProjectValue.DataBind();
        }

        /// <summary>
        /// 取得合同名称 
        /// </summary>
        private void bindCprName()
        {
            TG.BLL.cm_Project bllProject = new TG.BLL.cm_Project();
            TG.Model.cm_Project cop = bllProject.GetModel(ProSysNo);
            lblCprName.Text = cop.pro_name;
        }
    }
}