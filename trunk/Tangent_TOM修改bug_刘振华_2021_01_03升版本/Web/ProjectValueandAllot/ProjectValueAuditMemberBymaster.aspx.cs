﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjectValueAuditMemberBymaster : PageBase
    {
        #region 参数

        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }

        //项目ID
        public int Pro_ID
        {
            get
            {
                int proID = 0;
                int.TryParse(Request["proID"], out proID);
                return proID;
            }
        }

        //分配ID
        public int AllotID
        {
            get
            {
                int allotID = 0;
                int.TryParse(Request["allotID"], out allotID);
                return allotID;
            }
        }
        /// <summary>
        /// 审核记录系统编号
        /// </summary>
        public int ValueAllotAuditSysNo
        {
            get
            {
                int valueAllotAuditSysNo = 0;
                int.TryParse(Request["ValueAllotAuditSysNo"], out valueAllotAuditSysNo);
                return valueAllotAuditSysNo;
            }
        }

        /// <summary>
        /// 产值类型
        /// </summary>
        public string proType
        {
            get
            {
                return Request["proType"];
            }
        }
        public string IsDone { get; set; }
        #endregion

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getProInfo();
                //判断该审批是否完成
                IsDone = new TG.BLL.cm_SysMsg().IsDone(MessageID);

                //得到人员审批状态信息
                getMemberStatusInfo();
            }
        }

        /// <summary>
        /// 得到人员审批状态信息
        /// </summary>
        private void getMemberStatusInfo()
        {
            TG.Model.cm_ProjectValueByMemberAuditStatus model = new TG.Model.cm_ProjectValueByMemberAuditStatus();
            model = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetModel(Pro_ID, AllotID, UserSysNo);
            if (model != null)
            {
                AuditSuggsion.InnerText = model.AuditSuggsion;
            }
        }
        /// <summary>
        /// 项目信息
        /// </summary>
        private void getProInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(Pro_ID);

            if (pro_model != null)
            {
                //项目名称
                lblProjectName.Text = pro_model.pro_name;
                //关联合同
                this.lblReletive.Text = pro_model.Project_reletive;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }


                //建设规模
                this.lblScale.Text = pro_model.ProjectScale.ToString();
                //建筑类别
                lblBuildType.Text = pro_model.BuildType;
                //项目总负责
                this.lblPMName.Text = pro_model.PMName;
                //承接部门
                lblcpr_Unit.Text = pro_model.Unit;

                //项目阶段

                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }

                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }

                string unit = pro_model.Unit;
                if (unit.Contains("经济") || proType == "tranjjs")
                {
                    if (proType == "trantjs")
                    {
                        bindDatail();
                    }
                    else
                    {
                        getjjsMemberShow();
                    }
                }
                else
                {
                    bindDatail();
                }
            }

        }


        /// <summary>
        /// 绑定分配明细
        /// </summary>
        private void bindDatail()
        {
            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();

            DataTable dtData = BLL.GetUserValueBySpeMemberAcount(AllotID, UserSysNo).Tables[0];

            var processOne = new DataView(dtData) { RowFilter = "ItemType='0'" }.ToTable().Rows.Count;
            var processTwo = new DataView(dtData) { RowFilter = "ItemType='1'" }.ToTable().Rows.Count;
            var processThree = new DataView(dtData) { RowFilter = "ItemType='2'" }.ToTable().Rows.Count;
            var processFour = new DataView(dtData) { RowFilter = "ItemType='3'" }.ToTable().Rows.Count;

            //专业 表头
            StringBuilder sbHead = new StringBuilder();

            if (processOne <= 0 && processTwo <= 0 && processThree <= 0 && processFour <= 0)
            {
                lbl_Detail.Text = sbHead.Append(DetailMember(dtData, 4)).ToString();
            }
            else
            {
                sbHead.Append(" <div id=\"tabsMemAmount\">  <ul style=\"width:98%; margin:auto;\"> ");
                if (processOne > 0)
                {
                    sbHead.Append("<li><div class=\"cls_1\"> <a href=\"#tabsMemAmount-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
                }
                if (processTwo > 0)
                {
                    sbHead.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");
                }
                if (processThree > 0)
                {
                    sbHead.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                }
                if (processFour > 0)
                {
                    sbHead.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
                }
                sbHead.Append(" </ul>");
                if (processOne > 0)
                {
                    sbHead.Append(" <div id=\"tabsMemAmount-1\">");
                    sbHead.Append(DetailMember(dtData, 0));
                    sbHead.Append(" </div>");
                }
                if (processTwo > 0)
                {
                    sbHead.Append(" <div id=\"tabsMemAmount-2\">");
                    sbHead.Append(DetailMember(dtData, 1));
                    sbHead.Append(" </div>");
                }
                if (processThree > 0)
                {
                    sbHead.Append(" <div id=\"tabsMemAmount-3\">");
                    sbHead.Append(DetailMember(dtData, 2));
                    sbHead.Append(" </div>");
                }
                if (processFour > 0)
                {
                    sbHead.Append(" <div id=\"tabsMemAmount-4\">");
                    sbHead.Append(DetailMember(dtData, 3));
                    sbHead.Append(" </div>");
                }
                sbHead.Append(" </div>");

                lbl_Detail.Text = sbHead.ToString();
            }
        }

        /// <summary>
        /// 人员明细
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private string DetailMember(DataTable dt, int itemType)
        {
            DataTable dtData = new DataTable();
            if (itemType > 3)
            {
                dtData = dt;
            }
            else
            {
                dtData = new DataView(dt) { RowFilter = "ItemType=" + itemType + "" }.ToTable();
            }
            //专业 表头
            StringBuilder sbHead = new StringBuilder();
            sbHead.Append(" <div  class=\"cls_Container_Report\" >");
            sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td> 项目策划人员产值分配比例% </td> </tr> </table>");
            sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbHead.Append("<tr ><td style=\"width: 20%; text-align: center;\">  人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sbHead.Append(" <tr > <td style=\"width: 20%; text-align: center;\" >  姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
            sbHead.Append(" </table>");

            sbHead.Append("<table  width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");
            if (dtData.Rows.Count > 0)
            {
                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    sbHead.Append("<tr>");
                    sbHead.Append("<td style=\"text-align: center; width:20%;\">" + dtData.Rows[i]["mem_Name"].ToString() + "</td>");
                    sbHead.Append("<td style=\"text-align: center;width:10%;\">" + dtData.Rows[i]["DesignPercent"].ToString() + "%</td>");
                    sbHead.Append("<td style=\"text-align: center;width:10%;\">" + Math.Round(decimal.Parse(dtData.Rows[i]["DesignCount"].ToString()), 0) + "</td>");
                    sbHead.Append("<td style=\"text-align: center;width:10%;\">" + dtData.Rows[i]["SpecialtyHeadPercent"].ToString() + "%</td>");
                    sbHead.Append("<td style=\"text-align: center;width:10%;\">" + Math.Round(decimal.Parse(dtData.Rows[i]["SpecialtyHeadCount"].ToString()), 0) + "</td>");
                    sbHead.Append("<td style=\"text-align: center;width:10%;\">" + dtData.Rows[i]["AuditPercent"].ToString() + "%</td>");
                    sbHead.Append("<td style=\"text-align: center;width:10%;\">" + Math.Round(decimal.Parse(dtData.Rows[i]["AuditCount"].ToString()), 0) + "</td>");
                    sbHead.Append("<td style=\"text-align: center;width:10%;\">" + dtData.Rows[i]["ProofreadPercent"].ToString() + "%</td>");
                    sbHead.Append("<td style=\"text-align: center;width:10%;\">" + Math.Round(decimal.Parse(dtData.Rows[i]["ProofreadCount"].ToString()), 0) + "</td>");
                    sbHead.Append("</tr>");
                }
                sbHead.Append("</table>");
            }

            else
            {
                sbHead.Append("<tr><td colspan=\"9\" style=\"color: Red;\">没有产值信息</td></tr></table>");
            }
            sbHead.Append("</div>");
            return sbHead.ToString();
        }

        /// <summary>
        /// 经济所分配明细
        /// </summary>
        /// <returns></returns>
        private void getjjsMemberShow()
        {
            //专业 表头
            StringBuilder sbHead = new StringBuilder();

            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
            DataTable dtData = BLL.GetUserValueBySpeMemberAcount(AllotID, UserSysNo).Tables[0];

            sbHead.Append(" <div id=\"tbProjectValueBymemberAmount\" class=\"cls_Container_Report\" >");
            sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td> 项目策划人员产值分配比例% </td> </tr> </table>");
            sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbHead.Append("<tr ><td style=\"width: 15%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 35%; text-align: center;\"  > 编制 </td> <td style=\"width: 35%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sbHead.Append(" <tr ><td style=\"width: 15%; text-align: center;\">   姓名 </td> <td style=\"width: 17.5%; text-align: center;\"> 比例 </td> <td style=\"width: 17.5%; text-align: center;\"> 金额(元) </td> <td style=\"width: 17.5%; text-align: center;\"> 比例 </td> <td style=\"width: 17.5%; text-align: center;\">金额(元) </td>  </tr>");
            sbHead.Append(" </table>");

            sbHead.Append("<table id=\"gvProjectValueBymemberAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

            StringBuilder sbHtml = new StringBuilder();

            for (int j = 0; j < dtData.Rows.Count; j++)
            {
                sbHtml.Append("<tr>");
                sbHtml.Append("<td width= \"15%\">" + dtData.Rows[j]["mem_Name"].ToString() + "</td>");
                sbHtml.Append("<td width= \"17.5%\"> " + dtData.Rows[j]["DesignPercent"].ToString() + "%</td>");
                sbHtml.Append("<td width= \"17.5%\">" + dtData.Rows[j]["DesignCount"].ToString() + "</td>");
                sbHtml.Append("<td width= \"17.5%\"> " + dtData.Rows[j]["ProofreadPercent"].ToString() + "%</td>");
                sbHtml.Append("<td width= \"17.5%\">" + dtData.Rows[j]["ProofreadCount"].ToString() + "</td>");
                sbHtml.Append("</tr>");
            }

            sbHead.Append(sbHtml.ToString());
            sbHead.Append(" </table></div>");

            lbl_Detail.Text = sbHead.ToString();

        }
    }
}