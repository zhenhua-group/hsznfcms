﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProValueandAllListBymaster : PageBase
    {
        //是否生产经营部
        public string isFlag;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定年份
                BindYear();
                //选中当前年份
                SelectCurrentYear();
                //获取项目信息
                bindProjectValueNoList();
                bindProjectValueYesList();
                //绑定
                BindUnit();
                //绑定权限
                BindPreviewPower();

                List<TG.Model.cm_Role> roleList = new TG.BLL.cm_Role().GetRoleList(UserSysNo);
                isFlag = "false";
                if ((from role in roleList where role.RoleName == "生产经营部" select role).Count() > 0)
                {
                    isFlag = "true";
                }
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //获取用户权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }


        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_Name  not like '%暖通%' and unit_Name  not like '%经济所%' AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }


        private void bindProjectValueNoList()
        {
            StringBuilder strWhere = new StringBuilder();
            ////项目名称
            //if (this.txt_cprName.Text != "")
            //{
            //    strWhere.AppendFormat(" AND pro_name LIKE '%{0}%'", this.txt_cprName.Text.Trim());
            //}
            ////绑定单位
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    strWhere.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            //}
            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    strWhere.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            //}
            //strWhere.Append(" AND Unit not like '%暖通%' and Unit  not like '%经济所%'");

            GetPreviewPowerSql(ref strWhere);
            this.hid_where.Value = strWhere.ToString();

            //  TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
            //取得分页总数
            //  DataSet dsCount = BLL.P_cm_ProjectValuelNoListCount(strWhere.ToString());
            //  AspNetPager1.RecordCount = Convert.ToInt32(dsCount.Tables[0].Rows[0][0]);

            // List<TG.Model.cm_projectValueAllotView> resultList = BLL.P_cm_ProjectValuelNoListByPager(AspNetPager1.StartRecordIndex, AspNetPager1.EndRecordIndex, strWhere.ToString());
            //  this.gvproAlloting.DataSource = TG.BLL.TransformSource.ConvertToDataSet<TG.Model.cm_projectValueAllotView>(resultList);
            //  this.gvproAlloting.DataBind();
        }
        private void bindProjectValueYesList()
        {
            StringBuilder strWhere = new StringBuilder();
            //合同名称
            if (this.txt_cprName.Value != "")
            {
                strWhere.AppendFormat(" AND pro_name LIKE '%{0}%'", this.txt_cprName.Value.Trim());
            }
            //按照部门
            if (this.drp_unit.SelectedIndex != 0)
            {
                strWhere.Append(" AND Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            }
            //按照年份
            if (this.drp_year.SelectedIndex != 0)
            {
                strWhere.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            }
            strWhere.Append(" AND Unit not like '%暖通%' and Unit  not like '%经济所%'");
            GetPreviewPowerSql(ref strWhere);
            this.hid_where2.Value = strWhere.ToString();

            // TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
            //取得分页总数
            //  DataSet dsCount = BLL.P_cm_ProjectValuelYesListCount(strWhere.ToString());
            // AspNetPager2.RecordCount = Convert.ToInt32(dsCount.Tables[0].Rows[0][0]);

            //DataSet ds = BLL.P_cm_ProjectValuelYesListByPager(AspNetPager2.StartRecordIndex, AspNetPager2.EndRecordIndex, strWhere.ToString());
            //this.gvproAlloted.DataSource = ds;
            // this.gvproAlloted.DataBind();
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
    }
}