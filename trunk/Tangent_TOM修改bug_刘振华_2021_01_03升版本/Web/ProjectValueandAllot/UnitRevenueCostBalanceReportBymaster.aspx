﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="UnitRevenueCostBalanceReportBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.UnitRevenueCostBalanceReportBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="/css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="/css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #mytab {
            border-collapse: collapse;
            font-size: 9pt;
            font-family: 微软雅黑;
            margin-left: 10px;
            margin-right: 20px;
            margin-bottom: 20px;
            margin: auto;
        }

            #mytab th {
                padding: 0px 4px;
                height: 24px;
                text-align: center;
                background: white;
                border: solid 1px black;
                font-family: 微软雅黑;
                font-size: 9pt;
                text-align: center;
                word-wrap: break-word;
                word-break: break-all;
            }

            #mytab td {
                padding: 0px 4px;
                height: 22px;
                text-align: center;
                background: white;
                border: solid 1px black;
                font-family: 微软雅黑;
                font-size: 9pt;
                text-align: center;
                word-wrap: break-word;
                word-break: break-all;
            }

        .fontBold {
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>生产部门收入成本结余明细表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="/mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目信息管理</a><i class="fa fa-angle-right"> </i><a>产值分配</a><i class="fa fa-angle-right">
    </i><a>生产部门收入成本结余明细表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>查询生产部门收入成本结余明细表
                    </div>

                </div>

                <div class="portlet-body" style="display: block;">

                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control " runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>年份:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control " runat="server" AppendDataBoundItems="True" Width="100">
                                    <asp:ListItem Value="-1">--请选择--</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btn_Search" runat="server" Text="查询" CssClass="btn blue" OnClick="btn_Search_Click" />
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>生产部门收入成本结余明细表
                    </div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <asp:Button ID="btn_Output" runat="server" Text="导出Excel" CssClass="btn  red btn-sm" OnClick="btn_Output_Click" />
                    </div>
                   
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="cls_Container_Report">
                                <div class="cls_Container_Tip" style="margin-top: 5px; font-weight: bold;">
                                    <asp:Label ID="lblYear" runat="server" Text="Label"></asp:Label>
                                    <asp:Label ID="lblUnit" runat="server" Text="Label"></asp:Label>
                                    收入成本结余明细表
                                </div>
                                <div style="width: 100%;">
                                    <div id="yeardiv" runat="server">
                                        <table id="mytab" style="width: 80%;">
                                            <tr>
                                                <th width="5%">序号</th>
                                                <th width="10%">单位</th>
                                                <th width="12%">收入</th>
                                                <th width="20%">所留</th>
                                                <th width="8%">比例</th>
                                                <th width="12%">本所收入</th>
                                                <th width="20%">成本/已发固定工资</th>
                                                <th width="10%">结余</th>
                                            </tr>
                                            <asp:Literal ID="lblHtml" runat="server"></asp:Literal>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
