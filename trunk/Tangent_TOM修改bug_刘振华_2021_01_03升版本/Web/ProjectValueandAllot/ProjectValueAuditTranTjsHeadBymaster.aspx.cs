﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjectValueAuditTranTjsHeadBymaster : PageBase
    {
        #region 参数

        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }

        /// <summary>
        /// 项目系统编号
        /// </summary>
        public int proSysNo
        {
            get
            {
                int proSysNo = 0;
                int.TryParse(Request["proID"], out proSysNo);
                return proSysNo;
            }
        }



        public TG.Model.cm_TranBulidingProjectValueAllot ProjectValueAllot { get; set; }

        public int allotID
        {
            get
            {
                int allotID = 0;
                int.TryParse(Request["allotID"], out allotID);
                return allotID;
            }
        }

        public int UnitID
        {
            get
            {
                int UnitID = 0;
                int.TryParse(Request["UnitID"], out UnitID);
                return UnitID;
            }
        }
        public string IsDone { get; set; }

        /// <summary>
        /// 产值类型
        /// </summary>
        public string proType
        {
            get
            {
                return Request["proType"];
            }
        }
        /// <summary>
        /// 审核记录系统编号
        /// </summary>
        public int ValueAllotAuditSysNo
        {
            get
            {
                int valueAllotAuditSysNo = 0;
                int.TryParse(Request["ValueAllotAuditSysNo"], out valueAllotAuditSysNo);
                return valueAllotAuditSysNo;
            }
        }
        /// <summary>
        /// 合同系统阶段
        /// </summary>
        public int CoperationProcess { get; set; }
        public string TypeMark { get; set; }
        /// <summary>
        /// 是否四舍五入
        /// </summary>
        public string IsRounding = ConfigurationManager.AppSettings["IsRounding"] ?? "0";

        /// <summary>
        /// 是否可以编辑
        /// </summary>
        public int IsParamterEdit { get; set; }

        /// <summary>
        /// tg中项目
        /// </summary>
        public int ProReferenceSysNo { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string UnitName { get; set; }
        #endregion
        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                getProInfo();
                GetProjectValueAllotInfo();
                //判断该审批是否完成
                IsDone = new TG.BLL.cm_SysMsg().IsDone(MessageID);
            }
        }


        /// <summary>
        /// 项目信息
        /// </summary>
        private void getProInfo()
        {

            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(proSysNo);

            if (pro_model != null)
            {
                //项目名称
                lblProjectName.Text = pro_model.pro_name;
                //关联合同
                this.lblReletive.Text = pro_model.Project_reletive;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }

                //建设规模
                this.lblScale.Text = pro_model.ProjectScale.ToString();
                //建筑类别
                lblBuildType.Text = pro_model.BuildType;
                //项目总负责
                this.lblPMName.Text = pro_model.PMName;
                //承接部门
                lblcpr_Unit.Text = pro_model.Unit;

                //项目阶段
                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }

                //合同额
                lblCoperationAmount.Text = pro_model.Cpr_Acount.ToString();

                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }

                ProReferenceSysNo = pro_model.ReferenceSysNo;



                //显示项目信息
                TG.BLL.cm_Coperation cop = new TG.BLL.cm_Coperation();
                TG.Model.cm_Coperation cop_model = cop.GetModel(pro_model.CoperationSysNo);
                if (cop_model != null)
                {
                    string isEdit = cop_model.IsEdit == "" ? "0" : cop_model.IsEdit;
                    IsParamterEdit = int.Parse(isEdit);
                }
            }

            //查询 
            TG.BLL.tg_unit unit = new TG.BLL.tg_unit();
            TG.Model.tg_unit unit_model = unit.GetModel(UnitID);
            if (unit_model != null)
            {
                UnitName = unit_model.unit_Name;
            }
        }


        /// <summary>
        /// 得到分配信息
        /// </summary>
        private void GetProjectValueAllotInfo()
        {
            TG.BLL.cm_TranBulidingProjectValueAllot bll = new BLL.cm_TranBulidingProjectValueAllot();
            ProjectValueAllot = bll.GetModelByProID(proSysNo, allotID);
            if (ProjectValueAllot != null)
            {
                stage.Value = ProjectValueAllot.ItemType.ToString();
                CoperationProcess = int.Parse(ProjectValueAllot.ItemType.ToString());
                TypeMark = ProjectValueAllot.Mark;
                txt_ShouldBeValueCount.Text = ProjectValueAllot.AllotCount.ToString();
                string year = ProjectValueAllot.ActualAllountTime;
                if (!string.IsNullOrEmpty(year))
                {
                    if (this.drp_year.Items.FindByText(year.Trim()) != null)
                    {
                        this.drp_year.Items.FindByText(year.Trim()).Selected = true;
                    }
                }

                lblTotalCount.Text = ProjectValueAllot.TotalCount.ToString();
                lblAllotCount.Text = ProjectValueAllot.TotalCount.ToString();

                if (ProjectValueAllot.LastItemType.Trim() == ProjectValueAllot.ItemType)
                {
                    bindValue();
                    CreateDataTableByMember();
                }
                else
                {
                    bindValue1();
                    CreateDataTableByMember1();
                }
            }
        }

        /// <summary>
        /// 绑定产值分配
        /// </summary>
        private void bindValue()
        {
            if (ProjectValueAllot != null)
            {
                string itemType = lblBuildType.Text.Trim();

                decimal allotCount = decimal.Parse(txt_ShouldBeValueCount.Text);
                TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();


                //DataSet dsAllot = BLL.GetCprProcessValueDataList(itemType, CoperationProcess, allotCount, specialty, IsRounding);
                DataSet ds = BLL.GetValuedTranDataList(allotID);

                //设计阶段
                DataTable dtStageValue = ds.Tables[0];

                //工序
                DataTable dtProcessValue = ds.Tables[2];

                //设计阶段与专业
                DataTable dtStageSpeValue = ds.Tables[1];

                //生成方案设计 初步设计 施工图 后期服务表单
                if (CoperationProcess < 4 || CoperationProcess==10)
                {
                    CreateTable(dtStageValue, dtStageSpeValue, dtProcessValue);
                }
                else
                {
                    CreateOtherTable(ds.Tables[3], dtProcessValue, allotCount);
                }

                gvOne.DataSource = dtStageValue;
                gvOne.DataBind();

                gvTwo.DataSource = dtStageValue;
                gvTwo.DataBind();

                gvThree.DataSource = dtStageValue;
                gvThree.DataBind();

                gvFour.DataSource = dtStageValue;
                gvFour.DataBind();

                gvTen.DataSource = dtStageValue;
                gvTen.DataBind();
            }

        }
        /// <summary>
        /// 生成表单
        /// </summary>
        /// <param name="dtStageValue">设计阶段</param>
        /// <param name="dtStageSpeValue">专业</param>
        /// <param name="dtProcessValue">工序</param>
        private void CreateTable(DataTable dtStageValue, DataTable dtStageSpeValue, DataTable dtProcessValue)
        {

            //取得专业
            var dtSpecialtyList = new DataView(dtStageSpeValue).ToTable("Specialty", true, "Specialty");

            int count = dtSpecialtyList.Rows.Count;

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();
            sbSpeHead.Append(" <div id=\"stagespetable\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  项目各设计阶段产值分配比例% </td> </tr> </table>");
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 设计阶段</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">合计</td>");

            //比例

            sbSpeHead.Append(" </tr> </table>");

            sbSpeHead.Append("<table id=\"gvProjectStageSpe\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\"  >");


            #region  方案设计
            //方案设计工序
            StringBuilder sb_ProcessOneTable = new StringBuilder();
            sb_ProcessOneTable.Append("<div id=\"designProcessOneTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  方案设计工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td> </tr> </table>");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllotHead\" style=\"width: 98%;\">");
            sb_ProcessOneTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
            sb_ProcessOneTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
            sb_ProcessOneTable.Append("</tr></table>");
            sb_ProcessOneTable.Append("<table id=\"gvdesignProcessOne\" width=\"98%\"  class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\" >");

            //方案设计
            var dataProgram = new DataView(dtStageSpeValue).ToTable("ProgramPercent", false, "ProgramPercent", "Specialty");


            //第一行循环方案设计专业
            StringBuilder sb_one = new StringBuilder();

            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                sb_one.Append("<tr>");
                sb_one.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">方案设计</td>");

                //方案设计金额
                decimal ProgramCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["ProgramCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["ProgramCOUNT"].ToString());

                //计算方案设计总百分比
                var sumProgramPercent = dataProgram.AsEnumerable().Sum(s => s.Field<decimal>("ProgramPercent"));

                decimal sumOther = 100 - sumProgramPercent;

                #region 建筑

                //取得建筑
                decimal bulidingOnePercent = 0;
                //看是否有建筑人员参与
                var bulidingOneData = new DataView(dataProgram) { RowFilter = "Specialty like '%建筑%'" }.ToTable();

                if (bulidingOneData != null && bulidingOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingOnePercent = decimal.Parse(bulidingOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }

                decimal bulidingOneCount = 0;

                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingOneCount = Math.Round(bulidingOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingOneCount = Math.Floor(bulidingOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"9%\">" + bulidingOneCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureOnePercent = 0;
                //看是否有结构人员参与
                var structureOneData = new DataView(dataProgram) { RowFilter = "Specialty like '%结构%'" }.ToTable();

                if (structureOneData != null && structureOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureOnePercent = decimal.Parse(structureOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal structureOneCount = 0;

                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureOneCount = Math.Round(structureOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureOneCount = Math.Floor(structureOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"9%\">" + structureOneCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainOnePercent = 0;
                //看是否有给排水人员参与
                var drainOneData = new DataView(dataProgram) { RowFilter = "Specialty like '%给排水%'" }.ToTable();

                if (drainOneData != null && drainOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainOnePercent = decimal.Parse(drainOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal drainOneCount = 0;

                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainOneCount = Math.Round(drainOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainOneCount = Math.Floor(drainOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"9%\">" + drainOneCount + "</td>");

                #endregion

                #region 电气

                //取得电气
                decimal electricOnePercent = 0;
                //看是否有电气人员参与
                var electricOneData = new DataView(dataProgram) { RowFilter = "Specialty like '%电气%'" }.ToTable();

                if (electricOneData != null && electricOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricOnePercent = decimal.Parse(electricOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal electricOneCount = 0;

                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricOneCount = Math.Round(electricOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricOneCount = Math.Floor(electricOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"9%\">" + electricOneCount + "</td>");

                #endregion

                #region 合计
                decimal programCount = bulidingOneCount + structureOneCount + drainOneCount + electricOneCount;

                decimal programPercent = bulidingOnePercent + structureOnePercent + drainOnePercent + electricOnePercent;

                sb_one.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(programPercent, 0) + "</span></td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_one.Append("<td width=\"9%\">" + Math.Round(programCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_one.Append("<td width=\"9%\">" + Math.Floor(programCount) + "</td>");
                }

                sb_one.Append("</tr>");
                #endregion

                //方案设计工序 --方案为0
                var dtProgramProcessValue = new DataView(dtProcessValue) { RowFilter = "type=0" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("建筑", dtProgramProcessValue, bulidingOneCount, 0));

                //取得结构工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("结构", dtProgramProcessValue, structureOneCount, 0));

                //取得给排水工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("给排水", dtProgramProcessValue, drainOneCount, 0));

                //取得电气工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("电气", dtProgramProcessValue, electricOneCount, 0));

                sb_ProcessOneTable.Append("</table></div>");

            }
            #endregion

            #region 初步设计
            //第一行循环方案设计专业
            StringBuilder sb_two = new StringBuilder();
            //初步设计工序
            StringBuilder sb_ProcessTwoTable = new StringBuilder();
            // 0个阶段 1 方案+初设 二阶段 3 三阶段
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                sb_two.Append("<tr>");
                sb_two.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">初步设计</td>");


                //方案设计
                var dataPreliminary = new DataView(dtStageSpeValue).ToTable("preliminaryPercent", false, "preliminaryPercent", "Specialty");

                //初步设计金额
                decimal preliminaryCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["preliminaryCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["preliminaryCOUNT"].ToString());

                //计算初步设计总百分比
                var sumPreliminaryPercent = dataPreliminary.AsEnumerable().Sum(s => s.Field<decimal>("preliminaryPercent"));

                sb_ProcessTwoTable.Append("<div id=\"designProcessTwoTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb_ProcessTwoTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  初步设计工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td> </tr> </table>");
                sb_ProcessTwoTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                sb_ProcessTwoTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%; height:30px text-align: center;\" colspan=\"2\">    审核   </td> ");
                sb_ProcessTwoTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");

                sb_ProcessTwoTable.Append("</tr></table>");
                sb_ProcessTwoTable.Append("<table id=\"gvdesignProcessTwo\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

                var sumTwo = 100 - sumPreliminaryPercent;
                //初步设计
                #region 建筑

                //取得建筑
                decimal bulidingTwoPercent = 0;
                //看是否有建筑人员参与
                var bulidingTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty like '%建筑%'" }.ToTable();

                if (bulidingTwoData != null && bulidingTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingTwoPercent = decimal.Parse(bulidingTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }

                decimal bulidingTwoCount = 0;

                sb_two.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingTwoCount = Math.Round(bulidingTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingTwoCount = Math.Floor(bulidingTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"9%\">" + bulidingTwoCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureTwoPercent = 0;
                //看是否有结构人员参与
                var structureTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty like '%结构%'" }.ToTable();

                if (structureTwoData != null && structureTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureTwoPercent = decimal.Parse(structureTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal structureTwoCount = 0;

                sb_two.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureTwoCount = Math.Round(structureTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureTwoCount = Math.Floor(structureTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"9%\">" + structureTwoCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainTwoPercent = 0;
                //看是否有给排水人员参与
                var drainTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty like '%给排水%'" }.ToTable();

                if (drainTwoData != null && drainTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainTwoPercent = decimal.Parse(drainTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal drainTwoCount = 0;

                sb_two.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainTwoCount = Math.Round(drainTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainTwoCount = Math.Floor(drainTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"9%\">" + drainTwoCount + "</td>");

                #endregion

                #region 电气

                //取得电气
                decimal electricTwoPercent = 0;
                //看是否有电气人员参与
                var electricTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty like '%电气%'" }.ToTable();

                if (electricTwoData != null && electricTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricTwoPercent = decimal.Parse(electricTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal electricTwoCount = 0;

                sb_two.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricTwoCount = Math.Round(electricTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricTwoCount = Math.Floor(electricTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"9%\">" + electricTwoCount + "</td>");

                #endregion

                #region 合计
                decimal preliminarySumCount = bulidingTwoCount + structureTwoCount + drainTwoCount + electricTwoCount;

                decimal preliminarySumPercent = bulidingTwoPercent + structureTwoPercent + drainTwoPercent + electricTwoPercent;

                sb_two.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(preliminarySumPercent, 0) + "</span></td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_two.Append("<td width=\"9%\">" + Math.Round(preliminarySumCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_two.Append("<td width=\"9%\">" + Math.Floor(preliminarySumCount) + "</td>");
                }

                sb_two.Append("</tr>");
                #endregion

                //初步设计工序 --初步为1
                var dtPreliminaryProcessValue = new DataView(dtProcessValue) { RowFilter = "type=1" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("建筑", dtPreliminaryProcessValue, bulidingTwoCount, 1));

                //取得结构工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("结构", dtPreliminaryProcessValue, structureTwoCount, 1));

                //取得给排水工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("给排水", dtPreliminaryProcessValue, drainTwoCount, 1));

                //取得电气工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("电气", dtPreliminaryProcessValue, electricTwoCount, 1));

                sb_ProcessTwoTable.Append("</table></div>");

            }
            #endregion

            #region 施工图设计
            //第一行循环方案设计专业
            StringBuilder sb_three = new StringBuilder();

            StringBuilder sb_ProcessThreeTable = new StringBuilder();

            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                sb_three.Append("<tr>");
                sb_three.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">施工图设计</td>");


                //施工图设计
                var dataWorkDraw = new DataView(dtStageSpeValue).ToTable("WorkDrawPercent", false, "WorkDrawPercent", "Specialty");

                //施工图设计金额
                decimal WorkDrawPercentCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["WorkDrawCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["WorkDrawCOUNT"].ToString());

                //计算施工图总百分比
                var sumWorkDrawPercent = dataWorkDraw.AsEnumerable().Sum(s => s.Field<decimal>("WorkDrawPercent"));

                //初步设计工序
                sb_ProcessThreeTable.Append("<div id=\"designProcessThreeTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb_ProcessThreeTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  施工图工序产值分配比例%<span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span> </td> </tr> </table>");
                sb_ProcessThreeTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");

                sb_ProcessThreeTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
                sb_ProcessThreeTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center; \" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
                sb_ProcessThreeTable.Append("</tr></table>");
                sb_ProcessThreeTable.Append("<table id=\"gvdesignProcessThree\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

                var sumThree = 100 - sumWorkDrawPercent;
                #region 建筑

                //取得建筑
                decimal bulidingThreePercent = 0;
                //看是否有建筑人员参与
                var bulidingThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty like'%建筑%'" }.ToTable();

                if (bulidingThreeData != null && bulidingThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingThreePercent = decimal.Parse(bulidingThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }

                decimal bulidingThreeCount = 0;

                sb_three.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingThreeCount = Math.Round(bulidingThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingThreeCount = Math.Floor(bulidingThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"9%\">" + bulidingThreeCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureThreePercent = 0;
                //看是否有结构人员参与
                var structureThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty like '%结构%'" }.ToTable();

                if (structureThreeData != null && structureThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureThreePercent = decimal.Parse(structureThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal structureThreeCount = 0;

                sb_three.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureThreeCount = Math.Round(structureThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureThreeCount = Math.Floor(structureThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"9%\">" + structureThreeCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainThreePercent = 0;
                //看是否有给排水人员参与
                var drainThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty like '%给排水%'" }.ToTable();

                if (drainThreeData != null && drainThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainThreePercent = decimal.Parse(drainThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal drainThreeCount = 0;

                sb_three.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainThreeCount = Math.Round(drainThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainThreeCount = Math.Floor(drainThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"9%\">" + drainThreeCount + "</td>");

                #endregion

                #region 电气

                //取得电气
                decimal electricThreePercent = 0;
                //看是否有电气人员参与
                var electricThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty like '%电气%'" }.ToTable();

                if (electricThreeData != null && electricThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricThreePercent = decimal.Parse(electricThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal electricThreeCount = 0;

                sb_three.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricThreeCount = Math.Round(electricThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricThreeCount = Math.Floor(electricThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"9%\">" + electricThreeCount + "</td>");

                #endregion

                #region 合计
                decimal workDrawSumCount = bulidingThreeCount + structureThreeCount + drainThreeCount + electricThreeCount;

                decimal workDrawSumPercent = bulidingThreePercent + structureThreePercent + drainThreePercent + electricThreePercent;

                sb_three.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(workDrawSumPercent, 0) + "</span></td>");


                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_three.Append("<td width=\"9%\">" + Math.Round(workDrawSumCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_three.Append("<td width=\"9%\">" + Math.Floor(workDrawSumCount) + "</td>");
                }

                sb_three.Append("</tr>");
                #endregion
                //施工图 --施工图为2
                var dtWorkingProcessValue = new DataView(dtProcessValue) { RowFilter = "type=2" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("建筑", dtWorkingProcessValue, bulidingThreeCount, 2));

                //取得结构工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("结构", dtWorkingProcessValue, structureThreeCount, 2));

                //取得给排水工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("给排水", dtWorkingProcessValue, drainThreeCount, 2));

                //取得电气工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("电气", dtWorkingProcessValue, electricThreeCount, 2));

                sb_ProcessThreeTable.Append("</table></div>");
            }
            #endregion

            #region 后期服务
            //第一行循环方案设计专业
            StringBuilder sb_four = new StringBuilder();
            StringBuilder sb_ProcessFourTable = new StringBuilder();
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                sb_four.Append("<tr>");
                sb_four.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">后期服务</td>");

                //后期服务
                var dataLateStage = new DataView(dtStageSpeValue).ToTable("LateStagePercent", false, "LateStagePercent", "Specialty");

                //后期服务金额
                decimal lastStageCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["LateStageCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["LateStageCOUNT"].ToString());

                //后期服务总百分比
                var sumLateStagePercent = dataLateStage.AsEnumerable().Sum(s => s.Field<decimal>("LateStagePercent"));


                //后期服务工序

                sb_ProcessFourTable.Append("<div id=\"designProcessFourTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb_ProcessFourTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  后期服务工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td> </tr> </table>");
                sb_ProcessFourTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");

                sb_ProcessFourTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%; text-align: center;\" colspan=\"2\">    审核   </td> ");
                sb_ProcessFourTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
                sb_ProcessFourTable.Append("</tr></table>");
                sb_ProcessFourTable.Append("<table id=\"gvdesignProcessFour\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

                var sumFour = 100 - sumLateStagePercent;

                #region 建筑

                //取得建筑
                decimal bulidingFourPercent = 0;
                //看是否有建筑人员参与
                var bulidingFourData = new DataView(dataLateStage) { RowFilter = " Specialty like '%建筑%'" }.ToTable();

                if (bulidingFourData != null && bulidingFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingFourPercent = decimal.Parse(bulidingFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }

                decimal bulidingFourCount = 0;

                sb_four.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingFourCount = Math.Round(bulidingFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingFourCount = Math.Floor(bulidingFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"9%\">" + bulidingFourCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureFourPercent = 0;
                //看是否有结构人员参与
                var structureFourData = new DataView(dataLateStage) { RowFilter = " Specialty like '%结构%'" }.ToTable();

                if (structureFourData != null && structureFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureFourPercent = decimal.Parse(structureFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal structureFourCount = 0;

                sb_four.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureFourCount = Math.Round(structureFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureFourCount = Math.Floor(structureFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"9%\">" + structureFourCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainFourPercent = 0;
                //看是否有给排水人员参与
                var drainFourData = new DataView(dataLateStage) { RowFilter = " Specialty like '%给排水%'" }.ToTable();

                if (drainFourData != null && drainFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainFourPercent = decimal.Parse(drainFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal drainFourCount = 0;

                sb_four.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainFourCount = Math.Round(drainFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainFourCount = Math.Floor(drainFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"9%\">" + drainFourCount + "</td>");

                #endregion

                #region 电气

                //取得电气
                decimal electricFourPercent = 0;
                //看是否有电气人员参与
                var electricFourData = new DataView(dataLateStage) { RowFilter = "Specialty like '%电气%'" }.ToTable();

                if (electricFourData != null && electricFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricFourPercent = decimal.Parse(electricFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal electricFourCount = 0;

                sb_four.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricFourCount = Math.Round(electricFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricFourCount = Math.Floor(electricFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"9%\">" + electricFourCount + "</td>");

                #endregion

                #region 合计
                decimal lastSumCount = bulidingFourCount + structureFourCount + drainFourCount + electricFourCount;

                decimal lastSumPercent = bulidingFourPercent + structureFourPercent + drainFourPercent + electricFourPercent;

                sb_four.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(lastSumPercent, 0) + "</span></td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_four.Append("<td width=\"9%\">" + Math.Round(lastSumCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_four.Append("<td width=\"9%\">" + Math.Floor(lastSumCount) + "</td>");
                }

                sb_four.Append("</tr>");
                #endregion

                //后期--后期为3
                var dtLateStageProcessValue = new DataView(dtProcessValue) { RowFilter = "type=3" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("建筑", dtLateStageProcessValue, bulidingFourCount, 3));

                //取得结构工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("结构", dtLateStageProcessValue, structureFourCount, 3));

                //取得给排水工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("给排水", dtLateStageProcessValue, drainFourCount, 3));

                //取得电气工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue("电气", dtLateStageProcessValue, electricFourCount, 3));
                sb_ProcessFourTable.Append("</table></div>");
            }
            #endregion

            if (CoperationProcess == 0)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 1)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
            }

            else if (CoperationProcess == 2)
            {
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 3)
            {
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 10)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            sbSpeHead.Append("</table></div>");

            lbl_stagespetable.Text = sbSpeHead.ToString();

            StringBuilder strTab = new StringBuilder();
            strTab.Append(" <div id=\"tabs\" >  <ul style=\"width:98%; margin:auto;\"> ");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabs-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabs-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");
            }

            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabs-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabs-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
            }

            strTab.Append("</ul>");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append(" <div id=\"tabs-1\">");
                strTab.Append(sb_ProcessOneTable.ToString());
                strTab.Append(" </div>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append(" <div id=\"tabs-2\">");
                strTab.Append(sb_ProcessTwoTable.ToString());
                strTab.Append(" </div>");
            }
        
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                strTab.Append(" <div id=\"tabs-3\">");
                strTab.Append(sb_ProcessThreeTable.ToString());
                strTab.Append(" </div>");
                strTab.Append(" <div id=\"tabs-4\">");
                strTab.Append(sb_ProcessFourTable.ToString());
                strTab.Append(" </div>");
            }
            strTab.Append(" </div>");
            lbl_Tab.Text = strTab.ToString();

        }

        /// <summary>
        /// 得到各专业工序产值
        /// </summary>
        /// <param name="speName">专业名称</param>
        /// <param name="dtProcessValue">工序</param>
        /// <param name="speCount">专业产值</param>
        /// <returns>各专业工序产值</returns>
        private string getSpeProcessValue(string speName, DataTable dtProcessValue, decimal speCount, int processID)
        {
            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
            StringBuilder sb_ProcessOneTable = new StringBuilder();

            //计算 该专业下 该审核角色是否有人员参与

            //看该专业审核 是否有人参与
            int countAudit = BLL.IsRolePlay(4, speName, proSysNo);

            //该专业 专业负责 是否有人参与
            int countSpecialtyHead = BLL.IsRolePlay(2, speName, proSysNo);

            // 该专业 校对 是否有人参与
            int countProofread = BLL.IsRolePlay(3, speName, proSysNo);

            //该专业 审核 是否有人参与
            int countDesign = BLL.IsDesignerPlay(proSysNo, speName);

            var programBulidingData = new DataView(dtProcessValue) { RowFilter = "Specialty='" + speName + "'" }.ToTable();
            // 审核比例
            decimal auditpercent = 0;
            decimal specialtyHeadPercent = 0;
            decimal proofreadPercent = 0;
            decimal designPercent = 0;
            if (programBulidingData != null && programBulidingData.Rows.Count > 0)
            {
                //审核比例
                auditpercent = string.IsNullOrEmpty(programBulidingData.Rows[0]["AuditPercent"].ToString()) ? 0 : decimal.Parse(programBulidingData.Rows[0]["AuditPercent"].ToString());

                //专业负责比例
                specialtyHeadPercent = string.IsNullOrEmpty(programBulidingData.Rows[0]["SpecialtyHeadPercent"].ToString()) ? 0 : decimal.Parse(programBulidingData.Rows[0]["SpecialtyHeadPercent"].ToString());

                //校对比例
                proofreadPercent = string.IsNullOrEmpty(programBulidingData.Rows[0]["ProofreadPercent"].ToString()) ? 0 : decimal.Parse(programBulidingData.Rows[0]["ProofreadPercent"].ToString());

                //设计比例
                designPercent = string.IsNullOrEmpty(programBulidingData.Rows[0]["DesignPercent"].ToString()) ? 0 : decimal.Parse(programBulidingData.Rows[0]["DesignPercent"].ToString());

            }


            decimal totalRoleCount = 0;
            int count = 0;
            if (countAudit > 0)
            {
                totalRoleCount = totalRoleCount + auditpercent;
                count = count + 1;
            }
            if (countSpecialtyHead > 0)
            {
                totalRoleCount = totalRoleCount + specialtyHeadPercent;
                count = count + 1;
            }

            if (countProofread > 0)
            {
                totalRoleCount = totalRoleCount + proofreadPercent;
                count = count + 1;
            }
            if (countDesign > 0)
            {
                totalRoleCount = totalRoleCount + designPercent;
                count = count + 1;
            }


            decimal otherTotal = 100 - totalRoleCount;

            sb_ProcessOneTable.Append("<tr>");
            sb_ProcessOneTable.Append("<td width= 10% class=\"cls_Column\"> " + speName + "</td>");

            //审核
            if (countAudit > 0)
            {
                if (totalRoleCount != 0 && count != 0)
                {
                    auditpercent = auditpercent + Convert.ToDecimal(otherTotal / count);
                }
            }
            else
            {
                auditpercent = 0;
            }


            decimal auditCount = auditpercent / 100 * speCount;
            //0 表示方案设计 3表示后期服务
            if (processID == 0 || processID == 3)
            {
                if (auditpercent == 0)
                {
                    sb_ProcessOneTable.Append("<td width= 9%> 0%</td>");
                    sb_ProcessOneTable.Append("<td width= 9%> 0</td>");
                }
                else
                {
                    auditpercent = Math.Round(auditpercent, 2);
                    sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"6\" type=\"text\"  value=" + auditpercent + " />%</td>");

                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(auditCount) + "</td>");
                    }
                    else
                    {
                        sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(auditCount) + "</td>");
                    }
                }
            }
            else
            {
                auditpercent = Math.Round(auditpercent, 2);
                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"6\" type=\"text\"  value=" + auditpercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(auditCount) + "</td>");
                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(auditCount) + "</td>");
                }
            }

            //专业负责
            if (countSpecialtyHead > 0)
            {
                if (totalRoleCount != 0 && count != 0)
                {
                    // specialtyHeadPercent = specialtyHeadPercent + specialtyHeadPercent * otherTotal / totalRoleCount;
                    specialtyHeadPercent = specialtyHeadPercent + Convert.ToDecimal(otherTotal / count);
                }
            }
            else
            {
                specialtyHeadPercent = 0;
            }


            decimal specialtyHeadCount = specialtyHeadPercent / 100 * speCount;

            //该专业没有专业负责人员参与
            if (processID == 0 || processID == 3)
            {
                if (specialtyHeadPercent == 0)
                {
                    sb_ProcessOneTable.Append("<td width= 9%> 0%</td>");
                    sb_ProcessOneTable.Append("<td width= 9%> 0</td>");
                }
                else
                {
                    specialtyHeadPercent = Math.Round(specialtyHeadPercent, 2);

                    sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + specialtyHeadPercent + " />%</td>");

                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(specialtyHeadCount, 0) + "</td>");

                    }
                    else
                    {
                        sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(specialtyHeadCount) + "</td>");
                    }
                }
            }
            else
            {
                specialtyHeadPercent = Math.Round(specialtyHeadPercent, 2);

                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + specialtyHeadPercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(specialtyHeadCount, 0) + "</td>");

                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(specialtyHeadCount) + "</td>");
                }
            }

            //校对
            if (countProofread > 0)
            {
                if (totalRoleCount != 0 && count != 0)
                {
                    proofreadPercent = proofreadPercent + Convert.ToDecimal(otherTotal / count);
                }
            }
            else
            {
                proofreadPercent = 0;
            }

            decimal proofreadCount = proofreadPercent / 100 * speCount;
            //没有校对人员参与
            if (processID == 0 || processID == 3)
            {
                if (proofreadPercent == 0)
                {
                    sb_ProcessOneTable.Append("<td width= 9%> 0%</td>");
                    sb_ProcessOneTable.Append("<td width= 9%> 0</td>");
                }
                else
                {
                    proofreadPercent = Math.Round(proofreadPercent, 2);

                    sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + proofreadPercent + " />%</td>");

                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        sb_ProcessOneTable.Append("<td width= 9%> " + Math.Round(proofreadCount, 0) + "</td>");
                    }
                    else
                    {
                        sb_ProcessOneTable.Append("<td width= 9%> " + Math.Floor(proofreadCount) + "</td>");
                    }
                }
            }
            else
            {
                proofreadPercent = Math.Round(proofreadPercent, 2);

                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + proofreadPercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%> " + Math.Round(proofreadCount, 0) + "</td>");
                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%> " + Math.Floor(proofreadCount) + "</td>");
                }
            }


            //设计
            if (countDesign > 0)
            {
                if (totalRoleCount != 0 && count != 0)
                {
                    // designPercent = designPercent + designPercent * otherTotal / totalRoleCount;
                    designPercent = designPercent + Convert.ToDecimal(otherTotal / count);
                }
            }
            else
            {
                designPercent = 0;
            }


            decimal designCount = designPercent / 100 * speCount;
            //没有设计人员参与
            if (processID == 0 || processID == 3)
            {
                //if (designPercent == 0)
                //{
                //    sb_ProcessOneTable.Append("<td width= 9%> 0%</td>");
                //    sb_ProcessOneTable.Append("<td width= 9%> 0</td>");
                //}
                //else
                //{
                designPercent = Math.Round(designPercent, 2);

                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + designPercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(designCount, 0) + "</td>");
                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(designCount) + "</td>");
                }
                // }
            }
            else
            {
                designPercent = Math.Round(designPercent, 2);

                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + designPercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(designCount, 0) + "</td>");
                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(designCount) + "</td>");
                }
            }

            decimal totalPercent = auditpercent + specialtyHeadPercent + proofreadPercent + designPercent;
            //decimal 
            if (totalPercent == 0)
            {
                sb_ProcessOneTable.Append("<td width= 9%> <span>0</span>%</td>");
            }
            else
            {
                sb_ProcessOneTable.Append("<td width= 9%  style=\"background-color:#D2F3CB\"> <span>" + Math.Round(totalPercent, 0) + "</span>%</td>");
            }

            decimal countTotal = specialtyHeadCount + auditCount + proofreadCount + designCount;

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                sb_ProcessOneTable.Append("<td width= 9%> " + Math.Round(countTotal, 0) + "</td>");
            }
            else
            {
                sb_ProcessOneTable.Append("<td width= 9%> " + Math.Floor(countTotal) + "</td>");

            }
            sb_ProcessOneTable.Append("</tr>");

            return sb_ProcessOneTable.ToString();

        }


        /// <summary>
        /// 生成表四之后的表单
        /// </summary>
        /// <param name="dtSpe">专业</param>
        /// <param name="dtProcess">工序</param>
        private void CreateOtherTable(DataTable dtSpe, DataTable dtProcess, decimal allotCount)
        {
            //取得专业
            var dtSpecialtyList = new DataView(dtSpe).ToTable("Specialty", true, "Specialty");

            int count = dtSpecialtyList.Rows.Count;

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();
            sbSpeHead.Append(" <div id=\"stagetabletfive\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> ");

            if (CoperationProcess == 4)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  室外工程产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 5)
            {
                sbSpeHead.Append("<tr><td align=\"center\">  锅炉房产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 6)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  地上单建水泵房产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 7)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  地上单建变配所(室)产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 8)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  单建地下室（车库）产值分配比例%</td></tr>");
            }
            else if (CoperationProcess == 9)
            {
                sbSpeHead.Append("<tr><td align=\"center\">  市政道路工程产值分配比例% </td></tr>");
            }

            //比例
            decimal percent = decimal.Divide(92, count * 2);
            percent = decimal.Round(percent, 2);
            sbSpeHead.Append("</table>");
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 专业</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">合计</td>");

            sbSpeHead.Append("</tr></table>");

            sbSpeHead.Append("<table id=\"gvFive\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\"  >");


            //
            StringBuilder sb_one = new StringBuilder();
            sb_one.Append("<tr>");
            sb_one.Append("<td class=\"cls_Column\" style=\"width:10%\">比例</td>");

            //计算方案设计总百分比
            var sumProgramPercent = dtSpe.AsEnumerable().Sum(s => s.Field<decimal>("SpecialtyPercent"));


            decimal sumOther = 100 - sumProgramPercent;

            #region 建筑

            //取得建筑
            decimal bulidingOnePercent = 0;
            //看是否有建筑人员参与
            var bulidingOneData = new DataView(dtSpe) { RowFilter = "Specialty='建筑'" }.ToTable();

            if (bulidingOneData != null && bulidingOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    bulidingOnePercent = decimal.Parse(bulidingOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }

            decimal bulidingOneCount = 0;
            if (bulidingOnePercent == 0)
            {
                sb_one.Append("<td width=\"9%\">0</td>");
                sb_one.Append("<td width=\"9%\">0</td>");
            }
            else
            {
                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingOneCount = Math.Round(bulidingOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingOneCount = Math.Floor(bulidingOnePercent / 100 * allotCount);
                }

                sb_one.Append("<td width=\"9%\">" + bulidingOneCount + "</td>");
            }
            #endregion

            #region 结构

            //取得结构
            decimal structureOnePercent = 0;
            //看是否有结构人员参与
            var structureOneData = new DataView(dtSpe) { RowFilter = "Specialty='结构'" }.ToTable();

            if (structureOneData != null && structureOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    structureOnePercent = decimal.Parse(structureOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal structureOneCount = 0;
            if (structureOnePercent == 0)
            {
                sb_one.Append("<td width=\"9%\">0</td>");
                sb_one.Append("<td width=\"9%\">0</td>");
            }
            else
            {
                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureOneCount = Math.Round(structureOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureOneCount = Math.Floor(structureOnePercent / 100 * allotCount);
                }

                sb_one.Append("<td width=\"9%\">" + structureOneCount + "</td>");
            }
            #endregion

            #region 给排水

            //取得给排水
            decimal drainOnePercent = 0;
            //看是否有给排水人员参与
            var drainOneData = new DataView(dtSpe) { RowFilter = "Specialty='给排水'" }.ToTable();

            if (drainOneData != null && drainOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    drainOnePercent = decimal.Parse(drainOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal drainOneCount = 0;
            if (drainOnePercent == 0)
            {
                sb_one.Append("<td width=\"9%\">0</td>");
                sb_one.Append("<td width=\"9%\">0</td>");
            }
            else
            {
                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainOneCount = Math.Round(drainOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainOneCount = Math.Floor(drainOnePercent / 100 * allotCount);
                }

                sb_one.Append("<td width=\"9%\">" + drainOneCount + "</td>");
            }
            #endregion

            #region 电气

            //取得电气
            decimal electricOnePercent = 0;
            //看是否有电气人员参与
            var electricOneData = new DataView(dtSpe) { RowFilter = "Specialty='电气'" }.ToTable();

            if (electricOneData != null && electricOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    electricOnePercent = decimal.Parse(electricOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal electricOneCount = 0;
            if (electricOnePercent == 0)
            {
                sb_one.Append("<td width=\"9%\">0</td>");
                sb_one.Append("<td width=\"9%\">0</td>");
            }
            else
            {
                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricOneCount = Math.Round(electricOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricOneCount = Math.Floor(electricOnePercent / 100 * allotCount);
                }

                sb_one.Append("<td width=\"9%\">" + electricOneCount + "</td>");
            }
            #endregion

            #region 合计
            decimal programCount = bulidingOneCount + structureOneCount + drainOneCount + electricOneCount;

            decimal programPercent = bulidingOnePercent + structureOnePercent + drainOnePercent + electricOnePercent;

            sb_one.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(programPercent, 0) + "</span></td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                sb_one.Append("<td width=\"9%\">" + Math.Round(programCount, MidpointRounding.AwayFromZero) + "</td>");
            }
            else //去整
            {
                sb_one.Append("<td width=\"9%\">" + Math.Floor(programCount) + "</td>");
            }

            sb_one.Append("</tr>");
            #endregion


            StringBuilder sb_ProcessOneTable = new StringBuilder();
            sb_ProcessOneTable.Append(" <div id=\"tbOutDoor\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> ");


            if (CoperationProcess == 4)
            {
                sb_ProcessOneTable.Append("<tr>  <td  align=\"center\" >    室外工程工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td></tr>");
            }
            else if (CoperationProcess == 5)
            {
                sb_ProcessOneTable.Append("<tr>  <td   align=\"center\" >   锅炉房工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td></tr>");
            }
            else if (CoperationProcess == 6)
            {
                sb_ProcessOneTable.Append("<tr>  <td  align=\"center\" >    地上单建水泵房工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td></tr>");
            }
            else if (CoperationProcess == 7)
            {
                sb_ProcessOneTable.Append("<tr>   <td   align=\"center\" >    地上单建变配所(室)工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td></tr>");
            }
            else if (CoperationProcess == 8)
            {
                sb_ProcessOneTable.Append("<tr>   <td   align=\"center\" >    单建地下室（车库）工序产值分配比例%<span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span> </td></tr>");
            }
            else if (CoperationProcess == 9)
            {
                sb_ProcessOneTable.Append("<tr>   <td  align=\"center\" >  市政道路工程工序产值分配比例%<span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span> </td></tr>");
            }
            sb_ProcessOneTable.Append("</table>");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sb_ProcessOneTable.Append(" <tr> <td style=\"width: 10%; text-align: center\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
            sb_ProcessOneTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
            sb_ProcessOneTable.Append("</table>");

            sb_ProcessOneTable.Append("<table id=\"gvdesignProcessFive\" width=\"98%\"  class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\" >");

            //取得室外工程的工序
            var dtProcessValue = new DataView(dtProcess) { RowFilter = "type=4" }.ToTable();

            //取得建筑工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("建筑", dtProcessValue, bulidingOneCount, CoperationProcess));

            //取得结构工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("结构", dtProcessValue, structureOneCount, CoperationProcess));

            //取得给排水工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("给排水", dtProcessValue, drainOneCount, CoperationProcess));

            //取得电气工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("电气", dtProcessValue, electricOneCount, CoperationProcess));

            sb_ProcessOneTable.Append("</table></div>");

            sbSpeHead.Append(sb_one.ToString());
            sbSpeHead.Append("</table></div>");
            lbl_StageFive.Text = sbSpeHead.ToString();
            lbl_out.Text = sb_ProcessOneTable.ToString();
        }

        /// <summary>
        /// 创建人员的表单
        /// </summary>
        private void CreateDataTableByMember()
        {
            if (ProjectValueAllot != null)
            {
                TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
                DataSet ds = new TG.BLL.cm_TranProjectValueAllot().GetTranProAuditedUser(proSysNo, "tjs", allotID);

                DataTable dtmember = ds.Tables[0];

                //工序
                DataTable dtProcess = BLL.GetDesignProcessDataList(allotID).Tables[0];

                if (dtmember.Rows.Count > 0)
                {
                    StringBuilder sbTotal = new StringBuilder();

                    //方案
                    StringBuilder sbOne = new StringBuilder();
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                    {
                        DataTable dtProcessOne = new DataView(dtProcess) { RowFilter = "type=0" }.ToTable();
                        DataTable dtProgram = new DataView(dtmember) { RowFilter = "ItemType=0" }.ToTable();
                        sbOne.Append(getMemberValue(dtProgram, 0, dtProcessOne));
                    }
                    //初步
                    StringBuilder sbTwo = new StringBuilder();
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                    {
                        DataTable dtProgram = new DataView(dtmember) { RowFilter = "ItemType=1" }.ToTable();
                        DataTable dtProcessOne = new DataView(dtProcess) { RowFilter = "type=1" }.ToTable();
                        sbTwo.Append(getMemberValue(dtProgram, 1, dtProcessOne));
                    }

                    //施工图
                    StringBuilder sbThree = new StringBuilder();
                    StringBuilder sbFour = new StringBuilder();
                    if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        DataTable dtProgram = new DataView(dtmember) { RowFilter = "ItemType=2" }.ToTable();
                        DataTable dtProcessTwo = new DataView(dtProcess) { RowFilter = "type=2" }.ToTable();
                        sbThree.Append(getMemberValue(dtProgram, 2, dtProcessTwo));
                        DataTable dtProgram1 = new DataView(dtmember) { RowFilter = "ItemType=3" }.ToTable();
                        DataTable dtProcessThree = new DataView(dtProcess) { RowFilter = "type=3" }.ToTable();
                        //后期                 
                        sbFour.Append(getMemberValue(dtProgram1, 3, dtProcessThree));
                    }
                    StringBuilder strTab = new StringBuilder();
                    StringBuilder sbOut = new StringBuilder();
                    //室外工程
                    if (CoperationProcess >= 4 &&  CoperationProcess != 10)
                    {
                        sbOut.Append(getMemberValue(dtmember, CoperationProcess, dtProcess));
                    }
                    else
                    {
                        strTab.Append(sbTotal.ToString());
                    }
                    strTab.Append("<fieldset id=\"memField\" ><legend>人员产值填写</legend>");
                    strTab.Append(" <div id=\"tabsMem\">  <ul style=\"width:98%; margin:auto;\"> ");
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                    {
                        strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabsMem-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
                    }
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                    {
                        strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMem-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");

                    }
                    if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMem-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                        strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMem-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
                    }
                    strTab.Append(" </ul>");
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                    {
                        strTab.Append("<div id=\"tabsMem-1\">");
                        strTab.Append(sbOne.ToString());
                        strTab.Append(" </div>");
                    }
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                    {
                        strTab.Append(" <div id=\"tabsMem-2\">");
                        strTab.Append(sbTwo.ToString());
                        strTab.Append(" </div>");
                    }

                    strTab.Append(" <div id=\"tabsMem-3\">");
                    strTab.Append(sbThree.ToString());
                    strTab.Append(" </div>");
                    if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        strTab.Append(" <div id=\"tabsMem-4\">");
                        strTab.Append(sbFour.ToString());
                        strTab.Append(" </div>");
                    }
                    strTab.Append(" </div>");
                    strTab.Append("</fieldset>");
                    if (CoperationProcess >= 4 &&  CoperationProcess != 10)
                    {
                        lblMember.Text = sbOut.ToString();
                    }
                    else
                    {
                        lblMember.Text = strTab.ToString();
                    }
                }
                else
                {
                    string noData = "<table style=\"100%\" id=\"tdNoMember\"><tr><td colspan=\"10\" style=\"color: Red; \">没有人员信息</td></tr></table>";
                    lblMember.Text = noData;
                }
            }
        }

        /// <summary>
        /// 得到人员产值
        /// </summary>
        /// <param name="dtProcess">阶段金额</param>
        /// <param name="speName">专业</param>
        /// <param name="dtMember">人员</param>
        /// <param name="process">阶段</param>
        /// <returns></returns>
        private string getMemberValue(DataTable dtMember, int process, DataTable dtProcess)
        {


            StringBuilder sb = new StringBuilder();
            sb.Append(" <div id=\"tbProjectValueBymember\" class=\"cls_Container_Report\" >");
            sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"><tr> ");

            //取得设计人员的专业
            var dtSpecialtyList = new DataView(dtMember).ToTable("Specialty", true, "spe_Name");


            if (process == 0)
            {
                sb.Append("<td style=\"text-align: center;\"> 方案设计-策划人员产值分配比例% </td> </tr>");

            }
            else if (process == 1)
            {
                sb.Append("<td  style=\"text-align: center;\"> 初步设计-策划人员产值分配比例% </td> </tr>");
            }
            else if (process == 2)
            {
                sb.Append("<td  style=\"text-align: center;\"> 施工图设计-策划人员产值分配比例% </td> </tr>");
            }
            else if (process == 3)
            {
                sb.Append("<td  style=\"text-align: center;\"> 后期服务-策划人员产值分配比例% </td> </tr>");
            }
            else if (process > 3)
            {
                sb.Append("<td  style=\"text-align: center;\"> 策划人员产值分配比例% </td> </tr>");
            }
            sb.Append("</table>");
            sb.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sb.Append("<tr ><td style=\"width: 10%; text-align: center;height:15px;\">  名称</td><td style=\"width: 10%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sb.Append(" <tr> <td style=\"width: 10%; text-align: center;\" > 专业 </td><td style=\"width: 10%; text-align: center;\">   姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
            sb.Append(" </table>");

            if (process == 0)
            {
                sb.Append("<table id=\"gvProjectValueOneBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\"  class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
            }
            else if (process == 1)
            {
                sb.Append("<table id=\"gvProjectValueTwoBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
            }
            else if (process == 2)
            {
                sb.Append("<table id=\"gvProjectValueThreeBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
            }
            else if (process == 3)
            {
                sb.Append("<table id=\"gvProjectValueFourBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
            }
            else
            {
                sb.Append("<table id=\"gvProjectValueBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
            }

            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                var dv = new DataView(dtMember) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();

                sb.Append("<tr>");
                sb.Append("<td  width= \"10%\" class=\"cls_Column\" rowspan=\"" + dtMemberBySpecialty.Rows.Count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                //取得本专业的工序信息

                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td class=\"display\"></td>");
                    }
                    string name = dtMemberBySpecialty.Rows[j]["mem_Name"].ToString();

                    sb.Append("<td class=\"display\" wp=\"0\">" + dtMemberBySpecialty.Rows[j]["mem_ID"].ToString() + "</td>");
                    sb.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");

                    var dtProcess_Data = new DataView(dtProcess) { RowFilter = " Specialty like '%" + dtSpecialtyList.Rows[i]["spe_Name"] + "%'" }.ToTable();
                    int dtProcessRow = dtProcess_Data.Rows.Count;
                    //建筑金额
                    decimal deSignCount = 0;
                    if (dtProcessRow > 0)
                    {
                        deSignCount = decimal.Parse(dtProcess_Data.Rows[0]["DesignAmount"].ToString()) * decimal.Parse(dtMemberBySpecialty.Rows[j]["DesignPercent"].ToString()) / 100;
                    }
                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        deSignCount = Math.Round(deSignCount, MidpointRounding.AwayFromZero);
                    }
                    else //去整
                    {
                        deSignCount = Math.Floor(deSignCount);
                    }
                    if (process == 0 || process == 3)
                    {
                        sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"  value=" + dtMemberBySpecialty.Rows[j]["DesignPercent"].ToString() + "  runat=\"server\" runat=\"server\" roles=\"sj\" spe=" + dtSpecialtyList.Rows[i]["spe_Name"] + " sz=0  />%</td>");
                        sb.Append("<td width= \"10%\">" + deSignCount + "</td>");
                    }
                    else
                    {
                        sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"  value=" + dtMemberBySpecialty.Rows[j]["DesignPercent"].ToString() + "  runat=\"server\" runat=\"server\" roles=\"sj\" spe=" + dtSpecialtyList.Rows[i]["spe_Name"] + " sz=0  />%</td>");
                        sb.Append("<td width= \"10%\">" + deSignCount + "</td>");

                    }

                    decimal specialtyHeadAmount = 0;
                    if (dtProcessRow > 0)
                    {
                        specialtyHeadAmount = decimal.Parse(dtProcess_Data.Rows[0]["SpecialtyHeadAmount"].ToString()) * decimal.Parse(dtMemberBySpecialty.Rows[j]["SpecialtyHeadPercent"].ToString()) / 100;
                    }
                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        specialtyHeadAmount = Math.Round(specialtyHeadAmount, MidpointRounding.AwayFromZero);
                    }
                    else //去整
                    {
                        specialtyHeadAmount = Math.Floor(specialtyHeadAmount);
                    }
                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\" roles=\"fz\" value=" + dtMemberBySpecialty.Rows[j]["SpecialtyHeadPercent"].ToString() + " spe=" + dtSpecialtyList.Rows[i]["spe_Name"] + " runat=\"server\" />%</td>");
                    sb.Append("<td width= \"10%\">" + specialtyHeadAmount + "</td>");


                    decimal auditAmount = 0;
                    if (dtProcessRow > 0)
                    {
                        auditAmount = decimal.Parse(dtProcess_Data.Rows[0]["AuditAmount"].ToString()) * decimal.Parse(dtMemberBySpecialty.Rows[j]["AuditPercent"].ToString()) / 100;
                    }
                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        auditAmount = Math.Round(auditAmount, MidpointRounding.AwayFromZero);
                    }
                    else //去整
                    {
                        auditAmount = Math.Floor(auditAmount);
                    }
                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\" roles=\"sh\" value=" + dtMemberBySpecialty.Rows[j]["AuditPercent"].ToString() + " spe=" + dtSpecialtyList.Rows[i]["spe_Name"] + "  runat=\"server\" />%</td>");
                    sb.Append("<td width= \"10%\">" + auditAmount + "</td>");

                    decimal proofreadAmount = 0;
                    if (dtProcessRow > 0)
                    {
                        proofreadAmount = decimal.Parse(dtProcess_Data.Rows[0]["ProofreadAmount"].ToString()) * decimal.Parse(dtMemberBySpecialty.Rows[j]["ProofreadPercent"].ToString()) / 100;
                    }
                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        proofreadAmount = Math.Round(proofreadAmount, MidpointRounding.AwayFromZero);
                    }
                    else //去整
                    {
                        proofreadAmount = Math.Floor(proofreadAmount);
                    }

                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\" roles=\"jd\" value=" + dtMemberBySpecialty.Rows[j]["ProofreadPercent"].ToString() + " spe=" + dtSpecialtyList.Rows[i]["spe_Name"] + " runat=\"server\"  sz=0 />%</td>");
                    sb.Append("<td width= \"10%\">" + proofreadAmount + "</td>");
                    sb.Append("<td class=\"display\" >" + process + "</td>");
                    sb.Append("</tr>");
                }
            }
            sb.Append("</table>");
            sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\" id=\"tbChooseUser\"><tr> ");
            if (process == 0)
            {
                sb.Append("<td><span id=\"chooseOneUser\" style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseExternalOneUser\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
            }
            else if (process == 1)
            {
                sb.Append("<td><span id=\"chooseTwoUser\"  style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseTwoExternalUser\" style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
            }
            else if (process == 2)
            {
                sb.Append("<td><span id=\"chooseThreeUser\"  style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseThreeExternalUser\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
            }
            else if (process == 3)
            {
                sb.Append("<td><span id=\"chooseFourUser\"  style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseFourExternalUser\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
            }
            else
            {
                sb.Append("<td><span id=\"chooseUser\" style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseExternalUser\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
            }
            sb.Append("</tr></table>");
            sb.Append(" </div>");

            return sb.ToString();
        }


        /// <summary>
        /// 绑定产值分配
        /// </summary>
        private void bindValue1()
        {
            if (ProjectValueAllot != null)
            {
                string itemType = lblBuildType.Text.Trim();

                decimal allotCount = decimal.Parse(txt_ShouldBeValueCount.Text);
                TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
                //参与策划人员的专业
                DataTable dtPlanSpecialty = BLL.GetPlanUserSpecialty(proSysNo).Tables[0];

                string specialty = "";
                foreach (DataRow row in dtPlanSpecialty.Rows)
                {
                    specialty += "'" + row["spe_Name"].ToString() + "'" + ",";
                }
                if (!string.IsNullOrEmpty(specialty))
                {
                    specialty = specialty.Remove(specialty.Length - 1);


                    DataSet dsAllot = BLL.GetCprProcessValueDataList(itemType, CoperationProcess, allotCount, specialty, IsRounding);

                    //设计阶段
                    DataTable dtStageValue = dsAllot.Tables[0];

                    //工序
                    DataTable dtProcessValue = dsAllot.Tables[2];

                    //设计阶段与专业
                    DataTable dtStageSpeValue = new DataView(dsAllot.Tables[1]) { RowFilter = "IsHaved=0" }.ToTable();

                    //生成方案设计 初步设计 施工图 后期服务表单
                    if (CoperationProcess < 4 &&  CoperationProcess != 10)
                    {
                        CreateTable1(dtStageValue, dtStageSpeValue, dtProcessValue);
                    }
                    else
                    {
                        DataTable dt3 = new DataView(dsAllot.Tables[3]) { RowFilter = "IsHaved=0" }.ToTable();
                        if (CoperationProcess == 5)
                        {
                            dt3 = new DataView(dt3) { RowFilter = "type like '" + TypeMark + "'" }.ToTable();
                        }
                        CreateOtherTable1(dt3, dtProcessValue, allotCount);
                    }

                    gvOne.DataSource = dtStageValue;
                    gvOne.DataBind();

                    gvTwo.DataSource = dtStageValue;
                    gvTwo.DataBind();

                    gvThree.DataSource = dtStageValue;
                    gvThree.DataBind();

                    gvFour.DataSource = dtStageValue;
                    gvFour.DataBind();

                    gvTen.DataSource = dtStageValue;
                    gvTen.DataBind();
                }
            }

        }
        /// <summary>
        /// 生成表单
        /// </summary>
        /// <param name="dtStageValue">设计阶段</param>
        /// <param name="dtStageSpeValue">专业</param>
        /// <param name="dtProcessValue">工序</param>
        private void CreateTable1(DataTable dtStageValue, DataTable dtStageSpeValue, DataTable dtProcessValue)
        {

            //取得专业
            var dtSpecialtyList = new DataView(dtStageSpeValue).ToTable("Specialty", true, "Specialty");

            int count = dtSpecialtyList.Rows.Count;

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();
            sbSpeHead.Append(" <div id=\"stagespetable\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  项目各设计阶段产值分配比例% </td> </tr> </table>");
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 设计阶段</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">合计</td>");

            //比例

            sbSpeHead.Append(" </tr> </table>");

            sbSpeHead.Append("<table id=\"gvProjectStageSpe\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\"  >");


            #region  方案设计
            //方案设计工序
            StringBuilder sb_ProcessOneTable = new StringBuilder();
            sb_ProcessOneTable.Append("<div id=\"designProcessOneTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  方案设计工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td> </tr> </table>");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllotHead\" style=\"width: 98%;\">");
            sb_ProcessOneTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
            sb_ProcessOneTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
            sb_ProcessOneTable.Append("</tr></table>");
            sb_ProcessOneTable.Append("<table id=\"gvdesignProcessOne\" width=\"98%\"  class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\" >");

            //方案设计
            var dataProgram = new DataView(dtStageSpeValue).ToTable("ProgramPercent", false, "ProgramPercent", "Specialty");


            //第一行循环方案设计专业
            StringBuilder sb_one = new StringBuilder();

            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                sb_one.Append("<tr>");
                sb_one.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">方案设计</td>");

                //方案设计金额
                decimal ProgramCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["ProgramCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["ProgramCOUNT"].ToString());

                //计算方案设计总百分比
                var sumProgramPercent = dataProgram.AsEnumerable().Sum(s => s.Field<decimal>("ProgramPercent"));

                decimal sumOther = 100 - sumProgramPercent;

                #region 建筑

                //取得建筑
                decimal bulidingOnePercent = 0;
                //看是否有建筑人员参与
                var bulidingOneData = new DataView(dataProgram) { RowFilter = "Specialty='建筑'" }.ToTable();

                if (bulidingOneData != null && bulidingOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingOnePercent = decimal.Parse(bulidingOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }

                decimal bulidingOneCount = 0;

                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingOneCount = Math.Round(bulidingOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingOneCount = Math.Floor(bulidingOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"9%\">" + bulidingOneCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureOnePercent = 0;
                //看是否有结构人员参与
                var structureOneData = new DataView(dataProgram) { RowFilter = "Specialty='结构'" }.ToTable();

                if (structureOneData != null && structureOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureOnePercent = decimal.Parse(structureOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal structureOneCount = 0;

                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureOneCount = Math.Round(structureOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureOneCount = Math.Floor(structureOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"9%\">" + structureOneCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainOnePercent = 0;
                //看是否有给排水人员参与
                var drainOneData = new DataView(dataProgram) { RowFilter = "Specialty='给排水'" }.ToTable();

                if (drainOneData != null && drainOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainOnePercent = decimal.Parse(drainOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal drainOneCount = 0;

                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainOneCount = Math.Round(drainOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainOneCount = Math.Floor(drainOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"9%\">" + drainOneCount + "</td>");

                #endregion

                #region 电气

                //取得电气
                decimal electricOnePercent = 0;
                //看是否有电气人员参与
                var electricOneData = new DataView(dataProgram) { RowFilter = "Specialty='电气'" }.ToTable();

                if (electricOneData != null && electricOneData.Rows.Count > 0)
                {
                    if (sumProgramPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricOnePercent = decimal.Parse(electricOneData.Rows[0][0].ToString()) + Convert.ToDecimal(sumOther / count);

                    }
                }
                decimal electricOneCount = 0;

                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricOneCount = Math.Round(electricOnePercent / 100 * ProgramCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricOneCount = Math.Floor(electricOnePercent / 100 * ProgramCount);
                }

                sb_one.Append("<td width=\"9%\">" + electricOneCount + "</td>");

                #endregion

                #region 合计
                decimal programCount = bulidingOneCount + structureOneCount + drainOneCount + electricOneCount;

                decimal programPercent = bulidingOnePercent + structureOnePercent + drainOnePercent + electricOnePercent;

                sb_one.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(programPercent, 0) + "</span></td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_one.Append("<td width=\"9%\">" + Math.Round(programCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_one.Append("<td width=\"9%\">" + Math.Floor(programCount) + "</td>");
                }

                sb_one.Append("</tr>");
                #endregion

                //方案设计工序 --方案为0
                var dtProgramProcessValue = new DataView(dtProcessValue) { RowFilter = "ItemType=0" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("建筑", dtProgramProcessValue, bulidingOneCount, 0));

                //取得结构工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("结构", dtProgramProcessValue, structureOneCount, 0));

                //取得给排水工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("给排水", dtProgramProcessValue, drainOneCount, 0));

                //取得电气工序的产值
                sb_ProcessOneTable.Append(getSpeProcessValue("电气", dtProgramProcessValue, electricOneCount, 0));

                sb_ProcessOneTable.Append("</table></div>");

            }
            #endregion

            #region 初步设计
            //第一行循环方案设计专业
            StringBuilder sb_two = new StringBuilder();
            //初步设计工序
            StringBuilder sb_ProcessTwoTable = new StringBuilder();
            // 0个阶段 1 方案+初设 二阶段 3 三阶段
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                sb_two.Append("<tr>");
                sb_two.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">初步设计</td>");


                //方案设计
                var dataPreliminary = new DataView(dtStageSpeValue).ToTable("preliminaryPercent", false, "preliminaryPercent", "Specialty");

                //初步设计金额
                decimal preliminaryCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["preliminaryCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["preliminaryCOUNT"].ToString());

                //计算初步设计总百分比
                var sumPreliminaryPercent = dataPreliminary.AsEnumerable().Sum(s => s.Field<decimal>("preliminaryPercent"));

                sb_ProcessTwoTable.Append("<div id=\"designProcessTwoTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb_ProcessTwoTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  初步设计工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td> </tr> </table>");
                sb_ProcessTwoTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                sb_ProcessTwoTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%; height:30px text-align: center;\" colspan=\"2\">    审核   </td> ");
                sb_ProcessTwoTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");

                sb_ProcessTwoTable.Append("</tr></table>");
                sb_ProcessTwoTable.Append("<table id=\"gvdesignProcessTwo\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

                var sumTwo = 100 - sumPreliminaryPercent;
                //初步设计
                #region 建筑

                //取得建筑
                decimal bulidingTwoPercent = 0;
                //看是否有建筑人员参与
                var bulidingTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='建筑'" }.ToTable();

                if (bulidingTwoData != null && bulidingTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingTwoPercent = decimal.Parse(bulidingTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }

                decimal bulidingTwoCount = 0;

                sb_two.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingTwoCount = Math.Round(bulidingTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingTwoCount = Math.Floor(bulidingTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"9%\">" + bulidingTwoCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureTwoPercent = 0;
                //看是否有结构人员参与
                var structureTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='结构'" }.ToTable();

                if (structureTwoData != null && structureTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureTwoPercent = decimal.Parse(structureTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal structureTwoCount = 0;

                sb_two.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureTwoCount = Math.Round(structureTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureTwoCount = Math.Floor(structureTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"9%\">" + structureTwoCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainTwoPercent = 0;
                //看是否有给排水人员参与
                var drainTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='给排水'" }.ToTable();

                if (drainTwoData != null && drainTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainTwoPercent = decimal.Parse(drainTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal drainTwoCount = 0;

                sb_two.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainTwoCount = Math.Round(drainTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainTwoCount = Math.Floor(drainTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"9%\">" + drainTwoCount + "</td>");

                #endregion

                #region 电气

                //取得电气
                decimal electricTwoPercent = 0;
                //看是否有电气人员参与
                var electricTwoData = new DataView(dataPreliminary) { RowFilter = "Specialty='电气'" }.ToTable();

                if (electricTwoData != null && electricTwoData.Rows.Count > 0)
                {
                    if (sumPreliminaryPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricTwoPercent = decimal.Parse(electricTwoData.Rows[0][0].ToString()) + Convert.ToDecimal(sumTwo / count);

                    }
                }
                decimal electricTwoCount = 0;

                sb_two.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricTwoPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricTwoCount = Math.Round(electricTwoPercent / 100 * preliminaryCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricTwoCount = Math.Floor(electricTwoPercent / 100 * preliminaryCount);
                }

                sb_two.Append("<td width=\"9%\">" + electricTwoCount + "</td>");

                #endregion

                #region 合计
                decimal preliminarySumCount = bulidingTwoCount + structureTwoCount + drainTwoCount + electricTwoCount;

                decimal preliminarySumPercent = bulidingTwoPercent + structureTwoPercent + drainTwoPercent + electricTwoPercent;

                sb_two.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(preliminarySumPercent, 0) + "</span></td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_two.Append("<td width=\"9%\">" + Math.Round(preliminarySumCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_two.Append("<td width=\"9%\">" + Math.Floor(preliminarySumCount) + "</td>");
                }

                sb_two.Append("</tr>");
                #endregion

                //初步设计工序 --初步为1
                var dtPreliminaryProcessValue = new DataView(dtProcessValue) { RowFilter = "ItemType=1" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("建筑", dtPreliminaryProcessValue, bulidingTwoCount, 1));

                //取得结构工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("结构", dtPreliminaryProcessValue, structureTwoCount, 1));

                //取得给排水工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("给排水", dtPreliminaryProcessValue, drainTwoCount, 1));

                //取得电气工序的产值
                sb_ProcessTwoTable.Append(getSpeProcessValue("电气", dtPreliminaryProcessValue, electricTwoCount, 1));

                sb_ProcessTwoTable.Append("</table></div>");

            }
            #endregion

            #region 施工图设计
            //第一行循环方案设计专业
            StringBuilder sb_three = new StringBuilder();

            StringBuilder sb_ProcessThreeTable = new StringBuilder();

            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                sb_three.Append("<tr>");
                sb_three.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">施工图设计</td>");


                //施工图设计
                var dataWorkDraw = new DataView(dtStageSpeValue).ToTable("WorkDrawPercent", false, "WorkDrawPercent", "Specialty");

                //施工图设计金额
                decimal WorkDrawPercentCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["WorkDrawCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["WorkDrawCOUNT"].ToString());

                //计算施工图总百分比
                var sumWorkDrawPercent = dataWorkDraw.AsEnumerable().Sum(s => s.Field<decimal>("WorkDrawPercent"));

                //初步设计工序
                sb_ProcessThreeTable.Append("<div id=\"designProcessThreeTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb_ProcessThreeTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  施工图工序产值分配比例%<span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span> </td> </tr> </table>");
                sb_ProcessThreeTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");

                sb_ProcessThreeTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
                sb_ProcessThreeTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center; \" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
                sb_ProcessThreeTable.Append("</tr></table>");
                sb_ProcessThreeTable.Append("<table id=\"gvdesignProcessThree\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

                var sumThree = 100 - sumWorkDrawPercent;
                #region 建筑

                //取得建筑
                decimal bulidingThreePercent = 0;
                //看是否有建筑人员参与
                var bulidingThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='建筑'" }.ToTable();

                if (bulidingThreeData != null && bulidingThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingThreePercent = decimal.Parse(bulidingThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }

                decimal bulidingThreeCount = 0;

                sb_three.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingThreeCount = Math.Round(bulidingThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingThreeCount = Math.Floor(bulidingThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"9%\">" + bulidingThreeCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureThreePercent = 0;
                //看是否有结构人员参与
                var structureThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='结构'" }.ToTable();

                if (structureThreeData != null && structureThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureThreePercent = decimal.Parse(structureThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal structureThreeCount = 0;

                sb_three.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureThreeCount = Math.Round(structureThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureThreeCount = Math.Floor(structureThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"9%\">" + structureThreeCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainThreePercent = 0;
                //看是否有给排水人员参与
                var drainThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='给排水'" }.ToTable();

                if (drainThreeData != null && drainThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainThreePercent = decimal.Parse(drainThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal drainThreeCount = 0;

                sb_three.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainThreeCount = Math.Round(drainThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainThreeCount = Math.Floor(drainThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"9%\">" + drainThreeCount + "</td>");

                #endregion

                #region 电气

                //取得电气
                decimal electricThreePercent = 0;
                //看是否有电气人员参与
                var electricThreeData = new DataView(dataWorkDraw) { RowFilter = "Specialty='电气'" }.ToTable();

                if (electricThreeData != null && electricThreeData.Rows.Count > 0)
                {
                    if (sumWorkDrawPercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricThreePercent = decimal.Parse(electricThreeData.Rows[0][0].ToString()) + Convert.ToDecimal(sumThree / count);

                    }
                }
                decimal electricThreeCount = 0;

                sb_three.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricThreePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricThreeCount = Math.Round(electricThreePercent / 100 * WorkDrawPercentCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricThreeCount = Math.Floor(electricThreePercent / 100 * WorkDrawPercentCount);
                }

                sb_three.Append("<td width=\"9%\">" + electricThreeCount + "</td>");

                #endregion

                #region 合计
                decimal workDrawSumCount = bulidingThreeCount + structureThreeCount + drainThreeCount + electricThreeCount;

                decimal workDrawSumPercent = bulidingThreePercent + structureThreePercent + drainThreePercent + electricThreePercent;

                sb_three.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(workDrawSumPercent, 0) + "</span></td>");


                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_three.Append("<td width=\"9%\">" + Math.Round(workDrawSumCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_three.Append("<td width=\"9%\">" + Math.Floor(workDrawSumCount) + "</td>");
                }

                sb_three.Append("</tr>");
                #endregion
                //施工图 --施工图为2
                var dtWorkingProcessValue = new DataView(dtProcessValue) { RowFilter = "ItemType=2" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("建筑", dtWorkingProcessValue, bulidingThreeCount, 2));

                //取得结构工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("结构", dtWorkingProcessValue, structureThreeCount, 2));

                //取得给排水工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("给排水", dtWorkingProcessValue, drainThreeCount, 2));

                //取得电气工序的产值
                sb_ProcessThreeTable.Append(getSpeProcessValue("电气", dtWorkingProcessValue, electricThreeCount, 2));

                sb_ProcessThreeTable.Append("</table></div>");
            }
            #endregion

            #region 后期服务
            //第一行循环方案设计专业
            StringBuilder sb_four = new StringBuilder();
            StringBuilder sb_ProcessFourTable = new StringBuilder();
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                sb_four.Append("<tr>");
                sb_four.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">后期服务</td>");

                //后期服务
                var dataLateStage = new DataView(dtStageSpeValue).ToTable("LateStagePercent", false, "LateStagePercent", "Specialty");

                //后期服务金额
                decimal lastStageCount = string.IsNullOrEmpty(dtStageValue.Rows[0]["LateStageCOUNT"].ToString()) ? 0 : decimal.Parse(dtStageValue.Rows[0]["LateStageCOUNT"].ToString());

                //后期服务总百分比
                var sumLateStagePercent = dataLateStage.AsEnumerable().Sum(s => s.Field<decimal>("LateStagePercent"));


                //后期服务工序

                sb_ProcessFourTable.Append("<div id=\"designProcessFourTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb_ProcessFourTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  后期服务工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td> </tr> </table>");
                sb_ProcessFourTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");

                sb_ProcessFourTable.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%; text-align: center;\" colspan=\"2\">    审核   </td> ");
                sb_ProcessFourTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
                sb_ProcessFourTable.Append("</tr></table>");
                sb_ProcessFourTable.Append("<table id=\"gvdesignProcessFour\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

                var sumFour = 100 - sumLateStagePercent;

                #region 建筑

                //取得建筑
                decimal bulidingFourPercent = 0;
                //看是否有建筑人员参与
                var bulidingFourData = new DataView(dataLateStage) { RowFilter = "Specialty='建筑'" }.ToTable();

                if (bulidingFourData != null && bulidingFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        bulidingFourPercent = decimal.Parse(bulidingFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }

                decimal bulidingFourCount = 0;

                sb_four.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingFourCount = Math.Round(bulidingFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingFourCount = Math.Floor(bulidingFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"9%\">" + bulidingFourCount + "</td>");

                #endregion

                #region 结构

                //取得结构
                decimal structureFourPercent = 0;
                //看是否有结构人员参与
                var structureFourData = new DataView(dataLateStage) { RowFilter = "Specialty='结构'" }.ToTable();

                if (structureFourData != null && structureFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        structureFourPercent = decimal.Parse(structureFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal structureFourCount = 0;

                sb_four.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureFourCount = Math.Round(structureFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureFourCount = Math.Floor(structureFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"9%\">" + structureFourCount + "</td>");

                #endregion

                #region 给排水

                //取得给排水
                decimal drainFourPercent = 0;
                //看是否有给排水人员参与
                var drainFourData = new DataView(dataLateStage) { RowFilter = "Specialty='给排水'" }.ToTable();

                if (drainFourData != null && drainFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        drainFourPercent = decimal.Parse(drainFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal drainFourCount = 0;

                sb_four.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainFourCount = Math.Round(drainFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainFourCount = Math.Floor(drainFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"9%\">" + drainFourCount + "</td>");

                #endregion

                #region 电气

                //取得电气
                decimal electricFourPercent = 0;
                //看是否有电气人员参与
                var electricFourData = new DataView(dataLateStage) { RowFilter = "Specialty='电气'" }.ToTable();

                if (electricFourData != null && electricFourData.Rows.Count > 0)
                {
                    if (sumLateStagePercent != 0 && count != 0)
                    {
                        //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                        electricFourPercent = decimal.Parse(electricFourData.Rows[0][0].ToString()) + Convert.ToDecimal(sumFour / count);

                    }
                }
                decimal electricFourCount = 0;

                sb_four.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricFourPercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricFourCount = Math.Round(electricFourPercent / 100 * lastStageCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricFourCount = Math.Floor(electricFourPercent / 100 * lastStageCount);
                }

                sb_four.Append("<td width=\"9%\">" + electricFourCount + "</td>");

                #endregion

                #region 合计
                decimal lastSumCount = bulidingFourCount + structureFourCount + drainFourCount + electricFourCount;

                decimal lastSumPercent = bulidingFourPercent + structureFourPercent + drainFourPercent + electricFourPercent;

                sb_four.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(lastSumPercent, 0) + "</span></td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_four.Append("<td width=\"9%\">" + Math.Round(lastSumCount, MidpointRounding.AwayFromZero) + "</td>");
                }
                else //去整
                {
                    sb_four.Append("<td width=\"9%\">" + Math.Floor(lastSumCount) + "</td>");
                }

                sb_four.Append("</tr>");
                #endregion

                //后期--后期为3
                var dtLateStageProcessValue = new DataView(dtProcessValue) { RowFilter = "ItemType=3" }.ToTable();

                //取得建筑工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue1("建筑", dtLateStageProcessValue, bulidingFourCount, 3));

                //取得结构工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue1("结构", dtLateStageProcessValue, structureFourCount, 3));

                //取得给排水工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue1("给排水", dtLateStageProcessValue, drainFourCount, 3));

                //取得电气工序的产值
                sb_ProcessFourTable.Append(getSpeProcessValue1("电气", dtLateStageProcessValue, electricFourCount, 3));
                sb_ProcessFourTable.Append("</table></div>");
            }
            #endregion

            if (CoperationProcess == 0)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 1)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
            }

            else if (CoperationProcess == 2)
            {
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 3)
            {
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 10)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }

            sbSpeHead.Append("</table></div>");

            lbl_stagespetable.Text = sbSpeHead.ToString();

            StringBuilder strTab = new StringBuilder();
            strTab.Append(" <div id=\"tabs\" >  <ul style=\"width:98%; margin:auto;\"> ");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabs-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabs-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");
            }

            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabs-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabs-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
            }

            strTab.Append("</ul>");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append(" <div id=\"tabs-1\">");
                strTab.Append(sb_ProcessOneTable.ToString());
                strTab.Append(" </div>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append(" <div id=\"tabs-2\">");
                strTab.Append(sb_ProcessTwoTable.ToString());
                strTab.Append(" </div>");
            }

         
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                strTab.Append(" <div id=\"tabs-3\">");
                strTab.Append(sb_ProcessThreeTable.ToString());
                strTab.Append(" </div>");
                strTab.Append(" <div id=\"tabs-4\">");
                strTab.Append(sb_ProcessFourTable.ToString());
                strTab.Append(" </div>");
            }
            strTab.Append(" </div>");
            lbl_Tab.Text = strTab.ToString();

        }

        /// <summary>
        /// 得到各专业工序产值
        /// </summary>
        /// <param name="speName">专业名称</param>
        /// <param name="dtProcessValue">工序</param>
        /// <param name="speCount">专业产值</param>
        /// <returns>各专业工序产值</returns>
        private string getSpeProcessValue1(string speName, DataTable dtProcessValue, decimal speCount, int processID)
        {
            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
            StringBuilder sb_ProcessOneTable = new StringBuilder();

            //计算 该专业下 该审核角色是否有人员参与

            //看该专业审核 是否有人参与
            int countAudit = BLL.IsRolePlay(4, speName, proSysNo);

            //该专业 专业负责 是否有人参与
            int countSpecialtyHead = BLL.IsRolePlay(2, speName, proSysNo);

            // 该专业 校对 是否有人参与
            int countProofread = BLL.IsRolePlay(3, speName, proSysNo);

            //该专业 审核 是否有人参与
            int countDesign = BLL.IsDesignerPlay(proSysNo, speName);

            var programBulidingData = new DataView(dtProcessValue) { RowFilter = "Specialty='" + speName + "'" }.ToTable();
            // 审核比例
            decimal auditpercent = 0;
            decimal specialtyHeadPercent = 0;
            decimal proofreadPercent = 0;
            decimal designPercent = 0;
            if (programBulidingData != null && programBulidingData.Rows.Count > 0)
            {
                //审核比例
                auditpercent = string.IsNullOrEmpty(programBulidingData.Rows[0]["AuditPercent"].ToString()) ? 0 : decimal.Parse(programBulidingData.Rows[0]["AuditPercent"].ToString());

                //专业负责比例
                specialtyHeadPercent = string.IsNullOrEmpty(programBulidingData.Rows[0]["SpecialtyHeadPercent"].ToString()) ? 0 : decimal.Parse(programBulidingData.Rows[0]["SpecialtyHeadPercent"].ToString());

                //校对比例
                proofreadPercent = string.IsNullOrEmpty(programBulidingData.Rows[0]["ProofreadPercent"].ToString()) ? 0 : decimal.Parse(programBulidingData.Rows[0]["ProofreadPercent"].ToString());

                //设计比例
                designPercent = string.IsNullOrEmpty(programBulidingData.Rows[0]["DesignPercent"].ToString()) ? 0 : decimal.Parse(programBulidingData.Rows[0]["DesignPercent"].ToString());

            }


            decimal totalRoleCount = 0;
            int count = 0;
            if (countAudit > 0)
            {
                totalRoleCount = totalRoleCount + auditpercent;
                count = count + 1;
            }
            if (countSpecialtyHead > 0)
            {
                totalRoleCount = totalRoleCount + specialtyHeadPercent;
                count = count + 1;
            }

            if (countProofread > 0)
            {
                totalRoleCount = totalRoleCount + proofreadPercent;
                count = count + 1;
            }
            if (countDesign > 0)
            {
                totalRoleCount = totalRoleCount + designPercent;
                count = count + 1;
            }


            decimal otherTotal = 100 - totalRoleCount;

            sb_ProcessOneTable.Append("<tr>");
            sb_ProcessOneTable.Append("<td width= 10% class=\"cls_Column\"> " + speName + "</td>");

            //审核
            if (countAudit > 0)
            {
                if (totalRoleCount != 0 && count != 0)
                {
                    auditpercent = auditpercent + Convert.ToDecimal(otherTotal / count);
                }
            }
            else
            {
                auditpercent = 0;
            }


            decimal auditCount = auditpercent / 100 * speCount;
            //0 表示方案设计 3表示后期服务
            if (processID == 0 || processID == 3)
            {
                if (auditpercent == 0)
                {
                    sb_ProcessOneTable.Append("<td width= 9%> 0%</td>");
                    sb_ProcessOneTable.Append("<td width= 9%> 0</td>");
                }
                else
                {
                    auditpercent = Math.Round(auditpercent, 2);
                    sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"6\" type=\"text\"  value=" + auditpercent + " />%</td>");

                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(auditCount) + "</td>");
                    }
                    else
                    {
                        sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(auditCount) + "</td>");
                    }
                }
            }
            else
            {
                auditpercent = Math.Round(auditpercent, 2);
                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"6\" type=\"text\"  value=" + auditpercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(auditCount) + "</td>");
                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(auditCount) + "</td>");
                }
            }

            //专业负责
            if (countSpecialtyHead > 0)
            {
                if (totalRoleCount != 0 && count != 0)
                {
                    // specialtyHeadPercent = specialtyHeadPercent + specialtyHeadPercent * otherTotal / totalRoleCount;
                    specialtyHeadPercent = specialtyHeadPercent + Convert.ToDecimal(otherTotal / count);
                }
            }
            else
            {
                specialtyHeadPercent = 0;
            }


            decimal specialtyHeadCount = specialtyHeadPercent / 100 * speCount;

            //该专业没有专业负责人员参与
            if (processID == 0 || processID == 3)
            {
                if (specialtyHeadPercent == 0)
                {
                    sb_ProcessOneTable.Append("<td width= 9%> 0%</td>");
                    sb_ProcessOneTable.Append("<td width= 9%> 0</td>");
                }
                else
                {
                    specialtyHeadPercent = Math.Round(specialtyHeadPercent, 2);

                    sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + specialtyHeadPercent + " />%</td>");

                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(specialtyHeadCount, 0) + "</td>");

                    }
                    else
                    {
                        sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(specialtyHeadCount) + "</td>");
                    }
                }
            }
            else
            {
                specialtyHeadPercent = Math.Round(specialtyHeadPercent, 2);

                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + specialtyHeadPercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(specialtyHeadCount, 0) + "</td>");

                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(specialtyHeadCount) + "</td>");
                }
            }

            //校对
            if (countProofread > 0)
            {
                if (totalRoleCount != 0 && count != 0)
                {
                    proofreadPercent = proofreadPercent + Convert.ToDecimal(otherTotal / count);
                }
            }
            else
            {
                proofreadPercent = 0;
            }

            decimal proofreadCount = proofreadPercent / 100 * speCount;
            //没有校对人员参与
            if (processID == 0 || processID == 3)
            {
                if (proofreadPercent == 0)
                {
                    sb_ProcessOneTable.Append("<td width= 9%> 0%</td>");
                    sb_ProcessOneTable.Append("<td width= 9%> 0</td>");
                }
                else
                {
                    proofreadPercent = Math.Round(proofreadPercent, 2);

                    sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + proofreadPercent + " />%</td>");

                    //等于0是 四舍五入
                    if (IsRounding == "0")
                    {
                        sb_ProcessOneTable.Append("<td width= 9%> " + Math.Round(proofreadCount, 0) + "</td>");
                    }
                    else
                    {
                        sb_ProcessOneTable.Append("<td width= 9%> " + Math.Floor(proofreadCount) + "</td>");
                    }
                }
            }
            else
            {
                proofreadPercent = Math.Round(proofreadPercent, 2);

                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + proofreadPercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%> " + Math.Round(proofreadCount, 0) + "</td>");
                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%> " + Math.Floor(proofreadCount) + "</td>");
                }
            }


            //设计
            if (countDesign > 0)
            {
                if (totalRoleCount != 0 && count != 0)
                {
                    // designPercent = designPercent + designPercent * otherTotal / totalRoleCount;
                    designPercent = designPercent + Convert.ToDecimal(otherTotal / count);
                }
            }
            else
            {
                designPercent = 0;
            }


            decimal designCount = designPercent / 100 * speCount;
            //没有设计人员参与
            if (processID == 0 || processID == 3)
            {
                //if (designPercent == 0)
                //{
                //    sb_ProcessOneTable.Append("<td width= 9%> 0%</td>");
                //    sb_ProcessOneTable.Append("<td width= 9%> 0</td>");
                //}
                //else
                //{
                designPercent = Math.Round(designPercent, 2);

                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + designPercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(designCount, 0) + "</td>");
                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(designCount) + "</td>");
                }
                // }
            }
            else
            {
                designPercent = Math.Round(designPercent, 2);

                sb_ProcessOneTable.Append("<td width= 9%> <input  maxlength=\"15\" type=\"text\"  value=" + designPercent + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Round(designCount, 0) + "</td>");
                }
                else
                {
                    sb_ProcessOneTable.Append("<td width= 9%>" + Math.Floor(designCount) + "</td>");
                }
            }

            decimal totalPercent = auditpercent + specialtyHeadPercent + proofreadPercent + designPercent;
            //decimal 
            if (totalPercent == 0)
            {
                sb_ProcessOneTable.Append("<td width= 9%> <span>0</span>%</td>");
            }
            else
            {
                sb_ProcessOneTable.Append("<td width= 9%  style=\"background-color:#D2F3CB\"> <span>" + Math.Round(totalPercent, 0) + "</span>%</td>");
            }

            decimal countTotal = specialtyHeadCount + auditCount + proofreadCount + designCount;

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                sb_ProcessOneTable.Append("<td width= 9%> " + Math.Round(countTotal, 0) + "</td>");
            }
            else
            {
                sb_ProcessOneTable.Append("<td width= 9%> " + Math.Floor(countTotal) + "</td>");

            }
            sb_ProcessOneTable.Append("</tr>");

            return sb_ProcessOneTable.ToString();

        }


        /// <summary>
        /// 生成表四之后的表单
        /// </summary>
        /// <param name="dtSpe">专业</param>
        /// <param name="dtProcess">工序</param>
        private void CreateOtherTable1(DataTable dtSpe, DataTable dtProcess, decimal allotCount)
        {
            //取得专业
            var dtSpecialtyList = new DataView(dtSpe).ToTable("Specialty", true, "Specialty");

            int count = dtSpecialtyList.Rows.Count;

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();
            sbSpeHead.Append(" <div id=\"stagetabletfive\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> ");

            if (CoperationProcess == 4)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  室外工程产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 5)
            {
                sbSpeHead.Append("<tr><td align=\"center\">  锅炉房产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 6)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  地上单建水泵房产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 7)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  地上单建变配所(室)产值分配比例% </td></tr>");
            }
            else if (CoperationProcess == 8)
            {
                sbSpeHead.Append("<tr><td  align=\"center\">  单建地下室（车库）产值分配比例%</td></tr>");
            }
            else if (CoperationProcess == 9)
            {
                sbSpeHead.Append("<tr><td align=\"center\">  市政道路工程产值分配比例% </td></tr>");
            }

            //比例
            decimal percent = decimal.Divide(92, count * 2);
            percent = decimal.Round(percent, 2);
            sbSpeHead.Append("</table>");
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 专业</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">合计</td>");

            sbSpeHead.Append("</tr></table>");

            sbSpeHead.Append("<table id=\"gvFive\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\"  >");


            //
            StringBuilder sb_one = new StringBuilder();
            sb_one.Append("<tr>");
            sb_one.Append("<td class=\"cls_Column\" style=\"width:10%\">比例</td>");

            //计算方案设计总百分比
            var sumProgramPercent = dtSpe.AsEnumerable().Sum(s => s.Field<decimal>("SpecialtyPercent"));


            decimal sumOther = 100 - sumProgramPercent;

            #region 建筑

            //取得建筑
            decimal bulidingOnePercent = 0;
            //看是否有建筑人员参与
            var bulidingOneData = new DataView(dtSpe) { RowFilter = "Specialty='建筑'" }.ToTable();

            if (bulidingOneData != null && bulidingOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    bulidingOnePercent = decimal.Parse(bulidingOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }

            decimal bulidingOneCount = 0;
            if (bulidingOnePercent == 0)
            {
                sb_one.Append("<td width=\"9%\">0</td>");
                sb_one.Append("<td width=\"9%\">0</td>");
            }
            else
            {
                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(bulidingOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    bulidingOneCount = Math.Round(bulidingOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    bulidingOneCount = Math.Floor(bulidingOnePercent / 100 * allotCount);
                }

                sb_one.Append("<td width=\"9%\">" + bulidingOneCount + "</td>");
            }
            #endregion

            #region 结构

            //取得结构
            decimal structureOnePercent = 0;
            //看是否有结构人员参与
            var structureOneData = new DataView(dtSpe) { RowFilter = "Specialty='结构'" }.ToTable();

            if (structureOneData != null && structureOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    structureOnePercent = decimal.Parse(structureOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal structureOneCount = 0;
            if (structureOnePercent == 0)
            {
                sb_one.Append("<td width=\"9%\">0</td>");
                sb_one.Append("<td width=\"9%\">0</td>");
            }
            else
            {
                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(structureOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    structureOneCount = Math.Round(structureOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    structureOneCount = Math.Floor(structureOnePercent / 100 * allotCount);
                }

                sb_one.Append("<td width=\"9%\">" + structureOneCount + "</td>");
            }
            #endregion

            #region 给排水

            //取得给排水
            decimal drainOnePercent = 0;
            //看是否有给排水人员参与
            var drainOneData = new DataView(dtSpe) { RowFilter = "Specialty='给排水'" }.ToTable();

            if (drainOneData != null && drainOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    drainOnePercent = decimal.Parse(drainOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal drainOneCount = 0;
            if (drainOnePercent == 0)
            {
                sb_one.Append("<td width=\"9%\">0</td>");
                sb_one.Append("<td width=\"9%\">0</td>");
            }
            else
            {
                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(drainOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    drainOneCount = Math.Round(drainOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    drainOneCount = Math.Floor(drainOnePercent / 100 * allotCount);
                }

                sb_one.Append("<td width=\"9%\">" + drainOneCount + "</td>");
            }
            #endregion

            #region 电气

            //取得电气
            decimal electricOnePercent = 0;
            //看是否有电气人员参与
            var electricOneData = new DataView(dtSpe) { RowFilter = "Specialty='电气'" }.ToTable();

            if (electricOneData != null && electricOneData.Rows.Count > 0)
            {
                if (sumProgramPercent != 0 && count != 0)
                {
                    //programPercent = programPercent + sumOther * programPercent / sumProgramPercent;
                    electricOnePercent = decimal.Parse(electricOneData.Rows[0]["SpecialtyPercent"].ToString()) + Convert.ToDecimal(sumOther / count);

                }
            }
            decimal electricOneCount = 0;
            if (electricOnePercent == 0)
            {
                sb_one.Append("<td width=\"9%\">0</td>");
                sb_one.Append("<td width=\"9%\">0</td>");
            }
            else
            {
                sb_one.Append("<td width=\"9%\"> <input  maxlength=\"6\" type=\"text\" runat=\"server\" value=" + Math.Round(electricOnePercent, 2).ToString() + " />%</td>");

                //等于0是 四舍五入
                if (IsRounding == "0")
                {
                    electricOneCount = Math.Round(electricOnePercent / 100 * allotCount, MidpointRounding.AwayFromZero);
                }
                else //去整
                {
                    electricOneCount = Math.Floor(electricOnePercent / 100 * allotCount);
                }

                sb_one.Append("<td width=\"9%\">" + electricOneCount + "</td>");
            }
            #endregion

            #region 合计
            decimal programCount = bulidingOneCount + structureOneCount + drainOneCount + electricOneCount;

            decimal programPercent = bulidingOnePercent + structureOnePercent + drainOnePercent + electricOnePercent;

            sb_one.Append("<td width=\"9%\" style=\"background-color:#D2F3CB\"> <span>" + Math.Round(programPercent, 0) + "</span></td>");

            //等于0是 四舍五入
            if (IsRounding == "0")
            {
                sb_one.Append("<td width=\"9%\">" + Math.Round(programCount, MidpointRounding.AwayFromZero) + "</td>");
            }
            else //去整
            {
                sb_one.Append("<td width=\"9%\">" + Math.Floor(programCount) + "</td>");
            }

            sb_one.Append("</tr>");
            #endregion


            StringBuilder sb_ProcessOneTable = new StringBuilder();
            sb_ProcessOneTable.Append(" <div id=\"tbOutDoor\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> ");


            if (CoperationProcess == 4)
            {
                sb_ProcessOneTable.Append("<tr>  <td  align=\"center\" >    室外工程工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td></tr>");
            }
            else if (CoperationProcess == 5)
            {
                sb_ProcessOneTable.Append("<tr>  <td   align=\"center\" >   锅炉房工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td></tr>");
            }
            else if (CoperationProcess == 6)
            {
                sb_ProcessOneTable.Append("<tr>  <td  align=\"center\" >    地上单建水泵房工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td></tr>");
            }
            else if (CoperationProcess == 7)
            {
                sb_ProcessOneTable.Append("<tr>   <td   align=\"center\" >    地上单建变配所(室)工序产值分配比例% <span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span></td></tr>");
            }
            else if (CoperationProcess == 8)
            {
                sb_ProcessOneTable.Append("<tr>   <td   align=\"center\" >    单建地下室（车库）工序产值分配比例%<span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span> </td></tr>");
            }
            else if (CoperationProcess == 9)
            {
                sb_ProcessOneTable.Append("<tr>   <td  align=\"center\" >  市政道路工程工序产值分配比例%<span style=\"color:Red;\">(各设计阶段及各专业间产值分配系数应严格按照院工程项目产值分配细则执行) </span> </td></tr>");
            }
            sb_ProcessOneTable.Append("</table>");
            sb_ProcessOneTable.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sb_ProcessOneTable.Append(" <tr> <td style=\"width: 10%; text-align: center\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
            sb_ProcessOneTable.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
            sb_ProcessOneTable.Append("</table>");

            sb_ProcessOneTable.Append("<table id=\"gvdesignProcessFive\" width=\"98%\"  class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\" >");

            //取得室外工程的工序
            var dtProcessValue = new DataView(dtProcess) { RowFilter = "ItemType=4" }.ToTable();

            //取得建筑工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("建筑", dtProcessValue, bulidingOneCount, CoperationProcess));

            //取得结构工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("结构", dtProcessValue, structureOneCount, CoperationProcess));

            //取得给排水工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("给排水", dtProcessValue, drainOneCount, CoperationProcess));

            //取得电气工序的产值
            sb_ProcessOneTable.Append(getSpeProcessValue("电气", dtProcessValue, electricOneCount, CoperationProcess));

            sb_ProcessOneTable.Append("</table></div>");

            sbSpeHead.Append(sb_one.ToString());
            sbSpeHead.Append("</table></div>");
            lbl_StageFive.Text = sbSpeHead.ToString();
            lbl_out.Text = sb_ProcessOneTable.ToString();
        }

        /// <summary>
        /// 创建人员的表单
        /// </summary>
        private void CreateDataTableByMember1()
        {
            if (ProjectValueAllot != null)
            {
                TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
                DataSet ds = new TG.BLL.cm_TranProjectValueAllot().GetDistinctTranProAuditedUser(proSysNo, "tjs", allotID);

                DataTable dtmember = ds.Tables[0];

                if (dtmember.Rows.Count > 0)
                {
                    StringBuilder sbTotal = new StringBuilder();

                    //方案
                    StringBuilder sbOne = new StringBuilder();
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                    {

                        sbOne.Append(getMemberValue1(dtmember, 0));
                    }
                    //初步
                    StringBuilder sbTwo = new StringBuilder();
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                    {
                        sbTwo.Append(getMemberValue1(dtmember, 1));
                    }

                    //施工图
                    StringBuilder sbThree = new StringBuilder();
                    StringBuilder sbFour = new StringBuilder();
                    if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        sbThree.Append(getMemberValue1(dtmember, 2));
                        //后期                 
                        sbFour.Append(getMemberValue1(dtmember, 3));
                    }
                    StringBuilder strTab = new StringBuilder();
                    StringBuilder sbOut = new StringBuilder();
                    //室外工程
                    if (CoperationProcess >= 4 &&  CoperationProcess != 10)
                    {
                        sbOut.Append(getMemberValue1(dtmember, CoperationProcess));
                    }
                    else
                    {
                        strTab.Append(sbTotal.ToString());
                    }
                    strTab.Append("<fieldset id=\"memField\" ><legend>人员产值填写</legend>");
                    strTab.Append(" <div id=\"tabsMem\">  <ul style=\"width:98%; margin:auto;\"> ");
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                    {
                        strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabsMem-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
                    }
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3 )
                    {
                        strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMem-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");

                    }
                    if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMem-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                        strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMem-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
                    }
                    strTab.Append(" </ul>");
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                    {
                        strTab.Append("<div id=\"tabsMem-1\">");
                        strTab.Append(sbOne.ToString());
                        strTab.Append(" </div>");
                    }
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                    {
                        strTab.Append(" <div id=\"tabsMem-2\">");
                        strTab.Append(sbTwo.ToString());
                        strTab.Append(" </div>");
                    }

                    strTab.Append(" <div id=\"tabsMem-3\">");
                    strTab.Append(sbThree.ToString());
                    strTab.Append(" </div>");
                    if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        strTab.Append(" <div id=\"tabsMem-4\">");
                        strTab.Append(sbFour.ToString());
                        strTab.Append(" </div>");
                    }
                    strTab.Append(" </div>");
                    strTab.Append("</fieldset>");
                    if (CoperationProcess >= 4 && CoperationProcess != 10)
                    {
                        lblMember.Text = sbOut.ToString();
                    }
                    else
                    {
                        lblMember.Text = strTab.ToString();
                    }
                }
                else
                {
                    string noData = "<table style=\"100%\" id=\"tdNoMember\"><tr><td colspan=\"10\" style=\"color: Red; \">没有人员信息</td></tr></table>";
                    lblMember.Text = noData;
                }
            }
        }

        /// <summary>
        /// 得到人员产值
        /// </summary>
        /// <param name="dtProcess">阶段金额</param>
        /// <param name="speName">专业</param>
        /// <param name="dtMember">人员</param>
        /// <param name="process">阶段</param>
        /// <returns></returns>
        private string getMemberValue1(DataTable dtMember, int process)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" <div id=\"tbProjectValueBymember\" class=\"cls_Container_Report\" >");
            sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"><tr> ");

            //取得设计人员的专业
            var dtSpecialtyList = new DataView(dtMember).ToTable("Specialty", true, "spe_Name");

            if (process == 0)
            {
                sb.Append("<td style=\"text-align: center;\"> 方案设计-策划人员产值分配比例% </td> </tr>");
            }
            else if (process == 1)
            {
                sb.Append("<td  style=\"text-align: center;\"> 初步设计-策划人员产值分配比例% </td> </tr>");
            }
            else if (process == 2)
            {
                sb.Append("<td  style=\"text-align: center;\"> 施工图设计-策划人员产值分配比例% </td> </tr>");
            }
            else if (process == 3)
            {
                sb.Append("<td  style=\"text-align: center;\"> 后期服务-策划人员产值分配比例% </td> </tr>");
            }
            else if (process > 3)
            {
                sb.Append("<td  style=\"text-align: center;\"> 策划人员产值分配比例% </td> </tr>");
            }
            sb.Append("</table>");
            sb.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sb.Append("<tr ><td style=\"width: 10%; text-align: center;height:15px;\">  名称</td><td style=\"width: 10%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sb.Append(" <tr> <td style=\"width: 10%; text-align: center;\" > 专业 </td><td style=\"width: 10%; text-align: center;\">   姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
            sb.Append(" </table>");

            if (process == 0)
            {
                sb.Append("<table id=\"gvProjectValueOneBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\"  class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
            }
            else if (process == 1)
            {
                sb.Append("<table id=\"gvProjectValueTwoBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
            }
            else if (process == 2)
            {
                sb.Append("<table id=\"gvProjectValueThreeBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
            }
            else if (process == 3)
            {
                sb.Append("<table id=\"gvProjectValueFourBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
            }
            else
            {
                sb.Append("<table id=\"gvProjectValueBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
            }

            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                var dv = new DataView(dtMember) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();

                sb.Append("<tr>");
                sb.Append("<td  width= \"10%\" class=\"cls_Column\" rowspan=\"" + dtMemberBySpecialty.Rows.Count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");


                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td class=\"display\"></td>");
                    }
                    string name = dtMemberBySpecialty.Rows[j]["mem_Name"].ToString();

                    sb.Append("<td class=\"display\" wp=\"" + dtMemberBySpecialty.Rows[j]["IsExternal"].ToString() + "\">" + dtMemberBySpecialty.Rows[j]["mem_ID"].ToString() + "</td>");
                    sb.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");

                    //该人员是否是设计人员

                    if (process == 0 || process == 3)
                    {
                        sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" runat=\"server\" roles=\"sj\" spe=" + dtSpecialtyList.Rows[i]["spe_Name"] + " sz=0  />%</td>");
                        sb.Append("<td width= \"10%\"></td>");
                    }
                    else
                    {
                        sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" runat=\"server\" roles=\"sj\" spe=" + dtSpecialtyList.Rows[i]["spe_Name"] + " sz=0  />%</td>");
                        sb.Append("<td width= \"10%\"></td>");
                    }

                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\" roles=\"fz\" spe=" + dtSpecialtyList.Rows[i]["spe_Name"] + " runat=\"server\" />%</td>");
                    sb.Append("<td width= \"10%\"></td>");


                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\" roles=\"sh\" spe=" + dtSpecialtyList.Rows[i]["spe_Name"] + "  runat=\"server\" />%</td>");
                    sb.Append("<td width= \"10%\"></td>");

                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\" roles=\"jd\" spe=" + dtSpecialtyList.Rows[i]["spe_Name"] + " runat=\"server\"  sz=0  />%</td>");
                    sb.Append("<td width= \"10%\"></td>");

                    sb.Append("<td class=\"display\" >" + process + "</td>");
                    sb.Append("</tr>");
                }
            }
            sb.Append("</table>");
            sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\" id=\"tbChooseUser\"><tr> ");
            if (process == 0)
            {
                sb.Append("<td><span id=\"chooseOneUser\" style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseExternalOneUser\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
            }
            else if (process == 1)
            {
                sb.Append("<td><span id=\"chooseTwoUser\"  style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseTwoExternalUser\" style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
            }
            else if (process == 2)
            {
                sb.Append("<td><span id=\"chooseThreeUser\"  style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseThreeExternalUser\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
            }
            else if (process == 3)
            {
                sb.Append("<td><span id=\"chooseFourUser\"  style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseFourExternalUser\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
            }
            else
            {
                sb.Append("<td><span id=\"chooseUser\" style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseExternalUser\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
            }
            sb.Append("</tr></table>");
            sb.Append(" </div>");

            return sb.ToString();
        }


        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
    }
}