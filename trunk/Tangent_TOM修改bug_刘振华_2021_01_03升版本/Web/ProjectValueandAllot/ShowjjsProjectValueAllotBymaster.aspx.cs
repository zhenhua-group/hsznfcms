﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Configuration;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ShowjjsProjectValueAllotBymaster : PageBase
    {

        #region QueryString
        /// <summary>
        /// 项目系统编号
        /// </summary>
        public int proSysNo
        {
            get
            {
                int proSysNo = 0;
                int.TryParse(Request["proid"], out proSysNo);
                return proSysNo;
            }
        }
        /// <summary>
        /// 消息状态
        /// </summary>
        public string MessageStatus
        {
            get
            {
                return Request["MessageStatus"];
            }
        }
        /// <summary>
        /// 项目转过来的
        /// </summary>
        public string Prolink
        {
            get
            {
                return Request["prolink"];
            }
        }
        /// <summary>
        /// 审核记录系统编号
        /// </summary>
        public int ValueAllotAuditSysNo
        {
            get
            {
                int valueAllotAuditSysNo = 0;
                int.TryParse(Request["ValueAllotAuditSysNo"], out valueAllotAuditSysNo);
                return valueAllotAuditSysNo;
            }
        }

        public string Type
        {
            get
            {
                return Request["type"];
            }
        }


        /// <summary>
        /// 审核记录实体
        /// </summary>
        public TG.Model.cm_ProjectValueAuditRecord projectAuditRecordEntity { get; set; }


        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }
        //是否全部转出
        public string IsTAllPass { get; set; }
        /// <summary>
        /// 是否四舍五入
        /// </summary>
        public string IsRounding = ConfigurationManager.AppSettings["IsRounding"] ?? "0";

        public string AuditUser { get; set; }

        public string AuditDate { get; set; }
        public TG.Model.cm_ProjectValueAllot ProjectValueAllot { get; set; }

        public string Stage { get; set; }
        public string year = "";

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetAuditRecord();

                //获取项目信息
                getProInfo();

                //得到分配信息
                GetProjectValueAllotInfo();

                //得出借出金额
                GetIsLoadValueInfo();

                //项目分配信息
                GetConperationBaseInfo();
                //得到人员明细
                getMemberEdit();

                //得到人员信息
                getProcessDetailShow();

            }
        }

        /// <summary>
        /// 得到审核记录信息
        /// </summary>
        private void GetAuditRecord()
        {
            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();

            projectAuditRecordEntity = BLL.GetModel(ValueAllotAuditSysNo);
            if (projectAuditRecordEntity != null)
            {
                TG.Model.tg_member user = new TG.Model.tg_member();
                //查询联系人姓名
                if (!string.IsNullOrEmpty(projectAuditRecordEntity.AuditUser))
                {
                    string[] usrList = projectAuditRecordEntity.AuditUser.Split(',');
                    string[] datelist = projectAuditRecordEntity.AuditDate.Split(',');
                    if (usrList.Length >= 3 && datelist.Length >= 3)
                    {
                        if (usrList[2] != null && datelist[2] != null)
                        {
                            user = new TG.BLL.tg_member().GetModel(int.Parse(usrList[2]));
                            AuditUser = user == null ? "" : user.mem_Name;
                            AuditDate = datelist[2];
                        }
                    }
                }
            }
            else
            {
                lblNotData.Text = "<table id=\"tbdelete\" align=\"center\"><tr><td style=\" font-size: 12px;font-weight: bold; color: Red;\"  >此信息已被删除!</td></tr></table>";
            }
        }

        /// <summary>
        /// 得到分配信息
        /// </summary>
        private void GetProjectValueAllotInfo()
        {
            TG.BLL.cm_ProjectValueAllot bll = new BLL.cm_ProjectValueAllot();
            if (projectAuditRecordEntity != null)
            {
                ProjectValueAllot = bll.GetModel(projectAuditRecordEntity.AllotID);
                if (ProjectValueAllot != null)
                {
                    lblStage.Text = getTypeDetail(ProjectValueAllot.Itemtype.ToString());
                    lbl_PaidValueCount.Text = ProjectValueAllot.PaidValueCount.ToString();
                    lblAllotAmount.Text = ProjectValueAllot.AllotCount.ToString();
                    txtTheDeptValuePercent.Text = ProjectValueAllot.Thedeptallotpercent.ToString();
                    txtTheDeptValueCount.Text = ProjectValueAllot.Thedeptallotcount.ToString();
                    txt_ProgramPercent.Text = ProjectValueAllot.ProgramPercent.ToString();
                    txt_ProgramCount.Text = ProjectValueAllot.ProgramCount.ToString();
                    txt_DesignManagerPercent.Text = ProjectValueAllot.DesignManagerPercent.ToString();
                    lbl_DesignManagerPercent.Text = ProjectValueAllot.DesignManagerPercent.ToString();
                    txt_DesignManagerCount.Text = ProjectValueAllot.DesignManagerCount.ToString();
                    lbl_DesignManagerCount.Text = ProjectValueAllot.DesignManagerCount.ToString();
                    txt_ShouldBeValuePercent.Text = ProjectValueAllot.ShouldBeValuePercent.ToString();
                    txt_ShouldBeValueCount.Text = ProjectValueAllot.ShouldBeValueCount.ToString();
                    lblTranBulidingPercent.Text = ProjectValueAllot.TranBulidingPercent.ToString();
                    lblTranBulidingCount.Text = ProjectValueAllot.TranBulidingCount.ToString();
                    lblFinanceValuePercent.Text = ProjectValueAllot.FinanceValuePercent.ToString();
                    lblFinanceValueCount.Text = ProjectValueAllot.FinanceValueCount.ToString();
                    lblTheDeptShouldValuePercent.Text = ProjectValueAllot.TheDeptShouldValuePercent.ToString();
                    lblTheDeptShouldValueCount.Text = ProjectValueAllot.TheDeptShouldValueCount.ToString();
                    lblDividedPercent.Text = ProjectValueAllot.DividedPercent.ToString();
                    lblYear.Text = ProjectValueAllot.ActualAllountTime;
                    year = ProjectValueAllot.ActualAllountTime;
                    //查询
                    if (ProjectValueAllot.Itemtype == 25)
                    {
                        int unitId = ProjectValueAllot.UnitId == null ? 0 : int.Parse(ProjectValueAllot.UnitId.ToString());
                        TG.Model.tg_unit unit = new TG.BLL.tg_unit().GetModel(unitId);
                        if (unit != null)
                        {
                            lblName.Text = unit.unit_Name;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 得到借出金额
        /// </summary>
        private void GetIsLoadValueInfo()
        {
            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
            DataTable dt = BLL.GetAllotDataList(projectAuditRecordEntity.AllotID.ToString()).Tables[0];

            if (dt != null && dt.Rows.Count > 0)
            {
                //借出
                if (!string.IsNullOrEmpty(dt.Rows[0]["LoanValueCount"].ToString()))
                {
                    txt_loanCount.Value = dt.Rows[0]["LoanValueCount"].ToString();
                    lblLoanAcount.Text = dt.Rows[0]["LoanValueCount"].ToString();
                }

                if (!string.IsNullOrEmpty(dt.Rows[0]["ActualAllotCount"].ToString()))
                {
                    txt_ActulCount.Text = dt.Rows[0]["ActualAllotCount"].ToString();
                    lblActalAcount.Text = dt.Rows[0]["ActualAllotCount"].ToString();
                }
                else
                {
                    txt_ActulCount.Text = dt.Rows[0]["ShouldBeValueCount"].ToString();
                }

                //借入金额
                if (!string.IsNullOrEmpty(dt.Rows[0]["BorrowValueCount"].ToString()))
                {
                    txt_BorrowCount.Value = dt.Rows[0]["BorrowValueCount"].ToString();
                    lblBorrowAccount.Text = dt.Rows[0]["BorrowValueCount"].ToString();
                }

                //可用借入金额
                if (!string.IsNullOrEmpty(dt.Rows[0]["totalLoanValueCount"].ToString()))
                {
                    txt_ActulloanCount.Text = dt.Rows[0]["totalLoanValueCount"].ToString();
                    lblActalLoanAcount.Text = dt.Rows[0]["totalLoanValueCount"].ToString();
                }

                //是否全部借出
                if (!string.IsNullOrEmpty(dt.Rows[0]["IsTAllPass"].ToString()))
                {
                    radio_yes.Attributes.Add("disabled", "disabled");
                    radio_no.Attributes.Add("disabled", "disabled");
                    string isTAllPass = dt.Rows[0]["IsTAllPass"].ToString();
                    if (isTAllPass == "1")
                    {
                        lbl_IsTAllPass.Text = "是";

                        radio_yes.Checked = true;
                        radio_no.Checked = false;
                        IsTAllPass = "1";
                    }
                    else
                    {
                        lbl_IsTAllPass.Text = "否";
                        IsTAllPass = "0";
                        radio_yes.Checked = false;
                        radio_no.Checked = true;


                    }
                }
            }
        }

        /// <summary>
        /// 项目信息
        /// </summary>
        private void getProInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(proSysNo);

            if (pro_model != null)
            {
                //项目名称
                lblProjectName.Text = pro_model.pro_name;
                //关联合同
                this.lblReletive.Text = pro_model.Project_reletive;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }

                //建设规模
                this.lblScale.Text = pro_model.ProjectScale.ToString();
                //建筑类别
                lblBuildType.Text = pro_model.BuildType;
                //项目总负责
                this.lblPMName.Text = pro_model.PMName;
                //承接部门
                lblcpr_Unit.Text = pro_model.Unit;

                //项目阶段

                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }

                //合同额
                lblCoperationAmount.Text = pro_model.Cpr_Acount.ToString();

                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }
            }

        }

        /// <summary>
        /// 得到项目基本信息
        /// </summary>
        private void GetConperationBaseInfo()
        {
            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();

            DataTable dt = BLL.GetComList(proSysNo.ToString(), year).Tables[0];

            if (dt != null && dt.Rows.Count > 0)
            {

                //实收金额
                if (dt.Rows[0]["PayShiCount"] != null)
                {
                    lblPayShiCount.Text = dt.Rows[0]["PayShiCount"].ToString();
                }

                //分配金额
                if (dt.Rows[0]["AllotCount"] != null)
                {
                    lblAllotAccount.Text = dt.Rows[0]["AllotCount"].ToString();
                }

                // 未分配金额
                if (dt.Rows[0]["PayShiCount"] != null && dt.Rows[0]["AllotCount"] != null)
                {
                    lblNotAllotAccount.Text = Convert.ToString(decimal.Parse(dt.Rows[0]["PayShiCount"].ToString()) - decimal.Parse(dt.Rows[0]["AllotCount"].ToString()));
                }

            }

        }


        /// <summary>
        /// 得到人员信息
        /// </summary>
        private void getMemberEdit()
        {
            if (ProjectValueAllot != null)
            {
                if (projectAuditRecordEntity.Status == "A" || projectAuditRecordEntity.Status == "C")
                {
                    //绑定人员信息
                    string itemStatus = ProjectValueAllot.Itemtype.ToString();

                    TG.BLL.cm_jjsProjectValueAllotConfig bll = new TG.BLL.cm_jjsProjectValueAllotConfig();
                    TG.Model.cm_jjsProjectValueAllotConfig model = bll.GetModelByType(int.Parse(itemStatus));

                    StringBuilder sbHead = new StringBuilder();
                    sbHead.Append("<fieldset id=\"memField\" style=\"dispaly:none\" ><legend style=\"font-size:12px;\">经济所人员产值填写</legend>");
                    sbHead.Append(" <div id=\"tbProjectValueProcess\" class=\"cls_Container_Report\" >");
                    sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  " + getTypeDetail(itemStatus) + "-配置比例% </td> </tr> </table>");
                    sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                    sbHead.Append(" <tr><td rowspan=\"3\" width=\"18%\">项目分类 </td> <td colspan=\"8\" width=\"72%\">编制 </td> <td width=\"10%\" rowspan=\"3\"> 校对  </td> </tr>");
                    sbHead.Append("  <tr>  <td rowspan=\"2\" width=\"9%\">编制 <br/>合计 </td> <td colspan=\"3\" width=\"27%\"> 建筑</td> <td colspan=\"4\" width=\"36%\">  安装 </td> </tr>");
                    sbHead.Append("  <tr> <td width=\"9%\"> 建筑<br />  合计  </td><td width=\"9%\">土建 </td> <td width=\"9%\">  结构</td>");
                    sbHead.Append("<td width=\"9%\">  安装<br /> 合计 </td><td width=\"9%\"> 给排水 </td> <td width=\"9%\">  采暖</td> <td width=\"9%\"> 电气 </td> </tr>");
                    sbHead.Append("<table id=\"gvProjectValueProcess\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label2\" >");

                    decimal borrowCount = string.IsNullOrEmpty(txt_BorrowCount.Value) ? 0 : decimal.Parse(txt_BorrowCount.Value);
                    decimal loanCount = string.IsNullOrEmpty(txt_loanCount.Value) ? 0 : decimal.Parse(txt_loanCount.Value);
                    decimal shouldBeValueCount = string.IsNullOrEmpty(txt_ShouldBeValueCount.Text) ? 0 : decimal.Parse(txt_ShouldBeValueCount.Text);
                    decimal totalAllotValue = shouldBeValueCount + borrowCount - loanCount;

                    #region 状态判断
                    if (itemStatus == "11" || itemStatus == "12" || itemStatus == "18")
                    {
                        decimal proofreadPercent = model == null ? 0 : decimal.Parse(model.ProofreadPercent.ToString());
                        decimal bianzhiPercent = 100 - proofreadPercent;

                        //建筑
                        decimal buildingPercent = model == null ? 0 : decimal.Parse(model.BuildingPercent.ToString());
                        //结构
                        decimal structurePercent = model == null ? 0 : decimal.Parse(model.StructurePercent.ToString());
                        //建筑合计
                        decimal buildingTotalPercent = model == null ? 0 : decimal.Parse(model.Totalbuildingpercent.ToString());
                        //建筑
                        decimal drainPercent = model == null ? 0 : decimal.Parse(model.DrainPercent.ToString());
                        decimal havcPercent = model == null ? 0 : decimal.Parse(model.HavcPercent.ToString());
                        decimal electricPercent = model == null ? 0 : decimal.Parse(model.ElectricPercent.ToString());

                        decimal azTotal = model == null ? 0 : decimal.Parse(model.TotalInstallationpercent.ToString());

                        sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（%）</td>");
                        sbHead.Append("<td width=\"9%\">" + bianzhiPercent.ToString() + "%</td>");
                        sbHead.Append("<td width=\"9%\">" + buildingTotalPercent + "%</td>");
                        sbHead.Append("<td width=\"9%\">" + buildingPercent + "%</td>");
                        sbHead.Append("<td width=\"9%\">" + structurePercent + "%</td>");
                        sbHead.Append("<td width=\"9%\">" + azTotal.ToString() + "%</td>");
                        sbHead.Append("<td width=\"9%\">" + drainPercent.ToString() + "%</td>");
                        sbHead.Append("<td width=\"9%\">" + havcPercent.ToString() + "%</td>");
                        sbHead.Append("<td width=\"9%\">" + electricPercent.ToString() + "%</td>");
                        sbHead.Append("<td width=\"10%\"><input maxlength=\"15\" type=\"text\"  class=\"TextBoxBorde\" id=\"txt_jd\"  value=" + proofreadPercent.ToString() + ">%</td>");
                        sbHead.Append("</tr>");

                        sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（元）</td>");
                        sbHead.Append("<td width=\"9%\">" + (bianzhiPercent * totalAllotValue / 100).ToString("f2") + "</td>");
                        sbHead.Append("<td width=\"9%\">" + (buildingTotalPercent / 100 * bianzhiPercent / 100 * totalAllotValue).ToString("f2") + "</td>");
                        sbHead.Append("<td width=\"9%\">" + (buildingPercent / 100 * bianzhiPercent / 100 * totalAllotValue).ToString("f2") + "</td>");
                        sbHead.Append("<td width=\"9%\">" + (structurePercent / 100 * bianzhiPercent / 100 * totalAllotValue).ToString("f2") + "</td>");
                        sbHead.Append("<td width=\"9%\">" + (azTotal / 100 * bianzhiPercent / 100 * totalAllotValue).ToString("f2") + "</td>");

                        sbHead.Append("<td width=\"9%\">" + (drainPercent / 100 * bianzhiPercent / 100 * totalAllotValue).ToString("f2") + "</td>");
                        sbHead.Append("<td width=\"9%\">" + (havcPercent / 100 * bianzhiPercent / 100 * totalAllotValue).ToString("f2") + "</td>");
                        sbHead.Append("<td width=\"9%\">" + (electricPercent / 100 * bianzhiPercent / 100 * totalAllotValue).ToString("f2") + "</td>");
                        sbHead.Append("<td width=\"10%\">" + (proofreadPercent * totalAllotValue / 100).ToString("f2") + "</td>");
                        sbHead.Append("</tr>");
                        sbHead.Append("</table></div>");
                        sbHead.Append(getMemberDetailEdit(itemStatus));
                        sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\" id=\"tbChooseUser\"><tr> ");
                        sbHead.Append("<td><span id=\"chooseUser\"   data-toggle=\"modal\" href=\"#chooseUserMainDiv\" style=\"color: Blue; cursor: pointer;\">添加人员</span>&nbsp;&nbsp;<span id=\"chooseExternalUser\"  data-toggle=\"modal\" href=\"#chooseExtUserDiv\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                        sbHead.Append("</tr></table>");
                        sbHead.Append("</div>");
                        sbHead.Append("</fieldset>");

                        lblMemberEidt.Text = sbHead.ToString();

                    }
                    else if (itemStatus == "13" || itemStatus == "14" || itemStatus == "15" || itemStatus == "16" || itemStatus == "17" || itemStatus == "19" || itemStatus == "20" || itemStatus == "21" || itemStatus == "22")
                    {

                        decimal proofreadPercent = model == null ? 0 : decimal.Parse(model.ProofreadPercent.ToString());
                        decimal bianzhiPercent = 100 - proofreadPercent;
                        sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（%）</td>");
                        sbHead.Append("<td width=\"9%\">" + bianzhiPercent.ToString() + "%</td>");
                        sbHead.Append("<td width=\"27%\" colsapn=\"3\">" + model.Totalbuildingpercent + "%</td>");
                        sbHead.Append("<td width=\"36%\" colsapn=\"4\">" + model.TotalInstallationpercent + "%</td>");
                        sbHead.Append("<td width=\"10%\"><input maxlength=\"15\" type=\"text\"  class=\"TextBoxBorde\" id=\"txt_jd\"  value=" + proofreadPercent.ToString() + ">%</td>");
                        sbHead.Append("</tr>");
                        sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（元）</td>");
                        sbHead.Append("<td width=\"9%\">" + (bianzhiPercent * totalAllotValue / 100).ToString("f2") + "</td>");
                        sbHead.Append("<td width=\"27%\" colsapn=\"3\">" + (model.Totalbuildingpercent / 100 * bianzhiPercent / 100 * totalAllotValue).ToString("f2") + "</td>");
                        sbHead.Append("<td width=\"36%\" colsapn=\"4\">" + (model.TotalInstallationpercent / 100 * bianzhiPercent / 100 * totalAllotValue).ToString("f2") + "</td>");
                        sbHead.Append("<td width=\"10%\">" + (proofreadPercent * totalAllotValue / 100).ToString("f2") + "</td>");
                        sbHead.Append("</tr>");
                        sbHead.Append("</table></div>");
                        sbHead.Append(getMemberDetailEdit(itemStatus));
                        sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\" id=\"tbChooseUser\"><tr> ");
                        sbHead.Append("<td><span id=\"chooseUser\"data-toggle=\"modal\" href=\"#chooseUserMainDiv\" style=\"color: Blue; cursor: pointer;\">添加人员</span>&nbsp;&nbsp;<span id=\"chooseExternalUser\"    data-toggle=\"modal\" href=\"#chooseExtUserDiv\" style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                        sbHead.Append("</tr></table>");
                        sbHead.Append("</div>");
                        sbHead.Append("</fieldset>");


                    }
                    else
                    {

                        decimal proofreadPercent = model == null ? 0 : decimal.Parse(model.ProofreadPercent.ToString());
                        decimal bianzhiPercent = 100 - proofreadPercent;
                        sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（%）</td>");
                        sbHead.Append("<td width=\"9%\">" + bianzhiPercent.ToString() + "%</td>");

                        sbHead.Append("<td width=\"9%\">0%</td>");
                        sbHead.Append("<td width=\"9%\"><input  maxlength=\"15\" type=\"text\" id=\"txtBulidging\" runat=\"server\"  >%</td>");
                        sbHead.Append("<td width=\"9%\"><input  maxlength=\"15\" type=\"text\"  id=\"txtStructure\"  runat=\"server\"  >%</td>");
                        sbHead.Append("<td width=\"9%\">0%</td>");
                        sbHead.Append("<td width=\"9%\"><input  maxlength=\"15\" type=\"text\" id=\"txtDrain\" runat=\"server\"  >%</td>");
                        sbHead.Append("<td width=\"9%\"><input  maxlength=\"15\" type=\"text\"  id=\"txtHavc\"  runat=\"server\"  >%</td>");
                        sbHead.Append("<td width=\"9%\"><input  maxlength=\"15\" type=\"text\" id=\"txtElectric\" runat=\"server\"  >%</td>");
                        sbHead.Append("<td width=\"10%\"><input maxlength=\"15\" type=\"text\"  class=\"TextBoxBorde\" id=\"txt_jd\"  value=" + proofreadPercent.ToString() + ">%</td>");
                        sbHead.Append("</tr>");

                        sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（元）</td>");
                        sbHead.Append("<td width=\"9%\">" + bianzhiPercent * totalAllotValue / 100 + "</td>");
                        sbHead.Append("<td width=\"9%\">0</td>");
                        sbHead.Append("<td width=\"9%\">0</td>");
                        sbHead.Append("<td width=\"9%\">0</td>");
                        sbHead.Append("<td width=\"9%\">0</td>");
                        sbHead.Append("<td width=\"9%\">0</td>");
                        sbHead.Append("<td width=\"9%\">0</td>");
                        sbHead.Append("<td width=\"9%\">0</td>");
                        sbHead.Append("<td width=\"10%\">" + proofreadPercent * totalAllotValue / 100 + "</td>");
                        sbHead.Append("</tr>");
                        sbHead.Append("</table></div>");
                        sbHead.Append(getMemberDetailEdit(itemStatus));
                        sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\" id=\"tbChooseUser\"><tr> ");
                        sbHead.Append("<td><span id=\"chooseUser\" data-toggle=\"modal\" href=\"#chooseUserMainDiv\"style=\"color: Blue; cursor: pointer;\">添加人员</span>&nbsp;&nbsp;<span id=\"chooseExternalUser\" style=\"color: Blue; cursor: pointer;\"   data-toggle=\"modal\" href=\"#chooseExtUserDiv\" >添加外聘人员</span></td>");
                        sbHead.Append("</tr></table>");
                        sbHead.Append("</div>");
                        sbHead.Append("</fieldset>");

                    }
                    #endregion
                    lblMemberEidt.Text = sbHead.ToString();

                    //设计总负责比例分配
                    DesignManagerUser();
                }
            }
        }

        /// <summary>
        /// 绑定设总产值分配
        /// </summary>
        private void DesignManagerUser()
        {
            TG.BLL.cm_ProjectValueAuditRecord BLL = new BLL.cm_ProjectValueAuditRecord();
            //取得助理和项目总负责
            DataTable dt = BLL.GetDesinManagerUser(proSysNo).Tables[0];

            StringBuilder sb = new StringBuilder();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + (i + 1) + " </td>");
                    sb.Append("<td>" + dt.Rows[i]["mem_Name"] + " </td>");
                    sb.Append("<td> <input id=\"Text1\" maxlength=\"15\" type=\"text\" mem_id=" + dt.Rows[i]["mem_ID"] + " /> (%)</td>");
                    sb.Append("<td><span id=\"Span1\">0.00</span>(元) </td>");
                    sb.Append("</tr>");
                }
            }
            lbl_DesignManagerValue.Text = sb.ToString();
        }
        /// <summary>
        /// 得到人员明细信息--编辑
        /// </summary>
        /// <param name="dtMember"></param>
        /// <returns></returns>
        private string getMemberDetailEdit(string itemStatus)
        {
            //取得人员信息
            TG.BLL.cm_ProjectValueAuditRecord bllRecord = new BLL.cm_ProjectValueAuditRecord();
            DataTable dtMember = bllRecord.GetJjsProjectPlanUser(proSysNo).Tables[0];

            StringBuilder sbHtml = new StringBuilder();

            sbHtml.Append(" <div id=\"tbProjectValueBymemberAmount\" class=\"cls_Container_Report\" >");
            sbHtml.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  " + getTypeDetail(itemStatus) + "-项目策划人员产值分配比例% </td> </tr> </table>");
            sbHtml.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbHtml.Append("<tr ><td style=\"width: 15%; text-align: center;\">  名称</td><td style=\"width: 15%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 35%; text-align: center;\"  > 编制 </td> <td style=\"width: 35%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sbHtml.Append(" <tr > <td style=\"width: 15%; text-align: center;\" > 专业 </td><td style=\"width: 15%; text-align: center;\">   姓名 </td> <td style=\"width: 17.5%; text-align: center;\"> 比例 </td> <td style=\"width: 17.5%; text-align: center;\"> 金额(元) </td> <td style=\"width: 17.5%; text-align: center;\"> 比例 </td> <td style=\"width: 17.5%; text-align: center;\">金额(元) </td>  </tr>");
            sbHtml.Append(" </table>");
            sbHtml.Append("<table id=\"gvProjectValueBymember\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label2\" >");

            //取得参加人员专业 --处理校对
            //取得专业
            var dtSpecialtyList = new DataView(dtMember).ToTable("Specialty", true, "spe_Name");

            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                var dv = new DataView(dtMember) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();

                sbHtml.Append("<tr>");
                sbHtml.Append("<td  width= \"15%\" class=\"cls_Column\" rowspan=\"" + dtMemberBySpecialty.Rows.Count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sbHtml.Append("<tr>");
                        sbHtml.Append("<td class=\"display\"></td>");
                    }
                    sbHtml.Append("<td class=\"display\" wp=\"0\">" + dtMemberBySpecialty.Rows[j]["mem_ID"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"15%\" >" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");

                    //该人员是否是建筑 结构给排水暖通

                    sbHtml.Append("<td width= \"17.5%\"> <input  maxlength=\"15\" type=\"text\"  roles=\"design\" runat=\"server\"  spe=" + dtSpecialtyList.Rows[i]["spe_Name"] + ">%</td>");
                    sbHtml.Append("<td width= \"17.5%\" >0.00</td>");

                    sbHtml.Append("<td width= \"17.5%\" > <input  maxlength=\"15\" type=\"text\"  roles=\"jd\" runat=\"server\"  >%</td>");
                    sbHtml.Append("<td width= \"17.5%\" >0.00</td>");


                    sbHtml.Append("</tr>");
                }

            }
            sbHtml.Append(" </table></div>");

            return sbHtml.ToString();
        }


        /// <summary>
        /// 得到人员信息
        /// </summary>
        private void getProcessDetailShow()
        {

            TG.BLL.cm_jjsProjectValueAllotConfig bll = new TG.BLL.cm_jjsProjectValueAllotConfig();
            if (projectAuditRecordEntity != null)
            {
                if (projectAuditRecordEntity.Status != "A")
                {
                    TG.Model.cm_jjsProjectValueAllotDetail model = bll.GetDetailModel(proSysNo, int.Parse(projectAuditRecordEntity.AllotID.ToString()));

                    //专业 表头
                    StringBuilder sbHead = new StringBuilder();
                    if (model != null)
                    {
                        sbHead.Append("<fieldset id=\"memFieldShow\" style=\"dispaly:none\" ><legend style=\"font-size:12px;\">经济所人员产值填写</legend>");
                        sbHead.Append(" <div id=\"tbProjectValueProcess\" class=\"cls_Container_Report\" >");
                        sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  " + getTypeDetail(model == null ? "" : model.typeStatus.ToString()) + "-配置比例% </td> </tr> </table>");
                        sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                        sbHead.Append(" <tr><td rowspan=\"3\" width=\"18%\">项目分类 </td> <td colspan=\"8\" width=\"72%\">编制 </td> <td width=\"10%\" rowspan=\"3\"> 校对  </td> </tr>");
                        sbHead.Append("  <tr>  <td rowspan=\"2\" width=\"9%\">编制 <br/>合计 </td> <td colspan=\"3\" width=\"27%\"> 建筑</td> <td colspan=\"4\" width=\"36%\">  安装 </td> </tr>");
                        sbHead.Append("  <tr> <td width=\"9%\"> 建筑<br />  合计  </td><td width=\"9%\">土建 </td> <td width=\"9%\">  结构</td>");
                        sbHead.Append("<td width=\"9%\">  安装<br /> 合计 </td><td width=\"9%\"> 给排水 </td> <td width=\"9%\">  采暖</td> <td width=\"9%\"> 电气 </td> </tr>");
                        sbHead.Append("<table id=\"gvProjectValueProcess\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

                        string itemStatus = model.typeStatus.ToString();
                        Stage = model.typeStatus.ToString();
                        if (itemStatus == "13" || itemStatus == "14" || itemStatus == "15" || itemStatus == "16" || itemStatus == "17" || itemStatus == "19" || itemStatus == "20" || itemStatus == "21" || itemStatus == "22")
                        {
                            sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（%）</td>");
                            sbHead.Append("<td width=\"9%\">" + (100 - decimal.Parse(model.ProofreadPercent.ToString())).ToString("f2") + " %</td>");
                            sbHead.Append("<td width=\"27%\" colspan=\"3\">" + model.Totalbuildingpercent + "%</td>");
                            sbHead.Append("<td width=\"36%\" colspan=\"4\">" + model.TotalInstallationpercent + "%</td>");
                            sbHead.Append("<td width=\"10%\">" + model.ProofreadPercent + "%</td>");
                            sbHead.Append("</tr>");

                            sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（元）</td>");
                            sbHead.Append("<td width=\"9%\">" + (decimal.Parse(model.TotalInstallationcount.ToString()) + decimal.Parse(model.Totalbuildingcount.ToString())).ToString("f2") + "</td>");
                            sbHead.Append("<td width=\"27%\" colspan=\"3\">" + model.Totalbuildingcount + "</td>");
                            sbHead.Append("<td width=\"36%\" colspan=\"4\">" + model.TotalInstallationcount + "</td>");
                            sbHead.Append("<td width=\"10%\">" + model.ProofreadPercent + "</td>");
                            sbHead.Append("</tr>");
                        }
                        else
                        {
                            sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（%）</td>");
                            sbHead.Append("<td width=\"9%\">" + (100 - decimal.Parse(model.ProofreadPercent.ToString())).ToString("f2") + " %</td>");
                            sbHead.Append("<td width=\"9%\">" + model.Totalbuildingpercent + "%</td>");
                            sbHead.Append("<td width=\"9%\">" + model.BuildingPercent + "%</td>");
                            sbHead.Append("<td width=\"9%\">" + model.StructurePercent + "%</td>");
                            sbHead.Append("<td width=\"9%\">" + model.TotalInstallationpercent + "%</td>");
                            sbHead.Append("<td width=\"9%\">" + model.DrainPercent + "%</td>");
                            sbHead.Append("<td width=\"9%\">" + model.HavcPercent + "%</td>");
                            sbHead.Append("<td width=\"9%\">" + model.ElectricPercent + "%</td>");
                            sbHead.Append("<td width=\"10%\">" + model.ProofreadPercent + "%</td>");
                            sbHead.Append("</tr>");

                            sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（元）</td>");
                            sbHead.Append("<td width=\"9%\">" + (decimal.Parse(model.TotalInstallationcount.ToString()) + decimal.Parse(model.Totalbuildingcount.ToString())).ToString("f2") + "</td>");
                            sbHead.Append("<td width=\"9%\">" + model.Totalbuildingcount + "</td>");
                            sbHead.Append("<td width=\"9%\">" + model.BuildingCount + "</td>");
                            sbHead.Append("<td width=\"9%\">" + model.StructureCount + "</td>");
                            sbHead.Append("<td width=\"9%\">" + model.TotalInstallationcount + "</td>");

                            sbHead.Append("<td width=\"9%\">" + model.DrainCount + "</td>");
                            sbHead.Append("<td width=\"9%\">" + model.HavcCount + "</td>");
                            sbHead.Append("<td width=\"9%\">" + model.ElectricCount + "</td>");
                            sbHead.Append("<td width=\"10%\">" + model.ProofreadCount + "</td>");
                            sbHead.Append("</tr>");
                        }
                        sbHead.Append("</table></div>");
                        sbHead.Append("</fieldset>");
                        sbHead.Append(getMemberShow());
                        lblMemberShow.Text = sbHead.ToString(); ;
                    }

                    //设计产值
                    DesignManagerValue();
                }
            }
        }

        /// <summary>
        /// 绑定设总产值分配
        /// </summary>
        private void DesignManagerValue()
        {
            TG.BLL.cm_ProjectValueAuditRecord bll = new BLL.cm_ProjectValueAuditRecord();
            //取得助理和项目总负责
            DataTable dt = bll.GetDesinManagerUserValue(proSysNo, (int)projectAuditRecordEntity.AllotID).Tables[0];

            StringBuilder sb = new StringBuilder();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + (i + 1) + " </td>");
                    sb.Append("<td>" + dt.Rows[i]["mem_Name"] + " </td>");
                    sb.Append("<td> " + dt.Rows[i]["DesignManagerPercent"] + " (%)</td>");
                    sb.Append("<td>" + dt.Rows[i]["DesignManagerCount"] + " </td>");
                    sb.Append("</tr>");
                }
            }
            lbl_DesignManagerValue.Text = sb.ToString();
        }

        private string getMemberShow()
        {
            //专业 表头
            StringBuilder sbHead = new StringBuilder();
            if (projectAuditRecordEntity != null)
            {
                TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
                DataTable dtData = BLL.GetjjsProjectValueByMemberAcount(proSysNo, int.Parse(projectAuditRecordEntity.AllotID.ToString())).Tables[0];

                sbHead.Append(" <div id=\"tbProjectValueBymemberAmount\" class=\"cls_Container_Report\" >");
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td> 项目策划人员产值分配比例% </td> </tr> </table>");
                sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                sbHead.Append("<tr ><td style=\"width: 15%; text-align: center;\">  名称</td><td style=\"width: 15%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 35%; text-align: center;\"  > 编制 </td> <td style=\"width: 35%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
                sbHead.Append(" <tr > <td style=\"width: 15%; text-align: center;\" > 专业 </td><td style=\"width: 15%; text-align: center;\">   姓名 </td> <td style=\"width: 17.5%; text-align: center;\"> 比例 </td> <td style=\"width: 17.5%; text-align: center;\"> 金额(元) </td> <td style=\"width: 17.5%; text-align: center;\"> 比例 </td> <td style=\"width: 17.5%; text-align: center;\">金额(元) </td>  </tr>");
                sbHead.Append(" </table>");

                sbHead.Append("<table id=\"gvProjectValueBymemberAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

                StringBuilder sbHtml = new StringBuilder();

                //取得专业
                var dtSpecialtyList = new DataView(dtData).ToTable("Specialty", true, "spe_Name");

                for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
                {
                    // 查找该专业下面的所有人员
                    var dv = new DataView(dtData) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                    var dtMemberBySpecialty = dv.ToTable();

                    sbHtml.Append("<tr>");
                    sbHtml.Append("<td  width= \"15%\" class=\"cls_Column\" rowspan=\"" + dtMemberBySpecialty.Rows.Count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                    for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j++)
                    {
                        if (j != 0)
                        {
                            sbHtml.Append("<tr>");
                        }

                        sbHtml.Append("<td width= \"15%\">" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                        sbHtml.Append("<td width= \"17.5%\"> " + dtMemberBySpecialty.Rows[j]["DesignPercent"].ToString() + "%</td>");
                        sbHtml.Append("<td width= \"17.5%\">" + dtMemberBySpecialty.Rows[j]["DesignCount"].ToString() + "</td>");
                        sbHtml.Append("<td width= \"17.5%\"> " + dtMemberBySpecialty.Rows[j]["ProofreadPercent"].ToString() + "%</td>");
                        sbHtml.Append("<td width= \"17.5%\">" + dtMemberBySpecialty.Rows[j]["ProofreadCount"].ToString() + "</td>");
                        sbHtml.Append("</tr>");
                    }
                }
                sbHead.Append(sbHtml.ToString());
                sbHead.Append(" </table></div>");
            }
            return sbHead.ToString();

        }


        /// <summary>
        /// 得到信息
        /// </summary>
        /// <param name="itemType"></param>
        /// <returns></returns>
        private string getTypeDetail(string itemType)
        {
            string typeDetail = "";
            if (itemType == "11")
            {
                typeDetail = "概算（室外不单算）";
            }
            else if (itemType == "12")
            {
                typeDetail = "概算（室外单算）";
            }
            else if (itemType == "13")
            {
                typeDetail = "概算 (锅炉房)";
            }
            else if (itemType == "14")
            {
                typeDetail = "概算(变电所)";
            }
            else if (itemType == "15")
            {
                typeDetail = "概算(水泵房)";
            }
            else if (itemType == "16")
            {
                typeDetail = "概算(空调机房)";
            }
            else if (itemType == "17")
            {
                typeDetail = "概算（单独室外工程）";
            }
            else if (itemType == "18")
            {
                typeDetail = "预算";
            }
            else if (itemType == "19")
            {
                typeDetail = "预算 (锅炉房)";
            }
            else if (itemType == "20")
            {
                typeDetail = "预算(变电所)";
            }
            else if (itemType == "21")
            {
                typeDetail = "预算(水泵房)";
            }
            else if (itemType == "22")
            {
                typeDetail = "预算(空调机房)";
            }
            else if (itemType == "23")
            {
                typeDetail = "结算审核";
            }
            else if (itemType == "24")
            {
                typeDetail = "调整概算、造价鉴定";
            }
            else if (itemType == "25")
            {
                typeDetail = "建议书、可研";
            }
            return typeDetail;
        }
    }
}