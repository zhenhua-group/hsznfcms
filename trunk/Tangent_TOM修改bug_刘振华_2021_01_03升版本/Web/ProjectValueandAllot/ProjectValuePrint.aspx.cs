﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjectValuePrint : System.Web.UI.Page
    {

        /// <summary>
        /// 合同ID
        /// </summary>
        public int CoperationSysNo
        {
            get
            {
                int CoperationSysNo = 0;
                int.TryParse(Request["CoperationSysNo"], out CoperationSysNo);
                return CoperationSysNo;
            }
        }

        public int AllotId
        {
            get
            {
                int AllotId = 0;
                int.TryParse(Request["AllotId"], out AllotId);
                return AllotId;
            }
        }

        /// <summary>
        /// 合同系统阶段
        /// </summary>
        public int CoperationProcess { get; set; }

        TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //得到合同基本信息
                GetConperationBaseInfo();

                //得到分配信息
                GetAllotInfo();

                //绑定分配后的产值
                bindValuedData();

                //绑定人员产值分配之后信息
                bindProjectValueByMemberAmount();
            }
        }

        /// <summary>
        /// 得到合同基本信息
        /// </summary>
        private void GetConperationBaseInfo()
        {
           
        }

        /// <summary>
        /// 得到分配信息
        /// </summary>
        private void GetAllotInfo()
        {

            DataTable dt = BLL.GetAllotDataList(AllotId.ToString()).Tables[0];

            if (dt != null && dt.Rows.Count > 0)
            {

                if (dt.Rows[0]["AllotCount"] != null)
                {
                    txt_AllotAccount.Text = dt.Rows[0]["AllotCount"].ToString();

                }
                //实收产值
                if (dt.Rows[0]["PaidValueCount"] != null)
                {
                    txt_PaidValueCount.Text = dt.Rows[0]["PaidValueCount"].ToString();
                }
                //设总比例
                if (dt.Rows[0]["DesignManagerPercent"] != null)
                {
                    txt_DesignManagerPercent.Text = dt.Rows[0]["DesignManagerPercent"].ToString();

                }
                //设总金额
                if (dt.Rows[0]["DesignManagerCount"] != null)
                {
                    txt_DesignManagerCount.Text = dt.Rows[0]["DesignManagerCount"].ToString();
                }
                //转经济所比例
                if (dt.Rows[0]["EconomyValuePercent"] != null)
                {
                    txt_EconomyValuePercent.Text = dt.Rows[0]["EconomyValuePercent"].ToString();

                }
                //转经济所比例
                if (dt.Rows[0]["EconomyValueCount"] != null)
                {
                    txt_EconomyValueCount.Text = dt.Rows[0]["EconomyValueCount"].ToString();
                }
                //部门产值
                if (dt.Rows[0]["UnitValuePercent"] != null)
                {
                    txt_UnitValueCountPercent.Text = dt.Rows[0]["UnitValuePercent"].ToString();
                }

                //部门产值
                if (dt.Rows[0]["UnitValueCount"] != null)
                {
                    txtUnitValueCount.Text = dt.Rows[0]["UnitValueCount"].ToString();
                }

                //转其他部门产值比例
                if (dt.Rows[0]["OtherDeptValuePercent"] != null)
                {
                    txtOtherDeptValuePercent.Text = dt.Rows[0]["OtherDeptValuePercent"].ToString();
                }

                if (dt.Rows[0]["OtherDeptValueCount"] != null)
                {
                    txtOtherDeptValueCount.Text = dt.Rows[0]["OtherDeptValueCount"].ToString();
                }

                //本部门自留产值比例
                if (dt.Rows[0]["TheDeptValuePercent"] != null)
                {
                    txtTheDeptValuePercent.Text = dt.Rows[0]["TheDeptValuePercent"].ToString();
                }

                if (dt.Rows[0]["TheDeptValueCount"] != null)
                {
                    txtTheDeptValueCount.Text = dt.Rows[0]["TheDeptValueCount"].ToString();
                }

                if (dt.Rows[0]["ItemType"] != null)
                {
                    stage.Value = dt.Rows[0]["ItemType"].ToString();
                    CoperationProcess = int.Parse(dt.Rows[0]["ItemType"].ToString());
                }
            }

        }


        /// <summary>
        /// 绑定分配后的产值
        /// </summary>
        private void bindValuedData()
        {
            DataSet ds = BLL.GetValuedDataList(AllotId);

            gvStageOne.DataSource = ds.Tables[0];
            gvStageOne.DataBind();

            gvStageTwo.DataSource = ds.Tables[0];
            gvStageTwo.DataBind();

            gvStageThree.DataSource = ds.Tables[0];
            gvStageThree.DataBind();

            gvStageFour.DataSource = ds.Tables[0];
            gvStageFour.DataBind();

            gvSpeAmount.DataSource = ds.Tables[1];
            gvSpeAmount.DataBind();

            gvProcessAmountOne.DataSource = ds.Tables[2];
            gvProcessAmountOne.DataBind();

            gvProcessAmountTwo.DataSource = ds.Tables[3];
            gvProcessAmountTwo.DataBind();

            gvProcessAmountThree.DataSource = ds.Tables[4];
            gvProcessAmountThree.DataBind();

            gvProcessAmountFour.DataSource = ds.Tables[5];
            gvProcessAmountFour.DataBind();

            gvProcessAmountFive.DataSource = ds.Tables[6];
            gvProcessAmountFive.DataBind();

            gvOutDoorAmount.DataSource = ds.Tables[7];
            gvOutDoorAmount.DataBind();
        }

        /// <summary>
        /// 绑定人员产值分配之后信息
        /// </summary>
        private void bindProjectValueByMemberAmount()
        {
            //DataSet ds = BLL.GetProjectAllotValueByMemberDetatil(AllotId);
            //gvProjectValueBymemberAmount.DataSource = ds.Tables[0];
            //gvProjectValueBymemberAmount.DataBind();

        }

    }
}