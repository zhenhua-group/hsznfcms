﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace TG.Web.ProjectValueandAllot
{
    public partial class PrintValue : System.Web.UI.Page
    {
        /// <summary>
        /// 合同ID
        /// </summary>
        public int ProSysNo
        {
            get
            {
                int proSysNo = 0;
                int.TryParse(Request["ProjectSysNo"], out proSysNo);
                return proSysNo;
            }
        }

        public int AllotId
        {
            get
            {
                int AllotId = 0;
                int.TryParse(Request["AllotId"], out AllotId);
                return AllotId;
            }
        }

        public string Type
        {
            get
            {
                return Request["type"];
            }
        }

        public decimal TotalCount
        {
            get
            {
                decimal totalCount = 0;
                decimal.TryParse(Request["totalCount"], out totalCount);
                return totalCount;
            }
        }
        TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();

        public string UnitName { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //得到基本信息
                GetProjectBaseInfo();

                CreateTable();        
            }
        }

        /// <summary>
        /// 得到基本信息
        /// </summary>
        private void GetProjectBaseInfo()
        {
            lblAllcountCount.Text = TotalCount.ToString();
            lblType.Text = getTypeDetail();
            TG.BLL.cm_Project bll = new TG.BLL.cm_Project();
            TG.Model.cm_Project model = bll.GetModel(ProSysNo);

            if (model != null)
            {
                //项目名称
                lblProjectName.Text = model.pro_name;
                //承接部门
                lbl_unit.Text = model.Unit;
                UnitName = model.Unit;
            }

            TG.BLL.cm_ProjectValueAllot bllAllot = new TG.BLL.cm_ProjectValueAllot();
            TG.Model.cm_ProjectValueAllot modelAllot = new Model.cm_ProjectValueAllot();
            modelAllot = bllAllot.GetModel(AllotId);
            if (modelAllot != null)
            {
                lbl_Year.Text = modelAllot.ActualAllountTime;
            }
        }

        /// <summary>
        /// 生成Table
        /// </summary>
        private void CreateTable()
        {
            StringBuilder strWhere = new StringBuilder();
            strWhere.Append(" AND A.AllotId="+AllotId+"");
            if (Type == "tranjjs")
            {
                strWhere.Append(" AND TranType='jjs'");
            }
            else if (Type == "trannts")
            {
                strWhere.Append("AND TranType='nts'");
            }
            else if (Type == "trantjs")
            {
                strWhere.Append("AND TranType='tjs'");
            }
            else
            {
                strWhere.Append("AND TranType is null");
            }

            DataSet dtData = BLL.GetTranProjectValueByMemberTotalAcount(strWhere.ToString(), UnitName);
      
            //专业 表头
            StringBuilder sbHead = new StringBuilder();
            sbHead.Append("   <table class=\"cls_content\"> <tr>");
            sbHead.Append("  <td style=\"width: 15%;\">  专业  </td> <td width=\"12%\"> 姓名  </td> <td width=\"6.5%\"> 比例 </td><td width=\"12%\"> 产值  </td><td width=\"12%\"> 签名</td>");
            sbHead.Append(" <td width=\"12%\">  姓名</td><td width=\"6.5%\">  比例 </td> <td width=\"12%\"> 产值</td> <td width=\"12%\">签名</td> </tr>");

            StringBuilder sbHtml = new StringBuilder();

            var theUnitMember = dtData.Tables[0];
            decimal payShiAllotCount = 0;
            //取得专业
            var dtSpecialtyList = new DataView(theUnitMember).ToTable("Specialty", true, "spe_Name");
            if (theUnitMember.Rows.Count > 0)
            {
                if (Type != "tranjjs" && Type != "trannts")
                {
                    sbHead.Append("<tr><td colspan=\"9\">本所人员产值分配明细</td></tr>");
                }
            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                    var dv = new DataView(theUnitMember) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();

                int count = 0;
                if (dtMemberBySpecialty.Rows.Count % 2 == 0)
                {
                    count = dtMemberBySpecialty.Rows.Count / 2;
                }
                else
                {
                    count = dtMemberBySpecialty.Rows.Count / 2 + 1;
                }

                sbHtml.Append("<tr>");
                sbHtml.Append("<td  rowspan=\"" + count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j = j + 2)
                {
                    if (j != 0)
                    {
                        sbHtml.Append("<tr>");
                    }

                    sbHtml.Append("<td >" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                    sbHtml.Append("<td > </td>");
                    sbHtml.Append("<td >" + dtMemberBySpecialty.Rows[j]["totalCount"].ToString() + "</td>");
                    sbHtml.Append("<td > </td>");

                    if (j + 1 >= dtMemberBySpecialty.Rows.Count)
                    {
                        sbHtml.Append("<td ></td>");
                        sbHtml.Append("<td > </td>");
                        sbHtml.Append("<td ></td>");
                        sbHtml.Append("<td ></td>");
                    }
                    else
                    {
                        sbHtml.Append("<td >" + dtMemberBySpecialty.Rows[j + 1]["mem_Name"].ToString() + "</td>");
                        sbHtml.Append("<td > </td>");
                        sbHtml.Append("<td >" + dtMemberBySpecialty.Rows[j + 1]["totalCount"].ToString() + "</td>");
                        sbHtml.Append("<td > </td>");
                    }
                    sbHtml.Append("</tr>");
                }
                }
                sbHead.Append(sbHtml.ToString());
                payShiAllotCount = theUnitMember.AsEnumerable().Sum(s => s.Field<int>("totalCount"));
                if (Type != "tranjjs" && Type != "trannts")
                {
                    sbHead.Append("<tr><td style=\"width: 15%;\">  本所合计  </td><td width=\"12%\"> 本所分配产值  </td> <td width=\"6.5%\">  </td><td width=\"12%\"> " + payShiAllotCount.ToString() + "  </td><td width=\"12%\"> </td>");
                    sbHead.Append("<td width=\"12%\"> </td><td width=\"6.5%\">   </td> <td width=\"12%\"> </td> <td width=\"12%\"></td> </tr>");

                }
            }


            var otherUnitMember = dtData.Tables[1];
            decimal payOtherAllotCount = 0;
            if (otherUnitMember != null && otherUnitMember.Rows.Count > 0)
            {
                StringBuilder sbHtmlOther = new StringBuilder();
                if (Type != "tranjjs" && Type != "trannts")
                {
                    sbHead.Append("<tr><td colspan=\"9\">其他所人员产值分配明细</td></tr>");
                }
                //取得专业
                var dtotherSpecialtyList = new DataView(otherUnitMember).ToTable("Specialty", true, "spe_Name");

                for (int i = 0; i < dtotherSpecialtyList.Rows.Count; i++)
                {
                    // 查找该专业下面的所有人员
                    var dv = new DataView(otherUnitMember) { RowFilter = "spe_Name='" + dtotherSpecialtyList.Rows[i]["spe_Name"] + "'" };
                    var dtOtherMemberBySpecialty = dv.ToTable();

                    int count = 0;
                    if (dtOtherMemberBySpecialty.Rows.Count % 2 == 0)
                    {
                        count = dtOtherMemberBySpecialty.Rows.Count / 2;
                    }
                    else
                    {
                        count = dtOtherMemberBySpecialty.Rows.Count / 2 + 1;
                    }

                    sbHtmlOther.Append("<tr>");
                    sbHtmlOther.Append("<td  rowspan=\"" + count + "\">" + dtotherSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                    for (int j = 0; j < dtOtherMemberBySpecialty.Rows.Count; j = j + 2)
                    {
                        if (j != 0)
                        {
                            sbHtmlOther.Append("<tr>");
                        }

                        sbHtmlOther.Append("<td >" + dtOtherMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                        sbHtmlOther.Append("<td > </td>");
                        sbHtmlOther.Append("<td >" + dtOtherMemberBySpecialty.Rows[j]["totalCount"].ToString() + "</td>");
                        sbHtmlOther.Append("<td > </td>");

                        if (j + 1 >= dtOtherMemberBySpecialty.Rows.Count)
                        {
                            sbHtmlOther.Append("<td ></td>");
                            sbHtmlOther.Append("<td > </td>");
                            sbHtmlOther.Append("<td ></td>");
                            sbHtmlOther.Append("<td ></td>");
                        }
                        else
                        {
                            sbHtmlOther.Append("<td >" + dtOtherMemberBySpecialty.Rows[j + 1]["mem_Name"].ToString() + "</td>");
                            sbHtmlOther.Append("<td > </td>");
                            sbHtmlOther.Append("<td >" + dtOtherMemberBySpecialty.Rows[j + 1]["totalCount"].ToString() + "</td>");
                            sbHtmlOther.Append("<td > </td>");
                        }
                        sbHtmlOther.Append("</tr>");
                    }

                }
                sbHead.Append(sbHtmlOther.ToString());
                payOtherAllotCount = otherUnitMember.AsEnumerable().Sum(s => s.Field<int>("totalCount"));
                if (Type != "tranjjs" && Type != "trannts")
                {
                    sbHead.Append("<tr><td style=\"width: 15%;\">  外所合计  </td><td width=\"12%\"> 外所分配产值  </td> <td width=\"6.5%\">  </td><td width=\"12%\"> " + payOtherAllotCount.ToString() + "  </td><td width=\"12%\"> </td>");
                    sbHead.Append("<td width=\"12%\"> </td><td width=\"6.5%\">   </td> <td width=\"12%\"> </td> <td width=\"12%\"></td> </tr>");
                }
            }

            var wpUnitMember = dtData.Tables[2];
            decimal wpAllotCount = 0;
            if (wpUnitMember != null && wpUnitMember.Rows.Count > 0)
            {
                StringBuilder sbHtmlWp = new StringBuilder();
                if (Type != "tranjjs" || Type != "trannts")
                {
                    sbHead.Append("<tr><td colspan=\"9\">外聘人员产值分配明细</td></tr>");
                }
                //取得专业
                var dtwpSpecialtyList = new DataView(wpUnitMember).ToTable("Specialty", true, "spe_Name");

                for (int i = 0; i < dtwpSpecialtyList.Rows.Count; i++)
                {
                    // 查找该专业下面的所有人员
                    var dv = new DataView(wpUnitMember) { RowFilter = "spe_Name='" + dtwpSpecialtyList.Rows[i]["spe_Name"] + "'" };
                    var dtwpMemberBySpecialty = dv.ToTable();

                    int count = 0;
                    if (dtwpMemberBySpecialty.Rows.Count % 2 == 0)
                    {
                        count = dtwpMemberBySpecialty.Rows.Count / 2;
                    }
                    else
                    {
                        count = dtwpMemberBySpecialty.Rows.Count / 2 + 1;
                    }

                    sbHtmlWp.Append("<tr>");
                    sbHtmlWp.Append("<td  rowspan=\"" + count + "\">" + dtwpSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                    for (int j = 0; j < dtwpMemberBySpecialty.Rows.Count; j = j + 2)
                    {
                        if (j != 0)
                        {
                            sbHtmlWp.Append("<tr>");
                        }

                        sbHtmlWp.Append("<td >" + dtwpMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                        sbHtmlWp.Append("<td > </td>");
                        sbHtmlWp.Append("<td >" + dtwpMemberBySpecialty.Rows[j]["totalCount"].ToString() + "</td>");
                        sbHtmlWp.Append("<td > </td>");

                        if (j + 1 >= dtwpMemberBySpecialty.Rows.Count)
                        {
                            sbHtmlWp.Append("<td ></td>");
                            sbHtmlWp.Append("<td > </td>");
                            sbHtmlWp.Append("<td ></td>");
                            sbHtmlWp.Append("<td ></td>");
                        }
                        else
                        {
                            sbHtmlWp.Append("<td >" + dtwpMemberBySpecialty.Rows[j + 1]["mem_Name"].ToString() + "</td>");
                            sbHtmlWp.Append("<td > </td>");
                            sbHtmlWp.Append("<td >" + dtwpMemberBySpecialty.Rows[j + 1]["totalCount"].ToString() + "</td>");
                            sbHtmlWp.Append("<td > </td>");
                        }
                        sbHtmlWp.Append("</tr>");
                    }

                }
                sbHead.Append(sbHtmlWp.ToString());
                wpAllotCount = wpUnitMember.AsEnumerable().Sum(s => s.Field<int>("totalCount"));
                if (Type != "tranjjs" && Type != "trannts")
                {
                    sbHead.Append("<tr><td style=\"width: 15%;\">  外聘合计  </td><td width=\"12%\"> 外聘分配产值  </td> <td width=\"6.5%\">  </td><td width=\"12%\"> " + wpAllotCount.ToString() + "  </td><td width=\"12%\"> </td>");
                    sbHead.Append("<td width=\"12%\"> </td><td width=\"6.5%\">   </td> <td width=\"12%\"> </td> <td width=\"12%\"></td> </tr>");

                }
            }

            //差额
            decimal differenceAllotCount = 0;
            if (!string.IsNullOrEmpty(lblAllcountCount.Text))
            {
                differenceAllotCount = decimal.Parse(lblAllcountCount.Text) - payShiAllotCount - payOtherAllotCount - wpAllotCount;
            }
            sbHead.Append("<tr><td style=\"width: 15%;\">  总体合计  </td><td width=\"12%\"> 实分产值  </td> <td width=\"6.5%\">  </td><td width=\"12%\"> " + (payShiAllotCount + payOtherAllotCount + wpAllotCount).ToString("f0") + "  </td><td width=\"12%\"> </td>");
            sbHead.Append("<td width=\"12%\"> </td><td width=\"6.5%\">   </td> <td width=\"12%\"> </td> <td width=\"12%\"></td> </tr>");


            sbHead.Append(" </table>");

            lbl_Member.Text = sbHead.ToString();
        }

        /// <summary>
        /// 得到类型明细
        /// </summary>
        /// <returns></returns>
        private string getTypeDetail()
        {
            string typeDetail = "";
            if (Type == "tranjjs")
            {
                typeDetail = "转经济所";
            }
            else if (Type == "trannts")
            {
                typeDetail = "转暖通所";
            }
            
            return typeDetail;
        }
    }
}