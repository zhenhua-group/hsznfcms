﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace TG.Web.ProjectValueandAllot
{
    public partial class SecondPrintProjectValue : System.Web.UI.Page
    {
        /// <summary>
        /// 合同ID
        /// </summary>
        public int ProSysNo
        {
            get
            {
                int CoperationSysNo = 0;
                int.TryParse(Request["ProjectSysNo"], out CoperationSysNo);
                return CoperationSysNo;
            }
        }

        public int AllotId
        {
            get
            {
                int AllotId = 0;
                int.TryParse(Request["AllotId"], out AllotId);
                return AllotId;
            }
        }

        TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();

        public string unitName;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //得到合同基本信息
                GetProjectBaseInfo();

                //得到分配信息
                GetAllotInfo();

                CreateTable();
            }
        }


        /// <summary>
        /// 得到项目基本信息
        /// </summary>
        private void GetProjectBaseInfo()
        {
            TG.BLL.cm_Project bllProject = new TG.BLL.cm_Project();
            TG.Model.cm_Project model = new Model.cm_Project();
            model = bllProject.GetModel(ProSysNo);
            if (model != null)
            {
                lblProjectName.Text = model.pro_name;
                lbl_unit.Text = model.Unit;
                unitName = model.Unit;
            }
        }

        /// <summary>
        /// 得到分配信息
        /// </summary>
        private void GetAllotInfo()
        {
            TG.BLL.cm_ProjectValueAllot bllAllot = new TG.BLL.cm_ProjectValueAllot();
            TG.Model.cm_ProjectValueAllot model = new Model.cm_ProjectValueAllot();
            model = bllAllot.GetModel(AllotId);
            if (model != null)
            {
                lblAllotCount.Text = model.AllotCount.ToString();
                lbl_Year.Text = model.ActualAllountTime.ToString();
            }
        }

        /// <summary>
        /// 生成Table
        /// </summary>
        private void CreateTable()
        {
            DataSet dtData = BLL.GetProjectValueByMemberTotalAcount(AllotId, unitName);
            DataTable dtSecondData = BLL.GetSecondAuditDesignAmount(ProSysNo, AllotId).Tables[0];
            //专业 表头
            StringBuilder sbHead = new StringBuilder();
            sbHead.Append("   <table class=\"cls_content\"> <tr>");
            sbHead.Append("  <td style=\"width: 15%;\">  专业  </td> <td width=\"12%\"> 姓名  </td> <td width=\"6.5%\"> 比例 </td><td width=\"12%\"> 产值  </td><td width=\"12%\"> 签名</td>");
            sbHead.Append(" <td width=\"12%\">  姓名</td><td width=\"6.5%\">  比例 </td> <td width=\"12%\"> 产值</td> <td width=\"12%\">签名</td> </tr>");

            StringBuilder sbHtml = new StringBuilder();

            var theUnitMember = dtData.Tables[0];
            //实分产值
            decimal payShiAllotCount = 0;
            //取得专业
            var dtSpecialtyList = new DataView(theUnitMember).ToTable("Specialty", true, "spe_Name");
            if (theUnitMember.Rows.Count > 0)
            {
                sbHead.Append("<tr><td colspan=\"9\">本所人员产值分配明细</td></tr>");

            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                    var dv = new DataView(theUnitMember) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();

                int count = 0;
                if (dtMemberBySpecialty.Rows.Count % 2 == 0)
                {
                    count = dtMemberBySpecialty.Rows.Count / 2;
                }
                else
                {
                    count = dtMemberBySpecialty.Rows.Count / 2 + 1;
                }

                sbHtml.Append("<tr>");
                sbHtml.Append("<td  rowspan=\"" + count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j = j + 2)
                {
                    if (j != 0)
                    {
                        sbHtml.Append("<tr>");
                    }

                    sbHtml.Append("<td >" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                    sbHtml.Append("<td > </td>");
                    sbHtml.Append("<td >" + dtMemberBySpecialty.Rows[j]["totalCount"].ToString() + "</td>");
                    sbHtml.Append("<td > </td>");

                    if (j + 1 >= dtMemberBySpecialty.Rows.Count)
                    {
                        sbHtml.Append("<td ></td>");
                        sbHtml.Append("<td > </td>");
                        sbHtml.Append("<td ></td>");
                        sbHtml.Append("<td ></td>");
                    }
                    else
                    {
                        sbHtml.Append("<td >" + dtMemberBySpecialty.Rows[j + 1]["mem_Name"].ToString() + "</td>");
                        sbHtml.Append("<td > </td>");
                        sbHtml.Append("<td >" + dtMemberBySpecialty.Rows[j + 1]["totalCount"].ToString() + "</td>");
                        sbHtml.Append("<td > </td>");
                    }
                    sbHtml.Append("</tr>");
                }


            }

            sbHead.Append(sbHtml.ToString());
                payShiAllotCount = theUnitMember.AsEnumerable().Sum(s => s.Field<int>("totalCount"));
                sbHead.Append("<tr><td style=\"width: 15%;\">  本所合计  </td><td width=\"12%\"> 本所分配产值  </td> <td width=\"6.5%\">  </td><td width=\"12%\"> " + payShiAllotCount.ToString() + "  </td><td width=\"12%\"> </td>");
                sbHead.Append("<td width=\"12%\"> </td><td width=\"6.5%\">   </td> <td width=\"12%\"> </td> <td width=\"12%\"></td> </tr>");

            }

            var otherUnitMember = dtData.Tables[1];
            decimal payOtherAllotCount = 0;
            if (otherUnitMember != null && otherUnitMember.Rows.Count > 0)
            {
                StringBuilder sbHtmlOther = new StringBuilder();
                sbHead.Append("<tr><td colspan=\"9\">其他所人员产值分配明细</td></tr>");
                //取得专业
                var dtotherSpecialtyList = new DataView(otherUnitMember).ToTable("Specialty", true, "spe_Name");

                for (int i = 0; i < dtotherSpecialtyList.Rows.Count; i++)
                {
                    // 查找该专业下面的所有人员
                    var dv = new DataView(otherUnitMember) { RowFilter = "spe_Name='" + dtotherSpecialtyList.Rows[i]["spe_Name"] + "'" };
                    var dtOtherMemberBySpecialty = dv.ToTable();

                    int count = 0;
                    if (dtOtherMemberBySpecialty.Rows.Count % 2 == 0)
                    {
                        count = dtOtherMemberBySpecialty.Rows.Count / 2;
                    }
                    else
                    {
                        count = dtOtherMemberBySpecialty.Rows.Count / 2 + 1;
                    }

                    sbHtmlOther.Append("<tr>");
                    sbHtmlOther.Append("<td  rowspan=\"" + count + "\">" + dtotherSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                    for (int j = 0; j < dtOtherMemberBySpecialty.Rows.Count; j = j + 2)
                    {
                        if (j != 0)
                        {
                            sbHtmlOther.Append("<tr>");
                        }

                        sbHtmlOther.Append("<td >" + dtOtherMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                        sbHtmlOther.Append("<td > </td>");
                        sbHtmlOther.Append("<td >" + dtOtherMemberBySpecialty.Rows[j]["totalCount"].ToString() + "</td>");
                        sbHtmlOther.Append("<td > </td>");

                        if (j + 1 >= dtOtherMemberBySpecialty.Rows.Count)
                        {
                            sbHtmlOther.Append("<td ></td>");
                            sbHtmlOther.Append("<td > </td>");
                            sbHtmlOther.Append("<td ></td>");
                            sbHtmlOther.Append("<td ></td>");
                        }
                        else
                        {
                            sbHtmlOther.Append("<td >" + dtOtherMemberBySpecialty.Rows[j + 1]["mem_Name"].ToString() + "</td>");
                            sbHtmlOther.Append("<td > </td>");
                            sbHtmlOther.Append("<td >" + dtOtherMemberBySpecialty.Rows[j + 1]["totalCount"].ToString() + "</td>");
                            sbHtmlOther.Append("<td > </td>");
                        }
                        sbHtmlOther.Append("</tr>");
                    }

                }
                sbHead.Append(sbHtmlOther.ToString());
                payOtherAllotCount = otherUnitMember.AsEnumerable().Sum(s => s.Field<int>("totalCount"));
                sbHead.Append("<tr><td style=\"width: 15%;\">  外所合计  </td><td width=\"12%\"> 外所分配产值  </td> <td width=\"6.5%\">  </td><td width=\"12%\"> " + payOtherAllotCount.ToString() + "  </td><td width=\"12%\"> </td>");
                sbHead.Append("<td width=\"12%\"> </td><td width=\"6.5%\">   </td> <td width=\"12%\"> </td> <td width=\"12%\"></td> </tr>");

            }

            var wpUnitMember = dtData.Tables[2];
            decimal wpAllotCount = 0;
            if (wpUnitMember != null && wpUnitMember.Rows.Count > 0)
            {
                StringBuilder sbHtmlWp = new StringBuilder();

                sbHead.Append("<tr><td colspan=\"9\">外聘人员产值分配明细</td></tr>");
                //取得专业
                var dtwpSpecialtyList = new DataView(wpUnitMember).ToTable("Specialty", true, "spe_Name");

                for (int i = 0; i < dtwpSpecialtyList.Rows.Count; i++)
                {
                    // 查找该专业下面的所有人员
                    var dv = new DataView(wpUnitMember) { RowFilter = "spe_Name='" + dtwpSpecialtyList.Rows[i]["spe_Name"] + "'" };
                    var dtwpMemberBySpecialty = dv.ToTable();

                    int count = 0;
                    if (dtwpMemberBySpecialty.Rows.Count % 2 == 0)
                    {
                        count = dtwpMemberBySpecialty.Rows.Count / 2;
                    }
                    else
                    {
                        count = dtwpMemberBySpecialty.Rows.Count / 2 + 1;
                    }

                    sbHtmlWp.Append("<tr>");
                    sbHtmlWp.Append("<td  rowspan=\"" + count + "\">" + dtwpSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                    for (int j = 0; j < dtwpMemberBySpecialty.Rows.Count; j = j + 2)
                    {
                        if (j != 0)
                        {
                            sbHtmlWp.Append("<tr>");
                        }

                        sbHtmlWp.Append("<td >" + dtwpMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                        sbHtmlWp.Append("<td > </td>");
                        sbHtmlWp.Append("<td >" + dtwpMemberBySpecialty.Rows[j]["totalCount"].ToString() + "</td>");
                        sbHtmlWp.Append("<td > </td>");

                        if (j + 1 >= dtwpMemberBySpecialty.Rows.Count)
                        {
                            sbHtmlWp.Append("<td ></td>");
                            sbHtmlWp.Append("<td > </td>");
                            sbHtmlWp.Append("<td ></td>");
                            sbHtmlWp.Append("<td ></td>");
                        }
                        else
                        {
                            sbHtmlWp.Append("<td >" + dtwpMemberBySpecialty.Rows[j + 1]["mem_Name"].ToString() + "</td>");
                            sbHtmlWp.Append("<td > </td>");
                            sbHtmlWp.Append("<td >" + dtwpMemberBySpecialty.Rows[j + 1]["totalCount"].ToString() + "</td>");
                            sbHtmlWp.Append("<td > </td>");
                        }
                        sbHtmlWp.Append("</tr>");
                    }

                }
                sbHead.Append(sbHtmlWp.ToString());
                wpAllotCount = wpUnitMember.AsEnumerable().Sum(s => s.Field<int>("totalCount"));
                sbHead.Append("<tr><td style=\"width: 15%;\">  外聘合计  </td><td width=\"12%\"> 外聘分配产值  </td> <td width=\"6.5%\">  </td><td width=\"12%\"> " + wpAllotCount.ToString() + "  </td><td width=\"12%\"> </td>");
                sbHead.Append("<td width=\"12%\"> </td><td width=\"6.5%\">   </td> <td width=\"12%\"> </td> <td width=\"12%\"></td> </tr>");
            }

            //差额
            decimal differenceAllotCount = 0;
            if (!string.IsNullOrEmpty(lblAllotCount.Text))
            {
                differenceAllotCount = decimal.Parse(lblAllotCount.Text) - payShiAllotCount - payOtherAllotCount - wpAllotCount;
            }
            sbHead.Append("<tr><td style=\"width: 15%;\">  总体合计  </td><td width=\"12%\"> 实分产值  </td> <td width=\"6.5%\">  </td><td width=\"12%\"> " + (payShiAllotCount + payOtherAllotCount + wpAllotCount).ToString("f0") + "  </td><td width=\"12%\"> </td>");
            sbHead.Append("<td width=\"12%\"> </td><td width=\"6.5%\">   </td> <td width=\"12%\"> </td> <td width=\"12%\"></td> </tr>");


            sbHead.Append(" </table>");
            if (dtSecondData != null && dtSecondData.Rows.Count > 0)
            {
                StringBuilder sbSecond = new StringBuilder();
                sbSecond.Append("<table class=\"cls_content_second NoPrint\" >");
                sbSecond.Append("<tr><td>备注：</td></tr>");
                sbSecond.Append("<tr>");
                sbSecond.Append("<td>建筑、结构审核金额：&nbsp;" + dtSecondData.Rows[0]["AuditCount"].ToString() + "（元）</td></tr>");
                sbSecond.Append("<tr><td>所领导班子设计金额：&nbsp;" + dtSecondData.Rows[0]["DesignCount"].ToString() + "（元）</td></tr>");
                sbSecond.Append("</table>");
                lblSecondDetail.Text = sbSecond.ToString();
            }
            lbl_Member.Text = sbHead.ToString();
        }
    }
}