﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace TG.Web.ProjectValueandAllot
{
    public partial class CostUnitDetailsBymaster : PageBase
    {
        protected string UnitID
        {
            get
            {
                return Request.Params["unitid"];
            }
        }
        protected string TypeID
        {
            get
            {
                return Request.Params["typeid"];
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindType();
                InitDate();
                BindPreviewPower();
                InitUnit();
            }
        }
        protected void InitUnit()
        {
            string InitUnitId = base.UserUnitNo.ToString();
            if (this.drp_unit.Items.FindByValue(InitUnitId) != null)
            {
                this.drp_unit.Items.FindByValue(InitUnitId).Selected = true;
            }
            else
            {
                this.drp_unit.Items.FindByValue("-1").Selected = true;
            }
        }
        //绑定前台权限
        public void BindPreviewPower()
        {
            if (UnitID != null && UnitID != "")
            {
                this.userUnitNum.Value = UnitID;
            }
            else
            {
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
            if (TypeID != null && TypeID != "")
            {
                this.HiddenTypeID.Value = TypeID;
            }
            this.userSysNum.Value = base.UserSysNo.ToString();
            StringBuilder builder = new StringBuilder();
            GetPreviewPowerSql(ref builder);
            this.hid_where.Value = builder.ToString();
        }
        //判断是否进行权限判断
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (costUserId =" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND costUnit = (Select unit_Id From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //绑定单位
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //初始时间设置
        protected void InitDate()
        {
            string str_year = DateTime.Now.Year.ToString();
            this.txt_costdate1.Value = str_year + "-01-01";
            this.txt_costdate2.Value = str_year + "-12-31";
        }
        //绑定基本类型
        private void BindType()
        {
            //throw new NotImplementedException();
            TG.BLL.cm_CostType costbll = new TG.BLL.cm_CostType();
            string strwhereAll = "costgroup=0";
            this.drp_costTypeAll.DataSource = costbll.GetList(strwhereAll);
            this.drp_costTypeAll.DataTextField = "costName";
            this.drp_costTypeAll.DataValueField = "ID";
            this.drp_costTypeAll.DataBind();
        }

        protected void btn_export_Click(object sender, EventArgs e)
        {
            string unitid = this.drp_unit.SelectedItem.Value;
            string typeid = this.drp_costTypeAll.SelectedItem.Value;
            string starttime = this.txt_costdate1.Value;
            string endtime = this.txt_costdate2.Value;
            string sqlCost = @" SELECT c.* ,(SELECT SUM(s.costcharge) from cm_CostDetails as s where s.costTypeID=c.id {0}
  )as charge FROM  cm_CostType as c where costGroup IN (0,{1})";
            StringBuilder builder1 = new StringBuilder();
            StringBuilder builder2 = new StringBuilder();
            //年份
            if (starttime != "" && endtime != "")
            {
                builder1.AppendFormat(" AND s.costTime between '{0}' and '{1}' ", starttime, endtime);
            }
            //年份
            if (starttime == "" && endtime != "")
            {
                builder1.AppendFormat(" AND s.costTime between '{0}' and '{1}' ", DateTime.Now.ToString("yyyy-MM-dd"), endtime);
            }
            //年份
            if (starttime != "" && endtime == "")
            {
                builder1.AppendFormat(" AND s.costTime between '{0}' and '{1}' ", starttime, DateTime.Now.ToString("yyyy-MM-dd"));
            }
            if (unitid == "250")//252/257
            {
                sqlCost = string.Format(sqlCost, builder1 + "and s.costUnit=" + unitid, "1");
            }
            else if (unitid == "252")
            {
                sqlCost = string.Format(sqlCost, builder1 + "and s.costUnit=" + unitid, "2");
            }
            else if (unitid == "257")
            {
                sqlCost = string.Format(sqlCost, builder1 + "and s.costUnit=" + unitid, "3");
            }
            else if (unitid == "-1")
            {
                sqlCost = string.Format(sqlCost, builder1, "4");
            }
            else
            {
                sqlCost = string.Format(sqlCost, builder1 + "and s.costUnit=" + unitid, "4");
            }
            if (!string.IsNullOrEmpty(typeid) && typeid != "-1")
            {
                sqlCost += "and c.id=" + typeid;
            }
            string unitName = this.drp_unit.SelectedItem.Text.Trim();
            string typeName = this.drp_costTypeAll.SelectedItem.Text.Trim();
            string titleName = "";
            if (unitid == "-1")
            {
                if (typeid == "-1")
                {
                    titleName = "全院部门 " + starttime + "至" + endtime + "成本费用明细统计";
                }
                else
                {
                    titleName = "全院部门 " + starttime + "至" + endtime + " " + typeName + "费用明细统计";
                }

            }
            else
            {
                if (typeid == "-1")
                {
                    titleName = unitName + " " + starttime + "至" + endtime + " " + "成本费用明细统计";
                }
                else
                {
                    titleName = unitName + " " + starttime + "至" + endtime + " " + typeName + "费用明细统计";
                }

            }
            DataTable dtCost = TG.DBUtility.DbHelperSQL.Query(sqlCost) == null ? null : TG.DBUtility.DbHelperSQL.Query(sqlCost).Tables[0];
            List<CostListModel> GZlist = new List<CostListModel>();
            List<CostListModel> FYlist = new List<CostListModel>();
            bool isAddGZCost = false;
            bool isAddFYCost = false;
            if (dtCost != null)
            {
                for (int i = 0; i < dtCost.Rows.Count; i++)
                {
                    CostListModel model = new CostListModel();
                    if (dtCost.Rows[i]["costName"].ToString().Contains("工资"))
                    {
                        isAddGZCost = true;
                        model.CostName = dtCost.Rows[i]["costName"].ToString();
                        model.CosttypeID = dtCost.Rows[i]["id"].ToString();
                        if (dtCost.Rows[i]["charge"] != null && dtCost.Rows[i]["charge"].ToString() != "")
                        {
                            model.AllCharge = Convert.ToDecimal(dtCost.Rows[i]["charge"].ToString());
                        }
                        else
                        {
                            model.AllCharge = 0.00m;
                        }
                        GZlist.Add(model);
                    }
                    else
                    {
                        isAddFYCost = true;
                        model.CostName = dtCost.Rows[i]["costName"].ToString();
                        model.CosttypeID = dtCost.Rows[i]["id"].ToString();
                        if (dtCost.Rows[i]["charge"] != null && dtCost.Rows[i]["charge"].ToString() != "")
                        {
                            model.AllCharge = Convert.ToDecimal(dtCost.Rows[i]["charge"].ToString());
                        }
                        else
                        {
                            model.AllCharge = 0.00m;
                        }
                        FYlist.Add(model);
                    }
                }
            }
            CostListModel GZmodel = new CostListModel();
            CostListModel FYmodel = new CostListModel();
            CostListModel Allmodel = new CostListModel();
            foreach (var item in GZlist)
            {
                GZmodel.AllCharge += item.AllCharge;
            }
            GZmodel.CostName = "工资合计";
            GZmodel.CosttypeID = "gz";
            if (typeid == "-1")
            {
                GZlist.Add(GZmodel);
            }
            foreach (var item in FYlist)
            {
                FYmodel.AllCharge += item.AllCharge;
            }
            FYmodel.CostName = "费用成本合计";
            FYmodel.CosttypeID = "fy";
            if (typeid == "-1")
            {
                FYlist.Add(FYmodel);
            }
            Allmodel.AllCharge = FYmodel.AllCharge + GZmodel.AllCharge;
            Allmodel.CostName = "总合计";
            Allmodel.CosttypeID = "all";
            if (typeid == "-1")
            {
                FYlist.Add(Allmodel);
            }

            GZlist.AddRange(FYlist);
            string modelPath = " ~/TemplateXls/CostUnitDetailsBymaster.xls";
            ALlExportMethod(titleName, GZlist, modelPath);
        }
        private void ALlExportMethod(string unitName, List<CostListModel> GZlist, string modelPath)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);
            IRow irowTitle = ws.CreateRow(0);
            ICell icellF = irowTitle.CreateCell(0);
            icellF.CellStyle = style2;
            icellF.SetCellValue(unitName);
            int row = 1;
            foreach (CostListModel model in GZlist)
            {
                IRow irow = ws.CreateRow(row);
                ICell icell = irow.CreateCell(0);
                icell.CellStyle = style2;
                icell.SetCellValue(model.CostName);
                ICell icell1 = irow.CreateCell(1);
                icell1.CellStyle = style2;
                icell1.SetCellValue(Convert.ToDouble(model.AllCharge));
                row++;
            }

            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.Write(memoryStream);
                string name = System.Web.HttpContext.Current.Server.UrlEncode(unitName + "成本明细.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
    }
    public partial class CostListModel
    {
        private string costName;

        public string CostName
        {
            get { return costName; }
            set { costName = value; }
        }
        private decimal allCharge;

        public decimal AllCharge
        {
            get { return allCharge; }
            set { allCharge = value; }
        }
        private string costtypeID;

        public string CosttypeID
        {
            get { return costtypeID; }
            set { costtypeID = value; }
        }
    }
}