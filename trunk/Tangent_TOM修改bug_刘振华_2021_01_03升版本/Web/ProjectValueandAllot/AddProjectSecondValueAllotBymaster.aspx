﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="AddProjectSecondValueAllotBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.AddProjectSecondValueAllotBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectManage.css" rel="stylesheet" type="text/css" />
    <%--    <link href="../css/ProjectValueandAllot.css" rel="stylesheet" type="text/css" />--%>
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script type="text/javascript" src="../js/jquery.cookie.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/MessageComm.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ProjectValueandAllot/AddProjectSecondValueAllotBymaster.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>二次产值分配</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>产值分配<i class="fa fa-angle-right"> </i>二次产值分配</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i><font><font>项目信息</font></font>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h3 class="form-section">项目信息</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 15%;">项目名称:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">合同关联:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblReletive" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">管理级别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">审核级别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目类别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblBuildType" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">建设规模:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblScale" runat="server" Width="100px"></asp:Label>㎡
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">承接部门:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblcpr_Unit" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">项目总负责:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblPMName" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目阶段:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">合同额:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblCoperationAmount" runat="server" Text=""></asp:Label>万元
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">二次分配可用金额:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblNotAllotAccount" runat="server"></asp:Label>元
                                        </td>

                                        <td style="width: 15%;">已分配金额:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblAllotAccount" runat="server" Text=""></asp:Label>元
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td style="width: 15%;">二次分配总金额:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblTotalCount" runat="server" Width="100px"></asp:Label>元
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">其他参与部门:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i><font><font>项目分配表填写</font></font>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="cls_ProjAllot_Table" style="width: 100%;">
                                <tr>
                                    <td>项目分配年份：
                                    </td>
                                    <td style="text-align: center">
                                        <asp:DropDownList ID="drp_year" runat="server" AppendDataBoundItems="true" Width="100%"
                                            CssClass="form-control input-sm">
                                            <asp:ListItem Value="-1">--选择年份--</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>项目等级：
                                    </td>
                                    <td style="text-align: center">
                                        <asp:DropDownList ID="drp_buildtype" runat="server" Width="100%" AppendDataBoundItems="true"
                                            CssClass="form-control input-sm">
                                            <asp:ListItem Value="-1">------选择类别------</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%;">项目分配阶段：
                                    </td>
                                    <td style="width: 35%; text-align: center">
                                        <select id="stage" class="form-control input-sm" style="width: 100%;" name="D1">
                                            <option value="-1">-------请选择分配阶段-------</option>
                                            <option value="0">方案+初设+施工图+后期</option>
                                            <option value="1">方案+初设</option>
                                            <option value="10">方案+施工图+后期</option>
                                            <option value="2">施工图+后期</option>
                                            <option value="3">初设+施工图+后期</option>
                                            <option value="4">室外工程</option>
                                            <option value="5">锅炉房(4吨以下)</option>
                                            <option value="5">锅炉房(6吨以上)</option>
                                            <option value="6">地上单建水泵房</option>
                                            <option value="7">地上单建变配电所(室)</option>
                                            <option value="8">单建地下室（车库）</option>
                                            <%-- <option value="9">市政道路工程</option>--%>
                                        </select>
                                    </td>
                                    <td style="width: 15%;">项目分配金额：
                                    </td>
                                    <td style="width: 35%;" class="cls_ProjAllot_Input">
                                        <input id="txt_AllotAccount" maxlength="15" type="text" runat="server" />(元)
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" class="btn green" id="btnApproval" href="#AuditUser" data-toggle="modal">
                                <font><font>确定</font></font>
                            </button>
                            <button type="button" class="btn default" id="btnRefuse">
                                <font><font>返回</font></font>
                            </button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenProSysNo" value="<%=proSysNo %>" />
    <input type="hidden" id="HiddenAuditRecordSysNo" value="<%=ValueAllotAuditSysNo %>" />
    <input type="hidden" id="HiddenCprID" value="<%=CprID %>" />
    <input type="hidden" id="HiddenUnitName" value="<%=UnitName %>" />
    <div id="AuditUser" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default" id="btn_close">
                关闭</button>
        </div>
    </div>
</asp:Content>
