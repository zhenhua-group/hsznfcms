﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectIsEditBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.ProjectIsEditBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../css/CprChargStatus.css" rel="stylesheet" type="text/css" />
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script type="text/javascript" src="../js/MessageComm.js"></script>
    <script type="text/javascript" src="/js/jquery.alerts.js"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            CommonControl.SetFormWidth();
            CommonControl.SetTableStyle("td_input", "need");
          
            $("#ctl00_ContentPlaceHolder1_sureButton").click(function () {
                if ($("#ctl00_ContentPlaceHolder1_chk_no").get(0).checked) {
                    if ($.trim($("#txt_caiwu").val()) == "") {
                        alert("不通过原因不能为空！");
                        return false;
                    }
                }
            });
            //通过按钮
            $(".a").click(function () {

                $("#ctl00_ContentPlaceHolder1_chk_yes").attr("checked", "checked");
                $("#ctl00_ContentPlaceHolder1_chk_yes").parent().attr("class", "checked");
                $("#ctl00_ContentPlaceHolder1_chk_no").attr("checked", "");
                $("#ctl00_ContentPlaceHolder1_chk_no").parent().attr("class", "");

                $("#Caiwu").hide();
                $("#Suozhang").hide();
                $("#DanJu").show();

            });
            $(".b").click(function () {

                $("#Caiwu").show();
                $("#ctl00_ContentPlaceHolder1_chk_yes").attr("checked", "");
                $("#ctl00_ContentPlaceHolder1_chk_yes").parent().attr("class", "");
                $("#ctl00_ContentPlaceHolder1_chk_no").attr("checked", "checked");
                $("#ctl00_ContentPlaceHolder1_chk_no").parent().attr("class", "checked");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>特殊申请修改产值系数</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>产值分配<i class="fa fa-angle-right"> </i>特殊申请修改产值系数</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>特殊申请修改产值系数
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table table-bordered table-hover" style="margin: 0 auto;" id="td_input">
                        <tr>
                            <td style="width: 10%">项目名称：
                            </td>
                            <td>
                                <asp:Label Text="" runat="server" ID="proname" />
                            </td>
                            <td style="width: 10%">承接部门：
                            </td>
                            <td>
                                <asp:Label Text="" runat="server" ID="CJBM" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 120px;">特殊申请原因：
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lbl_Why" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>是否通过：
                            </td>
                            <td colspan="3" class="TextBoxBorder">

                                <asp:RadioButton ID="chk_yes" runat="server" Text="是" Checked="true" GroupName="A" CssClass="a" />
                                <asp:RadioButton ID="chk_no" runat="server" Text="否" GroupName="A" CssClass="b" />
                            </td>
                        </tr>
                        <tr id="Caiwu" style="display: none;">
                            <td>不通过原因：
                            </td>
                            <td colspan="3" class="TextBoxBorder">
                                <asp:TextBox ID="txt_suozhang" runat="server" MaxLength="100" TextMode="MultiLine"
                                    Height="50" Width="98%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="Suozhang" style="display: none;">
                            <td>不通过原因：
                            </td>
                            <td colspan="3" class="TextBoxBorder">
                                <asp:TextBox ID="txt_caiwu" runat="server" MaxLength="100" TextMode="MultiLine" Height="50"
                                    Width="98%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="sureButton" runat="server" CssClass="btn green" OnClick="sureButton_Click" Text="确定" />
                                <a href="javascript:history.back();" class="btn default">返回</a>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
