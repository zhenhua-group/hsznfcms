﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjectValueAuditTranHavcHeadBymaster : PageBase
    {
        /// <summary>
        /// 项目系统编号
        /// </summary>
        public int proSysNo
        {
            get
            {
                int proSysNo = 0;
                int.TryParse(Request["proID"], out proSysNo);
                return proSysNo;
            }
        }


        public int allotID
        {
            get
            {
                int allotID = 0;
                int.TryParse(Request["allotID"], out allotID);
                return allotID;
            }
        }
        /// <summary>
        /// 审核记录系统编号
        /// </summary>
        public int ValueAllotAuditSysNo
        {
            get
            {
                int valueAllotAuditSysNo = 0;
                int.TryParse(Request["ValueAllotAuditSysNo"], out valueAllotAuditSysNo);
                return valueAllotAuditSysNo;
            }
        }
        /// <summary>
        /// 是否四舍五入
        /// </summary>
        public string IsRounding = ConfigurationManager.AppSettings["IsRounding"] ?? "0";

        /// <summary>
        /// tg中项目
        /// </summary>
        public int ProReferenceSysNo { get; set; }

        public string IsDone { get; set; }

        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }

        /// <summary>
        /// 产值类型
        /// </summary>
        public string proType
        {
            get
            {
                return Request["proType"];
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                //获取项目信息
                getProInfo();

                getProjectValueAllotInfo();

                getMemberValue();
                //判断该审批是否完成
                IsDone = new TG.BLL.cm_SysMsg().IsDone(MessageID);
            }
        }
        /// <summary>
        /// 项目信息
        /// </summary>
        private void getProInfo()
        {

            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(proSysNo);

            if (pro_model != null)
            {
                //项目名称
                lblProjectName.Text = pro_model.pro_name;
                //关联合同
                this.lblReletive.Text = pro_model.Project_reletive;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }

                //建设规模
                this.lblScale.Text = pro_model.ProjectScale.ToString();
                //建筑类别
                lblBuildType.Text = pro_model.BuildType;
                //项目总负责
                this.lblPMName.Text = pro_model.PMName;
                //承接部门
                lblcpr_Unit.Text = pro_model.Unit;

                //项目阶段
                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }

                ProReferenceSysNo = pro_model.ReferenceSysNo;

                //合同额
                lblCoperationAmount.Text = pro_model.Cpr_Acount.ToString();
                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }

            }

        }

        /// <summary>
        /// 得到分配信息
        /// </summary>
        private void getProjectValueAllotInfo()
        {
            TG.BLL.cm_TranProjectValueAllot BLL = new TG.BLL.cm_TranProjectValueAllot();
            TG.Model.cm_TranProjectValueAllot modelTran = BLL.GetModelByProID(proSysNo, allotID);

            if (modelTran != null)
            {
                lblTotalCount.Text = modelTran.TotalCount.ToString();
                decimal TranCount = 0;
                decimal AllotCount = 0;
                if (!string.IsNullOrEmpty(modelTran.TranCount.ToString()))
                {
                    TranCount = decimal.Parse(modelTran.TranCount.ToString());
                }
                if (!string.IsNullOrEmpty(modelTran.AllotCount.ToString()))
                {
                    AllotCount = decimal.Parse(modelTran.AllotCount.ToString());
                }
                txt_TranValuePercent.Value = modelTran.Tranpercent.ToString();
                txt_TranValueCount.InnerHtml = TranCount.ToString();
                txtTheDeptValuePercent.Value = modelTran.Thedeptallotpercent.ToString();
                txtTheDeptValueCount.InnerHtml = modelTran.Thedeptallotcount.ToString();
                txt_AllotValuePercent.Value = modelTran.AllotPercent.ToString();
                txtAllotValueCount.InnerHtml = AllotCount.ToString();
                string year = modelTran.ActualAllountTime;
                if (!string.IsNullOrEmpty(year))
                {
                    if (this.drp_year.Items.FindByText(year.Trim()) != null)
                    {
                        this.drp_year.Items.FindByText(year.Trim()).Selected = true;
                    }
                }

            }

        }

        /// <summary>
        /// 得到人员产值
        /// </summary>
        /// <returns></returns>
        private void getMemberValue()
        {
            TG.BLL.cm_TranProjectValueAllot BLL = new TG.BLL.cm_TranProjectValueAllot();
            DataTable dtMember = new DataTable();
            dtMember = BLL.GetTranProAuditedUser(proSysNo, "nts", allotID).Tables[0];

            StringBuilder sb = new StringBuilder();
            sb.Append(" <div id=\"tbProjectValueBymember\" class=\"cls_Container_Report\" >");
            sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"><tr> ");

            sb.Append("<td style=\"text-align: center;\"> 暖通策划人员产值分配比例% &nbsp;(<span style=\"color:red\">注释：所有人员的比例之和要为100%（包括设计、专业负责、审核、校对）</span>) </td> </tr>");

            sb.Append("</table>");
            sb.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sb.Append("<tr ><td style=\"width: 10%; text-align: center;height:15px;\">  名称</td><td style=\"width: 10%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sb.Append(" <tr> <td style=\"width: 10%; text-align: center;\" > 专业 </td><td style=\"width: 10%; text-align: center;\">   姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
            sb.Append(" </table>");
            sb.Append("<table id=\"gvProjectValueBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
            if (dtMember.Rows.Count > 0)
            {
                sb.Append("<tr>");

                sb.Append("<td  width= \"10%\" class=\"cls_Column\" rowspan=\"" + dtMember.Rows.Count + "\">暖通</td>");

                for (int j = 0; j < dtMember.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td class=\"display\"></td>");
                    }
                    string name = dtMember.Rows[j]["mem_Name"].ToString();

                    sb.Append("<td class=\"display\" wp=\"0\">" + dtMember.Rows[j]["mem_ID"].ToString() + "</td>");
                    sb.Append("<td width= \"10%\">" + dtMember.Rows[j]["mem_Name"].ToString() + "</td>");

                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"  value=" + dtMember.Rows[j]["DesignPercent"].ToString() + "  runat=\"server\" runat=\"server\" sz=0 />%</td>");
                    sb.Append("<td width= \"10%\">" + dtMember.Rows[j]["DesignCount"].ToString() + "</td>");
                    sb.Append("<td class=\"display\" >0</td>");


                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\" value=" + dtMember.Rows[j]["SpecialtyHeadPercent"].ToString() + "  runat=\"server\" />%</td>");
                    sb.Append("<td width= \"10%\">" + dtMember.Rows[j]["SpecialtyHeadCount"].ToString() + "</td>");
                    sb.Append("<td class=\"display\" >0</td>");

                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" value=" + dtMember.Rows[j]["AuditPercent"].ToString() + " type=\"text\"   runat=\"server\" />%</td>");
                    sb.Append("<td width= \"10%\">" + dtMember.Rows[j]["AuditCount"].ToString() + "</td>");
                    sb.Append("<td class=\"display\" >0</td>");

                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\" value=" + dtMember.Rows[j]["ProofreadPercent"].ToString() + "  runat=\"server\" />%</td>");
                    sb.Append("<td width= \"10%\">" + dtMember.Rows[j]["ProofreadCount"].ToString() + "</td>");
                    sb.Append("<td class=\"display\" >0</td>");
                    sb.Append("<td class=\"display\" ></td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\" id=\"tbChooseUser\"><tr> ");
            sb.Append("<td><span id=\"chooseUser\"data-toggle=\"modal\" href=\"#chooseUserMainDiv\" style=\"color: Blue; cursor: pointer;\">添加人员</span>&nbsp;&nbsp;<span id=\"chooseExternalUser\"  data-toggle=\"modal\" href=\"#chooseExtUserDiv\" style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
            sb.Append("</tr></table>");
            sb.Append(" </div>");

            lblMember.Text = sb.ToString();
        }

        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
    }
}