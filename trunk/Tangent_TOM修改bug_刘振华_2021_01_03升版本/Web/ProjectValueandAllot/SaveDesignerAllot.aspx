﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="SaveDesignerAllot.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.SaveDesignerAllot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_green.js"></script>
    <link type="text/css" rel="stylesheet" href="../css/swfupload/default_cpr.css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_cpr.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/ProjectValueandAllot/SaveDesignerAllot.js"></script>
    <script type="text/javascript">
        $(function () {
            saveDesigner.init();
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导审核设计金额分配 <small>审核设计分配</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目管理</a><i class="fa fa-angle-right"> </i><a>领导审核设计费分配</a><i class="fa fa-angle-right">
    </i><a>审核设计分配</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>审核金额分配(<b><%= this.SpeName %></b>)
                    </div>
                    <div class="actions">
                        <a href="###" class="btn red btn-sm" id="btnAuditSave">保存</a>
                        <a href="/ProjectValueandAllot/AuditDesignValueCountListBymaster.aspx" class="btn default btn-sm">返回</a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" style="width: 450px;">
                            <thead>
                                <tr>
                                    <th colspan="3">审核总金额：<span id="divAllotCount" class="badge badge-danger"><%=ProjAuditAllotCount %></span>(元)</th>
                                </tr>
                                <tr>
                                    <th colspan="3">已分配：<span id="divAllotDone" class="badge badge-danger"><%= AuditAllotDone %></span>(元)</th>
                                </tr>
                                <tr>
                                    <th colspan="3">余额：<span id="divAllotHav" class="badge badge-danger"><%= AuditAllotHav %></span>(元)</th>
                                </tr>
                                <tr>
                                    <th style="width: 150px;">审核人
                                    </th>
                                    <th style="width: 150px;">分配金额(元)
                                    </th>
                                    <th style="width: 150px;">分配比例(%)
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="auditBody">
                                <asp:Literal ID="ltlAuditBody" runat="server"></asp:Literal>
                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>设计金额分配(<b><%= this.SpeName %></b>)
                    </div>
                    <div class="actions">
                        <a href="###" class="btn red btn-sm" id="btnDesignSave">保存</a>
                        <a href="/ProjectValueandAllot/AuditDesignValueCountListBymaster.aspx" class="btn default btn-sm">返回</a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" style="width: 450px;">
                            <thead>
                                <tr>
                                    <th colspan="3">设计金额：<span id="divDesigncount" class="badge badge-danger"><%=ProjDesignAllotCount %></span>(元)</th>
                                </tr>
                                <tr>
                                    <th colspan="3">已分配：<span id="divDesignDone" class="badge badge-danger"><%= DesignerAllotDone %></span>(元)</th>
                                </tr>
                                <tr>
                                    <th colspan="3">余额：<span id="divDesignHav" class="badge badge-danger"><%= DesignerAllotHav %></span>(元)</th>
                                </tr>
                                <tr>
                                    <th style="width: 150px;">设计人
                                    </th>
                                    <th style="width: 150px;">分配金额(元)
                                    </th>
                                    <th style="width: 150px;">分配比例(%)
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="designerBody">
                                <asp:Literal ID="ltlDesignBody" runat="server"></asp:Literal>
                            </tbody>

                        </table>
                        <hr />

                    </div>
                </div>
            </div>
            <!-- 保存基本信息 -->
            <input type="hidden" name="name" value="<%=ProjectSysNo %>" id="hidProjectSysNo" />
            <input type="hidden" name="name" value="<%=AllotYear %>" id="hidAllotyear" />
            <input type="hidden" name="name" value="<%=UserID %>" id="hidUserID" />
            <input type="hidden" name="name" value="<%=Action %>" id="hidaction" />
            <input type="hidden" name="name" value="<%=this.SpeID %>" id="hidspeid" />
        </div>
    </div>
</asp:Content>
