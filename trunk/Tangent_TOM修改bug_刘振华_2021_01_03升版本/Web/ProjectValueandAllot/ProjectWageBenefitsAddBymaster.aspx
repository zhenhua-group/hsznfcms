﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjectWageBenefitsAddBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.ProjectWageBenefitsAddBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesort.js"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script src="../js/ProjectValueandAllot/ProjectWageBenefits/ProjectWageBenefitsAdd.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>个人效益工资操作</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目信息管理</a><i class="fa fa-angle-right"> </i><a>个人效益工资</a><i class="fa fa-angle-right">
    </i><a>个人效益工资操作</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询个人效益工资
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>年份:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>
                            <td>姓名:</td>
                            <td>
                                <input type="text" class="form-control input-sm" id="txt_memberName" runat="server" /></td>
                            <td>
                                <input type="button" class="btn blue" value="查询" id="btn_Search" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>个人效益工资操作
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body  form" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <div id="ShowDiv" class="modal fade yellow" tabindex="-1" data-width="560" aria-hidden="true"
        style="display: none; width: 560px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">个人产值效益工资</h4>
        </div>
        <div class="modal-body">
            <div id="PopAreaDivMain">
                <table class="table table-bordered table-hover">
                    <tbody>
                        <tr>
                            <td style="width: 30%;">年份:</td>
                            <td style="width: 70%;"><span id="txt_year"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">姓名:</td>
                            <td style="width: 70%;"><span id="txt_name"></span><span id="txt_mem_id" style="display: none;"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">产值:</td>
                            <td style="width: 70%;"><span id="txt_Value"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">产值工资系数:</td>
                            <td style="width: 70%;">
                                <input type="text" id="txt_ValuePercent" class="form-control input-sm" /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">产值工资:</td>
                            <td style="width: 70%;"><span id="txt_ValueWage"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">所管人员工资:</td>
                            <td style="width: 70%;">
                                <input type="text" id="txt_PossessionMemWange" class="form-control input-sm" /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">全年工资:</td>
                            <td style="width: 70%;"><span id="txt_YearWange"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">资酬金工资比例:</td>
                            <td style="width: 70%;">
                                <input type="text" id="txt_RemunerationPercent" class="form-control input-sm" value="0.01" /></td>
                        </tr>

                        <tr>
                            <td style="width: 30%;">资酬金工资:</td>
                            <td style="width: 70%;"><span id="txt_RemunerationWange"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">税率:</td>
                            <td style="width: 70%;">
                                <input type="text" id="txt_TaxPercent" class="form-control input-sm" /></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">全年实际应交税额:</td>
                            <td style="width: 70%;"><span id="txt_YearTaxCount"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">应发工资:</td>
                            <td style="width: 70%;"><span id="txt_ShouldWange"></span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn blue btn-sm" id="btnSave">
                保存</button>
            <button type="button" data-dismiss="modal" class="btn btn-sm">
                关闭</button>
        </div>
    </div>
</asp:Content>
