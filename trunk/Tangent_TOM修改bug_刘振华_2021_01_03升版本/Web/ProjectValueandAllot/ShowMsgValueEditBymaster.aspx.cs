﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ShowMsgValueEditBymaster : System.Web.UI.Page
    {
        public string Reson { get; set; }
        public string ProUrl { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            Reson = Request.Params["Reson"];
            string proId = Request.Params["Proid"];
            string sysno = Request.Params["sysno"];
            string sql = @"SELECT ProjectUrlDetail FROM cm_ProjectVolltAuditEdit WHERE SysNo=" + sysno;
            object obj = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            if (obj != null)
            {
                ProUrl = obj.ToString();
            }
            else
            {
                ProUrl = "###";
            }
            TG.BLL.cm_Project probll = new TG.BLL.cm_Project();

            this.labNOPass.Text = Reson == "" ? "已通过" : "不通过 原因：" + Reson;
            this.proname.Text = probll.GetModel(int.Parse(proId)).pro_name;
            this.CJBM.Text = probll.GetModel(int.Parse(proId)).Unit;
        }
    }
}