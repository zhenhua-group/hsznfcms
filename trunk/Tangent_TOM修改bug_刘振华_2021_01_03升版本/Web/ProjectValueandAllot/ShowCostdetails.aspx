﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ShowCostdetails.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.ShowCostdetails" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#btn_back").click(function () {
                var unitid = $("#ctl00_ContentPlaceHolder1_HiddenUnitID").val();
                var typeid = $("#ctl00_ContentPlaceHolder1_HiddenTypeID").val();
                //window.location.href = "CostUnitDetailsBymaster.aspx?unitid=" + unitid + "&typeid=" + typeid;
                window.location.href = "CostUnitDetailsBymaster.aspx";
            });
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>所成本费用明细列表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>财务管理 </a><i class="fa fa-angle-right"></i><a>所成本费用明细列表</a> </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>所成本费用明细列表
                    </div>
                    <div class="actions">
                        <input type="button" class="btn red btn-sm" value="返回成本费用明细" id="btn_back" />
                    </div>
                </div>
            </div>
            <div class="portlet-body" style="display: block;">
                <div class="form-body">
                    <div class="row">
                        <div style="width: 98%; margin: 0 auto;">
                            <asp:GridView ID="grid_Financial" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                Width="100%" EnableModelValidation="True" CssClass="table table-bordered table-hover">
                                <Columns>
                                    <asp:BoundField DataField="costTimeS" HeaderText="日期">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="costNum" HeaderText="凭单号">
                                        <ItemStyle Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="costSub" HeaderText="明细">
                                        <ItemStyle Width="30%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="costCharge" HeaderText="金额(元)">
                                        <ItemStyle Width="10%" HorizontalAlign="left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="typeName" HeaderText="类别">
                                        <ItemStyle Width="10%" HorizontalAlign="left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="unitName" HeaderText="部门">
                                        <ItemStyle HorizontalAlign="left" Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="userName" HeaderText="录入人">
                                        <ItemStyle HorizontalAlign="left" Width="10%" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataTemplate>
                                    没有数据
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                CustomInfoSectionWidth="32%" CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                                CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                                PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                                SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                                PageSize="12" SubmitButtonClass="submitbtn">
                            </webdiyer:AspNetPager>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField runat="Server" ID="HiddenUnitID" Value="" />
    <asp:HiddenField runat="Server" ID="HiddenTypeID" Value="" />
</asp:Content>
