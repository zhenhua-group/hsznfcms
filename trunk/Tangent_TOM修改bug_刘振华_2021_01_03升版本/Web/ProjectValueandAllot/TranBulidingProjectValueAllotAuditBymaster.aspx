﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="TranBulidingProjectValueAllotAuditBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.TranBulidingProjectValueAllotAuditBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="../js/MessageComm.js" type="text/javascript"></script>
    <script src="../js/UserControl/ChooseProjectValueUser.js" type="text/javascript"></script>
    <script src="../js/UserControl/AddExternalMember.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../js/ProjectValueandAllot/TranBulidingProjectValueAllotAudit.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        function openValueWindow() {
            var openUrl = "PrintValue.aspx?type=trantjs&ProjectSysNo=" + $("#hidProID").val() + "&AllotId=" + $("#HidAllotID").val() + "&totalCount=" + $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text();

            var feature = "dialogWidth:960px;dialogHeight:550px;center:yes";

            window.showModalDialog(openUrl, '打印', feature);

        }
    </script>
    <style type="text/css">
        .display {
            display: none;
        }
        
        /* 表格基本样式*/
        .cls_show_cst_jiben {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }
            .cls_show_cst_jiben td {
            border: solid 1px #CCC;
            font-size: 12px;
            font-family: "微软雅黑";
        }
            .cls_show_cst_jiben tr {
            height: 22px;
        }
        .cls_content_head {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }
            .cls_content_head td {
            height: 20px;
            background-color: #E6E6E6;
            border: 1px solid Gray;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>转土建所产值分配审核</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>产值分配<i class="fa fa-angle-right"> </i>转土建所产值分配审核</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><font><font>项目信息</font></font>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <h3 class="form-section">项目信息</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                            <tr>
                                                <td style="width: 15%;">项目名称:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">合同关联:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblReletive" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">管理级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <input id="radio_yuan" name="aa" type="radio" checked="false" runat="server" value="0" />院管
                                                    <input id="radio_suo" checked="false" name="aa" type="radio" runat="server" value="1" />所管
                                                </td>
                                                <td style="width: 15%;">审核级别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <span id="yg" style="display: none" runat="server">
                                                        <input id="audit_yuan" name="bb" type="checkbox" runat="server" value="0" />院审
                                                    </span><span id="sg" style="display: none" runat="server">
                                                        <input id="audit_suo" name="bb" type="checkbox" runat="server" value="1" />所审
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目类别:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblBuildType" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">建设规模:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblScale" runat="server" Width="100px"></asp:Label>㎡
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">承接部门:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblcpr_Unit" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">项目总负责:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblPMName" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目阶段:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:CheckBox ID="CheckBox1" runat="server" Text="方案设计" CssClass="cls_jd" Enabled="false" />
                                                    <asp:CheckBox ID="CheckBox2" runat="server" Text="初步设计" CssClass="cls_jd" Enabled="false" />
                                                    <asp:CheckBox ID="CheckBox3" runat="server" Text="施工图设计" CssClass="cls_jd" Enabled="false" />
                                                    <asp:CheckBox ID="CheckBox4" runat="server" Text="其他" CssClass="cls_jd" Enabled="false" />
                                                </td>
                                                <td style="width: 15%;">合同额:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblCoperationAmount" runat="server" Text=""></asp:Label>万元
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">其他参与部门:
                                                </td>
                                                <td style="width: 35%;" colspan="3">
                                                    <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">项目分配年份:
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblYear" runat="server" Text=""></asp:Label>
                                                    (年)
                                                </td>
                                                <td style="width: 15%;">转土建所金额 :
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblTotalCount" runat="server" Text=""></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><font><font>转土建项目分配表填写</font></font>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Literal ID="lblNotData" runat="server"></asp:Literal>
                                    <div style="margin-top: 5px;" class="cls_data">
                                        <table class="cls_ProjAllot_Table" style="width: 100%;">
                                            <tr>
                                                <td style="width: 15%;">项目分配阶段：
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="lblStage" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">项目分配金额：
                                                </td>
                                                <td style="width: 35%;" class="cls_ProjAllot_Input">
                                                    <asp:Label ID="lblAllotAmount" runat="server" Text="Label"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="cls_ProjAllotHead" width="100%">
                                            <tr>
                                                <td align="center" style="width: 15%;">序号
                                                </td>
                                                <td align="center" style="width: 35%;">内容
                                                </td>
                                                <td align="center" style="width: 15%;">比例
                                                </td>
                                                <td align="center" style="width: 35%;">产值
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" width="100%">
                                            <tr>
                                                <td style="width: 15%;">1
                                                </td>
                                                <td style="width: 35%;">设总 &nbsp;
                                                    <asp:Label ID="lblDesignName" runat="server"></asp:Label>
                                                </td>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="txt_DesginPercent" runat="server" Text="Label"></asp:Label>
                                                    (%)
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txt_DesignCount" runat="server" Text="Label"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">2
                                                </td>
                                                <td style="width: 35%;">本部门自留产值
                                                </td>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="txtTheDeptValuePercent" runat="server" Text="Label"></asp:Label>
                                                    (%)
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:Label ID="txtTheDeptValueCount" runat="server" Text="Label"></asp:Label>
                                                    (元)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3
                                                </td>
                                                <td>应&nbsp;分&nbsp;产&nbsp;值
                                                </td>
                                                <td>
                                                    <asp:Label ID="txt_ShouldBeValuePercent" runat="server" Text="Label"></asp:Label>
                                                    (%)
                                                </td>
                                                <td>
                                                    <asp:Label ID="txt_ShouldBeValueCount" runat="server"></asp:Label>(元)
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--评审内容-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><font><font>产值分配系数</font></font>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="stageamountOne" class="cls_Container_Report" style="display: none;">
                                        <table class="cls_ProjAllot_Table" style="width: 98%;">
                                            <tr>
                                                <td>项目各设计阶段产值分配比例%
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                            <tr>
                                                <td width="10%" style="text-align: center;">项目类别
                                                </td>
                                                <td colspan="2" style="width: 18%; text-align: center;">方案设计
                                                </td>
                                                <td colspan="2" style="width: 18%; text-align: center;">初设设计
                                                </td>
                                                <td colspan="2" style="width: 18%; text-align: center;">施工图设计
                                                </td>
                                                <td colspan="2" style="width: 18%; text-align: center;">后期服务
                                                </td>
                                                <td colspan="2" style="width: 18%; text-align: center;">合计
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:GridView ID="gvStageOne" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                            Width="98%" CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label">
                                            <Columns>
                                                <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>
                                                            <%# Eval("ProgramPercent") %></span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ProgramAmount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>
                                                            <%# Eval("preliminaryPercent") %></span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="preliminaryAmount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>
                                                            <%# Eval("WorkDrawPercent") %></span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="WorkDrawAmount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>
                                                            <%# Eval("LateStagePercent") %>
                                                        </span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="LateStageAmount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>100</span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="9%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="9%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                没有信息!
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <div id="stageamountTwo" class="cls_Container_Report" style="display: none;">
                                        <table class="cls_ProjAllot_Table" style="width: 98%;">
                                            <tr>
                                                <td>项目(方案+初步设计)二阶段产值分配比例%
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                            <tr>
                                                <td style="text-align: center; width: 10%;">项目类别
                                                </td>
                                                <td style="text-align: center; width: 30%;">方案比例
                                                </td>
                                                <td style="text-align: center; width: 30%;">初设比例
                                                </td>
                                                <td style="width: 30%; text-align: center;">闭合
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:GridView ID="gvStageTwo" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                            CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                            <Columns>
                                                <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>
                                                            <%# Eval("ProgramPercent") %></span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ProgramAmount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>
                                                            <%# Eval("preliminaryPercent")%></span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="preliminaryAmount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>100</span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                没有信息!
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <div id="stageamountThree" class="cls_Container_Report" style="display: none;">
                                        <table class="cls_ProjAllot_Table" style="width: 98%;">
                                            <tr>
                                                <td>项目(施工图设计+后期服务)二阶段产值分配比例%
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                            <tr>
                                                <td style="text-align: center; width: 10%;">项目类别
                                                </td>
                                                <td style="text-align: center; width: 30%;">施工图
                                                </td>
                                                <td style="text-align: center; width: 30%;">后期服务
                                                </td>
                                                <td style="width: 30%; text-align: center;">闭合
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:GridView ID="gvStageThree" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                            CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                            <Columns>
                                                <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>
                                                            <%# Eval("WorkDrawPercent") %></span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="WorkDrawAmount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>
                                                            <%# Eval("LateStagePercent") %>
                                                        </span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="LateStageAmount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>100</span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                没有信息!
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <div id="stageamountFour" class="cls_Container_Report" style="display: none;">
                                        <table class="cls_ProjAllot_Table" style="width: 98%;">
                                            <tr>
                                                <td>项目(初步设计+施工图设计+后期服务)二阶段产值分配比例%
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="cls_ProjAllotHead" id="" style="width: 98%;">
                                            <tr>
                                                <td style="text-align: center; width: 10%;">项目类别
                                                </td>
                                                <td style="text-align: center; width: 22.5%;">初设设计
                                                </td>
                                                <td style="text-align: center; width: 22.5%;">施工图设计
                                                </td>
                                                <td style="text-align: center; width: 22.5%;">后期服务
                                                </td>
                                                <td style="width: 22.5%; text-align: center;">合计
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:GridView ID="gvStageFour" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                            CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                            <Columns>
                                                <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>
                                                            <%# Eval("preliminaryPercent")%></span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="preliminaryAmount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>
                                                            <%# Eval("WorkDrawPercent") %></span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="WorkDrawAmount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>
                                                            <%# Eval("LateStagePercent")%></span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="LateStageAmount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <span>100</span>%
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                    <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                没有信息!
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                     <div id="stageamountTen" class="cls_Container_Report" style="display: none;">
                                                            <table class="cls_ProjAllot_Table" style="width: 98%;">
                                                                <tr>
                                                                    <td>项目(方案设计+施工图设计+后期服务)二阶段产值分配比例%
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table class="cls_ProjAllotHead" id="Table2" style="width: 98%;">
                                                                <tr>
                                                                    <td style="text-align: center; width: 10%;">项目类别
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">方案设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">施工图设计
                                                                    </td>
                                                                    <td style="text-align: center; width: 22.5%;">后期服务
                                                                    </td>
                                                                    <td style="width: 22.5%; text-align: center;">合计
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:GridView ID="gvStageTen" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                                CssClass="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" Width="98%">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ItemType" ItemStyle-CssClass="cls_Column">
                                                                        <ItemStyle Width="10%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("ProgramPercent")%></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ProgramAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("WorkDrawPercent") %></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="WorkDrawAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>
                                                                                <%# Eval("LateStagePercent")%></span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="LateStageAmount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <span>100</span>%
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="11.25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="TotalCount" DataFormatString="{0:F0}">
                                                                        <ItemStyle Width="11.25%" HorizontalAlign="Center" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                    没有信息!
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                    <asp:Literal ID="lbl_SpeAmount" runat="server"></asp:Literal>
                                    <asp:Literal ID="lbl_outAmount" runat="server"></asp:Literal>
                                    <asp:Literal ID="lbl_ProcessAmount" runat="server"></asp:Literal>
                                    <asp:Literal ID="lbl_processAmountFiveTable" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><font><font>策划人员信息</font></font>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Literal ID="lblMember" runat="server"></asp:Literal>
                                    <!--评审内容-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane  active">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-reorder"></i><font><font>评审内容</font></font>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body" style="display: block;">
                            <div class="row">
                                <div class="col-md-12">
                                    <table style="width: 98%; height: auto; display: none;" class="cls_content_head"
                                        id="tbTopAudit">
                                        <tr>
                                            <td style="width: 15%; text-align: center;">评审部门
                                            </td>
                                            <td style="width: 70%; text-align: center;">评审内容
                                            </td>
                                            <td style="width: 15%; text-align: center;">评审人/日期
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 98%; height: auto; margin: auto; : none;" class="cls_show_cst_jiben"
                                        id="OneTable" audittable="auditTable">
                                        <tr>
                                            <td style="width: 15%; text-align: center;">生产经营部
                                            </td>
                                            <td style="width: 70%; text-align: center;">
                                                <textarea style="width: 98%; height: 60px;" id="OneSuggstion" class="TextBoxBorder"></textarea>
                                            </td>
                                            <td style="width: 15%; text-align: center;" id="AuditUser"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="tbAudit">
                <tr>
                    <td style="width: 100%; text-align: center;">
                        <input type="button" id="btnPass" name="controlBtn" class="btn green btn-default"
                            value="通过" />
                        <input type="button" id="btnNotPass" name="controlBtn" class="btn red btn-default"
                            value="不通过" />
                        <input id="btnPrintValue" type="button" onclick="openValueWindow();" value="打印产值"
                            class="btn purple btn-default" style="" />
                        <input type="button" id="btnRefuse" name="controlBtn" class="btn  btn-default" value="返回"
                            onclick="javascript: history.back();" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenOneSuggesiton" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.OneSuggestion %>" />
    <input type="hidden" id="HiddenStatus" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.Status %>" />
    <input type="hidden" id="HiddenAuditUser" value="<%=AuditUser %>" />
    <input type="hidden" id="HiddenAuditData" value="<%=projectAuditRecordEntity ==null?"":projectAuditRecordEntity.AuditDate %>" />
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <input type="hidden" id="hidProID" value="<%= proSysNo %>" />
    <input type="hidden" id="HidAllotID" value="<%= allotID %>" />
    <input type="hidden" id="HiddenSysNo" value="<%= ValueAllotAuditSysNo %>" />
    <input type="hidden" id="hiddenType" value="<%= Type %>" />
    <input type="hidden" id="hiddenTotalAcount" value="<%= TotalCount %>" />
    <input type="hidden" id="HiddenCoperationProcess" value="<%=CoperationProcess %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
</asp:Content>
