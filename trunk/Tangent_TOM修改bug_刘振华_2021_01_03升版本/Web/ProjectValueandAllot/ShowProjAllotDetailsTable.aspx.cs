﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using TG.Model;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ShowProjAllotDetailsTable : System.Web.UI.Page
    {
        TG.BLL.CommClassOperator bll_comm = new TG.BLL.CommClassOperator();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string proid = Request.QueryString["proid"] ?? "0";



                string costid = Request.QueryString["costid"] ?? "0";
                string cprid = Request.QueryString["cprid"] ?? "0";
                //生成数据表
                this.Literal1.Text = CreatTable(proid, cprid, costid);
            }
        }
        //创建完整配置表
        protected string CreatTable(string proid, string cprid, string costid)
        {
            //声明创建Table
            StringBuilder sb_tb = new StringBuilder();
            sb_tb.Append("<table id=\"UserAllotDetails\" class=\"cls_UserAllot\">");
            //创建第一部分
            sb_tb.Append(CreatePartOneTableData(proid, cprid, costid));
            //创建第二部分
            sb_tb.Append(CreatePartTwoTableData(costid));
            //创建第三部分
            sb_tb.Append(CreatePartThreeTableData(proid, costid));
            //结束
            sb_tb.Append("</table>");
            return sb_tb.ToString();
        }
        //生成第一部分数据
        protected string CreatePartOneTableData(string proid, string cprid, string costid)
        {
            //查询得到项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(int.Parse(proid));

            //合同信息
            TG.BLL.cm_Coperation cpr = new TG.BLL.cm_Coperation();
            TG.Model.cm_Coperation cpr_model = cpr.GetModel(int.Parse(cprid));
            //第一部分信息
            TG.BLL.ProjectAllotBP bll_allot = new TG.BLL.ProjectAllotBP();
            TG.Model.PartOne model_one = bll_allot.GetPartOneRecord(int.Parse(costid));
            if (model_one == null)
                return "";
            //字典信息
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            TG.Model.cm_Dictionary model_dic = new TG.Model.cm_Dictionary();
            //创建表格
            StringBuilder sb_row = new StringBuilder();
            int icolum = 11;
            string[] configs = new string[] { "1|9" };
            string[] rowconfigs = new string[] { };
            string[] htmls = new string[] { "<div class=\"cls_tb_title\">设计项目费用分配统计总表<div id=\"title_prt\"></div></div>", "<div class=\"cls_year\" id=\"cls_cell_year\"></div>", "<div class=\"cls_cell_jibie\" id=\"jibie\"></div>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));

            configs = new string[] { "4|5", "10|2" };
            rowconfigs = new string[] { };
            htmls = new string[] { "项目经理人", "<div class=\"cls_cell_red\" id=\"proj_mng\">" + cpr_model.ChgJia + "</div>", "建设单位", "<div class=\"cls_cell_red\" id=\"proj_unit\">" + cpr_model.cpr_Unit + "</div>", "出图日期", "<div class=\"cls_cell_red\" id=\"out_date\">" + model_one.InDate == null ? "" : model_one.InDate + "</div>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));

            configs = new string[] { "1|11" };
            rowconfigs = new string[] { };
            htmls = new string[] { "<div class=\"cls_cell_td_jy\">项目信息表<span class=\"cls_cell_small\">(本表经营部填写)</span></div>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));

            configs = new string[] { "2|7", "10|2" };
            rowconfigs = new string[] { };
            htmls = new string[] { "工程名称", "<div class=\"cls_cell_red\">" + pro_model.pro_name + "</div>", "合同编号", "<div class=\"cls_cell_red\">" + cpr_model.cpr_No + "</div>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));
            //设计编号
            string str_dsignNo = new TG.BLL.cm_projectNumber().GetProjectJobNumberByProjectSysNo(int.Parse(proid));
            configs = new string[] { "10|2" };
            rowconfigs = new string[] { };
            htmls = new string[] { "楼号", "", "", "", "", "", "", "合计", "设计编号", "<div class=\"cls_cell_red\">" + str_dsignNo + "</div>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));

            configs = new string[] { };
            rowconfigs = new string[] { };
            htmls = new string[] { "建筑面积", "<div class=\"cls_cell_red\">" + pro_model.ProjectScale + "</div>", "", "", "", "", "", "<div class=\"cls_cell_blue\" id=\"proj_count\">" + pro_model.ProjectScale + "</div>", "", "(元/㎡)", "(元)" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));
            //建筑规模
            string str_gm = "大型";
            configs = new string[] { };
            rowconfigs = new string[] { };
            //结构类型
            model_dic = bll_dic.GetModel(Convert.ToInt32(model_one.StrucType));
            string structType = "无";
            if (model_dic != null)
            {
                structType = model_dic.dic_Name;
            }
            htmls = new string[] { "<div class=\"cls_cell\">合同收费<br>(万元)</div>", "<div class=\"cls_cell_blue\">" + model_one.CprCharge + "</div>", "<div class=\"cls_cell\">本次收费<br>(万元)</div>", "<div class=\"cls_cell_red\">" + model_one.CurChange + "</div>", "建筑规模", "<div class=\"cls_cell_red\">" + str_gm + "</div>", "结构形式", "<div class=\"cls_cell_red\">" + structType + "</div>", "最低收费标准<br>分成基数", "<div class=\"cls_cell_red\">" + model_one.LowDivUnitArea + "</div>", "<div class=\"cls_cell_blue\">" + model_one.LowDivUnitPrice + "</div>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));
            //工程等级
            string dj = pro_model.pro_level.ToString();
            if (dj == "1")
                dj = "一级";
            if (dj == "2")
                dj = "二级";
            if (dj == "3")
                dj = "三级";

            configs = new string[] { };
            rowconfigs = new string[] { };
            htmls = new string[] { "<div class=\"cls_cell\">合同单价<br>(元/㎡)</div>", "<div class=\"cls_cell_red\">" + model_one.CprUnitprice + "</div>", "本次费率", "<div class=\"cls_cell_blue\">" + model_one.CurRate + "</div>", "工程等级", "<div class=\"cls_cell_red\">" + dj + "</div>", "税率", "" + model_one.TaxRate + "", "最低收费标准<br>税后分成基数", "<div class=\"cls_cell_blue\">" + model_one.LowRateUnitArea + "</div>", "<div class=\"cls_cell_blue\">" + model_one.LowRateUntiPrice + "</div>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));
            //返回
            return sb_row.ToString();
        }
        //生成第二部分数据
        protected string CreatePartTwoTableData(string costid)
        {
            //第二部分数据
            TG.Model.PartTwo model_two = new TG.BLL.ProjectAllotBP().GetPartTwoRecord(int.Parse(costid));
            if (model_two == null)
                return "";
            StringBuilder sb_row = new StringBuilder();

            int icolum = 11;
            string[] configs = new string[] { "1|11" };
            string[] rowconfigs = new string[] { };
            string[] htmls = new string[] { "<div class=\"cls_cell_td_cw\">各项费用控制明细表<span class=\"cls_cell_small\">(本表为财务提供支付控制)</span></div>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));

            configs = new string[] { };
            rowconfigs = new string[] { };
            htmls = new string[] { "", "税后单项<br/>(底限)", "院务管理<br/>(含超)", "项目费用<br/>(含超)", "项目负责", "注册师<br/>A", "设校人员<br/>B&nbsp;C", "校审核定<br>D&nbsp;E", "总工", "院长", "合计" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));

            configs = new string[] { };
            rowconfigs = new string[] { };
            htmls = new string[] { "本次发放比例", "<div class=\"cls_cell_cw_red\">" + model_two.TaxSignel + "%</div>", "" + model_two.CollegeTax + "%", "" + model_two.ProjectTax + "%", "" + model_two.ProChgTax + "%", "" + model_two.RegisterTax + "%", "" + model_two.ShexTax + "%", "" + model_two.ShedTax + "%", "" + model_two.ZonggongTax + "%", "" + model_two.YuanzTax + "%", "" + model_two.SumTax + "%" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));

            configs = new string[] { };
            rowconfigs = new string[] { };
            htmls = new string[] { "分成税后金额(元)", "<div class=\"cls_cell_cw_blue\">" + model_two.TaxSignelRate + "</div>", "<div class=\"cls_cell_cw_blue\">" + model_two.CollegeRate + "</div>", "<div class=\"cls_cell_cw_blue\">" + model_two.ProjectRate + "</div>", "<div class=\"cls_cell_cw_blue\">" + model_two.ProChgRate + "</div>", "<div class=\"cls_cell_cw_blue\">" + model_two.RegisterRate + "</div>", "<div class=\"cls_cell_cw_blue\">" + model_two.ShexRate + "</div>", "<div class=\"cls_cell_cw_blue\">" + model_two.ShedRate + "</div>", "<div class=\"cls_cell_cw_blue\">" + model_two.ZonggongRate + "</div>", "<div class=\"cls_cell_cw_blue\">" + model_two.YuanzRate + "</div>", "<div class=\"cls_cell_cw_blue\">" + model_two.SumRate + "</div>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));

            configs = new string[] { };
            rowconfigs = new string[] { };
            htmls = new string[] { "人员", "", "<div class=\"cls_cell\">" + model_two.TaxSignelUser + "</div>", "<div class=\"cls_cell\">" + model_two.CollegeUser + "</div>", "<div class=\"cls_cell\">" + model_two.ProjectUser + "</div>", "<div class=\"cls_cell\">" + model_two.ProChgUser + "</div>", "<div class=\"cls_cell\"></div>", "<div class=\"cls_cell\"></div>", "<div class=\"cls_cell\"></div>", "<div class=\"cls_cell\">" + model_two.ZonggongUser + "</div>", "<div class=\"cls_cell\">" + model_two.YuanzUser + "</div>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));

            configs = new string[] { };
            rowconfigs = new string[] { };
            htmls = new string[] { "签字", "<div class=\"cls_cell\"></span>", "<div class=\"cls_cell\"></span>", "<div class=\"cls_cell\"></span>", "<div class=\"cls_cell\"></span>", "<div class=\"cls_cell\"></span>", "<div class=\"cls_cell\"></span>", "<div class=\"cls_cell\"></span>", "<div class=\"cls_cell\"></span>", "<div class=\"cls_cell\"></span>", "<div class=\"cls_cell\"></span>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));

            return sb_row.ToString();
        }
        //生成第三部分数据
        protected string CreatePartThreeTableData(string proid, string costid)
        {
            StringBuilder sb_row = new StringBuilder();

            sb_row.Append(CreateAllotHeader(costid));
            sb_row.Append(CreateNumalRowCollection(proid, costid));
            sb_row.Append(CreateAllotBottom(costid));
            //返回数据
            return sb_row.ToString();
        }

        /// <summary>
        /// 穿件表单头
        /// </summary>
        /// <returns></returns>
        protected string CreateAllotHeader(string costid)
        {
            //第三部分的数据
            TG.Model.PartThree model_three = new TG.BLL.ProjectAllotBP().GetPartThreeRecord(int.Parse(costid));
            if (model_three == null)
                return "";
            string[] array_prt = model_three.ProfessionalPercent.Split(new char[] { ',' }, StringSplitOptions.None);
            string[] array_pay = model_three.ABCDPay.Split(new char[] { ',' }, StringSplitOptions.None);
            string[] array_spe = model_three.SpecialtyPayPercent.Split(new char[] { ',' }, StringSplitOptions.None);
            string[] array_design = model_three.DesignPay.Split(new char[] { ',' }, StringSplitOptions.None);


            StringBuilder sb_row = new StringBuilder();
            int icolum = 11;
            string[] configs_header = { "1|11" };
            string[] rowconfigs_header = { };
            string[] htmls_header = { "<div class=\"cls_tb_header\">设计、校核及专业注册、审核、审定人员设计报酬分配表(本表由生产部填写）</div>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs_header, rowconfigs_header, htmls_header));

            string[] configs = { "1|2", "3|4", "7|2" };
            string[] rowconfigs = { };
            string[] htmls = { "<span class=\"cls_cellheader\"></span>", "设计人员<br/>B", "<div style=\"width:120px;\">校核人员<br/>C</div>", "专业注册人员<br/>A", "审核人员<br/>D", "审定人员<br/>E" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));
            ////two
            TG.Model.cm_CostUserConfig cfg_cost = GetAllotUserConfig();
            string[] htmls2 = { "设计校审分成比例", "<span ids=\"partThreeDesignPercentTextBox\" class=\"cls_cell_label\" id=\"sp_disgnuser\" rel=\"" + array_prt[0] + "\">" + array_prt[0] + "</span>", "<span style=\"\" ids=\"partThreeDesignPercentTextBox\" class=\"cls_cell_label\" id=\"sp_xiaodui\" rel=\"" + array_prt[1] + "\">" + array_prt[1] + "</span>", "<span style=\"\" ids=\"partThreeDesignPercentTextBox\" class=\"cls_cell_label\" id=\"sp_zhuce\" rel=\"" + array_prt[2] + "\">" + array_prt[2] + "</span>", "<span ids=\"partThreeDesignPercentTextBox\" style=\"\" class=\"cls_cell_label\" id=\"sp_shenh\" rel=\"" + array_prt[3] + "\">" + array_prt[3] + "</span>", "<span style=\"\" ids=\"partThreeDesignPercentTextBox\" class=\"cls_cell_label\" id=\"sp_shend\" rel=\"" + cfg_cost.AuditDPrt + "\">" + cfg_cost.AuditDPrt + "%</span>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls2));
            ////three
            string[] configs2 = { "3|4", "7|2" };
            string[] htmls3 = { "设计报酬<br/>折合元/㎡", "ABCDE费用", "<span ids=\"partThreeABCDEPayTextBox\" class=\"cls_cell_label\" id=\"sp_disgnuserc\" rel=\"" + array_pay[0] + "\">" + array_pay[0] + "</span>", "<span  ids=\"partThreeABCDEPayTextBox\" style=\"\" class=\"cls_cell_label\" id=\"sp_xiaoduic\" rel=\"" + array_pay[1] + "\">" + array_pay[1] + "</span>", "<span style=\"\"  ids=\"partThreeABCDEPayTextBox\" class=\"cls_cell_label\" id=\"sp_zhucec\" rel=\"" + array_pay[2] + "\">" + array_pay[2] + "</span>", "<span  ids=\"partThreeABCDEPayTextBox\" style=\"\" class=\"cls_cell_label\" id=\"sp_shenhc\" rel=\"" + array_pay[3] + "\">" + array_pay[3] + "</span>", "<span  ids=\"partThreeABCDEPayTextBox\" style=\"\" class=\"cls_cell_label\" id=\"sp_shendc\" rel=\"" + array_pay[3] + "\">" + array_pay[3] + "</span>" };
            sb_row.Append(bll_comm.CreateTableRow(icolum, configs2, rowconfigs, htmls3));
            return sb_row.ToString();
        }

        /// <summary>
        /// 生成分配底部汇总表
        /// </summary>
        /// <returns></returns>
        protected string CreateAllotBottom(string costid)
        {
            TG.Model.PartFour four = new TG.BLL.ProjectAllotBP().GetPartFourRecord(int.Parse(costid));
            if (four == null)
                return "";
            string[] countcolumn = four.Statistics.Split(new char[] { ',' }, StringSplitOptions.None);
            StringBuilder sb_btm = new StringBuilder();
            //列数量
            int icolumn = 11;
            //合并统计列
            string[] configs_all = { };
            string[] rowconfigs_all = { };
            string[] htmls_all = { "<span id=\"bt_danwei_all\"  class=\"cls_cell_allcount\">" + countcolumn[0] + "</span>", "<span id=\"bt_danweip_all\" class=\"cls_cell_allcount\">" + countcolumn[1] + "</span>", "<span id=\"bt_b1_all\" class=\"cls_cell_allcount\">" + countcolumn[2] + "</span>", "<span id=\"bt_b2_all\" class=\"cls_cell_allcount\">" + countcolumn[3] + "</span>", "<span id=\"bt_b3_all\" class=\"cls_cell_allcount\">" + countcolumn[4] + "</span>", "<span id=\"bt_b4_all\" class=\"cls_cell_allcount\">" + countcolumn[5] + "</span>", "<span id=\"bt_c1_all\" class=\"cls_cell_allcount\">" + countcolumn[6] + "</span>", "<span id=\"bt_c2_all\" class=\"cls_cell_allcount\">" + countcolumn[7] + "</span>", "<span id=\"\" class=\"cls_cell_allcount\">" + countcolumn[8] + "</span>", "<span id=\"bt_d_all\" class=\"cls_cell_allcount\">" + countcolumn[9] + "</span>", "<span id=\"bt_e_all\" class=\"cls_cell_allcount\">" + countcolumn[10] + "</span>" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs_all, rowconfigs_all, htmls_all));
            //列配置
            string[] configs = { "2|2", "4|2", "7|5" };
            //行配置
            string[] rowconfigs = { "1|4", "6|4", "7|4" };
            //单元格控件
            string[] htmls = { "总体核验", "院留费用合计", "<span id=\"allcount_hy\" class=\"cls_allcount\">" + four.CourtyardKeep + "</span>", "备注", "<span id=\"allsub\" class=\"cls_mark\">" + four.Remark + "</span>" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs, rowconfigs, htmls));

            string[] rowconfigs2 = { };
            string[] htmls2 = { "NULL", "项目费用合计", "<span id=\"allcount_pro\" class=\"cls_allcount\">" + four.ProjectKeep + "</span>", "NULL", "NULL" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs, rowconfigs2, htmls2));

            string[] rowconfigs3 = { };
            string[] htmls3 = { "NULL", "设计人员合计", "<span id=\"allcount_user\" class=\"cls_allcount\">" + four.DesignUserKeep + "</span>", "NULL", "NULL" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs, rowconfigs3, htmls3));

            string[] rowconfigs4 = { };
            string[] htmls4 = { "NULL", "总计", "<span id=\"allcount_all\" class=\"cls_allcount\">" + four.Total + "</span>", "NULL", "NULL" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs, rowconfigs4, htmls4));

            //制表行
            string[] configs5 = { };
            string[] rowconfigs5 = { };
            string[] htmls5 = { "院长", "<span class=\"cls_cell\">" + four.Dean + "</span>", "生产院长", "<span class=\"cls_cell\">" + four.Produce + "</span>", "生产经营", "<span class=\"cls_cell\">" + four.ProduceManager + "</span>", "项目经理", "<span class=\"cls_cell\">" + four.PM + "</span>", "制表人", "<span class=\"cls_cell\">" + four.TabulationUser + "</span>", "" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs5, rowconfigs5, htmls5));
            //操作
            string[] configs_oper = { "1|11" };
            string[] rowconfigs_oper = { };
            string[] htmls_oper = { "<input type=\"button\" value=\"打印\" class=\"cls_oper\" id=\"btn_print\"/>&nbsp;<input type=\"button\" value=\"返回\" class=\"cls_oper\" id=\"btn_back\"/>" };
            sb_btm.Append(bll_comm.CreateTableRow(icolumn, configs_oper, rowconfigs_oper, htmls_oper));

            return sb_btm.ToString();
        }

        /// <summary>
        /// 生成专业设计人员分配行
        /// </summary>
        /// <param name="proid"></param>
        /// <returns></returns>
        protected string CreateNumalRowCollection(string proid, string costid)
        {
            //得到第三部分的
            TG.Model.PartThree model_three = new TG.BLL.ProjectAllotBP().GetPartThreeRecord(int.Parse(costid));
            if (model_three == null)
                return "";
            //专业分配比例
            string[] allotprt = model_three.DesignPay.Split(new char[] { ',' }, StringSplitOptions.None);
            string[] allotregprt = model_three.SpecialtyPayPercent.Split(new char[] { ',' }, StringSplitOptions.None);
            //专业分配下标
            int i_Index = 0;
            //查询专业的人数
            Dictionary<string, List<TG.Model.PartThreeTableViewEntity>> dataDesign = new TG.Model.PartThree().ResolveJSON(model_three.DesignUserPay);
            Dictionary<string, List<TG.Model.PartThreeTableViewEntity>> dataCheckUser = new TG.Model.PartThree().ResolveJSON(model_three.CheckUserPay);
            Dictionary<string, List<TG.Model.PartThreeTableViewEntity>> dataRegUser = new TG.Model.PartThree().ResolveJSON(model_three.SpeRegPay);
            Dictionary<string, List<TG.Model.PartThreeTableViewEntity>> dataAuditUser = new TG.Model.PartThree().ResolveJSON(model_three.AuditUserPay);
            Dictionary<string, List<TG.Model.PartThreeTableViewEntity>> dataJuageUser = new TG.Model.PartThree().ResolveJSON(model_three.JuageUserPay);
            //声明人员列表
            List<TG.Model.PartThreeTableViewEntity> designusers;
            List<TG.Model.PartThreeTableViewEntity> checkusers;
            List<TG.Model.PartThreeTableViewEntity> regusers;
            List<TG.Model.PartThreeTableViewEntity> auditusers;
            List<TG.Model.PartThreeTableViewEntity> juageusers;
            StringBuilder sb_row = new StringBuilder();
            //获取项目专业列表
            TG.BLL.tg_speciality bll_spe = new TG.BLL.tg_speciality();
            List<TG.Model.tg_speciality> models = bll_spe.GetModelList("");
            foreach (TG.Model.tg_speciality spe in models)
            {
                //对象索引变量
                int memID = 0;
                int mem_xdID = 0;
                int mem_regID = 0;
                int mem_shID = 0;
                int mem_sdID = 0;
                int itemp = 0;

                //设计人员
                designusers = GetUserListData(dataDesign, spe.spe_Name);
                int idesigncount = designusers.Count;
                checkusers = GetUserListData(dataCheckUser, spe.spe_Name);
                int icheckcount = checkusers.Count;
                regusers = GetUserListData(dataRegUser, spe.spe_Name);
                int iregcount = regusers.Count;
                juageusers = GetUserListData(dataJuageUser, spe.spe_Name);
                int ijuagecount = juageusers.Count;
                auditusers = GetUserListData(dataAuditUser, spe.spe_Name);
                int iauditcount = auditusers.Count;

                //计算行数
                int iresult = idesigncount / 4;
                int iresult_xd = icheckcount / 2;
                int iresult_sh = iregcount;
                int iresult_sd = iauditcount;
                int iresult_ju = ijuagecount;
                int rows = idesigncount % 4 == 0 ? iresult : iresult + 1;
                int rows_xd = icheckcount % 2 == 0 ? iresult_xd : iresult_xd + 1;
                int rows_sh = iresult_sh;
                int rows_sd = iresult_sd;
                int rows_ju = ijuagecount;
                int[] iarray = new int[5];
                iarray[0] = rows;
                iarray[1] = rows_xd;
                iarray[2] = rows_sh;
                iarray[3] = rows_sd;
                iarray[4] = rows_ju;
                rows = Sort(iarray)[0];
                //根据专业生成行
                if (rows > 0)
                {
                    //各个专业开始
                    string[] configs = { };
                    string[] rowconfigs = { };
                    string[] htmls = { "<div ids=\"partThreeDesignPayTextBox\" id=\"cfg_danw_" + allotprt[i_Index] + "\" class=\"cls_danwei\" rel=\"rel_danwei\">" + allotprt[i_Index] + "</div>", "<span ids=\"partThreeSpecialtyPayPercentTextBox\" id=\"cfg_danwp_" + spe.spe_ID + "\" class=\"cls_danweip\" rel=\"" + allotregprt[i_Index] + "\">" + allotregprt[i_Index] + "</span>", "<div class=\"cls_cell_tip\">B1</div>", "<div class=\"cls_cell_tip\">B2</div>", "<div class=\"cls_cell_tip\">B3</div>", "<div class=\"cls_cell_tip\">B4</div>", "<div class=\"cls_cell_tip\">C1</div>", "<div class=\"cls_cell_tip\">C2</div>", "<div class=\"cls_cell_tip\">A</div>", "<div class=\"cls_cell_tip\">D</div>", "<div class=\"cls_cell_tip\">E</div>" };
                    int icolum = 11;
                    //自增
                    i_Index++;
                    sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs, htmls));
                    //创建行
                    for (int i = 0; i < rows; i++)
                    {
                        string emptcell = "";
                        string[] htmls2 = new string[11];
                        string[] htmls3 = new string[11];
                        string[] htmls4 = new string[11];
                        htmls2[0] = spe.spe_Name.Trim() + "专业";
                        htmls3[0] = "NULL";
                        htmls4[0] = "NULL";
                        htmls2[1] = "设计比例";
                        htmls3[1] = "金额";
                        htmls4[1] = "人员";
                        htmls4[2] = memID < idesigncount ? "<span>" + designusers[memID++].Member + "</span>" : emptcell;
                        itemp = memID - 1;
                        if (itemp >= 0 && memID <= idesigncount)
                        {
                            if (memID == idesigncount)
                            {
                                memID++;
                            }
                            htmls2[2] = itemp < idesigncount ? "<span     class=\"cls_cell_dsign_bl\" id=\"dsuser_" + designusers[itemp].Member + "_" + spe.spe_ID + "\">" + designusers[itemp].DesignPercent + "</span>%" : emptcell;
                            htmls3[2] = itemp < idesigncount ? "<span     vname=\"B1\" class=\"cls_cell_dsign\" id=\"dsusercount_" + designusers[itemp].Member + "_" + spe.spe_ID + "\" >" + designusers[itemp].Money + "</span>" : emptcell;
                        }
                        else
                        {
                            htmls2[2] = "";
                            htmls3[2] = "";
                        }
                        htmls4[3] = memID < idesigncount ? "<span  containername=\"" + spe.spe_Name + "\" >" + designusers[memID++].Member + "</span>" : emptcell;
                        itemp = memID - 1;
                        if (itemp >= 0 && memID <= idesigncount)
                        {
                            if (memID == idesigncount)
                            {
                                memID++;
                            }
                            htmls2[3] = itemp < idesigncount ? "<span     class=\"cls_cell_dsign_bl\" id=\"dsuser_" + designusers[itemp].Member + "_" + spe.spe_ID + "\">" + designusers[itemp].DesignPercent + "</span>%" : emptcell;
                            htmls3[3] = itemp < idesigncount ? "<span     vname=\"B1\" class=\"cls_cell_dsign\" id=\"dsusercount_" + designusers[itemp].Member + "_" + spe.spe_ID + "\" >" + designusers[itemp].Money + "</span>" : emptcell;
                        }
                        else
                        {
                            htmls2[3] = "";
                            htmls3[3] = "";
                        }
                        htmls4[4] = memID < idesigncount ? "<span  containername=\"" + spe.spe_Name + "\" >" + designusers[memID++].Member + "</span>" : emptcell;
                        itemp = memID - 1;
                        if (itemp >= 0 && memID <= idesigncount)
                        {
                            if (memID == idesigncount)
                            {
                                memID++;
                            }
                            htmls2[4] = itemp < idesigncount ? "<span     class=\"cls_cell_dsign_bl\" id=\"dsuser_" + designusers[itemp].Member + "_" + spe.spe_ID + "\">" + designusers[itemp].DesignPercent + "</span>%" : emptcell;
                            htmls3[4] = itemp < idesigncount ? "<span     vname=\"B1\" class=\"cls_cell_dsign\" id=\"dsusercount_" + designusers[itemp].Member + "_" + spe.spe_ID + "\" >" + designusers[itemp].Money + "</span>" : emptcell;
                        }
                        else
                        {
                            htmls2[4] = "";
                            htmls3[4] = "";
                        }
                        htmls4[5] = memID < idesigncount ? "<span  class=\"cls_cell\">" + designusers[memID++].Member + "</span>" : emptcell;
                        itemp = memID - 1;
                        if (itemp >= 0 && memID <= idesigncount)
                        {
                            if (memID == idesigncount)
                            {
                                memID++;
                            }
                            htmls2[5] = itemp < idesigncount ? "<span     class=\"cls_cell_dsign_bl\" id=\"dsuser_" + designusers[itemp].Member + "_" + spe.spe_ID + "\">" + designusers[itemp].DesignPercent + "</span>%" : emptcell;
                            htmls3[5] = itemp < idesigncount ? "<span     vname=\"B1\" class=\"cls_cell_dsign\" id=\"dsusercount_" + designusers[itemp].Member + "_" + spe.spe_ID + "\" >" + designusers[itemp].Money + "</span>" : emptcell;
                        }
                        else
                        {
                            htmls2[5] = "";
                            htmls3[5] = "";
                        }
                        htmls4[6] = mem_xdID < icheckcount ? "<span containername=\"" + spe.spe_Name + "\" >" + checkusers[mem_xdID++].Member + "</span>" : emptcell;
                        itemp = mem_xdID - 1;
                        if (itemp >= 0)
                        {
                            if (mem_xdID == icheckcount)
                            {
                                mem_xdID++;
                            }
                            htmls2[6] = itemp < icheckcount ? "<span   class=\"cls_cell_xh_bl\" id=\"xiaohe_" + checkusers[itemp].DesignPercent + "_" + spe.spe_ID + "\">" + checkusers[itemp].DesignPercent + "</span>%" : emptcell;
                            htmls3[6] = itemp < icheckcount ? "<span   vname=\"C1\" class=\"cls_cell_xh\" id=\"xiaohecount_" + checkusers[itemp].Money + "_" + spe.spe_ID + "\"  >" + checkusers[itemp].Money + "</span>" : emptcell;
                        }
                        else
                        {
                            htmls2[6] = "";
                            htmls3[6] = "";
                        }
                        htmls4[7] = mem_xdID < icheckcount ? "<span class=\"cls_cell\" >" + checkusers[mem_xdID++].Member + "</span>" : emptcell;
                        itemp = mem_xdID - 1;
                        if (itemp >= 0)
                        {
                            if (mem_xdID == icheckcount)
                            {
                                mem_xdID++;
                            }
                            htmls2[7] = itemp < icheckcount ? "<span    class=\"cls_cell_xh_bl\" id=\"xiaohe_" + checkusers[itemp].DesignPercent + "_" + spe.spe_ID + "\">" + checkusers[itemp].DesignPercent + "</span>%" : emptcell;
                            htmls3[7] = itemp < icheckcount ? "<span    vname=\"C1\" class=\"cls_cell_xh\" id=\"xiaohecount_" + checkusers[itemp].Money + "_" + spe.spe_ID + "\"  >" + checkusers[itemp].Money + "</span>" : emptcell;
                        }
                        else
                        {
                            htmls2[7] = "";
                            htmls3[7] = "";
                        }
                        //名称
                        htmls4[8] = mem_regID < iregcount ? "<span class=\"cls_cell\" >" + regusers[mem_regID++].Member + "</span>" : emptcell;
                        itemp = mem_regID - 1;
                        if (itemp >= 0)
                        {
                            if (mem_regID == iregcount)
                            {
                                mem_regID++;
                            }
                            //比例
                            htmls2[8] = "<span  class=\"cls_cell\"  >" + regusers[itemp].DesignPercent + "%</span>";
                            //钱数
                            htmls3[8] = "<span  class=\"cls_cell\" >" + regusers[itemp].Money + "</span>";
                        }
                        else
                        {
                            htmls2[8] = "";
                            htmls3[8] = "";
                        }
                        htmls4[9] = mem_shID < iauditcount ? "<span class=\"cls_cell\" >" + auditusers[mem_shID++].Member + "</span>" : emptcell;
                        itemp = mem_shID - 1;
                        if (itemp >= 0)
                        {
                            if (mem_shID == iauditcount)
                            {
                                mem_shID++;
                            }

                            htmls2[9] = itemp < iauditcount ? "<span    class=\"cls_cell_sh_bl\" id=\"shenhe_" + auditusers[itemp].DesignPercent + "_" + spe.spe_ID + "\">" + auditusers[itemp].DesignPercent + "</span>%" : emptcell;
                            htmls3[9] = itemp < iauditcount ? "<span    vname=\"D\" class=\"cls_cell_sh\" id=\"shenhecount_" + auditusers[itemp].Money + "_" + spe.spe_ID + "\"  >" + auditusers[itemp].Money + "</span>" : emptcell;
                        }
                        else
                        {
                            htmls3[9] = "";
                            htmls2[9] = "";
                        }
                        htmls4[10] = mem_sdID < ijuagecount ? "<span >" + juageusers[mem_sdID++].Member + "</span>" : emptcell;
                        itemp = mem_sdID - 1;
                        if (itemp >= 0)
                        {
                            if (mem_sdID == ijuagecount)
                            {
                                mem_sdID++;
                            }
                            htmls2[10] = itemp < ijuagecount ? "<span      class=\"cls_cell_sd_bl\" id=\"shending_" + juageusers[itemp].DesignPercent + "_" + spe.spe_ID + "\">" + juageusers[itemp].DesignPercent + "</span>%" : emptcell;
                            htmls3[10] = itemp < ijuagecount ? "<span    vname=\"E\"  class=\"cls_cell_sd\" id=\"shendingcount_" + juageusers[itemp].Money + "_" + spe.spe_ID + "\"  >" + juageusers[itemp].Money + "</span>" : emptcell;
                        }
                        else
                        {
                            htmls2[10] = "";
                            htmls3[10] = "";
                        }
                        string[] rowconfigs2 = { "1|4" };
                        sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs2, htmls2));
                        string[] rowconfigs3 = { };
                        sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs3, htmls3));
                        string[] rowconfigs4 = { };
                        sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs4, htmls4));
                        string[] htmls5 = { "NULL", "签字", "", "", "", "", "", "", "", "", "" };
                        string[] rowconfigs5 = { };
                        sb_row.Append(bll_comm.CreateTableRow(icolum, configs, rowconfigs5, htmls5));
                    }
                }
            }
            return sb_row.ToString();
        }

        //计算最低平米钱数
        private decimal GetLowAllotMoney(decimal dlow, decimal allprt, decimal prsprt)
        {
            return dlow * allprt * prsprt * 0.01m;
        }
        //冒泡
        public int[] Sort(int[] array)
        {
            int i, j;  // 循环变量
            int temp;  // 临时变量
            for (i = 0; i < array.Length - 1; i++)
            {
                for (j = 0; j < array.Length - 1 - i; j++)
                {
                    if (array[j] < array[j + 1])
                    {
                        // 交换元素
                        temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }
            return array;
        }
        /// <summary>
        /// 获取用户配置
        /// </summary>
        /// <returns></returns>
        protected TG.Model.cm_CostUserConfig GetAllotUserConfig()
        {
            TG.Model.cm_CostUserConfig model = null;
            TG.BLL.cm_CostUserConfig bll_cfg = new TG.BLL.cm_CostUserConfig();
            string strWhere = " Used=1";
            List<TG.Model.cm_CostUserConfig> models = bll_cfg.GetModelList(strWhere);
            if (models.Count > 0)
            {
                model = models[0];
            }
            return model;
        }
        /// <summary>
        /// 根据角色名返回用户列表
        /// </summary>
        /// <param name="rolename"></param>
        /// <param name="speid"></param>
        /// <param name="proid"></param>
        /// <returns></returns>
        public List<TG.Model.tg_member> GetMemsByProRoleName(string rolename, string speid, string proid)
        {
            string strWhere = "";
            string mem_IDs = "";
            //查询专业下校对人
            TG.BLL.tg_relation bll_rel = new TG.BLL.tg_relation();
            List<TG.Model.tg_relation> models_rel = new List<TG.Model.tg_relation>();
            TG.BLL.tg_ProRole bll_role = new TG.BLL.tg_ProRole();
            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();

            strWhere = " RoleName='" + rolename + "'";
            List<TG.Model.tg_ProRole> model_role = bll_role.GetModelList(strWhere);
            string str_role = model_role[0].ID.ToString();
            //校对人员
            strWhere = " Pro_ID=" + proid + " AND Charindex('" + str_role + "',mem_RoleIDs,0)>0";
            models_rel = bll_rel.GetModelList(strWhere);
            foreach (TG.Model.tg_relation rel in models_rel)
            {
                mem_IDs += rel.mem_ID + ",";
            }
            mem_IDs = mem_IDs.Remove(mem_IDs.Length - 1);
            strWhere = " mem_ID IN (" + mem_IDs + ") AND mem_Speciality_ID=" + speid;
            //返回用户列表
            return bll_mem.GetModelList(strWhere);
        }

        private List<PartThreeTableViewEntity> GetUserListData(Dictionary<string, List<PartThreeTableViewEntity>> dic, string parameter)
        {
            foreach (KeyValuePair<string, List<PartThreeTableViewEntity>> item in dic)
            {
                if (item.Key == parameter)
                {
                    return item.Value;
                }
            }
            return null;
        }
    }
}
