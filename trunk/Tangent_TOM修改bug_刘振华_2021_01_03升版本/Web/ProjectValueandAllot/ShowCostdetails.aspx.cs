﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ShowCostdetails : System.Web.UI.Page
    {
        private TG.BLL.CostImportBymaster bll = new BLL.CostImportBymaster();
        protected string TypeID
        {
            get
            {
                return Request.Params["typeid"];
            }
        }
        protected string StartTime
        {
            get
            {
                return Request.Params["starttime"];
            }
        }
        protected string EndTime
        {
            get
            {
                return Request.Params["endtime"];
            }
        }
        protected string UnitID
        {
            get
            {
                return Request.Params["unitid"];
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindData();
                this.HiddenUnitID.Value = UnitID;
                this.HiddenTypeID.Value = TypeID;
            }
        }
        //绑定数据
        private void bindData()
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder sb = new StringBuilder();
            if (TypeID == "gz")
            {
                builder.Append("AND cs.costName LIKE '%工资%'");
                sb.Append("AND costName LIKE '%工资%'");
            }
            else if (TypeID == "fy")
            {
                builder.Append("AND cs.costName  not LIKE '%工资%'");
                sb.Append("AND costName  not LIKE '%工资%'");
            }
            else if (TypeID == "all")
            {
                //
            }
            else
            {
                builder.Append("AND cs.id=" + TypeID);
                sb.Append("AND cs.id=" + TypeID);
            }
            if (UnitID != "-1")
            {
                builder.Append("AND ss.costunit=" + UnitID);
                sb.Append("AND ss.costunit=" + UnitID);
            }
            if (!string.IsNullOrEmpty(StartTime) && !string.IsNullOrEmpty(EndTime))
            {
                builder.AppendFormat(" AND ss.costTime between '{0}' and '{1}' ", StartTime, EndTime);
                sb.AppendFormat(" AND ss.costTime between '{0}' and '{1}' ", StartTime, EndTime);
            }

            //年份
            if (string.IsNullOrEmpty(StartTime) && !string.IsNullOrEmpty(EndTime))
            {
                builder.AppendFormat(" AND ss.costTime between '{0}' and '{1}' ", DateTime.Now.ToString("yyyy-MM-dd"), EndTime);
                sb.AppendFormat(" AND ss.costTime between '{0}' and '{1}' ", DateTime.Now.ToString("yyyy-MM-dd"), EndTime);
            }
            //年份
            if (!string.IsNullOrEmpty(StartTime) && string.IsNullOrEmpty(EndTime))
            {
                builder.AppendFormat(" AND ss.costTime between '{0}' and '{1}' ", StartTime, DateTime.Now.ToString("yyyy-MM-dd"));
                sb.AppendFormat(" AND ss.costTime between '{0}' and '{1}' ", StartTime, DateTime.Now.ToString("yyyy-MM-dd"));
            }
            //记录数
            this.AspNetPager1.RecordCount = int.Parse(bll.GetCostListPageProcCount(builder.ToString()).ToString());
            //分页
            grid_Financial.DataSource = bll.GetCostListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex); ;
            grid_Financial.DataBind();
        }
        /// <summary>
        /// 分页执行绑定数据
        /// </summary>
        /// <param name="src"></param>
        /// <param name="e"></param>
        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            bindData();
        }
    }
}