﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="TranjjsProjectValueAllotBymaster.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.TranjjsProjectValueAllotBymaster" %>

<%@ Register Src="../UserControl/ChooseProjectValueUser.ascx" TagName="ChooseProjectValueUser"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/AddExternalMember.ascx" TagName="AddExternalMember"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <%--    <link href="../css/ProjectValueandAllot.css" rel="stylesheet" type="text/css" />--%>
    <link href="../css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <script src="../js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="/js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="../js/MessageComm.js" type="text/javascript"></script>
    <script src="../js/UserControl/ChooseProjectValueUser.js" type="text/javascript"></script>
    <script src="../js/UserControl/AddExternalMember.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/ProjectValueandAllot/TranjjsProjectValueAllot.js"></script>
    <style type="text/css">
        .display {
            display: none;
        }

        /* 表格基本样式*/
        .cls_show_cst_jiben {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }

            .cls_show_cst_jiben td {
                border: solid 1px #CCC;
                font-size: 12px;
                font-family: "微软雅黑";
            }

            .cls_show_cst_jiben tr {
                height: 22px;
            }

        .cls_content_head {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }

            .cls_content_head td {
                height: 20px;
                background-color: #E6E6E6;
                border: 1px solid Gray;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">项目信息管理 <small>转经济所产值分配</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i>项目信息管理<i class="fa fa-angle-right"> </i>产值分配<i class="fa fa-angle-right"> </i>转经济所产值分配</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i><font><font>项目信息</font></font>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h3 class="form-section">项目信息</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 15%;">项目名称:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">合同关联:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblReletive" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">管理级别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_level" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">审核级别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_auditlevel" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目类别:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblBuildType" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">建设规模:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblScale" runat="server" Width="100px"></asp:Label>㎡
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">承接部门:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblcpr_Unit" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">项目总负责:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblPMName" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目阶段:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lbl_purpose" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%;">合同额:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Label ID="lblCoperationAmount" runat="server" Text=""></asp:Label>万元
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">其他参与部门:
                                        </td>
                                        <td style="width: 35%;" colspan="3">
                                            <asp:Label ID="lbl_isotherprt" runat="server" Text="lbl_isotherprt"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%;">项目分配年份:
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:DropDownList ID="drp_year" runat="server" AppendDataBoundItems="true" CssClass="form-control ">
                                                <asp:ListItem Value="-1">--选择年份--</asp:ListItem>
                                            </asp:DropDownList>

                                        </td>
                                        <td style="width: 15%;">转经济所金额:
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTotalCount" runat="server" Text=""></asp:Label>
                                            (元)
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i><font><font>经济所项目分配表填写</font></font>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div style="margin-top: 5px;" class="cls_data">
                                    <table class="cls_ProjAllot_Table" style="width: 100%;" id="tbTranJjs">
                                        <tr>
                                            <td style="width: 15%;">项目分配阶段：
                                            </td>
                                            <td style="width: 35%;">
                                                <div class="col-md-2">
                                                </div>
                                                <div class="col-md-8">
                                                    <select id="stage" class="form-control input-sm" runat="server">
                                                        <option value="-1">---请选择分配阶段---</option>
                                                        <option value="11">概算（室外不单算）</option>
                                                        <option value="12">概算（室外单算）</option>
                                                        <option value="13">概算 (锅炉房)</option>
                                                        <option value="14">概算(变电所)</option>
                                                        <option value="15">概算(水泵房)</option>
                                                        <option value="16">概算(空调机房)</option>
                                                        <option value="17">概算（单独室外工程）</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                </div>
                                            </td>
                                            <td style="width: 15%;">项目分配金额：
                                            </td>
                                            <td style="width: 35%;" class="cls_ProjAllot_Input">
                                                <asp:Label ID="lblAllotCount" runat="server" Text="Label"></asp:Label>
                                                (元)
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="cls_ProjAllotHead" width="100%">
                                        <tr>
                                            <td align="center" style="width: 15%;">序号
                                            </td>
                                            <td align="center" style="width: 35%;">内容
                                            </td>
                                            <td align="center" style="width: 15%;">比例
                                            </td>
                                            <td align="center" style="width: 35%;">产值
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label" width="100%"
                                        id="tbInfo">
                                        <tr>
                                            <td style="width: 15%;">1
                                            </td>
                                            <td style="width: 35%;">实收产值
                                            </td>
                                            <td style="width: 15%;">
                                                <input id="txt_PaidValuePercent" maxlength="15" type="text" disabled="disabled" value="100" />(%)
                                            </td>
                                            <td style="width: 35%;">
                                                <asp:Label ID="txt_PaidValueCount" runat="server"></asp:Label>(元)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2
                                            </td>
                                            <td>本部门自留产值
                                            </td>
                                            <td>
                                                <input id="txtTheDeptValuePercent" maxlength="8" type="text" value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.Thedeptallotpercent.ToString() %>" />(%)
                                            </td>
                                            <td>
                                                <span id="txtTheDeptValueCount">
                                                    <%=ProjectValueAllot ==null?"":ProjectValueAllot.Thedeptallotcount.ToString() %></span>(元)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3
                                            </td>
                                            <td>方案产值
                                            </td>
                                            <td>
                                                <input id="txt_ProgramPercent" maxlength="15" type="text" value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.ProgramPercent.ToString() %>" />(%)
                                            </td>
                                            <td>
                                                <span id="txt_ProgramCount">
                                                    <%=ProjectValueAllot ==null?"":ProjectValueAllot.ProgramCount.ToString() %></span>(元)
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td>4
                                            </td>
                                            <td>项目总负责
                                            </td>
                                            <td>
                                                <input id="txt_DesignManagerPercent" maxlength="8" type="text" value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.DesignManagerPercent.ToString() %>" />(%)
                                            </td>
                                            <td>
                                                <span id="txt_DesignManagerCount">
                                                    <%=ProjectValueAllot ==null?"":ProjectValueAllot.DesignManagerCount.ToString() %></span>(元)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>4
                                            </td>
                                            <td>应分产值
                                            </td>
                                            <td>
                                                <input id="txt_ShouldBeValuePercent" maxlength="15" type="text" disabled="disabled"
                                                    value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.ShouldBeValuePercent.ToString() %>" />(%)
                                            </td>
                                            <td>
                                                <asp:Label ID="txt_ShouldBeValueCount" runat="server"></asp:Label>(元)
                                                <%--                        <span id="txt_ShouldBeValueCount" runat="server">0.00</span>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="tbOneSave">
                                    <tr>
                                        <td style="width: 100%; text-align: center;">
                                            <input type="button" id="btnApprovalOne" name="controlBtn" class="btn green btn-default"
                                                value="确定" />
                                            <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                                返回</button>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Literal ID="lblMember" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--保存按钮-->
            <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="tbTwoSave">
                <tr>
                    <td style="width: 100%; text-align: center;">
                        <input type="button" id="btn_Up" name="controlBtn" class="btn green"
                            value="上一步" />
                        <input type="button" id="btnApprovalTwo" name="controlBtn" class="btn green"
                            data-toggle="modal" href="#AuditUserDiv" value="确定" />
                        <button type="button" class="btn default" onclick="javascript:window.history.back();">
                            返回</button>
                        <%-- <button id="btn_back" class="btn btn-default">返回</button>--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!--是否四舍五入-->
    <input type="hidden" id="HiddenIsRounding" value="<%=IsRounding
    %>" />
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenProSysNo" value="<%=proSysNo %>" />
    <input type="hidden" id="HiddenAllotID" value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.ID.ToString()
    %>" />
    <input type="hidden" id="HiddenAllotUser" value="<%=ProjectValueAllot ==null?"":ProjectValueAllot.AllotUser.ToString()
    %>" />
    <input type="hidden" id="HiddenTranAllotID" value="<%=allotID %>" />
    <!--选择消息接收着-->
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
    <!--弹出层-->
    <div id="chooseUserMainDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加人员</h4>
        </div>
        <div class="modal-body" id="chooseUserDiv">
            <div id="chooseUserMain">
                <uc1:ChooseProjectValueUser ID="ChooseUser1" runat="server" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_UserMain" data-dismiss="modal" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <div id="chooseExtUserDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">添加外聘人员</h4>
        </div>
        <div class="modal-body" id="chooseExtDiv">
            <div id="chooseExternalUserDiv">
                <uc2:AddExternalMember ID="chooseExternalUser1" runat="server" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="btnChooseExt" data-dismiss="modal" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <div id="AuditUserDiv" class="modal
    fade yellow"
        tabindex="-1" data-width="490" aria-hidden="true" style="display: none; width: 490px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
