﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class UnitRevenueCostBalanceReportBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindYear();
                SelectCurrentYear();
                bindText();
                bindDataDetail();
            }
        }

        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }

        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
        private void bindText()
        {
            if (drp_unit.SelectedValue != "-1")
            {
                this.lblUnit.Text = drp_unit.SelectedItem.Text.Trim();
            }
            else
            {
                this.lblUnit.Text = "生产部门";
            }
            if (drp_year.SelectedValue != "-1")
            {
                this.lblYear.Text = drp_year.SelectedItem.Text.Trim() + "年";
            }
            else
            {
                this.lblYear.Text = "全部年份";
            }
        }

        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND a.unit_Name= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND a.unit_Name= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            bindText();
            bindDataDetail();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();

            string modelPath = " ~/TemplateXls/CostBalanceReport.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            NPOI.SS.UserModel.IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);


            //设置样式
            ICellStyle styleTotal = wb.CreateCellStyle();
            //设置字体
            IFont fontTotal = wb.CreateFont();
            fontTotal.FontHeightInPoints = 9;//字号
            fontTotal.FontName = "宋体";
            //字体加粗
            fontTotal.Boldweight = (short)2000;
            styleTotal.SetFont(fontTotal);

            //设置边框
            styleTotal.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            styleTotal.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleTotal.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleTotal.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            ws.GetRow(0).GetCell(0).SetCellValue(lblYear.Text.Trim() + lblUnit.Text.Trim() + "收入成本结余明细表");
            CellRangeAddress range = new CellRangeAddress(0, 0, 0, 7);
            ws.AddMergedRegion(range);
            ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

            //取得专业
            var dtUnit = new DataView(dt).ToTable("unit_Name", true, "unit_Name");

            int row = 2;

            ArrayList arrayList = new ArrayList();

            for (int i = 0; i < dtUnit.Rows.Count; i++)
            {
                var dv = new DataView();
                if (this.drp_year.SelectedIndex != 0)
                {
                    dv = new DataView(dt) { RowFilter = "unit_Name='" + dtUnit.Rows[i]["unit_Name"] + "' and DivideYear='" + this.drp_year.SelectedValue.Trim() + "'" };
                }
                else
                {
                    dv = new DataView(dt) { RowFilter = "unit_Name='" + dtUnit.Rows[i]["unit_Name"] + "' " };
                }

                var dtDetail = dv.ToTable();

                var dtcostCharge = new DataView(dt) { RowFilter = "unit_Name='" + dtUnit.Rows[i]["unit_Name"] + "'" }.ToTable();


                int count = dtDetail.Rows.Count;

                if (count == 0)
                {
                    count = 1;
                }

                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);

                cell.CellStyle = style2;
                cell.SetCellValue((i + 1));
                range = new CellRangeAddress(row, (row + count), 0, 0);
                ws.AddMergedRegion(range);
                ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

                cell = dataRow.GetCell(1);
                if (cell == null)
                    cell = dataRow.CreateCell(1);

                cell.CellStyle = style2;
                cell.SetCellValue(dtUnit.Rows[i]["unit_Name"].ToString());
                range = new CellRangeAddress(row, (row + count - 1), 1, 1);
                ws.AddMergedRegion(range);
                ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

                int row1 = 0;
                if (dtDetail.Rows.Count > 0)
                {
                    for (int j = 0; j < dtDetail.Rows.Count; j++)
                    {
                        var dataRow1 = ws.GetRow(row + row1);//生成行
                        if (dataRow == null)
                            dataRow = ws.CreateRow(row + row1);

                        var cell1 = dataRow1.CreateCell(2);
                        decimal allotCount = decimal.Parse((decimal.Parse(dtDetail.Rows[j]["AllotCount"].ToString()) * 10000).ToString("f2"));
                        WriteExcelValue(cell1, allotCount);
                        cell1.CellStyle = style2;

                        //所留

                        cell1 = dataRow1.CreateCell(3);
                        cell1.SetCellFormula("C" + (row + row1 + 1) + "*E" + (row + row1 + 1) + "");
                        cell1.CellStyle = style2;

                        cell1 = dataRow1.CreateCell(4);
                        decimal dividePercent = decimal.Parse(dtDetail.Rows[j]["DividePercent"].ToString());
                        WriteExcelValue(cell1, dividePercent);
                        cell1.CellStyle = style2;

                        //本所收入 =所留*0.97
                        decimal fundPercent = dtDetail.Rows[j]["fund_percent"].ToString() == "0.00" ? decimal.Parse("0.97") : decimal.Parse(((100 - decimal.Parse(dtDetail.Rows[j]["fund_percent"].ToString())) / 100).ToString());

                        cell1 = dataRow1.CreateCell(5);
                        cell1.SetCellFormula("D" + (row + row1 + 1) + "*" + fundPercent + "");
                        cell1.CellStyle = style2;

                        if (j == 0)
                        {
                            cell1 = dataRow1.CreateCell(6);
                            decimal costCharge = decimal.Parse(dtDetail.Rows[j]["costCharge"].ToString());
                            WriteExcelValue(cell1, costCharge);
                            range = new CellRangeAddress(row, (row + count), 6, 6);
                            ws.AddMergedRegion(range);
                            ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

                            cell1 = dataRow1.CreateCell(7);
                            cell1.SetCellFormula("F" + (row + count + 1) + "-G" + (row + row1 + 1) + "");
                            cell1.CellStyle = style2;
                            range = new CellRangeAddress(row, (row + count), 7, 7);
                            ws.AddMergedRegion(range);
                            ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

                        }

                        row1 = row1 + 1;
                    }
                }
                else
                {
                    var dataRow1 = ws.GetRow(row + row1);//生成行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row + row1);

                    var cell1 = dataRow1.CreateCell(2);
                    decimal allotCount = 0;
                    WriteExcelValue(cell1, allotCount);
                    cell1.CellStyle = style2;

                    cell1 = dataRow1.CreateCell(3);
                    cell1.SetCellFormula("C" + (row + row1 + 1) + "*E" + (row + row1 + 1) + "");
                    cell1.CellStyle = style2;

                    cell1 = dataRow1.CreateCell(4);
                    decimal dividePercent = 0;
                    WriteExcelValue(cell1, dividePercent);
                    cell1.CellStyle = style2;

                    //本所收入 =所留*0.97

                    cell1 = dataRow1.CreateCell(5);
                    cell1.SetCellFormula("D" + (row + row1 + 1) + "*" + 0.97 + "");
                    cell1.CellStyle = style2;

                    cell1 = dataRow1.CreateCell(6);
                    decimal costCharge = decimal.Parse(dtcostCharge.Rows[0]["costCharge"].ToString());
                    WriteExcelValue(cell1, costCharge);
                    cell1.CellStyle = style2;
                    range = new CellRangeAddress(row, (row + count), 6, 6);
                    ws.AddMergedRegion(range);
                    ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

                    cell1 = dataRow1.CreateCell(7);
                    cell1.SetCellFormula("F" + (row + count + 1) + "-G" + (row + row1 + 1) + "");
                    cell1.CellStyle = style2;
                    range = new CellRangeAddress(row, (row + count), 7, 7);
                    ws.AddMergedRegion(range);
                    ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

                    row1 = row1 + 1;

                }

                arrayList.Add(row + count + 1);

                var dataRowTotal = ws.GetRow(row + count);//生成行
                if (dataRowTotal == null)
                    dataRowTotal = ws.CreateRow(row + count);
                var cellTotal = dataRowTotal.GetCell(1);
                if (cellTotal == null)
                    cellTotal = dataRowTotal.CreateCell(1);
                cellTotal.SetCellValue("合计");
                cellTotal.CellStyle = style2;

                cellTotal = dataRowTotal.CreateCell(2);
                cellTotal.SetCellFormula("SUM(C" + (row + 1) + ":C" + (row + count) + ")");
                cellTotal.CellStyle = style2;

                cellTotal = dataRowTotal.CreateCell(3);
                cellTotal.SetCellFormula("SUM(D" + (row + 1) + ":D" + (row + count) + ")");
                cellTotal.CellStyle = style2;

                cellTotal = dataRowTotal.CreateCell(4);
                cellTotal.SetCellValue("");
                cellTotal.CellStyle = style2;

                cellTotal = dataRowTotal.CreateCell(5);
                cellTotal.SetCellFormula("SUM(F" + (row + 1) + ":F" + (row + count) + ")");
                cellTotal.CellStyle = style2;
                row = row + count + 1;
            }

            //总合计
            string strHj = "";
            for (int i = 0; i < arrayList.Count; i++)
            {
                strHj = strHj + arrayList[i] + ",";
            }
            if (!string.IsNullOrEmpty(strHj))
            {
                strHj = strHj.Substring(0, strHj.Length - 1);
                strHj = "," + strHj;
            }

            var dataTotal = ws.GetRow(row);//生成行
            if (dataTotal == null)
                dataTotal = ws.CreateRow(row);
            var total = dataTotal.GetCell(0);
            if (total == null)
                total = dataTotal.CreateCell(0);
            total.SetCellValue("");
            total.CellStyle = styleTotal;

            total = dataTotal.GetCell(1);
            if (total == null)
                total = dataTotal.CreateCell(1);
            total.SetCellValue("总计");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(2);
            if (string.IsNullOrEmpty(strHj))
            {
                total.SetCellFormula("SUM(C3:C" + (row) + ")");
            }
            else
            {
                string strHjTwo = strHj.Replace(",", "+C");
                strHjTwo = strHjTwo.Substring(1, strHjTwo.Length - 1);
                total.SetCellFormula("" + strHjTwo + "");
            }
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(3);
            if (string.IsNullOrEmpty(strHj))
            {
                total.SetCellFormula("SUM(D3:D" + (row) + ")");
            }
            else
            {
                string strHjTwo = strHj.Replace(",", "+D");
                strHjTwo = strHjTwo.Substring(1, strHjTwo.Length - 1);
                total.SetCellFormula("" + strHjTwo + "");
            }
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(4);
            total.SetCellValue("");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(5);
            if (string.IsNullOrEmpty(strHj))
            {
                total.SetCellFormula("SUM(F3:F" + (row) + ")");
            }
            else
            {
                string strHjTwo = strHj.Replace(",", "+F");
                strHjTwo = strHjTwo.Substring(1, strHjTwo.Length - 1);
                total.SetCellFormula("" + strHjTwo + "");
            }
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(6);
            total.SetCellFormula("SUM(G3:G" + (row) + ")");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(7);
            total.SetCellFormula("SUM(H3:H" + (row) + ")");
            total.CellStyle = styleTotal;

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(lblYear.Text.Trim() + lblUnit.Text.Trim() + "收入成本结余明细表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }

        /// <summary>
        /// 得到数据
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {
            StringBuilder sb = new StringBuilder();
            //绑定单位
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND a.unit_ID='" + this.drp_unit.SelectedValue.Trim() + "' ");
            }

            //按照年份
            string year = null;
            if (this.drp_year.SelectedIndex != 0)
            {
                //sb.Append(" AND  b.DivideYear='" + this.drp_year.SelectedItem.Text.Trim() + "' ");
                year = this.drp_year.SelectedValue;
            }
            //检查权限
            GetPreviewPowerSql(ref sb);
            TG.BLL.cm_Divideinfo bll = new TG.BLL.cm_Divideinfo();
            DataTable dt = bll.GetUnitRevenueCostBalance(sb.ToString(), year).Tables[0];
            return dt;
        }

        /// <summary>
        /// 明细
        /// </summary>
        private void bindDataDetail()
        {

            DataTable dt = getData();

            string year = null;
            if (this.drp_year.SelectedIndex != 0)
            {
                year = this.drp_year.SelectedValue;
            }

            StringBuilder sbHtml = new StringBuilder();
            if (dt != null && dt.Rows.Count > 0)
            {
                decimal AlltotalAllotCount = 0;//收入
                decimal AlltotalUnitCount = 0;//所留
                decimal AlltotalUnitAllotCount = 0;//本所收留
                decimal AllcostCharge = 0;//成本
                decimal Alljieyu = 0;//结余
                //取得专业
                var dtUnit = new DataView(dt).ToTable("unit_Name", true, "unit_Name");

                int rowNum = 1;
                for (int i = 0; i < dtUnit.Rows.Count; i++)
                {
                    var dv = new DataView();
                    if (this.drp_year.SelectedIndex != 0)
                    {
                        dv = new DataView(dt) { RowFilter = "unit_Name='" + dtUnit.Rows[i]["unit_Name"] + "' and DivideYear='" + this.drp_year.SelectedValue.Trim() + "'" };
                    }
                    else
                    {
                        dv = new DataView(dt) { RowFilter = "unit_Name='" + dtUnit.Rows[i]["unit_Name"] + "' " };
                    }

                    var dtDetail = dv.ToTable();

                    var dtcostCharge = new DataView(dt) { RowFilter = "unit_Name='" + dtUnit.Rows[i]["unit_Name"] + "'" }.ToTable();


                    int count = dtDetail.Rows.Count;
                    sbHtml.Append("<tr>");
                    if (count == 0)
                    {
                        count = 1;
                    }
                    sbHtml.Append("<td rowspan=\"" + (count + 1) + "\">" + rowNum + "</td>");
                    sbHtml.Append("<td rowspan=\"" + count + "\">" + dtUnit.Rows[i]["unit_Name"] + "</td>");

                    decimal totalAllotCount = 0;//收入
                    decimal totalUnitCount = 0;//所留
                    decimal totalUnitAllotCount = 0;//本所收留
                    decimal jieyu = 0;

                    StringBuilder sbTemp = new StringBuilder();
                    StringBuilder sbOneTemp = new StringBuilder();
                    if (dtDetail.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtDetail.Rows.Count; j++)
                        {
                            if (j != 0)
                            {
                                sbTemp.Append("<tr>");
                            }
                            decimal allotCount = decimal.Parse(dtDetail.Rows[j]["AllotCount"].ToString());
                            decimal fundPercent = dtDetail.Rows[j]["fund_percent"].ToString() == "0.00" ? decimal.Parse("0.97") : decimal.Parse(((100 - decimal.Parse(dtDetail.Rows[j]["fund_percent"].ToString())) / 100).ToString());
                            //所留=收入* 比例
                            decimal unitCount = allotCount * 10000 * decimal.Parse(dtDetail.Rows[j]["DividePercent"].ToString());
                            //本所收入
                            decimal unitAllotCount = unitCount * fundPercent;

                            if (j == 0)
                            {
                                //收入
                                sbOneTemp.Append("<td>" + (allotCount * 10000).ToString("f2") + "</td>");
                                //所留
                                sbOneTemp.Append("<td>" + unitCount.ToString("f2") + "</td>");

                                sbOneTemp.Append("<td>" + decimal.Parse(dtDetail.Rows[j]["DividePercent"].ToString()).ToString("f3") + "</td>");
                                sbOneTemp.Append("<td>" + unitAllotCount.ToString("f2") + "</td>");
                                sbOneTemp.Append("<td rowspan=\"" + (count + 1) + "\">" + dtcostCharge.Rows[0]["costCharge"] + "</td>");
                            }
                            else
                            {
                                sbTemp.Append("<td>" + (allotCount * 10000).ToString("f2") + "</td>");
                                sbTemp.Append("<td>" + unitCount.ToString("f2") + "</td>");
                                sbTemp.Append("<td>" + decimal.Parse(dtDetail.Rows[j]["DividePercent"].ToString()).ToString("f3") + "</td>");
                                sbTemp.Append("<td>" + unitAllotCount.ToString("f2") + "</td>");
                                sbTemp.Append("</tr>");
                            }
                            totalAllotCount = totalAllotCount + (allotCount * 10000);
                            totalUnitCount = totalUnitCount + unitCount;
                            totalUnitAllotCount = totalUnitAllotCount + unitAllotCount;
                        }
                        jieyu = totalUnitAllotCount - decimal.Parse(dtcostCharge.Rows[0]["costCharge"].ToString());
                        sbOneTemp.Append("<td rowspan=\"" + (count + 1) + "\">" + jieyu.ToString("f2") + "</td>");
                        sbOneTemp.Append("</tr>");
                        sbHtml.Append(sbOneTemp.ToString());
                        sbHtml.Append(sbTemp.ToString());
                    }
                    else
                    {
                        jieyu = totalUnitAllotCount - decimal.Parse(dtcostCharge.Rows[0]["costCharge"].ToString());
                        sbHtml.Append("<td>0.00</td>");
                        sbHtml.Append("<td>0.00</td>");
                        sbHtml.Append("<td>0.00</td>");
                        sbHtml.Append("<td>0.00</td>");
                        sbHtml.Append("<td rowspan=\"" + (count + 1) + "\">" + dtcostCharge.Rows[0]["costCharge"] + "</td>");
                        sbHtml.Append("<td rowspan=\"" + (count + 1) + "\">" + jieyu.ToString("f2") + "</td>");
                        sbHtml.Append("</tr>");
                    }

                    sbHtml.Append("<tr>");
                    sbHtml.Append("<td >合计</td>");
                    sbHtml.Append("<td >" + totalAllotCount.ToString("f2") + "</td>");
                    sbHtml.Append("<td >" + totalUnitCount.ToString("f2") + "</td>");
                    sbHtml.Append("<td></td>");
                    sbHtml.Append("<td>" + totalUnitAllotCount.ToString("f2") + "</td>");
                    sbHtml.Append("</tr>");
                    rowNum = rowNum + 1;

                    AlltotalAllotCount = AlltotalAllotCount + totalAllotCount;
                    AlltotalUnitCount = AlltotalUnitCount + totalUnitCount;
                    AlltotalUnitAllotCount = AlltotalUnitAllotCount + totalUnitAllotCount;
                    AllcostCharge = AllcostCharge + decimal.Parse(dtcostCharge.Rows[0]["costCharge"].ToString());
                    Alljieyu = Alljieyu + jieyu;
                }
                sbHtml.Append("<tr>");
                sbHtml.Append("<td></td>");
                sbHtml.Append("<td class=\"fontBold\">总合计</td>");
                sbHtml.Append("<td class=\"fontBold\">" + AlltotalAllotCount.ToString("f2") + "</td>");
                sbHtml.Append("<td class=\"fontBold\">" + AlltotalUnitCount.ToString("f2") + "</td>");
                sbHtml.Append("<td></td>");
                sbHtml.Append("<td class=\"fontBold\">" + AlltotalUnitAllotCount.ToString("f2") + "</td>");
                sbHtml.Append("<td class=\"fontBold\">" + AllcostCharge.ToString("f2") + "</td>");
                sbHtml.Append("<td class=\"fontBold\">" + Alljieyu.ToString("f2") + "</td>");
                sbHtml.Append("</tr>");
                lblHtml.Text = sbHtml.ToString();
            }
            else
            {
                sbHtml.Append("<tr><td colspan=8 style=\"color:red;\">没有数据</td><tr>");
                lblHtml.Text = sbHtml.ToString();
            }
        }

        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}