﻿using Aspose.Words;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjectValueAllotExportBymaster : System.Web.UI.Page
    {
        /// <summary>
        /// 合同系统号
        /// </summary>
        public int ProSysNo
        {
            get
            {
                int CoperationSysNo = 0;
                int.TryParse(Request["pro_id"], out CoperationSysNo);
                return CoperationSysNo;
            }
        }

        /// <summary>
        /// 合同系统号
        /// </summary>
        public int Allot_Id
        {
            get
            {
                int allot_id = 0;
                int.TryParse(Request["allot_id"], out allot_id);
                return allot_id;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindProject();
                bindAllot();
            }
        }

        /// <summary>
        /// 项目信息
        /// </summary>
        private void bindProject()
        {
            TG.Model.cm_Project model = new TG.BLL.cm_Project().GetModel(ProSysNo);
            if (model != null)
            {
                lbl_UnitName.Text = model.Unit;
                lblproName.Text = model.pro_name;
            }
        }

        /// <summary>
        /// 产值信息
        /// </summary>
        private void bindAllot()
        {
            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
            DataTable dt = BLL.GetAllotDataList(Allot_Id.ToString()).Tables[0];

            if (dt != null && dt.Rows.Count > 0)
            {

                //实收产值
                if (dt.Rows[0]["PaidValueCount"] != null)
                {
                    txt_PaidValueCount.Text = dt.Rows[0]["PaidValueCount"].ToString();
                }
                //设总比例
                if (dt.Rows[0]["DesignManagerPercent"] != null)
                {
                    txt_DesignManagerPercent.Text = dt.Rows[0]["DesignManagerPercent"].ToString();
                }
                //设总金额
                if (dt.Rows[0]["DesignManagerCount"] != null)
                {
                    txt_DesignManagerCount.Text = dt.Rows[0]["DesignManagerCount"].ToString();
                }
                //转经济所比例
                if (dt.Rows[0]["EconomyValuePercent"] != null)
                {
                    txt_EconomyValuePercent.Text = dt.Rows[0]["EconomyValuePercent"].ToString();
                }
                //转经济所比例
                if (dt.Rows[0]["EconomyValueCount"] != null)
                {
                    txt_EconomyValueCount.Text = dt.Rows[0]["EconomyValueCount"].ToString();
                }
                else
                {
                    txt_EconomyValueCount.Text = "0.00";
                }
                //部门产值
                if (dt.Rows[0]["UnitValuePercent"] != null)
                {
                    txt_UnitValueCountPercent.Text = dt.Rows[0]["UnitValuePercent"].ToString();
                }
                //部门产值
                if (dt.Rows[0]["UnitValueCount"] != null)
                {
                    txtUnitValueCount.Text = dt.Rows[0]["UnitValueCount"].ToString();
                }

                //分配产值
                if (dt.Rows[0]["AllotValuePercent"] != null)
                {
                    txtAllotValuePercent.Text = dt.Rows[0]["AllotValuePercent"].ToString();
                }

                if (dt.Rows[0]["AllotValueCount"] != null)
                {
                    txtAllotValueCount.Text = dt.Rows[0]["AllotValueCount"].ToString();
                }

                //转其他部门产值比例
                if (dt.Rows[0]["OtherDeptValuePercent"] != null)
                {
                    txtOtherDeptValuePercent.Text = dt.Rows[0]["OtherDeptValuePercent"].ToString();
                }

                if (dt.Rows[0]["OtherDeptValueCount"] != null)
                {
                    txtOtherDeptValueCount.Text = dt.Rows[0]["OtherDeptValueCount"].ToString();
                }

                //本部门自留产值比例
                if (dt.Rows[0]["TheDeptValuePercent"] != null)
                {
                    txtTheDeptValuePercent.Text = dt.Rows[0]["TheDeptValuePercent"].ToString();
                }
                //本部门产值
                if (dt.Rows[0]["TheDeptValueCount"] != null)
                {
                    txtTheDeptValueCount.Text = dt.Rows[0]["TheDeptValueCount"].ToString();
                }

                //转暖通产值比例
                if (dt.Rows[0]["HavcPercent"] != null)
                {
                    txt_HavcValuePercent.Text = dt.Rows[0]["HavcPercent"].ToString();
                }

                //转暖通产值
                if (dt.Rows[0]["HavcCount"] != null)
                {
                    txt_HavcValueCount.Text = dt.Rows[0]["HavcCount"].ToString();
                }

                //方案比例
                if (dt.Rows[0]["ProgramPercent"] != null)
                {
                    txt_ProgramPercent.Text = dt.Rows[0]["ProgramPercent"].ToString();
                }
                //方案产值
                if (dt.Rows[0]["ProgramCount"] != null)
                {
                    txt_ProgramCount.Text = dt.Rows[0]["ProgramCount"].ToString();
                }

                //应分金额
                if (dt.Rows[0]["ShouldBeValuePercent"] != null)
                {
                    txt_ShouldBeValuePercent.Text = dt.Rows[0]["ShouldBeValuePercent"].ToString();
                }
                //应分金额
                if (dt.Rows[0]["ShouldBeValueCount"] != null)
                {
                    txt_ShouldBeValueCount.Text = dt.Rows[0]["ShouldBeValueCount"].ToString();
                }

                if (dt.Rows[0]["ActualAllountTime"] != null)
                {
                    lbl_AllotYear.Text = dt.Rows[0]["ActualAllountTime"].ToString();
                }

                //财务
                if (!string.IsNullOrEmpty(dt.Rows[0]["FinanceValuePercent"].ToString()))
                {
                    lblFinanceValuePercent.Text = dt.Rows[0]["FinanceValuePercent"].ToString();
                }
                if (!string.IsNullOrEmpty(dt.Rows[0]["FinanceValueCount"].ToString()))
                {
                    lblFinanceValueCount.Text = dt.Rows[0]["FinanceValueCount"].ToString();
                }

                //部门应留产值
                if (!string.IsNullOrEmpty(dt.Rows[0]["TheDeptShouldValuePercent"].ToString()))
                {
                    lblTheDeptShouldValuePercent.Text = dt.Rows[0]["TheDeptShouldValuePercent"].ToString();
                }
                if (!string.IsNullOrEmpty(dt.Rows[0]["TheDeptShouldValueCount"].ToString()))
                {
                    lblTheDeptShouldValueCount.Text = dt.Rows[0]["TheDeptShouldValueCount"].ToString();
                }

            }
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Export_Click(object sender, EventArgs e)
        {
            OutputInfo();
        }

        private void OutputInfo()
        {

            string tmppath = Server.MapPath("~/TemplateWord/ProjectValue.doc");
            Document doc = new Document(tmppath); //载入模板
            if (doc.Range.Bookmarks["UnitName"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["UnitName"];
                mark.Text = lbl_UnitName.Text;
            }
            if (doc.Range.Bookmarks["ProjectName"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ProjectName"];
                mark.Text = lblproName.Text;
            }
            if (doc.Range.Bookmarks["PaidValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["PaidValueCount"];
                mark.Text = txt_PaidValueCount.Text + "(元)";
            }

            if (doc.Range.Bookmarks["DesignManagerPercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["DesignManagerPercent"];
                mark.Text = txt_DesignManagerPercent.Text + "%";
            }
            if (doc.Range.Bookmarks["DesignManagerCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["DesignManagerCount"];
                mark.Text = txt_DesignManagerCount.Text + "(元)";
            }
            if (doc.Range.Bookmarks["AllotValuePercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["AllotValuePercent"];
                mark.Text = txtAllotValuePercent.Text + "%";
            }
            if (doc.Range.Bookmarks["AllotValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["AllotValueCount"];
                mark.Text = txtAllotValueCount.Text + "(元)";
            }

            if (doc.Range.Bookmarks["EconomyValuePercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["EconomyValuePercent"];
                mark.Text = txt_EconomyValuePercent.Text + "%";
            }
            if (doc.Range.Bookmarks["EconomyValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["EconomyValueCount"];
                mark.Text = txt_EconomyValueCount.Text + "(元)";
            }
            if (doc.Range.Bookmarks["HavcPercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["HavcPercent"];
                mark.Text = txt_HavcValuePercent.Text + "%";
            }
            if (doc.Range.Bookmarks["HavcCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["HavcCount"];
                mark.Text = txt_HavcValueCount.Text + "(元)";
            }
            if (doc.Range.Bookmarks["OtherDeptValuePercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["OtherDeptValuePercent"];
                mark.Text = txtOtherDeptValuePercent.Text + "%";
            }
            if (doc.Range.Bookmarks["OtherDeptValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["OtherDeptValueCount"];
                mark.Text = txtOtherDeptValueCount.Text + "(元)";
            }
            if (doc.Range.Bookmarks["TheDeptValuePercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TheDeptValuePercent"];
                mark.Text = txtTheDeptValuePercent.Text + "%";
            }
            if (doc.Range.Bookmarks["TheDeptValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TheDeptValueCount"];
                mark.Text = txtTheDeptValueCount.Text + "(元)";
            }

            if (doc.Range.Bookmarks["ProgramPercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ProgramPercent"];
                mark.Text = txt_ProgramPercent.Text + "%";
            }
            if (doc.Range.Bookmarks["ProgramCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ProgramCount"];
                mark.Text = txt_ProgramCount.Text + "(元)";
            }
            if (doc.Range.Bookmarks["UnitValuePercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["UnitValuePercent"];
                mark.Text = txt_UnitValueCountPercent.Text + "%";
            }
            if (doc.Range.Bookmarks["UnitValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["UnitValueCount"];
                mark.Text = txtUnitValueCount.Text + "(元)";
            }
            if (doc.Range.Bookmarks["ShouldBeValuePercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ShouldBeValuePercent"];
                mark.Text = txt_ShouldBeValuePercent.Text + "%";
            }
            if (doc.Range.Bookmarks["ShouldBeValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ShouldBeValueCount"];
                mark.Text = txt_ShouldBeValueCount.Text + "(元)";
            }
            if (doc.Range.Bookmarks["FinanceValuePercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["FinanceValuePercent"];
                mark.Text = lblFinanceValuePercent.Text + "%";
            }
            if (doc.Range.Bookmarks["FinanceValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["FinanceValueCount"];
                mark.Text = lblFinanceValueCount.Text + "(元)";
            }
            if (doc.Range.Bookmarks["TheDeptShouldValuePercent"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TheDeptShouldValuePercent"];
                mark.Text = lblTheDeptShouldValuePercent.Text + "%";
            }
            if (doc.Range.Bookmarks["TheDeptShouldValueCount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TheDeptShouldValueCount"];
                mark.Text = lblTheDeptShouldValueCount.Text + "(元)";
            }


            doc.Save(System.Web.HttpContext.Current.Server.UrlEncode(lblproName.Text + "产值分配表" )+ ".doc", SaveFormat.Doc, SaveType.OpenInWord, Response);
            //保存为doc，并打开
        }
    }
}