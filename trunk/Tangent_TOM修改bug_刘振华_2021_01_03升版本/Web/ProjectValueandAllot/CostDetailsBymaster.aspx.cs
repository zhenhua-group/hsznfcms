﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class CostDetails : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //初始化时间
                InitDate();
                //单位信息
                BindUnit();
                //绑定类型
                BindType();
                //绑定权限
                BindPreviewPower();
            }
        }
        //检查是否需要权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        private void BindType()
        {
            //throw new NotImplementedException();
            TG.BLL.cm_CostType costbll = new TG.BLL.cm_CostType();
            string strwhere = "isInput=0 and costgroup in (0,5)";
            string strwhereAll = "costgroup=0";
            this.drp_costType.DataSource = costbll.GetList(strwhere);
            this.drp_costType.DataTextField = "costName";
            this.drp_costType.DataValueField = "ID";
            this.drp_costType.DataBind();
            this.drp_costTypeAll.DataSource = costbll.GetList(strwhereAll);
            this.drp_costTypeAll.DataTextField = "costName";
            this.drp_costTypeAll.DataValueField = "ID";
            this.drp_costTypeAll.DataBind();
        }
        //初始时间设置
        protected void InitDate()
        {
            string str_year = DateTime.Now.Year.ToString();
            this.txt_costdate1.Value = str_year + "-01-01";
            this.txt_costdate2.Value = str_year + "-12-31";
        }
        //绑定权限
        public void BindPreviewPower()
        {
            StringBuilder builder = new StringBuilder();
            GetPreviewPowerSql(ref builder);
            this.hidUserId.Value = UserSysNo.ToString();
            this.hid_where.Value = builder.ToString();
        }

        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (costUserId =" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND costUnit = (Select unit_Id From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
            this.drp_unitAdd.DataSource = bll_unit.GetList(strWhere);
            this.drp_unitAdd.DataTextField = "unit_Name";
            this.drp_unitAdd.DataValueField = "unit_ID";
            this.drp_unitAdd.DataBind();
        }

        protected void btn_export_Click(object sender, EventArgs e)
        {
            StringBuilder strsql = new StringBuilder();
            string unitid = this.drp_unit.SelectedItem.Value;
            string costtypeall = this.drp_costTypeAll.SelectedItem.Value;
            string date1 = this.txt_costdate1.Value;
            string date2 = this.txt_costdate2.Value;
            string keyname = this.txt_keyname.Value;
            //按名称查询
            if (keyname.Trim() != "")
            {
                keyname = TG.Common.StringPlus.SqlSplit(keyname);
                strsql.AppendFormat(" AND costSub like '%{0}%'", keyname);
            }
            //类别查询
            if (!string.IsNullOrEmpty(costtypeall) && costtypeall != "-1" && !costtypeall.Contains("选择类型"))
            {
                strsql.AppendFormat(" AND (costTypeID= '{0}')", costtypeall);
            }
            //按生产部门查询
            if (!string.IsNullOrEmpty(unitid) && unitid != "-1" && !unitid.Contains("全院部门"))
            {
                strsql.AppendFormat(" AND (costUnit= '{0}')", unitid);
                if (unitid == "250")
                {
                    this.HiddenISImport.Value = "1";
                }
                else if (unitid == "252")
                {
                    this.HiddenISImport.Value = "2";
                }
                else if (unitid == "257")
                {
                    this.HiddenISImport.Value = "3";
                }
                else
                {
                    this.HiddenISImport.Value = "4";
                }
            }
            string unitName = this.drp_unit.SelectedItem.Text.Trim();
            //年份
            if (date1 != "" && date2 != "")
            {
                strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", date1, date2);
            }
            //年份
            if (date1 == "" && date2 != "")
            {
                strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", DateTime.Now.ToString("yyyy-MM-dd"), date2);
            }
            //年份
            if (date1 != "" && date2 == "")
            {
                strsql.AppendFormat(" AND costTime between '{0}' and '{1}' ", date1, DateTime.Now.ToString("yyyy-MM-dd"));
            }
            GetPreviewPowerSql(ref strsql);
            string sql = @"SELECT *,(SELECT unit_Name FROM tg_unit where unit_ID=c.costUnit)as unitName,
                        (SELECT costName FROM cm_CostType where ID=c.costTypeID)as typeName,
                    (SELECT mem_Name FROM tg_member where mem_ID=c.costUserId)as userName
                      FROM cm_CostDetails as c where 1=1" + strsql.ToString();
            //得到成本列表
            DataSet ds = TG.DBUtility.DbHelperSQL.Query(sql);
            DataTable dt = null;
            if (ds != null)
            {
                dt = ds.Tables[0];
            }
            //模板路径
            string modelPath = "";
            //得到成本类型 1:勘察2:施工图审查室（图文公司）3：总承包(承包公司)4:设计所
            string TypeImport = this.HiddenISImport.Value;
            string sqlType = @"  SELECT* FROM cm_CostType where costGroup IN (0,{0})";

            if (TypeImport == "1")
            {
                sqlType = string.Format(sqlType, 1);
                modelPath = " ~/TemplateXls/KanChaDetails.xls";
            }
            else if (TypeImport == "2")
            {
                sqlType = string.Format(sqlType, 2);
            }
            else if (TypeImport == "3")
            {
                sqlType = string.Format(sqlType, 3);
            }
            else if (TypeImport == "4")
            {
                sqlType = string.Format(sqlType, 4);
                modelPath = " ~/TemplateXls/SheJiDetails.xls";

            }
            DataSet dstype = TG.DBUtility.DbHelperSQL.Query(sqlType);
            DataTable dtType = new DataTable();
            if (dstype != null)
            {
                dtType = dstype.Tables[0];
            }
            if (unitid == "-1")
            {
                modelPath = " ~/TemplateXls/CostDetails.xls";
                ALlExportMethod(unitName, dt, modelPath, dtType);
            }
            else
            {
                if (TypeImport == "1")
                {
                    modelPath = " ~/TemplateXls/KanChaDetails.xls";
                    KanChaExportMethod(unitName, dt, modelPath, dtType);
                }
                else if (TypeImport == "2")
                {

                }
                else if (TypeImport == "3")
                {

                }
                else if (TypeImport == "4")
                {

                    modelPath = " ~/TemplateXls/SheJiDetails.xls";
                    ShejiExportMethod(unitName, dt, modelPath, dtType);
                }
            }
        }
        private void ALlExportMethod(string unitName, DataTable dt, string modelPath, DataTable dtType)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["costTime"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["costNum"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["costSub"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["costCharge"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["costOutCharge"].ToString());
                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["typeName"].ToString());
                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["unitName"].ToString());
                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["userName"].ToString());

            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(unitName + "成本明细.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }   
        }
        private void ShejiExportMethod(string unitName, DataTable dt, string modelPath, DataTable dtType)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }
            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);
            var dataRowtype = ws.GetRow(5);//读行
            ws.GetRow(3).GetCell(1).SetCellValue("部门:" + unitName);
            int row = 6;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行
                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["costTime"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["costNum"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["costSub"].ToString());
                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["costSub"].ToString());
                cell.SetCellFormula("SUM(E" + (i + row + 1) + ":AL" + (i + row + 1) + ")");
                for (int j = 4; j < dtType.Rows.Count; j++)
                {
                    //勘察分院                    
                    string cellValue = dataRowtype.GetCell(j).ToString();
                    cell = dataRow.CreateCell(j);
                    cell.CellStyle = style2;

                    if (dt.Rows[i]["typeName"].ToString().Contains(cellValue))
                    {
                        cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["costCharge"].ToString()));
                    }
                    else
                    {
                        cell.SetCellValue(0);
                    }

                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(unitName + "成本明细.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        private void KanChaExportMethod(string unitName, DataTable dt, string modelPath, DataTable dtType)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }
            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);
            var dataRowtype = ws.GetRow(1);//读行
            int row = 2;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行
                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["costTime"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["costNum"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["costSub"].ToString());
                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["costSub"].ToString());
                cell.SetCellFormula("SUM(E" + (i + row + 1) + ":AL" + (i + row + 1) + ")");
                for (int j = 4; j < dtType.Rows.Count; j++)
                {
                    //勘察分院                    
                    string cellValue = dataRowtype.GetCell(j).ToString();
                    cell = dataRow.CreateCell(j);
                    cell.CellStyle = style2;

                    if (dt.Rows[i]["typeName"].ToString().Contains(cellValue))
                    {
                        cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["costCharge"].ToString()));
                    }
                    else
                    {
                        cell.SetCellValue(0);
                    }

                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(unitName + "成本明细.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
    }
}