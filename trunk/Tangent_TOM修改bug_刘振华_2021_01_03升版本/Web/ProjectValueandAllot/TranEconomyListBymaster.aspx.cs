﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace TG.Web.ProjectValueandAllot
{
    public partial class TranEconomyListBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定年份
                BindYear();
                //选中当前年份
                SelectCurrentYear();
                //获取项目信息
                bindProjectValueNoList();
                bindProjectValueYesList();

                //绑定权限
                BindPreviewPower();
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //获取用户权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void bindProjectValueNoList()
        {
            StringBuilder strWhere = new StringBuilder();
            ////项目名称
            //if (this.txt_cprName.Text != "")
            //{
            //    strWhere.AppendFormat(" AND pro_name LIKE '%{0}%'", this.txt_cprName.Text.Trim());
            //}

            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    strWhere.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            //}

            GetPreviewPowerSql(ref strWhere);
            this.hid_where.Value = strWhere.ToString();


            //TG.BLL.cm_TranProjectValueAllot BLL = new TG.BLL.cm_TranProjectValueAllot();
            //取得分页总数
            // DataSet dsCount = BLL.P_cm_TranProjectValuelNoListCount(strWhere.ToString(), "jjs");
            // AspNetPager1.RecordCount = Convert.ToInt32(dsCount.Tables[0].Rows[0][0]);

            // DataSet ds = BLL.P_cm_TranProjectValuelNoByPager(AspNetPager1.StartRecordIndex, AspNetPager1.EndRecordIndex, strWhere.ToString(), "jjs");
            //   this.gvTranJjsAlloting.DataSource = ds;
            //   this.gvTranJjsAlloting.DataBind();
        }

        private void bindProjectValueYesList()
        {
            StringBuilder strWhere = new StringBuilder();
            ////合同名称
            //if (this.txt_cprName.Text != "")
            //{
            //    strWhere.AppendFormat(" AND pro_name LIKE '%{0}%'", this.txt_cprName.Text.Trim());
            //}

            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    strWhere.AppendFormat(" AND year(pro_startTime)={0}", this.drp_year.SelectedValue);
            //}

            GetPreviewPowerSql(ref strWhere);

            this.hid_where2.Value = strWhere.ToString();

            TG.BLL.cm_TranProjectValueAllot BLL = new TG.BLL.cm_TranProjectValueAllot();
            //取得分页总数
            //  DataSet dsCount = BLL.P_cm_TranProjectValuelYesListCount(strWhere.ToString(), "jjs");
            // AspNetPager2.RecordCount = Convert.ToInt32(dsCount.Tables[0].Rows[0][0]);

            // DataSet ds = BLL.P_cm_TranProjectValuelYesByPager(AspNetPager2.StartRecordIndex, AspNetPager2.EndRecordIndex, strWhere.ToString(), "jjs");
            //  this.gvTranJjsAlloted.DataSource = ds;
            //  this.gvTranJjsAlloted.DataBind();
        }

        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
    }
}