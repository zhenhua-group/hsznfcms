﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class CostAllDetailsBymaster : PageBase
    {
        TG.BLL.CostImportBymaster bll = new TG.BLL.CostImportBymaster();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                BindType();
                InitDate();
                BindPreviewPower();
                bindData();

            }
        }

        //绑定前台权限
        public void BindPreviewPower()
        {

        }
        //判断是否进行权限判断
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (costUserId =" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND costUnit = (Select unit_Id From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //绑定单位
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            //strWhere += " AND unit_ID NOT  in (236,250,251,252,253,254,255,256,257,258,259)";
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //初始时间设置
        protected void InitDate()
        {
            string str_year = DateTime.Now.Year.ToString();
            this.txt_costdate1.Value = str_year + "-01-01";
            this.txt_costdate2.Value = str_year + "-12-31";
        }
        //绑定基本类型
        private void BindType()
        {
            //throw new NotImplementedException();
            TG.BLL.cm_CostType costbll = new TG.BLL.cm_CostType();
            string strwhereAll = "costgroup in (0,1,2,3,4,5,6,7)";
            this.drp_costTypeAll.DataSource = costbll.GetList(strwhereAll);
            this.drp_costTypeAll.DataTextField = "costName";
            this.drp_costTypeAll.DataValueField = "ID";
            this.drp_costTypeAll.DataBind();
        }
        //绑定数据
        private void bindData()
        {
            // throw new NotImplementedException();
            StringBuilder sbtype = new StringBuilder();
            StringBuilder sbunit = new StringBuilder();
            StringBuilder sbtime = new StringBuilder();
            string unittype = this.drp_unitType.SelectedItem.Value;
            string unitId = this.drp_unit.SelectedItem.Value;
            string typeId = this.drp_costTypeAll.SelectedItem.Value;
            this.hiddenType.Value = typeId;
            this.hiddenUnit.Value = unitId;
            string starttime = this.txt_costdate1.Value;
            string endtime = this.txt_costdate2.Value;
            string unitName = this.drp_unit.SelectedItem.Text.Trim();
            string typeName = this.drp_costTypeAll.SelectedItem.Text.Trim();
            string titleName = "";
            if (unittype == "4")
            {
                if (unitId == "-1")
                {
                    if (typeId == "-1")
                    {
                        titleName = "生产部门 " + starttime + "至" + endtime + "成本费用明细统计";
                    }
                    else
                    {
                        titleName = "生产部门 " + starttime + "至" + endtime + " " + typeName + "费用明细统计";
                    }

                }
                else
                {
                    if (typeId == "-1")
                    {
                        titleName = unitName + " " + starttime + "至" + endtime + " " + "成本费用明细统计";
                    }
                    else
                    {
                        titleName = unitName + " " + starttime + "至" + endtime + " " + typeName + "费用明细统计";
                    }

                }
            }
            else
            {
                if (typeId == "-1")
                {
                    titleName = unitName + " " + starttime + "至" + endtime + " " + "成本费用明细统计";
                }
                else
                {
                    titleName = unitName + " " + starttime + "至" + endtime + " " + typeName + "费用明细统计";
                }
            }
            this.costTitle.Text = titleName;
            if (unittype == "4")
            {
                if (unitId == "-1")
                {
                    sbtype.Append("and costgroup in(0,4)");
                    sbunit.Append("and unit_ID not in (236,250,251,252,253,254,255,256,257,258,259)");
                }
                else
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,4)");
                }
            }
            else
            {
                if (unitId == "250")
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,1)");
                }
                else if (unitId == "252")
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,2)");
                }
                else if (unitId == "257")
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,3)");
                }
            }
            sbunit.Append("AND unit_ID NOT IN (" + base.NotShowUnitList + ")");

            if (typeId != "-1")
            {
                sbtype.Append("and id=" + typeId);
            }
            if (!string.IsNullOrEmpty(starttime) && !string.IsNullOrEmpty(endtime))
            {
                sbtime.AppendFormat(" AND costTime between ''{0}'' and ''{1}'' ", starttime, endtime);
            }
            //年份
            if (string.IsNullOrEmpty(starttime) && !string.IsNullOrEmpty(endtime))
            {
                sbtime.AppendFormat(" AND costTime between ''{0}'' and ''{1}'' ", DateTime.Now.ToString("yyyy-MM-dd"), endtime);

            }
            //年份
            if (!string.IsNullOrEmpty(starttime) && string.IsNullOrEmpty(endtime))
            {
                sbtime.AppendFormat(" AND costTime between ''{0}'' and ''{1}'' ", starttime, DateTime.Now.ToString("yyyy-MM-dd"));

            }
            this.grid_Financial.DataSource = bll.GetCostAllByProc(sbtime.ToString(), sbunit.ToString(), sbtype.ToString());
            this.grid_Financial.DataBind();
        }

        //P_cm_CostAllDetails



        protected void btn_export_Click(object sender, EventArgs e)
        {
            //
            StringBuilder sbtype = new StringBuilder();
            StringBuilder sbunit = new StringBuilder();
            StringBuilder sbtime = new StringBuilder();

            string unittype = this.drp_unitType.SelectedItem.Value;
            string unitId = this.drp_unit.SelectedItem.Value;
            string typeId = this.drp_costTypeAll.SelectedItem.Value;
            string starttime = this.txt_costdate1.Value;
            string endtime = this.txt_costdate2.Value;
            string unitName = this.drp_unit.SelectedItem.Text.Trim();
            string typeName = this.drp_costTypeAll.SelectedItem.Text.Trim();
            string titleName = "";
            if (unittype == "4")
            {
                if (unitId == "-1")
                {
                    if (typeId == "-1")
                    {
                        titleName = "生产部门 " + starttime + "至" + endtime + "成本费用明细统计";
                    }
                    else
                    {
                        titleName = "生产部门 " + starttime + "至" + endtime + " " + typeName + "费用明细统计";
                    }

                }
                else
                {
                    if (typeId == "-1")
                    {
                        titleName = unitName + " " + starttime + "至" + endtime + " " + "成本费用明细统计";
                    }
                    else
                    {
                        titleName = unitName + " " + starttime + "至" + endtime + " " + typeName + "费用明细统计";
                    }

                }
            }
            else
            {
                if (typeId == "-1")
                {
                    titleName = unitName + " " + starttime + "至" + endtime + " " + "成本费用明细统计";
                }
                else
                {
                    titleName = unitName + " " + starttime + "至" + endtime + " " + typeName + "费用明细统计";
                }
            }
            if (unittype == "4")
            {
                if (unitId == "-1")
                {
                    sbtype.Append("and costgroup in(0,4)");
                    sbunit.Append("and unit_ID not in (236,250,251,252,253,254,255,256,257,258,259)");
                }
                else
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,4)");
                }
            }
            else
            {
                if (unitId == "250")
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,1)");
                }
                else if (unitId == "252")
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,2)");
                }
                else if (unitId == "257")
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,3)");
                }
            }
            sbunit.Append("AND unit_ID NOT IN (" + base.NotShowUnitList + ")");

            if (typeId != "-1")
            {
                sbtype.Append("and id=" + typeId);
            }
            if (!string.IsNullOrEmpty(starttime) && !string.IsNullOrEmpty(endtime))
            {
                sbtime.AppendFormat(" AND costTime between ''{0}'' and ''{1}'' ", starttime, endtime);
            }
            //年份
            if (string.IsNullOrEmpty(starttime) && !string.IsNullOrEmpty(endtime))
            {
                sbtime.AppendFormat(" AND costTime between ''{0}'' and ''{1}'' ", DateTime.Now.ToString("yyyy-MM-dd"), endtime);

            }
            //年份
            if (!string.IsNullOrEmpty(starttime) && string.IsNullOrEmpty(endtime))
            {
                sbtime.AppendFormat(" AND costTime between ''{0}'' and ''{1}'' ", starttime, DateTime.Now.ToString("yyyy-MM-dd"));

            }
            SqlDataReader reader = bll.GetCostAllByProc(sbtime.ToString(), sbunit.ToString(), sbtype.ToString()) as SqlDataReader;
            DataTable dt = DataReaderToDataTable(reader);
            string modelPath = " ~/TemplateXls/CostAllDetailsBymaster.xls";
            ALlExportMethod(titleName, dt, modelPath);
        }

        private void ALlExportMethod(string unitName, DataTable dt, string modelPath)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);
            IRow rowFT = ws.CreateRow(0);
            rowFT.Height = 18 * 20;
            ICell icellFT = rowFT.CreateCell(0);
            icellFT.CellStyle = style2;
            icellFT.SetCellValue(unitName);
            IRow rowtitle = ws.CreateRow(1);
            int IcellCols = 0;
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                ICell icell = rowtitle.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellValue(dt.Columns[i].ToString());
                IcellCols = i;
            }
            ICell icellL = rowtitle.CreateCell(IcellCols + 1);
            icellL.CellStyle = style2;
            icellL.SetCellValue("合计");
            int row = 2;
            bool isGZ = false;
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                if (dt.Rows[j][0].ToString().Contains("工资"))
                {
                    isGZ = true;
                    IRow rowCount = ws.CreateRow(row);
                    rowCount.Height = 18 * 20;
                    ICell icellF = rowCount.CreateCell(0);
                    icellF.CellStyle = style2;
                    icellF.SetCellValue(dt.Rows[j][0].ToString());
                    ICell icellM = rowCount.CreateCell(IcellCols + 1);
                    icellM.CellStyle = style2;
                    double valColsA = 0.00;
                    for (int i = 1; i < dt.Columns.Count; i++)
                    {
                        ICell icell = rowCount.CreateCell(i);
                        icell.CellStyle = style2;
                        icell.SetCellValue(Convert.ToDouble(dt.Rows[j][i].ToString()));
                        valColsA += Convert.ToDouble(dt.Rows[j][i].ToString());
                    }
                    icellM.SetCellValue(valColsA);
                    row++;
                }
            }
            int gzcout = row;
            if (isGZ)
            {

                IRow rowGZ = ws.CreateRow(row);
                rowGZ.Height = 18 * 20;
                ICell icellGZ = rowGZ.CreateCell(0);
                icellGZ.CellStyle = style2;
                icellGZ.SetCellValue("工资合计");
                ICell icellM = rowGZ.CreateCell(IcellCols + 1);
                icellM.CellStyle = style2;
                double valColsA = 0.00;
                for (int i = 1; i < dt.Columns.Count; i++)
                {
                    ICell icellGZi = rowGZ.CreateCell(i);
                    icellGZi.CellStyle = style2;
                    double valCols = 0.00;
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        if (dt.Rows[j][0].ToString().Contains("工资"))
                        {
                            valCols += Convert.ToDouble(dt.Rows[j][i].ToString());
                        }
                    }
                    valColsA += valCols;
                    icellGZi.SetCellValue(valCols);
                }
                icellM.SetCellValue(valColsA);
                row++;
            }
            int fycount = row;
            bool isFY = false;
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                if (!dt.Rows[j][0].ToString().Contains("工资"))
                {
                    isFY = true;
                    IRow rowCount = ws.CreateRow(row);
                    rowCount.Height = 18 * 20;
                    ICell icellF = rowCount.CreateCell(0);
                    icellF.CellStyle = style2;
                    icellF.SetCellValue(dt.Rows[j][0].ToString());
                    ICell icellM = rowCount.CreateCell(IcellCols + 1);
                    icellM.CellStyle = style2;
                    double valColsA = 0.00;
                    for (int i = 1; i < dt.Columns.Count; i++)
                    {
                        ICell icell = rowCount.CreateCell(i);
                        icell.CellStyle = style2;
                        icell.SetCellValue(Convert.ToDouble(dt.Rows[j][i].ToString()));
                        valColsA += Convert.ToDouble(dt.Rows[j][i].ToString());
                    }
                    icellM.SetCellValue(valColsA);
                    row++;
                }
            }
            if (isFY)
            {
                IRow rowFY = ws.CreateRow(row);
                rowFY.Height = 18 * 20;
                ICell icellFY = rowFY.CreateCell(0);
                icellFY.CellStyle = style2;
                icellFY.SetCellValue("费用合计");
                ICell icellM = rowFY.CreateCell(IcellCols + 1);
                icellM.CellStyle = style2;
                double valColsA = 0.00;
                row++;
                for (int i = 1; i < dt.Columns.Count; i++)
                {
                    ICell icellFYi = rowFY.CreateCell(i);
                    icellFYi.CellStyle = style2;
                    double valCols = 0.00;
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        if (!dt.Rows[j][0].ToString().Contains("工资"))
                        {
                            valCols += Convert.ToDouble(dt.Rows[j][i].ToString());
                        }
                    }
                    valColsA += valCols;
                    icellFYi.SetCellValue(valCols);
                }
                icellM.SetCellValue(valColsA);
            }
            IRow rowHJ = ws.CreateRow(row);
            rowHJ.Height = 18 * 20;
            ICell icellHJ = rowHJ.CreateCell(0);
            icellHJ.CellStyle = style2;
            icellHJ.SetCellValue("合计");
            ICell icellMA = rowHJ.CreateCell(IcellCols + 1);
            icellMA.CellStyle = style2;
            double valColsAA = 0.00;
            for (int i = 1; i < dt.Columns.Count; i++)
            {
                ICell icellHJi = rowHJ.CreateCell(i);
                icellHJi.CellStyle = style2;
                double valCols = 0.00;
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    valCols += Convert.ToDouble(dt.Rows[j][i].ToString());
                }
                valColsAA += valCols;
                icellHJi.SetCellValue(valCols);
            }
            icellMA.SetCellValue(valColsAA);
            //int rows = 2;
            //int rowss = 2;
            //int cols = 0;

            //if (reader.HasRows)
            //{
            //    cols = reader.FieldCount;
            //    int colsedit = reader.FieldCount;
            //    while (reader.Read())
            //    {
            //        //

            //        if (reader.GetValue(0).ToString().Contains("工资"))
            //        {
            //            IRow row = ws.CreateRow(rows);
            //            row.Height = 18 * 20;
            //            for (int i = 0; i < cols; i++)
            //            {
            //                ICell icell = row.CreateCell(i);
            //                icell.CellStyle = style2;
            //                icell.SetCellValue(Convert.ToDouble(reader.GetValue(i).ToString()));
            //            }
            //            rows++;
            //            IRow rowhj = ws.CreateRow(rows);
            //            ICell icellHj = rowhj.CreateCell(0);
            //            icellHj.CellStyle = style2;
            //            icellHj.SetCellValue("工资合计");
            //            for (int i = 1; i < cols; i++)
            //            {
            //                ICell icell = rowhj.CreateCell(i);
            //                icell.CellStyle = style2;
            //                icell.SetCellValue("");
            //            }
            //        }
            //        else
            //        {
            //            IRow row = ws.CreateRow(rowss);
            //            row.Height = 18 * 20;
            //            for (int i = 0; i < cols; i++)
            //            {
            //                ICell icell = row.CreateCell(i);
            //                icell.CellStyle = style2;
            //                icell.SetCellValue(reader.GetValue(i).ToString());
            //            }
            //            rowss++;
            //            IRow rowhj = ws.CreateRow(rowss);
            //            for (int i = 1; i < cols; i++)
            //            {
            //                ICell icell = rowhj.CreateCell(i);
            //                icell.CellStyle = style2;
            //                icell.SetCellValue("成本合计");
            //            }
            //        }
            //    }
            //    IRow rowcout = ws.CreateRow(rowss + 1);
            //    rowcout.Height = 18 * 20;
            //    for (int i = 0; i < cols; i++)
            //    {
            //        ICell icell = rowcout.CreateCell(i);
            //        icell.CellStyle = style2;
            //        icell.SetCellValue("宗二吉");
            //    }
            //}

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(unitName + "成本明细.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        protected string SetWord(int num)
        {
            string word = "A";
            switch (num)
            {
                case 1:
                    word = "A";
                    break;
            }
            return word;
        }
        protected void btn_search_Click(object sender, EventArgs e)
        {
            //
            bindData();
        }
        /// <summary>
        /// SqlDataReader转成DataTable实现方法
        /// </summary>
        /// <param name="strSql">Sql语句</param>
        /// <returns></returns>
        public DataTable DataReaderToDataTable(SqlDataReader dataReader)
        {
            DataTable dtReturn = null;
            object[] value = null;
            try
            {
                if (dataReader.HasRows)
                {
                    dtReturn = CreateTableBySchemaTable(dataReader.GetSchemaTable());
                    value = new object[dataReader.FieldCount];
                    while (dataReader.Read())
                    {
                        dataReader.GetValues(value);
                        dtReturn.LoadDataRow(value, true);
                    }
                    value = null;
                }

            }
            catch (Exception err)
            { }
            finally
            {
                dataReader.Close();
            }
            return dtReturn;
        }
        public DataTable CreateTableBySchemaTable(DataTable pSchemaTable)
        {
            DataTable dtReturn = new DataTable();
            DataColumn dc = null;
            DataRow dr = null;
            for (int i = 0; i < pSchemaTable.Rows.Count; i++)
            {
                dr = pSchemaTable.Rows[i];
                dc = new DataColumn(dr["ColumnName"].ToString(), dr["DataType"] as Type);
                dtReturn.Columns.Add(dc);
            }
            dr = null;
            dc = null;
            return dtReturn;
        }
    }
}