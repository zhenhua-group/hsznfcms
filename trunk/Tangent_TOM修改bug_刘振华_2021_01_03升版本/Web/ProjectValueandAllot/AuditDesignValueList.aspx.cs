﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using NPOI;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace TG.Web.ProjectValueandAllot
{
    public partial class AuditDesignValueList : PageBase
    {
        public string AllotType
        {
            get
            {
                return Request["type"] ?? "";
            }
        }
        public string AllotTypeString
        {
            get
            {
                if (this.AllotType == "1")
                {
                    return "审核";
                }
                else
                {
                    return "设计";
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定年份
                BindYear();
                //选中当前年份
                SelectCurrentYear();
                //绑定
                BindUnit();
            }
        }

        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }

        //获取用户权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (p.InsertUserID =" + UserSysNo + " OR p.PMUserID=" + UserSysNo + " or pro_id in (select pro_id from tg_relation where mem_roleids like '%1%') ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }


        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += "  AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //建筑
            this.SpeID = "1";
            DataTable dt = GetSearchDataTable();
            this.ArchUserHtml = CreateTableHtml(dt);
            this.SpeID = "2";
            dt = GetSearchDataTable();
            this.StructUserHtml = CreateTableHtml(dt);
        }

        private DataTable GetSearchDataTable()
        {
            //部门
            StringBuilder sb = new StringBuilder("");
            if (this.drp_unit.SelectedValue != "-1")
            {
                sb.AppendFormat(" AND UserID IN (Select mem_ID From tg_member Where mem_Unit_ID={0})", this.drp_unit.SelectedValue);
            }
            else
            {
                sb.AppendFormat(" AND UserID IN (Select mem_ID From tg_member Where mem_Unit_ID={0})", this.UserUnitNo);
            }
            //年份
            if (this.drp_year.SelectedValue != "-1")
            {
                sb.AppendFormat(" AND AllotYear={0}", this.drp_year.SelectedValue);
            }
            sb.AppendFormat(" AND AllotType='{0}' ", this.AllotType);
            sb.AppendFormat(" AND SpeID={0} ", this.SpeID);
            string strSql = string.Format(@"Select Max(UserName) as UserName,
		                                    SUM(AllotMoney) as AllotMoney,
		                                    MAX(AllotYear) as AllotYear
                                            From cm_AuditDesignCoefficient
                                            Where 1=1 {0} Group By UserID", sb.ToString());

            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];
            return dt;
        }
        public string SpeID { get; set; }
        public string ArchUserHtml { get; set; }
        public string StructUserHtml { get; set; }
        protected string CreateTableHtml(DataTable dt)
        {
            StringBuilder html = new StringBuilder();
            foreach (DataRow dr in dt.Rows)
            {
                html.Append("<tr>");
                html.AppendFormat("<td>{0}</td>", dr["UserName"].ToString());
                html.AppendFormat("<td>{0}</td>", dr["AllotMoney"].ToString());
                html.AppendFormat("<td>{0}</td>", dr["AllotYear"].ToString());
                string allottype = "审核";
                if (this.AllotType == "2")
                    allottype = "设计";
                html.AppendFormat("<td>{0}</td>", allottype);
                html.Append("</tr>");
            }

            return html.ToString();
        }

        protected void btnArchExport_Click(object sender, EventArgs e)
        {
            this.SpeID = "1";
            ExportExcel();
        }

        protected void btnStructExport_Click(object sender, EventArgs e)
        {
            this.SpeID = "2";
            ExportExcel();
        }

        protected void ExportExcel()
        {
            string modelPath = " ~/TemplateXls/ProjectPersonAuditAllot.xls";

            string fileName = this.drp_year.SelectedValue + "年项目审核金额分配个人汇总表";
            if (this.AllotType == "2")
            {
                fileName = this.drp_year.SelectedValue + "年项目自留设计费分配个人汇总表";
            }

            if (this.SpeID == "1")
            {
                fileName = fileName + "(建筑)";
            }
            else
            {
                fileName = fileName + "(结构)";
            }

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);


            int row = 2;
            DataTable dt = GetSearchDataTable();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["UserName"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dt.Rows[i]["AllotMoney"].ToString()));

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["AllotYear"].ToString());


            }

            row = row + dt.Rows.Count;

            IRow row2 = ws.CreateRow(row);
            ICell cellm1 = row2.CreateCell(0);
            cellm1.SetCellValue("");
            cellm1.CellStyle = style2;

            cellm1 = row2.CreateCell(1);
            cellm1.SetCellValue("本页合计");
            cellm1.CellStyle = style2;

            cellm1 = row2.CreateCell(2);
            cellm1.SetCellFormula("sum(C3:C" + row + ")");
            cellm1.CellStyle = style2;

            cellm1 = row2.CreateCell(3);
            cellm1.SetCellValue("");
            cellm1.CellStyle = style2;

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(fileName + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
    }
}