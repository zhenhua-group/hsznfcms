﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using System.Data;
using AjaxPro;
using TG.Model;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProAllot : PageBase
    {
        TG.BLL.cm_Project bll_proj = new TG.BLL.cm_Project();
        TG.Model.cm_Project model_proj = new TG.Model.cm_Project();
        TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();

        /// <summary>
        /// 项目ID
        /// </summary>
        public string StrProjectId
        {
            get { return ViewState["PRO_ID"].ToString(); }
        }
        /// <summary>
        /// 合同ID
        /// </summary>
        public string StrCoperationId
        {
            get { return ViewState["CPR_ID"].ToString(); }
        }
        /// <summary>
        /// 结构类型
        /// </summary>
        public string StrStructType
        {
            get;
            set;
        }
        /// <summary>
        /// 是否分配过
        /// </summary>
        public string IsAllot
        {
            set;
            get;
        }
        /// <summary>
        /// 是否已经完成分配
        /// </summary>
        public string IsComplete
        {
            get
            {
                decimal result = 0;
                string proid = ViewState["PRO_ID"].ToString();
                string strSql = " SELECT Sum(CurRate) FROM dbo.cm_ProjectCost WHERE pro_ID=" + proid;
                DataSet ds = bll.GetList(strSql);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string value = ds.Tables[0].Rows[0][0].ToString();
                        if (value != "")
                        {
                            result = Convert.ToDecimal(value);
                        }
                    }
                }
                //返回值
                if (result >= 100)
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
        }
        /// <summary>
        /// 浏览动作
        /// </summary>
        public string ActionFlag
        {
            get
            {
                return Request.QueryString["flag"] ?? "";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProAllot));

            if (!IsPostBack)
            {
                //产值分配
                GetProAllot();
                //项目信息
                GetProInfo();
            }
        }
        //绑定分配信息
        private void GetProAllot()
        {
            string proid = Request.QueryString["pro_id"] ?? "0";
            ViewState["PRO_ID"] = proid;
            TG.BLL.ProjectAllotBP bll_allot = new TG.BLL.ProjectAllotBP();
            DataSet ds_allot = bll_allot.GetAllotInfo(proid);

            if (ds_allot.Tables[0].Rows.Count > 0)
            {
                IsAllot = "1";
            }
            else
            {
                IsAllot = "0";
            }
            ViewState["IsAllot"] = IsAllot;

            gvAllot.DataSource = ds_allot;
            gvAllot.DataBind();
        }
        /// <summary>
        /// 获取信息
        /// </summary>
        public void GetProInfo()
        {
            //绑定项目信息
            string proid = ViewState["PRO_ID"].ToString();
            TG.Model.cm_Project model_pro = bll_proj.GetModel(int.Parse(proid));
            if (model_pro != null)
            {
                this.txt_proName.Text = model_pro.pro_name ?? "";
                txtproNo.Text = model_pro.JobNum ?? "";
                txtscale.Text = Convert.ToString(model_pro.ProjectScale ?? null);
                txtstarttime.Text = model_pro.StartTimeString;
                txtfinishtime.Text = model_pro.FinishTimeString;
                //结构类型
                StrStructType = new TG.BLL.cm_Project().CreateULHTML(new TG.BLL.cm_Project().ResolveProjectStruct(model_pro.pro_StruType));
                //合同信息
                if (model_pro.cprID != null)
                {
                    ViewState["CPR_ID"] = model_pro.cprID;
                    txtcpr.Text = model_pro.Project_reletive ?? "";
                    txtcprAcount.Text = Convert.ToString(model_pro.Cpr_Acount ?? null);
                    txtproManager.Text = model_pro.ChgJia ?? "";
                    txtbuildUnit.Text = model_pro.Unit ?? "";
                    //项目工号
                    string strCprNo = new TG.BLL.cm_Coperation().GetModel(int.Parse(model_pro.cprID)).cpr_No;
                    txtcprNo.Text = strCprNo;
                    //保存合同ID
                    this.hid_cprid.Value = model_pro.cprID;
                }
                //工号信息
                string strWhere = " Pro_Id=" + proid;
                TG.BLL.cm_projectNumber bll_num = new TG.BLL.cm_projectNumber();
                DataSet ds_result = bll_num.GetList(strWhere);
                if (ds_result.Tables.Count > 0)
                {
                    if (ds_result.Tables[0].Rows.Count > 0)
                    {
                        this.txtproNo.Text = ds_result.Tables[0].Rows[0][4].ToString();
                    }
                }
            }
        }
        //未关联合同时新添合同
        protected void btnSave_Click(object sender, EventArgs e)
        {
            //如果是选择的合同信息
            if (this.hid_cprid.Value != "0")
            {
                model_proj = bll_proj.GetModel(int.Parse(ViewState["PRO_ID"].ToString()));
                if (model_proj != null)
                {
                    model_proj.cprID = this.hid_cprid.Value;
                    model_proj.Project_reletive = this.txtcpr.Text;
                    model_proj.Cpr_Acount = Convert.ToDecimal(this.txtcprAcount.Text);
                    model_proj.ProjectScale = Convert.ToDecimal(this.txtscale.Text);
                    model_proj.Unit = this.txtbuildUnit.Text;
                    model_proj.BuildAddress = this.hid_addr.Value;
                    model_proj.ChgJia = this.txtproManager.Text;
                    //更新合同信息
                    if (bll_proj.Update(model_proj))
                    {
                        GetProInfo();
                        GetProAllot();
                    }
                }
            }
            else //直接添加的合同信息
            {
                TG.Model.cm_Coperation model_cpr = new TG.Model.cm_Coperation();
                TG.BLL.cm_Coperation bll_cpr = new TG.BLL.cm_Coperation();
                model_cpr.cpr_Name = this.txtcpr.Text;
                model_cpr.cpr_Acount = Convert.ToDecimal(this.txtcprAcount.Text);
                model_cpr.BuildArea = this.hid_addr.Value;
                model_cpr.ChgJia = this.txtproManager.Text;
                int iCprId = bll_cpr.Add(model_cpr);
                if (iCprId > 0)
                {
                    model_proj = bll_proj.GetModel(int.Parse(ViewState["PRO_ID"].ToString()));
                    if (model_proj != null)
                    {
                        model_proj.cprID = iCprId.ToString();
                        model_proj.Project_reletive = this.txtcpr.Text;
                        model_proj.Cpr_Acount = Convert.ToDecimal(this.txtcprAcount.Text);
                        model_proj.ProjectScale = Convert.ToDecimal(this.txtscale.Text);
                        model_proj.Unit = this.txtbuildUnit.Text;
                        model_proj.BuildAddress = this.hid_addr.Value;
                        model_proj.ChgJia = this.txtproManager.Text;
                        //更新合同信息
                        if (bll_proj.Update(model_proj))
                        {
                            GetProInfo();
                            GetProAllot();
                        }
                    }
                }
            }
        }
        //显示百分比
        protected void gvAllot_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //收费次数
                Label lbl_cs = e.Row.Cells[0].FindControl("Label1") as Label;
                lbl_cs.Text = "第" + lbl_cs.Text + "次分配";
                //本次收费比例
                e.Row.Cells[2].Text = e.Row.Cells[2].Text + "%";
            }
        }

        protected void btn_allot_Click(object sender, ImageClickEventArgs e)
        {
            //检查是否已经完成分配
            if (IsComplete == "1")
            {
                TG.Common.MessageBox.Show(this, "已经完成分配！");
                return;
            }

            //检查是否有未完成的分配流程
            string isCompleteSQL = string.Format("select top 1 1 from [View_cm_ProjectAllot] where ProjectSysNo ={0} and (isnull([PartTwoReferenceSysNo],0) =0 or isnull([PartThreeReferenceSysNo],0)=0)", StrProjectId);
            object hasCompleteObj = TG.DBUtility.DbHelperSQL.GetSingle(isCompleteSQL);

            if (hasCompleteObj != null)
            {
                Response.Write("<script type=\"text/javascript\">alert(\"有正在分配中项，不能再次发起分配。请先完成之前的分配流程！\");window.location.href=\"/ProjectValueandAllot/ProValueandAlltList.aspx\";</script>");
                Response.End();
                return;
            }


            //没有完成分配的场合
            //新规一条记录
            object objPartOneSysNo = TG.DBUtility.DbHelperSQL.GetSingle(string.Format("insert into cm_ProjectCost(pro_ID,InUser)values({0},{1});select @@IDENTITY", StrProjectId, UserSysNo));

            int partOneSysNo = objPartOneSysNo == null ? 0 : Convert.ToInt32(objPartOneSysNo);

            int roleSysNo = new TG.BLL.cm_ProjectAllot().GetProjectFeeAllotConfigRole(1);

            //得到项目名称
            TG.Model.cm_Project project = new TG.BLL.cm_Project().GetModel(int.Parse(StrProjectId));
            //检查是否已经分配
            IsAllot = Convert.ToString(ViewState["IsAllot"] ?? "0");

            SysMessageViewEntity msg = new SysMessageViewEntity
            {
                ReferenceSysNo = "cost_id=" + partOneSysNo.ToString() + "&projectSysNo=" + StrProjectId + "&IsAllot=" + IsAllot,
                FromUser = UserSysNo,
                InUser = UserSysNo,
                MsgType = 5,
                MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", project == null ? "" : project.pro_name, "项目产值分配审核"),
                QueryCondition = project == null ? "" : project.pro_name,
                Status = "A",
                ToRole = roleSysNo.ToString()
            };

            int count = new TG.BLL.cm_SysMsg().InsertSysMessage(msg);
            if (count > 0)
            {
                TG.Common.MessageBox.ShowAndRedirect(this, "分配申请成功，已经发送消息到下一环节填写人！", "ProValueandAlltList.aspx");
            }
            else
            {
                TG.Common.MessageBox.Show(this, "分配申请成功失败！");
            }
        }
    }
}
