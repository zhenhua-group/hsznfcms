﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintValue.aspx.cs" Inherits="TG.Web.ProjectValueandAllot.PrintValue" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>项目产值分配打印</title>
    <style type="text/css">
        .cls_content_head
        {
            width: 100%;
            height: 25px; /*margin: 0 auto;
            border: solid 1px black;*/
            border-collapse: collapse;
            font-size: 12px;
            font-family: "微软雅黑";
        }
        
        .cls_content_head td
        {
            border: solid 1px black;
            font-size: 12px;
            font-family: "微软雅黑";
            text-align: center;
        }
        
        .cls_content_head tr
        {
            height: 25px;
        }
        
        .cls_content
        {
            width: 100%;
            height: 25px; /*margin: 0 auto;
            border: solid 1px black;*/
            border-collapse: collapse;
            font-size: 12px;
            font-family: "微软雅黑";
        }
        
        .cls_content td
        {
            border: solid 1px black;
            font-size: 12px;
            font-family: "微软雅黑";
            text-align: center;
        }
        .cls_content tr
        {
            height: 25px;
        }
        .cls_head
        {
            height: 25px;
            font-size: 16px;
            text-align: center;
            font-weight: bold;
        }
        .cls_container
        {
            width: 80%;
            border-collapse: collapse;
            margin: 0 auto;
        }
        .cls_content_second
        {
            width: 100%;
            height: 25px;
            border-collapse: collapse;
            font-size: 12px;
            font-family: "微软雅黑";
        }
        
        .cls_content_second td
        {
            border: solid 1px black;
            font-size: 12px;
            font-family: "微软雅黑";
        }
        .cls_content_second tr
        {
            height: 25px;
        }
    </style>
    <style type="text/css" media="print">
        .NoPrint
        {
            display: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin: 0 auto; width: 90%;">
        <table class="cls_content_head" align="center">
            <tr>
                <td colspan="3" align="center" class="cls_head" style="height: 35px;">
                    <asp:Label ID="lbl_unit" runat="server"></asp:Label>
                    <asp:Label ID="lbl_Year" runat="server"></asp:Label>年<asp:Label ID="lblType" runat="server"></asp:Label>产值分配表
                </td>
            </tr>
            <tr>
                <td style="width: 15%;">
                    项目名称：
                </td>
                <td colspan="2" style="width: 85%;">
                    <asp:Label ID="lblProjectName" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 15%;">
                    分配产值：
                </td>
                <td colspan="2" style="width: 85%;">
                    <asp:Label ID="lblAllcountCount" runat="server" Text=""></asp:Label>(元)
                </td>
            </tr>
        </table>
        <asp:Label ID="lbl_Member" runat="server" Text="Label"></asp:Label>
        <table style="width: 100%; border: solid 1px black; height: 25px; font-size: 14px;">
            <tr>
                <td style="font-family: 微软雅黑;">
                    &nbsp; &nbsp; &nbsp; &nbsp; 所长：&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                    &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                    &nbsp;&nbsp;年&nbsp;&nbsp; &nbsp; 月&nbsp;&nbsp; &nbsp; 日
                </td>
            </tr>
        </table>
        <asp:Literal ID="lblSecondDetail" runat="server"></asp:Literal>
        <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="Table1">
            <tr>
                <td style="width: 100%; text-align: center;">
                    <input id="print" type="button" onclick="window.print()" value="打印" class="NoPrint"
                        style="width: 75px; height: 23px; cursor: pointer;" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
