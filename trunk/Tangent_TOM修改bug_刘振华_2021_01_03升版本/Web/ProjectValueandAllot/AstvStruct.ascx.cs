﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class AstvStruct : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Bindtreeview();
        }
        /// <summary>
        /// 向astreeview里添加值
        /// </summary>
        public void Bindtreeview()
        {
            this.asTreeviewStruct.RootNode
                .AppendChild(new ASTreeViewLinkNode("混凝土结构体系", "混凝土结构体系")
                .AppendChild(new ASTreeViewLinkNode("框架-支撑", "框架-支撑"))
                .AppendChild(new ASTreeViewLinkNode("框剪", "框剪"))
                .AppendChild(new ASTreeViewLinkNode("框架-核心筒", "框架-核心筒"))
                .AppendChild(new ASTreeViewLinkNode("筒中筒", "筒中筒"))
                .AppendChild(new ASTreeViewLinkNode("剪力墙", "剪力墙"))
                .AppendChild(new ASTreeViewLinkNode("短肢-剪力墙", "短肢-剪力墙"))
                .AppendChild(new ASTreeViewLinkNode("框架", "框架"))
                .AppendChild(new ASTreeViewLinkNode("部分框支抗震墙", "部分框支抗震墙"))
                .AppendChild(new ASTreeViewLinkNode("板柱剪力墙结构", "板柱剪力墙结构"))
                .AppendChild(new ASTreeViewLinkNode("异形柱框架（剪力墙）", "异形柱框架（剪力墙）"))
                )
                .AppendChild(new ASTreeViewLinkNode("钢结构体系", "钢结构体系")
                .AppendChild(new ASTreeViewLinkNode("框架结构", "框架结构"))
                .AppendChild(new ASTreeViewLinkNode("框架-支撑", "框架-支撑"))
                )
                .AppendChild(new ASTreeViewLinkNode("混合结构体系", "混合结构体系")
                .AppendChild(new ASTreeViewLinkNode("混合框架结构", "混合框架结构"))
                .AppendChild(new ASTreeViewLinkNode("钢框架-钢筋（钢骨）混凝土剪力墙", "钢框架-钢筋（钢骨）混凝土剪力墙"))
                .AppendChild(new ASTreeViewLinkNode("混合框架-钢筋(钢骨)混凝土剪力墙", "混合框架-钢筋(钢骨)混凝土剪力墙"))
                .AppendChild(new ASTreeViewLinkNode("钢框架-钢筋(钢骨)混凝土核心筒", "钢框架-钢筋(钢骨)混凝土核心筒"))
                .AppendChild(new ASTreeViewLinkNode("混合框架-钢筋(钢骨)混凝土核心筒", "混合框架-钢筋(钢骨)混凝土核心筒"))
                .AppendChild(new ASTreeViewLinkNode("钢框筒-钢筋混凝土内筒", "钢框筒-钢筋混凝土内筒"))
                .AppendChild(new ASTreeViewLinkNode("混合框筒-钢筋混凝土内筒", "混合框筒-钢筋混凝土内筒"))
                 .AppendChild(new ASTreeViewLinkNode("钢框筒-钢骨混凝土内筒", "钢框筒-钢骨混凝土内筒"))
                  .AppendChild(new ASTreeViewLinkNode("混合框筒-钢骨混凝土内筒", "混合框筒-钢骨混凝土内筒"))
                )
                .AppendChild(new ASTreeViewLinkNode("砌体结构体系", "砌体结构体系")
                .AppendChild(new ASTreeViewLinkNode("多层砌体房屋", "多层砌体房屋"))
                .AppendChild(new ASTreeViewLinkNode("底部框架-抗震墙房屋", "底部框架-抗震墙房屋"))
                .AppendChild(new ASTreeViewLinkNode("多排柱内框架房屋", "多排柱内框架房屋"))
                )
                .AppendChild(new ASTreeViewLinkNode("单层厂房", "单层厂房")
                .AppendChild(new ASTreeViewLinkNode("排架结构(砼或钢)", "排架结构(砼或钢)"))
                .AppendChild(new ASTreeViewLinkNode("门式刚架轻型钢结构", "门式刚架轻型钢结构"))
                );
            //TextBox1.Attributes.Add("onclick", this.asTreeviewStruct.GetCollapseAllScript() + "return false");

        }
    }
}