﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjectValueAuditSpeHeadBymaster : PageBase
    {
        #region 参数

        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }

        //项目ID
        public int Pro_ID
        {
            get
            {
                int proID = 0;
                int.TryParse(Request["proID"], out proID);
                return proID;
            }
        }

        //分配ID
        public int AllotID
        {
            get
            {
                int allotID = 0;
                int.TryParse(Request["allotID"], out allotID);
                return allotID;
            }
        }
        /// <summary>
        /// 审核记录系统编号
        /// </summary>
        public int ValueAllotAuditSysNo
        {
            get
            {
                int valueAllotAuditSysNo = 0;
                int.TryParse(Request["ValueAllotAuditSysNo"], out valueAllotAuditSysNo);
                return valueAllotAuditSysNo;
            }
        }
        /// <summary>
        /// 发送人
        /// </summary>
        public int InUserId
        {
            get
            {
                int userId = 0;
                int.TryParse(Request["inUserId"], out userId);
                return userId;
            }
        }
        public string IsDone { get; set; }

        /// <summary>
        /// 是否四舍五入
        /// </summary>
        public string IsRounding = ConfigurationManager.AppSettings["IsRounding"] ?? "0";

        public int CoperationProcess { get; set; }

        public string SpeName { get; set; }

        /// <summary>
        /// 产值类型
        /// </summary>
        public string proType
        {
            get
            {
                return Request["proType"];
            }
        }
        #endregion

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getProInfo();

                getMemberStatusInfo();

                CreateDataTableByMember();
            }
        }
        /// <summary>
        /// 项目信息
        /// </summary>
        private void getProInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(Pro_ID);

            if (pro_model != null)
            {
                //项目名称
                lblProjectName.Text = pro_model.pro_name;
                //关联合同
                this.lblReletive.Text = pro_model.Project_reletive;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }


                //建设规模
                this.lblScale.Text = pro_model.ProjectScale.ToString();
                //建筑类别
                lblBuildType.Text = pro_model.BuildType;
                //项目总负责
                this.lblPMName.Text = pro_model.PMName;
                //承接部门
                lblcpr_Unit.Text = pro_model.Unit;

                //项目阶段

                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }

                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }
            }

        }

        /// <summary>
        /// 得到人员审批状态信息
        /// </summary>
        private void getMemberStatusInfo()
        {
            TG.Model.cm_ProjectValueByMemberAuditStatus model = new TG.Model.cm_ProjectValueByMemberAuditStatus();
            model = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetModel(Pro_ID, AllotID, InUserId);
            if (model != null)
            {
                AuditSuggsion.InnerText = model.AuditSuggsion;
            }
        }

        /// <summary>
        /// 创建人员的表单
        /// </summary>
        private void CreateDataTableByMember()
        {
            //取得该用户信息
            TG.BLL.tg_member bll = new TG.BLL.tg_member();
            TG.Model.tg_member model = new TG.Model.tg_member();
            model = bll.GetModel(UserSysNo);


            TG.BLL.cm_ProjectValueAllot bll_allot = new TG.BLL.cm_ProjectValueAllot();

            TG.Model.cm_ProjectValueAllot model_allot = bll_allot.GetModel(AllotID);

            CoperationProcess = 0;
            if (model_allot != null)
            {
                CoperationProcess = int.Parse(model_allot.Itemtype.ToString());
            }

            //判断该审批是否完成
            IsDone = new TG.BLL.cm_SysMsg().IsDone(MessageID);

            if (IsDone.Trim() == "D")
            {
                CreateDataTableIsAuditUser(model.mem_Speciality_ID);
            }
            else
            {

                TG.Model.cm_Project model_pro = new TG.BLL.cm_Project().GetModel(Pro_ID);
                int proReferenceSysNo = model_pro == null ? 0 : model_pro.ReferenceSysNo;

                DataTable dtValue = new TG.BLL.cm_ProjectValueAuditRecord().GetProjectDesignProcessValueDetails(AllotID).Tables[0];

                DataTable dtmember = new TG.BLL.cm_ProjectValueAuditRecord().GetIsAuditedUserAcount(AllotID, model.mem_Speciality_ID).Tables[0];


                //取得设计人员的专业
                var dtSpecialtyList = new DataView(dtmember).ToTable("Specialty", true, "spe_Name");


                //判断改专业是否保存完毕
                DataTable dtMemberBySpecialty = new DataTable();
                DataTable dtProcess = new DataTable();
                if (dtmember.Rows.Count > 0)
                {

                    string speName = dtSpecialtyList.Rows[0]["spe_Name"].ToString();

                    SpeName = speName;

                    //取得方案工序金额
                    DataTable dtProgramData = new DataView(dtValue) { RowFilter = "Specialty='" + speName + "'AND type=0" }.ToTable();

                    //取得初步工序金额
                    DataTable dtPreliminaryData = new DataView(dtValue) { RowFilter = "Specialty='" + speName + "'AND type=1" }.ToTable();

                    //取得施工图工序金额
                    DataTable dtWorkDrawData = new DataView(dtValue) { RowFilter = "Specialty='" + speName + "'AND type=2" }.ToTable();

                    //取得后期工序金额
                    DataTable dtLateStageData = new DataView(dtValue) { RowFilter = "Specialty='" + speName + "'AND type=3" }.ToTable();

                    //显示工序
                    StringBuilder sbTotal = new StringBuilder();
                    sbTotal.Append(" <div id=\"memberProcessTotal\" class=\"cls_Container_Report\">");
                    sbTotal.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> ");

                    if (CoperationProcess >= 4 && CoperationProcess != 10)
                    {
                        if (CoperationProcess == 4)
                        {
                            sbTotal.Append("<tr><td > " + speName + "专业-室外工程工序产值合计<br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
                        }
                        else if (CoperationProcess == 5)
                        {
                            sbTotal.Append("<tr><td  > " + speName + "专业-锅炉房工序产值合计<br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
                        }
                        else if (CoperationProcess == 6)
                        {
                            sbTotal.Append("<tr><td  > " + speName + "专业-地上单建水泵房工序产值合计<br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
                        }
                        else if (CoperationProcess == 7)
                        {
                            sbTotal.Append("<tr><td > " + speName + "专业-地上单建变配所(室)工序产值合计<br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span> </td></tr>");
                        }
                        else if (CoperationProcess == 8)
                        {
                            sbTotal.Append("<tr><td  > " + speName + "专业-单建地下室（车库）工序产值合计 <br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span></td></tr>");
                        }
                    }
                    else
                    {
                        sbTotal.Append("<tr><td > " + speName + "专业各工序产值合计 <br><span style=\"color:Red;font-weight:bold;\">注释：1：建筑、结构的审核金额扣除到所留产值。2：所领导班子的施工图设计金额50%扣除到所留产值。</span></td></tr>");
                    }

                    sbTotal.Append("</table>");
                    sbTotal.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                    sbTotal.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 专业</td>");
                    sbTotal.Append("<td  style=\"width:15%;text-align: center;\">设计阶段</td>");
                    sbTotal.Append("<td  style=\"width:15%;text-align: center;\">审核金额(元)</td>");
                    sbTotal.Append("<td  style=\"width:15%;text-align: center;\">专业负责金额(元)</td>");
                    sbTotal.Append("<td  style=\"width:15%;text-align: center;\">校对金额(元)</td>");
                    sbTotal.Append("<td  style=\"width:15%;text-align: center;\">设计金额(元)</td>");
                    sbTotal.Append("<td  style=\"width:15%;text-align: center;\">合计(元)</td>");
                    sbTotal.Append("</tr></table>");
                    sbTotal.Append(" <table class=\"cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\"  width=\"98%\">");
                    if (CoperationProcess == 0)
                    {
                        sbTotal.Append("<tr><td align=\"center\" rowspan=\"4\" class=\"cls_Column\" style=\"width: 10%;\">" + speName + "</td>");
                    }
                    else if (CoperationProcess == 1 || CoperationProcess == 2)
                    {
                        sbTotal.Append("<tr><td align=\"center\" rowspan=\"2\" class=\"cls_Column\" style=\"width: 10%;\">" + speName + "</td>");
                    }
                    else if (CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        sbTotal.Append("<tr><td align=\"center\" rowspan=\"3\" class=\"cls_Column\" style=\"width: 10%;\">" + speName + "</td>");
                    }
                    else
                    {
                        sbTotal.Append("<tr><td align=\"center\" rowspan=\"1\" class=\"cls_Column\" style=\"width: 10%;\">" + speName + "</td>");
                    }

                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                    {
                        sbTotal.Append("<td style=\"width:15%;\">方案设计</td>");

                        if (dtProgramData.Rows.Count > 0)
                        {
                            sbTotal.Append("<td style=\"width:15%;\">" + dtProgramData.Rows[0]["AuditAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtProgramData.Rows[0]["SpecialtyHeadAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtProgramData.Rows[0]["ProofreadAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtProgramData.Rows[0]["DesignAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtProgramData.Rows[0]["TotalCount"].ToString() + "</td>");
                        }
                        else
                        {
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        }
                        sbTotal.Append("</tr>");
                    }
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                    {
                        if (CoperationProcess != 3)
                        {
                            sbTotal.Append("<tr>");
                        }
                        sbTotal.Append("<td style=\"width:15%;\">初步设计</td>");
                        if (dtPreliminaryData.Rows.Count > 0)
                        {
                            sbTotal.Append("<td style=\"width:15%;\">" + dtPreliminaryData.Rows[0]["AuditAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtPreliminaryData.Rows[0]["SpecialtyHeadAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\"> " + dtPreliminaryData.Rows[0]["ProofreadAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtPreliminaryData.Rows[0]["DesignAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtPreliminaryData.Rows[0]["TotalCount"].ToString() + "</td>");
                        }
                        else
                        {
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        }
                        sbTotal.Append("</tr>");
                    }
                    if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        if (CoperationProcess != 2)
                        {
                            sbTotal.Append("<tr>");
                        }
                        sbTotal.Append("<td style=\"width:15%;\">施工图设计</td>");
                        if (dtWorkDrawData.Rows.Count > 0)
                        {
                            sbTotal.Append("<td style=\"width:15%;\">" + dtWorkDrawData.Rows[0]["AuditAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtWorkDrawData.Rows[0]["SpecialtyHeadAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtWorkDrawData.Rows[0]["ProofreadAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtWorkDrawData.Rows[0]["DesignAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtWorkDrawData.Rows[0]["TotalCount"].ToString() + "</td>");
                        }
                        else
                        {
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        }
                        sbTotal.Append("</tr>");
                        sbTotal.Append("<tr><td style=\"width:15%;\">后期服务</td>");
                        if (dtLateStageData.Rows.Count > 0)
                        {
                            sbTotal.Append("<td style=\"width:15%;\">" + dtLateStageData.Rows[0]["AuditAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtLateStageData.Rows[0]["SpecialtyHeadAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtLateStageData.Rows[0]["ProofreadAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtLateStageData.Rows[0]["DesignAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtLateStageData.Rows[0]["TotalCount"].ToString() + "</td>");
                        }
                        else
                        {
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        }
                        sbTotal.Append("</tr>");
                    }
                    //室外工程
                    if (CoperationProcess >= 4 && CoperationProcess != 10)
                    {
                        if (CoperationProcess == 4)
                        {
                            sbTotal.Append("<td  style=\"width:15%;\">室外工程</td>");
                        }
                        else if (CoperationProcess == 5)
                        {
                            sbTotal.Append("<td style=\"width:15%;\"> 锅炉房 </td>");
                        }
                        else if (CoperationProcess == 6)
                        {
                            sbTotal.Append("<td style=\"width:15%;\"> 地上单建水泵房 </td>");

                        }
                        else if (CoperationProcess == 7)
                        {
                            sbTotal.Append("<td  style=\"width:15%;\"> 地上单建变配所(室) </td>");

                        }
                        else if (CoperationProcess == 8)
                        {
                            sbTotal.Append("<td  style=\"width:15%;\"> 单建地下室（车库） </td>");
                        }
                        DataTable dtOut = new DataView(dtValue) { RowFilter = "Specialty='" + speName + "'" }.ToTable();
                        if (dtOut.Rows.Count > 0)
                        {
                            sbTotal.Append("<td style=\"width:15%;\">" + dtOut.Rows[0]["AuditAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtOut.Rows[0]["SpecialtyHeadAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtOut.Rows[0]["ProofreadAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtOut.Rows[0]["DesignAmount"].ToString() + "</td>");
                            sbTotal.Append("<td style=\"width:15%;\">" + dtOut.Rows[0]["TotalCount"].ToString() + "</td>");
                        }
                        else
                        {
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                            sbTotal.Append("<td style=\"width:15%;\">0</td>");
                        }
                        sbTotal.Append("</tr>");
                    }

                    sbTotal.Append("</table></div>");
                    //方案
                    StringBuilder sbOne = new StringBuilder();
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                    {
                        sbOne.Append(getMemberValue(dtProgramData, speName, dtmember, 0));
                    }
                    //初步
                    StringBuilder sbTwo = new StringBuilder();
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                    {
                        sbTwo.Append(getMemberValue(dtPreliminaryData, speName, dtmember, 1));
                    }

                    //施工图
                    StringBuilder sbThree = new StringBuilder();
                    StringBuilder sbFour = new StringBuilder();
                    if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        sbThree.Append(getMemberValue(dtWorkDrawData, speName, dtmember, 2));
                        //后期                 
                        sbFour.Append(getMemberValue(dtLateStageData, speName, dtmember, 3));
                    }
                    StringBuilder strTab = new StringBuilder();
                    StringBuilder sbOut = new StringBuilder();
                    //室外工程
                    if (CoperationProcess >= 4 && CoperationProcess != 10)
                    {
                        sbOut.Append(sbTotal.ToString());
                        DataTable dtOutProcess = new DataView(dtValue) { RowFilter = "Specialty='" + speName + "'" }.ToTable();
                        sbOut.Append(getMemberValue(dtOutProcess, speName, dtmember, 4));
                    }
                    else
                    {
                        strTab.Append(sbTotal.ToString());
                    }


                    strTab.Append(" <div id=\"tabsMem\">  <ul style=\"width:98%; margin:auto;\"> ");
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                    {
                        strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabsMem-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
                    }
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                    {
                        strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMem-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");

                    }
                    if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMem-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                        strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMem-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
                    }
                    strTab.Append(" </ul>");
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                    {
                        strTab.Append("<div id=\"tabsMem-1\">");
                        strTab.Append(sbOne.ToString());
                        strTab.Append(" </div>");
                    }
                    if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                    {
                        strTab.Append(" <div id=\"tabsMem-2\">");
                        strTab.Append(sbTwo.ToString());
                        strTab.Append(" </div>");
                    }

                    
                    if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                    {
                        strTab.Append(" <div id=\"tabsMem-3\">");
                        strTab.Append(sbThree.ToString());
                        strTab.Append(" </div>");

                        strTab.Append(" <div id=\"tabsMem-4\">");
                        strTab.Append(sbFour.ToString());
                        strTab.Append(" </div>");
                    }
                    strTab.Append(" </div>");
                    if (CoperationProcess >= 4 && CoperationProcess != 10)
                    {
                        lbl_Member.Text = sbOut.ToString();
                    }
                    else
                    {
                        lbl_Member.Text = strTab.ToString();
                    }
                }
                else
                {
                    string noData = "<table style=\"100%\"><tr><td colspan=\"10\" style=\"color: Red; \">没有人员信息</td></tr></table>";
                    lbl_Member.Text = noData;
                }
            }
        }

        /// <summary>
        /// 查询该专业负责人审核过的用户
        /// </summary>
        private void CreateDataTableIsAuditUser(int sepID)
        {
            //取得该用户信息
            TG.BLL.tg_member bll = new TG.BLL.tg_member();
            TG.Model.tg_member model = new TG.Model.tg_member();
            model = bll.GetModel(UserSysNo);

            DataTable dtData = new TG.BLL.cm_ProjectValueAuditRecord().GetIsAuditedUserAcount(AllotID, model.mem_Speciality_ID).Tables[0];

            lbl_MemberCount.Text = CreateTableByMemberCount(dtData, true);
        }

        /// <summary>
        /// 得到人员产值
        /// </summary>
        /// <param name="dtProcess">阶段金额</param>
        /// <param name="speName">专业</param>
        /// <param name="dtMember">人员</param>
        /// <param name="process">阶段</param>
        /// <returns></returns>
        private string getMemberValue(DataTable dtProcess, string speName, DataTable dtMember, int process)
        {
            dtMember = new DataView(dtMember) { RowFilter = "ItemType=" + process + "" }.ToTable();

            StringBuilder sb = new StringBuilder();
            if (dtProcess.Rows.Count > 0)
            {
                sb.Append(" <div id=\"tbProjectValueBymember\" class=\"cls_Container_Report\" >");
                sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"><tr> ");

                // sb.Append("  <table class=\"cls_Tbhead\" id=\"tbProjectValueBymember\"  width=\"100%\"> <tr> ");
                if (process == 0)
                {
                    sb.Append("<td style=\"text-align: center;\"> 方案设计-策划人员产值分配比例% </td> </tr>");
                }
                else if (process == 1)
                {
                    sb.Append("<td  style=\"text-align: center;\"> 初步设计-策划人员产值分配比例% </td> </tr>");
                }
                else if (process == 2)
                {
                    sb.Append("<td  style=\"text-align: center;\"> 施工图设计-策划人员产值分配比例% </td> </tr>");
                }
                else if (process == 3)
                {
                    sb.Append("<td  style=\"text-align: center;\"> 后期服务-策划人员产值分配比例% </td> </tr>");
                }
                else if (process == 4)
                {
                    sb.Append("<td  style=\"text-align: center;\"> 策划人员产值分配比例% </td> </tr>");
                }
                sb.Append("</table>");
                sb.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                sb.Append("<tr ><td style=\"width: 10%; text-align: center;height:15px;\">  名称</td><td style=\"width: 10%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
                sb.Append(" <tr> <td style=\"width: 10%; text-align: center;\" > 专业 </td><td style=\"width: 10%; text-align: center;\">   姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
                sb.Append(" </table>");

                if (process == 0)
                {
                    sb.Append("<table id=\"gvProjectValueOneBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\"  class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
                }
                else if (process == 1)
                {
                    sb.Append("<table id=\"gvProjectValueTwoBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
                }
                else if (process == 2)
                {
                    sb.Append("<table id=\"gvProjectValueThreeBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
                }
                else if (process == 3)
                {
                    sb.Append("<table id=\"gvProjectValueFourBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
                }
                else
                {
                    sb.Append("<table id=\"gvProjectValueBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
                }

                sb.Append("<tr>");
                sb.Append("<td  width= \"10%\" class=\"cls_Column\" rowspan=\"" + dtMember.Rows.Count + "\">" + speName + "</td>");


                for (int j = 0; j < dtMember.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td class=\"display\"></td>");
                    }
                    string name = dtMember.Rows[j]["mem_Name"].ToString();

                    sb.Append("<td class=\"display\" wp=\"" + dtMember.Rows[j]["IsExternal"].ToString() + "\">" + dtMember.Rows[j]["mem_ID"].ToString() + "</td>");
                    sb.Append("<td width= \"10%\">" + dtMember.Rows[j]["mem_Name"].ToString() + "</td>");


                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" value=" + dtMember.Rows[j]["DesignPercent"].ToString() + "  zy=" + speName + " sz=" + dtMember.Rows[j]["mem_Principalship_ID"].ToString() + " />%</td>");
                    sb.Append("<td width= \"10%\">" + dtMember.Rows[j]["DesignCount"].ToString() + "</td>");

                    sb.Append("<td class=\"display\" >" + dtProcess.Rows[0]["DesignAmount"].ToString() + "</td>");


                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\" value=" + dtMember.Rows[j]["SpecialtyHeadPercent"].ToString() + "  runat=\"server\" />%</td>");
                    sb.Append("<td width= \"10%\">" + dtMember.Rows[j]["SpecialtyHeadCount"].ToString() + "</td>");
                    sb.Append("<td class=\"display\" >" + dtProcess.Rows[0]["SpecialtyHeadAmount"].ToString() + "</td>");

                    //该人员是否是审核
                    if (speName == "建筑" || speName == "结构")
                    {
                        sb.Append("<td width= \"10%\">0% </td>");
                        sb.Append("<td width= \"10%\">0</td>");
                        sb.Append("<td class=\"display\" >0</td>");
                    }
                    else
                    {

                        sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\" value=" + dtMember.Rows[j]["AuditPercent"].ToString() + "  runat=\"server\" />%</td>");
                        sb.Append("<td width= \"10%\">" + dtMember.Rows[j]["AuditCount"].ToString() + "</td>");
                        sb.Append("<td class=\"display\" >" + dtProcess.Rows[0]["AuditAmount"].ToString() + "</td>");
                    }

                    sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\" value=" + dtMember.Rows[j]["ProofreadPercent"].ToString() + "   runat=\"server\" />%</td>");
                    sb.Append("<td width= \"10%\">" + dtMember.Rows[j]["ProofreadCount"].ToString() + "</td>");
                    sb.Append("<td class=\"display\" >" + dtProcess.Rows[0]["ProofreadAmount"].ToString() + "</td>");
                    sb.Append("<td class=\"display\" >" + process + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
                sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\" id=\"tbChooseUser\"><tr> ");
                if (process == 0)
                {
                    sb.Append("<td><span id=\"chooseOneUser\" style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseExternalOneUser\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                }
                else if (process == 1)
                {
                    sb.Append("<td><span id=\"chooseTwoUser\" style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseTwoExternalUser\"   style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                }
                else if (process == 2)
                {
                    sb.Append("<td><span id=\"chooseThreeUser\"  style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseThreeExternalUser\"   style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                }
                else if (process == 3)
                {
                    sb.Append("<td><span id=\"chooseFourUser\"  style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseFourExternalUser\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                }
                else
                {
                    sb.Append("<td><span id=\"chooseUser\"  style=\"color: Blue; cursor: pointer;\">添加人员</span> &nbsp;&nbsp;<span id=\"chooseExternalUser\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                }
                sb.Append("</tr></table>");
                sb.Append(" </div>");
            }
            return sb.ToString();
        }


        /// <summary>
        /// 生成分配之后的产值人员
        /// </summary>
        /// <param name="dtData"></param>
        /// <returns></returns>
        private string CreateTableByMemberCount(DataTable dtData, bool isAudited)
        {

            //方案工序
            string program = "";
            if (CoperationProcess == 0 || CoperationProcess == 1 && CoperationProcess == 10)
            {
                DataTable dtProgram = new DataView(dtData) { RowFilter = "ItemType=0" }.ToTable();
                program = createMemberCountDetail(dtProgram, 0, isAudited);
            }
            //初步工序
            string preliminary = "";
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                DataTable dtPreliminary = new DataView(dtData) { RowFilter = "ItemType=1" }.ToTable();
                preliminary = createMemberCountDetail(dtPreliminary, 1, isAudited);
            }
            //施工图
            string workDraw = "";
            string lateStage = "";
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 && CoperationProcess == 10)
            {
                DataTable dtWorkDraw = new DataView(dtData) { RowFilter = "ItemType=2" }.ToTable();
                workDraw = createMemberCountDetail(dtWorkDraw, 2, isAudited);

                //后期服务
                DataTable dtLateStage = new DataView(dtData) { RowFilter = "ItemType=3" }.ToTable();
                lateStage = createMemberCountDetail(dtLateStage, 3, isAudited);
            }

            if (CoperationProcess >= 4 && CoperationProcess != 10)
            {
                return createMemberCountDetail(dtData, CoperationProcess, isAudited);
            }
            else
            {
                StringBuilder strTab = new StringBuilder();
                strTab.Append(" <div id=\"tabsMemAmount\">  <ul style=\"width:98%; margin:auto;\"> ");
                if (CoperationProcess == 0 || CoperationProcess == 1 && CoperationProcess == 10)
                {
                    strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabsMemAmount-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                {
                    strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");

                }
                if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 && CoperationProcess == 10)
                {
                    strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                    strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");

                }
                strTab.Append(" </ul>");
                if (CoperationProcess == 0 || CoperationProcess == 1 && CoperationProcess == 10)
                {
                    strTab.Append("<div id=\"tabsMemAmount-1\">");
                    strTab.Append(program);
                    strTab.Append(" </div>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                {
                    strTab.Append(" <div id=\"tabsMemAmount-2\">");
                    strTab.Append(preliminary);
                    strTab.Append(" </div>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 && CoperationProcess == 10)
                {
                    strTab.Append(" <div id=\"tabsMemAmount-3\">");
                    strTab.Append(workDraw);
                    strTab.Append(" </div>");
                    strTab.Append(" <div id=\"tabsMemAmount-4\">");
                    strTab.Append(lateStage);
                    strTab.Append(" </div>");
                }
                strTab.Append(" </div>");

                return strTab.ToString();
            }
        }

        /// <summary>
        /// 生成人员产值明细
        /// </summary>
        /// <param name="dtMemberCount"></param>
        /// <param name="process"></param>
        /// <returns></returns>
        private string createMemberCountDetail(DataTable dtData, int process, bool isAudited)
        {
            //专业 表头
            StringBuilder sbHead = new StringBuilder();

            if (isAudited)
            {
                sbHead.Append(" <div id=\"tbIsAuditedBymemberAmount\" class=\"cls_Container_Report\" >");

            }
            else
            {
                sbHead.Append(" <div id=\"tbProjectValueBymemberAmount\" class=\"cls_Container_Report\" >");
            }
            if (process == 0)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  方案设计-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            else if (process == 1)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  初步设计-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 2)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  施工图设计-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 3)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  后期服务-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 4)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  室外工程-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 5)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  锅炉房-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            else if (process == 6)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建水泵房-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            else if (process == 7)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建变配所(室)-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 8)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  单建地下室（车库）-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbHead.Append("<tr ><td style=\"width: 10%; text-align: center;\">  名称</td><td style=\"width: 10%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sbHead.Append(" <tr > <td style=\"width: 10%; text-align: center;\" > 专业 </td><td style=\"width: 10%; text-align: center;\">   姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
            sbHead.Append(" </table>");

            sbHead.Append("<table id=\"gvProjectValueBymemberAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

            StringBuilder sbHtml = new StringBuilder();

            //取得专业
            var dtSpecialtyList = new DataView(dtData).ToTable("Specialty", true, "spe_Name");

            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                var dv = new DataView(dtData) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();

                sbHtml.Append("<tr>");
                sbHtml.Append("<td  width= \"10%\" class=\"cls_Column\" rowspan=\"" + dtMemberBySpecialty.Rows.Count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sbHtml.Append("<tr>");
                    }

                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["DesignPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["DesignCount"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["SpecialtyHeadPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["SpecialtyHeadCount"].ToString() + "</td>");

                    sbHtml.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["AuditPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["AuditCount"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["ProofreadPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["ProofreadCount"].ToString() + "</td>");
                    sbHtml.Append("</tr>");
                }


            }
            sbHead.Append(sbHtml.ToString());
            sbHead.Append(" </table></div>");

            return sbHead.ToString();
        }

    }
}