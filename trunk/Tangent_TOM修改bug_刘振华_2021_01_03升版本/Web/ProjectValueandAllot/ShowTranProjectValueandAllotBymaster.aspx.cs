﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ShowTranProjectValueandAllotBymaster : PageBase
    {
        /// <summary>
        /// 项目系统编号
        /// </summary>
        public int proSysNo
        {
            get
            {
                int proSysNo = 0;
                int.TryParse(Request["proid"], out proSysNo);
                return proSysNo;
            }
        }

        public decimal TotalCount
        {
            get
            {
                decimal totalCount = 0;
                decimal.TryParse(Request["totalCount"], out totalCount);
                return totalCount;
            }
        }

        /// <summary>
        /// 审核记录系统编号
        /// </summary>
        public int ValueAllotAuditSysNo
        {
            get
            {
                int valueAllotAuditSysNo = 0;
                int.TryParse(Request["auditSysID"], out valueAllotAuditSysNo);
                return valueAllotAuditSysNo;
            }
        }

        public string Type
        {
            get
            {
                return Request["type"];
            }
        }

        public TG.Model.cm_TranjjsProjectValueAllot ProjectValueAllot { get; set; }


        /// <summary>
        /// 审核记录实体
        /// </summary>
        public TG.Model.cm_TranProjectValueAuditRecord projectAuditRecordEntity { get; set; }

        public int allotID
        {
            get
            {
                int allotID = 0;
                int.TryParse(Request["allotID"], out allotID);
                return allotID;
            }
        }

        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }

        public string AuditUser { get; set; }

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //获取项目信息
                getProInfo();

                getMemberValue();
                GetAuditRecord();
            }
        }
        /// <summary>
        /// 项目信息
        /// </summary>
        private void getProInfo()
        {


            lblTitle.Text = "[土建转暖通产值分配查看]";
            lblTranTitle.Text = "转暖通所金额";

            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(proSysNo);

            if (pro_model != null)
            {
                //项目名称
                lblProjectName.Text = pro_model.pro_name;
                //关联合同
                this.lblReletive.Text = pro_model.Project_reletive;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }

                //建设规模
                this.lblScale.Text = pro_model.ProjectScale.ToString();
                //建筑类别
                lblBuildType.Text = pro_model.BuildType;
                //项目总负责
                this.lblPMName.Text = pro_model.PMName;
                //承接部门
                lblcpr_Unit.Text = pro_model.Unit;

                //项目阶段

                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }

                //合同额

                lblCoperationAmount.Text = pro_model.Cpr_Acount.ToString();

                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }
                TG.BLL.cm_TranProjectValueAllot BLL = new TG.BLL.cm_TranProjectValueAllot();
                TG.Model.cm_TranProjectValueAllot modelTran = BLL.GetModelByProID(proSysNo, allotID);

                if (modelTran != null)
                {
                    lblTotalCount.Text = modelTran.TotalCount.ToString();
                    decimal TranCount = 0;
                    decimal AllotCount = 0;
                    if (!string.IsNullOrEmpty(modelTran.TranCount.ToString()))
                    {
                        TranCount = decimal.Parse(modelTran.TranCount.ToString());
                    }
                    if (!string.IsNullOrEmpty(modelTran.AllotCount.ToString()))
                    {
                        AllotCount = decimal.Parse(modelTran.AllotCount.ToString());
                    }

                    txt_TranValueCount.Text = TranCount.ToString();

                    txtAllotValueCount.Text = AllotCount.ToString();
                    txtTheDeptValuePercent.Text = modelTran.Thedeptallotpercent.ToString();
                    txtTheDeptValueCount.Text = modelTran.Thedeptallotcount.ToString();
                    txt_TranPercent.Text = modelTran.Tranpercent.ToString();
                    txtAllotValuePercent.Text = modelTran.AllotPercent.ToString();
                    lblYear.Text = modelTran.ActualAllountTime;
                }
                else
                {
                    lblNotData.Text = "<table id=\"tbdelete\" align=\"center\"><tr><td style=\" font-size: 12px;font-weight: bold; color: Red;\"  >此信息已被删除!</td></tr></table>";

                }

            }

        }

        /// <summary>
        /// 得到人员产值
        /// </summary>
        /// <returns></returns>
        private void getMemberValue()
        {
            TG.BLL.cm_TranProjectValueAllot BLL = new TG.BLL.cm_TranProjectValueAllot();
            DataTable dtMember = new DataTable();

            dtMember = BLL.GetTranProAuditedUser(proSysNo, "nts", allotID).Tables[0];

            StringBuilder sb = new StringBuilder();
            sb.Append(" <div id=\"tbProjectValueBymember\" class=\"cls_Container_Report\" >");
            sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"><tr> ");

            sb.Append("<td style=\"text-align: center;\"> 暖通策划人员产值分配比例% </td> </tr>");

            sb.Append("</table>");
            sb.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sb.Append("<tr ><td style=\"width: 10%; text-align: center;height:15px;\">  名称</td><td style=\"width: 10%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sb.Append(" <tr> <td style=\"width: 10%; text-align: center;\" > 专业 </td><td style=\"width: 10%; text-align: center;\">   姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
            sb.Append(" </table>");
            sb.Append("<table id=\"gvProjectValueBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");

            //取得专业
            var dtSpecialtyList = new DataView(dtMember).ToTable("Specialty", true, "spe_Name");

            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                var dv = new DataView(dtMember) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();

                sb.Append("<tr>");
                sb.Append("<td  width= \"10%\" class=\"cls_Column\" rowspan=\"" + dtMemberBySpecialty.Rows.Count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sb.Append("<tr>");
                    }

                    sb.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                    sb.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["DesignPercent"].ToString() + "%</td>");
                    sb.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["DesignCount"].ToString() + "</td>");
                    sb.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["SpecialtyHeadPercent"].ToString() + "%</td>");
                    sb.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["SpecialtyHeadCount"].ToString() + "</td>");

                    sb.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["AuditPercent"].ToString() + "%</td>");
                    sb.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["AuditCount"].ToString() + "</td>");
                    sb.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["ProofreadPercent"].ToString() + "%</td>");
                    sb.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["ProofreadCount"].ToString() + "</td>");
                    sb.Append("</tr>");
                }

            }
            sb.Append(" </table></div>");

            lblMember.Text = sb.ToString();
        }

        private void GetAuditRecord()
        {
            TG.BLL.cm_TranProjectValueAuditRecord BLL = new TG.BLL.cm_TranProjectValueAuditRecord();

            projectAuditRecordEntity = BLL.GetModel(ValueAllotAuditSysNo);
            if (projectAuditRecordEntity != null)
            {
                TG.Model.tg_member user = new TG.Model.tg_member();
                //查询联系人姓名
                if (!string.IsNullOrEmpty(projectAuditRecordEntity.AuditUser))
                {
                    user = new TG.BLL.tg_member().GetModel(int.Parse(projectAuditRecordEntity.AuditUser));
                    AuditUser = user == null ? "" : user.mem_Name;
                }
            }
        }
    }
}