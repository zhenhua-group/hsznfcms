﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using NPOI.SS.Util;
using System.Data;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Collections;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjectWageBenefitsSearchBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定部门
                BindUnit();
                //绑定年份
                BindYear();
                //默认选中年份
                SelectCurrentYear();
                //绑定权限
                BindPreviewPower();
                BindPower();
            }
        }

        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //判断权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
            string unitid = base.UserUnitNo.ToString();
            if (this.drp_unit.Items.FindByValue(unitid) != null)
            {
                this.drp_unit.Items.FindByValue(unitid).Selected = true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND me.mem_ID=" + UserSysNo + "");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND me.mem_ID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Project().GetProjectYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();

            if (this.drp_unit.Items.FindByValue(UserUnitNo.ToString()) != null)
            {
                this.drp_unit.Items.FindByValue(UserUnitNo.ToString()).Selected = true;
            }
        }

        //根据条件查询
        public void BindPower()
        {
            StringBuilder sb = new StringBuilder();
            //权限
            GetPreviewPowerSql(ref sb);

            this.hid_where.Value = sb.ToString();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Output_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();

            string modelPath = " ~/TemplateXls/ProjectWageBenefits.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            NPOI.SS.UserModel.IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);


            //设置样式
            ICellStyle styleTotal = wb.CreateCellStyle();
            //设置字体
            IFont fontTotal = wb.CreateFont();
            fontTotal.FontHeightInPoints = 9;//字号
            fontTotal.FontName = "宋体";
            //字体加粗
            fontTotal.Boldweight = (short)2000;
            styleTotal.SetFont(fontTotal);

            //设置边框
            styleTotal.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            styleTotal.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            styleTotal.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            styleTotal.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            string title = getTitle() + "效益工资发放表 ";
            ws.GetRow(0).GetCell(0).SetCellValue(title);
            CellRangeAddress range = new CellRangeAddress(0, 0, 0, 9);
            ws.AddMergedRegion(range);
            ((HSSFSheet)ws).SetEnclosedBorderOfRegion(range, NPOI.SS.UserModel.BorderStyle.THIN, NPOI.HSSF.Util.HSSFColor.BLACK.index);

            int row = 3;
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);

                cell.CellStyle = style2;
                cell.SetCellValue((i + 1));

                cell = dataRow.CreateCell(1);
                cell.SetCellValue(dt.Rows[i]["mem_Name"].ToString());
                cell.CellStyle = style2;

                cell = dataRow.CreateCell(2);
                decimal memTotalCount = dt.Rows[i]["memTotalCount"].ToString() == "" ? 0 : decimal.Parse(dt.Rows[i]["memTotalCount"].ToString());
                WriteExcelValue(cell, memTotalCount);
                cell.CellStyle = style2;

                cell = dataRow.CreateCell(3);
                decimal valueWage = dt.Rows[i]["ValueWage"].ToString() == "" ? 0 : decimal.Parse(dt.Rows[i]["ValueWage"].ToString());
                WriteExcelValue(cell, valueWage);
                cell.CellStyle = style2;

                cell = dataRow.CreateCell(4);
                decimal possessionMemWange = dt.Rows[i]["PossessionMemWange"].ToString() == "" ? 0 : decimal.Parse(dt.Rows[i]["PossessionMemWange"].ToString());
                WriteExcelValue(cell, possessionMemWange);
                cell.CellStyle = style2;

                cell = dataRow.CreateCell(5);
                cell.SetCellFormula("D" + (row + 1) + "+E" + (row + 1) + "");
                cell.CellStyle = style2;

                cell = dataRow.CreateCell(6);
                decimal remunerationPercent = dt.Rows[i]["RemunerationPercent"].ToString() == "" ? 0 : decimal.Parse(dt.Rows[i]["RemunerationPercent"].ToString());
                cell.SetCellFormula("F" + (row + 1) + "*" + remunerationPercent + "");
                cell.CellStyle = style2;

                cell = dataRow.CreateCell(7);
                decimal taxPercent = dt.Rows[i]["TaxPercent"].ToString() == "" ? 0 : decimal.Parse(dt.Rows[i]["TaxPercent"].ToString());
                WriteExcelValue(cell, taxPercent);
                cell.CellStyle = style2;

                cell = dataRow.CreateCell(8);
                cell.SetCellFormula("G" + (row + 1) + "*H" + (row + 1) + "/100");
                cell.CellStyle = style2;

                cell = dataRow.CreateCell(9);
                cell.SetCellFormula("G" + (row + 1) + "-I" + (row + 1) + "");
                cell.CellStyle = style2;

                row = row + 1;

            }

            var dataTotal = ws.GetRow(row);//生成行
            if (dataTotal == null)
                dataTotal = ws.CreateRow(row);
            var total = dataTotal.GetCell(0);
            if (total == null)
                total = dataTotal.CreateCell(0);
            total.SetCellValue("");
            total.CellStyle = styleTotal;

            total = dataTotal.GetCell(1);
            if (total == null)
                total = dataTotal.CreateCell(1);
            total.SetCellValue("合计");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(2);
            total.SetCellFormula("SUM(C4:C" + (row) + ")");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(3);
            total.SetCellFormula("SUM(D4:D" + (row) + ")");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(4);
            total.SetCellFormula("SUM(E4:E" + (row) + ")");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(5);
            total.SetCellFormula("SUM(F4:F" + (row) + ")");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(6);
            total.SetCellFormula("SUM(G4:G" + (row) + ")");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(7);
            total.SetCellValue("");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(8);
            total.SetCellFormula("SUM(I4:I" + (row) + ")");
            total.CellStyle = styleTotal;

            total = dataTotal.CreateCell(9);
            total.SetCellFormula("SUM(J4:J" + (row) + ")");
            total.CellStyle = styleTotal;

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(title + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }

        }

        private string getTitle()
        {
            string title = "";
            if (drp_unit.SelectedValue != "-1")
            {
                title = title + this.drp_unit.SelectedItem.Text.Trim();
            }
            else
            {
                title = title + "全部生产部门";
            }
            if (drp_year.SelectedValue != "-1")
            {
                title = title += drp_year.SelectedItem.Text.Trim() + "年";
            }
            else
            {
                title = title + "全部年份";
            }
            return title;
        }

        /// <summary>
        /// 得到数据
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {
            StringBuilder sb = new StringBuilder("");
            StringBuilder sb1 = new StringBuilder("");
            StringBuilder sb2 = new StringBuilder("");
            StringBuilder sb3 = new StringBuilder("");


            //绑定单位
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND me.mem_Unit_ID=" + this.drp_unit.SelectedValue.Trim() + "");
                //sb1.Append(" and cs.UnitId=" + this.drp_unit.SelectedValue.Trim() + "");
            }

            //按照年份
            string year = null;
            if (this.drp_year.SelectedIndex != 0)
            {
                year = this.drp_year.SelectedValue;

                sb1.AppendFormat(" AND cs.ValueYear='{0}'", year);
                sb2.AppendFormat(" AND b.ActualAllountTime='{0}'", year);
                sb3.AppendFormat(" AND yearWang='{0}'", year);
            }
            string name = txt_memberName.Value;
            if (!string.IsNullOrEmpty(name))
            {
                sb.Append(" AND me.mem_name like '%" + name.Trim() + "%'");
            }
            //检查权限
            GetPreviewPowerSql(ref sb);


            TG.BLL.cm_ProjectWageBenefits bll = new TG.BLL.cm_ProjectWageBenefits();
            DataTable dt = bll.GetProjectWageBenefitsList(sb.ToString(), sb1.ToString(), sb2.ToString(), sb3.ToString()).Tables[0];
            return dt;

        }

        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}