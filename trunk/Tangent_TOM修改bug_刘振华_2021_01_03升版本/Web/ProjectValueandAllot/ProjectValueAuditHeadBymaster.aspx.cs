﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjectValueAuditHeadBymaster : PageBase
    {
        #region 参数

        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }

        //项目ID
        public int Pro_ID
        {
            get
            {
                int proID = 0;
                int.TryParse(Request["proID"], out proID);
                return proID;
            }
        }

        //分配ID
        public int AllotID
        {
            get
            {
                int allotID = 0;
                int.TryParse(Request["allotID"], out allotID);
                return allotID;
            }
        }
        /// <summary>
        /// 审核记录系统编号
        /// </summary>
        public int ValueAllotAuditSysNo
        {
            get
            {
                int valueAllotAuditSysNo = 0;
                int.TryParse(Request["ValueAllotAuditSysNo"], out valueAllotAuditSysNo);
                return valueAllotAuditSysNo;
            }
        }
        /// <summary>
        /// 发送人
        /// </summary>
        public int InUserId
        {
            get
            {
                int userId = 0;
                int.TryParse(Request["inUserId"], out userId);
                return userId;
            }
        }
        public string IsDone { get; set; }

        /// <summary>
        /// 是否四舍五入
        /// </summary>
        public string IsRounding = ConfigurationManager.AppSettings["IsRounding"] ?? "0";

        public int CoperationProcess { get; set; }

        public string SpeName { get; set; }

        public string Itemtype { get; set; }

        public string ShouldBeValueCount { get; set; }
        /// <summary>
        /// 产值类型
        /// </summary>
        public string proType
        {
            get
            {
                return Request["proType"];
            }
        }
        #endregion

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getProInfo();

                getMemberStatusInfo();

                bindMemberInfo();
            }
        }
        /// <summary>
        /// 项目信息
        /// </summary>
        private void getProInfo()
        {
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(Pro_ID);

            if (pro_model != null)
            {
                //项目名称
                lblProjectName.Text = pro_model.pro_name;
                //关联合同
                this.lblReletive.Text = pro_model.Project_reletive;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }


                //建设规模
                this.lblScale.Text = pro_model.ProjectScale.ToString();
                //建筑类别
                lblBuildType.Text = pro_model.BuildType;
                //项目总负责
                this.lblPMName.Text = pro_model.PMName;
                //承接部门
                lblcpr_Unit.Text = pro_model.Unit;

                //项目阶段

                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }

                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }
            }

        }

        /// <summary>
        /// 得到人员审批状态信息
        /// </summary>
        private void getMemberStatusInfo()
        {
            TG.Model.cm_ProjectValueByMemberAuditStatus model = new TG.Model.cm_ProjectValueByMemberAuditStatus();
            model = new TG.BLL.cm_ProjectValueByMemberAuditStatus().GetModel(Pro_ID, AllotID, InUserId);
            if (model != null)
            {
                AuditSuggsion.InnerText = model.AuditSuggsion;
            }
        }


        private void bindMemberInfo()
        {
            //判断该审批是否完成
            IsDone = new TG.BLL.cm_SysMsg().IsDone(MessageID);

            if (IsDone.Trim() != "D")
            {
                getMemberEdit();
            }
            else
            {
                getProcessDetailShow();
            }
        }
        /// <summary>
        /// 得到人员信息
        /// </summary>
        private void getMemberEdit()
        {
            TG.BLL.cm_ProjectValueAllot bll_Allot = new BLL.cm_ProjectValueAllot();

            TG.Model.cm_ProjectValueAllot projectValueAllot = bll_Allot.GetModel(AllotID);

            if (projectValueAllot != null)
            {
                Itemtype = projectValueAllot.Itemtype.ToString();

                ShouldBeValueCount = projectValueAllot.ShouldBeValueCount.ToString();
                //绑定人员信息
                string itemStatus = projectValueAllot.Itemtype.ToString();

                TG.BLL.cm_jjsProjectValueAllotConfig bll = new TG.BLL.cm_jjsProjectValueAllotConfig();

                TG.Model.cm_jjsProjectValueAllotDetail model = bll.GetDetailModel(Pro_ID, AllotID);

                StringBuilder sbHead = new StringBuilder();
                sbHead.Append("<fieldset id=\"memField\"><legend style=\"font-size:12px;\">经济所人员产值填写</legend>");
                sbHead.Append(" <div id=\"tbProjectValueProcess\" class=\"cls_Container_Report\" >");
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  " + getTypeDetail(itemStatus) + "-配置比例% </td> </tr> </table>");
                sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                sbHead.Append(" <tr><td rowspan=\"3\" width=\"18%\">项目分类 </td> <td colspan=\"8\" width=\"72%\">编制 </td> <td width=\"10%\" rowspan=\"3\"> 校对  </td> </tr>");
                sbHead.Append("  <tr>  <td rowspan=\"2\" width=\"9%\">编制 <br/>合计 </td> <td colspan=\"3\" width=\"27%\"> 建筑</td> <td colspan=\"4\" width=\"36%\">  安装 </td> </tr>");
                sbHead.Append("  <tr> <td width=\"9%\"> 建筑<br />  合计  </td><td width=\"9%\">土建 </td> <td width=\"9%\">  结构</td>");
                sbHead.Append("<td width=\"9%\">  安装<br /> 合计 </td><td width=\"9%\"> 给排水 </td> <td width=\"9%\">  采暖</td> <td width=\"9%\"> 电气 </td> </tr>");
                sbHead.Append("<table id=\"gvProjectValueProcess\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label2\" >");

                decimal totalAllotValue = decimal.Parse(projectValueAllot.ShouldBeValueCount.ToString());

                #region 状态判断
                if (itemStatus == "11" || itemStatus == "12" || itemStatus == "18")
                {
                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（%）</td>");
                    sbHead.Append("<td width=\"9%\">" + (100 - decimal.Parse(model.ProofreadPercent.ToString())).ToString("f2") + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.Totalbuildingpercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.BuildingPercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.StructurePercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.TotalInstallationpercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.DrainPercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.HavcPercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.ElectricPercent + "%</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadPercent + "%</td>");
                    sbHead.Append("</tr>");

                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（元）</td>");
                    sbHead.Append("<td width=\"9%\">" + (decimal.Parse(model.TotalInstallationcount.ToString()) + decimal.Parse(model.Totalbuildingcount.ToString())).ToString("f2") + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.Totalbuildingcount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.BuildingCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.StructureCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.TotalInstallationcount + "</td>");

                    sbHead.Append("<td width=\"9%\">" + model.DrainCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.HavcCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.ElectricCount + "</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadCount + "</td>");
                    sbHead.Append("</tr>");
                    sbHead.Append("</table>");
                    sbHead.Append(getMemberDetailEdit(itemStatus));
                    sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\" id=\"tbChooseUser\"><tr> ");
                    sbHead.Append("<td><span id=\"chooseUser\"   data-toggle=\"modal\" href=\"#chooseUserMainDiv\" style=\"color: Blue; cursor: pointer;\">添加人员</span>&nbsp;&nbsp;<span id=\"chooseExternalUser\"  data-toggle=\"modal\" href=\"#chooseExtUserDiv\"  style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                    sbHead.Append("</tr></table>");
                    sbHead.Append("</div>");
                    sbHead.Append("</fieldset>");

                    lbl_Member.Text = sbHead.ToString();

                }
                else if (itemStatus == "13" || itemStatus == "14" || itemStatus == "15" || itemStatus == "16" || itemStatus == "17" || itemStatus == "19" || itemStatus == "20" || itemStatus == "21" || itemStatus == "22")
                {

                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（%）</td>");
                    sbHead.Append("<td width=\"9%\">" + (100 - decimal.Parse(model.ProofreadPercent.ToString())).ToString("f2") + "%</td>");
                    sbHead.Append("<td width=\"27%\" colsapn=\"3\">" + model.Totalbuildingpercent + "%</td>");
                    sbHead.Append("<td width=\"36%\" colsapn=\"4\">" + model.TotalInstallationpercent + "%</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadPercent + "%</td>");
                    sbHead.Append("</tr>");
                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（元）</td>");
                    sbHead.Append("<td width=\"9%\">" + (decimal.Parse(model.TotalInstallationcount.ToString()) + decimal.Parse(model.Totalbuildingcount.ToString())).ToString("f2") + "</td>");
                    sbHead.Append("<td width=\"27%\" colsapn=\"3\">" + model.Totalbuildingcount + "</td>");
                    sbHead.Append("<td width=\"36%\" colsapn=\"4\">" + model.TotalInstallationcount + "</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadCount + "</td>");
                    sbHead.Append("</tr>");
                    sbHead.Append("</table>");
                    sbHead.Append(getMemberDetailEdit(itemStatus));
                    sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\" id=\"tbChooseUser\"><tr> ");
                    sbHead.Append("<td><span id=\"chooseUser\"data-toggle=\"modal\" href=\"#chooseUserMainDiv\" style=\"color: Blue; cursor: pointer;\">添加人员</span>&nbsp;&nbsp;<span id=\"chooseExternalUser\"    data-toggle=\"modal\" href=\"#chooseExtUserDiv\" style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
                    sbHead.Append("</tr></table>");
                    sbHead.Append("</div>");
                    sbHead.Append("</fieldset>");


                }
                else
                {

                    decimal proofreadPercent = model == null ? 0 : decimal.Parse(model.ProofreadPercent.ToString());
                    decimal bianzhiPercent = 100 - proofreadPercent;
                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（%）</td>");
                    sbHead.Append("<td width=\"9%\">" + (100 - decimal.Parse(model.ProofreadPercent.ToString())).ToString("f2") + "%</td>");

                    sbHead.Append("<td width=\"9%\">" + model.Totalbuildingpercent + "%</td>");
                    sbHead.Append("<td width=\"9%\"><input  maxlength=\"15\" type=\"text\" id=\"txtBulidging\" runat=\"server\" value=" + model.BuildingPercent + "  >%</td>");
                    sbHead.Append("<td width=\"9%\"><input  maxlength=\"15\" type=\"text\"  id=\"txtStructure\"  runat=\"server\" value=" + model.StructurePercent + "   >%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.TotalInstallationpercent + "%</td>");
                    sbHead.Append("<td width=\"9%\"><input  maxlength=\"15\" type=\"text\" id=\"txtDrain\" runat=\"server\" value=" + model.DrainPercent + " >%</td>");
                    sbHead.Append("<td width=\"9%\"><input  maxlength=\"15\" type=\"text\"  id=\"txtHavc\"  runat=\"server\" value=" + model.HavcPercent + " >%</td>");
                    sbHead.Append("<td width=\"9%\"><input  maxlength=\"15\" type=\"text\" id=\"txtElectric\" runat=\"server\" value=" + model.ElectricPercent + " >%</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadPercent + "%</td>");
                    sbHead.Append("</tr>");

                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（元）</td>");
                    sbHead.Append("<td width=\"9%\">" + (decimal.Parse(model.TotalInstallationcount.ToString()) + decimal.Parse(model.Totalbuildingcount.ToString())).ToString("f2") + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.Totalbuildingcount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.BuildingCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.StructureCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.TotalInstallationcount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.DrainCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.HavcCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.ElectricCount + "</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadCount + "</td>");
                    sbHead.Append("</tr>");
                    sbHead.Append("</table>");
                    sbHead.Append(getMemberDetailEdit(itemStatus));
                    sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\" id=\"tbChooseUser\"><tr> ");
                    sbHead.Append("<td><span id=\"chooseUser\" data-toggle=\"modal\" href=\"#chooseUserMainDiv\"style=\"color: Blue; cursor: pointer;\">添加人员</span>&nbsp;&nbsp;<span id=\"chooseExternalUser\" style=\"color: Blue; cursor: pointer;\"   data-toggle=\"modal\" href=\"#chooseExtUserDiv\" >添加外聘人员</span></td>");
                    sbHead.Append("</tr></table>");
                    sbHead.Append("</div>");
                    sbHead.Append("</fieldset>");

                }
                #endregion
                lbl_Member.Text = sbHead.ToString();

            }
        }
        /// <summary>
        /// 得到人员明细信息--编辑
        /// </summary>
        /// <param name="dtMember"></param>
        /// <returns></returns>
        private string getMemberDetailEdit(string itemStatus)
        {
            //取得人员信息
            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
            DataTable dtMember = BLL.GetjjsProjectValueByMemberAcount(Pro_ID, AllotID).Tables[0];

            StringBuilder sbHtml = new StringBuilder();

            sbHtml.Append(" <div id=\"tbProjectValueBymemberAmount\" class=\"cls_Container_Report\" >");
            sbHtml.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  " + getTypeDetail(itemStatus) + "-项目策划人员产值分配比例% </td> </tr> </table>");
            sbHtml.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbHtml.Append("<tr ><td style=\"width: 15%; text-align: center;\">  名称</td><td style=\"width: 15%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 35%; text-align: center;\"  > 编制 </td> <td style=\"width: 35%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sbHtml.Append(" <tr > <td style=\"width: 15%; text-align: center;\" > 专业 </td><td style=\"width: 15%; text-align: center;\">   姓名 </td> <td style=\"width: 17.5%; text-align: center;\"> 比例 </td> <td style=\"width: 17.5%; text-align: center;\"> 金额(元) </td> <td style=\"width: 17.5%; text-align: center;\"> 比例 </td> <td style=\"width: 17.5%; text-align: center;\">金额(元) </td>  </tr>");
            sbHtml.Append(" </table>");
            sbHtml.Append("<table id=\"gvProjectValueBymember\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_Input cls_ProjAllot_label2\" >");

            //取得参加人员专业 --处理校对
            //取得专业
            var dtSpecialtyList = new DataView(dtMember).ToTable("Specialty", true, "spe_Name");

            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                var dv = new DataView(dtMember) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();

                sbHtml.Append("<tr>");
                sbHtml.Append("<td  width= \"15%\" class=\"cls_Column\" rowspan=\"" + dtMemberBySpecialty.Rows.Count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sbHtml.Append("<tr>");
                        sbHtml.Append("<td class=\"display\"></td>");
                    }
                    sbHtml.Append("<td class=\"display\" wp=\"" + dtMemberBySpecialty.Rows[j]["IsExternal"].ToString() + "\">" + dtMemberBySpecialty.Rows[j]["mem_ID"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"15%\" >" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");

                    //该人员是否是建筑 结构给排水暖通

                    sbHtml.Append("<td width= \"17.5%\"> <input  maxlength=\"15\" type=\"text\"  value=" + dtMemberBySpecialty.Rows[j]["DesignPercent"].ToString() + "  roles=\"design\" runat=\"server\"  spe=" + dtSpecialtyList.Rows[i]["spe_Name"] + ">%</td>");
                    sbHtml.Append("<td width= \"17.5%\" >" + dtMemberBySpecialty.Rows[j]["DesignCount"].ToString() + "</td>");

                    sbHtml.Append("<td width= \"17.5%\" > <input  maxlength=\"15\" type=\"text\"  roles=\"jd\" runat=\"server\" value=" + dtMemberBySpecialty.Rows[j]["ProofreadPercent"].ToString() + " >%</td>");
                    sbHtml.Append("<td width= \"17.5%\" >" + dtMemberBySpecialty.Rows[j]["ProofreadCount"].ToString() + "</td>");


                    sbHtml.Append("</tr>");
                }

            }
            sbHtml.Append(" </table></div>");

            return sbHtml.ToString();
        }

        /// <summary>
        /// 得到信息
        /// </summary>
        /// <param name="itemType"></param>
        /// <returns></returns>
        private string getTypeDetail(string itemType)
        {
            string typeDetail = "";
            if (itemType == "11")
            {
                typeDetail = "概算（室外不单算）";
            }
            else if (itemType == "12")
            {
                typeDetail = "概算（室外单算）";
            }
            else if (itemType == "13")
            {
                typeDetail = "概算 (锅炉房)";
            }
            else if (itemType == "14")
            {
                typeDetail = "概算(变电所)";
            }
            else if (itemType == "15")
            {
                typeDetail = "概算(水泵房)";
            }
            else if (itemType == "16")
            {
                typeDetail = "概算(空调机房)";
            }
            else if (itemType == "17")
            {
                typeDetail = "概算（单独室外工程）";
            }
            else if (itemType == "18")
            {
                typeDetail = "预算";
            }
            else if (itemType == "19")
            {
                typeDetail = "预算 (锅炉房)";
            }
            else if (itemType == "20")
            {
                typeDetail = "预算(变电所)";
            }
            else if (itemType == "21")
            {
                typeDetail = "预算(水泵房)";
            }
            else if (itemType == "22")
            {
                typeDetail = "预算(空调机房)";
            }
            else if (itemType == "23")
            {
                typeDetail = "结算审核";
            }
            else if (itemType == "24")
            {
                typeDetail = "调整概算、造价鉴定";
            }
            else if (itemType == "25")
            {
                typeDetail = "建议书、可研";
            }
            return typeDetail;
        }


        /// <summary>
        /// 得到人员信息
        /// </summary>
        private void getProcessDetailShow()
        {
            TG.BLL.cm_jjsProjectValueAllotConfig bll = new TG.BLL.cm_jjsProjectValueAllotConfig();

            TG.Model.cm_jjsProjectValueAllotDetail model = bll.GetDetailModel(Pro_ID, AllotID);
            //专业 表头
            StringBuilder sbHead = new StringBuilder();
            if (model != null)
            {
                sbHead.Append("<fieldset id=\"memFieldShow\"><legend style=\"font-size:12px;\">经济所人员产值填写</legend>");
                sbHead.Append(" <div id=\"tbProjectValueProcess\" class=\"cls_Container_Report\" >");
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  " + getTypeDetail(model == null ? "" : model.typeStatus.ToString()) + "-配置比例% </td> </tr> </table>");
                sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
                sbHead.Append(" <tr><td rowspan=\"3\" width=\"18%\">项目分类 </td> <td colspan=\"8\" width=\"72%\">编制 </td> <td width=\"10%\" rowspan=\"3\"> 校对  </td> </tr>");
                sbHead.Append("  <tr>  <td rowspan=\"2\" width=\"9%\">编制 <br/>合计 </td> <td colspan=\"3\" width=\"27%\"> 建筑</td> <td colspan=\"4\" width=\"36%\">  安装 </td> </tr>");
                sbHead.Append("  <tr> <td width=\"9%\"> 建筑<br />  合计  </td><td width=\"9%\">土建 </td> <td width=\"9%\">  结构</td>");
                sbHead.Append("<td width=\"9%\">  安装<br /> 合计 </td><td width=\"9%\"> 给排水 </td> <td width=\"9%\">  采暖</td> <td width=\"9%\"> 电气 </td> </tr>");
                sbHead.Append("<table id=\"gvProjectValueProcess\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

                string itemStatus = model.typeStatus.ToString();

                if (itemStatus == "13" || itemStatus == "14" || itemStatus == "15" || itemStatus == "16" || itemStatus == "17" || itemStatus == "19" || itemStatus == "20" || itemStatus == "21" || itemStatus == "22")
                {
                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（%）</td>");
                    sbHead.Append("<td width=\"9%\">" + (100 - decimal.Parse(model.ProofreadPercent.ToString())).ToString("f2") + " %</td>");
                    sbHead.Append("<td width=\"27%\" colspan=\"3\">" + model.Totalbuildingpercent + "%</td>");
                    sbHead.Append("<td width=\"36%\" colspan=\"4\">" + model.TotalInstallationpercent + "%</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadPercent + "%</td>");
                    sbHead.Append("</tr>");

                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（元）</td>");
                    sbHead.Append("<td width=\"9%\">" + (decimal.Parse(model.TotalInstallationcount.ToString()) + decimal.Parse(model.Totalbuildingcount.ToString())).ToString("f2") + "</td>");
                    sbHead.Append("<td width=\"27%\" colspan=\"3\">" + model.Totalbuildingcount + "</td>");
                    sbHead.Append("<td width=\"36%\" colspan=\"4\">" + model.TotalInstallationcount + "</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadPercent + "</td>");
                    sbHead.Append("</tr>");
                }
                else
                {
                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（%）</td>");
                    sbHead.Append("<td width=\"9%\">" + (100 - decimal.Parse(model.ProofreadPercent.ToString())).ToString("f2") + " %</td>");
                    sbHead.Append("<td width=\"9%\">" + model.Totalbuildingpercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.BuildingPercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.StructurePercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.TotalInstallationpercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.DrainPercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.HavcPercent + "%</td>");
                    sbHead.Append("<td width=\"9%\">" + model.ElectricPercent + "%</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadPercent + "%</td>");
                    sbHead.Append("</tr>");

                    sbHead.Append("<tr><td width=\"18%\" class=\"cls_Column\">" + getTypeDetail(itemStatus) + "（元）</td>");
                    sbHead.Append("<td width=\"9%\">" + (decimal.Parse(model.TotalInstallationcount.ToString()) + decimal.Parse(model.Totalbuildingcount.ToString())).ToString("f2") + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.Totalbuildingcount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.BuildingCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.StructureCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.TotalInstallationcount + "</td>");

                    sbHead.Append("<td width=\"9%\">" + model.DrainCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.HavcCount + "</td>");
                    sbHead.Append("<td width=\"9%\">" + model.ElectricCount + "</td>");
                    sbHead.Append("<td width=\"10%\">" + model.ProofreadCount + "</td>");
                    sbHead.Append("</tr>");
                }
                sbHead.Append("</table></div>");
                sbHead.Append("</fieldset>");
                sbHead.Append(getMemberShow());
                lbl_Member.Text = sbHead.ToString(); ;
            }

        }


        private string getMemberShow()
        {
            //专业 表头
            StringBuilder sbHead = new StringBuilder();

            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
            DataTable dtData = BLL.GetjjsProjectValueByMemberAcount(Pro_ID, AllotID).Tables[0];

            sbHead.Append(" <div id=\"tbProjectValueBymemberAmount\" class=\"cls_Container_Report\" >");
            sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td> 项目策划人员产值分配比例% </td> </tr> </table>");
            sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbHead.Append("<tr ><td style=\"width: 15%; text-align: center;\">  名称</td><td style=\"width: 15%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 35%; text-align: center;\"  > 编制 </td> <td style=\"width: 35%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sbHead.Append(" <tr > <td style=\"width: 15%; text-align: center;\" > 专业 </td><td style=\"width: 15%; text-align: center;\">   姓名 </td> <td style=\"width: 17.5%; text-align: center;\"> 比例 </td> <td style=\"width: 17.5%; text-align: center;\"> 金额(元) </td> <td style=\"width: 17.5%; text-align: center;\"> 比例 </td> <td style=\"width: 17.5%; text-align: center;\">金额(元) </td>  </tr>");
            sbHead.Append(" </table>");

            sbHead.Append("<table id=\"gvProjectValueBymemberAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

            StringBuilder sbHtml = new StringBuilder();

            //取得专业
            var dtSpecialtyList = new DataView(dtData).ToTable("Specialty", true, "spe_Name");

            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                var dv = new DataView(dtData) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();

                sbHtml.Append("<tr>");
                sbHtml.Append("<td  width= \"15%\" class=\"cls_Column\" rowspan=\"" + dtMemberBySpecialty.Rows.Count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sbHtml.Append("<tr>");
                    }

                    sbHtml.Append("<td width= \"15%\">" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"17.5%\"> " + dtMemberBySpecialty.Rows[j]["DesignPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"17.5%\">" + dtMemberBySpecialty.Rows[j]["DesignCount"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"17.5%\"> " + dtMemberBySpecialty.Rows[j]["ProofreadPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"17.5%\">" + dtMemberBySpecialty.Rows[j]["ProofreadCount"].ToString() + "</td>");
                    sbHtml.Append("</tr>");
                }
            }
            sbHead.Append(sbHtml.ToString());
            sbHead.Append(" </table></div>");

            return sbHead.ToString();

        }
    }
}