﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace TG.Web.ProjectValueandAllot
{
    public partial class TranBulidingProjectValueAllotAuditBymaster : PageBase
    {
        /// <summary>
        /// 项目系统编号
        /// </summary>
        public int proSysNo
        {
            get
            {
                int proSysNo = 0;
                int.TryParse(Request["proid"], out proSysNo);
                return proSysNo;
            }
        }

        public decimal TotalCount
        {
            get
            {
                decimal totalCount = 0;
                decimal.TryParse(Request["totalCount"], out totalCount);
                return totalCount;
            }
        }

        /// <summary>
        /// 审核记录系统编号
        /// </summary>
        public int ValueAllotAuditSysNo
        {
            get
            {
                int valueAllotAuditSysNo = 0;
                int.TryParse(Request["auditSysID"], out valueAllotAuditSysNo);
                return valueAllotAuditSysNo;
            }
        }

        public string Type
        {
            get
            {
                return Request["type"];
            }
        }

        public TG.Model.cm_TranjjsProjectValueAllot ProjectValueAllot { get; set; }


        /// <summary>
        /// 审核记录实体
        /// </summary>
        public TG.Model.cm_TranProjectValueAuditRecord projectAuditRecordEntity { get; set; }

        public int allotID
        {
            get
            {
                int allotID = 0;
                int.TryParse(Request["allotID"], out allotID);
                return allotID;
            }
        }

        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }

        public string AuditUser { get; set; }

        public int CoperationProcess { get; set; }

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetAuditRecord();
                getProInfo();
                GetProjectValueAllotInfo();
                bindValuedData();
                CreateTableByMemberCount();

            }
        }

        /// <summary>
        /// 项目信息
        /// </summary>
        private void getProInfo()
        {

            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(proSysNo);

            if (pro_model != null)
            {
                //项目名称
                lblProjectName.Text = pro_model.pro_name;
                //关联合同
                this.lblReletive.Text = pro_model.Project_reletive;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.radio_yuan.Checked = true;
                    this.radio_suo.Disabled = true;
                    this.yg.Style.Add("display", "block");
                }
                else if (level.Trim() == "1")
                {
                    this.radio_suo.Checked = true;
                    this.radio_yuan.Disabled = true;
                    this.yg.Style.Add("display", "inline-block");
                    this.sg.Style.Add("display", "inline-block");
                }

                //审核级别
                string[] auditlevel = pro_model.AuditLevel.Split(new char[] { ',' }, StringSplitOptions.None);
                for (int i = 0; i < auditlevel.Length; i++)
                {
                    if (auditlevel[i].Trim() == "0")
                    {
                        audit_yuan.Checked = true;
                    }
                    if (auditlevel[i].Trim() == "1")
                    {
                        audit_suo.Checked = true;
                    }

                }
                audit_yuan.Disabled = true;
                audit_suo.Disabled = true;

                //建设规模
                this.lblScale.Text = pro_model.ProjectScale.ToString();
                //建筑类别
                lblBuildType.Text = pro_model.BuildType;
                //项目总负责
                this.lblPMName.Text = pro_model.PMName;
                //承接部门
                lblcpr_Unit.Text = pro_model.Unit;

                //项目阶段

                string process = pro_model.pro_status;


                string[] prostatus = process.Split(new char[] { ',' }, StringSplitOptions.None);
                for (int i = 0; i < prostatus.Length - 1; i++)
                {
                    if (prostatus[i].Trim() == "方案设计")
                    {
                        CheckBox1.Checked = true;
                    }
                    if (prostatus[i].Trim() == "初步设计")
                    {
                        CheckBox2.Checked = true;
                    }
                    if (prostatus[i].Trim() == "施工图设计")
                    {
                        CheckBox3.Checked = true;
                    }
                    if (prostatus[i].Trim() == "其他")
                    {
                        CheckBox4.Checked = true;
                    }
                }

                //合同额

                lblCoperationAmount.Text = pro_model.Cpr_Acount.ToString();
                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //暖通
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }
                lblTotalCount.Text = TotalCount.ToString();

            }

        }

        /// <summary>
        /// 得到分配信息
        /// </summary>
        private void GetProjectValueAllotInfo()
        {
            TG.BLL.cm_TranBulidingProjectValueAllot bll = new BLL.cm_TranBulidingProjectValueAllot();
            TG.Model.cm_TranBulidingProjectValueAllot ProjectValueAllot = bll.GetModelByProID(proSysNo, allotID);
            if (ProjectValueAllot != null)
            {
                lblStage.Text = setProcessText(ProjectValueAllot.ItemType.ToString());
                CoperationProcess = int.Parse(ProjectValueAllot.ItemType.ToString());
                lblAllotAmount.Text = ProjectValueAllot.AllotCount.ToString();
                txtTheDeptValuePercent.Text = ProjectValueAllot.Thedeptallotpercent.ToString();
                txtTheDeptValueCount.Text = ProjectValueAllot.Thedeptallotcount.ToString();
                txt_DesginPercent.Text = ProjectValueAllot.DesignManagerPercent.ToString();
                txt_DesignCount.Text = ProjectValueAllot.DesignManagerCount.ToString();
                txt_ShouldBeValuePercent.Text = ProjectValueAllot.AllotPercent.ToString();
                txt_ShouldBeValueCount.Text = ProjectValueAllot.AllotCount.ToString();
                lblTotalCount.Text = ProjectValueAllot.TotalCount.ToString();
                lblAllotAmount.Text = ProjectValueAllot.TotalCount.ToString();
                lblYear.Text = ProjectValueAllot.ActualAllountTime;
                lblDesignName.Text = ProjectValueAllot.DesignUserName;
            }
        }

        /// <summary>
        /// 绑定分配后的产值
        /// </summary>
        private void bindValuedData()
        {
            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();

            DataSet ds = BLL.GetValuedDataList(allotID);

            gvStageOne.DataSource = ds.Tables[0];
            gvStageOne.DataBind();

            gvStageTwo.DataSource = ds.Tables[0];
            gvStageTwo.DataBind();

            gvStageThree.DataSource = ds.Tables[0];
            gvStageThree.DataBind();

            gvStageFour.DataSource = ds.Tables[0];
            gvStageFour.DataBind();

            gvStageTen.DataSource = ds.Tables[0];
            gvStageTen.DataBind();
            DataTable dtProcess = ds.Tables[2];

            //生成方案设计 初步设计 施工图 后期服务表单
            if (CoperationProcess < 4 || CoperationProcess==10)
            {
                CreateSpeAmountTable(ds.Tables[1]);
                CreateProcessTable(dtProcess);
            }
            else
            {
                CreateOtherAmountTable(ds.Tables[3], dtProcess);
            }

        }

        /// 生成阶段专业产值表单--保存之后数据
        /// </summary>
        /// <param name="dtSpeAmount"></param>
        private void CreateSpeAmountTable(DataTable dtSpeAmount)
        {

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();
            sbSpeHead.Append(" <div id=\"spetamountable\" class=\"cls_Container_Report\" style=\"display: none;\">");
            sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  项目各设计阶段专业之间产值分配比例% </td> </tr> </table>");
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;height: 30px; \"> 设计阶段</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">合计</td>");
            sbSpeHead.Append(" </table>");

            sbSpeHead.Append("<table id=\"gvSpeAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

            //方案设计
            var dataProgram = new DataView(dtSpeAmount).ToTable("ProgramCount", false, "ProgramCount", "Specialty", "ProgramPercent");

            //第一行循环方案设计专业
            StringBuilder sb_one = new StringBuilder();
            sb_one.Append("<tr>");
            sb_one.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">方案设计</td>");

            //方案的金额
            for (int j = 0; j < dataProgram.Rows.Count; j++)
            {
                sb_one.Append("<td width=\"9%\">" + dataProgram.Rows[j]["ProgramPercent"].ToString() + "%</td>");
                sb_one.Append("<td width=\"9%\">" + dataProgram.Rows[j]["ProgramCount"].ToString() + "</td>");
            }
            sb_one.Append("<td width=\"9%\">100%</td>");
            //取得总和
            var sumProgramCount = dataProgram.AsEnumerable().Sum(s => s.Field<decimal>("ProgramCount"));
            sb_one.Append("<td width=\"9%\">" + sumProgramCount + "</td>");
            sb_one.Append("</tr>");

            //初步设计
            var dataPreliminary = new DataView(dtSpeAmount).ToTable("preliminaryCount", false, "preliminaryCount", "Specialty", "preliminaryPercent");

            //第一行循环方案设计专业
            StringBuilder sb_two = new StringBuilder();
            sb_two.Append("<tr>");
            sb_two.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">初步设计</td>");


            for (int j = 0; j < dataPreliminary.Rows.Count; j++)
            {
                sb_two.Append("<td width=\"9%\">" + dataPreliminary.Rows[j]["preliminaryPercent"].ToString() + "%</td>");
                sb_two.Append("<td  width=\"9%\">" + dataPreliminary.Rows[j]["preliminaryCount"].ToString() + "</td>");
            }
            sb_two.Append("<td width=\"9%\">100%</td>");
            //取得总和
            var sumPreliminaryCount = dataPreliminary.AsEnumerable().Sum(s => s.Field<decimal>("preliminaryCount"));
            sb_two.Append("<td width=\"9%\">" + sumPreliminaryCount + "</td>");

            sb_two.Append("</tr>");

            //施工图设计
            var dataWorkDraw = new DataView(dtSpeAmount).ToTable("WorkDrawCount", false, "WorkDrawCount", "Specialty", "WorkDrawPercent");

            //第一行循环方案设计专业
            StringBuilder sb_three = new StringBuilder();
            sb_three.Append("<tr>");
            sb_three.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">施工图设计</td>");

            for (int j = 0; j < dataWorkDraw.Rows.Count; j++)
            {
                sb_three.Append("<td width=\"9%\">" + dataWorkDraw.Rows[j]["WorkDrawPercent"].ToString() + "%</td>");
                sb_three.Append("<td width=\"9%\">" + dataWorkDraw.Rows[j]["WorkDrawCount"].ToString() + "</td>");
            }
            sb_three.Append("<td width=\"9%\">100%</td>");
            //取得总和
            var sumWorkDrawCount = dataWorkDraw.AsEnumerable().Sum(s => s.Field<decimal>("WorkDrawCount"));
            sb_three.Append("<td width=\"9%\">" + sumWorkDrawCount + "</td>");
            sb_three.Append("</tr>");

            //后期服务
            var dataLateStage = new DataView(dtSpeAmount).ToTable("LateStageCount", false, "LateStageCount", "Specialty", "LateStagePercent");

            //第一行循环方案设计专业
            StringBuilder sb_four = new StringBuilder();
            sb_four.Append("<tr>");
            sb_four.Append("<td style=\"width: 10%; text-align: center;\" class=\"cls_Column\">后期服务</td>");

            for (int j = 0; j < dataLateStage.Rows.Count; j++)
            {
                sb_four.Append("<td width=\"9%\">" + dataLateStage.Rows[j]["LateStagePercent"].ToString() + "%</td>");
                sb_four.Append("<td width=\"9%\">" + dataLateStage.Rows[j]["LateStageCount"].ToString() + "</td>");
            }
            sb_four.Append("<td width=\"9%\">100%</td>");
            //取得总和
            var sumLateStageCount = dataLateStage.AsEnumerable().Sum(s => s.Field<decimal>("LateStageCount"));
            sb_four.Append("<td width=\"9%\">" + sumLateStageCount + "</td>");
            sb_four.Append("</tr>");


            if (CoperationProcess == 0)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 1)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_two.ToString());
            }

            else if (CoperationProcess == 2)
            {
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 3)
            {
                sbSpeHead.Append(sb_two.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }
            else if (CoperationProcess == 10)
            {
                sbSpeHead.Append(sb_one.ToString());
                sbSpeHead.Append(sb_three.ToString());
                sbSpeHead.Append(sb_four.ToString());
            }

            sbSpeHead.Append(" </table></div> ");
            lbl_SpeAmount.Text = sbSpeHead.ToString();


        }

        /// <summary>
        /// 生成工序--保存之后数据
        /// </summary>
        /// <param name="dtProcessAmount">工序金额</param>
        private void CreateProcessTable(DataTable dtProcessAmount)
        {

            //方案工序
            DataTable dtProgram = new DataView(dtProcessAmount) { RowFilter = "type=0" }.ToTable();
            string program = getProcessDeatil(dtProgram, 0);

            //初步工序
            DataTable dtPreliminary = new DataView(dtProcessAmount) { RowFilter = "type=1" }.ToTable();
            string preliminary = getProcessDeatil(dtPreliminary, 1);

            //施工图
            DataTable dtWorkDraw = new DataView(dtProcessAmount) { RowFilter = "type=2" }.ToTable();
            string workDraw = getProcessDeatil(dtWorkDraw, 2);

            //后期服务
            DataTable dtLateStage = new DataView(dtProcessAmount) { RowFilter = "type=3" }.ToTable();
            string lateStage = getProcessDeatil(dtLateStage, 3);

            StringBuilder strTab = new StringBuilder();
            strTab.Append(" <div id=\"tabsProcess\">  <ul style=\"width:98%; margin:auto;\"> ");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabsProcess-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");

            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsProcess-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");

            }
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsProcess-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsProcess-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
            }
            strTab.Append(" </ul>");
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                strTab.Append("<div id=\"tabsProcess-1\">");
                strTab.Append(program);
                strTab.Append(" </div>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                strTab.Append(" <div id=\"tabsProcess-2\">");
                strTab.Append(preliminary);
                strTab.Append(" </div>");
            }
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                strTab.Append(" <div id=\"tabsProcess-3\">");
                strTab.Append(workDraw);
                strTab.Append(" </div>");
                strTab.Append(" <div id=\"tabsProcess-4\">");
                strTab.Append(lateStage);
                strTab.Append(" </div>");
            }
            strTab.Append(" </div>");
            lbl_ProcessAmount.Text = strTab.ToString();

        }

        /// <summary>
        /// 得到工序分配明细
        /// </summary>
        /// <param name="dtProcess"></param>
        /// <param name="process">哪个阶段</param>
        /// <returns></returns>
        private string getProcessDeatil(DataTable dtProcess, int process)
        {
            StringBuilder sb = new StringBuilder();

            if (process == 0)
            {
                sb.Append(" <div id=\"processAmountOneTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  方案设计工序产值分配比例% </td> </tr> </table>");
            }
            else if (process == 1)
            {
                sb.Append(" <div id=\"processAmountTwoTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  初步设计工序产值分配比例% </td> </tr> </table>");
            }
            else if (process == 2)
            {
                sb.Append(" <div id=\"processAmountThreeTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  施工图工序产值分配比例% </td> </tr> </table>");
            }
            else if (process == 3)
            {
                sb.Append(" <div id=\"processAmountFourTable\" class=\"cls_Container_Report\" style=\"display: none;\">");
                sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  后期服务工序产值分配比例% </td> </tr> </table>");

            }
            else
            {
                sb.Append(" <div id=\"processAmountFiveTable\" class=\"cls_Container_Report\" style=\"display: none;\">");


                if (CoperationProcess == 4)
                {
                    sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  室外工程工序产值分配比例% </td> </tr> </table>");

                }
                else if (CoperationProcess == 5)
                {
                    sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  锅炉房工序产值分配比例% </td> </tr> </table>");

                }
                else if (CoperationProcess == 6)
                {
                    sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建水泵房工序产值分配比例% </td> </tr> </table>");

                }
                else if (CoperationProcess == 7)
                {
                    sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建变配所(室)工序产值分配比例% </td> </tr> </table>");

                }
                else if (CoperationProcess == 8)
                {
                    sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  单建地下室（车库）工序产值分配比例% </td> </tr> </table>");

                }
                else if (CoperationProcess == 9)
                {
                    sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  市政道路工程工序产值分配比例% </td> </tr> </table>");

                }

            }
            sb.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sb.Append(" <tr> <td style=\"width: 10%; text-align: center;\">   专业 </td>  <td style=\"width: 18%;  text-align: center;\" colspan=\"2\">    审核   </td> ");
            sb.Append(" <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 专业负责 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">  校对 </td>   <td style=\"width: 18%; text-align: center;\" colspan=\"2\">设计  </td> <td style=\"width: 18%; text-align: center;\" colspan=\"2\"> 合计</td>  </tr>");
            sb.Append(" </table>");
            sb.Append("<table id=\"gvProcessAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\"  >");

            for (int j = 0; j < dtProcess.Rows.Count; j++)
            {
                sb.Append("<tr>");
                sb.Append("<td width=10% class=\"cls_Column\">" + dtProcess.Rows[j]["Specialty"].ToString() + "</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["AuditPercent"].ToString() + "%</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["AuditAmount"].ToString() + "</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["SpecialtyHeadPercent"].ToString() + "%</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["SpecialtyHeadAmount"].ToString() + "</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["ProofreadPercent"].ToString() + "%</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["ProofreadAmount"].ToString() + "</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["DesignPercent"].ToString() + "%</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["DesignAmount"].ToString() + "</td>");
                sb.Append("<td width=9%>100%</td>");
                sb.Append("<td width=9%>" + dtProcess.Rows[j]["TotalAmount"].ToString() + "</td>");
                sb.Append("</tr>");
            }
            sb.Append("</table></div>");
            return sb.ToString();

        }

        /// <summary>
        /// 生成表四之后的表单--保存之后数据
        /// </summary>
        /// <param name="dtOutAmount"></param>
        /// <param name="dtProcess"></param>
        private void CreateOtherAmountTable(DataTable dtOutAmount, DataTable dtProcess)
        {

            //专业 表头
            StringBuilder sbSpeHead = new StringBuilder();

            sbSpeHead.Append(" <div id=\"outdooramounttable\" class=\"cls_Container_Report\" style=\"display: none;\">");

            if (CoperationProcess == 4)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  室外工程产值分配比例% </td> </tr> </table>");
            }
            else if (CoperationProcess == 5)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  锅炉房产值分配比例% </td> </tr> </table>");
            }
            else if (CoperationProcess == 6)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建水泵房产值分配比例% </td> </tr> </table>");

            }
            else if (CoperationProcess == 7)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建变配所(室)产值分配比例% </td> </tr> </table>");

            }
            else if (CoperationProcess == 8)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  单建地下室（车库）产值分配比例% </td> </tr> </table>");

            }
            else if (CoperationProcess == 9)
            {
                sbSpeHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  市政道路工程产值分配比例% </td> </tr> </table>");

            }
            sbSpeHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbSpeHead.Append("<tr> <td style=\"width: 10%; text-align: center;\"> 专业</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">建筑</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">结构</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">给排水</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">电气</td>");
            sbSpeHead.Append("<td colspan=\"2\" style=\"width:18%;text-align: center;\">合计</td>");

            sbSpeHead.Append(" </table>");

            sbSpeHead.Append("<table id=\"gvOutDoorAmount\" width=\"98%\"  class=\"cls_ProjAllot_Table cls_ProjAllot_label2\"  >");
            if (dtProcess.Rows.Count > 0)
            {
                sbSpeHead.Append("<tr><td width=\"10%\" class=\"cls_Column\">比例</td>");
                //建筑
                var bulidingData = new DataView(dtOutAmount) { RowFilter = "Specialty='建筑'" }.ToTable();
                if (bulidingData != null && bulidingData.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td width=\"9%\">" + bulidingData.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td width=\"9%\">" + bulidingData.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                }

                //结构
                var structureCount = new DataView(dtOutAmount) { RowFilter = "Specialty='结构'" }.ToTable();
                if (structureCount != null && structureCount.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td  width=\"9%\">" + structureCount.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td  width=\"9%\">" + structureCount.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                }

                //给排水
                var drainCount = new DataView(dtOutAmount) { RowFilter = "Specialty='给排水'" }.ToTable();
                if (drainCount != null && drainCount.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td  width=\"9%\">" + drainCount.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td  width=\"9%\">" + drainCount.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                }

                //电气
                var electricCount = new DataView(dtOutAmount) { RowFilter = "Specialty='电气'" }.ToTable();
                if (electricCount != null && electricCount.Rows.Count > 0)
                {
                    sbSpeHead.Append("<td  width=\"9%\">" + electricCount.Rows[0]["SpecialtyPercent"].ToString() + "</td>");
                    sbSpeHead.Append("<td  width=\"9%\">" + electricCount.Rows[0]["SpecialtyCount"].ToString() + "</td>");
                }
                else
                {
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                    sbSpeHead.Append("<td width=\"9%\">0</td>");
                }

                var sumTotal = dtOutAmount.AsEnumerable().Sum(s => s.Field<decimal>("SpecialtyCount"));
                //合计
                sbSpeHead.Append("<td  width=\"9%\">100%</td>");
                sbSpeHead.Append("<td  width=\"9%\">" + sumTotal + "</td>");
                sbSpeHead.Append("</tr>");
            }
            else
            {
                sbSpeHead.Append("<tr><td colspan=\"11\">没有数据</td></tr>");
            }

            sbSpeHead.Append(" </table></div>");

            lbl_outAmount.Text = sbSpeHead.ToString();

            lbl_processAmountFiveTable.Text = getProcessDeatil(dtProcess, 4);
        }


        /// <summary>
        /// 生成分配之后的产值人员
        /// </summary>
        private void CreateTableByMemberCount()
        {
            if (projectAuditRecordEntity != null)
            {
                TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();
                DataTable dtData = BLL.GetTranBulidingProjectValueByMemberAcount((int)projectAuditRecordEntity.AllotID).Tables[0];

                lblMember.Text = CreateTableByMemberCount(dtData);
            }
        }

        /// <summary>
        /// 生成分配之后的产值人员
        /// </summary>
        /// <param name="dtData"></param>
        /// <returns></returns>
        private string CreateTableByMemberCount(DataTable dtData)
        {
            //方案工序
            string program = "";
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
            {
                DataTable dtProgram = new DataView(dtData) { RowFilter = "ItemType=0" }.ToTable();
                program = createMemberCountDetail(dtProgram, 0);
            }
            //初步工序
            string preliminary = "";
            if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
            {
                DataTable dtPreliminary = new DataView(dtData) { RowFilter = "ItemType=1" }.ToTable();
                preliminary = createMemberCountDetail(dtPreliminary, 1);
            }
            //施工图
            string workDraw = "";
            string lateStage = "";
            if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
            {
                DataTable dtWorkDraw = new DataView(dtData) { RowFilter = "ItemType=2" }.ToTable();
                workDraw = createMemberCountDetail(dtWorkDraw, 2);

                //后期服务
                DataTable dtLateStage = new DataView(dtData) { RowFilter = "ItemType=3" }.ToTable();
                lateStage = createMemberCountDetail(dtLateStage, 3);
            }

            if (CoperationProcess >= 4 && CoperationProcess != 10)
            {
                return createMemberCountDetail(dtData, CoperationProcess);
            }
            else
            {
                StringBuilder strTab = new StringBuilder();
                strTab.Append(" <div id=\"tabsMemAmount\">  <ul style=\"width:98%; margin:auto;\"> ");
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                {
                    strTab.Append("<li><div class=\"cls_1\"> <a href=\"#tabsMemAmount-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                {
                    strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");

                }
                if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                {
                    strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                    strTab.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");

                }
                strTab.Append(" </ul>");
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 10)
                {
                    strTab.Append("<div id=\"tabsMemAmount-1\">");
                    strTab.Append(program);
                    strTab.Append(" </div>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 1 || CoperationProcess == 3)
                {
                    strTab.Append(" <div id=\"tabsMemAmount-2\">");
                    strTab.Append(preliminary);
                    strTab.Append(" </div>");
                }
                if (CoperationProcess == 0 || CoperationProcess == 2 || CoperationProcess == 3 || CoperationProcess == 10)
                {
                    strTab.Append(" <div id=\"tabsMemAmount-3\">");
                    strTab.Append(workDraw);
                    strTab.Append(" </div>");
                    strTab.Append(" <div id=\"tabsMemAmount-4\">");
                    strTab.Append(lateStage);
                    strTab.Append(" </div>");
                }
                strTab.Append(" </div>");

                return strTab.ToString();
            }
        }

        /// <summary>
        /// 生成人员产值明细
        /// </summary>
        /// <param name="dtMemberCount"></param>
        /// <param name="process"></param>
        /// <returns></returns>
        private string createMemberCountDetail(DataTable dtData, int process)
        {
            //专业 表头
            StringBuilder sbHead = new StringBuilder();

            sbHead.Append(" <div id=\"tbProjectValueBymemberAmount\" class=\"cls_Container_Report\" >");

            if (process == 0)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  方案设计-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            else if (process == 1)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  初步设计-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 2)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  施工图设计-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 3)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  后期服务-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 4)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  室外工程-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 5)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  锅炉房-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            else if (process == 6)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建水泵房-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            else if (process == 7)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  地上单建变配所(室)-项目策划人员产值分配比例% </td> </tr> </table>");

            }
            else if (process == 8)
            {
                sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td>  单建地下室（车库）-项目策划人员产值分配比例% </td> </tr> </table>");
            }
            sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbHead.Append("<tr ><td style=\"width: 10%; text-align: center;\">  名称</td><td style=\"width: 10%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sbHead.Append(" <tr > <td style=\"width: 10%; text-align: center;\" > 专业 </td><td style=\"width: 10%; text-align: center;\">   姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
            sbHead.Append(" </table>");

            sbHead.Append("<table id=\"gvProjectValueBymemberAmount\" width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");

            StringBuilder sbHtml = new StringBuilder();

            //取得专业
            var dtSpecialtyList = new DataView(dtData).ToTable("Specialty", true, "spe_Name");

            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                // 查找该专业下面的所有人员
                var dv = new DataView(dtData) { RowFilter = "spe_Name='" + dtSpecialtyList.Rows[i]["spe_Name"] + "'" };
                var dtMemberBySpecialty = dv.ToTable();

                sbHtml.Append("<tr>");
                sbHtml.Append("<td  width= \"10%\" class=\"cls_Column\" rowspan=\"" + dtMemberBySpecialty.Rows.Count + "\">" + dtSpecialtyList.Rows[i]["spe_Name"] + "</td>");

                for (int j = 0; j < dtMemberBySpecialty.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sbHtml.Append("<tr>");
                    }

                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["mem_Name"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["DesignPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["DesignCount"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["SpecialtyHeadPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["SpecialtyHeadCount"].ToString() + "</td>");

                    sbHtml.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["AuditPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["AuditCount"].ToString() + "</td>");
                    sbHtml.Append("<td width= \"10%\"> " + dtMemberBySpecialty.Rows[j]["ProofreadPercent"].ToString() + "%</td>");
                    sbHtml.Append("<td width= \"10%\">" + dtMemberBySpecialty.Rows[j]["ProofreadCount"].ToString() + "</td>");
                    sbHtml.Append("</tr>");
                }


            }
            sbHead.Append(sbHtml.ToString());
            sbHead.Append(" </table></div>");

            return sbHead.ToString();
        }

        /// <summary>
        /// 设置状态文本
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        private string setProcessText(string process)
        {
            string processText = "";

            if (process == "0")
            {
                processText = "方案+初设+施工图+后期";
            }
            else if (process == "1")
            {
                processText = "方案+初设";
            }
            else if (process == "2")
            {
                processText = "施工图+后期";
            }
            else if (process == "3")
            {
                processText = "初设+施工图+后期";
            }
            else if (process == "4")
            {
                processText = "室外工程";
            }
            else if (process == "5")
            {
                processText = "锅炉房";
            }
            else if (process == "6")
            {
                processText = "地上单建水泵房";
            }
            else if (process == "7")
            {
                processText = "地上单建变配电所(室)";
            }
            else if (process == "8")
            {
                processText = "单建地下室（车库）";
            }
            else if (process == "10")
            {
                processText = "方案+施工图+后期";
            }
            return processText;
        }

        private void GetAuditRecord()
        {
            TG.BLL.cm_TranProjectValueAuditRecord BLL = new TG.BLL.cm_TranProjectValueAuditRecord();

            projectAuditRecordEntity = BLL.GetModel(ValueAllotAuditSysNo);
            if (projectAuditRecordEntity != null)
            {
                TG.Model.tg_member user = new TG.Model.tg_member();
                //查询联系人姓名
                if (!string.IsNullOrEmpty(projectAuditRecordEntity.AuditUser))
                {
                    user = new TG.BLL.tg_member().GetModel(int.Parse(projectAuditRecordEntity.AuditUser));
                    AuditUser = user == null ? "" : user.mem_Name;
                }
            }
            else
            {
                lblNotData.Text = "<table id=\"tbdelete\" align=\"center\"><tr><td style=\" font-size: 12px;font-weight: bold; color: Red;\"  >此信息已被删除!</td></tr></table>";
            }
        }
    }
}