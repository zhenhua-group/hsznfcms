﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.ProjectValueandAllot
{
    public partial class CostPreAllDetailsBymaster : PageBase
    {
        TG.BLL.CostImportBymaster bll = new TG.BLL.CostImportBymaster();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                InitDate();
                BindPreviewPower();
                bindData();

            }
        }
        //绑定前台权限
        public void BindPreviewPower()
        {

        }
        //判断是否进行权限判断
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (costUserId =" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND costUnit = (Select unit_Id From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //绑定单位
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            //strWhere += " AND unit_ID NOT between 250 and 259";
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //初始时间设置
        protected void InitDate()
        {
            string str_year = DateTime.Now.Year.ToString();
            this.txt_costdate1.Value = str_year + "-01-01";
            this.txt_costdate2.Value = str_year + "-12-31";
        }
        //绑定基本类型
        private void BindType()
        {
            //throw new NotImplementedException();
            //TG.BLL.cm_CostType costbll = new TG.BLL.cm_CostType();
            //string strwhereAll = "costgroup in (0,4)";
            //this.drp_costTypeAll.DataSource = costbll.GetList(strwhereAll);
            //this.drp_costTypeAll.DataTextField = "costName";
            //this.drp_costTypeAll.DataValueField = "ID";
            //this.drp_costTypeAll.DataBind();
        }
        //绑定数据
        private void bindData()
        {
            // throw new NotImplementedException();
            StringBuilder sbtype = new StringBuilder();
            StringBuilder sbunit = new StringBuilder();
            StringBuilder sbtime = new StringBuilder();
            StringBuilder srtime = new StringBuilder();
            string unittype = this.drp_unitType.SelectedItem.Value;
            string unitId = this.drp_unit.SelectedItem.Value;
            string starttime = this.txt_costdate1.Value;
            string endtime = this.txt_costdate2.Value;
            this.hiddenUnit.Value = unitId;
            string costGroup = "0";
            string unitName = this.drp_unit.SelectedItem.Text.Trim();
            //string typeName = this.drp_costTypeAll.SelectedItem.Text.Trim();
            string titleName = "";
            if (unittype == "4")
            {
                if (unitId == "-1")
                {
                    titleName = "生产部门" + starttime + "至" + endtime + "费用明细统计";
                }
                else
                {
                    titleName = unitName + " " + starttime + "至" + endtime + "费用明细统计";
                }
            }
            else
            {
                titleName = unitName + " " + starttime + "至" + endtime + "费用明细统计";
            }
            this.costTitle.Text = titleName;
            if (unittype == "4")
            {
                costGroup = "4";
                if (unitId == "-1")
                {
                    sbtype.Append("and costgroup in(0,4)");
                    sbunit.Append("and unit_ID not in (236,250,251,252,253,254,255,256,257,258,259)");
                }
                else
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,4)");
                }
            }
            else
            {
                if (unitId == "250")
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,1)");
                    costGroup = "1";
                }
                else if (unitId == "252")
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,2)");
                    costGroup = "2";
                }
                else if (unitId == "257")
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,3)");
                    costGroup = "3";
                }
            }
            sbunit.Append("AND unit_ID NOT IN (" + base.NotShowUnitList + ")");

            //if (typeId != "-1")
            //{
            //    sbtype.Append("and id=" + typeId);
            //}
            if (!string.IsNullOrEmpty(starttime) && !string.IsNullOrEmpty(endtime))
            {
                sbtime.AppendFormat(" AND costTime between ''{0}'' and ''{1}'' ", starttime, endtime);
                srtime.AppendFormat(" AND ch.InAcountTime between '{0}' and '{1}' ", starttime, endtime);
                //srtime
            }
            //年份
            if (string.IsNullOrEmpty(starttime) && !string.IsNullOrEmpty(endtime))
            {
                sbtime.AppendFormat(" AND costTime between ''{0}'' and ''{1}'' ", DateTime.Now.ToString("yyyy-MM-dd"), endtime);
                srtime.AppendFormat(" AND ch.InAcountTime between '{0}' and '{1}' ", starttime, endtime);

            }
            //年份
            if (!string.IsNullOrEmpty(starttime) && string.IsNullOrEmpty(endtime))
            {
                sbtime.AppendFormat(" AND costTime between ''{0}'' and ''{1}'' ", starttime, DateTime.Now.ToString("yyyy-MM-dd"));
                srtime.AppendFormat(" AND ch.InAcountTime between '{0}' and '{1}' ", starttime, endtime);

            }
            this.txt_costdate1.Value = starttime;
            this.txt_costdate2.Value = endtime;
            //收入
            string SRsql = @"SELECT *,(SELECT isnull(SUM(ch.acount),0.00) from cm_ProjectCharge ch left JOIN cm_Coperation cop on ch.cprID=cop.cpr_Id
WHERE cop.cpr_Unit=unit_Name AND ch.Status='E' {0}
 ) as ChargeAcount
 FROM tg_unit where 1=1 {1}";
            SRsql = string.Format(SRsql, srtime.ToString(), sbunit.ToString());
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(SRsql).Tables[0];
            //成本
            this.grid_Financial.DataSource = bll.GetCostAllByProc(sbtime.ToString(), sbunit.ToString(), sbtype.ToString());
            this.grid_Financial.DataBind();
            //  SqlDataReader reader = bll.GetAllCostByProc(sbtime.ToString(), sbtype.ToString(), sbunit.ToString(), costGroup) as SqlDataReader;
            //  List<ChargeType> modellist = new List<ChargeType>();
            //  if (reader.HasRows)
            //  {
            //      int cols = reader.FieldCount;
            //      ChargeType model1 = new ChargeType();
            //      model1.TypeName = reader.GetName(0);
            //      List<string> listCharge = new List<string>();
            //      for (int i = 1; i < cols; i++)
            //      {
            //          string colsName = reader.GetName(i).ToString();
            //          listCharge.Add(colsName);
            //      }
            //      model1.ChargeArray = listCharge;
            //      modellist.Add(model1);
            //      while (reader.Read())
            //      {
            //          ChargeType model = new ChargeType();
            //          model.TypeName = reader.GetValue(0).ToString();
            //          List<string> listValue = new List<string>();
            //          for (int i = 1; i < cols; i++)
            //          {
            //              string colsValue = reader.GetValue(i).ToString();
            //              listValue.Add(colsValue);
            //          }
            //          model.ChargeArray = listValue;
            //          modellist.Add(model);
            //      }
            //  }
            //  List<ChargeType> listreturn = ReturnCharge(modellist);
            //  if (listreturn.Count > 0)
            //  {
            //      foreach (var item in listreturn)
            //      {
            //          this.test.Text += item.TypeName;
            //          foreach (var itemcharge in item.ChargeArray)
            //          {
            //              this.test.Text += itemcharge + "--";
            //          }
            //          this.test.Text += "<br/>";
            //      }
            //  }
            //  //表头所名
            //  StringBuilder sbTableTile = new StringBuilder();
            //  //对应所收入
            //  StringBuilder sbTableCharge = new StringBuilder();
            //  //成本费用内容
            //  StringBuilder sbTableContent = new StringBuilder();
            //  //遍历行，增加表头信息
            //  sbTableTile.Append("<table class=\"table table-bordered table-hover\" style=\"width:100%;\"><tr><td>序号</td><td>项目</td>");
            //  sbTableCharge.Append("<tr><td>一</td><td>实际收入</td>");
            //  decimal AllChargeAcount = 0.00m;
            //  for (int i = 0; i < dt.Rows.Count; i++)
            //  {
            //      sbTableTile.Append("<td>" + dt.Rows[i]["unit_Name"].ToString().Trim() + "</td>");
            //      sbTableCharge.Append("<td>" + Convert.ToDecimal(dt.Rows[i]["ChargeAcount"].ToString()) + "</td>");
            //      AllChargeAcount += Convert.ToDecimal(dt.Rows[i]["ChargeAcount"].ToString());
            //  }
            //  sbTableTile.Append("<td>合计</td>");
            //  sbTableCharge.Append("<td>" + AllChargeAcount.ToString("f2") + "</td>");
            //  sbTableTile.Append("</tr>");
            //  sbTableCharge.Append("</tr>");
            //  sbTableTile.Append(sbTableCharge.ToString());

            ////  收入表头结束
            //  //成本项开始合并

            //  sbTableContent.Append("<tr>");

            //  sbTableTile.Append("</table>");
            //  this.Literal1.Text = sbTableTile.ToString();
        }



        protected void btn_export_Click(object sender, EventArgs e)
        {

            // throw new NotImplementedException();
            StringBuilder sbtype = new StringBuilder();
            StringBuilder sbunit = new StringBuilder();
            StringBuilder sbtime = new StringBuilder();
            StringBuilder srtime = new StringBuilder();
            string unittype = this.drp_unitType.SelectedItem.Value;
            string unitId = this.drp_unit.SelectedItem.Value;
            string starttime = this.txt_costdate1.Value;
            string endtime = this.txt_costdate2.Value;
            this.hiddenUnit.Value = unitId;
            string unitName = this.drp_unit.SelectedItem.Text.Trim();
            //string typeName = this.drp_costTypeAll.SelectedItem.Text.Trim();
            string titleName = "";
            if (unittype == "4")
            {
                if (unitId == "-1")
                {
                    titleName = "全院部门 " + starttime + "至" + endtime + "费用明细统计";
                }
                else
                {
                    titleName = unitName + " " + starttime + "至" + endtime + "费用明细统计";
                }
            }
            else
            {
                titleName = unitName + " " + starttime + "至" + endtime + "费用明细统计";
            }
            string costGroup = "0";
            if (unittype == "4")
            {
                costGroup = "4";
                if (unitId == "-1")
                {
                    sbtype.Append("and costgroup in(0,4)");
                    sbunit.Append("and unit_ID not in (236,250,251,252,253,254,255,256,257,258,259)");
                }
                else
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,4)");
                }
            }
            else
            {
                if (unitId == "250")
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,1)");
                    costGroup = "1";
                }
                else if (unitId == "252")
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,2)");
                    costGroup = "2";
                }
                else if (unitId == "257")
                {
                    sbunit.Append("and unit_ID=" + unitId);
                    sbtype.Append("and costgroup in(0,3)");
                    costGroup = "3";
                }
            }
            sbunit.Append("AND unit_ID NOT IN (" + base.NotShowUnitList + ")");

            //if (typeId != "-1")
            //{
            //    sbtype.Append("and id=" + typeId);
            //}
            if (!string.IsNullOrEmpty(starttime) && !string.IsNullOrEmpty(endtime))
            {
                sbtime.AppendFormat(" AND costTime between ''{0}'' and ''{1}'' ", starttime, endtime);
                srtime.AppendFormat(" AND ch.InAcountTime between '{0}' and '{1}' ", starttime, endtime);
                //srtime
            }
            //年份
            if (string.IsNullOrEmpty(starttime) && !string.IsNullOrEmpty(endtime))
            {
                sbtime.AppendFormat(" AND costTime between ''{0}'' and ''{1}'' ", DateTime.Now.ToString("yyyy-MM-dd"), endtime);
                srtime.AppendFormat(" AND ch.InAcountTime between '{0}' and '{1}' ", starttime, endtime);

            }
            //年份
            if (!string.IsNullOrEmpty(starttime) && string.IsNullOrEmpty(endtime))
            {
                sbtime.AppendFormat(" AND costTime between ''{0}'' and ''{1}'' ", starttime, DateTime.Now.ToString("yyyy-MM-dd"));
                srtime.AppendFormat(" AND ch.InAcountTime between '{0}' and '{1}' ", starttime, endtime);

            }
            this.txt_costdate1.Value = starttime;
            this.txt_costdate2.Value = endtime;
            //收入
            string SRsql = @"SELECT *,(SELECT isnull(SUM(ch.acount),0.00) from cm_ProjectCharge ch left JOIN cm_Coperation cop on ch.cprID=cop.cpr_Id
WHERE cop.cpr_Unit=unit_Name AND ch.Status='E' {0}
 ) as ChargeAcount
 FROM tg_unit where 1=1 {1}";
            SRsql = string.Format(SRsql, srtime.ToString(), sbunit.ToString());
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(SRsql).Tables[0];
            //成本
            SqlDataReader reader = bll.GetCostAllByProc(sbtime.ToString(), sbunit.ToString(), sbtype.ToString()) as SqlDataReader;
            string modelPath = " ~/TemplateXls/CostPreAllDetailsBymaster.xls";
            ALlExportMethod(titleName, reader, modelPath, dt);
        }
        private void ALlExportMethod(string unitName, SqlDataReader reader, string modelPath, DataTable table)
        {
            HSSFWorkbook wb = null;
            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);
            int cols = 0;
            cols = reader.FieldCount;
            IRow rowF = ws.CreateRow(0);
            rowF.Height = 18 * 20;

            ICell icellF = rowF.CreateCell(0);
            icellF.CellStyle = style2;
            icellF.SetCellValue(unitName);
            string colsName = SetWord(cols);
            string colsNameA = SetWord(cols + 1);
            IRow rowtitle = ws.CreateRow(1);
            rowtitle.Height = 18 * 20;
            for (int i = 0; i < reader.FieldCount; i++)
            {
                ICell icell = rowtitle.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellValue(reader.GetName(i).ToString());
            }

            ICell icellTHJ = rowtitle.CreateCell(cols);
            icellTHJ.CellStyle = style2;
            icellTHJ.SetCellValue("合计");
            int rows = 2;
            int rownum = 3;
            int colsedit = reader.FieldCount;
            //1、导出创建收入行
            IRow rowInsert = ws.CreateRow(rows);
            ICell icellI = rowInsert.CreateCell(0);
            icellI.SetCellValue("收入总和");
            for (int i = 0; i < table.Rows.Count; i++)
            {
                ICell icellIC = rowInsert.CreateCell(i + 1);
                icellIC.CellStyle = style2;
                icellIC.SetCellValue(Convert.ToDouble(table.Rows[i]["ChargeAcount"].ToString()) * 10000);
            }
            ICell icellInsert = rowInsert.CreateCell(cols);
            icellInsert.CellStyle = style2;
            icellInsert.SetCellFormula("sum(B" + rownum + ":" + colsName + rownum + ")");
            //成本总合计
            IRow rowZCBH = ws.CreateRow(rows + 1);
            ICell icellZCBH = rowZCBH.CreateCell(0);
            icellZCBH.CellStyle = style2;
            icellZCBH.SetCellValue("总成本合计");
            ICell icellZCBHA = rowZCBH.CreateCell(cols);
            icellZCBHA.CellStyle = style2;
            icellZCBHA.SetCellFormula("sum(B" + (rownum + 1) + ":" + colsName + (rownum + 1) + ")");

            //2、循环得到工资、五险一金以及合并项的所有。
            IRow rowGZ = ws.CreateRow(rows + 2);
            ICell icellGZF = rowGZ.CreateCell(0);
            icellGZF.CellStyle = style2;
            icellGZF.SetCellValue("工资");
            for (int i = 1; i < cols; i++)
            {
                ICell icell = rowGZ.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellValue(0.00);
            }
            ICell icell2 = rowGZ.CreateCell(cols);
            icell2.CellStyle = style2;
            icell2.SetCellFormula("sum(B" + (rownum + 2) + ":" + colsName + (rownum + 2) + ")");

            IRow rowWXYJ = ws.CreateRow(rows + 3);
            ICell icellWXYJ = rowWXYJ.CreateCell(0);
            icellWXYJ.CellStyle = style2;
            icellWXYJ.SetCellValue("五险一金");
            for (int i = 1; i < cols; i++)
            {
                ICell icell = rowWXYJ.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellValue(0.00);
            }
            ICell icell3 = rowWXYJ.CreateCell(cols);
            icell3.CellStyle = style2;
            icell3.SetCellFormula("sum(B" + (rownum + 3) + ":" + colsName + (rownum + 3) + ")");
            IRow rowFLFY = ws.CreateRow(rows + 4);
            ICell icellFLFY = rowFLFY.CreateCell(0);
            icellFLFY.CellStyle = style2;
            icellFLFY.SetCellValue("福利费用");
            for (int i = 1; i < cols; i++)
            {
                ICell icell = rowFLFY.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellValue(0.00);
            }

            ICell icell4 = rowFLFY.CreateCell(cols);
            icell4.CellStyle = style2;
            icell4.SetCellFormula("sum(B" + (rownum + 4) + ":" + colsName + (rownum + 4) + ")");
            IRow rowFSDF = ws.CreateRow(rows + 5);
            ICell icellFSDF = rowFSDF.CreateCell(0);
            icellFSDF.CellStyle = style2;
            icellFSDF.SetCellValue("房水电费");
            for (int i = 1; i < cols; i++)
            {
                ICell icell = rowFSDF.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellValue(0.00);
            }

            ICell icell5 = rowFSDF.CreateCell(cols);
            icell5.CellStyle = style2;
            icell5.SetCellFormula("sum(B" + (rownum + 5) + ":" + colsName + (rownum + 5) + ")");

            IRow rowGDCB = ws.CreateRow(rows + 6);
            ICell icellGDCB = rowGDCB.CreateCell(0);
            icellGDCB.CellStyle = style2;
            icellGDCB.SetCellValue("固定成本合计");
            for (int i = 1; i < cols; i++)
            {
                string icellWord = SetWord(i + 1);
                ICell icell = rowGDCB.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellFormula("sum(" + icellWord + "5:" + icellWord + "8)");
            }
            ICell icell7 = rowGDCB.CreateCell(cols);
            icell7.CellStyle = style2;
            icell7.SetCellFormula("sum(B" + (rownum + 6) + ":" + colsName + (rownum + 6) + ")");

            IRow rowBGYD = ws.CreateRow(rows + 7);
            ICell icellBGYD = rowBGYD.CreateCell(0);
            icellBGYD.CellStyle = style2;
            icellBGYD.SetCellValue("办公邮电费");
            for (int i = 1; i < cols; i++)
            {
                ICell icell = rowBGYD.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellValue(0.00);
            }
            ICell icell6 = rowBGYD.CreateCell(cols);
            icell6.CellStyle = style2;
            icell6.SetCellFormula("sum(B" + (rownum + 7) + ":" + colsName + (rownum + 7) + ")");


            IRow rowDSST = ws.CreateRow(rows + 8);
            ICell icellDSST = rowDSST.CreateCell(0);
            icellDSST.CellStyle = style2;
            icellDSST.SetCellValue("电算晒图费");
            for (int i = 1; i < cols; i++)
            {
                ICell icell = rowDSST.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellValue(0.00);
            }
            ICell icell8 = rowDSST.CreateCell(cols);
            icell8.CellStyle = style2;
            icell8.SetCellFormula("sum(B" + (rownum + 8) + ":" + colsName + (rownum + 8) + ")");
            IRow rowHWZB = ws.CreateRow(rows + 9);
            ICell icellHWZB = rowHWZB.CreateCell(0);
            icellHWZB.CellStyle = style2;
            icellHWZB.SetCellValue("会务招标费");
            for (int i = 1; i < cols; i++)
            {
                ICell icell = rowHWZB.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellValue(0.00);
            }
            ICell icell9 = rowHWZB.CreateCell(cols);
            icell9.CellStyle = style2;
            icell9.SetCellFormula("sum(B" + (rownum + 9) + ":" + colsName + (rownum + 9) + ")");
            IRow rowCL = ws.CreateRow(rows + 10);
            ICell icellCL = rowCL.CreateCell(0);
            icellCL.CellStyle = style2;
            icellCL.SetCellValue("材料");
            for (int i = 1; i < cols; i++)
            {
                ICell icell = rowCL.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellValue(0.00);
            }
            ICell icell10 = rowCL.CreateCell(cols);
            icell10.CellStyle = style2;
            icell10.SetCellFormula("sum(B" + (rownum + 10) + ":" + colsName + (rownum + 10) + ")");
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (reader.GetValue(0).ToString().Contains("工资"))
                    {
                        for (int i = 1; i < cols; i++)
                        {
                            var icellVal = rowGZ.Cells[i].ToString();
                            rowGZ.GetCell(i).SetCellValue(Convert.ToDouble(reader.GetValue(i).ToString()) + Convert.ToDouble(icellVal));
                        }

                    }
                    else if (reader.GetValue(0).ToString() == "养老金" || reader.GetValue(0).ToString() == "公积金" || reader.GetValue(0).ToString() == "医保费")
                    {
                        for (int i = 1; i < cols; i++)
                        {
                            var icellVal = rowWXYJ.Cells[i].ToString();
                            rowWXYJ.GetCell(i).SetCellValue(Convert.ToDouble(reader.GetValue(i).ToString()) + Convert.ToDouble(icellVal));
                        }

                    }
                    //rowFLFY
                    else if (reader.GetValue(0).ToString() == "暖气费补贴" || reader.GetValue(0).ToString() == "福利费")
                    {
                        for (int i = 1; i < cols; i++)
                        {
                            var icellVal = rowFLFY.Cells[i].ToString();
                            rowFLFY.GetCell(i).SetCellValue(Convert.ToDouble(reader.GetValue(i).ToString()) + Convert.ToDouble(icellVal));
                        }
                    }
                    //rowFSDF
                    else if (reader.GetValue(0).ToString() == "房费" || reader.GetValue(0).ToString() == "水费" || reader.GetValue(0).ToString() == "电费" || reader.GetValue(0).ToString() == "物业费")
                    {
                        for (int i = 1; i < cols; i++)
                        {
                            var icellVal = rowFSDF.Cells[i].ToString();
                            rowFSDF.GetCell(i).SetCellValue(Convert.ToDouble(reader.GetValue(i).ToString()) + Convert.ToDouble(icellVal));
                        }
                    }
                    //rowBGYD
                    else if (reader.GetValue(0).ToString() == "办公费" || reader.GetValue(0).ToString() == "邮电费")
                    {
                        for (int i = 1; i < cols; i++)
                        {
                            var icellVal = rowBGYD.Cells[i].ToString();
                            rowBGYD.GetCell(i).SetCellValue(Convert.ToDouble(reader.GetValue(i).ToString()) + Convert.ToDouble(icellVal));
                        }
                    }
                    //rowDSST
                    else if (reader.GetValue(0).ToString() == "电算制图费" || reader.GetValue(0).ToString() == "晒图费")
                    {
                        for (int i = 1; i < cols; i++)
                        {
                            var icellVal = rowDSST.Cells[i].ToString();
                            rowDSST.GetCell(i).SetCellValue(Convert.ToDouble(reader.GetValue(i).ToString()) + Convert.ToDouble(icellVal));
                        }
                    }
                    //rowHWZB
                    else if (reader.GetValue(0).ToString() == "培训费" || reader.GetValue(0).ToString() == "招标及咨询费" || reader.GetValue(0).ToString() == "会务费")
                    {
                        for (int i = 1; i < cols; i++)
                        {
                            var icellVal = rowHWZB.Cells[i].ToString();
                            rowHWZB.GetCell(i).SetCellValue(Convert.ToDouble(reader.GetValue(i).ToString()) + Convert.ToDouble(icellVal));
                        }
                    }
                    //rowQT
                    else if (reader.GetValue(0).ToString() == "电算制图费")
                    {
                        for (int i = 1; i < cols; i++)
                        {
                            var icellVal = rowDSST.Cells[i].ToString();
                            rowDSST.GetCell(i).SetCellValue(Convert.ToDouble(reader.GetValue(i).ToString()) + Convert.ToDouble(icellVal));
                        }
                    }
                    //rowCL
                    else if (reader.GetValue(0).ToString() == "材料")
                    {
                        for (int i = 1; i < cols; i++)
                        {
                            var icellVal = rowCL.Cells[i].ToString();
                            rowCL.GetCell(i).SetCellValue(Convert.ToDouble(reader.GetValue(i).ToString()) + Convert.ToDouble(icellVal));
                        }
                    }
                    else
                    {
                        IRow rowELSE = ws.CreateRow(rows + 11);
                        ICell icelltitle = rowELSE.CreateCell(0);
                        icelltitle.CellStyle = style2;
                        icelltitle.SetCellValue(reader.GetValue(0).ToString());
                        for (int i = 1; i < cols; i++)
                        {
                            ICell icell = rowELSE.CreateCell(i);
                            icell.CellStyle = style2;
                            icell.SetCellValue(Convert.ToDouble(reader.GetValue(i).ToString()));
                        }
                        ICell icell11 = rowELSE.CreateCell(cols);
                        icell11.CellStyle = style2;
                        icell11.SetCellFormula("sum(B" + (rownum + 11) + ":" + colsName + (rownum + 11) + ")");
                        rownum++;
                        rows++;
                    }
                }
            }
            IRow rowBDCB = ws.CreateRow(rows + 11);
            ICell icellBDCB = rowBDCB.CreateCell(0);
            icellBDCB.CellStyle = style2;
            icellBDCB.SetCellValue("变动成本合计");
            for (int i = 1; i < cols; i++)
            {
                string icellWord = SetWord(i + 1);
                ICell icell = rowBDCB.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellFormula("sum(" + icellWord + "10:" + icellWord + (rows + 10) + ")");
            }
            ICell icell12 = rowBDCB.CreateCell(cols);
            icell12.CellStyle = style2;
            icell12.SetCellFormula("sum(B" + (rownum + 11) + ":" + colsName + (rownum + 11) + ")");
            for (int i = 1; i < cols; i++)
            {
                string icellWord = SetWord(i + 1);
                ICell icell = rowZCBH.CreateCell(i);
                icell.CellStyle = style2;
                //icell.SetCellValue(0.00);
                icell.SetCellFormula("sum(" + icellWord + "9:" + icellWord + (rows + 11) + ")");// icell.SetCellFormula("(" + icellWord + (rows + 11) + "+" + icellWord + "9)*100/" + icellWord + "3");
            }
            IRow rowBDCBB = ws.CreateRow(rows + 12);
            ICell icellBDCBB = rowBDCBB.CreateCell(0);
            icellBDCBB.CellStyle = style2;
            icellBDCBB.SetCellValue("变动成本比例");
            for (int i = 1; i < cols; i++)
            {
                string icellWord = SetWord(i + 1);
                ICell icell = rowBDCBB.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellFormula(icellWord + (rows + 12) + "*100/" + icellWord + "3");
            }
            ICell icell13 = rowBDCBB.CreateCell(cols);
            icell13.CellStyle = style2;
            icell13.SetCellFormula(colsNameA + (rownum + 11) + "*100/" + colsNameA + "3");

            IRow rowZCBB = ws.CreateRow(rows + 13);
            ICell icellZCBB = rowZCBB.CreateCell(0);
            icellZCBB.CellStyle = style2;
            icellZCBB.SetCellValue("总成本比例");
            for (int i = 1; i < cols; i++)
            {
                string icellWord = SetWord(i + 1);
                ICell icell = rowZCBB.CreateCell(i);
                icell.CellStyle = style2;
                icell.SetCellFormula("(" + icellWord + (rows + 12) + "+" + icellWord + "9)*100/" + icellWord + "3");
            }
            ICell icell14 = rowZCBB.CreateCell(cols);
            icell14.CellStyle = style2;
            icell14.SetCellFormula(colsNameA + "4*100/" + colsNameA + "3");

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(unitName + "成本明细.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        protected void btn_search_Click(object sender, EventArgs e)
        {
            bindData();
        }

        protected string SetWord(int num)
        {
            string word = "A";
            switch (num)
            {
                case 1:
                    word = "A";
                    break;
                case 2:
                    word = "B";
                    break;
                case 3:
                    word = "C";
                    break;
                case 4:
                    word = "D";
                    break;
                case 5:
                    word = "E";
                    break;
                case 6:
                    word = "F";
                    break;
                case 7:
                    word = "G";
                    break;
                case 8:
                    word = "H";
                    break;
                case 9:
                    word = "I";
                    break;
                case 10:
                    word = "J";
                    break;
                case 11:
                    word = "K";
                    break;
                case 12:
                    word = "L";
                    break;
                case 13:
                    word = "M";
                    break;
                case 14:
                    word = "N";
                    break;
                case 15:
                    word = "O";
                    break;
                case 16:
                    word = "P";
                    break;
                case 17:
                    word = "Q";
                    break;
                case 18:
                    word = "R";
                    break;
                case 19:
                    word = "S";
                    break;
                case 20:
                    word = "T";
                    break;
                case 21:
                    word = "U";
                    break;
                case 22:
                    word = "V";
                    break;
                case 23:
                    word = "W";
                    break;
                case 24:
                    word = "X";
                    break;
                case 25:
                    word = "Y";
                    break;
                case 26:
                    word = "Z";
                    break;
            }
            return word;
        }
        protected int SetNum(string num)
        {
            int word = 1;
            switch (num)
            {
                case "A":
                    word = 1;
                    break;
                case "B":
                    word = 2;
                    break;
                case "C":
                    word = 3;
                    break;
                case "D":
                    word = 4;
                    break;
                case "E":
                    word = 5;
                    break;
                case "F":
                    word = 6;
                    break;
                case "G":
                    word = 7;
                    break;
                case "H":
                    word = 8;
                    break;
                case "I":
                    word = 9;
                    break;
                case "J":
                    word = 10;
                    break;
                case "K":
                    word = 11;
                    break;
                case "L":
                    word = 12;
                    break;
                case "M":
                    word = 13;
                    break;
                case "N":
                    word = 14;
                    break;
                case "O":
                    word = 15;
                    break;
                case "P":
                    word = 16;
                    break;
                case "Q":
                    word = 17;
                    break;
                case "R":
                    word = 18;
                    break;
                case "S":
                    word = 19;
                    break;
                case "T":
                    word = 20;
                    break;
                case "U":
                    word = 21;
                    break;
                case "V":
                    word = 22;
                    break;
                case "W":
                    word = 23;
                    break;
                case "X":
                    word = 24;
                    break;
                case "Y":
                    word = 25;
                    break;
                case "Z":
                    word = 26;
                    break;
            }
            return word;
        }
        protected List<ChargeType> ReturnCharge(List<ChargeType> ListCharge)
        {
            //1、contains('工资')2、养老金+公积金+医疗3、福利费+暖气费补贴4、房.水.电.物业
            //1、办公费+邮电费2、差旅费3、电算制图费+晒图费4、汽车使用费5、购置费6、招待费用7、会务费及培训费+投标咨询费8、软件使用费9、税金10、其他11、资料费12、维修费13、取暖费14、人工费15、材料、咨询费
            List<ChargeType> list = new List<ChargeType>();
            ChargeType demoModel1 = new ChargeType();
            List<string> stringValue = new List<string>();
            foreach (var item in ListCharge)
            {
                if (item.TypeName.Contains("工资"))
                {
                    stringValue.AddRange(item.ChargeArray);
                }
            }
            demoModel1.TypeName = "工资";
            demoModel1.ChargeArray = stringValue;
            list.Add(demoModel1);
            return list;
        }
        /// <summary>
        /// SqlDataReader转成DataTable实现方法
        /// </summary>
        /// <param name="strSql">Sql语句</param>
        /// <returns></returns>
        public DataTable DataReaderToDataTable(SqlDataReader dataReader)
        {
            DataTable dtReturn = null;
            object[] value = null;
            try
            {
                if (dataReader.HasRows)
                {
                    dtReturn = CreateTableBySchemaTable(dataReader.GetSchemaTable());
                    value = new object[dataReader.FieldCount];
                    while (dataReader.Read())
                    {
                        dataReader.GetValues(value);
                        dtReturn.LoadDataRow(value, true);
                    }
                    value = null;
                }

            }
            catch (Exception err)
            { }
            finally
            {
                dataReader.Close();
            }
            return dtReturn;
        }
        public DataTable CreateTableBySchemaTable(DataTable pSchemaTable)
        {
            DataTable dtReturn = new DataTable();
            DataColumn dc = null;
            DataRow dr = null;
            for (int i = 0; i < pSchemaTable.Rows.Count; i++)
            {
                dr = pSchemaTable.Rows[i];
                dc = new DataColumn(dr["ColumnName"].ToString(), dr["DataType"] as Type);
                dtReturn.Columns.Add(dc);
            }
            dr = null;
            dc = null;
            return dtReturn;
        }
    }
    public class ChargeType
    {
        private string typeName;

        public string TypeName
        {
            get { return typeName; }
            set { typeName = value; }
        }
        private List<string> chargeArray;

        public List<string> ChargeArray
        {
            get { return chargeArray; }
            set { chargeArray = value; }
        }
    }
}