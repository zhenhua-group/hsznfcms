﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text;
using System.Data;

namespace TG.Web.ProjectValueandAllot
{
    public partial class TranProjectValueAllotBymaster : PageBase
    {    /// <summary>
        /// 项目系统编号
        /// </summary>
        public int proSysNo
        {
            get
            {
                int proSysNo = 0;
                int.TryParse(Request["proid"], out proSysNo);
                return proSysNo;
            }
        }

        public decimal TotalCount
        {
            get
            {
                int totalCount = 0;
                int.TryParse(Request["totalCount"], out totalCount);
                return totalCount;
            }
        }

        public string Type
        {
            get
            {
                return Request["type"];
            }
        }



        public decimal allotID
        {
            get
            {
                int allotID = 0;
                int.TryParse(Request["allotID"], out allotID);
                return allotID;
            }
        }

        /// <summary>
        /// 是否四舍五入
        /// </summary>
        public string IsRounding = ConfigurationManager.AppSettings["IsRounding"] ?? "0";

        /// <summary>
        /// tg中项目
        /// </summary>
        public int ProReferenceSysNo { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                //获取项目信息
                getProInfo();

                getMemberValue();
            }
        }
        /// <summary>
        /// 项目信息
        /// </summary>
        private void getProInfo()
        {
            if (Type == "nts")
            {
                lblTitle.Text = "土建转暖通产值分配";
                Label1.Text = "土建转暖通产值分配";
                lblTranTitle.Text = "转暖通所金额";
            }
            else
            {
                lblTitle.Text = "转经济所产值分配";
                Label1.Text = "转经济所产值分配";
                lblTranTitle.Text = "转经济所金额";
            }
            //显示项目信息
            TG.BLL.cm_Project pro = new TG.BLL.cm_Project();
            TG.Model.cm_Project pro_model = pro.GetModel(proSysNo);

            if (pro_model != null)
            {
                //项目名称
                lblProjectName.Text = pro_model.pro_name;
                //关联合同
                this.lblReletive.Text = pro_model.Project_reletive;
                //管理级别
                string level = pro_model.pro_level.ToString();
                if (level.Trim() == "0")
                {
                    this.lbl_level.Text = "院管";
                }
                else
                {
                    this.lbl_level.Text = "所管";
                }

                //审核级别
                string auditlevel = pro_model.AuditLevel == null ? "0,0" : pro_model.AuditLevel.Trim();
                if (auditlevel.Trim() == "1,0")
                {
                    this.lbl_auditlevel.Text = "院审";
                }
                else if (auditlevel.Trim() == "0,1")
                {
                    this.lbl_auditlevel.Text = "所审";
                }
                else if (auditlevel.Trim() == "1,1")
                {
                    this.lbl_auditlevel.Text = "院审,所审";
                }

                //建设规模
                this.lblScale.Text = pro_model.ProjectScale.ToString();
                //建筑类别
                lblBuildType.Text = pro_model.BuildType;
                //项目总负责
                this.lblPMName.Text = pro_model.PMName;
                //承接部门
                lblcpr_Unit.Text = pro_model.Unit;

                //项目阶段
                string str_jd = pro_model.pro_status;
                if (str_jd.IndexOf(',') > -1)
                {
                    this.lbl_purpose.Text = pro_model.pro_status.Remove(pro_model.pro_status.LastIndexOf(','));
                }

                ProReferenceSysNo = pro_model.ReferenceSysNo;

                //合同额
                lblCoperationAmount.Text = pro_model.Cpr_Acount.ToString();
                //经济所
                string isotherprt = "";
                if (pro_model.ISTrunEconomy == "1")
                {
                    isotherprt += "经济所,";
                }
                //暖通
                if (pro_model.ISHvac == "1")
                {
                    isotherprt += "暖通热力所,";
                }
                //土建所
                if (pro_model.ISArch == "1")
                {
                    isotherprt += "土建所,";
                }
                if (isotherprt.IndexOf(',') > -1)
                {
                    this.lbl_isotherprt.Text = isotherprt.Remove(isotherprt.LastIndexOf(','));
                }
                else
                {
                    this.lbl_isotherprt.Text = "无";
                }
                lblTotalCount.Text = TotalCount.ToString();

            }

        }


        /// <summary>
        /// 得到人员产值
        /// </summary>
        /// <returns></returns>
        private void getMemberValue()
        {
            TG.BLL.cm_TranProjectValueAllot BLL = new TG.BLL.cm_TranProjectValueAllot();
            DataTable dtMember = new DataTable();

            if (Type == "nts")
            {
                dtMember = BLL.GetPlanMember("暖通", ProReferenceSysNo).Tables[0];
            }
            else
            {
                dtMember = BLL.GetPlanMember("预算", ProReferenceSysNo).Tables[0];
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(" <div id=\"tbProjectValueBymember\" class=\"cls_Container_Report\" >");
            sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"><tr> ");
            if (Type == "nts")
            {
                sb.Append("<td style=\"text-align: center;\"> 暖通策划人员产值分配比例% &nbsp;(<span style=\"color:red\">注释：所有人员的比例之和要为100%（包括设计、专业负责、审核、校对）</span>) </td> </tr>");
            }
            else
            {
                sb.Append("<td style=\"text-align: center;\"> 预算专业策划人员产值分配比例% </td> </tr>");
            }
            sb.Append("</table>");
            sb.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sb.Append("<tr ><td style=\"width: 10%; text-align: center;height:15px;\">  名称</td><td style=\"width: 10%; text-align: center;\"> 人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sb.Append(" <tr> <td style=\"width: 10%; text-align: center;\" > 专业 </td><td style=\"width: 10%; text-align: center;\">   姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
            sb.Append(" </table>");
            sb.Append("<table id=\"gvProjectValueBymember\" width=\"98%\" memMoney=\"gvProjectValueBymember\" class=\" cls_ProjAllot_Table cls_ProjAllot_Input2 cls_ProjAllot_label2\">");
            if (dtMember.Rows.Count > 0)
            {
                sb.Append("<tr>");
                if (Type == "nts")
                {
                    sb.Append("<td  width= \"10%\" class=\"cls_Column\" rowspan=\"" + dtMember.Rows.Count + "\">暖通</td>");
                }
                else
                {
                    sb.Append("<td  width= \"10%\" class=\"cls_Column\" rowspan=\"" + dtMember.Rows.Count + "\">预算</td>");
                }
                for (int j = 0; j < dtMember.Rows.Count; j++)
                {
                    if (j != 0)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td class=\"display\"></td>");
                    }
                    string name = dtMember.Rows[j]["mem_Name"].ToString();

                    sb.Append("<td class=\"display\" wp=\"0\">" + dtMember.Rows[j]["mem_ID"].ToString() + "</td>");
                    sb.Append("<td width= \"10%\">" + dtMember.Rows[j]["mem_Name"].ToString() + "</td>");

                    //该人员是否是设计人员
                    int designCount = new DataView(dtMember) { RowFilter = " mem_Name='" + name + "' AND mem_isDesigner='1'" }.ToTable().Rows.Count;


                    if (designCount <= 0)
                    {
                        sb.Append("<td width= \"10%\"> 0%</td>");
                        sb.Append("<td width= \"10%\">0</td>");
                    }
                    else
                    {
                        sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" runat=\"server\" sz=" + dtMember.Rows[j]["mem_Principalship_ID"].ToString() + " />%</td>");
                        sb.Append("<td width= \"10%\"></td>");
                    }

                    sb.Append("<td class=\"display\" >0</td>");

                    //该人员是否是专业负责
                    int headCount = new DataView(dtMember) { RowFilter = "  mem_Name='" + name + "' AND mem_RoleIDs like '%2%'" }.ToTable().Rows.Count;
                    if (headCount <= 0)
                    {
                        sb.Append("<td width= \"10%\"> 0%</td>");
                        sb.Append("<td width= \"10%\">0</td>");
                    }
                    else
                    {
                        sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" />%</td>");
                        sb.Append("<td width= \"10%\"></td>");
                    }
                    sb.Append("<td class=\"display\" >0</td>");

                    //该人员是否审核
                    int auditCount = new DataView(dtMember) { RowFilter = "   mem_Name='" + name + "' AND mem_RoleIDs like '%4%'" }.ToTable().Rows.Count;
                    if (auditCount <= 0)
                    {
                        sb.Append("<td width= \"10%\"> 0%</td>");
                        sb.Append("<td width= \"10%\">0</td>");
                    }
                    else
                    {
                        sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" />%</td>");
                        sb.Append("<td width= \"10%\"></td>");
                    }

                    sb.Append("<td class=\"display\" >0</td>");
                    //该人员是否是校对
                    int jdCount = new DataView(dtMember) { RowFilter = "  mem_Name='" + name + "' AND mem_RoleIDs like '%3%'" }.ToTable().Rows.Count;
                    if (jdCount <= 0)
                    {
                        sb.Append("<td width= \"10%\">0% </td>");
                        sb.Append("<td width= \"10%\">0</td>");
                    }
                    else
                    {
                        sb.Append("<td width= \"10%\">  <input  maxlength=\"15\" type=\"text\"   runat=\"server\" />%</td>");
                        sb.Append("<td width= \"10%\"></td>");
                    }
                    sb.Append("<td class=\"display\" >0</td>");
                    sb.Append("<td class=\"display\" ></td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
            }
            sb.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\" id=\"tbChooseUser\"><tr> ");
            sb.Append("<td><span id=\"chooseUser\"data-toggle=\"modal\" href=\"#chooseUserMainDiv\" style=\"color: Blue; cursor: pointer;\">添加人员</span>&nbsp;&nbsp;<span id=\"chooseExternalUser\"  data-toggle=\"modal\" href=\"#chooseExtUserDiv\" style=\"color: Blue; cursor: pointer;\">添加外聘人员</span></td>");
            sb.Append("</tr></table>");
            sb.Append(" </div>");

            lblMember.Text = sb.ToString();
        }

        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
    }
}