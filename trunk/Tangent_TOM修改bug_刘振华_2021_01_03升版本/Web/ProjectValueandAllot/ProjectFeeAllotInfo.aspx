﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectFeeAllotInfo.aspx.cs"
    Inherits="TG.Web.ProjectFeeAllot" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjectFee.css" rel="stylesheet" type="text/css" />
    <link href="../css/ProjStructCss3.css" rel="stylesheet" type="text/css" />

    <script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="../../js/wdate/WdatePicker.js"></script>

</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
        <div style="width: 100%;">
            <!--PartOne-->
            <div style="width: 100%;" id="partOneDivContainer">
                <table class="cls_container">
                    <tr>
                        <td class="cls_head">当前位置：[项目立项]-[设计项目费用表]
                        </td>
                    </tr>
                </table>
                <table style="width: 100%;" id="cls_project">
                    <tr>
                        <td colspan="9" align="center">
                            <div class="cls_cell_header">
                                设计项目费用分配统计总表<input id="partTwoSingelspan" type="text" style="width: 80px;" disabled="disabled"
                                    value="">
                            </div>
                        </td>
                        <td>
                            <div class="cls_cell_year" id="sp_year">
                                &nbsp;
                            </div>
                        </td>
                        <td>
                            <div class="cls_cell_jibie" id="sp_jibie">
                                &nbsp;
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">项目经理人</td>
                        <td align="center">
                            <asp:Label ID="txtProPerson" runat="server" Width="60px"></asp:Label>
                        </td>
                        <td align="center">建设单位</td>
                        <td align="left" colspan="5">
                            <asp:Label ID="txtBuildUnit" runat="server" Width="263px"></asp:Label>
                        </td>
                        <td align="center">出图日期
                        </td>
                        <td align="center" colspan="2">
                            <input type="text" id="txtdate" width="120px" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" size="20" value="<%=PartOneEntity.InDate.ToString("yyyy-MM-dd") %>"
                                style="width: 120px;" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="7">&nbsp;</td>
                        <td align="center">&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="11">
                            <div class="cls_cell_header2">
                                项&nbsp;目&nbsp;信&nbsp;息&nbsp;表<span style="font-size: 8pt;">(本表由经营部填写)</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">工程名称
                        </td>
                        <td colspan="7">
                            <asp:Label ID="txtprojectname" runat="server" Width="420px"></asp:Label>
                        </td>
                        <td align="center">合同编号
                        </td>
                        <td colspan="2">
                            <asp:Label ID="txtcprNo" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">楼号
                        </td>
                        <td align="center">住宅楼
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="center">合计
                        </td>
                        <td align="center">设计编号
                        </td>
                        <td colspan="2">
                            <asp:Label ID="txtdesignNo" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">建筑面积
                        </td>
                        <td align="center">
                            <asp:Label ID="partOneBuildAreatext" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td align="center">&nbsp;
                        <asp:Label ID="partOneSumtext" runat="server" Width="60px"></asp:Label>
                        </td>
                        <td>&nbsp;
                        </td>
                        <td align="center">(元/㎡)
                        </td>
                        <td align="center">&nbsp;(元)
                        </td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;合同收费<br />
                            (万元)
                        </td>
                        <td align="center">
                            <input id="partOnecprCharge" type="text" style="width: 80px; border: none; background-color: #f0f0f0;"
                                value="<%=PartOneEntity ==null?"":PartOneEntity.CprCharge.ToString() %>" readonly="readonly" />
                        </td>
                        <td align="center">本次收费<br />
                            (万元)
                        </td>
                        <td align="center">
                            <input id="partOneCurChangetext" type="text" style="width: 70px; height: 18px;" value="<%=PartOneEntity ==null?"":PartOneEntity.CurChange.ToString() %>"
                                class="valider" />
                        </td>
                        <td align="center">&nbsp; 建筑规模
                        </td>
                        <td align="center">
                            <asp:Label ID="lbscale" runat="server" Width="60px"></asp:Label>
                        </td>
                        <td align="center">结构形式
                        </td>
                        <td align="center">
                            <asp:DropDownList ID="drp_strutype" runat="server" CssClass="TextBoxBorder"
                                Width="80px" AppendDataBoundItems="True">
                                <asp:ListItem Value="-1">--请选择--</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lbl_structype" runat="server" Visible="False"></asp:Label>
                        </td>
                        <td align="center">最低收费标准<br />
                            分 成 基 数&nbsp;
                        </td>
                        <td align="center">
                            <input id="partOneLowDivUnitAreatext" type="text" style="width: 70px;" value="<%=PartOneEntity ==null?"":PartOneEntity.LowDivUnitArea.ToString() %>"
                                class="valider" />
                        </td>
                        <td align="center">
                            <input id="partOneLowDivUnitPricetext" disabled="disabled" type="text" style="width: 70px; border: none; background-color: #f0f0f0;"
                                value="<%=PartOneEntity ==null?"":PartOneEntity.LowDivUnitPrice.ToString() %>" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;合同单价<br />
                            (元/㎡)
                        </td>
                        <td align="center">
                            <input id="partOnecprunitpricetext" type="text" style="width: 70px; height: 18px;"
                                value="<%=PartOneEntity ==null?"":PartOneEntity.CprUnitprice.ToString() %>" class="valider" />
                            <span id="sp_partOnecprunitpricetext" style="width: 70px; height: 18px; display: none;"><%=PartOneEntity ==null?"":PartOneEntity.CprUnitprice.ToString() %></span>
                        </td>
                        <td align="center">本次费率
                        </td>
                        <td align="center">
                            <input id="partOneCurRatetext" type="text" style="width: 70px; border: none; background-color: #f0f0f0;"
                                readonly value="<%=PartOneEntity ==null?"":PartOneEntity.CurRate.ToString() %>" />%
                        </td>
                        <td align="center">&nbsp; 工程等级
                        </td>
                        <td align="center">
                            <asp:Label ID="txtprorate" runat="server" Width="60px"></asp:Label>
                        </td>
                        <td align="center">税率
                        </td>
                        <td align="center">
                            <input id="partOneTaxRatetext" type="text" style="width: 70px; height: 18px;" value="<%=PartOneEntity ==null?"":PartOneEntity.TaxRate.ToString() %>"
                                class="valider" />%
                        </td>
                        <td align="center">最低收费标准<br />
                            税后分成基数
                        </td>
                        <td align="center">
                            <input id="partOneLowRateUnitAreatext" disabled="disabled" type="text" style="width: 70px; border: none; background-color: #f0f0f0;"
                                readonly value="<%=PartOneEntity ==null?"":PartOneEntity.LowRateUnitArea.ToString() %>" />
                        </td>
                        <td align="center">
                            <input id="partOneLowRateUnitPricetext" disabled="disabled" type="text" style="width: 70px; border: none; background-color: #f0f0f0;"
                                readonly="readonly" value="<%=PartOneEntity ==null?"":PartOneEntity.LowRateUntiPrice.ToString()%>" />
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="11">
                            <input id="partOneSaveButton" type="button" value="保存" style="width: 48px; height: 20px; border: none; background-image: url(../images/buttons/btn_bg_image.gif)">
                        </td>
                    </tr>
                </table>
            </div>
            <!--PartTwo-->
            <div style="width: 100%; display: none;" id="partTwoDivContainer">
                <table width="100%" id="cls_college">
                    <tr>
                        <td align="center" colspan="11">
                            <div class="cls_cell_header2">
                                各项费用控制明细表<span style="font-size: 8pt;">(本表为财务提供支付控制)</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">&nbsp;
                        </td>
                        <td align="center">税后单项<br />
                            (低限)
                        </td>
                        <td align="center">院务管理<br />
                            (含超)
                        </td>
                        <td align="center">项目费用<br />
                            (含超)
                        </td>
                        <td align="center">项目负责
                        </td>
                        <td align="center">注册师<br />
                            A
                        </td>
                        <td align="center">设校人员<br />
                            B C
                        </td>
                        <td align="center">校审核定<br />
                            D E
                        </td>
                        <td align="center">总工
                        </td>
                        <td align="center">院长
                        </td>
                        <td align="center">合计
                        </td>
                    </tr>
                    <tr class="cls_tr_text">
                        <td align="center">本次发放比例
                        </td>
                        <td>
                            <input id="partTwoTaxSigneltext" type="text" style="border: solid 1px #CCC; background-color: White;"
                                value="<%=PartTwoEntity ==null?"":PartTwoEntity.TaxSignel.ToString() %>" />%
                        </td>
                        <td align="center">
                            <input id="partTwoCollegeTaxtext" type="text" value="<%=CostCompanyConfig ==null?"":CostCompanyConfig.CmpMngPrt.ToString() %>"
                                readonly="readonly" />%
                        </td>
                        <td>
                            <input id="partTwoProjectTaxtext" type="text" readonly="readonly" value="<%=CostCompanyConfig ==null?"":CostCompanyConfig.ProCostPrt.ToString() %>" />%
                        </td>
                        <td align="center">
                            <input id="partTwoProChgTaxtext" type="text" readonly="readonly" value="<%=CostCompanyConfig ==null?"":CostCompanyConfig.ProChgPrt.ToString() %>" />%
                        </td>
                        <td>
                            <input id="partTwoRegisterTaxtext" type="text" readonly="readonly" value="<%=CostCompanyConfig ==null?"":CostCompanyConfig.RegUserPrt.ToString() %>" />%
                        </td>
                        <td align="center">
                            <input id="partTwoShexTaxtext" type="text" readonly="readonly" value="<%=CostCompanyConfig ==null?"":CostCompanyConfig.DesignPrt.ToString() %>" />%
                        </td>
                        <td>
                            <input id="partTwoShedTaxtext" type="text" readonly="readonly" value="<%=CostCompanyConfig ==null?"":CostCompanyConfig.AuditPrt.ToString() %>" />%
                        </td>
                        <td align="center">
                            <input id="partTwoZonggongTaxtext" type="text" readonly="readonly" value="<%=CostCompanyConfig ==null?"":CostCompanyConfig.TotalUser.ToString() %>" />%
                        </td>
                        <td>
                            <input id="partTwoYuanzTaxtext" type="text" readonly="readonly" value="<%=CostCompanyConfig ==null?"":CostCompanyConfig.CollegeUser.ToString() %>" />%
                        </td>
                        <td>
                            <input id="partTwoSumTaxtext" disabled="disabled" type="text" readonly="readonly"
                                value="<%=PartTwoEntity ==null?"":PartTwoEntity.SumTax.ToString() %>" />%
                        </td>
                    </tr>
                    <tr class="cls_tr_text">
                        <td align="center">分成税后金额(元)
                        </td>
                        <td>
                            <input id="partTwoTaxSignelRatetext" type="text" disabled="disabled" value="<%=PartTwoEntity ==null?"":PartTwoEntity.TaxSignelRate.ToString() %>" />
                        </td>
                        <td align="center">
                            <input id="partTwoCollegeRatetext" type="text" disabled="disabled" class="" value="<%=PartTwoEntity ==null?"":PartTwoEntity.CollegeRate.ToString() %>" />
                        </td>
                        <td>
                            <input id="partTwoProjectRatetext" type="text" disabled="disabled" value=" <%=PartTwoEntity ==null?"":PartTwoEntity.ProjectRate.ToString() %>" />
                        </td>
                        <td align="center">
                            <input id="partTwoProChgRatetext" type="text" disabled="disabled" value="<%=PartTwoEntity ==null?"":PartTwoEntity.ProChgRate.ToString() %>" />
                        </td>
                        <td>
                            <input id="partTwoRegisterRatetext" type="text" disabled="disabled" value="<%=PartTwoEntity ==null?"":PartTwoEntity.RegisterRate.ToString() %>" />
                        </td>
                        <td align="center">
                            <input id="partTwoShexRatetext" type="text" disabled="disabled" value="<%=PartTwoEntity ==null?"":PartTwoEntity.ShexRate.ToString() %>" />
                        </td>
                        <td>
                            <input id="partTwoShedRatetext" type="text" disabled="disabled" value="<%=PartTwoEntity ==null?"":PartTwoEntity.ShedRate.ToString() %>" />
                        </td>
                        <td align="center">
                            <input id="partTwoZonggongRatetext" type="text" disabled="disabled" value="<%=PartTwoEntity ==null?"":PartTwoEntity.ZonggongRate.ToString() %>" />
                        </td>
                        <td>
                            <input id="partTwoYuanzRatetext" type="text" disabled="disabled" value="<%=PartTwoEntity ==null?"":PartTwoEntity.YuanzRate.ToString() %>" />
                        </td>
                        <td>
                            <input id="partTwoSumRatetext" type="text" disabled="disabled" value="<%=PartTwoEntity ==null?"":PartTwoEntity.SumRate.ToString() %>" />
                        </td>
                    </tr>
                    <tr class="cls_tr_text_center">
                        <td align="center">人&nbsp; 员
                        </td>
                        <td align="center">
                            <input id="partTwoTaxSignelUsertext" type="text" value="<%=PartTwoEntity ==null?"":PartTwoEntity.TaxSignelUser.ToString() %>"
                                readonly="readonly" style="border: none; background-color: #f0f0f0" />
                        </td>
                        <td align="center">
                            <input id="partTwoCollegeUsertext" type="text" value="<%=PartTwoEntity ==null?"":PartTwoEntity.CollegeUser.ToString() %>" />
                        </td>
                        <td align="center">
                            <input id="partTwoProjectUsertext" type="text" value="<%=PartTwoEntity ==null?"":PartTwoEntity.ProjectUser.ToString() %>" />
                        </td>
                        <td align="center">
                            <input id="partTwoProChgUsertext" type="text" value="<%=PartTwoEntity ==null?"":PartTwoEntity.ProChgUser.ToString() %>" />
                        </td>
                        <td align="center">
                            <input id="partTwoRegisterUsertext" type="text" value="<%=PartTwoEntity ==null?"":PartTwoEntity.RegisterUser.ToString() %>"
                                readonly="readonly" style="border: none; background-color: #f0f0f0" />
                        </td>
                        <td align="center">
                            <input id="partTwoShexUsertext" type="text" value="<%=PartTwoEntity ==null?"":PartTwoEntity.ShexUser.ToString() %>"
                                readonly="readonly" style="border: none; background-color: #f0f0f0" />
                        </td>
                        <td align="center">
                            <input id="partTwoShedUsertext" type="text" value="<%=PartTwoEntity ==null?"":PartTwoEntity.ShedUser.ToString() %>"
                                readonly="readonly" style="border: none; background-color: #f0f0f0" />
                        </td>
                        <td align="center">
                            <input id="partTwoZonggongUsertext" type="text" value="<%=PartTwoEntity ==null?"":PartTwoEntity.ZonggongUser.ToString() %>" />
                        </td>
                        <td align="center">
                            <input id="partTwoYuanzUsertext" type="text" value="<%=PartTwoEntity ==null?"":PartTwoEntity.YuanzUser.ToString() %>" />
                        </td>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr class="cls_tr_text2">
                        <td align="center" colspan="11">
                            <input type="button" id="partTwoSaveButton" value="保存" style="width: 48px; height: 20px; border: none; background-image: url(../images/buttons/btn_bg_image.gif)" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <!--HiddenArea-->
        <input type="hidden" id="HiddenProjectSysNo" value="<%=ProjectSysNo %>" />
        <input type="hidden" id="HiddenProcessType" value="<%=ProcessType %>" />
        <input type="hidden" id="HiddenPartOneSysNo" value="<%=PartOneSysNo %>" />
        <input type="hidden" id="HiddenHasPower" value="<%=HasPower %>" />
        <input type="hidden" id="HiddenPartOneStatus" value="<%=PartOneEntity ==null?"":PartOneEntity.Status %>" />
        <input type="hidden" id="HiddenCoprationSysNo" value="<%=CoperationSysNo %>" />
        <input type="hidden" id="HiddenIsAllot" value="<%=IsAllot %>" />
        <asp:HiddenField ID="HiddenSubAllot" runat="server" Value="0" />
    </form>
    <!--Sciprt Area-->

    <script src="../js/jquery-1.8.0.min.js" type="text/javascript"></script>

    <script src="/js/Global.js" type="text/javascript"></script>

    <script src="/js/ProjectValueandAllot/ProjectAllot.js" type="text/javascript"></script>

    <script src="/js/ProjectValueandAllot/ProjectAllotDetail.js" type="text/javascript"></script>

    <script src="../js/jquery.alerts.js" type="text/javascript"></script>

    <script type="text/javascript">
        var projectAllotDetail = new ProjectAllotDetail();
    </script>

</body>
</html>
