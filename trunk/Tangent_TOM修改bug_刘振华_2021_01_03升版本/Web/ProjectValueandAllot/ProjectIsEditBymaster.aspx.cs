﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ProjectIsEditBymaster : PageBase
    {   /// <summary>
        /// 审核记录系统编号
        /// </summary>
        public int ValueAllotAuditSysNo
        {
            get
            {
                int valueAllotAuditSysNo = 0;
                int.TryParse(Request["ValueAllotAuditSysNo"], out valueAllotAuditSysNo);
                return valueAllotAuditSysNo;
            }
        }
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }
        public TG.Model.cm_Project ProObj { get; set; }
        TG.BLL.cm_Project probll = new TG.BLL.cm_Project();
        protected void Page_Load(object sender, EventArgs e)
        {
            string sqlAudit = @"SELECT ProjectSysNo FROM cm_ProjectVolltAuditEdit WHERE SysNo=" + ValueAllotAuditSysNo;
            object obj = TG.DBUtility.DbHelperSQL.GetSingle(sqlAudit);
            string sqlAudit2 = @"SELECT Status FROM cm_ProjectVolltAuditEdit WHERE SysNo=" + ValueAllotAuditSysNo;
            object obj2 = TG.DBUtility.DbHelperSQL.GetSingle(sqlAudit2);
            if (obj != null)
            {
                int projectNo = Convert.ToInt32(obj);
                ProObj = probll.GetModel(projectNo);
                this.proname.Text = ProObj.pro_name == null ? "" : ProObj.pro_name.Trim();
                this.CJBM.Text = ProObj.Unit == null ? "" : ProObj.Unit.Trim();
                string sqlwhyAudit = @"SELECT options FROM cm_ProjectVolltAuditEdit WHERE SysNo=" + ValueAllotAuditSysNo;
                object objwhy = TG.DBUtility.DbHelperSQL.GetSingle(sqlwhyAudit);
                this.lbl_Why.Text = objwhy == null ? "" : objwhy.ToString();
            }
            if (obj2 != null)
            {
                if (obj2.ToString() == "C" || obj2.ToString() == "B")
                {
                    Response.Write("<script>alert('消息已审批！'); window.location.href = \"/Coperation/cpr_SysMsgListViewBymaster.aspx\";</script>");
                }
            }
        }
        protected void sureButton_Click(object sender, EventArgs e)
        {
            string sqlAudit = @"SELECT InUser FROM cm_ProjectVolltAuditEdit WHERE SysNo=" + ValueAllotAuditSysNo;
            object obj = TG.DBUtility.DbHelperSQL.GetSingle(sqlAudit);
            if (chk_yes.Checked)
            {
                //是
                //发送消息给审核发起人
                //修改可编辑
                if (ProObj != null && obj != null)
                {
                    string upsql = @"UPDATE cm_Coperation SET IsParamterEdit=1 WHERE cpr_Id=" + ProObj.CoperationSysNo;
                    int queryresult = TG.DBUtility.DbHelperSQL.ExecuteSql(upsql);
                    if (queryresult > 0)
                    {
                        //成功的消息发送生成。
                        SysMessageViewEntity sysMessageDataEntity = new SysMessageViewEntity
                        {
                            //ReferenceSysNo = identitySysNo.ToString(),
                            ReferenceSysNo = string.Format("Reson={0}&Proid={1}&sysno={2}", "", ProObj.bs_project_Id, ValueAllotAuditSysNo),
                            FromUser = Convert.ToInt32(obj),
                            MsgType = 17,
                            InUser = UserSysNo,
                            ToRole = "0",
                            MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", ProObj.pro_name, "产值分配系数设置通过"),
                            QueryCondition = "ddddd",
                            IsDone = "B",
                            Status = "A"
                        };
                        new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageDataEntity);
                        string upsql2 = @"UPDATE cm_ProjectVolltAuditEdit SET Status='C' WHERE SysNo=" + ValueAllotAuditSysNo;
                        int queryresult2 = TG.DBUtility.DbHelperSQL.ExecuteSql(upsql2);
                        new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatusByID(MessageID);
                        Response.Write("<script>alert('消息已通过，已发给申请人'); window.location.href =\"/Coperation/cpr_SysMsgListViewBymaster.aspx\";</script>");
                    }
                    else
                    {
                        //alert('系统错误')
                    }
                }
            }
            else
            {
                //否
                //成功的消息发送生成。
                SysMessageViewEntity sysMessageDataEntity = new SysMessageViewEntity
                {
                    //ReferenceSysNo = identitySysNo.ToString(),
                    ReferenceSysNo = string.Format("Reson={0}&Proid={1}&sysno={2}", this.txt_suozhang.Text, ProObj.bs_project_Id, ValueAllotAuditSysNo),
                    FromUser = Convert.ToInt32(obj),
                    MsgType = 17,
                    InUser = UserSysNo,
                    ToRole = "0",
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", ProObj.pro_name, "系数设置不通过"),
                    QueryCondition = "ddddd",
                    IsDone = "B",
                    Status = "A"
                };
                new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageDataEntity);
                string upsql2 = @"UPDATE cm_ProjectVolltAuditEdit SET Status='B' WHERE SysNo=" + ValueAllotAuditSysNo;
                int queryresult2 = TG.DBUtility.DbHelperSQL.ExecuteSql(upsql2);
                new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatusByID(MessageID);
                Response.Write("<script>alert('消息不通过，已发给申请人'); window.location.href = \"/Coperation/cpr_SysMsgListViewBymaster.aspx\";</script>");
            }
        }
    }
}