﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace TG.Web.ProjectValueandAllot
{
    public partial class ShowUserValueDatail : PageBase
    {

        #region QueryString
        /// <summary>
        /// 合同系统编号
        /// </summary>
        public int CprID
        {
            get
            {
                int CprID = 0;
                int.TryParse(Request["proID"], out CprID);
                return CprID;
            }
        }

        /// <summary>
        /// 审核记录系统编号
        /// </summary>
        public int AllotID
        {
            get
            {
                int AllotID = 0;
                int.TryParse(Request["AllotID"], out AllotID);
                return AllotID;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindProInfo();
                bindDatail();
            }
        }

        /// <summary>
        /// 绑定项目名称
        /// </summary>
        private void bindProInfo()
        {
            TG.Model.cm_Project com = new Model.cm_Project();
            TG.BLL.cm_Project bll = new BLL.cm_Project();
            com = bll.GetModel(CprID);
            if (com != null)
            {
                lblCprName.Text = com.pro_name;
            }
        }

        /// <summary>
        /// 绑定分配明细
        /// </summary>
        private void bindDatail()
        {
            TG.BLL.cm_ProjectValueAuditRecord BLL = new TG.BLL.cm_ProjectValueAuditRecord();

            DataTable dtData = BLL.GetUserValueByMemberAcount(AllotID, UserSysNo).Tables[0];

            var processOne = new DataView(dtData) { RowFilter = "ItemType='0'" }.ToTable().Rows.Count;
            var processTwo = new DataView(dtData) { RowFilter = "ItemType='1'" }.ToTable().Rows.Count;
            var processThree = new DataView(dtData) { RowFilter = "ItemType='2'" }.ToTable().Rows.Count;
            var processFour = new DataView(dtData) { RowFilter = "ItemType='3'" }.ToTable().Rows.Count;

            //专业 表头
            StringBuilder sbHead = new StringBuilder();

            if (processOne <= 0 && processTwo <=0 && processThree <=0 && processFour <= 0)
            {
                lbl_Detail.Text = sbHead.Append(DetailMember(dtData, 4)).ToString();
            }
            else
            {
                sbHead.Append(" <div id=\"tabsMemAmount\">  <ul style=\"width:98%; margin:auto;\"> ");
                if (processOne > 0)
                {
                    sbHead.Append("<li><div class=\"cls_1\"> <a href=\"#tabsMemAmount-1\" style=\"color: Black; font-size: 12px; font-weight: normal;\">方案设计</a>  </div> </li>");
                }
                if (processTwo > 0)
                {
                    sbHead.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-2\" style=\"color: Black; font-size: 12px; font-weight: normal;\">初步设计</a></div>  </li>");
                }
                if (processThree > 0)
                {
                    sbHead.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-3\" style=\"color: Black; font-size: 12px; font-weight: normal;\">施工图</a></div>  </li>");
                }
                if (processFour > 0)
                {
                    sbHead.Append("<li> <div class=\"cls_1\">  <a href=\"#tabsMemAmount-4\" style=\"color: Black; font-size: 12px; font-weight: normal;\">后期服务</a></div>  </li>");
                }
                sbHead.Append(" </ul>");
                if (processOne > 0)
                {
                    sbHead.Append(" <div id=\"tabsMemAmount-1\">");
                    sbHead.Append(DetailMember(dtData, 0));
                    sbHead.Append(" </div>");
                }
                if (processTwo > 0)
                {
                    sbHead.Append(" <div id=\"tabsMemAmount-2\">");
                    sbHead.Append(DetailMember(dtData, 1));
                    sbHead.Append(" </div>");
                }
                if (processThree > 0)
                {
                    sbHead.Append(" <div id=\"tabsMemAmount-3\">");
                    sbHead.Append(DetailMember(dtData, 2));
                    sbHead.Append(" </div>");
                }
                if (processFour > 0)
                {
                    sbHead.Append(" <div id=\"tabsMemAmount-4\">");
                    sbHead.Append(DetailMember(dtData, 3));
                    sbHead.Append(" </div>");
                }
                sbHead.Append(" </div>");

                lbl_Detail.Text = sbHead.ToString();
            }
        }

        /// <summary>
        /// 人员明细
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private string DetailMember(DataTable dt, int itemType)
        {
            DataTable dtData = new DataTable();
            if (itemType > 3)
            {
                dtData = dt;
            }
            else
            {
                dtData = new DataView(dt) { RowFilter = "ItemType=" + itemType + "" }.ToTable();
            }
            //专业 表头
            StringBuilder sbHead = new StringBuilder();
            sbHead.Append(" <div  class=\"cls_Container_Report\" >");
            sbHead.Append(" <table class=\"cls_ProjAllot_Table\" style=\"width: 98%;\"> <tr> <td> 项目策划人员产值分配比例% </td> </tr> </table>");
            sbHead.Append(" <table class=\"cls_ProjAllotHead\" style=\" width: 98%;\">");
            sbHead.Append("<tr ><td style=\"width: 20%; text-align: center;\">  人员 </td> <td colspan=\"2\" style=\"width: 20%; text-align: center;\"  > 设计人 </td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 专业负责</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 审核人</td> <td style=\"width: 20%; text-align: center;\" colspan=\"2\"> 校对人</td> </tr> ");
            sbHead.Append(" <tr > <td style=\"width: 20%; text-align: center;\" >  姓名 </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\"> 比例 </td> <td style=\"width: 10%; text-align: center;\">金额(元) </td>  <td style=\"width: 10%; text-align: center;\">比例 </td> <td style=\"width: 10%; text-align: center;\"> 金额(元) </td> <td style=\"width: 10%; text-align: center;\">比例</td><td style=\"width: 10%; text-align: center;\">金额(元) </td> </tr>");
            sbHead.Append(" </table>");

            sbHead.Append("<table  width=\"98%\" class=\"cls_ProjAllot_Table cls_ProjAllot_label2\" >");
            if (dtData.Rows.Count > 0)
            {
                TG.Model.tg_member tg = new TG.Model.tg_member();
                TG.BLL.tg_member bll = new BLL.tg_member();
                tg = bll.GetModel(UserSysNo);

                string name = "";
                if (tg != null)
                {
                    name = tg.mem_Name;
                }

                sbHead.Append("<tr>");
                sbHead.Append("<td style=\"text-align: center; width:20%;\">" + name + "</td>");
                sbHead.Append("<td style=\"text-align: center;width:10%;\">" + dtData.Rows[0]["DesignPercent"].ToString() + "%</td>");
                sbHead.Append("<td style=\"text-align: center;width:10%;\">" + Math.Round(decimal.Parse(dtData.Rows[0]["DesignCount"].ToString()), 0) + "</td>");
                sbHead.Append("<td style=\"text-align: center;width:10%;\">" + dtData.Rows[0]["SpecialtyHeadPercent"].ToString() + "%</td>");
                sbHead.Append("<td style=\"text-align: center;width:10%;\">" + Math.Round(decimal.Parse(dtData.Rows[0]["SpecialtyHeadCount"].ToString()), 0) + "</td>");
                sbHead.Append("<td style=\"text-align: center;width:10%;\">" + dtData.Rows[0]["AuditPercent"].ToString() + "%</td>");
                sbHead.Append("<td style=\"text-align: center;width:10%;\">" + Math.Round(decimal.Parse(dtData.Rows[0]["AuditCount"].ToString()), 0) + "</td>");
                sbHead.Append("<td style=\"text-align: center;width:10%;\">" + dtData.Rows[0]["ProofreadPercent"].ToString() + "%</td>");
                sbHead.Append("<td style=\"text-align: center;width:10%;\">" + Math.Round(decimal.Parse(dtData.Rows[0]["ProofreadCount"].ToString()), 0) + "</td>");
                sbHead.Append("</tr></table>");
            }
            else
            {
                sbHead.Append("<tr><td colspan=\"9\" style=\"color: Red;\">没有产值信息</td></tr></table>");
            }
            sbHead.Append("</div>");
            return sbHead.ToString();
        }
    }
}