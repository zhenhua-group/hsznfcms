﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TG.Web.Login" %>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>智能房产mis</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="MobileOptimized" content="320">
    <!-- 全局定义 -->
    <link href="../js/assets/plugins/font-awesome/css/font-awesome.mi n.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../js/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet"
        type="text/css" />
    <!-- 结束 -->
    <!-- 页面定义 -->
    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/select2/select2_metro.css" />
    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/bootstrap-toastr/toastr.min.css" />
    <!-- 结束 -->
    <!-- 皮肤定义 -->
    <link href="../js/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/css/themes/default.css" rel="stylesheet" type="text/css"
        id="style_color" />
    <link href="../js/assets/css/pages/login-soft.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/css/custom.css" rel="stylesheet" type="text/css" />
    <!-- 核心JS -->
    <!--[if lt IE 9]>
	<script src="../js/assets/plugins/respond.min.js"></script>
	<script src="../js/assets/plugins/excanvas.min.js"></script> 
	<![endif]-->
    <script src="../js/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"
        type="text/javascript"></script>
    <script src="../js/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
    <!-- 结束 -->
    <!-- 插件JS -->
    <script src="../js/assets/plugins/jquery-validation/dist/jquery.validate.min.js"
        type="text/javascript"></script>
    <script src="../js/assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/assets/plugins/select2/select2.min.js"></script>
    <script src="../js/assets/plugins/bootstrap-toastr/toastr.min.js"></script>
    <script src="../js/assets/plugins/jquery.cookie.min.js"></script>
    <!-- 结束 -->
    <!-- 页面JS -->
    <script src="../js/assets/scripts/app.js" type="text/javascript"></script>
    <script src="../js/assets/scripts/login-soft.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/jquery-validation/localization/messages_zh.js"
        type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            App.init();
            Login.init();
        });
    </script>
</head>
<body class="login">
    <div class="logo">
        <div style="font-weight: 600; color: White;">
            <h3>
               智能房产mis</h3>
        </div>
    </div>
    <div class="content">
        <form class="login-form">
        <h3 class="form-title">
            用户登录</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert">
            </button>
            <span>请输入用户名密码.</span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">
                用户名</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input id="username" class="form-control placeholder-no-fix" type="text" autocomplete="off"
                    placeholder="用户名" name="username" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">
                密码</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input id="userpwd" class="form-control placeholder-no-fix" type="password" autocomplete="off"
                    placeholder="密码" name="password" />
            </div>
        </div>
        <div class="form-actions">
            <label class="checkbox">
                <input id="chk_remember" type="checkbox" name="remember" />
                记住密码
            </label>
            <button id="btn_login" type="button" class="btn blue pull-right">
                登录 <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
        <div class="forget-password">
            <h4>
                忘记密码 ?</h4>
            <p>
                点击<a href="javascript:;" id="forget-password">这里</a> 联系管理员重置密码.
            </p>
        </div>
        </form>
        <!-- 忘记密码 -->
        <form class="forget-form" action="#">
        <h3>
            忘记密码 ?</h3>
        <p>
            请输入您的登录名.</p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input id="username2" class="form-control placeholder-no-fix" type="text" autocomplete="off"
                    placeholder="登录名" name="username2" />
            </div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn">
                <i class="m-icon-swapleft"></i>返回
            </button>
            <button id="btn_repwd" type="button" class="btn blue pull-right">
                发送 <i class="m-icon-swapright m-icon-white"></i>
            </button>
        </div>
        </form>
    </div>
    <div class="copyright">
        2016 &copy; 天正软件股份有限公司（<span style="color:#0026ff;">请使用IE8以上版本，Chrom或360浏览器</span>）
    </div>
</body>
</html>
