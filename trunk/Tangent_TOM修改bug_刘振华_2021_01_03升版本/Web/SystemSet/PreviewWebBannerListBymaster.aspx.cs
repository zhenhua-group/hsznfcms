﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class PreviewWebBannerListBymaster : System.Web.UI.Page
    {
        //公用数据处理类
        TG.BLL.CommDBHelper bll_comm = new TG.BLL.CommDBHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindPubMsg();
            }
        }
        //绑定公告
        protected void BindPubMsg()
        {
            string strWhere = "";
            //if (this.txt_keyname.Value != "")
            //{
            //    strWhere += "  AND Title Like '%" + this.txt_keyname.Value + "%'";
            //}
            //记录数
            this.AspNetPager1.RecordCount = bll_comm.GetDataPageListCount(strWhere, "cm_WebBanner");
            //排序
            string orderString = " Order By SysNo DESC ";
            //绑定
            this.GridView1.DataSource = bll_comm.GetDataPageList(strWhere, orderString, this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize, "cm_WebBanner");
            this.GridView1.DataBind();
        }
        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindPubMsg();
        }
        //人员实体
        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string mem_id = e.Row.Cells[2].Text;
                if (mem_id != "")
                {
                    e.Row.Cells[2].Text = bll_mem.GetModel(int.Parse(mem_id)).mem_Name;
                }
                //状态

            }
        }

    }
}