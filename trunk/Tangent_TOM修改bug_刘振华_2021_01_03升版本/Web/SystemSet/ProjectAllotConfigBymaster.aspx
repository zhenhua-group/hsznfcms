﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProjectAllotConfigBymaster.aspx.cs" Inherits="TG.Web.SystemSet.ProjectAllotConfigBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../js/SystemSet/ProjectAllotConfig.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>

    <style type="text/css">
        #tb_CmpTable, #tb_SpeTable, #tb_UserTable
        {
            display: none;
        }
        #tb_CmpTable td, #tb_SpeTable td, #tb_UserTable td
        {
            border: solid 1px #CCC;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>分配比例配置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用管理</a><i class="fa fa-angle-right"> </i><a>分配比例配置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>分配比例</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <h4 class="form-section">
                            院级分配比例</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                    Width="100%" OnRowCommand="GridView1_RowCommand" CssClass="table table-striped table-bordered table-hover dataTable">
                                    <Columns>
                                        <asp:TemplateField HeaderText=" 财务管理(%)">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("CmpMngPrt") %>'></asp:Label>
                                                <asp:HiddenField ID="hid_id" runat="server" Value='<%# Eval("ID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="11%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ProCostPrt" HeaderText=" 项目费用(%)">
                                            <ItemStyle Width="11%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProChgPrt" HeaderText=" 项目负责(%)">
                                            <ItemStyle Width="11%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="RegUserPrt" HeaderText=" 注册师A(%)">
                                            <ItemStyle Width="11%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DesignPrt" HeaderText=" 设校人员BC(%)">
                                            <ItemStyle Width="11%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AuditPrt" HeaderText=" 校审核定DE(%)">
                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TotalUser" HeaderText=" 总工(%)">
                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CollegeUser" HeaderText=" 院长(%)">
                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText=" 状态">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hid_status" runat="server" Value='<%# Eval("Used") %>' />
                                                <asp:ImageButton ID="btn_SetDefault" runat="server" CssClass="cls_btnCmp" ImageUrl="~/Images/buttons/btn_setdf.gif"
                                                    CommandName="setdefault" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="7%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText=" 添加">
                                            <ItemTemplate>
                                                <a href="###" class="cls_addCmpConfig">添加</a>| <a href="###" class="cls_delCmpConfig">
                                                    删除</a>
                                                <asp:HiddenField ID="hid_cmp" runat="server" Value='<%# Eval("ID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有配置信息！<a href="###" class="cls_addCmpConfig">添加</a>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <h4 class="form-section">
                            专业分配比例</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                    Width="100%" OnRowDataBound="GridView2_RowDataBound" CssClass="table table-striped table-bordered table-hover dataTable">
                                    <Columns>
                                        <asp:BoundField DataField="StrucType" HeaderText="结构类型">
                                            <ItemStyle Width="30%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="spe_Name" HeaderText=" 专业(%)">
                                            <ItemStyle Width="30%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ListPrt" HeaderText=" 分配额(%)">
                                            <ItemStyle Width="30%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText=" 添加">
                                            <ItemTemplate>
                                                <a href="###" class="cls_addSpeConfig">添加</a>| <a href="###" class="cls_delSpeConfig">
                                                    删除</a>
                                                <asp:HiddenField ID="hid_spe" runat="server" Value='<%# Eval("ID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有配置信息！<a href="###" class="cls_addSpeConfig">添加</a>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <h4 class="form-section">
                            员工分配比例</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                    Width="100%" OnRowCommand="GridView3_RowCommand" CssClass="table table-striped table-bordered table-hover dataTable">
                                    <Columns>
                                        <asp:TemplateField HeaderText="设计人员(%)">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("DisgnUserPrt") %>'></asp:Label>
                                                <asp:HiddenField ID="hid_id2" runat="server" Value='<%# Eval("ID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="AuditDPrt" HeaderText="校核人员(%)">
                                            <ItemStyle Width="15%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SpeRegPrt" HeaderText="专业注册(%)">
                                            <ItemStyle Width="15%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AuditPrt" HeaderText="审核人员(%)">
                                            <ItemStyle Width="15%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ExamPrt" HeaderText="审定人员(%)">
                                            <ItemStyle Width="15%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="状态">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("Used") %>' />
                                                <asp:ImageButton ID="btn_SetDefault" runat="server" CssClass="cls_btnUser" ImageUrl="~/Images/buttons/btn_setdf.gif"
                                                    CommandName="setdefault" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="添加">
                                            <ItemTemplate>
                                                <a href="###" class="cls_addUserConfig">添加</a>| <a href="###" class="cls_delUserConfig">
                                                    删除</a>
                                                <asp:HiddenField ID="hid_user" runat="server" Value='<%# Eval("ID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="10%" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有配置信息！<a href="###" class="cls_addUserConfig">添加</a>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cls_data_bottom" id="tb_CmpTable">
        <h3 class="form-section">
            院级分配比例</h3>
        <div class="row">
            <div class="form-group">
                <label class="control-label col-md-1">
                    财务管理(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config1" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
                <label class="control-label col-md-1">
                    项目费用(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config2" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
                <label class="control-label col-md-1">
                    项目负责(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config3" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
                <label class="control-label col-md-1">
                    注册师A(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config4" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
                <label class="control-label col-md-1">
                    设校人员BC(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config5" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label class="control-label col-md-1">
                    校审核定DE(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config6" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
                <label class="control-label col-md-1">
                    总工(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config7" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
                <label class="control-label col-md-1">
                    院长(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config8" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="chk_cmpstatus" runat="server" Text="默认" />
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-offset-12 col-md-12">
                    <asp:Button ID="btn_Save" runat="server" Text="确定" CssClass="btn blue" OnClick="btn_Save_Click" />
                    <button type="button" class="btn default" id="deCanle">
                        取消</button>
                </div>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="cls_data_bottom" id="tb_SpeTable">
        <h3 class="form-section">
            专业分配比例</h3>
        <div class="row">
            <div class="form-group">
                <label class="control-label col-md-1">
                    结构类型:</label>
                <div class="col-md-2">
                    <asp:DropDownList ID="drp_strutype" CssClass="form-control input-sm" runat="server"
                        AppendDataBoundItems="True">
                        <asp:ListItem Value="-1">---请选择---</asp:ListItem>
                    </asp:DropDownList>
                    
                </div>
                <label class="control-label col-md-1">
                    专业:</label>
                <div class="col-md-2">
                    <asp:DropDownList ID="DropDownList1" CssClass="form-control input-sm" runat="server"
                        AppendDataBoundItems="True">
                        <asp:ListItem Value="-1">---请选择---</asp:ListItem>
                    </asp:DropDownList>
                    
                </div>
                <label class="control-label col-md-1">
                    分配比例:</label>
                <div class="col-md-2">
                    <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-offset-12 col-md-12">
                    <asp:Button ID="btn_SaveSpe" runat="server" Text="确定" CssClass="btn blue" OnClick="btn_SaveSpe_Click" />
                    <button type="button" class="btn default" id="speCance">
                        取消</button>
                </div>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="cls_data_bottom" id="tb_UserTable">
        <h3 class="form-section">
            员工分配比例</h3>
        <div class="row">
            <div class="form-group">
                <label class="control-label col-md-1">
                    设计人员(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config9" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
                <label class="control-label col-md-1">
                    校核人员(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
                <label class="control-label col-md-1">
                    专业注册(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config11" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
                <label class="control-label col-md-1">
                    审核人员(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config12" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
                <label class="control-label col-md-1">
                    审定人员(%):</label>
                <div class="col-md-1">
                    <asp:TextBox ID="txt_config13" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="chk_userstatus" runat="server" Text="默认" />
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="col-md-offset-12 col-md-12">
                    <asp:Button ID="btn_SaveUser" runat="server" Text="确定" CssClass="btn blue" OnClick="btn_SaveUser_Click" />
                    <button type="button" class="btn default" id="userCanle">
                        取消</button>
                </div>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
</asp:Content>
