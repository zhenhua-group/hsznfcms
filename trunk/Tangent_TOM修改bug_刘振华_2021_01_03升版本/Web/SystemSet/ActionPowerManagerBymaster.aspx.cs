﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using TG.Model;
using TG.BLL;

namespace TG.Web.SystemSet
{
    public partial class ActionPowerManagerBymaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ActionPowerManagerBymaster));
            if (!IsPostBack)
            {
                GetActionPowerList(LeftMenuType.All);
            }
        }
        //获取功能模块列表
        private void GetActionPowerList(LeftMenuType leftMenuType)
        {
            this.RepeaterPowerManager.DataSource = new ActionPowerBP().GetActionPowerList(leftMenuType);
            this.RepeaterPowerManager.DataBind();
        }

        protected void TypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LeftMenuType leftType = (LeftMenuType)int.Parse(this.TypeDropDownList.SelectedValue);

            GetActionPowerList(leftType);
        }
        /// <summary>
        /// 新规ActionPower
        /// </summary>
        /// <param name="leftMenuString"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string InsertActionPower(string actionPower)
        {
            ActionPowerViewEntity actionPowerViewEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ActionPowerViewEntity>(actionPower);

            return new ActionPowerBP().InsertActionPower(actionPowerViewEntity) + "";
        }

        [AjaxMethod]
        public string GetActionPower(int actionPowerSysNo)
        {
            ActionPowerViewEntity resultEntity = new ActionPowerBP().GetActionPowerViewEntity(actionPowerSysNo);

            return Newtonsoft.Json.JsonConvert.SerializeObject(resultEntity);
        }

        /// <summary>
        /// 修改LeftMenu
        /// </summary>
        /// <returns></returns>
        [AjaxMethod]
        public string UpdateActionPower(string actionPowerString)
        {
            ActionPowerViewEntity actionPowerViewEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<ActionPowerViewEntity>(actionPowerString);

            return new ActionPowerBP().UpDateActionPower(actionPowerViewEntity).ToString();
        }

        /// <summary>
        /// 删除LeftMenu
        /// </summary>
        /// <param name="leftMenuSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string DeleteActionPower(int actionPowerSysNo)
        {
            return new ActionPowerBP().DeleteActionPower(actionPowerSysNo).ToString();
        }
    }
}