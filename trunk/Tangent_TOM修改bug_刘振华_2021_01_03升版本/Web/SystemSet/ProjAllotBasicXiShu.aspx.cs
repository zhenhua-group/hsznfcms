﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class ProjAllotBasicXiShu : System.Web.UI.Page
    {
        /// <summary>
        /// 获取系数列表
        /// </summary>
        public List<TG.Model.cm_SysProjAllotXiShu> XiShuList
        {
            get
            {
                return new TG.BLL.cm_SysProjAllotXiShu().GetModelList("");
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}