﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class ProjectNumConfigBymaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                for (int i = 1; i < 9; i++)
                {
                    ShowInfo(i);
                }
            }
        }
        private void ShowInfo(int ID)
        {
            TG.BLL.cm_ProjectNumConfig bll = new TG.BLL.cm_ProjectNumConfig();
            TG.Model.cm_ProjectNumConfig model = bll.GetModel(ID);
            //查询
            if (model != null)
            {
                if (ID == 1)
                {
                    this.txt_Prefix.Text = model.PreFix.Trim();
                    this.txt_mark.Text = model.Mark;
                    this.txtStartNum.Text = model.StartNum.Trim();
                    this.txtEndNum.Text = model.EndNum.Trim();

                }
                else if (ID == 2)
                {
                    this.txt_Prefix0.Text = model.PreFix.Trim();
                    this.txt_mark0.Text = model.Mark;
                    this.txtStartNum0.Text = model.StartNum.Trim();
                    this.txtEndNum0.Text = model.EndNum.Trim();
                }
                else if (ID == 3)
                {
                    this.txt_Prefix1.Text = model.PreFix.Trim();
                    this.txt_mark1.Text = model.Mark;
                    this.txtStartNum1.Text = model.StartNum.Trim();
                    this.txtEndNum1.Text = model.EndNum.Trim();
                }
                else if (ID == 4)
                {
                    this.txt_Prefix2.Text = model.PreFix.Trim();
                    this.txt_mark2.Text = model.Mark;
                    this.txtStartNum2.Text = model.StartNum.Trim();
                    this.txtEndNum2.Text = model.EndNum.Trim();
                }
                else if (ID == 5)
                {
                    this.txt_Prefix3.Text = model.PreFix.Trim();
                    this.txt_mark3.Text = model.Mark;
                    this.txtStartNum3.Text = model.StartNum.Trim();
                    this.txtEndNum3.Text = model.EndNum.Trim();
                }
                else if (ID == 6)
                {
                    this.txt_Prefix4.Text = model.PreFix.Trim();
                    this.txt_mark4.Text = model.Mark;
                    this.txtStartNum4.Text = model.StartNum.Trim();
                    this.txtEndNum4.Text = model.EndNum.Trim();
                }
                else if (ID == 7)
                {
                    this.txt_Prefix5.Text = model.PreFix.Trim();
                    this.txt_mark5.Text = model.Mark;
                    this.txtStartNum5.Text = model.StartNum.Trim();
                    this.txtEndNum5.Text = model.EndNum.Trim();
                }
                else if (ID == 8)
                {
                    this.txt_Prefix6.Text = model.PreFix.Trim();
                    this.txt_mark6.Text = model.Mark;
                    this.txtStartNum6.Text = model.StartNum.Trim();
                    this.txtEndNum6.Text = model.EndNum.Trim();
                }
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            TG.Model.cm_ProjectNumConfig model = new TG.Model.cm_ProjectNumConfig();
            TG.BLL.cm_ProjectNumConfig bll = new TG.BLL.cm_ProjectNumConfig();
            //民用建筑
            int ID = 1;
            string Prefix = this.txt_Prefix.Text;
            string mark = this.txt_mark.Text;
            string StartNum = this.txtStartNum.Text;
            string EndNum = this.txtEndNum.Text;
            string ProType = "民用建筑";
            int Flag = 0;

            model.ID = ID;
            model.PreFix = Prefix;
            model.Mark = mark;
            model.StartNum = StartNum;
            model.EndNum = EndNum;
            model.ProType = ProType;
            model.Flag = Flag;

            bll.Update(model);
            //工业建筑
            ID = 2;
            Prefix = this.txt_Prefix0.Text;
            mark = this.txt_mark0.Text;
            StartNum = this.txtStartNum0.Text;
            EndNum = this.txtEndNum0.Text;
            ProType = "工业建筑";
            Flag = 0;

            model.ID = ID;
            model.PreFix = Prefix;
            model.Mark = mark;
            model.StartNum = StartNum;
            model.EndNum = EndNum;
            model.ProType = ProType;
            model.Flag = Flag;

            bll.Update(model);
            //工程勘察
            ID = 3;
            Prefix = this.txt_Prefix1.Text;
            mark = this.txt_mark1.Text;
            StartNum = this.txtStartNum1.Text;
            EndNum = this.txtEndNum1.Text;
            ProType = "工程勘察";
            Flag = 0;

            model.ID = ID;
            model.PreFix = Prefix;
            model.Mark = mark;
            model.StartNum = StartNum;
            model.EndNum = EndNum;
            model.ProType = ProType;
            model.Flag = Flag;

            bll.Update(model);
            //工程监理
            ID = 4;
            Prefix = this.txt_Prefix2.Text;
            mark = this.txt_mark2.Text;
            StartNum = this.txtStartNum2.Text;
            EndNum = this.txtEndNum2.Text;
            ProType = "工程监理";
            Flag = 0;

            model.ID = ID;
            model.PreFix = Prefix;
            model.Mark = mark;
            model.StartNum = StartNum;
            model.EndNum = EndNum;
            model.ProType = ProType;
            model.Flag = Flag;

            bll.Update(model);
            //工程施工
            ID = 5;
            Prefix = this.txt_Prefix3.Text;
            mark = this.txt_mark3.Text;
            StartNum = this.txtStartNum3.Text;
            EndNum = this.txtEndNum3.Text;
            ProType = "工程施工";
            Flag = 0;

            model.ID = ID;
            model.PreFix = Prefix;
            model.Mark = mark;
            model.StartNum = StartNum;
            model.EndNum = EndNum;
            model.ProType = ProType;
            model.Flag = Flag;

            bll.Update(model);
            //工程咨询
            ID = 6;
            Prefix = this.txt_Prefix4.Text;
            mark = this.txt_mark4.Text;
            StartNum = this.txtStartNum4.Text;
            EndNum = this.txtEndNum4.Text;
            ProType = "工程咨询";
            Flag = 0;

            model.ID = ID;
            model.PreFix = Prefix;
            model.Mark = mark;
            model.StartNum = StartNum;
            model.EndNum = EndNum;
            model.ProType = ProType;
            model.Flag = Flag;

            bll.Update(model);
            //工程总承包
            ID = 7;
            Prefix = this.txt_Prefix5.Text;
            mark = this.txt_mark5.Text;
            StartNum = this.txtStartNum5.Text;
            EndNum = this.txtEndNum5.Text;
            ProType = "工程总承包";
            Flag = 0;

            model.ID = ID;
            model.PreFix = Prefix;
            model.Mark = mark;
            model.StartNum = StartNum;
            model.EndNum = EndNum;
            model.ProType = ProType;
            model.Flag = Flag;

            bll.Update(model);
            //工程代建
            ID = 8;
            Prefix = this.txt_Prefix6.Text;
            mark = this.txt_mark6.Text;
            StartNum = this.txtStartNum6.Text;
            EndNum = this.txtEndNum6.Text;
            ProType = "工程代建";
            Flag = 0;

            model.ID = ID;
            model.PreFix = Prefix;
            model.Mark = mark;
            model.StartNum = StartNum;
            model.EndNum = EndNum;
            model.ProType = ProType;
            model.Flag = Flag;

            bll.Update(model);


            TG.Common.MessageBox.Show(this, "保存成功！");
        }
    }
}