﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace TG.Web.SystemSet
{
    public partial class ProValueParameterConfigBymaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindAllData();
            }
        }

        protected void BindAllData()
        {
            //绑定各阶段
            TG.BLL.cm_ProjectStageValueConfig bll = new TG.BLL.cm_ProjectStageValueConfig();
            DataSet ds = bll.GetListAll();
            this.gvOne.DataSource = ds.Tables[0];
            this.gvOne.DataBind();

            //绑定二阶段
            this.gvTwo.DataSource = ds.Tables[1];
            this.gvTwo.DataBind();

            //绑定二阶段
            this.gvThree.DataSource = ds.Tables[2];
            this.gvThree.DataBind();

            //绑定三阶段
            this.gvFour.DataSource = ds.Tables[3];
            this.gvFour.DataBind();

            //方案+施工图+后期
            this.gv_Eleven.DataSource = ds.Tables[7];
            this.gv_Eleven.DataBind();

            //绑定各设计阶段专业

            DataTable dtProjectStageSpe = ds.Tables[4];

            CreateProjectStageSpe(dtProjectStageSpe);


            DataTable dtProcess = ds.Tables[5];

            var processOne = new DataView(dtProcess) { RowFilter = "ItemType=0" }.ToTable();
            //绑定设计工序
            this.gvDesignProcess.DataSource = processOne;
            this.gvDesignProcess.DataBind();

            var processTwo = new DataView(dtProcess) { RowFilter = "ItemType=1" }.ToTable();
            //绑定设计工序
            this.gvDesignProcessTwo.DataSource = processTwo;
            this.gvDesignProcessTwo.DataBind();

            var processThree = new DataView(dtProcess) { RowFilter = "ItemType=2" }.ToTable();
            //绑定设计工序
            this.gvDesignProcessThree.DataSource = processThree;
            this.gvDesignProcessThree.DataBind();

            var processFour = new DataView(dtProcess) { RowFilter = "ItemType=3" }.ToTable();
            //绑定设计工序
            this.gvDesignProcessFour.DataSource = processFour;
            this.gvDesignProcessFour.DataBind();

            var processFive = new DataView(dtProcess) { RowFilter = "ItemType=4" }.ToTable();
            //绑定设计工序
            this.gvDesignProcessFive.DataSource = processFive;
            this.gvDesignProcessFive.DataBind();

            DataTable dt = ds.Tables[6];

            CreateTable(dt);

        }

        /// <summary>
        /// 生成表三 --项目个设计阶段专业之间产值分配比例
        /// </summary>
        /// <param name="dt"></param>
        private void CreateProjectStageSpe(DataTable dt)
        {
            #region 无暖通专业参与
            //取得没有暖通所参与的table
            DataTable dtNotHave = new DataView(dt) { RowFilter = "IsHaved=0" }.ToTable();
            //取得专业
            var dtSpecialtyList = new DataView(dtNotHave).ToTable("Specialty", true, "Specialty");

            int count = dtSpecialtyList.Rows.Count;

            //总
            StringBuilder sb = new StringBuilder();
            sb.Append(" <fieldset><legend>项目各设计阶段专业之间产值分配比例-列表</legend>");

            //声明创建Table 表头
            StringBuilder sb_Head = new StringBuilder();
            sb_Head.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_Head.Append("<tr><td colspan=" + (count + 2) + " style=\"width: 100%; text-align: center;background-color: White;\"> 项目各设计阶段专业之间产值分配比例(无暖通专业)% </td> </tr>");
            sb_Head.Append("<tr>");
            sb_Head.Append("<td width=\"16%\">设计阶段</td>");

            //内容
            StringBuilder sb_Html = new StringBuilder();
            sb_Html.Append("<table id=\"gvProjectStageSpe\"  width=\"100%\" class=\"gridView_comm\">");

            //第一行
            StringBuilder sb_one = new StringBuilder();
            sb_one.Append("<tr >");
            sb_one.Append("<td width=\"16%\">方案设计</td>");

            //第二行
            StringBuilder sb_two = new StringBuilder();
            sb_two.Append("<tr>");
            sb_two.Append("<td width=\"16%\">初步设计</td>");

            //第三不
            StringBuilder sb_three = new StringBuilder();
            sb_three.Append("<tr>");
            sb_three.Append("<td width=\"16%\">施工图设计</td>");

            //第三行
            StringBuilder sb_four = new StringBuilder();
            sb_four.Append("<tr>");
            sb_four.Append("<td width=\"16%\">后期服务</td>");

            //比例
            decimal percent = decimal.Divide(74, count);
            percent = decimal.Round(percent, 2);

            for (int i = 0; i < count; i++)
            {
                sb_Head.Append("<td width=" + percent + "%>" + dtSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            var dataProgram = new DataView(dtNotHave).ToTable("ProgramPercent", false, "ProgramPercent");

            for (int j = 0; j < dataProgram.Rows.Count; j++)
            {
                sb_one.Append("<td width=" + percent + "%>" + dataProgram.Rows[j][0].ToString() + "</td>");
            }

            var datapreliminary = new DataView(dtNotHave).ToTable("preliminaryPercent", false, "preliminaryPercent");
            for (int j = 0; j < datapreliminary.Rows.Count; j++)
            {
                sb_two.Append("<td>" + datapreliminary.Rows[j][0].ToString() + "</td>");
            }

            var dataWorkDraw = new DataView(dtNotHave).ToTable("WorkDrawPercent", false, "WorkDrawPercent");
            for (int j = 0; j < dataWorkDraw.Rows.Count; j++)
            {
                sb_three.Append("<td>" + dataWorkDraw.Rows[j][0].ToString() + "</td>");
            }

            var dataLateStage = new DataView(dtNotHave).ToTable("LateStagePercent", false, "LateStagePercent");
            for (int j = 0; j < dataLateStage.Rows.Count; j++)
            {
                sb_four.Append("<td>" + dataLateStage.Rows[j][0].ToString() + "</td>");
            }

            sb_Head.Append("<td width=\"10%\">编辑</td>");
            sb_Head.Append("</tr>");
            sb_Head.Append("</table>");

            sb_one.Append("<td width=\"10%\"> <a href=\"###\" class=\"cls_Speselect\">编辑</a></td>");
            sb_one.Append("</tr>");
            sb_two.Append("<td>  <a href=\"###\" class=\"cls_Speselect\">编辑</a></td>");
            sb_two.Append("</tr>");
            sb_three.Append("<td>  <a href=\"###\" class=\"cls_Speselect\">编辑</a></td>");
            sb_three.Append("</tr>");
            sb_four.Append("<td>  <a href=\"###\" class=\"cls_Speselect\">编辑</a></td>");
            sb_four.Append("</tr>");

            sb_Html.Append(sb_one);
            sb_Html.Append(sb_two);
            sb_Html.Append(sb_three);
            sb_Html.Append(sb_four);
            sb_Html.Append("</table>");

            StringBuilder sb_Eidt = new StringBuilder();
            sb_Eidt.Append(" <div class=\"cls_data\" id=\"divSpa\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>项目各设计阶段专业之间产值分配比例(无暖通专业)-编辑</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 设计阶段 </td> <td> <span ID=\"lblProjectStage\" ></span> </td></tr>");


            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                sb_Eidt.Append("<tr>");
                sb_Eidt.Append("<td>" + dtSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_Eidt.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanbulidingPercent\">请输入整数！</span> </td>");
                sb_Eidt.Append("</tr>");
            }

            sb_Eidt.Append(" <tr><td> &nbsp;  </td> <td>   <img src=\"../Images/buttons/btn_ok.gif\" id=\"btnSpeSave\" style=\"cursor: pointer\"></td></tr>");
            sb_Eidt.Append("</table> </fieldset></div>");

            sb.Append(sb_Head);
            sb.Append(sb_Html);
            sb.Append(sb_Eidt);


            #endregion

            #region 有暖通专业
            //取得有暖通所参与的table
            DataTable dtHave = new DataView(dt) { RowFilter = "IsHaved=1" }.ToTable();
            //取得专业
            var dtIsHaveSpecialtyList = new DataView(dtHave).ToTable("Specialty", true, "Specialty");

            int countIsHave = dtIsHaveSpecialtyList.Rows.Count;


            //声明创建Table 表头
            StringBuilder sb_IsHaveHead = new StringBuilder();
            sb_IsHaveHead.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_IsHaveHead.Append("<tr><td colspan=" + (countIsHave + 2) + " style=\"width: 100%; text-align: center;background-color: White;\"> 项目各设计阶段专业之间产值分配比例(有暖通专业)% </td> </tr>");
            sb_IsHaveHead.Append("<tr>");
            sb_IsHaveHead.Append("<td width=\"15%\">设计阶段</td>");

            //内容
            StringBuilder sb_IsHaveHtml = new StringBuilder();
            sb_IsHaveHtml.Append("<table id=\"gvProjectStageIsHaveSpe\"  width=\"100%\" class=\"gridView_comm\">");

            //第一行
            StringBuilder sb_oneIsHave = new StringBuilder();
            sb_oneIsHave.Append("<tr >");
            sb_oneIsHave.Append("<td width=\"15%\">方案设计</td>");

            //第二行
            StringBuilder sb_twoIsHave = new StringBuilder();
            sb_twoIsHave.Append("<tr>");
            sb_twoIsHave.Append("<td width=\"15%\">初步设计</td>");

            //第三不
            StringBuilder sb_threeIsHave = new StringBuilder();
            sb_threeIsHave.Append("<tr>");
            sb_threeIsHave.Append("<td width=\"15%\">施工图设计</td>");

            //第三行
            StringBuilder sb_fourIsHave = new StringBuilder();
            sb_fourIsHave.Append("<tr>");
            sb_fourIsHave.Append("<td width=\"15%\">后期服务</td>");

            //比例
            decimal percentIsHave = decimal.Divide(75, countIsHave);
            percentIsHave = decimal.Round(percentIsHave, 2);

            for (int i = 0; i < countIsHave; i++)
            {
                sb_IsHaveHead.Append("<td width=" + percentIsHave + "%>" + dtIsHaveSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            var dataIsHaveProgram = new DataView(dtHave).ToTable("ProgramPercent", false, "ProgramPercent");

            for (int j = 0; j < dataIsHaveProgram.Rows.Count; j++)
            {
                sb_oneIsHave.Append("<td width=" + percentIsHave + "%>" + dataIsHaveProgram.Rows[j][0].ToString() + "</td>");
            }

            var datIsHaveApreliminary = new DataView(dtHave).ToTable("preliminaryPercent", false, "preliminaryPercent");
            for (int j = 0; j < datIsHaveApreliminary.Rows.Count; j++)
            {
                sb_twoIsHave.Append("<td>" + datIsHaveApreliminary.Rows[j][0].ToString() + "</td>");
            }

            var dataIsHavWorkDraw = new DataView(dtHave).ToTable("WorkDrawPercent", false, "WorkDrawPercent");
            for (int j = 0; j < dataIsHavWorkDraw.Rows.Count; j++)
            {
                sb_threeIsHave.Append("<td>" + dataIsHavWorkDraw.Rows[j][0].ToString() + "</td>");
            }

            var dataIsHaveLateStage = new DataView(dtHave).ToTable("LateStagePercent", false, "LateStagePercent");
            for (int j = 0; j < dataIsHaveLateStage.Rows.Count; j++)
            {
                sb_fourIsHave.Append("<td>" + dataIsHaveLateStage.Rows[j][0].ToString() + "</td>");
            }

            sb_IsHaveHead.Append("<td width=\"10%\">编辑</td>");
            sb_IsHaveHead.Append("</tr>");
            sb_IsHaveHead.Append("</table>");

            sb_oneIsHave.Append("<td width=\"10%\"> <a href=\"###\" class=\"cls_IsHaveSpeselect\">编辑</a></td>");
            sb_oneIsHave.Append("</tr>");
            sb_twoIsHave.Append("<td>  <a href=\"###\" class=\"cls_IsHaveSpeselect\">编辑</a></td>");
            sb_twoIsHave.Append("</tr>");
            sb_threeIsHave.Append("<td>  <a href=\"###\" class=\"cls_IsHaveSpeselect\">编辑</a></td>");
            sb_threeIsHave.Append("</tr>");
            sb_fourIsHave.Append("<td>  <a href=\"###\" class=\"cls_IsHaveSpeselect\">编辑</a></td>");
            sb_fourIsHave.Append("</tr>");

            sb_IsHaveHtml.Append(sb_oneIsHave);
            sb_IsHaveHtml.Append(sb_twoIsHave);
            sb_IsHaveHtml.Append(sb_threeIsHave);
            sb_IsHaveHtml.Append(sb_fourIsHave);
            sb_IsHaveHtml.Append("</table>");

            StringBuilder sb_IsHaveEidt = new StringBuilder();
            sb_IsHaveEidt.Append(" <div class=\"cls_data\" id=\"divIsHaveSpa\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>项目各设计阶段专业之间产值分配比例(有暖通专业)-编辑</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 设计阶段 </td> <td> <span ID=\"lblProjectStage\" ></span> </td></tr>");


            for (int i = 0; i < dtIsHaveSpecialtyList.Rows.Count; i++)
            {
                sb_IsHaveEidt.Append("<tr>");
                sb_IsHaveEidt.Append("<td>" + dtIsHaveSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_IsHaveEidt.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanbulidingPercent\">请输入整数！</span> </td>");
                sb_IsHaveEidt.Append("</tr>");
            }

            sb_IsHaveEidt.Append(" <tr><td> &nbsp;  </td> <td>   <img src=\"../Images/buttons/btn_ok.gif\" id=\"btnIsHaveSpeSave\" style=\"cursor: pointer\"></td></tr>");
            sb_IsHaveEidt.Append("</table> </fieldset></div>");

            sb.Append(sb_IsHaveHead);
            sb.Append(sb_IsHaveHtml);
            sb.Append(sb_IsHaveEidt);

            sb.Append("</fieldset>");
            #endregion
            lbl_ProjectStageSpe.Text = sb.ToString();


        }

        /// <summary>
        /// 生成表单
        /// </summary>
        /// <param name="dt"></param>
        private void CreateTable(DataTable dt)
        {

            //取得表四的值

            #region 取得表四的值
            var dvFour = new DataView(dt) { RowFilter = "status=4 And IsHaved=0" };
            var dtFour = dvFour.ToTable();

            //取得专业
            var dtSpecialtyList = new DataView(dtFour).ToTable("Specialty", true, "Specialty");

            int count = dtSpecialtyList.Rows.Count;
            int colspan = count + 2;
            //声明创建Table 表头
            StringBuilder sb_Head = new StringBuilder();
            sb_Head.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_Head.Append("<tr><td colspan=" + (colspan) + " style=\"width: 100%; text-align: center; background-color: White;\"> 室外工程产值分配比例(无暖通专业)% </td> </tr>");
            sb_Head.Append("<tr>");
            sb_Head.Append("<td width=\"16%\">专业</td>");

            //比例
            decimal percent = decimal.Divide(74, count);
            percent = decimal.Round(percent, 2);

            for (int i = 0; i < count; i++)
            {
                sb_Head.Append("<td width=" + percent + "%>" + dtSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            sb_Head.Append("<td width=\"10%\">编辑</td>");
            sb_Head.Append("</tr>");
            sb_Head.Append("</table>");


            //内容
            StringBuilder sb_Html = new StringBuilder();
            sb_Html.Append("<table id=\"gvFive\" width=\"100%\"class=\"gridView_comm\">");
            sb_Html.Append("<tr>");
            sb_Html.Append("<td width=\"16%\">比例</td>");
            for (int j = 0; j < dtFour.Rows.Count; j++)
            {
                sb_Html.Append("<td  width=" + percent + "%>" + dtFour.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_Html.Append("<td width=\"10%\" ><a href=\"#\" class=\"cls_Fiveselect\">编辑</a></td>");
            sb_Html.Append("</tr>");
            sb_Html.Append("</table>");


            StringBuilder sb_Eidt = new StringBuilder();
            sb_Eidt.Append(" <div class=\"cls_data\" id=\"divFiveEdit\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>室外工程产值分配-编辑（无暖通专业）</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 专业 </td> <td> <span ID=\"lblspetialty\" ></span> </td></tr>");


            for (int i = 0; i < dtSpecialtyList.Rows.Count; i++)
            {
                sb_Eidt.Append("<tr>");
                sb_Eidt.Append("<td>" + dtSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_Eidt.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanSpetytyPercent\">请输入整数！</span> </td>");
                sb_Eidt.Append("</tr>");
            }

            sb_Eidt.Append(" <tr><td> &nbsp;  </td> <td><img src=\"../Images/buttons/btn_ok.gif\" id=\"btnFive\" style=\"cursor: pointer\"></td></tr>");
            sb_Eidt.Append("</table> </fieldset></div>");

            StringBuilder sb = new StringBuilder();
            sb.Append(sb_Head.ToString());
            sb.Append(sb_Html.ToString());
            sb.Append(sb_Eidt.ToString());

            //有暖通参与
            var dvIsHaveFour = new DataView(dt) { RowFilter = "status=4 And IsHaved=1" };
            var dtIsHaveFour = dvIsHaveFour.ToTable();

            //取得专业
            var dtIsHaveSpecialtyList = new DataView(dtIsHaveFour).ToTable("Specialty", true, "Specialty");

            int countIsHave = dtIsHaveSpecialtyList.Rows.Count;
            int colspanIsHave = countIsHave + 2;
            //声明创建Table 表头
            StringBuilder sb_HeadIsHave = new StringBuilder();
            sb_HeadIsHave.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_HeadIsHave.Append("<tr><td colspan=" + (colspanIsHave) + " style=\"width: 100%; text-align: center; background-color: White;\"> 室外工程产值分配比例(有暖通专业)% </td> </tr>");
            sb_HeadIsHave.Append("<tr>");
            sb_HeadIsHave.Append("<td width=\"15%\">专业</td>");

            //比例
            decimal percentIsHave = decimal.Divide(75, colspanIsHave);
            percentIsHave = decimal.Round(percentIsHave, 2);

            for (int i = 0; i < countIsHave; i++)
            {
                sb_HeadIsHave.Append("<td width=" + percentIsHave + "%>" + dtIsHaveSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            sb_HeadIsHave.Append("<td width=\"10%\">编辑</td>");
            sb_HeadIsHave.Append("</tr>");
            sb_HeadIsHave.Append("</table>");


            //内容
            StringBuilder sb_HtmlIsHave = new StringBuilder();
            sb_HtmlIsHave.Append("<table id=\"gvIsHaveFive\" width=\"100%\"class=\"gridView_comm\">");
            sb_HtmlIsHave.Append("<tr>");
            sb_HtmlIsHave.Append("<td width=\"15%\">比例</td>");
            for (int j = 0; j < dtIsHaveFour.Rows.Count; j++)
            {
                sb_HtmlIsHave.Append("<td  width=" + percentIsHave + "%>" + dtIsHaveFour.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_HtmlIsHave.Append("<td width=\"10%\" ><a href=\"#\" class=\"cls_IsHaveFiveselect\">编辑</a></td>");
            sb_HtmlIsHave.Append("</tr>");
            sb_HtmlIsHave.Append("</table>");


            StringBuilder sb_EidtIsHave = new StringBuilder();
            sb_EidtIsHave.Append(" <div class=\"cls_data\" id=\"divIsHaveFiveEdit\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>室外工程产值分配-编辑（有暖通专业）</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 专业 </td> <td> <span ID=\"lblspetialty\" ></span> </td></tr>");


            for (int i = 0; i < dtIsHaveSpecialtyList.Rows.Count; i++)
            {
                sb_EidtIsHave.Append("<tr>");
                sb_EidtIsHave.Append("<td>" + dtIsHaveSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_EidtIsHave.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanSpetytyPercent\">请输入整数！</span> </td>");
                sb_EidtIsHave.Append("</tr>");
            }

            sb_EidtIsHave.Append(" <tr><td> &nbsp;  </td> <td><img src=\"../Images/buttons/btn_ok.gif\" id=\"btnIsHaveFive\" style=\"cursor: pointer\"></td></tr>");
            sb_EidtIsHave.Append("</table> </fieldset></div>");

            sb.Append(sb_HeadIsHave.ToString());
            sb.Append(sb_HtmlIsHave.ToString());
            sb.Append(sb_EidtIsHave.ToString());

            lbl_Five.Text = sb.ToString();

            #endregion

            #region 取得表五的值
            var dvFive = new DataView(dt) { RowFilter = "status=5 and IsHaved=0 " };
            var dtFive = dvFive.ToTable();

            //取得专业
            var dtFiveSpecialtyList = new DataView(dtFive).ToTable("Specialty", true, "Specialty");

            int countFive = dtFiveSpecialtyList.Rows.Count;

            //声明创建Table 表头
            StringBuilder sb_HeadFive = new StringBuilder();
            sb_HeadFive.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_HeadFive.Append("<tr><td colspan=" + (count + 2) + " style=\"width: 100%; text-align: center; background-color: White;\">  锅炉房产值分配比例（无暖通专业）% </td> </tr>");
            sb_HeadFive.Append("<tr>");
            sb_HeadFive.Append("<td width=\"16%\">吨位</td>");

            //比例
            decimal percentFive = decimal.Divide(74, countFive);
            percentFive = decimal.Round(percentFive, 2);

            for (int i = 0; i < countFive; i++)
            {
                sb_HeadFive.Append("<td width=" + percentFive + "%>" + dtFiveSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            sb_HeadFive.Append("<td width=\"10%\">编辑</td>");
            sb_HeadFive.Append("</tr>");
            sb_HeadFive.Append("</table>");



            //内容
            StringBuilder sb_HtmlFive = new StringBuilder();
            sb_HtmlFive.Append("<table id=\"gvSix\" width=\"100%\" class=\"gridView_comm\">");
            var dvFive_one = new DataView(dvFive.ToTable()) { RowFilter = " type='4吨以下' " }.ToTable();
            sb_HtmlFive.Append("<tr>");
            sb_HtmlFive.Append("<td width=\"16%\">" + dvFive_one.Rows[0]["type"].ToString() + "</td>");
            for (int j = 0; j < dvFive_one.Rows.Count; j++)
            {
                sb_HtmlFive.Append("<td width=" + percentFive + "%>" + dvFive_one.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_HtmlFive.Append("<td width=\"10%\" ><a href=\"###\" class=\"cls_Sixselect\">编辑</a></td>");
            sb_HtmlFive.Append("</tr>");

            var dvFive_two = new DataView(dvFive.ToTable()) { RowFilter = "type='6吨以上' " }.ToTable();
            sb_HtmlFive.Append("<tr>");
            sb_HtmlFive.Append("<td width=\"16%\">" + dvFive_two.Rows[0]["type"].ToString() + "</td>");
            for (int j = 0; j < dvFive_two.Rows.Count; j++)
            {
                sb_HtmlFive.Append("<td width=" + percentFive + "%>" + dvFive_two.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_HtmlFive.Append("<td width=\"10%\" ><a href=\"###\" class=\"cls_Sixselect\">编辑</a></td>");
            sb_HtmlFive.Append("</tr>");
            sb_HtmlFive.Append("</table>");


            StringBuilder sb_EidtFive = new StringBuilder();
            sb_EidtFive.Append(" <div class=\"cls_data\" id=\"dixSixEdit\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>锅炉房产值分配-编辑（无暖通专业）</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 吨位 </td> <td>  <span id=\"drp_select\" ></span> </td></tr>");


            for (int i = 0; i < dtFiveSpecialtyList.Rows.Count; i++)
            {
                sb_EidtFive.Append("<tr>");
                sb_EidtFive.Append("<td>" + dtFiveSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_EidtFive.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanSixPercent\">请输入整数！</span> </td>");
                sb_EidtFive.Append("</tr>");
            }

            sb_EidtFive.Append(" <tr><td> &nbsp;  </td> <td> <img src=\"../Images/buttons/btn_ok.gif\" id=\"btnBoiler\" style=\"cursor: pointer\"></td></tr>");
            sb_EidtFive.Append("</table> </fieldset></div>");

            StringBuilder sbFive = new StringBuilder();
            sbFive.Append(sb_HeadFive.ToString());
            sbFive.Append(sb_HtmlFive.ToString());
            sbFive.Append(sb_EidtFive.ToString());

            //取得有暖通专业参与的人员
            var dvIsHaveFive = new DataView(dt) { RowFilter = "status=5 and IsHaved=1" };
            var dtIsHaveFive = dvIsHaveFive.ToTable();

            //取得专业
            var dtIsHaveFiveSpecialtyList = new DataView(dtIsHaveFive).ToTable("Specialty", true, "Specialty");

            int countIsHaveFive = dtIsHaveFiveSpecialtyList.Rows.Count;

            //声明创建Table 表头
            StringBuilder sb_HeadIsHaveFive = new StringBuilder();
            sb_HeadIsHaveFive.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_HeadIsHaveFive.Append("<tr><td colspan=" + (countIsHaveFive + 2) + " style=\"width: 100%; text-align: center; background-color: White;\">  锅炉房产值分配比例(有暖通专业)% </td> </tr>");
            sb_HeadIsHaveFive.Append("<tr>");
            sb_HeadIsHaveFive.Append("<td width=\"16%\">吨位</td>");

            //比例
            decimal percentIsHaveFive = decimal.Divide(75, countIsHaveFive);
            percentIsHaveFive = decimal.Round(percentIsHaveFive, 2);

            for (int i = 0; i < countIsHaveFive; i++)
            {
                sb_HeadIsHaveFive.Append("<td width=" + percentIsHaveFive + "%>" + dtIsHaveFiveSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            sb_HeadIsHaveFive.Append("<td width=\"10%\">编辑</td>");
            sb_HeadIsHaveFive.Append("</tr>");
            sb_HeadIsHaveFive.Append("</table>");



            //内容
            StringBuilder sb_HtmlIsHaveFive = new StringBuilder();
            sb_HtmlIsHaveFive.Append("<table id=\"gvIsHaveSix\" width=\"100%\" class=\"gridView_comm\">");
            var dtIsHaveFive_one = new DataView(dvIsHaveFive.ToTable()) { RowFilter = "type='4吨以下'" }.ToTable();
            sb_HtmlIsHaveFive.Append("<tr>");
            sb_HtmlIsHaveFive.Append("<td width=\"16%\">" + dtIsHaveFive_one.Rows[0]["type"].ToString() + "</td>");
            for (int j = 0; j < dtIsHaveFive_one.Rows.Count; j++)
            {
                sb_HtmlIsHaveFive.Append("<td width=" + percentIsHaveFive + "%>" + dtIsHaveFive_one.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_HtmlIsHaveFive.Append("<td width=\"10%\" ><a href=\"###\" class=\"cls_IsHaveSixselect\">编辑</a></td>");
            sb_HtmlIsHaveFive.Append("</tr>");

            var dtIsHaveFive_two = new DataView(dvIsHaveFive.ToTable()) { RowFilter = "type='6吨以上'" }.ToTable();
            sb_HtmlIsHaveFive.Append("<tr>");
            sb_HtmlIsHaveFive.Append("<td width=\"16%\">" + dtIsHaveFive_two.Rows[0]["type"].ToString() + "</td>");
            for (int j = 0; j < dtIsHaveFive_two.Rows.Count; j++)
            {
                sb_HtmlIsHaveFive.Append("<td width=" + percentIsHaveFive + "%>" + dtIsHaveFive_two.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_HtmlIsHaveFive.Append("<td width=\"10%\" ><a href=\"###\" class=\"cls_IsHaveSixselect\">编辑</a></td>");
            sb_HtmlIsHaveFive.Append("</tr>");
            sb_HtmlIsHaveFive.Append("</table>");


            StringBuilder sb_EidtIsHaveFive = new StringBuilder();
            sb_EidtIsHaveFive.Append(" <div class=\"cls_data\" id=\"dixIsHaveSixEdit\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>锅炉房产值分配-编辑</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 吨位 </td> <td>  <span id=\"drp_IsHavcSelect\" > </span> </td></tr>");


            for (int i = 0; i < dtIsHaveFiveSpecialtyList.Rows.Count; i++)
            {
                sb_EidtIsHaveFive.Append("<tr>");
                sb_EidtIsHaveFive.Append("<td>" + dtIsHaveFiveSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_EidtIsHaveFive.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanSixPercent\">请输入整数！</span> </td>");
                sb_EidtIsHaveFive.Append("</tr>");
            }

            sb_EidtIsHaveFive.Append(" <tr><td> &nbsp;  </td> <td> <img src=\"../Images/buttons/btn_ok.gif\" id=\"btnIsHaveBoiler\" style=\"cursor: pointer\"></td></tr>");
            sb_EidtIsHaveFive.Append("</table> </fieldset></div>");


            sbFive.Append(sb_HeadIsHaveFive.ToString());
            sbFive.Append(sb_HtmlIsHaveFive.ToString());
            sbFive.Append(sb_EidtIsHaveFive.ToString());

            lbl_Six.Text = sbFive.ToString();

            #endregion

            #region 取得表六的值
            var dvSix = new DataView(dt) { RowFilter = "status=6 and IsHaved=0" };
            var dtSix = dvSix.ToTable();

            //取得专业
            var dtSixSpecialtyList = new DataView(dtSix).ToTable("Specialty", true, "Specialty");

            int countSix = dtSixSpecialtyList.Rows.Count;

            //声明创建Table 表头
            StringBuilder sb_HeadSix = new StringBuilder();
            sb_HeadSix.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_HeadSix.Append("<tr><td colspan=" + (count + 2) + " style=\"width: 100%; text-align: center; background-color: White;\"> 地上单建水泵房产值分配比例（无暖通专业）%</td> </tr>");
            sb_HeadSix.Append("<tr>");
            sb_HeadSix.Append("<td width=\"16%\">专业</td>");

            //比例
            decimal percentSix = decimal.Divide(74, countSix);
            percentSix = decimal.Round(percentSix, 2);

            for (int i = 0; i < countSix; i++)
            {
                sb_HeadSix.Append("<td width=" + percentSix + "%>" + dtSixSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            sb_HeadSix.Append("<td width=\"10%\">编辑</td>");
            sb_HeadSix.Append("</tr>");
            sb_HeadSix.Append("</table>");


            //内容
            StringBuilder sb_HtmlSix = new StringBuilder();
            sb_HtmlSix.Append("<table id=\"gvSeven\" width=\"100%\" class=\"gridView_comm\">");
            sb_HtmlSix.Append("<tr>");
            sb_HtmlSix.Append("<td width=\"16%\">比例</td>");
            for (int j = 0; j < dtSix.Rows.Count; j++)
            {
                sb_HtmlSix.Append("<td width=" + percentSix + "%>" + dtSix.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_HtmlSix.Append("<td width=\"10%\"><a href=\"#\" class=\"cls_Servenselect\">编辑</a></td>");
            sb_HtmlSix.Append("</tr>");
            sb_HtmlSix.Append("</table>");


            StringBuilder sb_EidtSix = new StringBuilder();
            sb_EidtSix.Append(" <div class=\"cls_data\" id=\"divServenEdit\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>地上单建水泵房产值分配-编辑（无暖通专业）</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 设计阶段 </td> <td> <span id=\"lblServenType\" ></span> </td></tr>");


            for (int i = 0; i < dtSixSpecialtyList.Rows.Count; i++)
            {
                sb_EidtSix.Append("<tr>");
                sb_EidtSix.Append("<td>" + dtSixSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_EidtSix.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanSevenPercent\">请输入整数！</span> </td>");
                sb_EidtSix.Append("</tr>");
            }

            sb_EidtSix.Append(" <tr><td> &nbsp;  </td> <td> <img src=\"../Images/buttons/btn_ok.gif\" id=\"btnSeven\" style=\"cursor: pointer\"></td></tr>");
            sb_EidtSix.Append("</table> </fieldset></div>");

            StringBuilder sbSix = new StringBuilder();
            sbSix.Append(sb_HeadSix.ToString());
            sbSix.Append(sb_HtmlSix.ToString());
            sbSix.Append(sb_EidtSix.ToString());

            var dvIsHaveSix = new DataView(dt) { RowFilter = "status=6 and IsHaved=1" };
            var dtIsHaveSix = dvIsHaveSix.ToTable();

            //取得专业
            var dtIsHaveSixSpecialtyList = new DataView(dtIsHaveSix).ToTable("Specialty", true, "Specialty");

            int countIsHaveSix = dtIsHaveSixSpecialtyList.Rows.Count;

            //声明创建Table 表头
            StringBuilder sb_HeadIsHaveSix = new StringBuilder();
            sb_HeadIsHaveSix.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_HeadIsHaveSix.Append("<tr><td colspan=" + (countIsHaveSix + 2) + " style=\"width: 100%; text-align: center; background-color: White;\"> 地上单建水泵房产值分配比例（有暖通专业）%</td> </tr>");
            sb_HeadIsHaveSix.Append("<tr>");
            sb_HeadIsHaveSix.Append("<td width=\"15%\">专业</td>");

            //比例
            decimal percentIsHaveSix = decimal.Divide(75, countIsHaveSix);
            percentIsHaveSix = decimal.Round(percentIsHaveSix, 2);

            for (int i = 0; i < countIsHaveSix; i++)
            {
                sb_HeadIsHaveSix.Append("<td width=" + percentIsHaveSix + "%>" + dtIsHaveSixSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            sb_HeadIsHaveSix.Append("<td width=\"10%\">编辑</td>");
            sb_HeadIsHaveSix.Append("</tr>");
            sb_HeadIsHaveSix.Append("</table>");


            //内容
            StringBuilder sb_HtmlIsHaveSix = new StringBuilder();
            sb_HtmlIsHaveSix.Append("<table id=\"gvSeven\" width=\"100%\" class=\"gridView_comm\">");
            sb_HtmlIsHaveSix.Append("<tr>");
            sb_HtmlIsHaveSix.Append("<td width=\"15%\">比例</td>");
            for (int j = 0; j < dtIsHaveSix.Rows.Count; j++)
            {
                sb_HtmlIsHaveSix.Append("<td width=" + percentIsHaveSix + "%>" + dtIsHaveSix.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_HtmlIsHaveSix.Append("<td width=\"10%\"><a href=\"#\" class=\"cls_IsHaveServenselect\">编辑</a></td>");
            sb_HtmlIsHaveSix.Append("</tr>");
            sb_HtmlIsHaveSix.Append("</table>");


            StringBuilder sb_EidtIsHaveSix = new StringBuilder();
            sb_EidtIsHaveSix.Append(" <div class=\"cls_data\" id=\"divIsHaveServenEdit\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>地上单建水泵房产值分配-编辑（有暖通专业）</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 设计阶段 </td> <td> <span id=\"lblServenType\" ></span> </td></tr>");


            for (int i = 0; i < dtIsHaveSixSpecialtyList.Rows.Count; i++)
            {
                sb_EidtIsHaveSix.Append("<tr>");
                sb_EidtIsHaveSix.Append("<td>" + dtIsHaveSixSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_EidtIsHaveSix.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanSevenPercent\">请输入整数！</span> </td>");
                sb_EidtIsHaveSix.Append("</tr>");
            }

            sb_EidtIsHaveSix.Append(" <tr><td> &nbsp;  </td> <td> <img src=\"../Images/buttons/btn_ok.gif\" id=\"btnIsHaveSeven\" style=\"cursor: pointer\"></td></tr>");
            sb_EidtIsHaveSix.Append("</table> </fieldset></div>");


            sbSix.Append(sb_HeadIsHaveSix.ToString());
            sbSix.Append(sb_HtmlIsHaveSix.ToString());
            sbSix.Append(sb_EidtIsHaveSix.ToString());
            lbl_Seven.Text = sbSix.ToString();

            #endregion

            #region 取得表7的值
            var dvSeven = new DataView(dt) { RowFilter = "status=7 and IsHaved=0" };
            var dtSeven = dvSeven.ToTable();

            //取得专业
            var dtSevenSpecialtyList = new DataView(dtSeven).ToTable("Specialty", true, "Specialty");

            int countSeven = dtSixSpecialtyList.Rows.Count;

            //声明创建Table 表头
            StringBuilder sb_HeadSeven = new StringBuilder();
            sb_HeadSeven.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_HeadSeven.Append("<tr><td colspan=" + (countSeven + 2) + " style=\"width: 100%; text-align: center; background-color: White;\">  地上单建变配电所（室）产值分配比例（无暖通专业）%</td> </tr>");
            sb_HeadSeven.Append("<tr>");
            sb_HeadSeven.Append("<td width=\"16%\">专业</td>");

            //比例
            decimal percentSeven = decimal.Divide(74, countSeven);
            percentSeven = decimal.Round(percentSeven, 2);

            for (int i = 0; i < countSeven; i++)
            {
                sb_HeadSeven.Append("<td width=" + percentSeven + "%>" + dtSevenSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            sb_HeadSeven.Append("<td width=\"10%\">编辑</td>");
            sb_HeadSeven.Append("</tr>");
            sb_HeadSeven.Append("</table>");


            //内容
            StringBuilder sb_HtmlSeven = new StringBuilder();
            sb_HtmlSeven.Append("<table id=\"gvEight\" width=\"100%\" class=\"gridView_comm\">");
            sb_HtmlSeven.Append("<tr>");
            sb_HtmlSeven.Append("<td width=\"16%\">比例</td>");
            for (int j = 0; j < dtSeven.Rows.Count; j++)
            {
                sb_HtmlSeven.Append("<td width=" + percentSeven + "%>" + dtSeven.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_HtmlSeven.Append("<td width=\"10%\"><a href=\"#\" class=\"cls_Eightselect\">编辑</a> </td>");
            sb_HtmlSeven.Append("</tr>");
            sb_HtmlSeven.Append("</table>");


            StringBuilder sb_EidtSeven = new StringBuilder();
            sb_EidtSeven.Append(" <div class=\"cls_data\" id=\"divEightEidt\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>地上单建水泵房产值分配-编辑（无暖通专业）</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 专业 </td> <td> <span ID=\"lblEightType\" ></span> </td></tr>");


            for (int i = 0; i < dtSevenSpecialtyList.Rows.Count; i++)
            {
                sb_EidtSeven.Append("<tr>");
                sb_EidtSeven.Append("<td>" + dtSevenSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_EidtSeven.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanEightPercent\">请输入整数！</span> </td>");
                sb_EidtSeven.Append("</tr>");
            }

            sb_EidtSeven.Append(" <tr><td> &nbsp;  </td> <td> <img src=\"../Images/buttons/btn_ok.gif\" id=\"btnEight\" style=\"cursor: pointer\"></td></tr>");
            sb_EidtSeven.Append("</table> </fieldset></div>");

            StringBuilder sbSeven = new StringBuilder();
            sbSeven.Append(sb_HeadSeven.ToString());
            sbSeven.Append(sb_HtmlSeven.ToString());
            sbSeven.Append(sb_EidtSeven.ToString());

            var dvIsHaveSeven = new DataView(dt) { RowFilter = "status=7 and IsHaved=1" };
            var dtIsHaveSeven = dvIsHaveSeven.ToTable();

            //取得专业
            var dtIsHaveSevenSpecialtyList = new DataView(dtIsHaveSeven).ToTable("Specialty", true, "Specialty");

            int countIsHaveSeven = dtIsHaveSixSpecialtyList.Rows.Count;

            //声明创建Table 表头
            StringBuilder sb_HeadIsHaveSeven = new StringBuilder();
            sb_HeadIsHaveSeven.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_HeadIsHaveSeven.Append("<tr><td colspan=" + (countIsHaveSeven + 2) + " style=\"width: 100%; text-align: center; background-color: White;\">  地上单建变配电所（室）产值分配比例(有暖通专业)%</td> </tr>");
            sb_HeadIsHaveSeven.Append("<tr>");
            sb_HeadIsHaveSeven.Append("<td width=\"15%\">专业</td>");

            //比例
            decimal percentIsHaveSeven = decimal.Divide(75, countIsHaveSeven);
            percentIsHaveSeven = decimal.Round(percentIsHaveSeven, 2);

            for (int i = 0; i < countIsHaveSeven; i++)
            {
                sb_HeadIsHaveSeven.Append("<td width=" + percentIsHaveSeven + "%>" + dtIsHaveSevenSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            sb_HeadIsHaveSeven.Append("<td width=\"10%\">编辑</td>");
            sb_HeadIsHaveSeven.Append("</tr>");
            sb_HeadIsHaveSeven.Append("</table>");


            //内容
            StringBuilder sb_HtmlIsHaveSeven = new StringBuilder();
            sb_HtmlIsHaveSeven.Append("<table id=\"gvEight\" width=\"100%\" class=\"gridView_comm\">");
            sb_HtmlIsHaveSeven.Append("<tr>");
            sb_HtmlIsHaveSeven.Append("<td width=\"15%\">比例</td>");
            for (int j = 0; j < dtIsHaveSeven.Rows.Count; j++)
            {
                sb_HtmlIsHaveSeven.Append("<td width=" + percentIsHaveSeven + "%>" + dtIsHaveSeven.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_HtmlIsHaveSeven.Append("<td width=\"10%\"><a href=\"###\" class=\"cls_IsHaveEightselect\">编辑</a> </td>");
            sb_HtmlIsHaveSeven.Append("</tr>");
            sb_HtmlIsHaveSeven.Append("</table>");


            StringBuilder sb_EidtIsHaveSeven = new StringBuilder();
            sb_EidtIsHaveSeven.Append(" <div class=\"cls_data\" id=\"divIsHaveEightEidt\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>地上单建水泵房产值分配-编辑（有暖通专业）</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 专业 </td> <td> <span ID=\"lblEightType\" ></span> </td></tr>");


            for (int i = 0; i < dtIsHaveSevenSpecialtyList.Rows.Count; i++)
            {
                sb_EidtIsHaveSeven.Append("<tr>");
                sb_EidtIsHaveSeven.Append("<td>" + dtIsHaveSevenSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_EidtIsHaveSeven.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanEightPercent\">请输入整数！</span> </td>");
                sb_EidtIsHaveSeven.Append("</tr>");
            }

            sb_EidtIsHaveSeven.Append(" <tr><td> &nbsp;  </td> <td> <img src=\"../Images/buttons/btn_ok.gif\" id=\"btnIsHaveEight\" style=\"cursor: pointer\"></td></tr>");
            sb_EidtIsHaveSeven.Append("</table> </fieldset></div>");


            sbSeven.Append(sb_HeadIsHaveSeven.ToString());
            sbSeven.Append(sb_HtmlIsHaveSeven.ToString());
            sbSeven.Append(sb_EidtIsHaveSeven.ToString());
            lbl_Eight.Text = sbSeven.ToString();

            #endregion

            #region 取得表8的值
            var dvEight = new DataView(dt) { RowFilter = "status=8 and IsHaved=0" };
            var dtEight = dvEight.ToTable();

            //取得专业
            var dtEightSpecialtyList = new DataView(dtEight).ToTable("Specialty", true, "Specialty");

            int countEight = dtEightSpecialtyList.Rows.Count;

            //声明创建Table 表头
            StringBuilder sb_HeadEight = new StringBuilder();
            sb_HeadEight.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_HeadEight.Append("<tr><td colspan=" + (countEight + 2) + " style=\"width: 100%; text-align: center; background-color: White;\">   单建地下室（车库）产值分配比例(无暖通专业)%</td> </tr>");
            sb_HeadEight.Append("<tr>");
            sb_HeadEight.Append("<td width=\"16%\">专业</td>");

            //比例
            decimal percentEight = decimal.Divide(74, count);
            percentEight = decimal.Round(percentEight, 2);

            for (int i = 0; i < countEight; i++)
            {
                sb_HeadEight.Append("<td width=" + percentEight + "%>" + dtEightSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            sb_HeadEight.Append("<td width=\"10%\">编辑</td>");
            sb_HeadEight.Append("</tr>");
            sb_HeadEight.Append("</table>");


            //内容
            StringBuilder sb_HtmlEight = new StringBuilder();
            sb_HtmlEight.Append("<table id=\"gvNine\" width=\"100%\" class=\"gridView_comm\">");
            sb_HtmlEight.Append("<tr>");
            sb_HtmlEight.Append("<td width=\"16%\">比例 </td>");
            for (int j = 0; j < dtEight.Rows.Count; j++)
            {
                sb_HtmlEight.Append("<td width=" + percentEight + "%>" + dtEight.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_HtmlEight.Append("<td width=\"10%\" ><a href=\"#\" class=\"cls_Nineselect\">编辑</a></td>");
            sb_HtmlEight.Append("</tr>");
            sb_HtmlEight.Append("</table>");


            StringBuilder sb_EidtEight = new StringBuilder();
            sb_EidtEight.Append(" <div class=\"cls_data\" id=\"divNineEidt\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>单建地下室（车库）产值分配-编辑（无暖通专业）</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 专业 </td> <td> <span ID=\"lblNineType\" ></span> </td></tr>");


            for (int i = 0; i < dtEightSpecialtyList.Rows.Count; i++)
            {
                sb_EidtEight.Append("<tr>");
                sb_EidtEight.Append("<td>" + dtSevenSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_EidtEight.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanNinePercent\">请输入整数！</span> </td>");
                sb_EidtEight.Append("</tr>");
            }

            sb_EidtEight.Append(" <tr><td> &nbsp;  </td> <td> <img src=\"../Images/buttons/btn_ok.gif\" id=\"btnNine\" style=\"cursor: pointer\"></td></tr>");
            sb_EidtEight.Append("</table> </fieldset></div>");

            StringBuilder sbEight = new StringBuilder();
            sbEight.Append(sb_HeadEight.ToString());
            sbEight.Append(sb_HtmlEight.ToString());
            sbEight.Append(sb_EidtEight.ToString());

            var dvIsHaveEight = new DataView(dt) { RowFilter = "status=8 and IsHaved=1" };
            var dtIsHaveEight = dvIsHaveEight.ToTable();

            //取得专业
            var dtIsHaveEightSpecialtyList = new DataView(dtIsHaveEight).ToTable("Specialty", true, "Specialty");

            int countIsHaveEight = dtIsHaveEightSpecialtyList.Rows.Count;

            //声明创建Table 表头
            StringBuilder sb_HeadIsHaveEight = new StringBuilder();
            sb_HeadIsHaveEight.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_HeadIsHaveEight.Append("<tr><td colspan=" + (countIsHaveEight + 2) + " style=\"width: 100%; text-align: center; background-color: White;\">   单建地下室（车库）产值分配比例(无暖通专业)%</td> </tr>");
            sb_HeadIsHaveEight.Append("<tr>");
            sb_HeadIsHaveEight.Append("<td width=\"15%\">专业</td>");

            //比例
            decimal percentIsHaveEight = decimal.Divide(75, countIsHaveEight);
            percentIsHaveEight = decimal.Round(percentIsHaveEight, 2);

            for (int i = 0; i < countIsHaveEight; i++)
            {
                sb_HeadIsHaveEight.Append("<td width=" + percentIsHaveEight + "%>" + dtIsHaveEightSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            sb_HeadIsHaveEight.Append("<td width=\"10%\">编辑</td>");
            sb_HeadIsHaveEight.Append("</tr>");
            sb_HeadIsHaveEight.Append("</table>");


            //内容
            StringBuilder sb_HtmlIsHaveEight = new StringBuilder();
            sb_HtmlIsHaveEight.Append("<table id=\"gvIsHaveNine\" width=\"100%\" class=\"gridView_comm\">");
            sb_HtmlIsHaveEight.Append("<tr>");
            sb_HtmlIsHaveEight.Append("<td width=\"15%\">比例 </td>");
            for (int j = 0; j < dtIsHaveEight.Rows.Count; j++)
            {
                sb_HtmlIsHaveEight.Append("<td width=" + percentIsHaveEight + "%>" + dtIsHaveEight.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_HtmlIsHaveEight.Append("<td width=\"10%\" ><a href=\"#\" class=\"cls_IsHaveNineselect\">编辑</a></td>");
            sb_HtmlIsHaveEight.Append("</tr>");
            sb_HtmlIsHaveEight.Append("</table>");


            StringBuilder sb_EidtIsHaveEight = new StringBuilder();
            sb_EidtIsHaveEight.Append(" <div class=\"cls_data\" id=\"divIsHaveNineEidt\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>单建地下室（车库）产值分配-编辑（有暖通专业）</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 专业 </td> <td> <span ID=\"lblNineType\" ></span> </td></tr>");


            for (int i = 0; i < dtIsHaveEightSpecialtyList.Rows.Count; i++)
            {
                sb_EidtIsHaveEight.Append("<tr>");
                sb_EidtIsHaveEight.Append("<td>" + dtIsHaveEightSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_EidtIsHaveEight.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanNinePercent\">请输入整数！</span> </td>");
                sb_EidtIsHaveEight.Append("</tr>");
            }

            sb_EidtIsHaveEight.Append(" <tr><td> &nbsp;  </td> <td> <img src=\"../Images/buttons/btn_ok.gif\" id=\"btnIsHaveNine\" style=\"cursor: pointer\"></td></tr>");
            sb_EidtIsHaveEight.Append("</table> </fieldset></div>");

            sbEight.Append(sb_HeadIsHaveEight.ToString());
            sbEight.Append(sb_HtmlIsHaveEight.ToString());
            sbEight.Append(sb_EidtIsHaveEight.ToString());

            lbl_Nine.Text = sbEight.ToString();

            #endregion

            #region 取得表9的值
            var dvNine = new DataView(dt) { RowFilter = "status=9" };
            var dtNine = dvNine.ToTable();

            //取得专业
            var dtNineSpecialtyList = new DataView(dtNine).ToTable("Specialty", true, "Specialty");

            int countNine = dtNineSpecialtyList.Rows.Count;

            //声明创建Table 表头
            StringBuilder sb_HeadNine = new StringBuilder();
            sb_HeadNine.Append("<table class=\"cls_content_head\"  style=\"text-align: center;\">");
            sb_HeadNine.Append("<tr><td colspan=" + (count + 2) + " style=\"width: 100%; text-align: center; background-color: White;\">   市政道路工程产值分配比例%</td> </tr>");
            sb_HeadNine.Append("<tr>");
            sb_HeadNine.Append("<td width=\"15%\">专业</td>");

            //比例
            decimal percentNine = decimal.Divide(85, count);
            percentNine = decimal.Round(percentNine, 2);

            for (int i = 0; i < countNine; i++)
            {
                sb_HeadNine.Append("<td width=" + percentNine + "%>" + dtNineSpecialtyList.Rows[i][0].ToString() + "</td>");
            }

            sb_HeadNine.Append("<td width=\"10%\">编辑</td>");
            sb_HeadNine.Append("</tr>");
            sb_HeadNine.Append("</table>");


            //内容
            StringBuilder sb_HtmlNine = new StringBuilder();
            sb_HtmlNine.Append("<table id=\"gvTen\" width=\"100%\" class=\"gridView_comm\">");
            sb_HtmlNine.Append("<tr>");
            sb_HtmlNine.Append("<td width=\"15%\">比例 </td>");
            for (int j = 0; j < dtNine.Rows.Count; j++)
            {
                sb_HtmlNine.Append("<td width=" + percentNine + "%>" + dtNine.Rows[j]["SpecialtyPercent"].ToString() + "</td>");
            }
            sb_HtmlNine.Append("<td width=\"10%\" ><a href=\"#\" class=\"cls_Tenselect\">编辑</a></td>");
            sb_HtmlNine.Append("</tr>");
            sb_HtmlNine.Append("</table>");


            StringBuilder sb_EidtNine = new StringBuilder();
            sb_EidtNine.Append(" <div class=\"cls_data\" id=\"divTenEdit\" style=\"display: none\"><fieldset style=\"font-size: 12px;\"> <legend>市政道路工程产值分配-编辑</legend><table style=\"width: 100%;\"> <tr> <td style=\"width: 100px;\"> 专业 </td> <td> <span ID=\"lblTenType\" ></span> </td></tr>");


            for (int i = 0; i < dtNineSpecialtyList.Rows.Count; i++)
            {
                sb_EidtNine.Append("<tr>");
                sb_EidtNine.Append("<td>" + dtNineSpecialtyList.Rows[i][0].ToString() + "：</td>");
                sb_EidtNine.Append(" <td><input maxlength=\"10\" type=\"text\" class=\"cls_input_text\" />(%)<span style=\"color: red; display: none;\" id=\"spanTenPercent\">请输入整数！</span> </td>");
                sb_EidtNine.Append("</tr>");
            }

            sb_EidtNine.Append(" <tr><td> &nbsp;  </td> <td> <img src=\"../Images/buttons/btn_ok.gif\" id=\"btnTen\" style=\"cursor: pointer\"></td></tr>");
            sb_EidtNine.Append("</table> </fieldset></div>");

            StringBuilder sbNine = new StringBuilder();
            sbNine.Append(sb_HeadNine.ToString());
            sbNine.Append(sb_HtmlNine.ToString());
            sbNine.Append(sb_EidtNine.ToString());

            lbl_Ten.Text = sbNine.ToString();

            #endregion
        }
    }
}