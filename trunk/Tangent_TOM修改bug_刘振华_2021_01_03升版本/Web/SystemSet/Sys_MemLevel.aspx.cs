﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;
using TG.DBUtility;

namespace TG.Web.SystemSet
{
    public partial class Sys_MemLevel : PageBase
    {

        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();

        protected DataTable ResultTable
        { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //人员信息
                BindMem();
                //部门信息
                BindUnit();
                //默认加载当前日期
                SelectCurYear();
            }
        }
        /// <summary>
        /// 选中当前
        /// </summary>
        private void SelectCurYear()
        {
            this.drp_year.ClearSelection();

            int curyear = DateTime.Now.Year;

            if (this.drp_year.Items.FindByValue(curyear.ToString()) != null)
            {
                this.drp_year.Items.FindByValue(curyear.ToString()).Selected = true;
            }
        }

        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }

        //绑定角色信息
        protected void BindMem()
        {
            StringBuilder sb = new StringBuilder("");
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.AppendFormat(" AND (A.mem_Name Like '%{0}%' or mem_Login Like '%{0}%') ", keyname);
            }
            //单位
            if (this.drp_unit3.SelectedIndex > 0)
            {
                sb.AppendFormat(" AND A.mem_Unit_ID={0}", this.drp_unit3.SelectedValue);
            }
            //显示在职和离职时间大于当前时间
            sb.Append(" and (mem_isFired=0 or A.mem_ID in (select mem_ID from tg_memberExt where year(mem_OutTime)>=" + this.drp_year.SelectedValue + "))");
            //角色数量
            this.AspNetPager1.RecordCount = int.Parse(this.GetRecordCount(sb.ToString()).ToString());
            //排序
            string orderby = " (select unit_Order from tg_unitExt ue where ue.unit_ID=A.mem_Unit_ID) asc,mem_Order asc";
            //绑定
            this.ResultTable = this.GetListByPage(sb.ToString(), orderby, this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize - 1);
        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();

            this.drp_unit3.DataSource = bll_unit.GetList(" unit_ParentID<>0 order by (select unit_Order from tg_unitExt ue where ue.unit_ID=tg_unit.unit_ID) asc");
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }

        public DataTable GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            string strSql = string.Format(@"Select A.mem_ID,mem_Name
	                                        ,(select unit_Name from tg_unit where unit_ID=A.mem_Unit_ID) AS UnitName 
	                                        ,mem_Unit_ID
                                            ,mem_Login
	                                        ,isnull(B.mem_Year,0) AS memYear
	                                        ,isnull(B.mem_Level,0) AS memLevel
                                            From tg_member A left join (Select mem_Year,mem_Level,mem_ID From tg_memberLevel Where mem_Year={1}) B on A.mem_ID=B.mem_ID 
                                            Where 1=1 {0} order by {2}", strWhere, this.drp_year.SelectedValue,orderby);

            return DbHelperSQL.Query(strSql, startIndex, endIndex).Tables[0];
        }
        //获取的
        public int GetRecordCount(string strWhere)
        {
            string strSql = string.Format(@"Select Count(A.mem_ID)
                                            From tg_member A left join (Select mem_Year,mem_Level,mem_ID From tg_memberLevel Where mem_Year={1}) B on A.mem_ID=B.mem_ID 
                                            Where 1=1 {0}", strWhere, this.drp_year.SelectedValue);

            return Convert.ToInt32(DbHelperSQL.GetSingle(strSql));
        }


        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindMem();
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindMem();
        }

        //生产部门改变重新绑定数据
        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataSetPageIndex();
        }
        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>
        protected void BindDataSetPageIndex()
        {
            this.AspNetPager1.CurrentPageIndex = 0;
            //绑定数据
            BindMem();
        }

        protected void drp_year_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}