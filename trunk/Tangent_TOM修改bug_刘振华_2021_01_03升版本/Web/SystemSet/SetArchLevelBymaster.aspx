﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="SetArchLevelBymaster.aspx.cs" Inherits="TG.Web.SystemSet.SetArchLevelBymaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" />
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            //删除是判断有无记录选中
            $("#<%=btn_DelCst.ClientID%>").click(function () {
                if ($("#<%=grid_level.ClientID%> .checked").length == 0) {
                    jAlert("请选择要删除的部门！", "提示");
                    return false;
                }
                //判断是否要删除
                return confirm("是否要删除职称信息？");
            });

            //编辑
            $(".cls_select").click(function () {
                window.setTimeout("scrollToBottom()", 500); //500毫秒延迟加载
                $("#div_add").hide("slow");
                $("#div_edit").show("slow");
            });
            //编辑-保存
            $("#<%=btn_edit.ClientID%>").click(function () {
                var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
                var msg = "";
                //职称名称
                if ($("#<%=txt_archName0.ClientID%>").val() == "") {
                    msg += "职称名不能为空！<br/>";
                }
                else {
                    if ($("#<%=txt_archName0.ClientID%>").val().length > 15) {
                        msg += "职称名太长！<br/>";
                    }
                }
                //分配比例
                if ($("#<%=txt_allotprt0.ClientID%>").val() == "") {
                    msg += "分配比例不能为空！<br/>";
                }
                else {
                    if (!reg_math.test($("#<%=txt_allotprt0.ClientID%>").val())) {
                        msg += "请输入数字！<br/>";
                    }
                }
                //排序
                if ($("#<%=drp_order0.ClientID%>").val() == "0") {
                    msg += "请选择职位排序！<br/>";
                }
                //描述
                if ($("#<%=txt_archInfo0.ClientID%>").val().length > 30) {
                    msg += "部门简介输入过长！";
                }
                if (msg != "") {
                    jAlert(msg, "提示");
                    return false;
                }
            });
            //选择
            $(".cls_select").click(function () {
                $("#<%=lbl_archid.ClientID%>").text($.trim($(this).parent().parent().find("TD").eq(1).text()));
                $("#<%=hid_archid.ClientID%>").val($.trim($(this).parent().parent().find("TD").eq(1).text()));
                $("#<%=txt_archName0.ClientID%>").val($.trim($(this).parent().parent().find("TD").eq(2).text()));
                $("#<%=txt_allotprt0.ClientID%>").val($.trim($(this).parent().parent().find("TD").eq(3).text()));
                $("#<%=drp_order0.ClientID%>").val($.trim($(this).parent().parent().find("TD").eq(4).text()));
                $("#<%=txt_archInfo0.ClientID%>").val($.trim($(this).parent().parent().find("TD").eq(5).text()));
                var showname = $.trim($(this).parent().parent().find("TD").eq(4).text());
                if (showname == "显示") {
                    $("#raBtnshow").attr("checked", "checked");
                }
                else {
                    $("#raBtnHidd").attr("checked", "checked");
                }
            });

            //隐藏
            $(" .btn_cancel").click(function () {
                $("#div_add").hide("slow");
                $("#div_edit").hide("slow");
            });

            //添加
            $("#btn_showadd").click(function () {
                window.setTimeout("scrollToBottom()", 500); //500毫秒延迟加载
                $("#div_add").show("slow");
                $("#div_edit").hide("slow");
            });
            //添加保存
            $("#<%=btn_save.ClientID%>").click(function () {
                var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
                var msg = "";
                //职称名称
                if ($("#<%=txt_archName.ClientID%>").val() == "") {
                    msg += "职称名不能为空！<br/>";
                }
                else {
                    if ($("#<%=txt_archName.ClientID%>").val().length > 15) {
                        msg += "职称名太长！<br/>";
                    }
                }
                //分配比例
                if ($("#<%=txt_allotprt.ClientID%>").val() == "") {
                    msg += "分配比例不能为空！<br/>";
                }
                else {
                    if (!reg_math.test($("#<%=txt_allotprt.ClientID%>").val())) {
                        msg += "请输入数字！<br/>";
                    }
                }
                //排序
                if ($("#<%=drp_order.ClientID%>").val() == "0") {
                    msg += "请选择职位排序！<br/>";
                }
                //描述
                if ($("#<%=txt_archInfo.ClientID%>").val().length > 30) {
                    msg += "部门简介输入过长！";
                }
                if (msg != "") {
                    jAlert(msg, "提示");
                    return false;
                }
            });



            //全选-反选
            $('#<%=grid_level.ClientID%>_ctl01_chkAll').click(function () {

                var checks = $('#<%=grid_level.ClientID%> :checkbox');

                if ($('#<%=grid_level.ClientID%>_ctl01_chkAll').attr("checked") == "checked") {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.setAttribute("class", "checked");
                        $(checks[i]).attr("checked", "checked");
                    }
                }
                else {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.removeAttribute("class");
                        $(checks[i]).removeAttr("checked");
                    }
                }
            });
        });
        function scrollToBottom() {
            window.scrollTo(0, document.body.scrollHeight); //移动屏幕最下方
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>建筑职称设置 </small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用设置</a><i class="fa fa-angle-right"> </i><a>建筑职称设置</a>
    </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>职称搜索
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                    	<tr>
                    		<td>职称名称:</td>
                            <td><input id="txt_keyname" name="txt_keyname" type="text" runat="Server" class="form-control" /></td>
                            <td><asp:Button ID="btn_Search" runat="server" CssClass="btn blue" Text="查询" OnClick="btn_Search_Click" /></td>
                            <td><input id="btn_showadd" type="button" name="name" value="添加" class="btn blue" /></td>
                            <td><asp:Button ID="btn_DelCst" runat="server" CssClass="btn blue" Text="删除" OnClick="btn_DelCst_Click" /></td>
                    	</tr>
                    </table>
               
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>职称信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <asp:GridView ID="grid_level" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                        CssClass="table table-striped table-bordered table-hover dataTable" Width="100%"
                        EnableModelValidation="True">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_id" runat="server" CssClass="cls_chk" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="5%" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="ArchLevel_ID" HeaderText="编号">
                                <ItemStyle HorizontalAlign="Left" Width="5%" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="职称名称">
                                <ItemTemplate>
                                    <img src="../Images/part.png" style="width: 16px; height: 16px;" />
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="35%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="产值比例">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("AllotPrt") %>'></asp:Label>
                                    <asp:HiddenField ID="hid_prtid" runat="server" Value='<%# Bind("AllotPrt") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="15%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="职称排序">
                                <ItemTemplate>
                                    <a href="###" class="cls_unitshow" alt='<%# Eval("LevelOrder") %>'>
                                        <%# Eval("LevelOrder") %></a>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Info" HeaderText="职称描述">
                                <ItemStyle HorizontalAlign="Left" Width="25%" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="选择">
                                <ItemTemplate>
                                    <a href="javascript:;" class="cls_select">编辑</a>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="5%" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            没有数据！
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                        CustomInfoHTML="共%PageCount%页，当前为第%CurrentPageIndex%页，每页%PageSize%条" CustomInfoTextAlign="Left"
                        FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" OnPageChanged="AspNetPager1_PageChanged"
                        PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox" PrevPageText="上一页" ShowBoxThreshold="10"
                        ShowCustomInfoSection="Left" ShowPageIndexBox="Auto" SubmitButtonText="Go" TextAfterPageIndexBox="页"
                        TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;" PageSize="18" SubmitButtonClass="submitbtn">
                    </webdiyer:AspNetPager>
                </div>
            </div>
        </div>
    </div>
    <div class="cls_data" id="div_add" style="display: none;">
        <div class="row">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>添加职称
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-1">
                                职称名称:</label>
                            <div class="col-md-2">
                                <asp:TextBox ID="txt_archName" runat="server" CssClass="form-control" MaxLength="30"></asp:TextBox>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-1">
                                产值比例:</label>
                            <div class="col-md-2">
                                <asp:TextBox ID="txt_allotprt" runat="server" CssClass="form-control"></asp:TextBox>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-1">
                                %</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-1">
                                职称排序:</label>
                            <div class="col-md-2">
                                <asp:DropDownList ID="drp_order" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0">--请选择--</asp:ListItem>
                                </asp:DropDownList>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-1">
                                部门描述:</label>
                            <div class="col-md-2">
                                <asp:TextBox ID="txt_archInfo" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btn_save" runat="server" Text="保存" OnClick="btn_save_Click" CssClass="btn green" />
                            <input type="button" name="btn_cancel" value="取消" class="btn default btn_cancel" />
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cls_data" id="div_edit" style="display: none;">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>编辑职称
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body">
                <table border="0" cellspacing="0" cellpadding="0" width="400" align="center">
                    <tr>
                        <td>
                            ID:
                        </td>
                        <td>
                            <asp:Label ID="lbl_archid" runat="server"></asp:Label>
                            <asp:HiddenField ID="hid_archid" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            职称名称:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_archName0" runat="server" CssClass="form-control" MaxLength="30"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            产值比例:
                        </td>
                        <td>
                            <div class="input-group">
                                <asp:TextBox ID="txt_allotprt0" runat="server" CssClass="form-control"></asp:TextBox>%
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            职称排序:
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_order0" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0">--请选择--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            部门描述:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_archInfo0" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btn_edit" runat="server" Text="保存" CssClass="btn green" OnClick="btn_edit_Click" />
                            &nbsp;
                            <input type="button" name="btn_cancel" value="取消" class="btn default btn_cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
