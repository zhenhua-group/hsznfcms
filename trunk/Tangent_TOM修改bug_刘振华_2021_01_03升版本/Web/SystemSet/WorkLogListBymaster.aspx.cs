﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class WorkLogListBymaster : PageBase
    {
        TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();

        //用户ID
        public int SysMemNo
        {
            get
            {
                return Convert.ToInt32(this.UserInfo["memid"] ?? "0");
            }
        }

        //用户登录名
        public string SysMemLogin
        {
            get
            {
                return Convert.ToString(UserInfo["memLogin"] ?? "");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindWorkLog();
            }
        }

        //绑定公告
        protected void BindWorkLog()
        {
            //绑定个人日志
            StringBuilder sb = new StringBuilder();
            sb.Append(" Select * From cm_WorkLog Where 1=1 ");
            string txt_value = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
            //管理员
            if (SysMemLogin == base.SystemManagerName)
            {
                if (txt_value != "")
                {
                    sb.AppendFormat("  and (Title LIKE N'%{0}%')", txt_value);
                }
            }
            else //个人
            {
                if (txt_value != "")
                {
                    sb.AppendFormat("  and (InUser={0}) AND (Title LIKE N'{1}')", SysMemNo, txt_value);
                }
            }

            //所有记录数
            this.AspNetPager1.RecordCount = bll_db.GetDataPageListCount(sb.ToString());
            sb.Append(" ORDER BY SysNo DESC ");
            //绑定数据
            this.GridView1.DataSource = bll_db.GetDataPageList(sb.ToString(), this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize);
            this.GridView1.DataBind();
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindWorkLog();
        }

        protected void btn_DelCst_Click(object sender,  EventArgs e)
        {
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            for (int i = 0; i < this.GridView1.Rows.Count; i++)
            {
                string id = ((HiddenField)this.GridView1.Rows[i].Cells[0].FindControl("HiddenField1")).Value;
                CheckBox chk = this.GridView1.Rows[i].Cells[0].FindControl("CheckBox1") as CheckBox;
                string strSql = " Delete From cm_WorkLog Where SysNo=" + id;
                if (chk.Checked)
                {
                    bll_db.ExcuteBySql(strSql);
                }
            }

            //重新绑定
            BindWorkLog();
        }
        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string mem_id = e.Row.Cells[3].Text;
                if (mem_id != "")
                {
                    e.Row.Cells[3].Text = bll_mem.GetModel(int.Parse(mem_id)).mem_Name;
                }
            }
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindWorkLog();
        }
    }
}