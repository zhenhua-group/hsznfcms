﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class Sys_dictionaryBymaster : System.Web.UI.Page
    {
        TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
                BindDropList();
            }
        }

        TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
        /// <summary>
        /// 绑定字典数据
        /// </summary>
        public void BindData()
        {
            StringBuilder sb = new StringBuilder("1=1");
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.AppendFormat(" AND dic_name='{0}'", keyname);
            }
            if (this.drp_typelist.SelectedValue != "-1")
            {
                sb.AppendFormat(" AND dic_type='{0}'", this.drp_typelist.SelectedItem.Value);
            }
            //记录数
            this.AspNetPager1.RecordCount = bll_dic.GetRecordCount(sb.ToString());
            //分页
            grid_unit.DataSource = bll_dic.GetListByPage(sb.ToString(), "", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize); ;
            grid_unit.DataBind();
        }
        /// <summary>
        /// 查询字典
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindData();
        }
        /// <summary>
        /// 删除字典
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_DelCst_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.grid_unit.Rows.Count; i++)
            {
                CheckBox ck = this.grid_unit.Rows[i].Cells[0].FindControl("chk_id") as CheckBox;
                string str_dicId = this.grid_unit.Rows[i].Cells[1].Text;
                if (ck.Checked)
                {
                    dic.Delete(int.Parse(str_dicId));
                }
            }
            TG.Common.MessageBox.ShowAndRedirect(this, "字典删除成功", "Sys_dictionaryBymaster.aspx");
        }
        /// <summary>
        /// 添加字典
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_save_Click(object sender, EventArgs e)
        {
            TG.Model.cm_Dictionary model_dic = new TG.Model.cm_Dictionary();
            model_dic.dic_Name = this.txt_dicName.Text;
            model_dic.dic_Type = this.txt_dicType.Text;
            model_dic.dic_info = this.txt_dicDescrip.Text;
            dic.Add(model_dic);
            TG.Common.MessageBox.ShowAndRedirect(this, "字典添加成功！", "Sys_dictionaryBymaster.aspx");
        }
        /// <summary>
        /// 修改字典信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_edit_Click(object sender, EventArgs e)
        {
            TG.Model.cm_Dictionary model_dic = new TG.Model.cm_Dictionary();
            if (this.hid_dicid.Value != "")
            {
                model_dic.ID = int.Parse(this.hid_dicid.Value);
                model_dic.dic_Name = this.txt_dicName0.Text;
                model_dic.dic_Type = this.txt_dicType0.Text;
                model_dic.dic_info = this.txt_dicDescript.Text;
                dic.Update(model_dic);
                TG.Common.MessageBox.ShowAndRedirect(this, "字典修改成功！", "Sys_dictionaryBymaster.aspx");
            }
        }
        //绑定list
        protected void BindDropList()
        {
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            string strSql = " SELECT DISTINCT dic_info,dic_type FROM dbo.cm_Dictionary";

            DataSet ds_dic = bll_db.GetList(strSql);
            this.drp_typelist.DataSource = ds_dic;
            this.drp_typelist.DataTextField = "dic_info";
            this.drp_typelist.DataValueField = "dic_type";
            this.drp_typelist.DataBind();
        }
        //改变查询
        protected void drp_typelist_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindData();
        }
    }
}