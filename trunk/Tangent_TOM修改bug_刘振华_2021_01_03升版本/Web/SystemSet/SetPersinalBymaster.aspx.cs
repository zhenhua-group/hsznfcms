﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class SetPersinalBymaster : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            string str_flag = Request.QueryString["flag"] ?? "";
            if (str_flag != "")
            {
                ViewState["flag"] = str_flag;
            }
            if (!IsPostBack)
            {
                string str_userid = Convert.ToString(UserInfo["memid"] ?? "0");
                //显示
                ShowUserInfo(str_userid);
                //编辑
                ShowUserEditInfo(str_userid);
            }
        }
        //显示或编辑
        public string Flag()
        {
            return Convert.ToString(ViewState["flag"] ?? "");
        }
        //显示个人信息
        protected void ShowUserInfo(string userid)
        {
            TG.BLL.tg_member bll = new TG.BLL.tg_member();
            TG.Model.tg_member model = bll.GetModel(int.Parse(userid));

            if (model != null)
            {
                this.lbl_ID.Text = model.mem_ID.ToString();
                this.lbl_Name.Text = model.mem_Name.ToString();
                this.lbl_login.Text = model.mem_Login.ToString();
                this.lbl_sex.Text = model.mem_Sex;
                this.lbl_birth.Text = model.mem_Birthday == null ? "" : Convert.ToDateTime(model.mem_Birthday).ToString("yyyy-MM-dd");
                this.lbl_unit.Text = "";
                if (model.mem_Unit_ID != 0)
                {
                    TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
                    TG.Model.tg_unit model_unit = bll_unit.GetModel(model.mem_Unit_ID);
                    this.lbl_unit.Text = model_unit.unit_Name;
                }
                this.lbl_spec.Text = "";
                if (model.mem_Speciality_ID.ToString() != "")
                {
                    TG.BLL.tg_speciality bll_spec = new TG.BLL.tg_speciality();
                    TG.Model.tg_speciality model_spec = bll_spec.GetModel(model.mem_Speciality_ID);
                    this.lbl_spec.Text = model_spec.spe_Name;
                }
                this.lbl_pric.Text = "";
                if (model.mem_Principalship_ID.ToString() != "")
                {
                    TG.BLL.tg_principalship bll_pric = new TG.BLL.tg_principalship();
                    TG.Model.tg_principalship model_pric = bll_pric.GetModel(model.mem_Principalship_ID);
                    this.lbl_pric.Text = model_pric.pri_Name;
                }
                this.lbl_mobile.Text = model.mem_Mobile;
                this.lbl_phone.Text = model.mem_Telephone;
                this.lbl_email.Text = model.mem_Mailbox;
                this.lbl_sign.Text = model.mem_sign;
                TG.Model.cm_EleSign es = new TG.BLL.cm_EleSign().GetModel2(UserSysNo);
                if (es != null)
                {
                    this.lbl_img.ImageUrl = es.elesign;
                    this.txt_pic0.Text = es.elesign;
                }
            }
        }
        //显示编辑资料
        protected void ShowUserEditInfo(string userid)
        {
            TG.BLL.tg_member bll = new TG.BLL.tg_member();
            TG.Model.tg_member model = bll.GetModel(int.Parse(userid));
            //头像
            if (model != null)
            {
                this.lbl_ID0.Text = model.mem_ID.ToString();
                this.lbl_Name0.Text = model.mem_Name.ToString();
                this.txt_memLogin0.Text = model.mem_Login.ToString();
                if (model.mem_Sex != "")
                {
                    this.drp_sex.Items.FindByValue(model.mem_Sex.Trim()).Selected = true;
                }
                this.lbl_birth0.Text = model.mem_Birthday == null ? "" : Convert.ToDateTime(model.mem_Birthday).ToString("yyyy-MM-dd");
                this.lbl_unit0.Text = "";
                if (model.mem_Unit_ID != 0)
                {
                    TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
                    TG.Model.tg_unit model_unit = bll_unit.GetModel(model.mem_Unit_ID);
                    this.lbl_unit0.Text = model_unit.unit_Name;
                }
                this.lbl_spec0.Text = "";
                if (model.mem_Speciality_ID.ToString() != "")
                {
                    TG.BLL.tg_speciality bll_spec = new TG.BLL.tg_speciality();
                    TG.Model.tg_speciality model_spec = bll_spec.GetModel(model.mem_Speciality_ID);
                    this.lbl_spec0.Text = model_spec.spe_Name;
                }
                this.lbl_pric0.Text = "";
                if (model.mem_Principalship_ID.ToString() != "")
                {
                    TG.BLL.tg_principalship bll_pric = new TG.BLL.tg_principalship();
                    TG.Model.tg_principalship model_pric = bll_pric.GetModel(model.mem_Principalship_ID);
                    this.lbl_pric0.Text = model_pric.pri_Name;
                }
                this.lbl_mobile0.Text = model.mem_Mobile;
                this.lbl_phone0.Text = model.mem_Telephone;
                this.lbl_email0.Text = model.mem_Mailbox;
                this.lbl_sign0.Text = model.mem_sign;

            }
        }
        //保存
        protected void btn_save_Click(object sender, EventArgs e)
        {
            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
            TG.Model.tg_member model = bll_mem.GetModel(int.Parse(this.lbl_ID0.Text));
            if (model != null)
            {
                model.mem_ID = int.Parse(this.lbl_ID0.Text);
                model.mem_Name = this.lbl_Name0.Text;
                model.mem_Login = this.txt_memLogin0.Text;
                model.mem_Sex = this.drp_sex.SelectedItem.Value;
                if (this.lbl_birth0.Text.Trim() != "")
                {
                    model.mem_Birthday = Convert.ToDateTime(this.lbl_birth0.Text.Trim());
                }
                model.mem_Mobile = this.lbl_mobile0.Text;
                model.mem_Telephone = this.lbl_phone0.Text;
                model.mem_Mailbox = this.lbl_email0.Text;
                model.mem_sign = this.lbl_sign0.Text;
                //头像
                TG.BLL.cm_EleSign bll_ele = new TG.BLL.cm_EleSign();
                TG.Model.cm_EleSign model_ele = new TG.Model.cm_EleSign();
                TG.Model.cm_EleSign model_ele2 = new TG.Model.cm_EleSign();

                model_ele.mem_id = UserSysNo;
                model_ele.elesign_min = this.txt_pic0_s.Value;
                model_ele.elesign = this.txt_pic0.Text;
                //model_ele.elesign = WebUserControl2.GetValue();
                model_ele2 = bll_ele.GetModel2(UserSysNo);

                //已存在记录下直接修改。
                if (model_ele2 != null)
                {
                    model_ele.ID = model_ele2.ID;
                    bll_ele.Update(model_ele);

                }
                else
                {

                    bll_ele.Add(model_ele);
                }
                if (bll_mem.Update(model))
                {
                    //弹出提示
                    TG.Common.MessageBox.ShowAndRedirect(this, "用户信息更新成功！", "SetPersinalBymaster.aspx");
                }
            }
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        public string GetMemId()
        {
            return UserShortName.ToString();
        }

    }
}