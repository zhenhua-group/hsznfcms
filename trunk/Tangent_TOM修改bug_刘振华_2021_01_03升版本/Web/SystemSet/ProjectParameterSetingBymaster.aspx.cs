﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using System.Text;

namespace TG.Web.SystemSet
{
    public partial class ProjectParameterSetingBymaster : PageBase
    {
        //排序字段
        protected string OrderColumn
        {
            get
            {
                return this.hid_column.Value;
            }
        }
        //排序
        protected string Order
        {
            get
            {
                return this.hid_order.Value;
            }
        }
        TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(ProjectParameterSetingBymaster));
            if (!IsPostBack)
            {
                //绑定单位
                BindUnit();
                //绑定年份
                BindYear();
                //选中当前年份
                SelectCurrentYear();
                //绑定合同
                BindCoperation();
            }
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND Unit= (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 数据绑定
        /// </summary>
        public void BindCoperation()
        {

            StringBuilder strWhere = new StringBuilder("");
            //合同名称
            if (this.txt_keyname.Value != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                strWhere.AppendFormat(" AND cpr_Name LIKE '%{0}%'", keyname);
            }
            //按照部门
            if (this.drp_unit.SelectedIndex != 0)
            {
                strWhere.AppendFormat(" AND (cpr_Unit= '{0}')", this.drp_unit.SelectedItem.Text);
            }
            //按照年份
            if (this.drp_year.SelectedIndex != 0)
            {
                strWhere.AppendFormat(" AND year(cpr_SignDate)={0}", this.drp_year.SelectedValue);
            }
            //判断个人或全部
            GetPreviewPowerSql(ref strWhere);
            //所有记录数
            this.AspNetPager1.RecordCount = int.Parse(bll.GetListPageProcCount(strWhere.ToString()).ToString());
            //排序
            string orderString = " Order by " + OrderColumn + " " + Order;
            strWhere.Append(orderString);

            this.gv_Coperation.DataSource = bll.GetListByPageProc(strWhere.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            this.gv_Coperation.DataBind();
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        /// <summary>
        /// 查找合同
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindCoperation();
        }

        protected void gv_Coperation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string str_status = e.Row.Cells[10].Text;

                DropDownList hid_persent = e.Row.Cells[11].FindControl("selectID") as DropDownList;
                if (string.IsNullOrEmpty(str_status))
                {
                    str_status = "0";
                }
                hid_persent.SelectedValue = str_status;

            }
        }


        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="src"></param>
        /// <param name="e"></param>
        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindCoperation();
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }

        /// <summary>
        /// 项目产值系数设置是否编辑
        /// </summary>
        /// <param name="cprID"></param>
        /// <param name="Status"></param>
        [AjaxMethod]
        public int UpdateProjectValueParamterIsEidt(int cprID, int Status)
        {
            int count = bll.UpdateProjectValueParamterIsEidt(cprID, Status);

            return count;

        }
    }
}