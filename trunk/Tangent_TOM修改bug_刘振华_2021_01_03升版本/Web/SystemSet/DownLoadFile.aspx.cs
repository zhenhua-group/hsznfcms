﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace TG.Web.SystemSet
{
    public partial class DownLoadFile : System.Web.UI.Page
    {
        public string FileName
        {
            get
            {
                return Request["fileName"];
            }
        }

        public string FilePath
        {
            get
            {
                return Request["filePath"];
            }
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
            string path = Server.MapPath(FilePath);
            if (File.Exists(path))
            {

                Response.ContentType = "application/x-zip-compressed";
                Response.Charset = "UTF-8";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Server.UrlEncode(FileName));

                MemoryStream ms = new MemoryStream();
                FileStream file = new FileStream(Server.MapPath(FilePath), FileMode.Open, FileAccess.ReadWrite);
                byte[] bytes = new byte[file.Length];
                file.Read(bytes, 0, (int)file.Length);
                ms.Write(bytes, 0, (int)file.Length);
                file.Close();
                ms.Close();

                Response.BinaryWrite(ms.ToArray());
            }
            else
            {
                Response.Write("<script type=\"text/javascript\">alert(\"文件不存在，或者登陆超时！\");window.close();</script>");
            }
            Response.End();
        }
    }
}