﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace TG.Web.SystemSet
{
    public partial class PreviewWebBannerList : PageBase
    {
        //公共类
        TG.BLL.CommDBHelper bll_comm = new TG.BLL.CommDBHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindPubMsg();
            }
        }

        //绑定公告
        protected void BindPubMsg()
        {
            string strSql = @" select wb.* ,wbi.SysNo as WebBannerItemSysNo
                                    from cm_WebBanner wb join cm_WenBannerItem wbi on wb.SysNo = wbi.WebBannerSysNo
                                    where ToUser =" + UserSysNo + " and wb.Status=0  order by wb.SysNo desc";
            //所有记录数
            string strSql2 = @" select count(*) from cm_WebBanner wb join cm_WenBannerItem wbi on wb.SysNo = wbi.WebBannerSysNo
                                where ToUser =" + UserSysNo + " and wb.Status=0";

            this.AspNetPager1.RecordCount = int.Parse(bll_comm.GetDataPageListCount(strSql2).ToString());

            this.GridView1.DataSource = bll_comm.GetDataPageList(strSql, this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize);
            this.GridView1.DataBind();
        }

        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string mem_id = e.Row.Cells[3].Text;
                if (mem_id != "")
                {
                    e.Row.Cells[3].Text = bll_mem.GetModel(int.Parse(mem_id)).mem_Name;
                }
            }
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindPubMsg();
        }
    }
}