﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using TG.Model;
using System.Text;

namespace TG.Web.SystemSet
{
    public partial class UnitAuditSet : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(UnitAuditSet));
            if (!IsPostBack)
            {
                GetRoleViewEntityList();
            }
        }
        private void GetRoleViewEntityList()
        {
            List<TG.Model.tg_unit> roleViewEntity = new TG.BLL.tg_unit().GetModelList(" unit_ParentID<>0 and unit_ID<>232 order by (select unit_Order from tg_unitExt where unit_ID=tg_unit.unit_ID)");
            this.gridList.DataSource = roleViewEntity;
            this.gridList.DataBind();
        }
        protected void grid_mem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string uid = e.Row.Cells[0].Text;
                List<TG.Model.tg_member> userslist=new TG.BLL.cm_UnitManagerConfig().GetUserEntityList(" unit_id="+uid,"unitusers");
                List<TG.Model.tg_member> manageruserslist=new TG.BLL.cm_UnitManagerConfig().GetUserEntityList(" unit_id="+uid,"managerusers");
                Literal lit = e.Row.Cells[2].FindControl("lit_users") as Literal;
                Literal lit2 = e.Row.Cells[3].FindControl("lit_manager") as Literal;
                if (userslist != null && userslist.Count>0)
                {
                    userslist.ForEach(users =>
                    {
                        lit.Text+="<span id=\"userSpan\" usersysno=\"" + users.mem_ID + "\"  style=\"margin-right: 1px;\">" + users.mem_Name + "<img src=\"../../Images/pro_icon_03.gif\" style=\"cursor: pointer;\" alt=\"删除该用户\" name=\"deleteUserImgActionBtn\" /></span>";
                    });
                }
                if (manageruserslist != null && manageruserslist.Count > 0)
                {
                    manageruserslist.ForEach(users =>
                    {
                        lit2.Text += "<span id=\"userSpan\" usersysno=\"" + users.mem_ID + "\"  style=\"margin-right: 1px;\">" + users.mem_Name + "<img src=\"../../Images/pro_icon_03.gif\" style=\"cursor: pointer;\" alt=\"删除该用户\" name=\"deleteUserImgActionBtn\" /></span>";
                    });
                }
               
            }
        }
        
        /// <summary>
        /// 修改部门所对应的用户
        /// </summary>
        /// <param name="userSysNoString"></param>
        /// <param name="roleSysNoString"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string UpdateRoleUsers(string userSysNoString, string unitid,string columnsname)
        {
            int count = 0;
            TG.BLL.cm_UnitManagerConfig umBLL = new TG.BLL.cm_UnitManagerConfig();
            TG.Model.cm_UnitManagerConfig umModel = new TG.Model.cm_UnitManagerConfig();
            int sysno=umBLL.ExistsID(Convert.ToInt32(unitid));
            //存在
            if (sysno > 0)
            {
                umModel = umBLL.GetModel(sysno);
                if (columnsname == "unitusers")
                {
                    umModel.unitusers = userSysNoString;
                }
                else if (columnsname == "managerusers")
                {
                    umModel.managerusers = userSysNoString;
                }
                if (umBLL.Update(umModel))
                {
                    count = 1;
                }

            }
            else
            {
                umModel.unit_id = Convert.ToInt32(unitid);
                if (columnsname == "unitusers")
                {
                    umModel.unitusers = userSysNoString;
                    umModel.managerusers = "";
                }
                else if (columnsname == "managerusers")
                {
                    umModel.unitusers = "";
                    umModel.managerusers = userSysNoString;
                }
               count=umBLL.Add(umModel);
            }
          
            return count + "";
        }
    }
}