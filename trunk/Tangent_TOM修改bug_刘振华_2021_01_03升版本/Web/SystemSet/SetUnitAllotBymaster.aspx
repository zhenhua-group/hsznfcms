﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="SetUnitAllotBymaster.aspx.cs" Inherits="TG.Web.SystemSet.SetUnitAllotBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>--%>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" />
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            $('#ctl00_ContentPlaceHolder1_grid_mem_ctl01_chkAll').click(function () {

                var checks = $('#ctl00_ContentPlaceHolder1_grid_mem :checkbox');

                if ($('#ctl00_ContentPlaceHolder1_grid_mem_ctl01_chkAll').attr("checked") == "checked") {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.setAttribute("class", "checked");
                        checks[i].setAttribute("checked", "checked");
                    }
                }
                else {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.removeAttribute("class");
                        checks[i].removeAttribute("checked");
                    }
                }
            });

            $("#ctl00_ContentPlaceHolder1_btn_DelCst").click(function () {

                if ($("#ctl00_ContentPlaceHolder1_grid_mem :checkbox:checked").length == 0) {
                    jAlert("请选择要删除的目标值！", "提示");
                    return false;
                }
                //判断是否要删除
                return confirm("是否要删除此条目标值？");
            });

            $(".cls_select").click(function () {
                $("#div_add").hide("slow");
                $("#div_edit").show("slow");
            });

            $(".cls_select").live("click", function () {
                window.setTimeout("scrollToBottom()", 500); //500毫秒延迟加载
                $("#ctl00_ContentPlaceHolder1_hid_memid").val($(this).parent().parent().find("TD").eq(1).find(":hidden").val());
                $("#ctl00_ContentPlaceHolder1_drp_unit1").val($(this).parent().parent().find("TD").eq(2).find(":hidden").val());
                $("#ctl00_ContentPlaceHolder1_hid_unitid").val($(this).parent().parent().find("TD").eq(2).find(":hidden").val());
                $("#ctl00_ContentPlaceHolder1_drp_unit1").get(0).disabled = true;
                $("#ctl00_ContentPlaceHolder1_drp_allotyear0").val($.trim($(this).parent().parent().find("TD").eq(3).text()));
                $("#ctl00_ContentPlaceHolder1_hid_allotyear").val($.trim($(this).parent().parent().find("TD").eq(3).text()));
                $("#ctl00_ContentPlaceHolder1_drp_allotyear0").get(0).disabled = true;
                $("#ctl00_ContentPlaceHolder1_txt_allot0").val($(this).parent().parent().find("TD").eq(4).text());
            });

            //隐藏
            $(" .btn_cancel").click(function () {
                $("#div_add").hide("slow");
                $("#div_edit").hide("slow");
            });


            //添加
            $("#btn_showadd").click(function () {
                window.setTimeout("scrollToBottom()", 500); //500毫秒延迟加载
                $("#div_add").show("slow");
                $("#div_edit").hide("slow");
            });


            $("#ctl00_ContentPlaceHolder1_btn_save").click(function () {
                var msg = "";
                //生产部门
                if ($("#ctl00_ContentPlaceHolder1_drp_unit0").val() == "-1") {
                    msg += "请选择要生产部门！<br/>";
                }
                //年份
                if ($("#ctl00_ContentPlaceHolder1_drp_allotyear").val() == "-1") {
                    msg += "请选择年份！<br/>";
                }

                if ($("#ctl00_ContentPlaceHolder1_txt_allot").val() == "") {
                    msg += "请填写目标产值！<br/>";
                }
                else {
                    var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
                    if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_allot").val())) {
                        msg += "目标产值格式不正确！</br>";
                    }
                }
                if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_allot").val()) <= 0) {
                    msg += "目标产值必须大于0！";
                }
                if (msg != "") {
                    jAlert(msg, "提示");

                    return false;
                }
            });
        });
        function scrollToBottom() {
            window.scrollTo(0, document.body.scrollHeight); //移动屏幕最下方
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>部门目标产值设定</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用设置</a><i class="fa fa-angle-right"> </i><a>部门目标产值设定</a>
    </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>部门目标产值查询</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table-responsive">
                    	<tr>
                    		<td>生产部门:</td>
                            <td><asp:DropDownList ID="drp_unit" runat="server" AppendDataBoundItems="True" CssClass="form-control">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>年份:</td>
                            <td><asp:DropDownList ID="drp_year" runat="server" CssClass="form-control">
                                    <asp:ListItem>2010</asp:ListItem>
                                    <asp:ListItem>2011</asp:ListItem>
                                    <asp:ListItem>2012</asp:ListItem>
                                    <asp:ListItem>2013</asp:ListItem>
                                    <asp:ListItem>2014</asp:ListItem>
                                    <asp:ListItem>2015</asp:ListItem>
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                    <asp:ListItem>2018</asp:ListItem>
                                    <asp:ListItem>2019</asp:ListItem>
                                    <asp:ListItem>2020</asp:ListItem>
                                </asp:DropDownList></td>
                            <td> <asp:Button ID="btn_Search" runat="server" Text="查询" CssClass="btn blue" OnClick="btn_Search_Click" /></td>
                            <td> <input type="button" name="name" value="添加" class="btn green" id="btn_showadd" /></td>
                            <td><asp:Button ID="btn_DelCst" runat="server" Text="删除" CssClass="btn red" OnClick="btn_DelCst_Click" /></td>
                    	</tr>
                    </table>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>部门目标产值列表</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView ID="grid_mem" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                CssClass="table table-striped table-bordered table-hover dataTable" Width="100%"
                                OnRowDataBound="grid_mem_RowDataBound" EnableModelValidation="True">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_id" runat="server" CssClass="cls_chk" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="编号">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Container.DataItemIndex+1%>'></asp:Label>
                                            <asp:HiddenField ID="hid_id" runat="server" Value='<%# Bind("ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="生产部门">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_unitname" runat="server" Text='<%# Bind("UnitID") %>'></asp:Label>
                                            <asp:HiddenField ID="hid_unitid" runat="server" Value='<%# Bind("UnitID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="30%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AllotYear" HeaderText="目标年份(年)">
                                        <ItemStyle Width="25%" HorizontalAlign="Left" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UnitAllot" HeaderText="目标产值(万元)">
                                        <ItemStyle HorizontalAlign="Left" Width="25%" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="#" class="cls_select">编辑</a>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    生产部门产值数据为空！
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cls_data" id="div_add" style="display: none;">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>添加目标产值信息</div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"></a>
                </div>
            </div>
            <div class="portlet-body">
                <table border="0" cellspacing="0" cellpadding="0" width="500" align="center">
                    <tr>
                        <td>
                            生产部门:
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_unit0" runat="server" AppendDataBoundItems="True" CssClass="form-control">
                                <asp:ListItem Value="-1">------全院部门------</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            目标年份:
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_allotyear" runat="server" CssClass="form-control">
                                <asp:ListItem Value="-1">---年份---</asp:ListItem>
                                <asp:ListItem Value="2010">2010</asp:ListItem>
                                <asp:ListItem Value="2011">2011</asp:ListItem>
                                <asp:ListItem Value="2012">2012</asp:ListItem>
                                <asp:ListItem Value="2013">2013</asp:ListItem>
                                <asp:ListItem Value="2014">2014</asp:ListItem>
                                <asp:ListItem Value="2015">2015</asp:ListItem>
                                <asp:ListItem Value="2016">2016</asp:ListItem>
                                <asp:ListItem Value="2017">2017</asp:ListItem>
                                <asp:ListItem Value="2018">2018</asp:ListItem>
                                <asp:ListItem Value="2019">2019</asp:ListItem>
                                <asp:ListItem Value="2019">2020</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            目标产值:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_allot" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            万元(小数点后两位有效)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btn_save" runat="server" Text="保存" OnClick="btn_save_Click" CssClass="btn green" />&nbsp;
                            <input type="button" name="btn_cancel" value="取消" class="btn   btn_cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="cls_data" id="div_edit" style="display: none;">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>编辑目标产值值</div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"></a>
                </div>
            </div>
            <div class="portlet-body">
                <table border="0" cellspacing="0" cellpadding="0" width="500" align="center">
                    <tr>
                        <td>
                            生产部门:
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_unit1" runat="server" AppendDataBoundItems="True" CssClass="form-control">
                                <asp:ListItem Value="-1">------全院部门------</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            目标年份:
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_allotyear0" runat="server" CssClass="form-control">
                                <asp:ListItem Value="-1">---年份---</asp:ListItem>
                                <asp:ListItem Value="2010">2010</asp:ListItem>
                                <asp:ListItem Value="2011">2011</asp:ListItem>
                                <asp:ListItem Value="2012">2012</asp:ListItem>
                                <asp:ListItem Value="2013">2013</asp:ListItem>
                                <asp:ListItem Value="2014">2014</asp:ListItem>
                                <asp:ListItem Value="2015">2015</asp:ListItem>
                                <asp:ListItem Value="2016">2016</asp:ListItem>
                                <asp:ListItem Value="2017">2017</asp:ListItem>
                                <asp:ListItem Value="2018">2018</asp:ListItem>
                                <asp:ListItem Value="2019">2019</asp:ListItem>
                                <asp:ListItem Value="2019">2020</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            目标产值:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_allot0" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            万元(小数点后两位有效)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btn_edit" runat="server" CssClass="btn green" Text="保存" OnClick="btn_edit_Click" />
                            <input type="button" name="btn_cancel" value="取消" class="btn   btn_cancel" />
                            <asp:HiddenField ID="hid_memid" runat="server" />
                            <asp:HiddenField ID="hid_unitid" runat="server" />
                            <asp:HiddenField ID="hid_allotyear" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
