﻿
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class ExportUserInfoListToExcel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ExportUserInfoList();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="parameters"></param>
        private void ExportUserInfoList()
        {
            string memLogin = Request["mem_Login"] ?? "";
            string memName = Request["mem_Name"] ?? "";
            string memBirthday = Request["mem_Birthday"] ?? "";
            string memSex = Request["mem_Sex"] ?? "";
            string memUnitName = Request["memUnitName"] ?? "";
            string memSpeName = Request["memSpeName"] ?? "";
            string isFiredName = Request["isFiredName"] ?? "";
            string roleTecName = Request["RoleTecName"] ?? "";
            string roleMagName = Request["RoleMagName"] ?? "";
            string memArchReg = Request["mem_ArchReg"] ?? "";
            string memCode = Request["mem_Code"] ?? "";
            string memAge = Request["mem_Age"] ?? "";
            string memCoperationDate = Request["mem_CoperationDate"] ?? "";

            string memList = Request["mem_List"] ?? "";

            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(memLogin))
            {
                sb.AppendFormat(" AND mem_Login like '%{0}%'", memLogin);
            }

            if (!string.IsNullOrEmpty(memName))
            {
                sb.AppendFormat(" AND mem_Name like '%{0}%'", memName);
            }

            if (!string.IsNullOrEmpty(memBirthday))
            {
                sb.AppendFormat(" AND mem_Birthday='{0}'", memBirthday);
            }

            if (memSex != "-1")
            {
                sb.AppendFormat(" AND mem_Sex='{0}'", memSex);
            }

            //if (memUnitName != "-1")
            //{
            //    sb.AppendFormat(" AND memUnitName = '{0}'", memUnitName);
            //}

            if (memUnitName != "")
            {
                sb.AppendFormat(" AND memUnitName IN ({0})", memUnitName);
            }

            if (memList != "")
            {
                sb.AppendFormat(" AND memID IN ({0})", memList);
            }


            if (memSpeName != "-1")
            {
                sb.AppendFormat(" AND memSpeName = '{0}'", memSpeName);
            }

            if (isFiredName != "-1")
            {
                sb.AppendFormat(" AND isFiredName='{0}'", isFiredName);
            }

            if (roleTecName != "-1")
            {
                sb.AppendFormat(" AND RoleTecName = '{0}'", roleTecName);
            }

            if (roleMagName != "-1")
            {
                sb.AppendFormat(" AND RoleMagName = '{0}'", roleMagName);
            }

            if (memArchReg != "-1")
            {
                sb.AppendFormat(" AND mem_ArchReg = '{0}'", memArchReg);
            }

            if (!string.IsNullOrEmpty(memCode))
            {
                sb.AppendFormat(" AND mem_Code = '{0}'", memCode);
            }

            if (!string.IsNullOrEmpty(memAge))
            {
                sb.AppendFormat(" AND mem_Age = {0}", memAge);
            }

            if (!string.IsNullOrEmpty(memCoperationDate))
            {
                sb.AppendFormat(" AND mem_CoperationDate = '{0}'", memCoperationDate);
            }

            ExportUserInfoList(sb.ToString());
        }

        public void ExportUserInfoList(string strwhere)
        {
            string strSql = string.Format(" Select * From dbo.View_tg_memberExtInfo Where 1=1 {0} Order By mem_Unit_ID,mem_Order ", strwhere);
            DataTable dt = TG.DBUtility.DbHelperSQL.Query(strSql).Tables[0];

            string modelPath = " ~/TemplateXls/userinfotemp.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(HttpContext.Current.Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            ICellStyle style0 = wb.CreateCellStyle();
            style0.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style0.VerticalAlignment = VerticalAlignment.CENTER;
            style0.WrapText = true;

            IFont font0 = wb.CreateFont();
            font0.FontHeightInPoints = 12;//字号
            font0.FontName = "宋体";//字体
            font0.Boldweight = (short)700;
            style0.SetFont(font0);


            //标题样式
            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;

            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 12;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;

            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)

                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["memUnitName"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Sex"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["memSpeName"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["RoleMagName"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["RoleTecName"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_CodeAddr"].ToString());

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Age"].ToString());

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Birthday"].ToString());

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Code"].ToString());

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Mobile"].ToString());

                cell = dataRow.CreateCell(11);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_School"].ToString());

                cell = dataRow.CreateCell(12);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_SchLevel"].ToString());

                cell = dataRow.CreateCell(13);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_SchSpec"].ToString());

                cell = dataRow.CreateCell(14);
                cell.CellStyle = style2;
                if (!string.IsNullOrEmpty(dt.Rows[i]["mem_SchOutDate"].ToString()))
                {
                    cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["mem_SchOutDate"].ToString()).ToShortDateString());
                }
                else
                {
                    cell.SetCellValue("");
                }

                cell = dataRow.CreateCell(15);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_School2"].ToString());

                cell = dataRow.CreateCell(16);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_SchLevel2"].ToString());

                cell = dataRow.CreateCell(17);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_SchSpec2"].ToString());

                cell = dataRow.CreateCell(18);
                cell.CellStyle = style2;
                if (!string.IsNullOrEmpty(dt.Rows[i]["mem_SchOutDate2"].ToString()))
                {
                    cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["mem_SchOutDate2"].ToString()).ToShortDateString());
                }
                else
                {
                    cell.SetCellValue("");
                }

                cell = dataRow.CreateCell(19);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_ArchLevelName2"].ToString());

                cell = dataRow.CreateCell(20);
                cell.CellStyle = style2;
                if (!string.IsNullOrEmpty(dt.Rows[i]["mem_ArchLevelTime"].ToString()))
                {
                    cell.SetCellValue(dt.Rows[i]["mem_ArchLevelTime"].ToString());
                }
                else
                {
                    cell.SetCellValue("");
                }

                cell = dataRow.CreateCell(21);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ArchLevelName"].ToString());

                cell = dataRow.CreateCell(22);
                cell.CellStyle = style2;
                if (!string.IsNullOrEmpty(dt.Rows[i]["mem_ArchRegTime"].ToString()))
                {
                    cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["mem_ArchRegTime"].ToString()).ToShortDateString());
                }
                else
                {
                    cell.SetCellValue("");
                }

                cell = dataRow.CreateCell(23);
                cell.CellStyle = style2;
                if (!string.IsNullOrEmpty(dt.Rows[i]["mem_WorkTime"].ToString()))
                {
                    cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["mem_WorkTime"].ToString()).ToShortDateString());
                }
                else
                {
                    cell.SetCellValue("");
                }

                cell = dataRow.CreateCell(24);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_WorkDiff"].ToString());

                cell = dataRow.CreateCell(25);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_WorkYear"].ToString());

                cell = dataRow.CreateCell(26);
                cell.CellStyle = style2;
                if (!string.IsNullOrEmpty(dt.Rows[i]["mem_InCompanyTime"].ToString()))
                {
                    cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["mem_InCompanyTime"].ToString()).ToShortDateString());
                }
                else
                {
                    cell.SetCellValue("");
                }

                cell = dataRow.CreateCell(27);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_InCompanyDiffSub"].ToString());

                cell = dataRow.CreateCell(28);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_InCompanyDiff"].ToString());

                cell = dataRow.CreateCell(29);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_InCompanyYear"].ToString());

                cell = dataRow.CreateCell(30);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_InCompanyTemp"].ToString());

                cell = dataRow.CreateCell(31);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_FileAddr"].ToString());

                cell = dataRow.CreateCell(32);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Holidaybase"].ToString());

                cell = dataRow.CreateCell(33);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_HolidayYear"].ToString());

                cell = dataRow.CreateCell(34);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_MagPishi"].ToString());

                cell = dataRow.CreateCell(35);
                cell.CellStyle = style2;
                if (!string.IsNullOrEmpty(dt.Rows[i]["mem_ZhuanZheng"].ToString()))
                {
                    cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["mem_ZhuanZheng"].ToString()).ToShortDateString());
                }
                else
                {
                    cell.SetCellValue("");
                }

                cell = dataRow.CreateCell(36);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_GongZi"].ToString());

                cell = dataRow.CreateCell(37);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Jiaotong"].ToString());

                cell = dataRow.CreateCell(38);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Tongxun"].ToString());

                cell = dataRow.CreateCell(39);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_YingFa"].ToString());

                cell = dataRow.CreateCell(40);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_SheBaobase"].ToString());

                cell = dataRow.CreateCell(41);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Gongjijin"].ToString());

                cell = dataRow.CreateCell(42);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_GongjijinSub"].ToString());

                cell = dataRow.CreateCell(43);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_Yufa"].ToString());

                //2018年1月5日
                cell = dataRow.CreateCell(44);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_CoperationDate"].ToString());

                cell = dataRow.CreateCell(45);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_CoperationTypeName"].ToString());

                cell = dataRow.CreateCell(46);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_CoperationDateEnd"].ToString());

                cell = dataRow.CreateCell(47);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_CoperationSub"].ToString());

                cell = dataRow.CreateCell(48);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_YearCharge"].ToString());

                cell = dataRow.CreateCell(49);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_OutCompany"].ToString());

                cell = dataRow.CreateCell(50);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_OutCompanyDate"].ToString());

                cell = dataRow.CreateCell(51);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_OutCompanyTypeName"].ToString());

                cell = dataRow.CreateCell(52);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["mem_OutCompanySub"].ToString());
            }


            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("人力资源人员信息.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }


        }
    }
}