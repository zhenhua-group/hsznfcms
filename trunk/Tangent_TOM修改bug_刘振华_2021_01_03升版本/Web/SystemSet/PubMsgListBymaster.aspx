﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="PubMsgListBymaster.aspx.cs" Inherits="TG.Web.SystemSet.PubMsgListBymaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="../css/jquery.alerts.css" rel="stylesheet" />
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
     <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        $(function () {
            $('.menu_Open').click(function () {
                $("#show8").parent().attr("class", " arrow open");
                $("#show8_4").parent().attr("class", " arrow open");
                $("#show8_4").parent().parent().css("display", "block");
            });
            //显示与隐藏
            $("#<%=btn_Ycang.ClientID%>").click(function () {
                //var icount = $(":checked").length;
                var icount = $("#<%=GridView1.ClientID%> .checked").length;
                if (icount == 0) {
                    alert("请选择要隐藏的记录");
                    return false;
                }
                else  if (confirm("确实要隐藏消息吗？")) {
                    return true;
                }
                else { return false;}
            });
            $("#<%=btn_XShi.ClientID%>").click(function () {
                var icount = $("#<%=GridView1.ClientID%> .checked").length;
                //var icount = $(":checked").length;
                if (icount == 0) {
                    alert("请选择要显示的记录");
                    return false;
                } else  if (confirm("确实要显示消息吗？")) {
                    return true;
                } else { return false; }
            });

            //删除是判断有无记录选中
            $("#<%=btn_DelCst.ClientID%>").click(function () {
                if ($("#<%=GridView1.ClientID%> .checked").length == 0) {
                    jAlert("请选择要删除的公告！", "提示");
                    return false;
                }
                //判断是否要删除
                return confirm("是否要删除公告信息？");
            });

            //全选-反选
            $('#<%=GridView1.ClientID%>_ctl01_chkAll').click(function () {

                var checks = $('#<%=GridView1.ClientID%> :checkbox');

                if ($('#<%=GridView1.ClientID%>_ctl01_chkAll').attr("checked") == "checked") {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.setAttribute("class", "checked");
                        $(checks[i]).attr("checked", "checked");
                    }
                }
                else {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.removeAttribute("class");
                        $(checks[i]).removeAttr("checked");
                    }
                }
            });


        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>公告管理 </small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a>
        <a href="javascript:;"  class="menu_Open">系统设置</a><i class="fa fa-angle-right">
        </i><a href="javascript:;"  class="menu_Open">公告管理</a><i class="fa fa-angle-right">
        </i><a href="javascript:;" class="menu_Open">公告管理</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i><font><font>公告搜索</font></font>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table-responsive">
                    	<tr>
                    		<td>公告标题:</td>
                            <td><input id="txt_keyname" name="txt_keyname" type="text" runat="Server" class="form-control" /></td>
                            <td><asp:Button ID="btn_Search" runat="server" CssClass="btn blue" Text="查询"
                                    OnClick="btn_Search_Click" /></td>
                            <td><a href="BannerAndLogViewBymaster.aspx?viewType=1" target="_self" class="btn blue">
                                   添加</a></td>
                            <td> <asp:Button ID="btn_DelCst" runat="server" CssClass="btn blue" Text="删除"
                                    OnClick="btn_DelCst_Click" /></td>
                            <td><asp:Button ID="btn_Ycang" runat="server" CssClass="btn blue" Text="隐藏"
                                    OnClick="btn_Ycang_Click" /></td>
                            <td><asp:Button ID="btn_XShi" runat="server" CssClass="btn blue" Text="显示"
                                    OnClick="btn_XShi_Click" /></td>
                            <td></td>
                    	</tr>
                    </table>
                    
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>公告列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" ShowHeader="true"  CssClass="table table-striped table-bordered table-hover dataTable" 
                            Width="100%" OnRowDataBound="GridView1_RowDataBound" EnableModelValidation="True">
                            <Columns>
                                <asp:TemplateField><HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" runat="server" />
                                </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server" CssClass="cls_chk" />
                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("SysNo") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Title" HeaderText="公告标题">
                                    <ItemStyle HorizontalAlign="Left" Width="45%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="InDate" HeaderText="发布时间">
                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="inUser" HeaderText="发布用户">
                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="status" HeaderText="状态" DataFormatString="">
                                    <ItemStyle HorizontalAlign="Left" Width="5%" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="查看">
                                    <ItemTemplate>
                                        <a href='<%#"ShowMsgViewBymaster.aspx?type=0&sysno="+Eval("SysNo") %>' target="_self">查看</a>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="5%" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="编辑">
                                    <ItemTemplate>
                                        <a href='<%# "BannerAndLogViewBymaster.aspx?action=1&viewType=1&sysno="+Eval("SysNo") %>'
                                            target="_self">编辑</a>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="5%" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                            CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                            CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                            OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                            PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                            SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                            PageSize="30" SubmitButtonClass="submitbtn">
                        </webdiyer:AspNetPager>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
