﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using TG.Model;

namespace TG.Web.SystemSet
{
    public partial class RoleManagerBymaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(RoleManagerBymaster));
            if (!IsPostBack)
            {
                GetRoleViewEntityList();
            }
        }
        private void GetRoleViewEntityList()
        {
            List<RoleViewEntity> roleViewEntity = new TG.BLL.cm_Role().GetRoleViewEntityList(CreateRoleQueryEntity(""));
            this.RepeaterRoleList.DataSource = roleViewEntity;
            this.RepeaterRoleList.DataBind();
        }

        private RoleQueryEntity CreateRoleQueryEntity(string roleName)
        {
            return new RoleQueryEntity
            {
                RoleName = roleName
            };
        }

        [AjaxMethod]
        public string InsertRole(string roleName, string userSysNoString)
        {
            TG.Model.cm_Role role = new cm_Role
            {
                RoleName = roleName,
                Users = userSysNoString
            };

            return new TG.BLL.cm_Role().InsertRole(role).ToString();
        }

        [AjaxMethod]
        public string GetRoleViewEntity(int roleSysNo)
        {
            RoleViewEntity role = new TG.BLL.cm_Role().GetRoleViewEntity(new RoleQueryEntity { RoleSysNo = roleSysNo });

            return Newtonsoft.Json.JsonConvert.SerializeObject(role);
        }

        [AjaxMethod]
        public string UpdateRole(string roleName, string userSysNoString, int roleSysNo)
        {
            TG.Model.cm_Role role = new cm_Role
            {
                SysNo = roleSysNo,
                RoleName = roleName,
                Users = userSysNoString
            };
            return new TG.BLL.cm_Role().UpdateRole(role).ToString();
        }
    }
}