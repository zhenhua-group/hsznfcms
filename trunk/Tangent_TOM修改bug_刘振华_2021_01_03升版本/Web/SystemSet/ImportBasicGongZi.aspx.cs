﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class ImportBasicGongZi : System.Web.UI.Page
    {
        public List<TG.Model.cm_KaoHeMemsWages> MemsWagesList
        {
            get;
            set;
        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            this.drp_unit3.DataSource = bll_unit.GetList(" unit_ParentID<>0 ");
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }
        /// <summary>
        /// 
        /// </summary>
        protected void InitMemsList()
        {
            //string strWhere = string.Format(" SendYear={0} AND SendMonth={1}  ", this.drpYear.SelectedValue, this.drpMonth.SelectedValue);
            //if (this.drp_unit3.SelectedIndex > 0)
            //{
            //    strWhere = string.Format(" SendYear={0} AND SendMonth={1} AND (MemID IN (Select mem_ID From tg_member Where mem_Unit_ID={2})) ", this.drpYear.SelectedValue, this.drpMonth.SelectedValue, this.drp_unit3.SelectedValue);
            //}
            string strWhere = string.Format(" SendYear={0} AND SendMonth={1} AND (MemID IN (Select mem_ID From tg_member Where mem_Unit_ID={2})) ", this.drpYear.SelectedValue, this.drpMonth.SelectedValue, this.drp_unit3.SelectedValue);

            var bll = new TG.BLL.cm_KaoHeMemsWages();

            this.MemsWagesList = bll.GetModelList(strWhere);
        }
        /// <summary>
        /// 初始页面
        /// </summary>
        protected void InitPage()
        {
            string year = DateTime.Now.Year.ToString();
            string month = DateTime.Now.Month.ToString();

            this.drpYear.ClearSelection();
            this.drpMonth.ClearSelection();

            if (this.drpYear.Items.FindByValue(year) != null)
            {
                this.drpYear.Items.FindByValue(year).Selected = true;
            }

            if (this.drpMonth.Items.FindByValue(month) != null)
            {
                this.drpMonth.Items.FindByValue(month).Selected = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetInitPage();
            }
        }
        /// <summary>
        /// 初始化界面
        /// </summary>
        protected void GetInitPage()
        {
            BindUnit();
            InitPage();
            InitMemsList();
        }
        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (FileUpload.PostedFile != null && FileUpload.PostedFile.ContentLength > 0)
            {

                string FileImportDirectory = Server.MapPath("~/Template/"); ;
                //取得上传文件名称
                string uplodeName = FileUpload.PostedFile.FileName;
                //保存文件
                string extensionName = FileUpload.PostedFile.FileName.Substring(FileUpload.PostedFile.FileName.LastIndexOf("."));
                //生成的文件名称
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + (new Random()).Next(999).ToString("D3") + extensionName;
                //创建存放路径
                if (!Directory.Exists(FileImportDirectory))
                {
                    Directory.CreateDirectory(FileImportDirectory);
                }
                //保存文件
                FileUpload.SaveAs(FileImportDirectory + "\\" + fileName);
                //读取到DataSet
                DataSet ds = new DataSet();

                try
                {
                    ds = ReaderExcelToDataSet(FileImportDirectory + "\\" + fileName, true);
                }
                catch (Exception ex)
                {
                    //把07 .xlsx格式 强制改成xls格式 抛出异常
                    if (ex.Message.Trim().Equals("外部表不是预期的格式。"))
                    {
                        TG.Common.MessageBox.Show(this.Page, "请上传标准版本(2003或者2007）格式的excel！");
                        return;
                    }
                    else
                    {
                        TG.Common.MessageBox.Show(this.Page, "请选择正确的模板！");
                        return;
                    }
                }

                if (ds.Tables.Count == 0)
                {
                    TG.Common.MessageBox.Show(this.Page, "上传文件无数据！");
                    return;
                }
                //模板 默认是18列
                if (ds.Tables[0].Columns.Count != 18)
                {
                    TG.Common.MessageBox.Show(this.Page, "请选择正确的模板！");
                    return;
                }
                //循环模板里的数据
                foreach (DataTable dt in ds.Tables)
                {
                    InsertAndUpdateUserData(dt);
                }


                Response.Write("alert('数据导入成功！');");
            }

            GetInitPage();
        }
        /// <summary>
        /// 循环插入更新
        /// </summary>
        /// <param name="dt"></param>
        private void InsertAndUpdateUserData(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                var bll = new TG.BLL.cm_KaoHeMemsWages();
                var model = new TG.Model.cm_KaoHeMemsWages();
                var bllmem = new TG.BLL.tg_member();

                foreach (DataRow dr in dt.Rows)
                {
                    //姓名
                    string memName = dr[0].ToString().Trim();
                    //年
                    string year = dr[1].ToString().Trim();
                    //月
                    string month = dr[2].ToString().Trim();

                    string strWhere = "";


                    if (memName != "" && year != "" && month != "")
                    {
                        strWhere = string.Format(" MemName='{0}' AND SendYear={1} AND SendMonth={2} ", memName, year, month);
                        var list = bll.GetModelList(strWhere);

                        if (list.Count > 0)
                        {
                            model = list[0];
                            //基本工资
                            model.jbgz = dr[3].ToString() != "" ? Convert.ToDecimal(dr[3].ToString()) : 0;
                            //岗位工资
                            model.gwgz = dr[4].ToString() != "" ? Convert.ToDecimal(dr[4].ToString()) : 0;
                            //预发奖金
                            model.yfjj = dr[5].ToString() != "" ? Convert.ToDecimal(dr[5].ToString()) : 0;
                            //岗位津贴
                            model.gwjt = dr[6].ToString() != "" ? Convert.ToDecimal(dr[6].ToString()) : 0;
                            //交通，通讯
                            model.jtbt = dr[7].ToString() != "" ? Convert.ToDecimal(dr[7].ToString()) : 0;
                            //没用
                            model.txbt = 0;
                            //加班补助
                            model.jbbz = dr[8].ToString() != "" ? Convert.ToDecimal(dr[8].ToString()) : 0;
                            //车补差补
                            model.cbcb = dr[9].ToString() != "" ? Convert.ToDecimal(dr[9].ToString()) : 0;
                            //本所工资
                            model.qita = dr[16].ToString() != "" ? Convert.ToDecimal(dr[16].ToString()) : 0;
                            //缺勤扣款
                            model.qkkk = dr[10].ToString() != "" ? Convert.ToDecimal(dr[10].ToString()) : 0;
                            //应发工资
                            model.yfgz = dr[11].ToString() != "" ? Convert.ToDecimal(dr[11].ToString()) : 0;
                            //代扣社保
                            model.dksb = dr[12].ToString() != "" ? Convert.ToDecimal(dr[12].ToString()) : 0;
                            //实扣税金
                            model.sksj = dr[13].ToString() != "" ? Convert.ToDecimal(dr[13].ToString()) : 0;
                            //实发工资
                            model.sfgz = dr[14].ToString() != "" ? Convert.ToDecimal(dr[14].ToString()) : 0;
                            //中心工资
                            model.zxgz = dr[15].ToString() != "" ? Convert.ToDecimal(dr[15].ToString()) : 0;
                            //公司成本
                            model.gscb = dr[17].ToString() != "" ? Convert.ToDecimal(dr[17].ToString()) : 0;

                            bll.Update(model);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(dr[0].ToString()))
                            {
                                var mems = bllmem.GetModelList(" mem_Name='" + dr[0].ToString().Trim() + "'");
                                if (mems.Count > 0)
                                {
                                    model.MemID = mems[0].mem_ID;
                                    model.MemName = dr[0].ToString();
                                    model.SendYear = Convert.ToInt32(dr[1].ToString());
                                    model.SendMonth = Convert.ToInt32(dr[2].ToString());
                                    //基本工资
                                    model.jbgz = dr[3].ToString() != "" ? Convert.ToDecimal(dr[3].ToString()) : 0;
                                    //岗位工资
                                    model.gwgz = dr[4].ToString() != "" ? Convert.ToDecimal(dr[4].ToString()) : 0;
                                    //预发奖金
                                    model.yfjj = dr[5].ToString() != "" ? Convert.ToDecimal(dr[5].ToString()) : 0;
                                    //岗位津贴
                                    model.yfjj = dr[6].ToString() != "" ? Convert.ToDecimal(dr[6].ToString()) : 0;
                                    //交通，通讯
                                    model.jtbt = dr[7].ToString() != "" ? Convert.ToDecimal(dr[7].ToString()) : 0;
                                    //没用
                                    model.txbt = 0;
                                    //加班补助
                                    model.jbbz = dr[8].ToString() != "" ? Convert.ToDecimal(dr[8].ToString()) : 0;
                                    //车补差补
                                    model.cbcb = dr[9].ToString() != "" ? Convert.ToDecimal(dr[9].ToString()) : 0;
                                    //本所工资
                                    model.qita = dr[16].ToString() != "" ? Convert.ToDecimal(dr[16].ToString()) : 0;
                                    //缺勤扣款
                                    model.qkkk = dr[10].ToString() != "" ? Convert.ToDecimal(dr[10].ToString()) : 0;
                                    //应发工资
                                    model.yfgz = dr[11].ToString() != "" ? Convert.ToDecimal(dr[11].ToString()) : 0;
                                    //代扣社保
                                    model.dksb = dr[12].ToString() != "" ? Convert.ToDecimal(dr[12].ToString()) : 0;
                                    //实扣税金
                                    model.sksj = dr[13].ToString() != "" ? Convert.ToDecimal(dr[13].ToString()) : 0;
                                    //实发工资
                                    model.sfgz = dr[14].ToString() != "" ? Convert.ToDecimal(dr[14].ToString()) : 0;
                                    //中心工资
                                    model.zxgz = dr[15].ToString() != "" ? Convert.ToDecimal(dr[15].ToString()) : 0;
                                    //公司成本
                                    model.gscb = dr[17].ToString() != "" ? Convert.ToDecimal(dr[17].ToString()) : 0;

                                    bll.Add(model);
                                }
                            }
                        }
                    }
                }
            }
        }


        #region 读取Excel的内容到DataSet--第一行为标题行
        /// <summary>
        /// 读取Excel的内容到DataSet
        /// </summary>
        public static DataSet ReaderExcelToDataSet(string fileName, bool isDeleteFile)
        {
            //声明保存excel数据的DataSet
            DataSet ds = new DataSet();

            //读取方式
            string xlsDriver = "";
            string excelVersion = "";
            if (fileName.ToLower().EndsWith("xls"))
            {
                xlsDriver = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel {1};HDR=YES; IMEX=1;\"";
                excelVersion = "8.0";
            }
            else if (fileName.ToLower().EndsWith("xlsx"))
            {
                xlsDriver = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel {1};HDR=YES; IMEX=1;\"";
                excelVersion = "12.0";
            }
            else
            {
                throw new Exception("文件不是Excel格式");
            }


            OleDbConnection conn = new OleDbConnection(string.Format(xlsDriver, new string[] { fileName, excelVersion }));
            conn.Open();

            try
            {
                //excel中的所有sheet和视图
                DataTable schema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[]{null,null,null,"Table"});
                OleDbDataAdapter da = new OleDbDataAdapter();
                OleDbCommand dc = new OleDbCommand();
                dc.Connection = conn;

                //读取第一个Sheet
                List<string> tableNames = new List<string>();
                foreach (DataRow row in schema.Rows)
                {
                    string tablename = row["TABLE_NAME"].ToString();
                    tablename.Replace("'", string.Empty).Replace("\"", string.Empty).EndsWith("$");
                    tableNames.Add(tablename);
                }

                tableNames.ForEach(t =>
                {
                    if (!t.Equals(string.Empty))
                    {
                        dc.CommandText = "select * from [" + t + "] ";
                        da.SelectCommand = dc;
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        ds.Tables.Add(dt);
                    }
                    else
                    {
                        ds.Tables.Add(new DataTable());
                    }
                });

                da.Dispose();
                conn.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
            }

            //删除上传文件
            if (isDeleteFile && File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            return ds;
        }
        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            InitMemsList();
        }

        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitMemsList();
        }
    }
}