﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ImportUserInfoList.aspx.cs" Inherits="TG.Web.SystemSet.ImportUserInfoList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>批量导入</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>人力资源</a><i class="fa fa-angle-right"> </i><a>批量导入</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>人力资源信息导入
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">


                    <table class="table table-hover table-bordered table-full-width" id="tbData">
                            <tr>
                                <td style="width: 100px;">选择数据文件：</td>
                                <td style="width: 150px;">
                                    <asp:FileUpload ID="FileUpload" runat="server" Width="100%" /></td>
                                <td>
                                    <asp:Button Text="导入" runat="server" ID="btnImport" CssClass="btn btn-sm red" OnClick="btnImport_Click" /></td>
                            </tr>
                        </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
