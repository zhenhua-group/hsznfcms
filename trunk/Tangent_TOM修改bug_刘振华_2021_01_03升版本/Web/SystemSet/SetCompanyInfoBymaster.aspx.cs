﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class SetCompanyInfoBymaster : System.Web.UI.Page
    {
        public string TextContent
        {
            get;
            set;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetCollegePage();
            }
        }
        protected void GetCollegePage()
        {
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
            string strSql = " Select Top 1 * From cm_CollegeInfo";
            DataTable dt_content = bll.GetList(strSql).Tables[0];
            if (dt_content.Rows.Count > 0)
            {
                TextContent = dt_content.Rows[0][1].ToString();
            }
            else
            {
                TextContent = "请在系统设置中设置院系基本介绍！";
            }
        }
    }
}