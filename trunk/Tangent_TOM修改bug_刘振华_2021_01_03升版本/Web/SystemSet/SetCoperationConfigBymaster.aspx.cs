﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class SetCoperationConfigBymaster : System.Web.UI.Page
    {
        TG.BLL.cm_CoperationNumConfig bll = new TG.BLL.cm_CoperationNumConfig();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                for (int i = 1; i < 9; i++)
                {
                    ShowInfo(i);
                }
            }
        }
        //显示编号配置
        private void ShowInfo(int ID)
        {
            TG.BLL.cm_CoperationNumConfig bll = new BLL.cm_CoperationNumConfig();
            TG.Model.cm_CoperationNumConfig model = bll.GetModel(ID);
            //查询
            if (model != null)
            {
                if (ID == 1)
                {
                    this.txtStartNum.Text = model.CprNumStart.ToString().Trim();
                    this.txtEndNum.Text = model.CprNumEnd.ToString().Trim();
                }
                else if (ID == 2)
                {
                    this.txtStartNum2.Text = model.CprNumStart.ToString().Trim();
                    this.txtEndNum2.Text = model.CprNumEnd.ToString().Trim();
                }
                else if (ID == 3)
                {
                    this.txtStartNum3.Text = model.CprNumStart.ToString().Trim();
                    this.txtEndNum3.Text = model.CprNumEnd.ToString().Trim();
                }
                else if (ID == 4)
                {
                    this.txtStartNum4.Text = model.CprNumStart.ToString().Trim();
                    this.txtEndNum4.Text = model.CprNumEnd.ToString().Trim();
                }
                else if (ID == 5)
                {
                    this.txtStartNum5.Text = model.CprNumStart.ToString().Trim();
                    this.txtEndNum5.Text = model.CprNumEnd.ToString().Trim();
                }
                else if (ID == 6)
                {
                    this.txtStartNum6.Text = model.CprNumStart.ToString().Trim();
                    this.txtEndNum6.Text = model.CprNumEnd.ToString().Trim();
                }
                else if (ID == 7)
                {
                    this.txtStartNum7.Text = model.CprNumStart.ToString().Trim();
                    this.txtEndNum7.Text = model.CprNumEnd.ToString().Trim();
                }
                else if (ID == 8)
                {
                    this.txtStartNum8.Text = model.CprNumStart.ToString().Trim();
                    this.txtEndNum8.Text = model.CprNumEnd.ToString().Trim();
                }
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            TG.Model.cm_CoperationNumConfig model = new TG.Model.cm_CoperationNumConfig();
            TG.BLL.cm_CoperationNumConfig bll = new TG.BLL.cm_CoperationNumConfig();
            //建筑工程
            int ID = 1;
            string StartNum = this.txtStartNum.Text;
            string EndNum = this.txtEndNum.Text;
            string ProType = "民用建筑合同";

            model.ID = ID;
            model.CprNumStart = StartNum;
            model.CprNumEnd = EndNum;
            model.CprType = ProType;
            bll.Update(model);

            //城乡规划
            ID = 2;
            StartNum = this.txtStartNum2.Text;
            EndNum = this.txtEndNum2.Text;
            ProType = "工业建筑合同";

            model.ID = ID;
            model.CprNumStart = StartNum;
            model.CprNumEnd = EndNum;
            model.CprType = ProType;
            bll.Update(model);

            //市政工程
            ID = 3;
            StartNum = this.txtStartNum3.Text;
            EndNum = this.txtEndNum3.Text;
            ProType = "工程勘察合同";

            model.ID = ID;
            model.CprNumStart = StartNum;
            model.CprNumEnd = EndNum;
            model.CprType = ProType;
            bll.Update(model);

            //工程勘察
            ID = 4;
            StartNum = this.txtStartNum4.Text;
            EndNum = this.txtEndNum4.Text;
            ProType = "工程监理合同";

            model.ID = ID;
            model.CprNumStart = StartNum;
            model.CprNumEnd = EndNum;
            model.CprType = ProType;
            bll.Update(model);

            //工程施工
            ID = 5;
            StartNum = this.txtStartNum5.Text;
            EndNum = this.txtEndNum5.Text;
            ProType = "工程施工合同";

            model.ID = ID;
            model.CprNumStart = StartNum;
            model.CprNumEnd = EndNum;
            model.CprType = ProType;
            bll.Update(model);

            //工程咨询
            ID = 6;
            StartNum = this.txtStartNum6.Text;
            EndNum = this.txtEndNum6.Text;
            ProType = "工程咨询合同";

            model.ID = ID;
            model.CprNumStart = StartNum;
            model.CprNumEnd = EndNum;
            model.CprType = ProType;
            bll.Update(model);

            //工程总承包
            ID = 7;
            StartNum = this.txtStartNum7.Text;
            EndNum = this.txtEndNum7.Text;
            ProType = "工程总承包合同";

            model.ID = ID;
            model.CprNumStart = StartNum;
            model.CprNumEnd = EndNum;
            model.CprType = ProType;
            bll.Update(model);

            //工程代建
            ID = 8;
            StartNum = this.txtStartNum8.Text;
            EndNum = this.txtEndNum8.Text;
            ProType = "工程代建合同";

            model.ID = ID;
            model.CprNumStart = StartNum;
            model.CprNumEnd = EndNum;
            model.CprType = ProType;
            bll.Update(model);

            TG.Common.MessageBox.Show(this, "保存成功！");
        }
    }
}