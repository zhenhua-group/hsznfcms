﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Sys_Role.aspx.cs" Inherits="TG.Web.SystemSet.Sys_Role" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="../js/SystemSet/Sys_Role.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>

</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server" >
    <table class="cls_container">
        <tr>
            <td class="cls_head">
                &nbsp;&nbsp;当前位置：[角色管理]
            </td>
        </tr>
        <tr>
            <td class="cls_head_bar">
                <table class="cls_head_div">
                    <tr>
                        <td>
                            角色名称：<input id="txt_keyname" name="txt_keyname" type="text" runat="Server" /><td>
                                <td>
                                    <asp:ImageButton ID="btn_Search" runat="server" ImageUrl="~/Images/buttons/btn_search.gif"
                                        OnClick="btn_Search_Click" Width="64px" Height="21px" />
                                </td>
                                <td>
                                    <img src="../images/buttons/add.gif" style="border: none; cursor: hand;" id="btn_showadd">
                                </td>
                                <td>
                                    <asp:ImageButton ID="btn_DelCst" runat="server" Height="21px" ImageUrl="~/Images/buttons/delete.gif"
                                        OnClick="btn_DelCst_Click" Visible="False" />
                                </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table class="cls_content_head" style="width: 99%;">
            <tr>
                <td style="width: 5%;" align="center">
                    <input id="chk_All" type="checkbox" />
                </td>
                <td style="width: 5%;" align="center">
                    编号
                </td>
                <td style="width: 35%;" align="center">
                   角色名称
                </td>
                <td style="width: 50%;" align="center">
                    角色描述
                </td>
                <td style="width: 5%;" align="center">
                    编辑
                </td>
            </tr>
        </table>
        <asp:GridView ID="grid_pri" runat="server" AutoGenerateColumns="False" ShowHeader="False"
            Width="99%">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="chk_id" runat="server" CssClass="cls_chk" />
                        &nbsp;
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                </asp:TemplateField>
                <asp:BoundField DataField="pri_ID">
                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate>
                    <img src="../Images/role2.png" style="width:16px;height:16px;" />
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("pri_Name") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="35%" />
                </asp:TemplateField>
                <asp:BoundField DataField="pri_intro">
                    <ItemStyle HorizontalAlign="Center" Width="50%" />
                </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a href="#" class="cls_select">编辑</a>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
            CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条" CustomInfoTextAlign="Left"
            FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" OnPageChanged="AspNetPager1_PageChanged"
            PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox" PrevPageText="上一页" ShowBoxThreshold="10"
            ShowCustomInfoSection="Left" ShowPageIndexBox="Auto" SubmitButtonText="Go" TextAfterPageIndexBox="页"
            TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;" PageSize="10" SubmitButtonClass="submitbtn">
        </webdiyer:AspNetPager>
    </div>
    <div class="cls_data" id="div_add" style="display: none;">
        <fieldset style="font-size: 12px;">
            <legend>添加角色</legend>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 100px;">
                       角色名称：
                    </td>
                    <td>
                        <asp:TextBox ID="txt_priName" runat="server" CssClass="cls_input_text"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        角色描述：
                    </td>
                    <td>
                        <asp:TextBox ID="txt_priInfo" runat="server" CssClass="cls_input_text"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:ImageButton ID="btn_save" runat="server" ImageUrl="~/Images/buttons/btn_ok.gif" 
                            onclick="btn_save_Click" Visible="False"/>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div class="cls_data" id="div_edit" style="display: none;">
        <fieldset style="font-size: 12px;">
            <legend>编辑角色</legend>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 100px;">
                        ID
                    </td>
                    <td>
                        <asp:Label ID="lbl_priid" runat="server"></asp:Label>
                        <asp:HiddenField ID="hid_priid" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        角色名称：
                    </td>
                    <td>
                        <asp:TextBox ID="txt_priName0" runat="server" CssClass="cls_input_text"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        角色描述：
                    </td>
                    <td>
                        <asp:TextBox ID="txt_priInfo0" runat="server" CssClass="cls_input_text"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:ImageButton ID="btn_edit" runat="server" ImageUrl="~/Images/buttons/btn_ok.gif" 
                            onclick="btn_edit_Click" Visible="False" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    </form>
</body>
</html>
