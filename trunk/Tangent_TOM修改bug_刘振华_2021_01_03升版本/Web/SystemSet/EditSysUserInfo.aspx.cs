﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class EditSysUserInfo : PageBase
    {
        /// <summary>
        /// 判断请求类型
        /// </summary>
        public string ActionName
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        /// <summary>
        /// 返回所有部门
        /// </summary>
        public List<TG.Model.tg_unit> UnitList
        {
            get
            {
                string strWhere = " unit_ParentID<>0 AND unit_ID<>263 ";
                var list = new TG.BLL.tg_unit().GetModelList(strWhere);

                return list;
            }
        }
        /// <summary>
        /// 返回所有专业
        /// </summary>
        public List<TG.Model.tg_speciality> SpecList
        {
            get
            {
                string strWhere = "";
                var list = new TG.BLL.tg_speciality().GetModelList(strWhere);

                return list;
            }
        }

        /// <summary>
        /// 管理职位
        /// </summary>
        public List<TG.Model.cm_RoleMag> RoleMag
        {
            get
            {
                string strWhere = " IsMag=1 ";
                var list = new TG.BLL.cm_RoleMag().GetModelList(strWhere);

                return list;
            }
        }

        /// <summary>
        /// 技术职位
        /// </summary>
        public List<TG.Model.cm_RoleMag> RoleTec
        {
            get
            {
                string strWhere = " IsMag=0 ";
                var list = new TG.BLL.cm_RoleMag().GetModelList(strWhere);

                return list;
            }
        }
        /// <summary>
        /// 职称
        /// </summary>
        public List<TG.Model.cm_ProfesionName> Profesion
        {
            get
            {
                string strWhere = "";
                var list = new TG.BLL.cm_ProfesionName().GetModelList(strWhere);

                return list;
            }
        }

        /// <summary>
        /// 注册级别
        /// </summary>
        public List<TG.Model.cm_MemArchLevel> ArchLevel
        {
            get
            {
                string strWhere = "";
                var list = new TG.BLL.cm_MemArchLevel().GetModelList(strWhere);

                return list;
            }
        }

        /// <summary>
        /// 用户ID
        /// </summary>
        public int EditUserSysNo
        {
            get
            {
                int userid = 0;
                int.TryParse(Request["id"] ?? "0", out userid);
                return userid;
            }
        }
        /// <summary>
        /// 用户实例
        /// </summary>
        public TG.Model.tg_member EditMemModel
        {
            get
            {
                var model = new TG.BLL.tg_member().GetModel(this.EditUserSysNo);

                return model;
            }
        }
        /// <summary>
        /// 用户管理角色
        /// </summary>
        public TG.Model.tg_memberRole EditMemRoleModel
        {
            get
            {
                if (EditMemModel != null)
                {
                    string strWhere = string.Format(" mem_Id={0} AND mem_Unit_ID={1}", EditMemModel.mem_ID, EditMemModel.mem_Unit_ID);

                    var list = new TG.BLL.tg_memberRole().GetModelList(strWhere);

                    return list.Count > 0 ? list[0] : null;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// 注册角色
        /// </summary>
        public TG.Model.cm_MemArchLevelRelation EditMemRelationModel
        {
            get
            {
                if (EditMemModel != null)
                {
                    string strWhere = string.Format(" Mem_ID={0}", EditMemModel.mem_ID);

                    var list = new TG.BLL.cm_MemArchLevelRelation().GetModelList(strWhere);

                    return list.Count > 0 ? list[0] : null;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// 员工进出公司信息
        /// </summary>
        public TG.Model.tg_memberExt EditMemInOutCompanyModel
        {
            get
            {
                if (EditMemModel != null)
                {
                    string strWhere = string.Format(" Mem_ID={0}", EditMemModel.mem_ID);

                    var list = new TG.BLL.tg_memberExt().GetModelList(strWhere);

                    return list.Count > 0 ? list[0] : null;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// 扩展信息
        /// </summary>
        public TG.Model.tg_memberExtInfo EditMemExtInfoModel
        {
            get
            {
                if (EditMemModel != null)
                {
                    string strWhere = string.Format(" Mem_ID={0}", EditMemModel.mem_ID);

                    var list = new TG.BLL.tg_memberExtInfo().GetModelList(strWhere);

                    return list.Count > 0 ? list[0] : null;
                }
                else
                {
                    return null;
                }
            }
        }
        /// <summary>
        /// 调整记录
        /// </summary>
        public List<TG.Model.cm_PositionChangeRecord> PositionRecordList
        {
            get
            {
                if (EditMemModel != null)
                {
                    string strWhere = string.Format(" Mem_ID={0}", EditMemModel.mem_ID);

                    var list = new TG.BLL.cm_PositionChangeRecord().GetModelList(strWhere);

                    return list.Count > 0 ? list : null;
                }
                else
                {
                    return null;
                }
                
            }
        }
        /// <summary>
        /// 调整记录
        /// </summary>
        public List<TG.Model.cm_ChargeChangeRecord> ChargeChangeList
        {
            get
            {
                if (EditMemModel != null)
                {
                    string strWhere = string.Format(" Mem_ID={0}", EditMemModel.mem_ID);

                    var list = new TG.BLL.cm_ChargeChangeRecord().GetModelList(strWhere);

                    return list.Count > 0 ? list : null;
                }
                else
                {
                    return null;
                }

            }
        }

        /// <summary>
        /// 调整记录
        /// </summary>
        public List<TG.Model.cm_SheBaoChangeRecord> SheBaoChangeRecordList
        {
            get
            {
                if (EditMemModel != null)
                {
                    string strWhere = string.Format(" Mem_ID={0}", EditMemModel.mem_ID);

                    var list = new TG.BLL.cm_SheBaoChangeRecord().GetModelList(strWhere);

                    return list.Count > 0 ? list : null;
                }
                else
                {
                    return null;
                }

            }
        }

        /// <summary>
        /// 调整记录
        /// </summary>
        public List<TG.Model.cm_JiangliRecord> JiangliRecordList
        {
            get
            {
                if (EditMemModel != null)
                {
                    string strWhere = string.Format(" Mem_ID={0}", EditMemModel.mem_ID);

                    var list = new TG.BLL.cm_JiangliRecord().GetModelList(strWhere);

                    return list.Count > 0 ? list : null;
                }
                else
                {
                    return null;
                }

            }
        }

        /// <summary>
        /// 调整记录
        /// </summary>
        public List<TG.Model.cm_ChangfaRecord> ChangfaRecordList
        {
            get
            {
                if (EditMemModel != null)
                {
                    string strWhere = string.Format(" Mem_ID={0}", EditMemModel.mem_ID);

                    var list = new TG.BLL.cm_ChangfaRecord().GetModelList(strWhere);

                    return list.Count > 0 ? list : null;
                }
                else
                {
                    return null;
                }

            }
        }

        /// <summary>
        /// 调整记录
        /// </summary>
        public List<TG.Model.cm_SignCprRecord> SignCprRecordList
        {
            get
            {
                if (EditMemModel != null)
                {
                    string strWhere = string.Format(" Mem_ID={0}", EditMemModel.mem_ID);

                    var list = new TG.BLL.cm_SignCprRecord().GetModelList(strWhere);

                    return list.Count > 0 ? list : null;
                }
                else
                {
                    return null;
                }

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

    }
}