﻿using AjaxPro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.BLL;
using TG.Model;

namespace TG.Web.SystemSet
{
    public partial class BannerAndLogViewBymaster : System.Web.UI.Page
    {
        public int Type
        {
            get
            {
                int type = 0;
                int.TryParse(Request["viewType"], out type);
                return type;
            }
        }

        public string PageTitle
        {
            get
            {
                string title = "";
                if (Type == 0)
                {
                    title = "日志";
                }
                else
                {
                    title = "公告";
                }
                return title;
            }
        }

        /// <summary>
        /// 操作类型，0位新规
        /// </summary>
        public int Action
        {
            get
            {
                int action = 0;
                int.TryParse(Request["action"], out action);
                return action;
            }
        }

        public string InsertTitle
        {
            get
            {
                return Request["insertTitle"];
            }
        }

        public string InsertContent
        {
            get
            {
                return Request["insertContent"];
            }
        }

        public DateTime InsertInDate
        {
            get
            {
                return Convert.ToDateTime(Request["InDate"] ?? DateTime.Now.ToShortDateString());
            }
        }

        public List<WebBannerItemViewEntity> UserItemList
        {
            get
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<List<WebBannerItemViewEntity>>(Request["ItemList"]);
            }
        }

        public int SysNo
        {
            get
            {
                int sysNo = 0;
                int.TryParse(Request["sysNo"], out sysNo);
                return sysNo;
            }
        }

        public string TempWebbanerSysNo { get; set; }

        public string PreViewTitle { get; set; }

        public string PreViewContent { get; set; }

        public DateTime PreViewDate { get; set; }

        public string PreViewDateString
        {
            get
            {
                return PreViewDate.Year == 1 ? DateTime.Now.ToString("yyyy-MM-dd") : PreViewDate.ToString("yyyy-MM-dd");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(BannerAndLogViewBymaster));
            if (!IsPostBack)
            {
                if (Request.HttpMethod == "POST")
                {
                    if (Action == 0)
                    {
                        SaveRecord();
                    }
                    else
                    {
                        UpDateRecord();
                    }
                    Response.End();
                }
                else
                {
                    if (SysNo == 0)
                    {
                        TempWebbanerSysNo = DateTime.Now.ToString("yyyyMMddHHmmss");
                    }
                    else
                    {
                        TempWebbanerSysNo = SysNo.ToString();
                    }
                    if (Action == 1)
                    {
                        WebBannerBP bp = new WebBannerBP();

                        if (Type == 0)
                        {
                            WorkLogViewEntity entity = bp.GetWorkLog(SysNo);
                            PreViewContent = entity.Content;
                            PreViewTitle = entity.Title;
                            PreViewDate = entity.InDate;
                        }
                        else
                        {
                            WebBannerViewEntity entity = bp.GetWebBanner(SysNo);
                            PreViewContent = entity.Content;
                            PreViewTitle = entity.Title;
                            PreViewDate = entity.InDate;
                            this.RepeaterUsers.DataSource = entity.ItemList;
                            this.RepeaterUsers.DataBind();
                            RepeaterAttach.DataSource = new WebBannerAttachBP().GetWebBannerViewEntityList(SysNo.ToString());
                            RepeaterAttach.DataBind();
                        }
                    }
                }
            }
        }

        private void SaveRecord()
        {
            WebBannerBP bp = new WebBannerBP();

            int sysNo = 0;

            if (Type == 0)
            {
                sysNo = bp.InsertWorkLog(new WorkLogViewEntity { Title = InsertTitle, Content = InsertContent, InUser = 1, InDate = InsertInDate });
            }
            else
            {
                sysNo = bp.InsertWebBanner(new WebBannerViewEntity { Title = InsertTitle, Content = InsertContent, InUser = 1, ItemList = UserItemList, InDate = InsertInDate });
                new WebBannerAttachBP().UpdateWebBannerAttach(Request["tempSysNo"], sysNo);
            }
            Response.Write(sysNo);
        }

        private void UpDateRecord()
        {
            WebBannerBP bp = new WebBannerBP();

            int sysNo = 0;

            if (Type == 0)
            {
                sysNo = bp.UpdateWorkLog(new WorkLogViewEntity { Title = InsertTitle, Content = InsertContent, InUser = 1, SysNo = SysNo, InDate = InsertInDate });
            }
            else
            {
                sysNo = bp.UpdateWebBanner(new WebBannerViewEntity { Title = InsertTitle, Content = InsertContent, InUser = 1, SysNo = SysNo, ItemList = UserItemList, InDate = InsertInDate });
            }
            Response.Write(sysNo);
        }

        [AjaxMethod]
        public string GetAttachList(string webBannerSysNo)
        {
            List<WebBannerAttachViewEntity> resultList = new WebBannerAttachBP().GetWebBannerViewEntityList(webBannerSysNo);

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(resultList);

            return json;
        }

        [AjaxMethod]
        public string DeleteAttach(int sysNo)
        {
            string sql = string.Format("delete cm_WebBannerAttach where SysNo=" + sysNo);

            int count = TG.DBUtility.DbHelperSQL.ExecuteSql(sql);

            return count.ToString();
        }
    }
}