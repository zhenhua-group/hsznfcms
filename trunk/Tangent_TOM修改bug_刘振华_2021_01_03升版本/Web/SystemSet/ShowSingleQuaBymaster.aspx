﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ShowSingleQuaBymaster.aspx.cs" Inherits="TG.Web.SystemSet.ShowSingleQuaBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/SystemSet/ShowSingleQua.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        //获得ID
        var projid = '<%=GetProjectID() %>';
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>资质查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a>系统设置</a><i class="fa fa-angle-right"> </i><a>资质查看</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>资质信息</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h3 class="form-section">
                        资质信息</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>
                                            资质名称:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="lbl_title" runat="server" CssClass="form-control-static"></asp:Label>
                                            <asp:HiddenField ID="hid_proj" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:10%;">
                                            资质开始时间:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_Zztime" runat="server" CssClass="form-control-static"></asp:Label>
                                        </td>
                                        <td style="width:10%;">
                                            资质结束时间:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_Sptime" runat="server" CssClass="form-control-static"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            资质级别:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_jb" runat="server" CssClass="form-control-static"></asp:Label>
                                        </td>
                                        <td>
                                            年审时间:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_yeartime" runat="server" CssClass="form-control-static"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            资质内容:
                                        </td>
                                        <td colspan="3">
                                            <span id="lbl_content" runat="Server" cssclass="form-control-static"></span>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <h3 class="form-section">
                        缩略图</h3>
                    <div class="row">
                        <div class="form-group">
                            <label class="conttro-label col-md-2">
                                缩略图:
                            </label>
                        </div>
                        <div class="col-md-10" id="img_container" style="text-align: center">
                            <img id="img_small" alt="效果缩略图" src="../Attach_User/filedata/tempimg/tempimg.jpg"
                                style="width: 250px; height: 150px;" />
                            <span class="help-block"><font><font class=""></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="conttro-label col-md-2">
                                资质信息图:
                            </label>
                        </div>
                        <div class="col-md-10">
                            <table id="datas_att" class="table table-striped table-bordered table-advance table-hover">
                                <tr id="att_row">
                                    <td id="att_id" align="center" style="width: 40px;">
                                        序号
                                    </td>
                                    <td id="att_filename" align="center" style="width: 300px;">
                                        文件名称
                                    </td>
                                    <td id="att_filesize" align="center" style="width: 80px">
                                        文件大小
                                    </td>
                                    <td id="att_filetype" align="center" style="width: 80px;">
                                        文件类型
                                    </td>
                                    <td id="att_uptime" align="center" style="width: 120px;">
                                        上传时间
                                    </td>
                                    <td id="att_oper2" align="center" style="width: 40px;">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <span class="help-block"><font><font class=""></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <button type="button" id="btn_back" class="btn default" href="javascript:history.back();">
                                返回</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value='1' id="hid_type" />
</asp:Content>
