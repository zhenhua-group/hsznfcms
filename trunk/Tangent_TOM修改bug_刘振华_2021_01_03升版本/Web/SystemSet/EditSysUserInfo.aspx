﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="EditSysUserInfo.aspx.cs" Inherits="TG.Web.SystemSet.EditSysUserInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />

    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
     <link type="text/css" rel="stylesheet" href="../css/swfupload/default_cpr.css" />


    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>

    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>

    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_green.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_cpr.js"></script>
    <script type="text/javascript" src="../js/SystemSet/EditSysUserInfo.js"></script>

    <style>
        #tbUserForm
        {
            font-family: 微软雅黑;
            font-weight: 300;
        }

            #tbUserForm input, span, select
            {
                font-weight: 600;
            }

            #tbUserForm .title
            {
                font-weight: 600;
                font-size: 10pt;
                color: #4b8df8;
            }
    </style>

    <script>
        var cprid = '<%=EditUserSysNo %>';
        //用户ID
        var userid = '<%=EditUserSysNo %>';
        
        if (cprid != "0") {
            var swfu;
            window.onload = function () {
                swfu = new SWFUpload({

                    upload_url: "../ProcessUpload/upload_cpr.aspx?type=cpr&id=" + cprid + "&userid=" + userid,
                    flash_url: "../js/swfupload/swfupload.swf",
                    post_params: {
                        "ASPSESSID": "<%=Session.SessionID %>"
                    },
                    file_size_limit: "10 MB",
                    file_types: "*.pdf;*.jpg;*.doc;*.docx;*.ppt;*.pptx;*.xls;*.xlsx",
                    file_types_description: "上传",
                    file_upload_limit: "0",
                    file_queue_limit: "1",

                    //Events
                    file_queued_handler: fileQueued,
                    file_queue_error_handler: fileQueueError,
                    file_dialog_complete_handler: fileDialogComplete,
                    upload_progress_handler: uploadProgress,
                    upload_error_handler: uploadError,
                    upload_success_handler: uploadSuccessShowResult,
                    upload_complete_handler: uploadComplete,

                    // Button
                    button_placeholder_id: "spanButtonPlaceholder",
                    button_style: '{background-color:#d8d8d8 }',
                    button_width: 61,
                    button_height: 22, //<span class="btn default btn-file"><span class="fileupload-new" id="spanButtonPlaceholder"><i class="fa fa-paper-clip"></i>选择文件</span> </span>
                    button_text: '<span class="fileupload-new">选择文件</span>',
                    button_text_style: '.fileupload-new {background-color:#d8d8d8 ;} ',
                    button_text_top_padding: 1,
                    button_text_left_padding: 5,
                    custom_settings: {
                        upload_target: "divFileProgressContainer"
                    },
                    debug: false
                });
            }
        }
        //页面加载
        $(function () {
            //加载附件信息
            LoadCoperationAttach();
            //不能上传附件
            if (userid == "0") {
                $("#spanButtonPlaceholder").click(function () {
                    alert('请保存人员信息，再上传附件！');
                }).parent().attr('class', "btn default btn-file");
            }
        });
        //加载数据
        function GetAttachData() {
            LoadCoperationAttach("edit");
        }
        //加载附件信息
        function LoadCoperationAttach() {
            var data_att = "action=selectattach&cprid=" + $("#hidEditUserSysNo").val();
            $.ajax({
                type: "GET",
                url: "../HttpHandler/CommHandler.ashx",
                data: data_att,
                dataType: "json",
                success: function (result) {
                    if (result != null) {
                        var filedata = result == null ? "" : result.ds;
                        if ($("#datas_att tr").length > 1) {
                            $("#datas_att tr:gt(0)").remove();
                        }
                        $.each(filedata, function (i, n) {
                            var row = $("#att_row").clone();
                            var oper = "<span  style=\"cursor: pointer;color:Blue\" rel='" + n.ID + "'>删除</span>";
                            var oper2 = "<a href=\"/Coperation/DownLoadFile.aspx?fileName=" + escape(n.FileName) + "&FileURL=" + escape(n.FileUrl) + "\" target='_blank'>下载</a>";
                            var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                            row.find("#att_id").text(n.ID);
                            row.find("#att_filename").html(img + n.FileName);
                            row.find("#att_filename").attr("align", "left");
                            row.find("#att_filesize").text(n.FileSizeString);
                            row.find("#att_filetype").text(n.FileType);
                            row.find("#att_uptime").text(n.UploadTime);
                            row.find("#att_oper").html(oper);
                            row.find("#att_oper2").html(oper2);
                            row.find("#att_oper span").click(function () {
                                if (confirm("确定要删除本条附件吗？")) {
                                    delCprAttach($(this));
                                }
                            });
                            //修改样式
                            row.addClass("cls_TableRow");
                            row.appendTo("#datas_att");
                        });
                        filedata = "";
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        //删除附件
        function delCprAttach(link) {
            var data = "action=delcprattach&attid=" + link.attr("rel");
            $.ajax({
                type: "GET",
                url: "../HttpHandler/CommHandler.ashx",
                data: data,
                dataType: "text",
                success: function (result) {
                    if (result == "yes") {
                        link.parents("tr:first").remove();
                        alert("附件删除成功！");
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("系统错误！");
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>编辑人力资源信息</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>人力资源</a><i class="fa fa-angle-right"> </i><a>编辑人力资源信息</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-user"></i>编辑人力资源信息
            </div>
            <div class="tools">
                <a class="collapse" href="javascript:;"></a>
            </div>
        </div>
        <div class="portlet-body" style="display: block;">

            <table class="table table-hover table-bordered table-full-width" id="tbUserForm">
                <thead style="display: none;">
                    <tr>
                        <th colspan="5">
                            <a class="btn red" id="btnSave">保存信息</a>
                            <a class="btn default" id="btnBack" href="javascript:this.close();">关闭</a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="background-color: #eee; color: #000;" colspan="5"><span class="title">员工登录信息</span></td>

                    </tr>
                    <tr>
                        <td style="width: 150px;">员工账号：</td>
                        <td style="width: 250px;">
                            <input type="text" name="name" value="<%= EditMemModel!=null?EditMemModel.mem_Login:"" %>" id="txt_mem_Login" maxlength="16" />
                            <span id="sp_mem_Login"><%= EditMemModel!=null?EditMemModel.mem_Login:"" %></span>
                        </td>
                        <td style="width: 150px;">真实姓名：</td>
                        <td style="width: 250px;">
                            <input type="text" name="name" value="<%= EditMemModel!=null?EditMemModel.mem_Name:"" %>" id="txt_mem_Name" maxlength="16" />
                            <span id="sp_mem_Name"><%= EditMemModel!=null?EditMemModel.mem_Name:"" %></span>

                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>员工密码：</td>
                        <td>
                            <input type="password" name="name" value="<%= EditMemModel!=null?EditMemModel.mem_Password:"" %>" id="txt_mem_Password" maxlength="16" />
                        </td>
                        <td>在职状态：</td>
                        <td>
                            <select id="txt_mem_isFired">
                                <option value="0" <%= EditMemModel!=null?(EditMemModel.mem_isFired==0?"selected='selected'":""):"" %>>在职</option>
                                <option value="1" <%= EditMemModel!=null?(EditMemModel.mem_isFired==1?"selected='selected'":""):"" %>>离职</option>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="background-color: #eee; color: #000;" colspan="5"><span class="title">员工基本信息</span></td>

                    </tr>
                    <tr>
                        <td>所属部门：</td>
                        <td>
                            <select style="width: 100px;" id="txt_mem_Unit_ID">
                                <% UnitList.ForEach(c =>
                                       {
                                           if (EditMemModel != null)
                                           { %>
                                <option value="<%=c.unit_ID%>" <%= c.unit_ID==EditMemModel.mem_Unit_ID?"selected='selected'":"" %>><%=c.unit_Name%></option>
                                <%}
                                           else
                                           {%>

                                <option value="<%=c.unit_ID%>"><%=c.unit_Name%></option>
                                <%}
                                       }); %>
                            </select>
                        </td>
                        <td>性别：</td>
                        <td>
                            <span id="txt_mem_Sex"><%= EditMemModel!=null?EditMemModel.mem_Sex:"无"%></span>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>从事专业：</td>
                        <td>
                            <select style="width: 100px;" id="txt_mem_Speciality_ID">
                                <% SpecList.ForEach(c =>
                                       {
                                           if (EditMemModel != null)
                                           { %>
                                <option value="<%=c.spe_ID %>" <%=c.spe_ID==EditMemModel.mem_Speciality_ID?"selected='selected'":"" %>><%=c.spe_Name %></option>
                                <%}
                                           else
                                           {%>
                                <option value="<%=c.spe_ID %>"><%=c.spe_Name %></option>
                                <% }
                                       }); %>
                            </select>
                        </td>
                        <td>管理职位：</td>
                        <td>
                            <select style="width: 100px;" id="txt_RoleIdMag">
                                <option value="-1">--请选择--</option>
                                <% RoleMag.ForEach(c =>
                                       {
                                           if (EditMemRoleModel != null)
                                           {
                                %>
                                <option value="<%=c.ID %>" <%= c.ID== EditMemRoleModel.RoleIdMag?"selected='selected'":"" %>><%=c.RoleName %></option>
                                <%}
                                           else
                                           {%>
                                <option value="<%=c.ID %>"><%=c.RoleName %></option>

                                <% }
                                       }); %>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>技术职位：</td>
                        <td>
                            <select style="width: 100px;" id="txt_RoleIdTec">
                                <option value="-1">--请选择--</option>
                                <% RoleTec.ForEach(c =>
                                       {
                                           if (EditMemRoleModel != null)
                                           {
                                %>
                                <option value="<%=c.ID %>" <%= c.ID== EditMemRoleModel.RoleIdTec?"selected='selected'":"" %>><%=c.RoleName %></option>
                                <%}
                                           else
                                           {%>
                                <option value="<%=c.ID %>"><%=c.RoleName %></option>

                                <% }
                                       }); %>
                            </select>
                        </td>
                        <td>户籍所在地：</td>
                        <%--<td><span id="txt_mem_CodeAddr"><%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_CodeAddr:"" %></span></td>--%>
                        <td><input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_CodeAddr:"" %>" id="txt_mem_CodeAddr" maxlength="18"/></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>身份证号：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_Code:"" %>" id="txt_mem_Code" maxlength="18" />
                            <%--<span>*</span>--%>
                        </td>
                        <td>出生日期：</td>
                        <td>
                            <span id="txt_mem_Birthday"><%= EditMemModel!=null?Convert.ToDateTime(EditMemModel.mem_Birthday).ToString("yyyy-MM-dd"):"无" %></span>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>联系方式：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemModel!=null?EditMemModel.mem_Mobile:"" %>" id="txt_mem_Mobile" maxlength="11" /><span></span>
                        </td>
                        <td>分机号：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemModel!=null?EditMemModel.mem_Telephone:"" %>" id="txt_mem_Telephone" maxlength="11" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>年龄：</td>
                        <td>
                            <span id="txt_mem_Age"><%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_Age.ToString():"" %></span>
                        </td>
                        <td></td>
                        <td>
                            <input type="hidden" name="name" value="4" id="txt_mem_Principalship_ID" /></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="background-color: #eee; color: #000;" colspan="5"><span class="title">员工教育情况</span></td>
                    </tr>
                    <tr>
                        <td>毕业学校：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_School:"" %>" id="txt_mem_School" maxlength="300" />
                        </td>
                        <td>学位/历：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_SchLevel:"" %>" id="txt_mem_SchLevel" maxlength="50" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>毕业专业：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_SchSpec:"" %>" id="txt_mem_SchSpec" maxlength="50" />
                        </td>
                        <td>毕业时间：</td>
                        <td>
                            <input class="Wdate" onclick="WdatePicker({ readOnly: true })" type="text" name="name" value="<%= EditMemExtInfoModel!=null?(EditMemExtInfoModel.mem_SchOutDate!=null?Convert.ToDateTime(EditMemExtInfoModel.mem_SchOutDate).ToString("yyyy-MM-dd"):""):"" %>" id="txt_mem_SchOutDate" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>毕业学校：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_School2:"" %>" id="txt_mem_School2" maxlength="300" />
                        </td>
                        <td>学位/历：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_SchLevel2:"" %>" id="txt_mem_SchLevel2" maxlength="50" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>毕业专业：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_SchSpec2:"" %>" id="txt_mem_SchSpec2" maxlength="50" />
                        </td>
                        <td>毕业时间：</td>
                        <td>
                            <input class="Wdate" onclick="WdatePicker({ readOnly: true })" type="text" name="name" value="<%= EditMemExtInfoModel!=null?(EditMemExtInfoModel.mem_SchOutDate2!=null?Convert.ToDateTime(EditMemExtInfoModel.mem_SchOutDate2).ToString("yyyy-MM-dd"):""):"" %>" id="txt_mem_SchOutDate2" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="background-color: #eee; color: #000;" colspan="5"><span class="title">员工证书情况</span></td>

                    </tr>
                    <tr>
                        <td>职称评定：</td>
                        <td>
                            <select style="width: 100px;" id="txt_mem_ArchLevel">
                                <option value="-1">---请选择---</option>
                                <% Profesion.ForEach(c =>
                                       {
                                           if (EditMemExtInfoModel != null)
                                           { %>
                                <option value="<%=c.ID %>" <%= c.ID==EditMemExtInfoModel.mem_ArchLevel?"selected":"" %>><%=c.Name %></option>
                                <%}
                                           else
                                           {%>
                                <option value="<%=c.ID %>"><%=c.Name %></option>

                                <% }
                                       }); %>
                            </select>
                        </td>
                        <td>评定时间：</td>
                        <td>
                            <input class="Wdate" onclick="WdatePicker({ readOnly: true })" type="text" name="name" value="<%= EditMemExtInfoModel!=null?(EditMemExtInfoModel.mem_ArchLevelTime!=null?Convert.ToDateTime(EditMemExtInfoModel.mem_ArchLevelTime).ToString("yyyy-MM-dd"):""):"" %>" id="txt_mem_ArchLevelTime" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>注册：</td>
                        <td>
                            <select style="width: 220px;" id="txt_mem_ArchReg">
                                <option value="-1">---请选择---</option>

                                <% ArchLevel.ForEach(c =>
                                       {
                                           if (EditMemRelationModel != null)
                                           { %>
                                <option value="<%=c.ArchLevel_ID%>" <%= c.ArchLevel_ID== EditMemRelationModel.ArchLevel_ID?"selected='selected'":"" %>><%=c.Name%></option>
                                <%}
                                           else
                                           {%>
                                <option value="<%=c.ArchLevel_ID%>"><%=c.Name%></option>
                                <%}
                                       }); %>
                            </select>
                        </td>
                        <td>注册时间：</td>
                        <td>
                            <input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= EditMemExtInfoModel!=null?(EditMemExtInfoModel.mem_ArchRegTime!=null?Convert.ToDateTime(EditMemExtInfoModel.mem_ArchRegTime).ToString("yyyy-MM-dd"):""):"" %>" id="txt_mem_ArchRegTime" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>注册地点：</td>
                        <td>
                            <select style="width: 100px;" id="txt_mem_ArchRegAddr">
                                <option value="-1">---请选择---</option>
                                <option value="0" <%= EditMemExtInfoModel!=null?(0==EditMemExtInfoModel.mem_ArchRegAddr?"selected":""):"" %>>在公司注册</option>
                                <option value="1" <%= EditMemExtInfoModel!=null?(1==EditMemExtInfoModel.mem_ArchRegAddr?"selected":""):"" %>>不在公司注册</option>
                            </select>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="background-color: #eee; color: #000;" colspan="5"><span class="title">员工工作情况</span></td>
                    </tr>
                    <tr>
                        <td>参加工作时间：</td>
                        <td>
                            <input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= EditMemExtInfoModel!=null?(EditMemExtInfoModel.mem_WorkTime!=null?Convert.ToDateTime(EditMemExtInfoModel.mem_WorkTime).ToString("yyyy-MM-dd"):""):"" %>" id="txt_mem_WorkTime" />
                        </td>
                        <td>入司时间：</td>
                        <td>
                            <input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= EditMemExtInfoModel!=null?(EditMemExtInfoModel.mem_InCompanyTime!=null?Convert.ToDateTime(EditMemExtInfoModel.mem_InCompanyTime).ToString("yyyy-MM-dd"):"") :"" %>" id="txt_mem_InCompanyTime" />
                        </td>
                        <td></td>


                    </tr>
                    <tr>
                        <td>工作间隔时间：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_WorkDiff.ToString():"" %>" id="txt_mem_WorkDiff" maxlength="2" />年
                        </td>
                        <td>入司间隔时间：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_InCompanyDiff.ToString():"" %>" id="txt_mem_InCompanyDiff" maxlength="2" />年
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>工作间隔时间(备注)：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_WorkDiffSub:"" %>" id="txt_mem_WorkDiffSub" maxlength="50" />
                        </td>
                        <td>入司间隔时间(备注)：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_InCompanyDiffSub:"" %>" id="txt_mem_InCompanyDiffSub" maxlength="50" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>

                        <td>工作年限：</td>
                        <td>
                            <span id="txt_mem_WorkYear"><%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_WorkYear.ToString():"0" %></span>年
                        </td>
                        <td>入司年限：</td>
                        <td>
                            <span id="txt_mem_InCompanyYear"><%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_InCompanyYear.ToString():"0" %></span>年
                        </td>

                        <td></td>
                    </tr>
                    <tr>
                        <td>试用周期：</td>
                        <td>
                            <select style="width: 60px;" id="txt_mem_WorkTemp">
                                <option value="3" <%= EditMemExtInfoModel!=null?(3==EditMemExtInfoModel.mem_WorkTemp?"selected":""):"" %>>3</option>
                                <option value="6" <%= EditMemExtInfoModel!=null?(6==EditMemExtInfoModel.mem_WorkTemp?"selected":""):"" %>>6</option>

                            </select>月
                        </td>
                        <td>岗位工龄：</td>
                        <td>
                            <select style="width: 60px;" id="txt_mem_InCompanyTemp">
                                <option value="0" <%= EditMemExtInfoModel!=null?(0==EditMemExtInfoModel.mem_InCompanyTemp?"selected":""):"" %>>0</option>
                                <option value="1" <%= EditMemExtInfoModel!=null?(1==EditMemExtInfoModel.mem_InCompanyTemp?"selected":""):"" %>>1</option>
                                <%-- <option value="2" <%= EditMemExtInfoModel!=null?(2==EditMemExtInfoModel.mem_InCompanyTemp?"selected":""):"" %>>2</option>--%>
                                <option value="3" <%= EditMemExtInfoModel!=null?(3==EditMemExtInfoModel.mem_InCompanyTemp?"selected":""):"" %>>3</option>
                                <option value="4" <%= EditMemExtInfoModel!=null?(4==EditMemExtInfoModel.mem_InCompanyTemp?"selected":""):"" %>>4</option>
                                <option value="5" <%= EditMemExtInfoModel!=null?(5==EditMemExtInfoModel.mem_InCompanyTemp?"selected":""):"" %>>5</option>
                                <option value="6" <%= EditMemExtInfoModel!=null?(6==EditMemExtInfoModel.mem_InCompanyTemp?"selected":""):"" %>>6</option>
                            </select>年
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="background-color: #eee; color: #000;" colspan="5"><span class="title">员工人力信息</span></td>
                    </tr>
                    <tr>
                        <td>档案所在地：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_FileAddr.ToString():"" %>" id="txt_mem_FileAddr" maxlength="100" />
                        </td>
                        <td>休假基数：</td>
                        <td>
                            <span id="txt_mem_Holidaybase"><%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_Holidaybase.ToString():"0" %></span>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>年假时间：</td>
                        <td>
                            <span id="txt_mem_HolidayYear"><%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_HolidayYear.ToString():"0" %></span>天
                        </td>
                        <td>总经理特批：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_MagPishi.ToString():"" %>" id="txt_mem_MagPishi" maxlength="100" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>转正时间：</td>
                        <td>
                            <input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= EditMemExtInfoModel!=null?(EditMemExtInfoModel.mem_ZhuanZheng!=null?Convert.ToDateTime(EditMemExtInfoModel.mem_ZhuanZheng).ToString("yyyy-MM-dd"):""):"" %>" id="txt_mem_ZhuanZheng" />
                        </td>
                        <td>工资：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_GongZi.ToString():"" %>" id="txt_mem_GongZi" maxlength="8" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>交通补贴：</td>
                        <td>
                            <select style="width: 60px;" id="txt_mem_Jiaotong">
                                <option value="0" <%= EditMemExtInfoModel!=null?(0==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>0</option>
                                <option value="400" <%= EditMemExtInfoModel!=null?(400==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>400</option>
                                <option value="300" <%= EditMemExtInfoModel!=null?(300==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>300</option>
                                <option value="200" <%= EditMemExtInfoModel!=null?(200==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>200</option>
                                <option value="100" <%= EditMemExtInfoModel!=null?(100==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>100</option>
                            </select>
                        </td>
                        <td>通讯补贴：</td>
                        <td>
                            <select style="width: 60px;" id="txt_mem_Tongxun">
                                <option value="0" <%= EditMemExtInfoModel!=null?(0==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>0</option>
                                <option value="150" <%= EditMemExtInfoModel!=null?(150==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>150</option>
                                <option value="100" <%= EditMemExtInfoModel!=null?(100==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>100</option>
                                <option value="70" <%= EditMemExtInfoModel!=null?(70==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>70</option>
                                <option value="50" <%= EditMemExtInfoModel!=null?(50==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>50</option>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>应发工资：</td>
                        <td>
                            <span id="txt_mem_YingFa"><%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_YingFa.ToString():"" %></span>元
                        </td>
                        <td>社保基数：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_SheBaobase.ToString():"" %>" id="txt_mem_SheBaobase" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>公积金：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_Gongjijin.ToString():"" %>" id="txt_mem_Gongjijin" maxlength="8" />
                        </td>
                        <td>备注：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_GongjijinSub.ToString():"" %>" id="txt_mem_GongjijinSub" maxlength="100" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>预发奖金：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_Yufa.ToString():"" %>" id="txt_mem_Yufa" maxlength="8" />
                        </td>
                        <td>年薪：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_YearCharge.ToString():"" %>" id="txt_mem_YearCharge" maxlength="8" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="background-color: #eee; color: #000;" colspan="5"><span class="title">调整信息</span></td>
                    </tr>
                    <tr>
                        <td>职务调整情况</td>
                        <td colspan="3">
                            <table class="table table-hover table-bordered" id="tbPositionChangeList" style="font-size: 9px;">
                                <thead>
                                    <tr>
                                        <td style="width: 65px;">管理职位</td>
                                        <td style="width: 60px;">调整后</td>
                                        <td style="width: 65px;">技术职位</td>
                                        <td style="width: 60px;">调整后</td>
                                        <td style="width: 65px;">交通补贴</td>
                                        <td style="width: 60px;">调整后</td>
                                        <td style="width: 65px;">通讯补贴</td>
                                        <td style="width: 60px;">调整后</td>
                                        <td>调整时间</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% if (PositionRecordList != null)
                                       {

                                           PositionRecordList.ForEach(c =>
                                           { %>

                                    <tr>
                                        <td><%= c.MagRoleName %></td>
                                        <td><%= c.MagRoleName2 %></td>
                                        <td><%= c.TecRoleName %></td>
                                        <td><%= c.MagRoleName2 %></td>
                                        <td><%= c.JiaoTong %></td>
                                        <td><%= c.JiaoTong2 %></td>
                                        <td><%= c.Tongxun %></td>
                                        <td><%= c.Tongxun2 %></td>
                                        <td><%= Convert.ToDateTime(c.ChangeDate).ToString("yyyy-MM-dd") %></td>
                                    </tr>
                                    <%});
                                       } %>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="10" style="text-align: right">
                                            <% if (EditUserSysNo != 0)
                                               { %>
                                            <a href="#addChange" data-toggle="modal" class="btn default btn-xs green-stripe" id="btnSetMemPosition">添加</a>
                                            <%}
                                               else
                                               { %>
                                            <a href="#" data-toggle="modal" class="btn default btn-xs green-stripe disabled">添加</a>
                                            <%} %>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>

                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>工资调整情况</td>
                        <td colspan="3">
                            <table class="table table-hover table-bordered" id="tbChargeChangeList" style="font-size: 9px;">
                                <thead>
                                    <tr>
                                        <td style="width: 70px;">工资</td>
                                        <td style="width: 65px;">调整后</td>
                                        <td style="width: 70px;">预发奖金</td>
                                        <td style="width: 65px;">调整后</td>
                                        <td>调整时间</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% if (ChargeChangeList != null)
                                       {

                                           ChargeChangeList.ForEach(c =>
                                           { %>

                                    <tr>
                                        <td><%= c.mem_GongZi %></td>
                                        <td><%= c.mem_GongZi2 %></td>
                                        <td><%= c.mem_Yufa %></td>
                                        <td><%= c.mem_Yufa2 %></td>
                                        <td><%= Convert.ToDateTime(c.ChangeDate).ToString("yyyy-MM-dd") %></td>
                                    </tr>
                                    <%});
                                       } %>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="10" style="text-align: right">
                                            <% if (EditUserSysNo != 0)
                                               { %>
                                            <a href="#addCharge" data-toggle="modal" class="btn default btn-xs green-stripe" id="btnSetMemCharge">添加</a>
                                            <%}
                                               else
                                               { %>
                                            <a href="#" data-toggle="modal" class="btn default btn-xs green-stripe disabled">添加</a>
                                            <%} %>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>

                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>社保公积金调整</td>
                        <td colspan="3">
                            <table class="table table-hover table-bordered" id="tbGongjijinChangeList" style="font-size: 9px;">
                                <thead>
                                    <tr>
                                        <td style="width: 70px;">社保基数</td>
                                        <td style="width: 60px;">调整后</td>
                                        <td style="width: 70px;">公积金</td>
                                        <td style="width: 60px;">调整后</td>
                                        <td>调整时间</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% if (SheBaoChangeRecordList != null)
                                       {

                                           SheBaoChangeRecordList.ForEach(c =>
                                           { %>

                                    <tr>
                                        <td><%= c.mem_SheBaobase %></td>
                                        <td><%= c.mem_SheBaobase2 %></td>
                                        <td><%= c.mem_Gongjijin %></td>
                                        <td><%= c.mem_Gongjijin2 %></td>
                                        <td><%= Convert.ToDateTime(c.ChangeDate).ToString("yyyy-MM-dd") %></td>
                                    </tr>
                                    <%});
                                       } %>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="10" style="text-align: right">
                                            <% if (EditUserSysNo != 0)
                                               { %>
                                            <a href="#addGongjijin" data-toggle="modal" class="btn default btn-xs green-stripe" id="btnSetMemSheBao">添加</a>
                                            <%}
                                               else
                                               { %>
                                            <a href="#" data-toggle="modal" class="btn default btn-xs green-stripe disabled">添加</a>
                                            <%} %>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>

                        </td>
                        <td></td>
                    </tr>
                    <tr style="display: none;">
                        <td style="background-color: #eee; color: #000;" colspan="5"><span class="title">合同情况</span></td>
                    </tr>
                    <tr style="display: none;">
                        <td>签订时间：</td>
                        <td>
                            <input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= EditMemExtInfoModel!=null?(EditMemExtInfoModel.mem_CoperationDate!=null?Convert.ToDateTime(EditMemExtInfoModel.mem_CoperationDate).ToString("yyyy-MM-dd"):""):"" %>" id="txt_mem_CoperationDate" />
                        </td>
                        <td>合同性质：</td>
                        <td>
                            <select id="txt_mem_CoperationType">
                                <option value="0" <%= EditMemExtInfoModel!=null?(0==EditMemExtInfoModel.mem_CoperationType?"selected":""):"" %>>签订</option>
                                <option value="1" <%= EditMemExtInfoModel!=null?(1==EditMemExtInfoModel.mem_CoperationType?"selected":""):"" %>>续签</option>
                                <option value="2" <%= EditMemExtInfoModel!=null?(2==EditMemExtInfoModel.mem_CoperationType?"selected":""):"" %>>无固定</option>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                    <tr style="display: none;">
                        <td>到期时间：</td>
                        <td>
                            <span id="txt_mem_CoperationDateEnd"><%= EditMemExtInfoModel!=null?(EditMemExtInfoModel.mem_CoperationDateEnd!=null?Convert.ToDateTime(EditMemExtInfoModel.mem_CoperationDateEnd).ToString("yyyy-MM-dd"):""):"" %></span>
                        </td>
                        <td>备注：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_CoperationSub :"" %>" id="txt_mem_CoperationSub" maxlength="100" />
                        </td>
                        <td></td>
                    </tr>

                    <tr>
                        <td>合同签订</td>
                        <td colspan="3">
                            <table class="table table-hover table-bordered" id="tbSignCprList" style="font-size: 9px;">
                                <thead>
                                    <tr>
                                        <td style="width: 80px;">签订时间</td>
                                        <td style="width: 80px;">合同性质</td>
                                        <td style="width: 80px;">到期时间</td>
                                        <td>备注</td>
                                        <td style="width: 50px;display:none;"></td>
                                        <td style="width: 50px;"></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% if (SignCprRecordList != null)
                                       {

                                           SignCprRecordList.ForEach(c =>
                                           { %>

                                    <tr>
                                        <td><%= Convert.ToDateTime(c.mem_CoperationDate).ToString("yyyy-MM-dd") %></td>
                                        <% if (c.mem_CoperationType == 0)
                                           { %>
                                        <td>签订</td>
                                        <%}
                                           else if (c.mem_CoperationType == 1)
                                           { %>
                                        <td>续签</td>
                                        <%}
                                           else if (c.mem_CoperationType == 2)
                                           { %>
                                        <td>无固定</td>
                                        <%} %>
                                        <td><%= Convert.ToDateTime(c.mem_CoperationDateEnd).ToString("yyyy-MM-dd") %></td>
                                        <td><%= c.mem_CoperationSub %></td>
                                        <td style="display:none;"><a href="#addSignCpr" data-toggle="modal" data-id="<%= c.ID %>" data-type="edit">编辑</a></td>
                                        <td><a href="###" data-id="<%= c.ID %>" data-type="del">删除</a></td>
                                    </tr>
                                    <%});
                                       } %>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="10" style="text-align: right">
                                            <% if (EditUserSysNo != 0)
                                               { %>
                                            <a href="#addSignCpr" data-toggle="modal" class="btn default btn-xs green-stripe" id="btnAddSignCpr">添加</a>
                                            <%}
                                               else
                                               { %>
                                            <a href="#" data-toggle="modal" class="btn default btn-xs green-stripe disabled">添加</a>
                                            <%} %>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: #eee; color: #000;" colspan="5"><span class="title">参与项目情况</span></td>
                    </tr>
                    <tr>
                        <td>时间：</td>
                        <td></td>
                        <td>项目名称：</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="background-color: #eee; color: #000;" colspan="5"><span class="title">离职情况</span></td>
                    </tr>
                    <tr>
                        <td>离职情况：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_OutCompany:"" %>" id="txt_mem_OutCompany" />
                        </td>
                        <td>离职时间：</td>
                        <td>
                            <input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= EditMemInOutCompanyModel!=null? (EditMemInOutCompanyModel.mem_OutTime!=null?Convert.ToDateTime(EditMemInOutCompanyModel.mem_OutTime).ToString("yyyy-MM-dd"):""):"" %>" id="txt_mem_OutCompanyDate" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>离职性质：</td>
                        <td>
                            <select style="width: 100px;" id="txt_mem_OutCompanyType">
                                <option value="-1">--请选择--</option>
                                <option value="0" <%= EditMemExtInfoModel!=null?(0==EditMemExtInfoModel.mem_OutCompanyType?"selected":""):"" %>>主动</option>
                                <option value="1" <%= EditMemExtInfoModel!=null?(1==EditMemExtInfoModel.mem_OutCompanyType?"selected":""):"" %>>被动</option>
                                <option value="2" <%= EditMemExtInfoModel!=null?(2==EditMemExtInfoModel.mem_OutCompanyType?"selected":""):"" %>>辞退</option>
                            </select>
                        </td>
                        <td>原因：</td>
                        <td>
                            <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_OutCompanySub:"" %>" id="txt_mem_OutCompanySub" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>奖励情况：</td>
                        <td colspan="3">
                            <table class="table table-hover table-bordered" id="tbJiangliChangeList" style="font-size: 9px;">
                                <thead>
                                    <tr>
                                        <td style="width: 80px;">奖励时间</td>
                                        <td style="width: 200px;">奖励原因</td>
                                        <td>备注</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% if (JiangliRecordList != null)
                                       {

                                           JiangliRecordList.ForEach(c =>
                                           { %>

                                    <tr>
                                        <td><%= Convert.ToDateTime(c.mem_JiangliDate).ToString("yyyy-MM-dd") %></td>
                                        <td><%= c.mem_JiangliWhy %></td>
                                        <td><%= c.mem_JiangliSub %></td>

                                    </tr>
                                    <%});
                                       } %>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="10" style="text-align: right">
                                            <% if (EditUserSysNo != 0)
                                               { %>
                                            <a href="#addAward" data-toggle="modal" class="btn default btn-xs green-stripe" id="btnSetMemJiangli">添加</a>
                                            <%}
                                               else
                                               { %>
                                            <a href="#" data-toggle="modal" class="btn default btn-xs green-stripe disabled">添加</a>
                                            <%} %>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </td>

                        <td></td>
                    </tr>
                    <tr>
                        <td>惩罚情况：</td>
                        <td colspan="3">
                            <table class="table table-hover table-bordered" id="tbChengfaChangeList" style="font-size: 9px;">
                                <thead>
                                    <tr>
                                        <td style="width: 80px;">惩罚时间</td>
                                        <td style="width: 200px;">惩罚原因</td>
                                        <td>备注</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% if (ChangfaRecordList != null)
                                       {

                                           ChangfaRecordList.ForEach(c =>
                                           { %>

                                    <tr>
                                        <td><%= Convert.ToDateTime(c.mem_ChengfaDate).ToString("yyyy-MM-dd") %></td>
                                        <td><%= c.mem_ChengfaWhy %></td>
                                        <td><%= c.mem_ChengfaSub %></td>

                                    </tr>
                                    <%});
                                       } %>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="10" style="text-align: right">
                                            <% if (EditUserSysNo != 0)
                                               { %>
                                            <a href="#addPunish" data-toggle="modal" class="btn default btn-xs green-stripe" id="btnSetMemChengfa">添加</a>
                                            <%}
                                               else
                                               { %>
                                            <a href="#" data-toggle="modal" class="btn default btn-xs green-stripe disabled">添加</a>
                                            <%} %>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>

                        </td>
                        <td></td>
                    </tr>
                    <tr>

                        <td style="background-color: #eee; color: #000;"><span class="title">附件上传</span></td>
                        <td style="background-color: #eee; color: #000;" colspan="2">
                            <div id="divFileProgressContainer" style="float: right;">
                            </div>
                        </td>
                        <td style="background-color: #eee; color: #000;">
                            <span class="btn red btn-file"><span class="fileupload-new" id="spanButtonPlaceholder">
                                <i class="fa fa-paper-clip"></i>选择文件</span> </span>

                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table class="table table-bordered table-striped table-condensed flip-contentr" id="datas_att" style="width: 98%;" align="center">
                                <thead class="flip-content">
                                    <tr id="att_row">
                                        <td style="width: 40px;" align="center" id="att_id">序号
                                        </td>
                                        <td style="width: 300px;" align="center" id="att_filename">文件名称
                                        </td>
                                        <td style="width: 80px" align="center" id="att_filesize">文件大小
                                        </td>
                                        <td style="width: 80px;" align="center" id="att_filetype">文件类型
                                        </td>
                                        <td style="width: 120px;" align="center" id="att_uptime">上传时间
                                        </td>
                                        <td style="width: 40px;" align="center" id="att_oper">&nbsp;
                                        </td>
                                        <td style="width: 40px;" align="center" id="att_oper2">&nbsp;
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5">
                            <a class="btn red" id="btnSave2">保存信息</a>
                            <a class="btn default" id="btnBack2" href="javascript:this.close();">关闭</a>
                        </td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>

    <!--职务调整 --->
    <div id="addChange" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true" style="display: none; width: 900px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">职务调整添加
            </h4>
        </div>
        <div class="modal-body">
            <div style="color: #222222;">

                <table class="table table-hover table-bordered table-full-width">
                    <thead>
                        <tr>
                            <th colspan="4">调整时间：<input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= DateTime.Now.ToString("yyyy-MM-dd") %>" id="txt_ChangeDate1" style="width: 100px;" />
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 80px;">管理职位：</td>
                            <td>
                                <select style="width: 100px;" id="txt_RoleIdMag2" disabled="disabled">
                                    <option value="-1">--请选择--</option>
                                    <% RoleMag.ForEach(c =>
                                       {
                                           if (EditMemRoleModel != null)
                                           {
                                    %>
                                    <option value="<%=c.ID %>" <%= c.ID== EditMemRoleModel.RoleIdMag?"selected='selected'":"" %>><%=c.RoleName %></option>
                                    <%}
                                           else
                                           {%>
                                    <option value="<%=c.ID %>"><%=c.RoleName %></option>

                                    <% }
                                       }); %>
                                </select>
                            </td>
                            <td style="width: 80px;">调整后</td>
                            <td>
                                <select style="width: 100px;" id="txt_RoleIdMag3">
                                    <option value="-1">--请选择--</option>
                                    <% RoleMag.ForEach(c =>
                                       {
                                           if (EditMemRoleModel != null)
                                           {
                                    %>
                                    <option value="<%=c.ID %>" <%= c.ID== EditMemRoleModel.RoleIdMag?"selected='selected'":"" %>><%=c.RoleName %></option>
                                    <%}
                                           else
                                           {%>
                                    <option value="<%=c.ID %>"><%=c.RoleName %></option>

                                    <% }
                                       }); %>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>技术职位：</td>
                            <td>
                                <select style="width: 100px;" id="txt_RoleIdTec2" disabled="disabled">
                                    <option value="-1">--请选择--</option>
                                    <% RoleTec.ForEach(c =>
                                       {
                                           if (EditMemRoleModel != null)
                                           {
                                    %>
                                    <option value="<%=c.ID %>" <%= c.ID== EditMemRoleModel.RoleIdTec?"selected='selected'":"" %>><%=c.RoleName %></option>
                                    <%}
                                           else
                                           {%>
                                    <option value="<%=c.ID %>"><%=c.RoleName %></option>

                                    <% }
                                       }); %>
                                </select>
                            </td>
                            <td>调整后：</td>
                            <td>
                                <select style="width: 100px;" id="txt_RoleIdTec3">
                                    <option value="-1">--请选择--</option>
                                    <% RoleTec.ForEach(c =>
                                       {
                                           if (EditMemRoleModel != null)
                                           {
                                    %>
                                    <option value="<%=c.ID %>" <%= c.ID== EditMemRoleModel.RoleIdTec?"selected='selected'":"" %>><%=c.RoleName %></option>
                                    <%}
                                           else
                                           {%>
                                    <option value="<%=c.ID %>"><%=c.RoleName %></option>

                                    <% }
                                       }); %>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>交通补贴：</td>
                            <td>
                                <select style="width: 60px;" id="txt_mem_Jiaotong2" disabled="disabled">
                                    <option value="0">0</option>
                                    <option value="400" <%= EditMemExtInfoModel!=null?(400==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>400</option>
                                    <option value="300" <%= EditMemExtInfoModel!=null?(300==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>300</option>
                                    <option value="200" <%= EditMemExtInfoModel!=null?(200==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>200</option>
                                    <option value="100" <%= EditMemExtInfoModel!=null?(100==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>100</option>
                                </select>
                            </td>
                            <td>调整后：</td>
                            <td>
                                <select style="width: 60px;" id="txt_mem_Jiaotong3">
                                    <option value="0">0</option>
                                    <option value="400" <%= EditMemExtInfoModel!=null?(400==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>400</option>
                                    <option value="300" <%= EditMemExtInfoModel!=null?(300==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>300</option>
                                    <option value="200" <%= EditMemExtInfoModel!=null?(200==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>200</option>
                                    <option value="100" <%= EditMemExtInfoModel!=null?(100==EditMemExtInfoModel.mem_Jiaotong?"selected":""):"" %>>100</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>通讯补贴：</td>
                            <td>
                                <select style="width: 60px;" id="txt_mem_Tongxun2" disabled="disabled">
                                    <option value="0" <%= EditMemExtInfoModel!=null?(0==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>0</option>
                                    <option value="150" <%= EditMemExtInfoModel!=null?(150==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>150</option>
                                    <option value="100" <%= EditMemExtInfoModel!=null?(100==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>100</option>
                                    <option value="70" <%= EditMemExtInfoModel!=null?(70==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>70</option>
                                    <option value="50" <%= EditMemExtInfoModel!=null?(50==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>50</option>
                                </select>
                            </td>
                            <td>调整后：</td>
                            <td>
                                <select style="width: 60px;" id="txt_mem_Tongxun3">
                                    <option value="0" <%= EditMemExtInfoModel!=null?(0==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>0</option>
                                    <option value="150" <%= EditMemExtInfoModel!=null?(150==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>150</option>
                                    <option value="100" <%= EditMemExtInfoModel!=null?(100==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>100</option>
                                    <option value="70" <%= EditMemExtInfoModel!=null?(70==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>70</option>
                                    <option value="50" <%= EditMemExtInfoModel!=null?(50==EditMemExtInfoModel.mem_Tongxun?"selected":""):"" %>>50</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="center" colspan="4">
                                <a href="#" class="btn btn-sm red" id="btn_AddChange">保存</a>
                                <button type="button" data-dismiss="modal" class="btn btn-sm" id="btn_AddChangeClose">关闭</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>


            </div>
        </div>
    </div>

    <!--工资调整-->
    <div id="addCharge" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true" style="display: none; width: 900px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">工资调整添加
            </h4>
        </div>
        <div class="modal-body">
            <div style="color: #222222;">

                <table class="table table-hover table-bordered table-full-width">
                    <thead>
                        <tr>
                            <th colspan="4">调整时间：<input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= DateTime.Now.ToString("yyyy-MM-dd") %>" id="txt_ChangeDate2" style="width: 100px;" />
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 80px;">工资：</td>
                            <td>
                                <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_GongZi.ToString():"0" %>" id="txt_mem_GongZi2" maxlength="8" readonly />
                            </td>
                            <td style="width: 80px;">调整后</td>
                            <td>
                                <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_GongZi.ToString():"0" %>" id="txt_mem_GongZi3" maxlength="8" />
                            </td>
                        </tr>
                        <tr>
                            <td>预发奖金：</td>
                            <td>
                                <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_Yufa.ToString():"0" %>" id="txt_mem_Yufa2" maxlength="8" readonly />

                            </td>
                            <td>调整后：</td>
                            <td>
                                <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_Yufa.ToString():"0" %>" id="txt_mem_Yufa3" maxlength="8" />

                            </td>
                        </tr>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="center" colspan="4">
                                <a href="#" class="btn btn-sm red" id="btn_GongZiChange">保存</a>
                                <button type="button" data-dismiss="modal" class="btn btn-sm " id="btn_GongZiChangeClose">关闭</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>


            </div>
        </div>
    </div>

    <!---公积金调整--->
    <div id="addGongjijin" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true" style="display: none; width: 900px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">公积金调整添加
            </h4>
        </div>
        <div class="modal-body">
            <div style="color: #222222;">

                <table class="table table-hover table-bordered table-full-width">
                    <thead>
                        <tr>
                            <th colspan="4">调整时间：<input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= DateTime.Now.ToString("yyyy-MM-dd") %>" id="txt_ChangeDate3" style="width: 100px;" />
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 80px;">社保基数：</td>
                            <td>
                                <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_SheBaobase.ToString():"0" %>" id="txt_mem_SheBaobase2" readonly />

                            </td>
                            <td style="width: 80px;">调整后：</td>
                            <td>
                                <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_SheBaobase.ToString():"0" %>" id="txt_mem_SheBaobase3" />

                            </td>
                        </tr>
                        <tr>
                            <td>公积金：</td>
                            <td>
                                <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_Gongjijin.ToString():"0" %>" id="txt_mem_Gongjijin2" maxlength="8" readonly />

                            </td>
                            <td>调整后：</td>
                            <td>
                                <input type="text" name="name" value="<%= EditMemExtInfoModel!=null?EditMemExtInfoModel.mem_Gongjijin.ToString():"0" %>" id="txt_mem_Gongjijin3" maxlength="8" />

                            </td>
                        </tr>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="center" colspan="4">
                                <a href="#" class="btn btn-sm red" id="btn_SheBaoChange">保存</a>
                                <button type="button" data-dismiss="modal" class="btn btn-sm " id="btn_SheBaoChangeClose">关闭</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>


            </div>
        </div>
    </div>

    <!--奖励情况添加-->
    <div id="addAward" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true" style="display: none; width: 900px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">奖励情况
            </h4>
        </div>
        <div class="modal-body">
            <div style="color: #222222;">

                <table class="table table-hover table-bordered table-full-width">
                    <thead>
                        <tr>
                            <th colspan="4">奖励录入
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 80px;">奖励时间：</td>
                            <td>
                                <input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= DateTime.Now.ToString("yyyy-MM-dd") %>" id="txt_ChangeDate4" style="width: 100px;" />
                            </td>

                        </tr>
                        <tr>
                            <td>奖励原因：</td>
                            <td>
                                <input type="text" name="name" value="" id="txt_mem_JiangliWhy" maxlength="300" style="width: 350px;" />

                            </td>
                        </tr>
                        <tr>
                            <td>备注：</td>
                            <td>
                                <input type="text" name="name" value="" id="txt_mem_JiangliSub" maxlength="300" style="width: 350px;" />

                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="center" colspan="4">
                                <a href="#" class="btn btn-sm red" id="btn_Jiangli">保存</a>
                                <button type="button" data-dismiss="modal" class="btn btn-sm " id="btn_JiangliClose">关闭</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>


            </div>
        </div>
    </div>

    <!--惩罚情况-->
    <div id="addPunish" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true" style="display: none; width: 900px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">惩罚情况
            </h4>
        </div>
        <div class="modal-body">
            <div style="color: #222222;">

                <table class="table table-hover table-bordered table-full-width" id="tbNameAdd2">
                    <thead>
                        <tr>
                            <th colspan="4">惩罚录入

                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 80px;">惩罚时间：</td>
                            <td>
                                <input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= DateTime.Now.ToString("yyyy-MM-dd") %>" id="txt_ChangeDate5" style="width: 100px;" />
                            </td>

                        </tr>
                        <tr>
                            <td style="width: 80px;">惩罚原因：</td>
                            <td>
                                <input type="text" name="name" value="" id="txt_mem_ChengfaWhy" maxlength="300" style="width: 350px;" />
                            </td>

                        </tr>
                        <tr>
                            <td style="width: 80px;">备注：</td>
                            <td>
                                <input type="text" name="name" value="" id="txt_mem_ChengfaSub" maxlength="300" style="width: 350px;" />
                            </td>

                        </tr>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="center" colspan="4">
                                <a href="#" class="btn btn-sm red" id="btn_Chengfa">保存</a>
                                <button type="button" data-dismiss="modal" class="btn btn-sm " id="btn_ChengfaClose">关闭</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>


            </div>
        </div>
    </div>


    <!--合同签订-->
    <div id="addSignCpr" class="modal fade yellow" tabindex="-1" data-width="600" aria-hidden="true" style="display: none; width: 900px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">合同签订
            </h4>
        </div>
        <div class="modal-body">
            <div style="color: #222222;">

                <table class="table table-hover table-bordered table-full-width" id="tbSignCprAdd">
                    <thead>
                        <tr>
                            <th colspan="4">合同签订

                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 80px;">签订时间：</td>
                            <td>
                                <input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= DateTime.Now.ToString("yyyy-MM-dd") %>" id="txt_mem_CoperationDate2" style="width: 100px;" />
                            </td>

                        </tr>
                        <tr>
                            <td style="width: 80px;">合同性质：</td>
                            <td>

                                <select id="txt_mem_CoperationType2">
                                    <option value="0" <%= EditMemExtInfoModel!=null?(0==EditMemExtInfoModel.mem_CoperationType?"selected":""):"" %>>签订</option>
                                    <option value="1" <%= EditMemExtInfoModel!=null?(1==EditMemExtInfoModel.mem_CoperationType?"selected":""):"" %>>续签</option>
                                    <option value="2" <%= EditMemExtInfoModel!=null?(2==EditMemExtInfoModel.mem_CoperationType?"selected":""):"" %>>无固定</option>
                                </select>
                            </td>

                        </tr>
                        <tr>
                            <td style="width: 80px;">到期时间：</td>
                            <td>
                                <input type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" name="name" value="<%= DateTime.Now.ToString("yyyy-MM-dd") %>" id="txt_mem_CoperationDateEnd2" style="width: 100px;" />
                            </td>

                        </tr>
                        <tr>
                            <td style="width: 80px;">备注：</td>
                            <td>
                                <input type="text" name="name" value="" id="txt_mem_CoperationSub2" maxlength="300" style="width: 350px;" />
                            </td>

                        </tr>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td align="center" colspan="4">
                                <a href="#" class="btn btn-sm red" id="btn_SignCpr">保存</a>
                                <button type="button" data-dismiss="modal" class="btn btn-sm " id="btn_SignCprClose">关闭</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>


            </div>
        </div>
    </div>

    <!--用户ID-->
    <input type="hidden" name="name" value="<%= EditUserSysNo %>" id="hidEditUserSysNo" />
    <input type="hidden" name="name" value="<%= UserSysNo %>" id="hidUserSysNo" />
    <input type="hidden" name="name" value="0" id="hidSignCprID" />

    <!--用户浏览方式--->
    <input type="hidden" name="name" value="<%= ActionName %>" id="hidAction" />


    <script>
        SysUserInfo.Init();
    </script>
</asp:Content>
