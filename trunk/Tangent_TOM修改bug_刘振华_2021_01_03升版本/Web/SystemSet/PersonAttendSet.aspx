﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="PersonAttendSet.aspx.cs" Inherits="TG.Web.SystemSet.PersonAttendSet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery.alerts.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script src="../js/Global.js"></script>
    <script src="../js/SystemSet/personattendset.js"></script>
    <script type="text/javascript">

        $(function () {
       

           
        });

    </script>
    <style type="text/css">
        .kd {
            width:40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>人员出勤设置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>系统设置</a><i class="fa fa-angle-right"> </i><a>常用设置</a><i class="fa fa-angle-right"> </i><a>人员出勤设置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>人员出勤设置
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="table-responsive">

                        <table class="table table-hover table-bordered table-full-width">
                            <tr>
                                <td style="width: 80px;">部门:
                                </td>
                                <td style="width: 130px;">
                                    <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server" AppendDataBoundItems="True"
                                        OnSelectedIndexChanged="drp_unit3_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                                 <td style="width: 60px;">年份：</td>
                                <td style="width: 80px;">
                                    <asp:DropDownList runat="server" ID="drpYear">                                       
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 60px;">用户：</td>
                                <td style="width: 120px;">
                                    <input class="form-control input-sm" type="text" id="txt_keyname"
                                    runat="Server" />
                                </td> 
                                <td>
                                    <asp:Button CssClass="btn blue" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" />
                                </td>
                                <td></td>
                            </tr>
                        </table> 
                        <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>人员信息列表
                    </div>
                    <div class="actions">
                        <input type="button" id="btn_save" class="btn btn-sm red" value="保存" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView ID="grid_mem" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover dataTable" Width="100%" HeaderStyle-HorizontalAlign="Center" EnableModelValidation="True" OnRowDataBound="grid_mem_RowDataBound">
                                <Columns>
                                   
                                    <asp:BoundField DataField="mem_ID" HeaderText="编号">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="30px" />
                                    </asp:BoundField>                                   
                                    <asp:BoundField DataField="mem_Name" HeaderText="姓名">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="50px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="1月">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                           <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                           	<tr>
                                           		<td>上班：</td>
                                                   <td><asp:TextBox ID="shang1"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                               	<tr>
                                           		<td>下班：</td>
                                                   <td><asp:TextBox ID="xia1"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                           </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="2月">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                           <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                           	<tr>
                                           		<td>上班：</td>
                                                   <td><asp:TextBox ID="shang2"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                               	<tr>
                                           		<td>下班：</td>
                                                   <td><asp:TextBox ID="xia2"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                           </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="3月">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                           <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                           	<tr>
                                           		<td>上班：</td>
                                                   <td><asp:TextBox ID="shang3"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                               	<tr>
                                           		<td>下班：</td>
                                                   <td><asp:TextBox ID="xia3"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                           </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="4月">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                           <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                           	<tr>
                                           		<td>上班：</td>
                                                   <td><asp:TextBox ID="shang4"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                               	<tr>
                                           		<td>下班：</td>
                                                   <td><asp:TextBox ID="xia4"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                           </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="5月">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                           <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                           	<tr>
                                           		<td>上班：</td>
                                                   <td><asp:TextBox ID="shang5"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                               	<tr>
                                           		<td>下班：</td>
                                                   <td><asp:TextBox ID="xia5"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                           </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="6月">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                           <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                           	<tr>
                                           		<td>上班：</td>
                                                   <td><asp:TextBox ID="shang6"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                               	<tr>
                                           		<td>下班：</td>
                                                   <td><asp:TextBox ID="xia6"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                           </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="7月">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                           <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                           	<tr>
                                           		<td>上班：</td>
                                                   <td><asp:TextBox ID="shang7"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                               	<tr>
                                           		<td>下班：</td>
                                                   <td><asp:TextBox ID="xia7"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                           </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="8月">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                           <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                           	<tr>
                                           		<td>上班：</td>
                                                   <td><asp:TextBox ID="shang8"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                               	<tr>
                                           		<td>下班：</td>
                                                   <td><asp:TextBox ID="xia8"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                           </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="9月">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                           <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                           	<tr>
                                           		<td>上班：</td>
                                                   <td><asp:TextBox ID="shang9"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                               	<tr>
                                           		<td>下班：</td>
                                                   <td><asp:TextBox ID="xia9"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                           </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="10月">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                           <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                           	<tr>
                                           		<td>上班：</td>
                                                   <td><asp:TextBox ID="shang10"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                               	<tr>
                                           		<td>下班：</td>
                                                   <td><asp:TextBox ID="xia10"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                           </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="11月">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                           <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                           	<tr>
                                           		<td>上班：</td>
                                                   <td><asp:TextBox ID="shang11"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                               	<tr>
                                           		<td>下班：</td>
                                                   <td><asp:TextBox ID="xia11"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                           </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="12月">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                           <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                           	<tr>
                                           		<td>上班：</td>
                                                   <td><asp:TextBox ID="shang12"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                               	<tr>
                                           		<td>下班：</td>
                                                   <td><asp:TextBox ID="xia12"  CssClass="kd" runat="server" Text=""/></td>
                                           	</tr>
                                           </table>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>


                                </Columns>
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:GridView>                            
                           
                        </div>
                    </div>
                </div>
            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
