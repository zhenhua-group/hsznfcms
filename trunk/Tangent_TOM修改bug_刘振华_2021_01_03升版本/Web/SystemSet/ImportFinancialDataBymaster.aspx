﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ImportFinancialDataBymaster.aspx.cs" Inherits="TG.Web.SystemSet.ImportFinancialDataBymaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    </style>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            $("#<%=btnUpload.ClientID%>").click(function () {

                var name = $("#<%=FileUpload.ClientID%>").val();
                if (name == '') {
                    alert("请选择上传的文件");
                    return false;
                }
                var extend = name.substring(name.lastIndexOf('.') + 1);

                if (extend == "xls" || extend == "xlsx") {

                }
                else {
                    alert("请上传excel的文件格式");
                    return false;
                }
            });

    
        });
    </script>
    <script type="text/javascript" src="../js/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>财务报表数据导入 </small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用设置</a><i class="fa fa-angle-right"> </i><a>财务报表数据导入</a>
    </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>财务报表上传
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%" class=" table-bordered">
                            <tr>
                                <td>
                                    数据文件:
                                </td>
                                <td>
                                    <asp:FileUpload ID="FileUpload" runat="server" Width="600px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    文件类型:
                                </td>
                                <td>
                                    <asp:RadioButton ID="rad1" runat="server" GroupName="AA" Text="岩土工程" Checked="True" />
                                    <asp:RadioButton ID="rad2" runat="server" GroupName="AA" Text="建设监理" />
                                    <asp:RadioButton ID="rad3" runat="server" GroupName="AA" Text="建筑设计" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnUpload" runat="server" Text="上 传" CssClass="btn blue btn-sm" OnClick="btnUpload_Click" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i>报表详细
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
            </div>
        </div>
        <div class="portlet-body form">
            <div class="form-body">
                <table border="0" cellspacing="0" cellpadding="0" width="60%">
                    <tr>
                        <td>
                            文件类型:
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_Type" runat="server" CssClass="form-control" Width="120px">
                                <asp:ListItem>请选择</asp:ListItem>
                                <asp:ListItem Value="t">岩土工程</asp:ListItem>
                                <asp:ListItem Value="j">建设监理</asp:ListItem>
                                <asp:ListItem Value="y">建筑设计</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            年份:
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_year" runat="server" CssClass="form-control" Width="120px">
                                <asp:ListItem>请选择</asp:ListItem>
                                <asp:ListItem>2010</asp:ListItem>
                                <asp:ListItem>2011</asp:ListItem>
                                <asp:ListItem>2012</asp:ListItem>
                                <asp:ListItem>2013</asp:ListItem>
                                <asp:ListItem>2014</asp:ListItem>
                                <asp:ListItem>2015</asp:ListItem>
                                <asp:ListItem>2016</asp:ListItem>
                                <asp:ListItem>2017</asp:ListItem>
                                <asp:ListItem>2018</asp:ListItem>
                                <asp:ListItem>2019</asp:ListItem>
                                <asp:ListItem>2020</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            月份:
                        </td>
                        <td>
                            <asp:DropDownList ID="drp_month" runat="server" CssClass="form-control" Width="120px">
                                <asp:ListItem>请选择</asp:ListItem>
                                <asp:ListItem>01</asp:ListItem>
                                <asp:ListItem>02</asp:ListItem>
                                <asp:ListItem>03</asp:ListItem>
                                <asp:ListItem>04</asp:ListItem>
                                <asp:ListItem>05</asp:ListItem>
                                <asp:ListItem>06</asp:ListItem>
                                <asp:ListItem>07</asp:ListItem>
                                <asp:ListItem>08</asp:ListItem>
                                <asp:ListItem>09</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btn_Search" runat="server" Text="查询" CssClass="btn blue" OnClick="btn_Search_Click" />
                        </td>
                    </tr>
                </table>
                <br />
                <div class="row">
                    <div style="width: 98%; margin: 0 auto;">
                        <asp:GridView ID="grid_Financial" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                            Width="100%" EnableModelValidation="True" CssClass="table table-bordered table-hover">
                            <Columns>
                                <asp:BoundField DataField="FinancialNum" HeaderText="序号">
                                    <ItemStyle Width="20%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Name" HeaderText="汇款单位">
                                    <ItemStyle Width="40%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Account" HeaderText="金额">
                                    <ItemStyle Width="20%" HorizontalAlign="left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="VoucherNo" HeaderText="凭单号">
                                    <ItemStyle HorizontalAlign="left" Width="20%" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                没有数据
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                            CustomInfoSectionWidth="32%" CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                            CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                            OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                            PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                            SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                            PageSize="12" SubmitButtonClass="submitbtn">
                        </webdiyer:AspNetPager>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
