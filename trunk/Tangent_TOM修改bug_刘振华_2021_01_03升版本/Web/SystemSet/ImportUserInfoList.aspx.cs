﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class ImportUserInfoList : System.Web.UI.Page
    {

        private TG.BLL.tg_member bllmem = new TG.BLL.tg_member();
        private TG.BLL.tg_memberRole bllrole = new TG.BLL.tg_memberRole();
        private TG.BLL.tg_memberExt bllext = new TG.BLL.tg_memberExt();
        private TG.BLL.cm_MemArchLevelRelation bllrelation = new TG.BLL.cm_MemArchLevelRelation();
        private TG.BLL.tg_memberExtInfo bllextinfo = new TG.BLL.tg_memberExtInfo();

        private TG.BLL.cm_RoleMag bllrolemag = new BLL.cm_RoleMag();
        private TG.BLL.cm_MemArchLevel bllarchlevel = new BLL.cm_MemArchLevel();
        private TG.BLL.cm_Role bllcmrole = new BLL.cm_Role();
        private TG.BLL.cm_ProfesionName bllprofesion = new BLL.cm_ProfesionName();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (FileUpload.PostedFile != null && FileUpload.PostedFile.ContentLength > 0)
            {

                string FileImportDirectory = Server.MapPath("~/Template/"); ;
                //取得上传文件名称
                string uplodeName = FileUpload.PostedFile.FileName;
                //保存文件
                string extensionName = FileUpload.PostedFile.FileName.Substring(FileUpload.PostedFile.FileName.LastIndexOf("."));
                //生成的文件名称
                string fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + (new Random()).Next(999).ToString("D3") + extensionName;
                //创建存放路径
                if (!Directory.Exists(FileImportDirectory))
                {
                    Directory.CreateDirectory(FileImportDirectory);
                }
                //保存文件
                FileUpload.SaveAs(FileImportDirectory + "\\" + fileName);
                //读取到DataSet
                DataSet ds = new DataSet();

                try
                {
                    ds = ReaderExcelToDataSet(FileImportDirectory + "\\" + fileName, true);
                }
                catch (Exception ex)
                {
                    //把07 .xlsx格式 强制改成xls格式 抛出异常
                    if (ex.Message.Trim().Equals("外部表不是预期的格式。"))
                    {
                        TG.Common.MessageBox.Show(this.Page, "请上传标准版本(2003或者2007）格式的excel！");
                        return;
                    }
                    else
                    {
                        TG.Common.MessageBox.Show(this.Page, "请选择正确的模板！");
                        return;
                    }
                }

                if (ds.Tables.Count == 0)
                {
                    TG.Common.MessageBox.Show(this.Page, "上传文件无数据！");
                    return;
                }
                //模板 默认是9列
                if (ds.Tables[0].Columns.Count != 44)
                {
                    TG.Common.MessageBox.Show(this.Page, "请选择正确的模板！");
                    return;
                }
                //循环模板里的数据
                foreach (DataTable dt in ds.Tables)
                {
                    InsertAndUpdateUserData(dt);
                }


                Response.Write("alert('数据导入成功！');");
            }
        }
        /// <summary>
        /// 循环插入更新
        /// </summary>
        /// <param name="dt"></param>
        private void InsertAndUpdateUserData(DataTable dt)
        {

            if (dt.Rows.Count > 0)
            {

                var bllUnit = new TG.BLL.tg_unit();

                foreach (DataRow dr in dt.Rows)
                {
                    if (string.IsNullOrEmpty(dr[0].ToString()))
                    {
                        continue;
                    }
                    //姓名
                    string memName = dr[0].ToString().Trim();
                    //部门
                    string memUnit = dr[1].ToString().Trim();
                    var unitModelList = bllUnit.GetModelList(" unit_Name ='" + memUnit + "'");
                    var unitID = 0;
                    if (unitModelList.Count > 0)
                    {
                        unitID = unitModelList[0].unit_ID;
                    }
                    //查询条件
                    string strWhere = string.Format(" mem_Name='{0}' AND mem_Unit_ID={1}", memName, unitID);

                    var memList = bllmem.GetModelList(strWhere);
                    //已存在，更新
                    if (memList.Count > 0)
                    {
                        var oldtgMember = memList[0];
                        //更新人员的ID
                        int newMemID = oldtgMember.mem_ID;

                        //管理职位
                        var tgMemRole = new TG.Model.tg_memberRole();
                        string Where = string.Format(" mem_Id={0} AND mem_Unit_ID={1} ", newMemID, oldtgMember.mem_Unit_ID);
                        var listrole = bllrole.GetModelList(Where);
                        //不存在插入
                        if (listrole.Count == 0)
                        {
                            //管理职位
                            string roleMng = dr[4].ToString().Trim();
                            Where = string.Format(" RoleName='{0}'", roleMng);
                            var memRoleList = bllrolemag.GetModelList(Where);

                            int roleid = 0;
                            //查询此管理名称在系统中
                            if (memRoleList.Count > 0)
                            {
                                tgMemRole.mem_Id = newMemID;
                                tgMemRole.mem_Unit_ID = oldtgMember.mem_Unit_ID;
                                tgMemRole.RoleIdMag = memRoleList[0].ID;
                                tgMemRole.RoleIdTec = 0;
                                tgMemRole.Weights = memRoleList[0].Weights;
                                //添加
                                try
                                {
                                    roleid = bllrole.Add(tgMemRole);
                                }
                                catch (Exception ex)
                                { }
                            }

                            //技术职位
                            string roleTec = dr[5].ToString().Trim();
                            Where = string.Format(" RoleName='{0}'", roleTec);
                            memRoleList = bllrolemag.GetModelList(Where);

                            if (memRoleList.Count > 0)
                            {
                                tgMemRole.mem_Id = newMemID;
                                tgMemRole.mem_Unit_ID = oldtgMember.mem_Unit_ID;
                                tgMemRole.RoleIdMag = roleid != 0 ? tgMemRole.RoleIdMag : 0;
                                tgMemRole.RoleIdTec = memRoleList[0].ID;
                                tgMemRole.Weights = roleid != 0 ? tgMemRole.Weights : memRoleList[0].Weights;

                                if (roleid == 0)
                                {
                                    //添加
                                    bllrole.Add(tgMemRole);
                                }
                                else
                                {
                                    tgMemRole.ID = roleid;
                                    //更新
                                    bllrole.Update(tgMemRole);
                                }
                            }

                        }
                        else
                        {
                            tgMemRole = listrole[0];
                            //管理职位
                            string roleMng = dr[4].ToString().Trim();
                            Where = string.Format(" RoleName='{0}'", roleMng);
                            var memRoleList = bllrolemag.GetModelList(Where);

                            //查询此管理名称在系统中
                            if (memRoleList.Count > 0)
                            {
                                tgMemRole.RoleIdMag = memRoleList[0].ID;
                                tgMemRole.Weights = memRoleList[0].Weights;
                                //添加
                                bllrole.Update(tgMemRole);
                            }

                            //技术职位
                            string roleTec = dr[5].ToString().Trim();
                            Where = string.Format(" RoleName='{0}'", roleTec);
                            memRoleList = bllrolemag.GetModelList(Where);

                            if (memRoleList.Count > 0)
                            {
                                tgMemRole.RoleIdTec = memRoleList[0].ID;
                                tgMemRole.Weights = tgMemRole.RoleIdMag == 0 ? memRoleList[0].Weights : tgMemRole.Weights;

                                //更新
                                bllrole.Update(tgMemRole);
                            }
                        }


                        //======================插入扩展信息表=======================
                        var tgMemExtInfo = new TG.Model.tg_memberExtInfo();
                        Where = string.Format(" mem_ID={0} ", newMemID);
                        var listextinfo = bllextinfo.GetModelList(Where);
                        //存在，先赋值
                        if (listextinfo.Count>0)
                        {
                            tgMemExtInfo = listextinfo[0];
                        }
                        //出生年月
                        string mem_BirthDay = dr[8].ToString();
                        oldtgMember.mem_Birthday = mem_BirthDay != "" ? Convert.ToDateTime(mem_BirthDay) : DateTime.Now;
                        //联系方式
                        oldtgMember.mem_Mobile = dr[10].ToString();
                        //更新出生年月
                        bllmem.Update(oldtgMember);


                        //导入对象赋值
                        tgMemExtInfo.mem_ID = newMemID;
                        string mem_Code = dr[9].ToString();
                        tgMemExtInfo.mem_Code = mem_Code;
                        string mem_CodeAddr = dr[6].ToString();
                        tgMemExtInfo.mem_CodeAddr = mem_CodeAddr;
                        string mem_Age = dr[7].ToString();
                        tgMemExtInfo.mem_Age = mem_Age != "" ? Convert.ToInt32(mem_Age) : 0;
                        string mem_School = dr[11].ToString();
                        tgMemExtInfo.mem_School = mem_School;
                        string mem_SchLevel = dr[12].ToString();
                        tgMemExtInfo.mem_SchLevel = mem_SchLevel;
                        string mem_SchSpec = dr[13].ToString();
                        tgMemExtInfo.mem_SchSpec = mem_SchSpec;
                        string mem_SchOutDate = dr[14].ToString();
                        if (mem_SchOutDate != "")
                        {
                            tgMemExtInfo.mem_SchOutDate = Convert.ToDateTime(mem_SchOutDate);
                        }

                        string mem_School2 = dr[15].ToString();
                        tgMemExtInfo.mem_School2 = mem_School2;
                        string mem_SchLevel2 = dr[16].ToString();
                        tgMemExtInfo.mem_SchLevel2 = mem_SchLevel2;
                        string mem_SchSpec2 = dr[17].ToString();
                        tgMemExtInfo.mem_SchSpec2 = mem_SchSpec2;
                        string mem_SchOutDate2 = dr[18].ToString();
                        if (mem_SchOutDate2 != "")
                        {
                            tgMemExtInfo.mem_SchOutDate2 = Convert.ToDateTime(mem_SchOutDate2);
                        }

                        string mem_ArchLevel = dr[19].ToString();
                        //平台角色
                        var roleList = bllprofesion.GetModelList(" Name='" + mem_ArchLevel + "'");
                        if (roleList.Count > 0)
                        {
                            tgMemExtInfo.mem_ArchLevel = roleList[0].ID;
                        }

                        string mem_ArchLevelTime = dr[20].ToString();
                        if (mem_ArchLevelTime != "")
                        {
                            tgMemExtInfo.mem_ArchLevelTime = Convert.ToDateTime(mem_ArchLevelTime);
                        }
                        //注册角色
                        string mem_ArchReg = dr[21].ToString();
                        Where = string.Format(" Name='{0}'", mem_ArchReg);
                        var archLeveList = bllarchlevel.GetModelList(Where);
                        if (archLeveList.Count > 0)
                        {
                            tgMemExtInfo.mem_ArchReg = archLeveList[0].ArchLevel_ID;
                        }

                        string mem_ArchRegTime = dr[22].ToString();
                        if (mem_ArchRegTime != "")
                        {
                            tgMemExtInfo.mem_ArchRegTime = Convert.ToDateTime(mem_ArchRegTime);
                        }

                        string mem_WorkTime = dr[23].ToString();
                        if (mem_WorkTime != "")
                        {
                            tgMemExtInfo.mem_WorkTime = Convert.ToDateTime(mem_WorkTime);
                        }
                        string mem_WorkDiff = dr[24].ToString();
                        tgMemExtInfo.mem_WorkDiff = mem_WorkDiff != "" ? Convert.ToInt32(mem_WorkDiff) : 0;
                        string mem_WorkYear = dr[25].ToString();
                        tgMemExtInfo.mem_WorkYear = mem_WorkYear != "" ? Convert.ToInt32(mem_WorkYear) : 0;
                        string mem_InCompanyTime = dr[26].ToString();
                        if (mem_InCompanyTime != "")
                        {
                            tgMemExtInfo.mem_InCompanyTime = Convert.ToDateTime(mem_InCompanyTime);
                        }
                        string mem_InCompanyDiffSub = dr[27].ToString();
                        tgMemExtInfo.mem_InCompanyDiffSub = mem_InCompanyDiffSub;
                        string mem_InCompanyDiff = dr[28].ToString();
                        tgMemExtInfo.mem_InCompanyDiff = mem_InCompanyDiff != "" ? Convert.ToInt32(mem_InCompanyDiff) : 0;
                        string mem_InCompanyYear = dr[29].ToString();
                        tgMemExtInfo.mem_InCompanyYear = mem_InCompanyYear != "" ? Convert.ToInt32(mem_InCompanyYear) : 0;
                        string mem_InCompanyTemp = dr[30].ToString();
                        tgMemExtInfo.mem_InCompanyTemp = mem_InCompanyTemp != "" ? Convert.ToInt32(mem_InCompanyTemp) : 0;
                        string mem_FileAddr = dr[31].ToString();
                        tgMemExtInfo.mem_FileAddr = mem_FileAddr;
                        string mem_Holidaybase = dr[32].ToString();
                        tgMemExtInfo.mem_Holidaybase = mem_Holidaybase != "" ? Convert.ToInt32(mem_Holidaybase) : 0;
                        string mem_HolidayYear = dr[33].ToString();
                        tgMemExtInfo.mem_HolidayYear = mem_HolidayYear != "" ? Convert.ToInt32(mem_HolidayYear) : 0;
                        string mem_MagPishi = dr[34].ToString();
                        tgMemExtInfo.mem_MagPishi = mem_MagPishi;
                        string mem_ZhuanZheng = dr[35].ToString();
                        if (mem_ZhuanZheng != "")
                        {
                            tgMemExtInfo.mem_ZhuanZheng = Convert.ToDateTime(mem_ZhuanZheng);
                        }
                        string mem_GongZi = dr[36].ToString();
                        tgMemExtInfo.mem_GongZi = mem_GongZi != "" ? Convert.ToDecimal(mem_GongZi) : 0;
                        string mem_Jiaotong = dr[37].ToString();
                        tgMemExtInfo.mem_Jiaotong = mem_Jiaotong != "" ? Convert.ToInt32(mem_Jiaotong) : 0;
                        string mem_Tongxun = dr[38].ToString();
                        tgMemExtInfo.mem_Tongxun = mem_Tongxun != "" ? Convert.ToInt32(mem_Tongxun) : 0;
                        string mem_YingFa = dr[39].ToString();
                        tgMemExtInfo.mem_YingFa = mem_YingFa != "" ? Convert.ToDecimal(mem_YingFa) : 0;
                        string mem_SheBaobase = dr[40].ToString();
                        tgMemExtInfo.mem_SheBaobase = mem_SheBaobase != "" ? Convert.ToInt32(mem_SheBaobase) : 0;
                        string mem_Gongjijin = dr[41].ToString();
                        tgMemExtInfo.mem_Gongjijin = mem_Gongjijin != "" ? Convert.ToDecimal(mem_Gongjijin) : 0;
                        string mem_GongjijinSub = dr[42].ToString();
                        tgMemExtInfo.mem_GongjijinSub = mem_GongjijinSub;
                        string mem_Yufa = dr[43].ToString();
                        tgMemExtInfo.mem_Yufa = mem_Yufa != "" ? Convert.ToDecimal(mem_Yufa) : 0;


                        //不存在插入
                        if (listextinfo.Count == 0)
                        {
                            bllextinfo.Add(tgMemExtInfo);
                        }
                        else
                        {
                          //  var oldtgMemExtInfo = listextinfo[0];

                         //   int oldID = oldtgMemExtInfo.ID;
                            //赋值
                          //  oldtgMemExtInfo = tgMemExtInfo;
                          //  oldtgMemExtInfo.ID = oldID;

                            bllextinfo.Update(tgMemExtInfo);
                        }

                        //注册关联表
                        var tgMemRelation = new TG.Model.cm_MemArchLevelRelation();
                        Where = string.Format(" mem_Id={0} ", newMemID);
                        var listrelation = bllrelation.GetModelList(Where);
                        //不存在插入
                        if (listrelation.Count == 0)
                        {
                            string regArchName = dr[21].ToString();
                            Where = string.Format(" Name='{0}'", regArchName);
                            archLeveList = bllarchlevel.GetModelList(Where);

                            if (archLeveList.Count > 0)
                            {
                                //查询是否存在
                                tgMemRelation.Mem_ID = newMemID;
                                tgMemRelation.ArchLevel_ID = archLeveList[0].ArchLevel_ID;
                                //添加
                                bllrelation.Add(tgMemRelation);
                            }
                        }
                        else//更新
                        {
                            string regArchName = dr[21].ToString();
                            Where = string.Format(" Name='{0}'", regArchName);
                            archLeveList = bllarchlevel.GetModelList(Where);

                            if (archLeveList.Count > 0)
                            {
                                var oldtgMemRelation = listrelation[0];
                                int oldID = oldtgMemRelation.ID;
                                //查询是否存在
                                tgMemRelation.Mem_ID = newMemID;
                                tgMemRelation.ArchLevel_ID = archLeveList[0].ArchLevel_ID;
                                tgMemRelation.ID = oldID;
                                //添加
                                bllrelation.Update(tgMemRelation);
                            }
                        }

                        //用户入职离职信息
                        var tgMemExt = new TG.Model.tg_memberExt();
                        Where = string.Format(" mem_ID={0} ", newMemID);
                        var listext = bllext.GetModelList(Where);
                        //不存在插入
                        if (listext.Count == 0)
                        {
                            //查询是否存在
                            tgMemExt.mem_ID = newMemID;
                            tgMemExt.mem_Name = oldtgMember.mem_Name;
                            mem_InCompanyTime = dr[26].ToString();
                            if (mem_InCompanyTime != "")
                            {
                                tgMemExt.mem_InTime = Convert.ToDateTime(mem_InCompanyTime);
                            }
                            //添加
                            bllext.Add(tgMemExt);
                        }
                        else
                        {
                            var oldtgMemExt = listext[0];
                            int oldID = oldtgMemExt.ID;
                            //查询是否存在
                            tgMemExt.mem_ID = newMemID;
                            tgMemExt.mem_Name = oldtgMember.mem_Name;
                            mem_InCompanyTime = dr[26].ToString();
                            if (mem_InCompanyTime != "")
                            {
                                tgMemExt.mem_InTime = Convert.ToDateTime(mem_InCompanyTime);
                            }
                            tgMemExt.mem_OutTime = oldtgMemExt.mem_OutTime;
                            tgMemExt.ID = oldID;
                            //添加
                            bllext.Update(tgMemExt);
                        }
                    }
                    else
                    {
                        //不存在，不做处理
                    }
                }
            }
        }


        #region 读取Excel的内容到DataSet--第一行为标题行
        /// <summary>
        /// 读取Excel的内容到DataSet
        /// </summary>
        public static DataSet ReaderExcelToDataSet(string fileName, bool isDeleteFile)
        {
            //声明保存excel数据的DataSet
            DataSet ds = new DataSet();

            //读取方式
            string xlsDriver = "";
            string excelVersion = "";
            if (fileName.ToLower().EndsWith("xls"))
            {
                xlsDriver = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel {1};HDR=YES; IMEX=1;\"";
                excelVersion = "8.0";
            }
            else if (fileName.ToLower().EndsWith("xlsx"))
            {
                xlsDriver = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel {1};HDR=YES; IMEX=1;\"";
                excelVersion = "12.0";
            }
            else
            {
                throw new Exception("文件不是Excel格式");
            }


            OleDbConnection conn = new OleDbConnection(string.Format(xlsDriver, new string[] { fileName, excelVersion }));
            conn.Open();

            try
            {
                //excel中的所有sheet和视图
                DataTable schema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                OleDbDataAdapter da = new OleDbDataAdapter();
                OleDbCommand dc = new OleDbCommand();
                dc.Connection = conn;

                //读取第一个Sheet
                List<string> tableNames = new List<string>();
                foreach (DataRow row in schema.Rows)
                {
                    string tablename = row["TABLE_NAME"].ToString();
                    tablename.Replace("'", string.Empty).Replace("\"", string.Empty).EndsWith("$");
                    tableNames.Add(tablename);
                }

                tableNames.ForEach(t =>
                {
                    if (!t.Equals(string.Empty))
                    {
                        dc.CommandText = "select * from [" + t + "] ";
                        da.SelectCommand = dc;
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        ds.Tables.Add(dt);
                    }
                    else
                    {
                        ds.Tables.Add(new DataTable());
                    }
                });

                da.Dispose();
                conn.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                }
            }

            //删除上传文件
            if (isDeleteFile && File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            return ds;
        }
        #endregion
    }
}