﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="SetCompanyConfigBymaster.aspx.cs" Inherits="TG.Web.SystemSet.SetCompanyConfigBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            
            $("#AddLink").click(function () {
                $('#div_add').css('display', 'block');


            });
            $("#btn_Cancl").click(function () {
                $('#div_add').css('display', 'none');


            });

            $("#ctl00_ContentPlaceHolder1_btn_linkadd").click(function () {
                if ($("#ctl00_ContentPlaceHolder1_txt_linkname").val() == "") {
                    alert("请输入链接名称！");
                    return false;
                }
                if ($("#ctl00_ContentPlaceHolder1_txt_link").val() == "") {
                    alert("请输入链接地址！");
                    return false;
                }
                $('#div_add').css('display', 'none');
                return true;
            });
            //删除
            $(".cls_del").click(function () {
                if (confirm("确定要删除本条快捷方式吗？")) {
                    var id = $(this).next(":hidden").val();
                    var link = $(this);
                    $.post("../HttpHandler/SetCompanyInfo.ashx", { action: "del", id: "" + id + "" }, function (data) {
                        if (data == "ok") {
                            alert("删除成功！");
                            link.parents("tr:first").remove();
                        }
                        else {
                            alert("删除失败！");
                        }
                    });
                }
            });
            //实现表格样式

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>首页快捷入口设置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用设置</a><i class="fa fa-angle-right"> </i><a>首页快捷入口设置</a>
    </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>快捷链接列表</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td>
                                <button class="btn blue" type="button" id="AddLink">
                                    添加快捷链接</button>
                            </td>
                        </tr>
                    </table>
                    <asp:GridView ID="GridView1" runat="server" ShowHeader="true" CssClass="table table-striped table-bordered table-hover dataTable"
                        Width="100%" AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField HeaderText="序号">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex+1 %>
                                </ItemTemplate>
                                <ItemStyle Width="8%" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="LinkName" HeaderText="链接名称">
                                <ItemStyle Width="32%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LinkAddr" HeaderText="地址">
                                <ItemStyle Width="50%" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="操作">
                                <ItemTemplate>
                                    <a href="###" class="cls_del">删除</a>
                                    <input type="hidden" value='<%# Eval("ID") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="10%" HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="cls_data" id="div_add" style="display: none;">
        <div class="portlet box blue ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>添加链接</div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"></a>
                </div>
            </div>
            <div class="portlet-body">
                <table style="width: 40%" align="center">
                    <tr>
                        <td>
                            快捷名称:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_linkname" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            链接地址:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_link" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btn_linkadd" runat="server" Text="保存" CssClass="btn blue" OnClick="btn_linkadd_Click" />
                            <input type="button" class="btn btn-default" value="取消" id="btn_Cancl" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
