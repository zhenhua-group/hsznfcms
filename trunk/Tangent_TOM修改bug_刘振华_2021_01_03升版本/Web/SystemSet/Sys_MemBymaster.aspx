﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="Sys_MemBymaster.aspx.cs" Inherits="TG.Web.SystemSet.Sys_MemBymaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_sign.js"></script>
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/SystemSet/Sys_Mem.js" type="text/javascript"></script>

    <script type="text/javascript">
        var swfu;

        var userid = '<%=GetCurMemID() %>';
        function ActionPic(memid, param) {

            var divid = "divFileProgressContainer" + param;
            var spanid = "spanButtonPlaceholder" + param;


            //附件高清图
            swfu = new SWFUpload({

                upload_url: "../ProcessUpload/upload_sign.aspx?type=sign&id=" + memid + "&userid=" + userid,
                flash_url: "../js/swfupload/swfupload.swf",
                post_params: {
                    "ASPSESSID": "<%=Session.SessionID %>"
                },
                file_size_limit: "10 MB",
                file_types: "*.jpg;*.gif;*.bmp;.png",
                file_types_description: "文件资料上传",
                file_upload_limit: "0",
                file_queue_limit: "1",

                //Events
                file_queued_handler: fileQueued,
                file_queue_error_handler: fileQueueError,
                file_dialog_complete_handler: fileDialogComplete,
                upload_progress_handler: uploadProgress,
                upload_error_handler: uploadError,
                upload_success_handler: uploadSuccess,
                upload_complete_handler: uploadComplete,

                // Button
                button_placeholder_id: spanid,
                //                button_image_url: "../Images/swfupload/cc.png",
                button_style: '{background-color:#d8d8d8}',
                button_width: 61,
                button_height: 22,
                button_text: '<span class="fileupload-new">选择文件</span>',
                button_text_style: '.fileupload-new {background-color:#d8d8d8; } ',
                button_text_top_padding: 1,
                button_text_left_padding: 5,

                custom_settings: {
                    upload_target: divid
                },
                debug: false
            });

        }


    </script>
    <style type="text/css">
        .mycenter
        {
            text-algin: center;
        }

        #ctl00$ContentPlaceHolder1$AspNetPager1_input
        {
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>人员管理</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>机构管理</a><i class="fa fa-angle-right"> </i><a>人员管理</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>查询人员信息
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:
                            </td>
                            <td>
                                <asp:DropDownList CssClass="form-control" ID="drp_unit3" runat="server"
                                    AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="drp_unit3_SelectedIndexChanged">
                                    <asp:ListItem Value="-1">全部部门</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>用户:
                            </td>
                            <td>
                                <input class="form-control input-sm" type="text" id="txt_keyname"
                                    runat="Server" />
                            </td>
                            <td>状态：</td>
                            <td>
                                <select id="drpStatus" runat="server">
                                    <option value="-1">全部</option>
                                    <option value="0">在职</option>
                                    <option value="1">离职</option>
                                </select>
                            </td>
                            <td>&nbsp;
                                    <asp:Button CssClass="btn blue" ID="btn_Search" runat="server" Text="查询" OnClick="btn_Search_Click" />
                            </td>
                            <td>
                                <button class="btn blue" type="button" id="btn_showadd">添加</button>
                                <%--<a class="btn blue" data-toggle="modal" href="#full2">添加</a>--%>
                            </td>
                            <td>
                                <asp:Button CssClass="btn blue" ID="btn_DelCst" runat="server" Text="删除" OnClick="btn_DelCst_Click" />


                            </td>
                        </tr>
                    </table>

                </div>
            </div>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i>人员信息列表
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView ID="grid_mem" runat="server" AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover dataTable" Width="100%"
                                OnRowDataBound="grid_mem_RowDataBound" HeaderStyle-HorizontalAlign="Center" EnableModelValidation="True">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_id" runat="server" CssClass="cls_chk" />
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="mem_ID" HeaderText="编号">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="60px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="mem_Login" HeaderText="登录名">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="mem_Name" HeaderText="姓名">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="mem_Birthday" DataFormatString="{0:d}" HeaderText="出生日期">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="mem_Sex" HeaderText="性别">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="30px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="mem_Unit_ID" HeaderText="部门名称">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="mem_Speciality_ID" HeaderText="专业">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="60px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="mem_Principalship_ID" HeaderText="角色">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="mem_Telephone" HeaderText="联系电话">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="mem_Mobile" HeaderText="手机">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="80px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="职位">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hid_archlevel" runat="server" />
                                            <asp:Label ID="lbl_archlevel" runat="server" Text="<%# Bind('mem_ID') %>"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="130px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="头像">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hidden_memid" runat="server" Value="<%# Bind('mem_ID') %>" />
                                        </ItemTemplate>
                                        <ItemStyle Width="60px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="管理职位">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                            <input type="hidden" name="name" value="<%# Eval("RoleIdMag") %>" />
                                            <asp:Label ID="lblMagName" runat="server" Text="<%# Bind('MagName') %>"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="85px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="技术职位">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                            <input type="hidden" name="name" value="<%# Eval("RoleIdTec") %>" />
                                            <asp:Label ID="lblTecName" runat="server" Text="<%# Bind('TecName') %>"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="mem_isFired" HeaderText="状态">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="50px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="mem_InTime" HeaderText="入职时间" DataFormatString="{0:yyyy-MM-dd}">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="mem_OutTime2" HeaderText="离职时间" DataFormatString="{0:yyyy-MM-dd}">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="80px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="unitName2" HeaderText="任职部门">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemStyle Width="80px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="序号">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" Width="35px" />
                                        <ItemTemplate>
                                            <input type="text" name="name" value="<%# Eval("mem_Order") %>" style="width: 35px;" uid="<%# Eval("Mem_ID") %>" class="order" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="操作">
                                        <HeaderStyle HorizontalAlign="Center" CssClass="mycenter" />
                                        <ItemTemplate>
                                            <input type="hidden" name="name" value="<%# Eval("mem_Mailbox") %>" />


                                            <a class="cls_select" data-toggle="modal" href="#full">编辑</a>
                                            <input type="hidden" name="name" value="<%# Eval("mem_Order") %>" />
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                </Columns>

                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:GridView>

                            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                                CustomInfoSectionWidth="32%" CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                                CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                                PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                                SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px; height:23px;"
                                PageSize="30" SubmitButtonClass="btn green">
                            </webdiyer:AspNetPager>
                            <asp:HiddenField ID="hid_where" runat="server" Value="" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
            <!--添加-->
            <div class="cls_data" id="div_add" style="display: none;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">新增用户</h4>
                    </div>
                    <div class="modal-body">

                        <table border="0" cellspacing="0" cellpadding="0" width="95%" class="  table-bordered"
                            align="center">
                            <tr>
                                <td width="15%" align="center">姓名:
                                </td>
                                <td width="35%">
                                    <asp:TextBox ID="txt_memName" runat="server" CssClass="form-control input-sm" Width="200px"></asp:TextBox>
                                </td>
                                <td width="15%" align="center">登录名:
                                </td>
                                <td width="35%">
                                    <asp:TextBox ID="txt_memLogin" runat="server" CssClass="form-control input-sm" Width="200px"></asp:TextBox>
                                    <input id="hid_exsit" type="hidden" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">密码:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_pwd" TextMode="Password" runat="server" CssClass="form-control
    input-sm"
                                        Width="200px"></asp:TextBox>
                                </td>
                                <td align="center">出生日期:
                                </td>
                                <td>
                                    <input type="text" runat="server" style="width: 200px; height: 30px; border: 1px solid #e5e5e5"
                                        class="Wdate " onclick="WdatePicker({
    readOnly: true
})"
                                        id="txt_Birth" readonly="" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">性别:
                                </td>
                                <td>
                                    <asp:DropDownList ID="drp_sex" runat="server" CssClass="form-control input-sm" Width="60px">
                                        <asp:ListItem>男</asp:ListItem>
                                        <asp:ListItem>女</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="center">专业:
                                </td>
                                <td>
                                    <asp:DropDownList ID="drp_spec" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm" Width="200px">
                                        <asp:ListItem Value="-1">--选择专业--</asp:ListItem>
                                    </asp:DropDownList>
                                    <input id="Hidden1" type="hidden" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">部门名称:
                                </td>
                                <td>
                                    <asp:DropDownList ID="drp_unit" runat="server" AppendDataBoundItems="True" CssClass="form-control
    input-sm"
                                        Width="200px">
                                        <asp:ListItem Value="-1">--选择部门--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="center">角色:
                                </td>
                                <td>
                                    <asp:DropDownList ID="drp_roles" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm" Width="200px">
                                        <asp:ListItem Value="-1">--选择角色--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">邮箱:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_mail" runat="server" CssClass="form-control input-sm" Width="200px"></asp:TextBox>
                                </td>
                                <td align="center">手机:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_mobile" runat="server" CssClass="form-control input-sm" Width="200px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">联系电话:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_phone" runat="server" CssClass="form-control
    input-sm"
                                        Width="200px"></asp:TextBox>
                                </td>
                                <td align="center">设计职称:
                                </td>
                                <td>
                                    <asp:DropDownList ID="drp_level" runat="server" AppendDataBoundItems="True" CssClass="form-control
    input-sm"
                                        Width="200px">
                                        <asp:ListItem Value="0">--选择职称--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">管理职位:</td>
                                <td>
                                    <asp:DropDownList ID="drpRoleMag" runat="server" AppendDataBoundItems="True" CssClass="form-control
    input-sm"
                                        Width="200px">
                                        <asp:ListItem Value="0">--选择职位--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="center">技术职位:</td>
                                <td>
                                    <asp:DropDownList ID="drpRoleTec" runat="server" AppendDataBoundItems="True" CssClass="form-control
    input-sm"
                                        Width="200px">
                                        <asp:ListItem Value="0">--选择职位--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">职位隶属部门<span style="color: red;">(如不存在跨部门任职请勿设置！)</span>
                                </td>
                                <td colspan="3">
                                    <asp:CheckBoxList ID="chkList" runat="server" RepeatDirection="Horizontal" RepeatColumns="8"></asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">在职状态:</td>
                                <td>
                                    <asp:DropDownList ID="drpIsFired" runat="server" CssClass="form-control input-sm" Width="60px">
                                        <asp:ListItem Value="0">在职</asp:ListItem>
                                        <asp:ListItem Value="1">离职</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="center">入职时间：</td>
                                <td>
                                    <input type="text" runat="server" style="width: 200px; height: 30px; border: 1px solid #e5e5e5"
                                        class="Wdate " onclick="WdatePicker({ readOnly: true })" id="txtInTime" readonly="" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">上传头像:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_pic" runat="server" CssClass="cls_input_text" Width="300px"> </asp:TextBox>
                                    <span class="btn default btn-sm"><span id="spanButtonPlaceholder"></span></span><span style="color: red;">(注:请先填写登录名!图像比例1:1)</span><br />
                                    <div id="divFileProgressContainer">
                                    </div>
                                    <asp:HiddenField ID="txt_pic_s" runat="server"></asp:HiddenField>
                                </td>
                                <td align="center">离职时间:</td>
                                <td>
                                    <input type="text" runat="server" style="width: 200px; height: 30px; border: 1px solid #e5e5e5"
                                        class="Wdate " onclick="WdatePicker({ readOnly: true })" id="txtOutTime" readonly="" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">显示排序:</td>
                                <td>
                                    <input type="text" runat="server" style="width: 40px" value="0" id="txtOrder" />
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <asp:Button ID="btn_save" runat="server" Text="保存" CssClass="btn green" OnClick="btn_save_Click" />
                                    <input type="button" class="btn default" value="取消" id="btn_Cancl" />
                                </td>
                            </tr>
                        </table>

                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
                <!-- /.modal-content -->
                <%-- </div>
                </div>--%>
            </div>

            <!--编辑--->
            <div class="cls_data" id="div_edit" style="display: none;">
                <%-- <div class="modal fade" id="full" tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog modal-full">--%>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">编辑用户</h4>
                    </div>
                    <div class="modal-body">


                        <table border="0" cellspacing="0" cellpadding="0" width="95%" class=" table-bordered" align="center">
                            <tr>
                                <td>姓名:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_memName0" placeholder="姓名" runat="server" CssClass="form-control form-control-width" Width="200px"></asp:TextBox>
                                </td>
                                <td>登录名:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_memLogin0" placeholder="登录名" runat="server" CssClass="form-control
    input-sm"
                                        Width="200px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>密码:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_pwd0" TextMode="Password" Text="" runat="server" CssClass="form-control form-control-width" Width="200px"></asp:TextBox>
                                </td>
                                <td>出生日期:
                                </td>
                                <td>
                                    <input type="text" runat="server" style="width: 200px; height: 30px; border: 1px solid #e5e5e5"
                                        class="Wdate" onclick="WdatePicker({ readOnly: true })" id="txt_Birth0" readonly="" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>性别:
                                </td>
                                <td>
                                    <asp:DropDownList ID="drp_sex0" runat="server" CssClass="form-control input-sm" Width="60px">
                                        <asp:ListItem>男</asp:ListItem>
                                        <asp:ListItem>女</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>专业:
                                </td>
                                <td>
                                    <asp:DropDownList ID="drp_spec0" runat="server" AppendDataBoundItems="True" CssClass="form-control 
    input-sm"
                                        Width="200px">
                                        <asp:ListItem Value="-1">--选择专业--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>单位:
                                </td>
                                <td>
                                    <asp:DropDownList ID="drp_unit0" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm" Width="200px">
                                        <asp:ListItem Value="-1">--选择单位--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>角色:
                                </td>
                                <td>
                                    <asp:DropDownList ID="drp_roles0" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm" Width="200px">
                                        <asp:ListItem Value="-1">--选择角色--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>邮箱:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_mail0" placeholder="邮箱" runat="server" CssClass="form-control form-control-width" Width="200px"></asp:TextBox>
                                </td>
                                <td>手机:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_mobile0" placeholder="手机" runat="server" CssClass="form-control form-control-width" Width="200px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>联系电话:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_phone0" placeholder="联系电话" runat="server" CssClass="form-control form-control-width" Width="200px"></asp:TextBox>
                                </td>
                                <td>设计职称:
                                </td>
                                <td>
                                    <asp:DropDownList ID="drp_level0" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm" Width="200px">
                                        <asp:ListItem Value="0">--选择职称--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>管理职位:</td>
                                <td>
                                    <asp:DropDownList ID="drpRoleMag0" runat="server" AppendDataBoundItems="True" CssClass="form-control
    input-sm"
                                        Width="200px">
                                        <asp:ListItem Value="0">--选择职位--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>技术职位:</td>
                                <td>
                                    <asp:DropDownList ID="drpRoleTec0" runat="server" AppendDataBoundItems="True" CssClass="form-control
    input-sm"
                                        Width="200px">
                                        <asp:ListItem Value="0">--选择职位--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">职位隶属部门<br />
                                    <span style="color: red;">(如不存在跨部门任职请勿设置！)</span>
                                </td>
                                <td colspan="3">
                                    <asp:CheckBoxList ID="chkList0" runat="server" RepeatDirection="Horizontal" RepeatColumns="8"></asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>在职状态:</td>
                                <td>
                                    <asp:DropDownList ID="drpIsFired0" runat="server" CssClass="form-control input-sm" Width="60px">
                                        <asp:ListItem Value="0">在职</asp:ListItem>
                                        <asp:ListItem Value="1">离职</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>入职时间：</td>
                                <td>
                                    <input type="text" runat="server" style="width: 200px; height: 30px; border: 1px solid #e5e5e5"
                                        class="Wdate " onclick="WdatePicker({ readOnly: true })" id="txtInTime0" readonly="" />
                                </td>
                            </tr>
                            <tr>
                                <td>上传头像:
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_pic0" runat="server" CssClass="cls_input_text" Width="300px"> </asp:TextBox>
                                    <span class="btn default btn-sm"><span id="spanButtonPlaceholder0"></span></span><span style="color: red">(注:图像比例1:1)</span><br />
                                    <div id="divFileProgressContainer0">
                                    </div>
                                    <asp:HiddenField ID="txt_pic0_s" runat="server"></asp:HiddenField>
                                </td>
                                <td align="center">离职时间:</td>
                                <td>
                                    <input type="text" runat="server" style="width: 200px; height: 30px; border: 1px solid #e5e5e5"
                                        class="Wdate " onclick="WdatePicker({ readOnly: true })" id="txtOutTime0" readonly="" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">显示排序:</td>
                                <td>
                                    <input type="text" runat="server" style="width: 40px" value="0" id="txtOrder2" />
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">

                                    <input type="button" class="btn green" id="btnSaveEdit" value="保存" />
                                    <input type="button" class="btn btn-default" value="取消" id="btn_Cancle" />
                                    <asp:HiddenField ID="hid_memid" runat="server" />
                                    <asp:HiddenField ID="hid_memlogin" runat="server" />
                                    <asp:Button ID="btn_edit" runat="server" Text="" Height="0px" Width="0px" OnClick="btn_edit_Click" />
                                </td>
                            </tr>
                        </table>



                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
                <%--</div>
                </div>--%>
            </div>

            <input type="hidden" name="" value="0" id="hidUserSysNo" />
            <input type="hidden" name="name" value="0" id="hidTempOrder" />
        </div>
    </div>
</asp:Content>
