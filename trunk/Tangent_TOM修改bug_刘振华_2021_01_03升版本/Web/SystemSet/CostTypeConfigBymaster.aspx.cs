﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class CostTypeConfigBymaster : System.Web.UI.Page
    {
        TG.BLL.cm_CostType bll = new TG.BLL.cm_CostType();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
                bindData();
            }
        }

        /// <summary>
        /// 绑定
        /// </summary>
        private void bindData()
        {
            StringBuilder sb = new StringBuilder("");
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.AppendFormat(" and costName Like '%{0}%' ", keyname);
            }
            //单位
            if (this.drp_unit3.SelectedIndex > 0)
            {
                string type = "";
                if (this.drp_unit3.SelectedValue == "0")
                {
                    type = "0";
                }
                else if (this.drp_unit3.SelectedItem.Value=="1")
                {
                    type = "0,1";
                }
                else if (this.drp_unit3.SelectedItem.Value == "2")
                {
                    type = "0,2";
                }
                else if (this.drp_unit3.SelectedItem.Value == "3")
                {
                    type = "0,3";
                }
                else
                {
                    type = "0,4";
                }
                sb.AppendFormat("  and  costGroup in ({0})", type);
            }
            //操作类型
            if (this.drp_operation.SelectedIndex > 0)
            {
                sb.AppendFormat("  and  isInput={0}", this.drp_operation.SelectedValue);
            }

            //
            this.AspNetPager1.RecordCount = int.Parse(bll.GetListPageProcCount(sb.ToString()).ToString());
            //绑定
            this.grid_cost.DataSource = bll.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            this.grid_cost.DataBind();
        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            //this.drp_unit.DataSource = bll_unit.GetList("");
            //this.drp_unit.DataTextField = "unit_Name";
            //this.drp_unit.DataValueField = "unit_ID";
            //this.drp_unit.DataBind();

            //this.drp_unit1.DataSource = bll_unit.GetList("");
            //this.drp_unit1.DataTextField = "unit_Name";
            //this.drp_unit1.DataValueField = "unit_ID";
            //this.drp_unit1.DataBind();

            //this.drp_unit3.DataSource = bll_unit.GetList("");
            //this.drp_unit3.DataTextField = "unit_Name";
            //this.drp_unit3.DataValueField = "unit_ID";
            //this.drp_unit3.DataBind();
        }

        //添加人员
        protected void btn_save_Click(object sender, EventArgs e)
        {
            TG.Model.cm_CostType model = new TG.Model.cm_CostType();
            try
            {

                string type = "";
                model.costName = txt_name.Text.Trim();
                //操作类型
                if (this.drp_operationType.SelectedIndex > 0)
                {
                    model.isInput = int.Parse(this.drp_operationType.SelectedValue);
                }
                if (this.drp_unit.SelectedIndex > 0)
                {

                    if (this.drp_unit.SelectedValue == "0")
                    {
                        type = "0";
                    }
                    else if (this.drp_unit.SelectedItem.Text.Contains("勘察"))
                    {
                        type = "1";
                    }
                    else if (this.drp_unit.SelectedItem.Text.Contains("图文"))
                    {
                        type = "2";
                    }
                    else if (this.drp_unit.SelectedItem.Text.Contains("承包"))
                    {
                        type = "3";
                    }
                    else
                    {
                        type = "4";
                    }
                    model.costGroup = int.Parse(type);
                }

                bll.Add(model);
                //弹出提示
                TG.Common.MessageBox.ShowAndRedirect(this, "成本类型添加成功！", "CostTypeConfigBymaster.aspx");
            }
            catch
            {
                TG.Common.MessageBox.Show(this, "成本类型添加失败！");
            }

        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            this.AspNetPager1.CurrentPageIndex = 1;
            bindData();
        }
        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            bindData();
        }

        //编辑部门信息
        protected void btn_edit_Click(object sender, EventArgs e)
        {
            if (this.hid_id.Value.Trim() != "")
            {
                TG.Model.cm_CostType model = new TG.Model.cm_CostType();
                try
                {
                    model.costName = txt_name1.Text.Trim();
                    //操作类型
                   
                    model.ID = int.Parse(hid_id.Value);
                    bll.Update(model);
                    //弹出提示
                    TG.Common.MessageBox.ShowAndRedirect(this, "成本类型修改成功！", "CostTypeConfigBymaster.aspx");
                }
                catch
                {
                    TG.Common.MessageBox.Show(this, "成本类型修改失败！");
                }
            }
        }

    }
}