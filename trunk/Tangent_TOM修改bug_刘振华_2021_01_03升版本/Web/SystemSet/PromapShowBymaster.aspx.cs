﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class PromapShowBymaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindCoperation();
                BindCity();
                BindDangqian();
            }
        }

        //绑定已添加的页面
        private void BindCoperation()
        {
            TG.BLL.TblAreaAndAspx bll = new BLL.TblAreaAndAspx();
            StringBuilder strWhere = new StringBuilder();

            if (this.txt_keyname.Value != "")
            {
                strWhere.Append(" AND areaName LIKE '%" + this.txt_keyname.Value + "%'");
            }
            //分页
            this.gv_Coperation.DataSource = bll.GetList(strWhere.ToString());
            this.gv_Coperation.DataBind();
        }
        TG.BLL.TblAreaBll bll = new BLL.TblAreaBll();
        /// <summary>
        /// 绑定下拉框
        /// </summary>
        protected void BindCity()
        {

            this.drp_typelist.DataSource = bll.GetListName("0");
            this.drp_typelist.DataBind();

        }
        /// <summary>
        /// 绑定当前的显示地图是什么
        /// </summary>
        public void BindDangqian()
        {
            this.lab.Text = bll.GetShow();
        }
        /// <summary>
        /// 增加配置
        /// </summary>
        TG.BLL.TblAreaAndAspx tblbll = new BLL.TblAreaAndAspx();
        //增加
        protected void Addmap()
        {
            string city = this.txt_cityName.Text;
            string linkaspx = this.txt_toaspx.Text;

            int areaid = bll.GetId(city);
            TG.Model.TblAreaAndAspx model = new Model.TblAreaAndAspx();
            model.AreaId = areaid;
            model.areaName = city;
            model.Toaspx = linkaspx;
            tblbll.Add(model);
            TG.Common.MessageBox.ShowAndRedirect(this, "添加成功", "PromapShowBymaster.aspx");
        }

        protected void gv_Coperation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //
        }

        protected void btn_save_Click(object sender,  EventArgs e)
        {
            Addmap();
        }
        //修改
        protected void btn_edit_Click(object sender,  EventArgs e)
        {
            //
            Alertmap();
        }
        //删除
        protected void btn_DelCst_Click(object sender,  EventArgs e)
        {
            for (int i = 0; i < this.gv_Coperation.Rows.Count; i++)
            {
                CheckBox ck = this.gv_Coperation.Rows[i].Cells[0].FindControl("chk_id") as CheckBox;
                string str_dicId = this.gv_Coperation.Rows[i].Cells[1].Text;
                if (ck.Checked)
                {
                    tblbll.Delete(int.Parse(str_dicId));
                }
            }
            TG.Common.MessageBox.ShowAndRedirect(this, "删除成功", "PromapShowBymaster.aspx");
        }
        //查询
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindCoperation();
        }
        protected void Alertmap()
        {
            TG.Model.TblAreaAndAspx model = new Model.TblAreaAndAspx();
            model.AreaId = Convert.ToInt32(this.hid_dicid.Value);
            model.areaName = this.txt_dicName0.Text;
            model.Toaspx = this.txt_dicType0.Text;
            TG.BLL.TblAreaAndAspx tblbll = new BLL.TblAreaAndAspx();
            tblbll.Update(model);
            TG.Common.MessageBox.ShowAndRedirect(this, "修改成功", "PromapShowBymaster.aspx");
        }

    }
}