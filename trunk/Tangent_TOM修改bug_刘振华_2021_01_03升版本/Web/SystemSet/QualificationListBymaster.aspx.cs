﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace TG.Web.SystemSet
{
    public partial class QualificationListBymaster : System.Web.UI.Page
    {
        TG.BLL.cm_Qualification bll = new BLL.cm_Qualification();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Qua();
            }
        }
        //绑定数据列表
        protected void Bind_Qua()
        {
            StringBuilder builder1 = new StringBuilder();

            if (this.txt_keyname.Value != "")
            {
                builder1.Append("And qua_Name LIKE '%" + this.txt_keyname.Value.Trim() + "%'");
            }
            if (this.txt_jb.SelectedValue != "")
            {
                builder1.Append(" And qua_jb='" + this.txt_jb.SelectedValue.Trim() + "'");
            }
            if (this.txt_year.Value != "")
            {
                builder1.Append(" And DATEDIFF(day,qua_yeartime,'" + Convert.ToDateTime(this.txt_year.Value).ToString("yyyy-MM-dd") + "')=0 ");
            }
            builder1.Append(" order by qua_yeartime desc,qua_id desc");
            this.gv_Coperation.DataSource = bll.GetQuaSearchList(builder1.ToString()); ;
            this.gv_Coperation.DataBind();
        }
        //删除
        protected void btn_DelCst_Click(object sender, EventArgs e)
        {
            string strall = GetSelIDlist();
            if (bll.DelectCoperation(strall))
            {
                Bind_Qua();
            }
        }
        //查找
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            Bind_Qua();
        }
        /// <summary>
        /// 获得选中的字段
        /// </summary>
        /// <returns></returns>
        private string GetSelIDlist()
        {
            string text1 = "";
            for (int num1 = 0; num1 < this.gv_Coperation.Rows.Count; num1++)
            {
                CheckBox box1 = (CheckBox)this.gv_Coperation.Rows[num1].Cells[0].FindControl("chk_cpr");
                if (box1.Checked && (this.gv_Coperation.DataKeys[num1].Value != null))
                {
                    text1 = text1 + this.gv_Coperation.DataKeys[num1].Value.ToString() + ",";
                }
            }
            if (text1.IndexOf(",") > -1)
            {
                text1 = text1.Substring(0, text1.LastIndexOf(","));
            }
            return text1;
        }
        protected void gv_Coperation_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
    }
}