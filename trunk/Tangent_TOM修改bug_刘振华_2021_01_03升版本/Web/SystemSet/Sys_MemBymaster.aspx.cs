﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;

namespace TG.Web.SystemSet
{
    public partial class Sys_MemBymaster : PageBase
    {

        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //人员信息
                BindMem();
                //部门信息
                BindUnit();
                //角色信息
                BindRoles();
                //专业信息
                BindSpec();
                //绑定职称
                BindArchLevel();
                //绑定职位
                BindRoleMag();

            }
            //绑定ID属性
            BindChkListAttr();
        }

        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }

        //绑定角色信息
        protected void BindMem()
        {
            StringBuilder sb = new StringBuilder("");
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.AppendFormat(" (A.mem_Name Like '%{0}%' or mem_Login Like '%{0}%') ", keyname);
            }
            //单位
            if (this.drp_unit3.SelectedIndex > 0)
            {
                if (!string.IsNullOrEmpty(sb.ToString()))
                {
                    sb.AppendFormat(" and ");
                }
                sb.AppendFormat("  A.mem_Unit_ID={0}", this.drp_unit3.SelectedValue);
            }
            //状态
            if (this.drpStatus.SelectedIndex > 0)
            {
                if (!string.IsNullOrEmpty(sb.ToString()))
                {
                    sb.Append(" AND ");
                }
                sb.AppendFormat(" A.mem_isFired={0}",this.drpStatus.Value);
            }
            this.hid_where.Value = sb.ToString();
            //角色数量
            this.AspNetPager1.RecordCount = int.Parse(bll_mem.GetRecordCount(sb.ToString()).ToString());
            //绑定
            this.grid_mem.DataSource = bll_mem.GetListByPage(sb.ToString()," mem_Order ", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize);
            this.grid_mem.DataBind();
        }
        //绑定部门
        protected void BindUnit()
        {
            var bll_unit = new TG.BLL.tg_unit();
            var bll_unitExt = new TG.BLL.tg_unitExt();

            var unitList = bll_unit.GetModelList(" unit_ParentID<>0 ");
            var unitExtList = bll_unitExt.GetModelList("");

            //查询
            var query = from c in unitList
                        join ext in unitExtList on c.unit_ID equals ext.unit_ID
                        where c.unit_ParentID != 0
                        orderby ext.unit_Order ascending
                        select c;
            //绑定
            var list=query.ToList();

            list.ForEach(c=>{
                
                this.drp_unit.Items.Add(new ListItem(c.unit_Name,c.unit_ID.ToString()));
                this.drp_unit0.Items.Add(new ListItem(c.unit_Name,c.unit_ID.ToString()));
                this.drp_unit3.Items.Add(new ListItem(c.unit_Name,c.unit_ID.ToString()));
            });
        }

        

        protected void BindChkListAttr()
        {
            this.chkList.Items.Clear();
            this.chkList0.Items.Clear();


            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            DataTable dt = bll_unit.GetList(" unit_ParentID<>0 ").Tables[0];

            foreach (DataRow dr in dt.Rows)
            {
                ListItem item = new ListItem(dr["unit_Name"].ToString().Trim(), dr["unit_ID"].ToString());
                item.Attributes.Add("alt", item.Value);

                this.chkList.Items.Add(item);
                this.chkList0.Items.Add(item);
            }

        }
        //绑定角色
        protected void BindRoles()
        {
            TG.BLL.tg_principalship bll_pri = new TG.BLL.tg_principalship();
            this.drp_roles.DataSource = bll_pri.GetList("");
            this.drp_roles.DataTextField = "pri_Name";
            this.drp_roles.DataValueField = "pri_ID";
            this.drp_roles.DataBind();

            this.drp_roles0.DataSource = bll_pri.GetList("");
            this.drp_roles0.DataTextField = "pri_Name";
            this.drp_roles0.DataValueField = "pri_ID";
            this.drp_roles0.DataBind();
        }
        //绑定专业
        protected void BindSpec()
        {
            TG.BLL.tg_speciality bll_spec = new TG.BLL.tg_speciality();
            this.drp_spec.DataSource = bll_spec.GetList("");
            this.drp_spec.DataTextField = "spe_Name";
            this.drp_spec.DataValueField = "spe_ID";
            this.drp_spec.DataBind();

            this.drp_spec0.DataSource = bll_spec.GetList("");
            this.drp_spec0.DataTextField = "spe_Name";
            this.drp_spec0.DataValueField = "spe_ID";
            this.drp_spec0.DataBind();
        }
        //绑定职称
        protected void BindArchLevel()
        {
            TG.BLL.cm_MemArchLevel bll_level = new TG.BLL.cm_MemArchLevel();
            this.drp_level.DataSource = bll_level.GetList("");
            this.drp_level.DataTextField = "Name";
            this.drp_level.DataValueField = "ArchLevel_ID";
            this.drp_level.DataBind();

            this.drp_level0.DataSource = bll_level.GetList("");
            this.drp_level0.DataTextField = "Name";
            this.drp_level0.DataValueField = "ArchLevel_ID";
            this.drp_level0.DataBind();
        }
        //删除角色
        protected void btn_DelCst_Click(object sender, EventArgs e)
        {
            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
            TG.BLL.tg_memberExt bll_ext = new BLL.tg_memberExt();
            TG.BLL.tg_memberRole bll_role = new BLL.tg_memberRole();

            for (int i = 0; i < this.grid_mem.Rows.Count; i++)
            {
                CheckBox chk = this.grid_mem.Rows[i].Cells[0].FindControl("chk_id") as CheckBox;
                string str_memid = this.grid_mem.Rows[i].Cells[1].Text;
                if (chk.Checked)
                {
                    bll_mem.Delete(int.Parse(str_memid));
                    //删除离职入职信息
                    bll_ext.Delete(int.Parse(str_memid));
                    //删除角色信息
                    bll_role.Delete(int.Parse(str_memid));

                }


            }
            //弹出提示
            TG.Common.MessageBox.ShowAndRedirect(this, "用户信息删除成功！", "Sys_MemBymaster.aspx");

        }
        //绑定职位
        protected void BindRoleMag()
        {
            var list = new TG.BLL.cm_RoleMag().GetList(" IsMag=1");

            this.drpRoleMag.DataSource = list;
            this.drpRoleMag.DataTextField = "RoleName";
            this.drpRoleMag.DataValueField = "ID";
            this.drpRoleMag.DataBind();

            this.drpRoleMag0.DataSource = list;
            this.drpRoleMag0.DataTextField = "RoleName";
            this.drpRoleMag0.DataValueField = "ID";
            this.drpRoleMag0.DataBind();

            var list2 = new TG.BLL.cm_RoleMag().GetList(" IsMag=0");

            this.drpRoleTec.DataSource = list2;
            this.drpRoleTec.DataTextField = "RoleName";
            this.drpRoleTec.DataValueField = "ID";
            this.drpRoleTec.DataBind();

            this.drpRoleTec0.DataSource = list2;
            this.drpRoleTec0.DataTextField = "RoleName";
            this.drpRoleTec0.DataValueField = "ID";
            this.drpRoleTec0.DataBind();
        }
        //添加人员
        protected void btn_save_Click(object sender, EventArgs e)
        {
            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
            TG.Model.tg_member model_mem = new TG.Model.tg_member();

            //赋值
            model_mem.mem_Name = this.txt_memName.Text;
            model_mem.mem_Login = this.txt_memLogin.Text;
            model_mem.mem_Password = StringPlus.GetPwdEncryString(this.txt_pwd.Text.Trim());
            model_mem.mem_Birthday = string.IsNullOrEmpty(this.txt_Birth.Value) == true ? DateTime.Now : Convert.ToDateTime(this.txt_Birth.Value);
            model_mem.mem_Sex = this.drp_sex.SelectedItem.Value;
            model_mem.mem_Speciality_ID = int.Parse(this.drp_spec.SelectedItem.Value);
            model_mem.mem_Unit_ID = int.Parse(this.drp_unit.SelectedItem.Value);
            model_mem.mem_Principalship_ID = int.Parse(this.drp_roles.SelectedItem.Value);
            model_mem.mem_Mobile = this.txt_mobile.Text;
            model_mem.mem_Telephone = this.txt_phone.Text;
            model_mem.mem_Mailbox = this.txt_mail.Text;
            model_mem.mem_isFired = int.Parse(this.drpIsFired.SelectedValue);
            //排序
            model_mem.mem_Order = int.Parse(this.txtOrder.Value);

            //职称关系
            TG.BLL.cm_MemArchLevelRelation bll_relation = new TG.BLL.cm_MemArchLevelRelation();
            TG.Model.cm_MemArchLevelRelation model_relation = new TG.Model.cm_MemArchLevelRelation();
            //tg_member 职称字段更新
            model_mem.mem_Title = "";
            if (this.drp_level.SelectedIndex > 0)
            {
                model_mem.mem_Title = this.drp_level.SelectedItem.Text;
            }
            try
            {
                //添加人员
                int mem_ID = bll_mem.Add(model_mem);

                //添加职称表
                model_relation.Mem_ID = mem_ID;
                model_relation.ArchLevel_ID = int.Parse(this.drp_level.SelectedValue);
                //添加职称关系
                if (this.drp_level0.SelectedIndex > 0)
                {
                    bll_relation.Add(model_relation);
                }

                //头像
                TG.BLL.cm_EleSign bll_ele = new TG.BLL.cm_EleSign();
                TG.Model.cm_EleSign model_ele = new TG.Model.cm_EleSign();

                model_ele.mem_id = mem_ID;
                model_ele.elesign_min = txt_pic_s.Value;
                //model_ele.elesign = WebUserControl2.GetValue();
                model_ele.elesign = txt_pic.Text;
                if (txt_pic.Text != "")
                {
                    bll_ele.Add(model_ele);
                }

                //添加职位
                TG.BLL.tg_memberRole bll_role = new BLL.tg_memberRole();
                TG.Model.tg_memberRole roleModel = new Model.tg_memberRole();

                //如果没有选择兼职部门
                int chkcount = 0;
                foreach (ListItem chk in this.chkList.Items)
                {

                    if (chk.Selected)
                    {
                        chkcount++;

                        roleModel.mem_Id = mem_ID;
                        roleModel.RoleIdMag = int.Parse(this.drpRoleMag.SelectedValue);
                        roleModel.RoleIdTec = int.Parse(this.drpRoleTec.SelectedValue);
                        roleModel.mem_Unit_ID = int.Parse(chk.Value);
                        //计算权重
                        roleModel.Weights = GetWeightsByRoleType(this.drpRoleMag.SelectedValue, this.drpRoleTec.SelectedValue);
                        //保存职位信息
                        bll_role.Add(roleModel);
                    }
                }
                //默认角色
                if (chkcount == 0)
                {
                    roleModel.mem_Id = mem_ID;
                    roleModel.RoleIdMag = int.Parse(this.drpRoleMag.SelectedValue);
                    roleModel.RoleIdTec = int.Parse(this.drpRoleTec.SelectedValue);
                    roleModel.mem_Unit_ID = int.Parse(this.drp_unit.SelectedValue);
                    //计算权重
                    roleModel.Weights = GetWeightsByRoleType(this.drpRoleMag.SelectedValue, this.drpRoleTec.SelectedValue);
                    //保存职位信息
                    bll_role.Add(roleModel);
                }

                //添加入职信息
                SaveMemExtInfo(mem_ID);
            }
            catch (System.Exception ex)
            { }
            //弹出提示
            TG.Common.MessageBox.ShowAndRedirect(this, "用户添加成功！", "Sys_MemBymaster.aspx");

        }
        /// <summary>
        /// 得到用户的权重
        /// </summary>
        /// <param name="roleMagID"></param>
        /// <param name="roleTecID"></param>
        /// <param name="roleMagName"></param>
        /// <returns></returns>
        public int GetWeightsByRoleType(string roleMagID, string roleTecID)
        {
            int result = 0;

            //首选判断是否为管理职位
            if (roleMagID != "0")
            {
                //如果管理不为空
                var roleModel = new TG.BLL.cm_RoleMag().GetModel(int.Parse(roleMagID));
                if (roleModel != null)
                {
                    result = roleModel.Weights;
                }
            }
            else
            {
                //如果管理不为空
                var roleModel = new TG.BLL.cm_RoleMag().GetModel(int.Parse(roleTecID));
                if (roleModel != null)
                {
                    result = roleModel.Weights;
                }
            }

            return result;
        }
        //编辑部门信息
        protected void btn_edit_Click(object sender, EventArgs e)
        {
            TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
            TG.Model.tg_member model_mem = new TG.Model.tg_member();
            if (this.hid_memid.Value.Trim() != "")
            {
                model_mem.mem_ID = int.Parse(this.hid_memid.Value);
                model_mem.mem_Name = this.txt_memName0.Text;
                model_mem.mem_Login = this.hid_memlogin.Value;
                if (this.txt_pwd0.Text.Trim() != "")
                {
                    model_mem.mem_Password = StringPlus.GetPwdEncryString(this.txt_pwd0.Text);
                }
                if (this.txt_Birth0.Value.Trim() != "")
                {
                    model_mem.mem_Birthday = Convert.ToDateTime(this.txt_Birth0.Value);
                }
                model_mem.mem_Sex = this.drp_sex0.SelectedItem.Text;
                model_mem.mem_Principalship_ID = int.Parse(this.drp_roles0.SelectedItem.Value);
                model_mem.mem_Speciality_ID = int.Parse(this.drp_spec0.SelectedItem.Value);
                model_mem.mem_Unit_ID = int.Parse(this.drp_unit0.Text);
                model_mem.mem_Mailbox = this.txt_mail0.Text;
                model_mem.mem_isFired = int.Parse(this.drpIsFired0.SelectedValue);

                if (this.txt_phone0.Text.Trim() != "")
                {
                    model_mem.mem_Telephone = this.txt_phone0.Text.Trim();
                }
                if (this.txt_mobile0.Text.Trim() != "")
                {
                    model_mem.mem_Mobile = this.txt_mobile0.Text.Trim();
                }
                //排序
                model_mem.mem_Order = int.Parse(this.txtOrder2.Value);
                //职位信息
                TG.BLL.cm_MemArchLevelRelation bll_relation = new TG.BLL.cm_MemArchLevelRelation();
                TG.Model.cm_MemArchLevelRelation model_relation = new TG.Model.cm_MemArchLevelRelation();
                List<TG.Model.cm_MemArchLevelRelation> models_relation = bll_relation.GetModelList(" mem_ID=" + model_mem.mem_ID);
                //不存在职位信息
                if (models_relation.Count == 0)
                {
                    model_relation.ArchLevel_ID = int.Parse(this.drp_level0.SelectedValue);
                    model_relation.Mem_ID = model_mem.mem_ID;

                    if (drp_level0.SelectedIndex > 0)
                    {
                        //添加职位信息
                        bll_relation.Add(model_relation);
                        model_mem.mem_Title = this.drp_level0.SelectedItem.Text;
                    }
                }
                else
                {
                    model_relation = models_relation[0];
                    model_relation.ArchLevel_ID = int.Parse(this.drp_level0.SelectedValue);
                    if (drp_level0.SelectedIndex > 0)
                    {
                        //更新职位信息
                        bll_relation.Update(model_relation);
                        model_mem.mem_Title = this.drp_level0.SelectedItem.Text;
                    }
                    else
                    {
                        bll_relation.Delete(model_relation.ID);
                        model_mem.mem_Title = "";
                    }

                }

                //头像
                TG.BLL.cm_EleSign bll_ele = new TG.BLL.cm_EleSign();
                TG.Model.cm_EleSign model_ele = new TG.Model.cm_EleSign();
                TG.Model.cm_EleSign model_ele2 = new TG.Model.cm_EleSign();

                model_ele.mem_id = model_mem.mem_ID;
                model_ele.elesign_min = this.txt_pic0_s.Value;
                model_ele.elesign = this.txt_pic0.Text;
                //model_ele.elesign = WebUserControl2.GetValue();
                model_ele2 = bll_ele.GetModel2(model_mem.mem_ID);

                //已存在记录下直接修改。
                if (model_ele2 != null)
                {
                    model_ele.ID = model_ele2.ID;
                    bll_ele.Update(model_ele);
                }
                else
                {
                    bll_ele.Add(model_ele);
                }

                //更新职位信息
                //foreach (ListItem chk in this.chkList0.Items)
                //{
                //    string strWhere = string.Format(" mem_Id={0} AND mem_Unit_ID={1}", model_mem.mem_ID, chk.Value);
                //    var roleModelList = new TG.BLL.tg_memberRole().GetModelList(strWhere);
                //    if (roleModelList.Count > 0)
                //    {
                //        if (chk.Selected)
                //        {
                //            var roleModel = roleModelList[0];
                //            roleModel.RoleIdMag = int.Parse(this.drpRoleMag0.SelectedValue);
                //            roleModel.RoleIdTec = int.Parse(this.drpRoleTec0.SelectedValue);
                //            roleModel.mem_Unit_ID = int.Parse(chk.Value);
                //            //更新权重
                //            roleModel.Weights = GetWeightsByRoleType(this.drpRoleMag0.SelectedValue, this.drpRoleTec0.SelectedValue);

                //            new TG.BLL.tg_memberRole().Update(roleModel);
                //        }
                //        else
                //        {
                //            new TG.BLL.tg_memberRole().Delete(roleModelList[0].ID);
                //        }
                //    }
                //    else
                //    {
                //        if (chk.Selected)
                //        {
                //            var roleModel = new TG.Model.tg_memberRole();

                //            roleModel.mem_Id = model_mem.mem_ID;
                //            roleModel.RoleIdMag = int.Parse(this.drpRoleMag0.SelectedValue);
                //            roleModel.RoleIdTec = int.Parse(this.drpRoleTec0.SelectedValue);
                //            roleModel.mem_Unit_ID = int.Parse(chk.Value);
                //            //更新权重
                //            roleModel.Weights = GetWeightsByRoleType(this.drpRoleMag0.SelectedValue, this.drpRoleTec0.SelectedValue);

                //            new TG.BLL.tg_memberRole().Add(roleModel);
                //        }
                //    }
                //}

                //默认部门的职务信息
                string strWhere2 = string.Format(" mem_Id={0} AND mem_Unit_ID={1}", model_mem.mem_ID, this.drp_unit0.SelectedValue);
                var list = new TG.BLL.tg_memberRole().GetModelList(strWhere2);

                if (list.Count == 0)
                {
                    var tgmemRoleModel = new TG.Model.tg_memberRole();

                    tgmemRoleModel.mem_Id = model_mem.mem_ID;
                    tgmemRoleModel.RoleIdMag = int.Parse(this.drpRoleMag0.SelectedValue);
                    tgmemRoleModel.RoleIdTec = int.Parse(this.drpRoleTec0.SelectedValue);
                    tgmemRoleModel.mem_Unit_ID = int.Parse(this.drp_unit0.SelectedValue);
                    //更新权重
                    tgmemRoleModel.Weights = GetWeightsByRoleType(this.drpRoleMag0.SelectedValue, this.drpRoleTec0.SelectedValue);
                    //添加默认
                    new TG.BLL.tg_memberRole().Add(tgmemRoleModel);
                }
                else
                {
                    var tgmemRoleModel = list[0];

                    tgmemRoleModel.mem_Id = model_mem.mem_ID;
                    tgmemRoleModel.RoleIdMag = int.Parse(this.drpRoleMag0.SelectedValue);
                    tgmemRoleModel.RoleIdTec = int.Parse(this.drpRoleTec0.SelectedValue);
                    tgmemRoleModel.mem_Unit_ID = int.Parse(this.drp_unit0.SelectedValue);
                    //更新权重
                    tgmemRoleModel.Weights = GetWeightsByRoleType(this.drpRoleMag0.SelectedValue, this.drpRoleTec0.SelectedValue);
                    //添加默认
                    new TG.BLL.tg_memberRole().Update(tgmemRoleModel);
                }


                try
                {
                    bll_mem.Update(model_mem);
                }
                catch (System.Exception ex)
                { }

                //更新入职信息
                //添加入职信息
                SaveMemExtInfo(int.Parse(this.hid_memid.Value));

                //弹出提示
                //TG.Common.MessageBox.ShowAndRedirect(this, "用户资料修改成功！", "Sys_MemBymaster.aspx");
                TG.Common.MessageBox.Show(this, "用户资料修改成功");
                //重新绑定用户
                BindMem();
            }
        }

        /// <summary>
        /// 保存人员扩展信息
        /// </summary>
        /// <param name="memid"></param>
        public void SaveMemExtInfo(int memid)
        {
            TG.Model.tg_memberExt model = new Model.tg_memberExt();
            var bll = new TG.BLL.tg_memberExt();
            //查询
            string strWhere = string.Format(" mem_ID={0}", memid);
            var list = new TG.BLL.tg_memberExt().GetModelList(strWhere);
            //已存在更新
            if (list.Count > 0)
            {
                model = list[0];
                if (!string.IsNullOrEmpty(this.txtInTime0.Value.Trim()))
                {
                    model.mem_InTime = Convert.ToDateTime(this.txtInTime0.Value);
                }
                else
                {
                    model.mem_InTime = null;
                }

                if (!string.IsNullOrEmpty(this.txtOutTime0.Value.Trim()))
                {
                    model.mem_OutTime = Convert.ToDateTime(this.txtOutTime0.Value);
                }
                else
                {
                    model.mem_OutTime = null;
                }

                bll.Update(model);
            }
            else
            {
                model.mem_ID = memid;
                model.mem_Name = this.txt_memName.Text.Trim();

                if (!string.IsNullOrEmpty(this.txtInTime.Value.Trim()))
                {
                    model.mem_InTime = Convert.ToDateTime(this.txtInTime.Value);
                }
                else
                {
                    model.mem_InTime = null;
                }

                if (!string.IsNullOrEmpty(this.txtOutTime.Value.Trim()))
                {
                    model.mem_OutTime = Convert.ToDateTime(this.txtOutTime.Value);
                }
                else
                {
                    model.mem_OutTime = null;
                }

                bll.Add(model);
            }
        }


        TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
        TG.BLL.tg_principalship bll_pri = new TG.BLL.tg_principalship();
        TG.BLL.tg_speciality bll_spec = new TG.BLL.tg_speciality();
        TG.BLL.cm_MemArchLevelRelation bll_relation = new TG.BLL.cm_MemArchLevelRelation();
        TG.BLL.cm_MemArchLevel bll_level = new TG.BLL.cm_MemArchLevel();
        protected void grid_mem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //单位

                string str_unitid = e.Row.Cells[6].Text;
                if (str_unitid != "")
                {
                    TG.Model.tg_unit model_unit = bll_unit.GetModel(int.Parse(str_unitid));
                    if (model_unit != null)
                    {
                        e.Row.Cells[6].Text = model_unit.unit_Name.ToString().Trim();
                    }
                }
                //角色

                string str_priid = e.Row.Cells[8].Text;
                if (str_priid != "")
                {
                    TG.Model.tg_principalship model_pri = bll_pri.GetModel(int.Parse(str_priid));
                    if (model_pri != null)
                    {
                        e.Row.Cells[8].Text = model_pri.pri_Name.ToString().Trim();
                    }
                }
                //专业

                string str_specid = e.Row.Cells[7].Text;
                if (str_specid != "")
                {
                    TG.Model.tg_speciality model_spec = bll_spec.GetModel(int.Parse(str_specid));
                    if (model_spec != null)
                    {
                        e.Row.Cells[7].Text = model_spec.spe_Name.ToString().Trim();
                    }
                }
                //职位

                Label lbl_arch = e.Row.Cells[11].FindControl("lbl_archlevel") as Label;
                if (lbl_arch != null)
                {
                    List<TG.Model.cm_MemArchLevelRelation> models_relation = bll_relation.GetModelList(" mem_ID=" + lbl_arch.Text);
                    if (models_relation.Count > 0)
                    {
                        //职称
                        TG.Model.cm_MemArchLevel model_level = bll_level.GetModel(models_relation[0].ArchLevel_ID);
                        if (model_level != null)
                        {
                            lbl_arch.Text = model_level.Name.Trim();
                            HiddenField hid_archlevelid = e.Row.Cells[11].FindControl("hid_archlevel") as HiddenField;
                            //赋值
                            hid_archlevelid.Value = model_level.ArchLevel_ID.ToString();
                        }
                        else
                        {
                            lbl_arch.Text = "无";
                        }
                    }
                    else
                    {
                        lbl_arch.Text = "无";
                    }
                }
                //头像
                TG.BLL.cm_EleSign bll_ele = new TG.BLL.cm_EleSign();

                HiddenField memid_field = e.Row.Cells[1].FindControl("hidden_memid") as HiddenField;
                if (!string.IsNullOrEmpty(memid_field.Value))
                {
                    TG.Model.cm_EleSign model_ele = bll_ele.GetModel2(int.Parse(memid_field.Value));
                    if (model_ele != null)
                    {
                        e.Row.Cells[12].Text = "<a href='" + model_ele.elesign + "'><img src='" + model_ele.elesign + "' onerror=\"this.src='/Images/zw.jpg'\" border=0 width=20 height=20/></a>";
                        // e.Row.Cells[12].Text = model_ele.elesign;
                    }

                }

                //在职离职
                if (!string.IsNullOrEmpty(e.Row.Cells[15].Text))
                {
                    if (e.Row.Cells[15].Text == "0")
                    {
                        e.Row.Cells[15].Text = "在职";
                    }
                    else
                    {
                        e.Row.Cells[15].Text = "离职";
                    }
                }
            }
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindMem();
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindMem();
        }

        //生产部门改变重新绑定数据
        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataSetPageIndex();
        }
        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>
        protected void BindDataSetPageIndex()
        {
            this.AspNetPager1.CurrentPageIndex = 0;
            //绑定数据
            BindMem();
        }
    }
}