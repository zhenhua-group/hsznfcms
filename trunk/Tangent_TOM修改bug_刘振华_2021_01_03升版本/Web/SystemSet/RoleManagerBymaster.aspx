﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="RoleManagerBymaster.aspx.cs" Inherits="TG.Web.SystemSet.RoleManagerBymaster" %>

<%@ Register Src="../Coperation/SelectUserControl.ascx" TagName="SelectUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControl/EditRolePowerControl.ascx" TagName="EditRolePowerControl"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <%--
    <link href="/css/LeaderManage.css" rel="stylesheet" type="text/css" />--%>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/Coperation/SelectRoleUser.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/UserControl/EditRolePowerControl.js"></script>
    <script src="../js/SystemSet/RoleManagerBymaster.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        $(function () {

            var roleManager = new RoleManagerBymaster();
        });  //清空数据  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>角色权限设置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>权限管理</a><i class="fa fa-angle-right"> </i><a>角色权限设置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>角色信息列表</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="form-body">
                        <table border="0" cellspacing="0" cellpadding="0" width="25%">
                            <tr>
                                <td>
                                    角色管理：
                                </td>
                                <td>
                                    <button class="btn blue" type="button" id="AddNewRole" href="#EditRoleDiv" data-toggle="modal">
                                        添加角色管理</button>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div class="row">
                            <asp:Repeater ID="RepeaterRoleList" runat="server">
                                <HeaderTemplate>
                                    <table class="table table-bordered table-hover" id="GridView1">
                                        <tr>
                                            <th style="width: 15%;" align="center">
                                                角色名
                                            </th>
                                            <th style="width: 70%;" align="center">
                                                用户
                                            </th>
                                            <th style="width: 15%;" align="center">
                                                操作
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 15%;" align="left">
                                            <img src="/Images/role.png" style="width: 16px; height: 16px;" />
                                            <%#Eval("RoleName")%>
                                        </td>
                                        <td style="width: 70%;" align="left">
                                            <%#Eval("UserNameViewString")%>
                                        </td>
                                        <td style="width: 15%;" align="left">
                                            <a href="#EditRoleDiv" rolesysno="<%#Eval("SysNo")%>" id="editRoleLinkButton" data-toggle="modal"
                                                rolename="<%#Eval("RoleName") %>">编辑用户</a>&nbsp;<a href="#EditPowerDiv" rolesysno="<%#Eval("SysNo")%>"
                                                    id="editPowerLinkButton" data-toggle="modal" rolename="<%#Eval("RoleName") %>">
                                                    编辑权限</a>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div id="EditRoleDiv" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">
                编辑承接部门负责人所对应的用户</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="PopAreaRoleManager">
                        <div class="panel panel-success">
                            <div class="panel-heading" style="padding: 0; line-height: 25px;">
                                &nbsp;<i class="fa fa-cogs"></i> 包含用户</div>
                            <div class="panel-body" style="height: auto!important; height: 200px; min-height: 200px;">
                                <div style="width: 100%; padding-bottom: 5px;">
                                    角色名：
                                    <input type="text" id="roleNameTextBox" class="form-control input-sm" style="height: 20px;
                                        font-size: 12px; width: 150px; padding: 0px; display: inline;" />
                                </div>
                                <div style="width: 100%; height: 100%;" id="roleUserDiv">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_saveRole"  class="btn blue">
                保存</button>
            <button type="button" data-toggle="modal" id="btn_AddUser" href="#chooseUserMainDiv"
                class="btn  yellow">
                用户</button>
            <button type="button" data-dismiss="modal" class="btn default" id="btn_close">
                关闭</button>
        </div>
    </div>
    <div id="chooseUserMainDiv" class="modal fade yellow" tabindex="-1" data-width="760"
        aria-hidden="true" style="display: none; width: 760px; height: 530px; margin-left: -379px;
        margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">
                选择用户</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="chooseUserMain">
                        <uc1:SelectUserControl ID="SelectUserControl1" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn green btn-default" id="btn_SaveUser">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default" id="user_close">
                关闭</button>
        </div>
    </div>
    <!--编辑权限-->
    <div id="EditPowerDiv" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">
                编辑权限</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="editPowerControl">
                        <div style="overflow: auto; height: 300px;">
                            <uc2:EditRolePowerControl ID="EditRolePowerControl1" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button"  class="btn green btn-default" id="btn_SavePower">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default" id="power_close">
                关闭</button>
        </div>
    </div>
</asp:Content>
