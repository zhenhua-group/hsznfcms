﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="WorkLogListBymaster.aspx.cs" Inherits="TG.Web.SystemSet.WorkLogListBymaster" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery.alerts.css" rel="stylesheet" />

    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/jquery.chromatable.js"></script>
    <script type="text/javascript" src="../js/SystemSet/MsgComm.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        $(function () {

            //删除是判断有无记录选中
            $("#<%=btn_DelCst.ClientID%>").click(function () {
                if ($("#<%=GridView1.ClientID%> .checked").length == 0) {
                    jAlert("请选择要删除的日志！", "提示");
                    return false;
                }
                //判断是否要删除
                return confirm("是否要删除日志信息？");
            });

            //全选-反选
            $('#<%=GridView1.ClientID%>_ctl01_chkAll').click(function () {

                var checks = $('#<%=GridView1.ClientID%> :checkbox');

                if ($('#<%=GridView1.ClientID%>_ctl01_chkAll').attr("checked") == "checked") {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.setAttribute("class", "checked");
                        $(checks[i]).attr("checked", "checked");
                    }
                }
                else {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.removeAttribute("class");
                        $(checks[i]).removeAttr("checked");
                    }
                }
            });
            $('.menu_Open').click(function () {
                $("#show8").parent().attr("class", " arrow open");
                $("#show8_4").parent().attr("class", " arrow open");
                $("#show8_4").parent().parent().css("display", "block");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>日志管理 </small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a>
        <a href="javascript:;" class="menu_Open">系统设置</a> <i class="fa fa-angle-right"></i><a href="javascript:;" class="menu_Open">公告管理</a> <i class="fa fa-angle-right"></i><a href="javascript:;" class="menu_Open">日志管理</a> </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>日志搜索
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>日志标题:</td>
                            <td>
                                <input id="txt_keyname" name="txt_keyname" type="text" runat="Server" class="form-control" /></td>
                            <td>
                                <asp:Button ID="btn_Search" runat="server" CssClass="btn blue" Text="查询"
                                    OnClick="btn_Search_Click" /></td>
                            <td><a href="BannerAndLogViewBymaster.aspx?viewType=0" target="_self" class="btn blue">添加
                            </a></td>
                            <td>
                                <asp:Button ID="btn_DelCst" runat="server" CssClass="btn blue" Text="删除"
                                    OnClick="btn_DelCst_Click" /></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet   box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>日志列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" ShowHeader="true" CssClass="table table-striped table-bordered table-hover dataTable"
                        Width="100%" OnRowDataBound="GridView1_RowDataBound" EnableModelValidation="True">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAll" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("SysNo") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="5%" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Title" HeaderText="标题">
                                <ItemStyle HorizontalAlign="Left" Width="50%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="InDate" HeaderText="发布时间">
                                <ItemStyle HorizontalAlign="Left" Width="20%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="InUser" HeaderText="用户">
                                <ItemStyle HorizontalAlign="Left" Width="15%" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="查看">
                                <ItemTemplate>
                                    <a href='<%# "ShowMsgViewBymaster.aspx?type=1&sysno="+Eval("SysNo") %>' target="_self">查看</a>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="编辑">
                                <ItemTemplate>
                                    <a href='<%# "BannerAndLogViewBymaster.aspx?action=1&viewType=0&sysno="+Eval("SysNo") %>'
                                        target="_self">编辑</a>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="5%" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            无日志信息！
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pages" CurrentPageButtonClass="cpb"
                        CustomInfoHTML="共%PageCount%页，当前第%CurrentPageIndex%页，每页%PageSize%条，共%RecordCount%条"
                        CustomInfoTextAlign="Left" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                        OnPageChanged="AspNetPager1_PageChanged" PageIndexBoxClass="indexbox" PageIndexBoxType="TextBox"
                        PrevPageText="上一页" ShowBoxThreshold="10" ShowCustomInfoSection="Left" ShowPageIndexBox="Auto"
                        SubmitButtonText="Go" TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" PageIndexBoxStyle="width:25px;"
                        PageSize="30" SubmitButtonClass="submitbtn">
                    </webdiyer:AspNetPager>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
