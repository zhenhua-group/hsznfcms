﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.BLL;

namespace TG.Web.SystemSet
{
    public partial class ShowMsgViewBymaster : System.Web.UI.Page
    {
        //消息类型
        public string MsgType
        {
            get
            {
                return Request.QueryString["type"] ?? "";
            }
        }

        public int WebBannerItemSysNo
        {
            get
            {
                int webBannerItemSysNo = 0;
                int.TryParse(Request["itemSysNo"], out webBannerItemSysNo);
                return webBannerItemSysNo;
            }
        }
        public int allpeople = 0;
        public int allfile = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string id = Request.QueryString["sysno"] ?? "0";
                if (MsgType == "0")
                {
                    string strSql = " Select * From cm_WebBanner Where SysNo=" + id;
                    DataSet ds_wb = new TG.BLL.CommDBHelper().GetList(strSql);
                    if (ds_wb.Tables.Count > 0)
                    {
                        if (ds_wb.Tables[0].Rows.Count > 0)
                        {
                            this.lbl_title.Text = ds_wb.Tables[0].Rows[0]["Title"].ToString();
                            this.lbl_content.Text = ds_wb.Tables[0].Rows[0]["Content"].ToString();
                            this.lbl_user.Text = new TG.BLL.tg_member().GetModel(int.Parse(ds_wb.Tables[0].Rows[0]["InUser"].ToString())).mem_Name;
                            this.lbl_time.Text = ds_wb.Tables[0].Rows[0]["InDate"].ToString();
                        }
                    }
                    List<TG.Model.WebBannerItemViewEntity> list = new WebBannerBP().GetWebBannerListByWebBannerSysNo(Convert.ToInt32(ds_wb.Tables[0].Rows[0][0]));
                    allpeople = list.Count;
                    this.RepeaterUsers.DataSource = list;
                    this.RepeaterUsers.DataBind();
                    List<TG.Model.WebBannerAttachViewEntity> list2 = new WebBannerAttachBP().GetWebBannerViewEntityList(id);
                    allfile = list2.Count;
                    this.RepeaterAttach.DataSource = list2;
                    this.RepeaterAttach.DataBind();
                }
                else
                {
                    string strSql = " Select * From cm_WorkLog Where SysNo=" + id;
                    DataSet ds_wb = new TG.BLL.CommDBHelper().GetList(strSql);
                    if (ds_wb.Tables.Count > 0)
                    {
                        if (ds_wb.Tables[0].Rows.Count > 0)
                        {
                            this.lbl_title.Text = ds_wb.Tables[0].Rows[0]["Title"].ToString();
                            this.lbl_content.Text = ds_wb.Tables[0].Rows[0]["Content"].ToString();
                            this.lbl_user.Text = new TG.BLL.tg_member().GetModel(int.Parse(ds_wb.Tables[0].Rows[0]["InUser"].ToString())).mem_Name;
                            this.lbl_time.Text = ds_wb.Tables[0].Rows[0]["InDate"].ToString();
                        }
                    }
                }

                //更新状态
                UpdateStatus(int.Parse(id));
            }
        }
        //更新状态
        protected void UpdateStatus(int id)
        {
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
            if (MsgType == "0")
            {
                string strSql = " Update cm_WenBannerItem Set Status=1 Where SysNo=" + WebBannerItemSysNo;
                bll.ExcuteBySql(strSql);
            }
            else
            {
                string strSql = " Update cm_WorkLog Set Status=1 Where SysNo=" + id;
                bll.ExcuteBySql(strSql);
            }
        }
    }
}