﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class AddQualificationBymaster : System.Web.UI.Page
    {
        TG.BLL.cm_Qualification quabll = new BLL.cm_Qualification();
        TG.BLL.cm_AttachInfo attbll = new BLL.cm_AttachInfo();
        TG.BLL.cm_QuaAttach cm_bll = new BLL.cm_QuaAttach();

        /// <summary>
        /// 
        /// </summary>
        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btn_submin_Click(object sender, EventArgs e)
        {
            AddQua();

        }
        /// <summary>
        /// 增加资质
        /// </summary>
        private void AddQua()
        {
            // Session["memid"] = 1;
            TG.Model.cm_Qualification model = new Model.cm_Qualification();
            model.qua_Name = this.txt_name.Text;
            model.qua_ZzTime = Convert.ToDateTime(this.txt_startdate.Text);
            model.qua_Content = this.txt_remark.Text;
            model.qua_SpTime = Convert.ToDateTime(this.txt_finishdate.Text);
            model.qua_ImageSrc = GetQualFileName();
            model.qua_yeartime = Convert.ToDateTime(this.lbl_yeartime.Text);
            model.qua_jb = this.lbl_jb.SelectedValue;
            model.cm_AttaInfoId = Convert.ToInt32(this.hid_projid.Value);
            model.cst_Id = Convert.ToInt32(UserInfo["memid"]);
            int quaid = quabll.Add(model);
            string sql = "Temp_No IN (SELECT cast(cm_qualification.cm_AttaInfoId as char(30)) FROM cm_qualification WHERE  qua_Id=" + quaid + ")";
            List<TG.Model.cm_AttachInfo> list = attbll.GetModelList(sql);
            foreach (TG.Model.cm_AttachInfo attmodel in list)
            {
                TG.Model.cm_QuaAttach quamodel = new Model.cm_QuaAttach();
                quamodel.FileName = attmodel.FileName;
                quamodel.FileSize = Convert.ToInt32(attmodel.FileSize);
                quamodel.FileType = attmodel.FileType;
                quamodel.IconPath = attmodel.FileTypeImg;
                quamodel.RelativePath = "/Attach_User/filedata/qualfile/" + attmodel.Temp_No.Trim() + "/" + attmodel.FileName;
                quamodel.UpLoadDate = Convert.ToDateTime(attmodel.UploadTime);
                quamodel.WebBannerSysNo = quaid.ToString();
                cm_bll.Add(quamodel);
            }
            if (quaid > 0)
            {
                TG.Common.MessageBox.ShowAndRedirect(this, "资质添加成功！", "QualificationListBymaster.aspx");

            }
        }

        public string GetProjectID()
        {
            return (((((DateTime.Now.Year + DateTime.Now.Month) + DateTime.Now.Day) + DateTime.Now.Hour) + DateTime.Now.Minute) + DateTime.Now.Second.ToString());

        }
        /// <summary>
        /// 得到当前用户的id
        /// </summary>
        /// <returns></returns>
        public string GetCurMemID()
        {
            return Convert.ToString(this.UserInfo["memid"] ?? "0");
        }
        public string GetQualFileName()
        {
            string sql = "SELECT TOP 1 FileName FROM cm_AttachInfo ORDER BY UploadTime DESC;";
            //return TG.DBUtility.TGsqlHelp.ExecuteScalar(sql).ToString();
            return TG.DBUtility.DbHelperSQL.GetSingle(sql).ToString();
        }
    }
}