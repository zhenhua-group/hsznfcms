﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class SetLayout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //横向布局
                if (IsLayoutTop())
                {
                    this.lbl_Status.Text = "横向菜单布局";
                    this.btn_SetLayout.Text = "设置为左右菜单布局";
                }
                else
                {
                    this.lbl_Status.Text = "左右菜单布局";
                    this.btn_SetLayout.Text = "设置为横向菜单布局";
                }
            }
        }

        protected void btn_SetLayout_Click(object sender, EventArgs e)
        {
            string filePath = Server.MapPath("/");
            string src_fileName = filePath + "MainPage.Master";
            string tgt_fileName = filePath + "MainPage_H.Master";
            string real_fileName = filePath + "MaMainPage_V.Master";
            //横向布局
            if (IsLayoutTop())
            {
                File.Move(src_fileName,tgt_fileName);
                File.Move(real_fileName, src_fileName);
            }
            else
            {
                File.Move(src_fileName, real_fileName);
                File.Move(tgt_fileName, src_fileName);
            }

            TG.Common.MessageBox.ShowAndRedirect(this, "设置成功，请重新登录", "/Login.aspx");
        }

        //是否出于横向布局
        protected bool IsLayoutTop()
        {
            string filepath = Server.MapPath("/MainPage_H.Master");

            if (File.Exists(filepath))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}