﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class PubMsgListBymaster : System.Web.UI.Page
    {
        //公用数据处理类
        TG.BLL.CommDBHelper bll_comm = new TG.BLL.CommDBHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindPubMsg();
            }
        }

        //绑定公告
        protected void BindPubMsg()
        {
            string strWhere = "";
            if (this.txt_keyname.Value != "")
            {
                strWhere += "  AND Title Like '%" + this.txt_keyname.Value + "%'";
            }
            //记录数
            this.AspNetPager1.RecordCount = bll_comm.GetDataPageListCount(strWhere, "cm_WebBanner");
            //排序
            string orderString = " Order By SysNo DESC ";
            //绑定
            this.GridView1.DataSource = bll_comm.GetDataPageList(strWhere, orderString, this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize, "cm_WebBanner");
            this.GridView1.DataBind();
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindPubMsg();
        }

        protected void btn_DelCst_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.GridView1.Rows.Count; i++)
            {
                string id = ((HiddenField)this.GridView1.Rows[i].Cells[0].FindControl("HiddenField1")).Value;
                CheckBox chk = this.GridView1.Rows[i].Cells[0].FindControl("CheckBox1") as CheckBox;
                string strSql = " Delete From cm_WebBanner Where SysNo=" + id;
                if (chk.Checked)
                {
                    bll_comm.ExcuteBySql(strSql);
                }
            }

            //重新绑定
            BindPubMsg();
        }
        //人员实体
        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string mem_id = e.Row.Cells[3].Text;
                if (mem_id != "")
                {
                    e.Row.Cells[3].Text = bll_mem.GetModel(int.Parse(mem_id)).mem_Name;
                }
                //状态
                string status = e.Row.Cells[4].Text;
                if (status == "1")
                {
                    e.Row.Cells[4].Text = "隐藏";
                }
                else
                {
                    e.Row.Cells[4].Text = "显示";
                }
            }
        }

        protected void btn_Ycang_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.GridView1.Rows.Count; i++)
            {
                string id = ((HiddenField)this.GridView1.Rows[i].Cells[0].FindControl("HiddenField1")).Value;
                CheckBox chk = this.GridView1.Rows[i].Cells[0].FindControl("CheckBox1") as CheckBox;
                string strSql = " Update cm_WebBanner set Status=1 Where SysNo=" + id;
                if (chk.Checked)
                {
                    bll_comm.ExcuteBySql(strSql);
                }
            }

            //重新绑定
            BindPubMsg();
        }

        protected void btn_XShi_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.GridView1.Rows.Count; i++)
            {
                string id = ((HiddenField)this.GridView1.Rows[i].Cells[0].FindControl("HiddenField1")).Value;
                CheckBox chk = this.GridView1.Rows[i].Cells[0].FindControl("CheckBox1") as CheckBox;
                string strSql = " Update cm_WebBanner set Status=0 Where SysNo=" + id;
                if (chk.Checked)
                {
                    bll_comm.ExcuteBySql(strSql);
                }
            }

            //重新绑定
            BindPubMsg();
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindPubMsg();
        }
    }
}