﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;

namespace TG.Web.SystemSet
{
    public partial class SetCompanyConfigBymaster : System.Web.UI.Page
    {
        TG.BLL.cm_Links bll = new TG.BLL.cm_Links();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindLinkData();
            }
        }
        //绑定
        protected void BindLinkData()
        {
            this.GridView1.DataSource = bll.GetAllList();
            this.GridView1.DataBind();
        }
        protected void btn_linkadd_Click(object sender, EventArgs e)
        {
            TG.Model.cm_Links model = new TG.Model.cm_Links();
            model.LinkName = this.txt_linkname.Text.Trim();
            model.LinkAddr = this.txt_link.Text.Trim();
            if (bll.Add(model) > 0)
            {
                // MessageBox.Show(this, "首页快捷链接添加成功！");
            }
            TG.Common.MessageBox.ShowAndRedirect(this, "首页快捷链接添加成功", "SetCompanyConfigBymaster.aspx");
        }
    }
}