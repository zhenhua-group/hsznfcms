﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;
using TG.DBUtility;

namespace TG.Web.SystemSet
{
    public partial class Sys_MemTravelThree : PageBase
    {

        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();

        protected List<TG.Model.tg_member> ResultList
        { get; set; }

        /// <summary>
        /// 设置的年份
        /// </summary>
        protected List<int> YearList
        {
            get
            {
                List<int> result = new List<int>();

                var bll = new TG.BLL.cm_MemOutHis();
                string strWhere = string.Format(" SetYear Between {0} AND {1}", 2011, this.drp_year1.SelectedValue);

                var list = bll.GetModelList(strWhere);

                var query = (from c in list
                             select c.SetYear).Distinct().OrderBy(c => c);

                query.ToList().ForEach(c =>
                {
                    result.Add(c.Value);
                });

                return result;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //默认加载当前日期
                SelectCurYear();
                //人员信息
                BindMem();
            }
        }
        /// <summary>
        /// 选中当前
        /// </summary>
        private void SelectCurYear()
        {
            this.drp_year1.ClearSelection();

            int curyear = DateTime.Now.Year;

            if (this.drp_year1.Items.FindByValue(curyear.ToString()) != null)
            {
                this.drp_year1.Items.FindByValue(curyear.ToString()).Selected = true;
            }
        }

        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }

        //绑定角色信息
        protected void BindMem()
        {
            var bll = new TG.BLL.tg_Level();
            var blllevel = new TG.BLL.tg_memberLevel();
            var bllmem = new TG.BLL.tg_member();
            var bllout = new TG.BLL.cm_MemOutHis();

            string strWhere;
            string curYear = this.drp_year1.SelectedValue;

            //查询人数
            //strWhere = string.Format(" mem_Year={0} ", curYear);
            var memlevelList = blllevel.GetModelList("");
            var memList = bllmem.GetModelList("");
            //var levelList = bll.GetModelList(" ID<16 ");
            ////查询人员
            //var query = from m in memlevelList
            //            join u in memList on m.mem_ID equals u.mem_ID
            //            join l in levelList on m.mem_Level equals l.ID into temp
            //            from tt in temp.DefaultIfEmpty()
            //            orderby u.mem_Order ascending
            //            select new MemOutTwo()
            //            {
            //                Mem_ID = m.mem_ID,
            //                Mem_Name = u.mem_Name,
            //                StartYear = 2011,
            //                LevelCount = tt.LevelCount,
            //                BeginCount = GetBeginCount(2011, int.Parse(curYear), m.mem_ID),
            //                CurYue = GetYue(int.Parse(curYear), m.mem_ID, tt.LevelCount),
            //                SetCount = GetSetCount(int.Parse(curYear), m.mem_ID)
            //            };

            var query = from m in memList
                        where memlevelList.Any(c => c.mem_ID == m.mem_ID)
                        select m;

            this.ResultList = query.ToList();
        }

        /// <summary>
        /// 获取设置值和
        /// </summary>
        protected TG.BLL.cm_MemOutHis bllhis = new BLL.cm_MemOutHis();

        /// <summary>
        /// 获取余额列表
        /// </summary>
        /// <param name="memid"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public List<TG.Model.cm_MemOutHis> GetYueList(int memid,int year)
        {
            string strWhere = string.Format(" Mem_ID={1} AND SetYear={0} ", year, memid);

            var modellist = bllhis.GetModelList(strWhere);

            return modellist;
        }
        

        public decimal? GetBeginCount(int? start, int? end, int? memid)
        {
            string strWhere = string.Format(" Mem_ID={0} AND (SetCount>={1} AND SetCount<{2})", memid, start, end);

            return bllhis.GetModelList(strWhere).Sum(c => c.SetCount);
        }

        /// <summary>
        /// 得到当年余额
        /// </summary>
        /// <param name="curyear"></param>
        /// <param name="memid"></param>
        /// <param name="levelCount"></param>
        /// <returns></returns>
        public decimal? GetYue(int? curyear, int? memid, decimal? levelCount)
        {
            string strWhere = string.Format(" Mem_ID={0} AND SetYear={1}", memid, curyear - 1);

            decimal? result = 0;
            decimal? yue = 0;
            decimal? setcount = 0;
            if (bllhis.GetRecordCount(strWhere) > 0)
            {
                yue = bllhis.GetModelList(strWhere)[0].Yue;
                setcount = bllhis.GetModelList(strWhere)[0].SetCount;
            }
            //C 次年
            decimal? ciyear = Math.Round(Convert.ToDecimal(1 / levelCount), 2);

            result = (yue + ciyear) - setcount;
            //结果
            return result;
        }

        /// <summary>
        /// 获取设置值
        /// </summary>
        /// <param name="curyear"></param>
        /// <param name="memid"></param>
        /// <returns></returns>
        public decimal? GetSetCount(int? curyear, int? memid)
        {
            string strWhere = string.Format(" Mem_ID={0} AND SetYear={1}", memid, curyear);

            decimal? result = 0;
            if (bllhis.GetRecordCount(strWhere) > 0)
            {
                result = bllhis.GetModelList(strWhere)[0].SetCount;
            }
            //结果
            return result;
        }

        protected void drp_year1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMem();
        }
    }

}