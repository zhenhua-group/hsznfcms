﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class ShowSingleQuaBymaster : System.Web.UI.Page
    {
        TG.BLL.cm_Qualification quabll = new TG.BLL.cm_Qualification();
        TG.BLL.cm_QuaAttach cm_bll = new TG.BLL.cm_QuaAttach();
        protected void Page_Load(object sender, EventArgs e)
        {
            string quaid = Request.QueryString["quaid"];
            string attinfoid = Request.QueryString["attinfoid"];
            if (!IsPostBack)
            {
                //绑定资质
                BindQuali(quaid);
            }
        }
        /// <summary>
        /// 绑定资质
        /// </summary>
        /// <param name="quaid"></param>
        private void BindQuali(string quaid)
        {
            TG.Model.cm_Qualification model = quabll.GetSingleModel(quaid);
            this.lbl_title.Text = model.qua_Name;
            this.lbl_content.InnerText = model.qua_Content;
            this.lbl_Zztime.Text = TG.Common.CommCoperation.GetEasyTime(Convert.ToDateTime(model.qua_ZzTime));
            this.lbl_Sptime.Text = TG.Common.CommCoperation.GetEasyTime(Convert.ToDateTime(model.qua_SpTime));
            this.lbl_jb.Text = model.qua_jb;
            this.lbl_yeartime.Text = TG.Common.CommCoperation.GetEasyTime(Convert.ToDateTime(model.qua_yeartime));
        }
        //得到Id
        public string GetProjectID()
        {
            string quaid = Request.QueryString["quaid"];
            return quabll.GetattInfoid(quaid);

        }
    }
}