﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;
using TG.DBUtility;

namespace TG.Web.SystemSet
{
    public partial class Sys_MemTravel : PageBase
    {

        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();

        protected List<MemOut> ResultList
        { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //默认加载当前日期
                SelectCurYear();
                //人员信息
                BindMem();
            }
        }
        /// <summary>
        /// 选中当前
        /// </summary>
        private void SelectCurYear()
        {
            this.drp_year.ClearSelection();

            int curyear = DateTime.Now.Year;

            if (this.drp_year.Items.FindByValue(curyear.ToString()) != null)
            {
                this.drp_year.Items.FindByValue(curyear.ToString()).Selected = true;
            }
        }

        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }

        //绑定角色信息
        protected void BindMem()
        {
            var bll = new TG.BLL.tg_Level();
            var blllevel = new TG.BLL.tg_memberLevel();
            var bllmem = new TG.BLL.tg_member();
            //查询所有人员等级
            var list = bll.GetModelList(" ID<16 ");
            //结果
            var outlist = new List<MemOut>();
            MemOut outmodel;
            string strWhere;
            string curYear = this.drp_year.SelectedValue;
            list.ForEach(c =>
            {

                outmodel = new MemOut();

                outmodel.Mem_Level = c.LevelName;
                outmodel.Mem_LevelID = c.ID;
                //查询人数
                strWhere = string.Format(" mem_Year={0} AND mem_Level={1}", curYear, c.ID);
                var memlevelList = blllevel.GetModelList(strWhere);
                var memList = bllmem.GetModelList("");
                outmodel.MemCount = memlevelList.Count;
                //年/次
                outmodel.MemCountTime = c.LevelCount;
                //次/年
                outmodel.MemCountTime2 = outmodel.MemCountTime == 0 ? 0 : Math.Round(Convert.ToDecimal(1 / outmodel.MemCountTime), 2);
                //人次
                outmodel.MemCountTime3 = outmodel.MemCount * outmodel.MemCountTime2;
                //查询人员
                var query = from m in memlevelList
                            join u in memList on m.mem_ID equals u.mem_ID
                            where m.mem_ID != 1440
                            orderby u.mem_Order ascending
                            select u;

                outmodel.MemList = query.ToList();

                //添加
                outlist.Add(outmodel);
            });


            this.ResultList = outlist;
        }
        protected void drp_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMem();
        }
    }

    public class MemOut
    {
        public string Mem_Level { get; set; }

        public int Mem_LevelID { get; set; }

        public int MemCount { get; set; }

        public decimal? MemCountTime { get; set; }

        public decimal? MemCountTime2 { get; set; }


        public decimal? MemCountTime3 { get; set; }

        public List<TG.Model.tg_member> MemList { get; set; }
    }
}