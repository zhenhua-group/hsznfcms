﻿using Geekees.Common.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;

namespace TG.Web.SystemSet
{
    public partial class SysUserInfoList : System.Web.UI.Page
    {
        /// <summary>
        /// 返回所有部门
        /// </summary>
        public List<TG.Model.tg_unit> UnitList
        {
            get
            {
                string strWhere = " unit_ParentID<>0 ";
                var list = new TG.BLL.tg_unit().GetModelList(strWhere);

                return list;
            }
        }
        /// <summary>
        /// 返回所有专业
        /// </summary>
        public List<TG.Model.tg_speciality> SpecList
        {
            get
            {
                string strWhere = "";
                var list = new TG.BLL.tg_speciality().GetModelList(strWhere);

                return list;
            }
        }

        /// <summary>
        /// 管理职位
        /// </summary>
        public List<TG.Model.cm_RoleMag> RoleMag
        {
            get
            {
                string strWhere = " IsMag=1 ";
                var list = new TG.BLL.cm_RoleMag().GetModelList(strWhere);

                return list;
            }
        }

        /// <summary>
        /// 技术职位
        /// </summary>
        public List<TG.Model.cm_RoleMag> RoleTec
        {
            get
            {
                string strWhere = " IsMag=0 ";
                var list = new TG.BLL.cm_RoleMag().GetModelList(strWhere);

                return list;
            }
        }
        /// <summary>
        /// 职称
        /// </summary>
        public List<TG.Model.cm_ProfesionName> Profesion
        {
            get
            {
                string strWhere = "";
                var list = new TG.BLL.cm_ProfesionName().GetModelList(strWhere);

                return list;
            }
        }

        /// <summary>
        /// 注册级别
        /// </summary>
        public List<TG.Model.cm_MemArchLevel> ArchLevel
        {
            get
            {
                string strWhere = "";
                var list = new TG.BLL.cm_MemArchLevel().GetModelList(strWhere);

                return list;
            }
        }

        public string ColumnsStr
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                Dictionary<string, string> dic = new Dictionary<string, string>();

                dic.Add("员工账号", "mem_Login");
                dic.Add("姓名", "mem_Name");
                dic.Add("在职状态", "isFiredName");
                dic.Add("所属部门", "memUnitName");
                dic.Add("性别", "mem_Sex");
                dic.Add("从事专业", "memSpeName");
                dic.Add("管理职位", "RoleMagName");
                dic.Add("技术职位", "RoleTecName");
                dic.Add("身份证号", "mem_Code");
                dic.Add("户籍所在地", "mem_CodeAddr");
                dic.Add("出生日期", "mem_Birthday");
                dic.Add("联系方式", "mem_Mobile");
                dic.Add("分机号", "mem_Telephone");
                dic.Add("职称评定", "mem_ArchLevelName2");
                dic.Add("评定时间", "mem_ArchLevelTime");
                dic.Add("年龄", "mem_Age");
                dic.Add("注册", "ArchLevelName");
                dic.Add("注册时间", "mem_ArchRegTime");
                dic.Add("注册地点", "mem_ArchRegAddrName");

                dic.Add("毕业学校", "mem_School");
                dic.Add("学位(历)", "mem_SchLevel");
                dic.Add("毕业专业", "mem_SchSpec");
                dic.Add("毕业时间", "mem_SchOutDate");
                dic.Add("毕业学校2", "mem_School2");
                dic.Add("学位(历)2", "mem_SchLevel2");
                dic.Add("毕业专业2", "mem_SchSpec2");
                dic.Add("毕业时间2", "mem_SchOutDate2");

                dic.Add("参加工作时间", "mem_WorkTime");
                dic.Add("工作间隔时间", "mem_WorkDiff");
                dic.Add("工作间隔时间(备注)", "mem_WorkDiffSub");
                dic.Add("工作年限", "mem_WorkYear");
                dic.Add("试用周期", "mem_WorkTemp");
                dic.Add("入司时间", "mem_InCompanyTime");
                dic.Add("入司间隔时间", "mem_InCompanyDiff");
                dic.Add("入司间隔时间(备注)", "mem_InCompanyDiffSub");
                dic.Add("入司年限", "mem_InCompanyYear");
                dic.Add("岗位工龄", "mem_InCompanyTemp");
                dic.Add("档案所在地", "mem_FileAddr");
                dic.Add("休假基数", "mem_Holidaybase");
                dic.Add("年假时间", "mem_HolidayYear");
                dic.Add("总经理特批", "mem_MagPishi");
                dic.Add("转正时间", "mem_ZhuanZheng");
                dic.Add("工资", "mem_GongZi");
                dic.Add("交通补贴", "mem_Jiaotong");
                dic.Add("通讯补贴", "mem_Tongxun");
                dic.Add("应发工资", "mem_YingFa");
                dic.Add("社保基数", "mem_SheBaobase");
                dic.Add("公积金", "mem_Gongjijin");
                dic.Add("备注", "mem_GongjijinSub");
                dic.Add("预发奖金", "mem_Yufa");
                dic.Add("签订时间", "mem_CoperationDate2");
                dic.Add("合同性质", "mem_CoperationTypeName2");
                dic.Add("到期时间", "mem_CoperationDateEnd2");
                dic.Add("年薪(万)", "mem_YearCharge");

                dic.Add("离职情况", "mem_OutCompany");
                dic.Add("离职时间", "mem_OutCompanyDate");
                dic.Add("离职性质", "mem_OutCompanyTypeName");
                dic.Add("离职原因", "mem_OutCompanySub");


                foreach (KeyValuePair<string, string> item in dic)
                {
                    sb.AppendFormat("<label><input type='checkbox' value='{0}' />{1}</label>", item.Value, item.Key);
                }

                return sb.ToString();
            }
        }

        public string asTreeviewdrpunitObjID
        {
            get
            {
                return this.drp_unit.GetClientTreeObjectId();
            }
        }

        public string asTreeviewdrpmemObjID
        {
            get
            {
                return this.astvMyTree.GetClientTreeObjectId();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadUnitTree();
                InitTree();
            }
        }
        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drp_unit.Theme = macOS;
            this.astvMyTree.Theme = macOS;
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = " 1=1 ";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }
        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "全部部门");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString().Trim(), dr["unit_Name"].ToString().Trim());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
        }

        private const string folderIcon = "../js/astreeview/astreeview/images/user_group2.png";

        private const string fileIcon = "../js/astreeview/astreeview/images/user_male.png";

        private const string rootIcon = "../js/astreeview/astreeview/images/user_part2.png";
        /// <summary>
        /// 是否单选
        /// </summary>
        public bool IsRadio { get; set; }
        private void InitTree()
        {
            List<TreeDepartmentEntity> treeDepartmentList = new TG.BLL.cm_ProjectPlanBP().GetTreeDepartmentList();

            ASTreeViewNode rootNode = new ASTreeViewNode("人员选择");
            rootNode.NodeIcon = rootIcon;
            rootNode.EnableCheckbox = false;
            if (IsRadio)
            {
                rootNode.EnableCheckbox = false;
            }
            this.astvMyTree.RootNode.AppendChild(rootNode);

            treeDepartmentList.ForEach((department) =>
            {
                ASTreeViewNode departmentNode = new ASTreeViewNode(department.DepartmentName, department.DepartmentSysNo.ToString(), folderIcon);
                departmentNode.OpenState = ASTreeViewNodeOpenState.Close;
                departmentNode.EnableCheckbox = false;
                if (IsRadio)
                {
                    departmentNode.EnableCheckbox = false;
                }
                rootNode.AppendChild(departmentNode);

                department.Users.ForEach((user) =>
                {
                    string userfileicon = fileIcon;
                    if (user.UserSex == "女")
                    {
                        userfileicon = "../js/astreeview/astreeview/images/user_female.png";
                    }
                    string specialtyName = "[" + user.SpecialtyName + "]";
                    ASTreeViewNode userNode = new ASTreeViewNode(user.UserName + specialtyName, user.UserSysNo.ToString(), userfileicon);

                    List<KeyValuePair<string, string>> customerAttributesList = new List<KeyValuePair<string, string>>
                                        {
                                                new KeyValuePair<string, string>("lastLeveFlag","true"),
                                                new KeyValuePair<string, string>("userSysNo",user.UserSysNo.ToString()),
                                                new KeyValuePair<string, string>("userName", user.UserName),
                                                new KeyValuePair<string, string>("specialtyno", user.SpecialtyNo.ToString()),
                                                new KeyValuePair<string, string>("specialtyName", user.SpecialtyName),
                                                new KeyValuePair<string, string>("departmentName", department.DepartmentName),
                                                new KeyValuePair<string, string>("userSex", user.UserSex)
                                        };
                    userNode.AdditionalAttributes.AddRange(customerAttributesList);
                    departmentNode.AppendChild(userNode);
                });
            });
        }
    }
}