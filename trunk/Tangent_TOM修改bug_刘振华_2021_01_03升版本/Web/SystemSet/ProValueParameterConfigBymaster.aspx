﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="ProValueParameterConfigBymaster.aspx.cs" Inherits="TG.Web.SystemSet.ProValueParameterConfigBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/SystemSet/ProValueParameterConfig.js"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Global.js"></script>

    <style type="text/css">
        .cls_content_head {
            width: 100%;
            margin: 0 auto;
            font-size: 12px;
            border-collapse: collapse;
            font-family: 微软雅黑;
            margin-top: 2px;
            border-bottom: none;
        }

            .cls_content_head td {
                height: 24px;
                font-weight: bold;
                border: 1px solid Gray;
            }

        .gridView_comm {
            margin: 0 auto;
            border-collapse: collapse;
            border-top: none;
        }

            .gridView_comm td {
                height: 24px;
                border-collapse: collapse;
                border: 1px solid Gray;
                border-top: none;
                text-align: center;
            }

        .cls_input_text {
            height: 30px;
        }

        legend {
            font-size: 12px;
            width: 100%;
        }

        fieldset {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>项目产值分配细则配置</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用管理</a><i class="fa fa-angle-right"> </i><a>项目产值分配细则配置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>项目产值分配细则配置
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <fieldset>
                        <legend>项目阶段产值分配比例</legend>
                        <select id="stage">
                            <option value="0">方案+初设+施工图+后期</option>
                            <option value="1">方案+初设</option>
                            <option value="10">方案+施工图+后期</option>
                            <option value="2">施工图+后期</option>
                            <option value="3">初设+施工图+后期</option>
                            <option value="4">室外工程</option>
                            <option value="5">锅炉房</option>
                            <option value="6">地上单建水泵房</option>
                            <option value="7">地上单建变配电所（室） </option>
                            <option value="8">单建地下室(车库) </option>
                            <%-- <option value="9">市政道路工程 </option>--%>
                        </select>
                        <div style="width: 100%">
                            <div id="divone">
                                <table class="cls_content_head">
                                    <tr>
                                        <td colspan="6" style="width: 100%; text-align: center; background-color: White;">项目各设计阶段产值分配比例%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 16%; text-align: center;">项目类别
                                        </td>
                                        <td style="width: 16%; text-align: center;">方案设计
                                        </td>
                                        <td style="width: 16%; text-align: center;">初步设计
                                        </td>
                                        <td style="width: 16%; text-align: center;">施工图设计
                                        </td>
                                        <td style="width: 16%; text-align: center;">后期服务
                                        </td>
                                        <td style="width: 10%; text-align: center;">编辑
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="gvOne" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                    CssClass="gridView_comm" Width="100%" EnableModelValidation="True">
                                    <Columns>
                                        <asp:BoundField DataField="ItemType">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProgramPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="preliminaryPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="WorkDrawPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LateStagePercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="#" class="cls_select">编辑</a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ID">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProjectStatues">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有信息!
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <div class="cls_data" id="divOneEdit" style="display: none">
                                    <fieldset style="font-size: 12px;">
                                        <legend>编辑(方案+初设+施工图+后期)各级阶段产值分配</legend>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 100px;">项目类别
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblOneType" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>方案设计：
                                                </td>
                                                <td>
                                                    <input id="txtOneProgram" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanOneProgram">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>初步设计：
                                                </td>
                                                <td>
                                                    <input id="txtOnepreliminary" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanpreliminary">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>施工图设计：
                                                </td>
                                                <td>
                                                    <input id="txtOneWorkDraw" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanWorkDraw">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>后期服务：
                                                </td>
                                                <td>
                                                    <input id="txtLateStage" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanLateStage">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input id="hidOneStatus" type="hidden" />
                                                    <input id="hidOneID" type="hidden" />
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <img src="../Images/buttons/btn_ok.gif" id="btnOneSave" style="cursor: pointer">
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                            <div id="divTwo" style="display: none">
                                <table class="cls_content_head">
                                    <tr>
                                        <td colspan="4" style="width: 100%; text-align: center; background-color: White;">项目(方案+初步设计)二阶段产值分配比例%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 25%; text-align: center;">项目类别
                                        </td>
                                        <td style="width: 25%; text-align: center;">方案
                                        </td>
                                        <td style="width: 25%; text-align: center;">初设
                                        </td>
                                        <td style="width: 25%; text-align: center;">编辑
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="gvTwo" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                    CssClass="gridView_comm" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="ItemType">
                                            <ItemStyle Width="25%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProgramPercent">
                                            <ItemStyle Width="25%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="preliminaryPercent">
                                            <ItemStyle Width="25%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="#" class="cls_Twoselect">编辑</a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="25%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ID">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProjectStatues">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有信息!
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <div class="cls_data" id="divTwoEdit" style="display: none">
                                    <fieldset style="font-size: 12px;">
                                        <legend>编辑(方案+初步设计)二级阶段产值分配</legend>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 100px;">项目类别
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblTwoType" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>方案设计：
                                                </td>
                                                <td>
                                                    <input id="txtTwoProgram" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanTwoProgram">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>初步设计：
                                                </td>
                                                <td>
                                                    <input id="txtTwopreliminary" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanTwopreliminary">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                    <input id="hidTwoStatus" type="hidden" />
                                                    <input id="hidTwoID" type="hidden" />
                                                </td>
                                                <td>
                                                    <img src="../Images/buttons/btn_ok.gif" id="btnTwoSave" style="cursor: pointer">
                                                    <%--  <input type="button" id="btnTwoSave" name="controlBtn" style="background-image: url(../Images/buttons/btn_ok.gif);
                                        width: 50px; height: 23px; border: none; cursor: pointer" />--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                            <div id="divEleven">
                                <table class="cls_content_head">
                                    <tr>
                                        <td colspan="5" style="width: 100%; text-align: center; background-color: White;">项目(方案设计+施工图设计+后期服务)三阶段产值分配比例%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;">项目类别
                                        </td>
                                        <td style="width: 20%; text-align: center;">方案
                                        </td>
                                        <td style="width: 20%; text-align: center;">施工图
                                        </td>
                                        <td style="width: 20%; text-align: center;">后期
                                        </td>
                                        <td style="width: 20%; text-align: center;">编辑
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="gv_Eleven" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                    CssClass="gridView_comm" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="ItemType">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProgramPercent">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="WorkDrawPercent">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LateStagePercent">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="#" class="cls_elevenselect">编辑</a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="20%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ID">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProjectStatues">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有信息!
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <div class="cls_data" id="divElevenEdit" style="display: none">
                                    <fieldset style="font-size: 12px;">
                                        <legend>编辑(方案设计+施工图设计+后期服务)三级阶段产值分配</legend>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 100px;">项目类别
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblElevenType" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>方案设计：
                                                </td>
                                                <td>
                                                    <input id="txtElevenProgramPercent" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanElevenProgramPercent">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>施工图设计：
                                                </td>
                                                <td>
                                                    <input id="txtElevenWorkDraw" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanElevenWorkDraw">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>后期服务：
                                                </td>
                                                <td>
                                                    <input id="txtElevenLateStage" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanElevenLateStage">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                    <input id="hidElevenStatus" type="hidden" />
                                                    <input id="hidElevenID" type="hidden" />
                                                </td>
                                                <td>
                                                    <img src="../Images/buttons/btn_ok.gif" id="btnElevenSave" style="cursor: pointer">
                                                    <%-- <input type="button" id="btnFourSave" name="controlBtn" style="background-image: url(../Images/buttons/btn_ok.gif);
                                        width: 50px; height: 23px; border: none; cursor: pointer" />--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                            <div id="divThree" style="display: none">
                                <table class="cls_content_head">
                                    <tr>
                                        <td colspan="4" style="width: 100%; text-align: center; background-color: White;">项目(施工图设计+后期服务)二阶段产值分配比例%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 25%; text-align: center;">项目类别
                                        </td>
                                        <td style="width: 25%; text-align: center;">施工图
                                        </td>
                                        <td style="width: 25%; text-align: center;">后期
                                        </td>
                                        <td style="width: 25%; text-align: center;">编辑
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="gvThree" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                    CssClass="gridView_comm" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="ItemType">
                                            <ItemStyle Width="25%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="WorkDrawPercent">
                                            <ItemStyle Width="25%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LateStagePercent">
                                            <ItemStyle Width="25%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="#" class="cls_Threeselect">编辑</a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="25%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ID">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProjectStatues">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有信息!
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <div class="cls_data" id="divThreeEdit" style="display: none">
                                    <fieldset style="font-size: 12px;">
                                        <legend>编辑(施工图设计+后期服务)二级阶段产值分配</legend>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 100px;">项目类别
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblThreeType" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>施工图设计：
                                                </td>
                                                <td>
                                                    <input id="txtThreeWorkDrawPercent" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanThreeWorkDrawPercent">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>后期服务：
                                                </td>
                                                <td>
                                                    <input id="txtThreeLateStagePercent" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanThreeLateStagePercent">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                    <input id="hidThreeStatus" type="hidden" />
                                                    <input id="hidThreeID" type="hidden" />
                                                </td>
                                                <td>
                                                    <img src="../Images/buttons/btn_ok.gif" id="btnThreeSave" style="cursor: pointer">
                                                    <%--  <input type="button" id="btnThreeSave" name="controlBtn" style="background-image: url(../Images/buttons/btn_ok.gif);
                                        width: 50px; height: 23px; border: none; cursor: pointer" />--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                            <div id="divFour" style="display: none">
                                <table class="cls_content_head">
                                    <tr>
                                        <td colspan="5" style="width: 100%; text-align: center; background-color: White;">项目(初步设计+施工图设计+后期服务)三阶段产值分配比例%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;">项目类别
                                        </td>
                                        <td style="width: 20%; text-align: center;">初设
                                        </td>
                                        <td style="width: 20%; text-align: center;">施工图
                                        </td>
                                        <td style="width: 20%; text-align: center;">后期
                                        </td>
                                        <td style="width: 20%; text-align: center;">编辑
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="gvFour" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                    CssClass="gridView_comm" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="ItemType">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="preliminaryPercent">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="WorkDrawPercent">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LateStagePercent">
                                            <ItemStyle Width="20%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="#" class="cls_fourselect">编辑</a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="20%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ID">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProjectStatues">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有信息!
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <div class="cls_data" id="divFourEdit" style="display: none">
                                    <fieldset style="font-size: 12px;">
                                        <legend>编辑(初步设计+施工图设计+后期服务)三级阶段产值分配</legend>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 100px;">项目类别
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblFourType" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>初步设计：
                                                </td>
                                                <td>
                                                    <input id="txtFourpreliminary" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanFourpreliminary">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>施工图设计：
                                                </td>
                                                <td>
                                                    <input id="txtFourWorkDraw" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanFourWorkDraw">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>后期服务：
                                                </td>
                                                <td>
                                                    <input id="txtFourLateStage" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                        style="color: red; display: none;" id="spanFourLateStage">请输入整数！</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                    <input id="hidFourStatus" type="hidden" />
                                                    <input id="hidFourID" type="hidden" />
                                                </td>
                                                <td>
                                                    <img src="../Images/buttons/btn_ok.gif" id="btnFourSave" style="cursor: pointer">
                                                    <%-- <input type="button" id="btnFourSave" name="controlBtn" style="background-image: url(../Images/buttons/btn_ok.gif);
                                        width: 50px; height: 23px; border: none; cursor: pointer" />--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </div>
                            </div>
                            <div id="divFive" style="display: none">
                                <asp:Literal ID="lbl_Five" runat="server"></asp:Literal>
                            </div>
                            <div id="divSix" style="display: none;">
                                <asp:Literal ID="lbl_Six" runat="server"></asp:Literal>
                            </div>
                            <div id="divSeven" style="display: none;">
                                <asp:Literal ID="lbl_Seven" runat="server"></asp:Literal>
                            </div>
                            <div id="divEight" style="display: none;">
                                <asp:Literal ID="lbl_Eight" runat="server"></asp:Literal>
                            </div>

                            <div id="divNine" style="display: none;">
                                <asp:Literal ID="lbl_Nine" runat="server"></asp:Literal>
                            </div>
                            <div id="divTen" style="display: none;">
                                <asp:Literal ID="lbl_Ten" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </fieldset>
                    <asp:Literal ID="lbl_ProjectStageSpe" runat="server"></asp:Literal>
                    <fieldset>
                        <legend>项目设计工序产值分配比例-列表</legend>
                        <select id="processStage">
                            <option value="0">方案设计</option>
                            <option value="1">初步设计</option>
                            <option value="2">施工图设计</option>
                            <option value="3">后期服务</option>
                            <option value="4">室外工程以及其他</option>
                        </select>
                        <div style="width: 100%">
                            <div id="processOne">
                                <table class="cls_content_head">
                                    <tr>
                                        <td colspan="6" style="width: 100%; text-align: center; background-color: White;">方案设计-- 项目设计工序产值分配比例%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 16%; text-align: center;">专业
                                        </td>
                                        <td style="width: 16%; text-align: center;">审核
                                        </td>
                                        <td style="width: 16%; text-align: center;">专业负责
                                        </td>
                                        <td style="width: 16%; text-align: center;">校对
                                        </td>
                                        <td style="width: 16%; text-align: center;">设计
                                        </td>
                                        <td style="width: 10%; text-align: center;">编辑
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="gvDesignProcess" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                    CssClass="gridView_comm" Width="100%" EnableModelValidation="True">
                                    <Columns>
                                        <asp:BoundField DataField="Specialty">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AuditPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SpecialtyHeadPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProofreadPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DesignPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="#" class="cls_Designselect">编辑</a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ID">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有信息!
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                            <div id="processTwo" style="display: none">
                                <table class="cls_content_head">
                                    <tr>
                                        <td colspan="6" style="width: 100%; text-align: center; background-color: White;">初步设计-- 项目设计工序产值分配比例%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 16%; text-align: center;">专业
                                        </td>
                                        <td style="width: 16%; text-align: center;">审核
                                        </td>
                                        <td style="width: 16%; text-align: center;">专业负责
                                        </td>
                                        <td style="width: 16%; text-align: center;">校对
                                        </td>
                                        <td style="width: 16%; text-align: center;">设计
                                        </td>
                                        <td style="width: 10%; text-align: center;">编辑
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="gvDesignProcessTwo" runat="server" AutoGenerateColumns="False"
                                    ShowHeader="False" CssClass="gridView_comm" Width="100%" EnableModelValidation="True">
                                    <Columns>
                                        <asp:BoundField DataField="Specialty">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AuditPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SpecialtyHeadPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProofreadPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DesignPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="#" class="cls_Designselect">编辑</a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ID">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有信息!
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                            <div id="processThree" style="display: none">
                                <table class="cls_content_head">
                                    <tr>
                                        <td colspan="6" style="width: 100%; text-align: center; background-color: White;">施工图设计-- 项目设计工序产值分配比例%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 16%; text-align: center;">专业
                                        </td>
                                        <td style="width: 16%; text-align: center;">审核
                                        </td>
                                        <td style="width: 16%; text-align: center;">专业负责
                                        </td>
                                        <td style="width: 16%; text-align: center;">校对
                                        </td>
                                        <td style="width: 16%; text-align: center;">设计
                                        </td>
                                        <td style="width: 10%; text-align: center;">编辑
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="gvDesignProcessThree" runat="server" AutoGenerateColumns="False"
                                    ShowHeader="False" CssClass="gridView_comm" Width="100%" EnableModelValidation="True">
                                    <Columns>
                                        <asp:BoundField DataField="Specialty">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AuditPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SpecialtyHeadPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProofreadPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DesignPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="#" class="cls_Designselect">编辑</a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ID">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有信息!
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                            <div id="processFour" style="display: none">
                                <table class="cls_content_head">
                                    <tr>
                                        <td colspan="6" style="width: 100%; text-align: center; background-color: White;">后期服务-- 项目设计工序产值分配比例%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 16%; text-align: center;">专业
                                        </td>
                                        <td style="width: 16%; text-align: center;">审核
                                        </td>
                                        <td style="width: 16%; text-align: center;">专业负责
                                        </td>
                                        <td style="width: 16%; text-align: center;">校对
                                        </td>
                                        <td style="width: 16%; text-align: center;">设计
                                        </td>
                                        <td style="width: 10%; text-align: center;">编辑
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="gvDesignProcessFour" runat="server" AutoGenerateColumns="False"
                                    ShowHeader="False" CssClass="gridView_comm" Width="100%" EnableModelValidation="True">
                                    <Columns>
                                        <asp:BoundField DataField="Specialty">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AuditPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SpecialtyHeadPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProofreadPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DesignPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="#" class="cls_Designselect">编辑</a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ID">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有信息!
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                            <div id="processFive" style="display: none">
                                <table class="cls_content_head">
                                    <tr>
                                        <td colspan="6" style="width: 100%; text-align: center; background-color: White;">室外工程以及其他-- 项目设计工序产值分配比例%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 16%; text-align: center;">专业
                                        </td>
                                        <td style="width: 16%; text-align: center;">审核
                                        </td>
                                        <td style="width: 16%; text-align: center;">专业负责
                                        </td>
                                        <td style="width: 16%; text-align: center;">校对
                                        </td>
                                        <td style="width: 16%; text-align: center;">设计
                                        </td>
                                        <td style="width: 10%; text-align: center;">编辑
                                        </td>
                                    </tr>
                                </table>
                                <asp:GridView ID="gvDesignProcessFive" runat="server" AutoGenerateColumns="False"
                                    ShowHeader="False" CssClass="gridView_comm" Width="100%" EnableModelValidation="True">
                                    <Columns>
                                        <asp:BoundField DataField="Specialty">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AuditPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SpecialtyHeadPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProofreadPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DesignPercent">
                                            <ItemStyle Width="16%" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <a href="#" class="cls_Designselect">编辑</a>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ID">
                                            <ItemStyle CssClass="display" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        没有信息!
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="cls_data" id="divDesignEdit" style="display: none">
                            <fieldset style="font-size: 12px;">
                                <legend>项目设计工序产值分配比例-编辑</legend>
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 100px;">专业：
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSpecialty" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>审核：
                                        </td>
                                        <td>
                                            <input id="txtAuditPercent" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                style="color: red; display: none;" id="spanAuditPercent">请输入整数！</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>专业负责：
                                        </td>
                                        <td>
                                            <input id="txtSpecialtyHeadPercent" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                style="color: red; display: none;" id="spanSpecialtyHeadPercent">请输入整数！</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>校对：
                                        </td>
                                        <td>
                                            <input id="txtProofreadPercent" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                style="color: red; display: none;" id="spanProofreadPercent">请输入整数！</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>设计：
                                        </td>
                                        <td>
                                            <input id="txtDesignPercent" maxlength="10" type="text" class="cls_input_text" />(%)<span
                                                style="color: red; display: none;" id="spanDesignPercent">请输入整数！</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                            <input id="hidDesignProcessID" type="hidden" />
                                        </td>
                                        <td>
                                            <img src="../Images/buttons/btn_ok.gif" id="btnDesignSave" style="cursor: pointer">
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
