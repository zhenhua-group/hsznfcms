﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="SysUserInfoList.aspx.cs" Inherits="TG.Web.SystemSet.SysUserInfoList" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />

    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />

    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>

    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>

    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>

    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>

    <script type="text/javascript" src="../js/jquery.tablesort.js"></script>
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>

    <script type="text/javascript" src="../js/SystemSet/SysUserInfo_jq.js"></script>
    <script type="text/javascript" src="../js/SystemSet/SysUserInfo_Sch.js"></script>

    <script>
      
        function IsStructCheckNode(obj) {
            nodecount = 0;
            nodeLenght="";    
            <%= asTreeviewdrpunitObjID %>.traverseTreeNode(displayNodeFun);
            <%= asTreeviewdrpunitObjID %>.traverseTreeNode(displayNodeFuns);            
            return nodeLenght;
        }
        //选中与半选中  qpl 20140115
        function displayNodeFun(elem) {
            if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
                nodecount++;
            }
        }
        function displayNodeFuns(elem) {
            if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
                nodeLenght+=  "'"+elem.getAttribute("treenodevalue")+"',";
            }
        }

        function IsStructCheckNodeMem(obj) {
            nodecount = 0;
            nodeLenght="";    
            <%= asTreeviewdrpmemObjID %>.traverseTreeNode(displayNodeFunMem);
            <%= asTreeviewdrpmemObjID %>.traverseTreeNode(displayNodeFunsMem);            
            return nodeLenght;
        }
        //选中与半选中  qpl 20140115
        function displayNodeFunMem(elem) {
            if (elem.getAttribute("checkedState") == "0") {
                nodecount++;
            }
        }
        function displayNodeFunsMem(elem) {
            if (elem.getAttribute("checkedState") == "0") {
                nodeLenght+=  ""+elem.getAttribute("treenodevalue")+",";
            }
        }
    </script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">人力管理 <small>人力信息列表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">系统设置</a> <i class="fa fa-angle-right"></i><a>机构设置</a><i class="fa fa-angle-right"> </i><a>人力管理</a><i class="fa fa-angle-right"> </i><a>人力信息列表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12">
            <!--- 查询表单-->
            <div class="portlet  box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>查询人员信息
                    </div>
                    <div class="actions"></div>
                    <div class="actions">
                        <div class="btn-group" id="selectChoose">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择查询字段
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="chkSchColums">
                                <label>
                                    <input type="checkbox" name="name" value="0" id="chkAll" />全选</label>


                                <%--<label>
                                    <input type="checkbox" name="name" value="0" id="mem_Login" />员工账号</label>
                                <label>
                                    <input type="checkbox" name="name" value="0" id="mem_Name" />姓名</label>
                                <label>
                                    <input type="checkbox" name="name" value="0" id="memUnitName" />部门</label>--%>


                                <label>
                                    <input type="checkbox" name="name" value="0" id="mem_Sex" />性别</label>
                                <label>
                                    <input type="checkbox" name="name" value="0" id="mem_Birthday" />生日</label>
                                <label>
                                    <input type="checkbox" name="name" value="0" id="memSpeName" />专业</label>
                                <%--<label>
                                    <input type="checkbox" name="name" value="0" id="memPriName" />权限</label>--%>
                                <label>
                                    <input type="checkbox" name="name" value="0" id="isFiredName" />在职状态</label>
                                <label>
                                    <input type="checkbox" name="name" value="0" id="RoleMagName" />管理职位</label>
                                <label>
                                    <input type="checkbox" name="name" value="0" id="RoleTecName" />技术职位</label>
                                <label>
                                    <input type="checkbox" name="name" value="0" id="mem_ArchReg" />职称评定</label>
                                <label>
                                    <input type="checkbox" name="name" value="0" id="ArchLevelName" />注册级别</label>
                                <label>
                                    <input type="checkbox" name="name" value="0" id="mem_Code" />身份证号</label>
                                <label>
                                    <input type="checkbox" name="name" value="0" id="mem_Age" />年龄</label>
                                <label>
                                    <input type="checkbox" name="name" value="0" id="mem_CoperationDate" />合同日期</label>
                            </div>

                            <a href="ImportUserInfoList.aspx" class="btn btn-sm red">批量导入</a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">

                    <div class="table-responsive">
                        <!--基本查询-->
                        <table class="table table-hover table-bordered" id="tbTableColumns">
                            <tbody>
                                <tr>
                                    <td style="width: 150px;">员工账号</td>
                                    <td style="width: 200px;">
                                        <input type="text" name="name" value="" id="txt_mem_Login" /></td>
                                    <td style="width: 150px;">姓名</td>
                                    <td style="width: 200px;">
                                        <input type="text" name="name" value="" id="txt_mem_Name" />
                                    </td>
                                    <td style="width: 150px;">部门</td>
                                    <td>
                                        <%--<select style="width: 100px;" id="txt_memUnitName">
                                            <option value="-1">---全部---</option>
                                            <% UnitList.ForEach(c =>
                                       { %>
                                            <option value="<%=c.unit_Name%>"><%=c.unit_Name%></option>
                                            <%}); %>
                                        </select>--%>
                                        <table>
                                            <tr>
                                                <td>
                                                    <cc1:ASDropDownTreeView ID="drp_unit" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------全部部门-------" />
                                                </td>
                                                <td>
                                                    <cc1:ASDropDownTreeView ID="astvMyTree" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------人员选择-------" />
                                                </td>
                                            </tr>
                                        </table>

                                    </td>

                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7" style="text-align: center;"><a href="#" class="btn blue" id="btnSearch">查询</a></td>
                                </tr>

                            </tfoot>
                        </table>

                        <!--查询待选项-->
                        <table class="table table-hover table-bordered" id="tbTableColumnItems" style="display: none;">
                            <tbody>
                                <tr for="mem_Sex">
                                    <td for="mem_Sex">性别</td>
                                    <td>
                                        <select id="txt_mem_Sex">
                                            <option value="-1">-全部-</option>
                                            <option value="男">男</option>
                                            <option value="女">女</option>
                                        </select>
                                    </td>

                                </tr>
                                <tr for="mem_Birthday">
                                    <td for="mem_Birthday">生日</td>
                                    <td>
                                        <input class="Wdate" onclick="WdatePicker({ readOnly: true })" type="text" name="name" value="" id="txt_mem_Birthday" />
                                    </td>
                                </tr>
                                <tr for="memSpeName">
                                    <td for="memSpeName">专业</td>
                                    <td>
                                        <select style="width: 100px;" id="txt_memSpeName">
                                            <option value="-1">---全部---</option>
                                            <% SpecList.ForEach(c =>
                                       { %>
                                            <option value="<%=c.spe_Name %>"><%=c.spe_Name %></option>
                                            <%}); %>
                                        </select>
                                    </td>

                                </tr>
                                <tr for="isFiredName">
                                    <td for="isFiredName">在职状态</td>
                                    <td>
                                        <select id="txt_isFiredName">
                                            <option value="-1">-全部-</option>
                                            <option value="在职" selected>在职</option>
                                            <option value="离职">离职</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr for="RoleMagName">
                                    <td for="RoleMagName">管理职位</td>
                                    <td>
                                        <select style="width: 100px;" id="txt_RoleMagName">
                                            <option value="-1">---全部---</option>
                                            <% RoleMag.ForEach(c =>
                                       { %>
                                            <option value="<%=c.ID %>"><%=c.RoleName %></option>
                                            <%}); %>
                                        </select></td>
                                </tr>
                                <tr for="RoleTecName">
                                    <td for="RoleTecName">技术职位</td>
                                    <td>
                                        <select style="width: 100px;" id="txt_RoleTecName">
                                            <option value="-1">---全部---</option>
                                            <% RoleTec.ForEach(c =>
                                       { %>
                                            <option value="<%=c.ID %>"><%=c.RoleName %></option>
                                            <%}); %>
                                        </select></td>

                                </tr>
                                <tr for="mem_ArchReg">

                                    <td for="mem_ArchReg">职称评定</td>
                                    <td>
                                        <select style="width: 100px;" id="txt_mem_ArchReg">
                                            <option value="-1">---请选择---</option>
                                            <% Profesion.ForEach(c =>
                                       { %>
                                            <option value="<%=c.ID %>"><%=c.Name %></option>
                                            <%}); %>
                                        </select>
                                    </td>
                                </tr>
                                <tr for="ArchLevelName">
                                    <td for="ArchLevelName">注册</td>
                                    <td>
                                        <select style="width: 220px;" id="txt_ArchLevelName">
                                            <option value="-1">---请选择---</option>

                                            <% ArchLevel.ForEach(c =>
                                       { %>
                                            <option value="<%=c.ArchLevel_ID %>"><%=c.Name %></option>
                                            <%}); %>
                                        </select></td>
                                </tr>
                                <tr for="mem_Code">
                                    <td for="mem_Code">身份证号</td>
                                    <td>
                                        <input type="text" name="name" value="" id="txt_mem_Code" /></td>
                                </tr>
                                <tr for="mem_Age">
                                    <td for="mem_Age">年龄</td>
                                    <td>
                                        <input type="text" name="name" value="" id="txt_mem_Age" /></td>
                                </tr>
                                <tr for="mem_CoperationDate">
                                    <td for="mem_CoperationDate">合同日期</td>
                                    <td>
                                        <input class="Wdate" onclick="WdatePicker({ readOnly: true })" type="text" name="name" value="" id="txt_memCode" /></td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!--列表信息-->
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>人员信息列表
                    </div>
                    <div class="actions">
                        <div class="btn-group" id="choose">
                            <a target="_blank" href="EditSysUserInfo.aspx" class="btn btn-sm red">新建账户</a>
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择显示列
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsuserid">
                                <%= ColumnsStr %>
                            </div>

                            <a href="#" id="btnExportExcel" class="btn btn-sm red">导出Excel</a>

                        </div>

                    </div>

                </div>
                <div class="portlet-body form" style="display: block;">

                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab_1_1" style="width: 100%">
                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                                没有符合条件数据！
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="name" value=" AND isFiredName='在职'" id="hidWhere" />


    <script>
        sysUserInfoList.init();

        SysUserInfoSch.init();
    </script>
</asp:Content>
