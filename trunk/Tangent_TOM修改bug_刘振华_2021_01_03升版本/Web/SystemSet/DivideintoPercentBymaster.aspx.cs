﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;

namespace TG.Web.SystemSet
{
    public partial class DivideintoPercentBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //初始分配年
                InitAllotYear();
                //绑定生产部门
                BindUnit();
                //绑定
                bindData();
            }
        }
        //是否需要检查权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //绑定目标值
        protected void bindData()
        {
            TG.BLL.cm_Divideinfo bll = new TG.BLL.cm_Divideinfo();
            string strWhere = "";

            if (this.drp_unit.SelectedIndex != 0)
            {
                strWhere = strWhere + "  AND UnitID=" + this.drp_unit.SelectedItem.Value + "";
            }
            if (this.drp_year.SelectedIndex != 0)
            {
                strWhere = strWhere + "  AND DivideYear=" + this.drp_year.SelectedItem.Value + "";
            }
            //
            this.AspNetPager1.RecordCount = int.Parse(bll.GetListPageProcCount(strWhere).ToString());
            //绑定
            this.grid_cost.DataSource = bll.GetListByPageProc(strWhere, this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            this.grid_cost.DataBind();
        }
        //初始当前年份
        protected void InitAllotYear()
        {
            string curYear = DateTime.Now.Year.ToString();
            this.drp_allotyear.Items.FindByText(curYear).Selected = true;
            this.drp_year.Items.FindByText(curYear).Selected = true;
        }
        //绑定生产部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = " 1=1 ";
            //不显示的单位
            //不显示的单位
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            else
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            DataSet ds = bll_unit.GetList(strWhere);
            this.drp_unit.DataSource = ds;
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();

            this.drp_unit0.DataSource = ds;
            this.drp_unit0.DataTextField = "unit_Name";
            this.drp_unit0.DataValueField = "unit_ID";
            this.drp_unit0.DataBind();

            this.drp_unit1.DataSource = ds;
            this.drp_unit1.DataTextField = "unit_Name";
            this.drp_unit1.DataValueField = "unit_ID";
            this.drp_unit1.DataBind();
        }
        //查询目标值
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            this.AspNetPager1.CurrentPageIndex = 1;
            bindData();
        }
        //删除目标值
        protected void btn_DelCst_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_Divideinfo bll = new TG.BLL.cm_Divideinfo();
            for (int i = 0; i < this.grid_cost.Rows.Count; i++)
            {
                CheckBox chk = this.grid_cost.Rows[i].Cells[0].FindControl("chk_id") as CheckBox;
                HiddenField hid_id = this.grid_cost.Rows[i].Cells[1].FindControl("hid_id") as HiddenField;
                string id = hid_id.Value;
                if (chk.Checked)
                {
                    bll.Delete(int.Parse(id));
                }
            }

            TG.Common.MessageBox.ShowAndRedirect(this.Page, "院所分成比例删除成功！", "DivideintoPercentBymaster.aspx");
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            bindData();
        }
        //保存
        protected void btn_save_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_Divideinfo bll = new TG.BLL.cm_Divideinfo();
            TG.Model.cm_Divideinfo model = new TG.Model.cm_Divideinfo();
            //检查当前年份是否已经添加了目标值

            //赋值
            model.UnitID = int.Parse(this.drp_unit0.SelectedValue);
            model.DividePercent = Convert.ToDecimal(this.txt_allot.Text);
            model.DivideYear = this.drp_allotyear.SelectedItem.Text;
            model.InsertUserID = UserSysNo;

            if (bll.Add(model) > 0)
            {
                MessageBox.ShowAndRedirect(this, this.drp_unit0.SelectedItem.Text.Trim() + this.drp_allotyear.SelectedItem.Text + "年的院所分成比例添加成功！","DivideintoPercentBymaster.aspx");
            }
        }
        //编辑
        protected void btn_edit_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_Divideinfo bll = new TG.BLL.cm_Divideinfo();
            TG.Model.cm_Divideinfo model = new TG.Model.cm_Divideinfo();
            //赋值
            model.ID = int.Parse(this.hid_id.Value);
            model.UnitID = int.Parse(this.drp_unit1.SelectedValue);
            model.DivideYear = this.drp_allotyear0.SelectedValue;
            model.DividePercent = Convert.ToDecimal(this.txt_allot0.Text);

            if (bll.Update(model))
            {
                MessageBox.ShowAndRedirect(this, "院所分成比例修改成功！", "DivideintoPercentBymaster.aspx");
            }
        }
    }
}