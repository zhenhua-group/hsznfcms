﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="PromapShowBymaster.aspx.cs" Inherits="TG.Web.SystemSet.PromapShowBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery.alerts.css" rel="stylesheet" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            $('#ctl00_ContentPlaceHolder1_gv_Coperation_ctl01_chkAll').click(function () {

                var checks = $('#ctl00_ContentPlaceHolder1_gv_Coperation :checkbox');

                if ($('#ctl00_ContentPlaceHolder1_gv_Coperation_ctl01_chkAll').attr("checked") == "checked") {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.setAttribute("class", "checked");
                    }
                }
                else {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.removeAttribute("class");

                    }
                }
            });



            //添加
            $("#btn_showadd").click(function () {
                window.setTimeout("scrollToBottom()", 500); //500毫秒延迟加载
                $("#div_add").show("slow");
                $("#div_edit").hide("slow");
            });

            //隐藏
            $(" .btn_cancel").click(function () {
                $("#div_add").hide("slow");
                $("#div_edit").hide("slow");
            });

            //新增保存
            $("#ctl00_ContentPlaceHolder1_btn_save").click(function () {
                var msg = "";
                if ($("#ctl00_ContentPlaceHolder1_txt_cityName").val() == "") {
                    msg += "城市名不能为空！<br/>";
                }
                else {
                    if ($("#ctl00_ContentPlaceHolder1_txt_toaspx").val().length > 25) {
                        msg += "城市页面太长！<br/>";
                    }
                }
                if (msg != "") {
                    jAlert(msg, "提示");
                    return false;
                }
            });

            //选择
            $(".choose").click(function () {
                var cityname = $(this).parent().parent().find("TD").eq(2).text();
                var linkaspx = $(this).parent().parent().find("TD").eq(3).text();
                $.get("../HttpHandler/ProMapshow.ashx", { "name": cityname, "aspx": linkaspx }, function (date) {
                    if (date == "ok") {
                        alert("修改成功地图显示为" + cityname + "地图");
                        $("#ctl00_ContentPlaceHolder1_lab").html(cityname);
                        window.location.reload();
                    }
                });
            });
            $(".cls_select").click(function () {
                $("#div_add").hide("slow");
                $("#div_edit").show("slow");
            });
            //编辑
            $(".cls_select").click(function () {
                window.setTimeout("scrollToBottom()", 500); //500毫秒延迟加载
                $("#ctl00_ContentPlaceHolder1_lbl_dicid").text($(this).parent().parent().find("TD").eq(1).text());
                $("#ctl00_ContentPlaceHolder1_hid_dicid").val($(this).parent().parent().find("TD").eq(1).text());
                $("#ctl00_ContentPlaceHolder1_txt_dicName0").val($(this).parent().parent().find("TD").eq(2).text());
                $("#ctl00_ContentPlaceHolder1_txt_dicType0").val($(this).parent().parent().find("TD").eq(3).text());
                $("#ctl00_ContentPlaceHolder1_txt_dicDescript").val($(this).parent().parent().find("TD").eq(4).text());
            });

            $("#ctl00_ContentPlaceHolder1_btn_edit").click(function () {
                var msg = "";
                if ($("#ctl00_ContentPlaceHolder1_txt_dicName0").val() == "") {
                    msg += "城市名不能为空！<br/>";
                }
                else {
                    if ($("#ctl00_ContentPlaceHolder1_txt_dicType0").val().length > 25) {
                        msg += "城市对应页面太长！<br/>";
                    }
                }
                if (msg != "") {
                    jAlert(msg, "提示");
                    return false;
                }
            });
            //删除 ctl00_ContentPlaceHolder1_gv_Coperation
            $("#ctl00_ContentPlaceHolder1_btn_DelCst").click(function () {
                if ($("#ctl00_ContentPlaceHolder1_gv_Coperation :checkbox:checked").length == 0) {
                    jAlert("请选择要删除的字典！", "提示");
                    $('#popup_ok').attr("class", "btn green");
                    return false;
                }
                //判断是否要删除
                return confirm("是否要删除字典信息？");
            });
        });
        function scrollToBottom() {
            window.scrollTo(0, document.body.scrollHeight); //移动屏幕最下方
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">系统设置 <small>项目分布地图配置 </small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页></a> <a>系统设置</a><i
        class="fa fa-angle-right"> </i><a>常用设置</a><i class="fa fa-angle-right"> </i><a>项目分布地图配置</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>城市查询
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table-responsive">
                    	<tr>
                    		<td>城市名称:</td>
                            <td><input id="txt_keyname" name="txt_keyname" type="text" runat="Server" class="form-control input-sm" /></td>
                            <td><asp:Button ID="btn_Search" runat="server" Text="查询" CssClass="btn blue" OnClick="btn_Search_Click" /></td>
                            <td><asp:DropDownList ID="drp_typelist" runat="server" AppendDataBoundItems="True" AutoPostBack="false"
                                    CssClass="form-control">
                                    <asp:ListItem Value="-1">----请选择----</asp:ListItem>
                                </asp:DropDownList></td>
                            <td><input type="button" name="name" value="添加" class="btn blue" id="btn_showadd" /></td>
                            <td><asp:Button ID="btn_DelCst" runat="server" Text="删除" CssClass="btn blue" OnClick="btn_DelCst_Click" /></td>
                            <td>当前地图为:<asp:Label ID="lab" runat="server"></asp:Label></td>
                    	</tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>配置信息
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView ID="gv_Coperation" runat="server" AutoGenerateColumns="False" ShowHeader="true"
                                CssClass="table table-striped table-bordered table-hover dataTable">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_id" runat="server" CssClass="cls_chk" />
                                            &nbsp;
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AreaId" HeaderText="城市编号">
                                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="areaName" HeaderText="城市名称">
                                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Toaspx" HeaderText="对应页面">
                                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="编辑">
                                        <ItemTemplate>
                                            <a href="javascript:;" class="choose">选择</a>|<a href="#" class="cls_select">编辑</a>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="100px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cls_data" id="div_add" style="display: none;">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>添加地图信息
                </div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"></a>
                </div>
            </div>
            <div class="portlet-body">
                <table border="0" cellspacing="0" cellpadding="0" width="500" align="center">
                    <tr>
                        <td>城市名称:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_cityName" runat="server" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>对应页面:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_toaspx" runat="server" CssClass="form-control "></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btn_save" runat="server" CssClass="btn green" Text="保存" OnClick="btn_save_Click" />
                            &nbsp;
                            <input type="button" name="btn_cancel" value="取消" class="btn  btn_cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="cls_data" id="div_edit" style="display: none;">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cogs"></i>编辑地图信息
                </div>
                <div class="tools">
                    <a class="collapse" href="javascript:;"></a>
                </div>
            </div>
            <div class="portlet-body">
                <table border="0" cellspacing="0" cellpadding="0" width="500" align="center">
                    <tr>
                        <td>ID:
                        </td>
                        <td>
                            <asp:Label ID="lbl_dicid" runat="server"></asp:Label>
                            <asp:HiddenField ID="hid_dicid" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>城市名称:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_dicName0" runat="server" CssClass="form-control "></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>对应页面:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_dicType0" runat="server" CssClass="form-control "></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btn_edit" runat="server" CssClass="btn green" Text="保存" OnClick="btn_edit_Click" />
                            &nbsp;
                            <input type="button" name="btn_cancel" value="取消" class="btn  btn_cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
