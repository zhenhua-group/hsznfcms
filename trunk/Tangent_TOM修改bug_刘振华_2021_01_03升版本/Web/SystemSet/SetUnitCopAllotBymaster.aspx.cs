﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Common;

namespace TG.Web.SystemSet
{
    public partial class SetUnitCopAllotBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //初始分配年
                InitAllotYear();
                //绑定生产部门
                BindUnit();
                //绑定
                BindUnitAllot();
            }
        }
        //是否需要检查权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //绑定目标值
        protected void BindUnitAllot()
        {
            TG.BLL.SetUnitCopAllot bll = new TG.BLL.SetUnitCopAllot();
            string strWhere = " AllotYear='" + this.drp_year.SelectedItem.Text + "'";
            if (this.drp_unit.SelectedIndex != 0)
            {
                strWhere = "UnitID=" + this.drp_unit.SelectedItem.Value + " AND AllotYear='" + this.drp_year.SelectedItem.Text + "'";
            }
            //绑定
            this.grid_mem.DataSource = bll.GetList(strWhere);
            this.grid_mem.DataBind();
        }
        //初始当前年份
        protected void InitAllotYear()
        {
            string curYear = DateTime.Now.Year.ToString();
            this.drp_allotyear.Items.FindByText(curYear).Selected = true;
            this.drp_year.Items.FindByText(curYear).Selected = true;
        }
        //绑定生产部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = " 1=1 ";
            //不显示的单位
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            else
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            DataSet ds = bll_unit.GetList(strWhere);
            this.drp_unit.DataSource = ds;
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();

            this.drp_unit0.DataSource = ds;
            this.drp_unit0.DataTextField = "unit_Name";
            this.drp_unit0.DataValueField = "unit_ID";
            this.drp_unit0.DataBind();

            this.drp_unit1.DataSource = ds;
            this.drp_unit1.DataTextField = "unit_Name";
            this.drp_unit1.DataValueField = "unit_ID";
            this.drp_unit1.DataBind();
        }
        //查询目标值
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindUnitAllot();
        }
        //删除目标值
        protected void btn_DelCst_Click(object sender, EventArgs e)
        {
            TG.BLL.SetUnitCopAllot bll = new TG.BLL.SetUnitCopAllot();
            for (int i = 0; i < this.grid_mem.Rows.Count; i++)
            {
                CheckBox chk = this.grid_mem.Rows[i].Cells[0].FindControl("chk_id") as CheckBox;
                HiddenField hid_id = this.grid_mem.Rows[i].Cells[1].FindControl("hid_id") as HiddenField;
                string id = hid_id.Value;
                if (chk.Checked)
                {
                    bll.Delete(int.Parse(id));
                }
            }

            //弹出提示
            TG.Common.MessageBox.Show(this.Page, "生产目标值删除成功！");
            //重新绑定
            BindUnitAllot();
        }
        //保存
        protected void btn_save_Click(object sender, EventArgs e)
        {
            TG.BLL.SetUnitCopAllot bll = new TG.BLL.SetUnitCopAllot();
            TG.Model.SetUnitCopAllot model = new TG.Model.SetUnitCopAllot();
            //检查当前年份是否已经添加了目标值
            string strWhere = " UnitID=" + this.drp_unit0.SelectedValue + " AND AllotYear='" + this.drp_allotyear.SelectedItem.Text.Trim() + "'";
            List<TG.Model.SetUnitCopAllot> list = bll.GetModelList(strWhere);
            if (list.Count == 0)
            {
                //赋值
                model.UnitID = int.Parse(this.drp_unit0.SelectedValue);
                model.UnitAllot = Convert.ToDecimal(this.txt_allot.Text);
                model.AllotYear = this.drp_allotyear.SelectedItem.Text;
                model.Status = 0;

                if (bll.Add(model) > 0)
                {
                    MessageBox.Show(this, this.drp_unit0.SelectedItem.Text.Trim() + this.drp_allotyear.SelectedItem.Text + "的目标产值添加成功！");
                }

                //绑定产值目标
                BindUnitAllot();
            }
            else
            {
                MessageBox.Show(this, this.drp_unit0.SelectedItem.Text.Trim() + this.drp_allotyear.SelectedItem.Text + "的目标产值已存在不能重复添加！");
            }
        }
        //编辑
        protected void btn_edit_Click(object sender,  EventArgs e)
        {
            TG.BLL.SetUnitCopAllot bll = new TG.BLL.SetUnitCopAllot();
            TG.Model.SetUnitCopAllot model = new TG.Model.SetUnitCopAllot();
            //赋值
            model.ID = int.Parse(this.hid_memid.Value);
            model.UnitID = int.Parse(this.hid_unitid.Value);
            model.UnitAllot = Convert.ToDecimal(this.txt_allot0.Text);
            model.AllotYear = this.hid_allotyear.Value;
            model.Status = 0;

            if (bll.Update(model))
            {
                MessageBox.Show(this, "目标产值修改成功！");
            }

            //绑定产值目标
            BindUnitAllot();
        }
        //绑定
        TG.BLL.tg_unit bll = new TG.BLL.tg_unit();
        protected void grid_mem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl_name = e.Row.Cells[2].FindControl("lbl_unitname") as Label;
                //查询单位名称
                string strWhere = " unit_ID=" + lbl_name.Text;
                DataSet ds_unit = bll.GetList(strWhere);
                if (ds_unit.Tables.Count > 0)
                {
                    if (ds_unit.Tables[0].Rows.Count > 0)
                    {
                        lbl_name.Text = ds_unit.Tables[0].Rows[0]["unit_Name"].ToString();
                    }
                }
            }
        }
    }
}