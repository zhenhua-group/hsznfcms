﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using TG.Model;
using TG.BLL;

namespace TG.Web.SystemSet
{
    public partial class LeftMenuConfigBymaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(LeftMenuConfigBymaster));

            if (!IsPostBack)
            {
                if (Request.HttpMethod == "GET")
                {
                    GetLeftMenuList();
                    GetRoleListAll();
                }
            }
        }
        /// <summary>
        /// 新规LeftMenu
        /// </summary>
        /// <param name="leftMenuString"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string InsertLeftMenu(string leftMenuString)
        {
            LeftMenuViewEntity leftMenuViewEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<LeftMenuViewEntity>(leftMenuString);

            return new LoadLeftMenuBP().SaveLeftMenu(leftMenuViewEntity) + "";
        }

        /// <summary>
        /// 根据SysNo得到一个LeftMenu
        /// </summary>
        /// <param name="leftMenuSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string GetLeftMenuViewEntity(int leftMenuSysNo)
        {
            LeftMenuViewEntity leftMenuViewEntity = new LoadLeftMenuBP().GetLeftMenuViewEntity(leftMenuSysNo);

            return Newtonsoft.Json.JsonConvert.SerializeObject(leftMenuViewEntity);
        }

        /// <summary>
        /// 修改LeftMenu
        /// </summary>
        /// <returns></returns>
        [AjaxMethod]
        public string UpdateLeftMenu(string leftMenuString)
        {
            LeftMenuViewEntity leftMenuViewEntity = Newtonsoft.Json.JsonConvert.DeserializeObject<LeftMenuViewEntity>(leftMenuString);

            return new LoadLeftMenuBP().UpDateLeftMenu(leftMenuViewEntity).ToString();
        }

        /// <summary>
        /// 删除LeftMenu
        /// </summary>
        /// <param name="leftMenuSysNo"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string DeleteLeftMenu(int leftMenuSysNo)
        {
            return new LoadLeftMenuBP().DeleteLeftMenu(leftMenuSysNo).ToString();
        }

        private void GetLeftMenuList()
        {
            List<LeftMenuViewEntity> sourceList = new LoadLeftMenuBP().GetLeftMenuListAll(LeftMenuType.All);
            this.RepeaterLeftMenu.DataSource = sourceList;
            this.RepeaterLeftMenu.DataBind();
        }

        protected void TypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LeftMenuType leftType = (LeftMenuType)int.Parse(this.TypeDropDownList.SelectedValue);

            List<LeftMenuViewEntity> sourceList = new LoadLeftMenuBP().GetLeftMenuListAll(leftType);
            this.RepeaterLeftMenu.DataSource = sourceList;
            this.RepeaterLeftMenu.DataBind();
        }

        private void GetRoleListAll()
        {
            List<TG.Model.cm_Role> roleList = new TG.BLL.cm_Role().GetRoleList();
            this.RepeaterRole.DataSource = roleList;
            this.RepeaterRole.DataBind();
        }
    }
}