﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class Sys_UnitListByMaster : System.Web.UI.Page
    {
        TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindUnit();
            }
        }
        protected void BindUnit()
        {
            StringBuilder sb = new StringBuilder("");
            string strwhere = "";
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.AppendFormat("  A.unit_Name Like '%{0}%' ", keyname);
                strwhere = " and A.unit_Name LIKE '%" + keyname + "%'";
            }
            //角色数量
            this.AspNetPager1.RecordCount = int.Parse(bll_unit.GetRecordCount(sb.ToString()).ToString());
            //绑定
            DataSet ds_unit = bll_unit.GetListByPage(sb.ToString(), "B.unit_Order", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize, true);
            this.grid_unit.DataSource = ds_unit;
            this.grid_unit.DataBind();

            this.hid_where.Value = strwhere;

            //DataSet ds_unit = bll_unit.GetList(sb.ToString());

            //绑定部门
            this.drp_unitlist.DataSource = ds_unit;
            this.drp_unitlist.DataTextField = "unit_Name";
            this.drp_unitlist.DataValueField = "unit_ID";
            this.drp_unitlist.DataBind();

            this.drp_unitlist0.DataSource = ds_unit;
            this.drp_unitlist0.DataTextField = "unit_Name";
            this.drp_unitlist0.DataValueField = "unit_ID";
            this.drp_unitlist0.DataBind();
        }


        //查询部门
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindUnit();


        }
        //删除部门
        protected void btn_DelCst_Click(object sender, EventArgs e)
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string msg = "部门删除成功";
            bool flag = true;
            for (int i = 0; i < this.grid_unit.Rows.Count; i++)
            {
                CheckBox chk = this.grid_unit.Rows[i].Cells[0].FindControl("chk_id") as CheckBox;
                string str_unitid = this.grid_unit.Rows[i].Cells[1].Text;
                if (chk.Checked)
                {
                    //检查是否该部门下存在成员
                    string existsUserSql = "select top 1 1 from tg_member where mem_Unit_ID = " + str_unitid;

                    object existsObjResult = TG.DBUtility.DbHelperSQL.GetSingle(existsUserSql);

                    if (existsObjResult == null)
                    {
                        bll_unit.Delete(int.Parse(str_unitid));
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }

            if (flag == false)
            {
                msg += "，其中一些部门包含用户，不能删除！";
            }

            //弹出提示
            TG.Common.MessageBox.ShowAndRedirect(this, msg, "Sys_UnitListByMaster.aspx");
        }
        //添加部门
        protected void btn_save_Click(object sender, EventArgs e)
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            TG.Model.tg_unit model_unit = new TG.Model.tg_unit();
            model_unit.unit_Name = this.txt_unitName.Text.Trim();
            model_unit.unit_ParentID = Convert.ToInt32(this.drp_unitlist.SelectedItem.Value);
            model_unit.unit_IsEndUnit = 0;
            if (this.drp_unitlist.SelectedValue == "0")
            {
                model_unit.unit_IsEndUnit = 1;
            }
            model_unit.unit_Intro = this.txt_unitInfo.Text.Trim();
            try
            {
                int AffectRow = bll_unit.Add(model_unit);

                if (AffectRow > 0)
                {
                    var bllExt = new TG.BLL.tg_unitExt();
                    //部门类型
                    int unitType = int.Parse(this.drpUnitType.SelectedValue);

                    var model = new TG.Model.tg_unitExt()
                    {
                        unit_ID = AffectRow,
                        unit_Name = model_unit.unit_Name,
                        unit_Type = unitType,
                        unit_Order = int.Parse(this.txt_Order.Text)
                    };

                    string strWhere = string.Format(" unit_ID={0} ", model.unit_ID);
                    int count = bllExt.GetRecordCount(strWhere);

                    if (count == 0)
                    {
                        bllExt.Add(model);
                    }
                    else
                    {
                        bllExt.Update(model);
                    }
                }
            }
            catch (System.Exception ex)
            { }
            //弹出提示
            TG.Common.MessageBox.ShowAndRedirect(this, "部门添加成功！", "Sys_UnitListByMaster.aspx");
        }
        //编辑部门信息
        protected void btn_edit_Click(object sender, EventArgs e)
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            TG.Model.tg_unit model_unit = new TG.Model.tg_unit();
            if (this.hid_unitid.Value != "")
            {
                model_unit.unit_ID = int.Parse(this.hid_unitid.Value);
                model_unit.unit_Name = this.txt_unitName0.Text.Trim();
                model_unit.unit_ParentID = Convert.ToInt32(this.drp_unitlist0.SelectedItem.Value);
                model_unit.unit_IsEndUnit = 0;
                if (this.drp_unitlist0.SelectedValue != "0")
                {
                    model_unit.unit_IsEndUnit = 1;
                }
                model_unit.unit_Intro = this.txt_unitInfo0.Text.Trim();
                try
                {
                    bll_unit.Update(model_unit);
                    string strWhere = " unit_ID=" + model_unit.unit_ID;
                    List<TG.Model.cm_DropUnitList> list = new TG.BLL.cm_DropUnitList().GetModelList(strWhere);
                    //存在
                    if (list.Count > 0)
                    {
                        TG.Model.cm_DropUnitList model = list[0];
                        if (raBtnHidd.Checked)
                        {
                            model.flag = 0;
                        }
                        else
                        {
                            model.flag = 1;
                        }
                        model.flag = model.flag == 0 ? 1 : 0;
                        new TG.BLL.cm_DropUnitList().Update(model);

                    }
                    else
                    {
                        TG.Model.cm_DropUnitList model = new TG.Model.cm_DropUnitList();
                        model.unit_ID = model_unit.unit_ID;
                        model.flag = 1;
                        new TG.BLL.cm_DropUnitList().Add(model);
                    }

                    //部门类型更新 qpl 2016年11月23日
                    var bllExt = new TG.BLL.tg_unitExt();
                    //部门类型
                    int unitType = int.Parse(this.drpUnitType1.SelectedValue);

                    var modelExt = new TG.Model.tg_unitExt()
                    {
                        unit_ID = model_unit.unit_ID,
                        unit_Name = model_unit.unit_Name,
                        unit_Type = unitType,
                        unit_Order=int.Parse(this.txt_Order2.Text)
                    };

                    string strWhereExt = string.Format(" unit_ID={0} ", modelExt.unit_ID);
                    var listExt = bllExt.GetRecordCount(strWhereExt);

                    if (list.Count == 0)
                    {
                        bllExt.Add(modelExt);
                    }
                    else
                    {
                        bllExt.Update(modelExt);
                    }

                }
                catch (System.Exception ex)
                { }
                //弹出提示
                TG.Common.MessageBox.ShowAndRedirect(this, "部门修改成功！", "Sys_UnitListByMaster.aspx");
            }
        }
        protected void grid_unit_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbl_name = e.Row.Cells[2].FindControl("Label2") as Label;
                Label lab_IsshowName = e.Row.Cells[3].FindControl("IsShow") as Label;
                string unitid = lbl_name.Text;
                string unitShow = lab_IsshowName.Text;
                TG.Model.cm_DropUnitList DropUnitModel = new TG.BLL.cm_DropUnitList().GetModelByUnitID(int.Parse(unitShow));
                //返回值
                if (DropUnitModel != null)
                {
                    lab_IsshowName.Text = DropUnitModel.flag.ToString() == "1" ? "隐藏" : "显示";
                }
                else
                {
                    lab_IsshowName.Text = "显示";
                }
                if (unitid != "0")
                {

                    TG.BLL.tg_unit bll = new TG.BLL.tg_unit();
                    TG.Model.tg_unit model = bll.GetModel(int.Parse(unitid));
                    if (model != null)
                    {
                        lbl_name.Text = model.unit_Name.Trim();
                    }
                }
                else
                {
                    lbl_name.Text = "一级部门";
                }

                string celval = e.Row.Cells[6].Text;

                if (celval == "0")
                {
                    e.Row.Cells[6].Text = "施工图部门";
                }
                else if (celval == "1")
                {
                    e.Row.Cells[6].Text = "方案部门";
                }
                else if (celval == "2")
                {
                    e.Row.Cells[6].Text = "特殊部门(设计)";
                }
                else if (celval == "3")
                {
                    e.Row.Cells[6].Text = "特殊部门(行政)";
                }
                else
                {
                    e.Row.Cells[6].Text = "其他部门";
                }
            }

        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindUnit();
        }

        public string UnitString
        {
            get
            {
                // qpl 2016年11月23日
                string str = "";
                // 0 为施工部门  
                string strWhere = string.Format(" unit_Type={0}", 0);
                var unitlist = new TG.BLL.tg_unitExt().GetModelList(strWhere);

                unitlist.ForEach(c =>
                {
                    str += c.unit_ID + ",";
                });

                return str;
            }
        }



    }

}