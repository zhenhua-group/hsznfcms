﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="SetPersinalBymaster.aspx.cs" Inherits="TG.Web.SystemSet.SetPersinalBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/SysSet.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_sign.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript">
        var flag = '<% = Flag() %>';
        var swfu;
        var memid = '<%=GetMemId() %>';
        var userid = '<%=GetCurMemID() %>';
     
        window.onload = function () {
            //附件高清图
            swfu = new SWFUpload({
                upload_url: "../ProcessUpload/upload_sign.aspx?type=sign&id=" + memid + "&userid=" + userid,
                flash_url: "../js/swfupload/swfupload.swf",
                post_params: {
                    "ASPSESSID": "<%=Session.SessionID %>"
                },
                file_size_limit: "10 MB",
                file_types: "*.jpg;*.gif;*.bmp;.png",
                file_types_description: "文件资料上传",
                file_upload_limit: "0",
                file_queue_limit: "1",

                //Events
                file_queued_handler: fileQueued,
                file_queue_error_handler: fileQueueError,
                file_dialog_complete_handler: fileDialogComplete,
                upload_progress_handler: uploadProgress,
                upload_error_handler: uploadError,
                upload_success_handler: uploadSuccess,
                upload_complete_handler: uploadComplete,

                // Button
                button_placeholder_id: "spanButtonPlaceholder",
                //                button_image_url: "../Images/swfupload/cc.png",
                button_style: '{background-color:#d8d8d8}',
                button_width: 61,
                button_height: 22,
                button_text: '<span class="fileupload-new">选择文件</span>',
                button_text_style: '.fileupload-new {background-color:#d8d8d8; } ',
                button_text_top_padding: 1,
                button_text_left_padding: 5,

                custom_settings: {
                    upload_target: "divFileProgressContainer"
                },
                debug: false
            });

        }
    </script>
    <script type="text/javascript" src="../js/SystemSet/SetPersinal.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        首页 <small>个人资料</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a>个人资料</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-3">
            <ul class="ver-inline-menu tabbable margin-bottom-10">
                <li class="active"><a data-toggle="tab" href="#tab_1"><i class="fa fa-briefcase"></i>
                    个人资料</a><span class="after"></span></li>
               <%-- <li><a data-toggle="tab" href="#tab_2"><i class="fa fa-group"></i>修改资料</a></li>--%>
                <li><a data-toggle="tab" href="#tab_3"><i class="fa fa-leaf"></i>修改密码</a></li>
            </ul>
        </div>
        <div class="col-md-9">
            <div class="tab-content">
                <div id="tab_1" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td>
                                            头像:
                                        </td>
                                        <td>
                                            编号:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_ID" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            用户名:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_Name" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            登录名:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_login" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="3" align="center">
                                            <asp:Image onerror="javascript:this.src='../Images/zw.jpg'" runat="server" ID="lbl_img"
                                                Width="50" Height="50" />
                                        </td>
                                        <td>
                                            性别:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_sex" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            出生日期:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_birth" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            单位:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_unit" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            专业:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_spec" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            角色:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_pric" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            手机:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_mobile" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            办公电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_phone" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            邮箱:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_email" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            电子签名:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_sign" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab_2" class="tab-pane">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td>
                                            编号:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_ID0" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            用户名:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="lbl_Name0" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        </td>
                                        <td>
                                            登录名:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_memLogin0" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            性别:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drp_sex" runat="server" CssClass="form-control input-sm">
                                                <asp:ListItem>男</asp:ListItem>
                                                <asp:ListItem>女</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            出生日期:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="lbl_birth0" runat="server" CssClass="Wdate" onclick="WdatePicker({readOnly:true})"
                                                Width="120px" Height="30px"></asp:TextBox>
                                        </td>
                                        <td>
                                            单位:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_unit0" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            专业:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_spec0" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </td>
                                        <td>
                                            角色:
                                        </td>
                                        <td>
                                            <asp:Label ID="lbl_pric0" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </td>
                                        <td>
                                            手机:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="lbl_mobile0" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            办公电话:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="lbl_phone0" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        </td>
                                        <td>
                                            邮箱:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="lbl_email0" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        </td>
                                        <td>
                                            电子签名:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="lbl_sign0" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            上传头像:
                                        </td>
                                        <td colspan="5">
                                            <asp:TextBox ID="txt_pic0" runat="server" CssClass="form-control input-sm" Width="300px"> </asp:TextBox>
                                            <span id="spanButtonPlaceholder"></span><span style="color: red">(注:图像比例1:1)</span><br />
                                            <div id="divFileProgressContainer">
                                            </div>
                                            <asp:HiddenField ID="txt_pic0_s" runat="server"></asp:HiddenField>
                                            <asp:HiddenField ID="txt_pic" runat="server"></asp:HiddenField>
                                            <asp:HiddenField ID="txt_pic_s" runat="server"></asp:HiddenField>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-offset-12 col-md-12">
                                    <asp:Button ID="btn_save" runat="server" CssClass="btn green btn-default" OnClick="btn_save_Click"
                                        Text="确定" />
                                    <a href="#" class="btn btn-default" id="btn_back">返回</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab_3" class="tab-pane">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td>
                                            原始密码:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_old" runat="server" CssClass="form-control input-sm" TextMode="Password"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            新密码:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_new" runat="server" CssClass="form-control input-sm" TextMode="Password"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            重复新密码:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txt_new1" runat="server" CssClass="form-control input-sm" TextMode="Password"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-offset-12 col-md-12">
                                    <a href="#" class="btn green btn-default" id="btn_pwd">确定</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
