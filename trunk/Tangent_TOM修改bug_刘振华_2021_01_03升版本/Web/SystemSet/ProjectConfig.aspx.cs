﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using TG.BLL;
using System.Text;

namespace TG.Web.SystemSet
{
	/// <summary>
	/// 项目配置文件上传
	/// </summary>
	public partial class ProjectConfig : System.Web.UI.Page
	{
		public string FacultyName
		{
			get
			{
				return Request["FacultyNameTextBox"];
			}
		}

		public HttpPostedFile ConfigFile
		{
			get
			{
				return Request.Files[0];
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.HttpMethod == "POST")
			{
				if (ConfigFile.InputStream.CanRead && ConfigFile.ContentLength > 0)
				{
					StreamReader streamReader = new StreamReader(ConfigFile.InputStream, Encoding.Default);

					string configContent = streamReader.ReadToEnd();

					int count = new cm_SysConfigBP().InsertProjectConfigFile(FacultyName, configContent);

					if (count > 0)
					{
						Response.Write("<script type=\"text/javascript\">alert(\"添加成功！\");</script>");
					}
					else
					{
						Response.Write("<script type=\"text/javascript\">alert(\"添加失败！\");</script>");	
					}
				}
			}
		}
	}
}
