﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class SetArchLevelBymaster : System.Web.UI.Page
    {
        TG.BLL.cm_MemArchLevel bll_level = new TG.BLL.cm_MemArchLevel();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //初始排序
                InitOrderDropDown();
                //绑定职称
                BindLevel();
            }
        }
        //绑定部门信息
        protected void BindLevel()
        {
            StringBuilder sb = new StringBuilder("");
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.AppendFormat(" [Name] Like '%{0}%' ", keyname);
            }
            //角色数量
            this.AspNetPager1.RecordCount = int.Parse(bll_level.GetRecordCount(sb.ToString()).ToString());
            //绑定
            DataSet ds_level = bll_level.GetListByPage(sb.ToString(), "", this.AspNetPager1.StartRecordIndex - 1, this.AspNetPager1.PageSize);
            this.grid_level.DataSource = ds_level;
            this.grid_level.DataBind();
        }
        //查询部门
        protected void btn_Search_Click(object sender,  EventArgs e)
        {
            TG.BLL.cm_MemArchLevel bll_level = new TG.BLL.cm_MemArchLevel();
            string strWhere = "";
            if (this.txt_keyname.Value.Trim() != "")
            {
                strWhere = " Name LIKE '%" + this.txt_keyname.Value + "%'";
            }
            this.grid_level.DataSource = bll_level.GetList(strWhere);
            this.grid_level.DataBind();
        }
        //删除部门
        protected void btn_DelCst_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_MemArchLevel bll_level = new TG.BLL.cm_MemArchLevel();
            string msg = "部门删除成功";
            for (int i = 0; i < this.grid_level.Rows.Count; i++)
            {
                CheckBox chk = this.grid_level.Rows[i].Cells[0].FindControl("chk_id") as CheckBox;
                string str_id = this.grid_level.Rows[i].Cells[1].Text;
                if (chk.Checked)
                {   
                    //检查是否该部门下存在成员
                    bll_level.Delete(int.Parse(str_id));
                }
            }
            //弹出提示
            TG.Common.MessageBox.ShowAndRedirect(this, msg, "SetArchLevelBymaster.aspx");
        }
        //添加部门
        protected void btn_save_Click(object sender,  EventArgs e)
        {
            TG.BLL.cm_MemArchLevel bll_level = new TG.BLL.cm_MemArchLevel();
            TG.Model.cm_MemArchLevel model_level = new TG.Model.cm_MemArchLevel();
            //职位名称
            model_level.Name = this.txt_archName.Text.Trim();
            //分配比例
            model_level.AllotPrt = Convert.ToDecimal(this.txt_allotprt.Text);
            //排序
            model_level.LevelOrder = Convert.ToInt32(this.drp_order.SelectedValue);
            model_level.Info = this.txt_archInfo.Text.Trim();

            bll_level.Add(model_level);
            //弹出提示
            TG.Common.MessageBox.ShowAndRedirect(this, "添加职位成功！", "SetArchLevelBymaster.aspx");
        }
        //编辑部门信息
        protected void btn_edit_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_MemArchLevel bll_level = new TG.BLL.cm_MemArchLevel();
            TG.Model.cm_MemArchLevel model_level = new TG.Model.cm_MemArchLevel();
            if (this.hid_archid.Value != "")
            {
                //职称ID
                model_level.ArchLevel_ID = int.Parse(this.hid_archid.Value);
                //职称名
                model_level.Name = this.txt_archName0.Text.Trim();
                //职称分配比例
                model_level.AllotPrt = Convert.ToDecimal(this.txt_allotprt0.Text);
                //职称排序
                model_level.LevelOrder = Convert.ToInt32(this.drp_order0.SelectedValue);
                //职称描述
                model_level.Info = this.txt_archInfo0.Text.Trim();

                bll_level.Update(model_level);
                //弹出提示
                TG.Common.MessageBox.ShowAndRedirect(this, "职称修改成功！", "SetArchLevelBymaster.aspx");
            }
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindLevel();
        }

        //初始下拉列表
        protected void InitOrderDropDown()
        {
            int orderCount = bll_level.GetRecordCount("");

            for (int i = 1; i <= orderCount + 1; i++)
            {
                drp_order.Items.Add(i.ToString());
                drp_order0.Items.Add(i.ToString());
            }
        }
    }
}