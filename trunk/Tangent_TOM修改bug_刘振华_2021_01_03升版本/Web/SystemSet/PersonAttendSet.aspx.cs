﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace TG.Web.SystemSet
{
    public partial class PersonAttendSet :PageBase
    {
        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int oldyear = 2016;
                //初始化年
                int curryear = DateTime.Now.Year;
                for (int i = oldyear; i <= curryear; i++)
                {
                    this.drpYear.Items.Add(i.ToString());
                }
                if (this.drpYear.Items.FindByValue(DateTime.Now.Year.ToString())!=null)
                {
                    this.drpYear.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
                }
                
                
                //部门信息
                BindUnit();
                //人员信息
                BindMem();
               
            }
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //绑定角色信息
        protected void BindMem()
        {
            StringBuilder sb = new StringBuilder("");
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.AppendFormat(" (mem_Name Like '%{0}%' or mem_Login Like '%{0}%') ", keyname);
            }
            //单位
           
                if (!string.IsNullOrEmpty(sb.ToString()))
                {
                    sb.AppendFormat(" and ");
                }
                sb.AppendFormat(" mem_Unit_ID={0} ", this.drp_unit3.SelectedValue);
         
              //排除离职人
                sb.Append(" and mem_isFired=0 ");
                
            //绑定
                this.grid_mem.DataSource = bll_mem.GetModelList(sb.ToString());
                this.grid_mem.DataBind();
        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();

            this.drp_unit3.DataSource = bll_unit.GetList("  unit_ParentID<>0 and unit_ID<>232  order by (select unit_Order from tg_unitExt where unit_ID=tg_unit.unit_ID)");
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }
        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindMem();
        }
        //生产部门改变重新绑定数据
        protected void drp_unit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMem();
        }
        protected void grid_mem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string mid = e.Row.Cells[0].Text;

                List<TG.Model.cm_PersonAttendSet> list = new TG.BLL.cm_PersonAttendSet().GetModelList(" mem_id=" + mid + " and attend_month>0 and attend_year="+this.drpYear.SelectedValue+" order by attend_month asc");
                for (int i = 1; i <= 12; i++)
                {
                     TextBox tb_shang = e.Row.Cells[(i + 1)].FindControl("shang"+i) as TextBox;
                     TextBox tb_xia = e.Row.Cells[(i + 1)].FindControl("xia"+i) as TextBox;
                    string strshang = "";
                    string strxia = "";
                    if (list!=null)
                    {
                        var pas = list.Where(p => p.attend_month == i).FirstOrDefault<TG.Model.cm_PersonAttendSet>();
                       
                        if (pas!=null)
                        {
                            strshang = pas.ToWork;
                            strxia = pas.OffWork;
                        }
                        else
                        {
                            strshang = "09:00";
                            strxia = "17:30";                         
                        }

                    }
                    else
                    {
                        strshang = "09:00";
                        strxia = "17:30";    

                    }

                    tb_shang.Text = strshang;
                    tb_xia.Text = strxia;
                }
            }
        }
    }
}