﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using TG.Common;

namespace TG.Web.SystemSet
{
    public partial class ProjectAllotConfigBymaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {    //绑定结构类型
                BindStructType();
                BindAllData();
                //绑定专业
                BindSpecial();
            }

        }
        protected void BindAllData()
        {

            //绑定公司配置
            BindCompanyData();
            //绑定专业配置
            BindSpedData();
            //绑定用户配置
            BindUserData();

        }
        //保存专业
        protected void BindSpecial()
        {
            this.DropDownList1.DataSource = new TG.BLL.tg_speciality().GetList("");
            this.DropDownList1.DataTextField = "spe_Name";
            this.DropDownList1.DataValueField = "spe_ID";
            this.DropDownList1.DataBind();
        }
        //绑定结构类型
        protected void BindStructType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            this.drp_strutype.DataSource = bll_dic.GetList(" dic_Type='proj_fp'");
            this.drp_strutype.DataTextField = "dic_Name";
            this.drp_strutype.DataValueField = "ID";
            this.drp_strutype.DataBind();
        }
        //加载配置
        protected void BindCompanyData()
        {
            TG.BLL.cm_CostCompanyConfig bll = new TG.BLL.cm_CostCompanyConfig();

            this.GridView1.DataSource = bll.GetList("");
            this.GridView1.DataBind();
        }
        //专业比例
        protected void BindSpedData()
        {
            TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();

            string strSql = " Select * From cm_CostSpeConfig A, tg_speciality B Where A.SpeID=B.spe_ID";
            //绑定分页
            this.GridView2.DataSource = bll.GetList(strSql);
            this.GridView2.DataBind();
        }
        //用户比例
        protected void BindUserData()
        {
            TG.BLL.cm_CostUserConfig bll = new TG.BLL.cm_CostUserConfig();
            //分页
            this.GridView3.DataSource = bll.GetList("");
            this.GridView3.DataBind();
        }
        //保存院分配比例
        protected void btn_Save_Click(object sender, EventArgs e)
        {
            bool ischecked = this.chk_cmpstatus.Checked;

            TG.Model.cm_CostCompanyConfig model = new TG.Model.cm_CostCompanyConfig();
            TG.BLL.cm_CostCompanyConfig bll = new TG.BLL.cm_CostCompanyConfig();

            model.CmpMngPrt = Convert.ToDecimal(this.txt_config1.Text);
            model.ProCostPrt = Convert.ToDecimal(this.txt_config2.Text);
            model.ProChgPrt = Convert.ToDecimal(this.txt_config3.Text);
            model.RegUserPrt = Convert.ToDecimal(this.txt_config4.Text);
            model.DesignPrt = Convert.ToDecimal(this.txt_config5.Text);
            model.AuditPrt = Convert.ToDecimal(this.txt_config6.Text);
            model.TotalUser = Convert.ToDecimal(this.txt_config7.Text);
            model.CollegeUser = Convert.ToDecimal(this.txt_config8.Text);
            model.Used = ischecked == true ? 1 : 0;

            if (bll.Add(model) > 0)
            {
                MessageBox.Show(this, "保存配置成功！");
                BindAllData();
            }
        }
        //专业分配
        protected void btn_SaveSpe_Click(object sender, EventArgs e)
        {
            TG.Model.cm_CostSpeConfig model = new TG.Model.cm_CostSpeConfig();
            TG.BLL.cm_CostSpeConfig bll = new TG.BLL.cm_CostSpeConfig();

            model.ListPrt = Convert.ToDecimal(this.TextBox2.Text);
            model.SpeID = Convert.ToInt32(this.DropDownList1.SelectedItem.Value);
            model.StrucType = Convert.ToInt32(this.drp_strutype.SelectedItem.Value);
            if (!bll.Exsit(this.DropDownList1.SelectedItem.Value, this.drp_strutype.SelectedItem.Value))
            {
                if (bll.Add(model) > 0)
                {
                    MessageBox.Show(this, "保存配置成功！");
                    BindAllData();
                }
            }
            else
            {
                string strWhere = " SpeID=" + this.DropDownList1.SelectedItem.Value;
                DataSet ds_rlt = bll.GetList(strWhere);
                model.ID = int.Parse(ds_rlt.Tables[0].Rows[0][0].ToString());
                if (bll.Update(model))
                {
                    MessageBox.Show(this, "保存配置成功！");
                    BindAllData();
                }
            }
        }
        //保存用户分配
        protected void btn_SaveUser_Click(object sender, EventArgs e)
        {
            TG.Model.cm_CostUserConfig model = new TG.Model.cm_CostUserConfig();
            TG.BLL.cm_CostUserConfig bll = new TG.BLL.cm_CostUserConfig();

            bool ischecked = this.chk_userstatus.Checked;
            model.DisgnUserPrt = Convert.ToDecimal(this.txt_config9.Text);
            model.AuditDPrt = Convert.ToDecimal(this.txt_config10.Text);
            model.SpeRegPrt = Convert.ToDecimal(this.txt_config11.Text);
            model.AuditPrt = Convert.ToDecimal(this.txt_config12.Text);
            model.ExamPrt = Convert.ToDecimal(this.txt_config13.Text);
            model.Used = ischecked == true ? 1 : 0;

            if (bll.Add(model) > 0)
            {
                MessageBox.Show(this, "保持配置成功");
                BindAllData();
            }
        }
        //设置为默认
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "setdefault")
            {
                int rowindex = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
                string id = ((HiddenField)this.GridView1.Rows[rowindex].Cells[0].FindControl("hid_id")).Value;
                string strSql = " Update cm_CostCompanyConfig Set Used=1 Where ID=" + id;

                TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
                if (bll.ExcuteBySql(strSql) > 0)
                {
                    strSql = " Update cm_CostCompanyConfig Set Used=0 Where ID<>" + id;
                    bll.ExcuteBySql(strSql);
                }
                BindAllData();
            }
        }
        //设为默认
        protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "setdefault")
            {
                int rowindex = ((GridViewRow)(((ImageButton)e.CommandSource).NamingContainer)).RowIndex;
                string id = ((HiddenField)this.GridView1.Rows[rowindex].Cells[0].FindControl("hid_id2")).Value;
                string strSql = " UPDATE cm_CostUserConfig SET Used = 1 Where ID=" + id;

                TG.BLL.CommDBHelper bll = new TG.BLL.CommDBHelper();
                if (bll.ExcuteBySql(strSql) > 0)
                {
                    strSql = " UPDATE cm_CostUserConfig SET Used=0 Where ID<>" + id;
                    bll.ExcuteBySql(strSql);
                }
                BindAllData();
            }
        }
        TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
        List<TG.Model.cm_Dictionary> list = new List<TG.Model.cm_Dictionary>();
        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string id = e.Row.Cells[0].Text;
                if (id != "&nbsp;")
                {
                    string strwhere = " dic_Type='proj_fp' AND ID=" + id;
                    list = bll_dic.GetModelList(strwhere);
                    if (list.Count > 0)
                    {
                        e.Row.Cells[0].Text = list[0].dic_Name;
                    }
                }
            }
        }
    }
}