﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.SystemSet
{
    public partial class ModifyQualificationBymaster : System.Web.UI.Page
    {

        TG.BLL.cm_Qualification quabll = new BLL.cm_Qualification();
        TG.BLL.cm_QuaAttach cm_bll = new BLL.cm_QuaAttach();
        TG.BLL.cm_AttachInfo attbll = new BLL.cm_AttachInfo();
        /// <summary>
        /// 
        /// </summary>
        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindQua();
            }
        }
        //初次加载时绑定各项的值
        private void BindQua()
        {
            string quaid = Request.QueryString["quaid"];
            string attinfoid = Request.QueryString["attinfoid"];
            TG.Model.cm_Qualification model = quabll.GetSingleModel(quaid);
            this.lbl_title.Text = model.qua_Name;
            this.lbl_content.Text = model.qua_Content;
            this.lbl_Zztime.Text = TG.Common.CommCoperation.GetEasyTime(Convert.ToDateTime(model.qua_ZzTime));
            this.lbl_Sptime.Text = TG.Common.CommCoperation.GetEasyTime(Convert.ToDateTime(model.qua_SpTime));
            this.lbl_jb.SelectedValue = model.qua_jb;
            DateTime dt = model.qua_yeartime == null ? DateTime.Now.Date : Convert.ToDateTime(model.qua_yeartime);
            this.lbl_yeartime.Text = TG.Common.CommCoperation.GetEasyTime(dt);
            this.hid_projid.Value = attinfoid;
        }

        public string GetProjectID()
        {
            string quaid = Request.QueryString["quaid"];
            return quabll.GetattInfoid(quaid);

        }
        public string GetCurMemID()
        {
            return Convert.ToString(this.UserInfo["memid"] ?? "0");
        }

        protected void btn_submin_Click(object sender, EventArgs e)
        {
            DoAlertQua();
        }
        /// <summary>
        /// 修改资质
        /// </summary>
        private void DoAlertQua()
        {
            string quaid = Request.QueryString["quaid"];
            if (cm_bll.Delete(quaid))
            {
                string sql = "Temp_No IN (SELECT cast(cm_qualification.cm_AttaInfoId as char(30)) FROM cm_qualification WHERE  qua_Id=" + quaid + ")";
                List<TG.Model.cm_AttachInfo> list = attbll.GetModelList(sql);
                foreach (TG.Model.cm_AttachInfo attmodel in list)
                {
                    TG.Model.cm_QuaAttach quamodel = new TG.Model.cm_QuaAttach();
                    quamodel.FileName = attmodel.FileName;
                    quamodel.FileSize = Convert.ToInt32(attmodel.FileSize);
                    quamodel.FileType = attmodel.FileType;
                    quamodel.IconPath = attmodel.FileTypeImg;
                    quamodel.RelativePath = "/Attach_User/filedata/qualfile/" + attmodel.Temp_No.Trim() + "/" + attmodel.FileName;
                    quamodel.UpLoadDate = Convert.ToDateTime(attmodel.UploadTime);
                    quamodel.WebBannerSysNo = quaid;
                    cm_bll.Add(quamodel);
                }
            }
            TG.Model.cm_Qualification model = quabll.GetSingleModel(quaid);
            model.qua_Content = this.lbl_content.Text;
            model.qua_Name = this.lbl_title.Text;
            model.qua_ZzTime = Convert.ToDateTime(this.lbl_Zztime.Text);
            model.qua_SpTime = Convert.ToDateTime(this.lbl_Sptime.Text);
            model.qua_yeartime = Convert.ToDateTime(this.lbl_yeartime.Text);
            model.qua_jb = this.lbl_jb.SelectedValue;

            if (quabll.Update(model))
            {
                TG.Common.MessageBox.ShowAndRedirect(this, "资质修改成功！", "QualificationListBymaster.aspx?src=" + GetProjectID() + "& cm_quaId=" + GetProjectID() + "");
                // Response.Redirect("QualificationList.aspx?src=" + GetProjectID() + "& cm_quaId=" + GetProjectID() + "");
            }
        }
    }
}