﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using TG.Model;
using System.Text;

namespace TG.Web.SystemSet
{
    public partial class UploadExcel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(UploadExcel));
            if (!IsPostBack)
            {
                int oldyear = 2016;
                //初始化年
                int curryear = DateTime.Now.Year;
                for (int i = oldyear; i <= curryear; i++)
                {
                    this.drpYear.Items.Add(i.ToString());
                }
                //当前月份12，显示下一年
                if (DateTime.Now.Month == 12)
                {
                    this.drpYear.Items.Add(((DateTime.Now.Year) + 1).ToString());
                }
                this.drpYear.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
                this.drp_month.Items.FindByValue(DateTime.Now.Month.ToString().PadLeft(2, '0')).Selected = true;

            
                for (int i = 2017; i <= curryear; i++)
                {
                    this.DropDownList1.Items.Add(i.ToString());
                }
                //当前月份12，显示下一年
                if (DateTime.Now.Month == 12)
                {
                    this.DropDownList1.Items.Add(((DateTime.Now.Year) + 1).ToString());
                }
                this.DropDownList1.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;
                this.DropDownList2.Items.FindByValue(DateTime.Now.Month.ToString()).Selected = true;

                //排除已选中的人
                List<TG.Model.tg_member> listUser = new TG.BLL.tg_member().GetModelList(" 1=1 order by mem_Name asc");
                this.userlist.DataSource = listUser;
                this.userlist.DataTextField = "mem_Name";
                this.userlist.DataValueField = "mem_ID";
                this.userlist.DataBind();

            }
            GetRoleViewEntityList();

        }
        private void GetRoleViewEntityList()        
        {
            List<TG.Model.cm_UnitManagerOverTime> roleViewEntity = new TG.BLL.cm_UnitManagerOverTime().GetModelList(" mem_id=0 and over_year=" + this.DropDownList1.SelectedValue + " and over_month=" + this.DropDownList2.SelectedValue + " order by id asc");
            this.gridList.DataSource = roleViewEntity;
            this.gridList.DataBind();
        }
        protected void btn_AddCar_Click(object sender, EventArgs e)
        {
            string memid = this.userlist.SelectedValue;
            string memname = this.userlist.SelectedItem.Text;
            if (this.userlist.SelectedIndex > 0)
            {
                List<TG.Model.cm_UnitManagerOverTime> list = new TG.BLL.cm_UnitManagerOverTime().GetModelList(" manager_id=" + memid + " and mem_id = 0 and over_year=" + this.DropDownList1.SelectedValue + " and over_month=" + this.DropDownList2.SelectedValue + "");
                if (list != null && list.Count > 0)
                {
                    Response.Write("<script>alert('已存在人员！');history.back();</script>");
                }
                else
                {
                    TG.Model.cm_UnitManagerOverTime uot = new TG.Model.cm_UnitManagerOverTime();
                    uot.manager_id = Convert.ToInt32(memid);
                    uot.manager_name = memname;
                    uot.mem_id = 0;
                    uot.mem_name = "";
                    uot.over_year = Convert.ToInt32(this.DropDownList1.SelectedValue);
                    uot.over_month = Convert.ToInt32(this.DropDownList2.SelectedValue);
                    int count = new TG.BLL.cm_UnitManagerOverTime().Add(uot);
                    if (count > 0)
                    {
                        Response.Write("<script>alert('添加成功！');window.location.href='UploadExcel.aspx';</script>");
                    }
                }
                
            }
            else
            {
                Response.Write("<script>alert('请选择人员！');history.back();</script>");
            }

        }
        protected void grid_mem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string uid = e.Row.Cells[0].Text;
                List<TG.Model.cm_UnitManagerOverTime> userslist = new TG.BLL.cm_UnitManagerOverTime().GetModelList(" manager_id=" + uid + " and mem_id<>0 and over_year=" + this.DropDownList1.SelectedValue + " and over_month=" + this.DropDownList2.SelectedValue + " order by id asc");
                Literal lit = e.Row.Cells[2].FindControl("lit_users") as Literal;
              
                if (userslist != null && userslist.Count > 0)
                {
                    userslist.ForEach(users =>
                    {
                        lit.Text += "<span id=\"userSpan\" usersysno=\"" + users.mem_id + "\"  style=\"margin-right: 1px;\">" + users.mem_name + "<img src=\"../../Images/pro_icon_03.gif\" style=\"cursor: pointer;\" alt=\"删除该用户\" name=\"deleteUserImgActionBtn\" /></span>";
                    });
                }
                

            }
        }

        /// <summary>
        /// 修改部门所对应的用户
        /// </summary>
        /// <param name="userSysNoString"></param>
        /// <param name="roleSysNoString"></param>
        /// <returns></returns>
        [AjaxMethod]
        public string UpdateRoleUsers(string userSysNoString, string managerid, string managername,string year,string month,string action)
        {
            int count = 0;
            if (!string.IsNullOrEmpty(userSysNoString))
            {
                if (action == "add")
                {
                    string[] usersysnolist = userSysNoString.Split(',');
                    foreach (string usersysno in usersysnolist)
                    {
                        TG.Model.tg_member member = new TG.BLL.tg_member().GetModel(Convert.ToInt32(usersysno));
                        if (member != null)
                        {

                            TG.Model.cm_UnitManagerOverTime uot = new TG.Model.cm_UnitManagerOverTime();
                            uot.manager_id = Convert.ToInt32(managerid);
                            uot.manager_name = managername;
                            uot.mem_id = Convert.ToInt32(usersysno);
                            uot.mem_name = member.mem_Name;
                            uot.over_year = Convert.ToInt32(year);
                            uot.over_month = Convert.ToInt32(month);
                            if ((new TG.BLL.cm_UnitManagerOverTime().Add(uot)) > 0)
                            {
                                count++;
                            }
                        }
                    }
                }
                else if (action == "del")
                {
                    //删除部门经理
                    count = TG.DBUtility.DbHelperSQL.ExecuteSql("delete from cm_UnitManagerOverTime where manager_id=" + managerid + " and mem_id=" + userSysNoString + " and over_year=" + year+ " and over_month=" + month + "");
                }

            }
            else
            {
                if (action == "del")
                {
                    //删除权限人员
                    count = TG.DBUtility.DbHelperSQL.ExecuteSql("delete from cm_UnitManagerOverTime where manager_id=" + managerid + " and over_year=" + year + " and over_month=" + month + "");
                }
            }
            return count + "";
        }

        protected void btn_Save_Click(object sender, EventArgs e)
        {
           //先删除再同步
            TG.DBUtility.DbHelperSQL.ExecuteSql("delete from cm_UnitManagerOverTime where over_year=" + this.DropDownList1.SelectedValue + " and over_month=" + this.DropDownList2.SelectedValue + "");

            int count=TG.DBUtility.DbHelperSQL.ExecuteSql("insert into cm_UnitManagerOverTime(manager_id,manager_name,mem_id,mem_name,over_year,over_month) select manager_id,manager_name,mem_id,mem_name,(" + this.DropDownList1.SelectedValue + ") as over_year,(" + this.DropDownList2.SelectedValue + ") as over_month from cm_UnitManagerOverTime where over_year=" + this.DropDownList1.SelectedValue + " and over_month=0 ");
            if (count>0)
            {
                Response.Write("<script>alert('同步成功！');window.location.href='UploadExcel.aspx';</script>");
            }
        }
    }
}