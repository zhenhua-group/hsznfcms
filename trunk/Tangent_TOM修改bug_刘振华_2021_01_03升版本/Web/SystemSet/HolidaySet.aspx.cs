﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;

namespace TG.Web.SystemSet
{
    public partial class HolidaySet : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            string hid = Request.QueryString["id"] ?? "";
            if (!IsPostBack)
            {
                //绑定部门
                BindUnit();
                //绑定年份
                BindYear();
                //绑定数据
                BindData();
                BindMem();
            }
            //删除
            if (!string.IsNullOrEmpty(hid))
            {
                new TG.BLL.cm_HolidayConfig().Delete(Convert.ToInt32(hid));
                Response.Redirect("HolidaySet.aspx");
            }

        }
        //绑定部门
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();

            this.drp_unit3.DataSource = bll_unit.GetList("  unit_ParentID<>0  and unit_ID<>232 order by (select unit_Order from tg_unitExt where unit_ID=tg_unit.unit_ID)");
            this.drp_unit3.DataTextField = "unit_Name";
            this.drp_unit3.DataValueField = "unit_ID";
            this.drp_unit3.DataBind();
        }
        //绑定年份
        public void BindYear()
        {
            int oldyear = 2016;
            //初始化年
            int curryear = DateTime.Now.Year + 10;
            for (int i = oldyear; i <= curryear; i++)
            {
                this.drpYear.Items.Add(i.ToString());
            }

            this.drpYear.Items.FindByValue(DateTime.Now.Year.ToString()).Selected = true;

            this.date.Value = DateTime.Now.ToString("yyyy-MM-dd");

            for (int i = 2016; i < DateTime.Now.Year; i++)
            {
                this.drp_year3.Items.Add(i.ToString());
            }
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }


        protected void btn_Search_Click(object sender, EventArgs e)
        {
            BindData();
        }
        public void BindData()
        {
            List<TG.Model.cm_HolidayConfig> list = new TG.BLL.cm_HolidayConfig().GetModelList(" year(holiday)=" + this.drpYear.SelectedValue);
            if (list != null && list.Count > 0)
            {
                string jjr = "", tsr = "";
                list.ForEach(h =>
                {
                    //代表节假日
                    if (h.daytype == 1)
                    {
                        jjr = jjr + "<span id=\"daySpan\" sysno=\"" + h.id + "\" style=\"margin-right: 1px;\">" + h.holiday.ToString("yyyy-MM-dd") + "<img src=\"../../Images/pro_icon_03.gif\" style=\"cursor: pointer;\" alt=\"删除\" name=\"delete\"></span>&nbsp;&nbsp;";
                    }
                    //代表特殊工作日
                    if (h.daytype == 2)
                    {
                        tsr = tsr + "<span id=\"daySpan\" sysno=\"" + h.id + "\" style=\"margin-right: 1px;\">" + h.holiday.ToString("yyyy-MM-dd") + "<img src=\"../../Images/pro_icon_03.gif\" style=\"cursor: pointer;\" alt=\"删除\" name=\"delete\"></span>&nbsp;&nbsp;";
                    }
                });
                this.ltl_jjr.Text = jjr;
                this.ltl_tsr.Text = tsr;
            }
            else
            {
                this.ltl_jjr.Text = "";
                this.ltl_tsr.Text = "";

            }

        }

        protected void btn_jjr_Click(object sender, EventArgs e)
        {
            TG.Model.cm_HolidayConfig model = new TG.Model.cm_HolidayConfig();
            model.daytype = 1;
            model.holiday = Convert.ToDateTime(this.date.Value);
            new TG.BLL.cm_HolidayConfig().Add(model);
            //绑定数据
            BindData();

        }

        protected void btn_tsr_Click(object sender, EventArgs e)
        {
            TG.Model.cm_HolidayConfig model = new TG.Model.cm_HolidayConfig();
            model.daytype = 2;
            model.holiday = Convert.ToDateTime(this.date.Value);
            new TG.BLL.cm_HolidayConfig().Add(model);
            //绑定数据
            BindData();
        }
        //绑定角色信息
        protected void BindMem()
        {
            StringBuilder sb = new StringBuilder("");

            //单位           
            sb.AppendFormat(" mem_Unit_ID={0} ", this.drp_unit3.SelectedValue);

            //排除离职人
            sb.Append(" and mem_isFired=0 ");
            //绑定
            this.grid_mem.DataSource = new TG.BLL.tg_member().GetModelList(sb.ToString());
            this.grid_mem.DataBind();
        }
        protected void btn_cx_Click(object sender, EventArgs e)
        {
            BindMem();
        }
        protected void grid_mem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string mid = e.Row.Cells[0].Text;

                List<TG.Model.cm_PersonAttendSet> list = new TG.BLL.cm_PersonAttendSet().GetModelList(" mem_id=" + mid + " and OffWork='" + this.drp_year3.SelectedValue + "' and attend_month=0 order by attend_month asc");
                TextBox days = e.Row.Cells[2].FindControl("days") as TextBox;
                string ts = "0";
                if (list != null && list.Count > 0)
                {
                    ts = list[0].ToWork;
                }
                days.Text = ts;


            }
        }
    }

}
