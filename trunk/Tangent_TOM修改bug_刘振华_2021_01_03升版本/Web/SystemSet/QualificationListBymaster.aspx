﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="QualificationListBymaster.aspx.cs" Inherits="TG.Web.SystemSet.QualificationListBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/jquery.alerts.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            $("#ctl00_ContentPlaceHolder1_btn_DelCst").click(function () {
                if ($("#ctl00_ContentPlaceHolder1_gv_Coperation :checkbox:checked").length == 0) {
                    jAlert("请选择要删除的资质！", "提示");
                    return false;
                }
                else {
                    $('#popup_ok').attr("class", "btn green");
                    return confirm("是否要删除资质信息？");
                }
            });
            $('#ctl00_ContentPlaceHolder1_gv_Coperation_ctl01_chkAll').click(function () {

                var checks = $('#ctl00_ContentPlaceHolder1_gv_Coperation :checkbox');


                if ($('#ctl00_ContentPlaceHolder1_gv_Coperation_ctl01_chkAll').attr("checked") == "checked") {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.setAttribute("class", "checked");
                        checks[i].setAttribute("checked", "checked");
                    }
                }
                else {
                    for (var i = 0; i < checks.length; i++) {
                        checks[i].parentNode.removeAttribute("class");
                        checks[i].removeAttribute("checked");
                    }
                }
            });


        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        系统设置 <small>资质管理</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a>系统设置</a><i class="fa fa-angle-right"> </i><a>资质管理</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>查询资质</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                    	<tr>
                    		<td>资质名称:</td>
                            <td><input type="text" class="form-control input-sm" id="txt_keyname" runat="server" /></td>
                            <td>级别:</td>
                            <td><asp:DropDownList ID="txt_jb" CssClass="form-control " runat="server" AppendDataBoundItems="true">
                                    <asp:ListItem Value="">--选择级别--</asp:ListItem>
                                    <asp:ListItem Value="甲级">甲级</asp:ListItem>
                                    <asp:ListItem Value="乙级">乙级</asp:ListItem>
                                    <asp:ListItem Value="丙级">丙级</asp:ListItem>
                                    <asp:ListItem Value="丁级">丁级</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>年审时间:</td>
                            <td><input type="text" name="txt_year" id="txt_year" onclick="WdatePicker({ readOnly: true })"
                                    class="Wdate" runat="Server" size="20" style="width: 90%; height: 22px" /></td>
                            <td><asp:Button ID="btn_Search" runat="server" OnClick="btn_Search_Click" CssClass="btn blue btn-default"
                                    Text="查询" /></td>
                            <td> <a href="AddQualificationBymaster.aspx" target="_self" class="btn blue btn-default">
                                    添加</a></td>
                            <td><asp:Button ID="btn_DelCst" runat="server" OnClick="btn_DelCst_Click" CssClass="btn blue btn-default"
                                    Text="删除" /></td>
                    	</tr>
                    </table>
               
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>资质信息列表</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:GridView ID="gv_Coperation" runat="server" CellPadding="0" DataKeyNames="qua_Id"
                                AutoGenerateColumns="False" CssClass="table table-bordered table-hover" RowStyle-HorizontalAlign="Center"
                                ShowHeader="true" Font-Size="13px" RowStyle-Height="24px" Width="100%" OnRowDataBound="gv_Coperation_RowDataBound">
                                <RowStyle HorizontalAlign="Left" Height="24px"></RowStyle>
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" /></HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_cpr" runat="server" CssClass="chk_Id" />
                                            <asp:HiddenField runat="Server" ID="cpr_Id" Value='<%# Eval("qua_Id") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="名称" SortExpression="ProjectName">
                                        <ItemTemplate>                                           
                                            <a href='ShowSingleQuaBymaster.aspx?flag=cprlist&quaid=<%# Eval("qua_Id") %>&attinfoid=<%#Eval("cm_AttaInfoId") %>'
                                                title='<%# Eval("qua_Name") %>'>
                                                <%# Eval("qua_Name").ToString().Length > 20 ? Eval("qua_Name").ToString().Substring(0, 20) + "..." : Eval("qua_Name").ToString()%></a>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="25%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="资质开始时间" SortExpression="qua_ZzTime">
                                        <ItemTemplate>
                                            <%#Convert.ToDateTime(Eval("qua_ZzTime").ToString()).ToString("yyyy-MM-dd")%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="资质结束时间" SortExpression="qua_SpTime">
                                        <ItemTemplate>
                                            <%#Convert.ToDateTime(Eval("qua_SpTime")).ToString("yyyy-MM-dd")%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="年审时间" SortExpression="qua_yeartime">
                                        <ItemTemplate>
                                            <%#Eval("qua_yeartime")%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="内容" SortExpression="qua_Content">
                                        <ItemTemplate>
                                            <%# Eval("qua_Content").ToString().Length > 15 ? Eval("qua_Content").ToString().Substring(0, 13) + "..." : Eval("qua_Content").ToString()%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="30%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="操作">
                                        <ItemTemplate>
                                            <a href='ShowSingleQuaBymaster.aspx?quaid=<%# Eval("qua_Id") %>&attinfoid=<%#Eval("cm_AttaInfoId") %>'
                                                target="_self">查看</a>|<a class='allowEdit' href='ModifyQualificationBymaster.aspx?quaid=<%# Eval("qua_Id") %>&attinfoid=<%#Eval("cm_AttaInfoId") %>'
                                                    target="_self"> 编辑</a>
                                        </ItemTemplate>
                                        <ItemStyle Width="10%" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    没有找到院资质
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
