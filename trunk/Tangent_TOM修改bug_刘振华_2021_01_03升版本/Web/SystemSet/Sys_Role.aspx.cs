﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace TG.Web.SystemSet
{
    public partial class Sys_Role : System.Web.UI.Page
    {
        TG.BLL.tg_principalship bll_pri = new TG.BLL.tg_principalship();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRole();
            }
        }
        //绑定角色信息
        protected void BindRole()
        {
            StringBuilder sb = new StringBuilder("");
            if (this.txt_keyname.Value.Trim()!="")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.AppendFormat(" pri_Name Like '%{0}%' ", keyname);
            }
            //角色数量
            this.AspNetPager1.RecordCount = int.Parse(bll_pri.GetRecordCount(sb.ToString()).ToString());
            //绑定数据
            this.grid_pri.DataSource = bll_pri.GetListByPage(sb.ToString(),"",this.AspNetPager1.StartRecordIndex-1,this.AspNetPager1.PageSize);
            this.grid_pri.DataBind();
        }
        //查询角色
        protected void btn_Search_Click(object sender, ImageClickEventArgs e)
        {
            BindRole();
        }
        //删除角色
        protected void btn_DelCst_Click(object sender, ImageClickEventArgs e)
        {
            TG.BLL.tg_principalship bll_pri = new TG.BLL.tg_principalship();
            for (int i = 0; i < this.grid_pri.Rows.Count; i++)
            {
                CheckBox chk = this.grid_pri.Rows[i].Cells[0].FindControl("chk_id") as CheckBox;
                string str_pri = this.grid_pri.Rows[i].Cells[1].Text;
                if (chk.Checked)
                {
                    bll_pri.Delete(int.Parse(str_pri));
                }
            }
            //弹出提示
            TG.Common.MessageBox.ShowAndRedirect(this, "角色删除成功！", "Sys_Role.aspx");
        }
        //添加部门
        protected void btn_save_Click(object sender, ImageClickEventArgs e)
        {
            TG.BLL.tg_principalship bll_pri = new TG.BLL.tg_principalship();
            TG.Model.tg_principalship model_pri = new TG.Model.tg_principalship();
            model_pri.pri_Name = this.txt_priName.Text.Trim();
            model_pri.pri_Intro = this.txt_priInfo.Text.Trim();
            try
            {
                bll_pri.Add(model_pri);
            }
            catch (System.Exception ex)
            { }
            //弹出提示
            TG.Common.MessageBox.ShowAndRedirect(this, "角色添加成功！", "Sys_Role.aspx");
        }
        //编辑部门信息
        protected void btn_edit_Click(object sender, ImageClickEventArgs e)
        {
            TG.BLL.tg_principalship bll_pri = new TG.BLL.tg_principalship();
            TG.Model.tg_principalship model_pri = new TG.Model.tg_principalship();
            if (this.hid_priid.Value != "")
            {
                model_pri.pri_ID = int.Parse(this.hid_priid.Value);
                model_pri.pri_Name = this.txt_priName0.Text.Trim();
                model_pri.pri_Intro = this.txt_priInfo0.Text.Trim();
                try
                {
                    bll_pri.Update(model_pri);
                }
                catch (System.Exception ex)
                { }
                //弹出提示
                TG.Common.MessageBox.ShowAndRedirect(this, "角色修改成功！", "Sys_Role.aspx");
            }
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            BindRole();
        }
    }
}
