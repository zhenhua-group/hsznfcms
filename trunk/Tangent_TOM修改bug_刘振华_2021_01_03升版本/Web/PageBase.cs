﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using TG.BLL;
using TG.Model;
using System.Linq;



namespace TG.Web
{
    public class PageBase : System.Web.UI.Page
    {
        private static TG.BLL.cm_SysMsg sysMsgBLL;
        //静态域初始化内容
        static PageBase()
        {
            sysMsgBLL = new TG.BLL.cm_SysMsg();
        }
        /// <summary>
        /// 
        /// </summary>
        protected HttpCookie UserInfo
        {
            get
            {
                HttpCookie user = HttpContext.Current.Request.Cookies["userinfo"];
                return user;
            }
        }
        /// <summary>
        /// 用户登录系统自增号
        /// </summary>
        protected int UserSysNo
        {
            get
            {
                object objUserSysNo = this.UserInfo["memid"];
                return objUserSysNo == null ? 1 : Convert.ToInt32(objUserSysNo);
            }
        }

        protected List<int> UserSysNoList
        {
            get
            {
                return new List<int>() { 1, 2 };
            }
        }
        /// <summary>
        /// 获取用户部门ID
        /// </summary>
        protected int UserUnitNo
        {
            get
            {

                TG.Model.tg_member user = new TG.BLL.tg_member().GetModel(Convert.ToInt32(this.UserInfo["memid"] ?? "0"));
                if (user != null)
                {
                    return user.mem_Unit_ID;
                }
                else
                {
                    return 1;
                }


            }
        }
        /// <summary>
        /// 登录用户简称
        /// </summary>
        protected string UserShortName
        {
            get
            {
                object objUserShortName = this.UserInfo["memlogin"];
                return objUserShortName == null ? "" : objUserShortName.ToString();
            }
        }
        /// <summary>
        /// 是否需要登录
        /// </summary>
        protected virtual bool IsAuth
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        ///  是否需要权限控制
        /// </summary>
        protected virtual bool CheckPreviewPower
        {
            get
            {
                return false;
            }
        }
        /// <summary>
        /// 角色功能权限
        /// </summary>
        protected ActionPowerParameterEntity RolePowerParameterEntity { get; private set; }
        /// <summary>
        /// 查询用户消息数
        /// </summary>
        public int UserMsgCount { get; set; }
        /// <summary>
        /// 用户权限集合List
        /// </summary>
        protected List<TG.Model.cm_Role> UserRolesList { get; set; }
        /// <summary>
        /// 登录跳转页
        /// </summary>
        protected string ReturnURL { get; set; }
        /// <summary>
        /// 页面初始
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            //判断是否需要登录
            if (IsAuth)
            {
                string redirectURL = Server.UrlEncode(Request.Url.PathAndQuery);
                //需要登录的场合，而用户没有登录。重定向到登录页
                if (UserSysNo == 0)
                {
                    Response.Write("<script type=\"text/javascript\" >window.parent.parent.parent.location.href=\"/Login.aspx?RedirectURL=" + redirectURL + "\"</script>");
                    Response.End();
                }
            }
            //需要检查查看权现的场合
            if (CheckPreviewPower)
            {
                var pointerIndex = Request.Path.LastIndexOf("/");
                var pageName = Request.Path.Substring(pointerIndex + 1);
                List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, pageName);

                RolePowerParameterEntity = new ActionPowerParameterEntity();
                //浏览权限
                RolePowerParameterEntity.PreviewPattern = 0;
                if ((from role in rolePowerParameterList where role.PreviewPattern == "2" select role).Count() > 0)
                {
                    //部门
                    RolePowerParameterEntity.PreviewPattern = 2;
                }
                if ((from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0)
                {
                    //全部
                    RolePowerParameterEntity.PreviewPattern = 1;
                }

                //编辑权限
                RolePowerParameterEntity.AllowEdit = (from role in rolePowerParameterList where role.AllowEdit == "1" select role).Count() > 0 ? 1 : 0;
                //删除权限
                RolePowerParameterEntity.AllowDelete = (from role in rolePowerParameterList where role.AllowDelete == "1" select role).Count() > 0 ? 1 : 0;
            }
            base.OnInit(e);
        }
        /// <summary>
        /// 页面加载
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            //修改于 20130814 页面问题解决

            //Response.Buffer = true;
            //Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
            //Response.AddHeader("pragma", "no-cache");
            //Response.AddHeader("cache-control", "");
            //Response.CacheControl = "no-cache";

            //查询用户权限
            UserRolesList = sysMsgBLL.GetRoleListByUserSysNo(UserSysNo);

            //查询用户消息数
            UserMsgCount = sysMsgBLL.GetSysMsgCount(UserSysNo);

            if (RolePowerParameterEntity != null)
            {
                string clientScript = string.Empty;

                if (RolePowerParameterEntity.AllowDelete == 0)
                {
                    var pointerIndex = Request.Path.IndexOf("/");
                    var pointerIndex2 = Request.Path.LastIndexOf("/");
                    var FoldName = Request.Path.Substring((pointerIndex + 1), (pointerIndex2 - pointerIndex - 1));
                    //考勤统计删除不受权限限制
                    if (FoldName != "Calendar")
                    {
                        clientScript += "$(\".allowDelete\").unbind(\"click\").bind(\"click\", function(e) {alert(\"没有权限，不允许删除\");if (e && e.stopPropagation) {e.stopPropagation();}else {window.event.cancelBubble = true;}return false;});";
                        clientScript += "$(\"#del_jqGrid\").unbind(\"click\").live(\"click\", function(e) {alert(\"没有权限，不允许删除\");if (e && e.stopPropagation) {e.stopPropagation();}else {window.event.cancelBubble = true;}return false;});";
                    }
                }
                if (RolePowerParameterEntity.AllowEdit == 0)
                {
                    clientScript += "$(\".allowEdit\").unbind(\"click\").live(\"click\", function(e) {alert(\"没有权限，不允许编辑\");if (e && e.stopPropagation) {e.stopPropagation();}else {window.event.cancelBubble = true;}return false;});";
                }
                Page.ClientScript.RegisterStartupScript(typeof(Page), "", "<script type=\"text/javascript\">$(function(){" + clientScript + " });</script>");
            }

            base.OnLoad(e);
        }
        /// <summary>
        /// 获取不显示的部门IDList
        /// </summary>
        protected string NotShowUnitList
        {
            get
            {
                string sqlwhere = "";
                var pointerIndex = Request.Path.IndexOf("/");
                var pointerIndex2 = Request.Path.LastIndexOf("/");
                var FoldName = Request.Path.Substring((pointerIndex + 1), (pointerIndex2 - pointerIndex - 1));
                if (FoldName == "Calendar")
                {
                    sqlwhere = " unit_ID=232 ";
                }
                else
                {
                    sqlwhere = " flag=1 ";
                }
                //获取显示的列表
                string strlist = "0,";
                List<TG.Model.cm_DropUnitList> list = new TG.BLL.cm_DropUnitList().GetModelList(sqlwhere);
                if (list.Count > 0)
                {
                    foreach (TG.Model.cm_DropUnitList model in list)
                    {
                        strlist += model.unit_ID.ToString() + ",";
                    }
                }
                strlist = strlist.Remove(strlist.Length - 1);
                return strlist;
            }

        }
        /// <summary>
        /// 系统管理员用户名
        /// </summary>
        protected string SystemManagerName
        {
            get
            {
                return ConfigurationManager.AppSettings["MenuControl"].ToString();
            }
        }
        /// <summary>
        /// 是否考勤管理员
        /// </summary>
        protected bool IsApplyManager
        {
            get
            {
                int count = 0;
                if (UserRolesList != null)
                {
                    count = UserRolesList.Where(r => r.RoleName == "考勤管理员").Count();
                }
                return count > 0 ? true : false;
            }
        }
        //协同TCD中工程师角色
        protected string TcdUserRole
        {
            get
            {
                object objUserShortName = UserInfo["principal"];
                return objUserShortName == null ? "" : objUserShortName.ToString();
            }
        }

        /// <summary>
        /// 当前月份考勤
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public DataTable GetCurrentMonth(string yearmonth, string type)
        {

            DataTable datatable = new DataTable();
            //Excel数据库赋值
            string filepath = Server.MapPath("~/Attach_User/kaoqin/KQ" + yearmonth + ".xls");
            //判断是否存在
            if (System.IO.File.Exists(filepath))
            {
                DataTable dt = new DataTable();

                // 查询语句
                string sql = "SELECT * FROM [Sheet 1$]";
                //读取1-15号的数据
                if (type == "1")
                {
                    TG.DBUtility.PubConstant.ExcelPath = filepath;
                    dt = TG.DBUtility.DbHelperOleDb.Query2(sql, TG.DBUtility.PubConstant.ConnectionExcel).Tables[0];
                }
                else
                {
                    TG.DBUtility.PubConstant.ExcelPath2 = filepath;
                    dt = TG.DBUtility.DbHelperOleDb.Query(sql).Tables[0];


                }

                // 在这里对DataSet中的数据进行操作  

                datatable.Columns.Add("ID");
                datatable.Columns.Add("UserName");
                datatable.Columns.Add("UnitName");
                datatable.Columns.Add("CHECKTIME");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = datatable.NewRow();
                    if (dt.Rows[i][1] != null && dt.Rows[i][3] != null && dt.Rows[i][1].ToString() != null && dt.Rows[i][3].ToString() != "")
                    {

                        dr["ID"] = dt.Rows[i][2].ToString();
                        dr["UserName"] = dt.Rows[i][1].ToString();
                        dr["UnitName"] = dt.Rows[i][0].ToString();
                        dr["CHECKTIME"] = Convert.ToDateTime(dt.Rows[i][3]).ToString("yyyy-MM-dd HH:mm:ss");
                        datatable.Rows.Add(dr);
                    }
                }
            }
            return datatable;
        }
        //考勤统计手动修改数据
        List<TG.Model.cm_ApplyStatisData> asd_list = new TG.BLL.cm_ApplyStatisData().GetModelList(" dataSource in ('StatisDetail','MonthSummary') order by id desc");
        //获取月加班时间
        public decimal GetOverTime(List<TG.Model.cm_HolidayConfig> holi_list, List<TG.Model.cm_PersonAttendSet> pas_list, DataTable dt_time, string year, string month, int mem_id, string mem_name, DateTime starttime, DateTime endtime, ref int wdkjl)
        {
            decimal sumtime_over = 0;

            ////申请出差加班          
            //if (dt_time != null && dt_time.Rows.Count > 0)
            //{
            //    DataTable dt_over = new DataView(dt_time) { RowFilter = "applytype='travel' and starttime>='" + starttime + "' and starttime<='" + (endtime.ToString("yyyy-MM-dd") + " 23:59:59") + "' and adduser=" + mem_id + "" }.ToTable();
            //    if (dt_over != null && dt_over.Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in dt_over.Rows)
            //        {
            //            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
            //            time_over = time_over + totaltime_over;
            //        }
            //    }
            //}

            ////加班离岗    
            //if (dt_time != null && dt_time.Rows.Count > 0)
            //{
            //    DataTable dt_addwork = new DataView(dt_time) { RowFilter = "applytype='addwork' and starttime>='" + starttime + "' and starttime<='" + (endtime.ToString("yyyy-MM-dd") + " 23:59:59") + "' and adduser=" + mem_id + "" }.ToTable();
            //    if (dt_addwork != null && dt_addwork.Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in dt_addwork.Rows)
            //        {
            //            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
            //            time_over = time_over - totaltime_over;
            //        }
            //    }
            //}

            //考勤
            string towork = "09:00", offwork = "17:30";
            //后台设置上班时间
            if (pas_list != null && pas_list.Count > 0)
            {
                List<TG.Model.cm_PersonAttendSet> list = pas_list.Where(p => p.mem_ID == mem_id && p.attend_year == Convert.ToInt32(year) && p.attend_month == Convert.ToInt32(month)).ToList();
                if (list != null && list.Count > 0)
                {
                    towork = list[0].ToWork;
                    offwork = list[0].OffWork;
                }
            }

            //考勤加班
            DataTable datatable = GetCurrentMonth((year + month.PadLeft(2, '0')), "1");

            //打卡统计
            //
            DataTable dt_late = new DataTable();
            if (datatable != null && datatable.Rows.Count > 0)
            {
                DateTime tomr_endtime = endtime;

                dt_late = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + mem_name + "'" }.ToTable();
                if (dt_late != null && dt_late.Rows.Count > 0)
                {
                    for (; starttime.CompareTo(endtime) <= 0; starttime = starttime.AddDays(1))
                    {
                        decimal time_over = 0;
                        DateTime shijioff = starttime;
                        DateTime houtai = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
                        DateTime houtaioff = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间
                        DateTime tomr_shijioff = shijioff;

                        DataTable dt_currt = new DataView(dt_late) { RowFilter = "CHECKTIME>='" + (shijioff.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (tomr_shijioff.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();
                        if (dt_currt != null && dt_currt.Rows.Count > 0)
                        {
                            //上班时间
                            // DateTime ontime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 09:00");
                            //中午下班时间
                            DateTime zwxbtime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 11:50");
                            //中午上班时间
                            DateTime zwsbtime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 13:00:59");
                            //下午下班时间
                            //  DateTime offtime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 17:30");

                            DateTime shiji = Convert.ToDateTime(Convert.ToDateTime(dt_currt.Rows[0]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际上班打卡时间
                            shijioff = Convert.ToDateTime(Convert.ToDateTime(dt_currt.Rows[(dt_currt.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际下班打卡时间

                            //判断是否有未打卡申请记录      
                            decimal totaltime_forget = 0;
                            if (dt_time != null && dt_time.Rows.Count > 0)
                            {
                                tomr_shijioff = starttime;
                                DataTable dt_over = new DataView(dt_time) { RowFilter = "applytype='forget' and starttime>='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and endtime<='" + (tomr_shijioff.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "' and adduser=" + mem_id + "" }.ToTable();
                                if (dt_over != null && dt_over.Rows.Count > 0)
                                {
                                    totaltime_forget = Convert.ToDecimal(dt_over.Rows[0]["totaltime"]);
                                }
                            }
                            
                            //判断是否假期加班
                            if (isListJQ(holi_list, starttime))
                            {
                                int mine = shiji.Minute;
                                DateTime jiaban = shiji;
                                if (mine > 0 && mine < 15)
                                {
                                    jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                }
                                else if (mine > 30 && mine < 45)
                                {
                                    jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                }
                                //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                if (totaltime_forget == 0)
                                {
                                    time_over = time_over + GetOver(jiaban, shijioff);
                                }
                                //节假日全天
                               // time_over = time_over + GetOver(jiaban, shijioff);
                                //全天
                                //if (shiji <= houtai && shijioff >= houtaioff)
                                //{
                                //    time_over = time_over + decimal.Parse("7.5");
                                //}
                                //else
                                //{
                                //    DateTime start = shiji;
                                //    DateTime end = shijioff;
                                //    //加班统计   
                                //    if (shiji < houtai)
                                //    {
                                //        start = houtai;
                                //    }
                                //    else if (shiji >= zwxbtime && shiji <= zwsbtime)
                                //    {
                                //        shiji = zwsbtime;
                                //    }
                                //    if (shijioff > houtaioff)
                                //    {
                                //        end = houtaioff;
                                //    }
                                //    else if (shijioff >= zwxbtime && shijioff <= zwsbtime)
                                //    {
                                //        end = zwxbtime;
                                //    }

                                //    //都是上午打卡    
                                //    if (end <= zwxbtime)
                                //    {
                                //        end = shijioff;
                                //        time_over = time_over + GetOver(start, end);
                                //    }
                                //    else //下班打卡是下午
                                //    {
                                //        //上班打卡也是下午
                                //        if (start >= zwsbtime)
                                //        {
                                //            time_over = time_over + GetOver(start, end);
                                //        }
                                //        else//上班打卡是上午
                                //        {
                                //            //上午加班时间
                                //            time_over = time_over + GetOver(start, zwxbtime);
                                //            //下午加班时间
                                //            time_over = time_over + GetOver(zwsbtime, end);
                                //        }
                                //    }
                                //}

                            }
                            else
                            {
                                //工作日只有晚上加班
                                //晚上加班
                                DateTime jiaban = houtaioff.AddMinutes(30);

                                if (shiji > jiaban && dt_currt.Rows.Count > 1)
                                {
                                    int mine = shiji.Minute;
                                    if (mine > 0 && mine < 15)
                                    {
                                        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                    }
                                    else if (mine > 30 && mine < 45)
                                    {
                                        jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                    }
                                    else
                                    {
                                        jiaban = shiji;
                                    }

                                }

                                //加班统计                       
                                if (shijioff > jiaban)
                                {                                   
                                    //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                    if (totaltime_forget == 0)
                                    {
                                        time_over = time_over + GetOver(jiaban, shijioff);
                                    }
                                    
                                }
                            }


                        }

                        //申请加班记录
                        if (dt_time != null && dt_time.Rows.Count > 0)
                        {
                            tomr_shijioff = starttime;
                            //加班
                            DataTable dt_over = new DataView(dt_time) { RowFilter = "applytype in ('travel','gomeet','forget') and starttime>='" + (starttime.ToString("yyyy-MM-dd") + " 06:00:00") + "' and endtime<='" + (tomr_shijioff.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "' and adduser=" + mem_id + "" }.ToTable();
                            if (dt_over != null && dt_over.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt_over.Rows)
                                {
                                    decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                    time_over = time_over + totaltime_over;
                                }
                            }
                            //离岗
                            DataTable dt_addwork = new DataView(dt_time) { RowFilter = "applytype='addwork' and starttime>='" + starttime + "' and starttime<='" + (starttime.ToString("yyyy-MM-dd") + " 23:59:59") + "' and adduser=" + mem_id + "" }.ToTable();
                            if (dt_addwork != null && dt_addwork.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt_addwork.Rows)
                                {
                                    decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                    time_over = time_over - totaltime_over;
                                }
                            }
                        }

                        //考勤统计手动修改记录
                        if (asd_list != null && asd_list.Count > 0)
                        {
                            var data_model = asd_list.Where(d => d.dataSource == "StatisDetail" && d.mem_id == mem_id && d.dataYear == starttime.Year && d.dataMonth == starttime.Month && d.dataDay == starttime.Day && d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                            if (data_model != null)
                            {
                                time_over = data_model.dataValue;
                            }
                        }
                        sumtime_over = sumtime_over + time_over;
                    }
                }
                else
                {
                    wdkjl = wdkjl + 1;
                }
            }
            else
            {
                wdkjl = wdkjl + 1;
            }

            return sumtime_over;
        }
        //返回加班时间
        public decimal GetOver(DateTime start, DateTime end)
        {

            decimal over = 0;
            if (end > start)
            {
                TimeSpan ts_jb = end - start;
                //强制转换，没有四舍五入
                int hours = (int)(ts_jb.TotalHours);

                double minutes = ts_jb.TotalMinutes - (hours * 60);
                //大于15分钟，即加班
                if (minutes >= 15 && minutes < 45)
                {
                    over = decimal.Parse("0.5");
                }
                else if (minutes >= 45 && minutes < 60)
                {
                    over = decimal.Parse("1");
                }

                over = over + hours;
            }
            return over;
        }
        //转换半个小时为单位
        public decimal GetHours(TimeSpan ts_jb)
        {
            decimal over = 0;
            //强制转换，没有四舍五入
            int hours = (int)(ts_jb.TotalHours);

            double minutes = ts_jb.TotalMinutes - (hours * 60);
            //大于15分钟，即加班
            if (minutes > 0 && minutes <= 30)
            {
                over = decimal.Parse("0.5");
            }
            else if (minutes > 30 && minutes < 60)
            {
                over = decimal.Parse("1");
            }

            over = over + hours;

            return over;
        }
        //判断是否是假期
        public bool isJQ(DateTime date)
        {
            bool flag = false;
            List<TG.Model.cm_HolidayConfig> list = new TG.BLL.cm_HolidayConfig().GetModelList(" convert(varchar(10),holiday,120)='" + date.ToString("yyyy-MM-dd") + "' order by id");
            //存在
            if (list != null && list.Count > 0)
            {
                //节假日，需减1天
                if (list[0].daytype == 1)
                {
                    flag = true;
                }
            }
            else
            {
                //周六日0~6
                string temp = Convert.ToDateTime(date.ToString("yyyy-MM-dd")).DayOfWeek.ToString();
                if (temp == "Sunday" || temp == "Saturday")
                {
                    flag = true;
                }
            }
            return flag;
        }

        //判断是否是假期
        public bool isListJQ(List<TG.Model.cm_HolidayConfig> list_date, DateTime date)
        {
            bool flag = false;
            if (list_date != null && list_date.Count > 0)
            {
                List<TG.Model.cm_HolidayConfig> list = list_date.Where(h => h.holiday.ToString("yyyy-MM-dd") == date.ToString("yyyy-MM-dd")).OrderBy(h => h.id).ToList();
                //存在
                if (list != null && list.Count > 0)
                {
                    //节假日，需减1天
                    if (list[0].daytype == 1)
                    {
                        flag = true;
                    }
                }
                else
                {
                    //周六日0~6
                    string temp = Convert.ToDateTime(date.ToString("yyyy-MM-dd")).DayOfWeek.ToString();
                    if (temp == "Sunday" || temp == "Saturday")
                    {
                        flag = true;
                    }
                }
            }
            else
            {
                //周六日0~6
                string temp = Convert.ToDateTime(date.ToString("yyyy-MM-dd")).DayOfWeek.ToString();
                if (temp == "Sunday" || temp == "Saturday")
                {
                    flag = true;
                }
            }

            return flag;
        }

        //从大到小排序
        public decimal[] sort(decimal[] a)
        {
            //排序  
            decimal t = 0;
            for (int i = 0; i < a.Length; i++)
            {
                for (int j = i + 1; j < a.Length; j++)
                {
                    if (a[i] < a[j])
                    {
                        t = a[j];
                        a[j] = a[i];
                        a[i] = t;
                    }
                }
            }
            return a;

        }

        //获取半年加班时间
        public decimal GetOverTimeYear(List<TG.Model.cm_HolidayConfig> holi_list, List<TG.Model.cm_PersonAttendSet> pas_list, DataTable dt_time, int mem_id, string mem_name, DateTime starttime, DateTime endtime, ref int wdkjl)
        {
            //获取锁定数据
            List<TG.Model.cm_MonthSummaryHis> his_list = new TG.BLL.cm_MonthSummaryHis().GetModelList("");

            decimal sumtime_over = 0;

            ////申请出差加班          
            //if (dt_time != null && dt_time.Rows.Count > 0)
            //{
            //    DataTable dt_over = new DataView(dt_time) { RowFilter = "applytype='travel' and starttime>='" + starttime + "' and starttime<='" + (endtime.ToString("yyyy-MM-dd") + " 23:59:59") + "' and adduser=" + mem_id + "" }.ToTable();
            //    if (dt_over != null && dt_over.Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in dt_over.Rows)
            //        {
            //            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
            //            time_over = time_over + totaltime_over;
            //        }
            //    }

            //}

            ////加班离岗    
            //if (dt_time != null && dt_time.Rows.Count > 0)
            //{
            //    DataTable dt_addwork = new DataView(dt_time) { RowFilter = "applytype='addwork' and starttime>='" + starttime + "' and starttime<='" + (endtime.ToString("yyyy-MM-dd") + " 23:59:59") + "' and adduser=" + mem_id + "" }.ToTable();
            //    if (dt_addwork != null && dt_addwork.Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in dt_addwork.Rows)
            //        {
            //            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
            //            time_over = time_over - totaltime_over;
            //        }
            //    }
            //}

            //判断这半年中此人是否有打卡记录
            bool flag = false;
            //循环最后一个日期 相同
            starttime = starttime.AddDays(-1);
            //按月循环
            for (; starttime.CompareTo(endtime) <= 0; starttime = starttime.AddMonths(1))
            {
                decimal monthtime_over = 0;
                //结束日期 到这个月15号结束
                DateTime enddate = starttime;
                //开始日期 上月16号开始
                DateTime startdate = starttime;
                startdate = startdate.AddMonths(-1).AddDays(1);
                TG.Model.cm_MonthSummaryHis his_mem = null;
                if (his_list != null && his_list.Count > 0)
                {
                    his_mem = his_list.Where(h => h.mem_id == mem_id && h.dataDate == endtime.ToString("yyyy-MM")).OrderByDescending(h => h.ID).FirstOrDefault();
                }
                //人员数据
                if (his_mem != null)
                {
                    monthtime_over = Convert.ToDecimal(his_mem.overtime);
                }
                else
                {

                    //考勤
                    string towork = "09:00", offwork = "17:30";
                    //后台设置上班时间
                    if (pas_list != null && pas_list.Count > 0)
                    {
                        List<TG.Model.cm_PersonAttendSet> list = pas_list.Where(p => p.mem_ID == mem_id && p.attend_year == endtime.Year && p.attend_month == endtime.Month).ToList();
                        if (list != null && list.Count > 0)
                        {
                            towork = list[0].ToWork;
                            offwork = list[0].OffWork;
                        }
                    }

                    //考勤加班
                    DataTable datatable = GetCurrentMonth((enddate.Year + enddate.Month.ToString().PadLeft(2, '0')), "1");

                    //打卡统计
                    //
                    DataTable dt_late = new DataTable();
                    if (datatable != null && datatable.Rows.Count > 0)
                    {
                        DateTime tomr_endtime = enddate;
                        dt_late = new DataView(datatable) { RowFilter = "CHECKTIME>='" + (startdate.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (tomr_endtime.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "' and UserName='" + mem_name + "'" }.ToTable();
                        if (dt_late != null && dt_late.Rows.Count > 0)
                        {
                            flag = true;
                            for (; startdate.CompareTo(enddate) <= 0; startdate = startdate.AddDays(1))
                            {
                                decimal time_over = 0;
                                DateTime shijioff = startdate;
                                DateTime houtai = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " " + towork + ":00");//弹性上班打卡时间
                                DateTime houtaioff = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " " + offwork + ":00");//弹性下班打卡时间
                                DateTime tomr_shijioff = shijioff;
                                //打卡记录
                                DataTable dt_currt = new DataView(dt_late) { RowFilter = "CHECKTIME>='" + (shijioff.ToString("yyyy-MM-dd") + " 06:00:00") + "' and CHECKTIME<='" + (tomr_shijioff.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "'" }.ToTable();
                                if (dt_currt != null && dt_currt.Rows.Count > 0)
                                {
                                    //上班时间
                                    // DateTime ontime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 09:00");
                                    //中午下班时间
                                    DateTime zwxbtime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 11:50:00");
                                    //中午上班时间
                                    DateTime zwsbtime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 13:00:59");
                                    //下午下班时间
                                    //  DateTime offtime = Convert.ToDateTime(shijioff.ToString("yyyy-MM-dd") + " 17:30");

                                    DateTime shiji = Convert.ToDateTime(Convert.ToDateTime(dt_currt.Rows[0]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际上班打卡时间
                                    shijioff = Convert.ToDateTime(Convert.ToDateTime(dt_currt.Rows[(dt_currt.Rows.Count - 1)]["CHECKTIME"]).ToString("yyyy-MM-dd HH:mm"));//实际下班打卡时间

                                    //判断是否有未打卡申请记录   
                                    decimal totaltime_forget = 0;                                                              
                                    if (dt_time != null && dt_time.Rows.Count > 0)
                                    {
                                        tomr_shijioff = startdate;
                                        DataTable dt_over = new DataView(dt_time) { RowFilter = "applytype='forget' and starttime>='" + (startdate.ToString("yyyy-MM-dd") + " 06:00:00") + "' and endtime<='" + (tomr_shijioff.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "' and adduser=" + mem_id + "" }.ToTable();
                                        if (dt_over != null && dt_over.Rows.Count > 0)
                                        {
                                            totaltime_forget = Convert.ToDecimal(dt_over.Rows[0]["totaltime"]);
                                        }
                                    }
                                    //判断是否假期加班
                                    if (isListJQ(holi_list, startdate))
                                    {
                                        //全天加班
                                        int mine = shiji.Minute;
                                        DateTime jiaban = shiji;
                                        if (mine > 0 && mine < 15)
                                        {
                                            jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                        }
                                        else if (mine > 30 && mine < 45)
                                        {
                                            jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                        }

                                        //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                        if (totaltime_forget == 0)
                                        {
                                            time_over = time_over + GetOver(jiaban, shijioff);
                                        }

                                       // time_over = time_over + GetOver(jiaban, shijioff);
                                        //全天
                                        //if (shiji <= houtai && shijioff >= houtaioff)
                                        //{
                                        //    time_over = time_over + decimal.Parse("7.5");
                                        //}
                                        //else
                                        //{
                                        //    DateTime start = shiji;
                                        //    DateTime end = shijioff;
                                        //    //加班统计   
                                        //    if (shiji < houtai)
                                        //    {
                                        //        start = houtai;
                                        //    }
                                        //    else if (shiji >= zwxbtime && shiji <= zwsbtime)
                                        //    {
                                        //        shiji = zwsbtime;
                                        //    }
                                        //    if (shijioff > houtaioff)
                                        //    {
                                        //        end = houtaioff;
                                        //    }
                                        //    else if (shijioff >= zwxbtime && shijioff <= zwsbtime)
                                        //    {
                                        //        end = zwxbtime;
                                        //    }

                                        //    //都是上午打卡    
                                        //    if (end <= zwxbtime)
                                        //    {
                                        //        end = shijioff;
                                        //        time_over = time_over + GetOver(start, end);
                                        //    }
                                        //    else //下班打卡是下午
                                        //    {
                                        //        //上班打卡也是下午
                                        //        if (start >= zwsbtime)
                                        //        {
                                        //            time_over = time_over + GetOver(start, end);
                                        //        }
                                        //        else//上班打卡是上午
                                        //        {
                                        //            //上午加班时间
                                        //            time_over = time_over + GetOver(start, zwxbtime);
                                        //            //下午加班时间
                                        //            time_over = time_over + GetOver(zwsbtime, end);
                                        //        }
                                        //    }
                                        //}

                                    }
                                    else
                                    {
                                        //工作日只有晚上加班
                                        //晚上加班
                                        DateTime jiaban = houtaioff.AddMinutes(30);

                                        if (shiji > jiaban && dt_currt.Rows.Count > 1)
                                        {
                                            int mine = shiji.Minute;
                                            if (mine > 0 && mine < 15)
                                            {
                                                jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":00");
                                            }
                                            else if (mine > 30 && mine < 45)
                                            {
                                                jiaban = Convert.ToDateTime(shiji.ToString("yyyy-MM-dd HH") + ":30");
                                            }
                                            else
                                            {
                                                jiaban = shiji;
                                            }

                                        }

                                        //加班统计                       
                                        if (shijioff > jiaban)
                                        {                                            
                                            //未打卡加班统计为0计算打卡加班时间，不为0不用计算打卡加班，不然就和未打卡加班统计冲突，会计算两遍。
                                            if (totaltime_forget == 0)
                                            {
                                                time_over = time_over + GetOver(jiaban, shijioff);
                                            }
                                           
                                        }
                                    }


                                }
                                //申请加班记录
                                if (dt_time != null && dt_time.Rows.Count > 0)
                                {
                                    //加班
                                    tomr_shijioff = startdate;
                                    DataTable dt_over = new DataView(dt_time) { RowFilter = "applytype in ('travel','gomeet','forget') and starttime>='" + (startdate.ToString("yyyy-MM-dd") + " 06:00:00") + "' and endtime<='" + (tomr_shijioff.AddDays(1).ToString("yyyy-MM-dd") + " 06:00:00") + "' and adduser=" + mem_id + "" }.ToTable();
                                    if (dt_over != null && dt_over.Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in dt_over.Rows)
                                        {
                                            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                            time_over = time_over + totaltime_over;
                                        }
                                    }
                                    //离岗
                                    DataTable dt_addwork = new DataView(dt_time) { RowFilter = "applytype='addwork' and starttime>='" + startdate + "' and starttime<='" + (startdate.ToString("yyyy-MM-dd") + " 23:59:59") + "' and adduser=" + mem_id + "" }.ToTable();
                                    if (dt_addwork != null && dt_addwork.Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in dt_addwork.Rows)
                                        {
                                            decimal totaltime_over = Convert.ToDecimal(dr["totaltime"]);
                                            time_over = time_over - totaltime_over;
                                        }
                                    }
                                }
                                //考勤统计手动修改记录
                                if (asd_list != null && asd_list.Count > 0)
                                {
                                    var data_model = asd_list.Where(d => d.dataSource == "StatisDetail" && d.mem_id == mem_id && d.dataYear == startdate.Year && d.dataMonth == startdate.Month && d.dataDay == startdate.Day && d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                                    if (data_model != null)
                                    {
                                        time_over = data_model.dataValue;
                                    }
                                }
                                monthtime_over = monthtime_over + time_over;
                            }
                        }
                    }

                    //加班统计表手动修改记录              
                    if (asd_list != null && asd_list.Count > 0)
                    {
                        //加班
                        var data_model = asd_list.Where(d => d.dataSource == "MonthSummary" && d.mem_id == mem_id && d.dataYear == enddate.Year && d.dataMonth == enddate.Month && d.dataType == "overtime").OrderByDescending(d => d.id).FirstOrDefault();
                        if (data_model != null)
                        {
                            monthtime_over = data_model.dataValue;
                        }
                        //周加班
                        data_model = asd_list.Where(d => d.dataSource == "MonthSummary" && d.mem_id == mem_id && d.dataYear == enddate.Year && d.dataMonth == enddate.Month && d.dataType == "weektime").OrderByDescending(d => d.id).FirstOrDefault();
                        if (data_model != null)
                        {
                            monthtime_over = (data_model.dataValue) * decimal.Parse("4.28");
                        }
                    }
                }
                sumtime_over = sumtime_over + monthtime_over;
            }

            //无打卡记录需加1
            if (!flag)
            {
                wdkjl = wdkjl + 1;
            }
            return sumtime_over;
        }
        //返回请假时间
        public decimal GetLeave(DataTable dt_leave, DateTime starttime)
        {
            decimal time_leave = 0;
            if (dt_leave != null && dt_leave.Rows.Count > 0)
            {
                //上班时间
                DateTime ontime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 09:00:00");
                //中午下班时间
                DateTime zwxbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 11:50:00");
                //中午上班时间
                DateTime zwsbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 13:00:59");
                //下午下班时间
                DateTime offtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 17:30:00");
                foreach (DataRow dr in dt_leave.Rows)
                {
                    DateTime starttime_leave = Convert.ToDateTime(dr["starttime"]);
                    DateTime endtime_leave = Convert.ToDateTime(dr["endtime"]);
                    decimal totaltime_leave = Convert.ToDecimal(dr["totaltime"]);
                    //请假一天以内
                    if (starttime_leave.ToString("yyyy-MM-dd") == endtime_leave.ToString("yyyy-MM-dd"))
                    {
                        time_leave = time_leave + totaltime_leave;
                    }
                    else //请假日期不在一天内
                    {

                        //分三种情况，开始时间相同，结束时间相同，中间时间也就是7.5小时
                        if (starttime_leave.ToString("yyyy-MM-dd") == starttime.ToString("yyyy-MM-dd"))
                        {
                            if (starttime_leave < ontime)
                            {
                                starttime_leave = ontime;
                            }
                            if (starttime_leave < offtime)
                            {
                                TimeSpan tspan = offtime - starttime_leave;
                                decimal hours = GetHours(tspan);
                                //开始时间不超过12点，需减去1小时中午休息时间
                                if (starttime_leave < zwxbtime)
                                {
                                    hours = hours - 1;
                                }
                                time_leave = time_leave + hours;
                            }

                        }
                        else if (endtime_leave.ToString("yyyy-MM-dd") == starttime.ToString("yyyy-MM-dd"))
                        {
                            //结束时间超过17:30点，按17:30点计算
                            if (endtime_leave > offtime)
                            {
                                endtime_leave = offtime;
                            }
                            if (endtime_leave > ontime)
                            {
                                TimeSpan tspan = endtime_leave - ontime;
                                decimal hours = GetHours(tspan);
                                //结束时间超过13点，需减去1小时中午休息时间
                                if (endtime_leave > zwsbtime)
                                {
                                    hours = hours - 1;
                                }
                                time_leave = time_leave + hours;
                            }
                        }
                        else
                        {
                            time_leave = time_leave + decimal.Parse("7.5");
                        }
                    }
                }
            }
            return time_leave;
        }
        //返回部门活动请假时间
        public decimal GetLeaveDepart(DataTable dt_leave, DateTime starttime, ref decimal sumunittime)
        {
            //22.5减去（当前月之前的数据总和）,剩余每年部门活动带薪小时
            decimal sytime = (decimal.Parse("22.5") - sumunittime);
            decimal time_leave = 0;
            if (dt_leave != null && dt_leave.Rows.Count > 0)
            {
                //上班时间
                DateTime ontime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 09:00");
                //中午下班时间
                DateTime zwxbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 11:50");
                //中午上班时间
                DateTime zwsbtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 13:00");
                //下午下班时间
                DateTime offtime = Convert.ToDateTime(starttime.ToString("yyyy-MM-dd") + " 17:30");
                foreach (DataRow dr in dt_leave.Rows)
                {
                    DateTime starttime_leave = Convert.ToDateTime(dr["starttime"]);
                    DateTime endtime_leave = Convert.ToDateTime(dr["endtime"]);
                    decimal totaltime_leave = Convert.ToDecimal(dr["totaltime"]);
                    //请假一天以内
                    if (starttime_leave.ToString("yyyy-MM-dd") == endtime_leave.ToString("yyyy-MM-dd"))
                    {
                        if (sytime == 0)
                        {
                            time_leave = time_leave + totaltime_leave;
                        }
                        else
                        {
                            //当前天部门活动小时大于剩余小时，即剩余部分小时按事假，小于等于无需处理。
                            if (sytime < totaltime_leave)
                            {
                                time_leave = time_leave + (totaltime_leave - sytime);
                                sytime = 0;
                            }
                            else
                            {
                                sytime = sytime - totaltime_leave;
                            }
                        }


                    }
                    else //请假日期不在一天内
                    {

                        //分三种情况，开始时间相同，结束时间相同，中间时间也就是7.5小时
                        if (starttime_leave.ToString("yyyy-MM-dd") == starttime.ToString("yyyy-MM-dd"))
                        {
                            if (starttime_leave < ontime)
                            {
                                starttime_leave = ontime;
                            }
                            if (starttime_leave < offtime)
                            {
                                TimeSpan tspan = offtime - starttime_leave;
                                decimal hours = GetHours(tspan);
                                //开始时间不超过12点，需减去1小时中午休息时间
                                if (starttime_leave < zwxbtime)
                                {
                                    hours = hours - 1;
                                }

                                if (sytime == 0)
                                {
                                    time_leave = time_leave + hours;
                                }
                                else
                                {
                                    //当前天部门活动小时大于剩余小时，即剩余部分小时按事假，小于等于无需处理。
                                    if (sytime < hours)
                                    {
                                        time_leave = time_leave + (hours - sytime);
                                        sytime = 0;
                                    }
                                    else
                                    {
                                        sytime = sytime - hours;
                                    }
                                }


                            }

                        }
                        else if (endtime_leave.ToString("yyyy-MM-dd") == starttime.ToString("yyyy-MM-dd"))
                        {

                            //结束时间超过17:30点，按17:30点计算
                            if (endtime_leave > offtime)
                            {
                                endtime_leave = offtime;
                            }
                            if (endtime_leave > ontime)
                            {
                                TimeSpan tspan = endtime_leave - ontime;
                                decimal hours = GetHours(tspan);
                                //结束时间超过13点，需减去1小时中午休息时间
                                if (endtime_leave > zwsbtime)
                                {
                                    hours = hours - 1;
                                }

                                //sytime 代表是总22.5小时剩余,sytemptime 代表是每次申请不得超过7.5的剩余小时
                                if (sytime == 0)
                                {
                                    time_leave = time_leave + hours;
                                }
                                else
                                {
                                    if (totaltime_leave > decimal.Parse("7.5"))
                                    {
                                        //排除当前日期，一条申请记录，之前日期请假合计
                                        decimal temptime = totaltime_leave - hours;
                                        decimal sytemptime = 0; //连续申请不能超过7.5小时，之前日期没有超过7.5小时,使用剩余的小时
                                        if (temptime < decimal.Parse("7.5"))
                                        {
                                            sytemptime = decimal.Parse("7.5") - temptime;
                                        }
                                        //有剩余
                                        if (sytemptime > 0)
                                        {
                                            //当前日期小时 大于使用后剩余的小时
                                            if (hours > sytemptime)
                                            {
                                                //当前天部门活动小时大于剩余小时，即剩余部分小时按事假，小于等于无需处理。
                                                if (sytime < sytemptime)
                                                {
                                                    time_leave = time_leave + (hours - sytime);
                                                    sytime = 0;
                                                }
                                                else
                                                {
                                                    sytime = sytime - sytemptime;
                                                    //超过请假7.5小时，请假时间加上去
                                                    time_leave = time_leave + (hours - sytemptime);
                                                }
                                            }
                                            else
                                            {
                                                //这部分应该执行不到，总统计超过7.5，又有剩余小时，当前日期小时肯定会大于使用后剩余的小时
                                                //使用超不过7.5小时
                                                if (sytime < hours)
                                                {
                                                    time_leave = time_leave + (hours - sytime);
                                                    sytime = 0;
                                                }
                                                else
                                                {
                                                    sytime = sytime - hours;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //没有剩余，全部用完
                                            time_leave = time_leave + hours;
                                        }

                                    }
                                    else
                                    {
                                        //使用超不过7.5小时
                                        if (sytime < hours)
                                        {
                                            time_leave = time_leave + (hours - sytime);
                                            sytime = 0;
                                        }
                                        else
                                        {
                                            sytime = sytime - hours;
                                        }
                                    }

                                }

                            }
                        }
                        else
                        {
                            //sytime 代表是总22.5小时剩余,sytemptime 代表是每次申请不得超过7.5的剩余小时
                            if (sytime == 0)
                            {
                                time_leave = time_leave + decimal.Parse("7.5");
                            }
                            else
                            {
                                //第二天是否相同
                                if (starttime_leave.AddDays(1).ToString("yyyy-MM-dd") == starttime.ToString("yyyy-MM-dd"))
                                {

                                    //第一天请假情况,剩余小时不为0，
                                    if (starttime_leave < ontime)
                                    {
                                        starttime_leave = ontime;
                                    }
                                    if (starttime_leave < offtime)
                                    {
                                        TimeSpan tspan = offtime - starttime_leave;
                                        decimal hours = GetHours(tspan);
                                        //开始时间不超过12点，需减去1小时中午休息时间
                                        if (starttime_leave < zwxbtime)
                                        {
                                            hours = hours - 1;
                                        }

                                        decimal sytemptime = 0; //连续申请不能超过7.5小时，之前日期没有超过7.5小时,使用剩余的小时
                                        if (hours < decimal.Parse("7.5"))
                                        {
                                            sytemptime = decimal.Parse("7.5") - hours;
                                        }

                                        //使用7.5小时，有剩余
                                        if (sytemptime > 0)
                                        {
                                            //当前日期是属于申请时间段的中间日期,也就是一天7.5小时
                                            //当前天部门活动小时大于剩余小时，即剩余部分小时按事假，小于等于无需处理。
                                            if (sytime < sytemptime)
                                            {
                                                time_leave = time_leave + (decimal.Parse("7.5") - sytime);
                                                sytime = 0;
                                            }
                                            else
                                            {
                                                sytime = sytime - sytemptime;
                                                //超过请假7.5小时，请假时间加上去
                                                time_leave = time_leave + (decimal.Parse("7.5") - sytemptime);
                                            }

                                        }
                                        else
                                        {
                                            //没有剩余，全部用完
                                            time_leave = time_leave + hours;
                                        }
                                    }
                                }
                                else
                                {
                                    time_leave = time_leave + decimal.Parse("7.5");
                                }
                            }

                        }
                    }


                }
            }
            //22.5小时后还有剩余
            if (sytime > 0)
            {
                //当前日期使用带薪小时
                decimal zytime = (decimal.Parse("22.5") - sumunittime) - sytime;
            }
            else
            {
                sumunittime = decimal.Parse("22.5");
            }
            return time_leave;
        }
    }
}
