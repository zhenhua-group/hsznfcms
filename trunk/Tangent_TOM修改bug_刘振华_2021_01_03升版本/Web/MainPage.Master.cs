﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web
{
    public partial class MainPage : MasterPageBase
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserNo
        {
            get
            {
                return UserSysNo;
            }
        }
        /// <summary>
        /// 登录名
        /// </summary>
        public string UserLogin
        {
            get
            {
                return UserSysLoginName;
            }
        }
        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName
        {
            get
            {
                return UserSysName;
            }
        }

        /// <summary>
        /// 用户权限
        /// </summary>
        public string princiPal
        {
            get
            {
                return Convert.ToString(UserInfo["principal"] ?? "0");
            }
        }
        //待办事项
        public string UserDoneMsgCount
        {
            get
            {
                return new TG.BLL.cm_SysMsg().GetSysMsgDoneCount(UserSysNo).ToString();
            }
        }
        //系统消息
        public string UserSysMsg
        {
            get
            {
                return new TG.BLL.cm_SysMsg().GetSysMsgCount(UserSysNo).ToString();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.HiddenMasterUserSysno.Value = UserSysNo.ToString();
            TG.BLL.tg_member bll = new TG.BLL.tg_member();
            TG.Model.tg_member mem = bll.GetModel(UserSysNo);

            TG.Model.cm_EleSign es = new TG.BLL.cm_EleSign().GetModel2(UserSysNo);
            if (es != null)
            {
                this.lbl_img.ImageUrl = es.elesign;
            }

            if (mem != null)
            {
                loginName.InnerText = mem.mem_Name;
            }
        }
        /// <summary>
        /// 当前路径
        /// </summary>
        public string CurrentPageUrl
        {
            get
            {
                return Request.Url.AbsolutePath;
            }
        }

        /// <summary>
        /// 内容页Url
        /// </summary>
        public string CurrentUrl
        {
            get
            {
                return HttpContext.Current.Request.Url.PathAndQuery.Replace('/', '$').Trim();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsManager
        {
            get
            {
                var bllRole = new TG.BLL.tg_memberRole();
                string strWhere = string.Format(" mem_Id={0} AND RoleIdMag<>0", this.UserSysNo);
                int count = bllRole.GetModelList(strWhere).Count;
                return count > 0 ? true : false;
            }
        }

        public bool IsAdmin
        {
            get
            {
                //查询管理员
                var memList = new TG.BLL.cm_Role().GetRoleUsersList(1);
            
                if (memList.Exists(c => c.mem_ID == this.UserSysNo))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}