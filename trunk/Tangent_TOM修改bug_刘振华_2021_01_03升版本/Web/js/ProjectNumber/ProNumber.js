﻿$(document).ready(function() {
    CommonControl.SetFormWidth();
 
    //隔行变色
    $("#gvpronumber tr:even").css({ background: "White" });
   
    //分配状态
    $(".apply").each(function(i) {
        var flag = $(this).next(":hidden").val();
        if (flag == "1") {
            $(this).attr("href", "###");
            $(this).text("已完成");
            $(this).click(function() {
                alert("项目已策划审批通过，项目信息已在TPM中生成！");
            });
        }
    });

    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#btn_Search"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();
    //提示
    $(".cls_column").tipsy({ opacity: 1 });
})