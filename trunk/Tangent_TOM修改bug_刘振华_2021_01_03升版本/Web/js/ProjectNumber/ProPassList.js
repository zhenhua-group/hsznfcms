﻿
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var proID;
var userObj;
var startAppControl; //发起审批对象

$(document).ready(function () {
    CommonControl.SetFormWidth();
    // Tabs
    $('#tabs').tabs({ cookie: { expires: 30} });
    //隔行变色
    $("#gvpropass tr:even").css({ background: "#f0f0f0" });
    //隔行变色
    $("#gvpronumber tr:even").css({ background: "#f0f0f0" });
   
    //绑定数据
    $(".apply").each(function (i) {
        var flag = $(this).next(":hidden").val();
        if (flag == "1") {
            $(this).hide();
            $(this).nextAll("a.process").show();
            $(this).nextAll("a.done").hide();
        }
        else if (flag == "2") {
            $(this).hide();
            $(this).nextAll("a.process").hide();
            $(this).nextAll("a.done").show();
        }
    });
    //提交申请
    $(".apply").live("click", function () {
        var pro_id = $(this).attr("rel");
        var user = $("#applyuser").val();
        proID = pro_id;
        userObj = user;
        startAppControl = $(this);
        getUserAndUpdateAudit('0', '0', pro_id, user);
    });
    //实例化类容
    messageDialog = $("#msgReceiverContainer").messageDialog({
        "button": {
            "发送消息": function () {
                //选中用户
                var _$mesUser = $(":checkbox[name=messageUser]:checked");

                if (_$mesUser.length == 0) {
                    alert("请至少选择一个流程审批人！");
                    return false;
                }

                getUserAndUpdateAudit('0', '1', proID, userObj);
            },
            "关闭": function () {
                messageDialog.hide();
            }
        }
    });
    sendMessageClass = new MessageCommon(messageDialog);

    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#btn_Search"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });
    //审批轨迹

    $(".allot").click(function () {
        var pro_id = $(this).attr("rel");
        $('#spgj').dialog('open');
        $('.ui-widget-overlay').css("display", "block");
        $.ajax({
            type: "POST",
            url: "../../HttpHandler/ProNumber/ProAuditLocus.ashx",
            data: "audit_No=" + pro_id,
            success: function (msg) {
                var dataArray = msg.split('|');
                var auditDateArray = dataArray[0] = dataArray[0].substring(0, dataArray[0].length - 1).split(",");
                var SuggestionArray = dataArray[1].substring(0, dataArray[1].length - 1).split(",");
                var userArray = dataArray[2].substring(0, dataArray[2].length - 1).split(",");
                var json = "";
                for (var i = 0; i < auditDateArray.length; i++) {
                    json += "<tr><td>" + auditDateArray[i] + "</td><td>" + SuggestionArray[i] + "</td><td>" + userArray[i] + "</td></tr>";
                }

                var table = $("#audit");
                table.append(json);
            },
            eror: function (XMLHttpRequest, textStatus, errorThrowm) {
                alert("错误");
            }
        });
    });



    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();
    //提示
    $(".cls_column").tipsy({ opacity: 1 });
});
//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, proid, user) {
    //地址
    var url = "../../HttpHandler/ProNumber/NumApplyHandler.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "pro_id": proid, "user": user };
    //提交数据
    $.post(url, data, function (jsonResult) {
        if (jsonResult == "0") {
            alert("流程审批错误，请联系管理员！");
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息回调方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        messageDialog.hide();
        //提示消息
        alert("发起工程号分配申请已成功！\n消息已成功发送到审批人等待审批！");
        //改变流程提示状态
        CommonControl.SetApplyStatus(startAppControl);
    } else {
        alert("消息发送失败！");
    }
}
