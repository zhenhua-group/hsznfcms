﻿$(document).ready(function () {
    CommonControl.SetFormWidth();

    //行变色
    $(".show_project>tbody>tr:odd").attr("style", "background-color:#FFF");
    //项目阶段
    var jd = $("#jdlist").val();
    if (jd) {
        if ($.trim(jd).indexOf(',') > -1) {
            var array = jd.split(',');
            for (var i = 0; i < array.length; i++) {
                if ($.trim(array[i]) == "方案设计") {
                    $("#Checkbox1").attr("checked", "checked");
                }
                else if ($.trim(array[i]) == "初设设计") {
                    $("#Checkbox2").attr("checked", "checked");
                }
                else if ($.trim(array[i]) == "施工图设计") {
                    $("#Checkbox3").attr("checked", "checked");
                }
            }
        }
        else {
            if ($.trim(jd) == "方案设计") {
                $("#Checkbox1").attr("checked", "checked");
            }
            else if ($.trim(jd) == "初设设计") {
                $("#Checkbox2").attr("checked", "checked");
            }
            else if ($.trim(jd) == "施工图设计") {
                $("#Checkbox3").attr("checked", "checked");
            }
        }
    }
    //级别
    var jb = $("#projb").val();
    if (jb) {
        if (jb == "1") {
            $("#Radio1").attr("checked", "checked");
        }
        else if (jb == "2") {
            $("#Radio2").attr("checked", "checked");
        }
    }
    //项目类型
    var protype = $("#typelist").val();
    if (protype) {
        if ($.trim(protype).indexOf(',') > -1) {
            var array = protype.split(',');
            for (var i = 0; i < array.length; i++) {
                if ($.trim(array[i]) == "建筑工程") {
                    $("#Checkbox4").attr("checked", "checked");
                }
                else if ($.trim(array[i]) == "城乡规划") {
                    $("#Checkbox5").attr("checked", "checked");
                }
                else if ($.trim(array[i]) == "市政工程") {
                    $("#Checkbox6").attr("checked", "checked");
                }
                else if ($.trim(array[i]) == "工程勘察") {
                    $("#Checkbox7").attr("checked", "checked");
                }
                else if ($.trim(array[i]) == "咨询") {
                    $("#Checkbox8").attr("checked", "checked");
                }
                else if ($.trim(array[i]) == "其他") {
                    $("#Checkbox9").attr("checked", "checked");
                }
            }
        }
        else {
            if ($.trim(protype) == "建筑工程") {
                $("#Checkbox4").attr("checked", "checked");
            }
            else if ($.trim(protype) == "城乡规划") {
                $("#Checkbox5").attr("checked", "checked");
            }
            else if ($.trim(protype) == "市政工程") {
                $("#Checkbox6").attr("checked", "checked");
            }
            else if ($.trim(protype) == "工程勘察") {
                $("#Checkbox7").attr("checked", "checked");
            }
            else if ($.trim(protype) == "咨询") {
                $("#Checkbox8").attr("checked", "checked");
            }
            else if ($.trim(protype) == "其他") {
                $("#Checkbox9").attr("checked", "checked");
            }
        }
    }

    $("#btnok").click(function () {
        var pronumber = $("#proNumber").val();
        //填写项目工号
        var msg = "";
        if (pronumber == "") {
            msg += "项目工号不能为空！</br >";
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        else {
            return true;

        }

    });
    //编号赋予
    $("#btnfygh").click(function () {
        /*
        var url = "../../ProjectNumber/SelectProjectNum.aspx";
        var feature = "dialogWidth:310px;dialogHeight:130px;center:yes";
        var result = window.showModalDialog(url, "", feature);
        if (!result) {
        result = window.returnValue;
        }
        if ($.trim(result) != "") {
        $("#proNumber").val($.trim(result));
        }*/
        //赋值
        showDivDialogClass.SetParameters({
            "pageSize": "0"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "0", "proNum", CprNumUnitCallBack);
        $("#cpr_Number").dialog({
            autoOpen: false,
            modal: true,
            width: 340,
            resizable: false,
            title: "选择工程编号"
        }).dialog("open");

    });
    //返回
    $("#btncancle").click(function () {
        location.href = "../../ProjectNumber/ProNumber.aspx";
    });

    //分配框状态判断
    if ($.trim($("#proNumber").val()) == "") {
        $("#btn_modify").hide();
        $("#btnfygh").show();
    }
    else {
        $("#btnfygh").hide();
        $("#btn_modify").show();
        $("#proNumber").removeClass();
        $("#proNumber").css({ "border": "none", "background-color": "#f0f0f0", "text-align": "center" });
    }
    //事件
    $("#btn_modify").toggle(function () {
        $(this).text("放弃");
        $("#btnfygh").show();
        $("#proNumber").addClass("TextBoxBorder");
        $("#proNumber").css({ "border": "solid #cccccc 1px", "background-color": "", "text-align": "" });
    }, function () {
        $(this).text("修改");
        $("#btnfygh").hide();
        $("#proNumber").removeClass();
        $("#proNumber").css({ "border": "none", "background-color": "#f0f0f0", "text-align": "center" });
    });
})
//合同编号
function CprNumUnitCallBack(result) {
        var data = result == null ? "" : result.ds;
        var cpr_typeSelect_htm = '<option value="-1">--------请选择--------</option>';
        $.each(data, function (i, n) {
            cpr_typeSelect_htm += '<option value="' + n.ID + '">' + n.ProType + '</option>';
        });
        $("#cpr_typeSelect").html(cpr_typeSelect_htm);
        $('#cpr_numSelect').empty();
    //注册合同类型选项改变的事件
    $("#cpr_typeSelect").unbind('change').change(function () {
        $("#noselectMsg").hide();
        var proTypeID = $(this).val();
        if (proTypeID > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", proTypeID, "false", "0", "proNum", CprNumCallBack);
        } else {
            $('#cpr_numSelect').empty();
        }

    });
    //注册确定按钮点击事件
    $("#btn_cprNum_close").unbind('click').click(function () {
        var CprNum = $("#cpr_numSelect").find("option:selected").text();
        if (CprNum != NaN && CprNum != "" && CprNum != undefined) {
            $("#noselectMsg").hide();
            $("#proNumber").val(CprNum);
            $("#cpr_Number").dialog().dialog("close");
        } else {
            $("#noselectMsg").show();
        }
    });
}
function CprNumCallBack(result) {
    var obj = result == null ? "" : result.ds;
    var cpr_TypeSelectChangeHtml;
    var prevTxt;//前缀
    var mark;//连接符
    var startNum; //开始编号
    var endNum ; //结束编号
    $.each(obj, function (i, n) {
        startNum = n.StartNum;
        endNum = n.EndNum;
        prevTxt = n.PreFix;
        mark = n.Mark;
        for (var i = startNum; i <= endNum; i++) {
            var cprNumOk = prevTxt + mark + GetCprNumFix(i);
            cpr_TypeSelectChangeHtml += '<option cprNum="' + cprNumOk + '" value="' + GetCprNumFix(i) + '">' + cprNumOk + '</option>';
        }
    });
    $('#cpr_numSelect').empty();
    $('#cpr_numSelect').html(cpr_TypeSelectChangeHtml);
    //去除已使用的编号
    BindCprNumber(prevTxt+mark);
}
//处理合同编号数字
function GetCprNumFix(cprNo) {
    if (cprNo < 10) {
        return "00" + cprNo;
    } else if (10 <= cprNo && cprNo < 100) {
        return "0" + cprNo;
    } else { return cprNo; }
}
//去除已使用的合同编号
function BindCprNumber(fixstr) {
    var data = "action=removeUsedCprNum&type=1&fixStr=" + fixstr;
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var obj = result.ds;
                $.each(obj, function (i, n) {
                    $("#cpr_numSelect option[cprNum=" + n.ProNumber + "]").remove();
                });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!未能去除已使用编号!");
        }
    });
}

