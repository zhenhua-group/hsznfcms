﻿$(document).ready(function () {

    CommonControl.SetFormWidth();
    
    //隔行变色
    $("#gv_Coperation tr:even").css({ background: "White" });

    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#btn_Search"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });

    //提示
    $(".cls_column").tipsy({ opacity: 1 });
});