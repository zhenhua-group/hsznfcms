﻿$(document).ready(function () {

    //Grid 高度指定
    $("#grid_allot tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#grid_allot tr:even").css({ background: "White" });

    var formwidth = $("#form1").width();
    if (formwidth < 1300) {
        $("#form1").addClass("cls_form_width");
    }

    $("#btn_ok").click(function () {
        if ($("#drp_unit").get(0).selectedIndex == 0) {
            if ($("#hiddenischeckallpower").val() == "0") {
                alert('没有查看全部生产部门权限，请选择本部门查看！');
                return false;
            }
        }
    });
    $("#ctl00_ContentPlaceHolder1_btn_Search").live("click", function () {
      
        //var unit = getCheckedUnitNodes();
       // var unitID = "";
      
        //var unitSpans = $("span[id=userSpan]", $("#ctl00_ContentPlaceHolder1_labUnit"));
        //for (var i = 0; i < unitSpans.length; i++) {
        //    unit += "'" + $.trim($(unitSpans[i]).attr("unitname")) + "',";
        //    unitID += "" + $.trim($(unitSpans[i]).attr("usersysno")) + ",";
        //}
       // $("#ctl00_ContentPlaceHolder1_HiddenlabUnit").val(unit);
        //$("#ctl00_ContentPlaceHolder1_HiddenlabUnitID").val(unitID);
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        var doneSignnum1 = $("#ctl00_ContentPlaceHolder1_doneSignnum1").val();
        var doneSignnum2 = $("#ctl00_ContentPlaceHolder1_doneSignnum2").val();
        if (doneSignnum1 != "") {
            if (!reg.test(doneSignnum1)) {
                msg += " 已签订合同数开始值格式错误，请输入数字！</br>";
            }
        }
        if (doneSignnum2 != "") {
            if (!reg.test(doneSignnum2)) {
                msg += "已签订合同数结束值格式错误，请输入数字！</br>";
            }
        }
        var doneChargenum1 = $("#ctl00_ContentPlaceHolder1_doneChargenum1").val();
        var doneChargenum2 = $("#ctl00_ContentPlaceHolder1_doneChargenum2").val();
        if (doneChargenum1 != "") {
            if (!reg.test(doneChargenum1)) {
                msg += " 已收款合同数开始值格式错误，请输入数字！</br>";
            }
        }
        if (doneChargenum2 != "") {
            if (!reg.test(doneChargenum2)) {
                msg += "已收款合同数结束值格式错误，请输入数字！</br>";
            }
        }
        var nodoneChargenum1 = $("#ctl00_ContentPlaceHolder1_nodoneChargenum1").val();
        var nodoneChargenum2 = $("#ctl00_ContentPlaceHolder1_nodoneChargenum2").val();
        if (nodoneChargenum1 != "") {
            if (!reg.test(nodoneChargenum1)) {
                msg += " 未收款合同数开始值格式错误，请输入数字！</br>";
            }
        }
        if (nodoneChargenum2 != "") {
            if (!reg.test(nodoneChargenum2)) {
                msg += "未收款合同数结束值格式错误，请输入数字！</br>";
            }
        }
        var copArea1 = $("#ctl00_ContentPlaceHolder1_copArea1").val();
        var copArea2 = $("#ctl00_ContentPlaceHolder1_copArea2").val();
        if (copArea1 != "") {
            if (!reg.test(copArea1)) {
                msg += " 合同面积开始值格式错误，请输入数字！</br>";
            }
        }
        if (copArea2 != "") {
            if (!reg.test(copArea2)) {
                msg += "合同面积结束值格式错误，请输入数字！</br>";
            }
        }
        var copTarget1 = $("#ctl00_ContentPlaceHolder1_copTarget1").val();
        var copTarget2 = $("#ctl00_ContentPlaceHolder1_copTarget2").val();
        if (copTarget1 != "") {
            if (!reg.test(copTarget1)) {
                msg += "合同目标值开始值格式错误，请输入数字！</br>";
            }
        }
        if (copTarget2 != "") {
            if (!reg.test(copTarget2)) {
                msg += "合同目标值结束值格式错误，请输入数字！</br>";
            }
        }
        var copAcount1 = $("#ctl00_ContentPlaceHolder1_copAcount1").val();
        var copAcount2 = $("#ctl00_ContentPlaceHolder1_copAcount2").val();
        if (copAcount1 != "") {
            if (!reg.test(copAcount1)) {
                msg += "合同总额开始值格式错误，请输入数字！</br>";
            }
        }
        if (copAcount2 != "") {
            if (!reg.test(copAcount2)) {
                msg += "合同总额结束值格式错误，请输入数字！</br>";
            }
        }
        var chargeTarget1 = $("#ctl00_ContentPlaceHolder1_chargeTarget1").val();
        var chargeTarget2 = $("#ctl00_ContentPlaceHolder1_chargeTarget2").val();
        if (chargeTarget1 != "") {
            if (!reg.test(chargeTarget1)) {
                msg += "产值目标值开始值格式错误，请输入数字！</br>";
            }
        }
        if (chargeTarget2 != "") {
            if (!reg.test(chargeTarget2)) {
                msg += "产值目标值结束值格式错误，请输入数字！</br>";
            }
        }
        var doneChargeacount1 = $("#ctl00_ContentPlaceHolder1_doneChargeacount1").val();
        var doneChargeacount2 = $("#ctl00_ContentPlaceHolder1_doneChargeacount2").val();
        if (doneChargeacount1 != "") {
            if (!reg.test(doneChargeacount1)) {
                msg += "已收费额开始值格式错误，请输入数字！</br>";
            }
        }
        if (doneChargeacount2 != "") {
            if (!reg.test(doneChargeacount2)) {
                msg += "已收费额结束值格式错误，请输入数字！</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    var $tr = "";
    //不显示隐藏部门行
    $("#ctl00_ContentPlaceHolder1_grid_allot tbody tr:not(:last)").each(function (i) {
        var tr_obj = $(this);
        var text = tr_obj.find("td").eq(0).text();
        //把生产经营部放到最后             
        if ($.trim(text) == "生产经营部") {
            $tr = tr_obj.clone();
            tr_obj.remove();
        }
        //合同额完成比例
        var cprprt = tr_obj.find("td").eq(7).text();
        if (parseFloat(cprprt) >= 100) {
            tr_obj.find("td").eq(7).css({ color: "red" });
        }
        //收费完成比例
        var allotPrt = tr_obj.find("td").eq(10).text();
        if (parseFloat(allotPrt) >= 100) {
            tr_obj.find("td").eq(10).css({ color: "red" });
        }
    });
    $("#ctl00_ContentPlaceHolder1_grid_allot tbody tr:last").before($tr);
    //隐藏查询条件
    $("#tbl_id2").children("tbody").children("tr").hide();
    //没有用开始
    //点击选择条件
    $("#btn_chooes").click(function () {
        var labtext = $("#ctl00_ContentPlaceHolder1_labUnit").text();
        var chooesUnit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var chooesUnitVal = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").val();
        var userSpans = $("span[id=userSpan]", $("#ctl00_ContentPlaceHolder1_labUnit"));
        //循环判断是否有重复用户的存在
        for (var i = 0; i < userSpans.length; i++) {
            if ($(userSpans[i]).attr("usersysno") == chooesUnitVal) {
                return false;
            }
            if ($(userSpans[i]).attr("usersysno") == "-1") {
                $("#ctl00_ContentPlaceHolder1_labUnit").empty();
            }
        }

        if (chooesUnitVal == "-1") {
            $("#ctl00_ContentPlaceHolder1_labUnit").empty();
            $("#ctl00_ContentPlaceHolder1_labUnit").append("<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"" + chooesUnitVal + "\" unitname=\"" + chooesUnit + "\">全院部门<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
        } else {
            $("#ctl00_ContentPlaceHolder1_labUnit").append("<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"" + chooesUnitVal + "\" unitname=\"" + chooesUnit + "\">" + chooesUnit + "<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
        }
        var unit = "";
        var unitID = "";
        var unitSpans = $("span[id=userSpan]", $("#ctl00_ContentPlaceHolder1_labUnit"));
        for (var i = 0; i < unitSpans.length; i++) {
            unit += "'" + $.trim($(unitSpans[i]).attr("unitname")) + "',";
            unitID += "" + $.trim($(unitSpans[i]).attr("usersysno")) + ",";
        }
      //  $("#ctl00_ContentPlaceHolder1_HiddenlabUnit").val(unit);
       // $("#ctl00_ContentPlaceHolder1_HiddenlabUnitID").val(unitID);
    });

    $("img[id=deleteUserlinkButton]").live("click", function () {
        if (confirm("确认要删除这个部门吗？")) {
            //删除用户
            $(this).parent("span[id=userSpan]:first").remove();
            var unit = "";
            var unitID = "";
            var unitSpans = $("span[id=userSpan]", $("#ctl00_ContentPlaceHolder1_labUnit"));
            for (var i = 0; i < unitSpans.length; i++) {
                unit += "'" + $.trim($(unitSpans[i]).attr("unitname")) + "',";
                unitID += "" + $.trim($(unitSpans[i]).attr("usersysno")) + ",";
            }
          //  $("#ctl00_ContentPlaceHolder1_HiddenlabUnit").val(unit);
           // $("#ctl00_ContentPlaceHolder1_HiddenlabUnitID").val(unitID);
        }
        return false;
    });
    //没用用结束
    $("input[name=bb]:checkbox", $("#tbl_id")).click(function () {
        //条件名称
        var commName = $.trim($(this).get(0).id);
        //获取数据库字段名      
        var columnsname = $.trim($(this).parent().parent().siblings("span").attr("for"));
        //获取隐藏域的名
        //获取表头字段名
        var columnsChinaName = $.trim($(this).parent().parent().siblings("span").text());
        if ($(this).is(":checked")) {
            $("#tbl_id2 tr[for=" + columnsname + "]").show();
            $(this).parent().parent().parent().find(":last").val("1");
        }
        else {
            //判断执行设总和甲方负责人 
            $("#tbl_id2 tr[for=" + columnsname + "]").hide();
            $("#tbl_id2 tr[for=" + columnsname + "]").removeData();
            if (columnsname != "cpr_time") {
                $(":text", "#tbl_id2 tr[for=" + columnsname + "]").val("");
            }
            $("select", "#tbl_id2 tr[for=" + columnsname + "]").val("-1");
            $(this).parent().parent().parent().find(":last").val("0");
        }
        //有条件显示查询按钮
        if ($("input[name=bb]:checkbox:checked", $("#tbl_id")).length > 0) {
            $("#tbl_id2 tr:last").show();
        }
        else {
            $("#tbl_id2 tr:last").hide();
           // window.location.reload();
        }
    });
});
