﻿$(function () {
    //隐藏查询条件
    $("#tbl_id2").children("tbody").children("tr").hide();
    //点击选择条件
    $("input[name=bb]:checkbox", $("#tbl_id")).click(function () {
        //条件名称
        var commName = $.trim($(this).get(0).id);
        //获取数据库字段名      
        var columnsname = $.trim($(this).parent().parent().siblings("span").attr("for"));
        //获取表头字段名
        var columnsChinaName = $.trim($(this).parent().parent().siblings("span").text());
        if ($(this).is(":checked")) {
            $("#tbl_id2 tr[for=" + commName + "]").show();
        }
        else {
            //判断执行设总和甲方负责人 
            $("#tbl_id2 tr[for=" + commName + "]").hide();
            $("#tbl_id2 tr[for=" + commName + "]").removeData();
            $(":text", "#tbl_id2 tr[for=" + commName + "]").val("");
            $("select", "#tbl_id2 tr[for=" + commName + "]").val("-1");
            if (commName == "cpr_Unit") {
                $("#labUnit").empty();
            }
        }
        //有条件显示查询按钮
        if ($("input[name=bb]:checkbox:checked", $("#tbl_id")).length > 0) {
            $("#tbl_id2 tr:last").show();
        }
        else {
            $("#tbl_id2 tr:last").hide();
            $(":text", "#tbl_id2 tr[for=" + commName + "]").val("");
            $("select", "#tbl_id2 tr[for=" + commName + "]").val("-1");
            if (commName == "cpr_Unit") {
                $("#labUnit").empty();
            }
            var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
            //var unit = $("#ctl00_ContentPlaceHolder1_drp_unitLone").find("option:selected").text();
            //unit = "'" + unit + "',";
            var unit = GetUnitNameNode();
            var year1 = $("#ctl00_ContentPlaceHolder1_drp_year1").val();
            var params = {
                "strwhere": strwhere,
                "unit": unit,
                "year1": year1,
                "year2": "",
                "txt_proName": "",
                "txt_buildUnit": "",
                "txt_Aperson": "",
                "txtproAcount": "",
                "txtproAcount2": "",
                "txt_signdate": "",
                "txt_signdate2": "",
                "txt_finishdate": "",
                "txt_finishdate2": "",
                "ddrank": ""
            };
            $("#jqGrid").jqGrid('setGridParam', {
                url: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx?action=selAllot",
                postData: params,
                page: 1

            }).trigger("reloadGrid");
        }
    });
    $("#btn_chooes").click(function () {
        var labtext = $("#labUnit").text();
        var chooesUnit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var chooesUnitVal = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").val();
        var userSpans = $("span[id=userSpan]", $("#labUnit"));
        //循环判断是否有重复用户的存在
        for (var i = 0; i < userSpans.length; i++) {
            if ($(userSpans[i]).attr("usersysno") == chooesUnitVal) {
                return false;
            }
            if ($(userSpans[i]).attr("usersysno") == "-1") {
                $("#labUnit").empty();
            }
        }

        if (chooesUnitVal == "-1") {
            $("#labUnit").empty();
            $("#labUnit").append("<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"" + chooesUnitVal + "\" unitname=\"" + chooesUnit + "\">全院部门<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
        } else {
            $("#labUnit").append("<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"" + chooesUnitVal + "\" unitname=\"" + chooesUnit + "\">" + chooesUnit + "<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
        }
    });
    $("img[id=deleteUserlinkButton]").live("click", function () {
        if (confirm("确认要删除这个部门吗？")) {
            //删除用户
            $(this).parent("span[id=userSpan]:first").remove();
        }
        return false;
    });
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectMamage/ProjectListHandler.ashx?action=selAllot&year1=' + $("#ctl00_ContentPlaceHolder1_drp_year1").val() + '&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '甲方负责人', '工程名称','承接部门', '建筑分类', '建设单位', '合同额(万元)', '开始时间', '结束时间', '分配详情'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'ChgJia', index: 'ChgJia', width: 100, align: 'center' },
                             { name: 'pro_name', index: 'pro_name', width: 250, formatter: colNameShowFormatter },
                              { name: 'Unit', index: 'Unit', width: 60, align: 'center' },
                             { name: 'BuildType', index: 'BuildType', width: 60, align: 'center' },
                             { name: 'pro_buildUnit', index: 'pro_buildUnit', width: 250 },
                             { name: 'Cpr_Acount', index: 'Cpr_Acount', width: 80, align: 'center' },
                             { name: 'qdrq', index: 'pro_startTime', width: 80, align: 'center' },
                             { name: 'wcrq', index: 'pro_finishTime', width: 80, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', width: 100, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'p.pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });




    //查询
    $("#btn_Search").click(function () {
        //查询按钮值更改
        $("#ctl00_ContentPlaceHolder1_SelectType").val("1");
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = getCheckedUnitNodes();
        //var unitSpans = $("span[id=userSpan]", $("#labUnit"));
        //for (var i = 0; i < unitSpans.length; i++) {
        //    unit += "'" + $(unitSpans[i]).attr("unitname") + "',";
        //}
        $("#ctl00_ContentPlaceHolder1_HiddenUnit").val(unit);
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var year2 = $("#ctl00_ContentPlaceHolder1_drp_year2").val();
        var txt_proName = $("#ctl00_ContentPlaceHolder1_txt_proName").val(); //项目名称
        var txt_buildUnit = $("#ctl00_ContentPlaceHolder1_txt_buildUnit").val(); //建设单位
        var txt_Aperson = $("#ctl00_ContentPlaceHolder1_txt_Aperson").val(); //甲方负责人
        var txtproAcount = $("#ctl00_ContentPlaceHolder1_txtproAcount").val(); //合同额
        var txtproAcount2 = $("#ctl00_ContentPlaceHolder1_txtproAcount2").val();
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate").val(); //开始时间
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate2").val();
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val(); //完成时间
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate2").val();
        var ddrank = $("#ctl00_ContentPlaceHolder1_ddrank").find("option:selected").text(); //建筑类别
        //数字验证正则    
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        if (txtproAcount != "") {
            if (!reg.test(txtproAcount)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtproAcount2 != "") {
            if (!reg.test(txtproAcount2)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        var params = {
            "strwhere": strwhere,
            "unit": unit,
            "year": year,
            "year1":$("#ctl00_ContentPlaceHolder1_drp_year1").val(),
            "year2": year2,
            "txt_proName": txt_proName,
            "txt_buildUnit": txt_buildUnit,
            "txt_Aperson": txt_Aperson,
            "txtproAcount": txtproAcount,
            "txtproAcount2": txtproAcount2,
            "txt_signdate": txt_signdate,
            "txt_signdate2": txt_signdate2,
            "txt_finishdate": txt_finishdate,
            "txt_finishdate2": txt_finishdate2,
            "ddrank": ddrank
        };
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx?action=selAllot&cxtype=gj",
            postData: params,
            page: 1

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#btn_cx").click(function () {
        //查询按钮值更改
        $("#ctl00_ContentPlaceHolder1_SelectType").val("0");
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //var unit = $("#ctl00_ContentPlaceHolder1_drp_unitLone").find("option:selected").text();
        //unit = "'" + unit + "',";
        var unit = GetUnitNameNode();
        var year1 = $("#ctl00_ContentPlaceHolder1_drp_year1").val();
        var params = {
            "strwhere": strwhere,
            "unit": unit,
            "year1": year1,
            "year2": "",
            "txt_proName": "",
            "txt_buildUnit": "",
            "txt_Aperson": "",
            "txtproAcount": "",
            "txtproAcount2": "",
            "txt_signdate": "",
            "txt_signdate2": "",
            "txt_finishdate": "",
            "txt_finishdate2": "",
            "ddrank": ""
        };
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx?action=selAllot",
            postData: params,
            page: 1

        }).trigger("reloadGrid");
    });
    $("#ctl00_ContentPlaceHolder1_drp_year1").change(function () {
        $("#btn_cx").click();
        //var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //var unit = $("#ctl00_ContentPlaceHolder1_drp_unitLone").find("option:selected").text();
        //var year1 = $("#ctl00_ContentPlaceHolder1_drp_year1").val();
        //unit = "'" + unit + "',";
        //var params = {
        //    "strwhere": strwhere,
        //    "unit": unit,
        //    "year1": year1,
        //    "year2": "",
        //    "txt_proName": "",
        //    "txt_buildUnit": "",
        //    "txt_Aperson": "",
        //    "txtproAcount": "",
        //    "txtproAcount2": "",
        //    "txt_signdate": "",
        //    "txt_signdate2": "",
        //    "txt_finishdate": "",
        //    "txt_finishdate2": "",
        //    "ddrank": ""
        //};
        //$("#jqGrid").jqGrid('setGridParam', {
        //    url: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx?action=selAllot",
        //    postData: params,
        //    page: 1

        //}).trigger("reloadGrid");
    });
});
//得到树选中的部门
function GetUnitNameNode() {
    var unitnamestr = $("#ctl00_ContentPlaceHolder1_drpunit_divDropdownTreeText").text();
    var unitstr = "";
    if ($.trim(unitnamestr) != "") {
        var unitnamelist = unitnamestr.split(",");
        for (var name in unitnamelist) {
            if ($.trim(unitnamelist[name]) != "全院部门") {
                unitstr += "'" + $.trim(unitnamelist[name]) + "',";
            }
        }
       
    }
  
    return unitstr;
}
//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var year1 = $("#ctl00_ContentPlaceHolder1_drp_year1").val();
    var pageurl = "/ProjectValueandAllot/ProjectValueAllotDetailsBymaster.aspx?proid=" + celvalue + "&year=" + year1 + "";
    return '<a href="' + pageurl + '" alt="查看分配详情" >查看分配详情</a>';

}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}
//获取选中的部门
var getCheckedUnitNodes = function () {

    var unitlist = $("#ctl00_ContentPlaceHolder1_drpunit_divDropdownTreeText").text();
    //temp var
    var unit = "";
    if ($.trim(unitlist) != "") {
        //部门数组
        var unitarr = unitlist.split(',');
        //得到部门
        for (var index in unitarr) {
            if ($.trim(unitarr[index]) != "全院部门") {
                unit += "'" + $.trim(unitarr[index]) + "',";
            }
        }
    }
    return unit;
}