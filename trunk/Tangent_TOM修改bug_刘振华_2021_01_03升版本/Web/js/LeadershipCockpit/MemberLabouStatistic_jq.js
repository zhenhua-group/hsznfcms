﻿$(function () {


    //详细 返回结果
    var strChk = $("#hid_checkbox").val();
    if (strChk != "") {

        var chk = strChk.substring(0, strChk.length - 1);
        var chkList = chk.split(',');
        for (i = 0; i < chkList.length; i++) {
            var clo = chkList[i];
            $("input[type=checkbox][clo=" + clo + "]", "#tb_check").attr("checked", "checked");
            $("input[type=checkbox][clo=" + clo + "]", "#tb_check").parent().attr("class", "checked");
            $("#tb_select tr:eq(" + parseInt(clo) + ")").show();
        }
        //有条件显示查询按钮
        if ($("input[name=bb]:checkbox:checked", $("#tb_check")).length > 0) {
            $("#tb_select tr:last").show();
        }
        else {
            $("#tb_select tr:last").hide();
            $(":text", "#tb_select tr:eq(" + clo + ")").val("");
            $("select", "#tb_select tr:eq(" + clo + ")").val("-1");
        }
    }

    $("#jqGrid").jqGrid({
        url: '/HttpHandler/LeadershipCockpit/MemberLabouStatisticHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '姓名', '性别', '单位', '专业', '参与项目总数', '进行中项目总数', '完工项目总数', '暂停项目总数', '查看'],
        colModel: [
                             { name: 'id', index: 'id', width: 30, align: 'center', sorttable: false },
                             { name: 'mem_ID', index: 'mem_ID', hidden: true, editable: true },
                             { name: 'mem_Name', index: 'mem_Name', width: 150 },
                             { name: 'mem_Sex', index: 'mem_Sex', width: 100, formatter: colShowMemFormatter },
                             { name: 'unit_Name', index: 'unit_Name', width: 200, align: 'center' },
                             { name: 'spe_Name', index: 'spe_Name', width: 100, align: 'center' },
                             { name: 'TotalCount', index: 'TotalCount', width: 100, align: 'center', formatter: selectLabouFormatter },
                             { name: 'JinXingCount', index: 'JinXingCount', width: 100, align: 'center', formatter: selectLabouFormatter1 },
                             { name: 'WanGongCount', index: 'WanGongCount', width: 100, align: 'center', formatter: selectLabouFormatter2 },
                             { name: 'ZantingCount', index: 'ZantingCount', width: 100, align: 'center', formatter: selectLabouFormatter3 },
                             { name: 'mem_ID', index: 'mem_ID', width: 60, align: 'center', sorttable: false, editable: false, formatter: selectLabouDetailFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: {
            "action": "statistic", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
            "unit": $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text(),
            "keyname": $("#ctl00_ContentPlaceHolder1_txt_keyname").val(),
            "spename": $("#ctl00_ContentPlaceHolder1_txt_SpeName").val(),
            "signdate": $("#ctl00_ContentPlaceHolder1_txt_signdate").val(),
            "finishdate": $("#ctl00_ContentPlaceHolder1_txt_finishdate").val(),
            "projectCount1": $("#ctl00_ContentPlaceHolder1_txt_ProjectCount1").val(),
            "projectCount2": $("#ctl00_ContentPlaceHolder1_txt_ProjectCount2").val(),
            "jinXingCount1": $("#ctl00_ContentPlaceHolder1_txt_ProjectJinXingCount1").val(),
            "jinXingCount2": $("#ctl00_ContentPlaceHolder1_txt_ProjectJinXingCount2").val(),
            "zanTingCount1": $("#ctl00_ContentPlaceHolder1_txt_ProjectZanTingCount1").val(),
            "zanTingCount2": $("#ctl00_ContentPlaceHolder1_txt_ProjectZanTingCount2").val(),
            "wangongCount1": $("#ctl00_ContentPlaceHolder1_txt_ProjectWanGongCount1").val(),
            "wangongCount2": $("#ctl00_ContentPlaceHolder1_txt_ProjectWanGongCount2").val()
        },
        loadonce: false,
        sortname: 'mem_ID',
        sortorder: 'asc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/LeadershipCockpit/MemberLabouStatisticHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        loadComplete: loadCompMethod
    });


    //列点击全部
    $(":checkbox:first", $("#columnsid")).click(function () {

        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid").showCol($(value).val());
            });
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid").hideCol($(value).val());
            });
        }
        // $("#jqGrid").trigger('reloadGrid');
    });

    //列点击字段
    $(":checkbox:not(:first)", $("#columnsid")).click(function () {

        var checkedstate = $(this).parent().attr("class");
        var columnsname = $(this).val();
        //选中全选多选
        if (($(":checkbox:not(:first):checked", $("#columnsid")).length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid").hideCol(columnsname);
        }

    });
    //点击导出
    $("#ctl00_ContentPlaceHolder1_btn_Output").click(function () {
        var colsName = "";
        //循环选中的字段列
        $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
            colsName = colsName + $.trim($(this).parent().parent().parent().text())+",";
        });
        if (colsName != "") {
            colsName = colsName.substr(0, (colsName.length - 1));
            $("#ctl00_ContentPlaceHolder1_hid_cols").val(colsName);
        }
       
    });

    //点击选择条件
    $("input[name=bb]:checkbox", "#tb_check").click(function () {

        //取得当前行数
        var clo = $(this).attr("clo");

        var isChecked = $(this).attr("checked");

        //取得当前行数
        var clo = $(this).attr("clo");
        if (isChecked == undefined) {
            //清空
            $(this).parent().attr("class", "");
            $(this).attr("checked", false);
            $("#tb_select tr:eq(" + clo + ")").hide();
            $(":text", "#tb_select tr:eq(" + clo + ")").val("");
            $("select", "#tb_select tr:eq(" + clo + ")").val("-1");
        }
        else {

            $(this).attr("checked", "checked");
            $(this).parent().attr("class", "checked");
            $("#tb_select tr:eq(" + parseInt(clo) + ")").show();
        }


        //有条件显示查询按钮
        if ($("input[name=bb]:checkbox:checked", $("#tb_check")).length > 0) {
            $("#tb_select tr:last").show();
        }
        else {
            $("#tb_select tr:last").hide();
            $(":text", "#tb_select tr:eq(" + clo + ")").val("");
            $("select", "#tb_select tr:eq(" + clo + ")").val("-1");
            $("#btn_search").click();
        }

    });

    //导出
    $("#ctl00_ContentPlaceHolder1_btn_Output").click(function () {

        var msg = Verification();
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });

    //查询
    $("#btn_search").click(function () {

        var msg = Verification();
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //单位
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        //姓名
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        //专业
        var spename = $("#ctl00_ContentPlaceHolder1_txt_SpeName").val();
        //开始时间
        var signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate").val();
        //完成时间
        var finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val();
        //项目总数       
        var projectCount1 = $("#ctl00_ContentPlaceHolder1_txt_ProjectCount1").val();
        var projectCount2 = $("#ctl00_ContentPlaceHolder1_txt_ProjectCount2").val();
        //进行中总数
        var jinXingCount1 = $("#ctl00_ContentPlaceHolder1_txt_ProjectJinXingCount1").val();
        var jinXingCount2 = $("#ctl00_ContentPlaceHolder1_txt_ProjectJinXingCount2").val();
        //暂停总数
        var zanTingCount1 = $("#ctl00_ContentPlaceHolder1_txt_ProjectZanTingCount1").val();
        var zanTingCount2 = $("#ctl00_ContentPlaceHolder1_txt_ProjectZanTingCount2").val();
        //完工
        var wangongCount1 = $("#ctl00_ContentPlaceHolder1_txt_ProjectWanGongCount1").val();
        var wangongCount2 = $("#ctl00_ContentPlaceHolder1_txt_ProjectWanGongCount2").val();
        var params = {
            "strwhere": strwhere,
            "unit": unit,
            "keyname": keyname,
            "spename": spename,
            "signdate": signdate,
            "finishdate": finishdate,
            "projectCount1": projectCount1,
            "projectCount2": projectCount2,
            "jinXingCount1": jinXingCount1,
            "jinXingCount2": jinXingCount2,
            "zanTingCount1": zanTingCount1,
            "zanTingCount2": zanTingCount2,
            "wangongCount1": wangongCount1,
            "wangongCount2": wangongCount2
        };

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/LeadershipCockpit/MemberLabouStatisticHandler.ashx",
            postData: params,
            page: 1,
            sortname: 'mem_ID',
            sortorder: 'asc'

        }).trigger("reloadGrid");

        //循环所有的checkbox 得到被选中的checkbox
        var strChk = "";
        //详细 返回结果
        $("input[type=checkbox]", "#tb_check").each(function () {
            if ($(this).is(":checked")) {
                strChk = strChk + $(this).attr("clo") + ",";
            }
        });
        $("#hid_checkbox").val(strChk);
    });

    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var msg = Verification();
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //单位
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        //姓名
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        //专业
        var spename = $("#ctl00_ContentPlaceHolder1_txt_SpeName").val();
        //开始时间
        var signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate").val();
        //完成时间
        var finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val();
        //项目总数       
        var projectCount1 = $("#ctl00_ContentPlaceHolder1_txt_ProjectCount1").val();
        var projectCount2 = $("#ctl00_ContentPlaceHolder1_txt_ProjectCount2").val();
        //进行中总数
        var jinXingCount1 = $("#ctl00_ContentPlaceHolder1_txt_ProjectJinXingCount1").val();
        var jinXingCount2 = $("#ctl00_ContentPlaceHolder1_txt_ProjectJinXingCount2").val();
        //暂停总数
        var zanTingCount1 = $("#ctl00_ContentPlaceHolder1_txt_ProjectZanTingCount1").val();
        var zanTingCount2 = $("#ctl00_ContentPlaceHolder1_txt_ProjectZanTingCount2").val();
        //完工
        var wangongCount1 = $("#ctl00_ContentPlaceHolder1_txt_ProjectWanGongCount1").val();
        var wangongCount2 = $("#ctl00_ContentPlaceHolder1_txt_ProjectWanGongCount2").val();
        var params = {
            "strwhere": strwhere,
            "unit": unit,
            "keyname": keyname,
            "spename": spename,
            "signdate": signdate,
            "finishdate": finishdate,
            "projectCount1": projectCount1,
            "projectCount2": projectCount2,
            "jinXingCount1": jinXingCount1,
            "jinXingCount2": jinXingCount2,
            "zanTingCount1": zanTingCount1,
            "zanTingCount2": zanTingCount2,
            "wangongCount1": wangongCount1,
            "wangongCount2": wangongCount2
        };
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/LeadershipCockpit/MemberLabouStatisticHandler.ashx",
            postData: params,
            page: 1,
            sortname: 'mem_ID',
            sortorder: 'asc'

        }).trigger("reloadGrid");
    });
});

//验证
function Verification() {

    var reg = /^[0-9]\d*$/;
    var msg = "";
    //项目总数
    if ($("#ctl00_ContentPlaceHolder1_txt_ProjectCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_ProjectCount1").val())) {
            msg += "参与项目总数格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txt_ProjectCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_ProjectCount2").val())) {
            msg += "参与项目总数格式错误，请输入数字！</br>";
        }
    }

    //进行中项目总数总数
    if ($("#ctl00_ContentPlaceHolder1_txt_ProjectJinXingCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_ProjectJinXingCount1").val())) {
            msg += "进行中项目总数格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txt_ProjectJinXingCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_ProjectJinXingCount2").val())) {
            msg += "进行中项目总数格式错误，请输入数字！</br>";
        }
    }

    //暂停项目总数总数
    if ($("#ctl00_ContentPlaceHolder1_txt_ProjectZanTingCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_ProjectZanTingCount1").val())) {
            msg += "暂停项目总数格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txt_ProjectZanTingCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_ProjectZanTingCount2").val())) {
            msg += "暂停项目总数格式错误，请输入数字！</br>";
        }
    }

    //完工项目总数总数
    if ($("#ctl00_ContentPlaceHolder1_txt_ProjectWanGongCount1").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_ProjectWanGongCount1").val())) {
            msg += "完工项目总数格式错误，请输入数字！</br>";
        }
    }
    if ($("#ctl00_ContentPlaceHolder1_txt_ProjectWanGongCount2").val() != "") {
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_ProjectWanGongCount2").val())) {
            msg += "完工项目总数格式错误，请输入数字！</br>";
        }
    }

    return msg;
}

function colShowMemFormatter(celvalue, options, rowData) {
    var imgstr = '<img src="/Images/' + celvalue + '.png" style="width: 16px; height: 16px; margin-left: 1px; border:none;" />';
    return imgstr + celvalue;
}

//查看人员参与产值明细
function selectLabouFormatter(celvalue, options, rowData) {
    var pageurl = "MemberLabouStatisticDetail.aspx?mem_ID=" + rowData["mem_ID"] + "&type=0";
    return '<a href="' + pageurl + '" alt="人员劳动力信息">' + $.trim(celvalue) + '</a>';
}

function selectLabouFormatter1(celvalue, options, rowData) {
    var pageurl = "MemberLabouStatisticDetail.aspx?mem_ID=" + rowData["mem_ID"] + "&type=1";
    return '<a href="' + pageurl + '" alt="人员劳动力信息">' + $.trim(celvalue) + '</a>';
}

function selectLabouFormatter2(celvalue, options, rowData) {
    var pageurl = "MemberLabouStatisticDetail.aspx?mem_ID=" + rowData["mem_ID"] + "&type=2";
    return '<a href="' + pageurl + '" alt="人员劳动力信息">' + $.trim(celvalue) + '</a>';
}

function selectLabouFormatter3(celvalue, options, rowData) {
    var pageurl = "MemberLabouStatisticDetail.aspx?mem_ID=" + rowData["mem_ID"] + "&type=3";
    return '<a href="' + pageurl + '" alt="人员劳动力信息">' + $.trim(celvalue) + '</a>';
}

//查看
function selectLabouDetailFormatter(celvalue, options, rowData) {
    var pageurl = "MemberLabouStatisticDetail.aspx?mem_ID=" + rowData["mem_ID"] + "&type=0";
    return '<a href="' + pageurl + '" alt="人员劳动力信息">查看</a>';
}

//统计 
function completeMethod() {

    var rowIds = $("#jqGrid").jqGrid('getDataIDs');

    var oneData = parseInt($("#jqGrid").getCol("TotalCount", false, 'sum')).toFixed(0);

    var twoData = $("#jqGrid").getCol("JinXingCount", false, 'sum');
    var threeData = $("#jqGrid").getCol("WanGongCount", false, 'sum');
    var fourData = $("#jqGrid").getCol("ZantingCount", false, 'sum');
    $("#jqGrid").footerData('set', { mem_Name: "合计:", TotalCount: oneData, JinXingCount: twoData, WanGongCount: threeData, ZantingCount: fourData }, false);
}

//无数据
function loadCompMethod() {

    $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}