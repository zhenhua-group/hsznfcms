﻿$(function () {
    //逐年显示
    $("#YearByYearCheckbox").click(function () {
        var checkedAttribute = $(this).attr("checked");
        if (checkedAttribute != undefined && checkedAttribute != null) {
            OpenProjectReportPage();
        }
    });

    //点击查看项目信息
    $("a[id=previewProject]").live("click", function () {
        var projectSysNo = $(this).attr("peojectSysNo");

        GetProjectDetailInfoByProjectSysNo(projectSysNo);
    });
});

//PageSize
var pageSize = $("#HiddenPageSize").val();
var backgroundInvoke = TG.Web.LeaderManage.ProjectCount;
//全局参数Array
var globalParametersArray = new Array();

//逐年显示
function OpenProjectReportPage() {
    $("#OneByOneShowResultDiv iframe").attr("src", "/ProjectStatistics/ProjectCountList.aspx");
    $("#OneByOneShowResultDiv").dialog({
        autoOpen: false,
        modal: true,
        width: 900,
        height: 500,
        resizable: false,
        buttons:
			{
			    "关闭": function () { $(this).dialog("close"); }
			}
    }).dialog("open");
}

//弹出显示项目列表
function PopAreaProjectList(titleString, bodyHTMLString) {
    $("#PopAreaProjectList").html(bodyHTMLString);
    CommonControl.SetTableStyle("resultTable", "need");
    $("#PopAreaProjectList").dialog({
        autoOpen: false,
        modal: true,
        width: 900,
        height: 500,
        resizable: false,
        title: titleString,
        buttons:
			{
			    "关闭": function () { $(this).dialog("close"); }
			}
    }).dialog("open");
}
function GetLength(item) {
    if (item.length > 13) {
        return item.substring(0, 13)
    }
    else {
        return item;
    }
}
//创建表格
function CreateTable(projectList, showType) {
    var tableString = "";
    tableString += "<div class=\"cls_data\"  style=\"height:90%;\">";
    tableString += "<table class=\"cls_content_head\" >";
    tableString += "<tr>";
    tableString += "<td style=\"width: 300px;overflow:hidden;\" align=\"center\">";
    tableString += "项目名称";
    tableString += "</td>";
    tableString += "<td style=\"width: 150px;\" align=\"center\">";
    tableString += "日期";
    tableString += "</td>";
    tableString += "<td style=\"width: 100px;\" align=\"center\">";
    tableString += "负责人";
    tableString += "</td>";

    var titleText = "";
    switch (showType) {
        case "0": titleText = "状态"; break;
        case "1": titleText = "项目类型"; break;
        case "2": titleText = "面积（平米）"; break;
        case "3": titleText = "项目状态"; break;
    }
    tableString += "<td style=\"width: 200px;\" align=\"center\">";
    tableString += titleText;
    tableString += "</td>";

    tableString += "<td style=\"width: 50px;\" align=\"center\">";
    tableString += "查看";
    tableString += "</td>";
    tableString += "</tr>";
    tableString += "</table>";

    //拼接项目列表结果Table
    var resultTableString = "";

    resultTableString += "<table class=\"show_projectNumber\" id=\"resultTable\" >";
    $.each(projectList, function (index, project) {
        resultTableString += "<tr>";
        resultTableString += "<td style=\"width: 300px;\" align=\"left\"  title=\"" + project.ProjectName + "\"><img src=\"../Images/proj.png\" style=\"width:16px;height:16px;margin-left:1px;\" />";
        resultTableString += project.ProjectName;
        resultTableString += "</td>";
        resultTableString += "<td style=\"width: 150px;\" align=\"center\">";
        resultTableString += project.StartTimeString;
        resultTableString += "</td>";
        resultTableString += "<td style=\"width: 100px;\" align=\"center\">";
        resultTableString += project.ChargeManJia;
        resultTableString += "</td>";

        var valueText = "";
        switch (showType) {
            case "0": valueText = project.AuditStatusString; break;
            case "1": valueText = project.ProjectKinds; break;
            case "2": valueText = project.Scale; break;
            case "3": valueText = project.ProjectStatusString; break;
        }
        resultTableString += "<td style=\"width: 200px;\" align=\"center\">";
        resultTableString += valueText;
        resultTableString += "</td>";

        resultTableString += "<td style=\"width: 50px;\" align=\"center\">";
        resultTableString += "<a href=\"#\" id=\"previewProject\" peojectSysNo=\"" + project.ProjectSysNo + "\">查看</a>";
        resultTableString += "</td>";
        resultTableString += "</tr>";
    });
    resultTableString += "</table>";
    tableString += resultTableString;
    tableString += "</div>";
    tableString += "<div id=\"pager\" style=\"height:5%;\"></div>";
    return tableString;
}

/*================AjaxMethod======================*/

//根据年份和评审状态查询项目列表
function GetProjectListByYearAndAuditPassCondition(year, isAuditPass) {
    globalParametersArray[0] = year;
    globalParametersArray[1] = isAuditPass;

    var projectListString = backgroundInvoke.GetProjectListByYearAndAuditPassCondition(year, isAuditPass, 1).value;
    if (projectListString != null && projectListString.length > 0) {
        var projectList = Global.evalJSON(projectListString);
        var tableString = CreateTable(projectList[1], "0");

        PopAreaProjectList(year + "年" + (isAuditPass == 0 ? "立项数" : "立项通过数"), tableString);

        var currentPage = 1;
        if (projectList[0] == undefined) {
            projectCount = 0;
            currentPage = 0;
        }

        var pageCount = 0;
        if (projectList[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (projectList[0] % pageSize == 0) {
                pageCount = projectList[0] / pageSize;
            } else {
                pageCount = Math.floor(projectList[0] / pageSize) + 1;
            }
        }
        $("#pager").pager({ "pagenumber": currentPage, "pagecount": pageCount, "pagesize": pageSize, "datacount": projectList[0], "buttonClickCallback": GetProjectListByYearAndAuditPassConditionCallBackFunction });
    }
}

//根据年份和评审状态查询项目列表回掉函数
function GetProjectListByYearAndAuditPassConditionCallBackFunction(clickNumber) {
    var result = backgroundInvoke.GetProjectListByYearAndAuditPassCondition(globalParametersArray[0], globalParametersArray[1], clickNumber);
    if (result.value != null && result.value.length > 0) {
        var resultArray = Global.evalJSON(result.value);
        $("#PopAreaProjectList").html("");
        $("#PopAreaProjectList").html(CreateTable(resultArray[1], "0"));
        CommonControl.SetTableStyle("resultTable", "need");
        var pageCount = 0;
        if (resultArray[0] % pageSize == 0) {
            pageCount = 1;
        } else {
            pageCount = Math.floor(resultArray[0] / pageSize) + 1;
        }
        $("#pager").pager({ "pagenumber": clickNumber, "pagecount": pageCount, "pagesize": pageSize, "datacount": resultArray[0], "buttonClickCallback": GetProjectListByYearAndAuditPassConditionCallBackFunction });
    }
}

//根据项目类型查看项目列表
function GetProjectListByProjectType(year, projectType) {
    globalParametersArray[0] = year;
    globalParametersArray[1] = projectType;
    var projectListString = backgroundInvoke.GetProjectListByProjectType(year, 1, globalParametersArray[1]).value;

    if (projectListString != null && projectListString.length > 0) {
        var projectList = Global.evalJSON(projectListString);

        var tableString = CreateTable(projectList[1], "1");

        PopAreaProjectList(year + "年项目类型统计", tableString);

        var currentPage = 1;
        if (projectList[0] == undefined) {
            projectCount = 0;
            currentPage = 0;
        }

        var pageCount = 0;
        if (projectList[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (projectList[0] % pageSize == 0) {
                pageCount = projectList[0] / pageSize;
            } else {
                pageCount = Math.floor(projectList[0] / pageSize) + 1;
            }
        }
        $("#pager").pager({ "pagenumber": currentPage, "pagecount": pageCount, "pagesize": pageSize, "datacount": projectList[0], "buttonClickCallback": GetProjectListByProjectTypeCallBackFunction });
    }
}

//根据项目类型查看项目列表回调函数
function GetProjectListByProjectTypeCallBackFunction(clickNumber) {
    var projectListString = backgroundInvoke.GetProjectListByProjectType(globalParametersArray[0], clickNumber, globalParametersArray[1]).value;
    if (projectListString != null && projectListString.length > 0) {
        var projectList = Global.evalJSON(projectListString);
        $("#PopAreaProjectList").html("");
        $("#PopAreaProjectList").html(CreateTable(projectList[1], "1"));
        CommonControl.SetTableStyle("resultTable", "need");
        var currentPage = 1;
        if (projectList[0] == undefined) {
            projectCount = 0;
            currentPage = 0;
        }

        var pageCount = 0;
        if (projectList[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (projectList[0] % pageSize == 0) {
                pageCount = projectList[0] / pageSize;
            } else {
                pageCount = Math.floor(projectList[0] / pageSize) + 1;
            }
        }
        $("#pager").pager({ "pagenumber": clickNumber, "pagecount": pageCount, "pagesize": pageSize, "datacount": projectList[0], "buttonClickCallback": GetProjectListByProjectTypeCallBackFunction });
    }
}

//得到项目平米数列表
function GetProjectScaleDetail(year) {
    globalParametersArray[0] = year;
    var projectListString = backgroundInvoke.GetProjectListByProjectScale(year, 1).value;

    if (projectListString != null && projectListString.length > 0) {
        var projectList = Global.evalJSON(projectListString);

        var tableString = CreateTable(projectList[1], "2");

        PopAreaProjectList(year + "年项目平米数统计", tableString);

        var currentPage = 1;
        if (projectList[0] == undefined) {
            projectCount = 0;
            currentPage = 0;
        }

        var pageCount = 0;
        if (projectList[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (projectList[0] % pageSize == 0) {
                pageCount = projectList[0] / pageSize;
            } else {
                pageCount = Math.floor(projectList[0] / pageSize) + 1;
            }
        }
        $("#pager").pager({ "pagenumber": currentPage, "pagecount": pageCount, "pagesize": pageSize, "datacount": projectList[0], "buttonClickCallback": GetProjectScaleDetailCallBackFunction });
    }
}

//得到项目平米数列表回调函数
function GetProjectScaleDetailCallBackFunction(clickNumber) {
    var projectListString = backgroundInvoke.GetProjectListByProjectScale(globalParametersArray[0], clickNumber).value;

    if (projectListString != null && projectListString.length > 0) {
        var projectList = Global.evalJSON(projectListString);

        $("#PopAreaProjectList").html("");
        $("#PopAreaProjectList").html(CreateTable(projectList[1], "2"));
        CommonControl.SetTableStyle("resultTable", "need");
        var currentPage = 1;
        if (projectList[0] == undefined) {
            projectCount = 0;
            currentPage = 0;
        }

        var pageCount = 0;
        if (projectList[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (projectList[0] % pageSize == 0) {
                pageCount = projectList[0] / pageSize;
            } else {
                pageCount = Math.floor(projectList[0] / pageSize) + 1;
            }
        }
        $("#pager").pager({ "pagenumber": clickNumber, "pagecount": pageCount, "pagesize": pageSize, "datacount": projectList[0], "buttonClickCallback": GetProjectScaleDetailCallBackFunction });
    }
}

//根据项目状态得到项目列表
function GetProjectListByProjectStatus(year) {
    globalParametersArray[0] = year;
    var projectListString = backgroundInvoke.GetProjectListByProjectStatus(year, 1).value;

    if (projectListString != null && projectListString.length > 0) {
        var projectList = Global.evalJSON(projectListString);

        var tableString = CreateTable(projectList[1], "3");

        PopAreaProjectList(year + "年项目状态统计", tableString);

        var currentPage = 1;
        if (projectList[0] == undefined) {
            projectCount = 0;
            currentPage = 0;
        }

        var pageCount = 0;
        if (projectList[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (projectList[0] % pageSize == 0) {
                pageCount = projectList[0] / pageSize;
            } else {
                pageCount = Math.floor(projectList[0] / pageSize) + 1;
            }
        }
        $("#pager").pager({ "pagenumber": currentPage, "pagecount": pageCount, "pagesize": pageSize, "datacount": projectList[0], "buttonClickCallback": GetProjectListByProjectStatusCallBack });
    }
}

//根据项目状态得到项目列表回调函数
function GetProjectListByProjectStatusCallBack(clickNumber) {
    var projectListString = backgroundInvoke.GetProjectListByProjectStatus(globalParametersArray[0], clickNumber).value;

    if (projectListString != null && projectListString.length > 0) {
        var projectList = Global.evalJSON(projectListString);

        $("#PopAreaProjectList").html("");
        $("#PopAreaProjectList").html(CreateTable(projectList[1], "3"));
        CommonControl.SetTableStyle("resultTable", "need");
        var currentPage = 1;
        if (projectList[0] == undefined) {
            projectCount = 0;
            currentPage = 0;
        }

        var pageCount = 0;
        if (projectList[0] <= pageSize) {
            pageCount = 1;
        } else {
            if (projectList[0] % pageSize == 0) {
                pageCount = projectList[0] / pageSize;
            } else {
                pageCount = Math.floor(projectList[0] / pageSize) + 1;
            }
        }
        $("#pager").pager({ "pagenumber": clickNumber, "pagecount": pageCount, "pagesize": pageSize, "datacount": projectList[0], "buttonClickCallback": GetProjectListByProjectStatusCallBack });
    }
}

//根据项目编号得到项目信息
function GetProjectDetailInfoByProjectSysNo(projectSysNo) {
    $("#auditLocusTableOfProject").html("");
    var projectDetailInfoString = backgroundInvoke.GetProjectDetailInfo(projectSysNo).value;

    if (projectDetailInfoString != null && projectDetailInfoString.length > 0) {
        var projectDetailEntity = Global.evalJSON(projectDetailInfoString);
        //绑定数据
        $.each(projectDetailEntity, function (propertyName, propertyValue) {
            $("span[id=" + propertyName + "]").html(propertyValue);
        });

        //用户SysNoString
        var userSysNoArray;

        if (projectDetailEntity.AuditUser != null) {
            userSysNoArray = projectDetailEntity.AuditUser.substring(0, projectDetailEntity.AuditUser.length - 1).split(",");
        }

        var auditDateArray;

        if (projectDetailEntity.AuditDate) {
            auditDateArray = projectDetailEntity.AuditDate.substring(0, projectDetailEntity.AuditDate.length - 1).split(",");
        }

        var auditContent;

        if (projectDetailEntity.Suggestion) {
            auditContent = projectDetailEntity.Suggestion.substring(0, projectDetailEntity.Suggestion.length - 1).split("|");
        }

        var auditUserInfoString = backgroundInvoke.GetUserRoleAndUserName(userSysNoArray).value;

        var auditUserInfoArray = Global.evalJSON(auditUserInfoString);

        var trString = "";
        if (auditUserInfoArray != null && auditUserInfoArray[0] != null) {
            trString += "<tr style=\"background: url(../images/bg_tdhead.gif) repeat-x;\"><td style=\"width:20%;\">审核角色</td><td style=\"width:10%;\">审核人</td><td style=\"width:50%;\">审核内容</td><td style=\"width:20%;\">审核时间</td></tr>";
            $.each(auditUserInfoArray[0], function (index, item) {
                trString += "<tr>";
                trString += "<td style=\"width:20%;\">" + item + "</td><td style=\"width:10%;\" >" + auditUserInfoArray[1][index] + "</td><td style=\"width:50%;\">" + auditContent[index] + "</td><td style=\"width:20%;\">" + auditDateArray[index] + "</td>";
                trString += "</tr>";
            });
            $("#auditLocusTableOfProject").html(trString);
        }

        //绑定审核数据
        $("#PopAreaForPorProjectDetail").dialog({
            autoOpen: false,
            modal: true,
            width: 700,
            height: 400,
            resizable: false,
            title: "查看项目详细信息",
            buttons:
			{
			    "关闭": function () { $(this).dialog("close"); }
			}
        }).dialog("open");
    }
}
/*================End AjaxMethod======================*/