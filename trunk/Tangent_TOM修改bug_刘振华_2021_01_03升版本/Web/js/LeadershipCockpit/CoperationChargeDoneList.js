﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //Grid 高度指定
    $("#grid_cpr tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#grid_cpr tr:even").css({ background: "White" });
    //进度条
    $(".progressbar").each(function () {
        var prst = parseInt($(this).next(":hidden").val());
        $(this).progressbar({
            value: prst
        });
        $(this).attr("title", prst + "%");
    });
   
    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#ImageButton1"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });

});
