﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectMamage/ProjectListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '工程名称', '开始时间', '结束时间', '项目阶段'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'pro_Name', index: 'pro_Name', width: 400, formatter: colNameShowFormatter },
                             { name: 'qdrq', index: 'pro_startTime', width: 80, align: 'center' },
                             { name: 'wcrq', index: 'pro_finishTime', width: 80, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', width: 100, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "ProjectAnalysis", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()) },
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        // var year = $("#drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_proname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    // var pageurl = "/ProjectManage/ShowProject.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    //return '<img src="../Images/buttons/icon_cpr.png" style="width: 16px; height: 16px; border: none;" /><a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';
    return celvalue;
}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = '<a href="###" class="chk_jd" rel="' + celvalue + '" prjname="' + rowData["pro_Name"] + '">查看阶段</a>';
    return pageurl;

}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}