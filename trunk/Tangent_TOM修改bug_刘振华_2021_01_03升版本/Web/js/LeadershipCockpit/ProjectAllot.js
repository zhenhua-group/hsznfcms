﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
   
    //隔行变色
    $("#gv_project tr:even").css({ background: "White" });

    //提示
    $(".cls_column").tipsy({ opacity: 1 });
    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();
});