﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/LeadershipCockpit/ProjectValueAllotHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '项目名称', '实收产值(万元)', '转经济所(万元)', '转暖通所(万元)', '转土建所(万元)', '本所产值(万元)', '所留(万元) ', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 300, formatter: colNameShowFormatter },
                             { name: 'Charge', index: 'Charge', width: 100, align: 'right' },
                             { name: 'EconomyValue', index: 'EconomyValue', width: 100, align: 'right' },
                             { name: 'HavcValue', index: 'HavcValue', width: 100, align: 'right' },
                             { name: 'TranBulidingValue', index: 'TranBulidingValue', width: 100, align: 'right' },
                             { name: 'UnitValue', index: 'UnitValue', width: 100, align: 'right' },
                             { name: 'TheDeptValue', index: 'TheDeptValue', width: 100, align: 'right' },
                             { name: 'pro_ID', index: 'pro_ID', width: 100, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "year": $("#ctl00_ContentPlaceHolder1_drp_year").val(), "unit": $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text(), "keyname": $("#ctl00_ContentPlaceHolder1_txt_keyname").val() },
        loadonce: false,
        sortname: 'p.pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/LeadershipCockpit/ProjectValueAllotHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh:false,
        deltext: ""
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/LeadershipCockpit/ProjectValueAllotHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'p.pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/LeadershipCockpit/ProjectValueAllotHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'p.pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "ProjectValueAllotBymaster.aspx?pro_id=" + celvalue + "&pp=" + $("#ctl00_ContentPlaceHolder1_previewPower").val() + "&year=" + $("#ctl00_ContentPlaceHolder1_drp_year").val();
    return '<a href="' + pageurl + '" class="chk" alt="个人产值" >个人产值</a>';

}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
    var sumCharge = parseFloat($("#jqGrid").getCol("Charge", false, 'sum')).toFixed(4);
    var sumEconomyValue = parseFloat($("#jqGrid").getCol("EconomyValue", false, 'sum')).toFixed(4);
    var sumHavcValue = parseFloat($("#jqGrid").getCol("HavcValue", false, 'sum')).toFixed(4);
    var sumTranBulidingValue = parseFloat($("#jqGrid").getCol("TranBulidingValue", false, 'sum')).toFixed(4);
    var sumUnitValue = parseFloat($("#jqGrid").getCol("UnitValue", false, 'sum')).toFixed(4);
    var sumTheDeptValue = parseFloat($("#jqGrid").getCol("TheDeptValue", false, 'sum')).toFixed(4);
    $("#jqGrid").footerData('set', { pro_name: "合计:", Charge: sumCharge, EconomyValue: sumEconomyValue, HavcValue: sumHavcValue, TranBulidingValue: sumTranBulidingValue, UnitValue: sumUnitValue, TheDeptValue: sumTheDeptValue }, false);
}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}