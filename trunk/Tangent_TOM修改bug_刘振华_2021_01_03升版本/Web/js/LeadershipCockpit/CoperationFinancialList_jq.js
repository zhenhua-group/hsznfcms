﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Coperation/ProjectChargeHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '合同名称', '合同类型', '承接部门', '建筑面积(㎡)', '合同额(万元)', '已收款(万元)', '未收款(万元) ', '百分比 ', '查看'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 250, formatter: colNameShowFormatter },
                             { name: 'cpr_Type', index: 'cpr_Type', width: 100, align: 'center' },
                             { name: 'cpr_Unit', index: 'cpr_Unit', width: 100, align: 'center' },
                             { name: 'BuildArea', index: 'BuildArea', width: 100, align: 'center' },
                             { name: 'cpr_Acount', index: 'cpr_Acount', width: 80, align: 'center' },
                             { name: 'ssze', index: 'ssze', width: 80, align: 'center' },
                             { name: 'wsk', index: 'wsk', width: 100, align: 'center' },
                             { name: 'sfjd', index: 'sfjd', width: 100, align: 'center', formatter: function (celvalue, options, rowData) { return parseFloat(celvalue).toFixed(2) + "%" } },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 50, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "cprfinalist", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/ProjectChargeHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh:false
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/ProjectChargeHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/ProjectChargeHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1

        }).trigger("reloadGrid");
    });
});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/Coperation/cpr_ShowCoprationBymaster.aspx?flag=cprlist&cprid=" + rowData["cpr_Id"];
    return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "CoperationChargeDetailViewBymaster.aspx?coperationSysNo=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看合同">查看</a>';

}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
    var sumBuildArea = parseFloat($("#jqGrid").getCol("BuildArea", false, 'sum')).toFixed(4);
    var sumcpr_Acount = parseFloat($("#jqGrid").getCol("cpr_Acount", false, 'sum')).toFixed(4);
    var sumssze = parseFloat($("#jqGrid").getCol("ssze", false, 'sum')).toFixed(4);
    var sumwsk = parseFloat($("#jqGrid").getCol("wsk", false, 'sum')).toFixed(4);
    $("#jqGrid").footerData('set', { cpr_Name: "合计:", BuildArea: sumBuildArea, cpr_Acount: sumcpr_Acount, ssze: sumssze, wsk: sumwsk }, false);

}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}