﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/LeadershipCockpit/MemberLabouStatisticDetailHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '项目名称', '承接部门', '项目状态', '开始时间', '结束时间', '项目进行阶段', '担当职责'],
        colModel: [
                             { name: 'id', index: 'id', width: 45, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'pro_Name', index: 'pro_Name', width: 350 },
                             { name: 'pro_DesignUnit', index: 'pro_DesignUnit', width: 80 },
                             { name: 'pro_Status', index: 'pro_Status', width: 80 },
                             { name: 'pro_StartTime', index: 'pro_StartTime', width: 80, align: 'center' },
                             { name: 'pro_FinishTime', index: 'pro_FinishTime', width: 80, align: 'center' },
                             { name: 'ProjectCurrentStatus', index: 'ProjectCurrentStatus', width: 100, align: 'center' },
                             { name: 'memRoles', index: 'memRoles', width: 200 }
                  ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "action": "statisticDetail", "mem_id": $("#ctl00_ContentPlaceHolder1_hid_mem_id").val() },
        loadonce: false,
        sortname: 'a.pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,

        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/LeadershipCockpit/MemberLabouStatisticDetailHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        loadComplete: loadCompMethod
    });

});

//无数据
function loadCompMethod() {
    $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        // $("#nodata").show();
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
    }
    else {
        $("#nodata").hide();
    }
}