﻿$(document).ready(function () {
    var isRounding = $("#HiddenIsRounding").val();

    $("input", "table[memMoney='gvProjectValueBymember'] tr ").each(function () {

        $(this).change(function () {
            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }
           
            var inputvalue = $(this).val();
            var total = $(this).parent().next().next().text();
            var proresultvalue = parseFloat(inputvalue) * parseFloat(total) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });

    });


    //浮点数
    function checkRate(value) {
        var re = /^[0-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+$/;
        if (re.test(value)) {
            return true;
        }
    }
});