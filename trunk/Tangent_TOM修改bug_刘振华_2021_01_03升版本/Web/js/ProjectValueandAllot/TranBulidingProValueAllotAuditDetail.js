﻿
var cprProcess;
var isRounding;
$(document).ready(function () {

    var zongValue = $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text();
    var $tab = $("#ctl00_ContentPlaceHolder1_gvTwo");
    var Numvalue;
    cprProcess = $("#HiddenCoperationProcess").val();
    isRounding = $("#HiddenIsRounding").val();
    var reg = /^\d+(\.\d+)?$/;
    var isHavc = $("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text();
    if (isHavc.indexOf("暖通") == -1) {
        $("input", $tab).each(function () {

            $(this).change(function () {
                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }
                var inputvalue = $(this).val();

                var resultValue = parseFloat(inputvalue) * parseFloat(zongValue) / 100;
                if (isRounding == "0") {
                    $(this).parent().next().text(resultValue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(resultValue));
                }
                //$(this).parent().next().text(resultValue.toFixed(0));
                var index = $(this).parent().next().index();
                if (index == 2) {

                    //设置最后 闭合100
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var ontValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var total = parseFloat(ontValue) + parseFloat(twoValue);

                    $(this).parent().parent().children(":eq(5)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(5)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(5)").css({ background: "#D2F3CB" });
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();

                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(6)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(6)").text(totalCount.toFixed(0));
                    }

                    $("input", $("#gvProjectStageSpe tr:eq(0)")).each(function () {
                        var provalue = $(this).val();

                        var proresultvalue = parseFloat(provalue) * parseFloat(resultValue) / 100;

                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }

                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {

                            $("input", $("#gvdesignProcessOne tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;

                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessOne tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessOne tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;

                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessOne tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessOne tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                    });


                }
                if (index == 4) {

                    //设置最后 闭合100
                    //设置最后 闭合100
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var ontValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var total = parseFloat(ontValue) + parseFloat(twoValue);

                    $(this).parent().parent().children(":eq(5)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(5)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(5)").css({ background: "#D2F3CB" });
                    }
                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();

                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(6)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(6)").text(totalCount.toFixed(0));
                    }
                    $("input", $("#gvProjectStageSpe tr:eq(1)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;
                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }

                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessTwo tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessTwo tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessTwo tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessTwo tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessTwo tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                    });
                }
                FiveUser();
                FourUser();
                ThreeUser();
                TwoUser();
                oneUser();

            });
        });

        //第一
        $("input", "#ctl00_ContentPlaceHolder1_gvOne").each(function () {

            $(this).change(function () {
                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }

                var inputvalue = $(this).val();
                var resultValue = parseFloat(inputvalue) * parseFloat(zongValue) / 100;
                if (isRounding == "1") {
                    $(this).parent().next().text(Math.floor(resultValue));
                } else {
                    $(this).parent().next().text(resultValue.toFixed(0));
                }

                // $(this).parent().next().text(resultValue.toFixed(0));
                var index = $(this).parent().next().index();
                if (index == 2) {
                    //设置最后 闭合100
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                    var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);

                    $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    $("input", $("#gvProjectStageSpe tr:eq(0)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(provalue) * parseFloat(resultValue) / 100;
                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }

                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }

                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessOne tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }

                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }

                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessOne tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }

                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //  $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessOne tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }

                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessOne tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }

                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessOne tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                            });
                        }
                    });

                }
                if (index == 4) {

                    //设置最后 闭合100
                    var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                    var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);


                    $(this).parent().parent().children().find("span").text(total.toFixed(0));

                    //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    $("input", $("#gvProjectStageSpe tr:eq(1)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;
                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        // $(this).parent().next().text(proresultvalue.toFixed(0));

                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }

                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessTwo tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }

                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessTwo tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessTwo tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }

                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessTwo tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }

                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessTwo tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                // $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                            });
                        }
                    });
                }
                if (index == 6) {

                    //设置最后 闭合100
                    var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                    var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);

                    $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));

                    // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }
                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    $("input", $("#gvProjectStageSpe tr:eq(2)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;
                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        // $(this).parent().next().text(proresultvalue.toFixed(0));

                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }

                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessThree tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }

                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessThree tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }

                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessThree tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessThree tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessThree tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                            });
                        }
                    });
                }
                if (index == 8) {

                    //设置最后 闭合100
                    var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);

                    $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));

                    //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }
                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    $("input", $("#gvProjectStageSpe tr:eq(3)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;
                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        // $(this).parent().next().text(proresultvalue.toFixed(0));
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessFour tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessFour tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessFour tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessFour tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //   $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessFour tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                // $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                            });
                        }
                    });
                }
                FiveUser();
                FourUser();
                ThreeUser();
                TwoUser();
                oneUser();

            });
        });

        $("input", "#ctl00_ContentPlaceHolder1_gvThree").each(function () {
            //Numvalue += $(this).val();
            $(this).change(function () {
                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }
                var inputvalue = $(this).val();

                var resultValue = parseFloat(inputvalue) * parseFloat(zongValue) / 100;
                if (isRounding == "1") {
                    $(this).parent().next().text(Math.floor(resultValue));
                } else {
                    $(this).parent().next().text(resultValue.toFixed(0));
                }
                //$(this).parent().next().text(resultValue.toFixed(0));
                var index = $(this).parent().next().index();
                if (index == 2) {
                    //设置最后 闭合100
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var total = parseFloat(inputvalue) + parseFloat(twoValue);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(5)").children("span").text(total.toFixed(0));
                    } else {
                        $(this).parent().parent().children(":eq(5)").children("span").text(total.toFixed(0));
                    }
                    //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(5)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(5)").css({ background: "#D2F3CB" });
                    }
                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();

                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(6)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(6)").text(totalCount.toFixed(0));
                    }
                    $("input", $("#gvProjectStageSpe tr:eq(0)")).each(function () {
                        var provalue = $(this).val();

                        var proresultvalue = parseFloat(provalue) * parseFloat(resultValue) / 100;

                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        // $(this).parent().next().text(proresultvalue.toFixed(0));
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessThree tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessThree tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessThree tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessThree tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessThree tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                    });

                }
                if (index == 4) {
                    //设置最后 闭合100
                    var twoValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var total = parseFloat(inputvalue) + parseFloat(twoValue);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(5)").children("span").text(total.toFixed(0));
                    } else {
                        $(this).parent().parent().children(":eq(5)").children("span").text(total.toFixed(0));
                    }
                    //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(5)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(5)").css({ background: "#D2F3CB" });
                    }
                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();

                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(6)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(6)").text(totalCount.toFixed(0));
                    }
                    $("input", $("#gvProjectStageSpe tr:eq(1)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;
                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }

                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessFour tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessFour tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessFour tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessFour tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessFour tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                    });
                }

                FiveUser();
                FourUser();
                ThreeUser();
                TwoUser();
                oneUser();
            });

        });

        $("input", "#ctl00_ContentPlaceHolder1_gvFour").each(function () {
            //Numvalue += $(this).val();
            $(this).change(function () {
                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }
                var inputvalue = $(this).val();

                var resultValue = parseFloat(inputvalue) * parseFloat(zongValue) / 100;
                if (isRounding == "1") {
                    $(this).parent().next().text(Math.floor(resultValue));
                } else {
                    $(this).parent().next().text(resultValue.toFixed(0));
                }

                //$(this).parent().next().text(resultValue.toFixed(0));
                var index = $(this).parent().next().index();
                if (index == 2) {
                    //设置最后 闭合100
                    var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var total = parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);

                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(7)").children("span").text(total.toFixed(0));
                    } else {
                        $(this).parent().parent().children(":eq(7)").children("span").text(total.toFixed(0));
                    }
                    // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));

                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(7)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(7)").css({ background: "#D2F3CB" });
                    }
                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();

                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(8)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(8)").text(totalCount.toFixed(0));
                    }

                    $("input", $("#gvProjectStageSpe tr:eq(0)")).each(function () {
                        var provalue = $(this).val();

                        var proresultvalue = parseFloat(provalue) * parseFloat(resultValue) / 100;

                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        // $(this).parent().next().text(proresultvalue.toFixed(0));

                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessTwo tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessTwo tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessTwo tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessTwo tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessTwo tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                            });
                        }
                    });

                }
                if (index == 4) {
                    //设置最后 闭合100
                    //设置最后 闭合100
                    var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var total = parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);

                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(7)").children("span").text(total.toFixed(0));
                    } else {
                        $(this).parent().parent().children(":eq(7)").children("span").text(total.toFixed(0));
                    }
                    // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));

                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(7)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(7)").css({ background: "#D2F3CB" });
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();

                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(8)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(8)").text(totalCount.toFixed(0));
                    }

                    $("input", $("#gvProjectStageSpe tr:eq(1)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;
                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        // $(this).parent().next().text(proresultvalue.toFixed(0));
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessThree tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessThree tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessThree tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessThree tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessThree tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                    });
                }
                if (index == 6) {
                    //设置最后 闭合100
                    var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var total = parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);

                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(7)").children("span").text(total.toFixed(0));
                    } else {
                        $(this).parent().parent().children(":eq(7)").children("span").text(total.toFixed(0));
                    }
                    // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));

                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(7)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(7)").css({ background: "#D2F3CB" });
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();

                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(8)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(8)").text(totalCount.toFixed(0));
                    }

                    $("input", $("#gvProjectStageSpe tr:eq(2)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;
                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        //$(this).parent().next().text(proresultvalue.toFixed(0));
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessFour tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessFour tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessFour tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessFour tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessFour tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                    });
                }

                FiveUser();
                FourUser();
                ThreeUser();
                TwoUser();
                oneUser();
            });

        });

        $("input", "#ctl00_ContentPlaceHolder1_gvTen").each(function () {
            //Numvalue += $(this).val();
            $(this).change(function () {
                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }
                var inputvalue = $(this).val();

                var resultValue = parseFloat(inputvalue) * parseFloat(zongValue) / 100;
                if (isRounding == "1") {
                    $(this).parent().next().text(Math.floor(resultValue));
                } else {
                    $(this).parent().next().text(resultValue.toFixed(0));
                }

                //$(this).parent().next().text(resultValue.toFixed(0));
                var index = $(this).parent().next().index();
                if (index == 2) {
                    //设置最后 闭合100
                    var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var total = parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);

                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(7)").children("span").text(total.toFixed(0));
                    } else {
                        $(this).parent().parent().children(":eq(7)").children("span").text(total.toFixed(0));
                    }
                    // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));

                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(7)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(7)").css({ background: "#D2F3CB" });
                    }
                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();

                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(8)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(8)").text(totalCount.toFixed(0));
                    }

                    $("input", $("#gvProjectStageSpe tr:eq(0)")).each(function () {
                        var provalue = $(this).val();

                        var proresultvalue = parseFloat(provalue) * parseFloat(resultValue) / 100;

                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        // $(this).parent().next().text(proresultvalue.toFixed(0));

                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessOne tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessOne tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessOne tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessOne tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessOne tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                            });
                        }
                    });

                }
                if (index == 4) {
                    //设置最后 闭合100
                    //设置最后 闭合100
                    var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var total = parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);

                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(7)").children("span").text(total.toFixed(0));
                    } else {
                        $(this).parent().parent().children(":eq(7)").children("span").text(total.toFixed(0));
                    }
                    // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));

                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(7)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(7)").css({ background: "#D2F3CB" });
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();

                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(8)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(8)").text(totalCount.toFixed(0));
                    }

                    $("input", $("#gvProjectStageSpe tr:eq(1)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;
                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        // $(this).parent().next().text(proresultvalue.toFixed(0));
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessThree tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessThree tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessThree tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessThree tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessThree tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                    });
                }
                if (index == 6) {
                    //设置最后 闭合100
                    var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var total = parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);

                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(7)").children("span").text(total.toFixed(0));
                    } else {
                        $(this).parent().parent().children(":eq(7)").children("span").text(total.toFixed(0));
                    }
                    // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));

                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(7)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(7)").css({ background: "#D2F3CB" });
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();

                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(8)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(8)").text(totalCount.toFixed(0));
                    }

                    $("input", $("#gvProjectStageSpe tr:eq(2)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;
                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        //$(this).parent().next().text(proresultvalue.toFixed(0));
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        var indexspeOne = $(this).parent().next().index();
                        if (indexspeOne == 2) {
                            $("input", $("#gvdesignProcessFour tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 4) {
                            $("input", $("#gvdesignProcessFour tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }

                        if (indexspeOne == 6) {
                            $("input", $("#gvdesignProcessFour tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 8) {
                            $("input", $("#gvdesignProcessFour tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                        if (indexspeOne == 10) {
                            $("input", $("#gvdesignProcessFour tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        }
                    });
                }

                FiveUser();
                FourUser();
                ThreeUser();
                TwoUser();
                oneUser();
            });

        });
        //设计阶段 0表示 各阶段
        if (cprProcess == 0 || cprProcess == 1 || cprProcess == 2 || cprProcess == 3 || cprProcess ==10) {

            Process();
        }
        // // }


        $("input", "#gvFive").each(function () {
            $(this).change(function () {
                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }
                var inputvalue = $(this).val();

                var resultValue = parseFloat(inputvalue) * parseFloat(zongValue) / 100;
                if (isRounding == "1") {
                    $(this).parent().next().text(Math.floor(resultValue));
                } else {
                    $(this).parent().next().text(resultValue.toFixed(0));
                }
                // $(this).parent().next().text(resultValue.toFixed(0));
                var index = $(this).parent().next().index();
                if (index == 2) {
                    //设置最后 闭合100
                    var twoValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var fourValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var fiveValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                    if (twoValue == undefined) {
                        twoValue = 0;
                    }
                    if (threeValue == undefined) {
                        threeValue = 0;
                    }
                    if (fourValue == undefined) {
                        fourValue = 0;
                    }
                    if (fiveValue == undefined) {
                        fiveValue = 0;
                    }
                    var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(fiveValue);

                    $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    $("input", $("#gvdesignProcessFive tr:eq(0)")).each(function () {
                        var provalue = $(this).val();

                        var proresultvalue = parseFloat(provalue) * parseFloat(resultValue) / 100;

                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                    });
                }
                if (index == 4) {
                    //设置最后 闭合100
                    //设置最后 闭合100
                    var twoValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var fourValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var fiveValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                    if (twoValue == undefined) {
                        twoValue = 0;
                    }
                    if (threeValue == undefined) {
                        threeValue = 0;
                    }
                    if (fourValue == undefined) {
                        fourValue = 0;
                    }
                    if (fiveValue == undefined) {
                        fiveValue = 0;
                    }
                    var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(fiveValue);

                    $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    $("input", $("#gvdesignProcessFive tr:eq(1)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;

                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }

                    });
                }
                if (index == 6) {
                    //设置最后 闭合100
                    //设置最后 闭合100
                    var twoValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var fourValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var fiveValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                    if (twoValue == undefined) {
                        twoValue = 0;
                    }
                    if (threeValue == undefined) {
                        threeValue = 0;
                    }
                    if (fourValue == undefined) {
                        fourValue = 0;
                    }
                    if (fiveValue == undefined) {
                        fiveValue = 0;
                    }
                    var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(fiveValue);

                    $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    $("input", $("#gvdesignProcessFive tr:eq(2)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;

                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                    });
                }
                if (index == 8) {
                    //设置最后 闭合100
                    //设置最后 闭合100
                    var twoValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var fourValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var fiveValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                    if (twoValue == undefined) {
                        twoValue = 0;
                    }
                    if (threeValue == undefined) {
                        threeValue = 0;
                    }
                    if (fourValue == undefined) {
                        fourValue = 0;
                    }
                    if (fiveValue == undefined) {
                        fiveValue = 0;
                    }
                    var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(fiveValue);

                    $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    $("input", $("#gvdesignProcessFive tr:eq(3)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;

                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                    });
                }
                if (index == 10) {
                    //设置最后 闭合100
                    //设置最后 闭合100
                    var twoValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var fourValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var fiveValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                    if (twoValue == undefined) {
                        twoValue = 0;
                    }
                    if (threeValue == undefined) {
                        threeValue = 0;
                    }
                    if (fourValue == undefined) {
                        fourValue = 0;
                    }
                    if (fiveValue == undefined) {
                        fiveValue = 0;
                    }
                    var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(fiveValue);

                    $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    $("input", $("#gvdesignProcessFive tr:eq(4)")).each(function () {
                        var provalue = $(this).val();
                        var proresultvalue = parseFloat(resultValue) * parseFloat(provalue) / 100;

                        if (isRounding == "1") {
                            $(this).parent().next().text(Math.floor(proresultvalue));
                        } else {
                            $(this).parent().next().text(proresultvalue.toFixed(0));
                        }
                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                    });
                }

                FiveUser();
                FourUser();
                ThreeUser();
                TwoUser();
                oneUser();
            });
        });



        $("input", "#gvdesignProcessFive tr:eq(0)").each(function () {
            $(this).change(function () {
                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }

                var value = $("#gvFive tr td").eq(2).text();

                var inputvalue = $(this).val();
                var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;

                if (isRounding == "1") {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                } else {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }

                //闭合100%
                var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                if (oneValue == undefined) {
                    oneValue = 0;
                }
                if (twoValue == undefined) {
                    twoValue = 0;
                }
                if (threeValue == undefined) {
                    threeValue = 0;
                }
                if (fourValue == undefined) {
                    fourValue = 0;
                }

                var total = parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                if (parseFloat(total) != 100) {
                    $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                }
                else {
                    $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                }

                //取得总值
                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                if (isRounding == "1") {
                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                }
                else {
                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                }

                FiveUser();
                FourUser();
                ThreeUser();
                TwoUser();
                oneUser();
            });

        });


        $("input", "#gvdesignProcessFive tr:eq(1)").each(function () {
            $(this).change(function () {

                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }
                var value = $("#gvFive tr td").eq(4).text();

                var inputvalue = $(this).val();
                var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;

                if (isRounding == "1") {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                } else {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                //$(this).parent().next().text(proresultvalue.toFixed(0));

                //闭合100%
                var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                if (oneValue == undefined) {
                    oneValue = 0;
                }
                if (twoValue == undefined) {
                    twoValue = 0;
                }
                if (threeValue == undefined) {
                    threeValue = 0;
                }
                if (fourValue == undefined) {
                    fourValue = 0;
                }

                var total = parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                if (parseFloat(total) != 100) {
                    $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                }
                else {
                    $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                }

                //取得总值
                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                if (isRounding == "1") {
                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                }
                else {
                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                }

                FiveUser();
                FourUser();
                ThreeUser();
                TwoUser();
                oneUser();
            });

        });
        $("input", "#gvdesignProcessFive tr:eq(2)").each(function () {
            $(this).change(function () {
                var value = $("#gvFive tr td").eq(6).text();

                var inputvalue = $(this).val();
                var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;

                if (isRounding == "1") {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                } else {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                // $(this).parent().next().text(proresultvalue.toFixed(0));

                //闭合100%
                var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                if (oneValue == undefined) {
                    oneValue = 0;
                }
                if (twoValue == undefined) {
                    twoValue = 0;
                }
                if (threeValue == undefined) {
                    threeValue = 0;
                }
                if (fourValue == undefined) {
                    fourValue = 0;
                }

                var total = parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                if (parseFloat(total) != 100) {
                    $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                }
                else {
                    $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                }

                //取得总值
                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                if (isRounding == "1") {
                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                }
                else {
                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                }

                FiveUser();
                FourUser();
                ThreeUser();
                TwoUser();
                oneUser();
            });

        });
        $("input", "#gvdesignProcessFive tr:eq(3)").each(function () {
            $(this).change(function () {

                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }
                var value = $("#gvFive tr td").eq(8).text();

                var inputvalue = $(this).val();
                var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;

                if (isRounding == "1") {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                } else {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                // $(this).parent().next().text(proresultvalue.toFixed(0));

                //闭合100%
                var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                if (oneValue == undefined) {
                    oneValue = 0;
                }
                if (twoValue == undefined) {
                    twoValue = 0;
                }
                if (threeValue == undefined) {
                    threeValue = 0;
                }
                if (fourValue == undefined) {
                    fourValue = 0;
                }

                var total = parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                if (parseFloat(total) != 100) {
                    $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                }
                else {
                    $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                }

                //取得总值
                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                if (isRounding == "1") {
                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                }
                else {
                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                }

                FiveUser();
                FourUser();
                ThreeUser();
                TwoUser();
                oneUser();
            });

        });

        $("input", "#gvdesignProcessFive tr:eq(4)").each(function () {
            $(this).change(function () {
                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }
                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }
                var value = $("#gvFive tr td").eq(10).text();

                var inputvalue = $(this).val();
                var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;

                if (isRounding == "1") {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                } else {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                // $(this).parent().next().text(proresultvalue.toFixed(0));

                var indexspeOne = $(this).parent().next().index();
                if (indexspeOne == 2) {
                    //闭合100%
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                    var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                    $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }

                }
                if (indexspeOne == 4) {
                    //闭合100%
                    var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                    var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                    var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                    $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }


                }

                if (indexspeOne == 6) {
                    //闭合100%
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                    var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(oneValue) + parseFloat(fourValue);
                    $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        s
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }


                }
                if (indexspeOne == 8) {

                    //闭合100%
                    var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                    var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                    var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();

                    var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(oneValue) + parseFloat(threeValue);
                    $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                    if (parseFloat(total) != 100) {
                        $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                    }
                    else {
                        $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                    }

                }

                FiveUser();
                FourUser();
                ThreeUser();
                TwoUser();
                oneUser();
            });

        });


        function Process() {

            $("input", "#gvProjectStageSpe tr:eq(0)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#ctl00_ContentPlaceHolder1_gvOne tr td").eq(2).text();
                    } else if (cprProcess == 1) {
                        value = $("#ctl00_ContentPlaceHolder1_gvTwo tr td").eq(2).text();
                    } else if (cprProcess == 2) {
                        value = $("#ctl00_ContentPlaceHolder1_gvThree tr td").eq(2).text();
                    } else if (cprProcess == 3) {
                        value = $("#ctl00_ContentPlaceHolder1_gvFour tr td").eq(2).text();
                    } else if (cprProcess == 10) {
                        value = $("#ctl00_ContentPlaceHolder1_gvTen tr td").eq(2).text();
                    }

                    var inputvalues = $(this).val();
                    var proresultvalue = parseFloat(inputvalues) * parseFloat(value) / 100;
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }
                    //$(this).parent().next().text(proresultvalue.toFixed(0));


                    var indexspeOne = $(this).parent().next().index();

                    if (indexspeOne == 2) {
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }


                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessOne tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 2) {
                            $("input", $("#gvdesignProcessThree tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                            });
                        } else if (cprProcess == 3) {
                            $("input", $("#gvdesignProcessTwo tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            });
                        }

                    }
                    if (indexspeOne == 4) {
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }


                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessOne tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //  $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 2) {

                            $("input", $("#gvdesignProcessThree tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //  $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 3) {

                            $("input", $("#gvdesignProcessTwo tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //  $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        }


                    }

                    if (indexspeOne == 6) {

                        //闭合100%
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }


                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessOne tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 2) {
                            $("input", $("#gvdesignProcessThree tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });

                        } else if (cprProcess == 3) {
                            $("input", $("#gvdesignProcessTwo tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });

                        }
                    }
                    if (indexspeOne == 8) {

                        //闭合100%
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }


                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }

                        if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessOne tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 2) {
                            $("input", $("#gvdesignProcessThree tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });

                        } else if (cprProcess == 3) {
                            $("input", $("#gvdesignProcessTwo tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });

                        }
                    }
                    if (indexspeOne == 10) {
                        //闭合100%
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                        if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessOne tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 2) {
                            $("input", $("#gvdesignProcessThree tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 3) {
                            $("input", $("#gvdesignProcessTwo tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                            });

                        }
                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                });

            });


            $("input", "#gvProjectStageSpe tr:eq(1)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#ctl00_ContentPlaceHolder1_gvOne tr td").eq(4).text();
                    } else if (cprProcess == 1) {
                        value = $("#ctl00_ContentPlaceHolder1_gvTwo tr td").eq(4).text();
                    } else if (cprProcess == 2) {
                        value = $("#ctl00_ContentPlaceHolder1_gvThree tr td").eq(4).text();
                    } else if (cprProcess == 3) {
                        value = $("#ctl00_ContentPlaceHolder1_gvFour tr td").eq(4).text();
                    } else if (cprProcess == 10) {
                        value = $("#ctl00_ContentPlaceHolder1_gvTen tr td").eq(4).text();
                    }

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }
                    // $(this).parent().next().text(proresultvalue.toFixed(0));

                    var indexspeOne = $(this).parent().next().index();
                    if (indexspeOne == 2) {
                        //设置最后 闭合100
                        var ontValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (ontValue == undefined) {
                            ontValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(ontValue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);

                        $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();

                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }


                        if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessTwo tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 2) {
                            $("input", $("#gvdesignProcessFour tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            });

                        } else if (cprProcess == 3) {

                            $("input", $("#gvdesignProcessThree tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            });
                        }

                    }
                    if (indexspeOne == 4) {
                        //设置最后 闭合100
                        var ontValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (ontValue == undefined) {
                            ontValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(ontValue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);

                        $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();

                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }

                        if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessTwo tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 2) {
                            $("input", $("#gvdesignProcessFour tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 3) {

                            $("input", $("#gvdesignProcessThree tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        }
                    }

                    if (indexspeOne == 6) {
                        //设置最后 闭合100
                        //设置最后 闭合100
                        var ontValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (ontValue == undefined) {
                            ontValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(ontValue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);

                        $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();

                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }

                        if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessTwo tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 2) {
                            $("input", $("#gvdesignProcessFour tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });

                        } else if (cprProcess == 3) {
                            $("input", $("#gvdesignProcessThree tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });
                        }

                    }
                    if (indexspeOne == 8) {
                        //设置最后 闭合100
                        var ontValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (ontValue == undefined) {
                            ontValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(ontValue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);

                        $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();

                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }


                        if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessTwo tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 2) {
                            $("input", $("#gvdesignProcessFour tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });

                        } else if (cprProcess == 3) {
                            $("input", $("#gvdesignProcessThree tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }

                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });

                        }
                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                });

            });
            $("input", "#gvProjectStageSpe tr:eq(2)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#ctl00_ContentPlaceHolder1_gvOne tr td").eq(6).text();
                    }
                    else if (cprProcess == 3) {
                        value = $("#ctl00_ContentPlaceHolder1_gvFour tr td").eq(6).text();
                    } else if (cprProcess == 10) {
                        value = $("#ctl00_ContentPlaceHolder1_gvTen tr td").eq(6).text();
                    }

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }
                    //$(this).parent().next().text(proresultvalue.toFixed(0));

                    var indexspeOne = $(this).parent().next().index();
                    if (indexspeOne == 2) {
                        //闭合100%
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        if (cprProcess == 0 || cprProcess ==10) {
                            $("input", $("#gvdesignProcessThree tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            });
                        }
                        else if (cprProcess == 3) {
                            $("input", $("#gvdesignProcessFour tr:eq(0)")).each(function () {
                                var speOneBulidingvalue = $(this).val();
                                var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            });

                        }

                    }
                    if (indexspeOne == 4) {
                        //闭合100%
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }


                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        if (cprProcess == 0 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessThree tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 3) {

                            $("input", $("#gvdesignProcessFour tr:eq(1)")).each(function () {
                                var speOnestructurevalue = $(this).val();
                                var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            });
                        }
                    }

                    if (indexspeOne == 6) {
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        if (cprProcess == 0 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessThree tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //  $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 3) {
                            $("input", $("#gvdesignProcessFour tr:eq(2)")).each(function () {
                                var speOnedrainvalue = $(this).val();
                                var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //  $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            });

                        }
                    }
                    if (indexspeOne == 8) {
                        //闭合100%
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        if (cprProcess == 0 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessThree tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });
                        } else if (cprProcess == 3) {

                            $("input", $("#gvdesignProcessFour tr:eq(3)")).each(function () {
                                var speOnehvacvalue = $(this).val();
                                var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                                } else {
                                    $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                //$(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            });
                        }
                    }
                    if (indexspeOne == 10) {

                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                        if (cprProcess == 0 || cprProcess == 10) {
                            $("input", $("#gvdesignProcessThree tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                            });
                        }
                        else if (cprProcess == 3) {
                            $("input", $("#gvdesignProcessFour tr:eq(4)")).each(function () {
                                var speOneelectricvalue = $(this).val();
                                var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                                if (isRounding == "1") {
                                    $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                                } else {
                                    $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                                }
                                //取得总值
                                var oneCount = $(this).parent().parent().children(":eq(2)").text();
                                var twoCount = $(this).parent().parent().children(":eq(4)").text();
                                var threeCount = $(this).parent().parent().children(":eq(6)").text();
                                var fourCount = $(this).parent().parent().children(":eq(8)").text();
                                var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                                if (isRounding == "1") {
                                    $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                                }
                                else {
                                    $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                                }
                                // $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                            });

                        }
                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                });

            });

            $("input", "#gvProjectStageSpe tr:eq(3)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = $("#ctl00_ContentPlaceHolder1_gvOne tr td").eq(8).text();

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }
                    //  $(this).parent().next().text(proresultvalue.toFixed(0));

                    var indexspeOne = $(this).parent().next().index();
                    if (indexspeOne == 2) {
                        //闭合100%
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }


                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        $("input", $("#gvdesignProcessFour tr:eq(0)")).each(function () {
                            var speOneBulidingvalue = $(this).val();
                            var speOneBulidingresultvalue = parseFloat(proresultvalue) * parseFloat(speOneBulidingvalue) / 100;
                            if (isRounding == "1") {
                                $(this).parent().next().text(Math.floor(speOneBulidingresultvalue));
                            } else {
                                $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                            }
                            //取得总值
                            var oneCount = $(this).parent().parent().children(":eq(2)").text();
                            var twoCount = $(this).parent().parent().children(":eq(4)").text();
                            var threeCount = $(this).parent().parent().children(":eq(6)").text();
                            var fourCount = $(this).parent().parent().children(":eq(8)").text();
                            var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                            if (isRounding == "1") {
                                $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                            }
                            else {
                                $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                            }
                            // $(this).parent().next().text(speOneBulidingresultvalue.toFixed(0));
                        });
                    }
                    if (indexspeOne == 4) {
                        //闭合100%
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }


                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }


                        $("input", $("#gvdesignProcessFour tr:eq(1)")).each(function () {
                            var speOnestructurevalue = $(this).val();
                            var speOnestructureresultvalue = parseFloat(proresultvalue) * parseFloat(speOnestructurevalue) / 100;
                            if (isRounding == "1") {
                                $(this).parent().next().text(Math.floor(speOnestructureresultvalue));
                            } else {
                                $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                            }
                            //取得总值
                            var oneCount = $(this).parent().parent().children(":eq(2)").text();
                            var twoCount = $(this).parent().parent().children(":eq(4)").text();
                            var threeCount = $(this).parent().parent().children(":eq(6)").text();
                            var fourCount = $(this).parent().parent().children(":eq(8)").text();
                            var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                            if (isRounding == "1") {
                                $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                            }
                            else {
                                $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                            }
                            // $(this).parent().next().text(speOnestructureresultvalue.toFixed(0));
                        });
                    }

                    if (indexspeOne == 6) {
                        //闭合100%
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }



                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }
                        $("input", $("#gvdesignProcessFour tr:eq(2)")).each(function () {
                            var speOnedrainvalue = $(this).val();
                            var speOnedrainresultvalue = parseFloat(proresultvalue) * parseFloat(speOnedrainvalue) / 100;
                            if (isRounding == "1") {
                                $(this).parent().next().text(Math.floor(speOnedrainresultvalue));
                            } else {
                                $(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                            }

                            //取得总值
                            var oneCount = $(this).parent().parent().children(":eq(2)").text();
                            var twoCount = $(this).parent().parent().children(":eq(4)").text();
                            var threeCount = $(this).parent().parent().children(":eq(6)").text();
                            var fourCount = $(this).parent().parent().children(":eq(8)").text();
                            var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                            if (isRounding == "1") {
                                $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                            }
                            else {
                                $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                            }
                            //$(this).parent().next().text(speOnedrainresultvalue.toFixed(0));
                        });
                    }
                    if (indexspeOne == 8) {
                        //闭合100%
                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();


                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue) + parseFloat(oneValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }



                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }

                        $("input", $("#gvdesignProcessFour tr:eq(3)")).each(function () {
                            var speOnehvacvalue = $(this).val();
                            var speOnehvacresultvalue = parseFloat(proresultvalue) * parseFloat(speOnehvacvalue) / 100;
                            if (isRounding == "1") {
                                $(this).parent().next().text(Math.floor(speOnehvacresultvalue));
                            } else {
                                $(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                            }

                            //取得总值
                            var oneCount = $(this).parent().parent().children(":eq(2)").text();
                            var twoCount = $(this).parent().parent().children(":eq(4)").text();
                            var threeCount = $(this).parent().parent().children(":eq(6)").text();
                            var fourCount = $(this).parent().parent().children(":eq(8)").text();
                            var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                            if (isRounding == "1") {
                                $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                            }
                            else {
                                $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                            }
                            //$(this).parent().next().text(speOnehvacresultvalue.toFixed(0));
                        });
                    }
                    if (indexspeOne == 10) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                        //取得总值
                        var oneCount = $(this).parent().parent().children(":eq(2)").text();
                        var twoCount = $(this).parent().parent().children(":eq(4)").text();
                        var threeCount = $(this).parent().parent().children(":eq(6)").text();
                        var fourCount = $(this).parent().parent().children(":eq(8)").text();
                        var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                        }
                        else {
                            $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                        }

                        $("input", $("#gvdesignProcessFour tr:eq(4)")).each(function () {
                            var speOneelectricvalue = $(this).val();
                            var speOneelectricresultvalue = parseFloat(proresultvalue) * parseFloat(speOneelectricvalue) / 100;
                            if (isRounding == "1") {
                                $(this).parent().next().text(Math.floor(speOneelectricresultvalue));
                            } else {
                                $(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                            }

                            //取得总值
                            var oneCount = $(this).parent().parent().children(":eq(2)").text();
                            var twoCount = $(this).parent().parent().children(":eq(4)").text();
                            var threeCount = $(this).parent().parent().children(":eq(6)").text();
                            var fourCount = $(this).parent().parent().children(":eq(8)").text();
                            var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                            if (isRounding == "1") {
                                $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                            }
                            else {
                                $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                            }
                            //$(this).parent().next().text(speOneelectricresultvalue.toFixed(0));
                        });
                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                });

            });

            $("input", "#gvdesignProcessOne tr:eq(0)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(2).text();
                    }

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);

                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }


                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    // $(this).parent().next().text(proresultvalue.toFixed(0));
                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                });
            });

            $("input", "#gvdesignProcessOne tr:eq(1)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(4).text();
                    }

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //  $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    // $(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessOne tr:eq(2)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(6).text();
                    }
                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //   $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    // $(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessOne tr:eq(3)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(8).text();
                    }
                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    //$(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessOne tr:eq(4)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(10).text();
                    }
                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }


                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    //$(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessTwo tr:eq(0)").each(function () {
                $(this).change(function () {

                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0 || cprProcess == 1 ) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(2).text();
                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(2).text();
                    }
                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();

                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    // $(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessTwo tr:eq(1)").each(function () {
                $(this).change(function () {

                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0 || cprProcess == 1 ) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(4).text();
                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(4).text();
                    }

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    //$(this).parent().next().text(proresultvalue.toFixed(0));
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                });
            });

            $("input", "#gvdesignProcessTwo tr:eq(2)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0 || cprProcess == 1 ) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(6).text();
                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(6).text();
                    }

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    // $(this).parent().next().text(proresultvalue.toFixed(0));
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();

                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                });
            });

            $("input", "#gvdesignProcessTwo tr:eq(3)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0 || cprProcess == 1) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(8).text();
                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(8).text();
                    }

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    //$(this).parent().next().text(proresultvalue.toFixed(0));
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //  $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();

                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                });
            });

            $("input", "#gvdesignProcessTwo tr:eq(4)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0 || cprProcess == 1) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(10).text();
                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(10).text();
                    }

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    //  $(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessThree tr:eq(0)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#gvProjectStageSpe tr:eq(2) td").eq(2).text();
                    } else if (cprProcess == 2) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(2).text();

                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(2).text();
                    }
                    else if (cprProcess == 10) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(2).text();
                    }

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //  $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //  $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    // $(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessThree tr:eq(1)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#gvProjectStageSpe tr:eq(2) td").eq(4).text();
                    } else if (cprProcess == 2) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(4).text();

                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(4).text();
                    }
                    else if (cprProcess == 10) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(4).text();
                    }

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    // $(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessThree tr:eq(2)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#gvProjectStageSpe tr:eq(2) td").eq(6).text();
                    } else if (cprProcess == 2) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(6).text();

                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(6).text();
                    } else if (cprProcess == 10) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(6).text();
                    }

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    //$(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessThree tr:eq(3)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#gvProjectStageSpe tr:eq(2) td").eq(8).text();
                    } else if (cprProcess == 2) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(8).text();

                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(8).text();
                    } else if (cprProcess == 10) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(8).text();
                    }


                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    //$(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessThree tr:eq(4)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#gvProjectStageSpe tr:eq(2) td").eq(10).text();
                    } else if (cprProcess == 2) {
                        value = $("#gvProjectStageSpe tr:eq(0) td").eq(10).text();

                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(10).text();
                    } else if (cprProcess == 10) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(10).text();
                    }

                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }

                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }
                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    // $(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessFour tr:eq(0)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#gvProjectStageSpe tr:eq(3) td").eq(2).text();
                    } else if (cprProcess == 2) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(2).text();

                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(2) td").eq(2).text();
                    }


                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    // $(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessFour tr:eq(1)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#gvProjectStageSpe tr:eq(3) td").eq(4).text();
                    } else if (cprProcess == 2) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(4).text();

                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(2) td").eq(4).text();
                    }


                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();

                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    //$(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessFour tr:eq(2)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#gvProjectStageSpe tr:eq(3) td").eq(6).text();
                    } else if (cprProcess == 2) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(6).text();

                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(2) td").eq(6).text();
                    }


                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();

                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();

                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }

                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    // $(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessFour tr:eq(3)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#gvProjectStageSpe tr:eq(3) td").eq(8).text();
                    } else if (cprProcess == 2) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(8).text();

                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(2) td").eq(8).text();
                    }


                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }

                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        // $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (fourValue == undefined) {
                            fourValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        if (oneValue == undefined) {
                            oneValue = 0;
                        }
                        if (twoValue == undefined) {
                            twoValue = 0;
                        }
                        if (threeValue == undefined) {
                            threeValue = 0;
                        }
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }

                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }
                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    //$(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });

            $("input", "#gvdesignProcessFour tr:eq(4)").each(function () {
                $(this).change(function () {
                    if (!checkRate($(this).val())) {
                        $(this).val(0);
                    }
                    if ($(this).val() == '') {
                        $(this).val(0);
                    }
                    var value = 0;
                    if (cprProcess == 0) {
                        value = $("#gvProjectStageSpe tr:eq(3) td").eq(10).text();
                    } else if (cprProcess == 2) {
                        value = $("#gvProjectStageSpe tr:eq(1) td").eq(10).text();

                    } else if (cprProcess == 3) {
                        value = $("#gvProjectStageSpe tr:eq(2) td").eq(10).text();
                    }



                    var inputvalue = $(this).val();
                    var proresultvalue = parseFloat(inputvalue) * parseFloat(value) / 100;
                    var index = $(this).parent().next().index();
                    if (index == 2) {
                        //闭合100%
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        var total = parseFloat(inputvalue) + parseFloat(twoValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 4) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(threeValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (index == 6) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var fourValue = $(this).parent().parent().children(":eq(7)").children(":input").val();
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(fourValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }

                    }
                    if (index == 8) {

                        //闭合100%
                        var oneValue = $(this).parent().parent().children(":eq(1)").children(":input").val();
                        var twoValue = $(this).parent().parent().children(":eq(3)").children(":input").val();
                        var threeValue = $(this).parent().parent().children(":eq(5)").children(":input").val();
                        var total = parseFloat(inputvalue) + parseFloat(oneValue) + parseFloat(twoValue) + parseFloat(threeValue);
                        if (isRounding == "1") {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        } else {
                            $(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        }
                        //$(this).parent().parent().children(":eq(9)").children("span").text(total.toFixed(0));
                        if (parseFloat(total) != 100) {
                            $(this).parent().parent().children(":eq(9)").css({ background: "Red" });
                        }
                        else {
                            $(this).parent().parent().children(":eq(9)").css({ background: "#D2F3CB" });
                        }
                    }
                    if (isRounding == "1") {
                        $(this).parent().next().text(Math.floor(proresultvalue));
                    } else {
                        $(this).parent().next().text(proresultvalue.toFixed(0));
                    }
                    //取得总值
                    var oneCount = $(this).parent().parent().children(":eq(2)").text();
                    var twoCount = $(this).parent().parent().children(":eq(4)").text();
                    var threeCount = $(this).parent().parent().children(":eq(6)").text();
                    var fourCount = $(this).parent().parent().children(":eq(8)").text();
                    var totalCount = parseFloat(oneCount) + parseFloat(twoCount) + parseFloat(threeCount) + parseFloat(fourCount);
                    if (isRounding == "1") {
                        $(this).parent().parent().children(":eq(10)").text(Math.floor(totalCount));
                    }
                    else {
                        $(this).parent().parent().children(":eq(10)").text(totalCount.toFixed(0));
                    }

                    FiveUser();
                    FourUser();
                    ThreeUser();
                    TwoUser();
                    oneUser();
                    //$(this).parent().next().text(proresultvalue.toFixed(0));
                });
            });


        }

        function oneUser() {
            $("input", "#gvProjectValueOneBymember tr").each(function () {

                var spe = $(this).attr("spe");
                var roles = $(this).attr("roles");

                var sjOneAmount = 0;
                if (spe == "建筑") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(0) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(0) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(0) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(0) td:eq(2)").text();
                    }

                } else if (spe == "结构") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(1) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(1) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(1) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(1) td:eq(2)").text();
                    }
                }
                else if (spe == "给排水") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(2) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(2) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(2) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(2) td:eq(2)").text();
                    }
                }
                else if (spe == "电气") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(3) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(3) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(3) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessOne tr:eq(3) td:eq(2)").text();
                    }
                }
                else {
                    sjOneAmount = 0;
                }
                var inputvalue = $(this).val().length == 0 ? 0 : parseFloat($(this).val());

                var proresultvalue = parseFloat(inputvalue) * parseFloat(sjOneAmount) / 100;
                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });
        }
        function TwoUser() {
            $("input", "#gvProjectValueTwoBymember tr").each(function () {

                var spe = $(this).attr("spe");
                var roles = $(this).attr("roles");

                var sjOneAmount = 0;
                if (spe == "建筑") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(0) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(0) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(0) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(0) td:eq(2)").text();
                    }

                } else if (spe == "结构") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(1) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(1) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(1) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(1) td:eq(2)").text();
                    }
                }
                else if (spe == "给排水") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(2) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(2) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(2) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(2) td:eq(2)").text();
                    }
                }
                else if (spe == "电气") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(3) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(3) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(3) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessTwo tr:eq(3) td:eq(2)").text();
                    }
                }
                else {
                    sjOneAmount = 0;
                }
                var inputvalue = $(this).val().length == 0 ? 0 : parseFloat($(this).val());

                var proresultvalue = parseFloat(inputvalue) * parseFloat(sjOneAmount) / 100;
                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });


        }
        function ThreeUser() {
            $("input", "#gvProjectValueThreeBymember tr").each(function () {

                var spe = $(this).attr("spe");
                var roles = $(this).attr("roles");

                var sjOneAmount = 0;
                if (spe == "建筑") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(0) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(0) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(0) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(0) td:eq(2)").text();
                    }

                } else if (spe == "结构") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(1) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(1) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(1) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(1) td:eq(2)").text();
                    }
                }
                else if (spe == "给排水") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(2) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(2) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(2) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(2) td:eq(2)").text();
                    }
                }
                else if (spe == "电气") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(3) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(3) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(3) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessThree tr:eq(3) td:eq(2)").text();
                    }
                }
                else {
                    sjOneAmount = 0;
                }
                var inputvalue = $(this).val().length == 0 ? 0 : parseFloat($(this).val());

                var proresultvalue = parseFloat(inputvalue) * parseFloat(sjOneAmount) / 100;
                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });
        }
        function FourUser() {
            $("input", "#gvProjectValueFourBymember tr").each(function () {

                var spe = $(this).attr("spe");
                var roles = $(this).attr("roles");

                var sjOneAmount = 0;
                if (spe == "建筑") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(0) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(0) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(0) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(0) td:eq(2)").text();
                    }

                } else if (spe == "结构") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(1) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(1) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(1) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(1) td:eq(2)").text();
                    }
                }
                else if (spe == "给排水") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(2) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(2) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(2) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(2) td:eq(2)").text();
                    }
                }
                else if (spe == "电气") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(3) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(3) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(3) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessFour tr:eq(3) td:eq(2)").text();
                    }
                }
                else {
                    sjOneAmount = 0;
                }
                var inputvalue = $(this).val().length == 0 ? 0 : parseFloat($(this).val());

                var proresultvalue = parseFloat(inputvalue) * parseFloat(sjOneAmount) / 100;
                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });
        }

        function FiveUser() {
            $("input", "#gvProjectValueBymember tr").each(function () {

                var spe = $(this).attr("spe");
                var roles = $(this).attr("roles");

                var sjOneAmount = 0;
                if (spe == "建筑") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(0) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(0) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(0) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(0) td:eq(2)").text();
                    }

                } else if (spe == "结构") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(1) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(1) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(1) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(1) td:eq(2)").text();
                    }
                }
                else if (spe == "给排水") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(2) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(2) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(2) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(2) td:eq(2)").text();
                    }
                }
                else if (spe == "电气") {
                    if (roles == "sj") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(3) td:eq(8)").text();
                    } else if (roles == "fz") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(3) td:eq(4)").text();
                    } else if (roles == "jd") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(3) td:eq(6)").text();
                    } else if (roles == "sh") {
                        sjOneAmount = $("#gvdesignProcessFive tr:eq(3) td:eq(2)").text();
                    }
                }
                else {
                    sjOneAmount = 0;
                }
                var inputvalue = $(this).val().length == 0 ? 0 : parseFloat($(this).val());

                var proresultvalue = parseFloat(inputvalue) * parseFloat(sjOneAmount) / 100;
                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });
        }
    }
});

//浮点数
function checkRate(value) {
    var re = /^[0-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+$/;
    if (re.test(value)) {
        return true;
    }
}
