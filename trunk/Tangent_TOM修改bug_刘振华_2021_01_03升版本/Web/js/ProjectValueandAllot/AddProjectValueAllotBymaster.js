﻿var auditRecordLength = 2;
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
var isTrunEconomy;
var isTrunHavc;
$(function () {


    $("#CoperationBaseInfo tr:odd").css({ background: "White" });
    //是否转经济所
    var IsTrunEconomy = $("#HiddenIsTrunEconomy").val();
    isTrunEconomy = IsTrunEconomy;
    if (IsTrunEconomy == 0) {
        $("#ctl00_ContentPlaceHolder1_txt_EconomyValuePercent").attr("disabled", "disabled");
        // $("#ctl00_ContentPlaceHolder1_txt_EconomyValuePercent").attr("class", "TextBoxStyle");
    }

    //部门
    var unitName = $("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text();

    //转暖通
    var IsTrunHavc = $("#HiddenIsTrunHavc").val();
    isTrunHavc = IsTrunHavc;
    if (IsTrunHavc == 0 || unitName.indexOf("暖通热力所") > -1) {
        $("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").val("0");
        $("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").attr("disabled", "disabled");
        // $("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").attr("class", "TextBoxStyle");
    }

    // 设置文本框样式
    CommonControl.SetTextBoxStyle();

    //项目阶段
    var cprProcess = $("#ctl00_ContentPlaceHolder1_HiddenCoperationProcess").val();

    //合同ID
    var cprId = $("#HiddenCprID").val();

    //年份变化
    $("#ctl00_ContentPlaceHolder1_drp_year").change(function () {

        var year = $("#ctl00_ContentPlaceHolder1_drp_year").children("option:selected").val();

        $.post("/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx", { "Action": "4", "year": year, "cprId": cprId, "pro_id": $("#HiddenProSysNo").val() }, function (jsonResult) {

            var jsonData = eval("(" + jsonResult + ")");
            var payshiAcount = jsonData.payShiCount;
            $("#ctl00_ContentPlaceHolder1_lblPayShiCount").text(payshiAcount);

            // var allotCount = $("#ctl00_ContentPlaceHolder1_lblAllotAccount").text();
            var allotCount = jsonData.allountAcount;
            $("#ctl00_ContentPlaceHolder1_lblAllotAccount").text(allotCount);
            var notallotCount = parseFloat(payshiAcount) - parseFloat(allotCount);
            $("#ctl00_ContentPlaceHolder1_lblNotAllotAccount").text(notallotCount.toFixed(2));

            CalculationValue();

        });
    });


    //设总
    $("#ctl00_ContentPlaceHolder1_txt_DesignManagerPercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_DesignManagerPercent").val())) {
            alert("设总比例请输入数字！");
            $(this).val("0");
        }

        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
            //            return false;
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_DesignManagerPercent").val()) > 100) {
            alert("设总比例不能大于100%！");
            //            return false;
        }

        CalculationValue();
        return false;

    });

    //转经济所
    $("#ctl00_ContentPlaceHolder1_txt_EconomyValuePercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_EconomyValuePercent").val())) {
            alert("转经济所产值比例请输入数字！");
            $(this).val("0");
        }

        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
            //            return false;
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_EconomyValuePercent").val()) > 100) {
            alert("转经济所产值比例不能大于100%！");
            //            return false;
        }

        CalculationValue();
        return false;

    });


    //转暖通
    $("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").val())) {
            alert("转暖通产值比例请输入数字！");
            $(this).val("0");
        }

        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
            //            return false;
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").val()) > 100) {
            alert("转暖通产值比例不能大于100%！");
            //            return false;
        }

        CalculationValue();
        return false;


    });

    //转其他部门
    $("#ctl00_ContentPlaceHolder1_txtOtherDeptValuePercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtOtherDeptValuePercent").val())) {
            alert("转出其他部门产值比例请输入数字！");
            $(this).val("0");
        }
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
            //            return false;
        }
        if (parseFloat($("#ctl00_ContentPlaceHolder1_txtOtherDeptValuePercent").val()) > 100) {
            alert("转出其他部门比例不能大于100%！");
            //            return false;
        }

        CalculationValue();
        return false;

    });


    //本部门自留产值
    $("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val())) {
            alert("本部门自留产值比例请输入数字！");
            $(this).val("0");
        }
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
            //            return false;
        }
        if (parseFloat($("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val()) > 100) {
            alert("本部门自留产值比例不能大于100%！");
            //            return false;
        }

        CalculationValue();
        return false;

    });

    //扣发金额
    $("#ctl00_ContentPlaceHolder1_txt_ProgramCount").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_ProgramCount").val())) {
            alert("扣发产值金额请输入数字！");
            $(this).val("0");
            //            return false;
        }
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_ProgramCount").val()).length == 0) {
            alert("分配金额不能为空！");
            //            return false;
        }
        CalculationValue();
        return false;
    });


    //分配金额
    $("#ctl00_ContentPlaceHolder1_txt_AllotAccount").change(function () {
        if (!SelectYear()) {
            return false;
        }

        //分配金额
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val())) {
            alert("请输入数字！");
            $(this).val("0");
        }

        var allotAccount = $("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val());

        if (parseFloat(allotAccount) > parseFloat($("#ctl00_ContentPlaceHolder1_lblNotAllotAccount").text())) {
            alert("请确认分配金额不超过未分配金额！");
            $(this).val("0");
            //            return false;
            //            $("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val("0");
        }

        CalculationValue();
        return false;
    });



    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

    messageDialog = $("#auditShow").messageDialog;
    sendMessageClass = new ProjectValueMessageCommon(messageDialog);
    //审核通过按钮
    $("#btnApproval").click(function () {

        if (!SelectYear()) {
            return false;
        }

        if ($.trim($("#txt_DividedPercent").val()).length == 0) {
            alert("请选择院所分成比例！");
            return false;
        }

        //分配金额
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val())) {
            this.focus();
            alert("分配金额请输入数字！");
            return false;
        }

        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
            return false;
        }
        var allotAccount = $("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val());

        if (parseFloat(allotAccount) > parseFloat($("#ctl00_ContentPlaceHolder1_lblNotAllotAccount").text())) {
            alert("请确认分配金额不超过未分配金额！");
            return false;
        }

        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_DesignManagerPercent").val())) {
            alert("设总比例请输入数字！");
            return false;
        }

        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_DesignManagerPercent").val()).length == 0) {
            alert("设总比例不能为空！");
            return false;
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_DesignManagerPercent").val()) > 100) {
            alert("设总比例不能大于100%！");
            return false;
        }

        //转暖通
        if (IsTrunEconomy == 1) {

            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_EconomyValuePercent").val())) {
                alert("转经济所产值比例请输入数字！");
                return false;
            }
            if ($.trim($("#ctl00_ContentPlaceHolder1_txt_EconomyValuePercent").val()).length == 0) {
                alert("转经济所产值不能为空！");
                return false;
            }
            if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_EconomyValuePercent").val()) > 100) {
                alert("转经济所产值比例不能大于100%！");
                return false;
            }
        }

        //转暖通
        if (unitName.indexOf("暖通") < -1) {
            if (IsTrunHavc == 1) {
                if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").val())) {
                    alert("转暖通产值比例请输入数字！");
                    return false;
                }
                if ($.trim($("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").val()).length == 0) {
                    alert("转暖通产值不能为空！");
                    return false;
                }
                if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").val()) > 100) {
                    alert("转暖通产值比例不能大于100%！");
                    return false;
                }
            }
        }

        //转其他部门
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtOtherDeptValuePercent").val())) {
            alert("转出其他部门产值比例请输入数字！");
            return false;
        }
        if ($.trim($("#ctl00_ContentPlaceHolder1_txtOtherDeptValuePercent").val()).length == 0) {
            alert("转出其他部门产值不能为空！");
            return false;
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_txtOtherDeptValuePercent").val()) > 100) {
            alert("转出其他部门比例不能大于100%！");
            return false;
        }

        //本部门自留产值
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val())) {
            alert("本部门自留产值比例请输入数字！");
            return false;
        }

        if ($.trim($("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val()).length == 0) {
            alert("本部门自留产值不能为空！");
            return false;
        }
        if (parseFloat($("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val()) > 100) {
            alert("本部门自留产值比例不能大于100%！");
            return false;
        }

        //方案产值
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_ProgramCount").val())) {
            alert("扣发产值请输入数字！");
            return false;
        }

        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_ProgramCount").val()).length == 0) {
            alert("扣发产值不能为空！");
            return false;
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_UnitValueCountPercent").val()) < 0) {
            alert("本部门产值比例不能为负数！");
            return false;
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text()) <= 0) {
            alert("应分产值比例不能为负数！");
            return false;
        }

        $("#btnApproval").hide();

        var ViewEntity = {
            "pro_ID": $("#HiddenProSysNo").val(),
            "AuditSysID": $("#HiddenAuditRecordSysNo").val(),
            "AllotCount": $("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val(),
            "AllotUser": $("#HiddenLoginUser").val(),
            "Payshicount": $("#ctl00_ContentPlaceHolder1_lblPayShiCount").text(),
            "PaidValuePercent": $("#ctl00_ContentPlaceHolder1_txt_PaidValuePercent").val(),
            "PaidValueCount": $("#txt_PaidValueCount").text(),
            "DesignManagerPercent": $("#ctl00_ContentPlaceHolder1_txt_DesignManagerPercent").val(),
            "DesignManagerCount": $("#txt_DesignManagerCount").text(),
            "AllotValuePercent": $("#ctl00_ContentPlaceHolder1_txtAllotValuePercent").val(),
            "Allotvaluecount": $("#txtAllotValueCount").text(),
            "UnitValuePercent": $("#ctl00_ContentPlaceHolder1_txt_UnitValueCountPercent").val(),
            "UnitValueCount": $("#txtUnitValueCount").text(),
            "EconomyValuePercent": $("#ctl00_ContentPlaceHolder1_txt_EconomyValuePercent").val(),
            "EconomyValueCount": $("#txt_EconomyValueCount").text(),
            "Itemtype": "",
            "Otherdeptallotpercent": $("#ctl00_ContentPlaceHolder1_txtOtherDeptValuePercent").val(),
            "Otherdeptallotcount": $("#txtOtherDeptValueCount").text(),
            "Thedeptallotpercent": $("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val(),
            "Thedeptallotcount": $("#txtTheDeptValueCount").text(),
            "IsTrunEconomy": $("#HiddenIsTrunEconomy").val(),
            "IsTrunHavc": $("#HiddenIsTrunHavc").val(),
            "HavcValuePercent": $("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").val(),
            "HavcValueCount": $("#txt_HavcValueCount").text(),
            "ProgramPercent": $("#ctl00_ContentPlaceHolder1_txt_ProgramPercent").val(),
            "ProgramCount": $("#ctl00_ContentPlaceHolder1_txt_ProgramCount").val(),
            "ShouldBeValuePercent": $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValuePercent").val(),
            "ShouldBeValueCount": $("#txt_ShouldBeValueCount").text(),
            "SecondValue": "1",
            "ActualAllountTime": $("#ctl00_ContentPlaceHolder1_drp_year").val(),
            "FinanceValuePercent": $("#txt_FinanceValuePercent").val(),
            "FinanceValueCount": $("#txt_FinanceValueCount").text(),
            "TheDeptShouldValuePercent": $("#txt_TheDeptShouldValuePercent").val(),
            "TheDeptShouldValueCount": $("#txt_TheDeptShouldValueCount").text(),
            "DividedPercent": $("#txt_DividedPercent").val()
        };

        var jsonObj = Global.toJSON(ViewEntity);
        jsonDataEntity = jsonObj;

        //申请分配
        getUserAndUpdateAudit('0', '0', jsonDataEntity);
    });

    $("#btn_Send").click(function () {
        var _$szmesUser = $(":checkbox[roles='所长']:checked");
        if (_$szmesUser.length == 0) {
            alert("所长审批，请至少选择一个审批人！");
            return false;
        }
        if (isTrunEconomy == 1) {
            var _$jjsmesUser = $(":checkbox[roles='经济所']:checked");
            if (_$jjsmesUser.length == 0) {
                alert("经济所审批，请至少选择一个审批人！");
                return false;
            }
        }
        if ($("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text().indexOf("暖通热力所") == -1) {
            if (isTrunHavc == 1) {
                var _$ntsmesUser = $(":checkbox[roles='暖通所']:checked");
                if (_$ntsmesUser.length == 0) {
                    alert("暖通所审批，请至少选择一个审批人！");
                    return false;
                }
            }
        }

        $(this).attr("disabled", "disabled");

        getUserAndUpdateAudit('0', '1', jsonDataEntity);


    });
    //关闭
    $("#btn_close").click(function () {
        $("#btnApproval").show();
    });

    //审核不通过按钮，具体流程业务，基本同上面的审核通过一致
    $("#btnRefuse").click(function () {

        if ($("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text().indexOf("暖通热力所") == -1) {
            window.location.href = "/ProjectValueandAllot/ProValueandAllListBymaster.aspx";
        }
        else {
            window.location.href = "/ProjectValueandAllot/HaveProjectValueAllotListBymaster.aspx";
        }
        //var auditObj = {
        //    SysNo: $("#HiddenAuditRecordSysNo").val(),
        //    AuditUser: $("#HiddenLoginUser").val()
        //};
        //var jsonObj = Global.toJSON(auditObj);
        //Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx", { "Action": "1", "data": jsonObj }, null, null, function (jsonResult) {
        //    if (jsonResult < 0) {
        //        alert("审核失败！");
        //    } else {
        //        //查询系统新消息
        //        if ($("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text().indexOf("暖通热力所") == -1) {
        //            window.location.href = "/ProjectValueandAllot/ProValueandAllListBymaster.aspx";
        //        }
        //        else {
        //            window.location.href = "/ProjectValueandAllot/HaveProjectValueAllotListBymaster.aspx";
        //        }
        //    }
        //});
    });

    $("#sch_reletive").live("click", function () {
        getDivided();
    });
    $("#btn_search").live("click", function () {
        getDivided();
    });
    $("a[id=spanSelect]").live("click", function () {
        var percent = $(this).attr("dividePercent");
        $("#txt_DividedPercent").val(percent);
        $("#div_Divided").modal("hide");
        CalculationValue();
    });
    //实例化类容
    //    messageDialog = $("#msgReceiverContainer").messageDialog({
    //        "button": {
    //            "发送消息": function () {

    //                var _$szmesUser = $(":checkbox[roles='所长']:checked");
    //                if (_$szmesUser.length == 0) {
    //                    alert("所长审批，请至少选择一个审批人！");
    //                    return false;
    //                }
    //                if (isTrunEconomy == 1) {
    //                    var _$jjsmesUser = $(":checkbox[roles='经济所']:checked");
    //                    if (_$jjsmesUser.length == 0) {
    //                        alert("经济所审批，请至少选择一个审批人！");
    //                        return false;
    //                    }
    //                }
    //                if ($("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text().indexOf("暖通热力所") == -1) {
    //                    if (isTrunHavc == 1) {
    //                        var _$ntsmesUser = $(":checkbox[roles='暖通所']:checked");
    //                        if (_$ntsmesUser.length == 0) {
    //                            alert("暖通所审批，请至少选择一个审批人！");
    //                            return false;
    //                        }
    //                    }
    //                }
    //                getUserAndUpdateAudit('0', '1', jsonDataEntity);
    //            },
    //            "关闭": function () {
    //                $("#btnApproval").attr("disabled", false);
    //                messageDialog.hide();
    //            }
    //        }
    //    });
    // sendMessageClass = new ProjectValueMessageCommon(messageDialog);
});
//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData };

    //提交数据
    $.post(url, data, function (jsonResult) {

        if (jsonResult == "0") {
            alert("发起分配错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("产值分配完成，已全部通过！");
            //查询系统新消息
            window.location.href = "/Coperation/cpr_CorperationListBymaster.aspx";
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}

//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        alert("消息发送成功,等待下一阶段审核人审批！");
        //查询系统新消息
        if ($("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text().indexOf("暖通热力所") == -1) {
            window.location.href = "/ProjectValueandAllot/ProValueandAllListBymaster.aspx";
        }
        else {
            window.location.href = "/ProjectValueandAllot/HaveProjectValueAllotListBymaster.aspx";
        }
    } else {
        alert("消息发送失败！");
    }
}

//计算产值
function CalculationValue() {

    //分配金额
    var allotAccount = $("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val());

    //实收产值(实)
    var paidValuePercent = $("#ctl00_ContentPlaceHolder1_txt_PaidValuePercent").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_PaidValuePercent").val());
    var paidValueCount = parseFloat(paidValuePercent) * parseFloat(allotAccount) * 100;
    $("#txt_PaidValueCount").text(paidValueCount.toFixed(0));

    //设总比例
    var designManagerPercent = $("#ctl00_ContentPlaceHolder1_txt_DesignManagerPercent").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_DesignManagerPercent").val());
    //设总金额(设)
    var DesignManagerCount = parseFloat(designManagerPercent) * parseFloat(allotAccount) * 100;
    $("#txt_DesignManagerCount").text(DesignManagerCount.toFixed(0));

    //转其他部门产值比例
    var otherCountPercent = $("#ctl00_ContentPlaceHolder1_txtOtherDeptValuePercent").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txtOtherDeptValuePercent").val());
    //转其他部门产值(其他)
    var otherCount = parseFloat(otherCountPercent) * parseFloat(allotAccount) * 100;
    $("#txtOtherDeptValueCount").text(otherCount.toFixed(0));

    //转经济所比例（经）
    var economy = $("#ctl00_ContentPlaceHolder1_txt_EconomyValuePercent").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_EconomyValuePercent").val());
    var economyCount = 0;
    if (parseFloat(economy) == 0.25) {
        var economyCountTemp = parseFloat(economy) * parseFloat(allotAccount) * 100;
        if (parseFloat(economyCountTemp) < 200) {
            economyCount = 200;
        } else {
            economyCount = parseFloat(economy) * parseFloat(allotAccount) * 100;
        }
    }
    else {
        //转经济产值
        economyCount = parseFloat(economy) * parseFloat(allotAccount) * 100;
    }
    if (isTrunEconomy == 1) {
        $("#txt_EconomyValueCount").text(economyCount.toFixed(0));
    }

    //转暖通(暖)
    var havcValuePercent = $("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").val());
    //转暖通
    var havcValueCount = parseFloat(havcValuePercent) * (parseFloat(allotAccount) * 10000 - parseFloat(economyCount) - parseFloat(DesignManagerCount)) / 100;
    if (isTrunHavc == 1) {
        $("#txt_HavcValueCount").text(havcValueCount.toFixed(0));
    } else {
        $("#ctl00_ContentPlaceHolder1_txt_HavcValuePercent").val("0.00");
    }

    //扣发金额（扣发）
    var programValueCount = $("#ctl00_ContentPlaceHolder1_txt_ProgramCount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_ProgramCount").val());
    //扣发比例
    var programValuePercent = 0;
    if (parseFloat(allotAccount) == 0) {
        programValuePercent = 0;
    }
    else {
        programValuePercent = parseFloat(programValueCount) / (parseFloat(allotAccount) * 100);
    }
    $("#ctl00_ContentPlaceHolder1_txt_ProgramPercent").attr("value", programValuePercent.toFixed(2));

    //分配产值（分配产值）
    var AllotValuePercent = 100 - parseFloat(designManagerPercent);
    $("#ctl00_ContentPlaceHolder1_txtAllotValuePercent").attr("value", AllotValuePercent);
    var allotValueCount = parseFloat(AllotValuePercent) * parseFloat(allotAccount) * 100;
    $("#txtAllotValueCount").text(allotValueCount.toFixed(0));

    // 100%-转经济所 -暖通 -转经济 -其他 =allotCount_A
    var allotCount_A_Prt = paidValuePercent - economy - havcValuePercent - otherCountPercent;
    var allotCount_A = parseFloat(allotAccount) * 10000 - parseFloat(economyCount) - parseFloat(havcValueCount) - parseFloat(otherCount);

    //本部门自留百分比
    var theDeptValuePercent = $("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txtTheDeptValuePercent").val());
    //本部门自留产值（自留）
    var theDeptValueCount = parseFloat(theDeptValuePercent) * parseFloat(allotCount_A) * 0.01;
    $("#txtTheDeptValueCount").text(theDeptValueCount.toFixed(0));

    // 本部门产值合计 等于 allotCount_A-自留
    var allotCount_B = allotCount_A - parseFloat(theDeptValueCount)
    var unitValueCountCount = allotCount_B; //parseFloat(allotAccount) * 10000 - parseFloat(economyCount) - parseFloat(otherCount) - parseFloat(havcValueCount) - parseFloat(theDeptValueCount);
    $("#txtUnitValueCount").text(unitValueCountCount.toFixed(0));

    //本部门产值合计比例 
    var allotCount_B_Prt = allotCount_A_Prt - parseFloat(allotCount_A_Prt * theDeptValuePercent * 0.01);
    //var unitValueCountPercent = allotCount_B_Prt;
    var unitValueCountPercent = 0;
    if (parseFloat(allotAccount) == 0) {
        unitValueCountPercent = 0;
    } else {
        unitValueCountPercent = parseFloat(unitValueCountCount) / (parseFloat(allotAccount) * 100);
    }
    $("#ctl00_ContentPlaceHolder1_txt_UnitValueCountPercent").attr("value", unitValueCountPercent.toFixed(2));

    //应分产值金额 等于分配产值 -转经济所-转暖通-转其他部门-本部门自留产值-设总-扣发
    var shouleBeValueCount = parseFloat(allotCount_B) - parseFloat(DesignManagerCount) - parseFloat(programValueCount);
    $("#txt_ShouldBeValueCount").text(shouleBeValueCount.toFixed(0));

    // 应分产值比例 等于100% -转经济所-转暖通-转其他部门-本部门自留产值-设总-扣发
    //var shouldBeValuePercent = parseFloat(allotCount_B_Prt) - parseFloat(designManagerPercent) - parseFloat(programValuePercent);
    var shouldBeValuePercent = 0;
    if (parseFloat(allotAccount) == 0) {
        shouldBeValuePercent = 0;
    } else {
        shouldBeValuePercent = parseFloat(shouleBeValueCount) / (parseFloat(allotAccount) * 100);
    }
    $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValuePercent").attr("value", shouldBeValuePercent.toFixed(2));

    //11 财务统计产值
    var financeValueCount = parseFloat(allotAccount) * 10000 - parseFloat(economyCount) - parseFloat(havcValueCount) - parseFloat(otherCount) - parseFloat(programValueCount);
    $("#txt_FinanceValueCount").text(financeValueCount.toFixed(0));

    var financeValuePercent = 0;
    if (parseFloat(allotAccount) == 0) {
        financeValuePercent = 0;
    } else {
        financeValuePercent = parseFloat(financeValueCount) / (parseFloat(allotAccount) * 100);
    }
    $("#txt_FinanceValuePercent").attr("value", financeValuePercent.toFixed(2));

    //12 部分应留产值 
    //系数
    var dividedPercent = $("#txt_DividedPercent").val().length == 0 ? 0 : parseFloat($("#txt_DividedPercent").val());
    var theDeptShouldValueCount = financeValueCount * parseFloat(dividedPercent);
    $("#txt_TheDeptShouldValueCount").text(theDeptShouldValueCount.toFixed(0));

    var theDeptShouldValuePercent = 0;
    if (parseFloat(allotAccount) == 0) {
        theDeptShouldValuePercent = 0;
    } else {
        theDeptShouldValuePercent = parseFloat(theDeptShouldValueCount) / (parseFloat(allotAccount) * 100);
    }
    $("#txt_TheDeptShouldValuePercent").attr("value", theDeptShouldValuePercent.toFixed(2));
}

//选择年份
function SelectYear() {

    var flag = true;
    if ($("#ctl00_ContentPlaceHolder1_drp_year").val() == "-1") {
        alert("请选择分配年份");
        flag = false;
        return false;
    }
    return flag;
}


function getDivided() {
    var year = $("#ctl00_ContentPlaceHolder1_drp_year1").children("option:selected").val();
    var unitName = $.trim($("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text());
    $.post("/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx", { "Action": "7", "year": year, "unitName": unitName }, function (jsonResult) {
        $("#tb_Divided tr:gt(0)").remove();
        if (jsonResult != null && jsonResult != "") {
            var json = eval('(' + jsonResult + ')');
            var data = json == null ? "" : json.ds;
            if (data != null && data != "") {
                $.each(data, function (i, n) {
                    var trString = "<tr><td>" + n.unit_Name + "</td>";
                    trString += "<td >" + n.DivideYear + "</td>";
                    trString += "<td >" + n.DividePercent + "</td>";
                    trString += "<td><a href=\"###\" style=\"color:blue;\" id=\"spanSelect\"  dividePercent=" + n.DividePercent + ">选择</a></td>";
                    trString += "</tr>";
                    $("#tb_Divided").append(trString);
                });
            }
        } else {
            var trString = "<tr style='color:Red; text-align:center;'><td colspan=4>没有对应的院所分成比例，请到对应页面添加!</td></tr>";
            $("#tb_Divided").append(trString);
        }
    });

}