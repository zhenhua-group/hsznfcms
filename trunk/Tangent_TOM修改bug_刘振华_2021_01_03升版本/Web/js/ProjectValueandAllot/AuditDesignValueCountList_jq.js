﻿$(function () {

    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/AuditDesignValueCountListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '项目名称', '承接部门', '合同额(万元)', '审核总金额(元)', '建筑审核金额(元)', '结构审核金额(元)', '设计总金额(元)', '建筑设计金额(元)', '结构设计金额(元)', '建筑专业分配', '结构专业分配', '分配状态'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 250, formatter: colNameShowFormatter },
                             { name: 'Unit', index: 'Unit', width: 80, align: 'center' },
                             { name: 'Cpr_Acount', index: 'p.cpr_Acount', width: 100, align: 'center' },
                              { name: 'AuditCount', index: 'AuditCount', width: 100, align: 'center' },
                             { name: 'jz_AuditAmount', index: 'jz_AuditAmount', width: 100, align: 'center',sort:false },
                             { name: 'jg_AuditAmount', index: 'jg_AuditAmount', width: 100, align: 'center', sort: false },
                             { name: 'DesignCount', index: 'DesignCount', width: 100, align: 'center' },
                             { name: 'jz_DesignCount', index: 'jz_DesignCount', width: 100, align: 'center', sort: false },
                             { name: 'jg_DesignCount', index: 'jg_DesignCount', width: 100, align: 'center', sort: false },                            
                             { name: 'pro_ID', index: 'pro_ID', width: 90, align: 'center', formatter: colStrJZShowFormatter },
                             { name: 'pro_ID', index: 'pro_ID', width: 90, align: 'center', formatter: colStrJGShowFormatter },
                             {

                                  name: 'fpzt', index: 'fpzt', width: 80, align: 'center', sort: false, formatter: function (celvalue, options, rowData) {
                                      if (parseInt(celvalue) > 0) {
                                          return "<font color='green'>已分配</font>";
                                      }
                                      else {
                                          return "<font color='red'>未分配</font>";
                                      }
                                  }
                              },
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'p.pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/AuditDesignValueCountListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });




    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/AuditDesignValueCountListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "action": "sel" },
            page: 1,
            sortname: 'p.pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/AuditDesignValueCountListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "action": "sel" },
            page: 1,
            sortname: 'p.pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });


});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}
//详细
function colStrShowFormatter(celvalue, options, rowData) {
    var auditString = '<a href="../ProjectValueandAllot/SaveDesignerAllot.aspx?proid=' + celvalue + '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year option:selected").val() + '&pagetype=edit" class="cls_chk" style="height: 26px;">编辑</a>&nbsp;|&nbsp;';
    auditString += '<a href="../ProjectValueandAllot/SaveDesignerAllot.aspx?proid=' + celvalue + '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year option:selected").val() + '&pagetype=ck" class="cls_chk" style="height: 26px;">查看</a>';

    return auditString;
}
//建筑详细
function colStrJZShowFormatter(celvalue, options, rowData) {
    var auditString = '<a href="../ProjectValueandAllot/SaveDesignerAllot.aspx?proid=' + celvalue + '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year option:selected").val() + '&speid=1&pagetype=edit" class="cls_chk" style="height: 26px;">编辑</a>&nbsp;|&nbsp;';
    auditString += '<a href="../ProjectValueandAllot/SaveDesignerAllot.aspx?proid=' + celvalue + '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year option:selected").val() + '&speid=1&pagetype=ck" class="cls_chk" style="height: 26px;">查看</a>';

    return auditString;
}
//结构详细
function colStrJGShowFormatter(celvalue, options, rowData) {
    var auditString = '<a href="../ProjectValueandAllot/SaveDesignerAllot.aspx?proid=' + celvalue + '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year option:selected").val() + '&speid=2&pagetype=edit" class="cls_chk" style="height: 26px;">编辑</a>&nbsp;|&nbsp;';
    auditString += '<a href="../ProjectValueandAllot/SaveDesignerAllot.aspx?proid=' + celvalue + '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year option:selected").val() + '&speid=2&pagetype=ck" class="cls_chk" style="height: 26px;">查看</a>';

    return auditString;
    }
//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));
    }

    var sumAcount = parseFloat($("#jqGrid").getCol("Cpr_Acount", false, 'sum')).toFixed(4);
    var allAuditCount = parseFloat($("#jqGrid").getCol("AuditCount", false, 'sum')).toFixed(4);
    var allDesignCount = parseFloat($("#jqGrid").getCol("DesignCount", false, 'sum')).toFixed(4);
    var alljzAuditCount = parseFloat($("#jqGrid").getCol("jz_AuditAmount", false, 'sum')).toFixed(4);
    var alljzDesignCount = parseFloat($("#jqGrid").getCol("jz_DesignCount", false, 'sum')).toFixed(4);
    var alljgAuditCount = parseFloat($("#jqGrid").getCol("jg_AuditAmount", false, 'sum')).toFixed(4);
    var alljgDesignCount = parseFloat($("#jqGrid").getCol("jg_DesignCount", false, 'sum')).toFixed(4);

    $("#jqGrid").footerData('set', { pro_name: "合计:", Cpr_Acount: sumAcount, AuditCount: allAuditCount, jz_AuditAmount: alljzAuditCount, jg_AuditAmount: alljgAuditCount, DesignCount: allDesignCount, jz_DesignCount: alljzDesignCount, jg_DesignCount: alljgDesignCount }, false);


}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}
