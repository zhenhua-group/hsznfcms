﻿//返回消息列表
var pageIndex;
var MessageType;
var TypePost;
var MessageAction;
var Aflag;
var MessageKeys;


$(function () {

    CommonControl.SetFormWidth();

    //获取消息列表参数
    pageIndex = $("#pageIndex").val();
    MessageType = $("#MessageType").val();
    TypePost = $("#TypePost").val();
    MessageAction = $("#MessageAction").val();
    Aflag = $("#Aflag").val();
    MessageKeys = $("#MessageKeys").val();

    $("#CoperationBaseInfo tr:odd").css({ background: "White" });

    // 设置文本框样式
    CommonControl.SetTextBoxStyle();

    var status = $("#HiddenStatus").val();
    if (status != 'S') {
        if ($("#hiddenType").val() == "tran") {
            $("#fieldAudit").hide();
            $("#tbTopAudit").hide();
            $("#OneTable").hide();
            $("#btnPass").hide();
            $("#btnNotPass").hide();
        }
        else {
            $("#fieldAudit").show();
            $("#tbTopAudit").show();
            $("#OneTable").show();
            $("#btnPrintValue").hide();
        }
    } else {
        $("#fieldAudit").hide();
        $("#tbTopAudit").hide();
        $("#OneTable").hide();
        $("#btnPass").hide();
        $("#btnNotPass").hide();
    }

    var oneSuggstion = $("#HiddenOneSuggesiton").val();
    if (oneSuggstion && oneSuggstion.length > 0) {
        $("#OneSuggstion").val(oneSuggstion).attr("disabled", true).css("color", "black");
        $("#AuditUser", $("#OneTable")).html($("#HiddenAuditUser").val() + " <br/> " + $("#HiddenAuditData").val());
        $("#fieldAudit").show();
        $("#tbTopAudit").show();
        $("#OneTable").show();
        $("#btnPass").hide();
        $("#btnNotPass").hide();
    }

    //判断此信息是否被删除
    if ($("#tbdelete tr td").text() == "此信息已被删除!") {

        $("#fieldset").hide();
        $("#fidMember").hide();
        $("#fieldAudit").hide();
        $("#OneTable").hide();
        $("#tbTopAudit").hide();
        $("#btnPass").hide();
        $("#btnNotPass").hide();
        $("#tbdelete").show();
        $("#btnPrintValue").hide();
    }
    //审核通过按钮
    $("#btnPass").click(function () {

        var auditObj = {
            SysNo: $("#HiddenSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            Status: "S",
            ProSysNo: $("#hidProID").val(),
            AllotID: $("#HidAllotID").val()
        };

        if ($.trim($("#OneSuggstion").val()).length == 0) {
            alert("生产经营部意见不能为空！");
            return false;
        }
        $("#btnPass").attr("disabled", true);
        auditObj.OneSuggestion = $.trim($("#OneSuggstion").val());


        if (!confirm("是否通过审核信息?")) {
            return false;
        }
        $("#btnPass").attr("disabled", true);

        var jsonObj = Global.toJSON(auditObj);

        Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": 6, "data": jsonObj, "flag": 1 }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {
                    //改变消息状态
                    var msg = new MessageCommProjAllot($("#msgno").val());
                    msg.ReadMsg();
                    //消息
                    alert("分配通过成功，消息已发送给申请人！");
                }
                //查询系统新消息
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

                //window.history.back();
            }
        });
    });

    //不通过
    //审核不通过按钮，具体流程业务，基本同上面的审核通过一致
    $("#btnNotPass").click(function () {

        var auditObj = {
            SysNo: $("#HiddenSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            Status: "D",
            ProSysNo: $("#hidProID").val(),
            AllotID: $("#HidAllotID").val()
        };

        if ($.trim($("#OneSuggstion").val()).length == 0) {
            alert("生产经营部意见不能为空！");
            return false;
        }
        auditObj.OneSuggestion = $.trim($("#OneSuggstion").val());

        if (!confirm("是否不通过审核信息?")) {
            return false;
        }
        $("#btnNotPass").attr("disabled", true);

        var jsonObj = Global.toJSON(auditObj);

        Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": 6, "data": jsonObj, "flag": 1, "AllotUser": $("#HiddenAllotUser").val() }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {
                    //改变消息状态
                    var msg = new MessageCommProjAllot($("#msgno").val());
                    msg.ReadMsg();
                    //消息
                    alert("分配不通过成功，消息已发送给申请人！");
                }
                //查询系统新消息
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

                // window.history.back();
            }
        });

    });

});


