﻿$(document).ready(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '项目名称', '承接部门', '自留产值(元)', '审核金额(元)', '设计金额(元)', '暖通转产值(元)', '分配金额(元)', '分配进度', '详细', '分配'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'AuditRecordSysNo', index: 'AuditRecordSysNo', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 200, formatter: colNameShowFormatter },
                             { name: 'Unit', index: 'Unit', width: 80, align: 'center' },
                             { name: 'TheDeptValueCount', index: 'TheDeptValueCount', width: 80, align: 'center' },
                             { name: 'AuditCount', index: 'AuditCount', width: 80, align: 'center' },
                             { name: 'DesignCount', index: 'DesignCount', width: 100, align: 'center' },
                             { name: 'HavcCount', index: 'HavcCount', width: 100, align: 'center' },
                              //{ name: 'LoanValueCount', index: 'LoanValueCount', width: 130, align: 'center' },
                             { name: 'AllotCount', index: 'AllotCount', width: 80, align: 'center' },
                             { name: 'Percent', index: 'Percent', width: 100, align: 'center', formatter: colStatusShowFormatter },
                             { name: 'pro_ID', index: 'pro_ID', width: 40, align: 'center', formatter: colStrShowFormatter },
                             { name: 'pro_ID', index: 'pro_ID', width: 60, align: 'center', formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "action": "ProjectSecond", "type": "", "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //当前登录用户SysNo
    var userSysNo = $("#HiddenUserSysNo").val();

    //发起审核按钮
    $("span[id=InitiatedAudit]").live("click", function () {

        window.location.href = "AddProjectSecondValueAllotBymaster.aspx?proid=" + $(this).attr("proSysNo") + "&ValueAllotAuditSysNo=0";
    });

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_cprName"));
    autoComplete.GetAutoAJAX();
});
//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}
//详细
function colStrShowFormatter(celvalue, options, rowData) {
    var auditString = '<a href="../ProjectValueandAllot/ProjectSecondValueAllotDetailsBymaster.aspx?proid=' + celvalue + '" class="cls_chk" style="height: 26px;">查看</a>';

    return auditString;
}
//进度
function colStatusShowFormatter(celvalue, options, rowData) {
    var str = '<div class="progressbar" style="cursor: pointer;height:20px;margin:1px 1px;" percent="' + celvalue + '" title="' + celvalue + '%"></div>';

    return str;
}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var linkAction = "";
    linkAction = "<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" proSysNo=\"" + celvalue + "\">发起分配</span>";
    return linkAction;
}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));
    }
    //进度条
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("percent"));
        $(item).progressbar({
            value: percent
        });
    });
}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}
