﻿var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
var cprProcess;
$(function () {
    CommonControl.SetFormWidth();

    $("#CoperationBaseInfo tr:odd").css({ background: "White" });

    // 设置文本框样式
    CommonControl.SetTextBoxStyle();

    //是否可以编辑
    IsEdit = $("#HiddenIsEdit").val();

    if (IsEdit == 1) {
        $("#fidProcess  input[type=text]").attr("disabled", false);
    } else {
        $("#fidProcess  input[type=text]").attr("disabled", true);
    }

    cprProcess = $("#HiddenCoperationProcess").val();

    $("#tabsMem").tabs();
    $("#tabs").tabs();

    if ($("#HiddenAllotID").val() == "") {
        $("#tbTwoSave").hide();
        $("#memField").show();
    } else {

        $("#ctl00_ContentPlaceHolder1_stage").attr("disabled", "disabled");
        $("input", "#fidOne").attr("disabled", "disabled");
        $("#btnApprovalOne").attr("disabled", "disabled");
        $("#txt_DesignManagerPercent").attr("disabled", "disabled");
        $("#txtTheDeptValuePercent").attr("disabled", "disabled");
        $("#designUserName").attr("disabled", "disabled");
        $("#ctl00_ContentPlaceHolder1_drp_year").attr("disabled", "disabled");
        if ($("#gvProjectValueProcess tr td").text() == "请确定策划人员是否添加’建筑‘专业的人员。或者安装专业的人员信息。") {
            $("#btnApprovalTwo").attr("disabled", "disabled");
        }

        SetProcess(cprProcess);

        if ($("#tdNoMember tr td").text() == "没有人员信息") {
            $("#btnApprovalTwo").attr("disabled", "disabled");
        }
    }

    var isRounding = $("#HiddenIsRounding").val();

    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;


    //设总
    $("#txt_DesignManagerPercent").change(function () {

        if (!reg.test($("#txt_DesignManagerPercent").val())) {
            alert("设总比例请输入数字！");
            //            return false;
        }

        if (parseFloat($("#txt_DesignManagerPercent").val()) > 100) {
            alert("设总比例不能大于100%！");
            //            return false;
        }

        CalculationValue();
        return false;
    });

    //本部门自留
    $("#txtTheDeptValuePercent").change(function () {

        if (!reg.test($("#txtTheDeptValuePercent").val())) {
            alert("本部门自留产值比例请输入数字！");
            //            return false;
        }

        if (parseFloat($("#txtTheDeptValuePercent").val()) > 100) {
            alert("本部门自留产值比例不能大于100%！");
            //            return false;
        }

        CalculationValue();
        return false;
    });


    var chooseUser = new ChooseUserControl($("#chooseUserotherDiv"), PMCallBack);




    //项目负责
    $("#designUserName").focus(function () {
        $("#chooseUserOtherDiv").modal();
        //        $("#chooseUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 600,
        //            height: "auto",
        //            resizable: false,
        //            title: "项目负责"
        //        }).dialog("open");
        //        return false;
    });


    function PMCallBack(userObj) {
        $("#designUserName").val(userObj.username);
        $("#designUserID").val(userObj.usersysno);
    }
    //保存按钮--保存信息
    $("#btnApprovalOne").click(function () {

        if ($("#ctl00_ContentPlaceHolder1_drp_year").val() == "-1") {
            alert("请选择项目分配年份！");
            return false;
        }

        if ($("#ctl00_ContentPlaceHolder1_stage").val() == "-1") {
            alert("请选择项目分配阶段！");
            return false;
        }

        if ($.trim($("#designUserName").val()).length == 0) {
            alert("请选择设总");
            return false;
        }

        if ($.trim($("#txt_DesignManagerPercent").val()).length == 0) {
            alert("设总比例不能为空！");
            return false;
        }

        if (!reg.test($("#txt_DesignManagerPercent").val())) {
            alert("设总比例请输入数字！");
            return false;
        }

        if (parseFloat($("#txt_DesignManagerPercent").val()) > 100) {
            alert("设总比例不能大于100%！");
            return false;
        }

        //本部门自留产值
        if (!reg.test($("#txtTheDeptValuePercent").val())) {
            alert("本部门自留产值比例请输入数字！");
            return false;
        }

        if ($.trim($("#txtTheDeptValuePercent").val()).length == 0) {
            alert("本部门自留产值不能为空！");
            return false;
        }
        if (parseFloat($("#txtTheDeptValuePercent").val()) > 100) {
            alert("本部门自留产值比例不能大于100%！");
            return false;
        }

        if (parseFloat($("#txt_ShouldBeValuePercent").val()) < 0) {
            alert("应分产值比例不能为负数！");
            return false;
        }

        var ViewEntity = {
            "Pro_ID": $("#HiddenProSysNo").val(),
            "AllotID": $("#HiddenTranAllotID").val(),
            "AllotCount": $("#lblTotalCount").text(),
            "AllotUser": $("#HiddenLoginUser").val(),
            "TotalCount": $("#ctl00_ContentPlaceHolder1_lblAllotCount").text(),
            "ItemType": $("#ctl00_ContentPlaceHolder1_stage").val(),
            "Thedeptallotpercent": $("#txtTheDeptValuePercent").val(),
            "Thedeptallotcount": $("#txtTheDeptValueCount").text(),
            "ProgramPercent": 0,
            "ProgramCount": 0,
            "AllotPercent": $("#txt_ShouldBeValuePercent").val(),
            "AllotCount": $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text(),
            "ActualAllountTime": $("#ctl00_ContentPlaceHolder1_drp_year").val(),
            "DesignUserName": $("#designUserName").val(),
            "DesignUserID": $("#designUserID").val(),
            "DesignManagerPercent": $("#txt_DesignManagerPercent").val(),
            "DesignManagerCount": $("#txt_DesignManagerCount").text()
        };
        $("#btnApprovalOne").attr("disabled", true);
        var jsonObj = Global.toJSON(ViewEntity);

        var mark = "";
        if ($.trim($("#ctl00_ContentPlaceHolder1_stage").val()) == "5") {
            var re = /\([^\)]+\)/g
            mark = $("#ctl00_ContentPlaceHolder1_stage option:selected").text();
            mark = mark.match(re)[0];
            mark = mark.substring(1, mark.length - 1);
        }
        Global.SendRequest("/HttpHandler/ProjectValueandAllot/TranProjectValueAllot.ashx", { "Action": "3", "data": jsonObj, "AllotID": $("#HiddenAllotID").val(), "mark": mark }, null, null, function (jsonResult) {
            if (jsonResult < 0) {
                alert("审核失败！");
            } else {
                //查询系统新消息
                $("#HiddenAllotID").val(jsonResult);
                window.location.reload();
                $("#designUserName").attr("href", "");
            }
        });
    });
    messageDialog = $("#auditShow").messageDialog;
    sendMessageClass = new MessageCommon(messageDialog);
    //上一步
    $("#btn_Up").click(function () {
        $("#ctl00_ContentPlaceHolder1_stage").attr("disabled", false);
        $("input", "#fidOne").attr("disabled", false);
        $("#btnApprovalOne").attr("disabled", false);
        $("#txt_DesignManagerPercent").attr("disabled", false);
        $("#txtTheDeptValuePercent").attr("disabled", false);
        $("#designUserName").attr("disabled", false);
        $("#ctl00_ContentPlaceHolder1_drp_year").attr("disabled", false);
    });
    //保存按钮--保存信息人员信息
    $("#btnApprovalTwo").click(function () {

        var finalDataObj = {
            "ProNo": $("#HiddenProSysNo").val(),
            "AllotID": $("#HiddenTranAllotID").val(),
            "Stage": new Array(),
            "AuditUser": $("#HiddenLoginUser").val(),
            "StageSpe": new Array(),
            "DesignProcess": new Array(),
            "DesignProcessTwo": new Array(),
            "DesignProcessThree": new Array(),
            "DesignProcessFour": new Array(),
            "OutDoor": new Array(),
            "DesignProcessFive": new Array(),
            "ItemType": cprProcess,
            "ValueByMember": new Array()
        };

        //项目各阶段
        if (cprProcess == 0) {

            if (IsEdit == "1") {
                //验证
                if (!ValidationOne()) {
                    return false;
                }
            }

            if (!ValidationMem()) {
                return false;
            }

            // 取得 项目各阶段
            $.each($("#ctl00_ContentPlaceHolder1_gvOne tr"), function (index, tr) {
                finalDataObj.Stage[index] =
                {
                    "ItemType": $.trim($(tr).children("td:eq(0)").text()),
                    "ProgramPercent": $.trim($(tr).children("td:eq(1)").find("input").val()),
                    "ProgramAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "preliminaryPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "preliminaryAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "WorkDrawPercent": $.trim($(tr).children("td:eq(5)").find("input").val()),
                    "WorkDrawAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "LateStagePercent": $.trim($(tr).children("td:eq(7)").find("input").val()),
                    "LateStageAmount": $.trim($(tr).children("td:eq(8)").text())
                };
            });


            // 取得工序
            $.each($("#gvdesignProcessOne tr"), function (index, tr) {
                finalDataObj.DesignProcess[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                    "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                    "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                    "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                    "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                };
            });

            //初步设计工序
            $.each($("#gvdesignProcessTwo tr"), function (index, tr) {
                finalDataObj.DesignProcessTwo[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
            });

            //施工图工序
            $.each($("#gvdesignProcessThree tr"), function (index, tr) {
                finalDataObj.DesignProcessThree[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
            });
            //后期服务
            $.each($("#gvdesignProcessFour tr"), function (index, tr) {
                finalDataObj.DesignProcessFour[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                    "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                    "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                    "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                    "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                };
            });

            //专业
            var arraySpecialty = new Array();
            $("#stagespetable table:eq(1)  td").not(":last").not(":first").each(function () {
                arraySpecialty.push($(this).text());
            });

            //方案设计
            var arrayProgram = new Array();
            $("#gvProjectStageSpe tr:eq(0) td:even").not(":first").not(":last").each(function () {
                arrayProgram.push($(this).text());
            });
            var arrayProgramPercent = ArrayProgramPercent();


            //初步设计
            var arrayPreliminary = new Array();

            $("#gvProjectStageSpe tr:eq(1) td:even").not(":first").not(":last").each(function () {
                arrayPreliminary.push($(this).text());
            });
            var arrayPreliminaryPercent = ArrayPreliminaryPercent();


            //施工图设计
            var arrayWorkDraw = new Array();
            $("#gvProjectStageSpe tr:eq(2) td:even").not(":first").not(":last").each(function () {
                arrayWorkDraw.push($(this).text());
            });
            var arrayWorkDrawPercent = ArrayWorkDrawPercent();

            //后期服务
            var arrayLateStage = new Array();
            $("#gvProjectStageSpe tr:eq(3) td:even").not(":first").not(":last").each(function () {
                arrayLateStage.push($(this).text());
            });
            var arrayLateStagePercent = ArrayLateStagePercent();

            for (var i = 0; i < arraySpecialty.length; i++) {
                finalDataObj.StageSpe[i] =
                {
                    "Specialty": arraySpecialty[i],
                    "ProgramPercent": arrayProgramPercent[i],
                    "ProgramCount": arrayProgram[i],
                    "preliminaryPercent": arrayPreliminaryPercent[i],
                    "preliminaryCount": arrayPreliminary[i],
                    "WorkDrawPercent": arrayWorkDrawPercent[i],
                    "WorkDrawCount": arrayWorkDraw[i],
                    "LateStagePercent": arrayLateStagePercent[i],
                    "LateStageCount": arrayLateStage[i]
                };

            }

        } //方案+初设
        else if (cprProcess == 1) {

            if (IsEdit == 1) {
                if (!ValidationTwo()) {
                    return false;
                }
            }

            if (!ValidationMem()) {
                return false;
            }

            // 取得
            $.each($("#ctl00_ContentPlaceHolder1_gvTwo tr"), function (index, tr) {
                finalDataObj.Stage[index] =
                { "ItemType": $.trim($(tr).children("td:eq(0)").text()), "ProgramPercent": $.trim($(tr).children("td:eq(1)").find("input").val()), "ProgramAmount": $.trim($(tr).children("td:eq(2)").text()), "preliminaryPercent": $.trim($(tr).children("td:eq(3)").find("input").val()), "preliminaryAmount": $.trim($(tr).children("td:eq(4)").text()) };
            });


            // 取得工序
            $.each($("#gvdesignProcessOne tr"), function (index, tr) {
                finalDataObj.DesignProcess[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                    "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                    "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                    "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                    "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                };
            });

            //初步设计工序
            $.each($("#gvdesignProcessTwo tr"), function (index, tr) {
                finalDataObj.DesignProcessTwo[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
            });

            //专业
            var arraySpecialty = new Array();
            $("#stagespetable table:eq(1)  td").not(":last").not(":first").each(function () {

                arraySpecialty.push($(this).text());
            });

            //方案设计
            var arrayProgram = new Array();
            $("#gvProjectStageSpe tr:eq(0) td:even").not(":first").not(":last").each(function () {
                arrayProgram.push($(this).text());
            });
            var arrayProgramPercent = ArrayProgramPercent();


            //初步设计
            var arrayPreliminary = new Array();

            $("#gvProjectStageSpe tr:eq(1) td:even").not(":first").not(":last").each(function () {
                arrayPreliminary.push($(this).text());
            });
            var arrayPreliminaryPercent = ArrayPreliminaryPercent();

            for (var i = 0; i < arraySpecialty.length; i++) {
                finalDataObj.StageSpe[i] =
                {
                    "Specialty": arraySpecialty[i],
                    "ProgramPercent": arrayProgramPercent[i],
                    "ProgramCount": arrayProgram[i],
                    "preliminaryPercent": arrayPreliminaryPercent[i],
                    "preliminaryCount": arrayPreliminary[i]

                };

            }
        } //施工图+后期
        else if (cprProcess == 2) {

            if (IsEdit == 1) {

                if (!ValidationThree()) {
                    return false;
                }
            }

            if (!ValidationMem()) {
                return false;
            }

            // 取得
            $.each($("#ctl00_ContentPlaceHolder1_gvThree tr"), function (index, tr) {
                finalDataObj.Stage[index] =
                { "ItemType": $.trim($(tr).children("td:eq(0)").text()), "WorkDrawPercent": $.trim($(tr).children("td:eq(1)").find("input").val()), "WorkDrawAmount": $.trim($(tr).children("td:eq(2)").text()), "LateStagePercent": $.trim($(tr).children("td:eq(3)").find("input").val()), "LateStageAmount": $.trim($(tr).children("td:eq(4)").text()) };
            });


            //施工图工序
            $.each($("#gvdesignProcessThree tr"), function (index, tr) {
                finalDataObj.DesignProcessThree[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
            });
            //后期服务
            $.each($("#gvdesignProcessFour tr"), function (index, tr) {
                finalDataObj.DesignProcessFour[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                    "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                    "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                    "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                    "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                };
            });

            //专业
            var arraySpecialty = new Array();
            $("#stagespetable table:eq(1)  td").not(":last").not(":first").each(function () {

                arraySpecialty.push($(this).text());
            });

            //施工图设计
            var arrayWorkDraw = new Array();
            $("#gvProjectStageSpe tr:eq(0) td:even").not(":first").not(":last").each(function () {
                arrayWorkDraw.push($(this).text());
            });
            var arrayWorkDrawPercent = ArrayProgramPercent();

            //后期服务
            var arrayLateStage = new Array();
            $("#gvProjectStageSpe tr:eq(1) td:even").not(":first").not(":last").each(function () {
                arrayLateStage.push($(this).text());
            });
            var arrayLateStagePercent = ArrayPreliminaryPercent();


            for (var i = 0; i < arraySpecialty.length; i++) {
                finalDataObj.StageSpe[i] =
                {
                    "Specialty": arraySpecialty[i],
                    "WorkDrawPercent": arrayWorkDrawPercent[i],
                    "WorkDrawCount": arrayWorkDraw[i],
                    "LateStagePercent": arrayLateStagePercent[i],
                    "LateStageCount": arrayLateStage[i]
                };

            }
        } //施工图+初设+后期
        else if (cprProcess == 3) {

            if (IsEdit == 1) {
                if (!ValidationFour()) {
                    return false;
                }
            }

            if (!ValidationMem()) {
                return false;
            }

            // 取得
            $.each($("#ctl00_ContentPlaceHolder1_gvFour tr"), function (index, tr) {
                finalDataObj.Stage[index] =
                {
                    "ItemType": $.trim($(tr).children("td:eq(0)").text()),
                    "preliminaryPercent": $.trim($(tr).children("td:eq(1)").find("input").val()),
                    "preliminaryAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "WorkDrawPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "WorkDrawAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "LateStagePercent": $.trim($(tr).children("td:eq(5)").find("input").val()),
                    "LateStageAmount": $.trim($(tr).children("td:eq(6)").text())
                };
            });

            //初步设计工序
            $.each($("#gvdesignProcessTwo tr"), function (index, tr) {
                finalDataObj.DesignProcessTwo[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
            });

            //施工图工序
            $.each($("#gvdesignProcessThree tr"), function (index, tr) {
                finalDataObj.DesignProcessThree[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
            });
            //后期服务
            $.each($("#gvdesignProcessFour tr"), function (index, tr) {
                finalDataObj.DesignProcessFour[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                    "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                    "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                    "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                    "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                };
            });

            //专业
            var arraySpecialty = new Array();
            $("#stagespetable table:eq(1)  td").not(":last").not(":first").each(function () {
                arraySpecialty.push($(this).text());
            });

            //初步设计
            var arrayPreliminary = new Array();

            $("#gvProjectStageSpe tr:eq(0) td:even").not(":first").not(":last").each(function () {
                arrayPreliminary.push($(this).text());
            });
            var arrayPreliminaryPercent = ArrayProgramPercent();


            //施工图设计
            var arrayWorkDraw = new Array();
            $("#gvProjectStageSpe tr:eq(1) td:even").not(":first").not(":last").each(function () {
                arrayWorkDraw.push($(this).text());
            });
            var arrayWorkDrawPercent = ArrayPreliminaryPercent();

            //后期服务
            var arrayLateStage = new Array();
            $("#gvProjectStageSpe tr:eq(2) td:even").not(":first").not(":last").each(function () {
                arrayLateStage.push($(this).text());
            });
            var arrayLateStagePercent = ArrayWorkDrawPercent();


            for (var i = 0; i < arraySpecialty.length; i++) {
                finalDataObj.StageSpe[i] =
                {
                    "Specialty": arraySpecialty[i],
                    "preliminaryPercent": arrayPreliminaryPercent[i],
                    "preliminaryCount": arrayPreliminary[i],
                    "WorkDrawPercent": arrayWorkDrawPercent[i],
                    "WorkDrawCount": arrayWorkDraw[i],
                    "LateStagePercent": arrayLateStagePercent[i],
                    "LateStageCount": arrayLateStage[i]
                };

            } //其他情况 例如 室外工程
        }
        else if (cprProcess == 10) {

            if (IsEdit == 1) {
                if (!ValidationTen()) {
                    return false;
                }
            }

            if (!ValidationMem()) {
                return false;
            }

            // 取得
            $.each($("#ctl00_ContentPlaceHolder1_gvTen tr"), function (index, tr) {
                finalDataObj.Stage[index] =
                {
                    "ItemType": $.trim($(tr).children("td:eq(0)").text()),
                    "ProgramPercent": $.trim($(tr).children("td:eq(1)").find("input").val()),
                    "ProgramAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "WorkDrawPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "WorkDrawAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "LateStagePercent": $.trim($(tr).children("td:eq(5)").find("input").val()),
                    "LateStageAmount": $.trim($(tr).children("td:eq(6)").text())
                };
            });

            //方案设计工序
            $.each($("#gvdesignProcessOne tr"), function (index, tr) {
                finalDataObj.DesignProcess[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
            });

            //施工图工序
            $.each($("#gvdesignProcessThree tr"), function (index, tr) {
                finalDataObj.DesignProcessThree[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
            });
            //后期服务
            $.each($("#gvdesignProcessFour tr"), function (index, tr) {
                finalDataObj.DesignProcessFour[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                    "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                    "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                    "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                    "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                    "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                    "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                    "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                };
            });

            //专业
            var arraySpecialty = new Array();
            $("#stagespetable table:eq(1)  td").not(":last").not(":first").each(function () {
                arraySpecialty.push($(this).text());
            });

            //方案设计
            var arrayPreliminary = new Array();

            $("#gvProjectStageSpe tr:eq(0) td:even").not(":first").not(":last").each(function () {
                arrayPreliminary.push($(this).text());
            });
            var arrayPreliminaryPercent = ArrayProgramPercent();


            //施工图设计
            var arrayWorkDraw = new Array();
            $("#gvProjectStageSpe tr:eq(1) td:even").not(":first").not(":last").each(function () {
                arrayWorkDraw.push($(this).text());
            });
            var arrayWorkDrawPercent = ArrayPreliminaryPercent();

            //后期服务
            var arrayLateStage = new Array();
            $("#gvProjectStageSpe tr:eq(2) td:even").not(":first").not(":last").each(function () {
                arrayLateStage.push($(this).text());
            });
            var arrayLateStagePercent = ArrayWorkDrawPercent();


            for (var i = 0; i < arraySpecialty.length; i++) {
                finalDataObj.StageSpe[i] =
                {
                    "Specialty": arraySpecialty[i],
                    "ProgramPercent": arrayPreliminaryPercent[i],
                    "ProgramCount": arrayPreliminary[i],
                    "WorkDrawPercent": arrayWorkDrawPercent[i],
                    "WorkDrawCount": arrayWorkDraw[i],
                    "LateStagePercent": arrayLateStagePercent[i],
                    "LateStageCount": arrayLateStage[i]
                };

            } //其他情况 例如 室外工程
        }
        else {

            if (IsEdit == 1) {
                if (!ValidationFive()) {
                    return false
                }

            }

            if (!ValidationMem()) {
                return false;
            }

            $.each($("#gvdesignProcessFive tr"), function (index, tr) {
                finalDataObj.DesignProcessFive[index] =
                 {
                     "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                     "AuditPercent": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : 0,
                     "AuditAmount": $.trim($(tr).children("td:eq(2)").text()),
                     "SpecialtyHeadPercent": $(tr).children("td:eq(3)").find("input").length > 0 ? $.trim($(tr).children("td:eq(3)").find("input").val()) : 0,
                     "SpecialtyHeadAmount": $.trim($(tr).children("td:eq(4)").text()),
                     "ProofreadPercent": $(tr).children("td:eq(5)").find("input").length > 0 ? $.trim($(tr).children("td:eq(5)").find("input").val()) : 0,
                     "ProofreadAmount": $.trim($(tr).children("td:eq(6)").text()),
                     "DesignPercent": $(tr).children("td:eq(7)").find("input").length > 0 ? $.trim($(tr).children("td:eq(7)").find("input").val()) : 0,
                     "DesignAmount": $.trim($(tr).children("td:eq(8)").text())
                 };
            });

            //专业
            var arraySpecialty = new Array();
            $("#stagetabletfive table:eq(1)  td").not(":last").not(":first").each(function () {

                arraySpecialty.push($(this).text());
            });

            var arrayPreliminary = new Array();
            var arrayPreliminaryPercent = new Array();
            $("#gvFive  tr:eq(0) td:even").not(":first").not(":last").each(function () {
                arrayPreliminary.push($(this).text());
            });

            var arrayPreliminaryPercent = ArrayOtherPercent();


            //室外工程
            if (cprProcess == 4) {
                for (var i = 0; i < arraySpecialty.length; i++) {
                    finalDataObj.OutDoor[i] =
                {
                    "Status": 4,
                    "Specialty": arraySpecialty[i],
                    "SpecialtyPercent": arrayPreliminaryPercent[i],
                    "SpecialtyCount": arrayPreliminary[i]
                };

                }
            } else if (cprProcess == 5) {

                for (var i = 0; i < arraySpecialty.length; i++) {
                    finalDataObj.OutDoor[i] =
                {
                    "Status": 5,
                    "Specialty": arraySpecialty[i],
                    "SpecialtyPercent": arrayPreliminaryPercent[i],
                    "SpecialtyCount": arrayPreliminary[i]
                };

                }
            } else if (cprProcess == 6) {

                for (var i = 0; i < arraySpecialty.length; i++) {
                    finalDataObj.OutDoor[i] =
                {
                    "Status": 6,
                    "Specialty": arraySpecialty[i],
                    "SpecialtyPercent": arrayPreliminaryPercent[i],
                    "SpecialtyCount": arrayPreliminary[i]
                };

                }
            }
            else if (cprProcess == 7) {

                for (var i = 0; i < arraySpecialty.length; i++) {
                    finalDataObj.OutDoor[i] =
                {
                    "Status": 7,
                    "Specialty": arraySpecialty[i],
                    "SpecialtyPercent": arrayPreliminaryPercent[i],
                    "SpecialtyCount": arrayPreliminary[i]
                };

                }
            }
            else if (cprProcess == 8) {

                for (var i = 0; i < arraySpecialty.length; i++) {
                    finalDataObj.OutDoor[i] =
                {
                    "Status": 8,
                    "Specialty": arraySpecialty[i],
                    "SpecialtyPercent": arrayPreliminaryPercent[i],
                    "SpecialtyCount": arrayPreliminary[i]
                };

                }
            }
            else if (cprProcess == 9) {

                for (var i = 0; i < arraySpecialty.length; i++) {
                    finalDataObj.OutDoor[i] =
                {
                    "Status": 9,
                    "Specialty": arraySpecialty[i],
                    "SpecialtyPercent": arrayPreliminaryPercent[i],
                    "SpecialtyCount": arrayPreliminary[i]
                };

                }
            }
        }


        // 取得
        $.each($("table[memMoney='gvProjectValueBymember'] tr"), function (index, tr) {
            finalDataObj.ValueByMember[index] =
                {
                    "mem_ID": $.trim($(tr).children("td:eq(1)").text()),
                    "DesignPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "DesignCount": $.trim($(tr).children("td:eq(4)").text()),
                    "SpecialtyHeadPercent": $.trim($(tr).children("td:eq(5)").find("input").val()),
                    "SpecialtyHeadCount": $.trim($(tr).children("td:eq(6)").text()),
                    "AuditPercent": $.trim($(tr).children("td:eq(7)").find("input").val()),
                    "AuditCount": $.trim($(tr).children("td:eq(8)").text()),
                    "ProofreadPercent": $.trim($(tr).children("td:eq(9)").find("input").val()), //$(tr).children("td:eq(10)").find("input").length > 0 ? $.trim($(tr).children("td:eq(10)").find("input").val()) : 0,
                    "ProofreadCount": $.trim($(tr).children("td:eq(10)").text()),
                    "ItemType": $.trim($(tr).children("td:eq(11)").text()),
                    "IsHead": 0,
                    "IsExternal": $.trim($(tr).children("td:eq(1)").attr("wp"))
                };

        });
        //== "%" ? "0" : $.trim($(tr).children("td:eq(10)").find("input").val()),
        //        $("#btnApprovalTwo").attr("disabled", true);

        if (!confirm("是否保存信息?")) {
            return false;
        }

        $("#btnApprovalTwo").attr("disabled", true);
        var jsonObj = Global.toJSON(finalDataObj);
        jsonDataEntity = jsonObj;
        getUserAndUpdateAudit("4", '1', jsonDataEntity);

    });



    //    messageDialog = $("#msgReceiverContainer").messageDialog({
    //        "button": {
    //            "发送消息": function () {
    //                //选中用户
    //                var _$mesUser = $(":checkbox[name=messageUser]:checked");

    //                if (_$mesUser.length == 0) {
    //                    alert("请至少选择一个流程审批人！");
    //                    return false;
    //                }

    //                getUserAndUpdateAudit('4', '1', jsonDataEntity);
    //            },
    //            "关闭": function () {
    //                $("#btnApprovalTwo").attr("disabled", false);
    //                messageDialog.hide();
    //            }
    //        }
    //    });
    //    sendMessageClass = new MessageCommon(messageDialog); 

    $("#btn_Send").click(function () {


        //选中用户
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        }

        getUserAndUpdateAudit('4', '1', jsonDataEntity);
    });

});

//计算产值
function CalculationValue() {

    //分配金额
    var allotAccount = 0;

    allotAccount = $("#ctl00_ContentPlaceHolder1_lblAllotCount").text();

    //本部门自留
    var theDeptValuePercent = $("#txtTheDeptValuePercent").val().length == 0 ? 0 : parseFloat($("#txtTheDeptValuePercent").val());
    //本部门自留产值
    var theDeptValueCount = parseFloat(theDeptValuePercent) * parseFloat(allotAccount) / 100;
    $("#txtTheDeptValueCount").text(theDeptValueCount.toFixed(0));

    //设总比例
    var designManagerPercent = $("#txt_DesignManagerPercent").val().length == 0 ? 0 : parseFloat($("#txt_DesignManagerPercent").val());
    //设总金额(设)
    var DesignManagerCount = parseFloat(designManagerPercent) * parseFloat(allotAccount) / 100;
    $("#txt_DesignManagerCount").text(DesignManagerCount.toFixed(0));

    // 应分产值比例 
    //应分产值金额 等于分配产值 -转经济所-转其他部门-转暖通-设总-本部门自留产值-方案
    var shouleBeValueCount = parseFloat(allotAccount) - parseFloat(theDeptValueCount) - parseFloat(DesignManagerCount);
    $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text(shouleBeValueCount.toFixed(0));

    $("#txt_ShouldBeValuePercent").attr("value", (shouleBeValueCount / allotAccount * 100).toFixed(2));
}

//浮点数
function checkRate(value) {
    var re = /^[0-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+$/;
    if (re.test(value)) {
        return true;
    }
}

//验证工序
function ValidationProcess() {
    var flag = true;

    var budingPercent = $("#txtBulidging").val();
    if (budingPercent == "") {
        alert("建筑配置比例不能为空");
        flag = false;
        return false;

    }
    var structure = $("#txtStructure").val()
    if (structure == "") {
        alert("结构配置比例不能为空");
        flag = false;
        return false;

    }
    var drain = $("#txtDrain").val();
    if (drain == "") {
        alert("给排水配置比例不能为空");
        flag = false;
        return false;

    }
    var havc = $("#txtHavc").val();
    if (havc == "") {
        alert("暖通配置比例不能为空");
        flag = false;
        return false;

    }
    var electric = $("#txtElectric").val();
    if (electric == "") {
        alert("电气配置比例不能为空");
        flag = false;
        return false;

    }

    var total = parseFloat(budingPercent) + parseFloat(structure) + parseFloat(drain) + parseFloat(havc) + parseFloat(electric);

    if (total.toFixed(0) != 100) {
        alert("配置比例没有闭合，请修改");
        flag = false;
        return false;
    }

    return flag;
}

function Validation() {

    var flag = true;

    var stage = $("#ctl00_ContentPlaceHolder1_stage").val();

    if (stage == "23" || stage == "24" || stage == "25") {

        if (!ValidationProcess()) {
            flag = false;
            return false;
        }
    }

    var valuespeemputy = "0";
    $.each($("input:not(:disabled)", "#gvProjectValueBymember tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valuespeemputy = $(this).val();
            return false;
        }
    });

    if ($.trim(valuespeemputy).length == 0) {
        alert("人员产值分配比例不能为空");
        flag = false;
        return false;
    }

    var designcountOne = 0;
    var isCheckOne = false;
    $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {
        designcountOne += parseFloat($(this).val());
        isCheckOne = true;
    });

    if (designcountOne.toFixed(0) != 100 && isCheckOne == true) {
        alert("人员编制比例没有闭合，必须为100%,请修改");
        flag = false;
        return false;
    }

    var designcountTwo = 0;
    var isCheckTwo = false;
    $(":text[roles=jd]", "#gvProjectValueBymember tr").each(function () {
        designcountTwo += parseFloat($(this).val());
        isCheckTwo = true;
    });

    if (designcountTwo.toFixed(0) != 100 && isCheckTwo == true) {
        alert("人员校对比例没有闭合，必须为100%,请修改");
        flag = false;
        return false;
    }
    return flag;
}

//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectValueandAllot/TranProjectValueAllot.ashx";

    //数据         
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    $.post(url, data, function (jsonResult) {
        if (flag == "1") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
        }
        if (jsonResult == "0") {
            alert("发起分配错误，请联系管理员！");
        } else if (jsonResult == "4") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
            alert("所长分配完毕，产值已发送给个人等待确认!");
            window.location.href = "/ProjectValueandAllot/TranBulidingListBymaster.aspx";
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}

//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
        $("#AuditUserDiv").modal();
    }
    else {

        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);

    }
}

//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        $("#AuditUserDiv").modal('hide');
        //        messageDialog.hide();
        alert("产值分配消息发送成功！");
        //查询系统新消息

        window.location.href = "/ProjectValueandAllot/TranBulidingListBymaster.aspx";
    } else {
        alert("消息发送失败！");
    }
}


function ValidationOne() {

    var flag = true;

    var valueemputystagetwo = "0";
    $.each($("input:not(:disabled)", "#ctl00_ContentPlaceHolder1_gvOne tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valueemputystagetwo = $(this).val();
            return false;
        }
    });

    if ($.trim(valueemputystagetwo).length == 0) {
        alert("项目各设计阶段专业之间产值分配比例不能为空");
        flag = false;
        return false;
    }

    var taotalstagetwo = -1;
    $("#ctl00_ContentPlaceHolder1_gvOne tr").each(function () {
        var valueTotal = 0;
        $("input:not(:disabled)", this).each(function () {
            valueTotal += parseFloat($(this).val());
        });
        if (valueTotal != 100) {
            taotalstagetwo = valueTotal;
            return false;
        }
    });

    if (taotalstagetwo.toFixed(0) != 100 && taotalstagetwo != -1) {
        alert("项目各设计阶段产值分配比例比例没有闭合，必须为100%");
        flag = false;
        return false;
    }

    if (!Validation()) {
        flag = false;
        return false;
    }

    return flag;
}



//验证 方案——初设
function ValidationTwo() {

    var flag = true;
    var valueemputy = "0";
    $.each($("input:not(:disabled)", "#ctl00_ContentPlaceHolder1_gvTwo tr td"), function (index, tr) {
        if ($.trim($(this).val()).length == 0) {
            valueemputy = $(this).val();
            return false;
        }
    });

    if ($.trim(valueemputy).length == 0) {
        alert("项目(方案+初步设计)二阶段产值分配比例不能为空");
        flag = false;
        return false;
    }

    var ComValue = 0;
    $("input:not(:disabled)", "#ctl00_ContentPlaceHolder1_gvTwo").each(function () {
        ComValue += parseFloat($(this).val());
    });

    if (ComValue.toFixed(0) != 100) {
        alert("项目(方案+初步设计)二阶段产值分配比例没有闭合，必须为100%");
        flag = false;
        return false;
    }


    if (!Validation()) {
        flag = false;
        return false;
    }

    return flag;
}

function ValidationThree() {

    var flag = true;
    var valueemputy = "0";
    $.each($("input:not(:disabled)", "#ctl00_ContentPlaceHolder1_gvThree tr td"), function (index, tr) {
        if ($.trim($(this).val()).length == 0) {
            valueemputy = $(this).val();
            return false;
        }
    });

    if ($.trim(valueemputy).length == 0) {
        alert("项目(施工图设计+后期服务)二阶段产值分配比例不能为空");
        flag = false;
        return false;
    }

    var ComValue = 0;
    $("input:not(:disabled)", "#ctl00_ContentPlaceHolder1_gvThree").each(function () {
        ComValue += parseFloat($(this).val());
    });

    if (ComValue.toFixed(0) != 100) {
        alert("项目(施工图设计+后期服务)二阶段产值分配比例没有闭合，必须为100%");
        flag = false;
        return false;
    }

    if (!Validation()) {
        flag = false;
        return false;
    }

    return flag;

}

function ValidationFour() {
    var flag = true;
    var valueemputystagetwo = "0";
    $.each($("input:not(:disabled)", "#ctl00_ContentPlaceHolder1_gvFour tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valueemputystagetwo = $(this).val();
            return false;
        }
    });

    if ($.trim(valueemputystagetwo).length == 0) {
        alert("(初步设计+施工图+后期)三阶段分配比例不能为空");
        flag = false;
        return false;
    }

    var taotalstagetwo = -1;
    $("#ctl00_ContentPlaceHolder1_gvFour tr").each(function () {
        var valueTotal = 0;
        $("input:not(:disabled)", this).each(function () {
            valueTotal += parseFloat($(this).val());
        });
        if (valueTotal != 100) {
            taotalstagetwo = valueTotal;
            return false;
        }
    });

    if (taotalstagetwo != 100 && taotalstagetwo != -1) {
        alert("(初步设计+施工图+后期)三阶段分配比例没有闭合，必须为100%");
        flag = false;
        return false;
    }

    if (!Validation()) {
        flag = false;
        return false;
    }

    return flag;

}
function ValidationTen() {
    var flag = true;
    var valueemputystagetwo = "0";
    $.each($("input:not(:disabled)", "#ctl00_ContentPlaceHolder1_gvTen tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valueemputystagetwo = $(this).val();
            return false;
        }
    });

    if ($.trim(valueemputystagetwo).length == 0) {
        alert("(方案设计+施工图+后期)三阶段分配比例不能为空");
        flag = false;
        return false;
    }

    var taotalstagetwo = -1;
    $("#ctl00_ContentPlaceHolder1_gvTen tr").each(function () {
        var valueTotal = 0;
        $("input:not(:disabled)", this).each(function () {
            valueTotal += parseFloat($(this).val());
        });
        if (valueTotal != 100) {
            taotalstagetwo = valueTotal;
            return false;
        }
    });

    if (taotalstagetwo != 100 && taotalstagetwo != -1) {
        alert("(方案设计+施工图+后期)三阶段分配比例没有闭合，必须为100%");
        flag = false;
        return false;
    }

    if (!Validation()) {
        flag = false;
        return false;
    }

    return flag;

}
function Validation() {

    var flag = true;

    var valuespeemputy = "0";
    $.each($("input:not(:disabled)", "#gvProjectStageSpe tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valuespeemputy = $(this).val();
            return false;
        }
    });

    if ($.trim(valuespeemputy).length == 0) {
        alert("项目各设计阶段专业之间产值分配比例不能为空");
        flag = false;
        return false;
    }

    var taotalspe = -1;
    $("#gvProjectStageSpe tr").each(function () {
        var valueTotal = 0;
        $("input:not(:disabled)", this).each(function () {
            valueTotal += parseFloat($(this).val());
        });
        if (valueTotal != 100) {
            taotalspe = valueTotal;
            return false;
        }
    });

    if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
        alert("项目各设计阶段专业之间产值分配比例没有闭合，必须为100%");
        flag = false;
        return false;
    }

    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
        var valueemputyone = "0";
        $.each($("input:not(:disabled)", "#gvdesignProcessOne tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyone = $(this).val();
                return false;
            }
        });



        if ($.trim(valueemputyone).length == 0) {
            alert("方案设计工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        var taotalone = -1;
        $("#gvdesignProcessOne tr").each(function () {
            var valueTotal = 0;
            if ($(this).children().find("input").length > 0) {

                $("input:not(:disabled)", this).each(function () {
                    valueTotal += parseFloat($(this).val());
                });
                if (valueTotal != 100) {
                    taotalone = valueTotal;
                    return false;
                }
            }
        });


        if (taotalone.toFixed(0) != 100 && taotalone != -1) {
            alert("方案设计工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }
    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 3) {
        var valueemputytwo = "0";
        $.each($("input:not(:disabled)", "#gvdesignProcessTwo tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputytwo = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputytwo).length == 0) {
            alert("初步设计工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        var taotaltwo = -1;
        $("#gvdesignProcessTwo tr").each(function () {
            if ($(this).children().find("input").length > 0) {
                var valueTotal = 0;
                $("input:not(:disabled)", this).each(function () {
                    valueTotal += parseFloat($(this).val());
                });
                if (valueTotal != 100) {
                    taotaltwo = valueTotal;
                    return false;
                }
            }
        });

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("初步设计工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }

    if (cprProcess == 0 || cprProcess == 2 || cprProcess == 3 || cprProcess == 10) {
        var valueemputythree = "0";
        $.each($("input:not(:disabled)", "#gvdesignProcessThree tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputythree = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputythree).length == 0) {
            alert("施工图工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        var taotalthree = -1;
        $("#gvdesignProcessThree tr").each(function () {
            if ($(this).children().find("input").length > 0) {
                var valueTotal = 0;
                $("input:not(:disabled)", this).each(function () {
                    valueTotal += parseFloat($(this).val());
                });
                if (valueTotal != 100) {
                    taotalthree = valueTotal;
                    return false;
                }
            }
        });

        if (taotalthree.toFixed(0) != 100 && taotalthree != -1) {
            alert("施工图工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }

    if (cprProcess == 0 || cprProcess == 2 || cprProcess == 3 || cprProcess == 10) {
        var valueemputyfour = "0";
        $.each($("input:not(:disabled)", "#gvdesignProcessFour tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyfour = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyfour).length == 0) {
            alert("后期服务工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        var taotalfour = -1;
        $("#gvdesignProcessFour tr").each(function () {
            if ($(this).children().find("input").length > 0) {
                var valueTotal = 0;
                $("input:not(:disabled)", this).each(function () {
                    valueTotal += parseFloat($(this).val());
                });
                if (valueTotal != 100) {
                    taotalfour = valueTotal;
                    return false;
                }
            }
        });

        if (taotalfour.toFixed(0) != 100 && taotalfour != -1) {
            alert("后期服务工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }
    return flag;
}

// 验证 
function ValidationFive() {

    var flag = true;
    var valuespeemputy = "0";
    $.each($("input:not(:disabled)", "#gvFive tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valuespeemputy = $(this).val();
            return false;
        }
    });

    var taotalspe = -1;
    $("#gvFive tr").each(function () {
        var valueTotal = 0;
        $("input:not(:disabled)", this).each(function () {
            valueTotal += parseFloat($(this).val());
        });
        if (valueTotal != 100) {
            taotalspe = valueTotal;
            return false;
        }
    });


    var valueemputytwo = "0";
    $.each($("input:not(:disabled)", "#gvdesignProcessFive tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valueemputytwo = $(this).val();
            return false;
        }
    });


    var taotaltwo = -1;
    $("#gvdesignProcessFive tr").each(function () {
        if ($(this).children().find("input").length > 0) {
            var valueTotal = 0;
            $("input:not(:disabled)", this).each(function () {
                valueTotal += parseFloat($(this).val());
            });
            if (valueTotal != 100) {

                taotaltwo = valueTotal;
                return false;
            }
        }
    });

    if (cprProcess == 4) {
        if ($.trim(valuespeemputy).length == 0) {
            alert("室外工程产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
            alert("室外工程产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

        if ($.trim(valueemputytwo).length == 0) {
            alert("室外工程工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("室外工程工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

    } else if (cprProcess == 5) {

        if ($.trim(valuespeemputy).length == 0) {
            alert("锅炉房产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
            alert("锅炉房产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

        if ($.trim(valueemputytwo).length == 0) {
            alert("锅炉房工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("锅炉房工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }
    else if (cprProcess == 6) {

        if ($.trim(valuespeemputy).length == 0) {
            alert("地上单建水泵房产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
            alert("地上单建水泵房产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

        if ($.trim(valueemputytwo).length == 0) {
            alert("地上单建水泵房工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("地上单建水泵房工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }
    else if (cprProcess == 7) {

        if ($.trim(valuespeemputy).length == 0) {
            alert("地上单建变配所(室)产值分配比例不能为空");
            flag = false;
            return false;
        }

        if ($.trim(valuespeemputy).length == 0) {
            alert("地上单建变配所(室)产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
            alert("地上单建变配所(室)产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

        if ($.trim(valueemputytwo).length == 0) {
            alert("地上单建变配所(室)工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("地上单建变配所(室)工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }
    else if (cprProcess == 8) {

        if ($.trim(valuespeemputy).length == 0) {
            alert(" 单建地下室（车库）产值分配比例不能为空");
            flag = false;
            return false;
        }

        if ($.trim(valuespeemputy).length == 0) {
            alert("单建地下室（车库）产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
            alert("单建地下室（车库）产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

        if ($.trim(valueemputytwo).length == 0) {
            alert("单建地下室（车库）工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("单建地下室（车库）工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }
    else if (cprProcess == 9) {

        if ($.trim(valuespeemputy).length == 0) {
            alert("市政道路工程产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotalspe.toFixed(0) != 100 && taotalspe != -1) {
            alert("市政道路工程产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }

        if ($.trim(valueemputytwo).length == 0) {
            alert("市政道路工程工序产值分配比例不能为空");
            flag = false;
            return false;
        }

        if (taotaltwo.toFixed(0) != 100 && taotaltwo != -1) {
            alert("市政道路工程工序产值分配比例没有闭合，必须为100%");
            flag = false;
            return false;
        }
    }

    return flag;
}

//验证人员
function ValidationMem() {

    var flag = true;

    var cprProcess = $("#HiddenCoperationProcess").val();

    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 10) {
        var valueemputy = "0";
        $.each($("input", "#gvProjectValueOneBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputy = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputy).length == 0) {
            alert("方案设计--项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var spe = new Array();
        $("#gvProjectValueOneBymember tr").each(function () {
            $("td[class=cls_Column]", this).each(function () {
                spe.push($(this).text());
            });
        });

        var prooFlag = true;
        var arraySpe = spe.reverse().join(",").match(/([^,]+)(?!.*\1)/ig).reverse();

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueOneBymember tr td:nth-child(4)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("方案设计：" + val + "专业,设计比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueOneBymember tr td:nth-child(6)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("方案设计：" + val + "专业,专业负责比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueOneBymember tr td:nth-child(8)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("方案设计：" + val + "专业,审核比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueOneBymember tr td:nth-child(10)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("方案设计：" + val + "专业,校对比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }
    }

    if (cprProcess == 0 || cprProcess == 1 || cprProcess == 3) {
        var valueemputyTwo = "0";
        $.each($("input", "#gvProjectValueTwoBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyTwo = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyTwo).length == 0) {
            alert("初步设计--项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }


        var spe = new Array();
        $("#gvProjectValueTwoBymember tr").each(function () {
            $("td[class=cls_Column]", this).each(function () {
                spe.push($(this).text());
            });
        });

        var prooFlag = true;
        var arraySpe = spe.reverse().join(",").match(/([^,]+)(?!.*\1)/ig).reverse();

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueTwoBymember tr td:nth-child(4)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("初步设计：" + val + "专业,设计比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueTwoBymember tr td:nth-child(6)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("初步设计：" + val + "专业,专业负责比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueTwoBymember tr td:nth-child(8)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("初步设计：" + val + "专业,审核比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueTwoBymember tr td:nth-child(10)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("初步设计：" + val + "专业,校对比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }
    }

    if (cprProcess == 0 || cprProcess == 2 || cprProcess == 3 || cprProcess == 10) {
        var valueemputyThree = "0";
        $.each($("input", "#gvProjectValueThreeBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyThree = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyThree).length == 0) {
            alert("施工图设计--项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var spe = new Array();
        $("#gvProjectValueThreeBymember tr").each(function () {
            $("td[class=cls_Column]", this).each(function () {
                spe.push($(this).text());
            });
        });

        var prooFlag = true;
        var arraySpe = spe.reverse().join(",").match(/([^,]+)(?!.*\1)/ig).reverse();

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueThreeBymember tr td:nth-child(4)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("施工图设计：" + val + "专业,设计比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueThreeBymember tr td:nth-child(6)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("施工图设计：" + val + "专业,专业负责比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueThreeBymember tr td:nth-child(8)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("施工图设计：" + val + "专业,审核比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueThreeBymember tr td:nth-child(10)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("施工图设计：" + val + "专业,校对比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }
    }

    if (cprProcess == 0 || cprProcess == 2 || cprProcess == 3 || cprProcess == 10) {
        var valueemputyFour = "0";
        $.each($("input", "#gvProjectValueFourBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyFour = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyFour).length == 0) {
            alert("后期服务--项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var spe = new Array();
        $("#gvProjectValueFourBymember tr").each(function () {
            $("td[class=cls_Column]", this).each(function () {
                spe.push($(this).text());
            });
        });

        var prooFlag = true;
        var arraySpe = spe.reverse().join(",").match(/([^,]+)(?!.*\1)/ig).reverse();

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueFourBymember tr td:nth-child(4)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("后期服务：" + val + "专业,设计比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueFourBymember tr td:nth-child(6)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("后期服务：" + val + "专业,专业负责比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueFourBymember tr td:nth-child(8)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("后期服务：" + val + "专业,审核比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueThreeBymember tr td:nth-child(10)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("后期服务：" + val + "专业,校对比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }
    }

    if (cprProcess > 3 &&   cprProcess != 10) {

        var valueemputyFour = "0";
        $.each($("input", "#gvProjectValueBymember tr td"), function () {
            if ($.trim($(this).val()).length == 0) {
                valueemputyFour = $(this).val();
                return false;
            }
        });

        if ($.trim(valueemputyFour).length == 0) {
            alert("项目策划人员产值分配比例不能为空");
            flag = false;
            return false;
        }

        var spe = new Array();
        $("#gvProjectValueBymember tr").each(function () {
            $("td[class=cls_Column]", this).each(function () {
                spe.push($(this).text());
            });
        });

        var prooFlag = true;
        var arraySpe = spe.reverse().join(",").match(/([^,]+)(?!.*\1)/ig).reverse();

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueBymember tr td:nth-child(4)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("" + val + "专业,设计比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueBymember tr td:nth-child(6)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("" + val + "专业,专业负责比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueBymember tr td:nth-child(8)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("" + val + "专业,审核比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }

        $.each(arraySpe, function (key, val) {
            var valueSjTotal = 0;
            var isCheck = false;
            $(":text[spe='" + $.trim(val) + "']", "#gvProjectValueBymember tr td:nth-child(10)").each(function () {
                valueSjTotal += parseFloat($(this).val());
                isCheck = true;
            });

            if (valueSjTotal.toFixed(0) != 100 && isCheck) {
                alert("" + val + "专业,校对比例没有闭合!,请修改");
                prooFlag = false;
                return false;
            }

        });

        if (!prooFlag) {
            flag = false;
            return false;
        }
    }
    return flag;
}

//方案比例
function ArrayProgramPercent() {

    var arrayProgramPercent = new Array();


    if ($("#gvProjectStageSpe tr:eq(0) td:eq(1)").find("input").length > 0) {
        arrayProgramPercent.push($("input", "#gvProjectStageSpe tr:eq(0) td:eq(1)").val());
    }
    else {
        arrayProgramPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(0) td:eq(3)").find("input").length > 0) {
        arrayProgramPercent.push($("input", "#gvProjectStageSpe tr:eq(0) td:eq(3)").val());
    }
    else {
        arrayProgramPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(0) td:eq(5)").find("input").length > 0) {
        arrayProgramPercent.push($("input", "#gvProjectStageSpe tr:eq(0) td:eq(5)").val());
    }
    else {
        arrayProgramPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(0) td:eq(7)").find("input").length > 0) {
        arrayProgramPercent.push($("input", "#gvProjectStageSpe tr:eq(0) td:eq(7)").val());
    }
    else {
        arrayProgramPercent.push(0);
    }

    return arrayProgramPercent;
}


//初步比例

function ArrayPreliminaryPercent() {

    var arrayPreliminaryPercent = new Array();


    if ($("#gvProjectStageSpe tr:eq(1) td:eq(1)").find("input").length > 0) {
        arrayPreliminaryPercent.push($("input", "#gvProjectStageSpe tr:eq(1) td:eq(1)").val());
    }
    else {
        arrayPreliminaryPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(1) td:eq(3)").find("input").length > 0) {
        arrayPreliminaryPercent.push($("input", "#gvProjectStageSpe tr:eq(1) td:eq(3)").val());
    }
    else {
        arrayPreliminaryPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(1) td:eq(5)").find("input").length > 0) {
        arrayPreliminaryPercent.push($("input", "#gvProjectStageSpe tr:eq(1) td:eq(5)").val());
    }
    else {
        arrayPreliminaryPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(1) td:eq(7)").find("input").length > 0) {
        arrayPreliminaryPercent.push($("input", "#gvProjectStageSpe tr:eq(1) td:eq(7)").val());
    }
    else {
        arrayPreliminaryPercent.push(0);
    }

    return arrayPreliminaryPercent;

}

//工图比例

function ArrayWorkDrawPercent() {

    var arrayWorkDrawPercent = new Array();


    if ($("#gvProjectStageSpe tr:eq(2) td:eq(1)").find("input").length > 0) {
        arrayWorkDrawPercent.push($("input", "#gvProjectStageSpe tr:eq(2) td:eq(1)").val());
    }
    else {
        arrayWorkDrawPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(2) td:eq(3)").find("input").length > 0) {
        arrayWorkDrawPercent.push($("input", "#gvProjectStageSpe tr:eq(2) td:eq(3)").val());
    }
    else {
        arrayWorkDrawPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(2) td:eq(5)").find("input").length > 0) {
        arrayWorkDrawPercent.push($("input", "#gvProjectStageSpe tr:eq(2) td:eq(5)").val());
    }
    else {
        arrayWorkDrawPercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(2) td:eq(7)").find("input").length > 0) {
        arrayWorkDrawPercent.push($("input", "#gvProjectStageSpe tr:eq(2) td:eq(7)").val());
    }
    else {
        arrayWorkDrawPercent.push(0);
    }

    return arrayWorkDrawPercent;
}

//后期
function ArrayLateStagePercent() {


    var arrayLateStagePercent = new Array();


    if ($("#gvProjectStageSpe tr:eq(3) td:eq(1)").find("input").length > 0) {
        arrayLateStagePercent.push($("input", "#gvProjectStageSpe tr:eq(3) td:eq(1)").val());
    }
    else {
        arrayLateStagePercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(3) td:eq(3)").find("input").length > 0) {
        arrayLateStagePercent.push($("input", "#gvProjectStageSpe tr:eq(3) td:eq(3)").val());
    }
    else {
        arrayLateStagePercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(3) td:eq(5)").find("input").length > 0) {
        arrayLateStagePercent.push($("input", "#gvProjectStageSpe tr:eq(3) td:eq(5)").val());
    }
    else {
        arrayLateStagePercent.push(0);
    }

    if ($("#gvProjectStageSpe tr:eq(3) td:eq(7)").find("input").length > 0) {
        arrayLateStagePercent.push($("input", "#gvProjectStageSpe tr:eq(3) td:eq(7)").val());
    }
    else {
        arrayLateStagePercent.push(0);
    }

    return arrayLateStagePercent;
}

//其他
function ArrayOtherPercent() {


    var arrayOtherPercent = new Array();


    if ($("#gvFive tr:eq(0) td:eq(1)").find("input").length > 0) {
        arrayOtherPercent.push($("input", "#gvFive tr:eq(0) td:eq(1)").val());
    }
    else {
        arrayOtherPercent.push(0);
    }

    if ($("#gvFive tr:eq(0) td:eq(3)").find("input").length > 0) {
        arrayOtherPercent.push($("input", "#gvFive tr:eq(0) td:eq(3)").val());
    }
    else {
        arrayOtherPercent.push(0);
    }

    if ($("#gvFive tr:eq(0) td:eq(5)").find("input").length > 0) {
        arrayOtherPercent.push($("input", "#gvFive tr:eq(0) td:eq(5)").val());
    }
    else {
        arrayOtherPercent.push(0);
    }

    if ($("#gvFive tr:eq(0) td:eq(7)").find("input").length > 0) {
        arrayOtherPercent.push($("input", "#gvFive tr:eq(0) td:eq(7)").val());
    }
    else {
        arrayOtherPercent.push(0);
    }

    return arrayOtherPercent;
}


//设置分配阶段
function SetProcess(process) {

    if (process == 0) {
        $("#stagetablezero").show();
        $("#stagetableone").hide();
        $("#designProcessOneTable").show();
        $("#designProcessTwoTable").show();
        $("#designProcessThreeTable").show();
        $("#designProcessFourTable").show();
        $("#stagespetable").show();
    }
    else if (process == 1) {

        $("#stagetablezero").hide();
        $("#stagetableone").show();
        $("#designProcessOneTable").show();
        $("#designProcessTwoTable").show();
        $("#designProcessThreeTable").hide();
        $("#designProcessFourTable").hide();
        $("#stagespetable").show();
    }
    else if (process == 2) {
        $("#stagetabletwo").show();
        $("#designProcessThreeTable").show();
        $("#designProcessFourTable").show();
        $("#stagespetable").show();
    }
    else if (process == 3) {
        $("#stagetablethree").show();
        $("#designProcessTwoTable").show();
        $("#designProcessThreeTable").show();
        $("#designProcessFourTable").show();
        $("#stagespetable").show();
    } else if (process == 10) {
        $("#stagetableten").show();
        $("#designProcessOneTable").show();
        $("#designProcessThreeTable").show();
        $("#designProcessFourTable").show();
        $("#stagespetable").show();
    } else {
        $("#stagetabletfive").show();
        $("#tbOutDoor").show();
    }
}