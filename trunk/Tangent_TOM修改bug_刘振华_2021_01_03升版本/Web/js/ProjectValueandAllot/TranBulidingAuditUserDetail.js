﻿
var cprProcess;
$(function () {

    //是否可以编辑
    IsEdit = $("#HiddenIsEdit").val();
    cprProcess = $("#HiddenCoperationProcess").val();

    var isRounding = $("#HiddenIsRounding").val();

    //人员金额计算--方案设计
    $(":text", "#gvProjectValueOneBymember tr").live('change', function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        var spe = $(this).attr("spe");
        var roles = $(this).attr("roles");

        var sjOneAmount = 0;
        if (spe == "建筑") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(0) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(0) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(0) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(0) td:eq(2)").text();
            }

        } else if (spe == "结构") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(1) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(1) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(1) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(1) td:eq(2)").text();
            }
        }
        else if (spe == "给排水") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(2) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(2) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(2) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(2) td:eq(2)").text();
            }
        }
        else if (spe == "电气") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(3) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(3) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(3) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessOne tr:eq(3) td:eq(2)").text();
            }
        }
        else {
            sjOneAmount = 0;
        }
        var inputvalue = $(this).val();
        var proresultvalue = parseFloat(inputvalue) * parseFloat(sjOneAmount) / 100;
        if (isRounding == "0") {
            $(this).parent().next().text(proresultvalue.toFixed(0));
        }
        else {
            $(this).parent().next().text(Math.floor(proresultvalue));
        }

    });

    //人员金额计算--初步设计
    $(":text", "#gvProjectValueTwoBymember tr").live('change', function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        var spe = $(this).attr("spe");
        var roles = $(this).attr("roles");

        var sjOneAmount = 0;
        if (spe == "建筑") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(0) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(0) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(0) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(0) td:eq(2)").text();
            }

        } else if (spe == "结构") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(1) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(1) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(1) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(1) td:eq(2)").text();
            }
        }
        else if (spe == "给排水") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(2) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(2) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(2) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(2) td:eq(2)").text();
            }
        }
        else if (spe == "电气") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(3) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(3) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(3) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessTwo tr:eq(3) td:eq(2)").text();
            }
        }
        else {
            sjOneAmount = 0;
        }
        var inputvalue = $(this).val();
        var proresultvalue = parseFloat(inputvalue) * parseFloat(sjOneAmount) / 100;
        if (isRounding == "0") {
            $(this).parent().next().text(proresultvalue.toFixed(0));
        }
        else {
            $(this).parent().next().text(Math.floor(proresultvalue));
        }

    });

    //人员金额计算--施工图设计

    $(":text", "#gvProjectValueThreeBymember tr").live('change', function () {


        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        var spe = $(this).attr("spe");
        var roles = $(this).attr("roles");

        var sjOneAmount = 0;
        if (spe == "建筑") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(0) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(0) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(0) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(0) td:eq(2)").text();
            }

        } else if (spe == "结构") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(1) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(1) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(1) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(1) td:eq(2)").text();
            }
        }
        else if (spe == "给排水") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(2) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(2) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(2) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(2) td:eq(2)").text();
            }
        }
        else if (spe == "电气") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(3) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(3) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(3) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessThree tr:eq(3) td:eq(2)").text();
            }
        }
        else {
            sjOneAmount = 0;
        }
        var inputvalue = $(this).val();
        var proresultvalue = parseFloat(inputvalue) * parseFloat(sjOneAmount) / 100;
        if (isRounding == "0") {
            $(this).parent().next().text(proresultvalue.toFixed(0));
        }
        else {
            $(this).parent().next().text(Math.floor(proresultvalue));
        }

    });

    //人员金额计算--后期服务

    $(":text", "#gvProjectValueFourBymember tr").live('change', function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        var spe = $(this).attr("spe");
        var roles = $(this).attr("roles");

        var sjOneAmount = 0;
        if (spe == "建筑") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(0) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(0) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(0) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(0) td:eq(2)").text();
            }

        } else if (spe == "结构") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(1) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(1) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(1) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(1) td:eq(2)").text();
            }
        }
        else if (spe == "给排水") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(2) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(2) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(2) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(2) td:eq(2)").text();
            }
        }
        else if (spe == "电气") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(3) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(3) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(3) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessFour tr:eq(3) td:eq(2)").text();
            }
        }
        else {
            sjOneAmount = 0;
        }
        var inputvalue = $(this).val();
        var proresultvalue = parseFloat(inputvalue) * parseFloat(sjOneAmount) / 100;
        if (isRounding == "0") {
            $(this).parent().next().text(proresultvalue.toFixed(0));
        }
        else {
            $(this).parent().next().text(Math.floor(proresultvalue));
        }

    });


    //人员金额计算--室外工程

    $(":text", "#gvProjectValueBymember tr").live('change', function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        var spe = $(this).attr("spe");
        var roles = $(this).attr("roles");

        var sjOneAmount = 0;
        if (spe == "建筑") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(0) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(0) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(0) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(0) td:eq(2)").text();
            }

        } else if (spe == "结构") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(1) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(1) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(1) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(1) td:eq(2)").text();
            }
        }
        else if (spe == "给排水") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(2) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(2) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(2) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(2) td:eq(2)").text();
            }
        }
        else if (spe == "电气") {
            if (roles == "sj") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(3) td:eq(8)").text();
            } else if (roles == "fz") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(3) td:eq(4)").text();
            } else if (roles == "jd") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(3) td:eq(6)").text();
            } else if (roles == "sh") {
                sjOneAmount = $("#gvdesignProcessFive tr:eq(3) td:eq(2)").text();
            }
        }
        else {
            sjOneAmount = 0;
        }
        var inputvalue = $(this).val();
        var proresultvalue = parseFloat(inputvalue) * parseFloat(sjOneAmount) / 100;
        if (isRounding == "0") {
            $(this).parent().next().text(proresultvalue.toFixed(0));
        }
        else {
            $(this).parent().next().text(Math.floor(proresultvalue));
        }

    });

    //添加人员
    var chooseProjectValueUserMain = new ChooseProjectValueUserControl($("#chooseProjectValueUserMain"));


    //删除用户A标签点击事件
    $("#resultUserRemoveA").live("click", function () {
        var spe = $(this).attr("spe");
        var rowspan = 0;
        $($(this).parent().parent().parent().parent().children().find("td:eq(0)")).each(function () {
            if ($(this).text() == spe) {
                rowspan = $(this).attr("rowspan");
                $(this).attr("rowspan", parseInt(rowspan) - 1);
                return false;
            }
        });
        if (rowspan != 0) {
            $(this).parent().parent().parent().remove();
        }

    });

    //删除用户A标签点击事件
    $("#resultUserRemoveB").live("click", function () {

        var rowspan = $(this).parent().parent().prev().prev().attr("rowspan");
     
        var classDisplay = $(this).parent().parent().parent().children(":eq(0)").attr("class");
        if (rowspan != undefined && classDisplay != 'display') {
            //把这一行删掉把下一行的td换成文本框
            if (rowspan == 1) {
                $(this).parent().parent().parent().remove();
            }
            else {
                var rowspanText = $(this).parent().parent().prev().prev().text();
                $(this).parent().parent().parent().next().children(":eq(0)").attr("width", "10%");
                $(this).parent().parent().parent().next().children(":eq(0)").attr("class", "cls_Column");
                $(this).parent().parent().parent().next().children(":eq(0)").attr("rowspan", rowspan - 1);
                $(this).parent().parent().parent().next().children(":eq(0)").text(rowspanText);

                $(this).parent().parent().parent().remove();
            }
        } else {

            var speName = $(this).attr("spe");

            $("tr", $(this).parent().parent().parent().parent()).each(function () {
                $("td[class=cls_Column]", this).each(function () {
                    if (speName == $(this).text()) {
                        var tempRow1 = $(this).attr("rowspan");
                        $(this).attr("rowspan", (tempRow1 - 1));
                    }
                });
            });

            $(this).parent().parent().parent().remove();
        }

    });

    //选择用户--方案设计
    $("#chooseOneUser").click(function () {
        $("#chooseUserMainDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseUserMainDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseUserMainDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = "";
        parObj.SpeName = "";
        chooseProjectValueUserMain.Clear();
        chooseProjectValueUserMain.BindData(parObj);
        //        $("#chooseProjectValueUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "添加用户",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			             
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //			    return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btn_UserMain1\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseUserMainDiv").append(trString);
        $("#btn_UserMain1").click(function () {

            chooseProjectValueUserMain.SaveUser(ChooseOneUserOfTheDepartmentCallBack);
        })
    });

    //添加外聘人员
    var chooseExternalUser = new ChooseExternalUserControl($("#chooseExternalUserDiv"));
    //选择用户
    $("#chooseExternalOneUser").click(function () {
        $("#chooseExtUserDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseExtUserDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseExtUserDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = $.trim($("#HiddenUnitName").val());
        parObj.SpeName = "";
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        //        $("#chooseExternalUserDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "外聘人员",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseExternalUser.SaveUser(ChooseOneUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //			    return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btnChooseExt1\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseExtUserDiv").append(trString);
        $("#btnChooseExt1").click(function () {
            chooseExternalUser.SaveUser(ChooseOneUserOfTheDepartmentCallBack);
        })
    });

    //选择用户
    function ChooseOneUserOfTheDepartmentCallBack(userArray, isWp) {
        //设置行数
        var trLength = 0;
        var IsAdd = false;

        $.each(userArray, function (index, item) {
            $("#gvProjectValueOneBymember tr").each(function () {
                $("td[class=cls_Column]", this).each(function () {
                    if (item.userSpecialtyname == $(this).text()) {
                        index = parseInt($(this).parent().index());
                        lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        IsAdd = true;
                    }
                });
            });
        });
        if (IsAdd) {

            var index = 0; //此行的索引
            var lengthTemp = 0; //本专业之前的rowspan
            var _tempTr = 0; //是否第一次添加
            var _userLength = userArray.length;
            $.each(userArray, function (index, item) {

                $("#gvProjectValueOneBymember tr").each(function () {
                    $("td[class=cls_Column]", this).each(function () {
                        if (item.userSpecialtyname == $(this).text()) {
                            index = parseInt($(this).parent().index());
                            lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        }
                    });
                });

                var IsExisted = true;

                //循环该人员是否存在
                $("#gvProjectValueOneBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;
                    }
                });

                //不存在添加
                if (IsExisted) {
                    //得到这个专业rowspanID
                    if (_tempTr == 0) {

                        $.each(userArray, function (index, items) {
                            $("#gvProjectValueOneBymember tr").each(function () {
                                var userSysNO = $(this).children(":eq(1)").text();
                                if (userSysNO == items.userSysNo) {
                                    _userLength = _userLength - 1;
                                }
                            });
                        });

                        $("#gvProjectValueOneBymember  tr:eq(" + index + ") td:eq(0)").attr("rowspan", parseInt(lengthTemp) + parseInt(_userLength));
                    }
                    var trString = "";
                    trString = "<tr><td class=\"display\"></td>";
                    trString += "<td class=\"display\" wp=" + isWp + " >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    if (item.checkDesign == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"sj\" spe=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkHead == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"fz\" />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkAudit == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\" spe=" + item.userSpecialtyname + " roles=\"sh\" />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkProof == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"jd\"  />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }
                    trString += "<td class=\"display\">0</td>";
                    trString += "</tr>";

                    $(trString).insertAfter("#gvProjectValueOneBymember tr:eq(" + index + ")");
                    _tempTr = _tempTr + 1;
                }
            });
        }
        else {
            $.each(userArray, function (index, item) {

                //获取原Table下已有的用户          
                var IsExisted = true;
                //遍历已有的用户信息数组，如果已经存在，不允许添加
                var _userLength = userArray.length;
                $("#gvProjectValueOneBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;
                        _userLength = _userLength - 1;
                    }
                });

                if (IsExisted) {

                    var trString = "";
                    if (trLength == 0) {
                        trString = "<tr><td  width= \"10%\" class=\"cls_Column\" rowspan=" + (parseInt(userArray.length)) + ">" + item.userSpecialtyname + "</td>";
                    }
                    else {
                        trString = "<tr><td class=\"display\" width= \"10%\"></td>";
                    }
                    trString += "<td class=\"display\"  wp=" + isWp + "  >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveB\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    if (item.checkDesign == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"sj\" spe=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkHead == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\" spe=" + item.userSpecialtyname + " roles=\"fz\"  />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkAudit == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"sh\" />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkProof == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\" spe=" + item.userSpecialtyname + "  roles=\"jd\" />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }
                    trString += "<td class=\"display\"> 0</td>";
                    trString += "</tr>";
                    trLength = trLength + 1;
                    $("#gvProjectValueOneBymember").append(trString);

                }
            });
        }
    }



    //选择用户--初步设计
    $("#chooseTwoUser").click(function () {
        $("#chooseUserMainDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseUserMainDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseUserMainDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = "";
        parObj.SpeName = "";
        chooseProjectValueUserMain.Clear();
        chooseProjectValueUserMain.BindData(parObj);
        //        $("#chooseProjectValueUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "添加用户",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			               
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btn_UserMain2\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseUserMainDiv").append(trString);
        $("#btn_UserMain2").click(function () {

            chooseProjectValueUserMain.SaveUser(ChooseTwoUserOfTheDepartmentCallBack);

        })
    });

    //选择用户
    $("#chooseTwoExternalUser").click(function () {
        $("#chooseExtUserDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseExtUserDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseExtUserDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = $.trim($("#HiddenUnitName").val());
        parObj.SpeName = "";
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        //        $("#chooseExternalUserDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "外聘人员",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			              
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;.
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btnChooseExt2\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseExtUserDiv").append(trString);
        $("#btnChooseExt2").click(function () {
            chooseExternalUser.SaveUser(ChooseTwoUserOfTheDepartmentCallBack);

        })
    });

    //选择用户
    function ChooseTwoUserOfTheDepartmentCallBack(userArray, isWp) {
        //设置行数
        var trLength = 0;
        var IsAdd = false;

        $.each(userArray, function (index, item) {
            $("#gvProjectValueTwoBymember tr").each(function () {
                $("td[class=cls_Column]", this).each(function () {
                    if (item.userSpecialtyname == $(this).text()) {
                        index = parseInt($(this).parent().index());
                        lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        IsAdd = true;
                    }
                });
            });
        });
        if (IsAdd) {

            var index = 0; //此行的索引
            var lengthTemp = 0; //本专业之前的rowspan
            var _tempTr = 0; //是否第一次添加
            var _userLength = userArray.length;
            $.each(userArray, function (index, item) {

                $("#gvProjectValueTwoBymember tr").each(function () {
                    $("td[class=cls_Column]", this).each(function () {
                        if (item.userSpecialtyname == $(this).text()) {
                            index = parseInt($(this).parent().index());
                            lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        }
                    });
                });

                var IsExisted = true;

                //循环该人员是否存在
                $("#gvProjectValueTwoBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (userSysNO == item.userSysNo) {
                        IsExisted = false;
                    }
                });

                //不存在添加
                if (IsExisted) {

                    //得到这个专业rowspanID
                    if (_tempTr == 0) {
                        $.each(userArray, function (index, items) {
                            $("#gvProjectValueTwoBymember tr").each(function () {
                                var userSysNO = $(this).children(":eq(1)").text();
                                if (userSysNO == items.userSysNo) {
                                    _userLength = _userLength - 1;
                                }
                            });
                        });
                        $("#gvProjectValueTwoBymember  tr:eq(" + index + ") td:eq(0)").attr("rowspan", parseInt(lengthTemp) + parseInt(_userLength));
                    }
                    var trString = "";
                    trString = "<tr><td class=\"display\"></td>";
                    trString += "<td class=\"display\" wp=" + isWp + " >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    if (item.checkDesign == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"sj\" spe=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkHead == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"fz\" />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkAudit == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\" spe=" + item.userSpecialtyname + " roles=\"sh\" />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkProof == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"jd\"  />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }
                    trString += "<td class=\"display\">1</td>";
                    trString += "</tr>";

                    $(trString).insertAfter("#gvProjectValueTwoBymember tr:eq(" + index + ")");
                    _tempTr = _tempTr + 1;
                }

            });

        }
        else {
            $.each(userArray, function (index, item) {

                //获取原Table下已有的用户          
                var IsExisted = true;
                //遍历已有的用户信息数组，如果已经存在，不允许添加

                $("#gvProjectValueTwoBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;

                    }
                });

                if (IsExisted) {

                    var trString = "";
                    if (trLength == 0) {
                        trString = "<tr><td  width= \"10%\" class=\"cls_Column\" rowspan=" + (parseInt(userArray.length)) + ">" + item.userSpecialtyname + "</td>";
                    }
                    else {
                        trString = "<tr><td class=\"display\" width= \"10%\"></td>";
                    }
                    trString += "<td class=\"display\"  wp=" + isWp + "  >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveB\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    if (item.checkDesign == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"sj\" spe=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkHead == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\" spe=" + item.userSpecialtyname + " roles=\"fz\"  />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkAudit == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"sh\" />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkProof == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\" spe=" + item.userSpecialtyname + "  roles=\"jd\" />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }
                    trString += "<td class=\"display\">1</td>";
                    trString += "</tr>";
                    trLength = trLength + 1;
                    $("#gvProjectValueTwoBymember").append(trString);

                }
            });
        }
    }


    //选择用户--施工图
    $("#chooseThreeUser").click(function () {
        $("#chooseUserMainDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseUserMainDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseUserMainDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = "";
        parObj.SpeName = "";
        chooseProjectValueUserMain.Clear();
        chooseProjectValueUserMain.BindData(parObj);
        //        $("#chooseProjectValueUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "添加用户",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btn_UserMain3\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseUserMainDiv").append(trString);
        $("#btn_UserMain3").click(function () {

            chooseProjectValueUserMain.SaveUser(ChooseThreeUserOfTheDepartmentCallBack);

        })
    });

    //选择用户
    $("#chooseThreeExternalUser").click(function () {
        $("#chooseExtUserDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseExtUserDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseExtUserDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = $.trim($("#HiddenUnitName").val());
        parObj.SpeName = "";
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        //        $("#chooseExternalUserDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "外聘人员",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件

        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btnChooseExt3\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseExtUserDiv").append(trString);
        $("#btnChooseExt3").click(function () {
            chooseExternalUser.SaveUser(ChooseThreeUserOfTheDepartmentCallBack);

        })
    });

    //选择用户
    function ChooseThreeUserOfTheDepartmentCallBack(userArray, isWp) {
        //设置行数
        var trLength = 0;
        var IsAdd = false;

        $.each(userArray, function (index, item) {
            $("#gvProjectValueThreeBymember tr").each(function () {
                $("td[class=cls_Column]", this).each(function () {
                    if (item.userSpecialtyname == $(this).text()) {
                        index = parseInt($(this).parent().index());
                        lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        IsAdd = true;
                    }
                });
            });
        });
        if (IsAdd) {

            var index = 0; //此行的索引
            var lengthTemp = 0; //本专业之前的rowspan
            var _tempTr = 0; //是否第一次添加
            var _userLength = userArray.length;
            $.each(userArray, function (index, item) {

                $("#gvProjectValueThreeBymember tr").each(function () {
                    $("td[class=cls_Column]", this).each(function () {
                        if (item.userSpecialtyname == $(this).text()) {
                            index = parseInt($(this).parent().index());
                            lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        }
                    });
                });

                var IsExisted = true;

                //循环该人员是否存在
                $("#gvProjectValueThreeBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;
                    }
                });

                //不存在添加
                if (IsExisted) {

                    //得到这个专业rowspanID
                    if (_tempTr == 0) {
                        $.each(userArray, function (index, items) {
                            $("#gvProjectValueThreeBymember tr").each(function () {
                                var userSysNO = $(this).children(":eq(1)").text();
                                if (userSysNO == items.userSysNo) {
                                    _userLength = _userLength - 1;
                                }
                            });
                        });
                        $("#gvProjectValueThreeBymember  tr:eq(" + index + ") td:eq(0)").attr("rowspan", parseInt(lengthTemp) + parseInt(_userLength));
                    }
                    var trString = "";
                    trString = "<tr><td class=\"display\"></td>";
                    trString += "<td class=\"display\" wp=" + isWp + " >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    if (item.checkDesign == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"sj\" spe=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkHead == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"fz\" />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkAudit == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\" spe=" + item.userSpecialtyname + " roles=\"sh\" />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkProof == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"jd\"  />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }
                    trString += "<td class=\"display\">2</td>";
                    trString += "</tr>";

                    $(trString).insertAfter("#gvProjectValueThreeBymember tr:eq(" + index + ")");
                    _tempTr = _tempTr + 1;
                }

            });

        }
        else {
            $.each(userArray, function (index, item) {

                //获取原Table下已有的用户          
                var IsExisted = true;
                //遍历已有的用户信息数组，如果已经存在，不允许添加

                $("#gvProjectValueThreeBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;

                    }
                });

                if (IsExisted) {

                    var trString = "";
                    if (trLength == 0) {
                        trString = "<tr><td  width= \"10%\" class=\"cls_Column\" rowspan=" + (parseInt(userArray.length)) + ">" + item.userSpecialtyname + "</td>";
                    }
                    else {
                        trString = "<tr><td class=\"display\" width= \"10%\"></td>";
                    }
                    trString += "<td class=\"display\"  wp=" + isWp + "  >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveB\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    if (item.checkDesign == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"sj\" spe=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkHead == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\" spe=" + item.userSpecialtyname + " roles=\"fz\"  />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkAudit == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"sh\" />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkProof == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\" spe=" + item.userSpecialtyname + "  roles=\"jd\" />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }
                    trString += "<td class=\"display\">2 </td>";
                    trString += "</tr>";
                    trLength = trLength + 1;
                    $("#gvProjectValueThreeBymember").append(trString);

                }
            });
        }
    }


    //选择用户--后期服务
    $("#chooseFourUser").click(function () {
        $("#chooseUserMainDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseUserMainDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseUserMainDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = "";
        parObj.SpeName = "";
        chooseProjectValueUserMain.Clear();
        chooseProjectValueUserMain.BindData(parObj);
        //        $("#chooseProjectValueUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "添加用户",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseProjectValueUserMain.SaveUser(ChooseFourUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btn_UserMain4\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseUserMainDiv").append(trString);
        $("#btn_UserMain4").click(function () {

            chooseProjectValueUserMain.SaveUser(ChooseFourUserOfTheDepartmentCallBack);

        })
    });

    //选择用户
    $("#chooseFourExternalUser").click(function () {
        $("#chooseExtUserDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseExtUserDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseExtUserDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = $.trim($("#HiddenUnitName").val());
        parObj.SpeName = "";
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        //        $("#chooseExternalUserDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "外聘人员",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseExternalUser.SaveUser(ChooseFourUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btnChooseExt4\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseExtUserDiv").append(trString);
        $("#btnChooseExt4").click(function () {
            chooseExternalUser.SaveUser(ChooseFourUserOfTheDepartmentCallBack);

        })
    });

    //选择用户
    function ChooseFourUserOfTheDepartmentCallBack(userArray, isWp) {
        //设置行数
        var trLength = 0;
        var IsAdd = false;

        $.each(userArray, function (index, item) {
            $("#gvProjectValueFourBymember tr").each(function () {
                $("td[class=cls_Column]", this).each(function () {
                    if (item.userSpecialtyname == $(this).text()) {
                        index = parseInt($(this).parent().index());
                        lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        IsAdd = true;
                    }
                });
            });
        });
        if (IsAdd) {

            var index = 0; //此行的索引
            var lengthTemp = 0; //本专业之前的rowspan
            var _tempTr = 0; //是否第一次添加
            var _userLength = userArray.length;
            $.each(userArray, function (index, item) {

                $("#gvProjectValueFourBymember tr").each(function () {
                    $("td[class=cls_Column]", this).each(function () {
                        if (item.userSpecialtyname == $(this).text()) {
                            index = parseInt($(this).parent().index());
                            lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        }
                    });
                });

                var IsExisted = true;

                //循环该人员是否存在
                $("#gvProjectValueFourBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;
                    }
                });

                //不存在添加
                if (IsExisted) {

                    //得到这个专业rowspanID
                    if (_tempTr == 0) {
                        $.each(userArray, function (index, items) {
                            $("#gvProjectValueFourBymember tr").each(function () {
                                var userSysNO = $(this).children(":eq(1)").text();
                                if (userSysNO == items.userSysNo) {
                                    _userLength = _userLength - 1;
                                }
                            });
                        });
                        $("#gvProjectValueFourBymember  tr:eq(" + index + ") td:eq(0)").attr("rowspan", parseInt(lengthTemp) + parseInt(_userLength));
                    }
                    var trString = "";
                    trString = "<tr><td class=\"display\"></td>";
                    trString += "<td class=\"display\" wp=" + isWp + " >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    if (item.checkDesign == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"sj\" spe=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkHead == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"fz\" />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkAudit == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\" spe=" + item.userSpecialtyname + " roles=\"sh\" />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkProof == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"jd\"  />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }
                    trString += "<td class=\"display\">3</td>";
                    trString += "</tr>";

                    $(trString).insertAfter("#gvProjectValueFourBymember tr:eq(" + index + ")");
                    _tempTr = _tempTr + 1;
                }

            });

        }
        else {
            $.each(userArray, function (index, item) {

                //获取原Table下已有的用户          
                var IsExisted = true;
                //遍历已有的用户信息数组，如果已经存在，不允许添加

                $("#gvProjectValueFourBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;

                    }
                });

                if (IsExisted) {

                    var trString = "";
                    if (trLength == 0) {
                        trString = "<tr><td  width= \"10%\" class=\"cls_Column\" rowspan=" + (parseInt(userArray.length)) + ">" + item.userSpecialtyname + "</td>";
                    }
                    else {
                        trString = "<tr><td class=\"display\" width= \"10%\"></td>";
                    }
                    trString += "<td class=\"display\"  wp=" + isWp + "  >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveB\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    if (item.checkDesign == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"sj\" spe=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkHead == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\" spe=" + item.userSpecialtyname + " roles=\"fz\"  />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkAudit == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"sh\" />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkProof == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\" spe=" + item.userSpecialtyname + "  roles=\"jd\" />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }
                    trString += "<td class=\"display\"> 3</td>";
                    trString += "</tr>";
                    trLength = trLength + 1;
                    $("#gvProjectValueFourBymember").append(trString);

                }
            });
        }
    }


    //选择用户
    $("#chooseUser").click(function () {
        $("#chooseUserMainDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseUserMainDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseUserMainDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = "";
        parObj.SpeName = "";
        chooseProjectValueUserMain.Clear();
        chooseProjectValueUserMain.BindData(parObj);
        //        $("#chooseProjectValueUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "添加用户",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseProjectValueUserMain.SaveUser(ChooseUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btn_UserMain\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseUserMainDiv").append(trString);
        $("#btn_UserMain").click(function () {

            chooseProjectValueUserMain.SaveUser(ChooseUserOfTheDepartmentCallBack);

        })
    });

    //选择用户
    $("#chooseExternalUser").click(function () {
        $("#chooseExtUserDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseExtUserDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseExtUserDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = $.trim($("#HiddenUnitName").val());
        parObj.SpeName = "";
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        //        $("#chooseExternalUserDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "外聘人员",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseExternalUser.SaveUser(ChooseUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btnChooseExt\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseExtUserDiv").append(trString);
        $("#btnChooseExt").click(function () {

            chooseExternalUser.SaveUser(ChooseUserOfTheDepartmentCallBack);

        })
    });
    //选择用户
    function ChooseUserOfTheDepartmentCallBack(userArray, isWp) {

        //设置行数
        var trLength = 0;
        var IsAdd = false;

        $.each(userArray, function (index, item) {
            $("#gvProjectValueBymember tr").each(function () {
                $("td[class=cls_Column]", this).each(function () {
                    if (item.userSpecialtyname == $(this).text()) {
                        index = parseInt($(this).parent().index());
                        lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        IsAdd = true;
                    }
                });
            });
        });
        if (IsAdd) {

            var index = 0; //此行的索引
            var lengthTemp = 0; //本专业之前的rowspan
            var _tempTr = 0; //是否第一次添加
            var _userLength = userArray.length;
            $.each(userArray, function (index, item) {

                $("#gvProjectValueBymember tr").each(function () {
                    $("td[class=cls_Column]", this).each(function () {
                        if (item.userSpecialtyname == $(this).text()) {
                            index = parseInt($(this).parent().index());
                            lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        }
                    });
                });

                var IsExisted = true;

                //循环该人员是否存在
                $("#gvProjectValueBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;
                    }
                });

                //不存在添加
                if (IsExisted) {

                    //得到这个专业rowspanID
                    if (_tempTr == 0) {
                        $.each(userArray, function (index, items) {
                            $("#gvProjectValueBymember tr").each(function () {
                                var userSysNO = $(this).children(":eq(1)").text();
                                if (userSysNO == items.userSysNo) {
                                    _userLength = _userLength - 1;
                                }
                            });
                        });
                        $("#gvProjectValueBymember  tr:eq(" + index + ") td:eq(0)").attr("rowspan", parseInt(lengthTemp) + parseInt(_userLength));
                    }
                    var trString = "";
                    trString = "<tr><td class=\"display\"></td>";
                    trString += "<td class=\"display\" wp=" + isWp + " >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    if (item.checkDesign == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"sj\" spe=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkHead == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"fz\" />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkAudit == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\" spe=" + item.userSpecialtyname + " roles=\"sh\" />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }

                    if (item.checkProof == "1") {
                        trString += "<td><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"jd\"  />%</td>";
                        trString += "<td></td>";
                    }
                    else {
                        trString += "<td>0</td>";
                        trString += "<td>0</td>";
                    }
                    trString += "<td class=\"display\">" + cprProcess + "</td>";
                    trString += "</tr>";

                    $(trString).insertAfter("#gvProjectValueBymember tr:eq(" + index + ")");
                    _tempTr = _tempTr + 1;
                }

            });

        }
        else {
            $.each(userArray, function (index, item) {

                //获取原Table下已有的用户          
                var IsExisted = true;
                //遍历已有的用户信息数组，如果已经存在，不允许添加

                $("#gvProjectValueBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;

                    }
                });

                if (IsExisted) {

                    var trString = "";
                    if (trLength == 0) {
                        trString = "<tr><td  width= \"10%\" class=\"cls_Column\" rowspan=" + (parseInt(userArray.length)) + ">" + item.userSpecialtyname + "</td>";
                    }
                    else {
                        trString = "<tr><td class=\"display\" width= \"10%\"></td>";
                    }
                    trString += "<td class=\"display\"  wp=" + isWp + "  >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveB\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    if (item.checkDesign == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\" id=\"txtChooseUser\"  roles=\"sj\" spe=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkHead == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"   id=\"txtChooseUser\" spe=" + item.userSpecialtyname + " roles=\"fz\"  />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkAudit == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\"  spe=" + item.userSpecialtyname + " roles=\"sh\" />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }

                    if (item.checkProof == "1") {
                        trString += "<td width= \"10%\"><input  maxlength=\"6\" type=\"text\"  id=\"txtChooseUser\" spe=" + item.userSpecialtyname + "  roles=\"jd\" />%</td>";
                        trString += "<td width= \"10%\"></td>";
                    }
                    else {
                        trString += "<td width= \"10%\">0</td>";
                        trString += "<td width= \"10%\">0</td>";
                    }
                    trString += "<td class=\"display\">" + cprProcess + " </td>";
                    trString += "</tr>";
                    trLength = trLength + 1;
                    $("#gvProjectValueBymember").append(trString);

                }
            });
        }
    }
});


//浮点数
function checkRate(value) {
    var re = /^[0-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+$/;
    if (re.test(value)) {
        return true;
    }
}


