﻿//项目设计费用分配类
function ProjectAllotDetail() {
    this.Dom = {};
    this.BackgroundInvoke = TG.Web.ProjectValueandAllot.ProjAllotDetailsTable;
    this.ProjectSysNo = $("#hid_proid").val();
    this.ProcessType = $("#HiddenProcessType").val();  //操作预览类型 0表示为新规、1表示为编辑、2表示为预览
    this.Status = $("#HiddenPartOneStatus").val();
    this.PartOneSysNo = $("#hid_costid").val();
    this.CoperationSysNo = $("#hid_cprid").val();
    this.ProjectName = $("#hid_proname").val();
    this.HasPower = $("#HiddenHasPower").val();
    this.PartOneRecordInUserSysNo = $("#HiddenPartOneRecordInUserSysNo").val();

    //第三部分保存按钮
    this.Dom.PartThreeSaveButton = $("#partThreeSaveButton");
    //初始
    var Instance = this;

    //第三部分情报取得
    this.GetRecordFromPartThree = function() {
        var dataObj = {};
        dataObj.PartOneSysNo = Instance.PartOneSysNo;

        //取得设计报酬情报
        var designPayString = "";
        $.each($("div[ids=partThreeDesignPayTextBox]"), function(index, item) {

            designPayString += item.innerText + ",";
        });
        designPayString = designPayString.substring(0, designPayString.length - 1);
        dataObj.DesignPay = designPayString;

        //取得ABCD费用
        var ABCDPayString = "";
        $.each($("span[ids=partThreeABCDEPayTextBox]"), function(index, item) {
            ABCDPayString += item.innerText + ",";
        });
        ABCDPayString = ABCDPayString.substring(0, ABCDPayString.length - 1);
        dataObj.ABCDPay = ABCDPayString;

        //取得各专业所占比例
        var specialtyPayPercentString = "";
        $.each($("span[ids=partThreeSpecialtyPayPercentTextBox]"), function(index, item) {
            specialtyPayPercentString += item.innerText + ",";
        });
        specialtyPayPercentString = specialtyPayPercentString.substring(0, specialtyPayPercentString.length - 1);
        dataObj.SpecialtyPayPercent = specialtyPayPercentString;

        //专业比例取得
        var designPercentString = "";
        $.each($("span[ids=partThreeDesignPercentTextBox]"), function(index, item) {
            designPercentString += item.innerText + ",";
        });
        designPercentString = designPercentString.substring(0, designPercentString.length - 1);
        dataObj.ProfessionalPercent = designPercentString;

        //取得设计人员数据
        var designArray = Instance.PickDataFromPartThreeContainer("B");
        dataObj.DesignUserPay = Global.toJSON(designArray);

        //取得审校人员情报
        var checkUserArray = Instance.PickDataFromPartThreeContainer("C");
        dataObj.CheckUserPay = Global.toJSON(checkUserArray);
        //取得专业注册人员情报
        var specialtyArray = Instance.PickDataFromPartThreeContainer("S");
        dataObj.SpeRegPay = Global.toJSON(specialtyArray);

        //取得审核人员情报
        var auditArray = Instance.PickDataFromPartThreeContainer("A");
        dataObj.AuditUserPay = Global.toJSON(auditArray);

        //审定人员
        var validationArray = Instance.PickDataFromPartThreeContainer("J");
        dataObj.JuageUserPay = Global.toJSON(validationArray);

        return dataObj;
    }

    //第四部分情报取得
    this.GetRecordFromPartFour = function() {
        var dataObj =
             {
                 "PartOneSysNo": Instance.PartOneSysNo,
                 "CourtyardKeep": $("#allcount_hy").text(),
                 "ProjectKeep": $("#allcount_pro").text(),
                 "DesignUserKeep": $("#allcount_user").text(),
                 "Total": $("#allcount_all").text(),
                 "Remark": $("#allsub").val(),
                 "Dean": $("#collegeuser").val(),
                 "Produce": $("#collegeuser2").val(),
                 "ProduceManager": $("#collegeuser3").val(),
                 "PM": $("#collegeuser4").val(),
                 "TabulationUser": $("#collegeuser5").val()
             };


        //取得统计信息
        var StatisticsString = "";
        $.each($("span[ids=partFourStatisticsSpan]"), function(index, item) {
            StatisticsString += $.trim(item.innerText) + ",";
        });
        StatisticsString = StatisticsString.substring(0, StatisticsString.length - 1);
        dataObj.Statistics = StatisticsString
        return dataObj;

    }

    this.Validation = function() {
        var flag = true;
        $(":text[rel='valide']").each(function(i) {
            if ($(this).val() == "") {
                flag = false;
                return false;
            }
        });
        return flag;
    }

    Instance.Dom.PartThreeSaveButton.click(function() {
        //if (Instance.Validation()) {
        //保存情报
        var result = Instance.BackgroundInvoke.SavePartThreeAndPartFourRecord(Global.toJSON(Instance.GetRecordFromPartThree()), Global.toJSON(Instance.GetRecordFromPartFour()), Instance.PartOneSysNo, Instance.CoperationSysNo, Instance.ProjectSysNo, Instance.ProjectName, Instance.PartOneRecordInUserSysNo);

        if (result.value != null && parseInt(result.value, 10) > 0) {
            $("#HiddenPartOneSysNo").val(result.value);
            alert("项目成员分配明细表数据保存成功！");
            $(this).attr("disabled", true);
        } else {
            alert("数据保存失败！");
        }
        //}
    });

    this.PickDataFromPartThreeContainer = function(roleFlag) {
        var percentOfSomething = $(":text[bindattribute=designperenct_" + roleFlag + "]");
        var array = Instance.CutUpTextBox(percentOfSomething, roleFlag);

        return array;
    }

    this.CutUpTextBox = function(textBoxArray, bindAttributeFlag) {
        if (bindAttributeFlag) {
            //得到所有专业
            var specialityListResult = Instance.BackgroundInvoke.GetAllSpeciality();

            var specialityList = Global.evalJSON(specialityListResult.value);

            var rsultObject = new Object();

            $.each(specialityList, function(index, item) {
                var parameterTextBoxArray = textBoxArray.filter("[containername=" + item + "]");
                rsultObject[item] = Instance.PickDataFromTextBox(parameterTextBoxArray, item, bindAttributeFlag);
            });

            return rsultObject;
        }
        else {
            alert("绑定标识不能为null");
            return false;
        }
    }

    this.PickDataFromTextBox = function(subItemTextBox, containerName, bindAttributeFlag) {
        var resultArray = new Array();
        $.each(subItemTextBox, function(index, item) {
            var money = $.trim($(":text[bindattribute=money_" + bindAttributeFlag + "][containername=" + containerName + "]:eq(" + index + ")").val());
            if (money.length == 0) {
                money = 0;
            }
            var prt = $.trim(item.value);
            if (prt.length == 0) {
                prt = 0;
            }
            var objData = {
                "DesignPercent": prt,
                "Money": money,
                "Member": $.trim($("span[bindattribute=employee_" + bindAttributeFlag + "][containername=" + containerName + "]:eq(" + index + ")").text()),
                "Signature": " "
            };
            resultArray[index] = objData;
        });
        return resultArray;
    }
}



