﻿$(document).ready(function () {

    CommonControl.SetFormWidth();
    //隔行变色
    $("#gvSecondValue tr:even").css({ background: "White" });

    //进度条
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("percent"));
        $(item).progressbar({
            value: percent
        });
    });

    ChangedBackgroundColor();

    //当前登录用户SysNo
    var userSysNo = $("#HiddenUserSysNo").val();


    //当前登录用户SysNo
    var userSysNo = $("#HiddenUserSysNo").val();
    //发起审核按钮
    $("span[id=InitiatedAudit]").live("click",function () {
        //获取合同SysNo
        var proSysNo = $(this).attr("prosysno");
        var objData =
		{
		    ProSysNo: proSysNo,
		    InUser: userSysNo,
		    SecondValue: "2"
		};
        var jsonData = Global.toJSON(objData);
        $.post("/HttpHandler/ProjectValueandAllot/ProjectSecondValueandAllotHandler.ashx", { "Action": 0, "data": jsonData }, function (jsonResult) {
            if (parseInt(jsonResult) > 0) {
                CommonControl.GetSysMsgCount("/ProjectValueandAllot/AddProjectSecondValueAllot.aspx?proid=" + proSysNo + "&ValueAllotAuditSysNo=" + jsonResult + "");
            } else {
                alert("此合同无需再次申请或者服务器错误！请联系系统管理员！");
            }
        });
    });

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_cprName"));
    autoComplete.GetAutoAJAX();
    //提示
    $(".cls_column").tipsy({ opacity: 1 });
});