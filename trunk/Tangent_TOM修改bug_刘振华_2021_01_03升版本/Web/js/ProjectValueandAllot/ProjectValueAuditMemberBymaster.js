﻿var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
$(function () {

    CommonControl.SetFormWidth();
    //Tab页
    $("#tabsMemAmount").tabs();


    //获取消息列表参数
    pageIndex = $("#pageIndex").val();
    MessageType = $("#MessageType").val();
    TypePost = $("#TypePost").val();
    MessageAction = $("#MessageAction").val();
    Aflag = $("#Aflag").val();
    MessageKeys = $("#MessageKeys").val();


    //实例化类容
    messageDialog = $("#auditShow").messageDialog;
    sendMessageClass = new MessageCommon(messageDialog);

    //设置状态
    setUnitStatus();

    //审核通过按钮
    $("#btnApproval").click(function () {

        var auditObj = {
            AllotID: $("#hid_AllotID").val(),
            mem_id: $("#hid_user").val(),
            Status: "S",
            ProID: $("#hid_proID").val(),
            SysNo: $("#HiddenAuditRecordSysNo").val()
        };

        if (!confirm("是否通过审核信息?")) {
            return false;
        }
        auditObj.AuditSuggsion = $.trim($("#ctl00_ContentPlaceHolder1_AuditSuggsion").val());

        $("#btnApproval").attr("disabled", true);
        var jsonObj = Global.toJSON(auditObj);
        jsonDataEntity = jsonObj;

        if ($("#hid_proType").val() == "jjs") {
            getjjsUserAndUpdateAudit("9", '0', jsonDataEntity);
        } else if ($("#hid_proType").val() == "tranjjs") {
            getjjsUserAndUpdateAudit("13", '0', jsonDataEntity);
        } else if ($("#hid_proType").val() == "trannts") {
            getntsUserAndUpdateAudit("7", '0', jsonDataEntity);
        } else if ($("#hid_proType").val() == "trantjs") {
            getntsUserAndUpdateAudit("11", '0', jsonDataEntity);
        }
        else {
            getUserAndUpdateAudit("8", '0', jsonDataEntity);
        }

    });

    //审核不通过按钮，具体流程业务，基本同上面的审核通过一致
    $("#btnRefuse").click(function () {

        var auditObj = {
            AllotID: $("#hid_AllotID").val(),
            mem_id: $("#hid_user").val(),
            Status: "D",
            ProID: $("#hid_proID").val(),
            SysNo: $("#HiddenAuditRecordSysNo").val()
        };


        if ($.trim($("#ctl00_ContentPlaceHolder1_AuditSuggsion").val()).length == 0) {
            alert("个人确认意见不能为空！");
            return false;
        }
        if (!confirm("是否不通过审核信息?")) {
            return false;
        }

        auditObj.AuditSuggsion = $.trim($("#ctl00_ContentPlaceHolder1_AuditSuggsion").val());

        var jsonObj = Global.toJSON(auditObj);

        var urlHadler = "/HttpHandler/ProjectValueandAllot/ProjectValueandAllotHandler.ashx";
        var action = 9;
        if ($("#hid_proType").val() == "jjs") {
            urlHadler = "/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx";
            action = 10;
        } else if ($("#hid_proType").val() == "tranjjs") {
            urlHadler = "/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx";
            action = 14;
        }
        else if ($("#hid_proType").val() == "trannts") {
            urlHadler = "/HttpHandler/ProjectValueandAllot/TranProjectValueAllot.ashx";
            action = 8;
        } else if ($("#hid_proType").val() == "trantjs") {
            urlHadler = "/HttpHandler/ProjectValueandAllot/TranProjectValueAllot.ashx";
            action = 12;
        }
        Global.SendRequest(urlHadler, { "Action": action, "data": jsonObj, "flag": 1, "msgID": $("#msgno").val(), "proType": $("#hid_proType").val() }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {
                    //消息
                    if ($("#hid_proType").val() == "jjs" || $("#hid_proType").val() == "tranjjs" || $("#hid_proType").val() == "trannts" || $("#hid_proType").val() == "trantjs") {
                        alert("个人产值确认不通过，已发给所长重新进行分配！");
                    }
                    else {
                        alert("个人产值确认不通过，已发给对应专业负责人重新进行分配！");
                    }
                }
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
            }
        });
    });

    $("#btn_Send").click(function () {

        //选中用户
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        }
        $(this).attr("disabled", "disabled");

        if ($("#hid_proType").val() == "jjs") {
            getjjsUserAndUpdateAudit("9", '1', jsonDataEntity);
        } else if ($("#hid_proType").val() == "tranjjs") {
            getjjsUserAndUpdateAudit("13", '1', jsonDataEntity);
        } else if ($("#hid_proType").val() == "trannts") {
            getntsUserAndUpdateAudit("7", '1', jsonDataEntity);
        } else if ($("#hid_proType").val() == "trantjs") {
            getntsUserAndUpdateAudit("11", '1', jsonDataEntity);
        }
        else {
            getUserAndUpdateAudit("8", '1', jsonDataEntity);
        }

    });


    //关闭
    $("#btn_close").click(function () {
        $("#btnApproval").attr("disabled", false);
    });

});


//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectValueandAllot/ProjectValueandAllotHandler.ashx";

    //数据         
    var data = { "Action": action, "flag": flag, "data": jsonData, "msgID": $("#msgno").val(), "proType": $("#hid_proType").val() };
    //提交数据
    $.post(url, data, function (jsonResult) {
        if (flag == "1") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
        }
        if (jsonResult == "0") {
            alert("发起分配错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("个人产值确认通过！");
            //查询系统新消息
            window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}

function getjjsUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx";

    //数据         
    var data = { "Action": action, "flag": flag, "data": jsonData, "msgID": $("#msgno").val(), "proType": $("#hid_proType").val() };
    //提交数据
    $.post(url, data, function (jsonResult) {

        if (jsonResult == "0") {
            alert("发起分配错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("个人产值确认通过！");
            //查询系统新消息
            window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}

function getntsUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectValueandAllot/TranProjectValueAllot.ashx";

    //数据         
    var data = { "Action": action, "flag": flag, "data": jsonData, "msgID": $("#msgno").val(), "proType": $("#hid_proType").val() };
    //提交数据
    $.post(url, data, function (jsonResult) {

        if (jsonResult == "0") {
            alert("发起分配错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("个人产值确认通过！");
            //查询系统新消息
            window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName); $("#AuditUserDiv").modal();
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}

//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        $('#AuditUserDiv').modal('hide');

        alert("个人产值全部确认完毕,消息已发给下一阶段审批人审批！");
        //查询系统新消息
        window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

    } else {
        alert("消息发送失败！");
    }
}

//设置状态
function setUnitStatus() {
    var isDone = $("#hid_IsDone").val();
    if ($.trim(isDone) == "D") {
        $("#ctl00_ContentPlaceHolder1_AuditSuggsion").attr("disabled", "disabled");
        $("#btnApproval").hide();
        $("#btnRefuse").hide();
    }
}