﻿var actionFlag;
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;

//返回消息列表
var pageIndex;
var MessageType;
var TypePost;
var MessageAction;
var Aflag;
var MessageKeys;

$(function () {

    CommonControl.SetFormWidth();


    //获取消息列表参数
    pageIndex = $("#pageIndex").val();
    MessageType = $("#MessageType").val();
    TypePost = $("#TypePost").val();
    MessageAction = $("#MessageAction").val();
    Aflag = $("#Aflag").val();
    MessageKeys = $("#MessageKeys").val();

    $("#CoperationBaseInfo tr:odd").css({ background: "White" });

    // 设置文本框样式
    CommonControl.SetTextBoxStyle();
    var isRounding = $("#HiddenIsRounding").val();
    var status = $("#HiddenStatus").val();


    var oneSuggstion = $("#HiddenOneSuggesiton").val();
    if (oneSuggstion && oneSuggstion.length > 0) {
        $("#OneSuggstion").val(oneSuggstion).attr("disabled", true).css("color", "black");
        $("#AuditUser", $("#OneTable")).html($("#HiddenAuditUser").val() + " <br/> " + $("#HiddenAuditData").val());
        $("#fieldAudit").show();
        $("#tbTopAudit").show();
        $("#OneTable").show();
        $("#btnPass").hide();
        $("#btnNotPass").hide();
    }



    if ($("#HiddenItemType").val() == "25") {
        $("#tbInfo tr:eq(0)").show();
    }
    else {
        $("#tbInfo tr:eq(0)").hide();
    }


    //消息审核状态
    var messageStauts = $("#hiddenMessageStatus").val();

    if (messageStauts == status || messageStauts == "") {
        InitViewStateTemp(status);
    } else {

        InitViewMessageState(messageStauts, status);
    }

    if ($("#hiddenProlink").val() == "prolink") {
        scjyHide();//生产经营 收费金额
        $("#fieldset").hide();
    }
    //判断是否删除
    if ($("#tbdelete tr td").text() == "此信息已被删除!") {
        $("#fieldset").children().hide();
        $("#btnPass").hide();
        $("#btnNotPass").hide();
    }

    //应分产值,为零不能进行全部金额借出
    var allotAccount = $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text());
    if (allotAccount == "0") {
        $("input[type=radio]").attr("disabled", "disabled");
    }
    //全部借出是
    $("#ctl00_ContentPlaceHolder1_radio_yes").click(function () {

        if ($(this).get(0).checked) {
            $("#tbSaveProcessInfo tr:eq(1)").hide();
            var allotAccount = $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text());
            $("#ctl00_ContentPlaceHolder1_txt_loanCount").val(allotAccount);
            $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text("0");
            $("#ctl00_ContentPlaceHolder1_txt_loanCount").attr("disabled", "disabled");
        }
        else {
            $("#tbSaveProcessInfo tr:eq(1)").show();
            $("#ctl00_ContentPlaceHolder1_txt_loanCount").val("");
            $("#ctl00_ContentPlaceHolder1_txt_loanCount").attr("disabled", false);
        }
    });

    //全部借出否
    $("#ctl00_ContentPlaceHolder1_radio_no").click(function () {

        if ($(this).get(0).checked) {
            $("#tbSaveProcessInfo tr:eq(1)").show();
            $("#ctl00_ContentPlaceHolder1_txt_loanCount").val("");
            $("#ctl00_ContentPlaceHolder1_txt_loanCount").attr("disabled", false);
        }
        else {
            $("#tbSaveProcessInfo tr:eq(1)").hide();
            var allotAccount = $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text());
            $("#ctl00_ContentPlaceHolder1_txt_loanCount").val(allotAccount);
            $("#ctl00_ContentPlaceHolder1_txt_loanCount").attr("disabled", "disabled");
            $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text("0");
        }
    });

    //项目借入金额
    $("#ctl00_ContentPlaceHolder1_txt_BorrowCount").change(function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        //可用借入金额
        var actulloanCount = $("#ctl00_ContentPlaceHolder1_txt_ActulloanCount").text().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_ActulloanCount").text());

        //借入金额
        var borrowCount = $("#ctl00_ContentPlaceHolder1_txt_BorrowCount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_BorrowCount").val());

        if (parseFloat(borrowCount) > parseFloat(actulloanCount)) {
            alert("项目借入金额金额不能超过可用借入金额！");
            $(this).val(0);
        }

        var allotAccount = $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text());

        var loanCount = $("#ctl00_ContentPlaceHolder1_txt_loanCount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_loanCount").val());

        borrowCount = $("#ctl00_ContentPlaceHolder1_txt_BorrowCount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_BorrowCount").val());

        $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text((parseFloat(allotAccount) - parseFloat(loanCount) + parseFloat(borrowCount)).toFixed(0));

    });
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

    //项目借出金额
    $("#ctl00_ContentPlaceHolder1_txt_loanCount").change(function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        //应分产值
        var allotAccount = $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text());

        //实际分配
        var actulCount = $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_ActulCount").text());
        //借出金额
        var loanCount = $("#ctl00_ContentPlaceHolder1_txt_loanCount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_loanCount").val());
        //借入金额
        var borrowCount = $("#ctl00_ContentPlaceHolder1_txt_BorrowCount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_BorrowCount").val());

        if (parseFloat(loanCount) > parseFloat(allotAccount)) {
            alert("项目借出金额金额不能超过实际分配金额！");
            $(this).val(0);
        }

        loanCount = $("#ctl00_ContentPlaceHolder1_txt_loanCount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_loanCount").val());

        $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text((parseFloat(allotAccount) - parseFloat(loanCount) + parseFloat(borrowCount)).toFixed(0));
    });

    //保存项目阶段信息
    $("#btnSaveProcess").click(function () {

        //全部借出
        if ($("#ctl00_ContentPlaceHolder1_radio_yes").get(0).checked) {

            if ($.trim($("#ctl00_ContentPlaceHolder1_txt_loanCount").val()).length == 0) {
                alert("项目借出金额不能为空！");
                return false;
            }

            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_loanCount").val())) {
                alert("项目借出金额请输入数字！");
                return false;
            }

            var loanCount = $("#ctl00_ContentPlaceHolder1_txt_loanCount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_loanCount").val());
            //应分产值
            var allotAccount = $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text());

            if (loanCount != allotAccount) {
                alert("项目借出金额必须等于分配金额！");
                return false;
            }

            $.post("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": "17", "allotID": $("#HiddenAllot").val(), "loanCount": loanCount, "actulCount": 0, "proID": $("#hidProID").val() }, function (jsonResult) {

                if (jsonResult > 0) {
                    window.location.reload();
                }
            });
        }
        else {


            if ($.trim($("#ctl00_ContentPlaceHolder1_txt_BorrowCount").val()).length == 0) {
                alert("项目借入金额不能为空！");
                return false;
            }

            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_BorrowCount").val())) {
                alert("项目借入金额请输入数字！");
                return false;
            }

            if ($.trim($("#ctl00_ContentPlaceHolder1_txt_loanCount").val()).length == 0) {
                alert("项目借出金额不能为空！");
                return false;
            }

            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_loanCount").val())) {
                alert("项目借出金额请输入数字！");
                return false;
            }

            //可用借入金额
            var actulloanCount = $("#ctl00_ContentPlaceHolder1_txt_ActulloanCount").text().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_ActulloanCount").text());

            //借入金额
            var borrowCount = $("#ctl00_ContentPlaceHolder1_txt_BorrowCount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_BorrowCount").val());

            if (parseFloat(borrowCount) > parseFloat(actulloanCount)) {
                alert("项目借入金额金额不能超过可用借入金额！");
                return false;
            }
            //应分产值
            var allotAccount = $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text());

            //实际分配
            var actulCount = $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_ActulCount").text());

            var loanCount = $("#ctl00_ContentPlaceHolder1_txt_loanCount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_loanCount").val());

            if (parseFloat(loanCount) > parseFloat(allotAccount)) {
                alert("项目借出金额金额不能超过应分金额！");
                return false;
            }

            if (parseFloat($("#ctl00_ContentPlaceHolder1_txt_ActulCount").text()) <= 0) {
                //alert("实际分配金额比例不能为负数！");
                alert("分配金额全部借出请点击是！");
                return false;
            }

            var allotID = $("#HiddenAllot").val();
            var loanCount = $("#ctl00_ContentPlaceHolder1_txt_loanCount").val();
            var actulCount = $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text();
            var proID = $("#hidProID").val();

            var borrowCount = $("#ctl00_ContentPlaceHolder1_txt_BorrowCount").val();

            $.post("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": "18", "allotID": allotID, "loanCount": loanCount, "actulCount": actulCount, "proID": proID, "borrowCount": borrowCount }, function (jsonResult) {

                if (jsonResult > 0) {
                    window.location.reload();
                }
            });
        }
    });

    //修改
    $("#addExternal").click(function () {

        $("#ctl00_ContentPlaceHolder1_txt_loanCount").attr("disabled", false);
        $("#ctl00_ContentPlaceHolder1_txt_BorrowCount").attr("disabled", false);
        $("#btnSaveProcess").show();
        $("#btnCancelProcess").show();
        $("#addExternal").hide();
        $("#spanSuccessInfo").hide();

        if (allotAccount == "0") {
            $("input[type=radio]").attr("disabled", "disabled");
        }
        else {
            $("input[type=radio]").attr("disabled", false);
        }
        if ($("#ctl00_ContentPlaceHolder1_radio_yes").get(0).checked) {
            $("#tbSaveProcessInfo tr:eq(1)").hide();
            $("#ctl00_ContentPlaceHolder1_txt_loanCount").attr("disabled", "disabled");
        }
        else {
            $("#tbSaveProcessInfo tr:eq(1)").show();
        }

    });

    //取消
    $("#btnCancelProcess").click(function () {

        window.location.reload();

    });


    //全部通过
    $("#btn_AllApproval").click(function () {


        var auditObj = {
            SysNo: $("#HiddenAuditRecordSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            Status: "B",
            ProSysNo: $("#hidProID").val()
        };
        auditObj.TwoSuggstion = $.trim($("#TwoSuggstion").val());
        var jsonObj = Global.toJSON(auditObj);

        if (!confirm("是否全部通过审核信息?")) {
            return false;
        }

        //地址
        var url = "/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx";

        //数据         
        var data = { "Action": 19, "data": jsonObj, "msgID": $("#msgno").val() };

        //提交数据
        $.post(url, data, function (jsonResult) {

            if (jsonResult == "-1") {
                alert("分配错误，请联系管理员！");
            }
            else if (jsonResult == "1") {
                alert("经济所产值全部通过成功，消息已发给审批人！");
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

            } else if (jsonResult == "0") {
                alert("经济所或者暖通所所长全部确认完毕之后，才能全部通过！");
            }
        });

    });


    //审核通过按钮
    $("#btnPass").click(function () {

        if (status == "A") {

            var msg = "";

            if (!ValidationDesignManager()) {
                return false;
            }

            if (!Validation()) {
                return false;
            }

            var _jzTotalPercent = 0;
            var _jzTotalCount = 0;
            var _azTotalPercent = 0;
            var _azTotalCount = 0;
            //设计 --建筑
            var _jzCount = 0;
            var _jzPercent = 0;
            //设计 --结构
            var _jgPercent = 0;
            var _jgCount = 0;
            //设计 --给排水
            var _gpsPercent = 0;
            var _gpsCount = 0;
            //设计 --暖通
            var _ntPercent = 0;
            var _ntCount = 0;
            //设计 --电气
            var _dqPercent = 0;
            var _dqCount = 0;
            //取得校对金额
            var prooPercent = 0;
            var prooAmount = 0;

            var stage = $("#HiddenItemType").val();

            if (stage == "13" || stage == "14" || stage == "15" || stage == "16" || stage == "17" || stage == "19" || stage == "20" || stage == "21" || stage == "22") {

                _jzTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text().replace("%", "");
                _jzTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text();
                _azTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(3)").text().replace("%", "");
                _azTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text();

                //设计 --建筑
                _jzCount = 0;
                _jzPercent = 0;
                //设计 --结构
                _jgPercent = 0;
                _jgCount = 0;

                //设计 --给排水
                _gpsPercent = 0;
                _gpsCount = 0;

                //设计 --暖通
                _ntPercent = 0;
                _ntCount = 0;

                //设计 --电气
                _dqPercent = 0;
                _dqCount = 0;

                //取得校对金额
                prooPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(4)").find("input").val();
                prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();
            }
            else if (stage == "23" || stage == "24" || stage == "25") {

                _jzTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text().replace("%", "");
                _jzTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text();

                _azTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text().replace("%", "");
                _azTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text();

                //设计 --建筑
                _jzPercent = $("#txtBulidging").val();
                _jzCount = $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text();
                //设计 --结构
                _jgPercent = $("#txtStructure").val();
                _jgCount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();

                //设计 --给排水
                _gpsPercent = $("#txtDrain").val();
                _gpsCount = $("#gvProjectValueProcess tr:eq(1) td:eq(6)").text();

                //设计 --暖通
                _ntPercent = $("#txtHavc").val();
                _ntCount = $("#gvProjectValueProcess tr:eq(1) td:eq(7)").text();

                //设计 --电气
                _dqPercent = $("#txtElectric").val();
                _dqCount = $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text();

                //取得校对金额
                prooPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(9)").find("input").val();
                prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(9)").text();
            }
            else {

                _jzTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text().replace("%", "");
                _jzTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text();

                _azTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text().replace("%", "");
                _azTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text();

                //设计 --建筑
                _jzCount = $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text();
                _jzPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(3)").text().replace("%", "");
                //设计 --结构
                _jgPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(4)").text().replace("%", "");
                _jgCount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();

                //设计 --给排水
                _gpsPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(6)").text().replace("%", "");
                _gpsCount = $("#gvProjectValueProcess tr:eq(1) td:eq(6)").text();

                //设计 --暖通
                _ntPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(7)").text().replace("%", "");
                _ntCount = $("#gvProjectValueProcess tr:eq(1) td:eq(7)").text();

                //设计 --电气
                _dqPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(8)").text().replace("%", "");
                _dqCount = $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text();

                //取得校对金额
                prooPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(9)").find("input").val();
                prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(9)").text();
            }

            var finalDataObj = {
                "ProNo": $("#hidProID").val(),
                "SysNo": $("#HiddenAuditRecordSysNo").val(),
                "ValueByMember": new Array(),
                "AuditUser": $("#HiddenLoginUser").val(),
                "typeStatus": $("#HiddenItemType").val(),
                "AllotID": $("#HiddenAllot").val(),
                "ProofreadPercent": prooPercent,
                "ProofreadCount": prooAmount,
                "BuildingPercent": _jzPercent,
                "BuildingCount": _jzCount,
                "StructurePercent": _jgPercent,
                "StructureCount": _jgCount,
                "DrainPercent": _gpsPercent,
                "DrainCount": _gpsCount,
                "HavcPercent": _ntPercent,
                "HavcCount": _ntCount,
                "ElectricPercent": _dqPercent,
                "ElectricCount": _dqCount,
                "TotalBuildingPercent": _jzTotalPercent,
                "TotalBuildingCount": _jzTotalCount,
                "TotalInstallationPercent": _azTotalPercent,
                "TotalInstallationCount": _azTotalCount,
                "DesinManagerUser": new Array()
            };

            // 取得
            $.each($("#gvProjectValueBymember tr"), function (index, tr) {
                finalDataObj.ValueByMember[index] =
                {
                    "mem_ID": $.trim($(tr).children("td:eq(1)").text()),
                    "DesignPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "DesignCount": $.trim($(tr).children("td:eq(4)").text()),
                    "SpecialtyHeadPercent": 0,
                    "SpecialtyHeadCount": 0,
                    "AuditPercent": 0,
                    "AuditCount": 0,
                    "ProofreadPercent": $.trim($(tr).children("td:eq(5)").find("input").val()),
                    "ProofreadCount": $.trim($(tr).children("td:eq(6)").text()),
                    "ItemType": $("#HiddenItemType").val(),
                    "IsHead": 0,
                    "IsExternal": $.trim($(tr).children("td:eq(1)").attr("wp"))
                };
            });



            // 取得
            $.each($("#tb_szxs tr:gt(0)"), function (index, tr) {
                finalDataObj.DesinManagerUser[index] =
            {
                "mem_ID": $.trim($(tr).children("td:eq(2)").find("input").attr("mem_id")),
                "DesignManagerPercent": $.trim($(tr).children("td:eq(2)").find("input").val()),
                "DesignManagerCount": $.trim($(tr).children("td:eq(3)").find("span").text()),
                "DesignType": "designManager"
            };
            });
            if (!confirm("是否通过审核信息?")) {
                return false;
            }

            $("#btnPass").attr("disabled", true);
            var jsonObj = Global.toJSON(finalDataObj);
            jsonDataEntity = jsonObj;
            getUserAndUpdateAudit("2", '0', jsonDataEntity);
        }
        else if (status == 'D') {
            var auditObj = {
                SysNo: $("#HiddenAuditRecordSysNo").val(),
                AuditUser: $("#HiddenLoginUser").val(),
                Status: "F",
                ProSysNo: $("#hidProID").val(),
                AllotID: $("#HiddenAllot").val()
            };

            if ($.trim($("#OneSuggstion").val()).length == 0) {
                alert("生产经营部意见不能为空！");
                return false;
            }
            auditObj.OneSuggestion = $.trim($("#OneSuggstion").val());

            if (!confirm("是否通过审核信息?")) {
                return false;
            }

            $("#btnPass").attr("disabled", true);
            var jsonObj = Global.toJSON(auditObj);

            Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": 3, "data": jsonObj, "flag": 1 }, null, null, function (jsonResult) {
                if (jsonResult == "0") {
                    alert("审核失败！");
                } else {
                    if (jsonResult == "1") {
                        //改变消息状态
                        var msg = new MessageCommProjAllot($("#msgno").val());
                        msg.ReadMsg();
                        //消息
                        alert("分配通过成功，消息已发送给申请人！");
                    }
                    //查询系统新消息
                    window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

                    // window.history.back();
                }
            });
        }
    });

    //不通过
    //审核不通过按钮，具体流程业务，基本同上面的审核通过一致
    $("#btnNotPass").click(function () {

        var auditObj = {
            SysNo: $("#HiddenAuditRecordSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            ProSysNo: $("#hidProID").val(),
            AllotID: $("#HiddenAllot").val()
        };

        if (status == "D") {
            if ($.trim($("#OneSuggstion").val()).length == 0) {
                alert("生产经营部意见不能为空！");
                return false;
            }
            auditObj.OneSuggestion = $.trim($("#OneSuggstion").val());
            auditObj.Status = "G";
        }
        else {
            auditObj.Status = "C";
        }

        if (!confirm("是否不通过审核信息?")) {
            return false;
        }
        var jsonObj = Global.toJSON(auditObj);

        Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": 3, "data": jsonObj, "flag": 1, "AllotUser": $("#HiddenAllotUser").val() }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {
                    //改变消息状态
                    var msg = new MessageCommProjAllot($("#msgno").val());
                    msg.ReadMsg();
                    //消息
                    alert("分配不通过成功，消息已发送给申请人！");
                }
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;

                // window.history.back();
            }
        });

    });


    //添加人员

    var chooseUserMain = new ChooseProjectValueUserControl($("#chooseUserMain"));
    //选择用户
    $("#chooseUser").click(function () {

        var parObj = {};
        parObj.UnitName = ""
        parObj.SpeName = "预算";
        chooseUserMain.Clear();
        chooseUserMain.BindData(parObj);


        $("#btn_UserMain").click(function () {
            //调用处理事件
            chooseUserMain.SaveUser(ChooseUserOfTheDepartmentCallBack);
        })
    });

    //选择用户
    function ChooseUserOfTheDepartmentCallBack(userArray, isWp) {

        //设置行数
        var trLength = 0;
        var IsAdd = false;

        $.each(userArray, function (index, item) {
            $("#gvProjectValueBymember tr").each(function () {
                $("td[class=cls_Column]", this).each(function () {
                    if (item.userSpecialtyname == $(this).text()) {
                        index = parseInt($(this).parent().index());
                        lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        IsAdd = true;
                    }
                });
            });
        });
        if (IsAdd) {

            var index = 0; //此行的索引
            var lengthTemp = 0; //本专业之前的rowspan
            var _tempTr = 0; //是否第一次添加
            var _userLength = userArray.length;
            $.each(userArray, function (index, item) {

                $("#gvProjectValueBymember tr").each(function () {
                    var tr = $(this);
                    $("td[class=cls_Column]", tr).each(function () {
                        var td = $(this);
                        if (item.userSpecialtyname == td.text()) {
                            index = parseInt(td.parent().index());
                            lengthTemp = td.attr("rowspan") == undefined ? 1 : td.attr("rowSpan");
                        }
                    });
                });

                var IsExisted = true;

                //循环该人员是否存在
                $("#gvProjectValueBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;
                        //_userLength = _userLength - 1;
                    }
                });

                //不存在添加
                if (IsExisted) {

                    //得到这个专业rowspanID
                    if (_tempTr == 0) {
                        $.each(userArray, function (index, items) {
                            $("#gvProjectValueBymember tr").each(function () {
                                var userSysNO = $(this).children(":eq(1)").text();
                                if (userSysNO == items.userSysNo) {
                                    _userLength = _userLength - 1;
                                }
                            });
                        });
                        $("#gvProjectValueBymember  tr:eq(" + index + ") td:eq(0)").attr("rowspan", parseInt(lengthTemp) + parseInt(_userLength));
                    }

                    //得到这个专业rowspanID
                    //if (_tempTr == 0) {
                    //    $("#gvProjectValueBymember  tr:eq(" + index + ") td:eq(0)").attr("rowspan", parseInt(lengthTemp) + parseInt(_userLength));
                    //}
                    var trString = "";
                    trString += "<tr>";
                    trString = "<tr><td class=\"display\"></td>";
                    trString += "<td class=\"display\" wp=" + isWp + ">" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    trString += "<td width= \"10%\"><input  maxlength=\"15\" type=\"text\" id=\"txtChooseUser\"  roles=\"design\" sz=" + item.userPrincipalship + " spe=" + item.userSpecialtyname + " />%</td>";
                    trString += "<td width= \"10%\"></td>";
                    trString += "<td width= \"10%\"><input  maxlength=\"15\" type=\"text\"   id=\"txtChooseUser\"  roles=\"jd\"  />%</td>";
                    trString += "<td width= \"10%\"></td>";
                    trString += "</tr>";

                    $(trString).insertAfter("#gvProjectValueBymember tr:eq(" + index + ")");
                    _tempTr = _tempTr + 1;
                }

            });

        }
        else {
            $.each(userArray, function (index, item) {

                //获取原Table下已有的用户          
                var IsExisted = true;
                //遍历已有的用户信息数组，如果已经存在，不允许添加

                $("#gvProjectValueBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;

                    }
                });

                if (IsExisted) {

                    var trString = "";
                    if (trLength == 0) {
                        trString = "<tr><td  width= \"15%\" class=\"cls_Column\" rowspan=" + (parseInt(userArray.length)) + ">" + item.userSpecialtyname + "</td>";
                    }
                    else {
                        trString = "<tr><td class=\"display\" width= \"10%\"></td>";
                    }
                    trString += "<td class=\"display\" wp=" + isWp + ">" + item.userSysNo + "</td>";
                    trString += "<td  width= \"15%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveB\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    trString += "<td width= \"17.5%\"><input  maxlength=\"15\" type=\"text\" id=\"txtChooseUser\"  roles=\"design\" sz=" + item.userPrincipalship + " spe=" + item.userSpecialtyname + " />%</td>";
                    trString += "<td width= \"17.5%\"></td>";
                    trString += "<td width= \"17.5%\"><input  maxlength=\"15\" type=\"text\"   id=\"txtChooseUser\"  roles=\"jd\"  />%</td>";
                    trString += "<td width= \"17.5%\"></td>";
                    trString += "</tr>";
                    trLength = trLength + 1;
                    $("#gvProjectValueBymember").append(trString);

                }
            });
        }
    }

    //添加外聘人员

    var chooseExternalUser = new ChooseExternalUserControl($("#chooseExternalUserDiv"));
    //选择用户
    $("#chooseExternalUser").click(function () {

        var parObj = {};
        parObj.UnitName = "经济所"
        parObj.SpeName = "预算";
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        //        $("#chooseExternalUserDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "外聘人员",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseExternalUser.SaveUser(ChooseUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        $("#btnChooseExt").click(function () {
            //调用处理事件
            chooseExternalUser.SaveUser(ChooseUserOfTheDepartmentCallBack);
        });
    });


    //删除用户A标签点击事件
    $("#resultUserRemoveA").live("click", function () {
        var spe = $(this).attr("spe");
        var rowspan = 0;
        $($(this).parent().parent().parent().parent().children().find("td:eq(0)")).each(function () {
            if ($(this).text() == spe) {
                rowspan = $(this).attr("rowspan");
                $(this).attr("rowspan", parseInt(rowspan) - 1);
            }
        });
        if (rowspan != 0) {
            $(this).parent().parent().parent().remove();
        }
    });

    //删除用户A标签点击事件
    $("#resultUserRemoveB").live("click", function () {
        $(this).parent().parent().parent().remove();
    });



    //建筑
    $("#txtBulidging").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var bzPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(1)").text().replace("%", "");
        var allotCount = $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text(total.toFixed(2));

        var otherPercent = $("#txtStructure").val().length == 0 ? 0 : parseFloat($("#txtStructure").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent);
        $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text(percent.toFixed(2));

        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text(count.toFixed(2));

    });

    //结构
    $("#txtStructure").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var bzPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(1)").text().replace("%", "");
        var allotCount = $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text(total.toFixed(2));

        var otherPercent = $("#txtBulidging").val().length == 0 ? 0 : parseFloat($("#txtBulidging").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent);
        $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text(percent.toFixed(2));
        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text(count.toFixed(2));

    });

    //给排水
    $("#txtDrain").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var bzPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(1)").text().replace("%", "");
        var allotCount = $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(6)").text(total.toFixed(2));

        var otherPercent = $("#txtHavc").val().length == 0 ? 0 : parseFloat($("#txtHavc").val());
        var otherPercent1 = $("#txtElectric").val().length == 0 ? 0 : parseFloat($("#txtElectric").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent) + parseFloat(otherPercent1);
        $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text(percent.toFixed(2));

        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text(count.toFixed(2));


    });

    //暖通
    $("#txtHavc").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var bzPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(1)").text().replace("%", "");
        var allotCount = $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(7)").text(total.toFixed(2));

        var otherPercent = $("#txtDrain").val().length == 0 ? 0 : parseFloat($("#txtDrain").val());
        var otherPercent1 = $("#txtElectric").val().length == 0 ? 0 : parseFloat($("#txtElectric").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent) + parseFloat(otherPercent1);
        $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text(percent.toFixed(2));
        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text(count.toFixed(2));


    });

    //电气
    $("#txtElectric").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var bzPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(1)").text().replace("%", "");
        var allotCount = $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text(total.toFixed(2));

        var otherPercent = $("#txtDrain").val().length == 0 ? 0 : parseFloat($("#txtDrain").val());
        var otherPercent1 = $("#txtHavc").val().length == 0 ? 0 : parseFloat($("#txtHavc").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent) + parseFloat(otherPercent1);
        $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text(percent.toFixed(2));

        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text(count.toFixed(2));

    });




    //校对--人员金额计算
    $(":text[roles=jd]", "#gvProjectValueBymember tr").each(function () {

        $(this).change(function () {


            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }

            var inputvalue = $(this).val();
            //取得校对金额
            var prooAmount = 0;
            var stage = $("#HiddenItemType").val();
            if (stage == "13" || stage == "14" || stage == "15" || stage == "16" || stage == "17" || stage == "19" || stage == "20" || stage == "21" || stage == "22") {
                prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();
            } else {
                prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(9)").text();
            }
            var proresultvalue = parseFloat(inputvalue) * parseFloat(prooAmount) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }
        });
    });

    //锅炉房 
    if (stage == "13" || stage == "14" || stage == "15" || stage == "16" || stage == "17" || stage == "19" || stage == "20" || stage == "21" || stage == "22") {
        //设计 --建筑
        // var _jz = $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text();
        //设计 --结构
        // var _gps = $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text();


        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {

            $(this).change(function () {
                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }
                var inputvalue = $(this).val();
                var proresultvalue = 0;

                var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text();

                proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });

        });

        //设计--人员金额计算--后来添加人员
        $("#txtChooseUser[roles=design]", "#gvProjectValueBymember tr").live('change', function () {
            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }
            var inputvalue = $(this).val();
            var proresultvalue = 0;
            var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text();
            proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });

    }
    else if (stage == "23" || stage == "24" || stage == "25") {


        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {

            $(this).change(function () {

                if (!ValidationProcess()) {
                    $(this).val(0);
                    return false;
                }

                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }

                var inputvalue = $(this).val();
                var proresultvalue = 0;
                var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text();
                proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });

        });

        //设计--人员金额计算--后来添加人员
        $("#txtChooseUser[roles=design]", "#gvProjectValueBymember tr").live('change', function () {
            if (!ValidationProcess()) {
                return false;
            }

            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }

            var inputvalue = $(this).val();
            var proresultvalue = 0;
            var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text();
            proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });
    } else {



        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {


            $(this).change(function () {

                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }

                var inputvalue = $(this).val();
                var proresultvalue = 0;
                //设计 --建
                var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text();
                proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });

        });

        //设计--人员金额计算--后来添加人员
        $("#txtChooseUser[roles=design]", "#gvProjectValueBymember tr").live('change', function () {

            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }
            var inputvalue = $(this).val();
            var proresultvalue = 0;
            //设计 --建
            var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text();
            proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });
    }


    //校对--人员金额计算--后来添加人员
    $("#txtChooseUser[roles=jd]", "#gvProjectValueBymember tr").live('change', function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        var inputvalue = $(this).val();

        //取得校对金额
        var prooAmount = 0;
        var stage = $("#HiddenItemType").val();
        if (stage == "13" || stage == "14" || stage == "15" || stage == "16" || stage == "17" || stage == "19" || stage == "20" || stage == "21" || stage == "22") {
            prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();
        } else {
            prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(9)").text();
        }

        var proresultvalue = parseFloat(inputvalue) * parseFloat(prooAmount) / 100;
        if (isRounding == "0") {
            $(this).parent().next().text(proresultvalue.toFixed(0));
        }
        else {
            $(this).parent().next().text(Math.floor(proresultvalue));
        }

    });

    //设总产值分配
    $("input[type=text]", "#tb_szxs").live("change", function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputvalue = $(this).val();

        var designManagerCount = $("#ctl00_ContentPlaceHolder1_lbl_DesignManagerCount").text();

        var inputAccount = parseFloat(inputvalue) * parseFloat(designManagerCount) / 100;

        if (isRounding == "0") {
            $(this).parent().next().children("span").text(inputAccount.toFixed(0));
        }
        else {
            $(this).parent().next().children("span").text(Math.floor(inputAccount));
        }

    });

    //校对计算
    $("input[id=txt_jd]").live("change", function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        if ($(this).val() > 100) {
            alert("比例不能大于100%！");
            $(this).val(0);
        }
        var inputvalue = $(this).val();
        var allotCount = $("#ctl00_ContentPlaceHolder1_txt_ActulCount").text();

        //校对金额
        // var prooAmount = 0;

        if (isRounding == "0") {
            prooAmount = (parseFloat(inputvalue) * parseFloat(allotCount) / 100).toFixed(0);
        }
        else {
            prooAmount = Math.floor(parseFloat(inputvalue) * parseFloat(allotCount) / 100);
        }

        var stage = $.trim($("#HiddenItemType").val());
       
        if (stage == "13" || stage == "14" || stage == "15" || stage == "16" || stage == "17" || stage == "19" || stage == "20" || stage == "21" || stage == "22") {
            $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text(prooAmount);
        } else {
            $("#gvProjectValueProcess tr:eq(1) td:eq(9)").text(prooAmount);
        }


        //编制合计

        var bzPercent = 100 - parseFloat(inputvalue);
        $("#gvProjectValueProcess tr:eq(0) td:eq(1)").text(bzPercent.toFixed(2));

        var bzTotal = parseFloat(allotCount) - parseFloat(prooAmount);

        var _total = bzTotal.toFixed(0);
        $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text(_total);


        if (stage == "25" || stage == "24" || stage == "23") {

            var inputValue_jz = $("#txtBulidging").val().length == 0 ? 0 : parseFloat($("#txtBulidging").val());
            var total_jz = parseFloat(inputValue_jz) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text(total_jz.toFixed(2));

            //结构
            var inputValue_jg = $("#txtStructure").val().length == 0 ? 0 : parseFloat($("#txtStructure").val());
            var total_jg = parseFloat(inputValue_jg) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text(total_jg.toFixed(2));

            //建筑合计
            var percent_jzTotalPercent = parseFloat(inputValue_jz) + parseFloat(inputValue_jg);
            $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text(percent_jzTotalPercent.toFixed(2));
            var count_jzhj = parseFloat(percent_jzTotalPercent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text(count_jzhj.toFixed(2));

            //给排水
            var inputValue_gps = $("#txtDrain").val().length == 0 ? 0 : parseFloat($("#txtDrain").val());
            var total_gps = parseFloat(inputValue_gps) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(6)").text(total_gps.toFixed(2));

            //暖通
            var inputValue_nt = $("#txtHavc").val().length == 0 ? 0 : parseFloat($("#txtHavc").val());
            var total_nt = parseFloat(inputValue_nt) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(7)").text(total_nt.toFixed(2));

            //电气
            var inputValue_dq = $("#txtElectric").val().length == 0 ? 0 : parseFloat($("#txtElectric").val());
            var total_dq = parseFloat(inputValue_dq) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text(total_dq.toFixed(2));

            //安装合计
            var percent_azTotal = parseFloat(inputValue_gps) + parseFloat(inputValue_nt) + parseFloat(inputValue_dq);
            $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text(percent_azTotal.toFixed(2));

            var count_an = parseFloat(percent_azTotal) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text(count_an.toFixed(2));
        }
        else if (stage == "11" || stage == "12" || stage == "18") {

            var inputValue_jz = $("#gvProjectValueProcess tr:eq(0) td:eq(3)").text().replace("%", "");
            var total_jz = parseFloat(inputValue_jz) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text(total_jz.toFixed(2));

            //结构
            var inputValue_jg = $("#gvProjectValueProcess tr:eq(0) td:eq(4)").text().replace("%", "");
            var total_jg = parseFloat(inputValue_jg) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text(total_jg.toFixed(2));

            //建筑合计
            var percent_jzTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text().replace("%", "");
            var count_jzhj = parseFloat(percent_jzTotalPercent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text(count_jzhj.toFixed(2));

            //给排水
            var inputValue_gps = $("#gvProjectValueProcess tr:eq(0) td:eq(6)").text().replace("%", "");
            var total_gps = parseFloat(inputValue_gps) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(6)").text(total_gps.toFixed(2));

            //暖通
            var inputValue_nt = $("#gvProjectValueProcess tr:eq(0) td:eq(7)").text().replace("%", "");
            var total_nt = parseFloat(inputValue_nt) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(7)").text(total_nt.toFixed(2));

            //电气
            var inputValue_dq = $("#gvProjectValueProcess tr:eq(0) td:eq(8)").text().replace("%", "");
            var total_dq = parseFloat(inputValue_dq) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text(total_dq.toFixed(2));

            //安装合计
            var percent_azTotal = $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text().replace("%", "");

            var count_an = parseFloat(percent_azTotal) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text(count_an.toFixed(2));
        }
        else {

            //建筑合计
            var percent_jzTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text().replace("%", "");
            var count_jzhj = parseFloat(percent_jzTotalPercent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text(count_jzhj.toFixed(2));

            //安装合计
            var percent_azTotal = $("#gvProjectValueProcess tr:eq(0) td:eq(3)").text().replace("%", "");

            var count_an = parseFloat(percent_azTotal) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
            $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text(count_an.toFixed(2));
        }
        //人员
        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {

            var inputvalue = $(this).val().length == 0 ? 0 : parseFloat($(this).val());
            var proresultvalue = 0;

            proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });


        $(":text[roles=jd]", "#gvProjectValueBymember tr").each(function () {

            var inputvalue = $(this).val().length == 0 ? 0 : parseFloat($(this).val());
            var proresultvalue = parseFloat(inputvalue) * parseFloat(prooAmount) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });
    });

    messageDialog = $("#auditShow").messageDialog;
    sendMessageClass = new MessageCommon(messageDialog);
    $("#btn_Send").click(function () {
        //选中用户
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        }
        $(this).attr("disabled", "disabled");
        getUserAndUpdateAudit('2', '1', jsonDataEntity);
    });
    //    messageDialog = $("#msgReceiverContainer").messageDialog({
    //        "button": {
    //            "发送消息": function () {
    //                //选中用户
    //                var _$mesUser = $(":checkbox[name=messageUser]:checked");

    //                if (_$mesUser.length == 0) {
    //                    alert("请至少选择一个流程审批人！");
    //                    return false;
    //                }

    //                getUserAndUpdateAudit('2', '1', jsonDataEntity);
    //            },
    //            "关闭": function () {
    //                $("#btnPass").attr("disabled", false);
    //                messageDialog.hide();
    //            }
    //        }
    //    });
    //    sendMessageClass = new MessageCommon(messageDialog);
    //关闭
    $("#btn_close").click(function () {
        $("#btnPass").attr("disabled", false);
    });
});

//验证设总比例
function ValidationDesignManager() {
    var flag = true;
    var valueemputy = "0";
    $.each($("input", "#tb_szxs tr td"), function (index, tr) {
        if ($.trim($(this).val()).length == 0) {
            valueemputy = $(this).val();
            return false;
        }
    });

    if ($.trim(valueemputy).length == 0) {
        alert("设总系数分配比例不能为空");
        flag = false;
        return false;
    }

    var ComValue = 0;
    $("input", "#tb_szxs").each(function () {
        ComValue += parseFloat($(this).val());
    });

    if (ComValue.toFixed(0) != 100) {
        alert("设总系数分配比例没有闭合，必须为100%");
        flag = false;
        return false;
    }

    return flag;
}

//验证工序
function ValidationProcess() {
    var flag = true;

    var budingPercent = $("#txtBulidging").val();
    if (budingPercent == "") {
        alert("建筑配置比例不能为空");
        flag = false;
        return false;

    }
    var structure = $("#txtStructure").val()
    if (structure == "") {
        alert("结构配置比例不能为空");
        flag = false;
        return false;

    }
    var drain = $("#txtDrain").val();
    if (drain == "") {
        alert("给排水配置比例不能为空");
        flag = false;
        return false;

    }
    var havc = $("#txtHavc").val();
    if (havc == "") {
        alert("暖通配置比例不能为空");
        flag = false;
        return false;

    }
    var electric = $("#txtElectric").val();
    if (electric == "") {
        alert("电气配置比例不能为空");
        flag = false;
        return false;

    }

    var total = parseFloat(budingPercent) + parseFloat(structure) + parseFloat(drain) + parseFloat(havc) + parseFloat(electric);

    if (total.toFixed(0) != 100) {
        alert("配置比例没有闭合，请修改");
        flag = false;
        return false;
    }

    return flag;
}

function Validation() {

    var flag = true;

    var stage = $("#HiddenItemType").val();

    if (stage == "23" || stage == "24" || stage == "25") {

        if (!ValidationProcess()) {
            flag = false;
            return false;
        }
    }

    var trLength = $("#gvProjectValueBymember tr").length;
    if (trLength == 0) {
        alert("请添加人员");
        flag = false;
        return false;
    }

    var valuespeemputy = "0";
    $.each($("input:not(:disabled)", "#gvProjectValueBymember tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valuespeemputy = $(this).val();
            return false;
        }
    });

    if ($.trim(valuespeemputy).length == 0) {
        alert("人员产值分配比例不能为空");
        flag = false;
        return false;
    }

    var designcountOne = 0;
    var isCheckOne = false;
    $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {
        designcountOne += parseFloat($(this).val());
        isCheckOne = true;
    });

    if (designcountOne.toFixed(0) != 100 && isCheckOne == true) {
        alert("人员编制比例没有闭合，必须为100%,请修改");
        flag = false;
        return false;
    }

    var designcountTwo = 0;
    var isCheckTwo = false;
    $(":text[roles=jd]", "#gvProjectValueBymember tr").each(function () {
        designcountTwo += parseFloat($(this).val());
        isCheckTwo = true;
    });

    if (designcountTwo.toFixed(0) != 100 && isCheckTwo == true) {
        alert("人员校对比例没有闭合，必须为100%,请修改");
        flag = false;
        return false;
    }
    return flag;
}

//浮点数
function checkRate(value) {
    var re = /^[0-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+$/;
    if (re.test(value)) {
        return true;
    }
}


//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //  地址
    var url = "/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx";

    //数据         
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    $.post(url, data, function (jsonResult) {
        if (flag == "1") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
        }
        if (jsonResult == "0") {
            alert("发起分配错误，请联系管理员！");
        }
        else if (jsonResult == "4") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
            alert("所长分配完毕，产值已发送给个人等待确认!");
            window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}

//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
        $("#AuditUserDiv").modal();
    }
    else {

        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);

    }
}

//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        $("#AuditUserDiv").modal('hide');
        alert("产值分配消息发送成功！");
        // 查询系统新消息
        window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx";
        //window.history.back();
    } else {
        alert("消息发送失败！");
    }
}

//初始化页面状态
function InitViewStateTemp(auditStatus) {

    switch (auditStatus) {
        case "A":

            if ($("#hiddenType").val() != "tran") {
                scjyHide();//生产经营 收费金额
                $("#tb_ysfc tr:eq(0) td:eq(1)").html("");
                $("#tb_ysfc tr:eq(0) td:eq(2)").html("");
                $("#tbInfo tr:eq(6)").hide();
                $("#tbInfo tr:eq(7)").hide();
                //$("#fieldset").hide();


                if ($("#hidIsTAllPass").val() == "") {
                    $("#tbSaveProcessInfo").show();
                    $("#tbSaveed").hide();
                    $("#btnSaveProcess").show();

                    $("#saveProcessInfo").show();
                    $("#tbSaveed").hide();
                    $("#tbSaveProcessInfo").show();
                    $("#btnSaveProcess").show();


                    $("#memField").hide();
                    $("#file_szxs").hide();

                    $("#btnPass").hide();
                    $("#btnNotPass").hide();
                }
                else {
                    if ($("#hidIsTAllPass").val() == "1") {

                        $("#saveProcessInfo").show();
                        $("#tbSaveProcessInfo").show();
                        if ($("#hidIsTAllPass").val() == "1") {
                            $("#tbSaveProcessInfo tr:eq(1)").hide();
                        }

                        $("#tbSaveed").hide();

                        $("#ctl00_ContentPlaceHolder1_drp_buildtype").attr("disabled", "disabled");
                        $("#ctl00_ContentPlaceHolder1_stage").attr("disabled", "disabled");
                        $("#ctl00_ContentPlaceHolder1_txt_loanCount").attr("disabled", "disabled");
                        $("#auditSuggstion").show();
                        $("#spanSuccessInfo").show();
                        $("#addExternal").show();

                        $("#btnApproval").hide();
                        $("#btn_AllApproval").show();

                        $("#memField").hide();
                        $("#file_szxs").hide();
                        $("#btnPass").hide();
                        $("#btnNotPass").hide();
                    }
                    else {

                        $("#saveProcessInfo").show();
                        $("#tbSaveProcessInfo").show();
                        $("#tbSaveed").hide();

                        $("#ctl00_ContentPlaceHolder1_txt_BorrowCount").attr("disabled", "disabled");
                        $("#ctl00_ContentPlaceHolder1_txt_loanCount").attr("disabled", "disabled");
                        $("#auditSuggstion").show();
                        $("#spanSuccessInfo").show();
                        $("#addExternal").show();
                        $("#memField").show();
                        $("#file_szxs").show();
                        $("#btnPass").show();
                        $("#btnNotPass").show();
                    }


                }

            }
            else {

                $("#file_szxs").hide();
                $("#memField").hide();
                $("#btnPass").hide();
                $("#btnNotPass").hide();
            }
            break;
        case "B":
            $("#file_szxs").show();

            $("#tbSaveProcessInfo").hide();
            $("#tbSaveed").show();

            if ($("#hiddenType").val() != "tran") {
                setActul();
                $("#memFieldShow").show();
                $("#fieldAudit").show();


            } else {

                $("#btnPass").hide();
                $("#btnNotPass").hide();
                $("#memFieldShow").show();
                $("#btnPrintValue").show();

            }

            if ($("#hidIsTAllPass").val() == "1") {
                $("#btnPass").hide();
                $("#btnNotPass").hide();
                $("#file_szxs").hide();
                $("#memFieldShow").hide();
                $("#fieldAudit").hide();
                $("#btnPrintValue").hide();
                $("#tbSaveed tr:eq(1)").hide();
                $("#tbSaveed tr:eq(2)").show();
            }
            else {
                $("#tbSaveed tr:eq(1)").show();
                $("#tbSaveed tr:eq(2)").show();
            }
            break;
        case "C":
            setActul();
            $("#file_szxs").show();
            $("#memField").show();
            $("#chooseUser").hide();
            $("#chooseExternalUser").hide();
            $("input", $("#memField")).attr("disabled", true);
            $("#btnPass").hide();
            $("#btnNotPass").hide();
            break;
        case "D":
            setActul();
            $("#file_szxs").show();
            $("#memFieldShow").show();
            $("#fieldAudit").show();
            $("#btnPrintValue").show();


            break;
        case "E":
            setActul();
            $("#file_szxs").show();
            $("#memFieldShow").show();
            $("#fieldAudit").show();
            $("input", $("#fieldAudit")).attr("disabled", true);
            break;
        case "F":
            setActul();
            $("#file_szxs").show();
            $("#memFieldShow").show();
            $("#fieldAudit").show();
            $("#btnPrintValue").show();


            break;
        case "G":
            setActul();
            $("#file_szxs").show();
            $("#memFieldShow").show();
            $("#fieldAudit").show();
            $("input", $("#fieldAudit")).attr("disabled", true);
            break;
    }
}

function setActul() {
    $("#saveProcessInfo").show();
    $("#tbSaveed").show();
    if ($("#hidIsTAllPass").val() == "1") {
        $("#tbSaveed tr:eq(1)").hide();
        $("#tbSaveed tr:eq(2)").hide();
    }
    $("#tbSaveProcessInfo").hide();
    $("#auditSuggstion").show();
    $("#btnEditALL").hide();

}

function InitViewMessageState(messageStatus, staus) {

    if (messageStatus == "A") {
        scjyHide();//生产经营 收费金额
        $("#fieldset").hide();
        $("#memField").show();
        $("#file_szxs").show();
        $("#btnPass").hide();
        $("#btnNotPass").hide();
    }
    if (messageStatus == "A" && staus == "C") {
        $("#memField").show();
        $("#file_szxs").show();
        $("#chooseUser").hide();
        $("#chooseExternalUser").hide();
        $("input", $("#memField")).attr("disabled", true);
    }
    if (messageStatus == "B" && staus != "E") {
        $("#memFieldShow").show();
        $("#file_szxs").show();
        $("#fieldAudit").show();
        $("input", $("#fieldAudit")).attr("disabled", true);
    }
    if (messageStatus == "D" && staus != "G") {
        $("#memFieldShow").show();
        $("#file_szxs").show();
        $("#fieldAudit").show();
        $("input", $("#fieldAudit")).attr("disabled", true);
    }
}
//不是生产经营 设置金额隐藏
function scjyHide() {
    // $("#tb_project  tr[class=scjy]").hide();
}