﻿$(document).ready(function () {

    var chooseUserMain = new ChooseProjectValueUserControl($("#chooseUserMain"));
    //选择用户
    $("#chooseOneUser").click(function () {

        $("#chooseUserMainDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseUserMainDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseUserMainDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = "";
        parObj.SpeName = $("#hiddenSpeName").val();
        chooseUserMain.Clear();
        chooseUserMain.BindData(parObj);
        //        $("#chooseUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "添加用户",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			              
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btn_UserMain1\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseUserMainDiv").append(trString);
        $("#btn_UserMain1").click(function () {
            //$("#btn_UserMain1").live("click", function () {
            //调用处理事件
            chooseUserMain.SaveUser(ChooseOneUserOfTheDepartmentCallBack);
        })
    });

    //选择用户
    function ChooseOneUserOfTheDepartmentCallBack(userArray, isWp) {

        //设置行数
        var trLength = $("#gvProjectValueOneBymember tr").length;
        $("#gvProjectValueOneBymember tr td:eq(0)").attr("rowspan", parseInt(trLength) + parseInt(userArray.length));

        //设计金额
        $tr = $("#gvProjectValueOneBymember tr:eq(0)");
        var designAmount = $tr.children(":eq(5)").text();
        var headAmount = $tr.children(":eq(8)").text();
        var auditAmount = $tr.children(":eq(11)").text();
        var jdAmount = $tr.children(":eq(14)").text();
        var process = $tr.children(":eq(15)").text();
        $.each(userArray, function (index, item) {

            //获取原Table下已有的用户          
            var AllowAdd = true;
            //遍历已有的用户信息数组，如果已经存在，不允许添加
            if (isWp == 0) {
                $("#gvProjectValueOneBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    var istempWpOne = $(this).children(":eq(1)").attr("wp");
                    if (istempWpOne == 0) {
                        if (parseInt(userSysNO) == item.userSysNo) {
                            AllowAdd = false;
                        }
                    }
                });
            } else {
                $("#gvProjectValueOneBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    var istempWpOne = $(this).children(":eq(1)").attr("wp");
                    if (istempWpOne == 1) {
                        if (parseInt(userSysNO) == item.userSysNo) {
                            AllowAdd = false;
                        }
                    }
                });
            }

            if (AllowAdd) {

                var trString = "<tr><td class=\"display\"></td>";
                trString += "<td class=\"display\" wp=" + isWp + ">" + item.userSysNo + "</td>";
                trString += "<td><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\">X</a></span></td>";
                if (item.checkDesign == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\" id=\"txtChooseUser\" zy=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }

                trString += "<td class=\"display\">" + designAmount + "</td>";
                if (item.checkHead == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"   id=\"txtChooseUser\"    />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + headAmount + "</td>";
                if (item.checkAudit == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"  id=\"txtChooseUser\"   />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + auditAmount + "</td>";
                if (item.checkProof == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"  id=\"txtChooseUser\"    />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + jdAmount + "</td>";
                trString += "<td class=\"display\">" + process + "</td>";
                trString += "</tr>";
                $("#gvProjectValueOneBymember").append(trString);
            }
        });
    }

    var chooseExternalUser = new ChooseExternalUserControl($("#chooseExternalUserDiv"));
    //选择用户--外聘人员
    $("#chooseExternalOneUser").click(function () {

        $("#chooseExtUserDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseExtUserDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseExtUserDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = $("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text();
        parObj.SpeName = $("#hiddenSpeName").val();
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        //        $("#chooseExternalUserDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "外聘人员",
        //            buttons:
        //			        {
        //			            "确定": function () {

        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btnChooseExt1\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseExtUserDiv").append(trString);
        $("#btnChooseExt1").click(function () {
            //调用处理事件
            chooseExternalUser.SaveUser(ChooseOneUserOfTheDepartmentCallBack);
        });
    });


    //选择用户
    $("#chooseTwoUser").click(function () {

        $("#chooseUserMainDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseUserMainDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseUserMainDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = "";
        parObj.SpeName = $("#hiddenSpeName").val();
        chooseUserMain.Clear();
        chooseUserMain.BindData(parObj);
        //        $("#chooseUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "添加用户",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseUserMain.SaveUser(ChooseTwoUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btn_UserMain2\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseUserMainDiv").append(trString);
        $("#btn_UserMain2").click(function () {
            //调用处理事件
            chooseUserMain.SaveUser(ChooseTwoUserOfTheDepartmentCallBack);
        })
    });

    //选择用户
    function ChooseTwoUserOfTheDepartmentCallBack(userArray, isWp) {

        //设置行数
        var trLength = $("#gvProjectValueTwoBymember tr").length;
        $("#gvProjectValueTwoBymember tr td:eq(0)").attr("rowspan", parseInt(trLength) + parseInt(userArray.length));

        //设计金额
        $tr = $("#gvProjectValueTwoBymember tr:eq(0)");
        var designAmount = $tr.children(":eq(5)").text();
        var headAmount = $tr.children(":eq(8)").text();
        var auditAmount = $tr.children(":eq(11)").text();
        var jdAmount = $tr.children(":eq(14)").text();
        var process = $tr.children(":eq(15)").text();
        $.each(userArray, function (index, item) {

            //获取原Table下已有的用户          
            var AllowAdd = true;
            //遍历已有的用户信息数组，如果已经存在，不允许添加

            if (isWp == 0) {
                $("#gvProjectValueTwoBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    var istempWpOne = $(this).children(":eq(1)").attr("wp");
                    if (istempWpOne == 0) {
                        if (parseInt(userSysNO) == item.userSysNo) {
                            AllowAdd = false;
                        }
                    }
                });
            } else {
                $("#gvProjectValueTwoBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    var istempWpOne = $(this).children(":eq(1)").attr("wp");
                    if (istempWpOne == 1) {
                        if (parseInt(userSysNO) == item.userSysNo) {
                            AllowAdd = false;
                        }
                    }
                });
            }
            if (AllowAdd) {

                var trString = "<tr><td class=\"display\"></td>";
                trString += "<td class=\"display\" wp=" + isWp + ">" + item.userSysNo + "</td>";
                trString += "<td><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\">X</a></span></td>";
                if (item.checkDesign == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\" id=\"txtChooseUser\"  zy=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }

                trString += "<td class=\"display\">" + designAmount + "</td>";
                if (item.checkHead == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"   id=\"txtChooseUser\"    />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + headAmount + "</td>";
                if (item.checkAudit == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"  id=\"txtChooseUser\"   />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + auditAmount + "</td>";
                if (item.checkProof == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"  id=\"txtChooseUser\"    />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + jdAmount + "</td>";
                trString += "<td class=\"display\">" + process + "</td>";
                trString += "</tr>";
                $("#gvProjectValueTwoBymember").append(trString);
            }
        });
    }

    //选择用户--外聘人员
    $("#chooseTwoExternalUser").click(function () {
        $("#chooseExtUserDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseExtUserDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseExtUserDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = $("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text();
        parObj.SpeName = $("#hiddenSpeName").val();
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        //        $("#chooseExternalUserDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "外聘人员",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseExternalUser.SaveUser(ChooseTwoUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btnChooseExt2\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseExtUserDiv").append(trString);
        $("#btnChooseExt2").click(function () {
            //调用处理事件
            chooseExternalUser.SaveUser(ChooseTwoUserOfTheDepartmentCallBack);
        });
    });


    //选择用户
    $("#chooseThreeUser").click(function () {
        $("#chooseUserMainDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseUserMainDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseUserMainDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = "";
        parObj.SpeName = $("#hiddenSpeName").val();
        chooseUserMain.Clear();
        chooseUserMain.BindData(parObj);
        //        $("#chooseUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "添加用户",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseUserMain.SaveUser(ChooseThreeUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btn_UserMain3\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseUserMainDiv").append(trString);
        $("#btn_UserMain3").click(function () {
            //调用处理事件
            chooseUserMain.SaveUser(ChooseThreeUserOfTheDepartmentCallBack);
        })
    });

    //选择用户
    function ChooseThreeUserOfTheDepartmentCallBack(userArray, isWp) {

        //设置行数
        var trLength = $("#gvProjectValueThreeBymember tr").length;
        $("#gvProjectValueThreeBymember tr td:eq(0)").attr("rowspan", parseInt(trLength) + parseInt(userArray.length));

        //设计金额
        $tr = $("#gvProjectValueThreeBymember tr:eq(0)");
        var designAmount = $tr.children(":eq(5)").text();
        var headAmount = $tr.children(":eq(8)").text();
        var auditAmount = $tr.children(":eq(11)").text();
        var jdAmount = $tr.children(":eq(14)").text();
        var process = $tr.children(":eq(15)").text();
        $.each(userArray, function (index, item) {

            //获取原Table下已有的用户          
            var AllowAdd = true;
            //遍历已有的用户信息数组，如果已经存在，不允许添加

            if (isWp == 0) {
                $("#gvProjectValueThreeBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    var istempWpOne = $(this).children(":eq(1)").attr("wp");
                    if (istempWpOne == 0) {
                        if (parseInt(userSysNO) == item.userSysNo) {
                            AllowAdd = false;
                        }
                    }
                });
            } else {
                $("#gvProjectValueThreeBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    var istempWpOne = $(this).children(":eq(1)").attr("wp");
                    if (istempWpOne == 1) {
                        if (parseInt(userSysNO) == item.userSysNo) {
                            AllowAdd = false;
                        }
                    }
                });
            }
            if (AllowAdd) {

                var trString = "<tr><td class=\"display\"></td>";
                trString += "<td class=\"display\" wp=" + isWp + ">" + item.userSysNo + "</td>";
                trString += "<td><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\">X</a></span></td>";
                if (item.checkDesign == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\" id=\"txtChooseUser\" zy=" + item.userSpecialtyname + " sz=" + item.userPrincipalship + " />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }

                trString += "<td class=\"display\">" + designAmount + "</td>";
                if (item.checkHead == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"   id=\"txtChooseUser\"    />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + headAmount + "</td>";
                if (item.checkAudit == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"  id=\"txtChooseUser\"   />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + auditAmount + "</td>";
                if (item.checkProof == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"  id=\"txtChooseUser\"    />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + jdAmount + "</td>";
                trString += "<td class=\"display\">" + process + "</td>";
                trString += "</tr>";
                $("#gvProjectValueThreeBymember").append(trString);
            }
        });
    }

    //选择用户--外聘人员
    $("#chooseThreeExternalUser").click(function () {
        $("#chooseExtUserDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseExtUserDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseExtUserDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = $("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text();
        parObj.SpeName = $("#hiddenSpeName").val();
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        //        $("#chooseExternalUserDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "外聘人员",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseExternalUser.SaveUser(ChooseThreeUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btnChooseExt3\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseExtUserDiv").append(trString);
        $("#btnChooseExt3").click(function () {
            //调用处理事件
            chooseExternalUser.SaveUser(ChooseThreeUserOfTheDepartmentCallBack);
        });
    });


    //选择用户
    $("#chooseFourUser").click(function () {
        $("#chooseUserMainDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseUserMainDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseUserMainDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = "";
        parObj.SpeName = $("#hiddenSpeName").val();
        chooseUserMain.Clear();
        chooseUserMain.BindData(parObj);
        //        $("#chooseUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "添加用户",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseUserMain.SaveUser(ChooseFourUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btn_UserMain4\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseUserMainDiv").append(trString);
        $("#btn_UserMain4").click(function () {
            //调用处理事件
            chooseUserMain.SaveUser(ChooseFourUserOfTheDepartmentCallBack);
        })
    });

    //选择用户
    function ChooseFourUserOfTheDepartmentCallBack(userArray, isWp) {

        //设置行数
        var trLength = $("#gvProjectValueFourBymember tr").length;
        $("#gvProjectValueFourBymember tr td:eq(0)").attr("rowspan", parseInt(trLength) + parseInt(userArray.length));

        //设计金额
        $tr = $("#gvProjectValueFourBymember tr:eq(0)");
        var designAmount = $tr.children(":eq(5)").text();
        var headAmount = $tr.children(":eq(8)").text();
        var auditAmount = $tr.children(":eq(11)").text();
        var jdAmount = $tr.children(":eq(14)").text();
        var process = $tr.children(":eq(15)").text();
        $.each(userArray, function (index, item) {

            //获取原Table下已有的用户          
            var AllowAdd = true;
            //遍历已有的用户信息数组，如果已经存在，不允许添加


            if (isWp == 0) {
                $("#gvProjectValueFourBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    var istempWpOne = $(this).children(":eq(1)").attr("wp");
                    if (istempWpOne == 0) {
                        if (parseInt(userSysNO) == item.userSysNo) {
                            AllowAdd = false;
                        }
                    }
                });
            } else {
                $("#gvProjectValueFourBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    var istempWpOne = $(this).children(":eq(1)").attr("wp");
                    if (istempWpOne == 1) {
                        if (parseInt(userSysNO) == item.userSysNo) {
                            AllowAdd = false;
                        }
                    }
                });
            }

            if (AllowAdd) {

                var trString = "<tr><td class=\"display\"></td>";
                trString += "<td class=\"display\" wp=" + isWp + ">" + item.userSysNo + "</td>";
                trString += "<td><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\">X</a></span></td>";
                if (item.checkDesign == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\" id=\"txtChooseUser\"  sz=" + item.userPrincipalship + " />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }

                trString += "<td class=\"display\">" + designAmount + "</td>";
                if (item.checkHead == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"   id=\"txtChooseUser\"    />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + headAmount + "</td>";
                if (item.checkAudit == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"  id=\"txtChooseUser\"   />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + auditAmount + "</td>";
                if (item.checkProof == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"  id=\"txtChooseUser\"    />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + jdAmount + "</td>";
                trString += "<td class=\"display\">" + process + "</td>";
                trString += "</tr>";
                $("#gvProjectValueFourBymember").append(trString);
            }
        });
    }


    //选择用户--外聘人员
    $("#chooseFourExternalUser").click(function () {
        $("#chooseExtUserDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseExtUserDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseExtUserDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = $("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text();
        parObj.SpeName = $("#hiddenSpeName").val();
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        //        $("#chooseExternalUserDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "外聘人员",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseExternalUser.SaveUser(ChooseFourUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btnChooseExt4\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseExtUserDiv").append(trString);
        $("#btnChooseExt4").click(function () {
            //调用处理事件
            chooseExternalUser.SaveUser(ChooseFourUserOfTheDepartmentCallBack);
        });
    });


    //
    //选择用户
    $("#chooseUser").click(function () {
        $("#chooseUserMainDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseUserMainDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseUserMainDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = "";
        parObj.SpeName = $("#hiddenSpeName").val();
        chooseUserMain.Clear();
        chooseUserMain.BindData(parObj);
        //        $("#chooseUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "添加用户",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseUserMain.SaveUser(ChooseUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btn_UserMain\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseUserMainDiv").append(trString);
        $("#btn_UserMain").click(function () {
            //调用处理事件
            chooseUserMain.SaveUser(ChooseUserOfTheDepartmentCallBack);
        })
    });

    //选择用户
    function ChooseUserOfTheDepartmentCallBack(userArray, isWp) {

        //设置行数
        var trLength = $("#gvProjectValueBymember tr").length;
        $("#gvProjectValueBymember tr td:eq(0)").attr("rowspan", parseInt(trLength) + parseInt(userArray.length));

        //设计金额
        $tr = $("#gvProjectValueBymember tr:eq(0)");
        var designAmount = $tr.children(":eq(5)").text();
        var headAmount = $tr.children(":eq(8)").text();
        var auditAmount = $tr.children(":eq(11)").text();
        var jdAmount = $tr.children(":eq(14)").text();
        var process = $tr.children(":eq(15)").text();
        $.each(userArray, function (index, item) {

            //获取原Table下已有的用户          
            var AllowAdd = true;
            //遍历已有的用户信息数组，如果已经存在，不允许添加

            if (isWp == 0) {
                $("#gvProjectValueBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    var istempWpOne = $(this).children(":eq(1)").attr("wp");
                    if (istempWpOne == 0) {
                        if (parseInt(userSysNO) == item.userSysNo) {
                            AllowAdd = false;
                        }
                    }
                });
            } else {
                $("#gvProjectValueBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    var istempWpOne = $(this).children(":eq(1)").attr("wp");
                    if (istempWpOne == 1) {
                        if (parseInt(userSysNO) == item.userSysNo) {
                            AllowAdd = false;
                        }
                    }
                });
            }

            if (AllowAdd) {

                var trString = "<tr><td class=\"display\"></td>";
                trString += "<td class=\"display\" wp=" + isWp + ">" + item.userSysNo + "</td>";
                trString += "<td><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\">X</a></span></td>";
                if (item.checkDesign == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\" id=\"txtChooseUser\"  sz=" + item.userPrincipalship + " />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }

                trString += "<td class=\"display\">" + designAmount + "</td>";
                if (item.checkHead == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"   id=\"txtChooseUser\"    />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + headAmount + "</td>";
                if (item.checkAudit == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"  id=\"txtChooseUser\"   />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + auditAmount + "</td>";
                if (item.checkProof == "1") {
                    trString += "<td><input  maxlength=\"15\" type=\"text\"  id=\"txtChooseUser\"    />%</td>";
                    trString += "<td></td>";
                }
                else {
                    trString += "<td>0%</td>";
                    trString += "<td>0</td>";
                }
                trString += "<td class=\"display\">" + jdAmount + "</td>";
                trString += "<td class=\"display\">" + process + "</td>";
                trString += "</tr>";
                $("#gvProjectValueBymember").append(trString);
            }
        });
    }


    //选择用户--外聘人员 室外工程
    $("#chooseExternalUser").click(function () {
        $("#chooseExtUserDiv").modal();
        $(this).attr("data-toggle", "modal");
        if ($("div[class=modal-footer]", "#chooseExtUserDiv").html() != undefined) {
            $("div[class=modal-footer]", "#chooseExtUserDiv").remove();
        }
        var parObj = {};
        parObj.UnitName = $("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text();
        parObj.SpeName = $("#hiddenSpeName").val();
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        //        $("#chooseExternalUserDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "外聘人员",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseExternalUser.SaveUser(ChooseUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        var trString = "<div class=\"modal-footer\">";
        trString = trString + "<button type=\"button\" id=\"btnChooseExt\" data-dismiss=\"modal\" class=\"btn green btn-default\">  确定</button>";
        trString = trString + "  <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-default\">关闭</button>";
        trString = trString + "</div>";
        $("#chooseExtUserDiv").append(trString);
        $("#btnChooseExt").click(function () {
            //调用处理事件
            chooseExternalUser.SaveUser(ChooseUserOfTheDepartmentCallBack);
        });
    });

});