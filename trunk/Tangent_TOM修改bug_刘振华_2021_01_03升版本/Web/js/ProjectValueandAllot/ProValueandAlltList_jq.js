﻿
//分配中
function AllotList3() {
    $("#jqGrid3").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '',  '', '项目名称', '承接部门', '建筑类别', '合同额(万元)', '实收总额(万元)', '分配金额(万元)', '分配进度', '详细', '分配'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'AuditRecordSysNo', index: 'AuditRecordSysNo', hidden: true, editable: true },
                            
                             { name: 'pro_name', index: 'pro_name', width: 250, formatter: colNameShowFormatter },
                             { name: 'Unit', index: 'Unit', width: 80, align: 'center' },
                             { name: 'BuildType', index: 'BuildType', width: 80, align: 'center' },
                             { name: 'Cpr_Acount', index: 'p.cpr_Acount', width: 90, align: 'center' },
                             { name: 'PayShiCount', index: 'PayShiCount', width: 110, align: 'center' },
                             { name: 'AllotCount', index: 'Allotcount', width: 110, align: 'center' },
                             { name: 'Percent', index: 'Percent', width: 100, align: 'center', formatter: colStatusShowFormatter },
                             { name: 'pro_ID', index: 'pro_ID', width: 60, align: 'center', formatter: colStrShowFormatter },
                             { name: 'pro_ID', index: 'pro_ID', width: 80, align: 'center', formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "action": "ProValueandAlltList", "type": "no", "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager3",
        viewrecords: true,
        caption: "未分配项目",
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod3,
        loadComplete: loadCompMethod3
    });




    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid3").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid3").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
}

//统计 
function completeMethod3() {
    var rowIds = $("#jqGrid3").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid3 [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));
    }


    var sumAcount = parseFloat($("#jqGrid3").getCol("Cpr_Acount", false, 'sum')).toFixed(4);
    var allPayShiCount = parseFloat($("#jqGrid3").getCol("PayShiCount", false, 'sum')).toFixed(4);

    $("#jqGrid3").footerData('set', { pro_name: "合计:", Cpr_Acount: sumAcount, PayShiCount: allPayShiCount, AllotCount: 0 }, false);
  
    //进度条
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("percent"));
        $(item).progressbar({
            value: percent
        });
    });
}
//无数据
function loadCompMethod3() {
    $("#jqGrid3").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid3").getGridParam("records"));

    if (rowcount <= 0) {
        if ($("#nodata3").text() == '') {
            $("#jqGrid3").parent().append("<div id='nodata3'>没有符合条件数据！</div>")
        }
        else { $("#nodata3").show(); }
    }
    else {
        $("#nodata3").hide();
    }
}
//分配中
function AllotList() {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '',  '', '项目名称', '承接部门', '建筑类别', '合同额(万元)', '实收总额(万元)', '分配金额(万元)', '分配进度', '详细', '分配'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'AuditRecordSysNo', index: 'AuditRecordSysNo', hidden: true, editable: true },
                            
                             { name: 'pro_name', index: 'pro_name', width: 250, formatter: colNameShowFormatter },
                             { name: 'Unit', index: 'Unit', width: 80, align: 'center' },
                             { name: 'BuildType', index: 'BuildType', width: 80, align: 'center' },
                             { name: 'Cpr_Acount', index: 'cpr_Acount', width: 90, align: 'center' },
                             { name: 'PayShiCount', index: 'PayShiCount', width: 110, align: 'center' },
                             { name: 'AllotCount', index: 'Allotcount', width: 110, align: 'center' },
                             { name: 'Percent', index: 'Percent', width: 100, align: 'center', formatter: colStatusShowFormatter },
                             { name: 'pro_ID', index: 'pro_ID', width: 60, align: 'center', formatter: colStrShowFormatter },
                             { name: 'pro_ID', index: 'pro_ID', width: 80, align: 'center', formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "action": "ProValueandAlltList", "type": "noing", "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        caption: "分配中项目",
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
}

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}
//详细
function colStrShowFormatter(celvalue, options, rowData) {
    var auditString = '<a href="../ProjectValueandAllot/ProjectValueAllotDetailsBymaster.aspx?proid=' + celvalue + '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year option:selected").val() + '" class="cls_chk" style="height: 26px;">查看</a>';

    return auditString;
}
//进度
function colStatusShowFormatter(celvalue, options, rowData) {
    var str = '<div class="progressbar" style="cursor: pointer;height:20px;margin:1px 1px;" percent="' + celvalue + '" title="' + celvalue + '%"></div>';

    return str;
}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var linkAction = "";
    if ($("#hid_isFlag").val() == "true") {
        linkAction = "<span style=\"cursor:pointer;color:blue;\" id=\"InitiatedAudit\" proSysNo=\"" + celvalue + "\"  proName=\"" + rowData["pro_name"] + "\">发起分配</span>";
    }
    else {
        linkAction = "---";
    }
    return linkAction;
}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));
    }

    var sumAcount = parseFloat($("#jqGrid").getCol("Cpr_Acount", false, 'sum')).toFixed(4);
    var allPayShiCount = parseFloat($("#jqGrid").getCol("PayShiCount", false, 'sum')).toFixed(4);
    var allAllotCount = parseFloat($("#jqGrid").getCol("AllotCount", false, 'sum')).toFixed(4);
    $("#jqGrid").footerData('set', { pro_name: "合计:", Cpr_Acount: sumAcount, PayShiCount: allPayShiCount, AllotCount: allAllotCount }, false);


    //进度条
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("percent"));
        $(item).progressbar({
            value: percent
        });
    });
}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}


//已分配完成
function AllotList2() {
    $("#jqGrid2").jqGrid({
        url: '/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '',  '项目名称', '承接部门', '建筑类别', '合同额(万元)', '实收总额(万元)', '分配金额(万元)', '详细'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 250, formatter: colNameShowFormatter },
                            
                             { name: 'Unit', index: 'Unit', width: 80, align: 'center' },
                             { name: 'BuildType', index: 'BuildType', width: 80, align: 'center' },
                             { name: 'Cpr_Acount', index: 'cpr_Acount', width: 90, align: 'center' },
                             { name: 'PayShiCount', index: 'PayShiCount', width: 110, align: 'center' },
                             { name: 'AllotCount', index: 'Allotcount', width: 110, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', width: 60, align: 'center', formatter: colShowFormatter2 }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "ProValueandAlltList", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "type": "yes", "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager2",
        viewrecords: true,
        caption: "分配完毕项目",
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod2,
        loadComplete: loadCompMethod2
    });


    //显示查询
    $("#jqGrid2").jqGrid("navGrid", "#gridpager2", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid2").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid2').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid2').jqGrid('getCell', sel_id, 'pro_ID');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectValueandAllot/ProjectValueAllotListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
}

//名称连接
function colNameShowFormatter2(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';
}
////静态状态
//function colStrShowFormatter2(celvalue, options, rowData) {
//    var percent = 0;
//    var count = 3;
//    switch (rowData["ProjectPlanAuditStatus"]) {
//        case "A":
//            percent = 0;
//            break;
//        case "B":
//        case "C":
//            percent = (100 / count) * 1;
//            break;
//        case "D":
//        case "E":
//            percent = (100 / count) * 2;
//            break;
//        case "F":
//        case "G":
//            percent = (100 / count) * 3;
//            break;
//        case "H":
//        case "I":
//            percent = 100;
//            break;
//    }
//    percent = parseFloat(percent).toFixed(2);
//    var pageurl = '<div class="progressbar" rel="' + percent + '" title="' + percent + '%" id="auditLocusContainer" action="PN" referencesysno="' + rowData["ProjectPlanAuditSysNo"] + '" style="cursor: pointer;height:20px;margin:1px 1px;"></div>';

//    return pageurl;
//}
//查看
function colShowFormatter2(celvalue, options, rowData) {
    var linkAction = '<a href="/ProjectValueandAllot/ProjectValueAllotDetailsBymaster.aspx?proid=' + celvalue + '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year option:selected").val() + '"  class="cls_chk" style="height: 26px;">查看</a>';
    return linkAction;
}

//统计 
function completeMethod2() {
    var rowIds = $("#jqGrid2").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid2 [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));
    }

    var sumAcount = parseFloat($("#jqGrid2").getCol("Cpr_Acount", false, 'sum')).toFixed(4);
    var allPayShiCount = parseFloat($("#jqGrid2").getCol("PayShiCount", false, 'sum')).toFixed(4);
    var allAllotCount = parseFloat($("#jqGrid2").getCol("AllotCount", false, 'sum')).toFixed(4);
    $("#jqGrid2").footerData('set', { pro_name: "合计:", Cpr_Acount: sumAcount, PayShiCount: allPayShiCount, AllotCount: allAllotCount }, false);


}
//无数据
function loadCompMethod2() {
    $("#jqGrid2").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid2").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata2").text() == '') {
            $("#jqGrid2").parent().append("<div id='nodata2'>没有符合条件数据！</div>")
        }
        else { $("#nodata2").show(); }
    }
    else {
        $("#nodata2").hide();
    }
}