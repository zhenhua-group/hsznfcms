﻿
var saveDesigner = function () {
    //初始
    var pageInit = function () {
        //查看
        if ($("#hidaction").val() == "ck") {
            $("input").attr("disabled", true);
            $("#btnAuditSave").hide();
            $("#btnDesignSave").hide();
        }
        //审核总金额
        var allAuditCount = parseFloat($("#divAllotCount").text());
        var auditDone = parseFloat($("#divAllotDone").text());
        var auditHav = parseFloat($("#divAllotHav").text());
        //设计总金额
        var allDesignerCount = parseFloat($("#divDesigncount").text());
        var designerDone = parseFloat($("#divDesignDone").text());
        var designerHav = parseFloat($("#divDesignHav").text());
        //获得焦点
        $(".clsAllotCount").focus(function () {
            var val = $(this).val();
            if (val == "0") {
                $(this).val("");
            }
        });
        //失去焦点
        $(".clsAllotCount").blur(function () {
            //类型
            var type = $(this).attr("allottype");
            //判断为空
            if ($(this).val() == "") {
                $(this).val("0");
            }
            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            //输入数字
            if (!reg.test($(this).val())) {
                alert("分配金额请输入数字！");
                $(this).val("0");
                $(this).focus();
                return false;
            }

            //审核
            if (type == "1") {
                //审核费
                auditDone = 0;
                auditHav = 0;

                //输入数字
                $(".clsAllotCount[allottype='1']").each(function (i) {

                    var allotval = parseFloat($(this).val());
                    if (allotval > 0) {
                        //已分配
                        auditDone = auditDone + allotval;
                        //余额
                        auditHav = allAuditCount - auditDone;
                    }
                });
                //分配完成
                if (auditDone > allAuditCount) {
                    alert("已分配金额大于分配总金额！");
                    var yu = auditDone - allAuditCount;
                    $(this).val(parseFloat($(this).val()) - yu);
                    auditDone = auditDone - yu;
                    auditHav = auditHav + yu;
                }
                //百分比
                var allotprt = 0;
                if (parseFloat($(this).val()) > 0) {
                    allotprt = ((parseFloat($(this).val()) / allAuditCount) * 100).toFixed(4);
                }
                //分配金额
                $(this).parent().next("td").find("span").text(allotprt);

                $("#divAllotDone").text(auditDone);
                $("#divAllotHav").text(auditHav);

            }
            else {
                //设计费
                designerDone = 0;
                designerHav = 0;

                $(".clsAllotCount[allottype='2']").each(function (i) {

                    var allotval = parseFloat($(this).val());
                    if (allotval > 0) {
                        //已分配
                        designerDone = designerDone + allotval;
                        //余额
                        designerHav = allDesignerCount - designerDone;
                    }
                });
                //分配完成
                if (designerDone > allDesignerCount) {
                    alert("已分配金额大于分配总金额！");
                    var yu = designerDone - allDesignerCount;
                    $(this).val(parseFloat($(this).val()) - yu);
                    designerDone = designerDone - yu;
                    designerHav = designerHav + yu;
                }

                //百分比
                var allotprt = 0;
                if (parseFloat($(this).val()) > 0) {
                    allotprt = ((parseFloat($(this).val()) / allDesignerCount) * 100).toFixed(4);
                }
                //百分比
                $(this).parent().next("td").find("span").text(allotprt);

                //分配金额
                $("#divDesignDone").text(designerDone);
                $("#divDesignHav").text(designerHav);

            }

        });
    }
    //按钮时间
    var submitData = function () {
        //审核提交
        $("#btnAuditSave").click(function () {
            $(this).attr("disabled", true);
            $("input[allottype='1']").attr("disabled", true);
            //提交确认
            if (confirm("确认提交分配吗？")) {
                //审核数据
                var AllotData = {};

                AllotData.ProID = $("#hidProjectSysNo").val();
                AllotData.AllotYear = $("#hidAllotyear").val();
                AllotData.AllotType = "1";
                AllotData.InserUser = $("#hidUserID").val();
                AllotData.SpeID = $("#hidspeid").val();
                AllotData.AllotItems = new Array();


                $("#auditBody tr").each(function (i) {
                    var obj = $(this);
                    AllotData.AllotItems[i] =
                        {
                            "UserID": obj.find("span:eq(0)").attr("usersysno"),
                            "UserName": obj.find("span:eq(0)").text(),
                            "AllotPrt": obj.find("span:eq(1)").text(),
                            "AllotMoney": obj.find("input").val()
                        }
                });

                //
                var data = JSON.stringify(AllotData);
                var url = "/HttpHandler/ProjectValueandAllot/SaveAllotHandler.ashx";
                $.post(url, { "data": data }, function (result) {
                    if (AllotData.SpeID == "1") {
                        alert("建筑审核费分配成功！");
                    }
                    else
                    {
                        alert("结构审核费分配成功！");
                    }
                });
            }
        });
        //设计提交
        $("#btnDesignSave").click(function () {
            $(this).attr("disabled", true);
            $("input[allottype='2']").attr("disabled", true);
            //提交确认
            if (confirm("确认提交分配吗？")) {
                //审核数据
                var AllotData = {};


                AllotData.ProID = $("#hidProjectSysNo").val();
                AllotData.AllotYear = $("#hidAllotyear").val();
                AllotData.AllotType = "2";
                AllotData.InserUser = $("#hidUserID").val();
                AllotData.SpeID = $("#hidspeid").val();
                AllotData.AllotItems = new Array();

                $("#designerBody tr").each(function (i) {
                    var obj = $(this);
                    AllotData.AllotItems[i] =
                        {
                            "UserID": obj.find("span:eq(0)").attr("usersysno"),
                            "UserName": obj.find("span:eq(0)").text(),
                            "AllotPrt": obj.find("span:eq(1)").text(),
                            "AllotMoney": obj.find("input").val()
                        }
                });

                var data = JSON.stringify(AllotData);
                var url = "/HttpHandler/ProjectValueandAllot/SaveAllotHandler.ashx";
                $.post(url, { "data": data }, function (result) {
                    if (AllotData.SpeID == "1") {
                        alert("建筑设计费分配成功！");
                    }
                    else {
                        alert("结构设计费分配成功！");
                    }
                });
            }
        });
    }
    //配置
    return {
        init: function () {
            pageInit();
            submitData();
        }
    }
}();