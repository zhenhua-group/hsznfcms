﻿var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
$(function () {

    CommonControl.SetFormWidth();

    $("#CoperationBaseInfo tr:odd").css({ background: "White" });

    // 设置文本框样式
    CommonControl.SetTextBoxStyle();

    //设计阶段
    $("#ctl00_ContentPlaceHolder1_stageIsNotTranJjs").change(function () {
        if ($(this).val() == "25") {
            $("#tbInfo tr:eq(0)").show();
        } else {
            $("#tbInfo tr:eq(0)").hide();
        }
    });

    if ($("#ctl00_ContentPlaceHolder1_stageIsNotTranJjs").val() == "25") {
        $("#tbInfo tr:eq(0)").show();
    }
    else {
        $("#tbInfo tr:eq(0)").hide();
    }
    var isRounding = $("#HiddenIsRounding").val();


    //合同ID
    var cprId = $("#HiddenCprID").val();

    //年份变化
    $("#ctl00_ContentPlaceHolder1_drp_year").change(function () {

        var year = $("#ctl00_ContentPlaceHolder1_drp_year").children("option:selected").val();

        $.post("/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx", { "Action": "4", "year": year, "cprId": cprId, "pro_id": $("#HiddenProSysNo").val() }, function (jsonResult) {

            var jsonData = eval("(" + jsonResult + ")");
            var payshiAcount = jsonData.payShiCount;
            $("#ctl00_ContentPlaceHolder1_lblPayShiCount").text(payshiAcount);
            // var allotCount = $("#ctl00_ContentPlaceHolder1_lblAllotAccount").text();
            var allotCount = jsonData.allountAcount;
            $("#ctl00_ContentPlaceHolder1_lblAllotAccount").text(allotCount);
            var notallotCount = parseFloat(payshiAcount) - parseFloat(allotCount);
            $("#ctl00_ContentPlaceHolder1_lblNotAllotAccount").text(notallotCount.toFixed(2));

            CalculationValue();

        });
    });

    //本部门自留
    $("#txtTheDeptValuePercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        if (parseFloat($("#txtTheDeptValuePercent").val()) > 100) {
            alert("本部门自留产值比例不能大于100%！");
            $(this).val(0);
        }

        CalculationValue();
        return false;
    });

    //方案
    $("#txt_ProgramPercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }


        if (parseFloat($("#txt_ProgramPercent").val()) > 100) {
            alert("方案比例不能大于100%！");
            $(this).val(0);
        }

        CalculationValue();
        return false;
    });


    //转土建
    $("#txtTranBulidingPercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }


        if (parseFloat($("#txtTranBulidingPercent").val()) > 100) {
            alert("转土建产值比例不能大于100%！");
            $(this).val(0);
        }

        CalculationValue();
        return false;
    });
    //项目总负责
    $("#txt_DesignManagerPercent").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        if (parseFloat($("#txt_DesignManagerPercent").val()) > 100) {
            alert("设计总负责比例不能大于100%！");
            $(this).val(0);
        }

        CalculationValue();
        return false;
    });

    //分配金额
    $("#txt_AllotAccount").change(function () {

        if (!SelectYear()) {
            return false;
        }

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        var allotAccount = $("#txt_AllotAccount").val().length == 0 ? 0 : parseFloat($("#txt_AllotAccount").val());

        if (parseFloat(allotAccount) > parseFloat($("#ctl00_ContentPlaceHolder1_lblNotAllotAccount").text())) {
            alert("请确认分配金额不超过未分配金额！");
            $(this).val(0);
        }

        CalculationValue();
        return false;
    });



    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    messageDialog = $("#auditShow").messageDialog;
    sendMessageClass = new MessageCommon(messageDialog);
    //保存按钮--保存信息
    $("#btnApproval").click(function () {

        if (!SelectYear()) {
            return false;
        }

        if ($.trim($("#txt_DividedPercent").val()).length == 0) {
            alert("请选择院所分成比例！");
            return false;
        }

        if ($("#ctl00_ContentPlaceHolder1_stageIsNotTranJjs").val() == "-1") {
            alert("请选择项目分配阶段！");
            return false;
        }
        //分配金额
        if (!reg.test($("#txt_AllotAccount").val())) {
            this.focus();
            alert("分配金额请输入数字！");
            return false;
        }

        if ($.trim($("#txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
            return false;
        }
        var allotAccount = $("#txt_AllotAccount").val().length == 0 ? 0 : parseFloat($("#txt_AllotAccount").val());

        if (parseFloat(allotAccount) > parseFloat($("#ctl00_ContentPlaceHolder1_lblNotAllotAccount").text())) {
            alert("请确认分配金额不超过未分配金额！");
            return false;
        }

        if (!reg.test($("#txt_DesignManagerPercent").val())) {
            alert("项目总负责比例请输入数字！");
            return false;
        }

        if ($.trim($("#txt_DesignManagerPercent").val()).length == 0) {
            alert("项目总负责比例不能为空！");
            return false;
        }

        if (parseFloat($("#txt_DesignManagerPercent").val()) > 100) {
            alert("项目总负责比例不能大于100%！");
            return false;
        }
        if ($("#ctl00_ContentPlaceHolder1_stageIsNotTranJjs").val() == "25") {

            if ($("#drp_unit").val() == "-1") {
                alert("请选择转土建产值部门信息！");
                return false;
            }
            //本部门自留产值
            if (!reg.test($("#txtTranBulidingPercent").val())) {
                alert("转土建产值比例请输入数字！");
                return false;
            }

            if ($.trim($("#txtTranBulidingPercent").val()).length == 0) {
                alert("转土建产值不能为空！");
                return false;
            }
            if (parseFloat($("#txtTranBulidingPercent").val()) > 100) {
                alert("转土建产值比例不能大于100%！");
                return false;
            }
        }
        //本部门自留产值
        if (!reg.test($("#txtTheDeptValuePercent").val())) {
            alert("本部门自留产值比例请输入数字！");
            return false;
        }

        if ($.trim($("#txtTheDeptValuePercent").val()).length == 0) {
            alert("本部门自留产值不能为空！");
            return false;
        }
        if (parseFloat($("#txtTheDeptValuePercent").val()) > 100) {
            alert("本部门自留产值比例不能大于100%！");
            return false;
        }

        if (!reg.test($("#txt_ProgramPercent").val())) {
            alert("方案比例请输入数字！");
            return false;
        }

        if ($.trim($("#txt_ProgramPercent").val()).length == 0) {
            alert("方案比例不能为空！");
            return false;
        }

        if (parseFloat($("#txt_ProgramPercent").val()) > 100) {
            alert("方案比例不能大于100%！");
            return false;
        }

        if (parseFloat($("#txt_ProgramCount").text()) > 5000) {
            alert("方案上限费用为5000，请修改！");
            return false;
        }

        if (parseFloat($("#txt_ShouldBeValuePercent").val()) < 0) {
            alert("应分产值比例不能小于零！");
            return false;
        }

        var ViewEntity = {
            "pro_ID": $("#HiddenProSysNo").val(),
            "AuditSysID": 0,
            "AllotCount": $("#txt_AllotAccount").val(),
            "AllotUser": $("#HiddenLoginUser").val(),
            "Payshicount": $("#ctl00_ContentPlaceHolder1_lblPayShiCount").text(),
            "PaidValuePercent": $("#txt_PaidValuePercent").val(),
            "PaidValueCount": $("#txt_PaidValueCount").text(),
            "DesignManagerPercent": $("#txt_DesignManagerPercent").val(),
            "DesignManagerCount": $("#txt_DesignManagerCount").text(),
            "Itemtype": $("#ctl00_ContentPlaceHolder1_stageIsNotTranJjs").val(),
            "Thedeptallotpercent": $("#txtTheDeptValuePercent").val(),
            "Thedeptallotcount": $("#txtTheDeptValueCount").text(),
            "ProgramPercent": $("#txt_ProgramPercent").val(),
            "ProgramCount": $("#txt_ProgramCount").text(),
            "ShouldBeValuePercent": $("#txt_ShouldBeValuePercent").val(),
            "ShouldBeValueCount": $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text(),
            "SecondValue": "1",
            "AllotValuePercent": 0,
            "Allotvaluecount": 0,
            "UnitValuePercent": 0,
            "UnitValueCount": 0,
            "EconomyValuePercent": 0,
            "EconomyValueCount": 0,
            "Otherdeptallotpercent": 0,
            "Otherdeptallotcount": 0,
            "IsTrunEconomy": 0,
            "IsTrunHavc": 0,
            "HavcValuePercent": 0,
            "HavcValueCount": 0,
            "TranBulidingPercent": $("#txtTranBulidingPercent").val() == "" ? 0 : $("#txtTranBulidingPercent").val(),
            "TranBulidingCount": $("#TranBulidgingCount").text() == "" ? 0 : $("#TranBulidgingCount").text(),
            "UnitId": $("#drp_unit").val() == "" ? 0 : $("#drp_unit").val(),
            "ActualAllountTime": $("#ctl00_ContentPlaceHolder1_drp_year").val(),
            "FinanceValuePercent": $("#txt_FinanceValuePercent").val(),
            "FinanceValueCount": $("#txt_FinanceValueCount").text(),
            "TheDeptShouldValuePercent": $("#txt_TheDeptShouldValuePercent").val(),
            "TheDeptShouldValueCount": $("#txt_TheDeptShouldValueCount").text(),
            "DividedPercent": $("#txt_DividedPercent").val()
        };

        $("#btnApproval").hide();
        var jsonObj = Global.toJSON(ViewEntity);


        var jsonObj = Global.toJSON(ViewEntity);
        jsonDataEntity = jsonObj;

        //申请分配
        getUserAndUpdateAudit('1', '0', jsonDataEntity);
    });



    //    messageDialog = $("#msgReceiverContainer").messageDialog({
    //        "button": {
    //            "发送消息": function () {
    //                //选中用户
    //                var _$mesUser = $(":checkbox[name=messageUser]:checked");

    //                if (_$mesUser.length == 0) {
    //                    alert("请至少选择一个流程审批人！");
    //                    return false;
    //                }

    //                getUserAndUpdateAudit('1', '1', jsonDataEntity);
    //            },
    //            "关闭": function () {
    //                $("#btnApproval").attr("disabled", false);
    //                messageDialog.hide();
    //            }
    //        }
    //    });
    $("#btn_Send").click(function () {
        //选中用户
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        }
        $(this).attr("disabled", "disabled");
        getUserAndUpdateAudit('1', '1', jsonDataEntity);

    });
    //关闭
    $("#btn_close").click(function () {
        $("#btnApproval").show();
    });
    $("#sch_reletive").live("click", function () {
        getDivided();
    });
    $("#btn_search").live("click", function () {
        getDivided();
    });
    $("span[id=spanSelect]").live("click", function () {
        var percent = $(this).attr("dividePercent");
        $("#txt_DividedPercent").val(percent);
        $("#div_Divided").modal("hide");
        CalculationValue();
    });
    //    sendMessageClass = new MessageCommon(messageDialog); 
    $("#btnRefuse").click(function () {
        window.history.back();
    });
});

//计算产值
function CalculationValue() {

    //分配金额

    //if ($("#ctl00_ContentPlaceHolder1_stageIsNotTranJjs").val() == "-1") {
    //    alert("请选择项目分配阶段！");
    //    return false;
    //}
    //if ($.trim($("#txt_AllotAccount").val()).length == 0) {
    //    alert("分配金额不能为空！");
    //    return false;
    //}
    var allotAccount = 0;
    allotAccount = $("#txt_AllotAccount").val().length == 0 ? 0 : parseFloat($("#txt_AllotAccount").val());

    var tranBulidingPercent = $("#txtTranBulidingPercent").val().length == 0 ? 0 : parseFloat($("#txtTranBulidingPercent").val());
    var tranBulidingCount = parseFloat(tranBulidingPercent) * parseFloat(allotAccount) * 100;
    $("#TranBulidgingCount").text(tranBulidingCount.toFixed(0));

    var shiAccount = parseFloat(allotAccount * 10000) - parseFloat(tranBulidingCount);

    //实收产值
    var paidValuePercent = $("#txt_PaidValuePercent").val().length == 0 ? 0 : parseFloat($("#txt_PaidValuePercent").val());
    var paidValueCount = parseFloat(paidValuePercent) * parseFloat(shiAccount) / 100;
    $("#txt_PaidValueCount").text(paidValueCount.toFixed(0));

    //本部门自留
    var theDeptValuePercent = $("#txtTheDeptValuePercent").val().length == 0 ? 0 : parseFloat($("#txtTheDeptValuePercent").val());
    //本部门自留产值
    var theDeptValueCount = parseFloat(theDeptValuePercent) * parseFloat(shiAccount) / 100;
    $("#txtTheDeptValueCount").text(theDeptValueCount.toFixed(0));

    //方案金额
    var programValuePercent = $("#txt_ProgramPercent").val().length == 0 ? 0 : parseFloat($("#txt_ProgramPercent").val());
    //方案金额
    var programValueCount = (parseFloat(shiAccount) - parseFloat(theDeptValueCount)) * programValuePercent / 100;

    //    var programValueCount = 0;
    //    if (parseFloat(programValueCountTemp) > 5000) {
    //        programValueCount = 5000;
    //    }
    //    else {
    //        programValueCount = programValueCountTemp;
    //    }
    $("#txt_ProgramCount").text(programValueCount.toFixed(2));

    //设总
    var designManagerPercent = $("#txt_DesignManagerPercent").val().length == 0 ? 0 : parseFloat($("#txt_DesignManagerPercent").val());
    //设总金额
    var DesignManagerCount = (parseFloat(shiAccount) - parseFloat(theDeptValueCount) - parseFloat(programValueCount)) * parseFloat(designManagerPercent) / 100;
    $("#txt_DesignManagerCount").text(DesignManagerCount.toFixed(0));

    // 应分产值比例 
    //应分产值金额 等于分配产值 -转经济所-转其他部门-转暖通-设总-本部门自留产值-方案
    var shouleBeValueCount = parseFloat(shiAccount) - parseFloat(programValueCount) - parseFloat(DesignManagerCount) - parseFloat(theDeptValueCount);
    $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text(shouleBeValueCount.toFixed(0));
    if (allotAccount == 0) {
        $("#txt_ShouldBeValuePercent").attr("value", "0.00");
    }
    else {
        $("#txt_ShouldBeValuePercent").attr("value", (shouleBeValueCount / allotAccount / 100).toFixed(2));
    }

    //财务统计产值
    var financeValueCount = parseFloat(allotAccount * 10000) - parseFloat(tranBulidingCount);
    $("#txt_FinanceValueCount").text(financeValueCount.toFixed(0));

    var financeValuePercent = 0;
    if (parseFloat(financeValueCount) == 0) {
        financeValuePercent = 0;
    } else {
        financeValuePercent = parseFloat(financeValueCount) / (parseFloat(allotAccount) * 100);
    }
    $("#txt_FinanceValuePercent").attr("value", financeValuePercent.toFixed(2));
    //12 部分应留产值 
    //系数
    var dividedPercent = $("#txt_DividedPercent").val().length == 0 ? 0 : parseFloat($("#txt_DividedPercent").val());
    var theDeptShouldValueCount = financeValueCount * parseFloat(dividedPercent);
    $("#txt_TheDeptShouldValueCount").text(theDeptShouldValueCount.toFixed(0));

    var theDeptShouldValuePercent = 0;
    if (parseFloat(theDeptShouldValueCount) == 0) {
        theDeptShouldValuePercent = 0;
    } else {
        theDeptShouldValuePercent = parseFloat(theDeptShouldValueCount) / (parseFloat(allotAccount) * 100);
    }
    $("#txt_TheDeptShouldValuePercent").attr("value", theDeptShouldValuePercent.toFixed(2));
}

//浮点数
function checkRate(value) {
    var re = /^[0-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+$/;
    if (re.test(value)) {
        return true;
    }
}



//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    $.post(url, data, function (jsonResult) {
        if (flag == "1") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
        }
        if (jsonResult == "0") {
            alert("发起分配错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("产值分配完成，已全部通过！");
            //查询系统新消息
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}

//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}

//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {

        alert("消息发送成功,等待审核人审批！");
        //查询系统新消息
        window.location.href = "/ProjectValueandAllot/EconomyProjectValueAllotListBymaster.aspx";
    } else {
        alert("消息发送失败！");
    }
}

//选择年份
function SelectYear() {

    var flag = true;
    if ($("#ctl00_ContentPlaceHolder1_drp_year").val() == "-1") {
        alert("请选择分配年份");
        flag = false;
        return false;
    }
    return flag;
}


function getDivided() {
    var year = $("#ctl00_ContentPlaceHolder1_drp_year1").children("option:selected").val();
    var unitName = $.trim($("#ctl00_ContentPlaceHolder1_lblcpr_Unit").text());
    $.post("/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx", { "Action": "7", "year": year, "unitName": unitName }, function (jsonResult) {
        $("#tb_Divided tr:gt(0)").remove();
        if (jsonResult != null && jsonResult != "") {
            var json = eval('(' + jsonResult + ')');
            var data = json == null ? "" : json.ds;
            if (data != null && data != "") {
                $.each(data, function (i, n) {
                    var trString = "<tr><td>" + n.unit_Name + "</td>";
                    trString += "<td >" + n.DivideYear + "</td>";
                    trString += "<td >" + n.DividePercent + "</td>";
                    trString += "<td><span style=\"color:blue;cursor:pointer;\" id=\"spanSelect\"  dividePercent=" + n.DividePercent + ">选择</span></td>";
                    trString += "</tr>";
                    $("#tb_Divided").append(trString);
                });
            }
        } else {
            var trString = "<tr style='color:Red; text-align:center;'><td colspan=4>没有对应的院所分成比例，请到对应页面添加!</td></tr>";
            $("#tb_Divided").append(trString);
        }
    });

}