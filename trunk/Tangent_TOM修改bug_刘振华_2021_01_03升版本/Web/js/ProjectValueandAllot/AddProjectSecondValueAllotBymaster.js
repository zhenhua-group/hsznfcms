﻿var auditRecordLength = 2;
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
var isTrunEconomy;
var isTrunHavc;
$(function () {


    //    $("#CoperationBaseInfo tr:odd").css({ background: "White" });

    // 设置文本框样式

    var cprProcess = $("#HiddenCoperationProcess").val();

    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;


    //合同ID
    var unitName = $("#HiddenUnitName").val();

    //年份变化
    $("#ctl00_ContentPlaceHolder1_drp_year").change(function () {

        var year = $("#ctl00_ContentPlaceHolder1_drp_year").children("option:selected").val();

        $.post("/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx", { "Action": "5", "year": year, "unitName": unitName }, function (jsonResult) {

            var json = eval('(' + jsonResult + ')');
            var data = json == null ? "" : json.ds;
            if (data != null && data != "") {
                $.each(data, function (i, n) {
                    var totalCount = n.totalCount;
                    var allountCount = n.allountCount;
                    var tranAllotCount = n.TranAllotCount;
                    $("#ctl00_ContentPlaceHolder1_lblTotalCount").text(totalCount);
                    $("#ctl00_ContentPlaceHolder1_lblAllotAccount").text(allountCount);
                    var ProgramCount = n.ProgramCount;
                    var notAllotCount = parseFloat(totalCount) + parseFloat(tranAllotCount) - parseFloat(allountCount) - parseFloat(ProgramCount);
                    $("#ctl00_ContentPlaceHolder1_lblNotAllotAccount").text(notAllotCount.toFixed(2));
                });
            } else {
                $("#ctl00_ContentPlaceHolder1_lblTotalCount").text("0");
                $("#ctl00_ContentPlaceHolder1_lblAllotAccount").text("0");
                $("#ctl00_ContentPlaceHolder1_lblNotAllotAccount").text("0");
            }

        });
    });
    //实例化类容
    messageDialog = $("#auditShow").messageDialog;
    sendMessageClass = new MessageCommon(messageDialog);
    //审核通过按钮
    $("#btnApproval").click(function () {

        if (!SelectYear()) {
            return false;
        }

        //分配金额
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val())) {
            this.focus();
            alert("分配金额请输入数字！");
            return false;
        }

        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val()).length == 0) {
            alert("分配金额不能为空！");
            return false;
        }
        var allotAccount = $("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val().length == 0 ? 0 : parseFloat($("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val());

        if (parseFloat(allotAccount) > parseFloat($("#ctl00_ContentPlaceHolder1_lblNotAllotAccount").text())) {
            alert("请确认分配金额不超过未分配金额！");
            return false;
        }

        if (parseFloat(allotAccount) <= 0) {
            alert("分配金额不能小于等于0！");
            return false;
        }

        if ($("#stage").val() == "-1") {
            alert("请选择项目分配阶段！");
            return false;
        }
        if ($("#drp_buildtype").val() == "-1") {
            alert("请选择项目等级！");
            return false;
        }

        var ViewEntity = {
            "pro_ID": $("#HiddenProSysNo").val(),
            "AuditSysID": $("#HiddenAuditRecordSysNo").val(),
            "AllotCount": $("#ctl00_ContentPlaceHolder1_txt_AllotAccount").val(),
            "AllotUser": $("#HiddenLoginUser").val(),
            "Itemtype": $("#stage").val(),
            "SecondValue": "2",
            "ActualAllountTime": $("#ctl00_ContentPlaceHolder1_drp_year").val(),
            "BulidType": $("#ctl00_ContentPlaceHolder1_drp_buildtype :selected").text()
        };

        //        $("#btnApproval").attr("disabled", true);

        var jsonObj = Global.toJSON(ViewEntity);
        jsonDataEntity = jsonObj;
        $("#btnApproval").hide();
        //申请分配
        getUserAndUpdateAudit('2', '0', jsonDataEntity);
    });


    //审核不通过按钮，具体流程业务，基本同上面的审核通过一致
    $("#btnRefuse").click(function () {
                window.location.href = "/ProjectValueandAllot/ProjectSecondValueListBymaster.aspx";
        
    });
    $("#btn_Send").click(function () {

        //选中用户
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        }
        $(this).attr("disabled", "disabled");
        getUserAndUpdateAudit('2', '1', jsonDataEntity);
    });
    //关闭
    $("#btn_close").click(function () {
        $("#btnApproval").show();
    });

});

//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectValueandAllot/AddProjectValueAllotHandler.ashx";
    var stage = $("#stage").val();
    var mark = "";
    if ($.trim(stage) == "5") {
        var re = /\([^\)]+\)/g
        mark = $("#stage option:selected").text();
        mark = mark.match(re)[0];
        mark = mark.substring(1, mark.length - 1);
    }

    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData, "mark": mark };
    //提交数据
    $.post(url, data, function (jsonResult) {
        if (flag == "1") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
        }
        if (jsonResult == "0") {
            alert("发起分配错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("产值分配完成，已全部通过！");
            //查询系统新消息
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}

//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}

//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        alert("消息发送成功,等待下一阶段审核人审批！");
        //查询系统新消息
        window.location.href = "/ProjectValueandAllot/ProjectSecondValueListBymaster.aspx";
    } else {
        alert("消息发送失败！");
    }
}


//选择年份
function SelectYear() {

    var flag = true;
    if ($("#ctl00_ContentPlaceHolder1_drp_year").val() == "-1") {
        alert("请选择分配年份");
        flag = false;
        return false;
    }
    return flag;
}




