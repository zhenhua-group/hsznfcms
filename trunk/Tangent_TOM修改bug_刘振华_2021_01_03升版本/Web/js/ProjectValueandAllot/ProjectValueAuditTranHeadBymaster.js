﻿var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
$(function () {

    CommonControl.SetFormWidth();

    $("#CoperationBaseInfo tr:odd").css({ background: "White" });

    setStatus();

    // 设置文本框样式
    CommonControl.SetTextBoxStyle();

    if ($("#HiddenAllotID").val() == "") {
        $("#tbTwoSave").hide();
        $("#memField").show();

    } else {
        $("#ctl00_ContentPlaceHolder1_stage").attr("disabled", "disabled");
        $("input", "#fidOne").attr("disabled", "disabled");
        $("#btnApprovalOne").attr("disabled", "disabled");
        $("#ctl00_ContentPlaceHolder1_drp_year").attr("disabled", "disabled");
        $("#txtTheDeptValuePercent").attr("disabled", "disabled");
        $("#txt_ProgramPercent").attr("disabled", "disabled");
        $("#txt_DesignManagerPercent").attr("disabled", "disabled");
        if ($("#gvProjectValueProcess tr td").text() == "请确定策划人员是否添加’建筑‘专业的人员。或者安装专业的人员信息。") {
            $("#btnApprovalTwo").attr("disabled", "disabled");
        }
    }

    var isRounding = $("#HiddenIsRounding").val();

    //本部门自留
    $("#txtTheDeptValuePercent").change(function () {

        if (!reg.test($("#txtTheDeptValuePercent").val())) {
            alert("本部门自留产值比例请输入数字！");
            $(this).val("0");
            //            return false;
        }

        if (parseFloat($("#txtTheDeptValuePercent").val()) > 100) {
            alert("本部门自留产值比例不能大于100%！");
            $(this).val("0");
            //            return false;
        }

        CalculationValue();
        return false;
    });

    //方案
    $("#txt_ProgramPercent").change(function () {

        if (!reg.test($("#txt_ProgramPercent").val())) {
            alert("方案比例请输入数字！");
            $(this).val("0");
            //            return false;
        }

        if (parseFloat($("#txt_ProgramPercent").val()) > 100) {
            alert("方案比例不能大于100%！");
            $(this).val("0");
            //            return false;
        }

        CalculationValue();
        return false;
    });

    //项目总负责
    $("#txt_DesignManagerPercent").change(function () {

        if (!reg.test($("#txt_DesignManagerPercent").val())) {
            alert("设计总负责比例请输入数字！");
            $(this).val("0");
            //            return false;
        }


        if (parseFloat($("#txt_DesignManagerPercent").val()) > 100) {
            alert("设计总负责比例不能大于100%！");
            $(this).val("0");
            //            return false;
        }

        CalculationValue();
        return false;
    });


    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

    //保存按钮--保存信息
    $("#btnApprovalOne").click(function () {

        if ($("#ctl00_ContentPlaceHolder1_drp_year").val() == "-1") {
            alert("请选择项目分配年份！");
            return false;
        }

        if ($("#ctl00_ContentPlaceHolder1_stage").val() == "-1") {
            alert("请选择项目分配阶段！");
            return false;
        }

        if (!reg.test($("#txt_DesignManagerPercent").val())) {
            alert("项目总负责比例请输入数字！");
            return false;
        }

        if ($.trim($("#txt_DesignManagerPercent").val()).length == 0) {
            alert("项目总负责比例不能为空！");
            return false;
        }

        if (parseFloat($("#txt_DesignManagerPercent").val()) > 100) {
            alert("项目总负责比例不能大于100%！");
            return false;
        }

        //本部门自留产值
        if (!reg.test($("#txtTheDeptValuePercent").val())) {
            alert("本部门自留产值比例请输入数字！");
            return false;
        }

        if ($.trim($("#txtTheDeptValuePercent").val()).length == 0) {
            alert("本部门自留产值不能为空！");
            return false;
        }
        if (parseFloat($("#txtTheDeptValuePercent").val()) > 100) {
            alert("本部门自留产值比例不能大于100%！");
            return false;
        }

        if (!reg.test($("#txt_ProgramPercent").val())) {
            alert("方案比例请输入数字！");
            return false;
        }

        if ($.trim($("#txt_ProgramPercent").val()).length == 0) {
            alert("方案比例不能为空！");
            return false;
        }

        if (parseFloat($("#txt_ProgramPercent").val()) > 100) {
            alert("方案比例不能大于100%！");
            return false;
        }

        if (parseFloat($("#txt_ProgramCount").text()) > 5000) {
            alert("方案上限费用为5000，请修改！");
            return false;
        }

        if (parseFloat($("#txt_ShouldBeValuePercent").val()) < 0) {
            alert("应分产值比例不能为负数！");
            return false;
        }

        var ViewEntity = {
            "Pro_ID": $("#HiddenProSysNo").val(),
            "AllotID": $("#HiddenTranAllotID").val(),
            "AllotCount": $("#ctl00_ContentPlaceHolder1_lblTotalCount").text(),
            "AllotUser": $("#HiddenLoginUser").val(),
            "PaidValuePercent": $("#txt_PaidValuePercent").val(),
            "PaidValueCount": $("#ctl00_ContentPlaceHolder1_txt_PaidValueCount").text(),
            "DesignManagerPercent": $("#txt_DesignManagerPercent").val(),
            "DesignManagerCount": $("#txt_DesignManagerCount").text(),
            "ItemType": $("#ctl00_ContentPlaceHolder1_stage").val(),
            "Thedeptallotpercent": $("#txtTheDeptValuePercent").val(),
            "Thedeptallotcount": $("#txtTheDeptValueCount").text(),
            "ProgramPercent": $("#txt_ProgramPercent").val(),
            "ProgramCount": $("#txt_ProgramCount").text(),
            "ShouldBeValuePercent": $("#txt_ShouldBeValuePercent").val(),
            "ShouldBeValueCount": $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text(),
            "ActualAllountTime": $("#ctl00_ContentPlaceHolder1_drp_year").val()
        };

        //        $("#btnApprovalOne").attr("disabled", true);

        var jsonObj = Global.toJSON(ViewEntity);

        Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": "4", "data": jsonObj, "AllotID": $("#HiddenAllotID").val() }, null, null, function (jsonResult) {
            if (jsonResult < 0) {
                alert("审核失败！");
            } else {
                //查询系统新消息
                if (jsonResult != 0) {
                    $("#HiddenAllotID").val(jsonResult);
                }
                window.location.reload();
            }
        });
    });

    //上一步
    $("#btn_Up").click(function () {
        $("#ctl00_ContentPlaceHolder1_stage").attr("disabled", false);
        $("input", "#fidOne").attr("disabled", false);
        $("#btnApprovalOne").attr("disabled", false);
        $("#ctl00_ContentPlaceHolder1_drp_year").attr("disabled", false);
        $("#ctl00_ContentPlaceHolder1_drp_year").show();
        $("#txtTheDeptValuePercent").attr("disabled", false);
        $("#txt_ProgramPercent").attr("disabled", false);
        $("#txt_DesignManagerPercent").attr("disabled", false);
    });
    //保存按钮--保存信息人员信息
    $("#btnApprovalTwo").click(function () {

        var msg = "";
        if (!Validation()) {
            return false;
        }

        var _jzTotalPercent = 0;
        var _jzTotalCount = 0;
        var _azTotalPercent = 0;
        var _azTotalCount = 0;
        //设计 --建筑
        var _jzCount = 0;
        var _jzPercent = 0;
        //设计 --结构
        var _jgPercent = 0;
        var _jgCount = 0;
        //设计 --给排水
        var _gpsPercent = 0;
        var _gpsCount = 0;
        //设计 --暖通
        var _ntPercent = 0;
        var _ntCount = 0;
        //设计 --电气
        var _dqPercent = 0;
        var _dqCount = 0;
        //取得校对金额
        var prooPercent = 0;
        var prooAmount = 0;

        var stage = $("#ctl00_ContentPlaceHolder1_stage").val();
        if (stage == "13" || stage == "14" || stage == "15" || stage == "16" || stage == "17" || stage == "19" || stage == "20" || stage == "21" || stage == "22") {

            _jzTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text().replace("%", "");
            _jzTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text();
            _azTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(3)").text().replace("%", "");
            _azTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text();

            //设计 --建筑
            _jzCount = 0;
            _jzPercent = 0;
            //设计 --结构
            _jgPercent = 0;
            _jgCount = 0;

            //设计 --给排水
            _gpsPercent = 0;
            _gpsCount = 0;

            //设计 --暖通
            _ntPercent = 0;
            _ntCount = 0;

            //设计 --电气
            _dqPercent = 0;
            _dqCount = 0;

            //取得校对金额
            prooPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(4)").text().replace("%", "");
            prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();
        }
        else if (stage == "23" || stage == "24" || stage == "25") {

            _jzTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text().replace("%", "");
            _jzTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text();

            _azTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text().replace("%", "");
            _azTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text();

            //设计 --建筑
            _jzPercent = $("#txtBulidging").val();
            _jzCount = $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text();
            //设计 --结构
            _jgPercent = $("#txtStructure").val();
            _jgCount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();

            //设计 --给排水
            _gpsPercent = $("#txtDrain").val();
            _gpsCount = $("#gvProjectValueProcess tr:eq(1) td:eq(6)").text();

            //设计 --暖通
            _ntPercent = $("#txtHavc").val();
            _ntCount = $("#gvProjectValueProcess tr:eq(1) td:eq(7)").text();

            //设计 --电气
            _dqPercent = $("#txtElectric").val();
            _dqCount = $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text();

            //取得校对金额
            prooPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(9)").text().replace("%", "");
            prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(9)").text();
        }
        else {

            _jzTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text().replace("%", "");
            _jzTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text();

            _azTotalPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text().replace("%", "");
            _azTotalCount = $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text();

            //设计 --建筑
            _jzCount = $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text();
            _jzPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(3)").text().replace("%", "");
            //设计 --结构
            _jgPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(4)").text().replace("%", "");
            _jgCount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();

            //设计 --给排水
            _gpsPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(6)").text().replace("%", "");
            _gpsCount = $("#gvProjectValueProcess tr:eq(1) td:eq(6)").text();

            //设计 --暖通
            _ntPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(7)").text().replace("%", "");
            _ntCount = $("#gvProjectValueProcess tr:eq(1) td:eq(7)").text();

            //设计 --电气
            _dqPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(8)").text().replace("%", "");
            _dqCount = $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text();

            //取得校对金额
            prooPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(9)").text().replace("%", "");
            prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(9)").text();
        }
        var finalDataObj = {
            "ProNo": $("#HiddenProSysNo").val(),
            "AllotID": $("#HiddenTranAllotID").val(),
            "ValueByMember": new Array(),
            "AuditUser": $("#HiddenLoginUser").val(),
            "typeStatus": $("#ctl00_ContentPlaceHolder1_stage").val(),
            "ProofreadPercent": prooPercent,
            "ProofreadCount": prooAmount,
            "BuildingPercent": _jzPercent,
            "BuildingCount": _jzCount,
            "StructurePercent": _jgPercent,
            "StructureCount": _jgCount,
            "DrainPercent": _gpsPercent,
            "DrainCount": _gpsCount,
            "HavcPercent": _ntPercent,
            "HavcCount": _ntCount,
            "ElectricPercent": _dqPercent,
            "ElectricCount": _dqCount,
            "TotalBuildingPercent": _jzTotalPercent,
            "TotalBuildingCount": _jzTotalCount,
            "TotalInstallationPercent": _azTotalPercent,
            "TotalInstallationCount": _azTotalCount
        };

        // 取得
        $.each($("#gvProjectValueBymember tr"), function (index, tr) {
            finalDataObj.ValueByMember[index] =
                {
                    "mem_ID": $.trim($(tr).children("td:eq(1)").text()),
                    "DesignPercent": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "DesignCount": $.trim($(tr).children("td:eq(4)").text()),
                    "SpecialtyHeadPercent": 0,
                    "SpecialtyHeadCount": 0,
                    "AuditPercent": 0,
                    "AuditCount": 0,
                    "ProofreadPercent": $.trim($(tr).children("td:eq(5)").find("input").val()),
                    "ProofreadCount": $.trim($(tr).children("td:eq(6)").text()),
                    "ItemType": $.trim($("#ctl00_ContentPlaceHolder1_stage").val()),
                    "IsHead": 0,
                    "IsExternal": $.trim($(tr).children("td:eq(1)").attr("wp"))
                };
        });

        if (!confirm("是否通过审核信息?")) {
            return false;
        }

        $("#btnApprovalTwo").attr("disabled", true);

        //        $("#btnApprovalTwo").attr("disabled", true);
        var jsonObj = Global.toJSON(finalDataObj);

        Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": 15, "data": jsonObj, "flag": 1, "msgID": $("#msgno").val(), "allotID": $("#HiddenTranAllotID").val(), "proType": $("#HiddenProSysNo").val(), "sys_no": $("#HiddenAuditRecordSysNo").val(), }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {
                    //消息
                    alert("所长再次审批通过，消息发给对应人员进行个人产值确认！");
                }
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
            }
        });

    });
    //不通过
    //审核不通过按钮，具体流程业务，基本同上面的审核通过一致
    $("#btnRefuse").click(function () {

        if (!confirm("是否不通过审核信息?")) {
            return false;
        }

        $("#btnRefuse").attr("disabled", true);
        Global.SendRequest("/HttpHandler/ProjectValueandAllot/AddjjsProjectValueAllotHandler.ashx", { "Action": 16, "flag": 1, "msgID": $("#msgno").val(), "allotID": $("#HiddenTranAllotID").val(), "pro_id": $("#HiddenProSysNo").val(), "mem_id": $("#HiddenLoginUser").val(), "sys_no": $("#HiddenAuditRecordSysNo").val(), "proType": $("#hid_proType").val() }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {
                    //消息
                    alert("所长再次审批不通过！");
                }
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
            }
        });
    });
    //返回
    $("#btn_back").click(function () {
        window.location.href = "/Coperation/cpr_SysValueMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
    });
    //添加人员

    var chooseUserMain = new ChooseProjectValueUserControl($("#chooseUserMain"));
    //选择用户
    $("#chooseUser").click(function () {

        var parObj = {};
        parObj.UnitName = ""
        parObj.SpeName = "预算";
        chooseUserMain.Clear();
        chooseUserMain.BindData(parObj);
        //        $("#chooseUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "添加用户",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseUserMain.SaveUser(ChooseUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        $("#btn_UserMain").click(function () {
            //调用处理事件
            chooseUserMain.SaveUser(ChooseUserOfTheDepartmentCallBack);
        })
    });


    //添加外聘人员

    var chooseExternalUser = new ChooseExternalUserControl($("#chooseExternalUserDiv"));
    //选择用户
    $("#chooseExternalUser").click(function () {

        var parObj = {};
        parObj.UnitName = "经济所"
        parObj.SpeName = "预算";
        chooseExternalUser.Clear();
        chooseExternalUser.BindData(parObj);
        //        $("#chooseExternalUserDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            resizable: false,
        //            title: "外聘人员",
        //            buttons:
        //			        {
        //			            "确定": function () {
        //			                //调用处理事件
        //			                chooseExternalUser.SaveUser(ChooseUserOfTheDepartmentCallBack);
        //			                $(this).dialog("close");
        //			            },
        //			            "取消": function () { $(this).dialog("close"); }
        //			        }
        //        }).dialog("open");
        //        return false;
        $("#btnChooseExt").click(function () {
            //调用处理事件
            chooseExternalUser.SaveUser(ChooseUserOfTheDepartmentCallBack);
        });
    });

    //选择用户
    function ChooseUserOfTheDepartmentCallBack(userArray, iswp) {
        //设置行数
        var trLength = 0;
        var IsAdd = false;

        $.each(userArray, function (index, item) {
            $("#gvProjectValueBymember tr").each(function () {
                $("td[class=cls_Column]", this).each(function () {
                    if (item.userSpecialtyname == $(this).text()) {
                        index = parseInt($(this).parent().index());
                        lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        IsAdd = true;
                    }
                });
            });
        });
        if (IsAdd) {

            var index = 0; //此行的索引
            var lengthTemp = 0; //本专业之前的rowspan
            var _tempTr = 0; //是否第一次添加
            var _userLength = userArray.length;
            $.each(userArray, function (index, item) {

                $("#gvProjectValueBymember tr").each(function () {
                    $("td[class=cls_Column]", this).each(function () {
                        if (item.userSpecialtyname == $(this).text()) {
                            index = parseInt($(this).parent().index());
                            lengthTemp = $(this).attr("rowspan") == undefined ? 1 : $(this).attr("rowspan");
                        }
                    });
                });

                var IsExisted = true;

                //循环该人员是否存在
                $("#gvProjectValueBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;
                        _userLength = _userLength - 1;
                    }
                });

                //不存在添加
                if (IsExisted) {

                    //得到这个专业rowspanID
                    if (_tempTr == 0) {
                        $("#gvProjectValueBymember  tr:eq(" + index + ") td:eq(0)").attr("rowspan", parseInt(lengthTemp) + parseInt(_userLength));
                    }
                    var trString = "";
                    trString += "<tr>";
                    trString = "<tr><td class=\"display\"></td>";
                    trString += "<td class=\"display\" wp=" + iswp + " >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"10%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveA\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    trString += "<td width= \"10%\"><input  maxlength=\"15\" type=\"text\" id=\"txtChooseUser\"  roles=\"design\"   sz=" + item.userPrincipalship + " spe=" + item.userSpecialtyname + " />%</td>";
                    trString += "<td width= \"10%\">0</td>";
                    trString += "<td width= \"10%\"><input  maxlength=\"15\" type=\"text\"   id=\"txtChooseUser\"  roles=\"jd\"  />%</td>";
                    trString += "<td width= \"10%\">0</td>";
                    trString += "</tr>";

                    $(trString).insertAfter("#gvProjectValueBymember tr:eq(" + index + ")");
                    _tempTr = _tempTr + 1;
                }

            });

        }
        else {
            $.each(userArray, function (index, item) {

                //获取原Table下已有的用户          
                var IsExisted = true;
                //遍历已有的用户信息数组，如果已经存在，不允许添加

                $("#gvProjectValueBymember tr").each(function () {
                    var userSysNO = $(this).children(":eq(1)").text();
                    if (parseInt(userSysNO) == item.userSysNo) {
                        IsExisted = false;

                    }
                });

                if (IsExisted) {

                    var trString = "";
                    if (trLength == 0) {
                        trString = "<tr><td  width= \"15%\" class=\"cls_Column\" rowspan=" + (parseInt(userArray.length)) + ">" + item.userSpecialtyname + "</td>";
                    }
                    else {
                        trString = "<tr><td class=\"display\" width= \"10%\"></td>";
                    }
                    trString += "<td class=\"display\"  wp=" + iswp + "  >" + item.userSysNo + "</td>";
                    trString += "<td  width= \"15%\"><span>" + item.userName + "<a href=\"###\" style=\"margin-left:3px;color:blue;\" id=\"resultUserRemoveB\" spe=" + item.userSpecialtyname + ">X</a></span></td>";
                    trString += "<td width= \"17.5%\"><input  maxlength=\"15\" type=\"text\" id=\"txtChooseUser\"  roles=\"design\" sz=" + item.userPrincipalship + " spe=" + item.userSpecialtyname + " />%</td>";
                    trString += "<td width= \"17.5%\">0</td>";
                    trString += "<td width= \"17.5%\"><input  maxlength=\"15\" type=\"text\"   id=\"txtChooseUser\"  roles=\"jd\"  />%</td>";
                    trString += "<td width= \"17.5%\">0</td>";
                    trString += "</tr>";
                    trLength = trLength + 1;
                    $("#gvProjectValueBymember").append(trString);

                }
            });
        }
    }

    //删除用户A标签点击事件
    $("#resultUserRemoveA").live("click", function () {
        var spe = $(this).attr("spe");
        var rowspan = 0;
        $($(this).parent().parent().parent().parent().children().find("td:eq(0)")).each(function () {
            if ($(this).text() == spe) {
                rowspan = $(this).attr("rowspan");
                $(this).attr("rowspan", parseInt(rowspan) - 1);
            }
        });
        if (rowspan != 0) {
            $(this).parent().parent().parent().remove();
        }
    });

    //删除用户A标签点击事件
    $("#resultUserRemoveB").live("click", function () {
        var rowspan = $(this).parent().parent().prev().prev().attr("rowspan");
        var classDisplay = $(this).parent().parent().parent().children(":eq(0)").attr("class");
        if (rowspan != undefined && classDisplay != 'display') {
            //把这一行删掉把下一行的td换成文本框
            if (rowspan == 1) {
                $(this).parent().parent().parent().remove();
            }
            else {
                var rowspanText = $(this).parent().parent().prev().prev().text();
                $(this).parent().parent().parent().next().children(":eq(0)").attr("width", "15%");
                $(this).parent().parent().parent().next().children(":eq(0)").attr("class", "cls_Column");
                $(this).parent().parent().parent().next().children(":eq(0)").attr("rowspan", rowspan - 1);
                $(this).parent().parent().parent().next().children(":eq(0)").text(rowspanText);
                $(this).parent().parent().parent().remove();
            }
        } else {
            var speName = $(this).attr("spe");
            $("tr", $(this).parent().parent().parent().parent()).each(function () {
                $("td[class=cls_Column]", this).each(function () {
                    if (speName == $(this).text()) {
                        var tempRow1 = $(this).attr("rowspan");
                        $(this).attr("rowspan", (tempRow1 - 1));
                    }
                });
            });
            $(this).parent().parent().parent().remove();
        }
        // $(this).parent().parent().parent().remove();
    });

    var bzPercent = $("#gvProjectValueProcess tr:eq(0) td:eq(1)").text().replace("%", "");
    var allotCount = $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text();
    //建筑
    $("#txtBulidging").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text(total.toFixed(2));

        var otherPercent = $("#txtStructure").val().length == 0 ? 0 : parseFloat($("#txtStructure").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent);
        $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text(percent.toFixed(2));

        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text(count.toFixed(2));

        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {

            var input = $(this).val().length == 0 ? 0 : parseFloat($(this).val());
            //设计 --建筑
            var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text();

            var inputvalue = $(this).val();
            var proresultvalue = 0;
            proresultvalue = parseFloat(input) * parseFloat(_total) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }
        });
    });

    //结构
    $("#txtStructure").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text(total.toFixed(2));

        var otherPercent = $("#txtBulidging").val().length == 0 ? 0 : parseFloat($("#txtBulidging").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent);
        $("#gvProjectValueProcess tr:eq(0) td:eq(2)").text(percent.toFixed(2));
        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text(count.toFixed(2));

        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {

            var input = $(this).val().length == 0 ? 0 : parseFloat($(this).val());
            //设计 --建筑
            var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text();

            var inputvalue = $(this).val();
            var proresultvalue = 0;
            proresultvalue = parseFloat(input) * parseFloat(_total) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }
        });
    });

    //给排水
    $("#txtDrain").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(6)").text(total.toFixed(2));

        var otherPercent = $("#txtHavc").val().length == 0 ? 0 : parseFloat($("#txtHavc").val());
        var otherPercent1 = $("#txtElectric").val().length == 0 ? 0 : parseFloat($("#txtElectric").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent) + parseFloat(otherPercent1);
        $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text(percent.toFixed(2));

        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text(count.toFixed(2));

        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {

            var input = $(this).val().length == 0 ? 0 : parseFloat($(this).val());
            //设计 --建筑
            var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text();

            var inputvalue = $(this).val();
            var proresultvalue = 0;
            proresultvalue = parseFloat(input) * parseFloat(_total) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }
        });
    });

    //暖通
    $("#txtHavc").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(7)").text(total.toFixed(2));

        var otherPercent = $("#txtDrain").val().length == 0 ? 0 : parseFloat($("#txtDrain").val());
        var otherPercent1 = $("#txtElectric").val().length == 0 ? 0 : parseFloat($("#txtElectric").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent) + parseFloat(otherPercent1);
        $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text(percent.toFixed(2));
        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text(count.toFixed(2));

        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {

            var input = $(this).val().length == 0 ? 0 : parseFloat($(this).val());
            //设计 --建筑
            var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text();

            var inputvalue = $(this).val();
            var proresultvalue = 0;
            proresultvalue = parseFloat(input) * parseFloat(_total) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }
        });
    });

    //电气
    $("#txtElectric").change(function () {
        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }
        var inputValue = $(this).val();
        var total = parseFloat(inputValue) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text(total.toFixed(2));

        var otherPercent = $("#txtDrain").val().length == 0 ? 0 : parseFloat($("#txtDrain").val());
        var otherPercent1 = $("#txtHavc").val().length == 0 ? 0 : parseFloat($("#txtHavc").val());
        var percent = parseFloat(inputValue) + parseFloat(otherPercent) + parseFloat(otherPercent1);
        $("#gvProjectValueProcess tr:eq(0) td:eq(5)").text(percent.toFixed(2));

        var count = parseFloat(percent) / 100 * parseFloat(bzPercent) / 100 * parseFloat(allotCount);
        $("#gvProjectValueProcess tr:eq(1) td:eq(5)").text(count.toFixed(2));

        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {

            var input = $(this).val().length == 0 ? 0 : parseFloat($(this).val());
            //设计 --建筑
            var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(8)").text();

            var inputvalue = $(this).val();
            var proresultvalue = 0;
            proresultvalue = parseFloat(input) * parseFloat(_total) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }
        });
    });

    //取得校对金额
    var prooAmount = 0;
    var stage = $("#ctl00_ContentPlaceHolder1_stage").val();
    if (stage == "13" || stage == "14" || stage == "15" || stage == "16" || stage == "17" || stage == "19" || stage == "20" || stage == "21" || stage == "22") {
        prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(4)").text();
    } else {
        prooAmount = $("#gvProjectValueProcess tr:eq(1) td:eq(9)").text();
    }


    //校对--人员金额计算
    $(":text[roles=jd]", "#gvProjectValueBymember tr").each(function () {

        $(this).change(function () {


            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }

            var inputvalue = $(this).val();
            var proresultvalue = parseFloat(inputvalue) * parseFloat(prooAmount) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }
        });
    });

    //锅炉房 
    if (stage == "13" || stage == "14" || stage == "15" || stage == "16" || stage == "17" || stage == "19" || stage == "20" || stage == "21" || stage == "22") {
        //设计 --建筑
        // var _jz = $("#gvProjectValueProcess tr:eq(1) td:eq(2)").text();
        //设计 --结构
        // var _gps = $("#gvProjectValueProcess tr:eq(1) td:eq(3)").text();
        var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text();

        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {

            $(this).change(function () {
                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }
                var inputvalue = $(this).val();
                var proresultvalue = 0;
                proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });

        });

        //设计--人员金额计算--后来添加人员
        $("#txtChooseUser[roles=design]", "#gvProjectValueBymember tr").live('change', function () {
            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }
            var inputvalue = $(this).val();
            var proresultvalue = 0;
            proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });

    }
    else if (stage == "23" || stage == "24" || stage == "25") {
        var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text();

        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {

            $(this).change(function () {

                if (!ValidationProcess()) {
                    return false;
                }

                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }

                var inputvalue = $(this).val();
                var proresultvalue = 0;

                proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });

        });

        //设计--人员金额计算--后来添加人员
        $("#txtChooseUser[roles=design]", "#gvProjectValueBymember tr").live('change', function () {
            if (!ValidationProcess()) {
                return false;
            }

            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }

            var inputvalue = $(this).val();
            var proresultvalue = 0;

            proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });
    } else {

        //设计 --建
        var _total = $("#gvProjectValueProcess tr:eq(1) td:eq(1)").text();

        $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {


            $(this).change(function () {

                if (!checkRate($(this).val())) {
                    $(this).val(0);
                }
                if ($(this).val() == '') {
                    $(this).val(0);
                }

                var inputvalue = $(this).val();
                var proresultvalue = 0;

                proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;

                if (isRounding == "0") {
                    $(this).parent().next().text(proresultvalue.toFixed(0));
                }
                else {
                    $(this).parent().next().text(Math.floor(proresultvalue));
                }

            });

        });

        //设计--人员金额计算--后来添加人员
        $("#txtChooseUser[roles=design]", "#gvProjectValueBymember tr").live('change', function () {

            if (!checkRate($(this).val())) {
                $(this).val(0);
            }
            if ($(this).val() == '') {
                $(this).val(0);
            }
            var inputvalue = $(this).val();
            var proresultvalue = 0;
            proresultvalue = parseFloat(inputvalue) * parseFloat(_total) / 100;
            if (isRounding == "0") {
                $(this).parent().next().text(proresultvalue.toFixed(0));
            }
            else {
                $(this).parent().next().text(Math.floor(proresultvalue));
            }

        });
    }

    //校对--人员金额计算--后来添加人员
    $("#txtChooseUser[roles=jd]", "#gvProjectValueBymember tr").live('change', function () {

        if (!checkRate($(this).val())) {
            $(this).val(0);
        }
        if ($(this).val() == '') {
            $(this).val(0);
        }

        var inputvalue = $(this).val();
        var proresultvalue = parseFloat(inputvalue) * parseFloat(prooAmount) / 100;
        if (isRounding == "0") {
            $(this).parent().next().text(proresultvalue.toFixed(0));
        }
        else {
            $(this).parent().next().text(Math.floor(proresultvalue));
        }

    });

    //    messageDialog = $("#msgReceiverContainer").messageDialog({
    //        "button": {
    //            "发送消息": function () {
    //                //选中用户
    //                var _$mesUser = $(":checkbox[name=messageUser]:checked");

    //                if (_$mesUser.length == 0) {
    //                    alert("请至少选择一个流程审批人！");
    //                    return false;
    //                }

    //                getUserAndUpdateAudit('5', '1', jsonDataEntity);
    //            },
    //            "关闭": function () {
    //                $("#btnApprovalTwo").attr("disabled", false);
    //                messageDialog.hide();
    //            }
    //        }
    //    });
    messageDialog = $("#auditShow").messageDialog;
    sendMessageClass = new MessageCommon(messageDialog);
});

//计算产值
function CalculationValue() {

    //分配金额
    var allotAccount = 0;

    allotAccount = $("#ctl00_ContentPlaceHolder1_lblAllotCount").text();

    //实收产值
    var paidValuePercent = $("#txt_PaidValuePercent").val().length == 0 ? 0 : parseFloat($("#txt_PaidValuePercent").val());
    var paidValueCount = parseFloat(paidValuePercent) * parseFloat(allotAccount) / 100;
    $("#ctl00_ContentPlaceHolder1_txt_PaidValueCount").text(paidValueCount.toFixed(0));

    //本部门自留
    var theDeptValuePercent = $("#txtTheDeptValuePercent").val().length == 0 ? 0 : parseFloat($("#txtTheDeptValuePercent").val());
    //本部门自留产值
    var theDeptValueCount = parseFloat(theDeptValuePercent) * parseFloat(allotAccount) / 100;
    $("#txtTheDeptValueCount").text(theDeptValueCount.toFixed(0));

    //方案金额
    var programValuePercent = $("#txt_ProgramPercent").val().length == 0 ? 0 : parseFloat($("#txt_ProgramPercent").val());
    //方案金额
    var programValueCount = (parseFloat(allotAccount) - parseFloat(theDeptValueCount)) * programValuePercent / 100;

    $("#txt_ProgramCount").text(programValueCount.toFixed(2));

    //设总
    var designManagerPercent = $("#txt_DesignManagerPercent").val().length == 0 ? 0 : parseFloat($("#txt_DesignManagerPercent").val());
    //设总金额
    var DesignManagerCount = (parseFloat(allotAccount) - parseFloat(theDeptValueCount) - parseFloat(programValueCount)) * parseFloat(designManagerPercent) / 100;
    $("#txt_DesignManagerCount").text(DesignManagerCount.toFixed(0));

    // 应分产值比例 
    //应分产值金额 等于分配产值 -转经济所-转其他部门-转暖通-设总-本部门自留产值-方案
    var shouleBeValueCount = parseFloat(allotAccount) - parseFloat(programValueCount) - parseFloat(DesignManagerCount) - parseFloat(theDeptValueCount);
    $("#ctl00_ContentPlaceHolder1_txt_ShouldBeValueCount").text(shouleBeValueCount.toFixed(0));

    $("#txt_ShouldBeValuePercent").attr("value", (shouleBeValueCount / allotAccount * 100).toFixed(2));
}

//浮点数
function checkRate(value) {
    var re = /^[0-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+$/;
    if (re.test(value)) {
        return true;
    }
}

//验证工序
function ValidationProcess() {
    var flag = true;

    var budingPercent = $("#txtBulidging").val();
    if (budingPercent == "") {
        alert("建筑配置比例不能为空");
        flag = false;
        return false;

    }
    var structure = $("#txtStructure").val()
    if (structure == "") {
        alert("结构配置比例不能为空");
        flag = false;
        return false;

    }
    var drain = $("#txtDrain").val();
    if (drain == "") {
        alert("给排水配置比例不能为空");
        flag = false;
        return false;

    }
    var havc = $("#txtHavc").val();
    if (havc == "") {
        alert("暖通配置比例不能为空");
        flag = false;
        return false;

    }
    var electric = $("#txtElectric").val();
    if (electric == "") {
        alert("电气配置比例不能为空");
        flag = false;
        return false;

    }

    var total = parseFloat(budingPercent) + parseFloat(structure) + parseFloat(drain) + parseFloat(havc) + parseFloat(electric);

    if (total.toFixed(0) != 100) {
        alert("配置比例没有闭合，请修改");
        flag = false;
        return false;
    }

    return flag;
}

function Validation() {

    var flag = true;

    var stage = $("#ctl00_ContentPlaceHolder1_stage").val();

    if (stage == "23" || stage == "24" || stage == "25") {

        if (!ValidationProcess()) {
            flag = false;
            return false;
        }
    }

    var trLength = $("#gvProjectValueBymember tr").length;
    if (trLength == 0) {
        alert("请添加人员");
        flag = false;
        return false;
    }

    var valuespeemputy = "0";
    $.each($("input:not(:disabled)", "#gvProjectValueBymember tr td"), function () {
        if ($.trim($(this).val()).length == 0) {
            valuespeemputy = $(this).val();
            return false;
        }
    });

    if ($.trim(valuespeemputy).length == 0) {
        alert("人员产值分配比例不能为空");
        flag = false;
        return false;
    }

    var designcountOne = 0;
    var isCheckOne = false;
    $(":text[roles=design]", "#gvProjectValueBymember tr").each(function () {
        designcountOne += parseFloat($(this).val());
        isCheckOne = true;
    });

    if (designcountOne.toFixed(0) != 100 && isCheckOne == true) {
        alert("人员编制比例没有闭合，必须为100%,请修改");
        flag = false;
        return false;
    }

    var designcountTwo = 0;
    var isCheckTwo = false;
    $(":text[roles=jd]", "#gvProjectValueBymember tr").each(function () {
        designcountTwo += parseFloat($(this).val());
        isCheckTwo = true;
    });

    if (designcountTwo.toFixed(0) != 100 && isCheckTwo == true) {
        alert("人员校对比例没有闭合，必须为100%,请修改");
        flag = false;
        return false;
    }
    return flag;
}

function setStatus() {
    if ($.trim($("#hid_IsDone").val()) == "D") {
        $("input").attr("disabled", "disabled");
        $("input[type=button]").hide();
        $("#chooseUser").hide();
        $("#chooseExternalUser").hide();
    }
}