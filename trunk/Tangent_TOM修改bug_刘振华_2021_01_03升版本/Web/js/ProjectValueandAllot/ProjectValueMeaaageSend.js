﻿//发送消息
function ProjectValueMessageCommon(md) {
    //消息框

    this.messageDialog = md;
    var _instance = this;
    //显示角色用户列表
    this.render = function (userInfoArray, roleName) {

        var _userInfoArray = userInfoArray;
        var _roleName = roleName;
        var roles = new Array();
        for (var i = 0; i < _userInfoArray.length; i++) {
            roles.push(_userInfoArray[i].RoleName);
        }

        //角色去重
        var _roles = roles.reverse().join(",").match(/([^,]+)(?!.*\1)/ig).reverse();

        var $div = $("<div style='overflow-y:auto;width: 450px; height: 200px;'></div>")
        var _$table = $("<table style=\"width:100%;font-size:9pt\"></table>");

        var trHead = "<tr style=\"font-size:10pt;font-weight:bold;background-color:#f0f0f0;height:20px;\"><td colspan=\"4\">请选择下一阶段<span style=\"color:Red;\">" + _roleName + "</span>审批人</td></tr>";

        _$table.append(trHead);
        var _$trString = $("<tr></tr>");
        var _$tdString = $("<td></td>");
        var _contentTable = $("<table style=\"width:100%;\" class=\"valueMessage\"></table>");
        var _contentHeadTr = "";
        if (_roleName == "专业负责人") {
            _contentHeadTr = "<tr><td width=\"20%\"  style=\"font-weight:bold; background-color: #E6E6E6;\" align=\"center\"> 专业</td><td width=\"80%\"  style=\"font-weight:bold; background-color: #E6E6E6;\" align=\"center\"> 专业负责人</td></tr>";

        } else {
            _contentHeadTr = "<tr><td width=\"20%\"  style=\"font-weight:bold; background-color: #E6E6E6;\" align=\"center\"> 所长</td><td width=\"80%\"  style=\"font-weight:bold; background-color: #E6E6E6;\" align=\"center\"> 审核人</td></tr>";

        }
        _contentTable.append(_contentHeadTr);

        $.each(_roles, function (key, val) {

            var _contentTr = "<tr>";

            _contentTr += "<td  style=\"color:Red;font-weight:bold;\" align=\"center\">" + val + "</td>";

            _contentTr += "<td>";

            var _contenTable = "<table style=\"border:0px;\"><tr>";
            //选择接收用户table
            for (var i = 0; i < _userInfoArray.length; i++) {

                if (_userInfoArray[i].RoleName == val) {
                    if (i % 4 == 0 && i != 0) {
                        _contenTable += "</tr><tr>";
                    }
                    _contenTable += "<td style=\"border:0px;\"><input checked=\"checked\" type=\"checkbox\"  name=\"messageUser\" roles=\"" + _userInfoArray[i].RoleName + "\" value=\"" + _userInfoArray[i].UserSysNo + "\" username=\"" + _userInfoArray[i].UserName + "\">" + _userInfoArray[i].UserName + "</td>";

                }
            }

            _contenTable += "</tr></table>";

            _contentTr += _contenTable + "</td>";

            _contentTr += "</tr>";

            _contentTable.append(_contentTr);

        });
        _$tdString.append(_contentTable);
        _$trString.append(_$tdString);

        _$table.append(_$trString);

        if (_roleName == "专业负责人") {
            var _trbottom = "<tr><td colspan=\"4\" style=\"height:20px; color:Red\">注释：如果该专业没有负责人,取的是本专业所有人员</td></tr>";
            _$table.append(_trbottom);
        }

        $div.append(_$table)
        //_instance.messageDialog.show($div);
        $("#auditShow").empty()
        $("#auditShow").append($div);
    }


    //设置消息实体
    this.setMsgTemplate = function (mt) {
        _instance.messageTemplate = mt;
    }
    //mt 消息实体，callBack 消息发送成功回调函数
    this.chooseUserForMessage = function (callBack) {
        //选中用户
        var _$mesUser = $(":checkbox[name=messageUser]:checked");


        var userArray = new Array();
        $.each(_$mesUser, function (i, u) {
            userArray.push({
                "UserSysNo": u.value,
                "UserName": $(u).attr("username")
            });
        });

        var _jsonDataObj = {
            "MessageTemplate":
                {
                    "FromUser": _instance.messageTemplate.FromUser,
                    "InUser": _instance.messageTemplate.InUser,
                    "MessageContent": _instance.messageTemplate.MessageContent,
                    "MsgType": _instance.messageTemplate.MsgType,
                    "QueryCondition": _instance.messageTemplate.QueryCondition,
                    "ReferenceSysNo": _instance.messageTemplate.ReferenceSysNo,
                    "ToRole": _instance.messageTemplate.ToRole,
                    "IsDone": _instance.messageTemplate.IsDone
                },
            "UserList": userArray
        };

        //请求发送消息
        $.post("/HttpHandler/MessageCommonHandler.ashx", { "jsonData": Global.toJSON(_jsonDataObj) }, callBack);
    }
}
