﻿$(document).ready(function () {

    $("#ctl00_ContentPlaceHolder1_hid_researchid").val(hid_researchid);


    //保存
    $("#btn_Save").click(function () {

        var msg = "";
        if (name == "") {
            msg += "请输入项目名称！</br>";
        }
       
        //提示

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        else {
            $(this).hide();
        }
    });


    //项目负责人
    var chooseUser = new ChooseUserControl($("#chooseUserMain"), PMCallBack);


    //承接部门
    $("#sch_unit").click(function () {
        showDivDialogClass.SetParameters({
            "nowIndex": "cjbm_nowPageIndex", //当前页id
            "pageSize": "10"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "proCjbm", ProCjbmCallBack);
        BindAllDataCount(); //绑定总数据
    });

    //协作单位内部  (多选)
    $("#sch_unitMultiple").click(function () {
        showDivDialogClass.SetParameters({
            "nowIndex": "cjbm_nowPageIndexMultiple", //当前页id
            "pageSize": "10"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "proCjbm", ProCjbmCallBackMultiple);
        BindAllDataCountMultiple(); //绑定总数据
    });
});


//设总返回值
function PMCallBack(userObj) {

    $("#ctl00_ContentPlaceHolder1_txt_PMName").val(userObj.username);
    $("#ctl00_ContentPlaceHolder1_hid_pmname").val(userObj.username);
    $("#ctl00_ContentPlaceHolder1_hid_pmuserid").val(userObj.usersysno);
}


//项目承接部门
function ProCjbmCallBack(result) {
    if (result != null) {
        var data = result.ds;
        $("#cpr_cjbmTable tr:gt(0)").remove();
        $.each(data, function (i, n) {
            var oper = "<span rel='" + n.unit_ID + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\">选择</span>";
            var trHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.unit_Name + "</td><td>" + oper + "</td></tr>";
            $("#cpr_cjbmTable").append(trHtml);
            $("#cpr_cjbmTable span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txt_unit").val($.trim(n.unit_Name));
                $("#ctl00_ContentPlaceHolder1_hid_unit").val($.trim(n.unit_Name));
            });
        });
        ControlTableCss("cpr_cjbmTable");
    }
}

//项目承接部门 多选
function ProCjbmCallBackMultiple(result) {
    if (result != null) {
        var data = result.ds;
        $("#cpr_cjbmTableMultiple tr:gt(0)").remove();
        $.each(data, function (i, n) {
            var oper = "<span rel='" + n.unit_ID + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\">选择</span>";
            var trHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.unit_Name + "</td><td>" + oper + "</td></tr>";
            $("#cpr_cjbmTableMultiple").append(trHtml);
            $("#cpr_cjbmTableMultiple span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txt_unit").val($.trim(n.unit_Name));
                $("#ctl00_ContentPlaceHolder1_hid_unit").val($.trim(n.unit_Name));
            });
        });
        ControlTableCss("cpr_cjbmTableMultiple");
    }
}

//承接部门数据总数CallBack函数
function GetCjbmAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        NoDataMessageOnTable("pro_cjbmTable", 3);
    }
}

//承接部门数据总数CallBack函数 多选
function GetCjbmAllDataCountMultiple(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        NoDataMessageOnTable("pro_cjbmTableMultiple", 3);
    }
}

//获得承接部门数据总数
function BindAllDataCount() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "cjbm_prevPage",
        "firstPage": "cjbm_firstPage",
        "nextPage": "cjbm_nextPage",
        "lastPage": "cjbm_lastPage",
        "gotoPage": "cjbm_gotoPageIndex",
        "allDataCount": "cjbm_allDataCount",
        "nowIndex": "cjbm_nowPageIndex",
        "allPageCount": "cjbm_allPageCount",
        "gotoIndex": "cjbm_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", "", "false", "proCjbm", GetCjbmAllDataCount);
    //alert("分页前,当前页数: "+$("#"+ParametersDict.nowIndex+"").text());
    //注册事件,先注销,再注册
    $("#cjbmByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#cjbm_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", pageIndex, "proCjbm", ProCjbmCallBack);
            //showDivDialogClass.BindPageValue($(this).attr("id"));
        }
    });
}
//获得承接部门数据总数  多选
function BindAllDataCountMultiple() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "cjbm_prevPageMultiple",
        "firstPage": "cjbm_firstPageMultiple",
        "nextPage": "cjbm_nextPageMultiple",
        "lastPage": "cjbm_lastPageMultiple",
        "gotoPage": "cjbm_gotoPageIndexMultiple",
        "allDataCount": "cjbm_allDataCountMultiple",
        "nowIndex": "cjbm_nowPageIndexMultiple",
        "allPageCount": "cjbm_allPageCountMultiple",
        "gotoIndex": "cjbm_pageIndexMultiple",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", "", "false", "proCjbm", GetCjbmAllDataCountMultiple);
    //alert("分页前,当前页数: "+$("#"+ParametersDict.nowIndex+"").text());
    //注册事件,先注销,再注册
    $("#cjbmByPageDivMultiple span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#cjbm_nowPageIndexMultiple").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", pageIndex, "proCjbm", ProCjbmCallBackMultiple);
            //showDivDialogClass.BindPageValue($(this).attr("id"));
        }
    });
}
//加载数据
function GetAttachData() {
    loadAttach();
}
//加载附件信息
function loadAttach() {
    var data = "action=getcprfiles&cprid=" + $("#ctl00_ContentPlaceHolder1_hid_researchid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            if (result != null) {

                var filedata = result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").remove();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    var oper = "<a href='#' rel='" + n.ID + "'>删除</a>";
                    var oper2 = "<a href='../Attach_User/filedata/cprfile/" + n.FileUrl + "' target='_blank'>查看</a>";
                    var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").html(img + n.FileName);
                    row.find("#att_filename").attr("align", "left");
                    row.find("#att_filesize").text(n.FileSizeString + 'KB');
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper").html(oper);
                    row.find("#att_oper2").html(oper2);
                    row.find("#att_oper a").click(function () {
                        delAttach($(this));
                    });
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas_att");
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//删除附件
function delAttach(link) {
    var data = "action=delcprattach&attid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "text",
        success: function (result) {
            if (result == "yes") {
                //加载附件
                link.parents("tr:first").remove();
                // alert("附件删除成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}

//表格样式-各行变色-鼠标覆盖事件
function ControlTableCss(tableId) {
    $("#" + tableId + " tr:gt(0):odd").attr("class", "trOddColor");
    $("#" + tableId + " tr:gt(0)").hover(function () {
        $(this).addClass("mouseOverColor");
    }, function () {
        $(this).removeClass("mouseOverColor");
    });
}

//无数据提示
function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}