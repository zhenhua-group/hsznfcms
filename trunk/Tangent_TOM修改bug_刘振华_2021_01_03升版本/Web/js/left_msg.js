﻿$(document).ready(function() {
    //未读消息
    $("#msg_wd").click(function() {
        $("#rightFrame", window.parent.document).attr("src", "../Coperation/cpr_SysMsgListView.aspx?flag=A&leftmsg=1");
    });
    //已读消息
    $("#msg_yd").click(function() {
        $("#rightFrame", window.parent.document).attr("src", "../Coperation/cpr_SysMsgListView.aspx?flag=D&leftmsg=2");
    });
    //已办事项
    $("#msg_nodone").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../Coperation/cpr_SysMsgListView.aspx?action=done&flag=A&leftmsg=4");
    });
    //未办事项
    $("#msg_done").click(function () {
        $("#rightFrame", window.parent.document).attr("src", "../Coperation/cpr_SysMsgListView.aspx?action=done&flag=D&leftmsg=3");
    });
});