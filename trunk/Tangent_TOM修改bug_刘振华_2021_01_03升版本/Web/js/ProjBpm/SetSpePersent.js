﻿
var SetSpe = function () {

    var pageLoad = function () {

        $("#tbData tbody :text").blur(function () {
            var obj = $(this);

            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            if (!reg_math.test(obj.val())) {
                obj.val("");
                return false;
            }

            var allcount = 0;
            $("#tbData tbody :text").each(function (i, item) {

                if (item.value != "") {
                    allcount += parseFloat(item.value);
                }
            });
            //比例总计
            $("#spAllCount").text(allcount.toFixed(2));
        }).focus(function () {
            if ($(this).val() == "0.00") {
                $(this).val('');
            }
            $(this).select();
        });

        //主持人保存按钮
        $("#btnSave").click(function () {

            if (parseFloat($("#spAllCount").text()) > 100 || parseFloat($("#spAllCount").text()) < 100) {
                alert("比例合计应等于100%，请重新分配！");
                return false;
            }

            var unitArr = new Array();

            $("#tbData tbody tr").each(function (index) {
                var obj = $(this);
                //创建对象
                unitArr[index] = {
                    "ID": "0",
                    "ProKHNameID": $("#hidKHNameID").val(),
                    "SpeRoleID": "-2",//主持人
                    "SpeID": obj.find("td").eq(2).find("input").attr("speid"),
                    "SpeName": obj.find("td").eq(2).find("input").attr("spename"),
                    "Persentold": obj.find("td").eq(1).find("span").text(),
                    "Persent1": obj.find("td").eq(2).find("input").val(),
                    "Persent2": "0",
                    "InserUserID1": $("#hidUserSysNo").val()
                };
            });

            var jsonData = JSON.stringify(unitArr);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savespeprst";
            var data = { "action": action, "data": jsonData };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("专业间比例设置成功！");
                        location.reload();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
        //主持人确认提交
        $("#btnSubmit").click(function () {

            if (parseFloat($("#spAllCount").text()) > 100 || parseFloat($("#spAllCount").text()) < 100) {
                alert("比例合计应等于100%，请重新分配！");
                return false;
            }
            //确认提交
            if (!confirm('确认提交比例？')) {
                return false;
            }

            var unitArr = new Array();

            $("#tbData tbody tr").each(function (index) {
                var obj = $(this);
                //创建对象
                unitArr[index] = {
                    "ID": "0",
                    "ProKHNameID": $("#hidKHNameID").val(),
                    "SpeRoleID": "-2",//主持人
                    "SpeID": obj.find("td").eq(2).find("input").attr("speid"),
                    "SpeName": obj.find("td").eq(2).find("input").attr("spename"),
                    "Persentold": obj.find("td").eq(1).find("span").text(),
                    "Persent1": obj.find("td").eq(2).find("input").val(),
                    "Persent2": "0",
                    "InserUserID1": $("#hidUserSysNo").val(),
                    "Stat": "1"
                };
            });

            var jsonData = JSON.stringify(unitArr);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var khnameid = $("#hidKHNameID").val();
            var action = "savespeprst";
            var senderid = $("#hidUserSysNo").val();
            var data = { "action": action, "data": jsonData, "khnameid": khnameid, "senderid": senderid };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("专业间比例设置成功，提交部门经理审批！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();

                        location.reload();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });

        //总建筑师保存
        $("#btnSave2").click(function () {
            if (parseFloat($("#spAllCount").text()) > 100 || parseFloat($("#spAllCount").text()) < 100) {
                alert("比例合计应等于100%，请重新分配！");
                return false;
            }

            var unitArr = new Array();

            $("#tbData tbody tr").each(function (index) {
                var obj = $(this);
                //创建对象
                unitArr[index] = {
                    "ID": "0",
                    "ProKHNameID": $("#hidKHNameID").val(),
                    "SpeRoleID": "-2",//主持人
                    "SpeID": obj.find("td").eq(2).find("span").attr("speid"),
                    "SpeName": obj.find("td").eq(2).find("span").attr("spename"),
                    "Persentold": obj.find("td").eq(1).find("span").text(),
                    "Persent1": obj.find("td").eq(2).find("span").text(),
                    "Persent2": obj.find("td").eq(4).find("input").val(),
                    "InserUserID2": $("#hidUserSysNo").val()
                };
            });

            var jsonData = JSON.stringify(unitArr);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savespeprst2";
            var jixiao = $("#selJixiao").val();
            var data = { "action": action, "data": jsonData, "jixiao": jixiao };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("专业间比例设置成功！");
                        location.reload();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
        //总建筑师提交
        $("#btnSubmit2").click(function () {
            if (parseFloat($("#spAllCount").text()) > 100 || parseFloat($("#spAllCount").text()) < 100) {
                alert("比例合计应等于100%，请重新分配！");
                return false;
            }

            //确认提交
            if (!confirm('确认要提交？')) {
                return false;
            }

            var unitArr = new Array();

            $("#tbData tbody tr").each(function (index) {
                var obj = $(this);
                //创建对象
                unitArr[index] = {
                    "ID": "0",
                    "ProKHNameID": $("#hidKHNameID").val(),
                    "SpeRoleID": "-2",//主持人
                    "SpeID": obj.find("td").eq(2).find("span").attr("speid"),
                    "SpeName": obj.find("td").eq(2).find("span").attr("spename"),
                    "Persentold": obj.find("td").eq(1).find("span").text(),
                    "Persent1": obj.find("td").eq(2).find("span").text(),
                    "Persent2": obj.find("td").eq(4).find("input").val(),
                    "InserUserID2": $("#hidUserSysNo").val(),
                    "Stat2": "1"
                };
            });

            var jsonData = JSON.stringify(unitArr);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savespeprst2";
            var jixiao = $("#selJixiao").val();
            var data = { "action": action, "data": jsonData, "jixiao": jixiao };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("建筑专业内人员,比例审批流程完成！");

                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();

                        location.reload();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });

        //部门经理保存
        $("#btnSave3").click(function () {
            if (parseFloat($("#spAllCount").text()) > 100 || parseFloat($("#spAllCount").text()) < 100) {
                alert("比例合计应等于100%，请重新分配！");
                return false;
            }

            var unitArr = new Array();

            $("#tbData tbody tr").each(function (index) {
                var obj = $(this);
                //创建对象
                unitArr[index] = {
                    "ID": "0",
                    "ProKHNameID": $("#hidKHNameID").val(),
                    "SpeRoleID": "-3",//部门经理
                    "SpeID": obj.find("td").eq(2).find("span").attr("speid"),
                    "SpeName": obj.find("td").eq(2).find("span").attr("spename"),
                    "Persentold": obj.find("td").eq(1).find("span").text(),
                    "Persent1": obj.find("td").eq(2).find("span").text(),
                    "Persent3": obj.find("td").eq(3).find("input").val(),
                    "InserUserID3": $("#hidUserSysNo").val()
                };
            });

            var jsonData = JSON.stringify(unitArr);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savespeprst3";
            var jixiao = $("#selJixiao").val();
            var data = { "action": action, "data": jsonData, "jixiao": jixiao };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("专业间比例设置成功！");
                        location.reload();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
        //部门经理提交
        $("#btnSubmit3").click(function () {
            if (parseFloat($("#spAllCount").text()) > 100 || parseFloat($("#spAllCount").text()) < 100) {
                alert("比例合计应等于100%，请重新分配！");
                return false;
            }

            //确认提交
            if (!confirm('确认要提交？')) {
                return false;
            }

            var unitArr = new Array();

            $("#tbData tbody tr").each(function (index) {
                var obj = $(this);
                //创建对象
                unitArr[index] = {
                    "ID": "0",
                    "ProKHNameID": $("#hidKHNameID").val(),
                    "SpeRoleID": "-3",//部门经理
                    "SpeID": obj.find("td").eq(2).find("span").attr("speid"),
                    "SpeName": obj.find("td").eq(2).find("span").attr("spename"),
                    "Persentold": obj.find("td").eq(1).find("span").text(),
                    "Persent1": obj.find("td").eq(2).find("span").text(),
                    "Persent3": obj.find("td").eq(3).find("input").val(),
                    "InserUserID3": $("#hidUserSysNo").val(),
                    "Stat3": "1"
                };
            });

            var jsonData = JSON.stringify(unitArr);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            
            var action = "savespeprst3";
            var jixiao = $("#selJixiao").val();
            //考核ID与发送人ID
            var khnameid = $("#hidKHNameID").val();
            var senderid = $("#hidUserSysNo").val();
            var data = { "action": action, "data": jsonData, "jixiao": jixiao, "khnameid": khnameid, "senderid": senderid };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("专业间比例审批成功,总建筑师审批！");

                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();

                        location.reload();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });

        //绩效
        if ($("#hidJixiao").val() == "0.0") {
            $("#selJixiao").val("1.0");
        }
        else {
            $("#selJixiao").val($("#hidJixiao").val());
        }


        //如果考核名称已经删除
        if ($("#hidKaoHeNameID").val() == "0") {

            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.DoneMsg();

            $("#btnSave").hide()
            $("#btnSubmit").hide();

            $("#btnSave2").hide();
            $("#btnSubmit2").hide();
        }


        if ($("#hidIsSubmit").val() == "True") {
            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.DoneMsg();
        }
        else {
            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.ReadMsg();
        }
    }


    return {
        init: function () {
            pageLoad();
        }
    }
}();