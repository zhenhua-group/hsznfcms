﻿

var nameList = function () {

    var pageLoad = function () {

        //添加考核名称
        $("#btnaddprojName").click(function () {

            if ($("#hidTempProid").val() == "0" || $("#hidTempUnitid").val() == "0") {
                alert("请选择项目！");
                return false;
            }

            if ($.trim($("#hidTempKaoHeProjId").val()) == "0" || $.trim($("#hidTempKaoHeProjId").val()) == "") {
                alert("项目未设置为考核项目！");
                return false;
            }

            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            if ($.trim($("#txtKName").val()) == "") {
                alert("考核名称不能为空！");
                return false;
            }
            else {
                if ($.trim($("#txtKName").val()).length > 30) {
                    alert("考核名称为空！");
                    return false;
                }
            }

            if ($.trim($("#txtAllScale").val()) == "") {
                alert("整体绩效不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($.trim($("#txtAllScale").val()))) {
                    alert("输入格式不能正确！");
                    return false;
                }
            }

            if ($.trim($("#txtDoneScale").val()) == "") {
                alert("完成比例不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($.trim($("#txtDoneScale").val()))) {
                    alert("输入格式不能正确！");
                    return false;
                }
            }

            if ($.trim($("#txtLeftScale").val()) == "") {
                alert("未完成不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($.trim($("#txtLeftScale").val()))) {
                    alert("输入格式不能正确！");
                    return false;
                }
            }

            if ($.trim($("#txtHisScale").val()) == "") {
                alert("未完成不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($.trim($("#txtHisScale").val()))) {
                    alert("输入格式不能正确！");
                    return false;
                }
            }
            //按钮不可用
            $("#btnaddprojName").attr("class", "btn btn-sm default disabled");

            var entityData = {
                ID: "0",
                KaoHeProjId: $("#hidTempKaoHeProjId").val(),
                KName: $("#txtKName").val(),
                AllScale: $("#txtAllScale").val(),
                DoneScale: $("#txtDoneScale").val(),
                Sub: $("#txtSub").val(),
                LeftScale: $("#txtLeftScale").val(),
                HisScale: $("#txtHisScale").val(),
                proID: $("#hidTempProid").val(),
                InserUserID: $("#hidUserSysNo").val()
            };

            if ($("#hidTempProKHNameid").val() != "0") {
                entityData.ID = $("#hidTempProKHNameid").val();
            }

            var jsonData = JSON.stringify(entityData);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savekhname";
            var data = { "action": action, "data": jsonData };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("更新考核名称成功！");
                        //加载名称
                        $("#ctl00_ContentPlaceHolder1_btn_Search").get(0).click();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });


        });
        //考核编辑
        $("a[action='edit']", $("#tbNameList tbody")).live('click', function () {
            
            getKaoHeNameInfo($(this));

            getKaoHeTypeInfo($(this));
        });
        //保存分类
        $("#btnSaveType").click(function () {

            if ($("#hidTempTypeProjNameid").val() == "0") {
                alert("请选择考核名称！");
                return false;
            }

            if ($("#selType").val() == "0") {
                alert("请选择考核类型！");
                return false;
            }

            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            if ($.trim($("#txtRealScale").val()) == "") {
                alert("实际规模不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($.trim($("#txtRealScale").val()))) {
                    alert("输入格式不能正确！");
                    return false;
                }
            }

            if ($.trim($("#txtTypeXishu").val()) == "") {
                alert("系数不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($.trim($("#txtTypeXishu").val()))) {
                    alert("输入格式不能正确！");
                    return false;
                }
            }
            //按钮不可用
            $("#btnSaveType").attr("class", "btn btn-sm default disabled");

            var entityData = {
                ID: "0",
                KaoheNameID: $("#hidTempTypeProjNameid").val(),
                KaoheTypeID: $("#selType option:selected").val(),
                KaoheTypeName: $("#selType option:selected").text(),
                RealScale: $("#txtRealScale").val(),
                TypeXishu: $("#txtTypeXishu").val(),
                InserUserID: $("#hidUserSysNo").val(),
                virallotCount: $("#spVirCount").text()
            };

            if ($("#hidTempTypeId").val() != "0") {
                entityData.ID = $("#hidTempTypeId").val();
            }

            var jsonData = JSON.stringify(entityData);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savetype";
            var data = { "action": action, "data": jsonData };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("添加考核类型成功！");
                        //加载名称
                        $("#ctl00_ContentPlaceHolder1_btn_Search").get(0).click();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
        //给文本框绑定时间
        $(":text", $("#tbTypeAdd")).live("keyup", function () {
            getvirAllotCount();
        });

        //类型绑定
        $("#selType").change(function () {
            getvirAllotCount();
        });
    }
    //获取项目历史完成比例
    var getHisComplete = function (proid) {

        var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
        var action = "gethiscomplete";
        var data = { "action": action, "projid": proid };

        $.get(url, data, function (result) {

            if (result) {

                var complete = parseFloat(result);
                if (complete > 0) {
                    $("#txtHisScale").val(complete);

                    //历史完成比例
                    $("#txtHisScale").attr("style", "width: 120px;border:none;").attr("readonly", true);
                    //历史未完成比例
                    $("#txtLeftScale").attr("style", "width: 120px;border:none;").attr("readonly", true);

                    $("#txtDoneScale").blur(function () {

                        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

                        var donescale = $("#txtDoneScale").val();
                        if (!reg_math.test(donescale)) {
                            $("#txtDoneScale").val(0);
                            donescale = "0";
                        }

                        var leftscale = 100 - parseFloat(donescale) - parseFloat(complete);

                        $("#txtLeftScale").val(leftscale);
                        var hisscale = $("#txtHisScale").val();

                        var hj = 0;
                        //计算合计
                        hj = parseFloat(donescale) + parseFloat(leftscale) + parseFloat(hisscale);

                        $("#txtHeji").val(hj);

                    });

                    $("#txtHeji").blur(function () {

                        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

                        var donescale = $("#txtDoneScale").val();
                        if (!reg_math.test(donescale)) {
                            $("#txtDoneScale").val(0);
                            donescale = "0";
                        }
                        var hj = $("#txtHeji").val();
                        if (!reg_math.test(hj)) {
                            $("#txtHeji").val(0);
                            hj = "0";
                        }
                        var hisscale = $("#txtHisScale").val();

                        var left = parseFloat(hj) - parseFloat(donescale) - parseFloat(hisscale);

                        $("#txtLeftScale").val(left);
                    });
                }
                else {

                    //当期完成比例
                    $("#txtDoneScale").blur(function () {

                        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

                        var donescale = $("#txtDoneScale").val();
                        if (!reg_math.test(donescale)) {
                            $("#txtDoneScale").val(0);
                            donescale = "0";
                        }
                        var leftscale = $("#txtLeftScale").val();
                        if (!reg_math.test(leftscale)) {
                            $("#txtLeftScale").val(0);
                            leftscale = "0";
                        }
                        var hisscale = $("#txtHisScale").val();
                        if (!reg_math.test(hisscale)) {
                            $("#txtHisScale").val(0);
                            hisscale = "0";
                        }

                        var hj = 0;
                        //计算合计
                        hj = parseFloat(donescale) + parseFloat(leftscale) + parseFloat(hisscale);

                        $("#txtHeji").val(hj);
                    });
                    //历史完成比例
                    $("#txtHisScale").blur(function () {
                        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

                        var donescale = $("#txtDoneScale").val();
                        if (!reg_math.test(donescale)) {
                            $("#txtDoneScale").val(0);
                            donescale = "0";
                        }
                        var leftscale = $("#txtLeftScale").val();
                        if (!reg_math.test(leftscale)) {
                            $("#txtLeftScale").val(0);
                            leftscale = "0";
                        }
                        var hisscale = $("#txtHisScale").val();
                        if (!reg_math.test(hisscale)) {
                            $("#txtHisScale").val(0);
                            hisscale = "0";
                        }

                        var hj = 0;
                        //计算合计
                        hj = parseFloat(donescale) + parseFloat(leftscale) + parseFloat(hisscale);
                        $("#txtHeji").val(hj);
                    }).attr("style", "width: 120px;").removeAttr("readonly");
                    //历史未完成比例
                    $("#txtLeftScale").blur(function () {
                        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

                        var donescale = $("#txtDoneScale").val();
                        if (!reg_math.test(donescale)) {
                            $("#txtDoneScale").val(0);
                            donescale = "0";
                        }
                        var leftscale = $("#txtLeftScale").val();
                        if (!reg_math.test(leftscale)) {
                            $("#txtLeftScale").val(0);
                            leftscale = "0";
                        }
                        var hisscale = $("#txtHisScale").val();
                        if (!reg_math.test(hisscale)) {
                            $("#txtHisScale").val(0);
                            hisscale = "0";
                        }

                        var hj = 0;
                        //计算合计
                        hj = parseFloat(donescale) + parseFloat(leftscale) + parseFloat(hisscale);
                        $("#txtHeji").val(hj);
                    }).attr("style", "width: 120px;").removeAttr("readonly");;
                }
            }
        });

        initJiSuan();
    }
    //获取考核名称xinxin
    var getKaoHeNameInfo = function (obj) {

        //var obj = $(this);
        var id = obj.attr("pronameid");
        var proid = obj.attr("proid");
        var prokhid = obj.attr("prokhid");

        var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
        var action = "getproname";
        var data = { "action": action, "id": id };

        $.get(url, data, function (result) {

            if (result) {
                if (result == "1") {
                    alert("查询失败");
                    return false;
                }
                else {
                    var model = eval("(" + result + ")");

                    $("#hidTempProKHNameid").val(model.ID);
                    $("#txtKName").val(model.KName);
                    $("#txtAllScale").val(model.AllScale);
                    $("#txtSub").val(model.Sub);
                    $("#txtDoneScale").val(model.DoneScale);
                    $("#txtLeftScale").val(model.LeftScale);
                    $("#txtHisScale").val(model.HisScale);
                }
            }
        });

        $("#btnaddprojName").attr("class", "btn btn-sm red");
        $("#tbNameAdd").show();
        //项目ID
        $("#hidTempProid").val(proid);
        //项目考核ID
        $("#hidTempKaoHeProjId").val(prokhid);
        //考核名称ID
        $("#hidTempTypeProjNameid").val(id);

        getHisComplete(proid);
    }
    //获取考核分类信息
    var getKaoHeTypeInfo = function (obj) {
        //var obj = $(this);
        var id = obj.attr("typeid");

        var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
        var action = "gettypeone";
        var data = { "action": action, "id": id };

        $.get(url, data, function (result) {

            if (result) {
                if (result == "1") {
                    alert("查询失败");
                    return false;
                }
                else {
                    var model = eval("(" + result + ")");

                    $("#selType").val(model.KaoheTypeID);
                    $("#txtRealScale").val(model.RealScale);
                    $("#txtTypeXishu").val(model.TypeXishu);
                    $("#hidTempTypeId").val(model.ID);
                    $("#spVirCount").text(model.virallotCount);
                }
            }
        });

        $("#tbTypeAdd").show();
    }
    //计算虚拟产值
    var getvirAllotCount = function () {

        //得到选中的类型
        var seltype = $("#selType option:selected");
        if (seltype.val() == "0") {
            alert("请选择分配类型！");
            //结果
            $("#spVirCount").text("0");
            return false;
        }
        var realScale = parseFloat($("#txtRealScale").val());
        var xishu = parseFloat($("#txtTypeXishu").val());
        if ($("#txtRealScale").val() == "") {
            realScale = 0;
        }
        if ($("#txtTypeXishu").val() == "") {
            xishu = 0;
        }
        //type10+(type5-type10)*(realscale-type6)/(type1-type6)
        var type1 = parseFloat(seltype.attr("type1"));
        var type5 = parseFloat(seltype.attr("type5"));
        var type6 = parseFloat(seltype.attr("type6"));
        var type10 = parseFloat(seltype.attr("type10"));
        //实际虚拟计算总产值
        var readvirCount = type10 + (type5 - type10) * (realScale - type6) / (type1 - type6);
        //实际虚拟总产值
        var virAllotCount = readvirCount * xishu;
        //结果
        $("#spVirCount").text(virAllotCount.toFixed(2));
    }
    var initJiSuan = function () {
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

        var donescale = $("#txtDoneScale").val();
        if (!reg_math.test(donescale)) {
            $("#txtDoneScale").val(0);
            donescale = "0";
        }
        var leftscale = $("#txtLeftScale").val();
        if (!reg_math.test(leftscale)) {
            $("#txtLeftScale").val(0);
            leftscale = "0";
        }
        var hisscale = $("#txtHisScale").val();
        if (!reg_math.test(hisscale)) {
            $("#txtHisScale").val(0);
            hisscale = "0";
        }

        var hj = 0;
        //计算合计
        hj = parseFloat(donescale) + parseFloat(leftscale) + parseFloat(hisscale);

        $("#txtHeji").val(hj);
    }
    return {
        init: function () {
            pageLoad();
        }
    }
}();