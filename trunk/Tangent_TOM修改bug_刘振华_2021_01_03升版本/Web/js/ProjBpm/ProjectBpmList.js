﻿
var ProjBpm = function () {

    showDivDialogClass.UserRolePower = {
        "previewPower": "1",
        "userSysNum": "0",
        "userUnitNum": "0",
        "notShowUnitList": ""
    };
    var _globalContainer;

    var pageLoad = function () {

        //全选
        $("#chkAll").click(function () {
            var obj = $(this);

            if (!obj.get(0).checked) {
                $(":checkbox", $("#tbData tbody")).not(this).attr("checked", !obj.checked).parent().attr("class", "");
            }
            else {
                $(":checkbox", $("#tbData tbody")).not(this).attr("checked", true).parent().attr("class", "checked");
            }
        });
        //选择任务名加载设置项目
        $(":checkbox[action='renwu']").click(function () {

            var obj = $(this);
            if (obj.is(":checked")) {
                $(":checkbox[action='renwu']").attr('checked', true);
                obj.attr('checked', false);

                //loadList(obj);
                //$("#hidRenwuId").val(obj.val());
                //初始化
                $("#tbTypeList").hide();
                $("#tbNameAdd").hide();
                $("#tbNameDetails").hide();
                $("#tbTypeAdd").hide();
                $("#tbNameList tbody tr").remove();
            }
            else {
                $(":checkbox[action='renwu']").attr('checked', false);
                obj.attr('checked', true);
            }
        });

        //设置考核项目
        $("#btnSaveProj").click(function () {
            //选择分配计划
            if ($("#tbData thead").find(":checkbox[action]").is(":checked") == false) {
                alert("请选择分配任务！");
                return false;
            }

            var unitArr = new Array();

            $("#tbData tbody tr").each(function (index) {
                var obj = $(this);

                if (obj.find("td").eq(0).find(":checkbox").attr("checked") == "checked") {
                    //创建对象
                    unitArr[index] = {
                        "ID": "0",
                        "RenWuId": $("#tbData thead").find(":checkbox[checked=checked]").val(),
                        "proID": obj.find("td").eq(2).find("span").attr("proid"),
                        "proName": obj.find("td").eq(2).find("span").text(),
                        "UnitName": obj.find("td").eq(2).find("span").attr("unitname"),
                        "UnitID": obj.find("td").eq(2).find("span").attr("unitid"),
                        "IsIn": obj.find("td").eq(3).find(":selected").val(),
                        "Statu": "0",
                        "InsertUserId": $("#hidUserSysNo").val()
                    };
                }
            });

            var jsonData = JSON.stringify(unitArr);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "saveproj";
            var data = { "action": action, "data": jsonData };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("设置考核项目成功！");
                        loadList($("#hidRenwuId"));
                        $("#ctl00_ContentPlaceHolder1_btn_Search").get(0).click();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
        //初始化加载
        if ($("#hidRenwuId").val() == "0") {
            loadList($("#ctl00_ContentPlaceHolder1_drprenwu"));
        }
        else {
            //如果任务iD为空，默认加载第一个任务
            //$("#ctl00_ContentPlaceHolder1_drprenwu").val($("#hidRenwuId").val());
            loadList($("#ctl00_ContentPlaceHolder1_drprenwu"));
        }
        //添加考核名称
        $("#tbData a[asetmem=addname]").click(function () {
            var obj = $(this);
            //项目与部门id
            var proid = obj.attr("proid");
            var unitid = obj.attr("unitid");
            var kprojid = obj.attr("kprojid");
            var proname = obj.attr("proname");

            //赋值
            $("#spProjName").text(proname);
            $("#spProjName2").text(proname);
            $("#hidTempProid").val(proid);
            $("#hidTempUnitid").val(unitid);
            $("#hidTempKaoHeProjId").val(kprojid);
            //标示添加与编辑
            $("#hidTempProKHNameid").val("0");
            //加载名称
            loadNameList();

            $("#tbNameAdd").show();
            $("#tbNameDetails").hide();
            $("#tbTypeList").hide();
            $("#tbTypeAdd").hide();
            $("#tbNameAdd :text").val('');
            $("#tbSetMems").hide();

            //按钮可用
            $("#btnaddprojName").attr("class", "btn btn-sm red");

            //获取历史值
            getCurComplete();
        });
        //添加考核名称
        $("#btnaddprojName").click(function () {

            if ($("#hidTempProid").val() == "0" || $("#hidTempUnitid").val() == "0") {
                alert("请选择项目！");
                return false;
            }

            if ($.trim($("#hidTempKaoHeProjId").val()) == "0" || $.trim($("#hidTempKaoHeProjId").val()) == "") {
                alert("项目未设置为考核项目！");
                return false;
            }

            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            if ($.trim($("#txtKName").val()) == "") {
                alert("考核名称不能为空！");
                return false;
            }
            else {
                if ($.trim($("#txtKName").val()).length > 30) {
                    alert("考核名称为空！");
                    return false;
                }
            }

            if ($.trim($("#txtAllScale").val()) == "") {
                alert("整体绩效不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($.trim($("#txtAllScale").val()))) {
                    alert("输入格式不能正确！");
                    return false;
                }
            }

            if ($.trim($("#txtDoneScale").val()) == "") {
                alert("完成比例不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($.trim($("#txtDoneScale").val()))) {
                    alert("输入格式不能正确！");
                    return false;
                }
            }

            if ($.trim($("#txtLeftScale").val()) == "") {
                alert("未完成不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($.trim($("#txtLeftScale").val()))) {
                    alert("输入格式不能正确！");
                    return false;
                }
            }

            if ($.trim($("#txtHisScale").val()) == "") {
                alert("未完成不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($.trim($("#txtHisScale").val()))) {
                    alert("输入格式不能正确！");
                    return false;
                }
            }
            //按钮不可用
            $("#btnaddprojName").attr("class", "btn btn-sm default disabled");

            var entityData = {
                ID: "0",
                KaoHeProjId: $("#hidTempKaoHeProjId").val(),
                KName: $("#txtKName").val(),
                AllScale: $("#txtAllScale").val(),
                DoneScale: $("#txtDoneScale").val(),
                Sub: $("#txtSub").val(),
                LeftScale: $("#txtLeftScale").val(),
                HisScale: $("#txtHisScale").val(),
                proID: $("#hidTempProid").val(),
                InserUserID: $("#hidUserSysNo").val()
            };

            if ($("#hidTempProKHNameid").val() != "0") {
                entityData.ID = $("#hidTempProKHNameid").val();
            }

            var jsonData = JSON.stringify(entityData);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savekhname";
            //考核历史数据的时候，历史考核名称ID
            var kaohenameidold = $("#hidTempProKHNameidOld").val();
            var data = { "action": action, "data": jsonData, "kaohenameidold": kaohenameidold };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        if ($("#hidDoFlag").val() == "edit") {
                            alert("考核名称编辑成功！");
                        }
                        else {
                            alert("添加考核名称成功！");
                        }
                        //加载名称
                        loadNameList();
                        $("#tbNameAdd :text").val("");
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });


        });
        //取消
        $("#btnProNameCancel").click(function () {
            $("#tbNameAdd").hide();
            $("#tbNameAdd :text").val("");
        });
        //查看考核名称
        $("a[action='check']", $("#tbNameList tbody")).live('click', function () {
            var obj = $(this);
            var id = obj.attr("pronameid");

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "getproname";
            var data = { "action": action, "id": id };

            $.get(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        alert("查询失败");
                        return false;
                    }
                    else {
                        var model = eval("(" + result + ")");

                        $("#spKName").text(model.KName);
                        $("#spAllScale").text(model.AllScale);
                        $("#spSub").text(model.Sub);
                        $("#spDoneScale").text(model.DoneScale);
                        $("#spLeftScale").text(model.LeftScale);
                        $("#spHisScale").text(model.HisScale);
                    }
                }
            });

            $("#tbNameAdd").hide();
            $("#tbNameDetails").show();
            $("#tbTypeList").hide();
            $("#tbTypeAdd").hide();
            $("#tbSetMems").hide();
        });

        //考核名称编辑
        $("a[action='edit']", $("#tbNameList tbody")).live('click', function () {
            var obj = $(this);
            var id = obj.attr("pronameid");
            var doflag = obj.attr("doflag");

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "getproname";
            var data = { "action": action, "id": id };

            //保存状态
            $("#hidDoFlag").val(doflag);

            $.get(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        alert("查询失败");
                        return false;
                    }
                    else {
                        var model = eval("(" + result + ")");
                        //本次考核编辑
                        if (doflag == "edit") {
                            $("#hidTempProKHNameid").val(model.ID);
                            $("#hidTempProKHNameidOld").val("0");
                            $("#txtKName").val(model.KName);
                            $("#txtAllScale").val(model.AllScale);
                            $("#txtSub").val(model.Sub);
                            $("#txtDoneScale").val(model.DoneScale);
                            $("#txtLeftScale").val(model.LeftScale);
                            $("#txtHisScale").val(model.HisScale);
                            $("#txtHeji").val(model.LeftScale + model.HisScale + model.DoneScale);
                            //自动计算
                            getCurComplete();
                        }
                        else {//上次考核的继续考核
                            //新建
                            $("#hidTempProKHNameid").val("0");
                            //考核名称ID
                            $("#hidTempProKHNameidOld").val(model.ID);
                            $("#txtKName").val(model.KName);
                            $("#txtAllScale").val(model.AllScale);
                            $("#txtSub").val(model.Sub);
                            //本次考核为0
                            $("#txtDoneScale").val(0);
                            $("#txtLeftScale").val(model.LeftScale);
                            $("#txtHisScale").val(model.HisScale + model.DoneScale);
                            $("#txtHeji").val(model.LeftScale + model.HisScale + model.DoneScale);

                            //自动计算
                            getHisComplete();
                        }
                    }
                }
            });

            $("#btnaddprojName").attr("class", "btn btn-sm red");
            $("#tbNameAdd").show();
            $("#tbNameDetails").hide();
            $("#tbTypeAdd").hide();
            $("#tbSetMems").hide();
        });

        //考核删除
        $("a[action='del']", $("#tbNameList tbody")).live('click', function () {
            var obj = $(this);
            var id = obj.attr("pronameid");

            if (confirm("确认删除？")) {
                var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
                var action = "delproname";
                var data = { "action": action, "id": id };

                $.get(url, data, function (result) {

                    if (result) {
                        if (result == "1") {
                            alert("删除成功！");
                            loadNameList();
                        }
                        else {
                            alert("删除失败！");
                            return false;
                        }
                    }
                });
            }
        });
        //查看分类
        $("a[action='checktype']", $("#tbNameList tbody")).live('click', function () {
            var obj = $(this);
            var id = obj.attr("pronameid");
            //加载分类
            loadtype(id);

            $("#tbNameDetails").hide();
            $("#tbNameAdd").hide();
            $("#tbTypeList").show();
            $("#tbTypeAdd").hide();
            $("#tbSetMems").hide();
        });
        //添加分类
        $("a[action='addtype']", $("#tbNameList tbody")).live('click', function () {
            var obj = $(this);
            var id = obj.attr("pronameid");

            loadtype(id);

            $("#tbNameDetails").hide();
            $("#tbNameAdd").hide();
            $("#tbTypeList").show();
            $("#tbTypeAdd").show();
            //新建时类型ID为0
            $("#hidTempTypeId").val("0");
            $("#tbSetMems").hide();
            //按钮可用
            $("#btnSaveType").attr("class", "btn btn-sm red");
        });

        //保存分类
        $("#btnSaveType").click(function () {

            if ($("#hidTempTypeProjNameid").val() == "0") {
                alert("请选择考核名称！");
                return false;
            }

            if ($("#selType").val() == "0") {
                alert("请选择考核类型！");
                return false;
            }

            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            if ($.trim($("#txtRealScale").val()) == "") {
                alert("实际规模不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($.trim($("#txtRealScale").val()))) {
                    alert("输入格式不能正确！");
                    return false;
                }
            }

            if ($.trim($("#txtTypeXishu").val()) == "") {
                alert("系数不能为空！");
                return false;
            }
            else {
                if (!reg_math.test($.trim($("#txtTypeXishu").val()))) {
                    alert("输入格式不能正确！");
                    return false;
                }
            }
            //按钮不可用
            $("#btnSaveType").attr("class", "btn btn-sm default disabled");

            var entityData = {
                ID: "0",
                KaoheNameID: $("#hidTempTypeProjNameid").val(),
                KaoheTypeID: $("#selType option:selected").val(),
                KaoheTypeName: $("#selType option:selected").text(),
                RealScale: $("#txtRealScale").val(),
                TypeXishu: $("#txtTypeXishu").val(),
                InserUserID: $("#hidUserSysNo").val(),
                virallotCount: $("#spVirCount").text()
            };

            if ($("#hidTempTypeId").val() != "0") {
                entityData.ID = $("#hidTempTypeId").val();
            }

            var jsonData = JSON.stringify(entityData);

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savetype";
            var data = { "action": action, "data": jsonData };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("添加考核类型成功！");
                        //加载名称
                        loadtype($("#hidTempTypeProjNameid").val());

                        $("#tbTypeAdd :text").val("");
                        loadNameList();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
        //取消
        $("#btnSaveTypeCancel").click(function () {
            $("#tbTypeAdd").hide();
            $("#tbTypeAdd :text").val("");
        });
        //分类编辑
        $("a[action='edit']", $("#tbTypeList tbody")).live('click', function () {
            var obj = $(this);
            var id = obj.attr("id");

            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "gettypeone";
            var data = { "action": action, "id": id };

            $.get(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        alert("查询失败");
                        return false;
                    }
                    else {
                        var model = eval("(" + result + ")");

                        $("#selType").val(model.KaoheTypeID);
                        $("#txtRealScale").val(model.RealScale);
                        $("#txtTypeXishu").val(model.TypeXishu);
                        $("#hidTempTypeId").val(model.ID);
                        $("#spVirCount").text(model.virallotCount);
                    }
                }
            });

            $("#tbTypeAdd").show();

            $("#btnSaveType").attr("class", "btn btn-sm red");
        });
        //分类删除
        $("a[action='del']", $("#tbTypeList tbody")).live('click', function () {
            var obj = $(this);
            var id = obj.attr("id");

            if (confirm("确认删除？")) {
                var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
                var action = "deltype";
                var data = { "action": action, "id": id };

                $.get(url, data, function (result) {

                    if (result) {
                        if (result == "1") {
                            alert("删除成功！");
                            loadtype($("#hidTempTypeProjNameid").val());
                        }
                        else {
                            alert("删除失败！");
                            return false;
                        }
                    }
                });
            }
        });

        //给文本框绑定时间
        $(":text", $("#tbTypeAdd")).live("keyup", function () {
            getvirAllotCount();
        });
        //类型绑定
        $("#selType").change(function () {
            getvirAllotCount();
        });
        //添加用户
        $("a[action='setusers']", $("#tbNameList tbody")).live('click', function () {
            $("#tbSetMems").show();
            var tip = $(this).parent().parent().find("td").eq(0).text();
            $("#roleTip").text(tip);
            //保存临时考核名称ID
            $("#hidTempTypeProjNameid").val($(this).attr("id"));
            $("#hidTempTypeId").val($(this).attr("id"));
            //加载
            loadsepcmems();

            $("#btnSaveMems").attr("class", "btn btn-sm red");

            $("#tbTypeAdd").hide();
        });

        $("a[action='deluser']", $("#tbSetMems tbody")).live('click', function () {
            $(this).remove();
        });
        //添加用户
        $("a[action='adduser']", $("#tbSetMems tbody")).live('click', function () {
            $("#tbSetMems").show();
            //保存当前选择
            _globalContainer = $(this);
            //先赋值
            showDivDialogClass.SetParameters({
                "prevPage": "gcfzr_prevPage",
                "firstPage": "gcfzr_firstPage",
                "nextPage": "gcfzr_nextPage",
                "lastPage": "gcfzr_lastPage",
                "gotoPage": "gcfzr_gotoPageIndex",
                "allDataCount": "gcfzr_allDataCount",
                "nowIndex": "gcfzr_nowPageIndex",
                "allPageCount": "gcfzr_allPageCount",
                "gotoIndex": "gcfzr_pageIndex",
                "pageSize": "10"
            });
            var unit_ID = $("#select_gcFzr_Unit").val();
            if (unit_ID < 0 || unit_ID == null) {
                //绑定工程负责部门
                showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "gcfzrCprUnit", CprTypeUnitCallBack);
            }

        });
        //取消人员保存
        $("#btnSaveMemsCancel").click(function () {
            $("#tbSetMems").hide();
        });
        //保存用户
        $("#btnSaveMems").click(function () {
            var memArray = new Array();
            var index = 0;
            var isAll = 0;
            $("#tbSetMems tbody tr").each(function (i, item) {
                //专业
                var spename = $(this).find("td").eq(2).find("a").attr("spename");
                var speid = $(this).find("td").eq(2).find("a").attr("speid");
                //对象
                var obj = $(this);
                var container = obj.find("td").eq(1).find("a");
                if (container) {
                    if (container.length > 0) {
                        isAll++;
                    }
                    container.each(function (j, item2) {
                        memArray[index] = {
                            ID: "0",
                            ProId: $("#hidTempProid").val(),//项目ID
                            ProKHId: $("#hidTempKaoHeProjId").val(), //项目考核ID
                            ProKHNameId: $("#hidTempTypeProjNameid").val(),//考核名称ID
                            KHTypeId: $("#hidTempTypeId").val(),
                            MemID: $(this).attr("memid"),
                            MemName: $(this).attr("memname"),
                            SpeName: spename,
                            SpeID: speid
                        };

                        index++;
                    });
                }
            });
            //判断是否添加人
            if (memArray.length == 0) {
                alert("请添加部门经理！");
                return false;
            }

            if (isAll < (7 - parseInt($("#hidIsSetMemsCount").val()))) {
                alert("请完整添加各个部门经理！");
                return false;
            }
            var jsonEntity = JSON.stringify(memArray);


            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savespecmems";
            var senderid = $("#hidUserSysNo").val();
            var khtypeid = $("#hidTempTypeId").val();
            var data = { "action": action, "data": jsonEntity, "senderid": senderid, "KHtypeid": khtypeid };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("设置部门经理人成功！");
                        //按钮不可用
                        $("#btnSaveMems").attr("class", "btn btn-sm default disabled");
                        $("#spSaveStat").text("已保存");
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
    }
    //获取考核名称
    var loadNameList = function () {
        //项目ID
        var proid = $("#hidTempProid").val();
        //考核ID
        var khproid = $("#hidTempKaoHeProjId").val();
        //任务id
        var renwuid = $("#ctl00_ContentPlaceHolder1_drprenwu").val();

        var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
        var action = "getkhname";
        var data = { "action": action, "projid": proid, "khproid": khproid, "renwuid": renwuid };

        $.get(url, data, function (result) {

            if (result) {
                var tbContainer = $("#tbNameList tbody");
                tbContainer.find("tr").remove();
                var tr = "";
                if (result == "1") {
                    tr += "<tr><td colspan=\"4\">无考核名称！</td></tr>";
                }
                else {
                    var list = eval("(" + result + ")");
                    $.each(list, function (index, item) {
                        tr += "<tr>";
                        tr += "<td>" + item.KName + "</td>";
                        tr += "<td><a class=\"btn default btn-xs green-stripe\" href=\"###\" pronameid=\"" + item.ID + "\" action=\"checktype\">查看</a>";
                        tr += "<a class=\"btn default btn-xs green-stripe\" href=\"###\" pronameid=\"" + item.ID + "\" action=\"addtype\">添加</a></td>";
                        tr += "<td><a class=\"btn default btn-xs green-stripe\" href=\"###\" pronameid=\"" + item.ID + "\" action=\"check\">查看</a>";
                        if (item.RenwuID == renwuid) {
                            tr += "<a class=\"btn default btn-xs green-stripe\" href=\"###\" pronameid=\"" + item.ID + "\" action=\"edit\" doflag=\"edit\">编辑</a>";
                        }
                        else {
                            tr += "<a class=\"btn default btn-xs green-stripe\" href=\"###\" pronameid=\"" + item.ID + "\" action=\"edit\" doflag=\"add\">编辑</a>";
                        }
                        tr += "<a class=\"btn default btn-xs green-stripe\" href=\"###\" pronameid=\"" + item.ID + "\" action=\"del\">删除</a></td>";
                        tr += "<td>" + item.ViralCount + "</td>";
                        tr += "<td>" + (item.HisScale + item.DoneScale) + "%</td>";
                        tr += "<td>" + item.LeftScale + "%</td>";
                        tr += "<td>" + item.RenwuName + "</td>";
                        if (item.RenwuID == renwuid) {
                            tr += "<td><a class=\"btn default btn-xs green-stripe\" href=\"###\" id=\"" + item.ID + "\" action=\"setusers\"><i class=\"fa fa-plus\">考核人</i></a></td>";
                        }
                        else {
                            tr += "<td></td>";
                        }
                        tr += "<td></td>";
                        tr += "</tr>";
                    });

                }

                tbContainer.append(tr);
            }
        });
    }
    //加载部门考核状态
    var loadList = function (obj) {
        var obj = obj;
        if (obj.val() != "0") {
            $("#btnSaveProj").attr("class", "btn btn-sm red");
        }
        else {
            $("#btnSaveProj").attr("class", "btn btn-sm default disabled");
            return false;
        }
        //获取设置的考核部门
        var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
        var id = obj.val();
        var action = "getproj";
        var data = { "action": action, "renwuid": id, "uid": $("#ctl00_ContentPlaceHolder1_drp_unit3").val() };

        $.get(url, data, function (result) {
            if (result) {

                if (result == "1") {

                    $("#tbData tbody tr").each(function (index) {
                        var obj = $(this);
                        //打钩
                        obj.find("td").eq(0).find(":checkbox").attr("checked", "checked").parent().attr("class", "checked");
                        //是否参与
                        obj.find("td").eq(3).find("select").val("0");
                        obj.find("td").eq(4).find("a").attr("class", "btn default btn-xs green-stripe disabled");
                    });
                }
                else {
                    var jsonResult = eval("(" + result + ")");

                    for (i = 0; i < jsonResult.length; i++) {
                        var proid = jsonResult[i].proID;
                        $("#tbData tbody tr").each(function (index) {
                            var obj = $(this);
                            //当前部门
                            if (obj.find("td").eq(0).find(":checkbox").attr("proid") == proid) {
                                //打钩
                                obj.find("td").eq(0).find(":checkbox").attr("checked", "checked").parent().attr("class", "checked");
                                //是否参与
                                if (jsonResult[i].IsIn == 1) {
                                    obj.find("td").eq(3).find("select").val(jsonResult[i].IsIn);
                                    obj.find("td").eq(4).find("a").attr("class", "btn default btn-xs green-stripe disabled");

                                }
                                else {
                                    //设置设置用户ID
                                    obj.find("td").eq(4).find("a").attr("class", "btn default btn-xs green-stripe");
                                }
                                return false;
                            }
                        });
                    }
                }


            }
            else {
                //设置成功！
                alert("设置失败请重试！");
            }
        });
    }
    //加载考核类型
    var loadtype = function (id) {
        var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
        var action = "gettypelist";
        var data = { "action": action, "id": id };
        //考核名称ID
        $("#hidTempTypeProjNameid").val(id);

        $.get(url, data, function (result) {

            if (result) {
                var tbContainer = $("#tbTypeList tbody");
                tbContainer.find("tr").remove();
                var tr = "";
                if (result == "1") {
                    tr += "<tr><td colspan=\"4\">无考核分类！</td></tr>";
                }
                else {
                    var list = eval("(" + result + ")");
                    $.each(list, function (index, item) {
                        tr += "<tr>";
                        tr += "<td><span id=\"" + item.ID + "\" typeid=\"" + item.KaoheTypeID + "\">" + item.KaoheTypeName + "</td>";
                        tr += "<td>" + item.RealScale + "</td>";
                        tr += "<td>" + item.TypeXishu + "</td>";
                        tr += "<td>" + item.virallotCount + "</td>";
                        // tr += "<td><a class=\"btn default btn-xs green-stripe\" href=\"###\" id=\"" + item.ID + "\" action=\"setusers\"><i class=\"fa fa-plus\">考核人</i></a>";
                        tr += "<td><a class=\"btn default btn-xs green-stripe\" href=\"###\" id=\"" + item.ID + "\" action=\"edit\">编辑</a>";
                        tr += "<a class=\"btn default btn-xs green-stripe\" href=\"###\" id=\"" + item.ID + "\" action=\"del\">删除</a></td>";
                        tr += "<td></td>";
                        tr += "</tr>";
                    });
                }

                tbContainer.append(tr);
            }
        });
    }
    //计算虚拟产值
    var getvirAllotCount = function () {

        //得到选中的类型
        var seltype = $("#selType option:selected");
        if (seltype.val() == "0") {
            alert("请选择分配类型！");
            //结果
            $("#spVirCount").text("0");
            return false;
        }
        var realScale = parseFloat($("#txtRealScale").val());
        var xishu = parseFloat($("#txtTypeXishu").val());
        if ($("#txtRealScale").val() == "") {
            realScale = 0;
        }
        if ($("#txtTypeXishu").val() == "") {
            xishu = 0;
        }
        //type10+(type5-type10)*(realscale-type6)/(type1-type6)
        var type1 = parseFloat(seltype.attr("type1"));
        var type5 = parseFloat(seltype.attr("type5"));
        var type6 = parseFloat(seltype.attr("type6"));
        var type10 = parseFloat(seltype.attr("type10"));
        //实际虚拟计算总产值
        var readvirCount = type10 + (type5 - type10) * (realScale - type6) / (type1 - type6);
        //实际虚拟总产值
        var virAllotCount = readvirCount * xishu;
        //结果
        $("#spVirCount").text(virAllotCount.toFixed(2));
    }
    //加载设置的人员
    var loadsepcmems = function () {
        //考核类型ID
        var typeid = $("#hidTempTypeId").val();
        //
        var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
        var action = "getspecmems";
        //实例虚拟一个设置对象
        var memArray = new Array();
        memArray[0] = {
            ID: "0",
            ProId: $("#hidTempProid").val(),//项目ID
            ProKHId: $("#hidTempKaoHeProjId").val(), //项目考核ID
            ProKHNameId: $("#hidTempTypeProjNameid").val(),//考核名称ID
            KHTypeId: $("#hidTempTypeId").val(),
            MemID: "0",
            MemName: "",
            SpeName: "",
            SpeID: "0"
        };

        var jsonEntity = JSON.stringify(memArray);

        var data = { "action": action, "id": typeid, "data": jsonEntity };

        $.get(url, data, function (result) {

            if (result) {

                //判断是否已经设置了类型
                if (result == "3") {
                    alert("请添加至少一个考核类型！");
                    $("#btnSaveMems").attr("class", "btn btn-sm red disabled");
                    return false;
                }
                //专业人员
                var mems = eval("(" + result + ")");
                //设置人数
                var isSetMemsCount = 0;
                $("#tbSetMems tbody tr").each(function (i) {

                    var obj = $(this);
                    var speid = obj.find("td").eq(2).find("a").attr("speid");
                    //清空已经存在的人员信息
                    if (obj.find("td").eq(1).find("a").length > 0) {
                        obj.find("td").eq(1).find("a").remove();
                    }
                    //循环加载对应角色人员
                    $.each(mems, function (i, item) {
                        if (speid == item.SpeID) {
                            //排除MemID的人员，这里是特殊的主持人
                            if (item.MemID != -1) {
                                var html = "<a href=\"#\" title=\"点击删除\" class=\"btn btn-xs blue\" action=\"deluser\" id=\"" + item.ID + "\" memname=\"" + item.MemName + "\" memid=\"" + item.MemID + "\">" + item.MemName + "<i class=\"fa fa-times\"></i></a>";
                                //添加人员
                                obj.find("td").eq(1).append(html);

                                obj.find("td").eq(0).attr("style", "width:120px;");
                                obj.find("td").eq(1).attr("style", "width:200px;");
                                obj.find("td").eq(2).attr("style", "width:50px;");
                                obj.find("td").eq(2).find("a").attr("class", "btn default btn-xs green-stripe");
                            }
                            else {
                                obj.find("td").eq(0).attr("style", "background-color:#d8d8d8;color:#333333;");
                                obj.find("td").eq(1).attr("style", "background-color:#d8d8d8;color:#333333;");
                                obj.find("td").eq(2).find("a").attr("class", "btn default btn-xs green-stripe");
                                //如果为特殊的分配类型，不能编辑  2016年8月12日  后期需要改回来
                                //obj.find("td").eq(2).find("a").attr("class", "btn default btn-xs green-stripe disabled");

                                isSetMemsCount++;
                            }
                        }
                    });
                });

                //循环查找是否有主持人信息 qpl 2016-8-25
                $.each(mems, function (i, item) {
                    //主持人是否已经设置
                    if (item.SpeID == -2) {
                        $("#spSaveStat").text("已保存");
                        return false;
                    }
                    else {
                        $("#spSaveStat").text("未保存");
                    }
                });

                $("#hidIsSetMemsCount").val(isSetMemsCount);
                $("#btnSaveMems").attr("class", "btn btn-sm red");
            }
        });
    }
    //获取项目历史完成比例
    var getHisComplete = function (proid) {

        $("#txtDoneScale").unbind();
        $("#txtHisScale").unbind();
        $("#txtLeftScale").unbind();
        $("#txtHeji").unbind();
        //历史完成比例
        $("#txtHisScale").attr("style", "width: 120px;border:none;").attr("readonly", true);
        //历史未完成比例
        $("#txtLeftScale").attr("style", "width: 120px;border:none;").attr("readonly", true);
        //当前完成比例
        $("#txtDoneScale").blur(function () {

            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            var donescale = $("#txtDoneScale").val();
            if (!reg_math.test(donescale)) {
                $("#txtDoneScale").val(0);
                donescale = "0";
            }

            var leftscale = parseFloat($("#txtLeftScale").val()) - parseFloat(donescale);

            $("#txtLeftScale").val(leftscale);
            var hisscale = $("#txtHisScale").val();

            var hj = 0;
            //计算合计
            hj = parseFloat(donescale) + parseFloat(leftscale) + parseFloat(hisscale);

            $("#txtHeji").val(hj);

        });
        //改变合计
        $("#txtHeji").blur(function () {

            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            var donescale = $("#txtDoneScale").val();
            if (!reg_math.test(donescale)) {
                $("#txtDoneScale").val(0);
                donescale = "0";
            }
            var hj = $("#txtHeji").val();
            if (!reg_math.test(hj)) {
                $("#txtHeji").val(0);
                hj = "0";
            }
            var hisscale = $("#txtHisScale").val();

            var left = parseFloat(hj) - parseFloat(donescale) - parseFloat(hisscale);

            $("#txtLeftScale").val(left);
        });
    }
    //当年考核名称编辑文本框计算
    var getCurComplete = function () {

        $("#txtDoneScale").unbind();
        $("#txtHisScale").unbind();
        $("#txtLeftScale").unbind();
        $("#txtHeji").unbind();
        //历史完成比例
        $("#txtHisScale").attr("style", "").attr("readonly", false);
        //历史未完成比例
        $("#txtLeftScale").attr("style", "").attr("readonly", false);
        //当期完成比例
        $("#txtDoneScale").blur(function () {

            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            var donescale = $("#txtDoneScale").val();
            if (!reg_math.test(donescale)) {
                $("#txtDoneScale").val(0);
                donescale = "0";
            }
            var leftscale = $("#txtLeftScale").val();
            if (!reg_math.test(leftscale)) {
                $("#txtLeftScale").val(0);
                leftscale = "0";
            }
            var hisscale = $("#txtHisScale").val();
            if (!reg_math.test(hisscale)) {
                $("#txtHisScale").val(0);
                hisscale = "0";
            }

            var hj = 0;
            //计算合计
            hj = parseFloat(donescale) + parseFloat(leftscale) + parseFloat(hisscale);

            $("#txtHeji").val(hj);
        }).focus(function () { $(this).select(); });
        //历史完成比例
        $("#txtHisScale").blur(function () {
            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            var donescale = $("#txtDoneScale").val();
            if (!reg_math.test(donescale)) {
                $("#txtDoneScale").val(0);
                donescale = "0";
            }
            var leftscale = $("#txtLeftScale").val();
            if (!reg_math.test(leftscale)) {
                $("#txtLeftScale").val(0);
                leftscale = "0";
            }
            var hisscale = $("#txtHisScale").val();
            if (!reg_math.test(hisscale)) {
                $("#txtHisScale").val(0);
                hisscale = "0";
            }

            var hj = 0;
            //计算合计
            hj = parseFloat(donescale) + parseFloat(leftscale) + parseFloat(hisscale);
            $("#txtHeji").val(hj);
        }).focus(function () { $(this).select(); }).attr("style", "width: 120px;").removeAttr("readonly");
        //历史未完成比例
        $("#txtLeftScale").blur(function () {
            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            var donescale = $("#txtDoneScale").val();
            if (!reg_math.test(donescale)) {
                $("#txtDoneScale").val(0);
                donescale = "0";
            }
            var leftscale = $("#txtLeftScale").val();
            if (!reg_math.test(leftscale)) {
                $("#txtLeftScale").val(0);
                leftscale = "0";
            }
            var hisscale = $("#txtHisScale").val();
            if (!reg_math.test(hisscale)) {
                $("#txtHisScale").val(0);
                hisscale = "0";
            }

            var hj = 0;
            //计算合计
            hj = parseFloat(donescale) + parseFloat(leftscale) + parseFloat(hisscale);
            $("#txtHeji").val(hj);
        }).focus(function () { $(this).select(); });

        //改变合计
        $("#txtHeji").blur(function () {

            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            var donescale = $("#txtDoneScale").val();
            if (!reg_math.test(donescale)) {
                $("#txtDoneScale").val(0);
                donescale = "0";
            }
            var hj = $("#txtHeji").val();
            if (!reg_math.test(hj)) {
                $("#txtHeji").val(0);
                hj = "0";
            }
            var hisscale = $("#txtHisScale").val();

            var left = parseFloat(hj) - parseFloat(donescale) - parseFloat(hisscale);

            $("#txtLeftScale").val(left);
        }).focus(function () { $(this).select(); });
    }
    /*弹出选择人员*/
    function CprTypeUnitCallBack(result) {
        if (result != null) {

            var data = result.ds;
            var gcFzr_UnitOptionHtml = '<option value="-1">---------请选择---------</option>';
            $.each(data, function (i, n) {
                //过滤本部门
                if (n.unit_ID != $("#hidUnitid").val()) {
                    gcFzr_UnitOptionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
                }
            });
            $("#select_gcFzr_Unit").html(gcFzr_UnitOptionHtml);
            //注册部门选项改变事件
            showDivDialogClass.SetParameters({ "pageSize": "10" });
            $("#select_gcFzr_Unit").unbind('change').change(function () {
                var unit_ID = $("#select_gcFzr_Unit").val();
                if (Math.abs(unit_ID) > 0) {
                    showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", "1", "gcfzrCprMem", BindGcfzrDataCallBack);
                    BindAllDataCountGcfzr(unit_ID);
                }
            });
        }
    }
    function BindGcfzrDataCallBack(result) {
        if (result != null) {

            var obj = result.ds;
            var gcFzrMemTableHtml;
            $("#gcFzr_MemTable tr:gt(0)").remove();
            $.each(obj, function (i, n) {
                var oper = "<span rel='" + n.mem_Login + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\" >选择</span>";
                gcFzrMemTableHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.mem_Name + "</td><td>" + GetSpeName(n.mem_Speciality_ID) + "</td><td>" + oper + "</td></tr>";
                $("#gcFzr_MemTable").append(gcFzrMemTableHtml);
                $("#gcFzr_MemTable span:last").click(function () {

                    //添加人员
                    if (_globalContainer) {

                        var item = "<a href=\"#\" title=\"点击删除\" class=\"btn btn-xs blue\" action=\"deluser\" memname=\"" + n.mem_Name + "\" memid=\"" + n.mem_ID + "\">" + n.mem_Name + "<i class=\"fa fa-times\"></i></a>";
                        _globalContainer.parent().prev("td").append(item);
                    }
                });
            });

        }
    }
    function BindAllDataCountGcfzr(unit_ID) {
        //设置参数
        showDivDialogClass.SetParameters({
            "prevPage": "gcfzr_prevPage",
            "firstPage": "gcfzr_firstPage",
            "nextPage": "gcfzr_nextPage",
            "lastPage": "gcfzr_lastPage",
            "gotoPage": "gcfzr_gotoPageIndex",
            "allDataCount": "gcfzr_allDataCount",
            "nowIndex": "gcfzr_nowPageIndex",
            "allPageCount": "gcfzr_allPageCount",
            "gotoIndex": "gcfzr_pageIndex",
            "pageSize": "10"
        });
        //获取总数据
        showDivDialogClass.GetDataTotalCount("getDataAllCount", unit_ID, "true", "gcfzrCprMem", GetGcfzrAllDataCount);
        //注册事件,先注销,再注册
        $("#gcFzr_ForPageDiv span").unbind('click').click(function () {
            var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
            if (isRegex) {
                var pageIndex = $("#gcfzr_nowPageIndex").text();
                showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", pageIndex, "gcfzrCprMem", BindGcfzrDataCallBack);
            }
        });
    }
    function GetGcfzrAllDataCount(result) {
        if (result > 0) {
            showDivDialogClass.BindPageValueFirst(result);
        } else {
            $("#gcFzr_MemTable tr:gt(0)").remove();
            $("#gcfzr_allDataCount").text(0);
            $("#gcfzr_nowPageIndex").text(0);
            $("#gcfzr_allPageCount").text(0);
            NoDataMessageOnTable("gcFzr_MemTable", 3);
        }
    }
    //获取专业名称
    function GetSpeName(id) {
        switch (id) {
            case "1":
                return "建筑";
                break;
            case "2":
                return "结构";
                break;
            case "3":
                return "暖通";
                break;
            case "4":
                return "给排水";
                break;
            case "5":
                return "电气";
                break;
            case "6":
                return "室内";
                break;
            case "7":
                return "概预算";
                break;
            case "8":
                return "人事行政";
                break;
            case "9":
                return "人力资源";
                break;
            case "10":
                return "行政";
                break;
            case "11":
                return "财务";
                break;
            case "12":
                return "网络管理";
                break;
        }
    }
    /*弹出选择人员 END*/
    return {
        init: function () {
            pageLoad();
        }
    }
}();