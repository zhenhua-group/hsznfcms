﻿
var projAllot = function () {

    var pageLoad = function () {

        //权重值
        $(":text", $("#tbDataTwo")).change(function () {

            var obj = $(this);
            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            if (!reg_math.test(obj.val())) {
                obj.val("");
                return false;
            }

            //奖金调整值
            changeAllotVal(obj);
        }).focus(function () {
            $(this).select();
            var obj = $(this);
            changeAllotVal(obj);
        });

        //保存
        $("#btnSave").click(function () {

            //按钮不可用
            $(this).attr("class", "btn btn-sm default disabled");
            saveAllotDetail("save");
        });

        //提交
        $("#btnSubmit").click(function () {

            //按钮不可用
            $(this).attr("class", "btn btn-sm default disabled");
            saveAllotDetail("submit");
        });
        //只限查看
        if ($("#hidIsCheck").val() == "chk") {
            $(":text").attr("readonly", true);
            $(".btn.btn-sm.red").attr("class", "btn btn-sm red disabled");
        }

        //查看个人
        $("a[memid]", "#tbDataOne").bind('click', function () {

            var obj = $(this);

            var memid = obj.attr('memid');
            var renwuid = $("#hidRenwuNo").val();
            var unitid = obj.attr("unitid");

            var weights = 0;

            if (unitid == "231" || unitid == "232") {
                weights = parseFloat($("#jianzhu").val());
            }
            else if (unitid == "237") {
                weights = parseFloat($("#jiegou").val());
            }
            else if (unitid == "238") {
                weights = parseFloat($("#shebei").val());
            }
            else if (unitid == "239") {
                weights = parseFloat($("#dianqi").val());
            } else {
                //西安分公司根据人员专业判断
                var spe = $.trim(obj.parent().parent().find('td').eq(2).text());
                if (spe == "建筑") {
                    weights = parseFloat($("#jianzhu").val());
                }
                else if (spe == "结构") {
                    weights = parseFloat($("#jiegou").val());
                }
                else if (spe == "暖通" || spe == "给排水") {
                    weights = parseFloat($("#shebei").val());
                }
                else if (spe == "电气") {
                    weights = parseFloat($("#dianqi").val());
                }
            }

            var url = "/HttpHandler/DeptBpm/ProjDeptHandler.ashx";
            var action = "getmemallotdetails";
            var data = { "action": action, "renwuid": renwuid, "memid": memid };

            $.get(url, data, function (result) {

                var tbContainer = $("#tbMemdetails tbody");

                tbContainer.find('tr').remove();

                var trArr = new Array();
                if (result) {

                    if (result.length > 0) {

                        $.each(result, function (i) {
                            var html = "";
                            html += "<tr>";
                            html += "<td>" + result[i].proName + "</td>";
                            html += "<td>" + result[i].KName + "</td>";
                            html += "<td style='text-align:right;'>" + result[i].AllotCount + "</td>";

                            //html += "<td style='text-align:right;'>" + parseFloat(result[i].AllotCount * weights).toFixed(2) + "</td>";
                            //保留百位
                            var count = Math.abs(parseFloat(result[i].AllotCount)) * weights;
                            count = Math.round(count * 0.01) * 100;
                            html += "<td style='text-align:right;'>" + count.toFixed(2) + "</td>";
                            //end

                            html += "</tr>";

                            trArr.push(html);
                        });
                    }
                }
                else {
                    tr += "<tr>";
                    tr += "<td colspan=4>没有分配数据！</td>";
                    tr += "</tr>";
                }


                tbContainer.append(trArr.toString());

            }, 'json');
        });


        if ($("#hidIsSubmit").val() == "True") {
            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.DoneMsg();
        }
        else {
            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.ReadMsg();
        }

        //归档项目调整数据
        $("#btnBak").click(function () {

            if (!confirm("确定要发起项目奖金调整归档吗？")) {
                return false;
            }
            if ($("#tbDataOne tbody tr").length == 0) {
                alert("没有要归档的信息！");
                return false;
            }

            var renwuid = $("#hidRenwuNo").val();
            var renwuName = $("#ctl00_ContentPlaceHolder1_drpRenwu option:selected").text();

            //获取归档统计记录
            var arryData = new Array();
            var i = 0;
            $("#tbDataOne tbody tr").each(function (index, domEle) {

                var rowdata = $(this);
                if (rowdata.children().length > 1) {
                    arryData[i++] = {
                        ID: 0,
                        RenwuID: renwuid,
                        RenwuName: renwuName,
                        IsFired: $.trim(rowdata.children().eq(0).text().replace(/[\r\n]/g, '')),
                        UnitName: $.trim(rowdata.children().eq(1).text().replace(/[\r\n]/g, '')),
                        UnitID: rowdata.children().eq(1).find("span").attr("unitid"),
                        SpeName: $.trim(rowdata.children().eq(2).text().replace(/[\r\n]/g, '')),
                        UserName: $.trim(rowdata.children().eq(3).text().replace(/[\r\n]/g, '')),
                        MemID: rowdata.children().eq(3).find("span").attr("memid"),
                        ProjAllot: $.trim(rowdata.children().eq(4).text()),
                        UnitOrder: $.trim(rowdata.children().eq(5).text()),
                        HPOrder: $.trim(rowdata.children().eq(6).text()),
                        ProjAllot2: $.trim(rowdata.children().eq(7).text()),
                        IsOther: rowdata.children().eq(0).attr("data-other") == '1' ? "1" : "0",
                    }
                }
            });
            //权重1
            var weight1 = $("#spProjAllotCount").text();
            var arryData2 = new Array();
            $("#tbDataTwo tbody tr").each(function (index, domEle) {

                var rowdata = $(this);
                arryData2[index] = {
                    ID: 0,
                    RenwuID: renwuid,
                    RenwuName: renwuName,
                    WeightName: rowdata.children().eq(0).text(),
                    JZ: rowdata.children().eq(1).text().replace(/%/, ''),
                    JG: rowdata.children().eq(2).text().replace(/%/, ''),
                    SB: rowdata.children().eq(3).text().replace(/%/, ''),
                    DQ: rowdata.children().eq(4).text().replace(/%/, ''),
                    Stat: 0,
                    ProjCount: weight1

                }
            });


            var weight2 = $("#spProjAllotCount2").text();
            var arryData3 = new Array();
            $("#tbDataThree tbody tr").each(function (index, domEle) {

                var rowdata = $(this);
                arryData3[index] = {
                    ID: 0,
                    RenwuID: renwuid,
                    RenwuName: renwuName,
                    WeightName: rowdata.children().eq(0).text(),
                    JZ: rowdata.children().eq(1).text().replace(/%/, ''),
                    JG: rowdata.children().eq(2).text().replace(/%/, ''),
                    SB: rowdata.children().eq(3).text().replace(/%/, ''),
                    DQ: rowdata.children().eq(4).text().replace(/%/, ''),
                    Stat: 1,
                    ProjCount: weight2
                }
            });

            var url = "/HttpHandler/DeptBpm/ProjDeptHandler.ashx";
            var action = "projbak";

            var jsonData = JSON.stringify(arryData);
            var jsonData2 = JSON.stringify(arryData2);
            var jsonData3 = JSON.stringify(arryData3);

            var data = { "action": action, "isNew": 0, "data": jsonData, "data2": jsonData2, "data3": jsonData3 };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == 1) {
                        alert("项目奖金调整数据归档成功!");

                        $("#btnBak").attr("class", "btn btn-sm default disabled").text('已归档');
                    }
                    else if (result == 2) {
                        alert('已归档，无需重复归档！');
                    }
                    else if (result == 3) {
                        alert("考核任务未归档，请先归档考核任务！");
                    }

                }
                else {
                    alert("错误！");
                }
            });

        });
    }
    //保存调整后数据
    var saveAllotDetail = function (action) {

        //对象
        var arrDetail = new Array();
        var arrWeights = new Array();
        var index = 0;
        $("#tbDataOne tbody tr").each(function (i) {

            var obj = $(this);

            if (obj.attr("class") != "active") {
                arrDetail[index++] = {
                    ID: "0",
                    RenwuID: $("#hidRenwuNo").val(),
                    IsFired: obj.find("td").eq(0).children("span").attr("isfired"),
                    UnitID: obj.find("td").eq(1).children("span").attr("unitid"),
                    UnitName: obj.find("td").eq(1).children("span").text(),
                    SpeID: obj.find("td").eq(2).children("span").attr("speid"),
                    SpeName: obj.find("td").eq(2).children("span").text(),
                    MemID: obj.find("td").eq(3).children("span").attr("memid"),
                    MemName: obj.find("td").eq(3).children("span").text(),
                    AllotCount: obj.find("td").eq(4).children("span").text(),
                    UnitOrder: obj.find("td").eq(5).children("span").text(),
                    UnitJudgeOrder: obj.find("td").eq(6).children("span").text(),
                    AllotCountBefore: obj.find("td").eq(7).children("span").text(),
                    Stat: action == "save" ? "0" : "1",
                };
            }
        });
        //权重
        index = 0;
        $("#tbDataTwo tbody tr:last-child").find("td").each(function (i) {

            var obj = $(this);

            if (obj.children("input").val() != "" && obj.children("input").val() != "0.00") {
                arrWeights[index++] = {
                    ID: "0",
                    RenwuID: $("#hidRenwuNo").val(),
                    UnitID: obj.children("input").attr("uid"),
                    weights: obj.children("input").val(),
                    Stat: "0",
                };
            }
        });

        //确认提交
        if (!confirm('确认要提交？')) {
            return false;
        }
        var jsonEntity = JSON.stringify(arrDetail);
        var jsonEntity2 = JSON.stringify(arrWeights);

        var url = "/HttpHandler/DeptBpm/ProjDeptHandler.ashx";
        var action = "savereport";
        var data = { "action": action, "data": jsonEntity, "data2": jsonEntity2 };

        $.post(url, data, function (result) {

            if (result) {
                if (result == "1") {
                    if (action == "save") {
                        //设置成功！
                        alert("调整数据保存成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.ReadMsg();
                    }
                    else {
                        alert("调整数据提交成功！");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();
                    }

                    location.reload();
                }
            }
            else {
                //设置成功！
                alert("设置失败请重试！");
            }
        });
    }

    var changeAllotVal = function (obj) {
        //unitid
        var unitid = obj.attr("unitid").split(',');
        //speids
        var speids = obj.attr("speids").split(',');
        //权重
        var weight = obj.val();

        $("span[allotcount]", $("#tbDataOne")).each(function (i, item) {
            //部门调整
            var uid = $(item).attr("unitid");
            //2017年5月24日  兼容西安分公司
            var speid = $(item).attr("speid");

            if (unitid.indexOf(uid) > -1 || speids.indexOf(speid) > -1) {
                //获取奖金值
                var count = Math.abs(parseFloat($(item).parent().parent().find("td").eq(4).text())) * weight;
                //百位四舍五入
                var count = Math.round(count * 0.01) * 100;

                $(item).text(count.toFixed(2));
            }
        });
    }

    //保存归档数据
    var saveBak = function () {

    }


    return {
        init: function () {
            pageLoad();
        }
    }
}();