﻿
var SetSpeMemFz = function () {

    showDivDialogClass.UserRolePower = {
        "previewPower": "1",
        "userSysNum": "0",
        "userUnitNum": "0",
        "notShowUnitList": ""
    };
    var _globalContainer;

    var pageLoad = function () {

        $("a[action='deluser']", $("#tbSetMems tbody")).live('click', function () {
            $(this).remove();
        });
        //添加用户
        $("a[action='adduser']", $("#tbSetMems tbody")).live('click', function () {
            $("#tbSetMems").show();

            $("#hidFlagSpe").val($(this).attr("speid"));
            //保存当前选择
            _globalContainer = $(this);
            //先赋值
            showDivDialogClass.SetParameters({
                "prevPage": "gcfzr_prevPage",
                "firstPage": "gcfzr_firstPage",
                "nextPage": "gcfzr_nextPage",
                "lastPage": "gcfzr_lastPage",
                "gotoPage": "gcfzr_gotoPageIndex",
                "allDataCount": "gcfzr_allDataCount",
                "nowIndex": "gcfzr_nowPageIndex",
                "allPageCount": "gcfzr_allPageCount",
                "gotoIndex": "gcfzr_pageIndex",
                "pageSize": "10"
            });
            var unit_ID = $("#select_gcFzr_Unit").val();
            if (unit_ID < 0 || unit_ID == null) {
                //绑定工程负责部门
                showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "gcfzrCprUnit", CprTypeUnitCallBack);
            }

        });

        
        //保存专业负责人
        $("#btnSaveMems").click(function () {
            var memArray = new Array();
            var index = 0;
            var isAll = 0;
            $("#tbSetMems tbody tr").each(function (i, item) {
                //专业
                var spename = $(this).find("td").eq(2).find("a").attr("spename");
                var speid = $(this).find("td").eq(2).find("a").attr("speid");
                //对象
                var obj = $(this);
                var container = obj.find("td").eq(1).find("a");
                if (container) {
                    if (container.length > 0) {
                        isAll++;
                    }
                    container.each(function (j, item2) {
                        memArray[index] = {
                            ID: "0",
                            ProId: $("#hidTempProid").val(),//项目ID
                            ProKHId: $("#hidTempKaoHeProjId").val(), //项目考核ID
                            ProKHNameId: $("#hidTempTypeProjNameid").val(),//考核名称ID
                            KHTypeId: $("#hidTempTypeId").val(),
                            MemID: $(this).attr("memid"),
                            MemName: $(this).attr("memname"),
                            SpeName: spename,
                            SpeID: speid
                        };

                        index++;
                    });
                }
            });
            //判断是否添加人
            if (memArray.length == 0) {
                alert("请添加负责人！");
                return false;
            }

            var jsonEntity = JSON.stringify(memArray);


            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savespecmemsfz";
            var senderid = $("#hidUserSysNo").val();
            var khtypeid = $("#hidTempTypeId").val();
            var speid = $("#hidSpeID").val();
            var data = { "action": action, "data": jsonEntity, "senderid": senderid, "KHtypeid": khtypeid, "speid": speid };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("设置专业负责人成功！");
                        //按钮不可用
                        $("#btnSaveMems").attr("class", "btn btn-sm default disabled");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
        //加载负责人
        loadsepcmems();

        //改变消息状态
        var msg = new MessageComm($("#msgno").val());
        msg.ReadMsg();

        //撤回专业负责人
        $("#btnSaveMemsyy").click(function () {
            var memArray = new Array();
            var index = 0;
            var isAll = 0;
            $("#tbSetMems tbody tr").each(function (i, item) {
                //专业
                var spename = $(this).find("td").eq(2).find("a").attr("spename");
                var speid = $(this).find("td").eq(2).find("a").attr("speid");
                //对象
                var obj = $(this);
                var container = obj.find("td").eq(1).find("a");
                if (container) {
                    if (container.length > 0) {
                        isAll++;
                    }
                    container.each(function (j, item2) {
                        memArray[index] = {
                            ID: "0",
                            ProId: $("#hidTempProid").val(),//项目ID
                            ProKHId: $("#hidTempKaoHeProjId").val(), //项目考核ID
                            ProKHNameId: $("#hidTempTypeProjNameid").val(),//考核名称ID
                            KHTypeId: $("#hidTempTypeId").val(),
                            MemID: $(this).attr("memid"),
                            MemName: $(this).attr("memname"),
                            SpeName: spename,
                            SpeID: speid
                        };

                        index++;
                    });
                }
            });
            //判断是否添加人
            if (memArray.length == 0) {
                alert("请添加负责人！");
                return false;
            }

            var jsonEntity = JSON.stringify(memArray);


            var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
            var action = "savespecmemsfz";
            var senderid = $("#hidUserSysNo").val();
            var khtypeid = $("#hidTempTypeId").val();
            var speid = $("#hidSpeID").val();
            var data = { "action": action, "data": jsonEntity, "senderid": senderid, "KHtypeid": khtypeid, "speid": speid };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //撤回成功！
                        alert("撤回专业负责人成功！");
                        //按钮可用
                        $("#btnSaveMems").attr("class", "btn btn-sm  red");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
    }
    
 

    //加载设置的人员
    var loadsepcmems = function () {
        //考核类型ID
        var typeid = $("#hidTempTypeId").val();
        //
        var url = "/HttpHandler/DeptBpm/ProjectBpmHandler.ashx";
        var action = "getspecmemsfz";
        //实例虚拟一个设置对象
        var memArray = new Array();
        memArray[0] = {
            ID: "0",
            ProId: $("#hidTempProid").val(),//项目ID
            ProKHId: $("#hidTempKaoHeProjId").val(), //项目考核ID
            ProKHNameId: $("#hidTempTypeProjNameid").val(),//考核名称ID
            KHTypeId: $("#hidTempTypeId").val(),
            MemID: "0",
            MemName: "",
            SpeName: "",
            SpeID: "0"
        };

        var jsonEntity = JSON.stringify(memArray);

        var data = { "action": action, "id": typeid, "data": jsonEntity };

        $.get(url, data, function (result) {

            if (result) {
                //专业人员
                var mems = eval("(" + result + ")");
                $("#tbSetMems tbody tr").each(function (i) {

                    var obj = $(this);
                    var speid = obj.find("td").eq(2).find("a").attr("speid");
                    //如果是同一专业
                    if (obj.find("td").eq(1).find("a").length > 0) {
                        obj.find("td").eq(1).find("a").remove();
                    }
                    $.each(mems, function (i, item) {
                        if (speid == item.SpeID) {
                            var html = "<a href=\"#\" title=\"点击删除\" class=\"btn btn-xs blue\" action=\"deluser\" id=\"" + item.ID + "\" memname=\"" + item.MemName + "\" memid=\"" + item.MemID + "\">" + item.MemName + "<i class=\"fa fa-times\"></i></a>";

                            obj.find("td").eq(1).append(html);
                            //主持人是否已经设置
                            if (item.SpeID == -2) {
                                $("#spSaveStat").text("已保存");
                            }
                        }
                    });
                });

            }
        });
    }
    /*弹出选择人员*/
    function CprTypeUnitCallBack(result) {
        if (result != null) {

            var data = result.ds;
            var gcFzr_UnitOptionHtml = '<option value="-1">---------请选择---------</option>';
            $.each(data, function (i, n) {
                //过滤本部门
                if (n.unit_ID != $("#hidUnitid").val()) {
                    gcFzr_UnitOptionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
                }
            });
            $("#select_gcFzr_Unit").html(gcFzr_UnitOptionHtml);
            //注册部门选项改变事件
            showDivDialogClass.SetParameters({ "pageSize": "10" });
            $("#select_gcFzr_Unit").unbind('change').change(function () {
                var unit_ID = $("#select_gcFzr_Unit").val();
                if (Math.abs(unit_ID) > 0) {
                    showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", "1", "gcfzrCprMem", BindGcfzrDataCallBack);
                    BindAllDataCountGcfzr(unit_ID);
                }
            });

            //不需要显示的部门 2016年12月28日
            if ($("#hidFlagSpe").val() == "1" || $("#hidFlagSpe").val() == "-2") {

                //var arrUnit = [230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,281];
                var arrUnit = [237, 238, 239, 240, 241, 243, 244];
                $.each(arrUnit, function (i, n) {
                    $("#select_gcFzr_Unit option[value='" + n + "']").remove();
                });
            }
            //结构
            if ($("#hidFlagSpe").val() == "2") {

                //var arrUnit = [230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,281];
                var arrUnit = [231, 232, 233, 234, 235, 236, 238, 239, 240, 241, 243, 244, 281];
                $.each(arrUnit, function (i, n) {
                    $("#select_gcFzr_Unit option[value='" + n + "']").remove();
                });
            }
            //暖通
            if ($("#hidFlagSpe").val() == "3") {

                //var arrUnit = [230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,281];
                var arrUnit = [230, 231, 232, 233, 234, 235, 236, 237, 239, 240, 241, 243, 244, 281];
                $.each(arrUnit, function (i, n) {
                    $("#select_gcFzr_Unit option[value='" + n + "']").remove();
                });
            }
            //给排水
            if ($("#hidFlagSpe").val() == "4") {

                //var arrUnit = [230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,281];
                var arrUnit = [230, 231, 232, 233, 234, 235, 236, 237, 239, 240, 241, 243, 244, 281];
                $.each(arrUnit, function (i, n) {
                    $("#select_gcFzr_Unit option[value='" + n + "']").remove();
                });
            }
            //电气
            if ($("#hidFlagSpe").val() == "5") {

                //var arrUnit = [230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,281];
                var arrUnit = [230, 231, 232, 233, 234, 235, 236, 237, 238, 240, 241, 243, 244, 281];
                $.each(arrUnit, function (i, n) {
                    $("#select_gcFzr_Unit option[value='" + n + "']").remove();
                });
            }
        }
    }
    function BindGcfzrDataCallBack(result) {
        if (result != null) {

            var obj = result.ds;
            var gcFzrMemTableHtml;
            $("#gcFzr_MemTable tr:gt(0)").remove();
            $.each(obj, function (i, n) {
                var oper = "<span rel='" + n.mem_Login + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\" >选择</span>";
                gcFzrMemTableHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.mem_Name + "</td><td>" + GetSpeName(n.mem_Speciality_ID) + "</td><td>" + oper + "</td></tr>";
                $("#gcFzr_MemTable").append(gcFzrMemTableHtml);
                $("#gcFzr_MemTable span:last").click(function () {

                    //添加人员
                    if (_globalContainer) {

                        var item = "<a href=\"#\" title=\"点击删除\" class=\"btn btn-xs blue\" action=\"deluser\" memname=\"" + n.mem_Name + "\" memid=\"" + n.mem_ID + "\">" + n.mem_Name + "<i class=\"fa fa-times\"></i></a>";
                        _globalContainer.parent().prev("td").append(item);
                    }
                });
            });

        }
    }
    function BindAllDataCountGcfzr(unit_ID) {
        //设置参数
        showDivDialogClass.SetParameters({
            "prevPage": "gcfzr_prevPage",
            "firstPage": "gcfzr_firstPage",
            "nextPage": "gcfzr_nextPage",
            "lastPage": "gcfzr_lastPage",
            "gotoPage": "gcfzr_gotoPageIndex",
            "allDataCount": "gcfzr_allDataCount",
            "nowIndex": "gcfzr_nowPageIndex",
            "allPageCount": "gcfzr_allPageCount",
            "gotoIndex": "gcfzr_pageIndex",
            "pageSize": "10"
        });
        //获取总数据
        showDivDialogClass.GetDataTotalCount("getDataAllCount", unit_ID, "true", "gcfzrCprMem", GetGcfzrAllDataCount);
        //注册事件,先注销,再注册
        $("#gcFzr_ForPageDiv span").unbind('click').click(function () {
            var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
            if (isRegex) {
                var pageIndex = $("#gcfzr_nowPageIndex").text();
                showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", pageIndex, "gcfzrCprMem", BindGcfzrDataCallBack);
            }
        });
    }
    function GetGcfzrAllDataCount(result) {
        if (result > 0) {
            showDivDialogClass.BindPageValueFirst(result);
        } else {
            $("#gcFzr_MemTable tr:gt(0)").remove();
            $("#gcfzr_allDataCount").text(0);
            $("#gcfzr_nowPageIndex").text(0);
            $("#gcfzr_allPageCount").text(0);
            NoDataMessageOnTable("gcFzr_MemTable", 3);
        }
    }
    //获取专业名称
    function GetSpeName(id) {
        switch (id) {
            case "1":
                return "建筑";
                break;
            case "2":
                return "结构";
                break;
            case "3":
                return "暖通";
                break;
            case "4":
                return "给排水";
                break;
            case "5":
                return "电气";
                break;
            case "6":
                return "室内";
                break;
            case "7":
                return "概预算";
                break;
            case "8":
                return "人事行政";
                break;
            case "9":
                return "人力资源";
                break;
            case "10":
                return "行政";
                break;
            case "11":
                return "财务";
                break;
            case "12":
                return "网络管理";
                break;
        }
    }


    return {
        init: function () {
            pageLoad();
        }
    }
}();