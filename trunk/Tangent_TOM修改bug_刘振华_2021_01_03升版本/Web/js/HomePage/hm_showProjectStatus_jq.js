﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/hm_projCountHandler.ashx?action=showStatus&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()) + '&strwhere1=' + escape($("#ctl00_ContentPlaceHolder1_hid_where1").val()),
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '阶段类型', '项目数(个) ', '进行中(个) ', '暂停(个) ', '完工(个) '],
        colModel: [
                             { name: 'nid', index: 'nid', width: 45, align: 'center' },
                             { name: 'Status', index: 'lenstatus', width: 200, align: 'left', formatter: colShowStatusFormatter },
                             { name: 'Jdcount', index: 'Jdcount', width: 100, align: 'center', formatter: colShowFormatter },
                             { name: 'JXcount', index: 'JXcount', width: 100, align: 'center', formatter: colShowFormatterA },
                             { name: 'ZTcount', index: 'ZTcount', width: 100, align: 'center', formatter: colShowFormatterC },
                             { name: 'WGcount', index: 'WGcount', width: 100, align: 'center', formatter: colShowFormatterB }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'lenstatus',
        sortorder: 'asc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/hm_projCountHandler.ashx?action=showStatus&strwhere=" + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()) + "&strwhere1=" + escape($("#ctl00_ContentPlaceHolder1_hid_where1").val()),
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false

    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                closeOnEscape: true,
                closeAfterDelete: true,
                reloadAfterSubmit: true,
                afterSubmit: function (response, postdata) {
                    if (response.responseText == "") {

                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, '');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询

});
function colShowStatusFormatter(celvalue, options, rowData) {
    var ShowValue = "";
    var Arraylist = celvalue.split(',');
    for (var i = 0; i < Arraylist.length; i++) {
        if ($.trim(Arraylist[i]) != "") {
            ShowValue += Arraylist[i] + "+";
        }

    }
    if (ShowValue == "") {
        return "无"
    }
    else {
        ShowValue = ShowValue.substring(0, ShowValue.length - 1);
        return ShowValue;
    }

}
//查看项目
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "hm_showProjectAboutStatusBymaster.aspx?status=" + rowData["Status"] + "&unitid=" + $("#ctl00_ContentPlaceHolder1_unitId").val();
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}
//进行中
function colShowFormatterA(celvalue, options, rowData) {
    var pageurl = "hm_showProjectAboutStatusBymaster.aspx?option=A&status=" + rowData["Status"] + "&unitid=" + $("#ctl00_ContentPlaceHolder1_unitId").val();
    return '<a option="A" href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}
//暂停中
function colShowFormatterC(celvalue, options, rowData) {
    var pageurl = "hm_showProjectAboutStatusBymaster.aspx?option=C&status=" + rowData["Status"] + "&unitid=" + $("#ctl00_ContentPlaceHolder1_unitId").val();
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}
//完工
function colShowFormatterB(celvalue, options, rowData) {
    var pageurl = "hm_showProjectAboutStatusBymaster.aspx?option=B&status=" + rowData["Status"] + "&unitid=" + $("#ctl00_ContentPlaceHolder1_unitId").val();
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}
//统计 
function completeMethod() {

    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    var summem = 0, sumpro = 0, sumproA = 0, sumproB = 0, sumproC = 0, sumproD = 0;

    //二种写法
    $(rowIds).each(function () {


        sumproA = sumproA + parseInt($("#" + this).find("td").eq(3).text());
        sumproB = sumproB + parseInt($("#" + this).find("td").eq(4).text());
        sumproC = sumproC + parseInt($("#" + this).find("td").eq(5).text());
        sumproD = sumproD + parseInt($("#" + this).find("td").eq(5).text());
        $("#" + this).find("td").eq(1).text(this);

    });

    $("#jqGrid").footerData('set', { Status: "合计:", Jdcount: sumproA, JXcount: sumproB, ZTcount: sumproC, WGcount: sumproD }, false);


}
//无数据
function loadCompMethod() {
    $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        // $("#nodata").show();
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
    }
    else {
        $("#nodata").hide();
    }
}