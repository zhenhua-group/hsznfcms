﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/hm_projCountHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '姓名', '性别', '专业', '职务', '职称', '注册类别', '项目数量'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 45, align: 'center' },
                             { name: 'mem_ID', index: 'mem_ID', hidden: true, editable: true },
                             { name: 'mem_Name', index: 'mem_Name', width: 100 },
                             { name: 'mem_Sex', index: 'mem_Sex', width: 200, formatter: colShowMemFormatter },
                             { name: 'zy', index: 'zy', width: 200 },
                             { name: 'js', index: 'js', width: 100 },
                             { name: 'zc', index: 'zc', width: 100 },
                             { name: 'zclb', index: 'zclb', width: 100 },
                             { name: 'xmsl', index: 'xmsl', width: 100, align: 'center' }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "action": "showcount" },
        loadonce: false,
        sortname: 'mem_Speciality_ID',
        sortorder: 'asc',
        pager: "#gridpager",
        viewrecords: true,

        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/hm_projCountHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                closeOnEscape: true,
                closeAfterDelete: true,
                reloadAfterSubmit: true,
                afterSubmit: function (response, postdata) {
                    if (response.responseText == "") {

                        $("#jqGrid").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'unit_ID');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/hm_projCountHandler.ashx?action=showcount",
            postData: { 'strwhere': strwhere },
            page: 1

        }).trigger("reloadGrid");
    });
});
//查看人员
function colShowMemFormatter(celvalue, options, rowData) {
    var imgstr = '<img src="/Images/' + celvalue + '.png" style="width: 16px; height: 16px; margin-left: 1px; border:none;" />';
    return imgstr + celvalue;
}

//查看负责人
//function colShowFzrFormatter(celvalue, options, rowData) {


//   var data = "action=fzr&unitid=" + celvalue;
//           $("#jqGrid").jqGrid('setGridParam', {
//               url: "/HttpHandler/CommHandler.ashx?action=fzr",
//               postData: { 'unitid': celvalue },
//               success: function (data) {
//                   return data;
//               }
//           });
//
//  $.ajax({
//      type: "get",
//      dataType: "text",
//     url: "/HttpHandler/CommHandler.ashx",
//    data: data,
//    success: function (result) {                   
// rowData["unit_ID"] = result;
//   }
// });

//}

//查看项目
//function colShowFormatter(celvalue, options, rowData) {
//    var pageurl = "hm_showProject.aspx?unitid=" + rowData["unit_ID"];
//   return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

//}
//编辑
//function colEditFormatter(celvalue, options, rowdata) {
//   if (celvalue != "") {
//       var pageurl = "EditCoperation.aspx?cprid=" + celvalue;
//      return '<a href="' + pageurl + '" alt="编辑合同" class="allow">编辑</a>';
//  }
//}
//统计 
function completeMethod() {

    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    var summem = 0, sumpro = 0;
    //一种写法
    //             for(var i = 0, j = rowIds.length; i < j; i++) {   
    //             var curRowData = $("#jqGrid").jqGrid('getRowData', rowIds[i]);   
    //             var curChk = $("#"+rowIds[i]+"").find("td").eq(4).attr("a");             
    //          }   
    //二种写法
    $(rowIds).each(function () {
        // summem = summem + parseInt($("#" + this).find("td").eq(4).text());
        // sumpro = sumpro + parseInt($("#" + this).find("td").eq(6).text());
        $("#" + this).find("td").eq(1).text(this);
        //                var aa = $("#" + this).find("td").eq(5);
        //                var data = "action=fzr&unitid=" + aa.text();
        //                $.ajax({
        //                    type: "get",
        //                    dataType: "text",
        //                    url: "/HttpHandler/CommHandler.ashx",
        //                    data: data,
        //                    success: function (result) {
        //                        aa.text(result);
        //                    }
        //                });
    });

    // var sumAcount=parseFloat($("#jqGrid").getCol("memcount",false,'sum')).toFixed(4);
    //  var sumArea=parseFloat($("#jqGrid").getCol("projectcount",false,'sum')).toFixed(4);
    //  $("#jqGrid").footerData('set', { unit_Name: "合计:", memcount: summem, projectcount: sumpro }, false);


}
//无数据
function loadCompMethod() {
    $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        // $("#nodata").show();
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
    }
    else {
        $("#nodata").hide();
    }
}