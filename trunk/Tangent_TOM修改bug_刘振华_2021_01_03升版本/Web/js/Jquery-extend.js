﻿//<reference path="jquery-1.2.6.js"/>

/* ---------------------------------------------------------------------------------- 
* jQuery plugin  
* created by Sago
*
* 1.1、MessageBox （$.MessageBox.Impl）
* 1.2、CoverDiv  ($.PopCover,$.CloseCover,$.GetBodySize,$.PopLoadding,$CloseLoadding)
* 1.3、pager  ($.fn.pager)
* 1.4、query ($.query.get,$.query.set) 
* 1.5、PopModel ($.popModel)
* 1.6、Draggable ($.Draggable)
* ---------------------------------------------------------------------------------- */

/* 1.1、MessageBox 消息框
* -------------------------------------------------- */
(function ($) {
    $.fn.messageBox = function () {
        var box = new $.MessageBoxImpl(this);
        return box;
    };

    //MessageBox实现类
    $.MessageBoxImpl = function (element) {
        var Instance = $(element);
        //显示方法
        this.show = function (msg) {
            if (msg) {
                Instance.text(msg).show();
            }
        }
        //隐藏方法
        this.hide = function () {
            Instance.hide();
        }
    };
})(jQuery);


/* 1.2、CoverDiv  遮蔽层
* -------------------------------------------------- */
(function ($) {
    //弹出遮蔽层
    $.PopCover = function (options) {
        var $coverDiv = $("#coverDiv");
        //检查是否存在此Div
        if ($coverDiv.length > 0) {
            $coverDiv.show();
        } else {
            //创建Div
            $coverDiv = $(window.document.createElement("div"));
            $coverDiv.attr("id", "coverDiv");
            //options 为空
            if (!options) {
                //样式集合
                options = {
                    position: "absolute",
                    background: "#CCCCCC",
                    left: "0px",
                    top: "0px",
                    width: $(document).width(),
                    height: $(document).height(),
                    zIndex: 1,
                    filter: "Alpha(Opacity=60)", //IE滤镜
                    opacity: 0.3
                };
            }
            $coverDiv.css(options);
            document.body.appendChild($coverDiv[0]);
        }
    }

    //关闭遮蔽层
    $.CloseCover = function () {
        var $coverDiv = $("#coverDiv");
        if ($coverDiv) {
            $coverDiv.hide();
        }
    }

    //取得页面高度和宽度
    $.GetBodySize = function () {
        var bodySize = {};
        with (document.documentElement) {
            //如果滚动条的宽度大于页面的宽度，取得滚动条的宽度，否则取页面宽度
            bodySize.Width = (scrollWidth > clientWidth) ? scrollWidth : clientWidth;
            //如果滚动条的高度大于页面的高度，取得滚动条的高度，否则取高度
            bodySize.Height = (scrollHeight > clientHeight) ? scrollHeight : clientHeight;
        }
        return bodySize;
    }

    //弹出Loadding层
    $.PopLoadding = function (options) {
        var $loaddingDiv = $("#loaddingDiv");
        if ($loaddingDiv.length > 0) {
            $loaddingDiv.show();
        } else {
            $loaddingDiv = $(window.document.createElement("div"));
            $loaddingDiv.attr("id", "loaddingDiv");
            if (!options) {
                options =
                {
                    position: "absolute",
                    width: 300,
                    height: 60,
                    zIndex: 999999
                };
                var width = options.width;
                var height = options.height;
                options.top = ($(document).height() / 2) - (height / 2)
                options.left = ($(document).width() / 2) - (width / 2)
            }
            $loaddingDiv.css(options);
            $loaddingDiv.append("<img src=\"/images/Lodding.gif\" align=\"middle\">Processing...Please wait a monment!");
            document.body.appendChild($loaddingDiv[0]);
        }
    }

    //关闭Loadding层
    $.CloseLoadding = function () {
        var $loaddingDiv = $("#loaddingDiv");
        if ($loaddingDiv.length > 0) {
            $loaddingDiv.hide();
        }
    }
})(jQuery);

/* 1.3、pager  分页控件 、暂时没有独立样式
* -------------------------------------------------- */
(function ($) {
    $.fn.pager = function (options) {
        var opts = $.extend({}, $.fn.pager.defaults, options);
        return this.each(function () {
            // empty out the destination element and then render out the pager with the supplied options
            var container = renderpager(parseInt(options.pagenumber), parseInt(options.pagecount), options.buttonClickCallback, parseInt(options.pagesize), options.datacount);
            $(this).empty().append(container);
            // specify correct cursor activity
            //$('.pageNumber a').mouseover(function() { document.body.style.cursor = "pointer"; }).mouseout(function() { document.body.style.cursor = "auto"; });

            //首页处理事件
            $("#pagerFirst", container).click(function () {
                options.buttonClickCallback(1);
                return false;
            });
            //尾页处理事件
            $("#pagerLast", container).click(function () {
                options.buttonClickCallback(options.pagecount);
                return false;
            });
            //上一页处理事件
            $("#pagerPrev", container).click(function () {
                options.buttonClickCallback(options.pagenumber == 1 ? 1 : options.pagenumber - 1);
                return false;
            });
            //下一页事件
            $("#pagerNext", container).click(function () {
                options.buttonClickCallback(options.pagenumber == options.pagecount ? options.pagecount : options.pagenumber + 1);
                return false;
            });
            //GO处理事件
            $("#pagerGo", container).click(function () {
                options.buttonClickCallback($(":text", container).val());
                return false;
            });
        });
    };

    // render and return the pager with the supplied options
    function renderpager(pagenumber, pagecount, buttonClickCallback, pagesize, datacount) {
        var defaultPageStyle = "color:blue;";
        var prevPageStyle = "color:blue;";
        var nextPageStyle = "color:blue;";
        var lastPageStyle = "color:blue;";

        if (pagenumber == 1) {
            defaultPageStyle = "color:gray;";
            prevPageStyle = "color:gray;";
            if (pagecount <= pagenumber) {
                nextPageStyle = "color:gray;";
                lastPageStyle = "color:gray;";
            }
        } else {
            if (pagecount <= pagenumber) {
                nextPageStyle = "color:gray;";
                lastPageStyle = "color:gray;";
            }
        }

        var tableString = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:Black;background-color:Transparent;font-family:宋体;font-size:9pt;height:25px;width:100%;border-collapse:collapse;\">";
        tableString += "<tbody>";
        tableString += "<tr>";
        tableString += "<td align=\"center\" style=\"width:35%;\">";
        tableString += "总计：<b>" + datacount + "</b>项 页次：<b>" + pagenumber + "</b>/<b>" + pagecount + "</b>页";
        tableString += "</td>";
        tableString += "<td align=\"center\">";
        tableString += "&lt;&lt;<a class=\"a_2\" title=\"转到第一页文本\"  id=\"pagerFirst\" style=\"" + defaultPageStyle + "cursor:pointer;\">首页</a>";
        tableString += "&nbsp;&lt;<a class=\"a_2\" title=\"转到上一页\"  id=\"pagerPrev\" style=\"" + prevPageStyle + "cursor:pointer;\">上一页</a>";
        tableString += "&nbsp;<a class=\"a_2\" title=\"转到下一页\"  id=\"pagerNext\" style=\"" + nextPageStyle + "cursor:pointer;\">下一页</a>";
        tableString += "&gt;&nbsp;<a class=\"a_2\" title=\"转到最后一页\"  id=\"pagerLast\" style=\"" + lastPageStyle + "cursor:pointer;\">尾页</a>&gt;&gt;";
        tableString += "</td>";
        tableString += "<td align=\"right\" style=\"width:120px;\">";
        tableString += "跳到 <input type=\"text\" onpropertychange=\"if(/\D/.test(value))value=value.replace(/\D/,'');\" value=\"1\" style=\"width:20px;font-size: 12px;color: #795D36;border-bottom-width: 1px;border-top-style: none;border-right-style: none;border-bottom-style: solid;border-left-style: none;border-bottom-color: #795D36;background-color:transparent;\" name=\"Pager1_PageNo\" id=\"Pager1_PageNo\"> 页";
        tableString += "</td>";
        tableString += "<td style=\"width:40px;\">";
        tableString += "<a href=\"#\" class=\"a_2\" id=\"pagerGo\">GO</a>";
        tableString += "</td>";
        tableString += "</tr>";
        tableString += "</tbody>";
        tableString += "</table>";
        //        // setup $pager to hold render
        //        var $pager = $('<div class="pageNumber"></div>');
        //        //出现总记录数提示Span
        //        $pager.append($('<span class="item">共'+datacount+'条记录'+pagenumber+'/'+parseInt(pagecount)+'页</span>'));
        //        //当前是第一页的情况下，不需要出现首页按钮
        //        if(pagenumber !=1){
        //                $pager.append(renderButton('首页', pagenumber, pagecount, buttonClickCallback));
        //        }
        //        // add in the previous and next buttons
        //        $pager.append(renderButton('上一页', pagenumber, pagecount, buttonClickCallback));

        // pager currently only handles 10 viewable pages ( could be easily parameterized, maybe in next version ) so handle edge cases
        //        var startPoint = 1;
        //        var endPoint = 9;

        //        if (pagenumber > 4) {
        //            startPoint = pagenumber - 4;
        //            endPoint = pagenumber + 4;
        //        }

        //        if (endPoint > pagecount) {
        //            startPoint = pagecount - 8;
        //            endPoint = pagecount;
        //        }

        //        if (startPoint < 1) {
        //            startPoint = 1;
        //        }

        //        // loop thru visible pages and render buttons
        //        for (var page = startPoint; page <= endPoint; page++) {

        //            var currentButton = $('<a href="javascript:void(0)">' + (page) + '</a>');

        //            page == pagenumber ? currentButton.addClass('currentPage') : currentButton.click(function() { buttonClickCallback(this.firstChild.data); });
        //            currentButton.appendTo($pager);
        //        }

        // render in the next and last buttons before returning the whole rendered control back.
        //        $pager.append(renderButton('下一页', pagenumber, pagecount, buttonClickCallback)).append(renderButton('尾页', pagenumber, pagecount, buttonClickCallback));

        return $(tableString);
    }

    // renders and returns a 'specialized' button, ie 'next', 'previous' etc. rather than a page number button
    //    function renderButton(buttonLabel, pagenumber, pagecount, buttonClickCallback) {
    //          var destPage = 1;
    //          var className ="";
    //	  var $Button ="";
    //          //判断Button处理类型 
    //         switch (buttonLabel) {
    //            case "首页":
    //                destPage = 1;
    //                $Button  =$("&lt;&lt;<a class=\"a_2\" title=\"转到第一页文本\"  id=\"Pager1_Pager1_First\">首页</a>");
    //                break;
    //            case "上一页":
    //                destPage = pagenumber ==1?1:pagenumber-1;
    //                className ="previous";
    //                buttonLabel ="";
    //                $Button  =$("&nbsp;&lt;<a class=\"a_2\" title=\"转到上一页\"  id=\"Pager1_Pager1_Prev\">上一页</a>");
    //                break;
    //            case "下一页":
    //                destPage = pagenumber ==pagecount?pagecount:pagenumber+1;
    //                className ="next";
    //                buttonLabel ="";
    //                break;
    //            case "尾页":
    //                destPage = pagecount;
    //                break;
    //        }
    //        //取得Button
    ////         = $('<a href="javascript:void(0)" class="'+className+'">' + buttonLabel + '</a>');
    //        //绑定Click事件 
    //        $Button.click(function() { buttonClickCallback(destPage); });

    //        return $Button;
    //    }
})(jQuery);

(function ($) {
    //大家喜欢滑动
    $.fn.Slider = function (options) {
        //取得当前滑动Div
        var $silderDiv = $("#silderContainer");
        //取得左边箭头
        var $forward = $("#forward").click(function () {
            animate("left");
        });
        //取得右边箭头
        var $next = $("#next").click(function () {
            animate("right");
        });
        function animate(direction) {
            //正在执行动画不能操作
            if ($(":animated").attr("id") == "silderContainer") {
                return false;
            }
            var pos = $silderDiv.position();
            var left = pos.left;
            //向左滑动的场合
            if (direction == "left") {
                var max_num = $silderDiv.children("div[class=itembox]").length;
                var totalWidth = max_num / 5 * options.width - options.width;
                if (left == -totalWidth || left < parseInt(-totalWidth)) {
                    return false;
                }
                left = (left - options.width) + "px";
                $silderDiv.animate({ left: left });
            } else {
                if (left == 0) {
                    return false;
                }
                left = (left + options.width) + "px";
                $silderDiv.animate({ left: left });
            }
        }
    }
})(jQuery);

/* 1.4、query  获得URL参数的方法 
* -------------------------------------------------- */
//jquery.query 插件,方便使用引用进来
new function (settings) {
    // Various Settings
    var $separator = settings.separator || '&';
    var $spaces = settings.spaces === false ? false : true;
    var $suffix = settings.suffix === false ? '' : '[]';
    var $prefix = settings.prefix === false ? false : true;
    var $hash = $prefix ? settings.hash === true ? "#" : "?" : "";
    var $numbers = settings.numbers === false ? false : true;

    jQuery.query = new function () {
        var is = function (o, t) {
            return o != undefined && o !== null && (!!t ? o.constructor == t : true);
        };
        var parse = function (path) {
            var m, rx = /\[([^[]*)\]/g, match = /^([^[]+)(\[.*\])?$/.exec(path), base = match[1], tokens = [];
            while (m = rx.exec(match[2])) tokens.push(m[1]);
            return [base, tokens];
        };
        var set = function (target, tokens, value) {
            var o, token = tokens.shift();
            if (typeof target != 'object') target = null;
            if (token === "") {
                if (!target) target = [];
                if (is(target, Array)) {
                    target.push(tokens.length == 0 ? value : set(null, tokens.slice(0), value));
                } else if (is(target, Object)) {
                    var i = 0;
                    while (target[i++] != null);
                    target[--i] = tokens.length == 0 ? value : set(target[i], tokens.slice(0), value);
                } else {
                    target = [];
                    target.push(tokens.length == 0 ? value : set(null, tokens.slice(0), value));
                }
            } else if (token && token.match(/^\s*[0-9]+\s*$/)) {
                var index = parseInt(token, 10);
                if (!target) target = [];
                target[index] = tokens.length == 0 ? value : set(target[index], tokens.slice(0), value);
            } else if (token) {
                var index = token.replace(/^\s*|\s*$/g, "");
                if (!target) target = {};
                if (is(target, Array)) {
                    var temp = {};
                    for (var i = 0; i < target.length; ++i) {
                        temp[i] = target[i];
                    }
                    target = temp;
                }
                target[index] = tokens.length == 0 ? value : set(target[index], tokens.slice(0), value);
            } else {
                return value;
            }
            return target;
        };

        var queryObject = function (a) {
            var self = this;
            self.keys = {};

            if (a.queryObject) {
                jQuery.each(a.get(), function (key, val) {
                    self.SET(key, val);
                });
            } else {
                jQuery.each(arguments, function () {
                    var q = "" + this;
                    q = q.replace(/^[?#]/, ''); // remove any leading ? || #
                    q = q.replace(/[;&]$/, ''); // remove any trailing & || ;
                    if ($spaces) q = q.replace(/[+]/g, ' '); // replace +'s with spaces

                    jQuery.each(q.split(/[&;]/), function () {
                        var key = decodeURIComponent(this.split('=')[0] || "");
                        var val = decodeURIComponent(this.split('=')[1] || "");

                        if (!key) return;

                        if ($numbers) {
                            if (/^[+-]?[0-9]+\.[0-9]*$/.test(val)) // simple float regex
                                val = parseFloat(val);
                            else if (/^[+-]?[0-9]+$/.test(val)) // simple int regex
                                val = parseInt(val, 10);
                        }

                        val = (!val && val !== 0) ? true : val;

                        if (val !== false && val !== true && typeof val != 'number')
                            val = val;

                        self.SET(key, val);
                    });
                });
            }
            return self;
        };

        queryObject.prototype = {
            queryObject: true,
            has: function (key, type) {
                var value = this.get(key);
                return is(value, type);
            },
            GET: function (key) {
                if (!is(key)) return this.keys;
                var parsed = parse(key), base = parsed[0], tokens = parsed[1];
                var target = this.keys[base];
                while (target != null && tokens.length != 0) {
                    target = target[tokens.shift()];
                }
                return typeof target == 'number' ? target : target || "";
            },
            get: function (key) {
                var target = this.GET(key);
                if (is(target, Object))
                    return jQuery.extend(true, {}, target);
                else if (is(target, Array))
                    return target.slice(0);
                return target;
            },
            SET: function (key, val) {
                var value = !is(val) ? null : val;
                var parsed = parse(key), base = parsed[0], tokens = parsed[1];
                var target = this.keys[base];
                this.keys[base] = set(target, tokens.slice(0), value);
                return this;
            },
            set: function (key, val) {
                return this.copy().SET(key, val);
            },
            REMOVE: function (key) {
                return this.SET(key, null).COMPACT();
            },
            remove: function (key) {
                return this.copy().REMOVE(key);
            },
            EMPTY: function () {
                var self = this;
                jQuery.each(self.keys, function (key, value) {
                    delete self.keys[key];
                });
                return self;
            },
            load: function (url) {
                var hash = url.replace(/^.*?[#](.+?)(?:\?.+)?$/, "$1");
                var search = url.replace(/^.*?[?](.+?)(?:#.+)?$/, "$1");
                return new queryObject(url.length == search.length ? '' : search, url.length == hash.length ? '' : hash);
            },
            empty: function () {
                return this.copy().EMPTY();
            },
            copy: function () {
                return new queryObject(this);
            },
            COMPACT: function () {
                function build(orig) {
                    var obj = typeof orig == "object" ? is(orig, Array) ? [] : {} : orig;
                    if (typeof orig == 'object') {
                        function add(o, key, value) {
                            if (is(o, Array))
                                o.push(value);
                            else
                                o[key] = value;
                        }
                        jQuery.each(orig, function (key, value) {
                            if (!is(value)) return true;
                            add(obj, key, build(value));
                        });
                    }
                    return obj;
                }
                this.keys = build(this.keys);
                return this;
            },
            compact: function () {
                return this.copy().COMPACT();
            },
            toString: function () {
                var i = 0, queryString = [], chunks = [], self = this;
                var encode = function (str) {
                    str = str + "";
                    if ($spaces) str = str.replace(/ /g, "+");
                    return encodeURIComponent(str);
                };
                var addFields = function (arr, key, value) {
                    if (!is(value) || value === false) return;
                    var o = [encode(key)];
                    if (value !== true) {
                        o.push("=");
                        o.push(encode(value));
                    }
                    arr.push(o.join(""));
                };
                var build = function (obj, base) {
                    var newKey = function (key) {
                        return !base || base == "" ? [key].join("") : [base, "[", key, "]"].join("");
                    };
                    jQuery.each(obj, function (key, value) {
                        if (typeof value == 'object')
                            build(value, newKey(key));
                        else
                            addFields(chunks, newKey(key), value);
                    });
                };

                build(this.keys);

                if (chunks.length > 0) queryString.push($hash);
                queryString.push(chunks.join($separator));

                return queryString.join("");
            }
        };

        return new queryObject(location.search, location.hash);
    };
}(jQuery.query || {}); // Pass in jQuery.query as settings object


/* 1.5、PopModel  reference CSS  box.css
* -------------------------------------------------- */
(function ($) {
    //显示弹出层
    $.fn.PopArea = function (title, closeFun) {
        var popModel = new $.popModel(this, title, closeFun);
        popModel.show();
        return popModel;
    }

    //弹出层
    $.PopArea = function (element, title, closeFun) {
        var popModel = new $.popModel(element, title, closeFun);
        popModel.show();
        return popModel;
    }

    //弹出层实现类
    $.popModel = function (element, title, closeFun) {
        var Instance = this;
        var popInstance = element;
        //清楚页面原始内容
        //popInstance.remove();

        //取得PopContnet的宽度
        var bodySize = $.GetBodySize();

        var width = parseInt(popInstance.css("width").replace("px", ""), 10);
        var height = parseInt(popInstance.css("height").replace("px", ""), 10);
        var windowheight = ($(window).height() - height) / 2;
        var windowWidth = (parseInt($(window).width(), 10) - width) / 2;
        var $popDiv = "<div class=\"box\" id=\"popDiv\" style=\"width:" + width + "px;height:" + (height + 40) + "px;;left:" + windowWidth + "px;;top:" + windowheight + "px;\"><!-- 显示/隐藏 ： show/hide --><div class=\"opacity-bg-iframe\"><!-- 半透明背景 --></div><a id=\"btnClose\" href=\"#\" class=\"btn-close-b show\">关闭</a><!-- 显示/隐藏 ： show/hide , 小版按钮 : btn-close , 大版按钮 : btn-close-b --><div class=\"box-loading\"><!--<span class=\"ico-loading-16\">加载中，请稍候...</span>--></div><div class=\"box-iframe-title\" id=\"PopModelTitle\"><h2>" + title + "</h2></div><div class=\"box-iframe\" id=\"popContent\" style=\"height:" + height + "px;\"></div></div>";
        //追加内容
        $(document.body).append($popDiv);

        $("#popContent", $("#popDiv"))[0].appendChild(element[0]);

        this.show = function () {
            popInstance.parents("body").children("div[id=popDiv]").addClass("show");
        }
        this.hide = function () {
            popInstance.parents("body").children("div[id=popDiv]").remove();
        }

        this.CloseHander = function () {
            Instance.hide();
            if (closeFun) {
                closeFun();
            }
        }
        $("#btnClose").live("click", Instance.CloseHander);
    }
    //关闭方法
    $.fn.PopClose = function () {
        this.hide();
        //                if(closeFun){
        //                        closeFun();
        //                }
        //                $(this).parents("div[id=popDiv]").remove();
    }

    //关闭弹出层
    $.PopClose = function (element) {
        element.hide();
    }
})(jQuery);


/* 1.6、Draggable  实现可以随鼠标拖动的效果
* -------------------------------------------------- */
(function ($) {
    $.fn.Draggable = function () {
        var instance = this;
        instance.mousemove(function (event) {
            if (event.button == 1) {
                var caoX = obj.clientLeft;
                var caoY = obj.clientTop;
                obj.style.pixelLeft = caoX + (event.x - cao_x);
                obj.style.pixelTop = caoY + (event.y - cao_y);

            }
        });
    }
})(jQuery);