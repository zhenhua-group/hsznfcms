﻿var auditRecordLength = 2;
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
var isEditApp;
$(function () {
    //判断当前部门
    var curunitid = $("#ctl00_ContentPlaceHolder1_userUnitNum").val();
    //经济所
    //经济所
    if (curunitid == '248') {
        $("#ctl00_ContentPlaceHolder1_chk_ISTrunEconomy").hide().parent().parent().hide();
    }
    else if (curunitid == '247') {//暖通所
        $("#ctl00_ContentPlaceHolder1_chk_ISHvac").hide().parent().parent().hide();
    }
    else {//土建所
        $("#ctl00_ContentPlaceHolder1_chk_ISArch").hide().parent().parent().hide();
    }

    $("#EditDetail tr:even").css({ background: "White" });
    $("#EditDetail tbody>tr").hide();
    $("#EditDetail").children().find("tr").eq(32).show();
    //合同ID
    //$("#hid_cprid").val(hid_cprid);
    var chargeid = "0";
    var subid = "0";
    $("#ctl00_ContentPlaceHolder1_ddcpr_Type").change(function () {
        if ($(":selected", this).text().trim() == "规划合同") {
            $("#ctl00_ContentPlaceHolder1_txt_buildAreaEdit").css("background-color", "white");
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_buildAreaEdit").css("background-color", "#FFC");

        }
    });
    //    var regOne =/[\\/:\*\?\""<>|]/;
    var regTwo = /^[^.]/;
    //检查项目名称是否重复
    $("#txt_nameEdit").blur(function () {
        var projectName = $.trim($(this).val());
        if (regOne.test(projectName)) {
            alert("项目名称不能包括以下任何字符！\/:\*\?\"<>|");
            projectNameRepeat = true;
            return false;
        }
        if (projectName.length > 0) {
            $.post("/HttpHandler/ProjectMgr/ExistsInTGProjectHandler.ashx", { "projectname": projectName, "sysno": $("#HiddenProjectSysNo").val() }, function (result) {
                if (result == "1") {
                    projectNameRepeat = true;
                    alert("项目名称已存在，请重新输入新项目名称！");
                } else {
                    projectNameRepeat = false;
                }
            });
        }
    });
    //绑定权限
    showDivDialogClass.UserRolePower = {
        "previewPower": $("#ctl00_ContentPlaceHolder1_previewPower").val(),
        "userSysNum": $("#ctl00_ContentPlaceHolder1_userSysNum").val(),
        "userUnitNum": $("#ctl00_ContentPlaceHolder1_userUnitNum").val(),
        "notShowUnitList": ""
    };
    //管理级别(院)
    $("#ctl00_ContentPlaceHolder1_radio_yuanEdit").click(function () {

        $("#ygedit").attr("style", "display:block");
        $("#sgedit").attr("style", "display:none");
        $("#ctl00_ContentPlaceHolder1_audit_yuanEdit").attr("checked", "true");
        $("#ctl00_ContentPlaceHolder1_audit_yuanEdit").parent().attr("class", "checked");
        $("#ctl00_ContentPlaceHolder1_audit_suoEdit").removeAttr("checked").parent().attr("class", "");
    });
    //管理级别(所)
    $("#ctl00_ContentPlaceHolder1_radio_suoEdit").click(function () {

        $("#ygedit").attr("style", "display:inline-block").attr("checked", "false");
        $("#sgedit").attr("style", "display:inline-block;").attr("checked", "true");
        $("#ctl00_ContentPlaceHolder1_audit_yuanEdit").parent().attr("class", "checked");
        $("#ctl00_ContentPlaceHolder1_audit_suoEdit").parent().attr("class", "checked");
    })
    //甲方负责人
    $("#sch_Aperson").click(function () {
        //var url = "pro_smallfzr.aspx";
        //ShowDialogWin_fzr(url);

        $("#txtName").val("");
        $("#txtPhone").val("");
        $("#txtCompName").val("");
        jffzr_strWhere_Serch = "";
        $("#jffzr_table tr:gt(0)").remove();
        //先赋值
        showDivDialogClass.SetParameters({
            "pageSize": "5"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "1", "jffzrCpr", JffzrCallBack);
        BindAllDataCountJffzr(); //绑定总数据
        //        $("#jffzr_DialogDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 550,
        //            height: 320,
        //            top: 100,
        //            resizable: false,
        //            title: "甲方负责人"
        //        }).dialog("open");
    });
    //承接部门
    $("#sch_unit").click(function () {
        //var url = "../../Coperation/cpr_SmallUnit.aspx";
        //showUnit(url);
        //加载数据-先赋值
        showDivDialogClass.SetParameters({
            "pageSize": "10"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "proCjbm", ProCjbmCallBack);
        BindAllDataCount(); //绑定总数据
        //        $("#pro_cjbmDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 470,
        //            height: 340,
        //            resizable: false,
        //            title: "承接部门"
        //        }).dialog("open");
    });

    $("#sch_PM").click(function () {
        //        $("#chooseUserMain").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 700,
        //            height: "auto",
        //            resizable: false,
        //            title: "选择项目经理"
        //        }).dialog("open");
        //        return false;
    });
    //甲方负责人搜索
    $("#btn_serch").click(function () {
        var str_name = $("#txtName").val();
        var str_phone = $("#txtPhone").val();
        var str_cpyName = $("#txtCompName").val();
        var strWhere = "";
        strWhere += "&Name=" + str_name + "&Phone=" + str_phone + "&Department=" + str_cpyName;
        jffzr_strWhere_Serch = strWhere;
        jffzr_strWhere_Serch = showDivDialogClass.ReplaceChars(jffzr_strWhere_Serch);
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", jffzr_strWhere_Serch, "true", "1", "jffzrCpr", JffzrCallBack);
        BindAllDataCountJffzr(); //绑定总数据
    });
    //隐藏和显示
    //1、项目名称
    $("#cprName").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cprName").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(0).hide();
        } else {
            $("#cprName").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(0).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、管理级别
    $("#btncancl").click(function () {
        window.history.back()
    });
    $("#Level").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#Level").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(1).hide(); $("#EditDetail").children().find("tr").eq(2).hide();
        } else {
            $("#Level").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(1).show();
            $("#EditDetail").children().find("tr").eq(2).show();
            $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、关联合同
    $("#cpr_coper").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cpr_coper").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(31).hide();
        } else {
            $("#cpr_coper").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(31).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    $("#cpr_Type").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cpr_Type").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(3).hide();
        } else {
            $("#cpr_Type").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(3).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、建筑面积
    $("#buildArea").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#buildArea").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(4).hide();
        } else {
            $("#buildArea").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(4).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、建筑单位
    $("#cprBuildUnit").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cprBuildUnit").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(5).hide();
        } else {
            $("#cprBuildUnit").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(5).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、规划面积
    $("#area").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#area").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(6).hide();
        } else {
            $("#area").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(6).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、结构形式
    $("#StructType").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#StructType").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(7).hide();
            $("#EditDetail").children().find("tr").eq(8).hide(); $("#EditDetail").children().find("tr").eq(9).hide(); $("#EditDetail").children().find("tr").eq(10).hide();


        } else {
            $("#StructType").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(7).show();
            $("#EditDetail").children().find("tr").eq(8).show(); $("#EditDetail").children().find("tr").eq(9).show(); $("#EditDetail").children().find("tr").eq(10).show();
            $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、建筑分类
    $("#BuildStructType").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#BuildStructType").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(11).hide(); $("#EditDetail").children().find("tr").eq(12).hide(); $("#EditDetail").children().find("tr").eq(13).hide(); $("#EditDetail").children().find("tr").eq(14).hide();
        } else {
            $("#BuildStructType").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(11).show(); $("#EditDetail").children().find("tr").eq(12).show(); $("#EditDetail").children().find("tr").eq(13).show(); $("#EditDetail").children().find("tr").eq(14).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、设计等级
    $("#buildType").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#buildType").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(15).hide();
        } else {
            $("#buildType").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(15).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、项目经理
    $("#proFuze").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#proFuze").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(16).hide();
            $("#EditDetail").children().find("tr").eq(17).hide();
        } else {
            $("#proFuze").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(16).show();
            $("#EditDetail").children().find("tr").eq(17).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });

    //1、甲方负责人
    $("#FParty").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#FParty").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(18).hide();
            $("#EditDetail").children().find("tr").eq(19).hide();
        } else {
            $("#FParty").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(18).show();
            $("#EditDetail").children().find("tr").eq(19).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、承接部门
    $("#cjbm").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cjbm").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(20).hide();
        } else {
            $("#cjbm").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(20).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、合同额
    $("#cpr_Account").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cpr_Account").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(21).hide();
        } else {
            $("#cpr_Account").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(21).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、建设地点
    $("#ddProjectPosition").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#ddProjectPosition").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(22).hide();
        } else {
            $("#ddProjectPosition").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(22).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、行业性质
    $("#ddProfessionTypeCheck").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#ddProfessionTypeCheck").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(23).hide();
        } else {
            $("#ddProfessionTypeCheck").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(23).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //-------
    //1、项目阶段
    $("#ddcpr_Stage").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#ddcpr_Stage").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(24).hide();
        } else {
            $("#ddcpr_Stage").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(24).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、工程来源
    $("#ddSourceWay").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#ddSourceWay").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(25).hide();
        } else {
            $("#ddSourceWay").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(25).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    $("#cpr_join").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cpr_join").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(26).hide();
        } else {
            $("#cpr_join").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(26).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、项目开始日期
    $("#SingnDate").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#SingnDate").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(27).hide();
        } else {
            $("#SingnDate").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(27).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、项目结束日期
    $("#DoneDate").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#DoneDate").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(28).hide();
        } else {
            $("#DoneDate").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(28).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、项目特征概况
    $("#multibuild").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#multibuild").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(29).hide();
        } else {
            $("#multibuild").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(29).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    //1、合同备注
    $("#cpr_Remark").click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $("#cpr_Remark").removeAttr("checked");
            $("#EditDetail").children().find("tr").eq(30).hide();
        } else {
            $("#cpr_Remark").attr("checked", "checked");
            $("#EditDetail").children().find("tr").eq(30).show(); $("#EditDetail").children().find("tr").eq(32).show();
        }
    });
    messageDialog = $("#auditShow").messageDialog;
    sendMessageClass = new MessageCommon(messageDialog);
    $("#btnsub").click(function () { 
        var sd = $('input[type="checkbox"][name="bbs"]:checked').length;
        if (sd == "0") {
            alert("请选择要修改的项");
            return false;
        }
        //数字验证正则
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        //电话验证
        var reg_phone = /^(1[3,5,8,7]{1}[\d]{9})|(((400)-(\d{3})-(\d{4}))|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{3,7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)$/;

        var msg = "";

        var optionsArray = "";
        $('input[type="checkbox"][name="bbs"]:checked').each(function () {
            var option = $(this).attr("id");
            switch (option) {
                case "cprName":
                    if ($("#ctl00_ContentPlaceHolder1_txt_nameEdit").val() == "") {
                        msg += "请输入项目名称！</br>";
                    }
                    optionsArray += "pro_name:" + $("#ctl00_ContentPlaceHolder1_txt_nameEdit").val() + "|*|";
                    break;
                case "cpr_coper":
                    if ($("#ctl00_ContentPlaceHolder1_txtcpr_id").val() == "") {
                        msg += "请选择关联合同";
                    }
                    optionsArray += "cpr_coper:" + $("#ctl00_ContentPlaceHolder1_txtcpr_id").val() + "|*|";
                    optionsArray += "cpr_copername:" + $("#ctl00_ContentPlaceHolder1_txt_reletive").val() + "|*|";
                    break;
                case "Level":

                    //管理级别
                    if ($("#ctl00_ContentPlaceHolder1_radio_yuanEdit").get(0).checked == false && $("#ctl00_ContentPlaceHolder1_radio_suoEdit").get(0).checked == false) {
                        msg += "请选择项目的管理级别！</br>";
                    }
                    var level = "0";
                    var val = $('input:radio[name="mm"]:checked').val();
                    //if (val == "radio_yuanEdit") {
                    //    level = "0";
                    //}
                    if ($("#ctl00_ContentPlaceHolder1_radio_yuanEdit").parent().attr("class") == "checked") {
                        level = "0";
                    }
                    else {
                        level = "1";
                    }
                    optionsArray += "pro_level:" + level + "|*|";
                    var auditlevel = "0";
                    if ($("#ctl00_ContentPlaceHolder1_audit_yuanEdit").parent().attr("class") != "checked" && $("#ctl00_ContentPlaceHolder1_audit_suoEdit").parent().attr("class") != "checked") {
                        msg += "请选择项目的审核级别！</br>";
                    }
                    if ($("#ctl00_ContentPlaceHolder1_audit_yuanEdit").parent().attr("class") == "checked" && $("#ctl00_ContentPlaceHolder1_audit_suoEdit").parent().attr("class") != "checked") {
                        auditlevel = "0"
                    }
                    if ($("#ctl00_ContentPlaceHolder1_audit_yuanEdit").parent().attr("class") != "checked" && $("#ctl00_ContentPlaceHolder1_audit_suoEdit").parent().attr("class") == "checked") {
                        auditlevel = "1"
                    }
                    if ($("#ctl00_ContentPlaceHolder1_audit_yuanEdit").parent().attr("class") == "checked" && $("#ctl00_ContentPlaceHolder1_audit_suoEdit").parent().attr("class") == "checked") {
                        auditlevel = "2"
                    }
                    optionsArray += "AuditLevel:" + auditlevel + "|*|";

                    break;
                case "cpr_Type":
                    //项目类型
                    if ($("#ctl00_ContentPlaceHolder1_drp_projtype").val() == "-1") {
                        msg += "请选择项目类型！</br>";
                    }
                    optionsArray += "ProjType:" + $("#ctl00_ContentPlaceHolder1_drp_projtype").val() + "|*|";
                    break;
                case "buildArea":
                    //建筑面积
                    if ($("#ctl00_ContentPlaceHolder1_txt_scaleEdit").val() == "") {
                        msg += "请输入建筑面积！</br>";
                    }
                    else {
                        if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txt_scaleEdit").val())) {
                            msg += "建筑面积请输入数字！</br>";
                        }
                    }
                    optionsArray += "ProjectScale:" + $("#ctl00_ContentPlaceHolder1_txt_scaleEdit").val() + "|*|";
                    break;
                case "cprBuildUnit":

                    if ($("#ctl00_ContentPlaceHolder1_txtbuildUnitEdit").val() == "") {
                        msg += "请输入建筑单位！</br>";
                    }

                    optionsArray += "pro_buildUnit:" + $("#ctl00_ContentPlaceHolder1_txtbuildUnitEdit").val() + "|*|";
                    break;
                case "area":
                    //建筑面积
                    if ($("#ctl00_ContentPlaceHolder1_txt_areaEdit").val() == "") {
                        msg += "请输入规划面积！</br>";
                    }
                    else {
                        if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txt_areaEdit").val())) {
                            msg += "规划面积请输入数字！</br>";
                        }
                    }
                    optionsArray += "ProjArea:" + $("#ctl00_ContentPlaceHolder1_txt_areaEdit").val() + "|*|";
                    break;
                    /////                                                                                                                                                                                   
                case "StructType":
                    optionsArray += "pro_StruType:" + $("#ctl00_ContentPlaceHolder1_asTreeviewStructEdit_divDropdownTreeText").text() + "|*|";
                    //                    var seerere = TG.Web.Coperation.cpr_CoperationEditChoose.output_Click();
                    //                    alert(seerere.value);
                    if (!IsStructCheckNode('struct')) {
                        msg += "请选择结构形式(结构样式太长或未选择)！</br>";
                    }

                    break;
                case "BuildStructType":
                    optionsArray += "pro_kinds:" + $("#ctl00_ContentPlaceHolder1_asTreeviewStructTypeEdit_divDropdownTreeText").text() + "|*|";

                    //建筑分类
                    if (!IsStructCheckNode('structtype')) {
                        msg += "请选择建筑分类(建筑分类太长或未选择)！</br>";
                    }
                    break;
                case "buildType":
                    optionsArray += "BuildTypelevel:" + $("#ctl00_ContentPlaceHolder1_drp_buildtype").val() + "|*|";
                    //设计等级
                    if ($("#ctl00_ContentPlaceHolder1_drp_buildtype").val() == "-1") {
                        msg += "请选择建筑类别！</br>";
                    }
                    break;
                    ///                                                                                                                                                                                   
                case "proFuze":
                    if ($("#ctl00_ContentPlaceHolder1_txt_PMNameEdit").val() == "") {
                        msg += "请输入项目经理！</br>";
                    }
                    optionsArray += "PMName:" + $("#ctl00_ContentPlaceHolder1_txt_PMNameEdit").val() + "|*|";
                    optionsArray += "PMPhone:" + $("#ctl00_ContentPlaceHolder1_txt_PMPhoneEdit").val() + "|*|";
                    optionsArray += "PMNameID:" + $("#ctl00_ContentPlaceHolder1_hid_pmuserid").val() + "|*|";
                    if ($("#ctl00_ContentPlaceHolder1_txt_PMPhoneEdit").val() != "") {
                        if (!reg_phone.test($("#ctl00_ContentPlaceHolder1_txt_PMPhoneEdit").val())) {
                            msg += "项目经理电话输入格式不正确！</br>";
                        }
                    }
                    break;
                case "FParty":
                    if ($("#ctl00_ContentPlaceHolder1_txt_ApersonEdit").val() == "") {
                        msg += "请输入甲方负责人！</br>";
                    }
                    optionsArray += "ChgJia:" + $("#ctl00_ContentPlaceHolder1_txt_ApersonEdit").val() + "|*|";
                    optionsArray += "Phone:" + $("#ctl00_ContentPlaceHolder1_txt_phoneEdit").val() + "|*|";
                    if ($("#ctl00_ContentPlaceHolder1_txt_phoneEdit").val() != "") {
                        if (!reg_phone.test($("#ctl00_ContentPlaceHolder1_txt_phoneEdit").val())) {
                            msg += "甲方负责人电话输入格式不正确！</br>";
                        }
                    }
                    break;
                case "cjbm":
                    if ($("#ctl00_ContentPlaceHolder1_txt_unitEdit").val() == "") {
                        msg += "请输入承接部门！</br>";
                    }
                    optionsArray += "Unit:" + $("#ctl00_ContentPlaceHolder1_txt_unitEdit").val() + "|*|";
                    break;
                case "cpr_Account":
                    if ($("#ctl00_ContentPlaceHolder1_txtproAcountEdit").val() == "") {
                        msg += "请输入合同额！</br>";
                    } else {
                        if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txtproAcountEdit").val())) {
                            msg += "合同额格式不正确！</br>";
                        }
                    }
                    optionsArray += "cpr_Acount:" + $("#ctl00_ContentPlaceHolder1_txtproAcountEdit").val() + "|*|";
                    break;

                case "ddProjectPosition":
                    if ($("#ctl00_ContentPlaceHolder1_txtbuildAddressEdit").val() == "") {
                        msg += "请输入建设地点！</br>";
                    }
                    optionsArray += "BuildAddress:" + $("#ctl00_ContentPlaceHolder1_txtbuildAddressEdit").val() + "|*|";
                    break;
                case "ddProfessionTypeCheck":
                    if ($("#ctl00_ContentPlaceHolder1_ddProfessionTypeEdit").val() == "0") {
                        msg += "请输入行业性质！</br>";
                    }
                    optionsArray += "Industry:" + $("#ctl00_ContentPlaceHolder1_ddProfessionTypeEdit").val() + "|*|";
                    break;
                    ////                                                                                                                                                      
                case "ddcpr_Stage":
                    //设计阶段
                    var processLength = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd").length;
                    if (processLength == 0) {
                        msg += "请至少选择一项设计阶段！</br>";
                    }

                    var Stage = "";
                    var see = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd").length;

                    for (var i = 0; i < parseInt(see) ; i++) {
                        var isChecked = $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().attr("class");
                        if (isChecked == undefined) {

                        } else {
                            Stage += $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().parent().next().text() + ",";

                        }
                    }
                    if (Stage == "") {
                        msg += "请选择项目阶段</br>";
                    }
                    optionsArray += "pro_status:" + Stage + "|*|";
                    break;
                    ////                                                                                                                                                      
                case "cpr_join":
                    optionsArray += "cpr_join:"
                    if (document.getElementById("ctl00_ContentPlaceHolder1_chk_ISTrunEconomy").checked) {
                        optionsArray += "ISTrunEconomy:" + 1 + ",";
                    }
                    else {
                        optionsArray += "ISTrunEconomy:" + 0 + ",";
                    }
                    if (document.getElementById("ctl00_ContentPlaceHolder1_chk_ISHvac").checked) {
                        optionsArray += "ISHvac:" + 1 + ",";
                    } else {
                        optionsArray += "ISHvac:" + 0 + ",";
                    }
                    if (document.getElementById("ctl00_ContentPlaceHolder1_chk_ISArch").checked) {
                        optionsArray += "ISArch:" + 1 + ",";
                    }
                    else {
                        optionsArray += "ISArch:" + 0 + ",";
                    }
                    optionsArray += "|*|";
                    break;
                case "ddSourceWay":
                    if ($("#ctl00_ContentPlaceHolder1_ddsourceEdit").val() == "0") {
                        msg += "请输入工程来源！</br>";
                    }
                    optionsArray += "Pro_src:" + $("#ctl00_ContentPlaceHolder1_ddsourceEdit").val() + "|*|";
                    break;
                case "SingnDate":
                    if ($("#ctl00_ContentPlaceHolder1_txt_startdateEdit").val() == "") {
                        msg += "请输入项目签订日期！</br>";

                    }
                    optionsArray += "pro_startTime:" + $("#ctl00_ContentPlaceHolder1_txt_startdateEdit").val() + "|*|";
                    break;
                case "DoneDate":
                    if ($("#ctl00_ContentPlaceHolder1_txt_finishdateEdit").val() == "") {
                        msg += "请输入项目完成日期！</br>";
                    }
                    optionsArray += "pro_finishTime:" + $("#ctl00_ContentPlaceHolder1_txt_finishdateEdit").val() + "|*|";
                    break;
                case "multibuild":
                    if ($("#ctl00_ContentPlaceHolder1_txt_subEdit").val() == "") {
                        msg += "请输入项目特征概况！</br>";
                    }
                    optionsArray += "ProjSub:" + $("#ctl00_ContentPlaceHolder1_txt_subEdit").val() + "|*|";
                    break;
                case "cpr_Remark":
                    optionsArray += "pro_Intro:" + $("#ctl00_ContentPlaceHolder1_txt_remarkEdit").val() + "|*|";
                    break;
            }

        });
        // 
        //提示
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        else {
            $(this).hide();
        }



        var auditObj = {
            ProjectSysNo: $("#HiddenProjectSysNo").val(),
            ProjectName: $("#HiddenProjectName").val(),
            copoption: optionsArray
        };
        jsonDataEntity = Global.toJSON(auditObj);
        isEditApp = "0";
        //弹出审批
        getUserAndUpdateAuditForEdit('0', '0', jsonDataEntity);

    });

    //实例化类容
    //    sendMessageClass = new MessageCommon(messageDialog);
    //    messageDialog = $("#msgReceiverContainer").messageDialog;
    //        "button": {
    //            "发送消息": function () {
    //                //选中用户
    //                var _$mesUser = $(":checkbox[name=messageUser]:checked");

    //                if (_$mesUser.length == 0) {
    //                    alert("请至少选择一个流程评审人！");
    //                    return false;
    //                }

    //                if (isEditApp == "0") {
    //                    getUserAndUpdateAuditForEdit('0', '1', jsonDataEntity);
    //                }
    //            },
    //            "关闭": function () {
    //             $("#btnsub").show();
    //                messageDialog.hide();
    //            }
    //        }
    //    });
    $("#btn_Send").click(function () {
        //选中用户
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程评审人！");
            return false;
        }

        if (isEditApp == "0") {
            getUserAndUpdateAuditForEdit('0', '1', jsonDataEntity);
        }
    });
    var chooseUser = new ChooseUserControl($("#chooseUserMain"), PMCallBack);
    $("#sch_reletive").click(function () {
        //加载数据
        $("#TxtCprName").val("");
        //先赋值
        showDivDialogClass.SetParameters({
            "nowIndex": "reletiveCpr_nowPageIndex", //当前页id
            "pageSize": "10"
        });
        //绑定生产部门数据
        GetReleCprUnit();
        //绑定项目数据
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "1", "reletiveCpr", ReletiveCprCallBack);
        BindAllDataCountReleCpr(); //绑定总数据

    });
    //合同关联-搜索
    $("#btn_SearchByCprName").click(function () {
        var strNameUnit = "";
        var cprName = $("#TxtCprName").val(); //当做where语句
        cprName = showDivDialogClass.ReplaceChars(cprName);
        strNameUnit += cprName + "|";
        var unitID = $("#select_releCprUnit option:selected").val();
        var unitName = $("#select_releCprUnit option:selected").text();

        if (unitID > 0) {
            strNameUnit += unitID + "|";
            strNameUnit += unitName;
        }
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", strNameUnit, "true", "1", "reletiveCpr", ReletiveCprCallBack);
        BindAllDataCountReleCpr(); //绑定总数据
    });
})
function PMCallBack(userObj) {
    $("#ctl00_ContentPlaceHolder1_txt_PMNameEdit").val(userObj.username);
    $("#ctl00_ContentPlaceHolder1_hid_pmnameEdit").val(userObj.username);
    $("#ctl00_ContentPlaceHolder1_txt_PMPhoneEdit").val(userObj.phonenumber);
    $("#ctl00_ContentPlaceHolder1_hid_pmuserid").val(userObj.usersysno);
}

//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAuditForEdit(action, flag, jsonData) {
    //提交地址
    var result = TG.Web.ProjectManage.ModifyProjectAuditBymaster.StartAppProject(jsonData, flag);
    //提交数据
    if (result.value == "0") {
        alert("发起项目申请修改失败，请联系系统管理员！");
    }
    else if (result.value == "1") {
        alert("项目申请修改已提交，不能重复提交！");
    }
    else {
        renderUserOrSendMsg(flag, result.value);
    }
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag == "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息回调方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {

        //提示消息
        alert("发起项目申请修改已成功！\n消息已成功发送到评审人等待评审！");
        //刷新
        window.location.href = "ApplyProjectEditListBymaster.aspx";
    } else {
        alert("消息发送失败！");
    }
}
//比较时间
function duibi(a, b) {
    var arr = a.split("-");
    var starttime = new Date(arr[0], arr[1], arr[2]);
    var starttimes = starttime.getTime();

    var arrs = b.split("-");
    var lktime = new Date(arrs[0], arrs[1], arrs[2]);
    var lktimes = lktime.getTime();

    if (starttimes > lktimes) {
        return false;
    }
    else
        return true;

}
//甲方负责人
function ShowDialogWin_fzr(url) {
    var feature = "dialogWidth:510px;dialogHeight:300px;center:yes;resizable:no";
    var result = window.showModalDialog(url, "", feature);
    //chrom
    if (!$.browser.msie) {
        result = window.returnValue;
    }
    $("#ctl00_ContentPlaceHolder1_txt_ApersonEdit").val(result.Name);
    $("#ctl00_ContentPlaceHolder1_txt_phoneEdit").val(result.Phone);
}
//项目关联
function ShowDialogWin_proReletive(url) {
    var feature = "dialogWidth:570px;dialogHeight:300px;ceter:yes";
    var result = window.showModalDialog(url, "", feature);
    //chrom 下
    if (!$.browser.msie) {
        result = window.returnValue;
    }
    if (result) {
        //合同名称
        $("#ctl00_ContentPlaceHolder1_txt_reletive").val(result[0]);
        //管理级别
        $("#ctl00_ContentPlaceHolder1_ddrank").val($.trim(result[1]));
        //设计阶段
        var chk_val = $.trim(result[2]);
        //重置
        $(".cls_jd :checkbox").attr("checked", false);
        if (chk_val.indexOf(',') > -1) {
            var array = chk_val.split(',');
            for (var i = 0; i < array.length; i++) {
                if (array[i] == "27") {
                    $("#ctl00_ContentPlaceHolder1_CheckBox1").attr("checked", "checked");
                }
                else if (array[i] == "28") {
                    $("#ctl00_ContentPlaceHolder1_CheckBox2").attr("checked", "checked");
                }
                else if (array[i] == "29") {
                    $("#ctl00_ContentPlaceHolder1_CheckBox3").attr("checked", "checked");
                }
            }
        }
        else if (chk_val != "") {
            if (chk_val == "27") {
                $("#ctl00_ContentPlaceHolder1_CheckBox1").attr("checked", "checked");
            }
            else if (chk_val == "28") {
                $("#ctl00_ContentPlaceHolder1_CheckBox2").attr("checked", "checked");
            }
            else if (chk_val == "29") {
                $("#ctl00_ContentPlaceHolder1_CheckBox3").attr("checked", "checked");
            }
        }
        //项目来源
        $("#ctl00_ContentPlaceHolder1_ddsource option[name='" + $.trim(result[3]) + "']").attr("selected", "selected");
        //合同额
        $("#ctl00_ContentPlaceHolder1_txtproAcount").val(result[4]);
        //建设单位
        $("#ctl00_ContentPlaceHolder1_txtbuildUnit").val($.trim(result[5]));
        //承接部门
        $("#ctl00_ContentPlaceHolder1_txt_unitEdit").val($.trim(result[6]));
        $("#ctl00_ContentPlaceHolder1_hid_unit").val($.trim(result[6]));
        //建筑面积
        $("#ctl00_ContentPlaceHolder1_txt_scale").val($.trim(result[7]));
        //甲方
        $("#ctl00_ContentPlaceHolder1_txt_Aperson").val($.trim(result[8]));
        //电话
        $("#ctl00_ContentPlaceHolder1_txt_phone").val($.trim(result[9]));
        //合同ID
        $("#ctl00_ContentPlaceHolder1_txtcpr_id").val(result[10]);

        //触发onBlur事件，检查项目名称是否存在
        $("#txt_name").focus();
    }
}

//绑定页面上的关联值
function BindCprReletive(n) {
    //合同名称
    $("#ctl00_ContentPlaceHolder1_txt_reletive").val(n.cpr_Name);
    //弱关联合同名称
    $("#ctl00_ContentPlaceHolder1_txt_name").val(n.cpr_Name);
    //设计阶段
    //    var chk_val = $.trim(n.cpr_Process);
    //    //重置
    //    $(".cls_jd :checkbox").attr("checked", false);
    //    if (chk_val.indexOf(',') > -1) {
    //        var array = chk_val.split(',');
    //        for (var i = 0; i < array.length; i++) {
    //            if (array[i] == "27") {
    //                $("#CheckBox1").attr("checked", "checked");
    //            }
    //            else if (array[i] == "28") {
    //                $("#CheckBox2").attr("checked", "checked");
    //            }
    //            else if (array[i] == "29") {
    //                $("#CheckBox3").attr("checked", "checked");
    //            }
    //        }
    //    } else if (chk_val != "") {
    //        if (chk_val == "27") {
    //            $("#CheckBox1").attr("checked", "checked");
    //        }
    //        else if (chk_val == "28") {
    //            $("#CheckBox2").attr("checked", "checked");
    //        }
    //        else if (chk_val == "29") {
    //            $("#CheckBox3").attr("checked", "checked");
    //        }
    //    }
    //    //项目来源
    //    $("#ddsource option[name='" + $.trim(n.BuildSrc) + "']").attr("selected", "selected");
    //    //合同额
    //    $("#txtproAcount").val($.trim(n.cpr_Acount));
    //    //建设单位
    //    $("#txtbuildUnit").val($.trim(n.BuildUnit));
    //    //承接部门
    //    $("#txt_unit").val($.trim(n.cpr_Unit));
    //    $("#hid_unit").val($.trim(n.cpr_Unit));
    //    //建设规模
    //    $("#txt_scale").val($.trim(n.BuildArea));

    //    //规划面积
    //    $("#txt_area").val($.trim(n.cpr_Area));
    //    //建设地点
    //    $("#txtbuildAddress").val($.trim(n.BuildPosition));
    //    //管理级别
    //    BindElseTextValue(n.cpr_Id);
    //    //项目经理
    //    $("#txt_PMName").val($.trim(n.ChgPeople));
    //     $("#hid_pmname").val($.trim(n.ChgPeople));
    //     $("#hid_pmuserid").val(n.PMUserID);
    //    //项目经理电话
    //    $("#txt_PMPhone").val($.trim(n.ChgPhone));
    //    //甲方
    //    $("#txt_Aperson").val($.trim(n.ChgJia));
    //    //电话
    //    $("#txt_phone").val($.trim(n.ChgJiaPhone));
    //    //合同ID
    //    $("#txtcpr_id").val($.trim(n.cpr_Id));
    //    //管理级别
    //    GetCoperationLevel($.trim(n.cpr_Id));
    //    //建筑类别
    //    $("#drp_buildtype").val(n.BuildType);
    //    //行业性质
    //    $("#ddProfessionType").val($.trim(n.Industry));
    //    //结构样式
    //    CommonControl.BindASTreeView("asTreeviewStruct_ulASTreeView", n.StructType, $("#StrructContainer"));
    //    //建筑分类
    //    CommonControl.BindASTreeView("asTreeviewStructType_ulASTreeView", n.BuildStructType, $("#buildTypeContainer"));

    //合同ID
    $("#ctl00_ContentPlaceHolder1_txtcpr_id").val($.trim(n.cpr_Id));
    //触发onBlur事件，检查项目名称是否存在
    $("#txt_name").focus();
}
//绑定其他值
function BindElseTextValue(id) {
    var data = "action=getReleCprElseVal&cprid=" + id;
    $.ajax({
        type: "GET",
        url: "../../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            if (result != null) {
                var data = result.ds;
                $.each(data, function (i, n) {
                    if (n.pro_level == "0") {
                        $("#radio_yuan").attr("checked", "checked");
                    } else if (n.pro_level == "1") {
                        $("#radio_suo").attr("checked", "checked");
                    } else {
                        $("#radio_yuan").removeAttr("checked");
                        $("#radio_suo").removeAttr("checked");
                    }
                    //项目特征概况
                    $("#ctl00_ContentPlaceHolder1_txt_sub").val($.trim(n.ProjSub));
                    //备注
                    $("#ctl00_ContentPlaceHolder1_txt_remark").val($.trim(n.pro_Intro));

                    $("#ctl00_ContentPlaceHolder1_drp_projtype").val($.trim(n.ProjType));
                    $("#ctl00_ContentPlaceHolder1_txt_startdate").val(GetShortDate(n.pro_startTime));
                    $("#ctl00_ContentPlaceHolder1_txt_finishdate").val(GetShortDate(n.pro_finishTime));
                });
            } else {
                //alert("部分值关联失败,请手动填写!");
                $("#radio_yuan").removeAttr("checked");
                $("#radio_suo").removeAttr("checked");
                //项目特征概况
                $("#ctl00_ContentPlaceHolder1_txt_sub").val("");
                //备注
                $("#ctl00_ContentPlaceHolder1_txt_remark").val("");
                $("#ctl00_ContentPlaceHolder1_drp_projtype").val("");
                $("#ctl00_ContentPlaceHolder1_txt_startdate").val("");
                $("#ctl00_ContentPlaceHolder1_txt_finishdate").val("");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}

//管理级别
function GetCoperationLevel(id) {
    //地址
    var url = "../../HttpHandler/SimpleHandler.ashx";
    $.get(url, { flag: "getcprlevel", id: id }, function (data) {
        if (data == "0") {
            $("#radio_yuan").atrr("checked", "checked");
        }
        else if (data == "1") {
            $("#radio_suo").atrr("checked", "checked");
        }
    });
}
//承接部门
function showUnit(url) {
    var feature = "dialogWidth:480px;dialogHeight:310px;center:yes;resizable:no";
    var result = window.showModalDialog(url, "", feature);
    //chrom 下
    if (!$.browser.msie) {
        result = window.returnValue;
    }
    $("#ctl00_ContentPlaceHolder1_txt_unitEdit").val(result);
    $("#ctl00_ContentPlaceHolder1_hid_unit").val(result);
}

//加载数据
function GetAttachData() {
    LoadCoperationAttach();
}
//加载附件信息
function LoadCoperationAttach() {
    var data_att = "action=getprojfiles&type=proj&projid=" + $("#ctl00_ContentPlaceHolder1_hidproId").val();
    $.ajax({
        type: "GET",
        url: "../../HttpHandler/CommHandler.ashx",
        data: data_att,
        dataType: "json",
        success: function (result) {
            if (result != null) {
                var filedata = result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").remove();
                }
                //缩略图列表
                if ($("#img_container img").length > 1) {
                    $("#img_container img:gt(0)").remove();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    //显示
                    $("#img_small").show();
                    //克隆
                    var thumd = $("#img_small").clone();
                    //隐藏
                    $("#img_small").hide();
                    var oper = "<span style='color:blue;cursor:pointer;' rel='" + n.ID + "'>删除</span>";
                    var oper2 = "<a href=\"/Coperation/DownLoadFile.aspx?fileName=" + n.FileName + "&FileURL=" + n.FileUrl + "\" target='_blank'>下载</a>";
                    var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                    //显示缩略图
                    var img_path = n.FileUrl.split('/');
                    thumd.attr("src", "../Attach_User/filedata/projfile/" + img_path[0] + "/min/" + img_path[1]);
                    thumd.attr("alt", n.ID);

                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").html(img + n.FileName);
                    row.find("#att_filesize").text(n.FileSizeString);
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper").html(oper);
                    row.find("#att_oper2").html(oper2);
                    row.find("#att_oper span").click(function () {
                        if (confirm("确定要删除本条附件吗？")) {
                            delCprAttach($(this));
                        }
                        //history.go(-1);
                    });
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas_att");
                    //缩略图
                    thumd.appendTo("#img_container");
                    if (i % 3 == 0) {
                        $("img_container").append("<br/>")
                    }
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("加载附件信息错误！");
        }
    });
}
//删除附件
function delCprAttach(link) {
    var data = "action=delcprattach&attid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "text",
        success: function (result) {
            if (result == "yes") {
                link.parents("tr:first").remove();
                //删除缩略图
                $("#img_container img[alt=" + link.attr("rel") + "]").remove();
                alert("附件删除成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}
//承接部门表格数据绑定-CallBack函数
function ProCjbmCallBack(result) {
    if (result != null) {
        var data = result.ds;
        $("#pro_cjbmTable tr:gt(0)").remove();
        $.each(data, function (i, n) {
            var oper = "<span rel='" + n.unit_ID + "' style=\"color:blue;cursor:pointer;\"data-dismiss=\"modal\" >选择</span>";
            var trHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.unit_Name + "</td><td>" + oper + "</td></tr>";
            $("#pro_cjbmTable").append(trHtml);
            $("#pro_cjbmTable span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txt_unitEdit").val($.trim(n.unit_Name));
                $("#ctl00_ContentPlaceHolder1_hid_unit").val($.trim(n.unit_Name));
                //            $("#ctl00_ContentPlaceHolder1_pro_cjbmDiv").dialog().dialog("close");
                //history.go(-1);
            });
        });
        ControlTableCss("pro_cjbmTable");
    }
}
//获得承接部门数据总数
function BindAllDataCount() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "cjbm_prevPage",
        "firstPage": "cjbm_firstPage",
        "nextPage": "cjbm_nextPage",
        "lastPage": "cjbm_lastPage",
        "gotoPage": "cjbm_gotoPageIndex",
        "allDataCount": "cjbm_allDataCount",
        "nowIndex": "cjbm_nowPageIndex",
        "allPageCount": "cjbm_allPageCount",
        "gotoIndex": "cjbm_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", "", "false", "proCjbm", GetCjbmAllDataCount);
    //alert("分页前,当前页数: "+$("#"+ParametersDict.nowIndex+"").text());
    //注册事件,先注销,再注册
    $("#cjbmByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#cjbm_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", pageIndex, "proCjbm", ProCjbmCallBack);
            //showDivDialogClass.BindPageValue($(this).attr("id"));
        }
        //history.go(-1);
    });
}
//承接部门数据总数CallBack函数
function GetCjbmAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        NoDataMessageOnTable("pro_cjbmTable", 3);
    }
}
//合同关联

//获取生产部门
function GetReleCprUnit() {
    var previewPower = showDivDialogClass.UserRolePower.previewPower;
    var userSysNum = showDivDialogClass.UserRolePower.userSysNum;
    var userUnitNum = showDivDialogClass.UserRolePower.userUnitNum;
    var data = "action=getReleCprUnit&previewPower=" + previewPower + "&userSysNum=" + userSysNum + "&userUnitNum=" + userUnitNum;
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            var data = result.ds;
            var optionHtml = '<option value="-1">------------请选择------------</option>';
            $.each(data, function (i, n) {
                optionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
            });
            $("#select_releCprUnit").html(optionHtml);
            $("#select_releCprUnit").unbind('change').change(function () {
                var strNameUnit = "";
                var cprName = $("#TxtCprName").val(); //当做where语句
                cprName = cprName.replace("#", "");
                strNameUnit += cprName + "|";
                var unitID = $("#select_releCprUnit option:selected").val();
                var unitName = $("#select_releCprUnit option:selected").text();
                if (unitID > 0) {
                    strNameUnit += unitID;
                }
                strNameUnit += "|" + unitName;
                showDivDialogClass.GetDataByAJAX("getDataToDivDialog", strNameUnit, "true", "1", "reletiveCpr", ReletiveCprCallBack);
                BindAllDataCountReleCpr(); //绑定总数据
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!");
        }
    });
}
//合同关联绑定表格数据-CallBack
function ReletiveCprCallBack(result) {
    var cprName = $("#TxtCprName").val();
    var data = result.ds;
    var releviteCprHtml;
    $("#cprReletiveTable tr:gt(0)").remove();
    $.each(data, function (i, n) {
        var signdate = GetShortDate(n.cpr_SignDate);
        var oper = "<span rel='" + n.cpr_Id + "' style=\"color:blue;cursor:pointer;\"data-dismiss=\"modal\" >选择</span>";
        var cprNameResult = n.cpr_Name;
        if (cprNameResult.length > 20) {
            cprNameResult = cprNameResult.substr(0, 20) + "...";
        }
        cprNameResult = cprNameResult.replace(cprName, "<span style='color:red'>" + cprName + "</span>");
        releviteCprHtml = '<tr><td title="' + n.cpr_Name + '">' + cprNameResult + '</td><td align=\"center\">' + signdate + '</td><td align=\"center\">' + oper + '</td></tr>';
        $("#cprReletiveTable").append(releviteCprHtml);
        $("#cprReletiveTable span:last").click(function () {
            //绑定关联值
            BindCprReletive(n);
            //            $("#cprReletiveDiv").dialog().dialog("close");
            //history.go(-1);
        });
    });
    ControlTableCss("cprReletiveTable");
}
//获得合同关联数据总数
function BindAllDataCountReleCpr() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "reletiveCpr_prevPage",
        "firstPage": "reletiveCpr_firstPage",
        "nextPage": "reletiveCpr_nextPage",
        "lastPage": "reletiveCpr_lastPage",
        "gotoPage": "reletiveCpr_gotoPage",
        "allDataCount": "reletiveCpr_totalCount",
        "nowIndex": "reletiveCpr_nowPageIndex",
        "allPageCount": "reletiveCpr_PagesCount",
        "gotoIndex": "reletiveCpr_gotoPageNum",
        "pageSize": "10"
    });
    var strNameUnit = $("#TxtCprName").val() + "|";
    var unitID = $("#select_releCprUnit option:selected").val();
    var unitName = $("#select_releCprUnit option:selected").text();
    if (unitID > 0) {
        strNameUnit += unitID;
    }
    strNameUnit += "|" + unitName;

    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", strNameUnit, "true", "reletiveCpr", GetReleCprAllDataCount);
    //alert("分页前,当前页数: "+$("#"+ParametersDict.nowIndex+"").text());
    //注册事件,先注销,再注册
    $("#cprReletivePage span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#reletiveCpr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", strNameUnit, "true", pageIndex, "reletiveCpr", ReletiveCprCallBack);
            //showDivDialogClass.BindPageValue($(this).attr("id"));
        }
        //history.go(-1);
    });
}
//合同关联数据总数CallBack函数
function GetReleCprAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#cprReletiveTable tr:gt(0)").remove();
        $("#reletiveCpr_totalCount").text(0);
        $("#reletiveCpr_nowPageIndex").text(0);
        $("#reletiveCpr_PagesCount").text(0);
        NoDataMessageOnTable("cprReletiveTable", 3);
    }
}
//甲方负责人绑定表格数据CallBack
function JffzrCallBack(result) {
    var data = result.ds;
    var jffzrHtml;
    $("#jffzr_table tr:gt(0)").remove();
    $.each(data, function (i, n) {
        var oper = "<span rel='" + n.Cst_Id + "' style=\"color:blue;cursor:pointer;\"data-dismiss=\"modal\" >选择</span>";
        jffzrHtml = '<tr><td>' + n.Name + '</td><td>' + n.Phone + '</td><td>' + n.Department + '</td><td>' + oper + '</td></tr>';
        $("#jffzr_table").append(jffzrHtml);
        $("#jffzr_table span:last").click(function () {
            $("#ctl00_ContentPlaceHolder1_txt_ApersonEdit").val($.trim(n.Name));
            $("#ctl00_ContentPlaceHolder1_txt_phoneEdit").val($.trim(n.Phone));
            //            $("#jffzr_DialogDiv").dialog().dialog("close");
            //history.go(-1);
        });
    });
    ControlTableCss("jffzr_table");
}
//获取甲方负责人总数据
function BindAllDataCountJffzr() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "jffzr_prevPage",
        "firstPage": "jffzr_firstPage",
        "nextPage": "jffzr_nextPage",
        "lastPage": "jffzr_lastPage",
        "gotoPage": "jffzr_gotoPage",
        "allDataCount": "jffzr_allDataCount",
        "nowIndex": "jffzr_nowPageIndex",
        "allPageCount": "jffzr_allPageCount",
        "gotoIndex": "jffzr_pageIndex",
        "pageSize": "5"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", jffzr_strWhere_Serch, "true", "jffzrCpr", GetJffzrAllDataCount);

    //注册分页事件
    $("#jffzr_forPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#jffzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", jffzr_strWhere_Serch, "true", pageIndex, "jffzrCpr", JffzrCallBack);
        }
        //history.go(-1);
    });
}
//甲方负责人数据总数CallBack函数
function GetJffzrAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#jffzr_table tr:gt(0)").remove();
        $("#jffzr_allDataCount").text(0);
        $("#jffzr_nowPageIndex").text(0);
        $("#jffzr_allPageCount").text(0);
        NoDataMessageOnTable("jffzr_table", 4);
    }
}
//无数据提示
function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}
//转换为短日期格式
function GetShortDate(dateString) {
    if (dateString != "" && dateString != null) {
        var index = 10;
        if (dateString.lastIndexOf("-") > 0) {
            index = dateString.lastIndexOf("-") + 2;
        } else if (dateString.lastIndexOf("/") > 0) {
            index = dateString.lastIndexOf("/") + 2;
        }
        return dateString.substr(0, index);
    } else {
        return "无";
    }
}
//表格样式-各行变色-鼠标覆盖事件
function ControlTableCss(tableId) {
    $("#" + tableId + " tr:gt(0):odd").attr("class", "trOddColor");
    $("#" + tableId + " tr:gt(0)").hover(function () {
        $(this).addClass("mouseOverColor");
    }, function () {
        $(this).removeClass("mouseOverColor");
    });
}

