﻿$(document).ready(function () {
    //行背景
    $("#gv_project tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //得到实际的现实高度
    var h = document.body.clientHeight - 125;
    //表格滚动
    //提示
    $(".chk").tipsy({ opacity: 1 });
    //隔行变色
    $("#gv_project tr:odd").css({ background: "White" });
    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#btn_Search"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });
    CommonControl.SetFormWidth();
    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();

});