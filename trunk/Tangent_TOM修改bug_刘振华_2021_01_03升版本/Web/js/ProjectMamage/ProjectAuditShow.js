﻿$(document).ready(function () {
    loadCoperationAttach();

    var projectAuditEdit = new ProjectAuditClassEdit($("#containerTable"));
    var stauts = $("#hiddenMessageStatus").val();
    var projectSysno = $("#hiddenProjectSysNo").val();
    //判断当前部门
    var curunitid = $("#ctl00_ContentPlaceHolder1_userUnitNum").val();
    //经济所
    if (curunitid == '经济所') {
        $("#ctl00_ContentPlaceHolder1_chk_ISTrunEconomy").hide().parent().parent().parent().parent().hide();
    }
    else if (curunitid == '暖通热力所') {//暖通所
        $("#ctl00_ContentPlaceHolder1_chk_ISHvac").hide().parent().parent().parent().parent().hide();
    }
    else {//土建所
        $("#ctl00_ContentPlaceHolder1_chk_ISArch").hide().parent().parent().parent().parent().hide();
    }
    if (stauts == "D") {
        $("#Button1").click(function () {
            window.history.back();
        })
    }
  
    $("#Button1").click(function () {
        window.history.back();
    });

});
//加载附件信息
function loadCoperationAttach() {
    var data = "action=getprojfiles&type=proj&projid=" + $("#ctl00_ContentPlaceHolder1_hid_projid").val();
    $.ajax({
        type: "GET",
        url: "../../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            if (result != null) {

                var filedata = result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").remove();
                }
                //缩略图列表
                if ($("#img_container img").length > 1) {
                    $("#img_container img:gt(0)").remove();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    //显示
                    $("#img_small").show();
                    //克隆
                    var thumd = $("#img_small").clone();
                    //隐藏
                    $("#img_small").hide();

                    var oper2 = "<a href='../../Attach_User/filedata/projfile/" + n.FileUrl + "' target='_blank'>查看</a>";
                    var img = "<img style='width:16px;height:16px;' src='../../" + n.FileTypeImg + "'/>";

                    //显示缩略图
                    var img_path = n.FileUrl.split('/');
                    thumd.attr("src", "../../Attach_User/filedata/projfile/" + img_path[0] + "/min/" + img_path[1]);

                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").html(img + n.FileName);
                    row.find("#att_filesize").text(n.FileSizeString);
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper2").html(oper2);
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas_att");
                    //缩略图
                    thumd.appendTo("#img_container");
                    if (i % 3 == 0) {
                        $("img_container").append("<br/>")
                    }
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}