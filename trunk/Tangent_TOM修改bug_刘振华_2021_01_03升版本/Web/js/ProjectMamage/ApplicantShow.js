﻿$(document).ready(function() {
    //甲方负责人
    $("#sch_Aperson").click(function() {
        //        var url = "../../Customer/ContactPersionInfo/SmallPer.aspx";
    var url = "../../ProjectManage/pro_smallfzr.aspx";
        ShowDialogWin_fzr(url); 

    });
    //合同关联
    $("#sch_reletive").click(function() {
    var url = "../../ProjectManage/Pro_smallReletive.aspx";
        ShowDialogWin_proReletive(url); 

    });
    //承接部门
    $("#sch_unit").click(function() {
    var url = "../../Coperation/cpr_SmallUnit.aspx";
        showUnit(url);
    }); 

    //行变色
    $(".show_project tr:odd").attr("style", "background-color:#FFF");
    //保存
    $("#btn_save").click(function() {
        var name = $("#txt_name").val();
        var project_No = $("#txt_projectNo").val();
        var ddType = $("#ddType").val();
        var rank = $("#ddrank").val();
        var source = $("#ddsource").val();
        var acount = $("#txtproAcount").val();
        var txtbuildUnit = $("#txtbuildUnit").val();
        var txt_unit = $("#txt_unit").val();
        var txtbuildAddress = $("#txtbuildAddress").val();
        var txt_scale = $("#txt_scale").val();
        var txt_Aperson = $("#txt_Aperson").val();
        var txt_phone = $("#txt_phone").val();
        var txt_startdate = $("#txt_startdate").val();
        var txt_finishdate = $("#txt_finishdate").val();
        var txt_remark = $("#txt_remark").val();

        var msg = "";
        //项目名称
        if (name == "") {
            msg += "项目名称不能为空！</br>";
        }
        //项目编号
        if (project_No == "") {
            msg += "项目编号不能为空！</br>";
        }
        else {
            var reg = /^([A-Z]{2})?-?\d+$/;
            if (!reg.test(project_No)) {
                msg += "项目编号不符合规则";
            }
        }
        //项目类型
        if (ddType == "0") {
            msg += "请选择项目类型！</br>";
        }
        //管理级别
        if (rank == "0") {
            msg += "请选择管理级别！</br>";
        }
        //项目来源
        if (source == "0") {
            msg += "请选择项目来源！</br>";
        }
        //合同额
        if (acount != "") {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test(acount)) {
                msg += "合同额请输入数字！</br>";
            }
        }

        //建设单位
        if (txtbuildUnit == "") {
            msg += "建设单位不能为空！</br>";
        }
        //承接部门
        if (txt_unit == "") {
            msg += "承接部门不能为空！</br>";
        }
        //建设地点
        if (txtbuildAddress == "") {
            msg += "建设地点不能为空！</br>";
        }
        //建设规模
        if (txt_scale == "") {
            msg += "建设规模不能为空！</br>";
        }
        else {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test(txt_scale)) {
                msg += "建设规模请输入数字！</br>";
            }
        }
        //甲方负责人
        if (txt_Aperson == "") {
            msg += "甲方负责人不能为空！</br>";
        }
        //电话
        if (txt_phone == "") {
            msg += "电话不能为空！</br>";
        }
        //项目开始日期
        if (txt_startdate == "") {
            msg += "项目开始日期不能为空！</br>";
        }
        //项目结束日期
        if (txt_finishdate == "") {
            msg += "项目完成日期不能为空!</br>";
        }
        //项目备注
        if (txt_remark == "") {
            msg += "备注不能为空！</br>";
        }
        //项目阶段
        //        if ($("#CheckBox1").attr("checked", "false") & $("#CheckBox2").attr("checked", "false") & $("#CheckBox3").attr("checked", "false")) {
        //            msg += "请选择设计阶段！";
        //        }

        if (msg != "") {

            jAlert(msg, "提示");
            return false;
        } else {

            return true;
        }

    });
})


//甲方负责人
function ShowDialogWin_fzr(url) {
    var feature = "dialogWidth:700px;dialogHeight:300px;center:yes;resizable:no";
    var result = window.showModalDialog(url, "", feature);
    $("#txt_Aperson").val(result);
}
//项目关联
function ShowDialogWin_proReletive(url) {
    var feature = "dialogWidth:500px;dialogHeight:200px;ceter:yes";
    var result = window.showModalDialog(url, "", feature);
    $("#txt_reletive").val(result[0]);
    //    项目来源、设计阶段、建设单位、建设规模、合同额、甲方联系人
    $("#source").val(result[1]);
    $("#chkliststage").val(result[2])
    $("#txtbuildUnit").val(result[3]);
    $("#txt_scale").val(result[4]);
    $("#txtproAcount").val(result[5]);
    $("#txt_Aperson").val(result[6]);
    $("#txt_phone").val(result[7]);
}
//承接部门
function showUnit(url) {
    var feature = "dialogWidth:500px;dialogHeight:200px;center:yes";
    var result = window.showModalDialog(url, "", feature);
    $("#txt_unit").val(result);
}

