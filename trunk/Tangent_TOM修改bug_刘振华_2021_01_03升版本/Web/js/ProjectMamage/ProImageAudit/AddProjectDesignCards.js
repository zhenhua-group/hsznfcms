﻿$(document).ready(function () {

    CommonControl.SetFormWidth();

    $("#projectInfo tr:odd").css({ background: "White" });


    var chooseUser = new ChooseUserControl($("#chooseUserMain"), PMCallBack);

    function PMCallBack(userObj) {
        $("#txt_PMName").val(userObj.username);
        $("#hid_pmname").val(userObj.username);
        $("#hid_pmuserid").val(userObj.usersysno);
    }

    //选择制图人
    $("#sch_PM").click(function () {
        $("#chooseUserMain").dialog({
            autoOpen: false,
            modal: true,
            width: 600,
            height: "auto",
            resizable: false,
            title: "选择执行设总"
        }).dialog("open");
        return false;
    });


    var reg = /^\+?[0-9][0-9]*$/;
    //验证文本框输入整数
    $("input", "#tbSunPlotInfo").change(function () {
        var _inputValue = $(this).val();
        if (!reg.test(_inputValue)) {
            alert("请输入整数");
            $(this).val(0);
        }

        //取得加长倍数
        var lenghts = $(this).attr("lenghts");

        //取得所有次的加长倍数
        var tempCount = 0;
        $("input[lenghts='" + lenghts + "']", "#tbSunPlotInfo").each(function () {
            tempCount = tempCount + parseInt($(this).val());
        });

        $("input[lenghts='Total" + lenghts + "']").val(tempCount);

        var spe = $(this).attr("spe");

        var totalA0 = getA0Count(spe);
        var totalA1 = getA1Count(spe);
        var totalA2 = getA2Count(spe);
        var totalA3 = getA3Count(spe);
        var totalA4 = getA4Count(spe);
        var total = parseFloat(totalA0) + parseFloat(totalA1) + parseFloat(totalA2) + parseFloat(totalA3) + parseFloat(totalA4);
        if (spe == "建筑") {
            $("#totalJz").val(total.toFixed(4));
        }
        else if (spe == "结构") {
            $("#totalJg").val(total.toFixed(4));
        }
        else if (spe == "给排水") {
            $("#totalGps").val(total.toFixed(4));
        }
        else if (spe == "暖通") {
            $("#totalNt").val(total.toFixed(4));
        }
        else if (spe == "电气") {
            $("#totalDq").val(total.toFixed(4));
        }

        var totalZh = parseFloat($("#totalJz").val()) + parseFloat($("#totalJg").val()) + parseFloat($("#totalGps").val()) + parseFloat($("#totalNt").val()) + parseFloat($("#totalDq").val());
        $("#totalZh").val(totalZh.toFixed(1));
    });


    //保存
    $("#btn_save").click(function () {

        var reg = /^\+?[0-9][0-9]*$/;

        var lengthRbtn = $("input[name='rbtlist']:checked").length;
        if (lengthRbtn == 0) {
            alert("请选择出图类型");
            return false;
        }

        if ($("#txt_BluePrintCounts").val() == "") {
            alert("请填写晒图份数");
            return false;
        }

        if (!reg.test($("#txt_BluePrintCounts").val())) {
            alert("晒图份数请输入整数");
            return false;
        }

        if ($.trim($("#txt_PMName").val()).length == 0) {
            alert("请选择出图人");
            return false;
        }

        var finalDataObj = {
            "ProNo": $("#HidProID").val(),
            "PlotType": $("input[name='rbtlist']:checked").next().text(),
            "BlueprintCounts": $("#txt_BluePrintCounts").val(),
            "PlotUserID": $("#hid_pmuserid").val(),
            "PlotUserName": $("#hid_pmname").val(),
            "PlotSub": new Array()
        };

        $.each($("#tbSunPlotInfo tr:lt(25)"), function (index, tr) {
            finalDataObj.PlotSub[index] =
                {
                    "Specialty": $.trim($(tr).children("td:eq(0)").text()),
                    "Mapsheet": $.trim($(tr).children("td:eq(1)").find("span").text()),
                    "StandardCount": $.trim($(tr).children("td:eq(1)").find("input").val()),
                    "QuarterCount": $.trim($(tr).children("td:eq(2)").find("input").val()),
                    "SecondQuarterCount": $.trim($(tr).children("td:eq(3)").find("input").val()),
                    "ThreeQuarterCount": $.trim($(tr).children("td:eq(4)").find("input").val()),
                    "FourQuarterCount": $.trim($(tr).children("td:eq(5)").find("input").val()),
                    "TotalCount": $(tr).children("td:eq(6)").find("input").length > 0 ? $.trim($(tr).children("td:eq(6)").find("input").val()) : 0
                };
        });
        var jsonObj = Global.toJSON(finalDataObj);
        var url = "/HttpHandler/ProjectIma/ProjectPlotInfo.ashx";

        //数据         
        var data = { "Action": "0", "data": jsonObj };
        //提交数据
        $.post(url, data, function (jsonResult) {
            if (jsonResult == "1") {
                alert("保存成功!");
                window.location.href = "/ProjectManage/ProImageAudit/ProjectDesignCardsList.aspx";
            }
            else {
                alert("保存失败");
            }
        });
    });

    //得到数量 A0数量
    function getA0Count(spe) {
        var count1 = 0;
        var count2 = 0;
        var count3 = 0;
        var count4 = 0;
        var count5 = 0;
        $("input[spe='" + spe + "'][lenghts='A0bz']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count1 = count1 + 2 * parseInt($(this).val());
            }
        });

        $("input[spe='" + spe + "'][lenghts='A0fzy']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count2 = count2 + 2.5 * parseInt($(this).val());
            }
        });

        $("input[spe='" + spe + "'][lenghts='A0fze']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count3 = count3 + 3 * parseInt($(this).val());
            }
        });

        $("input[spe='" + spe + "'][lenghts='A0fzs']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count4 = count4 + 3.5 * parseInt($(this).val());
            }
        });

        $("input[spe='" + spe + "'][lenghts='A0lb']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count5 = count5 + 4 * parseInt($(this).val());
            }
        });
        return parseFloat(count1) + parseFloat(count2) + parseFloat(count3) + parseFloat(count4) + parseFloat(count5);
    }


    //得到数量 A1数量
    function getA1Count(spe) {
        var count1 = 0;
        var count2 = 0;
        var count3 = 0;
        var count4 = 0;
        var count5 = 0;

        $("input[spe='" + spe + "'][lenghts='A1bz']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count1 = count1 + 1 * parseInt($(this).val());
            }
        });

        $("input[spe='" + spe + "'][lenghts='A1fzy']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count2 = count2 + 1.25 * parseInt($(this).val());
            }
        });

        $("input[spe='" + spe + "'][lenghts='A1fze']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count3 = count3 + 1.5 * parseInt($(this).val());
            }
        });

        $("input[spe='" + spe + "'][lenghts='A1fzs']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count4 = count4 + 1.75 * parseInt($(this).val());
            }
        });

        $("input[spe='" + spe + "'][lenghts='A1lb']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count5 = count5 + 2 * parseInt($(this).val());
            }
        });

        return parseFloat(count1) + parseFloat(count2) + parseFloat(count3) + parseFloat(count4) + parseFloat(count5);
    }

    //得到数量 A2数量
    function getA2Count(spe) {
        var count1 = 0;
        var count2 = 0;
        var count3 = 0;
        var count4 = 0;
        var count5 = 0;

        $("input[spe='" + $.trim(spe) + "'][lenghts='A2bz']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count1 = count1 + 0.5 * parseInt($(this).val());
            }
        });

        $("input[spe='" + spe + "'][lenghts='A2fzy']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count2 = count2 + 5 * parseInt($(this).val()) / 8;
            }
        });

        $("input[spe='" + spe + "'][lenghts='A2fze']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count3 = count3 + 0.75 * parseInt($(this).val());
            }
        });

        $("input[spe='" + spe + "'][lenghts='A2fzs']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count4 = count4 + 7 * parseInt($(this).val()) / 8;
            }
        });

        $("input[spe='" + spe + "'][lenghts='A2lb']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count5 = count5 + 1 * parseInt($(this).val());
            }
        });

        return parseFloat(count1) + parseFloat(count2) + parseFloat(count3) + parseFloat(count4) + parseFloat(count5);
    }

    //得到数量 A3数量
    function getA3Count(spe) {
        var count1 = 0;
        var count2 = 0;
        var count3 = 0;
        var count4 = 0;
        var count5 = 0;

        $("input[spe='" + $.trim(spe) + "'][lenghts='A3bz']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count1 = count1 + 0.25 * parseInt($(this).val());
            }
        });

        $("input[spe='" + spe + "'][lenghts='A3fzy']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count2 = count2 + 5 * parseInt($(this).val()) / 16;
            }
        });

        $("input[spe='" + spe + "'][lenghts='A3fze']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count3 = count3 + 3 * parseInt($(this).val()) / 8;
            }
        });

        $("input[spe='" + spe + "'][lenghts='A3fzs']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count4 = count4 + 7 * parseInt($(this).val()) / 16;
            }
        });

        $("input[spe='" + spe + "'][lenghts='A3lb']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count5 = count5 + 0.5 * parseInt($(this).val());
            }
        });

        return parseFloat(count1) + parseFloat(count2) + parseFloat(count3) + parseFloat(count4) + parseFloat(count5);
    }

    //得到数量 A4数量
    function getA4Count(spe) {
        var count1 = 0;
        var count2 = 0;
        var count3 = 0;
        var count4 = 0;
        var count5 = 0;
        $("input[spe='" + $.trim(spe) + "'][lenghts='A4bz']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count1 = count1 + 1 * parseInt($(this).val()) / 8;
            }
        });

        $("input[spe='" + spe + "'][lenghts='A4fzy']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count2 = count2 + 5 * parseInt($(this).val()) / 32;
            }
        });

        $("input[spe='" + spe + "'][lenghts='A4fze']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count3 = count3 + 3 * parseInt($(this).val()) / 16;
            }
        });

        $("input[spe='" + spe + "'][lenghts='A4fzs']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count4 = count4 + 7 * parseInt($(this).val()) / 32;
            }
        });

        $("input[spe='" + spe + "'][lenghts='A4lb']", "#tbSunPlotInfo").each(function () {
            if ($(this).val() != "0") {
                count5 = count5 + 0.25 * parseInt($(this).val());
            }
        });

        return parseFloat(count1) + parseFloat(count2) + parseFloat(count3) + parseFloat(count4) + parseFloat(count5);
    }
});