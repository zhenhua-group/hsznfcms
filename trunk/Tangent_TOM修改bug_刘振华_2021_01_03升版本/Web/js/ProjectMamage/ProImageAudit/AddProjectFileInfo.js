﻿$(document).ready(function () {

    CommonControl.SetFormWidth();

    $("#projectInfo tr:odd").css({ background: "White" });


    var reg = /^\+?[0-9][0-9]*$/;
    //验证文本框输入整数
    $("input", "#tbFile td:nth-child(3)").change(function () {
        var _inputValue = $(this).val();
        if (!reg.test(_inputValue)) {
            alert("请输入整数");
            $(this).val(0);
        }
    });

    //保存
    $("#btn_save").click(function () {

        var reg = /^\+?[0-9][0-9]*$/;

        if (!Validation()) {
            return false;
        }
        if (!Validation1()) {
            return false;
        }

        var finalDataObj = {
            "ProNo": $("#HidProID").val(),
            "UserId": $("#HidUserID").val(),
            "File": new Array()
        };

        $.each($("#tbFile tr"), function (index, tr) {
            finalDataObj.File[index] =
                {
                    "num": $.trim($(tr).children("td:eq(0)").text()),
                    "name": $(tr).children("td:eq(1)").find("input").length > 0 ? $.trim($(tr).children("td:eq(1)").find("input").val()) : $.trim($(tr).children("td:eq(1)").text()),
                    "PageCount": $.trim($(tr).children("td:eq(2)").find("input").val()),
                    "IsExist": $.trim($(tr).children("td:eq(3)").find("input:checked").val()),
                    "Remark": $.trim($(tr).children("td:eq(4)").find("input").val())
                };
        });
        var jsonObj = Global.toJSON(finalDataObj);
        var url = "/HttpHandler/ProjectIma/ProjectFileInfo.ashx";

        //数据         
        var data = { "Action": "0", "data": jsonObj };
        //提交数据
        $.post(url, data, function (jsonResult) {
            if (jsonResult == "1") {
                alert("保存成功!");
                window.location.href = "/ProjectManage/ProImageAudit/ProjectFileList.aspx";
            }
            else {
                alert("保存失败");
            }
        });
    });

    //验证
    function Validation() {

        var flag = true;

        //第六行不做控制
        var pageIndex = "0";
        var valueemputy = "0";
        var remarkEmputy = "0";
        var nameEmputy = "0";
        $.each($("#tbFile tr:not(:eq(5))"), function (index, tr) {

            var pageCount = $(tr).children("td:eq(2)").find("input").val();
            if ($.trim(pageCount).length == 0) {
                pageIndex = index;
                valueemputy = pageCount;
                return false;
            }

            var isExist = $(tr).children("td:eq(3)").find("input:checked").val();
            if (isExist == "0") {

                //取得备注
                var remark = $(tr).children("td:eq(4)").find("input").val()
                if ($.trim(remark).length == 0) {
                    pageIndex = index;
                    remarkEmputy = remark;
                    return false;
                }
            }

        });

        var count = 0;
        if (pageIndex <= 4) {
            count = parseInt(pageIndex) + 1;
        }
        else {
            count = parseInt(pageIndex) + 2;
        }


        if ($.trim(valueemputy).length == 0) {
            alert("第" + count + "行，页数不能为空");
            flag = false;
            return false;
        }

        if ($.trim(remarkEmputy).length == 0) {
            alert("第" + count + "行，备注不能为空");
            flag = false;
            return false;
        }


        return flag;
    }

    //验证
    function Validation1() {

        var flag = true;
        var pageIndex = "0";
        var nameEmputy = "0";

        $.each($("#tbFile tr:eq(9)"), function (index, tr) {

            var isExist1 = $(tr).children("td:eq(3)").find("input:checked").val();
            if (isExist1 == "1") {

                //取得备注
                var name = $(tr).children("td:eq(1)").find("input").val()
                if ($.trim(name).length == 0) {
                    pageIndex = index;
                    nameEmputy = name;
                    return false;
                }
            }

        });

        if ($.trim(nameEmputy).length == 0) {
            alert("第" + 10 + "行，名称不能为空");
            flag = false;
            return false;
        }

        var nameEmputy1 = "0";

        $.each($("#tbFile tr:eq(10)"), function (index, tr) {

            var isExist1 = $(tr).children("td:eq(3)").find("input:checked").val();
            if (isExist1 == "1") {

                //取得备注
                var name = $(tr).children("td:eq(1)").find("input").val()
                if ($.trim(name).length == 0) {
                    pageIndex = index;
                    nameEmputy1 = name;
                    return false;
                }
            }

        });

        if ($.trim(nameEmputy).length == 0) {
            alert("第" + 10 + "行，名称不能为空");
            flag = false;
            return false;
        }

        if ($.trim(nameEmputy1).length == 0) {
            alert("第" + 11 + "行，名称不能为空");
            flag = false;
            return false;
        }
        return flag;
    }

});