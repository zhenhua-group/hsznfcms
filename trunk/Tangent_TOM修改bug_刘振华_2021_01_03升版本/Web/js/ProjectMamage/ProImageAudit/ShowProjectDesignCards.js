﻿$(document).ready(function () {

    CommonControl.SetFormWidth();

    $("#projectInfo tr:odd").css({ background: "White" });

    $("input", "#rbtlist").attr("disabled", "disabled");
    //图幅改变
    $("#drop_Mapsheet").change(function () {

        var mapSheet = "0#图";
        if ($("#drop_Mapsheet").val() != "") {
            mapSheet = $("#drop_Mapsheet :selected").text();
        }
        var proID = $("#HidProID").val();
        var subInfo = TG.Web.ProjectManage.ProImageAudit.ShowProjectDesignCards.bindDropProjectPlotSunInfo(mapSheet, proID);
        $("#lblSubInfo").html("");
        $("#lblSubInfo").html(subInfo.value);
    });
});

function LoadProtSubInfo() {
    $.post("/HttpHandler/ProjectIma/ProjectPlotInfo.ashx", { "Action": 1, "proID": $("#HidProID").val() }, function (jsonResult) {
        var json = eval('(' + jsonResult + ')');
        var data = json == null ? "" : json.ds;
        if (data != null && data != "") {
            $("#tb_sub tr:gt(0)").remove();
            $.each(data, function (i, n) {
                var trString = "<tr>";
                trString += "<td align=\"center\" style=\"width:150px\">" + n.Specialty + "</td>";
                trString += "<td align=\"center\" style=\"width:150px\">" + n.Mapsheet + "</td>";
                trString += "<td  align=\"center\" style=\"width:150px\">" + n.LengthenType + "</td>";
                trString += "<td align=\"center\" style=\"align:center;width:200px\">" + n.Counts + "</td>";
                trString += "<td align=\"center\" style=\";width:150px\"></td>";
                trString += "</tr>";
                $("#tb_sub").append(trString);
                $("#tb_sub tr:gt(0)").addClass("cls_TableRow");
            });
        }
    });
}