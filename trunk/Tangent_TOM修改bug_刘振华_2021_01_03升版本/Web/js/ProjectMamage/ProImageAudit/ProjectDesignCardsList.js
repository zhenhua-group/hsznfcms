﻿var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
var startAppControl; //发起审批对象
$(document).ready(function () {
    CommonControl.SetFormWidth();
    //Tab页
    $('#tabs').tabs({ cookie: { expires: 30} });

    //隔行变色
    $("#gvProjectPlotNo tr:even").css({ background: "#f0f0f0" });

    //隔行变色
    $("#gvProjectPlotYes tr:even").css({ background: "#f0f0f0" });

    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#btn_Search"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });

//    $.each($(".progressbar"), function (index, item) {
//        var percent = parseFloat($(item).attr("percent"));
//        $(item).progressbar({
//            value: percent
//        });
//    });
    //改变背景颜色
    ChangedBackgroundColor();

    //当前登录用户SysNo
    var userSysNo = $("#HiddenUserSysNo").val();
    //发起审核按钮
    $("span[id=InitiatedAudit]").live("click",function () {
        //获SysNo
        var proSysNo = $(this).attr("proSysNo");
        var objData =
		{
		    ProjectSysNo: proSysNo,
		    InUser: userSysNo
		};
        var jsonData = Global.toJSON(objData);
        jsonDataEntity = jsonData;
        startAppControl = $(this);
        getUserAndUpdateAudit('0', '0', jsonDataEntity);
    });

    //绑定审批进度
//    $.each($(".progressbar"), function (index, item) {
//        var percent = parseFloat($(item).attr("percent"));
//        $(item).progressbar({
//            value: percent
//        });
//    });

    //审核按钮
    $("span[id=GoAudit]").live("click",function () {
        //获审核记录自增号
        var projectPlotAuditSysNo = $(this).attr("ProjectPlotAuditSysNo");
        window.location.href = "/ProjectManage/ProImageAudit/ProjectDesignCardsAudit.aspx?ProjectPlotAuditSysNo=" + projectPlotAuditSysNo;
    });

    //实例化类容
    messageDialog = $("#msgReceiverContainer").messageDialog({
        "button": {
            "发送消息": function () {
                //选中用户
                var _$mesUser = $(":checkbox[name=messageUser]:checked");

                if (_$mesUser.length == 0) {
                    alert("请至少选择一个流程审批人！");
                    return false;
                }

                getUserAndUpdateAudit('0', '1', jsonDataEntity);
            },
            "关闭": function () {
                messageDialog.hide();
            }
        }

    });
    sendMessageClass = new MessageCommon(messageDialog);

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = "";
    paramEntity.currYear = "";
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();

    //提示
    $(".cls_column").tipsy({ opacity: 1 });
});

//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectIma/ProjectPlotAuditInfo.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    $.post(url, data, function (jsonResult) {
        //发起审批返回数据
        if (jsonResult == "0") {
            alert("流程审批错误，请联系管理员！");
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息回调方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        messageDialog.hide();
        //提示消息
        alert("发起出图卡审批已成功！\n消息已成功发送到审批人等待审批！");
        //改变流程提示状态
        CommonControl.SetApplyStatus(startAppControl);
    } else {
        alert("消息发送失败！");
    }
}