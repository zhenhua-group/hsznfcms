﻿var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
//合同审核列表
$(function () {

    CommonControl.SetFormWidth();
    //设置Table样式
    CommonControl.SetTableStyle("gv_Coperation");

    //行背景
    $("#gv_project tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });

    //隔行变色

    $("#gv_project tr:even").css({ background: "White" });
    //设置文本框样式
    CommonControl.SetTextBoxStyle();

    ChangedBackgroundColor();
    //当前登录用户SysNo
    var userSysNo = $("#HiddenUserSysNo").val();
    //发起审核按钮
    $("span[id=InitiatedAudit]").click(function () {
        //获取项目SysNo
        var proSysNo = $(this).attr("proSysNo");
        var objData =
		{
		    ProjectSysNo: proSysNo,
		    InUser: userSysNo
		};
//        var jsonData = Global.toJSON(objData);
//        $.post("/HttpHandler/ProjectIma/ProImaAuditHandler.ashx", { "Action": 0, "data": jsonData }, function (jsonResult) {
//            if (parseInt(jsonResult) > 0) {
//                alert("发起工程设计审核成功，等待技术质量部人员审批！");
//                //查询系统新消息
//                CommonControl.GetSysMsgCount("/ProjectManage/ProImageAudit/ApplyProImage.aspx");
//            } else if (jsonResult == "-1") {
//                alert("没有审核人！");
//            } else {
//                alert("此工程设计无需再次申请或者服务器错误！请联系系统管理员！");
//            }
//        });

        var jsonData = Global.toJSON(objData);
        jsonDataEntity = jsonData;
        getUserAndUpdateAudit('0', '0', jsonDataEntity);
    });

    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("percent"));
        $(item).progressbar({
            value: percent
        });
    });


    //审核按钮
    $("span[id=GoAudit]").click(function () {
        //获取合同系统自增号
        var proSysNo = $(this).attr("proSysNo");
        var proImaAuditSysID = $(this).attr("proAuditSysNo");

        window.location.href = "/ProjectManage/ProImageAudit/ProImageAudit.aspx?projectImaAuditSysNo=" + proImaAuditSysID;
    });
  
    //实例化类容
    messageDialog = $("#msgReceiverContainer").messageDialog({
        "button": {
            "发送消息": function () {
                //选中用户
                var _$mesUser = $(":checkbox[name=messageUser]:checked");

                if (_$mesUser.length == 0) {
                    alert("请至少选择一个流程审批人！");
                    return false;
                }

                getUserAndUpdateAudit('0', '1', jsonDataEntity);
            },
            "关闭": function () {
                messageDialog.hide();
            }
        }
    });
    sendMessageClass = new MessageCommon(messageDialog);
});
//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectIma/ProImaAuditHandler.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    $.post(url, data, function (jsonResult) {
        if (jsonResult == "0") {
            alert("流程审批错误，请联系管理员！");
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息回调方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        messageDialog.hide();
        //提示消息
        alert("发起出图卡审批已成功！\n消息已成功发送到审批人等待审批！");
        //查询系统新消息
        CommonControl.GetSysMsgCount("/ProjectManage/ProImageAudit/ApplyProImage.aspx");
    } else {
        alert("消息发送失败！");
    }
}