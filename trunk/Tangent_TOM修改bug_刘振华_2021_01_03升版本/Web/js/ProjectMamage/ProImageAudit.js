﻿
var auditRecordLength = 2;
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
$(document).ready(function () {

    //当前合同审核记录
    var auditRecordStatus = $("#HiddenStatus").val();

    //检查用户权限
    var hasPower = TG.Web.ProjectManage.ProImageAudit.ProImageAudit.CheckPower(auditRecordStatus, $("#HiddenLoginUser").val());

    //取得审核用户
    var auditUserArr = $("#HiddenAuditUser").val().split(",");
    //取得审核时间
    var auditDate = $("#HiddenAuditDate").val().split(",");
    //消息审核状态
    var messageStauts = $("#hiddenMessageStatus").val();
    //判断审批阶段显示审批表单
    if (messageStauts == auditRecordStatus || messageStauts == "") {
        InitViewState(auditRecordStatus, hasPower);
    }
    else {
        InitViewMessageState(messageStauts);
    }
    if ($("#HiddenOneSuggestion").val().length > 0) {
        $("#txtOneSuggsion").val($("#HiddenOneSuggestion").val());
        $("#txtOneSuggsion").attr("disabled", "disabled");
        //填充审核人和审核时间
        $("#AuditUser", $("#oneTable")).text(auditUserArr[0] + "     /       " + auditDate[0]);
    }

    if ($("#HiddenTwoSuggestion").val().length > 0) {
        $("#txtTwoSuggsion").val($("#HiddenTwoSuggestion").val());
        $("#txtTwoSuggsion").attr("disabled", "disabled");
        //填充审核人和审核时间
        $("#AuditUser", $("#TwoTable")).text(auditUserArr[1] + "     /       " + auditDate[1]);
    }

    //审核通过按钮
    $("#btnApproval").click(function () {
        var auditObj = {
            SysNo: $("#HiddenSysNo").val(),
            ProjectSysNo: $("#HiddenProSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            Status: auditRecordStatus

        };
        //判断当前是在哪个状态
        switch (auditRecordStatus) {
            //新规状态                        
            case "A":
                if ($.trim($("#txtOneSuggsion").val()).length == 0) {
                    alert("技术质量部意见不能为空！");
                    return false;
                }
                auditObj.OneSuggestion = $("#txtOneSuggsion").val();
                break;
            //技术质量部通过               
            case "B":
                //B为部门负责人审核通过之后再次审核通过。结果为D
                if ($.trim($("#txtTwoSuggsion").val()).length == 0) {
                    alert("生产经营部意见不能为空！");
                    return false;
                }
                auditObj.TwoSuggestion = $("#txtTwoSuggsion").val();
                break;
        }
     

        var jsonObj = Global.toJSON(auditObj);
        jsonDataEntity = jsonObj;
        //合同最后完成阶段
        if (auditRecordStatus == "B") {
            getUserAndUpdateAudit('1', '1', jsonDataEntity);
        }
        else {
            getUserAndUpdateAudit('1', '0', jsonDataEntity);
        }
    });

    //审核不通过按钮，具体流程业务，基本同上面的审核通过一致
    $("#btnRefuse").click(function () {
        var auditObj = {
            SysNo: $("#HiddenSysNo").val(),
            ProjectSysNo: $("#HiddenProSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            Status: auditRecordStatus
        };
        switch (auditRecordStatus) {
            //新规状态                        
            case "A":
                if ($.trim($("#txtOneSuggsion").val()).length == 0) {
                    alert("技术质量部意见不能为空！");
                    return false;
                }
                auditObj.OneSuggestion = $("#txtOneSuggsion").val();
                break;
            //技术质量部通过               
            case "B":
                //B为部门负责人审核通过之后再次审核通过。结果为D
                if ($.trim($("#txtTwoSuggsion").val()).length == 0) {
                    alert("生产经营部意见不能为空！");
                    return false;
                }
                auditObj.TwoSuggestion = $("#txtTwoSuggsion").val();
                break;

        }
        var jsonObj = Global.toJSON(auditObj);
        Global.SendRequest("/HttpHandler/ProjectIma/ProImaAuditHandler.ashx", { "Action": 2, "data": jsonObj, "flag": 1 }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("审核失败！");
            } else {
                if (jsonResult == "1") {

                    //改变消息状态
                    var msg = new MessageCommProjAllot($("#msgno").val());
                    msg.ReadMsg();
                    //消息
                    alert("审核不通过成功，消息已发送给申请人！");
                }
                //查询系统新消息
                window.location.href = "/ProjectManage/ProImageAudit/ApplyProImage.aspx";
            }

        });
    });
    //返回重新审核按钮
    $("#FallBackCoperaion").click(function () {
        $.post("/HttpHandler/ProjectIma/ProImaAuditHandler.ashx", { "Action": 3, "CoperationAuditSysNo": $("#HiddenSysNo").val() }, function (jsonResult) {
            //查询系统新消息
            CommonControl.GetSysMsgCount("/ProjectManage/ProImageAudit/ApplyProImage.aspx");
        });
    });

    //实例化类容
    messageDialog = $("#msgReceiverContainer").messageDialog({
        "button": {
            "发送消息": function () {
                //选中用户
                var _$mesUser = $(":checkbox[name=messageUser]:checked");

                if (_$mesUser.length == 0) {
                    alert("请至少选择一个流程审批人！");
                    return false;
                }

                getUserAndUpdateAudit('1', '1', jsonDataEntity);
            },
            "关闭": function () {
                messageDialog.hide();
            }
        }
    });
    sendMessageClass = new MessageCommon(messageDialog);
});

//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/ProjectIma/ProImaAuditHandler.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    Global.SendRequest(url, data, null, null, function (jsonResult) {
        //改变消息状态
        if (flag == "1") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
        }

        if (jsonResult == "0") {
            alert("流程审批错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("工程出图卡审批流程完成，已全部通过！");
            //查询系统新消息
            window.location.href = "/Coperation/cpr_SysMsgListView.aspx";
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        messageDialog.hide();
        alert("工程出图审核已成功！\n消息已成功发送到审批人等待审批！");
        //查询系统新消息
        window.location.href = "/Coperation/cpr_SysMsgListView.aspx";
    } else {
        alert("消息发送失败！");
    }
}



//审核不通过按钮和审核通过按钮不可用
function DisableButton() {
    $(":button[name=controlBtn]").attr("disabled", true);
}

//重新申请审批按钮
function AuditAgain() {
    $("#FallBackCoperaion").show();
}

//没有权限审核
function NoPowerAudit() {
    $("#NoPowerTable").show();
}

//初始化页面状态
function InitViewState(auditStatus, hasPower) {
    var length = 0;
    switch (auditStatus) {
        case "B":
            length = 2;
            if (hasPower == "0") {
                //没有权限的场合
                length--;
                NoPowerAudit();
                DisableButton();
            }
            break;
        case "C":
            length = 1;
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;
        case "D":
            length = 3;
            if (hasPower == "0") {
                length--;
                NoPowerAudit();
                DisableButton();
            }
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            $("#backBtn").show();
            $("#printBtn").show();
            $("#dwprintBtn").show();
            break;
        case "E":
            length = 2;
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            AuditAgain();
            break;

    }
    showTable(length);
}
//初始化消息状态页面信息--审核人只能显示自己审核信息
function InitViewMessageState(messageStauts) {
    var length = 0;
    $("table:eq(0)", $("#TableContainer")).hide();
    if (messageStauts == "A") {
        $("table:eq(0)", $("#TableContainer")).show();
    }
    if (messageStauts == "B" || messageStauts == "C") {
        $("table:eq(1)", $("#TableContainer")).show();
    }
   
    $("#btnApproval").hide();
    $("#btnRefuse").hide();
}
function showTable(length) {
    $("table:lt(" + length + ")", $("#TableContainer")).show();
}
    
