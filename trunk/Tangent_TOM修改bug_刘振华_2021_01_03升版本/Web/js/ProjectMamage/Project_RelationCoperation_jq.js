﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectMamage/ProjectListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '项目名称', '关联合同', '建筑类别', '合同额(万元)', '立项状态', '开始日期', '结束日期', '执行设总', '管理级别', '审核级别', '建设单位', '建设地点', '建设规模(㎡)', '承接部门', '合同额(万元)', '结构形式', '建筑分类', '设计阶段', '项目来源', '行业性质', '甲方负责人', '甲方负责人电话', '执行设总电话', '参与部门', '', '', '项目概况', '项目备注', '录入人', '录入时间', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'CoperationSysNo', index: 'CoperationSysNo', hidden: true, editable: true },
                             { name: 'pro_name', index: 'pro_name', width: 250, formatter: colNameShowFormatter },
                             { name: 'pro_reletive', index: 'pro_reletive', width: 100, align: 'center' },
                             { name: 'BuildType', index: 'BuildType', width: 80, align: 'center' },
                             { name: 'Cpr_Acount', index: 'Cpr_Acount', width: 80, align: 'center' },
                             { name: 'AuditStatus', index: 'AuditStatus', width: 80, align: 'center', formatter: colStatusShowFormatter },
                             { name: 'qdrq', index: 'pro_startTime', width: 100, align: 'center' },
                             { name: 'wcrq', index: 'pro_finishTime', width: 100, align: 'center' },
                             { name: 'PMUserName', index: 'PMName', width: 60, align: 'center' },
                              { name: 'pro_jb', index: 'pro_level', width: 80, align: 'center', hidden: true },
                               {
                                   name: 'AuditLevel', index: 'AuditLevel', width: 80, align: 'center', hidden: true, formatter: function colAuditName(celvalue, options, rowData) {
                                       var str = "";
                                       if ($.trim(celvalue) != "1,0") {
                                           str = "院审";
                                       }
                                       else if ($.trim(celvalue) != "0,1") {
                                           str = "所审";
                                       }
                                       else if ($.trim(celvalue) != "1,1") {
                                           str = "院审,所审";
                                       }
                                       return str;
                                   }
                               },
                                { name: 'pro_buildUnit', index: 'pro_buildUnit', width: 80, hidden: true },
                                 { name: 'BuildAddress', index: 'BuildAddress', width: 80, hidden: true },
                                 { name: 'ProjectScale', index: 'ProjectScale', width: 80, align: 'center', hidden: true },
                               { name: 'Unit', index: 'Unit', width: 80, align: 'center', hidden: true },
                               { name: 'Cpr_Acount', index: 'Cpr_Acount', width: 80, align: 'center', hidden: true },
                                { name: 'pro_StruType', index: 'pro_StruType', width: 80, hidden: true },
                                 { name: 'pro_kinds', index: 'pro_kinds', width: 80, hidden: true },
                                 { name: 'pro_status', index: 'pro_status', width: 80, hidden: true },
                               { name: 'pro_from', index: 'Pro_src', width: 80, align: 'center', hidden: true },
                                { name: 'Industry', index: 'Industry', width: 80, align: 'center', hidden: true },
                                  { name: 'ChgJia', index: 'ChgJia', width: 60, align: 'center', hidden: true },
                                 { name: 'Phone', index: 'Phone', width: 80, align: 'center', hidden: true },                                
                               { name: 'PMPhone', index: 'PMPhone', align: 'center', width: 80, hidden: true },
                                { name: 'ISTrunEconomy', index: 'ISTrunEconomy', width: 80, hidden: true, formatter: colISFormatter },
                                 { name: 'ISHvac', index: 'ISHvac', hidden: true },
                                 { name: 'ISArch', index: 'ISArch', hidden: true },
                                 { name: 'ProjSub', index: 'ProjSub', width: 80, hidden: true },
                                 { name: 'pro_Intro', index: 'pro_Intro', width: 80, hidden: true },
                               { name: 'InsertUser', index: 'InsertUserID', align: 'center', width: 80, hidden: true },
                               { name: 'lrsj', index: 'InsertDate', width: 70, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', width: 80, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "relationcpr", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()) },
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        //      var year = $("#drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, "startTime": startTime, "endTime": endTime },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        //var year = $("#drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, "startTime": startTime, "endTime": endTime },
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //点击全部基本合同
    $(":checkbox:first", $("#columnsid")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_relation", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid")).click(function () {

        var checkedstate = $(this).parent().attr("class");
        var columnsname = $(this).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid")).length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid").hideCol(columnsname);
        }

        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_relation", columnslist, { expires: 365, path: "/" });

    });
    //初始化显示cookies字段列
    InitColumnsCookies();
});
//其他部门参与
function colISFormatter(celvalue, options, rowData) {
    var str = "";
    if ($.trim(celvalue) == "1") {
        str = "经济所,"
    }
    if ($.trim(rowData["ISHvac"]) == "1") {
        str += "暖通所,";
    }
    if ($.trim(rowData["ISArch"]) == "1") {
        str += "土建所,";
    }
    return str;
}
//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';
}
//状态
function colStatusShowFormatter(celvalue, options, rowData) {
    var str = "";
    if (celvalue == "D") {
        str = "立项通过";
    }
    else {
        str = "正在审批";
    }
    return str;
}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "EditProjectRelationBymaster.aspx?flag=list&pro_id=" + celvalue;
    return '<a href="' + pageurl + '" alt="关联合同">关联合同</a>';

}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {
    $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}
//初始化显示cookies字段列
var InitColumnsCookies = function () {
    //基本合同列表
    var columnslist = $.cookie("columnslist_relation");
    if (columnslist != null && columnslist != undefined && columnslist != "undefined" && columnslist != "") {
        var list = columnslist.split(",");
        $.each(list, function (i, c) {
            $(":checkbox:not(:first)[value='" + c + "']", $("#columnsid")).each(function (j, n) {
                var span = $(n).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(n).attr("checked", true);
                $("#jqGrid").showCol($(n).val());
            });

        });
        //是否需要选中 全选框
        if ((list.length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }

    }
}