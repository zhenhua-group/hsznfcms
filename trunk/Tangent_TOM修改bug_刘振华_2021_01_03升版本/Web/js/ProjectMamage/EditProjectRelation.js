﻿$(document).ready(function () {
    //行变色
    $(".show_project>tbody>tr:odd").attr("style", "background-color:#FFF");
    //加载数据
    loadCoperationAttach();
    //保存合同号
    $("#btn_Save").click(function () {
        if ($("#hid_cprid").val() == "") {
            alert("请选择要关联的合同信息！");
            return false;
        }
    });
    //绑定权限
    showDivDialogClass.UserRolePower = {
        "previewPower": $("#previewPower").val(),
        "userSysNum": $("#userSysNum").val(),
        "userUnitNum": $("#userUnitNum").val(),
        "notShowUnitList": ""
    };
    //合同关联
    $("#btn_select").click(function () {
        //var url = "Pro_smallReletive.aspx";
        //ShowDialogWin_proReletive(url);
        $("#TxtCprName").val("");
        //先赋值
        showDivDialogClass.SetParameters({
            "nowIndex": "reletiveCpr_nowPageIndex", //当前页id
            "pageSize": "10"
        });
        //绑定生产部门数据
        GetReleCprUnit();
        //绑定项目数据
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "1", "reletiveCpr", ReletiveCprCallBack);
        BindAllDataCountReleCpr(); //绑定总数据
        $("#cprReletiveDiv").dialog({
            autoOpen: false,
            modal: true,
            width: 590,
            top: 100,
            height: 405,
            resizable: false,
            title: "项目关联合同"
        }).dialog("open");
    });
    //合同关联搜索方法
    $("#btn_SearchByCprName").click(function () {
        var strNameUnit = "";
        var cprName = $("#TxtCprName").val(); //当做where语句
        cprName = showDivDialogClass.ReplaceChars(cprName);
        strNameUnit += cprName + "|";
        var unitID = $("#select_releCprUnit option:selected").val();
        var unitName = $("#select_releCprUnit option:selected").text();
        if (unitID > 0) {
            strNameUnit += unitID;
        }
        strNameUnit += "|" + unitName;
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", strNameUnit, "true", "1", "reletiveCpr", ReletiveCprCallBack);
        BindAllDataCountReleCpr(); //绑定总数据
    });
    CommonControl.SetFormWidth();
});

//加载附件信息
function loadCoperationAttach() {
    var data = "action=getprojfiles&type=proj&projid=" + projid;
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            if (result == null)
                return false;
            var filedata = result.ds;
            if ($("#datas_att tr").length > 1) {
                $("#datas_att tr:gt(0)").remove();
            }
            //缩略图列表
            if ($("#img_container img").length > 1) {
                $("#img_container img:gt(0)").remove();
            }
            $.each(filedata, function (i, n) {
                var row = $("#att_row").clone();
                //显示
                $("#img_small").show();
                //克隆
                var thumd = $("#img_small").clone();
                //隐藏
                $("#img_small").hide();

                var oper2 = "<a href='../Attach_User/filedata/projfile/" + n.FileUrl + "' target='_blank'>查看</a>";
                var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";

                //显示缩略图
                var img_path = n.FileUrl.split('/');
                thumd.attr("src", "../Attach_User/filedata/projfile/" + img_path[0] + "/min/" + img_path[1]);

                row.find("#att_id").text(n.ID);
                row.find("#att_filename").attr("align","left").html(img + n.FileName);
                row.find("#att_filesize").text(n.FileSizeString);
                row.find("#att_filetype").text(n.FileType);
                row.find("#att_uptime").text(n.UploadTime);
                row.find("#att_oper2").html(oper2);
                row.appendTo("#datas_att");
                //缩略图
                thumd.appendTo("#img_container");
                if (i % 3 == 0) {
                    $("img_container").append("<br/>")
                }
                //隐藏原始图
            });
            filedata = "";
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}

//项目关联
/*
function ShowDialogWin_proReletive(url) {
var feature = "dialogWidth:570px;dialogHeight:300px;ceter:yes";
var result = window.showModalDialog(url, "", feature);
//chrom 下
if (!$.browser.msie) {
result = window.returnValue;
}
if (result) {
//合同名称
$("#txt_coperation").val(result[5]);
//合同ID
$("#hid_cprid").val(result[10]);
}
}*/

//获取生产部门
function GetReleCprUnit() {
    var previewPower = showDivDialogClass.UserRolePower.previewPower;
    var userSysNum = showDivDialogClass.UserRolePower.userSysNum;
    var userUnitNum = showDivDialogClass.UserRolePower.userUnitNum;
    var data = "action=getReleCprUnit&previewPower=" + previewPower + "&userSysNum=" + userSysNum + "&userUnitNum=" + userUnitNum;
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            var data = result.ds;
            var optionHtml = '<option value="-1">--------请选择--------</option>';
            $.each(data, function (i, n) {
                optionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
            });
            $("#select_releCprUnit").html(optionHtml);

            $("#select_releCprUnit").unbind('change').change(function () {
                var strNameUnit = "";
                var cprName = $("#TxtCprName").val(); //当做where语句
                cprName = cprName.replace("#", "");
                strNameUnit += cprName + "|";

                var unitID = $("#select_releCprUnit option:selected").val();
                var unitName = $("#select_releCprUnit option:selected").text();
                if (unitID > 0) {
                    strNameUnit += unitID;
                }
                strNameUnit += "|" + unitName;
                showDivDialogClass.GetDataByAJAX("getDataToDivDialog", strNameUnit, "true", "1", "reletiveCpr", ReletiveCprCallBack);
                BindAllDataCountReleCpr(); //绑定总数据
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //s   alert("系统错误!");
        }
    });
}
//合同关联绑定表格数据-CallBack
function ReletiveCprCallBack(result) {
    var cprName = $("#TxtCprName").val();
    var data = result.ds;
    var releviteCprHtml;
    $("#cprReletiveTable tr:gt(0)").remove();
    $.each(data, function (i, n) {
        var signdate = GetShortDate(n.cpr_SignDate);
        var oper = "<span rel='" + n.cpr_Id + "' style=\"color:blue;cursor:pointer;\">选择</span>";
        var cprNameResult = n.cpr_Name;
        if (cprNameResult.length > 20) {
            cprNameResult = cprNameResult.substr(0, 20) + "...";
        }
        cprNameResult = cprNameResult.replace(cprName, "<span style='color:red'>" + cprName + "</span>");
        releviteCprHtml = '<tr><td align=\"left\">' + cprNameResult + '</td><td align=\"center\">' + signdate + '</td><td align=\"center\">' + oper + '</td></tr>';
        $("#cprReletiveTable").append(releviteCprHtml);
        $("#cprReletiveTable span:last").click(function () {
            //绑定值
            //合同名称
            $("#txt_coperation").val(n.cpr_Name);
            //合同ID
            $("#hid_cprid").val(n.cpr_Id);
            $("#cprReletiveDiv").dialog().dialog("close");
        });
    });
    ControlTableCss("cprReletiveTable");
}
//获得合同关联数据总数
function BindAllDataCountReleCpr() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "reletiveCpr_prevPage",
        "firstPage": "reletiveCpr_firstPage",
        "nextPage": "reletiveCpr_nextPage",
        "lastPage": "reletiveCpr_lastPage",
        "gotoPage": "reletiveCpr_gotoPage",
        "allDataCount": "reletiveCpr_allDataCount",
        "nowIndex": "reletiveCpr_nowPageIndex",
        "allPageCount": "reletiveCpr_allPageCount",
        "gotoIndex": "reletiveCpr_pageIndex",
        "pageSize": "10"
    });
    var strNameUnit = $("#TxtCprName").val() + "|";
    var unitID = $("#select_releCprUnit option:selected").val();
    var unitName = $("#select_releCprUnit option:selected").text();
    if (unitID > 0) {
        strNameUnit += unitID;
    }
    strNameUnit += "|" + unitName;
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", strNameUnit, "true", "reletiveCpr", GetReleCprAllDataCount);
    //alert("分页前,当前页数: "+$("#"+ParametersDict.nowIndex+"").text());
    //注册事件,先注销,再注册
    $("#cprReletivePage a").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#reletiveCpr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", strNameUnit, "true", pageIndex, "reletiveCpr", ReletiveCprCallBack);
            //showDivDialogClass.BindPageValue($(this).attr("id"));
        }
    });
}
//合同关联数据总数CallBack函数
function GetReleCprAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#cprReletiveTable tr:gt(0)").remove();
        $("#reletiveCpr_allDataCount").text(0);
        $("#reletiveCpr_nowPageIndex").text(0);
        $("#reletiveCpr_allPageCount").text(0);
        NoDataMessageOnTable("cprReletiveTable", 3);
    }
}

//无数据提示
function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}
//转换为短日期格式
function GetShortDate(dateString) {
    if (dateString != "" && dateString != null) {
        var index = 10;
        if (dateString.lastIndexOf("-") > 0) {
            index = dateString.lastIndexOf("-") + 2;
        } else if (dateString.lastIndexOf("/") > 0) {
            index = dateString.lastIndexOf("/") + 2;
        }
        return dateString.substr(0, index);
    } else {
        return "无";
    }
}
//表格样式-各行变色-鼠标覆盖事件
function ControlTableCss(tableId) {
    $("#" + tableId + " tr:gt(0):odd").attr("class", "trOddColor");
    $("#" + tableId + " tr:gt(0)").hover(function () {
        $(this).addClass("mouseOverColor");
    }, function () {
        $(this).removeClass("mouseOverColor");
    });
}
