﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //行背景
    $("#grid_mem tr").hover(function () {
        $(this).addClass("tr_in");
    }, function () {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#grid_mem tr:even").css({ background: "White" });

    //添加
    $("#btn_showadd").click(function () {
        Clear();
        $("#div_add").show("slow");
        $("#div_edit").hide("slow");
        $("#btn_close1").show();

        //初始化分配额
        GetAllotValue("0");

        //初始化人员
        if ($("#ctl00_ContentPlaceHolder1_drp_unit0_1").get(0).selectedIndex > 0) {
            GetMember($("#ctl00_ContentPlaceHolder1_drp_unit0_1").val());
        }

    });
    $(".cls_select").live("click", function () {
        $("#div_add").hide("slow");
        $("#div_edit").show("slow");
        $("#btn_close2").show();
    });
    //添加
    $("#ctl00_ContentPlaceHolder1_btn_save").click(function () {
        var msg = "";
        //生产部门
        if ($("#ctl00_ContentPlaceHolder1_drp_unit0").val() == "-1") {
            msg += "请选择要生产部门！<br/>";
        }
        //人员
        if ($("#drp_mem0").val() == "-1") {
            msg += "请选择人员！<br/>";
        }
        //年份
        if ($("#ctl00_ContentPlaceHolder1_drp_allotyear0").val() == "-1") {
            msg += "请选择年份！<br/>";
        }

        if ($("#ctl00_ContentPlaceHolder1_txt_allot0").val() == "") {
            msg += "请填写所补产值！<br/>";
        }
        else {
            var reg = /^-?\d+\.?\d{0,2}$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_allot0").val())) {
                msg += "所补产值格式不正确！</br>";
            }
        }

        if (parseFloat($.trim($("#ctl00_ContentPlaceHolder1_txt_allot0").val())) <= 0) {
            msg += "所补产值必须大于0！";
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_hid_totalcount").val()) < parseFloat($("#ctl00_ContentPlaceHolder1_stxt_allot0").val())) {
            msg += "所补产值不能大于分配补贴总额！<br>";
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });

    //修改
    $("#ctl00_ContentPlaceHolder1_btn_edit").click(function () {
        var msg = "";
        //生产部门
        if ($("#ctl00_ContentPlaceHolder1_drp_unit1").val() == "-1") {
            msg += "请选择要修改的生产部门！<br/>";
        }
        //人员
        if ($("#ctl00_ContentPlaceHolder1_drp_mem1").val() == "-1") {
            msg += "请选择人员！<br/>";
        }
        //年份
        if ($("#ctl00_ContentPlaceHolder1_drp_allotyear1").val() == "-1") {
            msg += "请选择年份！<br/>";
        }
        //产值额度
        if ($("#ctl00_ContentPlaceHolder1_txt_allot1").val() == "") {
            msg += "请填写所补产值！<br/>";
        }
        else {
            var reg = /^-?\d+\.?\d{0,2}$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_allot1").val())) {
                msg += "所补产值格式不正确！</br>";
            }
        }

        if (parseFloat($("#ctl00_ContentPlaceHolder1_hid_totalcount").val()) < parseFloat($("#ctl00_ContentPlaceHolder1_txt_allot1").val())) {
            msg += "所补产值不能大于分配补贴总额！<br>";
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }


    });

    //选择信息
    $(".cls_select").live("click", function () {
        Clear();

        $("#ctl00_ContentPlaceHolder1_hid_id").val($(this).parent().parent().find("TD").eq(2).text());

        $("#ctl00_ContentPlaceHolder1_drp_unit1").val($(this).parent().parent().find("TD").eq(3).text());
        $("#ctl00_ContentPlaceHolder1_hid_unitid").val($(this).parent().parent().find("TD").eq(3).text());
        $("#ctl00_ContentPlaceHolder1_drp_unit1").get(0).disabled = true;
        $("#ctl00_ContentPlaceHolder1_hid_memid").val($(this).parent().parent().find("TD").eq(4).text());
        var memid = $(this).parent().parent().find("TD").eq(4).text();
        var memname = $(this).parent().parent().find("TD").eq(6).text();
        $("#ctl00_ContentPlaceHolder1_drp_mem1").append("<option value=\"" + memid + "\" >" + memname + "</option>");
        $("#ctl00_ContentPlaceHolder1_drp_mem1").val(memid);
        $("#ctl00_ContentPlaceHolder1_drp_mem1").get(0).disabled = true;
        $("#ctl00_ContentPlaceHolder1_drp_allotyear1").val($.trim($(this).parent().parent().find("TD").eq(7).text()));
        $("#ctl00_ContentPlaceHolder1_hid_allotyear").val($.trim($(this).parent().parent().find("TD").eq(7).text()));
        $("#ctl00_ContentPlaceHolder1_drp_allotyear1").get(0).disabled = true;
        $("#ctl00_ContentPlaceHolder1_txt_allot1").val($(this).parent().parent().find("TD").eq(8).text());

        //得到分配总产值
        GetAllotValue("1");
    });
    //删除是判断有无记录选中
    $("#btn_DelCst").click(function () {
        if ($(".cls_chk :checkbox[checked]").length == 0) {
            jAlert("请选择要删除的所补产值！", "提示");
            return false;
        }
        //判断是否要删除
        return confirm("是否要删除此条所补产值？");
    });
    //全选
    $("#chk_All").click(function () {
        if ($(this).get(0).checked) {
            $(":checkbox").attr("checked", "true");
        }
        else {
            $(":checkbox").attr("checked", $(this).get(0).checked);
        }
    });
    //判断是否存在登录名
    $("#txt_memLogin").blur(function () {
        $.ajax({
            type: "POST",
            url: "../HttpHandler/LoginHandler.ashx?action=exsitlogin",
            data: "memlogin=" + escape($(this).val()),
            success: function (rlt) {
                if (rlt == "yes") {
                    $("#hid_exsit").val("1");
                    jAlert("用户名已存在！", "提示");
                    $("#txt_memLogin").addClass("cls_input_red");
                }
                else {
                    $("#hid_exsit").val("");
                    $("#txt_memLogin").removeClass("cls_input_red");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#hid_exsit").val("1");
            }
        });
    });
    //选择部门
    $("#ctl00_ContentPlaceHolder1_drp_unit0").change(function () {

        //得到部门的分配补贴总额
        GetAllotValue("0");
        // GetMember($(this).children("option:selected").val());
    });
    //选择部门
    $("#ctl00_ContentPlaceHolder1_drp_unit0_1").change(function () {

        GetMember($(this).children("option:selected").val());
    });
    //选择年份
    $("#ctl00_ContentPlaceHolder1_drp_allotyear0").change(function () {

        //得到部门的分配补贴总额
        GetAllotValue("0");
    });

    $("#btn_close1").click(function () {
        $("#div_add").hide("slow");
        $(this).hide();
    });

    $("#btn_close2").click(function () {
        $("#div_edit").hide("slow");
        $(this).hide();
    });

});
function GetAllotValue(param) {
    if (param == "1") {

        $.post("/HttpHandler/DrpMemberHandler.ashx", { "Action": "5", "unitID": $("#ctl00_ContentPlaceHolder1_drp_unit1").val(), "unitName": $("#ctl00_ContentPlaceHolder1_drp_unit1").children("option:selected").text(), "allYear": $("#ctl00_ContentPlaceHolder1_drp_allotyear1").val(), "n": Math.random() }, function (jsonResult) {
            var yue = (parseFloat($("#ctl00_ContentPlaceHolder1_txt_allot1").val()) + parseFloat(jsonResult));
            $("#totalhtml1").text(parseFloat(jsonResult).toFixed(2));
            $("#ctl00_ContentPlaceHolder1_hid_totalcount").val(parseFloat(jsonResult).toFixed(2));
        });
    }
    else if (param == "0") {
        $.post("/HttpHandler/DrpMemberHandler.ashx", { "Action": "5", "unitID": $("#ctl00_ContentPlaceHolder1_drp_unit0").val(), "unitName": $("#ctl00_ContentPlaceHolder1_drp_unit0").children("option:selected").text(), "allYear": $("#ctl00_ContentPlaceHolder1_drp_allotyear0").val(), "n": Math.random() }, function (jsonResult) {
            $("#totalhtml").text(parseFloat(jsonResult).toFixed(2));
            $("#ctl00_ContentPlaceHolder1_hid_totalcount").val(parseFloat(jsonResult).toFixed(2));
        });
    }
    else {
        $("#totalhtml").text("0.00");
        $("#ctl00_ContentPlaceHolder1_hid_totalcount").val("0.00");
        $("#totalhtml1").text("0.00");
        $("#ctl00_ContentPlaceHolder1_hid_totalcount").val("0.00");
    }

}
function Clear() {
    $(":text", $("#div_add")).val("");
    $("#totalhtml").text("0");
    $("#totalhtml1").text("0");
    $("#ctl00_ContentPlaceHolder1_hid_totalcount").val("0");
    $("#ctl00_ContentPlaceHolder1_drp_unit0").val($("#ctl00_ContentPlaceHolder1_hid_UserUnitNo").val());

}
//根据部门，得到人员
function GetMember(ddl) {

    var unitID = ddl;
    if (unitID != "-1" && unitID != "") {
        $.post("/HttpHandler/DrpMemberHandler.ashx", { "Action": "1", "unitID": unitID, "PreviewPattern": $("#ctl00_ContentPlaceHolder1_hid_PreviewPattern").val(), "UserSysNo": $("#ctl00_ContentPlaceHolder1_hid_UserSysNo").val() }, function (jsonResult) {
            if (jsonResult == "0") {
                alert("人员取得失败！");
            } else {
                var memberArr = eval('(' + jsonResult + ')');
                $("#drp_mem0").children("option:gt(0)").remove();
                var optiosnString = "";
                $.each(memberArr, function (index, item) {
                    optiosnString += "<option value=\"" + item.mem_ID + "\" >" + item.mem_Name + "</option>";
                });
                $("#drp_mem0").append(optiosnString);
            }
        });
    }
}
//获取人员id
function FillHidden(ddl) {
    $("#ctl00_ContentPlaceHolder1_drp_memid").val($(ddl).children("option:selected").val());

}