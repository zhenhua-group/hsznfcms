﻿$(function () {
    //判断当前部门
    var curunitid = $("#ctl00_ContentPlaceHolder1_userUnitNum").val();
    //经济所
    if (curunitid == '248') {
        $("#ctl00_ContentPlaceHolder1_chk_ISTrunEconomy").hide().parent().parent().hide();
    }
    else if (curunitid == '247') {//暖通所
        $("#ctl00_ContentPlaceHolder1_chk_ISHvac").hide().parent().parent().hide();
    }
    else {//土建所
        $("#ctl00_ContentPlaceHolder1_chk_ISArch").hide().parent().parent().hide();
    }

    //隐藏查询条件
    $("#tbl_id2").children("tbody").children("tr").hide();

    //字段导出记录
    var columnslist = new Array();
    var columnsChinaNameList = new Array();
    //点击选择条件
    $("input[name=bbs]:checkbox", $("#tbl_id")).click(function () {
        //条件名称
        var commName = $.trim($(this).get(0).id);
        //获取数据库字段名      
        var columnsname = $.trim($(this).parent().parent().siblings("span").attr("rel"));
        //获取表头字段名
        var columnsChinaName = $.trim($(this).parent().parent().siblings("span").text());
        if ($(this).is(":checked")) {
            $("#tbl_id2 tr[for=" + commName + "]").show();
            //判断执行设总和甲方负责人
            if (commName == "proFuze" || commName == "FParty") {
                $("#tbl_id2 tr[for=" + commName + "]").next().show();
            }
            //添加到数组里
            columnslist.push(columnsname);
            columnsChinaNameList.push(columnsChinaName);

            //显示字段
           // $("#jqGrid").showCol(columnsname).trigger('reloadGrid');
        }
        else {
            //判断执行设总和甲方负责人
            if (commName == "proFuze" || commName == "FParty") {
                $("#tbl_id2 tr[for=" + commName + "]").next().hide();
            }
            //清空
            $("#tbl_id2 tr[for=" + commName + "]").hide();
            $(":text", "#tbl_id2 tr[for=" + commName + "]").val("");
            $("select", "#tbl_id2 tr[for=" + commName + "]").val("-1");


            if (commName == "StructType") {
                $("#ctl00_ContentPlaceHolder1_asTreeviewStruct_divDropdownTreeText").text("");
            }
            if (commName == "BuildStructType") {
                var asTreeviewStructType = $("#ctl00_ContentPlaceHolder1_asTreeviewStructType_divDropdownTreeText").text("");
            }

            if (commName == "ddcpr_Stage"||commName=="cpr_join") {
               
                $('input[type=checkbox]', "#tbl_id2 tr[for=" + commName + "]").each(function () {  
                    $(this).parent().attr("class", "");
                });
            }


            //


            //隐藏字段
            if (columnsname != "pro_name" && columnsname != "PMUserName" && columnsname != "pro_status" && columnsname != "Unit" && columnsname != "Cpr_Acount" && columnsname != "qdrq" && columnsname != "wcrq") {
              //  $("#jqGrid").hideCol(columnsname).trigger('reloadGrid');

            }
            //删除数组
            columnslist.splice($.inArray(columnsname, columnslist), 1);
            columnsChinaNameList.splice($.inArray(columnsChinaName, columnsChinaNameList), 1);
        }

        //有条件显示查询按钮
        if ($("input[name=bbs]:checkbox:checked", $("#tbl_id")).length > 0) {
            $("#tbl_id2 tr:last").show();
        }
        else {
            $("#tbl_id2 tr:last").hide();
            $(":text").val("");
        }

    });

    //导出
    $("#btn_export").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var txt_proName = $("#txt_proName").val(); //项目名称
        var txt_buildUnit = $("#txt_buildUnit").val(); //建设单位
        var txtaddress = $("#txtaddress").val(); //建设地点
        var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text(); //承接部门
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType").find("option:selected").text(); //行业性质
        var ddsource = $("#ctl00_ContentPlaceHolder1_ddsource").val(); //工程来源    
        var txt_PMName = $("#txt_PMName").val(); //执行设总
        var txt_PMPhone = $("#txt_PMPhone").val(); //电话
        var txt_Aperson = $("#txt_Aperson").val(); //甲方负责人
        var txt_phone = $("#txt_phone").val(); //电话
        var txtproAcount = $("#txtproAcount").val(); //合同额
        var txtproAcount2 = $("#txtproAcount2").val();
        var txt_buildArea = $("#txt_buildArea").val(); //建筑规模
        var txt_buildArea2 = $("#txt_buildArea2").val();
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate").val(); //开始时间
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate2").val();
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val(); //完成时间
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate2").val();
        var ddrank = $("#ctl00_ContentPlaceHolder1_ddrank").find("option:selected").text(); //建筑类别
        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        //管理级别
        var managerLevelDropDownList = $("#ctl00_ContentPlaceHolder1_managerLevelDropDownList").val();
        //设计阶段
        var str_jd = "";
        $('input[type="checkbox"][rel="sjjd"]:checked').each(function () {
            str_jd = str_jd + $(this).parent().parent().next("label").first().text() + ",";
        });
        //参与部门
        var cybm = "";
        if ($("#ctl00_ContentPlaceHolder1_chk_ISTrunEconomy").is(":checked")) {
            cybm = "1,";
        }
        else { cybm = "0,"; }
        if ($("#ctl00_ContentPlaceHolder1_chk_ISHvac").is(":checked")) {
            cybm = cybm + "1,";
        }
        else { cybm = cybm + "0,"; }
        if ($("#ctl00_ContentPlaceHolder1_chk_ISArch").is(":checked")) {
            cybm = cybm + "1";
        }
        else {
            cybm = cybm + "0";
        }
        //结构形式
        var asTreeviewStruct = $("#ctl00_ContentPlaceHolder1_asTreeviewStruct_divDropdownTreeText").text();
        //建筑分类
        var asTreeviewStructType = $("#ctl00_ContentPlaceHolder1_asTreeviewStructType_divDropdownTreeText").text();
        //导出显示的字段
        var str_columnsname = "";
        var str_columnschinaname = "";
        if (columnslist != null && columnslist.length > 0) {
            for (var i = 0; i < columnslist.length; i++) {
                var arr = $.trim(columnslist[i]);
                var arr_china = $.trim(columnsChinaNameList[i]);
                if (arr != "pro_name" && arr != "PMUserName" && arr != "pro_status" && arr != "Unit" && arr != "Cpr_Acount" && arr != "qdrq" && arr != "wcrq") {
                    str_columnsname = str_columnsname + arr + ",";
                    str_columnschinaname = str_columnschinaname + arr_china + ",";
                }
            }
        }
        if (str_columnsname != "") {
            str_columnsname = str_columnsname.substring(str_columnsname, (str_columnsname.length - 1));
        }
        if (str_columnschinaname != "") {
            str_columnschinaname = str_columnschinaname.substring(str_columnschinaname, (str_columnschinaname.length - 1));
        }
        var params = {
            "strwhere": strwhere,
            "txt_proName": txt_proName,
            "txt_buildUnit": txt_buildUnit,
            "txtaddress": txtaddress,
            "drp_unit": drp_unit,
            "ddType": ddType,
            "ddsource": ddsource,
            "txt_Aperson": txt_Aperson,
            "txt_phone": txt_phone,
            "txt_PMName": txt_PMName,
            "txt_PMPhone": txt_PMPhone,
            "txtproAcount": txtproAcount,
            "txtproAcount2": txtproAcount2,
            "txt_buildArea": txt_buildArea,
            "txt_buildArea2": txt_buildArea2,
            "txt_signdate": txt_signdate,
            "txt_signdate2": txt_signdate2,
            "txt_finishdate": txt_finishdate,
            "txt_finishdate2": txt_finishdate2,
            "ddrank": ddrank,
            "managerLevelDropDownList": managerLevelDropDownList,
            "str_jd": str_jd,
            "cybm": cybm,
            "asTreeviewStruct": asTreeviewStruct,
            "asTreeviewStructType": asTreeviewStructType,
            "startTime": startTime,//录入时间
            "endTime": endTime,
            "str_columnsname": str_columnsname,
            "str_columnschinaname": str_columnschinaname
        };

        window.location.href = "Export_SearchProject.aspx?strwhere=" + strwhere + "&txt_proName=" + txt_proName + "&txt_buildUnit=" + txt_buildUnit + "&txtaddress=" + txtaddress + "&drp_unit=" + drp_unit + "&ddType=" + ddType + "&ddsource=" + ddsource + "&txt_Aperson=" + txt_Aperson + "&txt_phone=" + txt_phone + "&txt_PMName=" + txt_PMName + "&txt_PMPhone=" + txt_PMPhone + "&txtproAcount=" + txtproAcount + "&txtproAcount2=" + txtproAcount2 + "&txt_buildArea=" + txt_buildArea + "&txt_buildArea2=" + txt_buildArea2 + "&txt_signdate=" + txt_signdate + "&txt_signdate2=" + txt_signdate2 + "&txt_finishdate=" + txt_finishdate + "&txt_finishdate2=" + txt_finishdate2 + "&ddrank=" + ddrank + "&managerLevelDropDownList=" + managerLevelDropDownList + "&str_jd=" + str_jd + "&cybm=" + cybm + "&asTreeviewStruct=" + asTreeviewStruct + "&asTreeviewStructType=" + asTreeviewStructType + "&str_columnsname=" + str_columnsname + "&str_columnschinaname=" + str_columnschinaname+"&startTime=" + startTime + "&endTime="+endTime+"";
    });


});


////比较时间
//function duibi(a, b) {
//    var arr = a.split("-");
//    var starttime = new Date(arr[0], arr[1], arr[2]);
//    var starttimes = starttime.getTime();

//    var arrs = b.split("-");
//    var lktime = new Date(arrs[0], arrs[1], arrs[2]);
//    var lktimes = lktime.getTime();

//    if (starttimes > lktimes) {
//        return false;
//    }
//    else
//        return true;

//}

//无数据提示
function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}
//转换为短日期格式
//function GetShortDate(dateString) {
//    if (dateString != "" && dateString != null) {
//        var index = 10;
//        if (dateString.lastIndexOf("-") > 0) {
//            index = dateString.lastIndexOf("-") + 2;
//        } else if (dateString.lastIndexOf("/") > 0) {
//            index = dateString.lastIndexOf("/") + 2;
//        }
//        return dateString.substr(0, index);
//    } else {
//        return "无";
//    }
//}
//表格样式-各行变色-鼠标覆盖事件
//function ControlTableCss(tableId) {
//    $("#" + tableId + " tr:gt(0):odd").attr("class", "trOddColor");
//    $("#" + tableId + " tr:gt(0)").hover(function () {
//        $(this).addClass("mouseOverColor");
//    }, function () {
//        $(this).removeClass("mouseOverColor");
//    });
//}

