﻿var jffzr_strWhere_Serch = "";
var projectNameRepeat = false;
$(document).ready(function () {
    var regOne = /[\\/:\*\?\""<>|]/;
    var regTwo = /^[^.]/;
    //检查项目名称是否重复 
    $("#ctl00_ContentPlaceHolder1_txt_name").blur(function () {
        var projectName = $.trim($(this).val());
        if (regOne.test(projectName)) {
            alert("项目名称不能包括以下任何字符！\/:\*\?\"<>|");
            projectNameRepeat = true;
            return false;
        }
        if (!regTwo.test(projectName)) {
            alert("项目名称不能为空!");
            projectNameRepeat = true;
            return false;
        }
        if (projectName.length > 0) {
            $.post("/HttpHandler/ProjectMgr/ExistsInTGProjectHandler.ashx", { "projectname": projectName,"sysno":"0" }, function (result) {
                if (result == "1") {
                    projectNameRepeat = true;
                    alert("项目名称已存在，请重新输入新项目名称！");
                } else {
                    projectNameRepeat = false;
                }
            });
        }
    });
    //绑定权限
    showDivDialogClass.UserRolePower = {
        "previewPower": $("#ctl00_ContentPlaceHolder1_previewPower").val(),
        "userSysNum": $("#ctl00_ContentPlaceHolder1_userSysNum").val(),
        "userUnitNum": $("#ctl00_ContentPlaceHolder1_userUnitNum").val(),
        "notShowUnitList": ""
    };
    //管理级别(院)
    $("#ctl00_ContentPlaceHolder1_radio_yuan").click(function () {

        $("#ctl00_ContentPlaceHolder1_audit_yuan").attr("checked", "checked");
        $("#ctl00_ContentPlaceHolder1_audit_suo").attr("checked", false).hide().parent().parent().parent().parent().hide();
    });
    //管理级别(所)
    $("#ctl00_ContentPlaceHolder1_radio_suo").click(function () {

        $("#ctl00_ContentPlaceHolder1_audit_yuan").attr("checked", false);
        $("#ctl00_ContentPlaceHolder1_audit_suo").attr("checked", "checked").show().parent().parent().parent().parent().show();
    });
    //项目ID
    $("#ctl00_ContentPlaceHolder1_hid_projid").val(hid_projid);
    //保存
    $("#btn_Save").click(function () {
        //项目名称
        var name = $("#ctl00_ContentPlaceHolder1_txt_name").val();

        //建筑类别
        var structtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype").val();
        //项目来源
        var source = $("#ctl00_ContentPlaceHolder1_ddsource").val();
        //合同额
        var acount = $("#ctl00_ContentPlaceHolder1_txtproAcount").val();
        //建设单位
        var txtbuildUnit = $("#ctl00_ContentPlaceHolder1_txtbuildUnit").val();
        //承接部门
        var txt_unit = $("#ctl00_ContentPlaceHolder1_txt_unit").val();
        //建设地点
        var txtbuildAddress = $("#ctl00_ContentPlaceHolder1_txtbuildAddress").val();
        //建设规模
        var txt_scale = $("#ctl00_ContentPlaceHolder1_txt_scale").val();
        //行业性质
        var professionType = $("#ctl00_ContentPlaceHolder1_ddProfessionType").val();

        //甲方负责人
        var txt_Aperson = $("#ctl00_ContentPlaceHolder1_txt_Aperson").val();
        //甲方负责人电话
        var txt_phone = $("#ctl00_ContentPlaceHolder1_txt_phone").val();
        //开始时间
        var txt_startdate = $("#ctl00_ContentPlaceHolder1_txt_startdate").val();
        //结束时间
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val();
        //项目特征概况
        var txt_sub = $("#ctl00_ContentPlaceHolder1_txt_sub").val();
        //项目备注
        var txt_remark = $("#ctl00_ContentPlaceHolder1_txt_remark").val();
        //数字验证正则
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

        var msg = "";
        if (name == "") {
            msg += "请输入项目名称！</br>";
        }
        //管理级别
        if ($("#ctl00_ContentPlaceHolder1_radio_yuan").get(0).checked == false && $("#ctl00_ContentPlaceHolder1_radio_suo").get(0).checked == false) {
            msg += "请选择项目的管理级别！</br>";
        }

        //审核级别
        //        if ($(":checkbox[class=shen][checked=checked]").length == 0) {
        //            msg += "请至少选择一项审核级别！</br>";
        //        }
        if ($("input[type='checkbox'][class=shen]:checked").length == 0) {
            msg += "请至少选择一项审核级别！</br>";
        }

        //建设单位
        if (txtbuildUnit == "") {
            msg += "请输入建设单位！</br>";
        } //建设地点
        if (txtbuildAddress == "") {
            msg += "请输入建设地点！</br>";
        } //建设规模
        if (txt_scale == "") {
            msg += "请输入建设规模！</br>";
        }
        else {
            if (!reg_math.test(txt_scale)) {
                msg += "建设规模请输入数字！</br>";
            }
        }
        //建筑类别
        if (structtype == "-1") {
            msg += "请选择建筑类别！</br>";
        }

        //承接部门
        if (txt_unit == "") {
            msg += "请选择承接部门！</br>";
        }


        //结构形式

        if (!IsStructCheckNode('struct')) {
            msg += "请选择结构形式(结构样式太长或未选择)！</br>";
        }
        //建筑分类
        if (!IsStructCheckNode('structtype')) {
            msg += "请选择建筑分类(建筑分类太长或未选择)！</br>";
        }

        //设计阶段
        //        if ($("input[class=duan]> :checkbox[checked=checked]").length == 0) {
        //            msg += "请至少选择一项设计阶段！</br>";
        //        }
        if ($("input[type='checkbox'][class=duan]:checked").length == 0) {
            msg += "请至少选择一项设计阶段！</br>";
        }
        //         if ($("input[type='checkbox'][class=duan]:checked").length == 0) {
        //            msg += "请至少选择一项设计阶段！</br>";
        //        }
        //项目来源
        if (source == "-1") {
            msg += "请选择项目来源！</br>";
        }
        //合同额
        if (acount != "") {
            if (!reg_math.test(acount)) {
                msg += "合同额请输入数字！</br>";
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txtproAcount").val("0");
        }

        //行业性质
        if (professionType == "0") {
            msg += "请选择行业性质！</br>";
        }

        //甲方负责人
        if (txt_Aperson == "") {
            msg += "请输入甲方负责人！</br>";
        }
        //电话
        if (txt_phone == "") {
            msg += "请输入甲方负责人电话！</br>";
        }
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_PMName").val()).length == 0) {
            msg += "执行设总不能为空！<br/>";
        }

        //项目开始日期
        if (txt_startdate == "") {
            msg += "项目开始日期不能为空！</br>";
        }
        //项目结束日期
        if (txt_finishdate == "") {
            msg += "项目完成日期不能为空！</br>";
        }

        //比较时间
        if (txt_startdate != "" && txt_finishdate != "") {

            var result = duibi(txt_startdate, txt_finishdate);
            if (result == false) {
                msg += "项目开始时间大于结束时间！";
            }
        }

        if (projectNameRepeat) {
            msg += "项目名称不符合填写规范,不能创建项目！<br/>";
        }
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_sub").val()).length == 0) {
            msg += "项目特征概况不能为空！<br/>";
        }
        else if ($("#ctl00_ContentPlaceHolder1_txt_sub").val().length > 500) {
            msg += "项目特征概况不能超过500字！<br/>";
        }
        //提示

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        else {
            $(this).hide();
        }
    });
    $("#btn_SaveApply").click(function () {
        //绑定isapply的值为1
        $("#ctl00_ContentPlaceHolder1_hidIsAppay").val("1");
        //项目名称
        var name = $("#ctl00_ContentPlaceHolder1_txt_name").val();

        //建筑类别
        var structtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype").val();
        //项目来源
        var source = $("#ctl00_ContentPlaceHolder1_ddsource").val();
        //合同额
        var acount = $("#ctl00_ContentPlaceHolder1_txtproAcount").val();
        //建设单位
        var txtbuildUnit = $("#ctl00_ContentPlaceHolder1_txtbuildUnit").val();
        //承接部门
        var txt_unit = $("#ctl00_ContentPlaceHolder1_txt_unit").val();
        //建设地点
        var txtbuildAddress = $("#ctl00_ContentPlaceHolder1_txtbuildAddress").val();
        //建设规模
        var txt_scale = $("#ctl00_ContentPlaceHolder1_txt_scale").val();
        //行业性质
        var professionType = $("#ctl00_ContentPlaceHolder1_ddProfessionType").val();

        //甲方负责人
        var txt_Aperson = $("#ctl00_ContentPlaceHolder1_txt_Aperson").val();
        //甲方负责人电话
        var txt_phone = $("#ctl00_ContentPlaceHolder1_txt_phone").val();
        //开始时间
        var txt_startdate = $("#ctl00_ContentPlaceHolder1_txt_startdate").val();
        //结束时间
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val();
        //项目特征概况
        var txt_sub = $("#ctl00_ContentPlaceHolder1_txt_sub").val();
        //项目备注
        var txt_remark = $("#ctl00_ContentPlaceHolder1_txt_remark").val();
        //数字验证正则
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

        var msg = "";
        if (name == "") {
            msg += "请输入项目名称！</br>";
        }
        //管理级别
        if ($("#ctl00_ContentPlaceHolder1_radio_yuan").get(0).checked == false && $("#ctl00_ContentPlaceHolder1_radio_suo").get(0).checked == false) {
            msg += "请选择项目的管理级别！</br>";
        }

        //审核级别
        //        if ($(":checkbox[class=shen][checked=checked]").length == 0) {
        //            msg += "请至少选择一项审核级别！</br>";
        //        }
        if ($("input[type='checkbox'][class=shen]:checked").length == 0) {
            msg += "请至少选择一项审核级别！</br>";
        }

        //建设单位
        if (txtbuildUnit == "") {
            msg += "请输入建设单位！</br>";
        } //建设地点
        if (txtbuildAddress == "") {
            msg += "请输入建设地点！</br>";
        } //建设规模
        if (txt_scale == "") {
            msg += "请输入建设规模！</br>";
        }
        else {
            if (!reg_math.test(txt_scale)) {
                msg += "建设规模请输入数字！</br>";
            }
        }
        //建筑类别
        if (structtype == "-1") {
            msg += "请选择建筑类别！</br>";
        }

        //承接部门
        if (txt_unit == "") {
            msg += "请选择承接部门！</br>";
        }


        //结构形式

        if (!IsStructCheckNode('struct')) {
            msg += "请选择结构形式(结构样式太长或未选择)！</br>";
        }
        //建筑分类
        if (!IsStructCheckNode('structtype')) {
            msg += "请选择建筑分类(建筑分类太长或未选择)！</br>";
        }

        //设计阶段
        //        if ($("input[class=duan]> :checkbox[checked=checked]").length == 0) {
        //            msg += "请至少选择一项设计阶段！</br>";
        //        }
        if ($("input[type='checkbox'][class=duan]:checked").length == 0) {
            msg += "请至少选择一项设计阶段！</br>";
        }
        //         if ($("input[type='checkbox'][class=duan]:checked").length == 0) {
        //            msg += "请至少选择一项设计阶段！</br>";
        //        }
        //项目来源
        if (source == "-1") {
            msg += "请选择项目来源！</br>";
        }
        //合同额
        if (acount != "") {
            if (!reg_math.test(acount)) {
                msg += "合同额请输入数字！</br>";
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txtproAcount").val("0");
        }

        //行业性质
        if (professionType == "0") {
            msg += "请选择行业性质！</br>";
        }

        //甲方负责人
        if (txt_Aperson == "") {
            msg += "请输入甲方负责人！</br>";
        }
        //电话
        if (txt_phone == "") {
            msg += "请输入甲方负责人电话！</br>";
        }
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_PMName").val()).length == 0) {
            msg += "执行设总不能为空！<br/>";
        }

        //项目开始日期
        if (txt_startdate == "") {
            msg += "项目开始日期不能为空！</br>";
        }
        //项目结束日期
        if (txt_finishdate == "") {
            msg += "项目完成日期不能为空！</br>";
        }

        //比较时间
        if (txt_startdate != "" && txt_finishdate != "") {

            var result = duibi(txt_startdate, txt_finishdate);
            if (result == false) {
                msg += "项目开始时间大于结束时间！";
            }
        }

        if (projectNameRepeat) {
            msg += "项目名称不符合填写规范,不能创建项目！<br/>";
        }
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_sub").val()).length == 0) {
            msg += "项目特征概况不能为空！<br/>";
        }
        else if ($("#ctl00_ContentPlaceHolder1_txt_sub").val().length > 500) {
            msg += "项目特征概况不能超过500字！<br/>";
        }
        //提示

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        else {
            $(this).hide();
        }
    });
    //合同关联点击
    $("#sch_reletive").click(function () {
        $("#TxtCprName").val("");
        //先赋值
        showDivDialogClass.SetParameters({
            "nowIndex": "reletiveCpr_nowPageIndex", //当前页id
            "pageSize": "10"
        });
        //绑定生产部门数据
        GetReleCprUnit();
        //绑定项目数据
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "1", "reletiveCpr", ReletiveCprCallBack);
        BindAllDataCountReleCpr(); //绑定总数据
    });
    //合同关联-搜索
    $("#btn_SearchByCprName").click(function () {

        var strNameUnit = "";
        var cprName = $("#TxtCprName").val(); //当做where语句
        cprName = showDivDialogClass.ReplaceChars(cprName);
        strNameUnit += cprName + "|";
        var unitID = $("#select_releCprUnit option:selected").val();
        var unitName = $("#select_releCprUnit option:selected").text();

        if (unitID > 0) {
            strNameUnit += unitID + "|";
            strNameUnit += unitName;
        }
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", strNameUnit, "true", "1", "reletiveCpr", ReletiveCprCallBack);
        BindAllDataCountReleCpr(); //绑定总数据
    });
    //承接部门
    $("#sch_unit").click(function () {
        showDivDialogClass.SetParameters({
            "nowIndex": "cjbm_nowPageIndex", //当前页id
            "pageSize": "10"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "proCjbm", ProCjbmCallBack);
        BindAllDataCount(); //绑定总数据
    });
    //甲方负责
    $("#sch_Aperson").click(function () {
        $("#txtName").val("");
        $("#txtPhone").val("");
        $("#txtCompName").val("");
        jffzr_strWhere_Serch = "";
        $("#jffzr_table tr:gt(0)").remove();
        //先赋值
        showDivDialogClass.SetParameters({
            "pageSize": "5"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "1", "jffzrCpr", JffzrCallBack);
        BindAllDataCountJffzr(); //绑定总数据
    });
    //甲方搜索
    $("#btn_serch").click(function () {
        var str_name = $("#txtName").val();
        var str_phone = $("#txtPhone").val();
        var str_cpyName = $("#txtCompName").val();
        var strWhere = "";
        strWhere += "&Name=" + str_name + "&Phone=" + str_phone + "&Department=" + str_cpyName;
        jffzr_strWhere_Serch = strWhere;
        jffzr_strWhere_Serch = showDivDialogClass.ReplaceChars(jffzr_strWhere_Serch);
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", jffzr_strWhere_Serch, "true", "1", "jffzrCpr", JffzrCallBack);
        BindAllDataCountJffzr(); //绑定总数据
    });
    //执行设总
    var chooseUser = new ChooseUserControl($("#chooseUserMain"), PMCallBack);
    //判断当前部门
    var curunitid = $("#ctl00_ContentPlaceHolder1_userUnitNum").val();
    //经济所
    if (curunitid == '248') {
        $("#ctl00_ContentPlaceHolder1_chk_ISTrunEconomy").parent().parent().hide();
    }
    else if (curunitid == '247') {//暖通所
        $("#ctl00_ContentPlaceHolder1_chk_ISHvac").parent().parent().hide();
    }
    else {//土建所
        $("#ctl00_ContentPlaceHolder1_chk_ISArch").parent().parent().hide();
    }
});
//比较时间
function duibi(a, b) {
    var arr = a.split("-");
    var starttime = new Date(arr[0], arr[1], arr[2]);
    var starttimes = starttime.getTime();

    var arrs = b.split("-");
    var lktime = new Date(arrs[0], arrs[1], arrs[2]);
    var lktimes = lktime.getTime();

    if (starttimes >= lktimes) {
        return false;
    }
    else {
        return true;
    }
}
//设总返回值
function PMCallBack(userObj) {

    $("#ctl00_ContentPlaceHolder1_txt_PMName").val(userObj.username);
    $("#ctl00_ContentPlaceHolder1_hid_pmname").val(userObj.username);
    $("#ctl00_ContentPlaceHolder1_txt_PMPhone").val(userObj.phonenumber);
    $("#ctl00_ContentPlaceHolder1_hid_pmuserid").val(userObj.usersysno);
}
//获取甲方负责人总数据
function BindAllDataCountJffzr() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "jffzr_prevPage",
        "firstPage": "jffzr_firstPage",
        "nextPage": "jffzr_nextPage",
        "lastPage": "jffzr_lastPage",
        "gotoPage": "jffzr_gotoPage",
        "allDataCount": "jffzr_totalCount",
        "nowIndex": "jffzr_nowPageIndex",
        "allPageCount": "jffzr_PagesCount",
        "gotoIndex": "jffzr_gotoPageNum",
        "pageSize": "5"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", jffzr_strWhere_Serch, "true", "jffzrCpr", GetJffzrAllDataCount);

    //注册分页事件
    $("#jffzr_forPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#jffzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", jffzr_strWhere_Serch, "true", pageIndex, "jffzrCpr", JffzrCallBack);
        }
    });
}
//甲方负责人数据总数CallBack函数
function GetJffzrAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#jffzr_table tr:gt(0)").remove();
        $("#jffzr_allDataCount").text(0);
        $("#jffzr_nowPageIndex").text(0);
        $("#jffzr_allPageCount").text(0);
        NoDataMessageOnTable("jffzr_table", 4);
    }
}
//甲方负责人绑定表格数据CallBack
function JffzrCallBack(result) {
    if (result != null) {
        var data = result.ds;
        var jffzrHtml;
        $("#jffzr_table tr:gt(0)").remove();
        $.each(data, function (i, n) {
            var oper = "<span rel='" + n.Cst_Id + "'style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\">选择</span>";
            jffzrHtml = '<tr><td>' + n.Name + '</td><td>' + n.Phone + '</td><td>' + n.Department + '</td><td>' + oper + '</td></tr>';
            $("#jffzr_table").append(jffzrHtml);
            $("#jffzr_table span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txt_Aperson").val($.trim(n.Name));
                $("#ctl00_ContentPlaceHolder1_txt_phone").val($.trim(n.Phone));

            });
        });
        ControlTableCss("jffzr_table");
    }
}
//获得承接部门数据总数
function BindAllDataCount() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "cjbm_prevPage",
        "firstPage": "cjbm_firstPage",
        "nextPage": "cjbm_nextPage",
        "lastPage": "cjbm_lastPage",
        "gotoPage": "cjbm_gotoPageIndex",
        "allDataCount": "cjbm_allDataCount",
        "nowIndex": "cjbm_nowPageIndex",
        "allPageCount": "cjbm_allPageCount",
        "gotoIndex": "cjbm_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", "", "false", "proCjbm", GetCjbmAllDataCount);
    //alert("分页前,当前页数: "+$("#"+ParametersDict.nowIndex+"").text());
    //注册事件,先注销,再注册
    $("#cjbmByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#cjbm_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", pageIndex, "proCjbm", ProCjbmCallBack);
            //showDivDialogClass.BindPageValue($(this).attr("id"));
        }
    });
}
function ProCjbmCallBack(result) {
    if (result != null) {
        var data = result.ds;
        $("#cpr_cjbmTable tr:gt(0)").remove();
        $.each(data, function (i, n) {
            var oper = "<span rel='" + n.unit_ID + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\">选择</span>";
            var trHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.unit_Name + "</td><td>" + oper + "</td></tr>";
            $("#cpr_cjbmTable").append(trHtml);
            $("#cpr_cjbmTable span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txt_unit").val($.trim(n.unit_Name));
                $("#ctl00_ContentPlaceHolder1_hid_unit").val($.trim(n.unit_Name));
            });
        });
        ControlTableCss("cpr_cjbmTable");
    }
}
//承接部门数据总数CallBack函数
function GetCjbmAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        NoDataMessageOnTable("pro_cjbmTable", 3);
    }
}
//获取生产部门
function GetReleCprUnit() {
    var previewPower = showDivDialogClass.UserRolePower.previewPower;
    var userSysNum = showDivDialogClass.UserRolePower.userSysNum;
    var userUnitNum = showDivDialogClass.UserRolePower.userUnitNum;
    var data = "action=getReleCprUnit&previewPower=" + previewPower + "&userSysNum=" + userSysNum + "&userUnitNum=" + userUnitNum;
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var data = result.ds;
                var optionHtml = '<option value="-1">-----全院部门-----</option>';
                $.each(data, function (i, n) {
                    optionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
                });
                $("#select_releCprUnit").html(optionHtml);

                $("#select_releCprUnit").unbind('change').change(function () {
                    var strNameUnit = "";
                    var cprName = $("#TxtCprName").val(); //当做where语句
                    cprName = cprName.replace("#", "");
                    strNameUnit += cprName + "|";
                    var unitID = $("#select_releCprUnit option:selected").val();
                    var unitName = $("#select_releCprUnit option:selected").text();
                    //                 if(unitID>0){
                    //                    strNameUnit+=unitID;
                    //                 }
                    //                  strNameUnit += "|" + unitName;
                    if (unitID > 0) {
                        strNameUnit += unitID + "|";
                        strNameUnit += unitName;
                    }
                    showDivDialogClass.GetDataByAJAX("getDataToDivDialog", strNameUnit, "true", "1", "reletiveCpr", ReletiveCprCallBack);
                    BindAllDataCountReleCpr(); //绑定总数据
                });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误!");
        }
    });
}
//合同关联绑定表格数据-CallBack
function ReletiveCprCallBack(result) {
    if (result != null) {
        var cprName = $("#TxtCprName").val();
        var data = result.ds;
        var releviteCprHtml;
        $("#cprReletiveTable tr:gt(0)").remove();
        $.each(data, function (i, n) {
            var signdate = GetShortDate(n.cpr_SignDate);
            var oper = "<span rel='" + n.cpr_Id + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\">选择</span>";
            var cprNameResult = n.cpr_Name;
            if (cprNameResult.length > 20) {
                cprNameResult = cprNameResult.substr(0, 20) + "...";
            }
            cprNameResult = cprNameResult.replace(cprName, "<span style='color:red'>" + cprName + "</span>");
            releviteCprHtml = '<tr><td title="' + n.cpr_Name + '">' + cprNameResult + '</td><td align=\"center\">' + signdate + '</td><td align=\"center\">' + oper + '</td></tr>';
            $("#cprReletiveTable").append(releviteCprHtml);
            $("#cprReletiveTable span:last").click(function () {
                //绑定关联值
                BindCprReletive(n);

            });
        });
        ControlTableCss("cprReletiveTable");
    }
}
//获得合同关联数据总数
function BindAllDataCountReleCpr() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "reletiveCpr_prevPage",
        "firstPage": "reletiveCpr_firstPage",
        "nextPage": "reletiveCpr_nextPage",
        "lastPage": "reletiveCpr_lastPage",
        "gotoPage": "reletiveCpr_gotoPage",
        "allDataCount": "reletiveCpr_totalCount",
        "nowIndex": "reletiveCpr_nowPageIndex",
        "allPageCount": "reletiveCpr_PagesCount",
        "gotoIndex": "reletiveCpr_gotoPageNum",
        "pageSize": "10"
    });
    var strNameUnit = $("#TxtCprName").val() + "|";
    var unitID = $("#select_releCprUnit option:selected").val();
    var unitName = $("#select_releCprUnit option:selected").text();
    //        if(unitID>0){
    //        strNameUnit+=unitID;
    //    }
    //     strNameUnit += "|" + unitName;
    if (unitID > 0) {
        strNameUnit += unitID + "|";
        strNameUnit += unitName;
    }
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", strNameUnit, "true", "reletiveCpr", GetReleCprAllDataCount);
    //alert("分页前,当前页数: "+$("#"+ParametersDict.nowIndex+"").text());
    //注册事件,先注销,再注册
    $("#cprReletivePage span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#reletiveCpr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", strNameUnit, "true", pageIndex, "reletiveCpr", ReletiveCprCallBack);
            //showDivDialogClass.BindPageValue($(this).attr("id"));
        }
    });
}
//合同关联数据总数CallBack函数
function GetReleCprAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#cprReletiveTable tr:gt(0)").remove();
        $("#reletiveCpr_totalCount").text(0);
        $("#reletiveCpr_nowPageIndex").text(0);
        $("#reletiveCpr_PagesCount").text(0);
        NoDataMessageOnTable("cprReletiveTable", 3);
    }
}
//绑定页面上的关联值
function BindCprReletive(n) {
    //合同名称
    $("#ctl00_ContentPlaceHolder1_txt_reletive").val(n.cpr_Name);
    //弱关联合同名称
    $("#ctl00_ContentPlaceHolder1_txt_name").val(n.cpr_Name);
    //设计阶段
    var chk_val = $.trim(n.cpr_Process);
    //重置
    $(".cls_jd :checkbox").attr("checked", false);
    if (chk_val.indexOf(',') > -1) {
        var array = chk_val.split(',');
        for (var i = 0; i < array.length; i++) {
            if (array[i] == "27") {
                $("#ctl00_ContentPlaceHolder1_CheckBox1").attr("checked", "checked");
                $("#ctl00_ContentPlaceHolder1_CheckBox1").parent().attr("class", "checked");
            }
            else if (array[i] == "28") {
                $("#ctl00_ContentPlaceHolder1_CheckBox2").attr("checked", "checked");
                $("#ctl00_ContentPlaceHolder1_CheckBox2").parent().attr("class", "checked");
            }
            else if (array[i] == "29") {
                $("#ctl00_ContentPlaceHolder1_CheckBox3").attr("checked", "checked");
                $("#ctl00_ContentPlaceHolder1_CheckBox3").parent().attr("class", "checked");
            }
            else if (array[i] == "30") {
                $("#ctl00_ContentPlaceHolder1_CheckBox4").attr("checked", "checked");
                $("#ctl00_ContentPlaceHolder1_CheckBox4").parent().attr("class", "checked");
            }
        }
    } else if (chk_val != "") {
        if (chk_val == "27") {
            $("#ctl00_ContentPlaceHolder1_CheckBox1").attr("checked", "checked");
            $("#ctl00_ContentPlaceHolder1_CheckBox1").parent().attr("class", "checked");
        }
        else if (chk_val == "28") {
            $("#ctl00_ContentPlaceHolder1_CheckBox2").attr("checked", "checked");
            $("#ctl00_ContentPlaceHolder1_CheckBox2").parent().attr("class", "checked");
        }
        else if (chk_val == "29") {
            $("#ctl00_ContentPlaceHolder1_CheckBox3").attr("checked", "checked");
            $("#ctl00_ContentPlaceHolder1_CheckBox3").parent().attr("class", "checked");
        }
        else if (chk_val == "30") {
            $("#ctl00_ContentPlaceHolder1_CheckBox4").attr("checked", "checked");
            $("#ctl00_ContentPlaceHolder1_CheckBox4").parent().attr("class", "checked");
        }
    }
    //项目来源
    $("#ctl00_ContentPlaceHolder1_ddsource option[name='" + $.trim(n.BuildSrc) + "']").attr("selected", "selected");
    //合同额
    $("#ctl00_ContentPlaceHolder1_txtproAcount").val($.trim(n.cpr_Acount));
    //建设单位
    $("#ctl00_ContentPlaceHolder1_txtbuildUnit").val($.trim(n.BuildUnit));
    //承接部门
    $("#ctl00_ContentPlaceHolder1_txt_unit").val($.trim(n.cpr_Unit));
    $("#ctl00_ContentPlaceHolder1_hid_unit").val($.trim(n.cpr_Unit));
    //建设规模
    $("#ctl00_ContentPlaceHolder1_txt_scale").val($.trim(n.BuildArea));

    //建设地点
    $("#ctl00_ContentPlaceHolder1_txtbuildAddress").val($.trim(n.BuildPosition));
    //项目经理
    $("#ctl00_ContentPlaceHolder1_txt_PMName").val($.trim(n.ChgPeople));
    $("#ctl00_ContentPlaceHolder1_hid_pmname").val($.trim(n.ChgPeople));
    $("#ctl00_ContentPlaceHolder1_hid_pmuserid").val(n.PMUserID); //无此字段
    //项目经理电话
    $("#ctl00_ContentPlaceHolder1_txt_PMPhone").val($.trim(n.ChgPhone));

    //甲方
    $("#ctl00_ContentPlaceHolder1_txt_Aperson").val($.trim(n.ChgJia));
    //电话
    $("#ctl00_ContentPlaceHolder1_txt_phone").val($.trim(n.ChgJiaPhone));
    //合同ID
    $("#ctl00_ContentPlaceHolder1_txtcpr_id").val($.trim(n.cpr_Id));
    //建筑类别
    $("#ctl00_ContentPlaceHolder1_drp_buildtype").val(n.BuildType);
    //行业性质
    $("#ctl00_ContentPlaceHolder1_ddProfessionType").val(n.Industry);
    //结构样式
    CommonControl.BindASTreeView("ctl00_ContentPlaceHolder1_asTreeviewStruct_ulASTreeView", n.StructType, $("#StrructContainer"));
    //建筑分类
    CommonControl.BindASTreeView("ctl00_ContentPlaceHolder1_asTreeviewStructType_ulASTreeView", n.BuildStructType, $("#buildTypeContainer"));

    //触发onBlur事件，检查项目名称是否存在
    $("#ctl00_ContentPlaceHolder1_txt_name").focus();
}
//转换为短日期格式
function GetShortDate(dateString) {

    if (dateString != "" && dateString != null) {
        var index = 10;
        if (dateString.lastIndexOf("-") > 0) {
            index = dateString.lastIndexOf("-") + 3;
        } else if (dateString.lastIndexOf("/") > 0) {
            index = dateString.lastIndexOf("/") + 3;
        }
        return dateString.substr(0, index);
    } else {
        return "无";
    }

}
//表格样式-各行变色-鼠标覆盖事件
function ControlTableCss(tableId) {
    $("#" + tableId + " tr:gt(0):odd").attr("class", "trOddColor");
    $("#" + tableId + " tr:gt(0)").hover(function () {
        $(this).addClass("mouseOverColor");
    }, function () {
        $(this).removeClass("mouseOverColor");
    });
}
//无数据提示
function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}
//加载数据
function GetAttachData() {
    loadCoperationAttach();
}
//加载附件信息
function loadCoperationAttach() {
    var projId = $("#ctl00_ContentPlaceHolder1_hid_projid").val();

    var data = "action=getprojfiles&type=proj&projid=" + projId;

    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            var filedata = result == null ? "" : result.ds;
            if ($("#datas_att tr").length > 1) {
                $("#datas_att tr:gt(0)").remove();
            }
            //缩略图列表
            if ($("#img_container img").length > 1) {
                $("#img_container img:gt(0)").remove();
            }
            $.each(filedata, function (i, n) {
                var row = $("#att_row").clone();
                //显示
                $("#img_small").show();
                //克隆
                var thumd = $("#img_small").clone();
                //隐藏
                $("#img_small").hide();
                var oper = "<span style='color:blue;cursor:pointer;' rel='" + n.ID + "'>删除</span>";
                var oper2 = "<a href='../Attach_User/filedata/projfile/" + n.FileUrl + "' target='_blank'>查看</a>";
                var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                //显示缩略图
                var img_path = n.FileUrl.split('/');
                thumd.attr("src", "../Attach_User/filedata/projfile/" + img_path[0] + "/min/" + img_path[1]);
                thumd.attr("alt", n.ID);

                row.find("#att_id").text(n.ID);
                row.find("#att_filename").html(img + n.FileName);
                row.find("#att_filename").attr("align", "left");
                row.find("#att_filesize").text(n.FileSizeString + 'KB');
                row.find("#att_filetype").text(n.FileType);
                row.find("#att_uptime").text(n.UploadTime);
                row.find("#att_oper").html(oper);
                row.find("#att_oper2").html(oper2);
                row.find("#att_oper span").click(function () {
                    delCprAttach($(this));
                });
                //添加样式
                row.addClass("cls_TableRow");
                row.appendTo("#datas_att");
                //缩略图
                thumd.appendTo("#img_container");
                if (i % 3 == 0) {
                    $("img_container").append("<br/>")
                }
            });
            filedata = "";
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误22！");
        }
    });
}
//删除附件
function delCprAttach(link) {
    if (!confirm("确定删除附件吗？")) {
        return false;
    }
    var data = "action=delprojattach&attid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "text",
        success: function (result) {
            if (result == "yes") {
                //加载附件
                link.parents("tr:first").remove();
                //删除缩略图
                $("#img_container img[alt=" + link.attr("rel") + "]").remove();
                alert("附件删除成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
