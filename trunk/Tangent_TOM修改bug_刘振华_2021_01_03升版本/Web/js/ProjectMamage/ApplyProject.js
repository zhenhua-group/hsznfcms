﻿//接受消息
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
$(document).ready(function () {
    //CommonControl.SetFormWidth();
    //隔行变色
    $("#gv_project tr:odd").css({ background: "White" });
    //进度条
    $.each($("input[id$=HiddenFieldPercent]"), function () {
        var div = $(this).siblings("div:first");
        div.attr("percent", $(this).val());
    });
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("percent"));
        $(item).progressbar({
            value: percent
        });
    });
    ChangedBackgroundColor();
    //显示轨迹
    $(".applying").each(function (i) {
        var flag = $(this).next(":hidden").val();
        if (flag == "1") {
            $(this).show();
            $(this).prevAll("a").hide();
        }
        else if (flag == "2") {
            $(this).show();
            $(this).text("评审结果");
            var auditsysno = $(this).attr("auditsysno");
            $(this).attr("href", "ProjectAudit/ProjectAudit.aspx?projectAuditSysNo=" + auditsysno);
            $(this).prevAll("a").hide();
        }
    });
    //发起审批
    $(".cls_startapp").live("click",function () {
       // var projectsysno = $(this).next(":hidden").val();
        // var projname = $(this).next(":hidden").next(":hidden").val();

        var projectsysno = $(this).attr("proid");
        var projname = $(this).attr("proname");
        var objData = {
            ProjectSysNo: projectsysno,
            ProjectName: projname
        };
        var query = Global.toJSON(objData);
        jsonDataEntity = query;
        startAppControl = $(this);
        //发起审批
        getUserAndUpdateAudit('0', '0', jsonDataEntity);
    });
    //实例化类容
    messageDialog = $("#msgReceiverContainer").messageDialog({
        "button": {

            "发送消息": function () {
                //选中用户
                var _$mesUser = $(":checkbox[name=messageUser]:checked");

                if (_$mesUser.length == 0) {
                    alert("请至少选择一个流程审批人！");
                    return false;
                }

                getUserAndUpdateAudit('0', '1', jsonDataEntity);
            }
            ,
            "关闭": function () {
                messageDialog.hide();
            }
        }
    });
    sendMessageClass = new MessageCommon(messageDialog);

    //无合同显示
    $("#gv_project a[cprid=0]").each(function () {
        $(this).attr("href", "###").text("无合同");
    });

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Project";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();

    //提示
    $(".cls_column").tipsy({ opacity: 1 });
});
//发送消息回调方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        messageDialog.hide();
        //提示消息
        alert("发起项目评审已成功！\n消息已成功发送到评审人等待审批！");
        //改变流程提示状态
        CommonControl.SetApplyStatus(startAppControl);
    } else {
        alert("消息发送失败！");
    }
}

//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //提交地址
    var result = TG.Web.ProjectManage.ApplyProject.StartAppProject(jsonData, flag);
    //提交数据
    if (result == "0") {
        alert("发起项目评审失败，请联系系统管理员！");
    }
    else {
        renderUserOrSendMsg(flag, result.value);
    }
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag == "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
