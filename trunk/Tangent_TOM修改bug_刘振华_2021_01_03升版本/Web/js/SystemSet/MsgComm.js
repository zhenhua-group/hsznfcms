﻿$(document).ready(function () {
    //隔行变色
    $("#GridView1 tr:even").css({ background: "White" });

    CommonControl.SetFormWidth();
    //全选
    $("#chk_all").click(function () {
        if ($(this).get(0).checked) {
            $(":checkbox").attr("checked", "true");
        }
        else {
            $(":checkbox").attr("checked", $(this).get(0).checked);
        }
    });
    //删除
    $("#btn_DelCst").click(function () {
        var icount = $(":checked").length;
        if (icount == 0) {
            alert("请选择要删除的记录");
            return false;
        }

        return confirm("确定要删除消息吗？");
    });
    $("#btn_Ycang").click(function () {
        var icount = $(":checked").length;
        if (icount == 0) {
            alert("请选择要隐藏的记录");
            return false;
        }

        if (confirm("确实要隐藏消息吗？")) {
            return true;
        }
    });
    $("#btn_XShi").click(function () {
        var icount = $(":checked").length;
        if (icount == 0) {
            alert("请选择要显示的记录");
            return false;
        }

        if (confirm("确实要显示消息吗？")) {
            return true;
        }
    });

});