﻿$(document).ready(function () {
    if ($("#hid_type").val() == "1") {
        $("#addAttach").show();
    }
  //  $("#hid_projid").val(hid_projid);

    $(".show_project tr:even").css("background-color", "#ffffff");

    $("#btn_save").click(function () {
        //项目名称
        var name = $("#ctl00_ContentPlaceHolder1_lbl_title").val();
        //项目备注
        var txt_remark = $("#ctl00_ContentPlaceHolder1_lbl_content").val();
        //开始时间
        var txt_startdate = $("#ctl00_ContentPlaceHolder1_lbl_Zztime").val();
        //结束时间
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_lbl_Sptime").val();

        var txt_jb = $("ctl00_ContentPlaceHolder1_#lbl_jb").val();
        var txt_yeartime = $("#ctl00_ContentPlaceHolder1_lbl_yeartime").val();
        var msg = "";

        if (name == "") {
            msg += "请输入资质名称！</br>";
        }
        if (name.length > 50) {
            msg += "资质名称最多50字！</br>";
        }
        //项目开始日期
        if (txt_startdate == "") {
            msg += "资质开始时间不能为空！</br>";
        }
        //项目结束日期
        if (txt_finishdate == "") {
            msg += "资质结束时间不能为空！</br>";
        }
	 //级别
//        if (txt_jb == "") {
//            msg += "资质级别不能为空！</br>";
//        }
        //年审时间
        if (txt_yeartime == "") {
            msg += "年审时间不能为空！</br>";
        }
        //提示
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        } else {
            return true;
        }
    });
    $(".show_project tr:odd").css("background-color", "#F0F0F0");
    $(".cls_content_head tr:odd").css("background-color", "#F0F0F0");
    LoadCoperationAttach();
});

function GetAttachData() {
    LoadCoperationAttach();
}
//加载附件信息
function LoadCoperationAttach() {
    var data_att = "action=getprojfiles&type=qual&projid=" + $("#ctl00_ContentPlaceHolder1_hid_projid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data_att,
        dataType: "json",
        success: function (result) {
            if (result != null) {
                var filedata = result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").remove();
                }
                //缩略图列表
                if ($("#img_container img").length > 1) {
                    $("#img_container img:gt(0)").remove();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    //显示
                    $("#img_small").show();
                    //克隆
                    var thumd = $("#img_small").clone();
                    //隐藏
                    $("#img_small").hide();
                    var oper = "<a href='#' rel='" + n.ID + "'>删除</a>";
                    var oper2 = "<a href=\"/Coperation/DownLoadFile.aspx?fileName=" + n.FileName + "&FileURL=" + n.FileUrl + "\" target='_blank'>下载</a>";
                    var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                    //显示缩略图
                    var img_path = n.FileUrl.split('/');
                    thumd.attr("src", "../Attach_User/filedata/qualfile/" + img_path[0] + "/min/" + img_path[1]);
                    thumd.attr("alt", n.ID);

                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").html(img + n.FileName);
                    row.find("#att_filesize").text(n.FileSizeString);
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper").html(oper);
                    row.find("#att_oper2").html(oper2);
                    row.find("#att_oper a").click(function () {
                        if (confirm("确定要删除本条附件吗？")) {
                            delCprAttach($(this));
                        }
                    });
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas_att");
                    //缩略图
                    thumd.appendTo("#img_container");
                    if (i % 3 == 0) {
                        $("img_container").append("<br/>")
                    }
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("加载附件信息错误！");
        }
    });
}
//删除附件
function delCprAttach(link) {
    var data = "action=delcprattach&attid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "text",
        success: function (result) {
            if (result == "yes") {
                link.parents("tr:first").remove();
                //删除缩略图
                $("#img_container img[alt=" + link.attr("rel") + "]").remove();
                alert("附件删除成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}