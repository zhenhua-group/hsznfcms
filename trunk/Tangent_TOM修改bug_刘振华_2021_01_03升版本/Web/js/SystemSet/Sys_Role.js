﻿$(document).ready(function () {

    CommonControl.SetFormWidth();
    //行背景
    $("#grid_pri tr").hover(function() {
        $(this).addClass("tr_in");
    }, function() {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#grid_pri tr:even").css({ background: "White" });
    //添加
    $("#btn_showadd").click(function() {
        $("#div_add").show("slow");
        $("#div_edit").hide("slow");
    });
    $(".cls_select").click(function() {
        $("#div_add").hide("slow");
        $("#div_edit").show("slow");
    });
    //添加
    $("#btn_save").click(function() {
        var msg = "";
        if ($("#txt_priName").val() == "") {
            msg += "角色名不能为空！<br/>";
        }
        else {
            if ($("#txt_priName").val().length > 15) {
                msg += "角色名太长！<br/>";
            }
        }
        if ($("#txt_priInfo").val().length > 30) {
            msg += "角色简介输入过长！";
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    //修改
    $("#btn_edit").click(function() {
        var msg = "";
        if ($("#txt_priName0").val() == "") {
            msg += "角色名不能为空！<br/>";
        }
        else {
            if ($("#txt_priName0").val().length > 15) {
                msg += "角色名太长！<br/>";
            }
        }
        if ($("#txt_priInfo0").val().length > 30) {
            msg += "角色简介输入过长！";
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    //选择信息
    $(".cls_select").click(function() {
        $("#lbl_priid").text($(this).parent().parent().find("TD").eq(1).text());
        $("#hid_priid").val($(this).parent().parent().find("TD").eq(1).text());
        $("#txt_priName0").val($(this).parent().parent().find("TD").eq(2).text());
        $("#txt_priInfo0").val($(this).parent().parent().find("TD").eq(3).text());
    });
    //删除是判断有无记录选中
    $("#btn_DelCst").click(function() {
        if ($(".cls_chk :checkbox[checked='true']").length == 0) {
            jAlert("请选择要删除的部门！", "提示");
            return false;
        }
        //判断是否要删除
        return confirm("是否要删除部门信息？");
    });
    //全选
    $("#chk_All").click(function() {
        if ($(this).get(0).checked) {
            $(":checkbox").attr("checked", "true");
        }
        else {
            $(":checkbox").attr("checked", $(this).get(0).checked);
        }
    });
});