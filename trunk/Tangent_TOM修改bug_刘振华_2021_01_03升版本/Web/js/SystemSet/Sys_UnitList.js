﻿$(document).ready(function () {
    $("#ctl00_ContentPlaceHolder1_btn_DelCst").click(function () {

        if ($("#ctl00_ContentPlaceHolder1_grid_unit :checkbox:checked").length == 0) {
            jAlert("请选择要删除的部门！", "提示");
            return false;
        }
        else {
            $('#popup_ok').attr("class", "btn green");
            return confirm("是否要删除部门信息？");
        }
    });

    $(" .btn_cancel").click(function () {
        $("#div_add").hide("slow");
        $("#div_edit").hide("slow");
    });

    $("#show8_1_1").parent().attr("class", "active");
    $("#show8_1").attr("class", "arrow open");
    $("#show8").parent().attr("class", "open");
    $("#arrow8").attr("class", "arrow open");
    $("#show8_1_1").parent().parent().css("display", "block");
    $("#show8_1_1").parent().parent().parent().parent().css("display", "block");

    $('#ctl00_ContentPlaceHolder1_grid_unit_ctl01_chkAll').click(function () {

        var checks = $('#ctl00_ContentPlaceHolder1_grid_unit :checkbox');


        if ($('#ctl00_ContentPlaceHolder1_grid_unit_ctl01_chkAll').attr("checked") == "checked") {
            for (var i = 0; i < checks.length; i++) {
                checks[i].parentNode.setAttribute("class", "checked");
                checks[i].setAttribute("checked", "checked");
            }
        }
        else {
            for (var i = 0; i < checks.length; i++) {
                checks[i].parentNode.removeAttribute("class");
                checks[i].removeAttribute("checked");

            }
        }

    });

    $("#btn_showadd").click(function () {
        $('#div_add').css('display', 'block');
        $('#div_edit').css('display', 'none');
        window.scrollTo(0, document.body.scrollHeight);
        //$('#ctl00_ContentPlaceHolder1_btn_save').focus();
    });

    //部门列表显示加载数据
    //    var set = function () {

    //        $(".cls_unitshow").each(function (i) {
    //            var obj = $(this);
    //            var unitid = $(this).attr("alt");
    //            //地址
    //            var url = "../HttpHandler/SetUnitListShowHandler.ashx";
    //            var data = { "unitid": unitid, "flag": "getdata" };
    //            $.get(url, data, function (d) {
    //                if (d == "1") {
    //                    obj.text("隐藏");
    //                }
    //            }, "text");
    //        });
    //    }
    //部门列表状态改变

    //    $("[id='EditShow']").live("click", function () {
    //        alert("ss");
    //        var obj = $(this);
    //        var unitid = $(this).attr("alt");
    //        //地址
    //        var url = "../HttpHandler/SetUnitListShowHandler.ashx";
    //        var data = { "unitid": unitid, "flag": "update" };
    //        $.get(url, data, function (d) {
    //            if (d == "0") {
    //                obj.text("显示");
    //            }
    //            else {
    //                obj.text("隐藏");
    //            }
    //        });

    //    });

    // 查看
    $(".cls_select").click(function () {
        $("#div_add").hide("slow");
        $("#div_edit").show("slow");
        //window.scrollTo(0, document.body.scrollHeight);
        $("#ctl00_ContentPlaceHolder1_lbl_unitid").text($.trim($(this).parent().parent().find("TD").eq(1).text()));

        $("#ctl00_ContentPlaceHolder1_hid_unitid").val($.trim($(this).parent().parent().find("TD").eq(1).text()));

        $("#ctl00_ContentPlaceHolder1_txt_unitName0").val($.trim($(this).parent().parent().find("TD").eq(2).text()));
        //$("#ctl00_ContentPlaceHolder1_drp_unitlist0").val($.trim($(this).parent().parent().find("TD").eq(3).text()));


        var text = $.trim($(this).parent().parent().find("TD").eq(3).text()); //上一级部门
        var sel = $("#ctl00_ContentPlaceHolder1_drp_unitlist0").find("option"); //所有的部门
        for (var i = 0; i < sel.length; i++) {
            var s = sel[i];
            if (s.text == (text)) {
                s.setAttribute("selected", true);
                break;
            }
        }

        $("#ctl00_ContentPlaceHolder1_txt_unitInfo0").val($.trim($(this).parent().parent().find("TD").eq(5).text()));

        var showname = $.trim($(this).parent().parent().find("TD").eq(4).text());

        if (showname == "显示" && showname.length == 2) {

            $("#ctl00_ContentPlaceHolder1_raBtnshow").parent().attr("class", "checked");

            $("#ctl00_ContentPlaceHolder1_raBtnHidd").parent().removeAttr("class");

        }
        else {
            $("#ctl00_ContentPlaceHolder1_raBtnHidd").parent().attr("class", "checked");

            $("#ctl00_ContentPlaceHolder1_raBtnshow").parent().removeAttr("class");

        }

        var typename = $.trim($(this).parent().parent().find("TD").eq(6).text());
        switch (typename) {
            case "施工图部门":
                $("#ctl00_ContentPlaceHolder1_drpUnitType1").val("0");
                break;
            case "方案部门":
                $("#ctl00_ContentPlaceHolder1_drpUnitType1").val("1");
                break;
            case "特殊部门(设计)":
                $("#ctl00_ContentPlaceHolder1_drpUnitType1").val("2");
                break;
            case "特殊部门(行政)":
                $("#ctl00_ContentPlaceHolder1_drpUnitType1").val("3");
                break;
            case "其他部门":
                $("#ctl00_ContentPlaceHolder1_drpUnitType1").val("4");
                break;
        }

        $("#ctl00_ContentPlaceHolder1_txt_Order2").val($(this).parent().parent().find("TD").eq(7).text());
        // window.setTimeout("cls_select_Edit()", 500); //500毫秒延迟加载
    });

    //添加
    $("#ctl00_ContentPlaceHolder1_btn_save").click(function () {
        var msg = "";
        if ($("#ctl00_ContentPlaceHolder1_txt_unitName").val() == "") {
            msg += "部门名不能为空！<br/>";
        }
        else {
            if ($("#ctl00_ContentPlaceHolder1_txt_unitName").val().length > 15) {
                msg += "部门名太长！<br/>";
            }
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_unitInfo").val().length > 30) {
            msg += "部门简介输入过长！";
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_Order2").val().length > 0)
        {
            var reg = /^[1-9]\d*$/;

            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_Order2").val()))
            {
                msg += "排序请输入数字！";
            }
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });

    //修改
    $("#ctl00_ContentPlaceHolder1_btn_edit").live("click", function () {
        var msg = "";
        if ($("#ctl00_ContentPlaceHolder1_txt_unitName0").val() == "") {
            msg += "部门名不能为空！<br/>";
        }
        else {
            if ($("#ctl00_ContentPlaceHolder1_txt_unitName0").val().length > 15) {
                msg += "部门名太长！<br/>";
            }
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_unitInfo0").val().length > 30) {
            msg += "部门简介输入过长！";
        }

        if ($("#ctl00_ContentPlaceHolder1_txt_Order").val().length > 0) {
            var reg = /^[1-9]\d*$/;

            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_Order").val())) {
                msg += "排序请输入数字！";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });

    //屏幕移动到最下方，由于js效果显示，所以延迟加载500毫秒
    function cls_select_Edit() {
        window.scrollTo(0, document.body.scrollHeight); //移动屏幕最下方
    }
    $(".cls_unitshow").click(function () {
        var obj = $(this);
        var unitid = $(this).attr("alt");
        //地址
        var url = "../HttpHandler/SetUnitListShowHandler.ashx";
        var data = { "unitid": unitid, "flag": "update", "n": Math.random() * 12353 };
        $.get(url, data, function (d) {
            if (d == "0") {
                obj.text("显示");
            }
            else {
                obj.text("隐藏");
            }
        });

    });
    // set();
});

var set = function () {
    $(".cls_unitshow").click(function () {
        var obj = $(this);
        var unitid = $(this).attr("alt");
        //地址
        var url = "../HttpHandler/SetUnitListShowHandler.ashx";
        var data = { "unitid": unitid, "flag": "update", "n": Math.random() * 12353 };
        $.get(url, data, function (d) {
            if (d == "0") {
                obj.text("显示");
            }
            else {
                obj.text("隐藏");
            }
        });

    });
}
