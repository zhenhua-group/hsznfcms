﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/SystemSet/Unit_MemberHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '部门名称', '上级部门', '部门列表', '部门描述', '选择'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 45, align: 'center' },
                             { name: 'unit_ID', index: 'unit_ID', hidden: true, editable: true },
                             { name: 'unit_ParentID', index: 'unit_ParentID', hidden: true, editable: true },
                             { name: 'unit_Name', index: 'unit_Name', width: 200, formatter: colShowMemFormatter },
                             { name: 'unit_ParentName', index: 'unit_ParentName', width: 100, align: 'center' },
                             { name: 'unit_IsUnit', index: 'unit_IsUnit', width: 100, align: 'center', formatter: colShowUnitFormatter },
                             { name: 'unit_Intro', index: 'unit_Intro', width: 200, align: 'center' },
                             { name: 'unit_ID', index: 'unit_ID', width: 100, align: 'center', formatter: colShowFormatter }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#hid_where").val()) },
        loadonce: false,
        sortname: 'unit_id',
        sortorder: 'asc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/SystemSet/Unit_MemberHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                closeOnEscape: true,
                closeAfterDelete: true,
                reloadAfterSubmit: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid').jqGrid('getCell', sel_id[i], 'unit_ID') + ",";
                            }
                        }
                       // var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        //var value = $('#jqGrid').jqGrid('getCell', sel_id, 'unit_ID');
                        return values;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#hid_where").val());
        var unit_name = $("#txt_keyname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/SystemSet/Unit_MemberHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, "keyname": unit_name },
            page: 1

        }).trigger("reloadGrid");
    });
});
//部门名称
function colShowMemFormatter(celvalue, options, rowData) {
    var imgstr = '<img src="../Images/part.png" style="width: 16px; height: 16px; margin-left: 1px; border:none;" />';
    return imgstr + celvalue;   
}


//编辑
function colShowFormatter(celvalue, options, rowData) {
  
    return '<a href="#" class="cls_select">编辑</a>';

}
//显示列表
function colShowUnitFormatter(celvalue, options, rowdata) {
    var str = "显示";
    if ($.trim(celvalue) == "1") {
        str="隐藏";
    }
    var pageurl = '<a href="###" class="cls_unitshow" alt="' + rowdata["unit_ID"] + '">' + str + '</a>';
    return pageurl;
}
//统计 
function completeMethod() {

    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    var summem = 0, sumpro = 0;
    //一种写法
    //             for(var i = 0, j = rowIds.length; i < j; i++) {   
    //             var curRowData = $("#jqGrid").jqGrid('getRowData', rowIds[i]);   
    //             var curChk = $("#"+rowIds[i]+"").find("td").eq(4).attr("a");             
    //          }   
    //二种写法
    $(rowIds).each(function () {
       // summem = summem + parseInt($("#" + this).find("td").eq(4).text());
       // sumpro = sumpro + parseInt($("#" + this).find("td").eq(6).text());
        $("#" + this).find("td").eq(1).text(this);
        //                var aa = $("#" + this).find("td").eq(5);
        //                var data = "action=fzr&unitid=" + aa.text();
        //                $.ajax({
        //                    type: "get",
        //                    dataType: "text",
        //                    url: "/HttpHandler/CommHandler.ashx",
        //                    data: data,
        //                    success: function (result) {
        //                        aa.text(result);
        //                    }
        //                });
    });

    // var sumAcount=parseFloat($("#jqGrid").getCol("memcount",false,'sum')).toFixed(4);
    //  var sumArea=parseFloat($("#jqGrid").getCol("projectcount",false,'sum')).toFixed(4);
  //  $("#jqGrid").footerData('set', { unit_Name: "合计:", memcount: summem, projectcount: sumpro }, false);


}
//无数据
function loadCompMethod() {   
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        $("#nodata").show();
    }
    else {
        $("#nodata").hide();
    }
}