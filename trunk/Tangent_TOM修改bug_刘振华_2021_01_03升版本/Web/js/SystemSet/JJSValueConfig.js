﻿$(function () {


    $("#AddTable tr").empty();
    $.get("../HttpHandler/JJSValueConfigHandler.ashx?m=" + Math.random(), { "option": "get" }, function (dataJson) {

        //alert(dataJson);
        var json = eval('(' + dataJson + ')');

        $.each(json, function (key, valObj) {
            var $tr = $("<tr></tr>");
            if (valObj.typeStatus == "13" || valObj.typeStatus == "14" || valObj.typeStatus == "15" || valObj.typeStatus == "16" || valObj.typeStatus == "17" || valObj.typeStatus == "19" || valObj.typeStatus == "20" || valObj.typeStatus == "21" || valObj.typeStatus == "22") {

                var $tdA1 = $("<td style=\" width:30%\" align=\"left\">" + valObj.typeDetail + "</td>");
                var $tdA2 = $("<td style=\" width:7%\">" + (100 - valObj.ProofreadPercent) + "</td>");
                var $tdA3 = $("<td colspan=\"3\"style=\" width:21%\">" + GetNum(valObj.Totalbuildingpercent) + "</td>");
                var $tdA4 = $("<td colspan=\"4\"style=\" width:28%\">" + GetNum(valObj.TotalInstallationpercent) + "</td>");
                var $tdA5 = $("<td style=\" width:7%\">" + GetNum(valObj.ProofreadPercent) + "</td>");
                var $tdA6 = $("<td style=\" width:7%\"><a href=\"###\" class=\"Edit\">编辑</a></td>");
                var $hidd = $("<td style=\"display:none\">" + valObj.typeStatus + "</td>");
                $tr.append($tdA1); $tr.append($tdA2); $tr.append($tdA3); $tr.append($tdA4); $tr.append($tdA5); $tr.append($tdA6);
                $tr.append($hidd);
            }
            //            else if (valObj.typeStatus == "24" || valObj.typeStatus == "25") {
            //                var $tdA1 = $("<td>" + valObj.typeDetail + "</td>");
            //                var $tdA2 = $("<td>" + (100 - valObj.ProofreadPercent) + "</td>");
            //                var $tdA3 = $("<td colspan=\"7\"></td>");
            //                var $tdA4 = $("<td>" + valObj.ProofreadPercent + "</td>");
            //                var $tdA5 = $("<td><a href=\"###\" class=\"Edit\">编辑</a></td>");
            //                var $hidd = $("<input type=\"hidden\" value=\"" + valObj.typeStatus + "\">")
            //                $tr.append($tdA1); $tr.append($tdA2); $tr.append($tdA3); $tr.append($tdA4); $tr.append($tdA5); $tr.append($hidd);
            //            }
            else {
                var $tdA1 = $("<td style=\" width:30%\">" + valObj.typeDetail + "</td>");
                var $tdA2 = $("<td style=\" width:7%\">" + (100 - valObj.ProofreadPercent) + "</td>");
                var $tdA3 = $("<td style=\" width:7%\">" + valObj.Totalbuildingpercent + "</td>");
                var $tdA4 = $("<td style=\" width:7%\">" + GetNum(valObj.BuildingPercent) + "</td>");
                var $tdA5 = $("<td style=\" width:7%\">" + GetNum(valObj.StructurePercent) + "</td>");
                var $tdA6 = $("<td style=\" width:7%\">" + valObj.TotalInstallationpercent + "</td>");
                var $tdA7 = $("<td style=\" width:7%\">" + GetNum(valObj.DrainPercent) + "</td>");
                var $tdA8 = $("<td style=\" width:7%\">" + GetNum(valObj.HavcPercent) + "</td>");
                var $tdA9 = $("<td style=\" width:7%\">" + GetNum(valObj.ElectricPercent) + "</td>");
                var $tdA10 = $("<td style=\" width:7%\">" + GetNum(valObj.ProofreadPercent) + "</td>");
                var $tdA11 = $("<td style=\" width:7%\"><a href=\"###\" class=\"Edit\">编辑</a></td>");
                var $hidd = $("<td style=\"display:none\">" + valObj.typeStatus + "</td>");
                $tr.append($tdA1); $tr.append($tdA2); $tr.append($tdA3); $tr.append($tdA4);
                $tr.append($tdA5); $tr.append($tdA6); $tr.append($tdA7); $tr.append($tdA8); $tr.append($tdA9); $tr.append($tdA10); $tr.append($tdA11);
                $tr.append($hidd);
            }
            $("#AddTable").append($tr);
        })
    })
    var typestatus = "";
    //隔行变色
    $("#AddTable tr:even").css({ background: "White" });
    $(".Edit").live("click", function () {
        var $ThisTr = $(this).parent().parent();
        var hiddtype = $(this).parent().parent().find("TD").last().text();
        if (hiddtype == "13" || hiddtype == "14" || hiddtype == "15" || hiddtype == "16" || hiddtype == "17" || hiddtype == "19" || hiddtype == "20" || hiddtype == "21" || hiddtype == "22") {
            var title = $(this).parent().parent().find("TD").eq(0).text();
            $("#title2").text(title + "编辑专业编制、校对分配比例")
            var JZH = $(this).parent().parent().find("TD").eq(2).text();
            $("#JZH").val(JZH)
            var AZ = $(this).parent().parent().find("TD").eq(3).text();
            $("#AZ").val(AZ)
            var JD2 = $(this).parent().parent().find("TD").eq(4).text();
            $("#JD2").val(JD2)
            typestatus = hiddtype;
            $("#EditJJSValue2").show("4", function () {
                window.scrollTo(0, document.body.scrollHeight);
            });
            $("#EditJJSValue").hide();
        }
        //            else if (hiddtype == "24" || hiddtype == "25") {
        //                $("#EditJJSValue").hide();
        //                $("#EditJJSValue2").hide();
        //            }
        else {
            var title = $(this).parent().parent().find("TD").eq(0).text();
            $("#title").text(title + "编辑专业编制、校对分配比例")
            var TJ = $(this).parent().parent().find("TD").eq(3).text();
            $("#TJ").val(TJ)
            var JG = $(this).parent().parent().find("TD").eq(4).text();
            $("#JG").val(JG)
            var GPS = $(this).parent().parent().find("TD").eq(6).text();
            $("#GPS").val(GPS)
            var CN = $(this).parent().parent().find("TD").eq(7).text();
            $("#CN").val(CN)
            var DQ = $(this).parent().parent().find("TD").eq(8).text();
            $("#DQ").val(DQ)
            var JD = $(this).parent().parent().find("TD").eq(9).text();
            $("#JD").val(JD)

            typestatus = hiddtype;
            $("#EditJJSValue").show("4", function () {
                window.scrollTo(0, document.body.scrollHeight);
            });
            $("#EditJJSValue2").hide();
        }
    })
    $("#btnCancl").live("click", function () {
        $("#EditJJSValue").hide();
    });
    $("#btnCancl2").live("click", function () {
        $("#EditJJSValue2").hide();
    })
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    //项目各设计阶段保存
    $("#btnOK").live("click", function () {
        if (!reg.test($("#TJ").val())) {
            $("#TJSpan").text("请输入数字！").show();
            return false;
        }
        else {
            $("#TJSpan").hide();
        }
        if (!reg.test($("#JG").val())) {
            $("#JGSpan").text("请输入数字！").show();
            return false;
        }
        else {
            $("#JGSpan").hide();
        }
        if (!reg.test($("#GPS").val())) {
            $("#GPSSpan").text("请输入数字！").show();
            return false;
        }
        else {
            $("#GPSSpan").hide();
        }
        if (!reg.test($("#CN").val())) {
            $("#CNSpan").text("请输入数字！").show();
            return false;
        }
        else {
            $("#CNSpan").hide();
        }
        if (!reg.test($("#DQ").val())) {
            $("#DQSpan").text("请输入数字！").show();
            return false;
        }
        else {
            $("#DQSpan").hide();
        }
        if (!reg.test($("#JD").val())) {
            $("#JDSpan").text("请输入数字！").show();
            return false;
        }
        else {
            $("#JDSpan").hide();
        }
        if (parseInt($("#JD").val()) >= 100) {
            alert("校对分配比例额不能大于100，请重新填写");
            return false;
        }
        if ((parseInt($("#TJ").val()) + parseInt($("#JG").val()) + parseInt($("#GPS").val()) + parseInt($("#CN").val()) + parseInt($("#DQ").val())) != "100") {
            alert("建筑、安装下的各专业编制不闭合，请闭合数据");
            return false;
        }
        else {
            $.get("../HttpHandler/JJSValueConfigHandler.ashx", { "option": "save", "JD": $("#JD").val(), "TJ": $("#TJ").val(), "CN": $("#CN").val(), "GPS": $("#GPS").val(), "JG": $("#JG").val(), "DQ": $("#DQ").val(), "type": typestatus }, function (dataJson) {
                if (dataJson == "1") {
                    alert("保存成功！");
                    $("#EditJJSValue").hide();
                    $("#EditJJSValue2").hide();
                    window.location.href = "/SystemSet/JJSValueParameterConfigBymaster.aspx";
                }
                else {
                    alert("保存失败！");
                }
            });
        }

    });
    $("#btnOK2").live("click", function () {
        if (!reg.test($("#JZH").val())) {
            $("#JZHSpan").text("请输入数字！").show();
            return false;
        }
        else {
            $("#JZHSpan").hide();
        }
        if (!reg.test($("#AZ").val())) {
            $("#AZSpan").text("请输入数字！").show();
            return false;
        }
        else {
            $("#AZSpan").hide();
        }

        if (!reg.test($("#JD2").val())) {
            $("#JD2Span").text("请输入数字！").show();
            return false;
        }
        else {
            $("#JD2Span").hide();
        }
        if (parseInt($("#JD").val()) >= 100) {
            alert("校对分配比例额不能大于100，请重新填写");
            return false;
        }
        if ((parseInt($("#AZ").val()) + parseInt($("#JZH").val())) != "100") {
            alert("建筑、安装下的编制不闭合，请闭合数据");
            return false;
        }
        else {
            $.get("../HttpHandler/JJSValueConfigHandler.ashx", { "option": "save2", "JZH": $("#JZH").val(), "AZ": $("#AZ").val(), "JD2": $("#JD2").val(), "type": typestatus }, function (dataJson) {
                if (dataJson == "1") {
                    alert("保存成功！");
                    $("#EditJJSValue").hide();
                    $("#EditJJSValue2").hide();
                    window.location.href = "/SystemSet/JJSValueParameterConfigBymaster.aspx";
                }
                else {
                    alert("保存失败！");
                }
            });
        }

    });

})
function GetNum(item) {
    if (item == null) {
        return "0";
    }
    else {
        return item;
    }
}