﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //隔行变色
    $("#ctl00_ContentPlaceHolder1_GridView1 tr:even").css({ background: "White" });
    $("#ctl00_ContentPlaceHolder1_GridView2 tr:even").css({ background: "White" });
    $("#ctl00_ContentPlaceHolder1_GridView3 tr:even").css({ background: "White" });
    //默认判断
    $(".cls_btnCmp").each(function (i) {
        if ($(this).prev(":hidden").val() == "1") {
            $(this).hide();
            $(this).parent().html("使用中");
        }
    });
    $(".cls_btnUser").each(function (i) {
        if ($(this).prev(":hidden").val() == "1") {
            $(this).hide();
            $(this).parent().html("使用中");
        }
    });
    //院配置
    $(".cls_addCmpConfig").click(function () {
        $("#tb_CmpTable").show();
    });
    //删除
    $(".cls_delCmpConfig").click(function () {
        if (confirm("确定要删除本条配置吗？")) {
            var id = $(this).next(":hidden").val();
            delDataItem(id, 'cmp');
            $(this).parent().parent().remove();
        }
    });
    //专业配置 
    $(".cls_addSpeConfig").click(function () {
        $("#tb_SpeTable").show();
    });
    //删除
    $(".cls_delSpeConfig").click(function () {
        if (confirm("确定要删除本条配置吗？")) {
            var id = $(this).next(":hidden").val();
            delDataItem(id, 'spe');
            $(this).parent().parent().remove();
        }
    });
    //用户配置
    $(".cls_addUserConfig").click(function () {
        $("#tb_UserTable").show();
    });
    //删除
    $(".cls_delUserConfig").click(function () {
        if (confirm("确定要删除本条配置吗？")) {
            var id = $(this).next(":hidden").val();
            delDataItem(id, 'user');
            $(this).parent().parent().remove();
        }
    });
    //提交删除
    function delDataItem(id, type) {
        var data = "id=" + id + "&type=" + type;
        var url = "../HttpHandler/ProjectAllotConfig.ashx";
        $.ajax({
            type: "POST",
            dataType: "text",
            data: data,
            url: url,
            success: function (rlt) {
                if (rlt == "success") {
                    alert("删除成功！");
                }
            },
            error: function () {
                alert("数据库错误!");
            }
        });
    }
    //院保存 
    var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
    $("#ctl00_ContentPlaceHolder1_btn_Save").click(function () {
        if ($("#ctl00_ContentPlaceHolder1_txt_config1").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config1").val())) {
                alert("请输入数字1！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config1").val("0.00");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_config2").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config2").val())) {
                alert("请输入数字2！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config2").val("0.00");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_config3").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config3").val())) {
                alert("请输入数字3！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config3").val("0.00");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_config4").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config4").val())) {
                alert("请输入数字4！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config4").val("0.00");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_config5").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config5").val())) {
                alert("请输入数字5！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config5").val("0.00");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_config6").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config6").val())) {
                alert("请输入数字6！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config6").val("0.00");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_config7").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config7").val())) {
                alert("请输入数字7！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config7").val("0.00");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_config8").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config8").val())) {
                alert("请输入数字8！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config8").val("0.00");
        }
    });
    // 专业
    $("#ctl00_ContentPlaceHolder1_btn_SaveSpe").click(function () {
        if ($("#ctl00_ContentPlaceHolder1_drp_strutype").val() == "-1") {
            alert("请选择专业！");
            return false;
        }
        if ($("#ctl00_ContentPlaceHolder1_DropDownList1").val() == "-1") {
            alert("请选择专业！");
            return false;
        }
        if ($("#ctl00_ContentPlaceHolder1_TextBox2").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_TextBox2").val())) {
                alert("请输入数字！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_TextBox2").val("0.00");
        }
    });
    //用户
    $("#ctl00_ContentPlaceHolder1_btn_SaveUser").click(function () {
        if ($("#ctl00_ContentPlaceHolder1_txt_config9").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config9").val())) {
                alert("请输入数字！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config9").val("0.00");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_config10").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config10").val())) {
                alert("请输入数字！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config10").val("0.00");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_config11").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config11").val())) {
                alert("请输入数字！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config11").val("0.00");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_config12").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config12").val())) {
                alert("请输入数字！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config12").val("0.00");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_config13").val() != "") {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_config13").val())) {
                alert("请输入数字！");
                return false;
            }
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_config13").val("0.00");
        }
    });
    $("#userCanle").click(function () {
        $("#tb_UserTable").hide();
    });
    $("#speCance").click(function () {
        $("#tb_SpeTable").hide();
    }); $("#deCanle").click(function () {
        $("#tb_CmpTable").hide();
    });
});
 