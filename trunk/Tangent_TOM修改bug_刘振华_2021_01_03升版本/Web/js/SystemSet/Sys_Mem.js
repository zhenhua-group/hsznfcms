﻿var swfu;
$(document).ready(function () {
    var flag = true;

    $("#btn_showadd").click(function () {
        $('#div_add').show();
        $("#div_edit").hide();
        window.scrollTo(0, document.body.scrollHeight);

    });
    $("#btn_Cancl").click(function () {
        $('#div_add').hide("slow");

        window.scrollTo(0, 0);

    });
    $("#btn_Cancle").click(function () {

        $("#div_edit").hide("slow");
        window.scrollTo(0, 0);

    });
    //删除是判断有无记录选中
    $("#ctl00_ContentPlaceHolder1_btn_DelCst").click(function () {
        if ($("#ctl00_ContentPlaceHolder1_grid_mem :checkbox:checked").length == 0) {
            jAlert("请选择要删除的人员！", "提示");
            return false;
        }
        else {
            $('#popup_ok').attr("class", "btn green");
            return confirm("是否要删除人员信息？");
        }
    });
    $('#ctl00_ContentPlaceHolder1_grid_mem_ctl01_chkAll').click(function () {

        var checks = $('#ctl00_ContentPlaceHolder1_grid_mem :checkbox');


        if ($('#ctl00_ContentPlaceHolder1_grid_mem_ctl01_chkAll').attr("checked") == "checked") {
            for (var i = 0; i < checks.length; i++) {
                checks[i].parentNode.setAttribute("class", "checked");
                checks[i].setAttribute("checked", "checked");
            }
        }
        else {
            for (var i = 0; i < checks.length; i++) {
                checks[i].parentNode.removeAttribute("class");
                checks[i].removeAttribute("checked");
            }
        }
    });

    $(".cls_select").live("click", function () {
        $("#div_add").hide("slow");
        $("#div_edit").show("slow");
        //window.scrollTo(0, document.body.scrollHeight);
        $("#ctl00_ContentPlaceHolder1_hid_memid").val($(this).parent().parent().find("TD").eq(1).text());
        $("#ctl00_ContentPlaceHolder1_txt_memLogin0").val($(this).parent().parent().find("TD").eq(2).text());
        $("#ctl00_ContentPlaceHolder1_hid_memlogin").val($(this).parent().parent().find("TD").eq(2).text());
        $("#ctl00_ContentPlaceHolder1_txt_memLogin0").get(0).disabled = true;
        $("#ctl00_ContentPlaceHolder1_txt_memName0").val($(this).parent().parent().find("TD").eq(3).text());
        $("#ctl00_ContentPlaceHolder1_txt_Birth0").val($(this).parent().parent().find("TD").eq(4).text());
        $("#ctl00_ContentPlaceHolder1_drp_sex0").val($(this).parent().parent().find("TD").eq(5).text());

        $("#ctl00_ContentPlaceHolder1_drp_level0").val($(this).parent().parent().find("TD").eq(11).text());
        //在职离职
        if ($(this).parent().parent().find("TD").eq(15).text() == "在职") {
            $("#ctl00_ContentPlaceHolder1_drpIsFired0").val("0");
        }
        else {
            $("#ctl00_ContentPlaceHolder1_drpIsFired0").val("1");
        }
        //入职，离职时间
        $("#ctl00_ContentPlaceHolder1_txtInTime0").val($(this).parent().parent().find("TD").eq(16).text());
        $("#ctl00_ContentPlaceHolder1_txtOutTime0").val($(this).parent().parent().find("TD").eq(17).text());
        var dzqm = $(this).parent().parent().find("TD").eq(12).find("a").attr("href");


        if (dzqm == undefined || $.trim(dzqm) == "/Images/zw.jpg") {
            $("#ctl00_ContentPlaceHolder1_txt_pic0").val("");
            $("#WebUserControl2_BaseInfo_Image").val("");
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_pic0").val(dzqm);
            $("#WebUserControl2_BaseInfo_Image").val(dzqm);
        }

        //选中兼职部门
        CheckUnitList($(this).parent().parent().find("TD").eq(1).text());
        $("#hidUserSysNo").val($(this).parent().parent().find("TD").eq(1).text());


        flag = false;
        var unitString = $.trim($(this).parent().parent().find("TD").eq(6).text());
        $.each($("#ctl00_ContentPlaceHolder1_drp_unit0 option"), function (index, option) {
            if ($.trim($(option).text()) == unitString) {
                $(option).attr("selected", "selected");
            }
        });

        var specString = $.trim($(this).parent().parent().find("TD").eq(7).text());
        $.each($("#ctl00_ContentPlaceHolder1_drp_spec0 option"), function (index, option) {
            if ($.trim($(option).text()) == specString) {
                $(option).attr("selected", "selected");
            }
        });

        var rolesString = $.trim($(this).parent().parent().find("TD").eq(8).text());
        $.each($("#ctl00_ContentPlaceHolder1_drp_roles0 option"), function (index, option) {
            if ($.trim($(option).text()) == rolesString) {
                $(option).attr("selected", "selected");
            }
        });

        $("#ctl00_ContentPlaceHolder1_txt_phone0").val($.trim($(this).parent().parent().find("TD").eq(9).text()));

        $("#ctl00_ContentPlaceHolder1_txt_mobile0").val($.trim($(this).parent().parent().find("TD").eq(10).text()));
        $("#ctl00_ContentPlaceHolder1_txt_mail0").val($.trim($(this).parent().parent().find("TD").eq(15).find(":hidden").val()));
        //管理职位
        var roleMag = parseInt($(this).parent().parent().find("TD").eq(13).find(":hidden").val()) <= 0 ? '0' : $(this).parent().parent().find("TD").eq(13).find(":hidden").val();
        $("#ctl00_ContentPlaceHolder1_drpRoleMag0").val(roleMag);
        //技术职位
        var roleTec = parseInt($(this).parent().parent().find("TD").eq(14).find(":hidden").val()) <= 0 ? '0' : $(this).parent().parent().find("TD").eq(14).find(":hidden").val();
        $("#ctl00_ContentPlaceHolder1_drpRoleTec0").val(roleTec);
        //排序
        $("#ctl00_ContentPlaceHolder1_txtOrder2").val($(this).next().val());

        //if (flag) {
        ActionPic($("#ctl00_ContentPlaceHolder1_txt_memLogin0").val(), "0");
        //} 
    });

    $("#ctl00_ContentPlaceHolder1_txt_memLogin").blur(function () {
        ActionPic($(this).val(), "");
        $.ajax({
            type: "POST",
            url: "../HttpHandler/LoginHandler.ashx?action=exsitlogin",
            data: "memlogin=" + escape($(this).val()),
            success: function (rlt) {
                if (rlt == "yes") {
                    $("#ctl00_ContentPlaceHolder1_hid_exsit").val("1");
                    jAlert("用户名已存在！", "提示");
                    $("#ctl00_ContentPlaceHolder1_txt_memLogin").addClass("cls_input_red");
                }
                else {
                    $("#ctl00_ContentPlaceHolder1_hid_exsit").val("");
                    $("#ctl00_ContentPlaceHolder1_txt_memLogin").removeClass("cls_input_red");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $("#ctl00_ContentPlaceHolder1_hid_exsit").val("1");
            }
        });
    });

    //添加
    $("#ctl00_ContentPlaceHolder1_btn_save").click(function () {

        var msg = "";
        //姓名
        if ($("#ctl00_ContentPlaceHolder1_txt_memName").val() == "") {
            msg += "用户姓名不能为空！<br/>";
        }
        else {
            if ($("#ctl00_ContentPlaceHolder1_txt_memName").val().length > 10) {
                msg += "用户姓名太长！<br/>";
            }
        }
        //登录名
        if ($("#ctl00_ContentPlaceHolder1_txt_memLogin").val() == "") {
            msg += "登录名不能为空！<br/>";
        }
        else {
            if ($("#ctl00_ContentPlaceHolder1_txt_memLogin").val().length > 15) {
                msg += "登录名过长！<br/>";
            }

            var reg2 = /^[a-zA-Z]\w{3,15}$/;
            if (!reg2.test($("#ctl00_ContentPlaceHolder1_txt_memLogin").val())) {
                msg += "登录名只能是字母数字下划线，并且以字母开头！<br/>";
            }
        }
        if ($("#hid_exsit").val() == "1") {
            msg += "登录名已经存在！<br/>";
        }
        //密码
        if ($("#ctl00_ContentPlaceHolder1_txt_pwd").val().length > 16) {
            msg += "密码输入过长！<br/>";
        }
        //专业
        if ($("#ctl00_ContentPlaceHolder1_drp_spec").val() == "-1") {
            msg += "请选择专业！<br/>";
        }
        //单位
        if ($("#ctl00_ContentPlaceHolder1_drp_unit").val() == "-1") {
            msg += "请选择单位！<br/>";
        }
        //角色
        if ($("#ctl00_ContentPlaceHolder1_drp_roles").val() == "-1") {
            msg += "请选择角色！<br/>";
        }
        //邮箱
        if ($("#ctl00_ContentPlaceHolder1_txt_mail").val() != "") {
            var reg = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
            if (!reg.test($("##ctl00_ContentPlaceHolder1_txt_mail").val())) {
                msg += "邮件格式不正确！";
            }
        }
        //手机
        if ($("#ctl00_ContentPlaceHolder1_txt_mobile").val() != "") {
            if ($("#ctl00_ContentPlaceHolder1_txt_mobile").val().length > 11) {
                msg += "手机号码输入过长！<br/>";
            }
        }
        //电话
        if ($("#ctl00_ContentPlaceHolder1_txt_phone").val() != "") {
            if ($("#ctl00_ContentPlaceHolder1_txt_phone").val().length > 16) {
                msg += "电话输入长度过长！<br/>";
            }
        }
        //判断是否为数字
        var reg = /^\d+$/;
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtOrder").val())) {
            msg += "排序请输入数字！<br/>";
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    //修改
    $("#ctl00_ContentPlaceHolder1_btn_edit").live("click", function () {
        var msg = "";
        //姓名
        if ($("#ctl00_ContentPlaceHolder1_txt_memName0").val() == "") {
            msg += "用户姓名不能为空！<br/>";
        }
        else {
            if ($("#ctl00_ContentPlaceHolder1_txt_memName0").val().length > 10) {
                msg += "用户姓名太长！<br/>";
            }
        }
        //密码
        if ($("#ctl00_ContentPlaceHolder1_txt_pwd0").val().length > 16) {
            msg += "密码输入过长！<br/>";
        }
        //专业
        if ($("#ctl00_ContentPlaceHolder1_drp_spec0").val() == "-1") {
            msg += "请选择专业！<br/>";
        }
        //单位
        if ($("#ctl00_ContentPlaceHolder1_drp_unit0").val() == "-1") {
            msg += "请选择单位！<br/>";
        }
        //角色
        if ($("#ctl00_ContentPlaceHolder1_drp_roles0").val() == "-1") {
            msg += "请选择角色！<br/>";
        }
        //邮箱
        if ($("#ctl00_ContentPlaceHolder1_txt_mail0").val() != "") {
            var reg = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_mail0").val())) {
                msg += "邮件格式不正确！";
            }
        }
        //手机
        if ($("#ctl00_ContentPlaceHolder1_txt_mobile0").val() != "") {
            if ($("#ctl00_ContentPlaceHolder1_txt_mobile0").val().length > 11) {
                msg += "手机号码输入过长！<br/>";
            }
        }
        //电话
        if ($("#ctl00_ContentPlaceHolder1_txt_phone0").val() != "") {
            if ($("#ctl00_ContentPlaceHolder1_txt_phone0").val().length > 16) {
                msg += "电话输入长度过长！<br/>";
            }
        }
        //判断是否为数字
        var reg = /^\d+$/;
        if (!reg.test($("#ctl00_ContentPlaceHolder1_txtOrder2").val())) {
            msg += "排序请输入数字！<br/>";
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });

    //选中事件
    $("#ctl00_ContentPlaceHolder1_chkList span:has(input)").click(function () {

        if ($(this).find("span").attr("class") == "checked") {
            $(this).find("input").attr("checked", true);
        }
        else {
            $(this).find("input").attr("checked", false);
        }
    });
    $("#ctl00_ContentPlaceHolder1_chkList0 span:has(input)").click(function () {

        if ($(this).find("span").attr("class") == "checked") {
            $(this).find("input").attr("checked", true);
        }
        else {
            $(this).find("input").attr("checked", false);
        }
    });

    //保存编辑
    $("#btnSaveEdit").click(function () {
        //编辑时先保存人员角色
        SaveMemsRoles();
    });
    $("#ctl00_ContentPlaceHolder1_btn_edit").hide();

    //序号调整保存
    $(".order").focus(function () {
        $("#hidTempOrder").val($(this).val());
    }).blur(function () {
        if ($("#hidTempOrder").val() == $(this).val()) {
            return false;
        }
        var reg = /^[1-9]d*|0$/;
        if (!reg.test($(this).val())) {
            alert('请输入数字！');
            return false;
        }

        var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
        var action = "saveorder";
        var data = { "action": action, "order": $(this).val(), "uid": $(this).attr("uid") };

        $.post(url, data, function (result) {

            if (result) {
                if (result == "1") {
                    $("#ctl00_ContentPlaceHolder1_btn_Search").get(0).click();
                }
            }
        });
    });
});

function cls_select_Edit() {
    window.scrollTo(0, document.body.scrollHeight); //移动屏幕最下方
}

function Clear() {
    $("input", $("#div_add")).val("");
    window.document.getElementsByTagName("select").selectedIndex = 0;
}
function ActionPic(memid, param) {

    var divid = "divFileProgressContainer" + param;
    var spanid = "spanButtonPlaceholder" + param;

    //附件高清图
    swfu = new SWFUpload({

        upload_url: "../ProcessUpload/upload_sign.aspx?type=sign&id=" + memid + "&userid=" + userid,
        flash_url: "../js/swfupload/swfupload.swf",
        post_params: {
            "ASPSESSID": "<%=Session.SessionID %>"
        },
        file_size_limit: "10 MB",
        file_types: "*.jpg;*.gif;*.bmp;.png",
        file_types_description: "文件资料上传",
        file_upload_limit: "0",
        file_queue_limit: "1",

        //Events
        file_queued_handler: fileQueued,
        file_queue_error_handler: fileQueueError,
        file_dialog_complete_handler: fileDialogComplete,
        upload_progress_handler: uploadProgress,
        upload_error_handler: uploadError,
        upload_success_handler: uploadSuccess,
        upload_complete_handler: uploadComplete,

        // Button
        button_placeholder_id: spanid,
        button_image_url: "../images/swfupload/XPButtonNoText_61x22.png",
        button_width: 61,
        button_height: 22,
        button_text: '<span class="btnFile">选择文件</span>',
        button_text_style: '.btnFile { font-family: 微软雅黑; font-size: 11pt;background-color:Black; } ',
        button_text_top_padding: 1,
        button_text_left_padding: 5,

        custom_settings: {
            upload_target: divid
        },
        debug: false
    });

}

function CheckUnitList(memid) {
    // 获取多部门
    var url = "/HttpHandler/DeptBpm/ProjDeptHandler.ashx";
    var action = "getroleunit";
    var data = { "action": action, "id": memid };

    $.get(url, data, function (result) {

        if (result) {
            //专业人员
            var mems = eval("(" + result + ")");
            //clear
            $("#ctl00_ContentPlaceHolder1_chkList0 tbody tr td").each(function (i) {
                //部门ID
                var obj = $(this);
                //如果是同一专业
                obj.find("span").attr("class", "");
                obj.find("input").attr("checked", false);
            });


            //如果是同一专业
            $.each(mems, function (i, item) {
                $("#ctl00_ContentPlaceHolder1_chkList0 span[alt='" + item.mem_Unit_ID + "']").attr("class", "checked");
                $("#ctl00_ContentPlaceHolder1_chkList0 span[alt='" + item.mem_Unit_ID + "']").find(":checkbox").attr("checked", true).parent().attr("class", "checked");
            });
        }
    });
}
//保存用户多部门
function SaveMemsRoles() {
    // 获取多部门
    var url = "/HttpHandler/SystemSet/UserInfoHandler.ashx";
    var action = "savememroles";

    //选中的部门
    var arrUnit = [];

    $("#ctl00_ContentPlaceHolder1_chkList0 tbody tr td").each(function (i) {

        var obj = $(this);

        arrUnit[i] = {
            unitID: obj.find("span[alt]").attr("alt"),
            MngRoleID: $("#ctl00_ContentPlaceHolder1_drpRoleMag0").val(),
            TecRoleID: $("#ctl00_ContentPlaceHolder1_drpRoleTec0").val(),
            MemID: $("#hidUserSysNo").val(),
            IsSelected: obj.find("span[alt]").find("span").attr("class")
        };

    });
    var data = { "action": action, "data": JSON.stringify(arrUnit) };

    $.post(url, data, function (result) {

        if (result) {
            if (result == "1") {
                $("#ctl00_ContentPlaceHolder1_btn_edit").get(0).click();
            }
        }
    });
}