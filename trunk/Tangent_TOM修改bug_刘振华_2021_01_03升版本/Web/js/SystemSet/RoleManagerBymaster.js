﻿function RoleManagerBymaster() {
    this.Dom = {};
    var Instance = this;
    this.IsEdit = false;
    this.RoleSysNo = 0;
    this.BackgroundInvoke = TG.Web.SystemSet.RoleManagerBymaster;
    var selectUserControl = new SelectRoleUser(this, $("#chooseUserMain"));

    Instance.Dom.EditButton = $("a[id=editRoleLinkButton]");
    //保存
    Instance.Dom.BtnSaveRole = $("#btn_saveRole");
    this.Dom.BtnSaveRole.live("click", function () {
        Instance.SaveRoleUser();
        $("#btn_close").trigger("click");
    })
    this.Dom.EditButton.live("click", function () {
        Instance.Clear();
        Instance.IsEdit = true;
        var roleName = $(this).attr("rolename");
        var roleSysNo = $(this).attr("rolesysno");
        Instance.RoleSysNo = roleSysNo;
        $("h4").text("编辑" + roleName + "所对应的用户");
        //如果为普通员工，不允许修改角色名
        if ($.trim($(this).attr("rolename")) == "普通员工") {
            $("#roleNameTextBox").attr("disabled", "disabled");
        } else {
            $("#roleNameTextBox").removeAttr("disabled");
        }
        var result = Instance.BackgroundInvoke.GetRoleViewEntity(roleSysNo).value;
        if (result != null && result.length > 0) {
            var roleManagerViewEntity = Global.evalJSON(result);
            //循环拼接用户
            $.each(roleManagerViewEntity.DictionaryUser, function (userSysNo, userName) {
                $("#roleUserDiv").append("<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"" + userSysNo + "\">" + userName + "<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
            });
            $("#roleNameTextBox").val(roleManagerViewEntity.RoleName);
        }
    });

    this.Dom.AddButton = $("#AddNewRole");
    this.Dom.AddButton.click(function () {
        Instance.IsEdit = false;
        Instance.Clear();
        $("h4").text("添加角色");
    });
    //选择用户
    Instance.Dom.BtnAddUser = $("#btn_AddUser");
    this.Dom.BtnAddUser.live("click", function () {        
        selectUserControl.Init();
        selectUserControl.Clear();
    })
    //保存用户
    Instance.Dom.BtnSaveUser = $("#btn_SaveUser");
   
    this.Dom.BtnSaveUser.live("click",function () {
        selectUserControl.SelectedUserDone(Instance.PickUserCallBack, "");
        $("#user_close").trigger("click");
    })
    //编辑权限按钮
    Instance.Dom.EditPowerLinkButton = $("a[id=editPowerLinkButton]");
    var editRolePowerControl = new EditRolePowerControl($("#editRolePowerControlMain"));
    Instance.Dom.EditPowerLinkButton.click(function () {
       
        editRolePowerControl.Init($(this).attr("rolesysno"), $(this).attr("rolename"));
        $("h4").text("编辑" + $(this).attr("rolename") + "的权限")
    });
    Instance.Dom.BtnSavePower = $("#btn_SavePower");
    this.Dom.BtnSavePower.live("click", function () {
        editRolePowerControl.Save();
        $("#power_close").trigger("click");
    })
    //保存用户所对应的角色
    this.SaveRoleUser = function () {
        var userSysNoString = Instance.GetUserSysNoStringFromUserSpan();
        var roleNameString = $("#roleNameTextBox").val();
        if ($.trim(roleNameString).length == 0 || userSysNoString.length == 0) {
            alert("角色名称不允许为空，或者必须包含一个用户");
            return false;
        }
        if (Instance.IsEdit) {

            var result = Instance.BackgroundInvoke.UpdateRole(roleNameString, userSysNoString, Instance.RoleSysNo).value;
            if (parseInt(result, 10)) {
                alert("编辑角色成功！");
                document.location.reload();
            } else {
                alert("编辑角色失败！");
            }
        } else {
            var result = Instance.BackgroundInvoke.InsertRole(roleNameString, userSysNoString).value;
            if (parseInt(result, 10)) {
                alert("添加角色成功！");
                document.location.reload();
            } else {
                alert("添加角色失败！");
            }
        }
    }



    this.PickUserCallBack = function (userCheckBox,img) {
        //获取Div下所有的用户Span
        var userSpans = $("span[id=userSpan]", $("#roleUserDiv"));       
        $.each(userCheckBox, function (index, checkbox) {
            var flag = true;
            //循环判断是否有重复用户的存在
            for (var i = 0; i < userSpans.length; i++) {

                if ($(userSpans[i]).attr("usersysno") == checkbox.value) {
                    flag = false;
                    break;
                }
            }
          
            //允许添加的场合
            if (flag == true) {
                var userSpan = $("<span style=\"margin-right:10px;\" id=\"userSpan\" usersysno=\"" + checkbox.value + "\">" + $(checkbox).attr("username") + "<img style=\"margin-left:5px;cursor:pointer;\" id=\"deleteUserlinkButton\" src=\"/Images/pro_icon_03.gif\"></span>");
                $("#roleUserDiv").append(userSpan);
            }
        });
    }

    $("img[id=deleteUserlinkButton]").live("click", function () {
        if (confirm("确认要删除这个用户吗？")) {
            //删除用户
            $(this).parent("span[id=userSpan]:first").remove();
        }
        return false;
    });

    //从包含用户信息的Span里拼接用户SysNo的String
    this.GetUserSysNoStringFromUserSpan = function () {
        //取得所有的用户Span
        var userSpan = $("span[id=userSpan]");
        var userSpanString = "";
        $.each(userSpan, function (index, item) {
            userSpanString += $(item).attr("usersysno") + ",";
        });
        userSpanString = userSpanString.substring(0, userSpanString.length - 1);
        return userSpanString;
    }

    ///清空弹出层已入力的文本框和一些其他信息
    this.Clear = function () {
        $("#roleUserDiv").html("");
        $("#roleNameTextBox").val("");
    }
}