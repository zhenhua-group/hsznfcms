﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/SystemSet/Unit_MemberHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '登录名', '姓名', '出生日期', '性别', '单位', '专业', '角色', '联系电话', '手机', '职位', '头像','','操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 40, align: 'center' },
                             { name: 'mem_ID', index: 'mem_ID', hidden: true, editable: true },
                             { name: 'ArchLevel_ID', index: 'ArchLevel_ID', hidden: true, editable: true },
                             { name: 'mem_Login', index: 'mem_Login', width: 100, align: 'center' },
                             { name: 'mem_Name', index: 'mem_Name', width: 100, align: 'center' },
                             { name: 'Birthday', index: 'Birthday', width: 100, align: 'center' },
                             { name: 'mem_Sex', index: 'mem_Sex', width: 40, align: 'center' },
                             { name: 'unit_Name', index: 'unit_Name', width: 100, align: 'center' },
                             { name: 'spe_Name', index: 'spe_Name', width: 100, align: 'center' },
                             { name: 'pri_Name', index: 'pri_Name', width: 100, align: 'center' },
                             { name: 'mem_Telephone', index: 'mem_Telephone', width: 100, align: 'center' },
                             { name: 'mem_Mobile', index: 'mem_Mobile', width: 100, align: 'center' },
                             { name: 'ArchLevel_Name', index: 'ArchLevel_Name', width: 100, align: 'center' },
                             { name: 'mem_img', index: 'mem_img', width: 50, align: 'center', formatter: colShowImgFormatter },
                             { name: 'mem_Mailbox', index: 'mem_Mailbox', hidden: true, editable: true },
                             { name: 'mem_ID', index: 'mem_ID', width: 40, align: 'center', formatter: colShowFormatter}
                             
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData:{"action":"member","strwhere": escape($("#hid_where").val())},
        loadonce: false,
        sortname: 'mem_id',
        sortorder: 'asc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/SystemSet/Unit_MemberHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                closeOnEscape: true,
                closeAfterDelete: true,
                reloadAfterSubmit: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {                        
                        $("#jqGrid").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid').jqGrid('getCell', sel_id[i], 'mem_ID') + ",";
                            }
                        }
                       // var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        //var value = $('#jqGrid').jqGrid('getCell', sel_id, 'mem_ID');
                        return values;
                    },
                    SysType:"member"
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#hid_where").val());
        var unit_id = $("#drp_unit3").val();
        var mem_name = $("#txt_keyname").val();

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/SystemSet/Unit_MemberHandler.ashx?action=member",
            postData: { 'strwhere': strwhere, "unit": unit_id, "keyname": mem_name },
            page: 1

        }).trigger("reloadGrid");
    });
});
//部门名称
function colShowImgFormatter(celvalue, options, rowData) {
    var imgname=celvalue;
    if (imgname == "") {
        imgname = "/Images/zw.jpg";
    }
    var imgstr = "<a href='" + imgname + "' target='_blank'><img src='" + imgname + "' onerror=\"this.src='/Images/zw.jpg'\" border=0 width=20 height=20></a>";
    return imgstr;
}


//编辑
function colShowFormatter(celvalue, options, rowData) {
  
    return '<a href="#" class="cls_select">编辑</a>';

}
//显示列表
//function colShowUnitFormatter(celvalue, options, rowdata) {
//    var str = "显示";
//    if ($.trim(celvalue) == "1") {
//        str="隐藏";
//    }
//    var pageurl = '<a href="###" class="cls_unitshow" alt="' + rowdata["unit_ID"] + '">' + str + '</a>';
//    return pageurl;
//}
//统计 
function completeMethod() {

    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    var summem = 0, sumpro = 0;
    //一种写法
    //             for(var i = 0, j = rowIds.length; i < j; i++) {   
    //             var curRowData = $("#jqGrid").jqGrid('getRowData', rowIds[i]);   
    //             var curChk = $("#"+rowIds[i]+"").find("td").eq(4).attr("a");             
    //          }   
    //二种写法
    $(rowIds).each(function () {
       // summem = summem + parseInt($("#" + this).find("td").eq(4).text());
       // sumpro = sumpro + parseInt($("#" + this).find("td").eq(6).text());
        $("#" + this).find("td").eq(1).text(this);
        //                var aa = $("#" + this).find("td").eq(5);
        //                var data = "action=fzr&unitid=" + aa.text();
        //                $.ajax({
        //                    type: "get",
        //                    dataType: "text",
        //                    url: "/HttpHandler/CommHandler.ashx",
        //                    data: data,
        //                    success: function (result) {
        //                        aa.text(result);
        //                    }
        //                });
    });

    // var sumAcount=parseFloat($("#jqGrid").getCol("memcount",false,'sum')).toFixed(4);
    //  var sumArea=parseFloat($("#jqGrid").getCol("projectcount",false,'sum')).toFixed(4);
  //  $("#jqGrid").footerData('set', { unit_Name: "合计:", memcount: summem, projectcount: sumpro }, false);


}
//无数据
function loadCompMethod() {   
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        $("#nodata").show();
    }
    else {
        $("#nodata").hide();
    }
}