﻿//选择用户类
function ChooseProjectValueUserControl(container, callBack) {
    this.Pager = {
        "PageCurrent": 1,
        "PageSize": 10,
        "TotalCount": 0,
        "PageCount": 0
    };
    this.Dom = {
        "NameTextBox": $("#chooseUserOfNameTextBox", container).val(),
        "DepartmentDropDownList": $("#chooseUserDepartmentDropDownList :selected", container).val(),
        "chooseSpeName": $("#chooseSpeName", container).val()
    };
    this.CallBack = callBack;
    this.ResultTable = {};
    var Instance = this;

    //清除所有的CheckBox
    this.Clear = function () {
        $(":checkbox", container).removeAttr("checked");
        $(":checkbox", container).parent().removeAttr("class");
        $("#chooseUserOfNameTextBox").val("");
        $("#chkDesign", container).parent().attr("class", "checked");
        $("#chkDesign").attr("checked", "checked");
    }

    this.BindData = function (dataObj) {


        var unitString = dataObj.UnitName;
        $.each($("#chooseUserDepartmentDropDownList option"), function (index, option) {
            if ($.trim($(option).text()) == unitString) {
                $(option).attr("selected", "selected");
            }
        });

        if (dataObj.UnitName != "") {
            $("#chooseUserDepartmentDropDownList").attr("disabled", "disabled");
        }
        var specString = dataObj.SpeName;
        $.each($("#chooseSpeName option"), function (index, option) {
            if ($.trim($(option).text()) == specString) {
                $(option).attr("selected", "selected");
            }
        });
        if (dataObj.SpeName != "") {
            $("#chooseSpeName").attr("disabled", "disabled");
        }

        if (unitString == "经济所") {
            $("#tbSelect tr:eq(1) td:eq(2)").text("");
            $("#tbSelect tr:eq(1) td:eq(3)").text("");
        }
        Instance.Pager.PageCurrent = 1;
        var dataObj = {
            "NameOrLoginName": $("#chooseUserOfNameTextBox", container).val(),
            "DepartmentName": $("#chooseUserDepartmentDropDownList :selected", container).val(),
            "SpeName": $("#chooseSpeName :selected", container).val(),
            "PageSize": Instance.Pager.PageSize,
            "PageCurrent": Instance.Pager.PageCurrent
        };

        var reuslt = Instance.BackgroundInvoke.GetUsers(Global.toJSON(dataObj)).value;

        Instance.CreateTable(reuslt);
    }

    this.SaveUser = function (callBack) {
        //获取所有选中的人物   
        var alluserlist = $(":checked[name=userCheckBox]");

        var array = new Array();
        $.each(alluserlist, function (index, item) {
            array[index] = {
                "userSysNo": $(item).attr("userSysNo"),
                "userName": $(item).attr("userName"),
                "userSpecialtyname": $(item).attr("userspecialtyname"),
                "userPrincipalship": $(item).attr("userPrincipalship"),
                "checkDesign": $("#chkDesign").attr("checked") ? "1" : "0",
                "checkHead": $("#chkHead").attr("checked") ? "1" : "0",
                "checkAudit": $("#chkAudit").attr("checked") ? "1" : "0",
                "checkProof": $("#chkProof").attr("checked") ? "1" : "0"
            };
        });

        callBack(array, 0);
    }

    this.BackgroundInvoke = TG.Web.UserControl.ChooseProjectValueUser;

    //查询按钮点击事件
    this.SearchButton = $("#chooseUserSearchButton", container);
    this.SearchButton.click(function () {
        Instance.Pager.PageCurrent = 1;
        var dataObj = {
            "NameOrLoginName": $("#chooseUserOfNameTextBox", container).val(),
            "DepartmentName": $("#chooseUserDepartmentDropDownList :selected", container).val(),
            "SpeName": $("#chooseSpeName :selected", container).val(),
            "PageSize": Instance.Pager.PageSize,
            "PageCurrent": Instance.Pager.PageCurrent
        };

        var reuslt = Instance.BackgroundInvoke.GetUsers(Global.toJSON(dataObj)).value;

        Instance.CreateTable(reuslt);

        //全选
        $("#selectAllCheckBox", container).click(function () {
            var isChecked = $(this).attr("checked");
            if (isChecked == undefined) {
                $("input[name=userCheckBox]").removeAttr("checked");
                // $(":checkbox", $("#chooseUserResultTable")).removeAttr("checked");
            } else {
              
                $("input[name=userCheckBox]").attr("checked", "checked");
                //$(":checkbox", $("#chooseUserResultTable")).attr("checked", "checked");
            }

        });
    });

    //创建表格
    this.CreateTable = function (userListString) {
        var userList = Global.evalJSON(userListString);

        Instance.ResultTable.Clear();

        $.each(userList[1], function (index, user) {
            if (user.PhoneNumber == null)
                user.PhoneNumber = "";
            var resultAttribute = Global.toJSON(user);
            var trString = "<tr>";
            trString += "<td align=\"center\"><input type=\"checkbox\" userSysNo=" + user.UserSysNo + " name=\"userCheckBox\" userName=" + user.Name + " userSpecialtyName=\"" + user.SpeName + "\" userPrincipalship=\"" + user.Principalship + "\"></td>";
            trString += "<td align=\"center\">" + user.Name + "</td>";
            trString += "<td align=\"center\">" + user.DepartmentName + "</td>";
            trString += "<td align=\"center\">" + user.SpeName + "</td>";
            trString += "</tr>";

            $("#chooseUserResultTable", container).append(trString);
        });

        var pageCount = 0;
        if (userList[0] <= Instance.Pager.PageSize) {
            pageCount = 1;
        } else if (userList[0] % Instance.Pager.PageSize == 0) {
            pageCount = Math.floor(userList[0] / Instance.Pager.PageSize);
        } else {
            pageCount = Math.floor(userList[0] / Instance.Pager.PageSize) + 1;
        }

        $("#pager", container).pager({ "pagenumber": Instance.Pager.PageCurrent, "pagecount": pageCount, "pagesize": Instance.Pager.PageSize, "datacount": userList[0], "buttonClickCallback": Instance.PagerCallBack });

        CommonControl.SetTableStyle("chooseUserResultTable", "need");
    }

    this.ResultTable.Clear = function () {
        $("#chooseUserResultTable tr:gt(0)", container).remove();
    }

    this.PagerCallBack = function (clickNumber) {
        Instance.Pager.PageCurrent = clickNumber;

        var dataObj = {
            "NameOrLoginName": $("#chooseUserOfNameTextBox", container).val(),
            "DepartmentName": $("#chooseUserDepartmentDropDownList :selected", container).val(),
            "SpeName": $("#chooseSpeName :selected", container).val(),
            "PageSize": Instance.Pager.PageSize,
            "PageCurrent": Instance.Pager.PageCurrent
        };

        var reuslt = Instance.BackgroundInvoke.GetUsers(Global.toJSON(dataObj)).value;

        Instance.CreateTable(reuslt);
    }


    Instance.SearchButton.click();
}