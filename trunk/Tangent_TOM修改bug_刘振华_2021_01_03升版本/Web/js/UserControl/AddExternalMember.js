﻿//选择用户类
function ChooseExternalUserControl(container, callBack) {
    this.Pager = {
        "PageCurrent": 1,
        "PageSize": 10,
        "TotalCount": 0,
        "PageCount": 0
    };
    this.Dom = {
        "NameTextBox": $("#chooseUserOfNameTextBox", container).val(),
        "DepartmentDropDownList": $("#chooseWpUserDepartmentDropDownList :selected", container).val(),
        "chooseWpSpeName": $("#chooseWpSpeName", container).val()
    };
    this.CallBack = callBack;
    this.ResultTable = {};
    var Instance = this;

    //清除所有的CheckBox
    this.Clear = function () {
        $(":checkbox", container).removeAttr("checked");
        $("#chooseUserOfNameTextBox").val("");
        $("#chkWpDesign").attr("checked", "checked");
    }


    this.BindData = function (dataObj) {

        var unitString = dataObj.UnitName;
        $.each($("#chooseWpUserDepartmentDropDownList option"), function (index, option) {
            if ($.trim($(option).text()) == unitString) {
                $(option).attr("selected", "selected");
            }
        });

        if (dataObj.UnitName != "") {
            $("#chooseWpUserDepartmentDropDownList").attr("disabled", "disabled");
        }

        var specString = dataObj.SpeName;
        $.each($("#chooseWpSpeName option"), function (index, option) {
            if ($.trim($(option).text()) == specString) {
                $(option).attr("selected", "selected");
            }
        });

        if (dataObj.SpeName != "") {
            $("#chooseWpSpeName").attr("disabled", "disabled");

        }
        $.each($("#selectUnit option"), function (index, option) {
            if ($.trim($(option).text()) == unitString) {
                $(option).attr("selected", "selected");
            }
        });

        if (dataObj.UnitName != "") {
            $("#selectUnit").attr("disabled", "disabled");
        }

        $.each($("#selectSpe option"), function (index, option) {
            if ($.trim($(option).text()) == specString) {
                $(option).attr("selected", "selected");
            }
        });

        if (dataObj.SpeName != "") {
            $("#selectSpe").attr("disabled", "disabled");
        }
        if (unitString == "经济所") {
            $("#tbSelect tr:eq(1) td:eq(2)").text("");
            $("#tbSelect tr:eq(1) td:eq(3)").text("");
        }
        Instance.Pager.PageCurrent = 1;
        var dataObj = {
            "NameOrLoginName": $("#chooseUserOfNameTextBox", container).val(),
            "DepartmentName": $("#chooseWpUserDepartmentDropDownList :selected", container).val(),
            "SpeName": $("#chooseWpSpeName :selected", container).val(),
            "PageSize": Instance.Pager.PageSize,
            "PageCurrent": Instance.Pager.PageCurrent
        };

        var reuslt = Instance.BackgroundInvoke.GetUsers(Global.toJSON(dataObj)).value;

        Instance.CreateTable(reuslt);
    }

    this.SaveUser = function (callBack) {
        //获取所有选中的人物   
        var alluserlist = $(":checked[name=userExternalCheckBox]");

        var array = new Array();
        $.each(alluserlist, function (index, item) {
            array[index] = {
                "userSysNo": $(item).attr("userSysNo"),
                "userName": $(item).attr("userName"),
                "userSpecialtyname": $(item).attr("userspecialtyname"),
                "userPrincipalship": $(item).attr("userPrincipalship"),
                "checkDesign": $("#chkWpDesign").attr("checked") ? "1" : 0,
                "checkHead": $("#chkWpHead").attr("checked") ? "1" : 0,
                "checkAudit": $("#chkWpAudit").attr("checked") ? "1" : 0,
                "checkProof": $("#chkWpProof").attr("checked") ? "1" : 0
            };
        });

        callBack(array, 1);
    }

    this.BackgroundInvoke = TG.Web.UserControl.AddExternalMember;


    //新增按钮点击事件
    this.AddButton = $("#btnAdd", container);
    this.AddButton.click(function () {
        var allowSubmit = Instance.Validation();
        if (allowSubmit) {
            var dataObj = {
                "MemName": $("#txtAddUser", container).val(),
                "memSpecialityID": $("#selectSpe :selected", container).val(),
                "memUnitID": $("#selectUnit :selected", container).val()
            };

            var reuslt = Instance.BackgroundInvoke.AddExternalUser(Global.toJSON(dataObj)).value;

            if (reuslt == 1) {
                alert("" + $("#txtAddUser", container).val() + "人员，本部门已存在");
            } else if (reuslt == 2) {
                alert("" + $("#txtAddUser", container).val() + "人员，添加成功");
                $("#txtAddUser").val("");
            }

            $("#tbAddExternal", container).show();
            $("#fidAddExternal", container).hide();
            $("#fidSelectExternal", container).show();

            Instance.SearchButton.click();

            //全选
            $("#selectExternalAllCheckBox", container).click(function () {
                var isChecked = $(this).attr("checked");
                if (isChecked == undefined) {
                    $(":checkbox", $("#chooseExternalUserResultTable")).removeAttr("checked");
                } else {
                    $(":checkbox", $("#chooseExternalUserResultTable")).attr("checked", "checked");
                }

            });
        }
    });

    this.AddExternal = $("#addExternal", container);
    this.AddExternal.click(function () {

        $("#tbAddExternal", container).hide();
        $("#fidAddExternal", container).show();
        $("#fidSelectExternal", container).hide();
    });

    this.CancelExternal = $("#btnCancel", container);
    this.CancelExternal.click(function () {

        $("#tbAddExternal", container).show();
        $("#fidAddExternal", container).hide();
        $("#fidSelectExternal", container).show();
    });

    //查询按钮点击事件
    this.SearchButton = $("#chooseUserSearchButton", container);
    this.SearchButton.click(function () {
        Instance.Pager.PageCurrent = 1;
        var dataObj = {
            "NameOrLoginName": $("#chooseUserOfNameTextBox", container).val(),
            "DepartmentName": $("#chooseWpUserDepartmentDropDownList :selected", container).val(),
            "SpeName": $("#chooseWpSpeName :selected", container).val(),
            "PageSize": Instance.Pager.PageSize,
            "PageCurrent": Instance.Pager.PageCurrent
        };

        var reuslt = Instance.BackgroundInvoke.GetUsers(Global.toJSON(dataObj)).value;

        Instance.CreateTable(reuslt);

        //全选
        $("#selectExternalAllCheckBox", container).click(function () {
            var isChecked = $(this).attr("checked");
            if (isChecked == undefined) {
                $(":checkbox", $("#chooseExternalUserResultTable")).removeAttr("checked");
            } else {
                $(":checkbox", $("#chooseExternalUserResultTable")).attr("checked", "checked");
            }

        });
    });

    //创建表格
    this.CreateTable = function (userListString) {
        var userList = Global.evalJSON(userListString);

        Instance.ResultTable.Clear();

        if (userList[1] == null || userList[1] == "") {
            var trHtml = "<tr><td colspan=\"4\" style='color:Red; text-align:center;'>无数据,可选择添加按钮添加数据。</td></tr>";
            $("#chooseExternalUserResultTable", container).append(trHtml);
        }
        else {
            $.each(userList[1], function (index, user) {
                if (user.PhoneNumber == null)
                    user.PhoneNumber = "";
                var resultAttribute = Global.toJSON(user);
                var trString = "<tr>";
                trString += "<td align=\"center\"><input type=\"checkbox\" userSysNo=" + user.UserSysNo + " name=\"userExternalCheckBox\" userName=" + user.Name + " userSpecialtyName=\"" + user.SpeName + "\" userPrincipalship=\"" + user.Principalship + "\"></td>";
                trString += "<td align=\"center\">" + user.Name + "</td>";
                trString += "<td align=\"center\">" + user.DepartmentName + "</td>";
                trString += "<td align=\"center\">" + user.SpeName + "</td>";
                trString += "</tr>";

                $("#chooseExternalUserResultTable", container).append(trString);
            });
        }
        var pageCount = 0;
        if (userList[0] <= Instance.Pager.PageSize) {
            pageCount = 1;
        } else if (userList[0] % Instance.Pager.PageSize == 0) {
            pageCount = Math.floor(userList[0] / Instance.Pager.PageSize);
        } else {
            pageCount = Math.floor(userList[0] / Instance.Pager.PageSize) + 1;
        }

        $("#pager", container).pager({ "pagenumber": Instance.Pager.PageCurrent, "pagecount": pageCount, "pagesize": Instance.Pager.PageSize, "datacount": userList[0], "buttonClickCallback": Instance.PagerCallBack });

        CommonControl.SetTableStyle("chooseExternalUserResultTable", "need");
    }

    this.ResultTable.Clear = function () {
        $("#chooseExternalUserResultTable tr:gt(0)", container).remove();
    }

    this.PagerCallBack = function (clickNumber) {
        Instance.Pager.PageCurrent = clickNumber;

        var dataObj = {
            "NameOrLoginName": $("#chooseUserOfNameTextBox", container).val(),
            "DepartmentName": $("#chooseWpUserDepartmentDropDownList :selected", container).val(),
            "SpeName": $("#chooseWpSpeName :selected", container).val(),
            "PageSize": Instance.Pager.PageSize,
            "PageCurrent": Instance.Pager.PageCurrent
        };

        var reuslt = Instance.BackgroundInvoke.GetUsers(Global.toJSON(dataObj)).value;

        Instance.CreateTable(reuslt);
    }



    //验证
    this.Validation = function () {
        if ($("#txtAddUser", container).val().length == 0) {
            alert("请添加人员姓名！");
            return false;
        }
        if ($("#selectUnit :selected", container).val() == "0") {
            alert("请选择单位！");
            return false;
        }
        if ($("#selectSpe :selected", container).val() == "0") {
            alert("请选择专业！");
            return false;
        }

        return true;
    }
    Instance.SearchButton.click();
}