﻿function EditRolePowerControl(container) {
    this.Container = container;
    this.RoleSysNo = $("#editRolePowerControl_RoleSysNo", container).val();
    this.RoleName = $("#editRolePowerControl_RoleName", container).val();
    this.RoleTitle = $("#editRolePowerControl_Title", container);
    this.BackgroundInvoke = TG.Web.UserControl.EditRolePowerControl;
    var Instance = this;

    //初始化内容
    this.Init = function (roleSysNo, roleName) {
       
        Instance.RoleSysNo = roleSysNo;
        Instance.RoleName = roleName;
        Instance.RoleTitle.text(Instance.RoleName);

        var resultList = Instance.BackgroundInvoke.BindEditRolePowerControl(roleSysNo).value;

        Instance.BindRepeaterEditRolePower(resultList);
    }

    //保存数据
    this.Save = function () {
        var entityArray = Instance.PickDataFromTable();

        var jsonString = Global.toJSON(entityArray);

        var result = Instance.BackgroundInvoke.SaveRolePowerEntityList(jsonString).value;

        if (parseInt(result) > 0) {
            alert("角色权限修改成功！");

        } else {
            alert("角色权限修改失败！");
        }
    }

    //从表格内取得数据
    this.PickDataFromTable = function () {
        var trArray = $("#editRolePowerControl tr");
        var entityArray = new Array();
        $.each(trArray, function (index, tr) {
            var td = $(tr).children("td:eq(2)");
            var container = $(tr);
            var myDate = new Date();
            var obj =
                {
                    "SysNo": td.attr("rolepowersysno"),
                    "RoleSysNo": Instance.RoleSysNo,
                    "ActionPowerSysNo": td.attr("actionpowersysno"),
                    "InUser": 0,
                    "InDate": myDate.getYear() + '-' + (myDate.getMonth() + 1) + '-' + myDate.getDate()
                };
            //权限控制
            var powerString = "";
            //查看权限
            if ($("#previewOwnRadio:checked", container).length > 0) {
                powerString += "0"; //个人
            }
            else if ($("#previewAllRadio:checked", container).length > 0) {
                powerString += "1"; //全部
            }
            else {
                powerString += "2"; //部门
            }

            //编辑权限
            if ($("#editPowerCheckBox:checked", container).length > 0)
                powerString += ",1";
            else
                powerString += ",0";

            //删除权限
            if ($("#deletePowerCheckBox:checked", container).length > 0)
                powerString += ",1";
            else
                powerString += ",0";

            obj.Power = powerString;
            entityArray.push(obj);
        });
        return entityArray;
    }

    //添加内容到表格
    this.BindRepeaterEditRolePower = function (resultString) {
        $("#editRolePowerControl").html("");
        if (resultString != null && resultString.length > 0) {
            var rolePowerList = Global.evalJSON(resultString);
            $.each(rolePowerList, function (index, rolePowerEntity) {
                var editPower = "", deletePower = "", previewOwnPower = "", previewAllPower = "", previewUnitPower = "";
                if (rolePowerEntity.Power != null && rolePowerEntity.Power.length > 0) {
                    var powerArray = rolePowerEntity.Power.split(",");
                    if (powerArray[0] == "0") {
                        previewOwnPower = "checked=\"checked\"";
                    }
                    else if (powerArray[0] == "1") {
                        previewAllPower = "checked=\"checked\"";
                    }
                    else {
                        previewUnitPower = "checked=\"checked\"";
                    }

                    if (powerArray[1] == "1")
                        editPower = "checked=\"checked\"";
                    if (powerArray[2] == "1")
                        deletePower = "checked=\"checked\"";
                }
                var trString = "";
                trString += "<tr>";
                trString += "<td style=\"width: 20%;\">" + rolePowerEntity.Description + "</td>";
                trString += "<td style=\"width: 35%;\">" + rolePowerEntity.PageName + "</td>";
                trString += "<td style=\"width: 25%;\" actionpowersysno=\"" + rolePowerEntity.ActionPowerSysNo + "\" rolepowersysno=\"" + rolePowerEntity.RolePowerSysNo + "\">";
                trString += "<input type=\"radio\" id=\"previewOwnRadio\" name=\"previewPower_" + rolePowerEntity.Description + "\" " + previewOwnPower + " />个人";
                trString += "<input type=\"radio\" id=\"previewUnitRadio\" name=\"previewPower_" + rolePowerEntity.Description + "\" " + previewUnitPower + " />部门";
                trString += "<input type=\"radio\" id=\"previewAllRadio\" name=\"previewPower_" + rolePowerEntity.Description + "\" " + previewAllPower + " />全部";
                trString += "</td><td style=\"width: 20%;\">"
                trString += "<input type=\"checkbox\" id=\"editPowerCheckBox\" " + editPower + "/>编辑";
                trString += "<input type=\"checkbox\" id=\"deletePowerCheckBox\" " + deletePower + "/>删除";
                trString += "</td>";
                trString += "</tr>";

                $("#editRolePowerControl").append(trString);
            });
        }
    }
}