﻿//选择用户类
function ChooseUserControl(container, callBack) {
    this.Pager = {
        "PageCurrent": 1,
        "PageSize": 10,
        "TotalCount": 0,
        "PageCount": 0
    };
    this.Dom = {
        "NameTextBox": $("#chooseUserOfNameTextBox", container).val(),
        "DepartmentDropDownList": $("#chooseUserDepartmentDropDownList :selected", container).val(),
        "chooseUserPhoneTextBox": $("#chooseUserPhoneTextBox", container).val()
    };
    this.CallBack = callBack;
    this.ResultTable = {};
    var Instance = this;

    this.BackgroundInvoke = TG.Web.UserControl.ChooseUser;

    //查询按钮点击事件
    this.SearchButton = $("#chooseUserSearchButton", container);
    this.SearchButton.click(function () {
        Instance.Pager.PageCurrent = 1;
        var dataObj = {
            "NameOrLoginName": $("#chooseUserOfNameTextBox", container).val(),
            "DepartmentSysNo": $("#chooseUserDepartmentDropDownList :selected", container).val(),
            "PhoneNumber": $("#chooseUserPhoneTextBox", container).val(),
            "PageSize": Instance.Pager.PageSize,
            "PageCurrent": Instance.Pager.PageCurrent
        };

        var reuslt = Instance.BackgroundInvoke.GetUsers(Global.toJSON(dataObj)).value;

        Instance.CreateTable(reuslt);
    });

    //创建表格
    this.CreateTable = function (userListString) {
        var userList = Global.evalJSON(userListString);

        Instance.ResultTable.Clear();

        $.each(userList[1], function (index, user) {
            if (user.PhoneNumber == null)
                user.PhoneNumber = "";
            var resultAttribute = Global.toJSON(user);
            var trString = "<tr usersysno=\"" + user.UserSysNo + "\"  username=\"" + user.Name + "\" phonenumber=\"" + user.PhoneNumber + "\">";
            trString += "<td align=\"center\">" + user.Name + "</td>";
            trString += "<td align=\"center\">" + user.DepartmentName + "</td>";
            trString += "<td align=\"center\">" + user.PhoneNumber + "</td>";
            trString += "<td align=\"center\"><a href=\"#\" id=\"chooseUserLink\">选择</a></td>";
            trString += "</tr>";

            $("#chooseUserResultTable", container).append(trString);
        });

        var pageCount = 0;
        if (userList[0] % Instance.Pager.PageSize == 0) {
            pageCount = 1;
        } else {
            pageCount = Math.floor(userList[0] / Instance.Pager.PageSize) + 1;
        }

        $("#pager", container).pager({ "pagenumber": Instance.Pager.PageCurrent, "pagecount": pageCount, "pagesize": Instance.Pager.PageSize, "datacount": userList[0], "buttonClickCallback": Instance.PagerCallBack });

        CommonControl.SetTableStyle("chooseUserResultTable", "need");
    }

    this.ResultTable.Clear = function () {
        $("#chooseUserResultTable tr:gt(0)", container).remove();
    }

    this.PagerCallBack = function (clickNumber) {
        Instance.Pager.PageCurrent = clickNumber;

        var dataObj = {
            "NameOrLoginName": $("#chooseUserOfNameTextBox", container).val(),
            "DepartmentSysNo": $("#chooseUserDepartmentDropDownList :selected", container).val(),
            "PhoneNumber": $("#chooseUserPhoneTextBox", container).val(),
            "PageSize": Instance.Pager.PageSize,
            "PageCurrent": Instance.Pager.PageCurrent
        };

        var reuslt = Instance.BackgroundInvoke.GetUsers(Global.toJSON(dataObj)).value;

        Instance.CreateTable(reuslt);
    }

    $("a[id=chooseUserLink]", container).live("click", function () {
        var tr = $(this).parents("tr:first");

        Instance.CallBack({ "usersysno": tr.attr("usersysno"), "username": tr.attr("username"), "phonenumber": tr.attr("phonenumber") });
        //        $("#chooseUserMain").dialog("close");
        $("#btn_user_close").trigger('click');
        return false;
    });

    Instance.SearchButton.click();
}