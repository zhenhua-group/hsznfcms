﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Coperation/CoperationListHandler.ashx?strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '合同名称', '客户名称', '合同金额(万元)', '承接部门', '签订日期', '', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 40, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 250, formatter: colShowImgFormatter },
                             { name: 'cst_name', index: 'cst_name', width: 250, formatter: colNameImgFormatter },
                             { name: 'cpr_Acount', index: 'cpr_Acount', width: 100, align: 'center' },
                             { name: 'cpr_Unit', index: 'cpr_Unit', width: 100, align: 'center' },
                             { name: 'qdrq', index: 'qdrq', width: 100, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', editable: false,sorttable: false, formatter: colShowFormatter },
                             { name: 'cst_Id', index: 'cst_Id', width: 30, align: 'center', editable: false,sorttable: false, formatter: colEditFormatter }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/CoperationListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/CoperationListHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere },
            page: 1

        }).trigger("reloadGrid");
    });
});

//名称连接
function colShowImgFormatter(celvalue, options, rowData) {

    return '<img style="width: 16px; height: 16px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none;" src="../Images/buttons/icon_cpr.png" />' + celvalue;

}
//名称连接
function colNameImgFormatter(celvalue, options, rowData) {

    return '<img style="width: 16px; height: 16px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none;" src="../Images/buttons/icon_cst.png" />' + celvalue;

}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "cpr_ShowCoprationBymaster.aspx?flag=cprlist&cprid=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看合同"><img style="width: 16px; height: 16px; border: none;" src="../images/coperation.png" /></a>';

}
//编辑
function colEditFormatter(celvalue, options, rowdata) {
    if (celvalue != "") {
        var pageurl = "../Customer/cst_ShowCustomerInfoByMaster.aspx?Cst_Id=" + celvalue;
        return '<a href="' + pageurl + '" alt="查看客户" class="allow"><img style="width: 16px; height: 16px; border: none;" src="../images/customer.png" /></a>';
    }
}
//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {
    $("#jqGrid").jqGrid("setGridParam", {}).hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        $("#nodata").show();
    }
    else {
        $("#nodata").hide();
    }
}