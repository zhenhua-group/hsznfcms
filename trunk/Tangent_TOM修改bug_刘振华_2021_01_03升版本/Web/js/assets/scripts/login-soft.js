﻿var Login = function () {

    var handleLogin = function () {
        //登录验证
        $('.login-form').validate({
            errorElement: 'span',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },

            invalidHandler: function (event, validator) {
                $('.alert-danger', $('.login-form')).show();
            },

            highlight: function (element) {
                $(element)
	                    .closest('.form-group').addClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            }
        });
        //回车登录
        $('.login-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    return userLogin();
                }
            }
        });
        //登录按钮
        $("#btn_login").click(function () {
            if ($('.login-form').validate().form()) {
                return userLogin();
            }
        });

    }

    var handleForgetPassword = function () {
        $('.forget-form').validate({
            errorElement: 'span',
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                username2: {
                    required: true
                }
            },

            invalidHandler: function (event, validator) {

            },

            highlight: function (element) {
                $(element)
	                    .closest('.form-group').addClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            }
        });

        $('.forget-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    return rePassWord();
                }
                return false;
            }
        });

        jQuery('#forget-password').click(function () {
            jQuery('.login-form').hide();
            jQuery('.forget-form').show();
        });

        jQuery('#back-btn').click(function () {
            jQuery('.login-form').show();
            jQuery('.forget-form').hide();
        });

        //找回密码
        $("#btn_repwd").click(function () {
            if ($('.forget-form').validate().form()) {
                return rePassWord();
            }
            return false;
        });
    }
    //找回密码
    var rePassWord = function () {
        var url = "HttpHandler/LoginHttpHandler.ashx";
        var postdata = { "action": "repwd", "username": escape($("#username2").val()), "n": Math.random() * 12345 };
        $.post(url, postdata, function (data) {
            if (data != null) {
                if (data.result == "0") {
                    toastr.warning(data.msg);
                }
                else {
                    toastr.danger(data.msg);
                }
            }
        });
    }
    //登录
    var userLogin = function () {
        //登录地址
        var url = "HttpHandler/LoginHttpHandler.ashx";
        var postdata = { "action": "login", "username": escape($("#username").val()), "userpwd": escape($("#userpwd").val()), "n": Math.random() * 12345 };
        //post
        $.post(url, postdata, function (data) {
            if (data != null) {
                //success
                if (data.result == "-2") {
                    alert("网络锁登陆失败，请联系管理员！");
                    return false;
                }
                else {
                    if (data.result == "0") {
                        //登录成功,判断是否记住密码
                        if ($("#chk_remember").attr("checked")) {
                            $.cookie("_username", escape($("#username").val()), { expires: 14 });
                            $.cookie("_password", escape($("#userpwd").val()), { expires: 14 });
                        }
                        else {
                            //清空
                            $.cookie("_username", null);
                            $.cookie("_password", null);
                        }
                        //进入主界面
                        window.location.href = "/mainpage/WelcomePage.aspx";
                    }
                    else {
                        toastr.warning("用户名或密码错误！");
                        return false;
                    }
                }
            }
            else {
                toastr.warning("用户名或密码错误！");
                return false;
            }
        }, 'json');
    }
    //Cookie 判断
    var loadUserCookie = function () {
        //判断用户Cookie
        var cookie_uid = $.cookie("_username");
        var cookie_pwd = $.cookie("_password");
        //如果已经记住密码
        if (cookie_uid != 'null' && cookie_uid != 'undefined') {
            //用户名
            $("#username").val(cookie_uid);
            //密码
            $("#userpwd").val(cookie_pwd);
            //记住密码
            $("#chk_remember").attr("checked", true);
            //选中样式
            $("#chk_remember").parent("span").attr("class", "checked");
        }
    }
    return {
        //main function to initiate the module
        init: function () {

            handleLogin();
            handleForgetPassword();
            loadUserCookie();

            $.backstretch([
		        "/js/assets/img/bg/1.jpg",
		        "/js/assets/img/bg/2.jpg",
		        "/js/assets/img/bg/3.jpg",
		        "/js/assets/img/bg/4.jpg"
            ], {
                fade: 1000,
                duration: 8000
            });
        }

    };

}();