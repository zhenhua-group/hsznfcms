﻿var Lock = function () {
    //页面级
    var loadPage = function () {
        //登录
        $("#btn_login").click(function () {
            userLogin();
        });

        //        document.onkeydown = function (evt) {
        //            var evt = window.event ? window.event : evt;
        //            if (evt.keyCode == 13) {

        //                userLogin();

        //            }
        //        }
    
        document.onkeypress = function (e) {
            var code;
            if (!e) {
                var e = window.event;
            }
            if (e.keyCode) {
                code = e.keyCode;
            }
            else if (e.which) {
                code = e.which;
            }
            if (code == 13) {
                userLogin();
            }
        }
    }


    //登录
    var userLogin = function () {

        //login entity
        var url = "HttpHandler/LoginHttpHandler.ashx";
        var userlogin = $.trim($("#username").text());

        var postdata = { "action": "login", "username": $.trim(userlogin), "userpwd": $.trim($("#userpwd").val()), "n": Math.random() * 10000 };

        if ($.trim($("#userpwd").val()) == "") {
            toastr.warning("请输入密码！");
            return false;
        }
        //post
        $.post(url, postdata, function (data) {
            if (data != null) {
                //success
                if (data.result == "0") {
                    var url = $("#hid_url").val();
                    //进入主界面
                    window.location.href = "/mainpage/WelcomePage.aspx";
                }
                else {
                    toastr.warning("密码错误！");
                }
            }
            else {
                toastr.warning("密码错误！");
            }
        }, 'json');

        return false;
    }


    return {
        //main function to initiate the module
        init: function () {
            loadPage();
            $.backstretch([
    		        "/js/assets/img/bg/1.jpg",
    		        "/js/assets/img/bg/2.jpg",
    		        "/js/assets/img/bg/3.jpg",
    		        "/js/assets/img/bg/4.jpg"
    		        ], {
    		            fade: 1000,
    		            duration: 8000
    		        });
        }

    };

} ();

