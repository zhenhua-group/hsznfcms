﻿$(document).ready(function() {
    //客户信息
    $("#link_cst").click(function() {
        $("#rightFrame", window.parent.document).attr("src", "../Customer/cst_CustomerInfoList.aspx");
    });
    //客户录入
    $("#link_add").click(function() {
        $("#rightFrame", window.parent.document).attr("src", "../Customer/cst_AddCustomerInfo.aspx");
    });
    //联系人
    $("#link_hum").click(function() {
        $("#rightFrame", window.parent.document).attr("src", "../Customer/ContactPersionInfo/cp_ContactPersionInfo.aspx");
    });
    //文档
    $("#link_doc").click(function() {
        $("#rightFrame", window.parent.document).attr("src", "../Customer/AttachInfo/at_AttachInfoList.aspx");
    });
    //查询
    $("#link_search").click(function() {
        $("#rightFrame", window.parent.document).attr("src", "../Customer/cst_SearchCustomerInfo.aspx");
    });

});