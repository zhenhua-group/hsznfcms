﻿var sendMessageClassedit;  //发送消息共通方法
var messageDialog; //消息框
$(document).ready(function () {

    if ($("#HiddenAuditStatus").val() == "G" || $("#HiddenAuditStatus").val() == "C" || $("#HiddenAuditStatus").val() == "E") {
        $("#reportDiv").show();
    }
    $("#Button1").click(function () {
        window.history.back();
    });


    $("input:checkbox", $("#chooseUserResultTable")).attr("disabled", "disabled");
    $("#selectAllCheckBox").attr("disabled", "disabled");
    this.Dom = {};
    this.BackgroundInvoke = TG.Web.ProjectPlan.ProPlanAuditBymaster;
    this.JsonDataEntity = "";
    var Instance = this;
    this.Status = $("#HiddenAuditStatus").val();
    this.ProjectSysNo = $("#ProjectSysNo").val();
    this.CoperationName = $("#HiddenCoperationName").val();
    this.ProjectPlanAuditSysNo = $("#HiddenProjectPlanAuditSysNo").val();

    if (this.Status == "F") {
        $("#Report").css("display", "block");
        //审核通过
        this.Dom.btn_appeditcpr = $("#btn_appeditcpr");
        this.Dom.btn_appeditcpr.live("click", function () {
            $("#OptionDiv").css("display", "block");
            $("#opinion").css("color", "gray").val("请填写修改原因....");
            $("#opinion").live("focus", function () {
                if ($(this).attr("option") == "a") {
                    $(this).css("color", "black").val("");
                }
                else {

                }
            });
            $("#opinion").live("focusout", function () {
                if ($(this).val().trim().length < 1) {
                    $(this).css("color", "gray").val("请填写修改原因....");
                }
            });
            $("#opinion").live("change", function () {
                if ($(this).val().trim().length < 1) {
                    $(this).attr("option", "a");
                } else {
                    $(this).attr("option", "b");
                }
            })
        });
    }
    else {
        $("#Report").hide();
    }
    //发起修改

    this.Dom.btnsub = $("#btnsub");
    this.Dom.btnsub.live("focus", function () {
        var copoption = $("#opinion").val();
        if ($("#opinion").attr("option") == "a") {
            copoption = "";
        }

        if (copoption.length < 1) {
            alert("请填写意见");
            return false;
        }
    })
    this.Dom.btnsub.live("click", function () {
        var copoption = $("#opinion").val();
        if ($("#opinion").attr("option") == "a") {
            copoption = "";
        }

        if (copoption.length < 1) {
            alert("请填写意见");
            return false;
        }

        //弹出审批ren
        var dataObj = {
            ProjectSysNo: Instance.ProjectSysNo,
            CoperationName: Instance.CoperationName,
            copoption: copoption
        };

        var jsonData = Global.toJSON(dataObj);

        Instance.JsonDataEntity = jsonData;

        getUserAndUpdateRecordEdit('0', '0', Instance.JsonDataEntity);
        $("#btn_Send").attr("id", "btnDiv_Send");
    });
    $("#btnquit").live("click", function () {
        $(this).parent().hide();
        $("#opinion").val("");
    })
    messageDialog = $("#auditShow").messageDialog;
    //   sendMessageClass = new MessageCommon(this.messageDialog);
    sendMessageClassedit = new MessageCommon(messageDialog);
    //    //实例化类容
    //    messageDialog = $("#msgReceiverContainer").messageDialog({
    //        "button": {
    //            "发送消息": function () {
    //                //选中用户
    //                var _$mesUser = $(":checkbox[name=messageUser]:checked");

    //                if (_$mesUser.length == 0) {
    //                    alert("请至少选择一个流程审批人！");
    //                    return false;
    //                }

    //                getUserAndUpdateRecordEdit('0', '1', Instance.JsonDataEntity);
    //            }
    //           ,
    //            "关闭": function () {
    //                messageDialog.hide();
    //            }
    //        }
    //    });
    this.Dom.BtnSend = $("#btnDiv_Send");
    this.Dom.BtnSend.live("click", function () {
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        } else {
            getUserAndUpdateRecordEdit('0', '1', Instance.JsonDataEntity);
        }
        $("#btn_CanceAudit").trigger('click');
    });

})








//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程cccccc
function getUserAndUpdateRecordEdit(action, flag, jsonData) {
    //提交地址
    var result = TG.Web.ProjectPlan.ProPlanAuditBymaster.SubmitApplyEdit(jsonData, flag);
    //提交数据
    if (result.value == "0") {
        alert("发起项目调度申请修改失败，请联系系统管理员！");
    }
    else if (result.value == "1") {
        alert("项目调度申请修改已提交，不能重复提交！");
    }
    else {
        renderUserOrSendMsg(flag, result.value);
    }
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag == "0") {
        sendMessageClassedit.render(obj.UserList, obj.RoleName);
        $("#AuditUserDiv").modal();
    }
    else {
        sendMessageClassedit.setMsgTemplate(obj);
        sendMessageClassedit.chooseUserForMessage(sendMessage);
    }
}
//发送消息回调方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {

        //提示消息
        $("#AuditUserDiv").modal('hide');
        alert("发起项目调度申请修改已成功！\n消息已成功发送到审批人等待审批！");
        //返回消息列表
        //window.history.back();
        window.location.href = "ProPlanListBymaster.aspx";

    } else {
        alert("消息发送失败！");
    }
}
