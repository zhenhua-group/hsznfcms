﻿//未分配
function AllotList() {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/ProjectPlan/ProPlanListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '', '', '项目名称', '合同备案', '申请时间', '工号', '执行设总', '评审状态', '项目策划'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'ProjectSysNo', index: 'ProjectSysNo', hidden: true, editable: true },
                             { name: 'ProjectPlanAuditSysNo', index: 'ProjectPlanAuditSysNo', hidden: true, editable: true },
                             { name: 'ProjectPlanAuditStatus', index: 'ProjectPlanAuditStatus', hidden: true, editable: true },
                             { name: 'PMUserID', index: 'PMUserID', hidden: true, editable: true },
                             { name: 'ProjectName', index: 'ProjectName', width: 350, formatter: colNameShowFormatter },
                             { name: 'IsBackup', index: 'IsBackup', width: 80, align: 'center' },
                             { name: 'SubmitDateString', index: 'SubmitDateString', width: 200, align: 'center' },
                             { name: 'ProjectJobNumber', index: 'ProjectJobNumber', width: 100, align: 'center' },
                              { name: 'PMName', index: 'PMName', width: 80, align: 'center' },
                             { name: 'ProjectSysNo', index: 'ProjectSysNo', width: 80, align: 'center', formatter: colStrShowFormatter },
                             { name: 'CoperationName', index: 'CoperationName', width: 80, align: 'center', formatter: colShowFormatter }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()) },
        loadonce: false,
        sortname: 'ProjectSysNo',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectPlan/ProPlanListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'ProjectSysNo');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectPlan/ProPlanListHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'ProjectSysNo',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectPlan/ProPlanListHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'ProjectSysNo',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //添加策划
    $(".cls_addplan").live("click", function () {
        //是否当前用户为项目总负责
        // var curusersysno = $(this).next(":hidden").val();
        var curusersysno = $(this).parent().parent().find("td").eq(5).text();
        if (usersysno != curusersysno) {
            $(this).attr("href", "###");
            toastr.info("您不是本项目的执行设总，不能添加策划信息！");
            return false;
        }
    });

}

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["ProjectSysNo"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

}
//静态状态
function colStrShowFormatter(celvalue, options, rowData) {
    var auditString = "未提交评审";
    if (rowData["ProjectPlanAuditSysNo"] == "0") {
        auditString = "未提交评审";
    }
    else {
        switch (rowData["ProjectPlanAuditStatus"]) {
            case "A":
                auditString = "承接部门";
                break;
            case "B":
                auditString = "生产经营部";
                break;
            case "D":
                auditString = "总工办";
                break;
            case "F":
                auditString = "总经理";
                break;
            case "C":
            case "E":
            case "G":
            case "I":
                auditString = "评审不通过";
                break;
            case "H":
                auditString = "评审通过";
                break;
            case "R":
                auditString = "申请修改中";
                break;
        }
    }
    return auditString;
}
//状态
//function colStatusShowFormatter(celvalue, options, rowData) {
//    var str = "";
//    if (celvalue == "1") {
//        str = '<a href="###"  class="process">分配中</a>';
//    }
//    else if (celvalue == "2") {
//        str = '<a href="###" class="done">已分配</a>';
//    }
//    else {
//        str = '<a href="###" rel=' + celvalue + ' class="apply" runat="server">提交申请</a>';
//    }
//    return str;
//}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var linkAction = "";
    linkAction = "<a class=\"cls_addplan\"  href=\"/ProjectPlan/ProPlanAddBymaster.aspx?projectSysNo=" + rowData["ProjectSysNo"] + "&CoperationName=" + celvalue + "&Action=1&JobNumber=" + rowData["ProjectJobNumber"] + "\">添加策划</a><div id=\"" + rowData["ProjectSysNo"] + "\" style=\"display: none;\" class=\"cls_flag\"></div>";
    return linkAction;
}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));


        var div2 = $("#" + rowIds[i]).find("td").find(".cls_flag");
        if (projsysno != "") {
            if (div2.attr("id") == projsysno) {
                div2.parent().parent().css({ "background-color": "#999999" });
            }
        }

        //标示是否返回类型
        var obj = div2.parent().parent().find("td .cls_addplan");
        obj.attr("href", obj.attr("href") + "&flag=" + $("#ctl00_ContentPlaceHolder1_hid_flag").val());

    }
}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有符合条件数据！</div>")
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}


//已分配完成
function AllotList2() {
    $("#jqGrid2").jqGrid({
        url: '/HttpHandler/ProjectPlan/ProPlanListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '', '', '项目名称', '合同备案', '录入时间', '工号', '提交人', '评审状态', '操作'],
        colModel: [
                              { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'ProjectSysNo', index: 'ProjectSysNo', hidden: true, editable: true },
                             { name: 'ProjectPlanAuditSysNo', index: 'ProjectPlanAuditSysNo', hidden: true, editable: true },
                             { name: 'ProjectPlanAuditStatus', index: 'ProjectPlanAuditStatus', hidden: true, editable: true },
                             { name: 'PMUserID', index: 'PMUserID', hidden: true, editable: true },
                             { name: 'ProjectName', index: 'ProjectName', width: 350, formatter: colNameShowFormatter2 },
                             { name: 'IsBackup', index: 'IsBackup', width: 80, align: 'center' },
                             { name: 'SubmitDateString', index: 'SubmitDateString', width: 200, align: 'center' },
                             { name: 'ProjectJobNumber', index: 'ProjectJobNumber', width: 100, align: 'center' },
                             { name: 'ApplyAuditUserName', index: 'ApplyAuditUserName', width: 80, align: 'center' },
                             { name: 'ProjectSysNo', index: 'ProjectSysNo', width: 120, align: 'center', formatter: colStrShowFormatter2 },
                             { name: 'CoperationName', index: 'CoperationName', width: 100, align: 'center', formatter: colShowFormatter2 }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "sel2", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where2").val()), "year": $("#ctl00_ContentPlaceHolder1_drp_year").val() },
        loadonce: false,
        sortname: 'ProjectSysNo',
        sortorder: 'desc',
        pager: "#gridpager2",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectPlan/ProPlanListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod2,
        loadComplete: loadCompMethod2
    });


    //显示查询
    $("#jqGrid2").jqGrid("navGrid", "#gridpager2", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid2").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid2').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid2').jqGrid('getCell', sel_id, 'ProjectSysNo');
                        return value;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where2").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectPlan/ProPlanListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'ProjectSysNo',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where2").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectPlan/ProPlanListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year },
            page: 1,
            sortname: 'ProjectSysNo',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //编辑策划
    $(".cls_editplan").live("click", function () {
        //是否当前用户为项目总负责
        //var curusersysno = $(this).nextAll(":hidden").val();
        var curusersysno = $(this).parent().parent().find("td").eq(5).text();
        if (usersysno != curusersysno) {
            $(this).attr("href", "###");
            toastr.info("您不是本项目的执行设总，不能编辑策划信息！");
            return false;
        }
    });
}

//名称连接
function colNameShowFormatter2(celvalue, options, rowData) {
    var pageurl = "/ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["ProjectSysNo"];
    return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';
}
//静态状态
function colStrShowFormatter2(celvalue, options, rowData) {
    var percent = 0;
    var count = 3;
    switch (rowData["ProjectPlanAuditStatus"]) {
        case "A":
            percent = 0;
            break;
        case "B":
        case "C":
            percent = (100 / count) * 1;
            break;
        case "D":
        case "E":
            percent = (100 / count) * 2;
            break;
        case "F":
        case "G":
            percent = (100 / count) * 3;
            break;
        case "H":
        case "I":
            percent = 100;
            break;
    }
    percent = parseFloat(percent).toFixed(2);
    var pageurl = '<div class="progressbar" rel="' + percent + '" title="' + percent + '%" id="auditLocusContainer" action="PN" referencesysno="' + rowData["ProjectPlanAuditSysNo"] + '" style="cursor: pointer;height:20px;margin:1px 1px;"></div>';

    return pageurl;
}
//查看
function colShowFormatter2(celvalue, options, rowData) {
    //                var auditString = "";
    //                if (rowData["ProjectPlanAuditSysNo"] == 0)
    //                {
    //                    auditString = "未提交评审";
    //                }
    //                else
    //                {
    //                    switch (rowData["ProjectPlanAuditStatus"])
    //                    {
    //                        case "A":
    //                            auditString = "承接部门";
    //                            break;
    //                        case "B":
    //                            auditString = "生产经营部";
    //                            break;
    //                        case "D":
    //                            auditString = "总工办";
    //                            break;
    //                        case "F":
    //                            auditString = "总经理";
    //                            break;
    //                        case "C":
    //                        case "E":
    //                        case "G":
    //                        case "I":
    //                            auditString = "评审不通过";
    //                            break;
    //                        case "H":
    //                            auditString = "评审通过";
    //                            break;
    //                        case "R":
    //                            auditString = "申请修改中";
    //                            break;
    //                    }
    //                }

    var linkAction = "";
    if (rowData["ProjectPlanAuditSysNo"] == "0" || rowData["ProjectPlanAuditStatus"] == "C" || rowData["ProjectPlanAuditStatus"] == "E" || rowData["ProjectPlanAuditStatus"] == "G" || rowData["ProjectPlanAuditStatus"] == "I") {
        linkAction = "<a class=\"cls_editplan\" href=\"/ProjectPlan/ProPlanAddBymaster.aspx?projectSysNo=" + rowData["ProjectSysNo"] + "&Action=3&JobNumber=" + rowData["ProjectJobNumber"] + "&CoperationName=" + celvalue + "\">编辑</a><a  href=\"#AuditUser\" data-toggle=\"modal\" style=\"margin-left:5px;\" id=\"applyAuditImgButton\" projectsysno=\"" + rowData["ProjectSysNo"] + "\" coperationname=\"" + celvalue + "\">发起审批</a>";
    }
    else {
        linkAction = "审核中";
    }
    if (rowData["ProjectPlanAuditStatus"] == "F") {
        linkAction = "已完成 | <a href=\'/ProjectPlan/ProPlanAuditBymaster.aspx?projectPlanAuditSysNo=" + rowData["ProjectPlanAuditSysNo"] + "\'>评审结果</a>";
    }
    if (rowData["ProjectPlanAuditStatus"] == "R") {
        linkAction = "<a class=\"cls_editplan\" href=\"/ProjectPlan/ProPlanAddBymaster.aspx?projectSysNo=" + rowData["ProjectSysNo"] + "&Action=3&JobNumber=" + rowData["ProjectJobNumber"] + "&CoperationName=" + celvalue + "\">编辑</a> | <a href=\'/ProjectPlan/ProPlanAuditBymaster.aspx?projectPlanAuditSysNo=" + rowData["ProjectPlanAuditSysNo"] + "\'>评审结果</a>";
    }
    return linkAction + "<div id=\"" + rowData["ProjectSysNo"] + "\" style=\"display: none;\" class=\"cls_flag2\">";
}




//统计 
function completeMethod2() {
    var rowIds = $("#jqGrid2").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#jqGrid2 [id=" + rowIds[i] + "]").find("td").eq(1).text((i + 1));

        var percent = parseFloat($("#jqGrid2 [id=" + rowIds[i] + "]").children("td").children(".progressbar").attr("rel"));

        $("#jqGrid2 [id=" + rowIds[i] + "]").children("td").children(".progressbar").progressbar({
            value: percent
        });


        var div2 = $("#jqGrid2 [id=" + rowIds[i] + "]").find("td").find(".cls_flag2");
        if (div2.attr("id") == projsysno) {
            div2.parent().parent().css({ "background-color": "#999999" });
        }
        //标示是否返回类型
        var obj = div2.parent().parent().find("td .cls_editplan");
        obj.attr("href", obj.attr("href") + "&flag=" + $("#ctl00_ContentPlaceHolder1_hid_flag").val());
    }
}
//无数据
function loadCompMethod2() {
    $("#jqGrid2").setGridParam("").hideCol("cb").trigger('reloadGrid');
    var rowcount = parseInt($("#jqGrid2").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata2").text() == '') {
            $("#jqGrid2").parent().append("<div id='nodata2'>没有符合条件数据！</div>")
        }
        else { $("#nodata2").show(); }
    }
    else {
        $("#nodata2").hide();
    }
}