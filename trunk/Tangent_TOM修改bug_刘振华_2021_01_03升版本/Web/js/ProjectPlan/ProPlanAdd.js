﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //项目阶段
    var jd = $("#ProjectStatusHidden").val();
    if (jd) {
        if ($.trim(jd).indexOf(',') > -1) {
            var array = jd.split(',');
            for (var i = 0; i < array.length; i++) {
                if ($.trim(array[i]) == "方案设计") {
                    $("#Checkbox1").attr("checked", "checked");
                }
                else if ($.trim(array[i]) == "初步设计") {
                    $("#Checkbox2").attr("checked", "checked");
                }
                else if ($.trim(array[i]) == "施工图设计") {
                    $("#Checkbox3").attr("checked", "checked");
                }
            }
        }
        else {
            if ($.trim(jd) == "方案设计") {
                $("#Checkbox1").attr("checked", "checked");
            }
            else if ($.trim(jd) == "初步设计") {
                $("#Checkbox2").attr("checked", "checked");
            }
            else if ($.trim(jd) == "施工图设计") {
                $("#Checkbox3").attr("checked", "checked");
            }
        }
    }
    //级别
    var jb = $("#ProjectLevelHidden").val();
    if (jb) {
        if (jb == "1") {
            $("#Radio1").attr("checked", "checked");
        }
        else if (jb == "2") {
            $("#Radio2").attr("checked", "checked");
        }
    }
    if (true) {
        var projectNo = $("#ProjectSysNo").val();
        $("span[name='userSpan']").each(function () {
            var attrSysno = $(this).attr("userSysNo");
            var result = TG.Web.ProjectPlan.ProPlanAdd.isTruePack(parseInt(projectNo), parseInt(attrSysno));
            if (result.value == "1") {
                $(".remove", this).css("color", "gray");
            }
        });

    }
});