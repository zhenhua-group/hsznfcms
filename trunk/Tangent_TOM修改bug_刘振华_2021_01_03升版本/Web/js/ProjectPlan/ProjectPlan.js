﻿function ProjectPlan(container) {
    this.Container = container;
    this.Dom = {};
    this.BackgroundInvoke = TG.Web.ProjectPlan.ProPlanAdd;
    var Instance = this;

    this.IsEdit = $("#HiddenIsEdit", container).val();
    this.ProjectSysNo = $("#ProjectSysNo", container).val();
    this.CoperationName = $("#HiddenCoperationName", container).val();
    this.ProjectPlanAuditSysNo = $("#HiddenProjectPlanAuditSysNo", container).val();
    this.HiddenJJS = $("#HiddenJJS", container).val();
    this.HiddenNTS = $("#HiddenNTS", container).val();
    this.Dom.ChooseUserImageButton = $("#ChooseUserImageButton", container);
    //返回
    this.Dom.BackListButton = $("#backList", container);

    CommonControl.SetTableStyle("chooseUserResultTable", "");
    CommonControl.SetTableStyle("AddSubItemResultTable", "");
    CommonControl.SetTableStyle("infomationTable", "need");

    //验证
    this.Validation = function () {
        if ($("#EngineeringMater", container).children("span").length == 0) {
            alert("请选择工程主持人！");
            return false;
        }

        if ($("#Assistant", container).children("span").length == 0) {
            alert("请选择助理！");
            return false;
        }
        if ($("#ProfessionalPerson", container).children("span").length == 0) {
            alert("请选择专业负责人！");
            return false;
        }


        if ($("#Reviewer", container).children("span").length == 0) {
            alert("请选择校对人！");
            return false;
        }

        if ($("#AuditUser", container).children("span").length == 0) {
            alert("请选择审核人！");
            return false;
        }



        if ($("#Design", container).children("span").length == 0) {
            alert("请选择设计人！");
            return false;
        }
        //判断经济所参与zhuanyefuze
        if (this.HiddenJJS == "1") {
            var flag = false;
            $("span[name=userSpan]", "#AuditUser").each(function () {
                var valObj = $(this).text(); //得到span的值
                var strs = new Array(); //定义一数组
                strs = valObj.split("["); //字符分割     
                for (i = 1; i < strs.length; i++) {

                    if (strs[i].substring(0, 2) == "预算") {
                        flag = true;
                    }
                };
            });
            $("span[name=userSpan]", "#ProfessionalPerson").each(function () {
                var valObj = $(this).text(); //得到span的值
                var strs = new Array(); //定义一数组
                strs = valObj.split("["); //字符分割     
                for (i = 1; i < strs.length; i++) {

                    if (strs[i].substring(0, 2) == "预算") {
                        flag = true;
                    }
                };
            });
            $("span[name=userSpan]", "#Reviewer").each(function () {
                var valObj = $(this).text(); //得到span的值
                var strs = new Array(); //定义一数组
                strs = valObj.split("["); //字符分割     
                for (i = 1; i < strs.length; i++) {

                    if (strs[i].substring(0, 2) == "预算") {
                        flag = true;
                    }
                };
            });

            $("span[name=userSpan]", "#Design").each(function () {
                var valObj = $(this).text(); //得到span的值
                var strs = new Array(); //定义一数组
                strs = valObj.split("["); //字符分割     
                for (i = 1; i < strs.length; i++) {

                    if (strs[i].substring(0, 2) == "预算") {
                        flag = true;
                    }
                };
            });
            if (!flag) {
                alert("转经济所项目需要经济所人员参与！");
                return false;
            }

        }
        //判断暖通所参与
        if (this.HiddenNTS == "1") {
            var flag = false;
            $("span[name=userSpan]", "#Reviewer").each(function () {
                var valObj = $(this).text(); //得到span的值
                var strs = new Array(); //定义一数组
                strs = valObj.split("["); //字符分割     
                for (i = 1; i < strs.length; i++) {

                    if (strs[i].substring(0, 2) == "暖通") {
                        flag = true;
                    }
                };
            })
            $("span[name=userSpan]", "#Design").each(function () {
                var valObj = $(this).text(); //得到span的值
                var strs = new Array(); //定义一数组
                strs = valObj.split("["); //字符分割     
                for (i = 1; i < strs.length; i++) {

                    if (strs[i].substring(0, 2) == "暖通") {
                        flag = true;
                    }
                };
            })
            $("span[name=userSpan]", "#AuditUser").each(function () {
                var valObj = $(this).text(); //得到span的值
                var strs = new Array(); //定义一数组
                strs = valObj.split("["); //字符分割     
                for (i = 1; i < strs.length; i++) {

                    if (strs[i].substring(0, 2) == "暖通") {
                        flag = true;
                    }
                };
            })
            $("span[name=userSpan]", "#ProfessionalPerson").each(function () {
                var valObj = $(this).text(); //得到span的值
                var strs = new Array(); //定义一数组
                strs = valObj.split("["); //字符分割     
                for (i = 1; i < strs.length; i++) {

                    if (strs[i].substring(0, 2) == "暖通") {
                        flag = true;
                    }
                };
            })
            if (!flag) {
                alert("转暖通所项目需要暖通所人员参与！");
                return false;
            }
        }

        if ($("#AddSubItemResultTable tr", container).length == 0) {
            alert("请添加项目进度计划！");
            return false;
        }
        return true;
    }
    //保存按钮
    this.Dom.TotalSaveButton = $("#TotalSave", container);

    this.RedirectToList = function () {
        window.location.href = $("#hid_backurl", container).val();
    }

    this.DisabledControl = function () {
        $(":checkbox", container).attr("disabled", true);
        $(":text", container).attr("disabled", true);
    }

    //添加空表格
    this.FillEmptyTable = function () {
        for (var i = 0; i < 15; i++) {
            var trString = "<tr><td style=\"width: 5%;\" align=\"center\"></td><td style=\"width: 10%;\" align=\"center\"></td><td style=\"width: 10%;\" align=\"center\"></td><td style=\"width: 10%;\" align=\"center\"></td></tr>";
            $("#chooseUserResultTable").append(trString);
        }
        CommonControl.SetTableStyle("chooseUserResultTable", "need");
    }

    this.Init = function () {
        if (Instance.IsEdit == "1") {
            this.Dom.TotalSaveButton.show();
            Instance.FillEmptyTable();
        }
        var hasAudit = Instance.BackgroundInvoke.CheckAudit(parseInt(Instance.ProjectSysNo, 10));
        if (hasAudit.value == "1") {
            alert("该项在审核队列中，不允许编辑！");
            history.go(-1);
            CommonControl.CreateTransparentDiv();
            CommonControl.SetTableStyle("chooseUserResultTable", "need");
        }
    }
    //初始化
    Instance.Init();
    //项目策划人员列表
    var chooseUserOfTheDepartmentControl = new UserOfTheDepartmentTree($("#ChooseUserOfTherDepartmentContainer"));

    //选择用户回掉
    this.ChooseUserOfTheDepartmentCallBack = function (userArray) {
        var showResultTable = $("#chooseUserResultTable");
        showResultTable.find("tr").remove();

        $.each(userArray, function (index, item) {
            var trString = "<tr><td style=\"width: 5%;\" align=\"center\"><input type=\"checkbox\" userSysNo=" + item.userSysNo + " name=\"userCheckBox\" userName=" + item.userName + " userSpecialtyName=\"" + item.specialtyName + "\"></td><td style=\"width: 10%;\" align=\"left\"><img src=\"/Images/" + item.userSex + ".png\" style=\"width:16px;height:16px;\">" + item.userName + "</td><td style=\"width: 10%;\" align=\"center\">" + item.specialtyName + "</td><td style=\"width: 10%;\" align=\"center\">" + item.departmentName + "</td></tr>";
            $("#chooseUserResultTable").append(trString);
        });

        //人员表格加样式
        CommonControl.SetTableStyle("chooseUserResultTable", "need");
    }
    //清空checkbox
    this.ClearSelectedCheckBoxUser = function () {
        $(":checkbox", $("#chooseUserResultTable")).removeAttr("checked");
    }
    //全选
    $("#selectAllCheckBox", container).click(function () {
        var isChecked = $(this).attr("checked");
        if (isChecked == undefined) {
            $(":checkbox", $("#chooseUserResultTable")).removeAttr("checked");
        } else {
            $(":checkbox", $("#chooseUserResultTable")).attr("checked", "checked");
        }

    });

    //添加用户到已有的表格中
    this.Dom.ChooseUserImageButton.click(function () {
        $("#ChooseUserOfTherDepartmentContainer").dialog({
            autoOpen: false,
            modal: true,
            width: 500,
            height: 400,
            resizable: false,
            buttons:
			        {
			            "确定": function () {
			                //调用处理事件
			                chooseUserOfTheDepartmentControl.SaveUser(Instance.ChooseUserOfTheDepartmentCallBack);
			                $(this).dialog("close");
			            },
			            "取消": function () { $(this).dialog("close"); }
			        }
        }).dialog("open");
        return false;
    });

    //删除用户A标签点击事件
    $("#resultUserRemoveA", container).live("click", function () {
        //移除该包含用户信息的Span
        var UserSysno = $(this).parent("span:first").attr("userSysNo");
        var result = Instance.BackgroundInvoke.isTruePack(parseInt(Instance.ProjectSysNo, 10), parseInt(UserSysno, 10));
        if (result.value == "1") {
            alert("TCD已经有图纸请到TCD平台转交图纸！");
            return false;
        }

        $(this).parent("span:first").remove();
    });

    //从所有用户容器里取得数据
    this.ChooseUserCheckBox = function () {
        return $(":checked[name=userCheckBox]", container);
    }

    this.Dom.DeleteUserImageButton = $("#DeleteUserImageButton", container);
    this.Dom.DeleteUserImageButton.click(function () {
        $.each(Instance.ChooseUserCheckBox(), function (index, item) {
            $("span[name=userSpan][userSysNo=" + $(item).attr("userSysNo") + "]").remove();
            $(item).parents("tr:first").remove();
        });
        return false;
    });

    this.FormatData = function (cks) {
        var array = new Array();
        $.each(cks, function (index, item) {
            array[index] = {
                "userSysNo": $(item).attr("userSysNo"),
                "userName": $(item).attr("userName"),
                "userSex": $(item).attr("sex"),
                "userSpecialtyname": $(item).attr("userspecialtyname")
            };
        });
        return array;
    }

    this.AppendToTable = function (tableName, DataArray) {

        //已有人员
        var span_count = $("#" + tableName).find("span").length;
        //本次添加人员
        var curr_count = DataArray.length;
        //临时索引
        var tempindex = 0;
        //余数计算
        var tempspcount = (span_count % 4) > 4 ? (span_count % 4) : (4 - (span_count % 4));

        //添加人员
        $.each(DataArray, function (index, item) {
            //获取原Table下已有的用户
            var sourceUserSpan = $("#" + tableName).children("span");
            var AllowAdd = true;
            //遍历已有的用户信息数组，如果已经存在，不允许添加
            for (var i = 0; i < sourceUserSpan.length; i++) {
                if (parseInt($(sourceUserSpan[i]).attr("userSysNo"), 10) == item.userSysNo) {
                    AllowAdd = false;
                    break;
                }
            }
            if (AllowAdd) {
                var trString = "<span style=\"margin-left:5px;\" userSysNo=\"" + item.userSysNo + "\" name=\"userSpan\">" + item.userName + "[" + item.userSpecialtyname + "]<a href=\"#\" style=\"margin-left:3px;\" id=\"resultUserRemoveA\">X</a></span>";
                
                //---------------四个人一行显示数据开始
                index++;
                //追加
                if (span_count > 0) {//容器存在人员
                    
                    if (curr_count > tempspcount) {//选中人员大于每行差人数
                        if (index == tempspcount) {//第几个元素加br
                            trString += "<br/>";
                        }
                        else if (index > tempspcount) {//出去填补的外剩余的按正常逻辑添加
                            tempindex = index - tempspcount;
                            if (tempindex % 4 == 0) {
                                trString += "<br/>";
                            }
                        }
                    }
                    else {//选中人员小于所差
                        if (index == tempspcount) {
                            trString += "<br/>";
                        }
                    }
                }
                else { //容器不存在人员 
                    if (index % 4 == 0) {
                        trString += "<br/>";
                    }
                }

                $("#" + tableName).append(trString);
            }
        });
        //清空checkbox
        Instance.ClearSelectedCheckBoxUser();
    }

    //工程主持人操作
    this.Dom.EngineeringMaterAddButton = $("#EngineeringMaterAddButton", container);
    this.Dom.EngineeringMaterAddButton.click(function () {
        var dataArray = Instance.FormatData(Instance.ChooseUserCheckBox());
        Instance.AppendToTable("EngineeringMater", dataArray);
    });

    //助理Button
    this.Dom.AssistantAddButton = $("#AssistantAddButton", container);
    this.Dom.AssistantAddButton.click(function () {
        var dataArray = Instance.FormatData(Instance.ChooseUserCheckBox());
        Instance.AppendToTable("Assistant", dataArray);
    });

    //专业负责人
    this.Dom.ProfessionalPersonAddButton = $("#ProfessionalPersonAddButton", container);
    this.Dom.ProfessionalPersonAddButton.click(function () {
        var dataArray = Instance.FormatData(Instance.ChooseUserCheckBox());
        Instance.AppendToTable("ProfessionalPerson", dataArray);
    });

    //校对人
    this.Dom.ReviewerAddButton = $("#ReviewerAddButton", container);
    this.Dom.ReviewerAddButton.click(function () {
        var dataArray = Instance.FormatData(Instance.ChooseUserCheckBox());
        Instance.AppendToTable("Reviewer", dataArray);
    });

    //审核人
    this.Dom.AuditUserAddButton = $("#AuditUserAddButton", container);
    this.Dom.AuditUserAddButton.click(function () {
        var dataArray = Instance.FormatData(Instance.ChooseUserCheckBox());
        Instance.AppendToTable("AuditUser", dataArray);
    });

    //审定人
    this.Dom.ValidatorAddButton = $("#ValidatorAddButton", container);
    this.Dom.ValidatorAddButton.click(function () {
        var dataArray = Instance.FormatData(Instance.ChooseUserCheckBox());
        Instance.AppendToTable("Validator", dataArray);
    });


    // 设计人员
    this.Dom.ValidatorAddButton = $("#DesignAddButton", container);
    this.Dom.ValidatorAddButton.click(function () {
        var dataArray = Instance.FormatData(Instance.ChooseUserCheckBox());
        Instance.AppendToTable("Design", dataArray);
    });

    //添加子项类
    var addScheduledplan = new AddScheduledplan($("#AddSubProject"));

    this.AddSubProjectCallback = function (dataObj) {
        //计算总纸张数
        var trString = "<tr><td  align=\"center\" style=\"width:40%\">" + dataObj.designLevel + "</td>";
        trString += "<td align=\"center\" style=\"width:20%\">" + dataObj.txtStartTime + "</td>";
        trString += "<td align=\"center\" style=\"width:20%\">" + dataObj.txtFinishtime + "</td>";
        trString += "<td align=\"center\" style=\"align:center;width:10%\">" + dataObj.SumPaper + "<span style=\"display: none;\" id=\"paperObjSpan\">" + dataObj.PaperString + "</span></td>";
        trString += "<td align=\"center\" style=\";width:10%\"><a id=\"DeleteSubItemLinkButton\" sysno=\"0\">删除</a>&nbsp;<a href=\"#\" id=\"EditSubItemLinkButton\">编辑</a></td>";
        trString += "</tr>";
        $("#AddSubItemResultTable").append(trString);

        CommonControl.SetTableStyle("AddSubItemResultTable", "");
    }

    $("#DeleteSubItemLinkButton", container).live("click", function () {
        var sysNo = $(this).attr("sysno");
        $(this).parents("tr:first").remove();
        return false;
    });

    //编辑项目子项信息
    $("#EditSubItemLinkButton", container).live("click", function () {
        var tr = $(this).parents("tr:first");
        var parObj = eval("(" + $.trim($("#paperObjSpan", tr).text()) + ")");
        parObj.DesignLevel = $.trim(tr.children("td:eq(0)").text());
        parObj.StartDate = $.trim(tr.children("td:eq(1)").text());
        parObj.EndDate = $.trim(tr.children("td:eq(2)").text());

        //修改状态IsEdit等于True。有些地方需要判断是否是编辑状态
        addScheduledplan.IsEdit = true;
        addScheduledplan.Clear();
        addScheduledplan.BindData(parObj);
        $("#AddSubProject").dialog({
            autoOpen: false,
            modal: true,
            width: 560,
            height: 270,
            resizable: false,
            title: "编辑项目进度计划",
            buttons:
                    {
                        "确定": function () {
                            //调用处理事件
                            addScheduledplan.SaveScheduledplan(Instance.AddSubProjectCallback);
                            tr.remove();
                            $(this).dialog("close");
                        },
                        "取消": function () { $(this).dialog("close"); addScheduledplan.IsEdit = false; }
                    }
        }).dialog("open");
        return false;
    });

    //添加子项
    this.Dom.AddScheduledplanImageButton = $("#AddScheduledplanImageButton", container);
    this.Dom.AddScheduledplanImageButton.click(function () {
        addScheduledplan.Clear();
        $("#AddSubProject").dialog({
            autoOpen: false,
            modal: true,
            width: 560,
            height: 270,
            resizable: false,
            title: "添加项目进度计划",
            buttons:
                    {
                        "确定": function () {
                            //调用处理事件
                            addScheduledplan.SaveScheduledplan(Instance.AddSubProjectCallback, this);
                        },
                        "取消": function () { $(this).dialog("close"); }
                    }
        }).dialog("open");
        return false;
    });

    //保存
    this.Dom.TotalSaveButton.click(function () {
        var result = Instance.SaveMethod();

        if (parseInt(result, 10) > 0) {
            alert("项目进度计划信息，人员信息保存成功！");
            Instance.RedirectToList();
        }
    });

    //保存事件
    this.SaveMethod = function () {
        var allowSubmit = Instance.Validation();

        if (allowSubmit == true) {
            var finalDataObj = {
                "ProjectSysNo": Instance.ProjectSysNo,
                "Member": new Array(),
                "RoleUser": new Array(),
                "Items": new Array(),
                "DesignMember": new Array()

            };
            //取得所有的成员
            var arrAllUser = Instance.FormatData($(":checkbox[name=userCheckBox]", container));

            var arrayAll = new Array();
            //取得所有分配了角色的用户
            var arrAllRoleUser = $(".resultDiv span[name=userSpan]", container);
            $.each(arrAllRoleUser, function (index, item) {
                finalDataObj.RoleUser[index] = { "UserSysNo": $(item).attr("userSysNo"), "RoleSysNo": $(item).parent("div:first").attr("rolesysno"), "IsDesigner": 0 };
                arrayAll.push($(item).attr("userSysNo"));
            });

            //去掉 其他角色的重复值
            var arrayTotal = Instance.DistinctArray(arrayAll);

            //所有参与项目的人员
            $.each(arrayTotal, function (index, val) {
                finalDataObj.Member[index] = { "UserSysNo": val, "RoleSysNo": "", "IsDesigner": 0 };
            });

            //所有设计人员
            var arrDesignUser = $(".resultDesgionDiv span[name=userSpan]", container);
            $.each(arrDesignUser, function (index, item) {
                finalDataObj.DesignMember[index] = { "UserSysNo": $(item).attr("userSysNo"), "RoleSysNo": $(item).parent("div:first").attr("rolesysno"), "IsDesigner": 1 };
            });

            //保存子项
            $.each($("#AddSubItemResultTable tr"), function (index, tr) {
                //取得PaperSpan
                var parObj = eval("(" + $.trim($("#paperObjSpan", $(tr)).text()) + ")");
                parObj.DesignLevel = $.trim($(tr).children("td:eq(0)").text());
                parObj.StartDate = $.trim($(tr).children("td:eq(1)").text());
                parObj.EndDate = $.trim($(tr).children("td:eq(2)").text());
                finalDataObj.Items[index] = parObj;
            });
            //改变消息状态
            var msg = new MessageCommProjPlan($("#hid_msgid").val());
            msg.ReadMsg();

            var result = Instance.BackgroundInvoke.SavePlanRecord(Global.toJSON(finalDataObj));
            return result.value;
        }
    }
    //返回
    this.Dom.BackListButton.click(function () {
        Instance.RedirectToList();
    });
    //去除重复项
    this.DistinctArray = function (arr) {
        var obj = {}, temp = [];
        for (var i = 0; i < arr.length; i++) {
            if (!obj[arr[i]]) {
                temp.push(arr[i]);
                obj[arr[i]] = true;
            }
        }
        return temp;
    }
}