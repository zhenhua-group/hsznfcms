﻿var customer = function () {

    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Customer/CustomerInfoHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '', '客户名称', '联系人 ', '联系电话', '客户编号', '公司地址', '邮政编码', '传真号', '录入人', '录入时间', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'Cst_Id', index: 'Cst_Id', hidden: true, editable: true },
                             { name: 'Cst_Name', index: 'Cst_Name', width: 200, formatter: colNameShowFormatter },
                             { name: 'Linkman', index: 'Linkman', width: 60, align: 'center' },
                             { name: 'Cpy_Phone', index: 'Cpy_Phone', width: 100, align: 'center' },
                             {
                                 name: 'Cst_No', index: 'Cst_No', width: 100, align: 'center', formatter: function (celvalue, options, rowData) {
                                     return $.trim(celvalue);
                                 }
                             },
                             { name: 'Cpy_Address', index: 'Cpy_Address', width: 150 },
                             { name: 'Code', index: 'Code', width: 80, align: 'center' },
                             { name: 'Cpy_Fax', index: 'Cpy_Fax', width: 80, align: 'center' },
                              { name: 'InsertUser', index: 'InsertUserID', width: 60, align: 'center' },
                             { name: 'lrsj', index: 'InsertDate', width: 80, align: 'center' },
                             { name: 'Cst_Id', index: 'Cst_Id', width: 50, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_wherecust").val()) },
        loadonce: false,
        sortname: 'c.cst_id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Customer/CustomerInfoHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid').jqGrid('getCell', sel_id[i], 'Cst_Id') + ",";
                            }
                        }

                        return values;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#cust_btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_wherecust").val());
        var unit = $("#ctl00_ContentPlaceHolder1_cust_drp_unit").val();
        var year = $("#ctl00_ContentPlaceHolder1_cust_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_cust_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_cust_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_cust_txt_end").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Customer/CustomerInfoHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime },
            page: 1,
            sortname: 'c.cst_id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //选择生产部门
    $("#ctl00_ContentPlaceHolder1_cust_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_wherecust").val());
        var unit = $(this).val();
        var year = $("#ctl00_ContentPlaceHolder1_cust_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_cust_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_cust_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_cust_txt_end").val();
        $(".norecords").hide();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Customer/CustomerInfoHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime },
            page: 1,
            sortname: 'c.cst_id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //选择年份
    $("#ctl00_ContentPlaceHolder1_cust_drp_year").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_wherecust").val());
        var unit = $("#ctl00_ContentPlaceHolder1_cust_drp_unit").val();
        var year = $("#ctl00_ContentPlaceHolder1_cust_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_cust_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_cust_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_cust_txt_end").val();
        $(".norecords").hide();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Customer/CustomerInfoHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime },
            page: 1,
            sortname: 'c.cst_id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //名称连接
    function colNameShowFormatter(celvalue, options, rowData) {
        var pageurl = "/Customer/cst_ShowCustomerInfoByMaster.aspx?Cst_Id=" + rowData["Cst_Id"];
        return '<a href="' + pageurl + '" alt="查看客户">' + celvalue + '</a>';

    }

    //查看
    function colShowFormatter(celvalue, options, rowData) {
        var pageurl = "ProcessLine.aspx?Cst_Id=" + celvalue;
        return '<a href="' + pageurl + '" alt="查看客户">查看</a>';

    }
    //统计 
    function completeMethod() {
        var rowIds = $("#jqGrid").jqGrid('getDataIDs');
        for (var i = 0, j = rowIds.length; i < j; i++) {
            $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
        }

    }
    //无数据
    function loadCompMethod() {
        var rowcount = parseInt($("#jqGrid").getGridParam("records"));
        if (rowcount <= 0) {

            if ($("#nodata").text() == '') {
                $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
            }
            else { $("#nodata").show(); }
        }
        else {
            $("#nodata").hide();
        }
    }
}
var coperation = function () {

    var tempRandom = Math.random() + new Date().getMilliseconds();

    $("#jqGrid1").jqGrid({
        url: '/HttpHandler/Coperation/CoperationListHandler.ashx?n=' + (Math.random() + new Date().getMilliseconds()) + '&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_wherecop").val()),
        datatype: 'json',
        height: "auto",
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '', '合同编号', '工程名称', '合同分类', '建筑规模(㎡)', '合同额(万元)', '承接部门', '签订日期', '完成日期', '工程负责人', '已收费(万元)', '录入时间', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                             { name: 'cpr_No', index: 'cpr_No', width: 80, align: 'center' },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 200, formatter: colNameShowFormatter },
                             { name: 'cpr_Type', index: 'cpr_Type', width: 80, align: 'center' },
                             { name: 'BuildArea', index: 'BuildArea', width: 80, align: 'center' },
                             { name: 'cpr_Acount', index: 'cpr_Acount', width: 80, align: 'center' },
                             { name: 'cpr_Unit', index: 'cpr_Unit', width: 70, align: 'center' },
                             { name: 'qdrq', index: 'cpr_SignDate', width: 80, align: 'center' },
                             { name: 'wcrq', index: 'cpr_DoneDate', width: 80, align: 'center' },

                            { name: 'PMUserName', index: 'ChgPeople', width: 60, align: 'center' },
                              { name: 'ssze', index: 'ssze', width: 80, align: 'center' },
                             { name: 'lrsj', index: 'InsertDate', width: 70, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager1",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/CoperationListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod1,
        loadComplete: loadCompMethod1
    });


    //显示查询
    $("#jqGrid1").jqGrid("navGrid", "#gridpager1", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid1").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid1').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid1').jqGrid('getCell', sel_id[i], 'cpr_Id') + ",";
                            }
                        }
                        //var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        // var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
                        return values;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );



    //查询
    $("#btn_basesearch").click(function () {
        ClearjqGridParam("jqGrid1");
        //设置值为0基本查询
        $("#hid_cxtype").val("0");
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_wherecop").val());
        // var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var unit = getCheckedUnitNodes();
        var year = $("#ctl00_ContentPlaceHolder1_cop_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_cop_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_cop_txt_startdate").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_cop_txt_enddate").val();

        $(".norecords").hide();
        $("#jqGrid1").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/CoperationListHandler.ashx?n='" + tempRandom + " '&action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime },
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'
        }).trigger("reloadGrid");
    });
    //
    //名称连接
    function colNameShowFormatter(celvalue, options, rowData) {
        var pageurl = "/Coperation/cpr_ShowCoprationBymaster.aspx?flag=cprlist&cprid=" + rowData["cpr_Id"];
        return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

    }

    //合同阶段
    function colProcess(celvalue, options, rowData) {
        var temp = "";
        var arr = $.trim(celvalue).split(",");
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == "27") {
                temp = "方案,";
            }
            if (arr[i] == "28") {
                temp += "初设,";
            }
            if (arr[i] == "29") {
                temp += "施工图,";
            }
            if (arr[i] == "30") {
                temp += "其他,";
            }
        }
        return temp;
    }

    //查看
    function colShowFormatter(celvalue, options, rowData) {
        var pageurl = "ProcessLine.aspx?cprid=" + celvalue;
        return '<a href="' + pageurl + '" alt="查看合同">查看</a>';

    }

    //统计 
    function completeMethod1() {
        var rowIds = $("#jqGrid1").jqGrid('getDataIDs');
        for (var i = 0, j = rowIds.length; i < j; i++) {
            $("#" + rowIds[i], $("#jqGrid1")).find("td").eq(1).text((i + 1));
        }
    }
    //无数据
    function loadCompMethod1() {
        var rowcount = parseInt($("#jqGrid1").getGridParam("records"));
        if (rowcount <= 0) {
            if ($("#nodata1").text() == '') {
                $("#jqGrid1").parent().append("<div id='nodata1'>没有查询到数据!</div>");
            }
            else { $("#nodata1").show(); }

        }
        else {
            $("#nodata1").hide();
        }
    }
    //获取选中的部门
    var getCheckedUnitNodes = function () {

        var unitlist = $("#ctl00_ContentPlaceHolder1_cop_drp_unit_divDropdownTreeText").text();
        //temp var
        var unit = "";
        if ($.trim(unitlist) != "") {
            //选择的全部部门，部门值为空
            var allcheckedstate = $.trim($("#ctl00_ContentPlaceHolder1_cop_drp_unit_t_l_1").attr("checkedstate"));
            if (allcheckedstate != "0") {
                //部门数组
                var unitarr = unitlist.split(',');
                //得到部门
                for (var index in unitarr) {
                    if ($.trim(unitarr[index]) != "全院部门") {
                        unit += "'" + $.trim(unitarr[index]) + "',";
                    }
                }
            }
        }
        return unit;
    }
    //清空jqgrid postdata参数
    var ClearjqGridParam = function (jqgrid) {
        var postData = $("#" + jqgrid).getGridParam("postData");

        $.each(postData, function (k, v) {
            delete postData[k];
        });
    };

}
var project = function () {
    $("#jqGrid2").jqGrid({
        url: '/HttpHandler/ProjectMamage/ProjectListHandler.ashx?n=' + (Math.random() + new Date().getMilliseconds()) + '&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_whereproj").val()),
        datatype: 'json',
        height: "auto",
        mytype: 'POST',
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '', '', '', '', '甲方负责人', '项目名称', '建筑类别', '合同信息', '合同额(万元)', '开始日期', '结束日期', '执行设总', '录入时间', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', hidden: true, editable: true },
                             { name: 'CoperationSysNo', index: 'CoperationSysNo', hidden: true, editable: true },
                             { name: 'allAcount', index: 'allAcount', hidden: true, editable: true },
                             { name: 'allProjectScale', index: 'allProjectScale', hidden: true, editable: true },
                             { name: 'ChgJia', index: 'ChgJia', width: 80, align: 'center' },
                             { name: 'pro_name', index: 'pro_name', width: 200, formatter: colNameShowFormatter },
                             { name: 'BuildType', index: 'BuildType', width: 60, align: 'center' },
                             { name: 'Project_reletive', index: 'Project_reletive', width: 200, formatter: colNameCprFormatter },
                             { name: 'Cpr_Acount', index: 'Cpr_Acount', width: 80, align: 'center' },
                             { name: 'qdrq', index: 'pro_startTime', width: 100, align: 'center' },
                             { name: 'wcrq', index: 'pro_finishTime', width: 100, align: 'center' },
                             { name: 'PMUserName', index: 'PMName', width: 60, align: 'center' },
                             { name: 'lrsj', index: 'InsertDate', width: 70, align: 'center' },
                             { name: 'pro_ID', index: 'pro_ID', width: 30, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
        ],
        rowTotals: true,
        colTotals: true,
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'pro_ID',
        sortorder: 'desc',
        pager: "#gridpager2",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod2,
        loadComplete: loadCompMethod2
    });


    //显示查询
    $("#jqGrid2").jqGrid("navGrid", "#gridpager2", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid2").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {

                        var sel_id = $('#jqGrid2').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid2').jqGrid('getCell', sel_id[i], 'pro_ID') + ",";
                            }
                        }

                        //var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        //var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return values;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").live("click", function () {
        var allcheckedstate = $.trim($("#ctl00_ContentPlaceHolder1_proj_drp_unit_t_l_1").attr("checkedstate"));
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_whereproj").val());
        //var unit = $.trim(IsStructCheckNode('structtype'));
        var unit = getCheckedUnitNodes();
        if (allcheckedstate == "0") {
            unit = "";
        };
        var year = $("#ctl00_ContentPlaceHolder1_proj_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_proj_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_proj_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_proj_txt_end").val();
        var params =
            {
                "strwhere": strwhere,
                "unit": unit,
                "keyname": keyname,
                "year": year,
                "startTime": startTime,
                "endTime": endTime
            }
        clearGridPostData();
        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/ProjectMamage/ProjectListHandler.ashx?n=" + (Math.random() + new Date().getMilliseconds()) + "&action=sel",
            postData: params,
            mytype: 'POST',
            page: 1,
            sortname: 'pro_ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //其他部门参与
    function colISFormatter(celvalue, options, rowData) {
        var str = "";
        if ($.trim(celvalue) == "1") {
            str = "经济所,"
        }
        if ($.trim(rowData["ISHvac"]) == "1") {
            str += "暖通所,";
        }
        if ($.trim(rowData["ISArch"]) == "1") {
            str += "土建所,";
        }
        return str;
    }
    //名称连接
    function colNameShowFormatter(celvalue, options, rowData) {
        var pageurl = "../ProjectManage/ShowProjectBymaster.aspx?flag=list&pro_id=" + rowData["pro_ID"];
        return '<a href="' + pageurl + '" alt="查看项目">' + celvalue + '</a>';

    }
    //合同名称连接
    function colNameCprFormatter(celvalue, options, rowData) {
        var pageurl = "../Coperation/cpr_ShowCoprationBymaster.aspx?flag=projlist1&cprid=" + rowData["CoperationSysNo"];
        if (celvalue == null) {
            celvalue = "";
        }
        return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

    }

    //查看
    function colShowFormatter(celvalue, options, rowData) {
        var pageurl = "ProcessLine.aspx?pro_id=" + celvalue;
        return '<a href="' + pageurl + '" alt="查看项目">查看</a>';

    }

    //统计 
    function completeMethod2() {
        var rowIds = $("#jqGrid2").jqGrid('getDataIDs');
        for (var i = 0, j = rowIds.length; i < j; i++) {
            $("#" + rowIds[i], $("#jqGrid2")).find("td").eq(1).text((i + 1));
        }

    }
    //无数据
    function loadCompMethod2() {
        var rowcount = parseInt($("#jqGrid2").getGridParam("records"));
        if (rowcount <= 0) {
            if ($("#nodata2").text() == '') {
                $("#jqGrid2").append("<div id='nodata2'>没有查询到数据!</div>");
            }
            else { $("#nodata2").show(); }
        }
        else {
            $("#nodata2").hide();
        }
    }
    //获取选中的部门
    var getCheckedUnitNodes = function () {

        var unitlist = $("#ctl00_ContentPlaceHolder1_proj_drp_unit_divDropdownTreeText").text();
        //temp var
        var unit = "";
        if ($.trim(unitlist) != "") {
            //选择的全部部门，部门值为空
            var allcheckedstate = $.trim($("#ctl00_ContentPlaceHolder1_proj_drp_unit_t_l_1").attr("checkedstate"));
            if (allcheckedstate != "0") {
                //部门数组
                var unitarr = unitlist.split(',');
                //得到部门
                for (var index in unitarr) {
                    if ($.trim(unitarr[index]) != "全院部门") {
                        unit += "'" + $.trim(unitarr[index]) + "',";
                    }
                }
            }
        }
        return unit;
    }
    var clearGridPostData = function () {
        var postData = $("#jqGrid2").jqGrid("getGridParam", "postData");
        $.each(postData, function (k, v) {
            delete postData[k];
        });
    }
}

