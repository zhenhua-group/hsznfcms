﻿var swfu;
//配置文件上传JS公共脚本
function InitUploadFileHanlder (url){
        window.onload = function() {
                swfu = new SWFUpload({
                    upload_url: url,
                    flash_url: "../js/swfupload/swfupload.swf",
                    post_params: {
                        "ASPSESSID": "<%=Session.SessionID %>"
                    },
                    file_size_limit: "10 MB",
                    file_types: "*.txt;*.jpg;*.dwg;*.wmf;*.doc;*.docx;*.ppt;*.pptx;*.xls;*.xlsx",
                    file_types_description: "上传",
                    file_upload_limit: "0",
                    file_queue_limit: "1",

                    //Events
                    file_queued_handler: fileQueued,
                    file_queue_error_handler: fileQueueError,
                    file_dialog_complete_handler: fileDialogComplete,
                    upload_progress_handler: uploadProgress,
                    upload_error_handler: uploadError,
                    upload_success_handler: uploadSuccessShowResult,
                    upload_complete_handler: uploadComplete,

                    // Button
                    button_placeholder_id: "spanButtonPlaceholder",
                    button_image_url: "../images/swfupload/XPButtonNoText_61x22.png",
                    button_width: 61,
                    button_height: 22,
                    button_text: '<span class="btnFile">选择文件</span>',
                    button_text_style: '.btnFile { font-family: 微软雅黑; font-size: 12pt;background-color:Black; } ',
                    button_text_top_padding: 1,
                    button_text_left_padding: 5,
                    custom_settings: {
                        upload_target: "divFileProgressContainer"
                    },
                    debug: false
                });
            }
}