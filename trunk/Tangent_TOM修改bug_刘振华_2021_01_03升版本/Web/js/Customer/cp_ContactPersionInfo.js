﻿$(document).ready(function () {

    CommonControl.SetFormWidth();
    //行背景
    $("#gv_contactPerson tr").hover(function() {
        $(this).addClass("tr_in");
    }, function() {
        $(this).removeClass("tr_in");
    });
    //隔行变色
    $("#gv_contactPerson tr:even").css({ background: "White" });
    //全选
    $("#chk_All").click(function() {
        if ($(this).get(0).checked) {
            $(":checkbox").attr("checked", "true");
        }
        else {
            $(":checkbox").attr("checked", $(this).get(0).checked);
        }
    });
    //单选
    $(".chk_id").each(function(i) {
        $(this).click(function() {
            if ($(this).get(0).checked) {
                $(this).attr("checked", "true");
            }
            else {
                $(this).attr("checked", "");
            }
        });
    });
    //删除判断
    $("#btn_Cst_Del").click(function() {
        if ($(":checked", $("#gv_contactPerson")).length == 0) {
            alert("请选择要删除的记录！");
            return false;
        }
        return confirm("确认要删除选中的记录吗！");
    });

    //查看
    $(".cls_chk").click(function() {    
        $('#Show_cstInfo').dialog('open');
        $('.ui-widget-overlay').css("display", "block");

        var cstid = $(this).attr("rel");
        $.ajax({
            type: "POST",
            url: "../../HttpHandler/cp_ShowContactPersionInfo.ashx",
            dataType: "text",
            data: "id=" + cstid,
            success: function(msg) {
                var arrayValues = msg.split("|");
                $("#No").text(arrayValues[0]);
                $("#name").text(arrayValues[1]);
                $("#duties").text(arrayValues[2]);
                $("#department").text(arrayValues[3]);
                $("#phone").text(arrayValues[4]);
                $("#email").text(arrayValues[5]);
                $("#bphone").text(arrayValues[6]);
                $("#fphone").text(arrayValues[7]);
                $("#bfax").text(arrayValues[8]);
                $("#Ffax").text(arrayValues[9]);
                $("#bz").text(arrayValues[10]);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("错误");
            }
        });
    });

    // Dialog
    $('#Show_cstInfo').dialog({
        autoOpen: false,
        width: 650,
        height: 260
    });

    //关闭
    $("#Button1").click(function() {
        $('#Show_cstInfo').dialog("close");
        $('.ui-widget-overlay').css('display', 'none');
    });

    $("#ui-dialog-title-Show_cstInfo~a").click(function() {
        $('#Show_cstInfo').dialog("close");
        $('.ui-widget-overlay').css('display', 'none');
    });
    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#btn_Search"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Contact";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = "";
    paramEntity.currYear = "";
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();
});