﻿function SatisfactionInfo(parent, container) {
    this.Parnet = parent;
    this.Container = container;
    this.Dom = {};
    this.IsEdit = false;
    this.SysNo = 0;
    this.UseType = 0;
    var Instance = this;

    this.Clear = function () {
        $(":text", container).val("");
        $.each($("select", container), function (index, item) {
            item.selectedIndex = 0;
        });
        $("textarea", container).val("");
    }

    //得到满意度信息
    this.GetSatisfactionInfo = function (sid) {
        $.get("/Customer/SatisfactionInfo/stf_ModifySatisfactionInfo.aspx", { "id": sid, "n": Math.random() }, function (jsonResult) {
            if (jsonResult == "0") {
                alert("满意度信息获取失败！");
            } else {
                var dataObj = Global.evalJSON(jsonResult);
                //填充数据
                $("#txtSat_No", container).val(dataObj.Sat_No == null ? "" : $.trim(dataObj.Sat_No.replace("null", "")));
                $("#txtSch_Time", container).val($.trim(ChangeDateFormat(dataObj.Sch_Time)));
                $("#Sch_Context", container).val($.trim(dataObj.Sch_Context));
                $("#ddSat_Leve option[value=" + dataObj.Sat_Leve + "]", container).attr("selected", true);
                $("#txtSch_Reason", container).val(dataObj.Sch_Reason == null ? "" : $.trim(dataObj.Sch_Reason.replace("null", "")));
                $("#ddSat_Way option[value=" + dataObj.Sch_Way + "]", container).attr("selected", true);
                $("#txtSat_Best", container).val(dataObj.Sat_Best == null ? "" : $.trim(dataObj.Sat_Best.replace("null", "")));
                $("#txtSat_NotBest", container).val(dataObj.Sat_NotBest == null ? "" : $.trim(dataObj.Sat_NotBest.replace("null", "")));
                $("#Sat_Remark", container).val(dataObj.Remark == null ? "" : $.trim(dataObj.Remark.replace("null", "")));
                Instance.SysNo = dataObj.Sat_Id;
            }
        });
    }

    //保存满意度信息
    this.SaveSatisfaction = function (dialog) {
        //取得客户SysNo
        var customerSysNo = $("#ctl00_ContentPlaceHolder1_HiddenCustomerSysNo").val();
        var allowSubmit = true;
        ClearTipSpan();
        var txtSat_No = $("#txtSat_No", container).val();
        var txtSch_Time = $("#txtSch_Time", container).val();
        var txtSch_Context = $("#Sch_Context", container).val();

        if (txtSat_No == "") {
            CreateTipSpan($("#txtSat_No"), "序号不能为空");
            allowSubmit = false;
        } else {
            if (DataLength(txtSat_No) > 10) {
                CreateTipSpan($("#txtSat_No"), "序号长度过长");
                allowSubmit = false;
            }
        }
        if (txtSch_Context == "") {
            CreateTipSpan($("#txtSch_Context"), "调查内容不能为空！");
            allowSubmit = false;
        }
        if (txtSch_Time == "") {
            CreateTipSpan($("#txtSch_Time"), "调查时间不能为空！");
            allowSubmit = false;
        }

        //入力值是否验证通过
        if (allowSubmit) {
            var data =
				{
				    "customerSysNo": customerSysNo,
				    "txtSat_No": $("#txtSat_No", container).val(),
				    "Sch_Context": $("#Sch_Context", container).val(),
				    "txtSch_Time": $("#txtSch_Time", container).val(),
				    "ddSat_Leve": $("#ddSat_Leve", container).val(),
				    "txtSch_Reason": $("#txtSch_Reason", container).val(),
				    "ddSat_Way": $("#ddSat_Way", container).val(),
				    "txtSat_Best": $("#txtSat_Best", container).val(),
				    "txtSat_NotBest": $("#txtSat_NotBest", container).val(),
				    "Sat_Remark": $.trim($("#Sat_Remark", container).val())
				};
            //是修改还是新规场合的判断
            if (Instance.IsEdit) {
                //修改的场合
                data.Sat_Id = Instance.SysNo;
                $.post("/Customer/SatisfactionInfo/stf_ModifySatisfactionInfo.aspx", data, function (jsonResult) {
                    if (parseInt(jsonResult, 10) > 0) {
                        //alert("用户满意度修改成功");
                        //usetype=0是修改成功加载修改页面。=1是修改成功加载添加页面的满意度列表
                        if (Instance.UseType == 0) {
                            LoadContractData();
                        } else {
                            ReloadStaticList();
                        }

                    } else {
                        alert("用户满意度修改失败！");
                    }

                });

                $("#sat_close", container).trigger("click");
            }
            else {
                //新规的场合
                $.post("/Customer/SatisfactionInfo/stf_AddSatisfactionInfo.aspx", data, function (jsonResult) {
                    if (parseInt(jsonResult, 10) > 0) {
                         // alert("用户满意度添加成功！");
                        if (Instance.UseType == 0) {
                            LoadContractData();
                        }
                        else {
                            ReloadStaticList();
                        }
                    } else {
                        alert("用户满意度添加失败！");
                    }

                });

                $("#sat_close", container).trigger("click");
            }
        }
    }

    //数据长度
    function DataLength(fData) {
        var intLength = 0;
        for (var i = 0; i < fData.length; i++) {
            if ((fData.charCodeAt(i) < 0) || (fData.charCodeAt(i) > 255))
                intLength = intLength + 2;
            else
                intLength = intLength + 1;
        }
        return intLength;
    }

    function ChangeDateFormat(time) {
        if (time != null) {
            var date = new Date(parseInt(time.replace("/Date(", "").replace(")/", ""), 10));
            var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
            var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
            return date.getFullYear() + "-" + month + "-" + currentDate;
        }
        return "";
    }
}