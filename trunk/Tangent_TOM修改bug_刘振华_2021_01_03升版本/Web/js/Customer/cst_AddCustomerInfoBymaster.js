﻿$(document).ready(function () {
    //添加用户信息
    $("#ctl00_ContentPlaceHolder1_btn_Save").click(function () {
        //取得文化框
        var txtCst_No = $("#ctl00_ContentPlaceHolder1_txtCst_No").val();
        var txtCst_Brief = $("#ctl00_ContentPlaceHolder1_txtCst_Brief").val();
        var txtCst_Name = $("#ctl00_ContentPlaceHolder1_txtCst_Name").val();
        var txtCpy_Address = $("#ctl00_ContentPlaceHolder1_txtCpy_Address").val();
        var txtCode = $("#ctl00_ContentPlaceHolder1_txtCode").val();
        var txtLinkman = $("#ctl00_ContentPlaceHolder1_txtLinkman").val();
        var txtCpy_Phone = $("#ctl00_ContentPlaceHolder1_txtCpy_Phone").val();
        var txtCpy_Fax = $("#ctl00_ContentPlaceHolder1_txtCpy_Fax").val();
        var msg = "";
        //取得年份
        var year = new Date().getFullYear();
        //设置客户默认编码规则。部门+年份+编号
        var cstNoDefault = "编号规则：部门+年份+编号，例如：" + "IS" + year + "001";
        //序号
        if (txtCst_No == "" || txtCst_No == cstNoDefault) {
            msg += "请填写客户编号！<br/>";
        }
        //客户简称
        //if (txtCst_Brief == "") {
        //    msg += "请填写客户简称！<br/>";
        //}
        if (txtCst_Name == "") {
            msg += "请填写客户名称!！<br/>"
        }
        if (txtCpy_Address == "") {
            msg += "请填写客户联系地址！<br/>";
        }

        // 判断 甲方负责人必须填写
        var sf_rows = $("#cst_datas tr").length;
        if (sf_rows == 1) {
            msg += "请添加甲方负责人信息！</br>";
        }

        if (txtCode != "") {
            var reg = /^[0-9][0-9]{5}$/;
            if (!reg.test(txtCode)) {
                msg += "邮编格式不正确！<br/>";
            }
        }
        if (txtCpy_Fax != "") {
            var reg = /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/;
            if (!reg.test(txtCpy_Fax)) {
                msg += "公司传真格式不正确！</br>";
            }
        }
        if (txtCpy_Phone != "") {
            var reg = /(^((0[1,2]{1}\d{1}-?\d{8})|(0[3-9]{1}\d{2}-?\d{7,8}))$)|(^0?(13[0-9]|15[0-35-9]|18[01236789]|14[57])[0-9]{8}$)/;
            if (!reg.test(txtCpy_Phone)) {
                msg += "请输入固定电话或手机号！</br>";
            }
        }
        //扩展信息
        var txtCst_EnglishName = $("#ctl00_ContentPlaceHolder1_txtCst_EnglishName").val();
        var txtEmail = $("#ctl00_ContentPlaceHolder1_txtEmail").val();
        var txtCpy_PrincipalSheet = $("#ctl00_ContentPlaceHolder1_txtCpy_PrincipalSheet").val();
        var txtBankAccountNo = $("#ctl00_ContentPlaceHolder1_txtBankAccountNo").val();
        var txtBankName = $("#ctl00_ContentPlaceHolder1_txtBankName").val();

        if (txtBankAccountNo != "") {
            if (isNaN(txtBankAccountNo)) {
                msg += "银行帐号必须为数字！</br>";
            } else {
                if (txtBankName == "") {
                    msg += "需要填写银行名称！</br>";
                }
            }
        }
        if (txtEmail != "") {
            var reg = /^[a-zA-Z]([a-zA-Z0-9]*[-_.]?[a-zA-Z0-9]+)+@([\w-]+\.)+[a-zA-Z]{2,}$/;
            if (!reg.test(txtEmail)) {
                msg += "邮箱格式不正确！</br>";
            }
        }
        if (txtCpy_PrincipalSheet != "") {
            var strregex = "^((https|http|ftp|rtsp|mms)?://)"
                    + "?(([0-9a-za-z_!~*'().&=+$%-]+: )?[0-9a-za-z_!~*'().&=+$%-]+@)?"
                    + "(([0-9]{1,3}.){3}[0-9]{1,3}"
                    + "|"
                    + "([0-9a-za-z_!~*'()-]+.)*"
                    + "([0-9a-za-z][0-9a-za-z-]{0,61})?[0-9a-za-z]."
                    + "[a-za-z]{2,6})"
                    + "(:[0-9]{1,4})?"
                    + "((/?)|"
                    + "(/[0-9a-za-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
            var re = new RegExp(strregex);
            if (!re.test(txtCpy_PrincipalSheet)) {
                msg += "公司主页格式不正确！</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        } else {
            //获取所有Hidden
            var hiddenArr = $(":hidden", $("#gv_Attach"));
            $.each(hiddenArr, function (index, item) {

                var value = $("#ctl00_ContentPlaceHolder1_attachHidden").val() + $(item).val() + ",";
                $("#ctl00_ContentPlaceHolder1_attachHidden").val(value);
            });
            $(this).hide();
            return true;
        }
    });
    //行变色

    //联系人信息
    var contact = new Contact(null, $("#JFresponsive"));
    $("#addLinkManActionLink").click(function () {
        contact.Clear();
        contact.IsEdit = false;
        $("h4", "#JFresponsive").text("添加甲方负责人");
    });
    $("#btnJFAdd").click(function () {
        contact.usetype = 1;
        contact.SaveContact("#addLinkManActionLink");
    });

    $("span[class=update]").live("click", function () {

        var cid = $(this).attr("rel");
        contact.Clear();
        contact.IsEdit = true;
        contact.GetContactInfo(cid);
        contact.usetype = 1;
        contact.SysNo = cid;
        ClearTipSpan();
        $("h4", "#JFresponsive").text("编辑甲方负责人");
        //        $("#btnJFAdd").click(function () {
        //            contact.SaveContact("#addLinkManActionLink");
        //        })     
    });



    var satisfactionInfo = new SatisfactionInfo(null, $("#MYresponsive"));

    $("#addSatisfactionActionLink").click(function () {
        satisfactionInfo.Clear();
        satisfactionInfo.IsEdit = false;
        $("h4", "#JFresponsive").text("添加用户满意度");
    });

    $("#btnSaveSatisfaction").click(function () {
        satisfactionInfo.UseType = 1;
        satisfactionInfo.SaveSatisfaction("#addSatisfactionActionLink");
    })
    $("span[id=update]").live("click", function () {

        var sid = $(this).attr("rel");
        satisfactionInfo.IsEdit = true;
        satisfactionInfo.Clear();
        satisfactionInfo.UseType = 1;
        satisfactionInfo.SysNo = sid;
        satisfactionInfo.GetSatisfactionInfo(sid);
        ClearTipSpan();
        $("h4", "#MYresponsive").text("编辑用户满意度");
        //        $("#btnSaveSatisfaction").click(function () {
        //            satisfactionInfo.SaveSatisfaction("#addSatisfactionActionLink");
        //        })
    });

    //取得年份
    var year = new Date().getFullYear();
    //设置客户默认编码规则。部门+年份+编号
    var cstNoDefault = "编号规则：部门+年份+编号，例如：" + "IS" + year + "001";
    $("#ctl00_ContentPlaceHolder1_txtCst_No").val(cstNoDefault);
    //客户编号
    $("#ctl00_ContentPlaceHolder1_txtCst_No").css("color", "red");
    $("#ctl00_ContentPlaceHolder1_txtCst_No").blur(function () { if ($.trim($(this).val()) == "") { $(this).val(cstNoDefault); $(this).css("color", "red"); } });
    $("#ctl00_ContentPlaceHolder1_txtCst_No").focus(function () { if ($.trim($(this).val()) == cstNoDefault) { $(this).val(""); } $(this).css("color", "black"); });
    CommonControl.SetFormWidth();
});
//判断汉子的实际长度
function DataLength(fData) {
    var intLength = 0;
    for (var i = 0; i < fData.length; i++) {
        if ((fData.charCodeAt(i) < 0) || (fData.charCodeAt(i) > 255))
            intLength = intLength + 2;
        else
            intLength = intLength + 1;
    }
    return intLength;
}

//附件删除事件
function AttachDetele(tr) {
    if (confirm("确认是否删除附件信息?")) {
        //获取当前系统自增号
        var sysNo = $(tr).parents("tr:first").children("td:first").children("input").val();
        $.post("/HttpHandler/ProcessUoloadFile.ashx", { "Action": "1", "SysNo": sysNo }, function (jsonResult) {
            if (jsonResult == "1") {
                //服务端成功删除的场合
                $("tr[id=" + sysNo + "]", $("#gv_Attach")).remove();
            }
        });
    }
}
//创建提示信息Span
function CreateTipSpan(element, tipContent) {
    //element.parent("td:first").append("<span id=\"TipSpan\" style=\"color:red;\">" + tipContent + "</span>");
    alert(tipContent);
}
//清除提示信息Span 
function ClearTipSpan(container) {
    $("span[id=TipSpan]").remove();
}
//js 加载联系人信息
function ReloadCustomerList() {
    var data = "action=getcstinfo&cstno=" + $("#ctl00_ContentPlaceHolder1_HiddenCustomerSysNo").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            var data = result.ds;
            if ($("#cst_datas tr").length > 1) {
                $("#cst_datas tr:gt(0)").remove();
            }
            $.each(data, function (i, n) {
                var row = $("#cst_row").clone();
                var oper = "<span rel='" + n.CP_Id + "' class='update' data-toggle=\"modal\" href=\"#JFresponsive\" style='cursor: pointer;color:blue;'>编辑</span>&nbsp;&nbsp;<span rel='" + n.CP_Id + "' class='del' style='cursor: pointer;color:blue;' name='" + n.Name + "'>删除</span>";

                row.find("#id").text(n.ContactNo);
                row.find("#name").text(n.Name);
                row.find("#zhiwu").text(n.Duties);
                row.find("#bumen").html(n.Department);
                row.find("#phone").html(n.Phone);
                row.find("#email").html(n.Email);
                row.find("#oper").html(oper);
                var reMark = n.Remark;
                if (reMark.length > 20) {
                    reMark = reMark.substr(0, 20) + "...";
                }
                row.find("#remark").attr("title", n.Remark);
                row.find("#remark").html(reMark); //新增,备注列
                row.find("#oper span[class=del]").click(function () {
                    if (confirm("确定要删除甲方负责人吗？")) {
                        //删除事件
                        delLinkMan($(this));
                    }
                });
                row.addClass("cls_TableRow");
                row.appendTo("#cst_datas");
            });

            data = "";

            if (result == "0") {
                $("#linkman_nodata").show();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//删除联系人
function delLinkMan(link) {

    var data = "action=dellinkinfo&linkid=" + link.attr("rel") + "&name=" + link.attr("name");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        dataType: "text",
        data: data,
        success: function (result) {
            if (result == "yes") {
                //删除本行数据
                link.parents("tr:first").remove();
                // alert("删除联系人成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//添加满意度
function ReloadStaticList() {
    var data = "action=getstainfo&stano=" + $("#ctl00_ContentPlaceHolder1_HiddenCustomerSysNo").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            var data = result.ds;
            if ($("#static_datas tr").length > 1) {
                $("#static_datas tr:gt(0)").remove();
            }
            $.each(data, function (i, n) {
                var row = $("#static_row").clone();
                var oper = "<span data-toggle=\"modal\" href=\"#MYresponsive\" style='cursor: pointer;color:blue;' rel='" + n.Sat_Id + "'  id='update'>编辑</span>&nbsp;&nbsp;<span style='cursor: pointer;color:blue;' rel='" + n.Sat_Id + "' id='del' name='" + n.Sch_Context + "'>删除</span>";

                row.find("#st_id").text(n.Sat_No);
                row.find("#st_content").text(n.Sch_Context);
                row.find("#st_myd").text(n.Sat_Leve);
                row.find("#st_time").html(n.Sch_Time);
                row.find("#st_type").html(n.Sch_Way);
                var reMark = n.Remark;
                if (reMark.length > 20) {
                    reMark = reMark.substr(0, 20) + "...";
                }
                row.find("#st_remark").attr("title", n.Remark);
                row.find("#st_remark").html(reMark); //新增,加载满意度备注
                row.find("#st_oper").html(oper);
                row.find("#st_oper span[id=del]").click(function () {
                    if (confirm("确定要删除满意度调查吗？")) {
                        //删除事件
                        delStatic($(this));
                    }
                });
                row.addClass("cls_TableRow");
                row.appendTo("#static_datas");
            });
            if (result == "0") {
                $("#static_nodata").show();
            }
            data = "";
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}
//删除满意方式
function delStatic(link) {
    var data = "action=delsta&staid=" + link.attr("rel") + "&name=" + link.attr("name");;
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        dataType: "text",
        data: data,
        success: function (result) {
            if (result == "yes") {
                //删除本行数据
                link.parents("tr:first").remove();
                // alert("删除满意度成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误！");
        }
    });
}