﻿function Contact(parent, container) {
    this.Parnet = parent;
    this.Container = container;
    this.Dom = {};
    this.IsEdit = false;
    this.SysNo = 0;
    this.usetype = 0;
    var Instance = this;

    this.Clear = function () {
        $(":text", container).val("");
        $.each($("select", container), function (index, item) {
            item.selectedIndex = 0;
        });
        $("textarea", container).val("");
    }

    //得到联系人信息
    this.GetContactInfo = function (cid) {
        $.get("/Customer/ContactPersionInfo/cp_ModifyContactPersionInfo.aspx", { "cid": cid, "n": Math.random() }, function (jsonResult) {
            if (jsonResult) {
                var dataObj = Global.evalJSON(jsonResult);
                $("#txtContactNo", container).val($.trim(dataObj.ContactNo));
                $("#txtName", container).val($.trim(dataObj.Name));
                $("#txtDuties", container).val($.trim(dataObj.Duties));
                $("#txtPhone", container).val($.trim(dataObj.Phone));
                $("#txtDepartment", container).val($.trim(dataObj.Department));
                $("#txtBPhone", container).val($.trim(dataObj.BPhone));
                $("#txtFPhone", container).val($.trim(dataObj.FPhone));
                $("#txtBFax", container).val($.trim(dataObj.BFax));
                $("#txtFFax", container).val($.trim(dataObj.FFax));
                $("#LinkManEmail", container).val($.trim(dataObj.Email));
                $("#LinkManRemark", container).val($.trim(dataObj.Remark));
                Instance.SysNo = dataObj.CP_Id;
            } else {
                alert("获取联系人信息失败！");
            }
        });
    }

    //保存联系人信息
    this.SaveContact = function (dialog) {
        var allowSubmit = true;
        ClearTipSpan();
        var txtName = $("#txtName").val();
        var txtDuties = $("#txtDuties").val();
        var txtPhone = $("#txtPhone").val();
        var txtDepartment = $("#txtDepartment").val();
        var txtBPhone = $("#txtBPhone").val();
        var txtFPhone = $("#txtFPhone").val();
        var txtBFax = $("#txtBFax").val();
        var txtEmail = $("#LinkManEmail").val();
        var txtFFax = $("#txtFFax").val();
        var txtContactNo = $("#txtContactNo").val();
        //验证
        if (txtContactNo == "") {
            allowSubmit = false;
            CreateTipSpan($("#txtContactNo", container), "序号不为空！");
        }
        if (txtName == "") {
            allowSubmit = false;
            CreateTipSpan($("#txtName", container), "姓名不能为空！");
        }
        if (txtPhone == "") {
            allowSubmit = false;
            CreateTipSpan($("#txtPhone", container), "电话不能为空！");
        } else {
            var reg = /^1\d{10}$/;
            var regexp = /(^[0-9]{3,4}\-[0-9]{7,8}$)|(^[0-9]{7,8}$)|(^\([0-9]{3,4}\)[0-9]{3,8}$)|(^0{0,1}13[0-9]{9}$)/;
            if (!reg.test(txtPhone)) {
                if (!regexp.test(txtPhone)) {
                    allowSubmit = false;
                    CreateTipSpan($("#txtPhone", container), "电话格式错误！");
                }
            }
        }
        if (txtBPhone != "") {
            var reg = /(^[0-9]{3,4}\-[0-9]{7,8}$)|(^[0-9]{7,8}$)|(^\([0-9]{3,4}\)[0-9]{3,8}$)|(^0{0,1}13[0-9]{9}$)/;
            if (!reg.test(txtBPhone)) {
                allowSubmit = false;
                CreateTipSpan($("#txtBPhone", container), "商务电话格式错误！");
            }
        }
        if (txtFPhone != "") {
            var reg = /(^[0-9]{3,4}\-[0-9]{7,8}$)|(^[0-9]{7,8}$)|(^\([0-9]{3,4}\)[0-9]{3,8}$)|(^0{0,1}13[0-9]{9}$)/;
            if (!reg.test(txtFPhone)) {
                allowSubmit = false;
                CreateTipSpan($("#txtFPhone", container), "住宅电话格式错误！");
            }
        }
        if (txtBFax != "") {
            var patrn = /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/;
            if (!patrn.test(txtBFax)) {
                allowSubmit = false;
                CreateTipSpan($("#txtBFax", container), "商务传真格式错误！");
            }
        }
        if (txtFFax != "") {
            var patrn = /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/;
            if (!patrn.test(txtFFax)) {
                allowSubmit = false;
                CreateTipSpan($("#txtFFax", container), "住宅传真格式错误！");
            }
        }
        if (txtEmail != "") {
            var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
            if (!reg.test(txtEmail)) {
                allowSubmit = false;
                CreateTipSpan($("#LinkManEmail", container), "Email格式错误！");
            }
        }

        //是否允许提交保存
        if (allowSubmit) {

            var customerSysNo = $("#ctl00_ContentPlaceHolder1_HiddenCustomerSysNo").val();
            var data =
				{
				    "customerSysNo": customerSysNo,
				    "txtContactNo": $("#txtContactNo", container).val(),
				    "txtName": $("#txtName", container).val(),
				    "txtDuties": $("#txtDuties", container).val(),
				    "txtPhone": $("#txtPhone", container).val(),
				    "txtDepartment": $("#txtDepartment", container).val(),
				    "txtBPhone": $("#txtBPhone", container).val(),
				    "txtFPhone": $("#txtFPhone", container).val(),
				    "txtBFax": $("#txtBFax", container).val(),
				    "txtFFax": $("#txtFFax", container).val(),
				    "LinkManEmail": $("#LinkManEmail", container).val(),
				    "LinkManRemark": $.trim($("#LinkManRemark", container).val())
				};
            if (Instance.IsEdit) {
                //修改的场合
                data.Cp_ID = Instance.SysNo;
                $.post("/Customer/ContactPersionInfo/cp_ModifyContactPersionInfo.aspx", data, function (jsonResult) {
                    if (parseInt(jsonResult, 10) > 0) {
                        // alert("修改联系人信息成功！");
                        //by qing 20131029
                        if (Instance.usetype == 0) {
                            LoadLinkManData();
                        }
                        else {
                            ReloadCustomerList();
                        }

                    } else {
                        alert("修改联系人信息失败！");
                    }

                });

                $("#jf_close", container).trigger("click");
            }
            else {
                //新规的场合
                $.post("/Customer/ContactPersionInfo/cp_AddContactPersionInfo.aspx", data, function (jsonResult) {
                    if (parseInt(jsonResult, 10) > 0) {
                        //alert("联系人添加成功！");
                        if (Instance.usetype == 0) {
                            LoadLinkManData();
                        }
                        else {
                            ReloadCustomerList();
                        }
                    } else {
                        alert("联系人添加失败！");
                    }

                });

                $("#jf_close", container).trigger("click");
            }
        }
    }
}