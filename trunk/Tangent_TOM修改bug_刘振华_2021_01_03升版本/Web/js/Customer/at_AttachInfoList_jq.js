﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Customer/AttachInfoListHandler.ashx?strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '文件名', '上传时间', '文件大小', '文件类型', '用户', '录入时间', '客户', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'ID', index: 'ID', hidden: true, editable: true },
                             { name: 'FileTypeImg', index: 'FileTypeImg', hidden: true, editable: true },
                             { name: 'FileName', index: 'FileName', width: 273, formatter: colTBFormatter },
                             { name: 'UploadTime', index: 'UploadTime', width: 200, align: 'center' },
                             { name: 'wjdx', index: 'FileSize', width: 100, align: 'center', formatter: colKBFormatter },
                             { name: 'FileType', index: 'FileType', width: 100, align: 'center' },
                             { name: 'yh', index: 'UploadUser', width: 200, align: 'center' },
                               { name: 'lrsj', index: 'UploadTime', width: 80, align: 'center' },
                             { name: 'Cst_Id', index: 'Cst_Id', width: 50, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                             { name: 'ID', index: 'ID', width: 50, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'AT.ID',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Customer/AttachInfoListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid').jqGrid('getCell', sel_id[i], 'ID') + ",";
                            }
                        }

                        //var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        //var value = $('#jqGrid').jqGrid('getCell', sel_id, 'ID');
                        return values;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_Search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_name").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Customer/AttachInfoListHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'keyname': keyname, "startTime": startTime, "endTime": endTime },
            page: 1,
            sortname: 'AT.ID',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });


});
//增加文件图标
function colTBFormatter(celvalue, options, rowData) {
    var tb = '<img style="height: 16px; width: 16px;" src="/' + rowData.FileTypeImg + '" />';
    return tb + celvalue;
}
//增加文件类型
function colKBFormatter(celvalue, options, rowData) {
    return celvalue + 'KB';
}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "cst_ShowCustomerInfoByMaster.aspx?Cst_Id=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看客户">查看</a>';

}
//查看联系人
function colEditFormatter(celvalue, options, rowdata) {
    var pageurl = "/Customer/DownLoadFile.aspx?SysNo=" + celvalue;
    return '<a href="' + pageurl + '" alt="下载">下载</a>';
}
//统计 
function completeMethod() {
    //更改序号
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {

    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {

        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}