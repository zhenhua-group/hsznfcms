﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Customer/CustomerInfoHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '', '客户编号', '客户名称', '联系人 ', '联系电话', '公司地址', '邮政编码', '传真号', '客户简称', '客户英文名', '是否合作', '所在国家', '所在省份', '所在城市', '客户类型', '所属行业', '分支机构', '邮箱', '公司主页', '关系建立时间', '关系部门', '信用级别', '亲密度', '开户银行名称', '企业代码', '开户银行账号', '法定代表', '纳税人识别号', '备注', '录入人', '录入时间', '', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'Cst_Id', index: 'Cst_Id', hidden: true, editable: true },
                             {
                                 name: 'Cst_No', index: 'Cst_No', width: 100, align: 'center', formatter: function (celvalue, options, rowData) {
                                     return $.trim(celvalue);
                                 }
                             },
                             { name: 'Cst_Name', index: 'Cst_Name', width: 200, formatter: colNameShowFormatter },
                             { name: 'Linkman', index: 'Linkman', width: 60, align: 'center' },
                             { name: 'Cpy_Phone', index: 'Cpy_Phone', width: 100, align: 'center' },

                             { name: 'Cpy_Address', index: 'Cpy_Address', width: 150 },
                             { name: 'Code', index: 'Code', width: 80, align: 'center' },
                             { name: 'Cpy_Fax', index: 'Cpy_Fax', width: 80, align: 'center' },
                             { name: 'Cst_Brief', index: 'Cst_Brief', width: 80, align: 'center', hidden: true },
                             { name: 'Cst_EnglishName', index: 'Cst_EnglishName', width: 80, align: 'center', hidden: true },
                             { name: 'PartnerName', index: 'IsPartner', width: 80, align: 'center', hidden: true },
                             { name: 'Country', index: 'Country', width: 80, align: 'center', hidden: true },
                             { name: 'Province', index: 'Province', width: 80, align: 'center', hidden: true },
                             { name: 'City', index: 'City', width: 80, align: 'center', hidden: true },
                             { name: 'TypeName', index: 'Type', width: 80, align: 'center', hidden: true },
                             { name: 'ProfessionName', index: 'Profession', width: 80, align: 'center', hidden: true },
                              { name: 'BranchPart', index: 'BranchPart', width: 80, align: 'center', hidden: true },
                             { name: 'Email', index: 'Email', width: 80, align: 'center', hidden: true },
                             { name: 'Cpy_PrincipalSheet', index: 'Cpy_PrincipalSheet', width: 80, align: 'center', hidden: true },
                              { name: 'RelationTime', index: 'CreateRelationTime', width: 100, align: 'center', hidden: true },
                             { name: 'RelationDepartment', index: 'RelationDepartment', width: 80, align: 'center', hidden: true },
                             { name: 'CreditLeveName', index: 'CreditLeve', width: 80, align: 'center', hidden: true },
                             { name: 'CloseLeveName', index: 'CloseLeve', width: 80, align: 'center', hidden: true },
                             { name: 'BankName', index: 'BankName', width: 100, align: 'center', hidden: true },
                             { name: 'Cpy_Code', index: 'Cpy_Code', width: 80, align: 'center', hidden: true },
                             { name: 'BankAccountNo', index: 'BankAccountNo', width: 100, align: 'center', hidden: true },
                             { name: 'LawPerson', index: 'LawPerson', width: 80, align: 'center', hidden: true },
                             { name: 'TaxAccountNo', index: 'TaxAccountNo', width: 100, align: 'center', hidden: true },
                             { name: 'Remark', index: 'Remark', width: 80, align: 'center', hidden: true },
                              { name: 'InsertUser', index: 'InsertUserID', width: 60, align: 'center' },
                             { name: 'lrsj', index: 'InsertDate', width: 80, align: 'center' },
                             { name: 'Cst_Id', index: 'Cst_Id', width: 50, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                             { name: 'Cst_Id', index: 'Cst_Id', width: 50, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": $("#ctl00_ContentPlaceHolder1_hid_where").val(), "action": "sel", "unit": $("#ctl00_ContentPlaceHolder1_drp_unit").val(), "year": $("#ctl00_ContentPlaceHolder1_drp_year").val(), "keyname": $("#ctl00_ContentPlaceHolder1_txt_keyname").val(), "startTime": $("#ctl00_ContentPlaceHolder1_txt_start").val(), "endTime": $("#ctl00_ContentPlaceHolder1_txt_end").val() },
        loadonce: false,
        sortname: 'c.cst_id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Customer/CustomerInfoHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid').jqGrid('getCell', sel_id[i], 'Cst_Id') + ",";
                            }
                        }

                        return values;
                    }
                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        getSearchData();
    });
    $("#btn_Add").click(function () {
        window.location.href = "cst_AddCustomerInfoByMaster2.aspx";
    });

    //点击全部
    $(":checkbox:first", $("#columnsid")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_customer", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid")).click(function () {
        var checkedstate = $(this).parent().attr("class");
        var columnsname = $(this).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid")).length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid").hideCol(columnsname);
        }

        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_customer", columnslist, { expires: 365, path: "/" });

    });

    //导出
    $("#ctl00_ContentPlaceHolder1_btn_Output").click(function () {
        var values = "";
        $(":checkbox:checked", $("#columnsid")).each(function () {
            values += $.trim($(this).attr("name")) + ",";

        });
        if (values != "") {
            values = values.substr(0, (values.length - 1));
            $("#ctl00_ContentPlaceHolder1_hid_ColumnsID").val(values);
        }


    });

    //点击是否
    $("label[rel]").click(function () {
        $(this).addClass("yellow active");
        $(this).removeClass("default");
        $(this).siblings("label").removeClass("active yellow");
        $(this).siblings("label").addClass("default");
        andor = $(this).attr("rel");

    });

    //点击查询字段-全选
    $(":checkbox:first", $("#columnsid_sh")).click(function () {
        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid_sh")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);

                var columnsname = $(value).val();
                if (columnsname != 'Cst_Name' && columnsname != 'lrsj' && columnsname != 'Cst_No' && columnsname != 'Linkman') {

                    var tr_length = $("#tbl_id tr").length;
                    var tb_length = $("#tbl_id td").length;
                    if (tr_length == (tb_length / 6)) {
                        $("#tbl_id").append("<tr></tr>");
                        $("#tbl_id tr:eq(" + tr_length + ")").append($("#tb_serchDetail tr[rel='" + columnsname + "']").children());
                    }
                    else {
                        $("#tbl_id tr:eq(" + (tr_length - 1) + ")").append($("#tb_serchDetail tr[rel='" + columnsname + "']").children());
                    }
                }

            });

        }
        else {
            //把选中的数据还原回去
            $("td", "#tbl_id tr:gt(1)").each(function () {
                var tdrel = $(this).attr("rel");
                if (tdrel != "" && tdrel != undefined && tdrel != null) {
                    var $data_next = $(this).next();
                    $("#tb_serchDetail tr[rel=" + tdrel + "]").append($("#tbl_id td[rel='" + tdrel + "']"));
                    $("#tb_serchDetail tr[rel=" + tdrel + "]").append($data_next);
                }
            });

            $("#tbl_id tr:gt(1)").remove();

            //清空数据
            $("input", "#tb_serchDetail").val("");
            $("select", "#tb_serchDetail").val("-1");

            $(":checkbox:not(:first)", $("#columnsid_sh")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);

            });

        }

    });

    //点击查询字段
    $(":checkbox:not(:first)", $("#columnsid_sh")).click(function () {
        var checkedstate = $(this).parent().attr("class");
        var columnsname = $(this).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid_sh")).length) == ($(":checkbox:not(:first)", $("#columnsid_sh")).length)) {
            $(":checkbox:first", $("#columnsid_sh")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid_sh")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid_sh")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid_sh")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            if (columnsname != 'Cst_Name' && columnsname != 'lrsj' && columnsname != 'Cst_No' && columnsname != 'Linkman') {

                var tr_length = $("#tbl_id tr").length;
                var tb_length = $("#tbl_id td").length;
                if (tr_length == (tb_length / 6)) {
                    $("#tbl_id").append("<tr ></tr>");
                    $("#tbl_id tr:eq(" + tr_length + ")").append($("#tb_serchDetail tr[rel='" + columnsname + "']").children());
                }
                else {
                    $("#tbl_id tr:eq(" + (tr_length - 1) + ")").append($("#tb_serchDetail tr[rel='" + columnsname + "']").children());
                }
            }
        }
        else {
            $(this).attr("checked", false);

            //把选中的数据还原回去
            $("td", "#tbl_id tr:gt(1)").each(function () {
                var tdrel = $(this).attr("rel");
                if (tdrel != "" && tdrel != undefined && tdrel != null) {
                    //还原数据
                    var $data_next = $(this).next();
                    $("#tb_serchDetail tr[rel=" + tdrel + "]").append($("#tbl_id td[rel='" + tdrel + "']"));
                    $("#tb_serchDetail tr[rel=" + tdrel + "]").append($data_next);
                }
            });

            //清空数据
            $("input", "#tb_serchDetail tr[rel=" + columnsname + "]").val("");
            $("select", "#tb_serchDetail tr[rel=" + columnsname + "]").val("-1");
            $("#tbl_id tr:gt(1)").remove();
            //重新排列
            $(":checkbox:not(:first):checked", "#columnsid_sh").each(function () {
                var columnsname = $(this).val();
                if (columnsname != 'Cst_Name' && columnsname != 'lrsj' && columnsname != 'Cst_No' && columnsname != 'Linkman') {

                    var tr_length = $("#tbl_id tr").length;
                    var tb_length = $("#tbl_id td").length;
                    if (tr_length == (tb_length / 6)) {
                        $("#tbl_id").append("<tr style=\"line-height: 40px;\"></tr>");
                        $("#tbl_id tr:eq(" + tr_length + ")").append($("#tb_serchDetail tr[rel='" + columnsname + "']").children());
                    }
                    else {
                        $("#tbl_id tr:eq(" + (tr_length - 1) + ")").append($("#tb_serchDetail tr[rel='" + columnsname + "']").children());
                    }
                }
            });
        }

    });

    //初始化显示cookies字段列
    InitColumnsCookies();


    //导出
    $("#ctl00_ContentPlaceHolder1_btn_Output").click(function () {

        var columnslist = new Array();
        var columnsChinaNameList = new Array();

        $(":checkbox", $("#columnsid")).each(function () {
            if ($(this).parent().attr("class") == "checked") {
                var name = $(this).attr("name");
                var val = $(this).val();
                if (name != undefined) {
                    columnsChinaNameList.push(name);
                }
                if (val != "all") {
                    columnslist.push(val);
                }
            }
        });


        //导出显示的字段
        var str_columnsname = "";
        var str_columnschinaname = "";
        if (columnslist != null && columnslist.length > 0) {
            for (var i = 0; i < columnslist.length; i++) {
                var arr = $.trim(columnslist[i]);
                var arr_china = $.trim(columnsChinaNameList[i]);
                str_columnsname = str_columnsname + arr + ",";
                str_columnschinaname = str_columnschinaname + arr_china + ",";
            }
        }
        else {
            alert("请选择要导出的字段！");
            return false;
        }

        if (str_columnsname != "") {
            str_columnsname = str_columnsname.substring(str_columnsname, (str_columnsname.length - 1));
        }
        if (str_columnschinaname != "") {
            str_columnschinaname = str_columnschinaname.substring(str_columnschinaname, (str_columnschinaname.length - 1));
        }



        //声明查询方式
        var andor = "and";
        $("label[rel]", $("#div_sch")).each(function () {
            if ($(this).hasClass("active")) {
                andor = $.trim($(this).attr("rel"));
            }
        });

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //部门
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        //年份
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        //合同名称
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        //录入开始时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        //录入时间结束
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        //客户编码
        var custNo = $("#ctl00_ContentPlaceHolder1_txtCst_No").val();
        //联系人
        var linkMan = $("#ctl00_ContentPlaceHolder1_txtLinkman").val();
        //联系电话
        var linkPhone = $("#ctl00_ContentPlaceHolder1_txtCpy_Phone").val();
        //公司地址
        var address = $("#ctl00_ContentPlaceHolder1_txtCpy_Address").val();
        //邮政编码
        var code = $("#ctl00_ContentPlaceHolder1_txtCode").val();
        //传真号
        var cpyFax = $("#ctl00_ContentPlaceHolder1_txtCpy_Fax").val();
        //录入人
        var insertName = $("#ctl00_ContentPlaceHolder1_txt_InsertName").val();
        //客户简称
        var cst_Brief = $("#ctl00_ContentPlaceHolder1_txtCst_Brief").val();
        //客户英文名称
        var cstEnglishName = $("#ctl00_ContentPlaceHolder1_txtCst_EnglishName").val();
        //是否合作
        var isPartner = $("#ctl00_ContentPlaceHolder1_ddIsPartner").val();
        //所在国家
        var country = $("#ctl00_ContentPlaceHolder1_txt_Country").val();
        //所在省份
        var province = $("#ctl00_ContentPlaceHolder1_txtProvince").val();
        //所在城市
        var city = $("#ctl00_ContentPlaceHolder1_txtCity").val();
        //客户类型
        var cstType = $("#ctl00_ContentPlaceHolder1_ddType").val();
        //所属行业
        var profession = $("#ctl00_ContentPlaceHolder1_ddProfession").val();
        //分支机构
        var branchPart = $("#ctl00_ContentPlaceHolder1_txtBranchPart").val();
        //邮箱
        var email = $("#ctl00_ContentPlaceHolder1_txtEmail").val();
        //公司主页
        var principalSheet = $("#ctl00_ContentPlaceHolder1_txtCpy_PrincipalSheet").val();
        //关系建立时间
        var date_Start = $("#ctl00_ContentPlaceHolder1_txt_date_Start").val();
        //关系建立时间
        var date_End = $("#ctl00_ContentPlaceHolder1_txt_date_End").val();
        //关系部门
        var relationDepartment = $("#ctl00_ContentPlaceHolder1_txtRelationDepartment").val();
        //信用级别
        var creditLeve = $("#ctl00_ContentPlaceHolder1_ddCreditLeve").val();
        //亲密度
        var closeLeve = $("#ctl00_ContentPlaceHolder1_ddCloseLeve").val();
        //开户银行
        var bankName = $("#ctl00_ContentPlaceHolder1_txtBankName").val();
        //企业代码
        var cpy_Code = $("#ctl00_ContentPlaceHolder1_txtCpy_Code").val();
        //开户银行账号
        var bankAccountNo = $("#ctl00_ContentPlaceHolder1_txtBankAccountNo").val();
        //法定代表
        var lawPerson = $("#ctl00_ContentPlaceHolder1_txtLawPerson").val();
        //纳税人识别号
        var taxAccountNo = $("#ctl00_ContentPlaceHolder1_txtTaxAccountNo").val();
        //备注
        var remark = $("#ctl00_ContentPlaceHolder1_txtRemark").val();

        window.location.href = '/Customer/ExportToExcel.aspx?&andor=' + andor + '&strwhere=' + strwhere + '&unit=' + unit + '&keyname=' + keyname + '&year=' + year + '&startTime=' + startTime + '&endTime=' + endTime + '&custNo=' + custNo + '&linkMan=' + linkMan + ' &linkPhone=' + linkPhone + '&address=' + address + '&code=' + code + '&cpyFax=' + cpyFax + '&insertName=' + insertName + '&cst_Brief=' + cst_Brief + ' &cstEnglishName=' + cstEnglishName + '&isPartner=' + isPartner + '&country=' + country + '&province=' + province + '&city=' + city + '&cstType=' + cstType + ' &profession=' + profession + '&branchPart=' + branchPart + '&email=' + email + '&principalSheet=' + principalSheet + '&date_Start=' + date_Start + ' &date_End=' + date_End + '&relationDepartment=' + relationDepartment + '&creditLeve=' + creditLeve + '&closeLeve=' + closeLeve + '&bankName=' + bankName + '&cpy_Code=' + cpy_Code + '&bankAccountNo=' + bankAccountNo + '&lawPerson=' + lawPerson + '&taxAccountNo=' + taxAccountNo + '&remark=' + remark + '&str_columnsname=' + str_columnsname + '&str_columnschinaname=' + str_columnschinaname + '';


        return false;

    });
});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "cst_ShowCustomerInfoByMaster.aspx?Cst_Id=" + rowData["Cst_Id"];
    return '<a href="' + pageurl + '" alt="查看客户">' + celvalue + '</a>';

}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "cst_ShowCustomerInfoByMaster.aspx?Cst_Id=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看客户">查看</a>';

}
//编辑
function colEditFormatter(celvalue, options, rowdata) {
    if (celvalue != "") {
        var pageurl = "cst_ModifyCustomerInfoByMaster.aspx?Cst_Id=" + celvalue;
        return '<a href="' + pageurl + '" alt="编辑客户"class="allowEdit" >编辑</a>';
    } //class="allowEdit"
}
//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }

}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {

        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        }
        else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}
//初始化显示cookies字段列
var InitColumnsCookies = function () {
    //基本合同列表
    var columnslist = $.cookie("columnslist_customer");
    //cookie已有保存 
    if (columnslist != null && columnslist != undefined && columnslist != "undefined" && columnslist != "") {

        var list = columnslist.split(",");
        //  $.each(list, function (i, c) {
        $(":checkbox:not(:first)", $("#columnsid")).each(function (j, n) {
            //包含cookie中显示字段并选中
            if (list.indexOf($.trim($(n).val())) > -1) {
                var span = $(n).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(n).attr("checked", true);
                $("#jqGrid").showCol($(n).val());
            }
            else {
                $("#jqGrid").hideCol($(n).val());
            }

        });
        //  });
        //是否需要选中 全选框
        if ((list.length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }

    }
    else {
        //cookie未保存，显示默认的几个字段
        var list = new Array("Cst_No", "Cst_Name", "Linkman", "Cpy_Phone", "Cpy_Address", "Code", "Cpy_Fax", "InsertUser", "lrsj");//直接定义并初始化
        $.each(list, function (i, c) {
            $(":checkbox:not(:first)[value='" + c + "']", $("#columnsid")).each(function (j, n) {
                var span = $(n).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(n).attr("checked", true);
                // $("#jqGrid").showCol($(n).val());
            });

        });

    }
}


function getSearchData() {
    //声明查询方式
    var andor = "and";
    $("label[rel]", $("#div_sch")).each(function () {
        if ($(this).hasClass("active")) {
            andor = $.trim($(this).attr("rel"));
        }
    });

    var strwhere = $("#ctl00_ContentPlaceHolder1_hid_where").val();
    //部门
    var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
    //年份
    var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    //合同名称
    var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
    //录入开始时间
    var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
    //录入时间结束
    var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
    //客户编码
    var custNo = $("#ctl00_ContentPlaceHolder1_txtCst_No").val();
    //联系人
    var linkMan = $("#ctl00_ContentPlaceHolder1_txtLinkman").val();
    //联系电话
    var linkPhone = $("#ctl00_ContentPlaceHolder1_txtCpy_Phone").val();
    //公司地址
    var address = $("#ctl00_ContentPlaceHolder1_txtCpy_Address").val();
    //邮政编码
    var code = $("#ctl00_ContentPlaceHolder1_txtCode").val();
    //传真号
    var cpyFax = $("#ctl00_ContentPlaceHolder1_txtCpy_Fax").val();
    //录入人
    var insertName = $("#ctl00_ContentPlaceHolder1_txt_InsertName").val();
    //客户简称
    var cst_Brief = $("#ctl00_ContentPlaceHolder1_txtCst_Brief").val();
    //客户英文名称
    var cstEnglishName = $("#ctl00_ContentPlaceHolder1_txtCst_EnglishName").val();
    //是否合作
    var isPartner = $("#ctl00_ContentPlaceHolder1_ddIsPartner").val();
    //所在国家
    var country = $("#ctl00_ContentPlaceHolder1_txt_Country").val();
    //所在省份
    var province = $("#ctl00_ContentPlaceHolder1_txtProvince").val();
    //所在城市
    var city = $("#ctl00_ContentPlaceHolder1_txtCity").val();
    //客户类型
    var cstType = $("#ctl00_ContentPlaceHolder1_ddType").val();
    //所属行业
    var profession = $("#ctl00_ContentPlaceHolder1_ddProfession").val();
    //分支机构
    var branchPart = $("#ctl00_ContentPlaceHolder1_txtBranchPart").val();
    //邮箱
    var email = $("#ctl00_ContentPlaceHolder1_txtEmail").val();
    //公司主页
    var principalSheet = $("#ctl00_ContentPlaceHolder1_txtCpy_PrincipalSheet").val();
    //关系建立时间
    var date_Start = $("#ctl00_ContentPlaceHolder1_txt_date_Start").val();
    //关系建立时间
    var date_End = $("#ctl00_ContentPlaceHolder1_txt_date_End").val();
    //关系部门
    var relationDepartment = $("#ctl00_ContentPlaceHolder1_txtRelationDepartment").val();
    //信用级别
    var creditLeve = $("#ctl00_ContentPlaceHolder1_ddCreditLeve").val();
    //亲密度
    var closeLeve = $("#ctl00_ContentPlaceHolder1_ddCloseLeve").val();
    //开户银行
    var bankName = $("#ctl00_ContentPlaceHolder1_txtBankName").val();
    //企业代码
    var cpy_Code = $("#ctl00_ContentPlaceHolder1_txtCpy_Code").val();
    //开户银行账号
    var bankAccountNo = $("#ctl00_ContentPlaceHolder1_txtBankAccountNo").val();
    //法定代表
    var lawPerson = $("#ctl00_ContentPlaceHolder1_txtLawPerson").val();
    //纳税人识别号
    var taxAccountNo = $("#ctl00_ContentPlaceHolder1_txtTaxAccountNo").val();
    //备注
    var remark = $("#ctl00_ContentPlaceHolder1_txtRemark").val();
    $(".norecords").hide();
    $("#jqGrid").jqGrid('setGridParam', {
        url: "/HttpHandler/Customer/CustomerInfoHandler.ashx",
        postData: {
            "andor": andor,
            "action": 'sel',
            'strwhere': strwhere,
            'unit': unit,
            'keyname': keyname,
            'year': year,
            "startTime": startTime,
            "endTime": endTime,
            "custNo": custNo,//客户编号
            "linkMan": linkMan,//联系人
            "linkPhone": linkPhone,//联系电话
            "address": address,//公司地址
            "code": code,//邮政编号
            "cpyFax": cpyFax,//传真号
            "insertName": insertName,//录入人
            "cst_Brief": cst_Brief,//客户简称
            "cstEnglishName": cstEnglishName,//客户英文名称
            "isPartner": isPartner,//是否合作
            "country": country,//所在国家
            "province": province,//所在省份
            "city": city,//所在城市
            "cstType": cstType,//客户类型
            "profession": profession,//所属行业
            "branchPart": branchPart,//分支机构
            "email": email,//邮箱
            "principalSheet": principalSheet,//公司主页
            "date_Start": date_Start,//关系建立时间
            "date_End": date_End,//关系建立时间
            "relationDepartment": relationDepartment,//关系部门
            "creditLeve": creditLeve,//信用级别
            "closeLeve": closeLeve,//亲密度
            "bankName": bankName,//开户银行
            "cpy_Code": cpy_Code,//企业代表
            "bankAccountNo": bankAccountNo,//开户银行账户
            "lawPerson": lawPerson,//法定代表
            "taxAccountNo": taxAccountNo,//纳税人识别号
            "remark": remark//备注
        },
        page: 1,
        sortname: 'c.cst_id',
        sortorder: 'desc'

    }).trigger("reloadGrid");
}