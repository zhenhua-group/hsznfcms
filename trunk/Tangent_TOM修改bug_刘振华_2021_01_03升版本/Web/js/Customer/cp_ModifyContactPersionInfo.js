﻿//js判断事件
$(document).ready(function() {
    //保存
    $("#btn_Save").click(function() {
        var txtName = $("#txtName").val();
        var txtDuties = $("#txtDuties").val();
        var txtPhone = $("#txtPhone").val();
        var txtDepartment = $("#txtDepartment").val();
        var txtBPhone = $("#txtBPhone").val();
        var txtFPhone = $("#txtFPhone").val();
        var txtBFax = $("#txtBFax").val();
        var txtEmail = $("#txtEmail").val();
        var txtFFax = $("#txtFFax").val();
        var txtContactNo = $("#txtContactNo").val();

        var msg = "";
        if (txtContactNo == "") {
            msg += "序号不可为空!</br>";
        } else {
            if (DataLength(txtContactNo) > 10) {
                msg += "序号不可超出10字符(5汉字)";
            }
        }
        if (txtName == "") {
            msg += "姓名不可为空!</br>";
        } else {
            if (DataLength(txtName) > 10) {
                msg += "姓名长度不可大于10字符(5汉字)!</br>";
            }
        }
        if (txtPhone == "") {
            msg += "手机不可为空!</br>";
        } else {
            var reg = /^1[358]\d{9}$/;
            if (!reg.test(txtPhone)) {
                msg += "手机号码格式不正确!</br>"
            }
        }
        if (txtDuties != "") {
            if (DataLength(txtDuties) > 20) {
                msg += "职务不可超出20字符(10汉字)!</br>";
            }
        }
        if (txtDepartment != "") {
            if (DataLength(txtDepartment) > 20) {
                msg += "部门不可超出20字符(10汉字)!</br>";
            }
        }
        if (txtBPhone != "") {
            var reg = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
            if (!reg.test(txtBPhone)) {
                msg += "商务电话格式不正确!</br>";
            }
        }
        if (txtFPhone != "") {
            var reg = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
            if (!reg.test(txtFPhone)) {
                msg += "住宅电话格式不正确!</br>";
            }
        }
        if (txtBFax != "") {
            var patrn = /^[+]{0,1}(d){1,3}[ ]?([-]?((d)|[ ]){1,12})+$/;
            if (!patrn.test(txtBFax)) {
                msg += "商务传真格式不正确!</br>";
            }
        }
        if (txtFFax != "") {
            var patrn = /^[+]{0,1}(d){1,3}[ ]?([-]?((d)|[ ]){1,12})+$/;
            if (!patrn.test(txtFFax)) {
                msg += "住宅传真格式不正确!</br>";
            }
        }
        if (txtEmail != "") {
            var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
            if (!reg.test(txtEmail)) {
                msg += "邮箱格式不正确!</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
});
//汉字和英文的长度
function DataLength(fData) {
    var intLength = 0;
    for (var i = 0; i < fData.length; i++) {
        if (fData.charCodeAt(i) < 0 || fData.charCodeAt(i) > 255) {
            intLength = intLength + 2;
        }
        else {
            intLength = intLength + 1;
        }
    }
    return intLength;
}