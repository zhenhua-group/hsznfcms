﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Customer/CustomerInfoHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '客户编号', '客户名称 ', '公司地址', '邮政编码', '联系人', '联系电话', '传真号', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'Cst_Id', index: 'Cst_Id', hidden: true, editable: true },
                             { name: 'Cst_No', index: 'Cst_No', width: 100, align: 'center' },
                             { name: 'Cst_Name', index: 'Cst_Name', width: 273, formatter: colNameShowFormatter },
                             { name: 'Cpy_Address', index: 'Cpy_Address', width: 200 },
                             { name: 'Code', index: 'Code', width: 80, align: 'center' },
                             { name: 'Linkman', index: 'Linkman', width: 60, align: 'center' },
                             { name: 'Cpy_Phone', index: 'Cpy_Phone', width: 100, align: 'center' },
                             { name: 'Cpy_Fax', index: 'Cpy_Fax', width: 100, align: 'center' },
                             { name: 'Cst_Id', index: 'Cst_Id', width: 50, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "action": "Search", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()) },
        loadonce: false,
        sortname: 'c.cst_id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Customer/CustomerInfoHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'Cst_Id');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var Cst_No = $("#ctl00_ContentPlaceHolder1_txtCst_No").val();
        var Cst_Brief = $("#ctl00_ContentPlaceHolder1_txtCst_Brief").val();
        var Cst_Name = $("#ctl00_ContentPlaceHolder1_txtCst_Name").val();
        var dType = $("#ctl00_ContentPlaceHolder1_ddType").val();
        var dProfession = $("#ctl00_ContentPlaceHolder1_ddProfession").val();
        var City = $("#ctl00_ContentPlaceHolder1_txtCity").val();
        var Linkman = $("#ctl00_ContentPlaceHolder1_txtLinkman").val();
        var Province = $("#ctl00_ContentPlaceHolder1_txtProvince").val();
        var Cpy_Address = $("#ctl00_ContentPlaceHolder1_txtCpy_Address").val();

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Customer/CustomerInfoHandler.ashx",
            postData: { 'strwhere': strwhere, 'txtCst_No': Cst_No, 'txtCst_Brief': Cst_Brief, 'txtCst_Name': Cst_Name, 'ddType': dType, 'ddProfession': dProfession, 'txtCity': City, 'txtLinkman': Linkman, 'txtProvince': Province, 'txtCpy_Address': Cpy_Address, 'Cpy_Phone': '' },
            page: 1

        }).trigger("reloadGrid");
    });
});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "cst_ShowCustomerInfoByMaster.aspx?Cst_Id=" + rowData["Cst_Id"];
    return '<a href="' + pageurl + '" alt="查看客户">' + celvalue + '</a>';

}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "cst_ShowCustomerInfoByMaster.aspx?Cst_Id=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看客户">查看</a>';

}

//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');

    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        // $("#nodata").show();
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        } else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}