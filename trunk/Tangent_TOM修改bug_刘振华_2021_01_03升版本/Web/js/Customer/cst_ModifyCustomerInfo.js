﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    //加载联系人
    LoadLinkManData();
    //加载满意度
    LoadContractData();
    //GridView 固定高度
    var h = 120;
    //城市
    var val = $("#hiddenCountry").val();
    if (val != "") {
        $("#DropDownListCountry").val(val);
    }
    if ($("#hiddenProvince").val() != "") {
        $("#DropDownListProvince option:first").text($("#hiddenProvince").val());
    }
    if ($("#hiddenCity").val() != "") {
        $("#DropDownListCity option:first").text($("#hiddenCity").val());
    }

    //隔行变色
    $(".cls_show_cst_jiben>tbody>tr:odd").attr("style", "background-color:#FFF;");
    //保存信息
    $("#btn_Save").click(function () {
        var txtCst_No = $.trim($("#txtCst_No").val()); 
        var txtCst_Brief = $.trim($("#txtCst_Brief").val()); 
        var txtCst_Name = $.trim($("#txtCst_Name").val()); 
        var txtCpy_Address = $.trim($("#txtCpy_Address").val()); 
        var txtCode = $.trim($("#txtCode").val()); 
        var txtLinkman = $.trim($("#txtLinkman").val()); 
        var txtCpy_Phone = $.trim($("#txtCpy_Phone").val()); 
        var txtCpy_Fax = $.trim($("#txtCpy_Fax").val()); 
        //扩展信息
        var txtCst_EnglishName = $("#txtCst_EnglishName").val();
        var txtEmail = $("#txtEmail").val();
        var txtBankAccountNo = $("#txtBankAccountNo").val();
        var txtBankName = $("#txtBankName").val();

        var msg = "";
        if (txtCst_No == "") {
            msg += "编号不可为空！<br/>";
        }
        if (txtCst_Brief == "") {
            msg += "客户简称不能为空！<br/>";
        }
        if (txtCst_Name == "") {
            msg += "客户名称不可为空！<br/>"
        }
        if (txtCpy_Address == "") {
            msg += "公司地址不能为空！<br/>";
        }
        if (txtCode != "") {
            var reg = /^[0-9][0-9]{5}$/;
            if (!reg.test(txtCode)) {
                msg += "邮编格式不正确！<br/>";
            }
        }
        if (txtCpy_Fax != "") {
            var reg = /^[+]{0,1}(\d){1,3}[ ]?([-]?((\d)|[ ]){1,12})+$/;
            if (!reg.test(txtCpy_Fax)) {
                msg += "公司传真格式不正确！<br/>";
            }
        }
        if (txtCpy_Phone != "") {
            var reg = /(^((0[1,2]{1}\d{1}-?\d{8})|(0[3-9]{1}\d{2}-?\d{7,8}))$)|(^0?(13[0-9]|15[0-35-9]|18[0236789]|14[57])[0-9]{8}$)/;
            if (!reg.test(txtCpy_Phone)) {
                msg += "请输入固定电话或手机号！</br>";
            }
        }
        if (txtBankAccountNo != "") {
            if (isNaN(txtBankAccountNo)) {
                msg += "银行帐号必须为数字！</br>";
            } else {
                if (txtBankName == "") {
                    msg += "需要填写银行名称！<br/>";
                }
            }
        }
        if (txtEmail != "") {
            var reg = /^[a-zA-Z]([a-zA-Z0-9]*[-_.]?[a-zA-Z0-9]+)+@([\w-]+\.)+[a-zA-Z]{2,}$/;
            if (!reg.test(txtEmail)) {
                msg += "邮箱格式不正确！</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        } else {
            //获取所有Hidden
            var hiddenArr = $(":hidden", $("#gv_Attach"));
            $.each(hiddenArr, function (index, item) {
                var value = $("#attachHidden").val() + $(item).val() + ",";
                $("#attachHidden").val(value);
            });
            return true;
        }
    });
    var contact = new Contact(null, $("#DivAddLinkMan"));
    //添加联系人事件
    $("#addLinkManActionLink").click(function () {
        contact.Clear();
        contact.IsEdit = false;
        ClearTipSpan();
        $("#DivAddLinkMan").dialog({
            autoOpen: false,
            modal: true,
            width: 580,
            height: 310,
            resizable: false,
            buttons:
			{
			    "保存": function () {
			        contact.SaveContact(this);
			    },
			    "取消": function () { $(this).dialog("close"); }
			}
        }).dialog("open");
        //组织浏览器默认行为
        return false;
    });
    var satisfactionInfo = new SatisfactionInfo(null, $("#DivAddSatisfaction"));

    //用户满意度事件
    $("#addSatisfactionActionLink").click(function (event) {
        satisfactionInfo.IsEdit = false;
        satisfactionInfo.Clear();
        ClearTipSpan();
        $("#DivAddSatisfaction").dialog({
            autoOpen: false,
            modal: true,
            width: 580,
            height: 420,
            resizable: false,
            zIndex: 99999,
            title: "添加用户满意度",
            buttons:
			{
			    "保存": function () { satisfactionInfo.SaveSatisfaction(this) },
			    "取消": function () { $(this).dialog("close"); }
			}
        }).dialog("open");
        //组织浏览器默认行为
        return false;
    });

    //用户满意修改事件
    $("span[id=satisfactionEditA]").live("click", function () {
        var sid = $(this).attr("sid");
        satisfactionInfo.IsEdit = true;
        satisfactionInfo.Clear();
        satisfactionInfo.SysNo = sid;
        satisfactionInfo.GetSatisfactionInfo(sid);
        ClearTipSpan();
        //查询原始信息并绑定
        $("#DivAddSatisfaction").dialog({
            autoOpen: false,
            modal: true,
            width: 580,
            height: 420,
            resizable: false,
            zIndex: 99999,
            title: "修改用户满意度",
            buttons:
			{
			    "保存": function () { satisfactionInfo.SaveSatisfaction(this) },
			    "取消": function () { $(this).dialog("close"); }
			}
        }).dialog("open");
        //组织浏览器默认行为
        return false;
    });
    //编辑联系人事件
    $("span[id=EditContactA]").live("click", function () {
        var cid = $(this).attr("cid");
        contact.Clear();
        contact.IsEdit = true;
        contact.GetContactInfo(cid);
        contact.SysNo = cid;
        ClearTipSpan();
        //查询原始信息并绑定
        $("#DivAddLinkMan").dialog({
            autoOpen: false,
            modal: true,
            width: 580,
            height: 310,
            resizable: false,
            zIndex: 99999,
            title: "修改联系人",
            buttons:
			{
			    "保存": function () { contact.SaveContact(this); },
			    "取消": function () { $(this).dialog("close"); }
			}
        }).dialog("open");
        return false;
    });
    //查看
    $(".cls_chk").live('click', function () {

        $('#Show_cstInfo').dialog('open');
        $('.ui-widget-overlay').css("display", "block");

        var cstid = $(this).attr("rel");
        $.ajax({
            type: "POST",
            url: "../HttpHandler/cp_ShowContactPersionInfo.ashx",
            dataType: "text",
            data: "id=" + cstid,
            success: function (msg) {
                var arrayValues = msg.split("|");
                $("#lbl_No").text(arrayValues[0]);
                $("#lbl_name").text(arrayValues[1]);
                $("#lbl_duties").text(arrayValues[2]);
                $("#lbl_department").text(arrayValues[3]);
                $("#lbl_phone").text(arrayValues[4]);
                $("#lbl_email").text(arrayValues[5]);
                $("#lbl_bphone").text(arrayValues[6]);
                $("#lbl_fphone").text(arrayValues[7]);
                $("#lbl_bfax").text(arrayValues[8]);
                $("#lbl_Ffax").text(arrayValues[9]);
                $("#lbl_bz").text(arrayValues[10]);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("错误");
            }
        });
    });
    // Dialog
    $('#Show_cstInfo').dialog({
        autoOpen: false,
        width: 640,
        height: 250
    });
    //关闭
    $("#Button1").click(function () {
        $('#Show_cstInfo').dialog("close");
        $('.ui-widget-overlay').css('display', 'none');
    });

    $("#ui-dialog-title-Show_cstInfo~a").click(function () {
        $('#Show_cstInfo').dialog("close");
        $('.ui-widget-overlay').css('display', 'none');
    });
    //满意度查看
    $(".sat_chk").live('click', function () {

        $('#Show_sfcInfo').dialog('open');
        $('.ui-widget-overlay').css("display", "block");

        var sat_id = $(this).attr("rel");
        $.ajax({
            type: "POST",
            url: "../HttpHandler/ShowSatisfactionInfo.ashx",
            data: "sat_id=" + sat_id,
            success: function (msg) {
                var sat_arry = msg.split("|");
                $("#lblSat_No").text(sat_arry[0]);
                $("#lblSch_Context").text(sat_arry[1]);
                $("#lblSat_Leve").text(sat_arry[5]);
                $("#lblSch_Time").text(sat_arry[6]);
                $("#lblSch_Way").text(sat_arry[7]);
                $("#lblSat_Best").text(sat_arry[3]);
                $("#lblSat_NotBest").text(sat_arry[4]);
                $("#lblRemark").text(sat_arry[2]);
            },
            eror: function (XMLHttpRequest, textStatus, errorThrowm) {
                alert("错误");
            }
        });
    });
    // Dialog
    $('#Show_sfcInfo').dialog({
        autoOpen: false,
        width: 430,
        height: 280
    });
    //关闭
    $("#Button2").click(function () {
        $('#Show_sfcInfo').dialog("close");
        $('.ui-widget-overlay').css('display', 'none');
    });

    $("#ui-dialog-title-Show_cstInfo~a").click(function () {
        $('#Show_sfcInfo').dialog("close");
        $('.ui-widget-overlay').css('display', 'none');
    });
});
//判断汉字的实际长度
function DataLength(fData) {
    var intLength = 0;
    for (var i = 0; i < fData.length; i++) {
        if ((fData.charCodeAt(i) < 0) || (fData.charCodeAt(i) > 255))
            intLength = intLength + 2;
        else
            intLength = intLength + 1;
    }
    return intLength;
}
//附件删除事件
function AttachDetele(tr) {
    if (confirm("确认是否删除?")) {
        //获取当前系统自增号
        var sysNo = $(tr).parents("tr:first").children("td:first").children("input").val();
        $.post("/HttpHandler/ProcessUoloadFile.ashx", { "Action": "1", "SysNo": sysNo }, function (jsonResult) {
            if (jsonResult == "1") {
                //服务端成功删除的场合
                $(tr).parents("tr:first").remove();
            }
        });
    }
}
//创建提示信息Span 
function CreateTipSpan(element, tipContent) {
    element.parent("td:first").append("<span id=\"TipSpan\" style=\"color:red;\">" + tipContent + "</span>");
}
//清除提示信息Span 
function ClearTipSpan(container) {
    $("span[id=TipSpan]").remove();
}
//加载联系人信息
function LoadLinkManData() {
    var data = "action=getcstinfo&cstno=" + $("#HiddenCustomerSysNo").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result == "0") {
                $("#linkman_nodata").show();
            }
            else{
            var data = result == null ? "" : result.ds;
            if ($("#cst_datas tr").length > 1) {
                $("#cst_datas tr:gt(0)").remove();
            }
            $.each(data, function (i, n) {
                var row = $("#cst_row").clone();
                var oper = " <span  rel='" + n.CP_Id + "' id='chk' style='cursor: pointer;color:blue;' class='cls_chk'>查看</span>|";
                oper += "<span  cid='" + n.CP_Id + "' style='cursor: pointer;color:blue;' id='EditContactA'>编辑|</span>";
                oper += "<span  rel='" + n.CP_Id + "' style='cursor: pointer;color:blue;' class='cls_del'>删除</span>";
                row.find("#id").text(n.ContactNo);
                row.find("#name").text(n.Name);
                row.find("#name").attr("align", "left");
                row.find("#zhiwu").text(n.Duties);
                row.find("#bumen").html(n.Department);
                row.find("#phone").html(n.Phone);
                row.find("#email").html(n.Email);
                var shortMark = n.Remark;
                if (shortMark.length > 20) {
                    shortMark = shortMark.substr(0, 20) + "...";
                }
                row.find("#mark").attr("title", n.Remark);
                row.find("#mark").html(shortMark); //By fbw 新增
                row.find("#oper").html(oper);
                row.find("#oper span").click(function () {
                    //删除事件
                    delLinkManData($(this));
                });
                //添加样式
                row.addClass("cls_TableRow");
                row.appendTo("#cst_datas");
            });

            data = "";
         }
           
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}
//删除联系人信息
function delLinkManData(link) {
    if (link.attr("class") == "cls_del") {

        if (confirm("确定删除联系人信息吗？")) {
            var data = "action=dellinkinfo&linkid=" + link.attr("rel");
            $.ajax({
                type: "GET",
                url: "../HttpHandler/CommHandler.ashx",
                dataType: "text",
                data: data,
                success: function (result) {
                    if (result == "yes") {
                        //删除本行数据
                        link.parents("tr:first").remove();
                      //  alert("删除联系人成功！");
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("系统错误！");
                }
            });
        }
    }
}
//加载满意度调查信息
function LoadContractData() {
    var data = "action=getstainfo&stano=" + $("#HiddenCustomerSysNo").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result == "0") {
                $("#static_nodata").show();
            }
            else{
            var data = result == null ? "" : result.ds;
            if ($("#static_datas tr").length > 1) {
                $("#static_datas tr:gt(0)").remove();
            }
            $.each(data, function (i, n) {
                var row = $("#static_row").clone();
                var oper = "<span  rel='" + n.Sat_Id + "' style='cursor: pointer;color:blue;' id='sat_id' class='sat_chk'>查看</span>|";
                oper += "<span  sid='" + n.Sat_Id + "' style='cursor: pointer;color:blue;' id='satisfactionEditA'>编辑</span>|";
                oper += "<span  rel='" + n.Sat_Id + "' style='cursor: pointer;color:blue;' class='cls_del'>删除</span>";
                row.find("#st_id").text(n.Sat_No);
                row.find("#st_content").text(n.Sch_Context);
                row.find("#st_myd").text(n.Sat_Leve);
                row.find("#st_time").html(n.Sch_Time);
                row.find("#st_type").html(n.Sch_Way);
                var shortMark = n.Remark;
                if (shortMark.length > 20) {
                    shortMark = shortMark.substr(0, 20) + "...";
                }
                row.find("#st_mark").attr("title", n.Remark);
                row.find("#st_mark").html(shortMark); //By fbw 新增
                row.find("#st_oper").html(oper);
                row.find("#st_oper span").click(function () {
                    //删除事件
                    delContractData($(this));
                });
                //添加样式
                row.addClass("cls_TableRow");
                row.appendTo("#static_datas");
            });
           
            data = "";
           }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}
//删除满意度调查信息
function delContractData(link) {

    if (link.attr("class") == 'cls_del') {

        if (confirm("确定删除满意度调查信息？")) {
            var data = "action=delsta&staid=" + link.attr("rel");
            $.ajax({
                type: "GET",
                url: "../HttpHandler/CommHandler.ashx",
                dataType: "text",
                data: data,
                success: function (result) {
                    if (result == "yes") {
                        //删除本行数据
                        link.parents("tr:first").remove();
                        //alert("删除满意度调查成功！");
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //alert("系统错误！");
                }
            });
        }
    }
}