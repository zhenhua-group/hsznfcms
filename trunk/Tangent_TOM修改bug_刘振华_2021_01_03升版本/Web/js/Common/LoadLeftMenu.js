﻿//LoadLeftMenu菜单项
var LoadLeftMenu =
{
     "GetLeftMenu": function (type) {
        $.post("/HttpHandler/LoadLeftMenuHandler.ashx", { "leftMenuType": type }, function (jsonResult) {
            if (jsonResult != null) {
                var leftMenuList = eval("(" + jsonResult + ")");
                $.each(leftMenuList, function (index, leftMenu) {
                    $("#" + leftMenu.ElementID).parent("li:first").show();
                });
            }
        });
        
    }

};