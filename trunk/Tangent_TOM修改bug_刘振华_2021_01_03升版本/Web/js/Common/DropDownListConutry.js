﻿//根据国家编号，得到省份
function GetProvinces(ddl) {
    $("#ctl00_ContentPlaceHolder1_hiddenCountry").val($(ddl).children("option:selected").val());

    var countryID = $(ddl).children("option:selected").attr("countryid");
    $("#DropDownListCity").children("option:gt(0)").remove();
    if (countryID != "0") {
        $.post("/HttpHandler/DropDownListCountryHandler.ashx", { "Action": "2", "countryID": countryID }, function (jsonResult) {
            if (jsonResult == "0") {
                alert("城市情报取得失败！");
            } else {
                var provincesArr = Global.evalJSON(jsonResult);
                $("#DropDownListProvince").children("option:gt(0)").remove();
                var optiosnString = "";
                $.each(provincesArr, function (index, item) {
                    optiosnString += "<option value=\"" + item.ProvinceName + "\" provinceid=\"" + item.ProvinceID + "\">" + item.ProvinceName + "</option>";
                });
                $("#DropDownListProvince").append(optiosnString);
            }
        });
    }
}

//得到城市数据
function GetCitys(ddl) {
    $("#ctl00_ContentPlaceHolder1_hiddenProvince").val($(ddl).children("option:selected").val());

    var provinceID = $(ddl).children("option:selected").attr("provinceid");
    if (provinceID != "0") {
        $.post("/HttpHandler/DropDownListCountryHandler.ashx", { "Action": "3", "provinceID": provinceID }, function (jsonResult) {
            if (jsonResult == "0") {
                alert("城市情报取得失败！");
            } else {
                var cityArr = Global.evalJSON(jsonResult);
                $("#DropDownListCity").children("option:gt(0)").remove();
                var optiosnString = "";
                $.each(cityArr, function (index, item) {
                    optiosnString += "<option value=\"" + item.CityName + "\" cityid=\"" + item.CityID + "\">" + item.CityName + "</option>";
                });
                $("#DropDownListCity").append(optiosnString);
            }
        });
    }
}

function FillHidden(ddl) {
    $("#ctl00_ContentPlaceHolder1_hiddenCity").val($(ddl).children("option:selected").val());
}