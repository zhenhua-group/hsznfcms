﻿//LoadLeftMenu菜单项
var LoadLeftMenu =
{
    "GetLeftMenu": function(type) {
        $.post("/HttpHandler/LoadLeftMenuHandler.ashx", { "leftMenuType": type }, function(jsonResult) {
            if (jsonResult != null) {
                var leftMenuList = eval("(" + jsonResult + ")");
                $.each(leftMenuList, function(index, leftMenu) {
                    //有权限
                   $("#" + leftMenu.ElementID).unbind("click");
                });
            }
        });
    },
    "DisabledLink": function() {
    $("a.cls_control").each(function() {
            $(this).bind("click", function() {
                alert("没有访问权限！");
                return false;
            });
        });
    }
};