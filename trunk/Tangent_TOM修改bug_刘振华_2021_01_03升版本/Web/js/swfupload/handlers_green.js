﻿function fileQueueError(file, errorCode, message) {
    try {
        var imageName = "error.gif";
        var errorName = "";
        if (errorCode === SWFUpload.errorCode_QUEUE_LIMIT_EXCEEDED) {
            errorName = "每次只能上传一个文件！";
        }

        if (errorName !== "") {
            alert(errorName);
            return;
        }

        switch (errorCode) {
            case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
                imageName = "zerobyte.gif";
                break;
            case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
                imageName = "toobig.gif";
                break;
            case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
            case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
            default:
                //alert(message);
                break;
        }
        if (message == "1") {
            alert("错误！每次只能选择一个文件！");
        }
        //addImage("images/" + imageName);

    } catch (ex) {
        this.debug(ex);
    }

}
//获取上传文件队列
function fileQueued(file) {
    try {
        //var txtFileName = document.getElementById("hid_sigin");
        //txtFileName.value = file.name;
        //alert(file.name);
    }
    catch (e) {
    }
}
//选择文件对话框点击打开
function fileDialogComplete(numFilesSelected, numFilesQueued) {
    try {
        if (numFilesQueued > 0) {
            this.startUpload();
        }
    } catch (ex) {
        this.debug(ex);
    }
}
//上传文件进度
function uploadProgress(file, bytesLoaded) {

    try {
        var percent = Math.ceil((bytesLoaded / file.size) * 100);

        var progress = new FileProgress(file, this.customSettings.upload_target);
        progress.setProgress(percent);
        if (percent === 100) {
            progress.setStatus("上传中...");
            progress.toggleCancel(false, this);
        } else {
            progress.setStatus("上传中...");
            progress.toggleCancel(true, this);
        }
    } catch (ex) {
        this.debug(ex);
    }
}
//文件上传成功
function uploadSuccess(file, serverData) {
    try {


        var progress = new FileProgress(file, this.customSettings.upload_target);

        progress.setStatus("上传成功！");
        progress.toggleCancel(false);


    } catch (ex) {
        this.debug(ex);
    }
}
//上传完成


function uploadComplete(file) {
    try {
        /* 下次上传也自动完成自动上传 */
        if (this.getStats().files_queued > 0) {
            this.startUpload();
        } else {
            var progress = new FileProgress(file, this.customSettings.upload_target);
            progress.setComplete();
            progress.setStatus("附件上传成功");
            progress.toggleCancel(false);
            //加载附件数据
            GetAttachData();
        }

    } catch (ex) {
        this.debug(ex);
    }
}
//上传出现错误
function uploadError(file, errorCode, message) {
    var imageName = "error.gif";
    var progress;
    try {
        switch (errorCode) {
            case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
                try {
                    progress = new FileProgress(file, this.customSettings.upload_target);
                    progress.setCancelled();
                    progress.setStatus("取消");
                    progress.toggleCancel(false);
                }
                catch (ex1) {
                    this.debug(ex1);
                }
                break;
            case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
                try {
                    progress = new FileProgress(file, this.customSettings.upload_target);
                    progress.setCancelled();
                    progress.setStatus("暂停");
                    progress.toggleCancel(true);
                }
                catch (ex2) {
                    this.debug(ex2);
                }
            case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
                imageName = "uploadlimit.gif";
                break;
            default:
                //alert(message);
                break;
        }
        if (message == 500) {
            alert("登录超时，请重新登录！");
        }


    } catch (ex3) {
        this.debug(ex3);
    }

}

function fadeIn(element, opacity) {
    var reduceOpacityBy = 5;
    var rate = 30; // 15 fps

    if (opacity < 100) {
        opacity += reduceOpacityBy;
        if (opacity > 100) {
            opacity = 100;
        }

        if (element.filters) {
            try {
                element.filters.item("DXImageTransform.Microsoft.Alpha").opacity = opacity;
            } catch (e) {
                // If it is not set initially, the browser will throw an error.  This will set it if it is not set yet.
                element.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=' + opacity + ')';
            }
        } else {
            element.style.opacity = opacity / 100;
        }
    }

    if (opacity < 100) {
        setTimeout(function() {
            fadeIn(element, opacity);
        }, rate);
    }
}

/* ******************************************
*	FileProgress Object
*	Control object for displaying file info
* ****************************************** */

function FileProgress(file, targetID) {
    this.fileProgressID = "divFileProgress";

    this.fileProgressWrapper = document.getElementById(this.fileProgressID);
    if (!this.fileProgressWrapper) {
        this.fileProgressWrapper = document.createElement("div");
        this.fileProgressWrapper.className = "progressWrapper";
        this.fileProgressWrapper.id = this.fileProgressID;

        this.fileProgressElement = document.createElement("div");
        this.fileProgressElement.className = "progressContainer";

        var progressCancel = document.createElement("a");
        progressCancel.className = "progressCancel";
        progressCancel.href = "#";
        progressCancel.style.visibility = "hidden";
        progressCancel.appendChild(document.createTextNode(" "));

        var progressText = document.createElement("div");
        progressText.className = "progressName";
        progressText.appendChild(document.createTextNode(file.name));

        var progressBar = document.createElement("div");
        progressBar.className = "progressBarInProgress";

        var progressStatus = document.createElement("div");
        progressStatus.className = "progressBarStatus";
        progressStatus.innerHTML = "&nbsp;";

        this.fileProgressElement.appendChild(progressCancel);
        this.fileProgressElement.appendChild(progressText);
        this.fileProgressElement.appendChild(progressStatus);
        this.fileProgressElement.appendChild(progressBar);

        this.fileProgressWrapper.appendChild(this.fileProgressElement);

        document.getElementById(targetID).appendChild(this.fileProgressWrapper);
        fadeIn(this.fileProgressWrapper, 0);

    } else {
        this.fileProgressElement = this.fileProgressWrapper.firstChild;
        this.fileProgressElement.childNodes[1].firstChild.nodeValue = file.name;
    }

    this.height = this.fileProgressWrapper.offsetHeight;

}
FileProgress.prototype.setProgress = function(percentage) {
    this.fileProgressElement.className = "progressContainer green";
    this.fileProgressElement.childNodes[3].className = "progressBarInProgress";
    this.fileProgressElement.childNodes[3].style.width = percentage + "%";
};
FileProgress.prototype.setComplete = function() {
    this.fileProgressElement.className = "progressContainer blue";
    this.fileProgressElement.childNodes[3].className = "progressBarComplete";
    this.fileProgressElement.childNodes[3].style.width = "";

};
FileProgress.prototype.setError = function() {
    this.fileProgressElement.className = "progressContainer red";
    this.fileProgressElement.childNodes[3].className = "progressBarError";
    this.fileProgressElement.childNodes[3].style.width = "";

};
FileProgress.prototype.setCancelled = function() {
    this.fileProgressElement.className = "progressContainer";
    this.fileProgressElement.childNodes[3].className = "progressBarError";
    this.fileProgressElement.childNodes[3].style.width = "";

};
FileProgress.prototype.setStatus = function(status) {
    this.fileProgressElement.childNodes[2].innerHTML = status;
};

FileProgress.prototype.toggleCancel = function(show, swfuploadInstance) {
    this.fileProgressElement.childNodes[0].style.visibility = show ? "visible" : "hidden";
    if (swfuploadInstance) {
        var fileID = this.fileProgressID;
        this.fileProgressElement.childNodes[0].onclick = function() {
            swfuploadInstance.cancelUpload(fileID);
            return false;
        };
    }
};
//edit by Sago 上传成功回掉函数
function uploadSuccessShowResult(file,serverData){

                var progress = new FileProgress(file, this.customSettings.upload_target);
                progress.setStatus("上传成功！");
                progress.toggleCancel(false);
                //获取最近一次数据库自动更新号
                var identitySysNo = parseInt(serverData, 10);
                //判断是否是正确的自增号
                if(identitySysNo > 0){
                        //是正确的自增号的场合
                    $.ajax({
                        async: true,
                        cache: false,
                        type: "POST",
                        url: "/HttpHandler/UpLoadFileHanlder.ashx",
                        data: { "SysNo": identitySysNo },
                        datatype: "json",
                        contentType: "application/x-www-form-urlencoded; charset=utf-8",
                        success: function(jsonResult) {
                            //得到数据
                            var objResult = eval("(" + jsonResult + ")");
                            //拼接HTML内容
                            var tr = "<tr id=\"" + objResult.ID + "\" align=\"center\" style=\"width:40%;\" class=\"cls_TableRow\"><td align='left'>";
                            tr += "<input type=\"hidden\" value=\"" + objResult.ID + "\">";
                            tr +="<img style=\"width:16px;height:16px;\" src=\""+objResult.FileTypeImg+"\"/>";
                            tr += objResult.FileName + "</td>";
                            tr += "<td align=\"center\" style=\"width:15%;\">" + objResult.UploadTimeString + "</td>";
                            tr += "<td align=\"center\" style=\"width:15%;\">" + objResult.FileSizeString + "</td>";
                            tr += "<td align=\"center\" style=\"width:10%;\">" + objResult.FileType + "</td>";
                            tr += "<td align=\"center\" style=\"width:10%;\">" + objResult.UploadUser + "</td>";
                            tr += "<td align=\"center\" style=\"width:5%;\"><a href=\"#\" onclick=\"AttachDetele(this);\" style=\"color:blue;\">删除</a></td>";
                            tr += "<td align=\"center\" style=\"color:Blue;width:5%;\"><a href=\"/Customer/DownLoadFile.aspx?SysNo=" + objResult.ID + "\">下载</a></td>"
                            tr += "</tr>";
                            $("#gv_Attach").append(tr);
                        }
                    }); 
                }
}
