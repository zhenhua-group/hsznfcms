﻿$(function () {
    $("#jqGrid").jqGrid({

        url: '/HttpHandler/Coperation/ProjectInvoiceHandler.ashx?action=sel&strwhere=' + $("#ctl00_ContentPlaceHolder1_hid_where").val() + '&keyname=' + $("#ctl00_ContentPlaceHolder1_txt_keyname").val() + '&unit=' + $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text() + '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year").val() + '&cprType=' + $("#ctl00_ContentPlaceHolder1_drp_cprType").find("option:selected").val() + '&starttime=' + $("#ctl00_ContentPlaceHolder1_txt_start").val() + '&endtime= ' + $("#ctl00_ContentPlaceHolder1_txt_end").val(),
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '', '', '', '','', '', '合同编号', '合同名称', '子公司', '合同总额(万元)', '实际合同额(万元)', '已收总额(万元)', '开票总额(万元)', '开票进度', '查看', '开票'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                             { name: 'totalcpr_Acount', index: 'totalcpr_Acount', hidden: true },                            
                             { name: 'totalssze', index: 'totalssze', hidden: true },
                             { name: 'totalkpje', index: 'totalkpje', hidden: true },
                             { name: 'totalcpr_ShijiAcount', index: 'totalcpr_ShijiAcount', hidden: true },
                             { name: 'pro_name', index: 'pro_name', hidden: true },
                             { name: 'pro_id', index: 'pro_id', hidden: true },
                             { name: 'cst_name', index: 'cst_name', hidden: true },
                             { name: 'cpr_No', index: 'cpr_No', width: 120, align: 'center', formatter: colCprNoShowFormatter },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 300, formatter: colNameShowFormatter },
                             { name: 'cpr_Unit', index: 'cpr_Unit', width: 80, align: 'center' },
                             { name: 'cpr_Acount', index: 'cpr_Acount', width: 100, align: 'center', formatter: colcprAcountShowFormatter },
                             { name: 'cpr_ShijiAcount', index: 'cpr_ShijiAcount', width: 110, align: 'center', formatter: colcprShijiAcountShowFormatter },
                             { name: 'ssze', index: 'ssze', width: 100, align: 'center', formatter: colSszeShowFormatter },
                             { name: 'kpje', index: 'kpje', width: 100, align: 'center', formatter: colKpjeShowFormatter },
                             { name: 'kpjd', index: 'kpjd', width: 100, align: 'center', formatter: colSfjdFormatter },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/ProjectInvoiceHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });



    //查询
    $("#btn_search").click(function () {
        var strwhere = $("#ctl00_ContentPlaceHolder1_hid_where").val();
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var starttime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endtime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/ProjectInvoiceHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, 'starttime': starttime, 'endtime': endtime },
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = $("#ctl00_ContentPlaceHolder1_hid_where").val();
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var starttime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endtime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/ProjectInvoiceHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, 'starttime': starttime, 'endtime': endtime },
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

   

    //年份
    $("#ctl00_ContentPlaceHolder1_drp_year").change(function () {
        var strwhere = $("#ctl00_ContentPlaceHolder1_hid_where").val();
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var starttime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endtime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/ProjectInvoiceHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, 'starttime': starttime, 'endtime': endtime},
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //点击开票
    $(".applyInvoice").live("click", function () {

        $("#addCharge").modal();
        $(this).attr("data-toggle", "modal");


        var cprid = $(this).attr("rel");
        var proid = $(this).attr("pro_id");
        var proname = $.trim($(this).attr("pro_name"));
        var cprname = $.trim($(this).attr("cpr_name"));
        var cprno = $.trim($(this).attr("cpr_no"));
        var cstname = $.trim($(this).attr("cst_name"));       
        var allcount = $(this).attr("cpr_acount");//合同额      
       

        //绑定层中数据
        $("#lbl_cprname").text(cprname);
        $("#lbl_number").text(cprno);
        $("#lbl_proname").text(proname);
        $("#lbl_allcount").text(allcount);
        // $("#txt_invoicename").val(cstname);
        //隐藏字段赋值
        $("#hid_chgidNow").val(proid); //当前项目ID
        $("#hid_cprid").val(cprid); //当前合同ID
        //加载数据
        ShowInvoiceData("addChargeListTable");

        //添加收费-按钮
        $("#btn_addcount").unbind('click').click(function () {
            BtnAddEditCount("add");
        });       
        AddOrEditCharge("add");

    });   

    //查看
    $(".cls_chk").live("click", function () {
        $("#showInvoioce").modal();
        $(this).attr("data-toggle", "modal");
        var cpr_id = $(this).attr("rel");
        var projid = $(this).attr("pro_id");
        $("#span_cprName").text($(this).attr("cpr_name"));
        //隐藏字段赋值
        $("#hid_chgidNow").val(projid); //当前项目ID
        $("#hid_cprid").val(cpr_id); //当前合同ID

        ShowInvoiceData("tb_InvoiceList");
    });
});
//添加开票
function BtnAddEditCount(flag) {
    //验证
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    var thisPayCount = $("#txt_payCount").val(); //本次入账金额
   
    //入账金额
    if (thisPayCount == "" || thisPayCount == null) {
        $("#txt_paycount_valide").text("请输入入账金额!").show();
        return false;
    } else if (!reg.test(thisPayCount) || thisPayCount <= 0) {
        $("#txt_paycount_valide").text("请输入正确的数字!").show();
        return false;
    }  else {
        $("#txt_paycount_valide").hide();
    }
    //入账单据号
    //    if (flag == "add") {
    //        if ($("#txt_billcode").val() == "") {
    //            $("#txt_billcode_valide").show();
    //            return false;
    //        } else {
    //            $("#txt_billcode_valide").hide();
    //        }
    //    }
    //开票单位全称
    if ($("#txt_invoicename").val() == "") {
        $("#txt_invoicename_valide").show();
        return false;
    } else {
        $("#txt_invoicename_valide").hide();
    }
    //时间
    if ($("#txt_times").val() == "") {
        $("#txt_time_valide").show();
        return false;
    } else {
        $("#txt_time_valide").hide();
    }
    //备注
    if ($("#txt_chargeRemark").val() == "") {
        $("#txt_remark_valide").show();
        return false;
    } else {
        $("#txt_remark_valide").hide();
    }
    //添加收款信息
    var projid = $("#hid_chgidNow").val();
    var cprid = $("#hid_cprid").val();
    var chgid = $("#hid_chgidThis").val(); //每条项目收费数据ID
    var paycount = $("#lbl_allcount").text(); //总金额
    //var billcode = $("#txt_billcode").val();
    var time = $("#txt_times").val();
    var mark = $("#txt_chargeRemark").val();
    var remitter = $("#txt_invoicename").val();
    var curuser = $("#ctl00_ContentPlaceHolder1_hid_curuser").val();
    var drop_type = $("#drop_type").val();
    var url = "/HttpHandler/Coperation/ProjectInvoiceApplyHandler.ashx";
    if (flag == "add") {
        var data = { "action": "add", "projid": "" + projid + "", "cprid": "" + cprid + "", "drop_type": drop_type, "paycount": "" + thisPayCount + "", "time": "" + time + "", "mark": "" + mark + "", "remitter": remitter, "curuser": curuser };
        $.post(url, data, function (rlt) {
            if (rlt == "0") {
                alert("添加合同开票成功！");
              
                ShowInvoiceData("addChargeListTable");
                AddOrEditCharge("add");
            } else {
                alert("添加合同开票失败！");
            }
        });
    } else if (flag == "edit") {
        var data = { "action": "edit", "projid": "" + projid + "", "cprid": "" + cprid + "", "paycount": "" + thisPayCount + "", "time": "" + time + "", "mark": "" + mark + "", "remitter": remitter, "curuser": curuser, "chgid": chgid };
        $.post(url, data, function (rlt) {
            if (rlt == "0") {
                alert("合同开票修改成功！");
               
                ShowInvoiceData("addChargeListTable");
                AddOrEditCharge("add");
            }
            else {
                alert("合同开票修改失败！");
            }
        });
    }
}
//编辑与添加转换初始化
function AddOrEditCharge(flag) {
    if (flag == "add") {      
       // $("#ft_add").find("legend").text("添加项目收费");
        $("#btn_returnCharge").hide();
        //清空
        $("#txt_payCount").val("");
        $("#txt_invoicename").val("");
        $("#txt_times").val("");
        $("#txt_chargeRemark").val("");       
        //隐藏
        $("#txt_paycount_valide").hide();
        $("#txt_invoicename_valide").hide();
        $("#txt_remark_valide").hide();
        $("#txt_time_valide").hide();
        //重新绑定事件
        $("#btn_addcount").unbind('click').click(function () {
            BtnAddEditCount("add");
        });
    } else if (flag == "edit") {
        
      //  $("#ft_add").find("legend").text("修改项目收费");
        $("#btn_returnCharge").show();
        //隐藏
        $("#txt_paycount_valide").hide();
        $("#txt_invoicename_valide").hide();
        $("#txt_remark_valide").hide();
        $("#txt_time_valide").hide();
        $("#btn_addcount").unbind('click').click(function () {
            BtnAddEditCount("edit");
        });
    }
}

//查看开票详细
function ShowInvoiceData(tblid) {
    var cprId = $("#hid_cprid").val();
    var pro_id = $("#hid_chgidNow").val();
    var data = "action=getInvoiceDataList&cprId=" + cprId + "&proid=" + pro_id;
    $("#" + tblid + " tr:gt(0)").remove();
    $.ajax({
        type: "Post",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null && result != NaN) {
                var dataObj = result.ds;
                $.each(dataObj, function (i, n) {
                    var trHtml = "";

                    var status = $.trim(n.Status);
                    var dateShort = GetShortDate(n.InvoiceDate);

                    trHtml += "<tr>";
                    trHtml += "<td> " + (i + 1) + "</td>";

                    var statusName = "";
                    if (status == "A") {
                        statusName = "等待财务确认";
                    } else if (status == "B") {
                        statusName = "<font color='red'>财务不通过</font>";
                    } else if (status == "C") {
                        statusName = "财务通过";
                    }
                    var remarkSub = "";
                    if (n.ApplyMark != null && n.ApplyMark != "")
                    {
                        if (n.ApplyMark.length>10) {
                            remarkSub = n.ApplyMark.substr(0, 10)+"...";
                        }
                        else
                        {
                            remarkSub = n.ApplyMark;
                        }                       
                    }
                   
                    trHtml += "<td> " + n.InvoiceAmount + "</td>";
                    trHtml += "<td> " + n.InvoiceName + "</td>";                    
                    trHtml += "<td> " + n.InvoiceType + "</td>";
                    trHtml += "<td> " + dateShort + "</td>";
                    trHtml += "<td> " + n.InvoiceCode + "</td>";
                    trHtml += "<td> " + n.CountryAddress + "</td>";
                    trHtml += "<td> " + n.CountryPhone + "</td>";
                    trHtml += "<td> " + n.Bankaccount + "</td>";
                    trHtml += "<td> " + n.Accountinfo + "</td>";
                    trHtml += "<td> " + n.InsertUserName + "</td>";
               
                    if (tblid == "tb_InvoiceList") {
                        trHtml += "<td  title='" + n.ApplyMark + "'>" + remarkSub + "</td>";
                    }
                    trHtml += "<td> " + statusName + "</td>";
                        
                     if (tblid == "addChargeListTable") {
                        var oper = "<td style='background-color:#ffffff;'><a href='###' rel='" + n.ID + "' style=\"color:blue;\">删除</a>";
                        oper += "&nbsp;|&nbsp;<a href='###' rel='" + n.ID + "' style=\"color:blue;\">修改</a></td>";
                        trHtml += oper;
                    }
                    
                    trHtml += '</tr>';
                    $("#" + tblid).append(trHtml);

                    //操作
                    $("#" + tblid + " tr:last").find("a").click(function () {
                        var operTxt = $(this).text();
                        if (operTxt == "删除") {
                            if (status == "C" || status == "E") {
                                alert("已经完成入账确认,不可删除!");
                                return false;
                            } else {
                                if (confirm("确定删除这次收费记录?")) {
                                    $("#hid_chgidThis").val(n.ID);
                                    DelProCharge(this);
                                }
                            }
                        } else if (operTxt == "修改") {
                            if (status == "C" || status == "E") {
                                alert("已完成入账确认,不可修改!");
                                return false;
                            } else {
                                $("#hid_chgidThis").val(n.ID); //先绑定该项的ID
                                AddOrEditCharge("edit");
                                $("#txt_payCount").val(n.InvoiceAmount);
                                $("#txt_invoicename").val($.trim(n.InvoiceName));
                                $("#txt_times").val(dateShort);
                                $("#txt_chargeRemark").val(n.ApplyMark);
                                $("#drop_type").val(n.InvoiceType)

                            }
                        }
                    });

                });
                ControlTableCss(tblid);
            } else {

                NoDataMessageOnTable(13, tblid);

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!");
        }
    });
}

//无数据提示
function NoDataMessageOnTable(tdCount, tblid) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td style='background-color:#ffffff;' colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tblid).append(trHtml);
}
//删除收费
function DelProCharge(aObj) {
    var chg_id = $("#hid_chgidThis").val(); //每条项目收费数据ID
    var row = $(aObj).parent().parent();
    var url = "/HttpHandler/Coperation/ProjectInvoiceApplyHandler.ashx";
    var data = { 'action': 'del', 'chgid': chg_id };
    $.post(url, data, function (rlt) {
        if (rlt == "0") {
            alert('删除成功！');           
            row.remove();
        }
        else {
            alert('删除失败！');
        }
    }, 'text');
}
//转换为短日期格式
function GetShortDate(dateString) {
    if (dateString != "" && dateString != null) {
        var index = 10;
        if (dateString.lastIndexOf("-") > 0) {
            index = dateString.lastIndexOf("-") + 3;
        } else if (dateString.lastIndexOf("/") > 0) {
            index = dateString.lastIndexOf("/") + 3;
        }
        return dateString.substr(0, index);
    } else {
        return "无";
    }
}
//表格样式-各行变色-鼠标覆盖事件
function ControlTableCss(tableId) {
    $("#" + tableId + " tr:gt(0):odd").attr("class", "trOddColor");
    $("#" + tableId + " tr:gt(0)").hover(function () {
        $(this).addClass("mouseOverColor");
    }, function () {
        $(this).removeClass("mouseOverColor");
    });
}
//合同编号
function colCprNoShowFormatter(celvalue, options, rowData) {

    if (celvalue == "本页合计") {
        return "<font style='font-weight:bolder'>本页合计</font>"
    }
    else {
        return "<font>" + celvalue + "</font>"
    }
}

//合同额
function colcprAcountShowFormatter(celvalue, options, rowData) {
    if (rowData["cpr_No"] == "本页合计") {
        return "<font style='font-weight:bolder'>" + celvalue + "</font>"
    }
    else {
        return celvalue;
    }
}

//实际合同总额
function colcprShijiAcountShowFormatter(celvalue, options, rowData) {
    if (rowData["cpr_No"] == "本页合计") {
        return "<font style='font-weight:bolder'>" + celvalue + "</font>"
    }
    else {
        return celvalue;
    }
}


//收费金额
function colSszeShowFormatter(celvalue, options, rowData) {
    if (rowData["cpr_No"] == "本页合计") {
        return "<font style='font-weight:bolder'>" + celvalue + "</font>"
    }
    else {
        return celvalue;
    }
}
//收费金额
function colKpjeShowFormatter(celvalue, options, rowData) {
    if (rowData["cpr_No"] == "本页合计") {
        return "<font style='font-weight:bolder'>" + celvalue + "</font>"
    }
    else {
        return celvalue;
    }
}
//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "cpr_ShowCoprationBymaster.aspx?flag=cprlist&cprid=" + rowData["cpr_Id"];
    return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

}

function colSfjdFormatter(celvalue, options, rowData) {
    return '<div class="progressbar" style="height:20px;margin:1px 1px;" title="' + celvalue + '%" rel="' + celvalue + '"></div>';
}

//查看
function colShowFormatter(celvalue, options, rowData) {
    if (celvalue != null && celvalue != "" && celvalue != undefined) {
        return '<a  rel="' + celvalue + '" class="cls_chk"   style="cursor:pointer;" cpr_name=' + rowData["cpr_Name"] + ' pro_id="' + rowData["pro_id"] + '" alt="查看开票">查看</a>';
    }
    else {
        return "";
    }
}
//开票
function colEditFormatter(celvalue, options, rowdata) {
    var url = "";
    if (celvalue != "") {
        url = '<a  rel="' + celvalue + '"  alt="开票"  style="cursor:pointer;" class="applyInvoice" cpr_name="' + rowdata["cpr_Name"] + '" pro_name="' + rowdata["pro_name"]+'"  pro_id="' + rowdata["pro_id"] + '" cst_name="' + rowdata["cst_name"] + '" cpr_no="' + rowdata["cpr_No"] + '" cpr_acount="' + rowdata["cpr_ShijiAcount"] + '">开票</a>';
    }
    return url;
}
//统计 
function completeMethod() {

    //隐藏收费列
    //首页参数不显示收费
    var paramtype = $("#HiddenParamType").val();


    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    var sumSfjd = 0;
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }

    var sumAcount = 0;  
    sumAcount = rowIds.length == 0 ? "" : parseFloat($("#" + rowIds[0]).find("td").eq(3).text()).toFixed(4);    
    var sumssze = rowIds.length == 0 ? "" : parseFloat($("#" + rowIds[0]).find("td").eq(4).text()).toFixed(4);
    var sumKpje = rowIds.length == 0 ? "" : parseFloat($("#" + rowIds[0]).find("td").eq(5).text()).toFixed(4);
    var sumShijiAcount = rowIds.length == 0 ? "" : parseFloat($("#" + rowIds[0]).find("td").eq(6).text()).toFixed(4);

    $("#jqGrid").footerData('set', { cpr_No: "所有合计", cpr_Acount: sumAcount, cpr_ShijiAcount: sumShijiAcount, ssze: sumssze, kpje: sumKpje }, false);


    //进度条
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("rel"));

        $(item).progressbar({
            value: percent
        });
    });
}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger("reloadGrid");
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        } else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }

    var rowIds = $("#jqGrid").jqGrid('getDataIDs').length;
    if (rowIds > 0) {
        var sumAcount = parseFloat($("#jqGrid").getCol("cpr_Acount", false, 'sum')).toFixed(4);
        var sumcprShijiAcount = parseFloat($("#jqGrid").getCol("cpr_ShijiAcount", false, 'sum')).toFixed(4);
        var sumSsze = parseFloat($("#jqGrid").getCol("ssze", false, 'sum')).toFixed(4);
        var sumKpje = parseFloat($("#jqGrid").getCol("kpje", false, 'sum')).toFixed(4);
        $("#jqGrid").addRowData((rowIds + 1), { cpr_No: "本页合计", cpr_Name: "", cpr_Id: "", cpr_Acount: sumAcount, cpr_ShijiAcount: sumcprShijiAcount, ssze: sumSsze, kpje: sumKpje }, "last");

        var currentPage = $("#jqGrid").getGridParam("page");
        var currentRowNum = $("#jqGrid").getGridParam("rowNum");
        var currentrowIds = (rowIds + 1);
        var setrowIds = currentrowIds;
        if (currentPage == 2) {
            $("#jqGrid").setRowData(setrowIds, { nid: "1" });
            setrowIds = currentRowNum + setrowIds;
        }

        $("#jqGrid").setRowData(setrowIds, { nid: "", cb: "", kpjd: "" });

    }
}