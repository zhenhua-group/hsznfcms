﻿$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Coperation/CoperationForReportListHandler.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '合同名称', '合同分类', '合同额(万元)', '报备单位', '报备时间', '录入人', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                              { name: 'cst_Id', index: 'cst_Id', hidden: true, editable: true },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 250, formatter: colNameShowFormatter },
                             { name: 'cpr_Type', index: 'cpr_Type', width: 100, align: 'center' },
                             { name: 'cpr_Acount', index: 'cpr_Acount', width: 80, align: 'center' },
                             { name: 'cpr_Unit', index: 'cpr_Unit', width: 100, align: 'center' },
                              { name: 'qdrq', index: 'qdrq', width: 100, align: 'center' },
                             { name: 'InsertUser', index: 'InsertUser', width: 80, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
                          ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()), "action": "Search" },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/CoperationForReportListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
        },
            {//添加
        },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true}]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var drp_type = $("#ctl00_ContentPlaceHolder1_drp_type").find("option:selected").text();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_cprname").val();
        var txt_start = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var txt_end = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/CoperationForReportListHandler.ashx",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, "txt_start": txt_start, "txt_end": txt_end, "drp_type": drp_type },
            page: 1
        }).trigger("reloadGrid");
    });
});

//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "ShowSingleCoperationForReportBymaster.aspx?flag=cprlist&cstid=" + rowData.cst_Id + "&cprid=" + rowData["cpr_Id"];
    return '<img src="../Images/buttons/icon_cpr.png" style="width: 16px; height: 16px; border: none;" /><a href="' + pageurl + '" alt="查看合同报备">' + celvalue + '</a>';

}

//查看
function colShowFormatter(celvalue, options, rowData) {
    var pageurl = "ShowSingleCoperationForReportBymaster.aspx?flag=cprlist&cstid=" + rowData.cst_Id + "&cprid=" + celvalue;
    return '<a href="' + pageurl + '" alt="查看合同报备">查看</a>';

}
//编辑
function colEditFormatter(celvalue, options, rowData) {
    if (celvalue != "") {
        var pageurl = "AlertCoperationForReportBymaster.aspx?cstid=" + rowData.cst_Id + "&cprid=" + celvalue;
        return '<a href="' + pageurl + '" alt="编辑合同报备" class="allowEdit">编辑</a>';
    }
}
//统计 
function completeMethod() {
    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    }
}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        } else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}