﻿/// <reference path="../../Coperation/cpr_CoperationAuditListBymaster.aspx" />
//接受消息
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
$(function () {
    //发起审批
    $("#btn_startApply").live("click", function () {
        // var projectsysno = $(this).next(":hidden").val();
        // var projname = $(this).next(":hidden").next(":hidden").val();


        messageDialog = $("#auditShow");
        sendMessageClass = new MessageCommon(messageDialog);
        //获取合同SysNo
        var coperationSysNo = $("#ctl00_ContentPlaceHolder1_hidcprId").val();
        var userSysNo = $("#ctl00_ContentPlaceHolder1_hiduserid").val();
        var objData =
         {
          CoperationSysNo: coperationSysNo,
          InUser: userSysNo
         };
        var query = Global.toJSON(objData);
        jsonDataEntity = query;
        startAppControl = $(this);
        //发起审批
        getUserAndUpdateAudit('0', '0', jsonDataEntity);
    });
    $("#btn_Send").click(function () {
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        } else {
            getUserAndUpdateAudit('0', '1', jsonDataEntity);
        }
    });
})
function sendMessage(jsonResult) {
    if (jsonResult == "1") {
        //提示消息
        alert("发起合同评审已成功！\n消息已成功发送到评审人等待审批！");
        //改变流程提示状态
        //CommonControl.SetApplyStatus(startAppControl);
        window.location.href = "cpr_CoperationAuditListBymaster.aspx";
    } else {
        alert("消息发送失败！");
    }
}

//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/CoperationAuditHandler.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    $.post(url, data, function (jsonResult) {
        //发起审批返回数据
        if (jsonResult == "0") {
            alert("流程评审错误，请联系管理员！");
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag == "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}

