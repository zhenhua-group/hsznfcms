﻿
//create by long 20130605

$(document).ready(function () {

    CommonControl.SetFormWidth();
    $("#rad2").click(function () {
        if ($(this).attr("checked") == "checked") {
            $("#Selecttime").css("display", "block");
        }

    });
    $("#rad1").click(function () {
        if ($(this).attr("checked") == "checked") {
            $("#Selecttime").css("display", "none");
        }

    });

    //统计按钮
    $("#btn_ok").click(function () {
        if ($("#rad2").attr("checked") == "checked") {
            //开始时间
            var txt_startdate = $("#txt_startdate").val();
            //结束时间
            var txt_finishdate = $("#txt_finishdate").val();
            var year = $("#drp_year").val();
            var msg = "";
            if (txt_startdate == "") {
                msg += "开始日期不能为空！<br/>";
            }
            var beginbijiao = bijiao(txt_startdate, year);
            var endbijiao = bijiao(txt_finishdate, year);

            if (beginbijiao == false) {
                msg += "所选择的的年份不一样，请重新选择！";
            }
            if (endbijiao == false) {
                msg += "所选择的年份不一样，请重新选择！";
            }
            //项目结束日期
            if (txt_finishdate == "") {
                msg += "结束日期不能为空！<br/>";
            }
            var result = duibi(txt_startdate, txt_finishdate);
            if (result == false) {
                msg += "开始时间大于结束时间！";
            }
            if (msg != "") {
                alert(msg);
                return false;
            } else {
                return true;
            }
        }
    });
});

//比较当前年份和选择的年份是否一致
function bijiao(a, b) {
    var arr = a.split("-");
    var startyear = arr[0];
    if (b == startyear) {
        return true;
    } else {
        return false;
    }
};
//比较时间
function duibi(a, b) {
    var arr = a.split("-");
    var starttime = new Date(arr[0], arr[1], arr[2]);
    var starttimes = starttime.getTime();

    var arrs = b.split("-");
    var lktime = new Date(arrs[0], arrs[1], arrs[2]);
    var lktimes = lktime.getTime();

    if (starttimes >= lktimes) {
        return false;
    }
    else
        return true;
}