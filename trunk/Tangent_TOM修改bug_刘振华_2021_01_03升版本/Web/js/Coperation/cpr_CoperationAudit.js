﻿var auditRecordLength = 2;
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
//获取消息列表参数
var pageIndex;
var MessageType;
var TypePost;
var MessageAction;
var Aflag;
var MessageKeys;
$(function () {

    $(":radio[name=BuildNature][value=" + $("#ctl00_ContentPlaceHolder1_HiddenBuildNature").val() + "]").attr("checked", "checked");
    $(":radio[name=BuildType][value=" + $("#ctl00_ContentPlaceHolder1_HiddenCoperationType").val() + "]").attr("checked", "checked");
    $("input", $("#CoperationBaseInfo")).attr("disabled", true);
    // 设置文本框样式
    CommonControl.SetTextBoxStyle();
    CommonControl.SetFormWidth();

    //获取消息列表参数
    pageIndex = $("#pageIndex").val();
    MessageType = $("#MessageType").val();
    TypePost = $("#TypePost").val();
    MessageAction = $("#MessageAction").val();
    Aflag = $("#Aflag").val();
    MessageKeys = $("#MessageKeys").val();

    //当前合同审核记录
    var auditRecordStatus = $("#AuditRecordStatus").val();

    //检查用户权限
    var hasPower = TG.Web.Coperation.cpr_CoperationAuditBymaster.CheckPower(auditRecordStatus, $("#HiddenLoginUser").val(), $("#HiddenNeedLegalAdviser").val());

    //取得审核用户
    var auditUserArr = $("#HiddenAuditUser").val().split(",");
    //取得审核时间
    var auditDate = $("#HiddenAuditDate").val().split(",");

    //消息审核状态
    var messageStauts = $("#hiddenMessageStatus").val();

    //判断审批阶段显示审批表单
    if (messageStauts == auditRecordStatus || messageStauts == "") {
        InitViewState(auditRecordStatus, hasPower.value);
    } else {
        InitViewMessageState(messageStauts);
    }


    //取得承接部门意见
    var UndertakeProposalArr = $("#HiddenUndertakeProposal").val().split("|");
    if ($("#HiddenUndertakeProposal").val().length > 0) {
        for (var i = 0; i < UndertakeProposalArr.length; i++) {
            //审核意见不为空的场合，表示当前选中的checkbox是否。更改checkbox是和否的选中状态，并且填充值
            if (UndertakeProposalArr[i].length > 0) {
                $(":text:eq(" + i + ")", $("#undertakeDepartment")).val(UndertakeProposalArr[i]).css({ "color": "red", "marginLeft": "5px", "display": "inline" }).siblings().find(":radio.no").attr("checked", "checked");
                $(":text", $("#undertakeDepartment")).attr("disabled", true);
            }
        }
        //填充审核人和审核时间
        $("#AuditUser", $("#undertakeDepartment")).html(auditUserArr[0] + " :<br/> " + auditDate[0]);
        //承接部门radio 为不可用
        $(":radio", $("#undertakeDepartment")).attr("disabled", "disabled");
    }

    //取得经营部门意见
    var OperateDepartmentProposalArr = $("#HiddenOperateDepartmentProposal").val().split("|");
    if ($("#HiddenOperateDepartmentProposal").val().length > 0) {
        for (var i = 0; i < OperateDepartmentProposalArr.length; i++) {
            //审核意见不为空的场合，表示当前选中的checkbox是否。更改checkbox是和否的选中状态，并且填充值
            if (OperateDepartmentProposalArr[i].length > 0) {
                $(":text:eq(" + i + ")", $("#OperateDepartment")).val(OperateDepartmentProposalArr[i]).css({ "color": "red", "marginLeft": "5px", "display": "inline" }).siblings().find(":radio.no").attr("checked", "checked");
                $(":text", $("#OperateDepartment")).attr("disabled", true);
            }
        }
        $(":radio", $("#OperateDepartment")).attr("disabled", "disabled");
        $("#AuditUser", $("#OperateDepartment")).html(auditUserArr[1] + " :<br/>" + auditDate[1]);
        //绑定管理级别
        $("#HiddenManageLevel").val() == "0" ? $("#coperationAuditManageLevelOneRadioButton").attr("checked", "checked") : $("#coperationAuditManageLevelTwoRadioButton").attr("checked", "checked");
        //绑定是否需要法律顾问
        $("#HiddenNeedLegalAdviser").val() == "0" ? $("#UnNeedLegalAdviserRadioButton").attr("checked", "checked") : $("#NeedLegalAdviserRadioButton").attr("checked", "checked");

    }
    //取得法律顾问意见
    var legalAdviserProposal = $("#HiddenLegalAdviserProposal").val();
    $("#LegalAdviserTextarea").val(legalAdviserProposal);
    if (legalAdviserProposal.length > 0) {
        $("#LegalAdviserTextarea").attr("disabled", "disabled");
        $("#AuditUser", $("#LegalAdviserDepartment")).html(auditUserArr[auditRecordLength] + "   :<br/> " + auditDate[auditRecordLength]);
    } else {
        //如果法律意见为空，表示当前没有法律顾问这个环节。审核流程对应的数当 -1
        auditRecordLength--;
    }

    //取得技术部门意见
    var TechnologyDepartmentProposalArr = $("#HiddenTechnologyDepartmentProposal").val().split("|");
    if ($("#HiddenTechnologyDepartmentProposal").val().length > 0) {
        for (var i = 0; i < TechnologyDepartmentProposalArr.length; i++) {
            if (TechnologyDepartmentProposalArr[i].length > 0) {
                //审核意见不为空的场合，表示当前选中的checkbox是否。更改checkbox是和否的选中状态，并且填充值
                $(":text:eq(" + i + ")", $("#TechnologyDepartment")).val(TechnologyDepartmentProposalArr[i]).css({ "color": "red", "marginLeft": "5px", "display": "inline" }).siblings().find(":radio.no").attr("checked", "checked");
                $("input", $("#TechnologyDepartment")).attr("disabled", true);
            }
        }
        TechnologyDepartmentProposalArr[TechnologyDepartmentProposalArr.length - 1] == 0 ? $("#ManagementLevelOne").attr("checked", true) : $("#ManagementLevelTwo").attr("checked", true);
        $("#AuditUser", $("#TechnologyDepartment")).html(auditUserArr[auditRecordLength + 1] + "  :<br/> " + auditDate[auditRecordLength + 1]);
    }

    //取得总经理意见
    var GeneralManagerProposal = $("#HiddenGeneralManagerProposal").val();
    if (GeneralManagerProposal && GeneralManagerProposal.length > 0) {
        $("#GeneralManagerProposalTextarea").val(GeneralManagerProposal).attr("disabled", true).css("color", "blue");
        $("#AuditUser", $("#GeneralManagerTable")).text(auditUserArr[auditRecordLength + 2]);
        $("#AuditDate", $("#GeneralManagerTable")).text(auditDate[auditRecordLength + 2]);
    }

    //取得董事长意见
    var BossProposal = $("#HiddenBossProposal").val();
    if (BossProposal && BossProposal.length > 0) {
        $("#BossProposalTextarea").val(BossProposal).attr("disabled", true).css("color", "blue");
        $("#AuditUser", $("#BossTable")).text(auditUserArr[4]);
        $("#AuditDate", $("#BossTable")).text(auditDate[4]);
    }

    //审核通过按钮
    $("#btnApproval").click(function () {
        messageDialog = $("#auditShow").messageDialog;
        sendMessageClass = new MessageCommon(messageDialog);
        var auditObj = {
            SysNo: $("#HiddenAuditRecordSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            Status: auditRecordStatus,
            ManageLevel: $(":radio[name=coperationAuditMamageLevel]:checked").val(),
            NeedLegalAdviser: $(":radio[name=coperationNeedLegalAdviser]:checked").val()
        };
        //判断当前是在哪个状态
        switch (auditRecordStatus) {
            //新规状态                                                
            case "A":
                auditObj.UndertakeProposal = GetAuditContext("undertake");
                break;
            //承接部门通过                                       
            case "B":
                //B为部门负责人审核通过之后再次审核通过。结果为D
                auditObj.OperateDepartmentProposal = GetAuditContext("Operate");
                break;
            //经营处通过                                       
            case "D":
                //是否需要法律顾问
                if ($("#HiddenNeedLegalAdviser").val() == "1") {
                    //取得法律顾问是否填写值
                    if ($.trim($("#LegalAdviserTextarea").val()).length == 0) {
                        alert("法律意见不能为空！");
                        return false;
                    }
                    auditObj.LegalAdviserProposal = $.trim($("#LegalAdviserTextarea").val());
                } else {
                    //不需要法律顾问的场合，取得技术部意见
                    auditObj.TechnologyDepartmentProposal = GetAuditContext("Technology");
                }
                break;
            case "F":
                //法律顾问通过
                auditObj.TechnologyDepartmentProposal = GetAuditContext("Technology");
                break;
            case "H":
                //技术部通过
                auditObj.GeneralManagerProposal = $("#GeneralManagerProposalTextarea").val();
                break;
            case "C":
            case "E":
            case "G":
                //这段case因流程修改而弃用，不影响运行。暂时留着以备不时之需
                auditObj.GeneralManagerProposal = $("#GeneralManagerProposalTextarea").val();
                break;
        }
        var jsonObj = Global.toJSON(auditObj);
        jsonDataEntity = jsonObj;
        //合同最后完成阶段
        if (auditRecordStatus == "H") {
            getUserAndUpdateAudit('1', '1', jsonDataEntity);
            return false;
        }
        else if (auditObj.ManageLevel == "1" && auditRecordStatus == "F") {
            getUserAndUpdateAudit('1', '1', jsonDataEntity);
            return false;
        } //所管项目到技术部
        else if (auditObj.ManageLevel == "1" && auditRecordStatus == "D") {
            //需要法律顾问
            if ($("#HiddenNeedLegalAdviser").val() == "1") {
                getUserAndUpdateAudit('1', '0', jsonDataEntity);
            }
            else {
                getUserAndUpdateAudit('1', '1', jsonDataEntity);
                return false;
            }

        }
        else {
            getUserAndUpdateAudit('1', '0', jsonDataEntity);
        }
    });

    //审核不通过按钮
    $("#btnRefuse").click(function () {
        var auditObj = {
            SysNo: $("#HiddenAuditRecordSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            Status: auditRecordStatus
        };
        switch (auditRecordStatus) {
            //新规状态                                                
            case "A":
                auditObj.UndertakeProposal = GetAuditContext("undertake");
                break;
            case "B":
                //B为部门负责人审核通过之后再次审核通过。结果为D
                auditObj.OperateDepartmentProposal = GetAuditContext("Operate");
                break;
            case "D":
                if ($("#HiddenNeedLegalAdviser").val() == "1") {
                    if ($.trim($("#LegalAdviserTextarea").val()).length == 0) {
                        alert("法律意见不能为空！");
                        return false;
                    }
                    auditObj.LegalAdviserProposal = $.trim($("#LegalAdviserTextarea").val());
                } else {
                    auditObj.TechnologyDepartmentProposal = GetAuditContext("Technology");
                }
                break;
            case "F":
                auditObj.TechnologyDepartmentProposal = GetAuditContext("Technology");
                break;
            case "H":
                auditObj.GeneralManagerProposal = $("#GeneralManagerProposalTextarea").val();
                break;
            case "C":
            case "E":
            case "G":
                auditObj.GeneralManagerProposal = $("#GeneralManagerProposalTextarea").val();
                break;
        }
        var jsonObj = Global.toJSON(auditObj);

        Global.SendRequest("/HttpHandler/CoperationAuditHandler.ashx", { "Action": 2, "data": jsonObj, "flag": 1 }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("评审失败！");
            } else {
                if (jsonResult == "1") {

                    //改变消息状态
                    var msg = new MessageCommProjAllot($("#msgno").val());
                    msg.ReadMsg();
                    //消息
                    alert("评审不通过成功，消息已发送给申请人！");
                }

                window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
            }
        });
    });

    //返回重新审核按钮
    $("#FallBackCoperaion").click(function () {
        window.location.href = "/Coperation/cpr_CoperationAuditListBymaster.aspx";
        // $.post("/HttpHandler/CoperationAuditHandler.ashx", { "Action": 3, "CoperationAuditSysNo": $("#HiddenAuditRecordSysNo").val() }, function (jsonResult) {
        //查询系统新消息
        //  CommonControl.GetSysMsgCount("/Coperation/cpr_CoperationAuditList.aspx");
        // });
    });
    $("#btn_Send").click(function () {
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        } else {
            getUserAndUpdateAudit('1', '1', jsonDataEntity);
        }
    });
    //实例化类容
    //    messageDialog = $("#msgReceiverContainer").messageDialog({
    //        "button": {
    //            "发送消息": function () {
    //                //选中用户
    //                var _$mesUser = $(":checkbox[name=messageUser]:checked");

    //                if (_$mesUser.length == 0) {
    //                    alert("请至少选择一个流程评审人！");
    //                    return false;
    //                }

    //                getUserAndUpdateAudit('1', '1', jsonDataEntity);
    //            },
    //            "关闭": function () {
    //                messageDialog.hide();
    //            }
    //        }
    //    });
    //    sendMessageClass = new MessageCommon(messageDialog);


    $("#btnReport").click(function () {
        var data = "coperationSysNo=" + $.trim($("#HiddenCoperationSysNo").val()) + "&level=" + $.trim($("#HiddenManageLevel").val()) + "&auditNo=" + $.trim($("#HiddenAuditRecordSysNo").val()) + "&option=0";
        window.location.href = "ExportWord.aspx?" + data;
    });

});
//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/CoperationAuditHandler.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    Global.SendRequest(url, data, null, null, function (jsonResult) {
        //改变消息状态
        if (flag == "1") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
        }

        if (jsonResult == "0") {
            alert("流程评审错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("合同评审流程完成，已全部通过！");
            //查询系统新消息
            // window.history.back();
            window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {

        alert("合同评审已成功！\n消息已成功发送到评审人等待审批！");

        //查询系统新消息
        //window.history.back();

        window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
    } else {
        alert("消息发送失败！");
    }
}

//根据标识取得内容
function GetAuditContext(indentity) {
    //取得当前审核进度的意见内容
    var inputs = $(":text", $("#" + indentity + "Department"));
    var values = "";
    $.each(inputs, function (index, item) {
        values += $(item).val() + "|";
    });
    //表示是技术部门的场合
    if (indentity == "Technology") {
        values += $(":radio[name=ManagementLevel]:checked", $("#" + indentity + "Department")).val() + "|";
    }
    values = values.substring(0, values.length - 1);
    return values;
}

//审核不通过按钮和审核通过按钮不可用
function DisableButton() {
    $(":button[name=controlBtn]").attr("disabled", true);
}

//重新申请审批按钮
function AuditAgain() {
    $("#FallBackCoperaion").show();
}

//没有权限审核
function NoPowerAudit() {
    $("#NoPowerTable").show();
}

function VisbleButton() {
    $("#btnApproval").hide();
    $("#btnRefuse").hide();
}

//初始化页面状态
function InitViewState(auditStatus, hasPower) {
    var length = 0;
    switch (auditStatus) {
        case "B":
            length = 2;
            if (hasPower == "0") {
                //没有权限的场合
                length--;
                NoPowerAudit();
                DisableButton();
            }
            break;
        case "C":
            length = 1;
            DisableButton();
            AuditAgain();
            VisbleButton();
            break;
        case "D":
            if ($("#HiddenNeedLegalAdviser").val() == "0") {
                //不需要法律顾问的场合
                length = 4;
            } else {
                //需要法律顾问的场合
                length = 3;
                $("#HiddenLegalAdviserProposal").attr("disabled", "disabled");
            }
            if (hasPower == "0") {
                length--;
                NoPowerAudit();
                DisableButton();
            }
            break;
        case "E":
            length = 2;
            DisableButton();
            AuditAgain();
            VisbleButton();
            break;
        case "F":
            length = 4;
            if (hasPower == "0") {
                length--;
                NoPowerAudit();
                DisableButton();
            }
            break;
        case "G":
            length = 3;
            DisableButton();
            AuditAgain();
            VisbleButton();
            break;
        case "H":
            length = 5;
            if ($("#HiddenManageLevel").val() == "1") {
                length--;
                $("input", $("#MainDiv")).attr("disabled", true);
                $("#btnApproval").hide();
                $("#btnRefuse").hide();
                $("#exprot").attr("style", "display:inline-block;");
                $("#back").css("display", "inline-block");
            } else {
                if (hasPower == "0") {
                    length--;
                    NoPowerAudit();
                    DisableButton();
                }
            }
            break;
        case "I":
            length = 4;
            DisableButton();
            AuditAgain();
            VisbleButton();
            break;
        case "J":
        case "K":
            length = 5;
            $("input", $("#MainDiv")).attr("disabled", true);
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            $("#exprot").attr("style", "display:inline-block;");
            $("#back").css("display", "inline-block");
            break;
    }
    showTable(length);
}

//初始化消息状态页面信息--审核人只能显示自己审核信息
function InitViewMessageState(messageStauts) {
    var length = 0;
    $("table:eq(0)", $("#TableContainer")).hide();
    if (messageStauts == "A") {
        length = 0;
    }
    if (messageStauts == "B" || messageStauts == "C") {
        length = 1;
    }
    if (messageStauts == "D" || messageStauts == "E") {

        if ($("#HiddenNeedLegalAdviser").val() == "0") {
            //不需要法律顾问的场合
            length = 3;
        } else {
            //需要法律顾问的场合
            length = 2;
            $("#HiddenLegalAdviserProposal").attr("disabled", "disabled");
        }
    }
    if (messageStauts == "F" || messageStauts == "G") {
        length = 3;
    }
    if (messageStauts == "H") {

        length = 4;
        if ($("#HiddenManageLevel").val() == "1") {
            length--;
            $("input", $("#MainDiv")).attr("disabled", true);
        }
    }
    if (messageStauts == "I") {
        length = 4;
    }
    if (messageStauts == "J" || messageStauts == "K") {
        length = 4;
        $("input", $("#MainDiv")).attr("disabled", true);
    }
    $("#btnApproval").hide();
    $("#btnRefuse").hide();
    showMessageTable(length);
}
function showTable(length) {
    //按审批流程显示审批表格
    $("table:lt(" + length + ")", $("#TableContainer")).show();
    if ($("#HiddenNeedLegalAdviser").val() == "0") {
        $("#LegalAdviserDepartment").hide();
    }
}
function showMessageTable(length) {
    $("table:eq(" + length + ")", $("#TableContainer")).show();
    if ($("#HiddenNeedLegalAdviser").val() == "0") {
        $("#LegalAdviserDepartment").hide();
    }
}
