﻿$(document).ready(function () {
    $("#ctl00_ContentPlaceHolder1_hid_cprid").val(hid_cprid);
    //绑定权限
    showDivDialogClass.UserRolePower = {
        "previewPower": $("#previewPower").val(),
        "userSysNum": $("#userSysNum").val(),
        "userUnitNum": $("#userUnitNum").val(),
        "notShowUnitList": ""
    };
    //判断合同类型为规划合同  qpl  20140117
    $("#ctl00_ContentPlaceHolder1_ddcpr_Type").change(function () {
        if ($(this).find(":selected").text().indexOf('规划') > -1) {
            $("#ctl00_ContentPlaceHolder1_txt_buildArea").css("background-color", " #ffffff").val("0");
        }
        else {
            $("#ctl00_ContentPlaceHolder1_txt_buildArea").css("background-color", " #FFC");
        }
    });

    //保存合同信息
    $("#btn_Save").click(function () {

        //取得文本值
        var txtcpr_No = $("#ctl00_ContentPlaceHolder1_txtcpr_No").val();
        var txtcCprName = $("#ctl00_ContentPlaceHolder1_txt_cprName").val();
        var txtCprUnit = $("#ctl00_ContentPlaceHolder1_txt_cprBuildUnit").val();
        var drpBuildType = $("#ctl00_ContentPlaceHolder1_drp_buildtype").val();
        var txtAddress = $("#ctl00_ContentPlaceHolder1_txt_cprAddress").val();
        var txt_cjbm = $("#ctl00_ContentPlaceHolder1_txt_cjbm").val();
        var txtRegisterDate = $("#ctl00_ContentPlaceHolder1_txtRegisterDate").val();
        var txtcpr_Remark = $("#ctl00_ContentPlaceHolder1_txtcpr_Remark").val();
        var txtcjbm = $("#ctl00_ContentPlaceHolder1_txt_cjbm").val();
        var txtFParty = $("#ctl00_ContentPlaceHolder1_txtFParty").val();
        var txtBuildArea = $("#ctl00_ContentPlaceHolder1_txt_buildArea").val();
        var txtCompleteDate = $("#ctl00_ContentPlaceHolder1_txtCompleteDate").val();
        var txtFParty = $("#ctl00_ContentPlaceHolder1_txtFParty").val();
        var txt_jiafphone = $("#ctl00_ContentPlaceHolder1_txt_jiafphone").val();
        //数字验证正则
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        //用户
        if ($("#ctl00_ContentPlaceHolder1_hid_cstid").val() == "") {
            msg += "请选择合同报备关联的客户信息！</br>";
        }
        //请输入报备合同名称
        if ($.trim(txtcCprName).length == 0) {
            msg += "请输入报备合同名称！</br>";
        }
        //合同分类
        if ($("#ctl00_ContentPlaceHolder1_ddcpr_Type").val() == "-1") {
            msg += "请选择合同分类！</br>";
        }
        //项目建设地点
        if ($.trim(txtAddress).length == 0) {
            msg += "请输入项目建设地点！</br>";
        }
        //建设类别
        if (drpBuildType == "-1") {
            msg += "请选择建设类别！</br>";
        }
        //建设规模
        if (txtBuildArea == "") {
            msg += "请输入建设规模！</br>";
        }
        else {
            if (!reg_math.test(txtBuildArea)) {
                msg += "输入规模格式不正确！</br>";
            }
        }
        //结构形式
        if (!IsStructCheckNode('struct')) {
            msg += "请选择结构样式！</br>";
        }
        //建筑分类
        if (!IsStructCheckNode('structtype')) {
            msg += "请选择建筑分类！</br>";
        }
        //楼层数判断
        if ($("#ctl00_ContentPlaceHolder1_txt_upfloor").val() != "") {
            var reg = /^[1-9]\d*$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_upfloor").val())) {
                msg += "输入地上层数格式错误，请输入数字！</br>";
            }
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_downfloor").val() != "") {
            var reg = /^[1-9]\d*$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_downfloor").val())) {
                msg += "输入地下层数格式错误，请输入数字！</br>";
            }
        }
        //承接部门
        if (txt_cjbm == "") {
            msg += "请选择承接部门！</br>";
        }
        //甲方负责人
        if (txtFParty == "") {
            msg += "请输入甲方负责人！</br>";
        }
        //甲方电话
        if (txt_jiafphone == "") {
            msg += "请输入甲方负责人电话！</br>";
        }
        //工程地点
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_ProjectPosition").val()) == "") {
            msg += "请选择工程地点！</br>";
        }
        //合同金额
        if ($("#ctl00_ContentPlaceHolder1_txtcpr_Account").val() == "") {
            msg += "请输入合同金额！</br>";
        }
        else {
            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txtcpr_Account").val())) {
                msg += "合同金额格式不正确！</br>";
            }
        }
        if ($.trim($("#ctl00_ContentPlaceHolder1_ddProfessionType").val()) == "") {
            msg += "请选择行业性质！</br>";
        }
        if ($.trim($("#ctl00_ContentPlaceHolder1_ddSourceWay").val()) == "") {
            msg += "请选择工程来源！</br>";
        }
        //电话
        if ($("#ctl00_ContentPlaceHolder1_txt_fzphone").val() != "") {
            var reg = /^(0|86|17951)?(13[0-9]|15[012356789]|18[0236789]|14[57])[0-9]{8}$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_fzphone").val())) {
                msg += "手机号码格式输入不正确！<br/>";
            }
        }
        //电话
        if ($("#ctl00_ContentPlaceHolder1_txt_jiafphone").val() != "") {
            var reg = /^(0|86|17951)?(13[0-9]|15[012356789]|18[0236789]|14[57])[0-9]{8}$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_jiafphone").val())) {
                msg += "手机号码输入格式不正确！<br/>";
            }
        }

        //投资额
        if ($("#ctl00_ContentPlaceHolder1_txtInvestAccount").val() != "") {
            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txtInvestAccount").val())) {
                msg += "投资额请输入数字！</br>";
            }
        }
        //实际合同
        if ($("#ctl00_ContentPlaceHolder1_txtcpr_Account0").val() != "") {
            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txtcpr_Account0").val())) {
                msg += "实际合同额请输入数字！</br>";
            }
        }
        //实际投资
        if ($("#ctl00_ContentPlaceHolder1_txtInvestAccount0").val() != "") {
            if (!reg_math.test($("#ctl00_ContentPlaceHolder1_txtInvestAccount0").val())) {
                msg += "实际投资请输入数字！</br>";
            }
        }
        //是否立项
        if ($("#ctl00_ContentPlaceHolder1_drp_proapproval").val() == "-1") {
            msg += "请选择是否立项！";
        }
        if (txtcpr_Remark == "") {
            $("#ctl00_ContentPlaceHolder1_txtcpr_Remark").val("");
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    //首先加载客户列表
    var chooseCustomer = new ChooseCustomer($("#chooseCustomerContainer"), chooseCustomerCallBack);
    //查询 bianhao 按钮
    $("#btn_getcprnum").click(function () {
        //赋值
        showDivDialogClass.SetParameters({
            "pageSize": "0"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "0", "cprNumUnit", CprNumUnitCallBack);
    })

    //点击单选控件重新加载
    $(":radio[name=cprType]").click(function () {
        var typeID = $("#sele_cprNumUnit option:selected").val();
        if (typeID > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", typeID, "false", "1", "cprNumUnit", BindCprNumDataCallBack);
        }
    });
    //选择合同编号确定按钮
    $("#btn_seleCprNum").click(function () {
        var cpr_Num = $("#sele_cprNum option:selected").text();
        var cpr_ID = $("#sele_cprNum option:selected").val();
        if (cpr_ID > 0) {
            $("#ctl00_ContentPlaceHolder1_txtcpr_No").val(cpr_Num);
            $("#ctl00_ContentPlaceHolder1_hid_cprno").val(cpr_Num);
            //$("#cprNumDialogDiv").dialog().dialog("close");
        } else {
            $("#cprNo_noSelect").show();
            return false;
        }
    });
    //选择项目经理
    $("#btn_gcfz").click(function () {
        //先赋值
        showDivDialogClass.SetParameters({
            "prevPage": "gcfzr_prevPage",
            "firstPage": "gcfzr_firstPage",
            "nextPage": "gcfzr_nextPage",
            "lastPage": "gcfzr_lastPage",
            "gotoPage": "gcfzr_gotoPageIndex",
            "allDataCount": "gcfzr_allDataCount",
            "nowIndex": "gcfzr_nowPageIndex",
            "allPageCount": "gcfzr_allPageCount",
            "gotoIndex": "gcfzr_pageIndex",
            "pageSize": "10"
        });
        var unit_ID = $("#select_gcFzr_Unit").val();
        if (unit_ID < 0 || unit_ID == null) {
            //绑定工程负责部门
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "gcfzrCprUnit", CprTypeUnitCallBack);
        }
    });
    //选择承接部门
    //承接部门
    $("#btn_cjbm").click(function () {

        //加载数据-先赋值
        showDivDialogClass.SetParameters({
            "prevPage": "cjbm_prevPage",
            "firstPage": "cjbm_firstPage",
            "nextPage": "cjbm_nextPage",
            "lastPage": "cjbm_lastPage",
            "gotoPage": "cjbm_gotoPageIndex",
            "allDataCount": "cjbm_allDataCount",
            "nowIndex": "cjbm_nowPageIndex",
            "allPageCount": "cjbm_allPageCount",
            "gotoIndex": "cjbm_pageIndex",
            "pageSize": "10"
        });
        var isValue = $("#ctl00_ContentPlaceHolder1_txt_cjbm").val().length;
        if (isValue == null || isValue == undefined || isValue <= 0) {

            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "proCjbm", ProCjbmCallBack);
            BindAllDataCount(); //绑定总数据
        }
    });
    //合同类型
    $("#btn_cprType").click(function () {
        //加载数据-先赋值
        showDivDialogClass.SetParameters({
            "pageSize": "10"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "cprType", CproTypeCallBack);

    });
    //甲方负责人
    $("#btn_jffz").click(function () {

        showDivDialogClass.SetParameters({
            "prevPage": "jffzr_prevPage",
            "firstPage": "jffzr_firstPage",
            "nextPage": "jffzr_nextPage",
            "lastPage": "jffzr_lastPage",
            "gotoPage": "jffzr_gotoPageIndex",
            "allDataCount": "jffzr_allDataCount",
            "nowIndex": "jffzr_nowPageIndex",
            "allPageCount": "jffzr_allPageCount",
            "gotoIndex": "jffzr_pageIndex",
            "pageSize": "10"
        });
        var selectVal = $("#select_jffzrMem").val();
        if (selectVal == null || selectVal == undefined) {

            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "jffzrCop", JffzrUnitCallBack);
        }
    });
    //增加计划收费
    $("#btn_AddSf").click(function () {
        $("h4", "#TJSF").text("添加计划收费");
        var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").val();
        chargeid = "0";
        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txtcpr_Account").val())) {
                alert("合同金额格式不正确！");
                return false;
            } else {
                AddChargePlan();
                $("#chargeType_notselect").hide();
                $("#jine_notnull").hide();
                $("#jine_notint").hide();
                $("#jine_xiaoyu").hide();
                $("#date_notnull").hide();
                $("#span_Percent").hide();
                $("#span_PercentNotInt").hide();
            }
        } else {
            alert("请填写合同额！");
            return false;
        }

    });
    //增加计划收费确定按钮
    $("#btn_addPlanCharge").click(function () {
        var tempId = $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
        var txtPlanChargeNum = $("#txt_planChargeNum").val();
        var lblCprMoney = $("#lbl_copMoney").text();
        var txtDatePic = $("#txt_datePicker").val();
        var txtSkr = $("#ctl00_ContentPlaceHolder1_userShortName").val();
        var txtRemark = $("#txt_chargeRemark").val();
        var data = "action=addcprsk&flag=add";
        //金额
        if (txtPlanChargeNum == "") {
            $("#jine_xiaoyu").hide();
            $("#jine_notint").hide();
            $("#jine_notnull").show();
            return false;
        } else {
            $("#jine_notnull").hide();
            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+)$/;
            if (!reg.test(txtPlanChargeNum)) {
                $("#jine_xiaoyu").hide();
                $("#jine_notint").show();
                return false;
            } else {
                $("#jine_notint").hide();
            }
            //判断合同额
            if (parseFloat(lblCprMoney) < parseFloat(txtPlanChargeNum)) {
                $("#jine_xiaoyu").show();
                return false;
            } else {
                $("#jine_xiaoyu").hide();
            }
            data += "&jine=" + escape(txtPlanChargeNum);
        }
        //合同总金额
        data += "&allcount=" + escape($("#lbl_copMoney").text());
        //时间
        if (txtDatePic == "") {
            $("#date_notnull").show();
            return false;
        } else {
            $("#date_notnull").hide();
            data += "&date=" + escape($("#txt_datePicker").val());
        }
        //收款人
        if (txtSkr == "") {
            $("#skr_notnull").show();
            return false;
        } else {
            $("#skr_notnull").hide();
            data += "&skr=" + escape(txtSkr);
        }

        data += "&mark=" + escape(txtRemark) + "&cprid=" + tempId + "&chargeid=" + chargeid + "&payType=" + escape($("#chargeTypeSelect option:selected").text());
        //data = encodeURI(data);
        //添加收款信息
        $.ajax({
            type: "GET",
            url: "../HttpHandler/CommHandler.ashx",
            data: data,
            dataType: "text",
            success: function (result) {
                if (result == "yes") {
                    //alert("添加收款信息成功!");
                    loadCprCharge();
                    $("#ctl00_ContentPlaceHolder1_txtcpr_Account").attr("readonly", "readonly");
                }
                else {
                    alert("计划收款金额超过合同总额！合同余额：" + result + "万元。");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误！");
            }
        });
    });
    //修改计划收费
    $("span[class=update]").live("click", function () {
        var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").val();
        chargeid = $(this).attr("rel");
        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txtcpr_Account").val())) {
                alert("合同金额格式不正确！");
                return false;
            } else {
                $("h4", "#TJSF").text("编辑计划收费");
                $("#lbl_copMoney").text(cprAllcount);
                //初始化信息
                var trtd = $(this).parent().parent().find("TH");
                $("#txt_planChargeNum").val(trtd.eq(2).text());
                $("#txt_datePicker").val(trtd.eq(3).text());
                $("#txt_chargeRemark").val(trtd.eq(4).text());
                var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").val(); //总金额
                $("#lbl_copMoney").text(cprAllcount);
                //收费百分比
                var persent = trtd.eq(1).text();
                persent = persent.substring(0, (persent.length - 1));
                $("#planChargeNumPercent").val(persent);
                //收费金额
                $("#txt_planChargeNum").unbind('change').change(function () {
                    var chargeNum = $(this).val();
                    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
                    if (!reg.test(chargeNum)) {
                        $("#jine_notint").show();
                        return false;
                    } else {
                        $("#jine_notint").hide();
                    }
                    var lblCprMoney = $("#lbl_copMoney").text();
                    $("#planChargeNumPercent").val((parseFloat(chargeNum) / parseFloat(lblCprMoney) * 100).toFixed(2));
                });

                $("#chargeType_notselect").hide();
                $("#jine_notnull").hide();
                $("#jine_notint").hide();
                $("#jine_xiaoyu").hide();
                $("#date_notnull").hide();
            }
        } else {
            alert("请填写合同额！");
            return false;
        }

    });
    //添加子项
    $("#btn_addSub").click(function () {
        var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").val();
        var cprbuildArea = $("#ctl00_ContentPlaceHolder1_txt_buildArea").val();
        var sumOfPlanArea = $("#ctl00_ContentPlaceHolder1_txt_area").val();
        subid = "0";

        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txtcpr_Account").val())) {
                alert("合同金额格式不正确！");
                return false;
            } else if (!reg.test(cprbuildArea)) {
                alert("建筑面积格式不正确！");
                return false;
            }
            else {
                var timespan = new Date();
                $("h4", "#ZXObject").text("编辑工程子项");
                //清空
                $("#addsubName").val("");
                $("#addsubArea").val("");
                $("#addsubJine").val("");
                $("#addsubHouseArea").val("");
                $("#addsubEachAreaMoney").val("");
                $("#areaTxtAddSubRemark").val("");

            }
        }
        else {
            alert("请填写合同额！");
            return false;
        }
    });

    //修改子项
    $("span[id=update]").live("click", function () {
        var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").val();
        var cprbuildArea = $("#ctl00_ContentPlaceHolder1_txt_buildArea").val();

        subid = $(this).attr("rel");

        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txtcpr_Account").val())) {
                alert("合同金额格式不正确！");
                return false;
            } else if (!reg.test(cprbuildArea)) {
                alert("建筑面积格式不正确！");
                return false;
            }
            else {
                var timespan = new Date();
                $("h4", "#ZXObject").text("编辑工程子项");
                //查询信息
                var trtd = $(this).parent().parent().find("TH");
                $("#addsubName").val(trtd.eq(1).text());
                $("#addsubArea").val(trtd.eq(2).text());
                $("#addsubJine").val(trtd.eq(3).text());

                $("#areaTxtAddSubRemark").val(trtd.eq(4).text());
            }
        }
        else {
            alert("请填写合同额！");
        }
    });
    //工程子项-按钮事件
    $("#btn_addsubChild").click(function () {
        var addsubName = $("#addsubName").val();
        var addsubArea = $("#addsubArea").val();
        var addsubJine = $("#addsubJine").val();
        var areaTxtAddSubRemark = $("#areaTxtAddSubRemark").val();
        //总合同面积
        var sumOfArea = $("#ctl00_ContentPlaceHolder1_txt_buildArea").val();
        //总合同额
        var sumOfMoney = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").val();
        var cstno = $("#ctl00_ContentPlaceHolder1_hid_cprid").val();

        var subAllArea = 0; //已添加的总面积
        var subAllMoney = 0; //已添加的总金额
        var tableTrCount = $("#datas tr:gt(0)").length;
        for (var i = 1; i <= tableTrCount; i++) {

            if ($.trim($("#datas tr:eq(" + i + ")").find("th").find("span").attr("rel")) != subid) {
                var trArea = $("#datas tr:eq(" + i + ")").find("#sub_area").html();
                var trMoney = $("#datas tr:eq(" + i + ")").find("#sub_money").html();
                subAllArea += Math.abs(trArea);
                subAllMoney += Math.abs(trMoney);
            }

        }

        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+)$/;
        var data = "action=cpraddsub&flag=add&cstno=" + cstno + "&subid=" + subid;

        //判断输入是否合法
        //子项名称
        if (addsubName == "" || addsubName == null || addsubName == NaN) {
            $("#subNameNull").show();
            return false;
        } else {
            $("#subNameNull").hide();
            data += "&subname=" + addsubName;
        }
        //建筑面积
        if (addsubArea == "" || addsubArea == null || addsubArea == NaN) {
            $("#subAreaNull").show();
            $("#subAreaNoInt").hide();
            $("#subAreaErr").hide();
            return false;
        } else {
            $("#subAreaNull").hide();
            if (!reg.test(addsubArea)) {
                $("#subAreaNoInt").show();
                $("#subAreaErr").hide();
                return false;
            } else {
                $("#subAreaNoInt").hide();
            }
            if ((Math.abs(Math.abs(addsubArea) + Math.abs(subAllArea))) > Math.abs(sumOfArea)) {
                $("#subAreaErr").show();
                return false;
            } else {
                $("#subAreaErr").hide();
            }
            data += "&subarea=" + addsubArea;
        }
        //金额
        if (addsubJine == "" || addsubJine == null || addsubJine == NaN) {
            $("#subJineNull").show();
            $("#subJineErr").hide();
            $("#subJineNoInt").hide();
            return false;
        } else {
            $("#subJineNull").hide();
            if (!reg.test(addsubJine)) {
                $("#subJineNoInt").show();
                $("#subJineErr").hide();
                return false;
            } else {
                $("#subJineNoInt").hide();
            }
            if (Math.abs(Math.abs(addsubJine) + Math.abs(subAllMoney)) > Math.abs(sumOfMoney)) {
                $("#subJineErr").show();
                return false;
            } else {
                $("#subJineErr").hide();
            }
            data += "&money=" + addsubJine;
        }
        data += "&remark=" + areaTxtAddSubRemark;

        $.ajax({
            type: "POST",
            url: "../HttpHandler/CommHandler.ashx",
            data: data,
            success: function (result) {
                if (result == "yes") {
                    //alert("添加子项成功！"); 
                    loadSubCopration();
                    $("#ctl00_ContentPlaceHolder1_txtcpr_Account").attr("readonly", "true");
                    $("#ctl00_ContentPlaceHolder1_txt_buildArea").attr("readonly", "true");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误！");
            }
        });
    });
});
//选择用户callback
function chooseCustomerCallBack(recordObj) {

    $("#ctl00_ContentPlaceHolder1_txtCst_No").val(recordObj.CustomerNo);
    $("#ctl00_ContentPlaceHolder1_txtCst_Name").val(recordObj.CustomerName);
    $("#ctl00_ContentPlaceHolder1_txtCpy_Address").val(recordObj.Address);
    $("#ctl00_ContentPlaceHolder1_txtCode").val(recordObj.ZipCode == "0" ? "" : recordObj.ZipCode);
    $("#ctl00_ContentPlaceHolder1_txtLinkman").val(recordObj.LinkMan);
    $("#ctl00_ContentPlaceHolder1_txtCpy_Phone").val(recordObj.Phone);
    $("#ctl00_ContentPlaceHolder1_txtCpy_Fax").val(recordObj.Fax);
    $("#ctl00_ContentPlaceHolder1_hid_cstid").val(recordObj.SysNo);
    $("#ctl00_ContentPlaceHolder1_txtCst_Brief").val(recordObj.CustomerShortName);
}
//===========合同编号所属院CallBack
//合同编号
function CprNumUnitCallBack(result) {
    if ($("#ctl00_ContentPlaceHolder1_txtcpr_No").val() == "") {
        var data = result == null ? "" : result.ds;
        var cpr_typeSelect_htm = '<option value="-1">--------请选择--------</option>';
        $.each(data, function (i, n) {
            cpr_typeSelect_htm += '<option value="' + n.ID + '">' + n.CprType + '</option>';
        });
        $("#cpr_typeSelect").html(cpr_typeSelect_htm);
        $('#cpr_numSelect').empty();
    }
    //注册合同类型选项改变的事件
    $("#cpr_typeSelect").unbind('change').change(function () {
        $("#noselectMsg").hide();
        var CprTypeID = $(this).val();
        if (CprTypeID > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", CprTypeID, "false", "0", "cprNumUnit", CprNumCallBack);
        } else {
            $('#cpr_numSelect').empty();
        }

    });
    //注册确定按钮点击事件
    $("#btn_cprNum_close").unbind('click').click(function () {
        var CprNum = $("#cpr_numSelect").find("option:selected").text();
        if (CprNum != NaN && CprNum != "" && CprNum != undefined) {
            $("#noselectMsg").hide();
            $("#ctl00_ContentPlaceHolder1_txtcpr_No").val(CprNum);
            $("#ctl00_ContentPlaceHolder1_hid_cprno").val(CprNum);

        } else {
            $("#noselectMsg").show();
        }
    });
}
function CprNumCallBack(result) {
    var obj = result == null ? "" : result.ds;
    var cpr_TypeSelectChangeHtml;
    var prevTxt;
    $.each(obj, function (i, n) {
        var startNums = n.CprNumStart.split('-');
        var endNums = n.CprNumEnd.split('-');
        prevTxt = startNums[0] + "-";
        var startNum = Math.abs(GetCprNumFix(startNums[1])); //开始编号
        var endNum = Math.abs(GetCprNumFix(endNums[1])); //结束编号
        for (var i = startNum; i <= endNum; i++) {
            var cprNumOk = prevTxt + GetCprNumFix(i);
            cpr_TypeSelectChangeHtml += '<option cprNum="' + cprNumOk + '" value="' + GetCprNumFix(i) + '">' + cprNumOk + '</option>';
        }
    });
    $('#cpr_numSelect').empty();
    $('#cpr_numSelect').html(cpr_TypeSelectChangeHtml);
    //去除已使用的编号
    BindCprNumber(prevTxt);
}
//处理合同编号数字
function GetCprNumFix(cprNo) {
    if (cprNo < 10) {
        return "00" + cprNo;
    } else if (10 <= cprNo && cprNo < 100) {
        return "0" + cprNo;
    } else { return cprNo; }
}
//去除已使用的合同编号
function BindCprNumber(fixstr) {
    var data = "action=removeUsedCprNum&fixStr=" + fixstr;
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var obj = result.ds;
                $.each(obj, function (i, n) {
                    $("#cpr_numSelect option[cprNum=" + n.cpr_No + "]").remove();
                });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!未能去除已使用的合同编号!");
        }
    });
}

//=====合同编号
//合同类型绑定表格CallBack
function CproTypeCallBack(result) {
    $("#customerCompactTable tr:gt(0)").remove();
    if (result != null) {
        var data = result.ds;
        $.each(data, function (i, n) {
            var oper = "<span rel='" + n.ID + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\">选择</span>";
            var trHtml = "<tr><td>" + (i + 1) + "</td><td>" + n.dic_Name + "</td><td>" + oper + "</td></tr>";
            $("#customerCompactTable").append(trHtml);
            $("#customerCompactTable span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txt_cprType").val(n.dic_Name);

            });
        });
        ControlTableCss("customerCompactTable");
    }
}

//////---------项目经理工程负责人
//工程负责人绑定部门CallBack
function CprTypeUnitCallBack(result) {
    if (result != null) {

        var data = result.ds;
        var gcFzr_UnitOptionHtml = '<option value="-1">---------请选择---------</option>';
        $.each(data, function (i, n) {
            gcFzr_UnitOptionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
        });
        $("#select_gcFzr_Unit").html(gcFzr_UnitOptionHtml);
        //注册部门选项改变事件
        showDivDialogClass.SetParameters({ "pageSize": "10" });
        $("#select_gcFzr_Unit").unbind('change').change(function () {
            var unit_ID = $("#select_gcFzr_Unit").val();
            if (Math.abs(unit_ID) > 0) {
                showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", "1", "gcfzrCprMem", BindGcfzrDataCallBack);
                BindAllDataCountGcfzr(unit_ID);
            }
        });
    }
}
//绑定工程负责人表格数据CallBack
function BindGcfzrDataCallBack(result) {
    if (result != null) {

        var obj = result.ds;
        var gcFzrMemTableHtml;
        $("#gcFzr_MemTable tr:gt(0)").remove();
        $.each(obj, function (i, n) {
            var oper = "<span rel='" + n.mem_Login + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\" >选择</span>";
            gcFzrMemTableHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.mem_Name + "</td><td>" + oper + "</td></tr>";
            $("#gcFzr_MemTable").append(gcFzrMemTableHtml);
            $("#gcFzr_MemTable span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txt_proFuze").val(n.mem_Name);
                $("#ctl00_ContentPlaceHolder1_txt_fzphone").val(n.mem_Mobile);
                //增加项目经理ID qpl 20131225
                $("#ctl00_ContentPlaceHolder1_HiddenPMUserID").val(n.mem_ID);
            });
        });
        for (var trBland = 0; trBland < ParametersDict.pageSize - obj.length; trBland++) {

        }
        ControlTableCss("gcFzr_MemTable");
    }
}
//绑定工程负责人数据总数
function BindAllDataCountGcfzr(unit_ID) {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "gcfzr_prevPage",
        "firstPage": "gcfzr_firstPage",
        "nextPage": "gcfzr_nextPage",
        "lastPage": "gcfzr_lastPage",
        "gotoPage": "gcfzr_gotoPageIndex",
        "allDataCount": "gcfzr_allDataCount",
        "nowIndex": "gcfzr_nowPageIndex",
        "allPageCount": "gcfzr_allPageCount",
        "gotoIndex": "gcfzr_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", unit_ID, "true", "gcfzrCprMem", GetGcfzrAllDataCount);
    //注册事件,先注销,再注册
    $("#gcFzr_ForPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#gcfzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", pageIndex, "gcfzrCprMem", BindGcfzrDataCallBack);
        }
    });
}
//项目经理数据总数CallBack
function GetGcfzrAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#gcFzr_MemTable tr:gt(0)").remove();
        $("#gcfzr_allDataCount").text(0);
        $("#gcfzr_nowPageIndex").text(0);
        $("#gcfzr_allPageCount").text(0);
        NoDataMessageOnTable("gcFzr_MemTable", 3);
    }
}
////---------项目经理工程负责人
///-----------------承接部门
//承接部门表格数据绑定-CallBack函数
function ProCjbmCallBack(result) {
    if (result != null) {

        var data = result.ds;
        $("#pro_cjbmTable tr:gt(0)").remove();
        $.each(data, function (i, n) {
            var oper = "<span rel='" + n.unit_ID + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\">选择</span>";
            var trHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.unit_Name + "</td><td>" + oper + "</td></tr>";
            $("#pro_cjbmTable").append(trHtml);
            $("#pro_cjbmTable span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txt_cjbm").val($.trim(n.unit_Name));
                $("#ctl00_ContentPlaceHolder1_hid_cjbm").val($.trim(n.unit_Name));

            });
        });
        for (var trBland = 0; trBland < ParametersDict.pageSize - data.length; trBland++) {

        }
        ControlTableCss("pro_cjbmTable");
    }
}
//获得承接部门数据总数
function BindAllDataCount() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "cjbm_prevPage",
        "firstPage": "cjbm_firstPage",
        "nextPage": "cjbm_nextPage",
        "lastPage": "cjbm_lastPage",
        "gotoPage": "cjbm_gotoPageIndex",
        "allDataCount": "cjbm_allDataCount",
        "nowIndex": "cjbm_nowPageIndex",
        "allPageCount": "cjbm_allPageCount",
        "gotoIndex": "cjbm_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", "", "false", "proCjbm", GetCjbmAllDataCount);
    //注册事件,先注销,再注册
    $("#cjbmByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#cjbm_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", pageIndex, "proCjbm", ProCjbmCallBack);
        }
    });
}
//承接部门数据总数CallBack函数
function GetCjbmAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        NoDataMessageOnTable("pro_cjbmTable", 3);
    }
}
///---------------承接部门
////////////----------甲方负责人

//甲方负责人部门CallBack
function JffzrUnitCallBack(result) {
    if (result == null || result == undefined) {
        $("#select_jffzrMem").html('<option value="-1">-------------------------请选择-------------------------</option>');
        $("#noCustMsg").show();
        return false;
    }
    $("#noCustMsg").hide();
    var data = result.ds;
    var optionHtml = '<option value="-1">-------------------------请选择-------------------------</option>';
    $.each(data, function (i, n) {
        optionHtml += '<option value="' + n.Cst_Id + '">' + n.Cst_Name + '</option>';
    });
    $("#select_jffzrMem").html(optionHtml);
    //注册部门选项改变事件
    showDivDialogClass.SetParameters({ "pageSize": "10" });
    $("#select_jffzrMem").unbind('change').change(function () {
        var Cst_Id = $("#select_jffzrMem").val();
        if (Math.abs(Cst_Id) > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", Cst_Id, "true", "1", "jffzrMem", BindJffzrDataCallBack);
            BindAllDataCountJffzr(Cst_Id);
        }
    });
}
//绑定表格数据CallBack
function BindJffzrDataCallBack(result) {
    if (result != null) {
        var obj = result.ds;
        var jffzrTableHtml;
        $("#jffzr_dataTable tr:gt(0)").remove();
        $.each(obj, function (i, n) {
            var oper = "<span rel='" + n.Cst_Id + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\">选择</span>";
            jffzrTableHtml = "<tr style='text-align:center'><td>" + n.Cst_Id + "</td><td>"
        + n.Name + "</td><td>" + n.Duties + "</td><td>" + n.Department + "</td><td>" + n.Phone + "</td><td>" + oper + "</td></tr>";
            $("#jffzr_dataTable").append(jffzrTableHtml);
            $("#jffzr_dataTable span:last").click(function () {
                $("#ctl00_ContentPlaceHolder1_txtFParty").val($.trim(n.Name));
                $("#ctl00_ContentPlaceHolder1_txt_jiafphone").val($.trim(n.Phone));
            });
        });
        for (var trBland = 0; trBland < ParametersDict.pageSize - obj.length; trBland++) {

        }
        ControlTableCss("jffzr_dataTable");
    }
}
//甲方负责人数据总数
function BindAllDataCountJffzr(Cst_Id) {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "jffzr_prevPage",
        "firstPage": "jffzr_firstPage",
        "nextPage": "jffzr_nextPage",
        "lastPage": "jffzr_lastPage",
        "gotoPage": "jffzr_gotoPageIndex",
        "allDataCount": "jffzr_allDataCount",
        "nowIndex": "jffzr_nowPageIndex",
        "allPageCount": "jffzr_allPageCount",
        "gotoIndex": "jffzr_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", Cst_Id, "true", "jffzrMem", GetJffzrAllDataCount);
    //注册事件,先注销,再注册
    $("#jffzrByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#jffzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", Cst_Id, "true", pageIndex, "jffzrMem", BindJffzrDataCallBack);
        }
    });
}
//获取甲方负责人总数CallBack
function GetJffzrAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#jffzr_dataTable tr:gt(0)").remove();
        $("#jffzr_allDataCount").text(0);
        $("#jffzr_nowPageIndex").text(0);
        $("#jffzr_allPageCount").text(0);
        NoDataMessageOnTable("jffzr_dataTable", 6);
    }
}
//////*-----甲方负责人3
////////////------------添加计划收费
//添加收款计划 by fbw
function AddChargePlan() {
    $("#lbl_copMoney").text(cprAllcount);
    //每次初始化信息
    $("#ctl00_ContentPlaceHolder1_chargeTypeSelect").val(-1);
    $("#txt_planChargeNum").val("");
    $("#txt_datePicker").val("");
    $("#txt_chargeRemark").val("");
    $("#planChargeNumPercent").val("");
    var cprAllcount = $("#ctl00_ContentPlaceHolder1_txtcpr_Account").val(); //总金额
    $("#lbl_copMoney").text(cprAllcount);

    $("#txt_planChargeNum").change(function () {
        var chargeNum = $(this).val();
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        if (!reg.test(chargeNum)) {
            $("#jine_notint").show();
            return false;
        } else {
            $("#jine_notint").hide();
        }
        var lblCprMoney = $("#lbl_copMoney").text();
        $("#planChargeNumPercent").val((parseFloat(chargeNum) / parseFloat(lblCprMoney) * 100).toFixed(2));
    });

    //比例
    $("#planChargeNumPercent").change(function () {
        var chargeNum = $(this).val();
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        if (!reg.test(chargeNum)) {
            $("#span_PercentNotInt").show();
            return false;
        } else {
            $("#span_PercentNotInt").hide();
        }
        var lblCprMoney = $("#lbl_copMoney").text();
        $("#txt_planChargeNum").val(parseFloat(chargeNum) * parseFloat(lblCprMoney) / 100);
    });
}

//////////-------------- 计划收费
//加载收款信息
function loadCprCharge() {
    var data = "action=getplancharge&cstno=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var data = result.ds;
                if ($("#sf_datas tr").length > 1) {
                    $("#sf_datas tr:gt(0)").remove();
                }
                $.each(data, function (i, n) {
                    var row = $("#sf_row").clone();
                    var oper = "<span class='update' rel='" + n.ID + "' style='cursor: pointer;color:blue;' data-toggle=\"modal\" href=\"#TJSF\">编辑</span>&nbsp;<span class='del' rel='" + n.ID + "' style='cursor: pointer;color:blue;'>删除</span>";
                    row.find("#sf_id").text(i + 1);
                    row.find("#sf_bfb").text(n.persent + "%");
                    row.find("#sf_edu").text(n.payCount);
                    row.find("#sf_time").html(n.paytime);
                    var markShow = n.mark;
                    if (n.mark.length > 20) {
                        markShow = markShow.substr(0, 20) + "...";
                    }
                    row.find("#sf_mark").attr("title", n.mark);
                    row.find("#sf_mark").html(markShow); //新增,备注
                    row.find("#sf_oper").html(oper);
                    row.find("#sf_oper span[class=del]").click(function () {
                        if (confirm("确定要删除此条计划收费吗？")) {
                            //删除事件
                            delChargeItem($(this));
                        }
                    });
                    //添加样式
                    row.addClass("cls_TableRow");
                    row.appendTo("#sf_datas");
                });

                data = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误3！");
        }
    });
}
//删除计划收费
function delChargeItem(link) {
    var data = "action=delchargeitem&chrgid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        dataType: "text",
        data: data,
        success: function (result) {
            if (result == "yes") {
                //删除本行数据
                link.parents("tr:first").remove();
                //  alert("计划收费删除成功！");
                if (($("#sf_datas tr").length <= 1) && ($("#datas tr").length <=
 1)) {
                    $("#ctl00_ContentPlaceHolder1_txtcpr_Account").removeAttr("readonly");
                }

            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误4！");
        }
    });
}
/////////--------------- 计划收费
//------------------------------子项
//加载分项信息
function loadSubCopration() {
    var data = "action=getsubitemdata&cstno=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            var data = result.ds;
            if ($("#datas tr").length > 1) {
                $("#datas tr:gt(0)").remove();
            }
            var i_rowindex = 0;
            //总建筑面积
            var totalArea = 0;
            var totalMoney = 0;
            var totalLiving_Area = 0;
            $.each(data, function (i, n) {
                var row = $("#sub_row").clone();
                var oper = "<span id='update' rel='" + n.ID + "' style='cursor: pointer;color:blue;' data-toggle=\"modal\" href=\"#ZXObject\">编辑</span>&nbsp;<span id='del' rel='" + n.ID + "' style='cursor: pointer;color:blue;'>删除</span>";
                //增加行
                i_rowindex++
                row.find("#sub_id").text(i_rowindex);
                row.find("#sub_name").text(n.Item_Name);
                row.find("#sub_area").text(n.Item_Area);
                row.find("#sub_oper").html(oper);
                row.find("#sub_money").html(n.Money);
                var cprRemark = n.Remark;
                if (cprRemark.length > 20) {
                    cprRemark = cprRemark.substr(0, 20) + "...";
                }
                row.find("#sub_remark").attr("title", n.Remark);
                row.find("#sub_remark").html(cprRemark);
                row.find("#sub_oper span[id=del]").click(function () {
                    if (confirm("确定要删除此条分项信息吗？")) {
                        //删除事件
                        delSubItem($(this));
                    }
                });
                //添加样式
                row.addClass("cls_TableRow");
                row.appendTo("#datas");
            });

            data = "";
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误5！");
        }
    });
}
//删除子项
function delSubItem(link) {
    var data = "action=delsubitem&subid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        dataType: "text",
        data: data,
        success: function (result) {
            if (result == "yes") {
                //删除本行数据
                link.parents("tr:first").remove();
                //  alert("删除子项成功！");
                if (($("#datas tr").length <= 1)) {
                    $("#ctl00_ContentPlaceHolder1_txtcpr_Account").removeAttr("readonly");
                    $("#ctl00_ContentPlaceHolder1_txt_buildArea").removeAttr("readonly");

                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误6！");
        }
    });
}
///------子项
//删除附件
//加载数据
function GetAttachData() {
    loadCoperationAttach();
}
//加载附件信息
function loadCoperationAttach() {
    var data = "action=getcprfiles&cprid=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            if (result != null) {

                var filedata = result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").remove();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    var oper = "<a href='#' rel='" + n.ID + "'>删除</a>";
                    var oper2 = "<a href='../Attach_User/filedata/cprfile/" + n.FileUrl + "' target='_blank'>查看</a>";
                    var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").html(img + n.FileName);
                    row.find("#att_filename").attr("align", "left");
                    row.find("#att_filesize").text(n.FileSizeString + 'KB');
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper").html(oper);
                    row.find("#att_oper2").html(oper2);
                    row.find("#att_oper a").click(function () {
                        delCprAttach($(this));
                    });
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas_att");
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误7！");
        }
    });
}
//删除附件
function delCprAttach(link) {
    var data = "action=delcprattach&attid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "text",
        success: function (result) {
            if (result == "yes") {
                //加载附件
                link.parents("tr:first").remove();
                // alert("附件删除成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误8！");
        }
    });
}
//无数据提示                     

function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}

//表格样式-各行变色-鼠标覆盖事件
function ControlTableCss(tableId) {
    $("#" + tableId + " tr:gt(0):odd").attr("class", "trOddColor");
    $("#" + tableId + " tr:gt(0)").hover(function () {
        $(this).addClass("mouseOverColor");
    }, function () {
        $(this).removeClass("mouseOverColor");
    });
}
