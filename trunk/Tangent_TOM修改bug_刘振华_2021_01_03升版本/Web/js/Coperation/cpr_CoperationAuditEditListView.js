﻿
//合同审核列表
$(function () {
    CommonControl.SetFormWidth();
    //提示
    $(".cls_column").tipsy({ opacity: 1 });
    //设置Table样式
    $("#gv_Coperation tr:even").css({ background: "White" });
    //设置文本框样式
    CommonControl.SetTextBoxStyle();
    //改变背景颜色
    ChangedBackgroundColor();
    //当前登录用户SysNo
    var userSysNo = $("#HiddenUserSysNo").val();
    //发起审核按钮
    $("span[id=InitiatedAudit]").live("click",function () {
        //获取合同SysNo
        var coperationSysNo = $(this).attr("coperationSysNo");
        window.location.href = "/Coperation/cpr_CoperationEditChoose.aspx?coperationSysNo=" + coperationSysNo;
    });
    //绑定审批进度 
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("percent"));
        $(item).progressbar({
            value: percent
        });
    });

    //预览按钮
    $("span[id=PreviewAudit]").live("click",function () {
        //获取合同系统自增号
        var coperationSysNo = $(this).attr("coperationSysNo");
        window.location.href = "/Coperation/cpr_CoperationPreview.aspx?coperationSysNo=" + coperationSysNo;
    });

    //审核按钮
    $("span[id=GoAudit]").live("click",function () {
        //获取合同系统自增号
        var coperationSysNo = $(this).attr("coperationSysNo");
        //获取合同审核记录自增号
        var coperationAdutiSysNo = $(this).attr("coperationAuditSysNo");
        window.location.href = "/Coperation/cpr_CoperationAudit.aspx?CoperationAuditSysNo=" + coperationAdutiSysNo + "&CoperationSysNo=" + coperationSysNo;
    });

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Corperation";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#txt_keyname"));
    autoComplete.GetAutoAJAX();

});

