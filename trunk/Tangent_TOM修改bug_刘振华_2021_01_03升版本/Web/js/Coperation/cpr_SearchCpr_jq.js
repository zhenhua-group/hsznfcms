﻿
$(function () {
    //隐藏查询条件
    $("#tbl_id2").children("tbody").children("tr").hide();
    $("#tbl_id3_2").children("tbody").children("tr").hide();
    $("#tbl_id1_2").children("tbody").children("tr").hide();
    $("#tbl_id2_2").children("tbody").children("tr").hide();  
    $("#tbl_id4_2").children("tbody").children("tr").hide();
    $("#tbl_id7_2").children("tbody").children("tr").hide();
    $("#tbl_id6_2").children("tbody").children("tr").hide();
    $("#tbl_id8_2").children("tbody").children("tr").hide();
    //暂无用 begin
    //$("#jqGrid").jqGrid({
    //    url: '/HttpHandler/Coperation/ProjectChargeHandler.ashx',
    //    datatype: 'json',
    //    height: "auto",
    //    rowNum: 25,
    //    rowList: [25, 30],
    //    colNames: ['序号', '', '合同编号', '合同分类', '合同名称', '建筑类别', '建筑规模(平米)', '合同类型', '建设单位', '结构形式', '建筑分类', '层数', '工程负责人', '工程负责人电话', '甲方负责人', '甲方负责人电话', '承接部门', '合同额(万元)', '工程地点', '行业性质', '实际合同额(万元)', '投资额(万元)', '实际投资额(万元)', '合同阶段', '工程来源', '合同签订日期', '合同完成日期', '制表人', '多栋楼', '合同备注', '录入人', '已收费(万元)', '客户', '录入时间', '查看'],
    //    colModel: [
    //                       { name: 'nid', index: 'nid', width: 30, align: 'center' },
    //                       { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
    //                       { name: 'cpr_No', index: 'cpr_No', hidden: true, width: 100 },
    //                       { name: 'cpr_Type', index: 'cpr_Type', hidden: true, width: 100 },
    //                       { name: 'cpr_Name', index: 'cpr_Name', width: 200, formatter: colNameShowFormatter },
    //                       { name: 'BuildType', index: 'BuildType', hidden: true, width: 100 },
    //                       { name: 'BuildArea', index: 'BuildArea', hidden: true, width: 100 },
    //                       { name: 'cpr_Type2', index: 'cpr_Type2', hidden: true, width: 100 },
    //                       { name: 'BuildUnit', index: 'BuildUnit', hidden: true, width: 100 },
    //                       { name: 'StructType', index: 'StructType', hidden: true, width: 100 },
    //                       { name: 'BuildStructType', index: 'BuildStructType', hidden: true, width: 100 },
    //                       {
    //                           name: 'Floor', index: 'Floor', width: 80, align: 'center', hidden: true, formatter: function colShowName(celvalue, options, rowData) {
    //                               if (celvalue != null && celvalue != "null" && celvalue != "") {
    //                                   var arr = $.trim(celvalue).split("|");
    //                                   return "地上:" + $.trim(arr[0]) + " 地下:" + $.trim(arr[1]);
    //                               }
    //                               return "";
    //                           }
    //                       },
    //                       { name: 'ChgPeople', index: 'ChgPeople', width: 100, align: 'center' },
    //                       { name: 'ChgPhone', index: 'ChgPhone', hidden: true, width: 100, align: 'center' },
    //                       { name: 'ChgJia', index: 'ChgJia', hidden: true, width: 100, align: 'center' },
    //                        { name: 'ChgJiaPhone', index: 'ChgJiaPhone', hidden: true, width: 100, align: 'center' },
    //                       { name: 'cpr_Unit', index: 'cpr_Unit', width: 100, align: 'center' },
    //                       { name: 'cpr_Acount', index: 'cpr_Acount', width: 100, align: 'center' },
    //                       { name: 'BuildPosition', index: 'BuildPosition', hidden: true, width: 100, align: 'left' },
    //                        { name: 'Industry', index: 'Industry', hidden: true, width: 100, align: 'center' },
    //                        { name: 'cpr_ShijiAcount', index: 'cpr_ShijiAcount', hidden: true, width: 100, align: 'center' },
    //                        { name: 'cpr_Touzi', index: 'cpr_Touzi', hidden: true, width: 100, align: 'center' },
    //                        { name: 'cpr_ShijiTouzi', index: 'cpr_ShijiTouzi', hidden: true, width: 100, align: 'center' },
    //                       { name: 'cpr_Process', index: 'cpr_Process', width: 150, formatter: colProcessShowFormatter },
    //                       { name: 'BuildSrc', index: 'BuildSrc', hidden: true, width: 100, align: 'center' },
    //                       { name: 'cpr_SignDate', index: 'cpr_SignDate', width: 100, align: 'center', formatter: 'date', formatoptions: { srcformat: 'Y-m-d', newformat: 'Y-m-d' } },
    //                       { name: 'cpr_DoneDate', index: 'cpr_DoneDate', width: 100, align: 'center', formatter: 'date', formatoptions: { srcformat: 'Y-m-d', newformat: 'Y-m-d' } },
    //                        { name: 'TableMaker', index: 'TableMaker', width: 80, align: 'center', hidden: true },
    //                         { name: 'MultiBuild', index: 'MultiBuild', width: 100, align: 'center', hidden: true },
    //                         { name: 'cpr_Mark', index: 'cpr_Mark', width: 80, align: 'center', hidden: true },
    //                         { name: 'InsertUser', index: 'InsertUserID', width: 100, align: 'center', hidden: true },
    //                       { name: 'ssze', index: 'ssze', width: 80, align: 'center' },
    //                       { name: 'cstName', index: 'cstName', width: 80, hidden: true, align: 'center' },
    //                        { name: 'lrsj', index: 'InsertDate', width: 70, align: 'center' },
    //                       { name: 'cpr_Id', index: 'cpr_Id', width: 50, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
    //    ],
    //    jsonReader: {
    //        repeatitems: false,
    //        root: function (obj) { return obj.rows; },
    //        page: function (obj) { return obj.pageindex; },
    //        total: function (obj) { return obj.pagecount; },
    //        records: function (obj) { return obj.total; }
    //    },
    //    prmNames: {
    //        page: 'PageIndex',
    //        rows: 'PageSize',
    //        sort: 'OrderBy',
    //        order: 'Sort'
    //    },
    //    postData: { "action": "Search", "strwhere": escape($("#ctl00_ContentPlaceHolder1_hid_where").val()) },
    //    loadonce: false,
    //    sortname: 'cpr_Id',
    //    sortorder: 'desc',
    //    pager: "#gridpager",
    //    viewrecords: true,
    //    shrinkToFit: false,
    //    autowidth: true,
    //    editurl: "/HttpHandler/Coperation/ProjectChargeHandler.ashx",
    //    multiselect: true,
    //    multiselectWidth: 25,
    //    footerrow: true,
    //    gridComplete: completeMethod,
    //    loadComplete: loadCompMethod
    //});

    ////显示查询
    //$("#jqGrid").jqGrid("navGrid", "#gridpager", {
    //    add: false,
    //    edit: false,
    //    del: false,
    //    search: false,
    //    refresh: false
    //},
    //        {//编辑
    //        },
    //        {//添加
    //        },
    //        {//删除
    //            top: 200,
    //            left: 400,
    //            reloadAfterSubmit: true,
    //            closeAfterDelete: true,
    //            closeOnEscape: true,
    //            afterSubmit: function (response, postdata) {
    //                alert(response.responseText);
    //                if (response.responseText == "") {
    //                    $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
    //                    return [false, response.responseText]
    //                }
    //                else {
    //                    $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
    //                    return [true, response.responseText]
    //                }
    //            },
    //            delData: {
    //                EmpId: function () {
    //                    var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
    //                    var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
    //                    return value;
    //                }

    //            }
    //        },
    //        {//搜索
    //            top: 200,
    //            left: 400
    //        }
    //        );
    ////名称连接
    //function colNameShowFormatter(celvalue, options, rowData) {
    //    var pageurl = "cpr_ShowCoprationBymaster.aspx?flag=cprlist&cprid=" + rowData["cpr_Id"];
    //    return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

    //}
    //function colProcessShowFormatter(celvalue, options, rowData) {
    //    //return celvalue;
    //    var returnValues = "";
    //    if (celvalue.length > 2) {
    //        var arrayStatus = celvalue.split(',');
    //        var status = "";
    //        for (var i = 0; i < arrayStatus.length; i++) {
    //            var statusName = "";
    //            if (arrayStatus[i] == "27") {
    //                statusName = "方案,";
    //                status += statusName;
    //            }
    //            else if (arrayStatus[i] == "28") {
    //                statusName = "初设,";
    //                status += statusName;
    //            }
    //            else if (arrayStatus[i] == "29") {
    //                statusName = "施工图,";
    //                status += statusName;
    //            }
    //            else {
    //                statusName = "其他,";
    //                status += statusName;
    //            }

    //        }
    //        returnValues = status;
    //        return returnValues;
    //    }
    //    else {
    //        if (celvalue == "27") {
    //            returnValues = "方案";
    //        }
    //        else if (celvalue == "28") {
    //            returnValues = "初设";
    //        }
    //        else if (celvalue == "29") {
    //            returnValues = "施工图";
    //        }
    //        else {
    //            returnValues = "其他";
    //        }
    //        return returnValues;
    //    }
    //}

    ////查看
    //function colShowFormatter(celvalue, options, rowData) {
    //    var pageurl = "cpr_ShowCoprationBymaster.aspx?flag=cprlist&cprid=" + celvalue;
    //    return '<a href="' + pageurl + '" alt="查看合同">查看</a>';

    //}

    ////统计 
    //function completeMethod() {
    //    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    //    for (var i = 0, j = rowIds.length; i < j; i++) {
    //        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
    //    }
    //}
    ////无数据
    //function loadCompMethod() {
    //    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    //    $("#jqGrid").setGridParam("").hideCol("cb").trigger('reloadGrid');
    //    if (rowcount <= 0) {
    //        if ($("#nodata").text() == '') {
    //            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
    //        } else { $("#nodata").show(); }
    //    }
    //    else {
    //        $("#nodata").hide();
    //    }
    //}

    ////点击全部基本合同
    //$(":checkbox:first", $("#columnsid")).click(function () {

    //    var checkedstate = $(this).parent().attr("class");
    //    if (checkedstate != "checked")//代表选中
    //    {
    //        $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
    //            var span = $(value).parent();
    //            if (!span.hasClass("checked")) {
    //                span.addClass("checked");
    //            }
    //            $(value).attr("checked", true);
    //            $("#jqGrid").showCol($(value).val());
    //        });
    //    }
    //    else {
    //        $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
    //            var span = $(value).parent();
    //            span.removeClass("checked");
    //            $(value).attr("checked", false);
    //            $("#jqGrid").hideCol($(value).val());
    //        });
    //    }
    //    // $("#jqGrid").trigger('reloadGrid');
    //});

    ////点击客户字段
    //$(":checkbox:not(:first)", $("#columnsid")).click(function () {

    //    var checkedstate = $(this).parent().attr("class");
    //    var columnsname = $(this).val();

    //    if (($(":checkbox:not(:first):checked", $("#columnsid")).length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
    //        $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
    //        $(":checkbox:first", $("#columnsid")).attr("checked", true);
    //    }
    //    else {
    //        $(":checkbox:first", $("#columnsid")).parent().removeClass("checked");
    //        $(":checkbox:first", $("#columnsid")).attr("checked", false);
    //    }

    //    if (checkedstate != "checked")//代表选中
    //    {
    //        $("#jqGrid").showCol(columnsname);
    //    }
    //    else {
    //        $(this).attr("checked", false);
    //        $("#jqGrid").hideCol(columnsname);
    //    }

    //});

    //暂无用 end


    //基本合同查询
    $("#btn_search").click(function () {
        //声明查询方式
        var andor = "and";
        $("label[rel]", $("#div_sch")).each(function(){
            if ($(this).hasClass("active"))
            {
                andor=$.trim($(this).attr("rel"));
            }
        }) ;   

      
        ClearjqGridParam("jqGrid");

        //设置值为1高级查询
        $("#hid_cxtype").val("1");

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //合同编号
        var txtcpr_No = $("#txt_proName").val();
        //合同分类
        var drp_type = $("#ctl00_ContentPlaceHolder1_txt_ddcpr_Type").find("option:selected").text();
        //合同名称
        var txt_cprname = $("#txt_cprName").val();
        //建筑规模
        var txt_Area = $("#txt_buildArea").val();
        var txt_Area2 = $("#txt_buildArea2").val();
        //建设单位
        var txt_cprBuildUnit = $("#txt_BuildUnit").val();
        //结构形式
        var asTreeviewStruct = $("#ctl00_ContentPlaceHolder1_asTreeviewStruct_divDropdownTreeText").text();
        //建筑分类
        var asTreeviewStructType = $("#ctl00_ContentPlaceHolder1_asTreeviewStructType_divDropdownTreeText").text();
        //建筑类别
        var drp_buildtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype").find("option:selected").text();
        //工程负责人
        var txt_proFuze = $("#txt_proFuze").val();
        //甲方负责人
        var txtFParty = $("#txtFParty").val();
        //承接部门
        //  var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var drp_unit = getCheckedUnitNodes();
        //合同额
        var txtcpr_Account = $("#txtproAcount").val();
        var txtcpr_Account2 = $("#txtproAcount2").val();
        //工程地点
        var txt_Address = $("#txtaddress").val();
        //行业性质
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType").find("option:selected").text();
        //合同阶段
        var chk_cprjd = "";
        var see = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd").length;

        for (var i = 0; i < parseInt(see) ; i++) {
            var isChecked = $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().attr("class");
            if (isChecked == undefined || isChecked == "") {

            } else {
                chk_cprjd += $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().parent().next().text() + ",";

            }
        }
        //工程来源
        var ddSourceWay = $("#ctl00_ContentPlaceHolder1_ddsource").find("option:selected").text();
        //合同签订日期
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate").val();
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate2").val();
        //合同完成日期
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val();
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate2").val();
        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        //合同收费
        var projcharge1 = $("#projcharge1").val();
        var projcharge2 = $("#projcharge2").val();
        //按照客户
        var txt_cst = $("#txt_cstname").val();
        //数字验证正则    
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        if (txt_Area != "") {
            if (!reg.test(txt_Area)) {
                msg += "建筑规模开始值格式错误，请输入数字！</br>";
            }
        }
        if (txt_Area2 != "") {
            if (!reg.test(txt_Area2)) {
                msg += "建筑规模结束值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account != "") {
            if (!reg.test(txtcpr_Account)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account2 != "") {
            if (!reg.test(txtcpr_Account2)) {
                msg += "合同额结束值格式错误，请输入数字！</br>";
            }
        }
        if (projcharge1 != "") {
            if (!reg.test(projcharge1)) {
                msg += "合同收费开始值格式错误，请输入数字！</br>";
            }
        }
        if (projcharge2 != "") {
            if (!reg.test(projcharge2)) {
                msg += "合同收费结束值格式错误，请输入数字！</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        var params = {
            "strwhere": strwhere,
            "andor": andor,
            "txtcpr_No": txtcpr_No,     //合同编号
            "drp_type": drp_type,     //合同分类
            "txt_cprname": txt_cprname,     //合同名称
            "txt_Area": txt_Area,     //建筑规模1
            "txt_Area2": txt_Area2,     //建筑规模2
            "txt_cprBuildUnit": txt_cprBuildUnit,     //建设单位
            "asTreeviewStruct": asTreeviewStruct,     //结构形式
            "asTreeviewStructType": asTreeviewStructType,     //建筑分类
            "drp_buildtype": drp_buildtype,     //建筑类别
            "txt_proFuze": txt_proFuze,     //工程负责人
            "txtFParty": txtFParty,     //甲方负责人
            "drp_unit": drp_unit,     //承接部门
            "txtcpr_Account": txtcpr_Account,     //合同额1
            "txtcpr_Account2": txtcpr_Account2,     //合同额2
            "txt_Address": txt_Address,     //工程地点
            "ddType": ddType,     //行业性质
            "chk_cprjd": chk_cprjd,     //合同阶段
            "ddSourceWay": ddSourceWay,     //工程来源
            "txt_signdate": txt_signdate,     //合同签订日期1
            "txt_signdate2": txt_signdate2,     //合同签订日期2
            "txt_finishdate": txt_finishdate,     //合同完成日期1
            "txt_finishdate2": txt_finishdate,     //合同完成日期2
            "projcharge1": projcharge1,     //合同收费1
            "projcharge2": projcharge2,     //合同收费2
            "txt_cst": txt_cst,//按照客户
            "startTime": startTime,//录入时间
            "endTime": endTime
        };

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/ProjectChargeHandler.ashx?n='" + Math.random() + " '&action=Search",
            postData: params,
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });


    //监理合同查询
    $("#btn_search3").click(function () {

        //声明查询方式
        var andor = "and"; 

        $("label[rel]", $("#div_sch3")).each(function () {
            if ($(this).hasClass("active")) {
                andor = $.trim($(this).attr("rel"));
            }
        });
      
        ClearjqGridParam("jqGrid3");

        //设置值为1高级查询
        $("#hid_cxtype").val("1");

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //合同编号
        var txtcpr_No = $("#txt_proName3").val();
        //合同分类
        var drp_type = $("#ctl00_ContentPlaceHolder1_txt_ddcpr_Type3").find("option:selected").text();
        //合同名称
        var txt_cprname = $("#txt_cprName3").val();
        //建筑规模
        var txt_Area = $("#txt_buildArea3").val();
        var txt_Area2 = $("#txt_buildArea23").val();
        //建设单位
        var txt_cprBuildUnit = $("#txt_BuildUnit3").val();
        //结构形式
        var asTreeviewStruct = $("#ctl00_ContentPlaceHolder1_asTreeviewStruct3_divDropdownTreeText").text();
        //建筑分类
        var asTreeviewStructType = $("#ctl00_ContentPlaceHolder1_asTreeviewStructType3_divDropdownTreeText").text();
        //建筑类别
        var drp_buildtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype3").find("option:selected").text();
        //工程负责人
        var txt_proFuze = $("#txt_proFuze3").val();
        //甲方负责人
        var txtFParty = $("#txtFParty3").val();
        //外聘人员
        var txtextemalmember = $("#txt_extemalmember").val();
        //承接部门
        //  var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var drp_unit = getCheckedUnitNodes();
        //合同额
        var txtcpr_Account = $("#txtproAcount3").val();
        var txtcpr_Account2 = $("#txtproAcount23").val();
        //工程地点
        var txt_Address = $("#txtaddress3").val();
        //行业性质
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType3").find("option:selected").text();
        //合同阶段
        var chk_cprjd = "";
        var see = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd3").length;

        for (var i = 0; i < parseInt(see) ; i++) {
            var isChecked = $("#ctl00_ContentPlaceHolder1_chk_cprjd3_" + i).parent().attr("class");
            if (isChecked == undefined || isChecked == "") {

            } else {
                chk_cprjd += $("#ctl00_ContentPlaceHolder1_chk_cprjd3_" + i).parent().parent().next().text() + ",";

            }
        }
        //工程来源
        var ddSourceWay = $("#ctl00_ContentPlaceHolder1_ddsource3").find("option:selected").text();
        //合同签订日期
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate3").val();
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate23").val();
        //合同完成日期
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate3").val();
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate23").val();
        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start3").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end3").val();
        //合同收费
     //   var projcharge1 = $("#projcharge13").val();
       // var projcharge2 = $("#projcharge23").val();
        //按照客户
        var txt_cst = $("#txt_cstname3").val();
        //数字验证正则    
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        if (txt_Area != "") {
            if (!reg.test(txt_Area)) {
                msg += "建筑规模开始值格式错误，请输入数字！</br>";
            }
        }
        if (txt_Area2 != "") {
            if (!reg.test(txt_Area2)) {
                msg += "建筑规模结束值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account != "") {
            if (!reg.test(txtcpr_Account)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account2 != "") {
            if (!reg.test(txtcpr_Account2)) {
                msg += "合同额结束值格式错误，请输入数字！</br>";
            }
        }
        //if (projcharge1 != "") {
        //    if (!reg.test(projcharge1)) {
        //        msg += "合同收费开始值格式错误，请输入数字！</br>";
        //    }
        //}
        //if (projcharge2 != "") {
        //    if (!reg.test(projcharge2)) {
        //        msg += "合同收费结束值格式错误，请输入数字！</br>";
        //    }
        //}
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        var params = {
            "strwhere": strwhere,
            "andor": andor,
            "txtcpr_No": txtcpr_No,     //合同编号
            "drp_type": drp_type,     //合同分类
            "txt_cprname": txt_cprname,     //合同名称
            "txt_Area": txt_Area,     //建筑规模1
            "txt_Area2": txt_Area2,     //建筑规模2
            "txt_cprBuildUnit": txt_cprBuildUnit,     //建设单位
            "asTreeviewStruct": asTreeviewStruct,     //结构形式
            "asTreeviewStructType": asTreeviewStructType,     //建筑分类
            "drp_buildtype": drp_buildtype,     //建筑类别
            "txt_proFuze": txt_proFuze,     //工程负责人
            "txtFParty": txtFParty,     //甲方负责人
            "drp_unit": drp_unit,     //承接部门
            "txtextemalmember":txtextemalmember,//外聘人员
            "txtcpr_Account": txtcpr_Account,     //合同额1
            "txtcpr_Account2": txtcpr_Account2,     //合同额2
            "txt_Address": txt_Address,     //工程地点
            "ddType": ddType,     //行业性质
            "chk_cprjd": chk_cprjd,     //合同阶段
            "ddSourceWay": ddSourceWay,     //工程来源
            "txt_signdate": txt_signdate,     //合同签订日期1
            "txt_signdate2": txt_signdate2,     //合同签订日期2
            "txt_finishdate": txt_finishdate,     //合同完成日期1
            "txt_finishdate2": txt_finishdate,     //合同完成日期2      
            "txt_cst": txt_cst,//按照客户
            "startTime": startTime,//录入时间
            "endTime": endTime
        };

        $("#jqGrid3").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/SuperCoperationListHandler.ashx?n='" + Math.random() + " '&action=Search",
            postData: params,
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });


    //勘察合同查询
    $("#btn_search1").click(function () {
       
        //声明查询方式
        var andor = "and";

        $("label[rel]", $("#div_sch1")).each(function () {
            if ($(this).hasClass("active")) {
                andor = $.trim($(this).attr("rel"));
            }
        });

        //清空参数
        ClearjqGridParam("jqGrid1");

        //设置值为1高级查询
        $("#hid_cxtype").val("1");

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //合同编号
        var txtcpr_No = $("#txt_proName1").val();
        //合同分类
        var drp_type = $("#ctl00_ContentPlaceHolder1_txt_ddcpr_Type1").find("option:selected").text();
        //合同名称
        var txt_cprname = $("#txt_cprName1").val();
        //建筑规模
        var txt_Area = $("#txt_buildArea1").val();
        var txt_Area2 = $("#txt_buildArea21").val();
        //建设单位
        var txt_cprBuildUnit = $("#txt_BuildUnit1").val();
        //勘察等级
        var drp_SurveyClass = $("#ctl00_ContentPlaceHolder1_drp_SurveyClass1").find("option:selected").text();
        //建筑分类
        var asTreeviewStructType = $("#ctl00_ContentPlaceHolder1_asTreeviewStructType1_divDropdownTreeText").text();
        //建筑类别
        var drp_buildtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype1").find("option:selected").text();
        //工程负责人
        var txt_proFuze = $("#txt_proFuze1").val();
        //甲方负责人
        var txtFParty = $("#txtFParty1").val();
        //承接部门
        //  var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var drp_unit = getCheckedUnitNodes();
        //合同额
        var txtcpr_Account = $("#txtproAcount1").val();
        var txtcpr_Account2 = $("#txtproAcount21").val();
        //工程地点
        var txt_Address = $("#txtaddress1").val();
        //行业性质
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType1").find("option:selected").text();
        //合同阶段
        var chk_cprjd = "";
        var see = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd1").length;

        for (var i = 0; i < parseInt(see) ; i++) {
            var isChecked = $("#ctl00_ContentPlaceHolder1_chk_cprjd1_" + i).parent().attr("class");
            if (isChecked == undefined || isChecked == "") {

            } else {
                chk_cprjd += $("#ctl00_ContentPlaceHolder1_chk_cprjd1_" + i).parent().parent().next().text() + ",";

            }
        }
        //工程来源
        var ddSourceWay = $("#ctl00_ContentPlaceHolder1_ddsource1").find("option:selected").text();
        //合同签订日期
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate1").val();
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate21").val();
        //合同开始日期
        var txt_startdate = $("#ctl00_ContentPlaceHolder1_txt_startdate1").val();
        var txt_startdate2 = $("#ctl00_ContentPlaceHolder1_txt_startdate21").val();
        //合同完成日期
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate1").val();
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate21").val();
        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start1").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end1").val();
        //工程规模
        var projcharge1 = $("#projcharge11").val();
        var projcharge2 = $("#projcharge21").val();
        //按照客户
        var txt_cst = $("#txt_cstname1").val();
        //工程特征
        var txt_features = $("#txt_Features").val();
        //数字验证正则    
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        if (txt_Area != "") {
            if (!reg.test(txt_Area)) {
                msg += "占地面积开始值格式错误，请输入数字！</br>";
            }
        }
        if (txt_Area2 != "") {
            if (!reg.test(txt_Area2)) {
                msg += "占地面积结束值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account != "") {
            if (!reg.test(txtcpr_Account)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account2 != "") {
            if (!reg.test(txtcpr_Account2)) {
                msg += "合同额结束值格式错误，请输入数字！</br>";
            }
        }
        if (projcharge1 != "") {
            if (!reg.test(projcharge1)) {
                msg += "工程规模开始值格式错误，请输入数字！</br>";
            }
        }
        if (projcharge2 != "") {
            if (!reg.test(projcharge2)) {
                msg += "工程规模结束值格式错误，请输入数字！</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        var params = {
            "strwhere": strwhere,
            "andor": andor,
            "txtcpr_No": txtcpr_No,     //合同编号
            "drp_type": drp_type,     //合同分类
            "txt_cprname": txt_cprname,     //合同名称
            "txt_Area": txt_Area,     //建筑规模1
            "txt_Area2": txt_Area2,     //建筑规模2
            "txt_cprBuildUnit": txt_cprBuildUnit,     //建设单位
            "drp_SurveyClass": drp_SurveyClass,     //勘察等级
            "asTreeviewStructType": asTreeviewStructType,     //建筑分类
            "drp_buildtype": drp_buildtype,     //建筑类别
            "txt_proFuze": txt_proFuze,     //工程负责人
            "txtFParty": txtFParty,     //甲方负责人
            "drp_unit": drp_unit,     //承接部门
            "txtcpr_Account": txtcpr_Account,     //合同额1
            "txtcpr_Account2": txtcpr_Account2,     //合同额2
            "txt_Address": txt_Address,     //工程地点
            "ddType": ddType,     //行业性质
            "chk_cprjd": chk_cprjd,     //合同阶段
            "ddSourceWay": ddSourceWay,     //工程来源
            "txt_signdate": txt_signdate,     //合同签订日期1
            "txt_signdate2": txt_signdate2,     //合同签订日期2
            "txt_startdate": txt_startdate,     //合同开始日期1
            "txt_startdate2": txt_startdate2,     //合同开始日期2
            "txt_finishdate": txt_finishdate,     //合同完成日期1
            "txt_finishdate2": txt_finishdate,     //合同完成日期2
            "projcharge1": projcharge1,     //工程规模1
            "projcharge2": projcharge2,     //工程规模2
            "txt_features":txt_features,//工程特征
            "txt_cst": txt_cst,//按照客户
            "startTime": startTime,//录入时间
            "endTime": endTime
        };

        $("#jqGrid1").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/kcnew_coperationHandler.ashx?n='" + Math.random() + " '&action=Search",
            postData: params,
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //设计合同查询
    $("#btn_search2").click(function () {

        //声明查询方式
        var andor = "and";

        $("label[rel]", $("#div_sch2")).each(function () {
            if ($(this).hasClass("active")) {
                andor = $.trim($(this).attr("rel"));
            }
        });

        //清空参数
        ClearjqGridParam("jqGrid2");

        //设置值为1高级查询
        $("#hid_cxtype").val("1");

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //合同编号
        var txtcpr_No = $("#txt_proName2").val();
        //合同分类
        var drp_type = $("#ctl00_ContentPlaceHolder1_txt_ddcpr_Type2").find("option:selected").text();
        //合同名称
        var txt_cprname = $("#txt_cprName2").val();
        //建筑规模
        var txt_Area = $("#txt_buildArea2").val();
        var txt_Area2 = $("#txt_buildArea22").val();
        //建设单位
        var txt_cprBuildUnit = $("#txt_BuildUnit2").val();
        //建筑类别
        var drp_buildtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype2").find("option:selected").text();
        //工程负责人
        var txt_proFuze = $("#txt_proFuze2").val();
        //甲方负责人
        var txtFParty = $("#txtFParty2").val();
        //承接部门
        //  var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var drp_unit = getCheckedUnitNodes();
        //合同额
        var txtcpr_Account = $("#txtproAcount20").val();
        var txtcpr_Account2 = $("#txtproAcount22").val();
        //工程地点
        var txt_Address = $("#txtaddress2").val();
        //行业性质
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType2").find("option:selected").text();
       
        //工程来源
        var ddSourceWay = $("#ctl00_ContentPlaceHolder1_ddsource2").find("option:selected").text();
        //合同签订日期
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate20").val();
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate22").val();
      
        //合同完成日期
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate20").val();
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate22").val();
        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start2").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end2").val();
        //基坑安全等级
        var txtSafeClass = $("#txt_SafeClass").val();
        //基础类型
        var txtBaseType = $("#txt_BaseType").val();
        //按照客户
        var txt_cst = $("#txt_cstname2").val();       
        //数字验证正则    
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        if (txt_Area != "") {
            if (!reg.test(txt_Area)) {
                msg += "占地面积开始值格式错误，请输入数字！</br>";
            }
        }
        if (txt_Area2 != "") {
            if (!reg.test(txt_Area2)) {
                msg += "占地面积结束值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account != "") {
            if (!reg.test(txtcpr_Account)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account2 != "") {
            if (!reg.test(txtcpr_Account2)) {
                msg += "合同额结束值格式错误，请输入数字！</br>";
            }
        }
        //if (projcharge1 != "") {
        //    if (!reg.test(projcharge1)) {
        //        msg += "合同收费开始值格式错误，请输入数字！</br>";
        //    }
        //}
        //if (projcharge2 != "") {
        //    if (!reg.test(projcharge2)) {
        //        msg += "合同收费结束值格式错误，请输入数字！</br>";
        //    }
        //}
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        var params = {
            "strwhere": strwhere,
            "andor": andor,
            "txtcpr_No": txtcpr_No,     //合同编号
            "drp_type": drp_type,     //合同分类
            "txt_cprname": txt_cprname,     //合同名称
            "txt_Area": txt_Area,     //建筑规模1
            "txt_Area2": txt_Area2,     //建筑规模2
            "txt_cprBuildUnit": txt_cprBuildUnit,     //建设单位            
            "drp_buildtype": drp_buildtype,     //建筑类别
            "txt_proFuze": txt_proFuze,     //工程负责人
            "txtFParty": txtFParty,     //甲方负责人
            "drp_unit": drp_unit,     //承接部门
            "txtcpr_Account": txtcpr_Account,     //合同额1
            "txtcpr_Account2": txtcpr_Account2,     //合同额2
            "txt_Address": txt_Address,     //工程地点
            "ddType": ddType,     //行业性质          
            "ddSourceWay": ddSourceWay,     //工程来源
            "txt_signdate": txt_signdate,     //合同签订日期1
            "txt_signdate2": txt_signdate2,     //合同签订日期2            
            "txt_finishdate": txt_finishdate,     //合同完成日期1
            "txt_finishdate2": txt_finishdate,     //合同完成日期2
            "txtSafeClass": txtSafeClass,     //基坑安全等级
            "txtBaseType": txtBaseType,     //基础类型       
            "txt_cst": txt_cst,//按照客户
            "startTime": startTime,//录入时间
            "endTime": endTime
        };

        $("#jqGrid2").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/DesignCoperationListHandler.ashx?n='" + Math.random() + " '&action=Search",
            postData: params,
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //基坑及边坡监测合同查询
    $("#btn_search4").click(function () {
     
        //声明查询方式
        var andor = "and";
        $("label[rel]", $("#div_sch4")).each(function () {
            if ($(this).hasClass("active")) {
                andor = $.trim($(this).attr("rel"));
            }
        });
       

        ClearjqGridParam("jqGrid4");

        //设置值为1高级查询
        $("#hid_cxtype").val("1");

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //合同编号
        var txtcpr_No = $("#txt_proName4").val();
        //合同分类
        var drp_type = $("#ctl00_ContentPlaceHolder1_txt_ddcpr_Type4").find("option:selected").text();
        //合同名称
        var txt_cprname = $("#txt_cprName4").val();
        //建筑规模
        var txt_Area = $("#txt_buildArea4").val();
        var txt_Area2 = $("#txt_buildArea24").val();
        //建设单位
        var txt_cprBuildUnit = $("#txt_BuildUnit4").val();
        //基坑等级
        var drpPitLevel = $("#ctl00_ContentPlaceHolder1_drp_PitLevel").find("option:selected").text();      
        //建筑类别
        var drp_buildtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype4").find("option:selected").text();
        //工程负责人
        var txt_proFuze = $("#txt_proFuze4").val();
        //甲方负责人
        var txtFParty = $("#txtFParty4").val();
        //承接部门
        //  var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var drp_unit = getCheckedUnitNodes();
        //合同额
        var txtcpr_Account = $("#txtproAcount4").val();
        var txtcpr_Account2 = $("#txtproAcount24").val();
        //工程地点
        var txt_Address = $("#txtaddress4").val();
        //行业性质
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType4").find("option:selected").text();
     
        //工程来源
        var ddSourceWay = $("#ctl00_ContentPlaceHolder1_ddsource4").find("option:selected").text();
        //合同签订日期
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate4").val();
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate24").val();
        //合同完成日期
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate4").val();
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate24").val();
        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start4").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end4").val();
        //监测次数
        var projcharge1 = $("#projcharge14").val();
        var projcharge2 = $("#projcharge24").val();
        //按照客户
        var txt_cst = $("#txt_cstname4").val();
        //数字验证正则    
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        if (txt_Area != "") {
            if (!reg.test(txt_Area)) {
                msg += "占地面积开始值格式错误，请输入数字！</br>";
            }
        }
        if (txt_Area2 != "") {
            if (!reg.test(txt_Area2)) {
                msg += "占地面积结束值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account != "") {
            if (!reg.test(txtcpr_Account)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account2 != "") {
            if (!reg.test(txtcpr_Account2)) {
                msg += "合同额结束值格式错误，请输入数字！</br>";
            }
        }
        if (projcharge1 != "") {
            if (!reg.test(projcharge1)) {
                msg += "监测次数开始值格式错误，请输入数字！</br>";
            }
        }
        if (projcharge2 != "") {
            if (!reg.test(projcharge2)) {
                msg += "监测次数结束值格式错误，请输入数字！</br>";
            }
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        var params = {
            "strwhere": strwhere,
            "andor": andor,
            "txtcpr_No": txtcpr_No,     //合同编号
            "drp_type": drp_type,     //合同分类
            "txt_cprname": txt_cprname,     //合同名称
            "txt_Area": txt_Area,     //建筑规模1
            "txt_Area2": txt_Area2,     //建筑规模2
            "txt_cprBuildUnit": txt_cprBuildUnit,     //建设单位
            "drpPitLevel": drpPitLevel,     //基坑等级            
            "drp_buildtype": drp_buildtype,     //建筑类别
            "txt_proFuze": txt_proFuze,     //工程负责人
            "txtFParty": txtFParty,     //甲方负责人
            "drp_unit": drp_unit,     //承接部门
            "txtcpr_Account": txtcpr_Account,     //合同额1
            "txtcpr_Account2": txtcpr_Account2,     //合同额2
            "txt_Address": txt_Address,     //工程地点
            "ddType": ddType,     //行业性质           
            "ddSourceWay": ddSourceWay,     //工程来源
            "txt_signdate": txt_signdate,     //合同签订日期1
            "txt_signdate2": txt_signdate2,     //合同签订日期2
            "txt_finishdate": txt_finishdate,     //合同完成日期1
            "txt_finishdate2": txt_finishdate,     //合同完成日期2
            "projcharge1": projcharge1,     //监测次数1
            "projcharge2": projcharge2,     //监测次数2
            "txt_cst": txt_cst,//按照客户
            "startTime": startTime,//录入时间
            "endTime": endTime
        };

        $("#jqGrid4").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/PitCoperationListHandler.ashx?n='" + Math.random() + " '&action=Search",
            postData: params,
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //测量及沉降观测合同查询
    $("#btn_search7").click(function () {

        //声明查询方式
        var andor = "and";
        $("label[rel]", $("#div_sch7")).each(function () {
            if ($(this).hasClass("active")) {
                andor = $.trim($(this).attr("rel"));
            }
        });
        

        ClearjqGridParam("jqGrid7");

        //设置值为1高级查询
        $("#hid_cxtype").val("1");

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //合同编号
        var txtcpr_No = $("#txt_proName7").val();
        //合同分类
        var drp_type = $("#ctl00_ContentPlaceHolder1_txt_ddcpr_Type7").find("option:selected").text();
        //合同名称
        var txt_cprname = $("#txt_cprName7").val();
        //建筑规模
        var txt_Area = $("#txt_buildArea7").val();
        var txt_Area2 = $("#txt_buildArea27").val();
        //建设单位
        var txt_cprBuildUnit = $("#txt_BuildUnit7").val();
      
        //建筑类别
        var drp_buildtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype7").find("option:selected").text();
        //工程负责人
        var txt_proFuze = $("#txt_proFuze7").val();
        //甲方负责人
        var txtFParty = $("#txtFParty7").val();
        //承接部门
        //  var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var drp_unit = getCheckedUnitNodes();
        //合同额
        var txtcpr_Account = $("#txtproAcount7").val();
        var txtcpr_Account2 = $("#txtproAcount27").val();
        //工程地点
        var txt_Address = $("#txtaddress7").val();
        //行业性质
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType7").find("option:selected").text();

        //工程来源
        var ddSourceWay = $("#ctl00_ContentPlaceHolder1_ddsource7").find("option:selected").text();
        //合同签订日期
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate7").val();
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate27").val();
        //合同完成日期
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate7").val();
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate27").val();
        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start7").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end7").val();     
        //按照客户
        var txt_cst = $("#txt_cstname7").val();
        //数字验证正则    
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        if (txt_Area != "") {
            if (!reg.test(txt_Area)) {
                msg += "占地面积开始值格式错误，请输入数字！</br>";
            }
        }
        if (txt_Area2 != "") {
            if (!reg.test(txt_Area2)) {
                msg += "占地面积结束值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account != "") {
            if (!reg.test(txtcpr_Account)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account2 != "") {
            if (!reg.test(txtcpr_Account2)) {
                msg += "合同额结束值格式错误，请输入数字！</br>";
            }
        }
       
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        var params = {
            "strwhere": strwhere,
            "andor": andor,
            "txtcpr_No": txtcpr_No,     //合同编号
            "drp_type": drp_type,     //合同分类
            "txt_cprname": txt_cprname,     //合同名称
            "txt_Area": txt_Area,     //建筑规模1
            "txt_Area2": txt_Area2,     //建筑规模2
            "txt_cprBuildUnit": txt_cprBuildUnit,     //建设单位              
            "drp_buildtype": drp_buildtype,     //建筑类别
            "txt_proFuze": txt_proFuze,     //工程负责人
            "txtFParty": txtFParty,     //甲方负责人
            "drp_unit": drp_unit,     //承接部门
            "txtcpr_Account": txtcpr_Account,     //合同额1
            "txtcpr_Account2": txtcpr_Account2,     //合同额2
            "txt_Address": txt_Address,     //工程地点
            "ddType": ddType,     //行业性质           
            "ddSourceWay": ddSourceWay,     //工程来源
            "txt_signdate": txt_signdate,     //合同签订日期1
            "txt_signdate2": txt_signdate2,     //合同签订日期2
            "txt_finishdate": txt_finishdate,     //合同完成日期1
            "txt_finishdate2": txt_finishdate,     //合同完成日期2          
            "txt_cst": txt_cst,//按照客户
            "startTime": startTime,//录入时间
            "endTime": endTime
        };

        $("#jqGrid7").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/TestCoperationListHandler.ashx?n='" + Math.random() + " '&action=Search",
            postData: params,
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //岩土工程检测合同查询
    $("#btn_search6").click(function () {

        //声明查询方式
        var andor = "and";
        $("label[rel]", $("#div_sch6")).each(function () {
            if ($(this).hasClass("active")) {
                andor = $.trim($(this).attr("rel"));
            }
        });


        ClearjqGridParam("jqGrid6");

        //设置值为1高级查询
        $("#hid_cxtype").val("1");

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //合同编号
        var txtcpr_No = $("#txt_proName6").val();
        //合同分类
        var drp_type = $("#ctl00_ContentPlaceHolder1_txt_ddcpr_Type6").find("option:selected").text();
        //合同名称
        var txt_cprname = $("#txt_cprName6").val();
        //建筑规模
      //  var txt_Area = $("#txt_buildArea6").val();
       // var txt_Area2 = $("#txt_buildArea26").val();
        //建设单位
        var txt_cprBuildUnit = $("#txt_BuildUnit6").val();

        //建筑类别
        var drp_buildtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype6").find("option:selected").text();
        //工程负责人
        var txt_proFuze = $("#txt_proFuze6").val();
        //甲方负责人
        var txtFParty = $("#txtFParty6").val();
        //承接部门
        //  var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var drp_unit = getCheckedUnitNodes();
        //合同额
        var txtcpr_Account = $("#txtproAcount6").val();
        var txtcpr_Account2 = $("#txtproAcount26").val();
        //工程地点
        var txt_Address = $("#txtaddress6").val();
        //行业性质
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType6").find("option:selected").text();

        //工程来源
        var ddSourceWay = $("#ctl00_ContentPlaceHolder1_ddsource6").find("option:selected").text();
        //合同签订日期
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate6").val();
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate26").val();
        //合同完成日期
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate6").val();
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate26").val();
        //检测工程量
        var txt_multibuild = $("#ctl00_ContentPlaceHolder1_txt_MultiBuild").val();
        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start6").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end6").val();
        //按照客户
        var txt_cst = $("#txt_cstname6").val();
        //数字验证正则    
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        //if (txt_Area != "") {
        //    if (!reg.test(txt_Area)) {
        //        msg += "建筑规模开始值格式错误，请输入数字！</br>";
        //    }
        //}
        //if (txt_Area2 != "") {
        //    if (!reg.test(txt_Area2)) {
        //        msg += "建筑规模结束值格式错误，请输入数字！</br>";
        //    }
        //}
        if (txtcpr_Account != "") {
            if (!reg.test(txtcpr_Account)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account2 != "") {
            if (!reg.test(txtcpr_Account2)) {
                msg += "合同额结束值格式错误，请输入数字！</br>";
            }
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        var params = {
            "strwhere": strwhere,
            "andor": andor,
            "txtcpr_No": txtcpr_No,     //合同编号
            "drp_type": drp_type,     //合同分类
            "txt_cprname": txt_cprname,     //合同名称          
            "txt_cprBuildUnit": txt_cprBuildUnit,     //建设单位              
            "drp_buildtype": drp_buildtype,     //建筑类别
            "txt_proFuze": txt_proFuze,     //工程负责人
            "txtFParty": txtFParty,     //甲方负责人
            "drp_unit": drp_unit,     //承接部门
            "txtcpr_Account": txtcpr_Account,     //合同额1
            "txtcpr_Account2": txtcpr_Account2,     //合同额2
            "txt_Address": txt_Address,     //工程地点
            "ddType": ddType,     //行业性质           
            "ddSourceWay": ddSourceWay,     //工程来源
            "txt_signdate": txt_signdate,     //合同签订日期1
            "txt_signdate2": txt_signdate2,     //合同签订日期2
            "txt_finishdate": txt_finishdate,     //合同完成日期1
            "txt_finishdate2": txt_finishdate,     //合同完成日期2 
            "txt_multibuild": txt_multibuild,   //检测工程量
            "txt_cst": txt_cst,//按照客户
            "startTime": startTime,//录入时间
            "endTime": endTime
        };

        $("#jqGrid6").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/MeasureCoperationListHandler.ashx?n='" + Math.random() + " '&action=Search",
            postData: params,
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
    //岩土工程施工合同查询
    $("#btn_search8").click(function () {

        //声明查询方式
        var andor = "and";
        $("label[rel]", $("#div_sch8")).each(function () {
            if ($(this).hasClass("active")) {
                andor = $.trim($(this).attr("rel"));
            }
        });


        ClearjqGridParam("jqGrid8");

        //设置值为1高级查询
        $("#hid_cxtype").val("1");

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        //合同编号
        var txtcpr_No = $("#txt_proName8").val();
        //合同分类
        var drp_type = $("#ctl00_ContentPlaceHolder1_txt_ddcpr_Type8").find("option:selected").text();
        //合同名称
        var txt_cprname = $("#txt_cprName8").val();
        //建筑规模
        var txt_Area = $("#txt_buildArea8").val();
        var txt_Area2 = $("#txt_buildArea28").val();
        //建设单位
        var txt_cprBuildUnit = $("#txt_BuildUnit8").val();

        //建筑类别
        var drp_buildtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype8").find("option:selected").text();
        //工程负责人
        var txt_proFuze = $("#txt_proFuze8").val();
        //甲方负责人
        var txtFParty = $("#txtFParty8").val();
        //承接部门
        //  var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var drp_unit = getCheckedUnitNodes();
        //合同额
        var txtcpr_Account = $("#txtproAcount8").val();
        var txtcpr_Account2 = $("#txtproAcount28").val();
        //工程地点
        var txt_Address = $("#txtaddress8").val();
        //行业性质
        var ddType = $("#ctl00_ContentPlaceHolder1_ddType8").find("option:selected").text();

        //工程来源
        var ddSourceWay = $("#ctl00_ContentPlaceHolder1_ddsource8").find("option:selected").text();
        //合同签订日期
        var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate8").val();
        var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate28").val();
        //合同完成日期
        var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate8").val();
        var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate28").val();     
        //录入时间
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_start8").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_end8").val();
        //按照客户
        var txt_cst = $("#txt_cstname8").val();
        //数字验证正则    
        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        if (txt_Area != "") {
            if (!reg.test(txt_Area)) {
                msg += "建筑规模开始值格式错误，请输入数字！</br>";
            }
        }
        if (txt_Area2 != "") {
            if (!reg.test(txt_Area2)) {
                msg += "建筑规模结束值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account != "") {
            if (!reg.test(txtcpr_Account)) {
                msg += "合同额开始值格式错误，请输入数字！</br>";
            }
        }
        if (txtcpr_Account2 != "") {
            if (!reg.test(txtcpr_Account2)) {
                msg += "合同额结束值格式错误，请输入数字！</br>";
            }
        }

        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
        var params = {
            "strwhere": strwhere,
            "andor": andor,
            "txtcpr_No": txtcpr_No,     //合同编号
            "drp_type": drp_type,     //合同分类
            "txt_cprname": txt_cprname,     //合同名称   
            "txt_Area": txt_Area,     //建筑规模1
            "txt_Area2": txt_Area2,     //建筑规模2
            "txt_cprBuildUnit": txt_cprBuildUnit,     //建设单位              
            "drp_buildtype": drp_buildtype,     //建筑类别
            "txt_proFuze": txt_proFuze,     //工程负责人
            "txtFParty": txtFParty,     //甲方负责人
            "drp_unit": drp_unit,     //承接部门
            "txtcpr_Account": txtcpr_Account,     //合同额1
            "txtcpr_Account2": txtcpr_Account2,     //合同额2
            "txt_Address": txt_Address,     //工程地点
            "ddType": ddType,     //行业性质           
            "ddSourceWay": ddSourceWay,     //工程来源
            "txt_signdate": txt_signdate,     //合同签订日期1
            "txt_signdate2": txt_signdate2,     //合同签订日期2
            "txt_finishdate": txt_finishdate,     //合同完成日期1
            "txt_finishdate2": txt_finishdate,     //合同完成日期2           
            "txt_cst": txt_cst,//按照客户
            "startTime": startTime,//录入时间
            "endTime": endTime
        };

        $("#jqGrid8").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/ConstruCoperationListHandler.ashx?n='" + Math.random() + " '&action=Search",
            postData: params,
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });
});

//高级查询选择多选框字段
function CheckBoxComm(tblid, tblid2, divsel,btnid) {
    
    //字段导出记录
    var columnslist = new Array();
    var columnsChinaNameList = new Array();
    //点击选择条件
    $("input[name=bb]:checkbox", $("#" + tblid)).click(function () {
        //条件名称
        // var commName = $.trim($(this).get(0).id);
        var commName = $.trim($(this).parent().parent().siblings("span").attr("for"));
        //获取数据库字段名      
        var columnsname = $.trim($(this).parent().parent().siblings("span").attr("for"));
        //获取表头字段名
        var columnsChinaName = $.trim($(this).parent().parent().siblings("span").text());
        if ($(this).is(":checked")) {
            $("#" + tblid2 + " tr[for=" + commName + "]").show();
            //判断执行设总和甲方负责人
            if (commName == "proFuze" || commName == "FParty") {
                $("#" + tblid2 + " tr[for=" + commName + "]").next().show();
            }
            //添加到数组里
            columnslist.push(columnsname);
            columnsChinaNameList.push(columnsChinaName);

            //显示字段
            //  $("#jqGrid").showCol(columnsname).trigger('reloadGrid');
        }
        else {
            //判断执行设总和甲方负责人
            if (commName == "proFuze" || commName == "FParty") {
                $("#" + tblid2 + " tr[for=" + commName + "]").next().hide();
            }
            $("#" + tblid2 + " tr[for=" + commName + "]").hide();
            $("#" + tblid2 + " tr[for=" + commName + "]").removeData();
            $(":text", "#" + tblid2 + " tr[for=" + commName + "]").val("");
            $("select", "#" + tblid2 + " tr[for=" + commName + "]").val("-1");
            if (commName == "StructType") {
                $("#ctl00_ContentPlaceHolder1_asTreeviewStruct_divDropdownTreeText").text("");
            }
            if (commName == "BuildStructType") {
                var asTreeviewStructType = $("#ctl00_ContentPlaceHolder1_asTreeviewStructType_divDropdownTreeText").text("");
            }
            if (commName == "cpr_Process") {
                var see = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd").length;

                for (var i = 0; i < parseInt(see) ; i++) {
                    $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().attr("class", "");
                }

            }
            //隐藏字段
            if (columnsname != "cpr_Name" && columnsname != "ChgPeople" && columnsname != "cpr_Unit" && columnsname != "cpr_Acount" && columnsname != "cpr_Process" && columnsname != "cpr_SignDate" && columnsname != "cpr_DoneDate" && columnsname != "ssze") {
                // $("#jqGrid").hideCol(columnsname).trigger('reloadGrid');
                $(":text", "#" + tblid2 + " tr[for=" + commName + "]").val("");
            }
            //删除数组
            columnslist.splice($.inArray(columnsname, columnslist), 1);
            columnsChinaNameList.splice($.inArray(columnsChinaName, columnsChinaNameList), 1);
        }

        //有条件显示查询按钮
        if ($("input[name=bb]:checkbox:checked", $("#"+tblid)).length > 0) {
            $("#" + tblid2 + " tr:last").show();

        }
        else {
            $("#" + tblid2 + " tr:last").hide();
            $(":text", "#" + tblid2).val("");

        }

    });
    //点击是否
    $("label[rel]", "#" + divsel).click(function () {
        $(this).addClass("yellow active");
        $(this).removeClass("default");
        $(this).siblings("label").removeClass("active yellow");
        $(this).siblings("label").addClass("default");      
        $("#" + btnid).click();
    });
}
//导出
var SearchExport = function () {

    //声明查询方式
    var andor = "and";
    $("label[rel]", $("#div_sch")).each(function () {
        if ($(this).hasClass("active")) {
            andor = $.trim($(this).attr("rel"));
        }
    });

    //显示字段
    var colsname = $("#ctl00_ContentPlaceHolder1_hid_cols").val();
    var colsvalue = $("#ctl00_ContentPlaceHolder1_hid_colsvalue").val();
    //权限条件
    var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
    //合同编号
    var txtcpr_No = $("#txt_proName").val();
    //合同分类
    var drp_type = $("#ctl00_ContentPlaceHolder1_txt_ddcpr_Type").find("option:selected").text();
    //合同名称
    var txt_cprname = $("#txt_cprName").val();
    //建筑规模
    var txt_Area = $("#txt_buildArea").val();
    var txt_Area2 = $("#txt_buildArea2").val();
    //建设单位
    var txt_cprBuildUnit = $("#txt_BuildUnit").val();
    //结构形式
    var asTreeviewStruct = $("#ctl00_ContentPlaceHolder1_asTreeviewStruct_divDropdownTreeText").text();
    //建筑分类
    var asTreeviewStructType = $("#ctl00_ContentPlaceHolder1_asTreeviewStructType_divDropdownTreeText").text();
    //建筑类别
    var drp_buildtype = $("#ctl00_ContentPlaceHolder1_drp_buildtype").find("option:selected").text();
    //工程负责人
    var txt_proFuze = $("#txt_proFuze").val();
    //甲方负责人
    var txtFParty = $("#txtFParty").val();
    //承接部门
    //var drp_unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
    var drp_unit = getCheckedUnitNodes();
    //合同额
    var txtcpr_Account = $("#txtproAcount").val();
    var txtcpr_Account2 = $("#txtproAcount2").val();
    //工程地点
    var txt_Address = $("#txtaddress").val();
    //行业性质
    var ddType = $("#ctl00_ContentPlaceHolder1_ddType").find("option:selected").text();
    //合同阶段
    var chk_cprjd = "";
    var see = $(":checkbox", "#ctl00_ContentPlaceHolder1_chk_cprjd").length;

    for (var i = 0; i < parseInt(see) ; i++) {
        var isChecked = $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().attr("class");
        if (isChecked == undefined || isChecked == "") {

        } else {
            chk_cprjd += $("#ctl00_ContentPlaceHolder1_chk_cprjd_" + i).parent().parent().next().text() + ",";

        }
    }
    //工程来源
    var ddSourceWay = $("#ctl00_ContentPlaceHolder1_ddsource").find("option:selected").text();
    //合同签订日期
    var txt_signdate = $("#ctl00_ContentPlaceHolder1_txt_signdate").val();
    var txt_signdate2 = $("#ctl00_ContentPlaceHolder1_txt_signdate2").val();
    //合同完成日期
    var txt_finishdate = $("#ctl00_ContentPlaceHolder1_txt_finishdate").val();
    var txt_finishdate2 = $("#ctl00_ContentPlaceHolder1_txt_finishdate2").val();
    //录入时间
    var startTime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
    var endTime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
    //合同收费
    var projcharge1 = $("#projcharge1").val();
    var projcharge2 = $("#projcharge2").val();
    //按照客户
    var txt_cst = $("#txt_cstname").val();
    // var str_columnsname = "";
    // var str_columnschinaname = "";
    //if (columnslist != null && columnslist.length > 0) {
    //    for (var i = 0; i < columnslist.length; i++) {
    //        var arr = $.trim(columnslist[i]);
    //        var arr_china = $.trim(columnsChinaNameList[i]);
    //        if (arr != "cpr_Name" && arr != "ChgPeople" && arr != "cpr_Unit" && arr != "cpr_Acount" && arr != "cpr_Process" && arr != "cpr_SignDate" && arr != "cpr_DoneDate" && arr != "ssze") {
    //            str_columnsname = str_columnsname + arr + ",";
    //            str_columnschinaname = str_columnschinaname + arr_china + ",";
    //        }
    //    }
    //}
    //if (str_columnsname != "") {
    //    str_columnsname = str_columnsname.substring(str_columnsname, (str_columnsname.length - 1));
    //}
    //if (str_columnschinaname != "") {
    //    str_columnschinaname = str_columnschinaname.substring(str_columnschinaname, (str_columnschinaname.length - 1));
    //}
    //数字验证正则    
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    var msg = "";
    if (txt_Area != "") {
        if (!reg.test(txt_Area)) {
            msg += "建筑规模开始值格式错误，请输入数字！</br>";
        }
    }
    if (txt_Area2 != "") {
        if (!reg.test(txt_Area2)) {
            msg += "建筑规模结束值格式错误，请输入数字！</br>";
        }
    }
    if (txtcpr_Account != "") {
        if (!reg.test(txtcpr_Account)) {
            msg += "合同额开始值格式错误，请输入数字！</br>";
        }
    }
    if (txtcpr_Account2 != "") {
        if (!reg.test(txtcpr_Account2)) {
            msg += "合同额结束值格式错误，请输入数字！</br>";
        }
    }
    if (projcharge1 != "") {
        if (!reg.test(projcharge1)) {
            msg += "合同收费开始值格式错误，请输入数字！</br>";
        }
    }
    if (projcharge2 != "") {
        if (!reg.test(projcharge2)) {
            msg += "合同收费结束值格式错误，请输入数字！</br>";
        }
    }
    if (msg != "") {
        jAlert(msg, "提示");
        return false;
    }

    var params = {
        "strwhere": strwhere,
        "txtcpr_No": txtcpr_No,     //合同编号
        "drp_type": drp_type,     //合同分类
        "txt_cprname": txt_cprname,     //合同名称
        "txt_Area": txt_Area,     //建筑规模1
        "txt_Area2": txt_Area2,     //建筑规模2
        "txt_cprBuildUnit": txt_cprBuildUnit,     //建设单位
        "asTreeviewStruct": asTreeviewStruct,     //结构形式
        "asTreeviewStructType": asTreeviewStructType,     //建筑分类
        "drp_buildtype": drp_buildtype,     //建筑类别
        "txt_proFuze": txt_proFuze,     //工程负责人
        "txtFParty": txtFParty,     //甲方负责人
        "drp_unit": drp_unit,     //承接部门
        "txtcpr_Account": txtcpr_Account,     //合同额1
        "txtcpr_Account2": txtcpr_Account2,     //合同额2
        "txt_Address": txt_Address,     //工程地点
        "ddType": ddType,     //行业性质
        "chk_cprjd": chk_cprjd,     //合同阶段
        "ddSourceWay": ddSourceWay,     //工程来源
        "txt_signdate": txt_signdate,     //合同签订日期1
        "txt_signdate2": txt_signdate2,     //合同签订日期2
        "txt_finishdate": txt_finishdate,     //合同完成日期1
        "txt_finishdate2": txt_finishdate,     //合同完成日期2
        "projcharge1": projcharge1,     //合同收费1
        "projcharge2": projcharge2,     //合同收费2
        "txt_cst": txt_cst,//按照客户
        "startTime": startTime,//录入时间
        "endTime": endTime,
        "str_columnsname": colsvalue,
        "str_columnschinaname": colsname,
        "andor": andor
    };

    window.location.href = "ExportWord.aspx?action=search&strwhere=" + strwhere + "&txtcpr_No=" + txtcpr_No + "&drp_type=" + drp_type + "&txt_cprname=" + txt_cprname + "&txt_Area=" + txt_Area + "&txt_Area2=" + txt_Area2 + "&txt_cprBuildUnit=" + txt_cprBuildUnit + "&asTreeviewStruct=" + asTreeviewStruct + "&asTreeviewStructType=" + asTreeviewStructType + "&drp_buildtype=" + drp_buildtype + "&txt_proFuze=" + txt_proFuze + "&txtFParty=" + txtFParty + "&drp_unit=" + drp_unit + "&txtcpr_Account=" + txtcpr_Account + "&txtcpr_Account2=" + txtcpr_Account2 + "&txt_Address=" + txt_Address + "&ddType=" + ddType + "&chk_cprjd=" + chk_cprjd + "&ddSourceWay=" + ddSourceWay + "&txt_signdate=" + txt_signdate + "&txt_signdate2=" + txt_signdate2 + "&txt_finishdate=" + txt_finishdate + "&txt_finishdate2=" + txt_finishdate2 + "&projcharge1=" + projcharge1 + "&projcharge2=" + projcharge2 + "&txt_cst=" + txt_cst + "&str_columnsname=" + colsvalue + "&str_columnschinaname=" + colsname + "&andor=" + andor + "&startTime=" + startTime + "&endTime=" + endTime + "";

};