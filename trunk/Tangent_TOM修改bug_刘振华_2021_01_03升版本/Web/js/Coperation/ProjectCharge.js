﻿
$(document).ready(function () {

    CommonControl.SetFormWidth();

    //隔行变色
    $("#gv_Coperation tr:even").css({ background: "White" });

    ChangedBackgroundColor();
    //    //进度条
    //    $.each($(".progressbar"), function (index, item) {
    //      
    //        var percent = parseFloat($(item).parent().text());
    //              
    //        $(item).progressbar({
    //            value: percent
    //        });
    //    });


    //添加
    $(".cls_add").live("click", function () {
        var projid = $(this).attr("rel");
        // var projid = $(this).next(":hidden").val();
        var allcount = $(this).attr("data-cpracount");//合同额
        var cprName = $.trim($(this).attr("data-cprname")); //项目名字
        var cprpmname = $.trim($(this).attr("data-cprpmname")); //承接负责人
        var subcprname = $.trim($(this).attr("data-subcprname"));//合同子项
        //绑定隐藏字段
        $("#hide_cprName").val(cprName);
        $("#hid_chgidNow").val(projid); //当前项目ID       
        $("#hide_cprpmname").val(cprpmname); //绑定承接负责人

        //绑定层中数据
        $("#lbl_projname").text(cprName);
        $("#lbl_pmname").text(cprpmname);
        $("#lbl_allcount").text(allcount);
        //每次需清空
        $("#drp_subcprname").html("");
        //绑定合同信息下拉框
        if (subcprname!="") {
            var sublist = subcprname.split(",");
           
            for (var i = 0; i < sublist.length; i++)
            {
                var $option = $("<option value='" + sublist[i] + "'>" + sublist[i] + "</option>");
                $("#drp_subcprname").append($option);
            }
        }
       
        //var url = "ProjChgWin/AddProjCharge.aspx?projno=" + cprid + "&projallcount=" + allcount + "&tspan=" + Math.random();
        //ShowDialogWin_CprCharge(url);
        BindCompUnitValue();
        //        $("#addChargeDialogDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 680,
        //            height: "auto",
        //            resizable: false,
        //            title: "添加收费"
        //        }).dialog("open");
        //添加收费-按钮
        $("#btn_addcount").unbind('click').click(function () {
            BtnAddEditCount("add");
        });
        WatchToChargeData("addChargeListTable");
        AddOrEditCharge("add");

    });
    //查看
    $(".cls_chk").live("click", function () {
        var projid = $(this).attr("rel");
        // var projid = $(this).next(":hidden").val();
        var allcount = $(this).attr("data-cpracount");
        //var url = "ProjChgWin/ShowChargeDetails.aspx?projno=" + cprid + "&projallcount=" + allcount + "&tspan=" + Math.random();
        //ShowDialogWin_ChargeCheck(url);
       
        $("#hid_chgidNow").val(projid);
        $("#lbl_allcount").text(allcount);
        //        $("#seeChargeDialogDiv").dialog({
        //            autoOpen: false,
        //            modal: true,
        //            width: 750,
        //            height: "auto",
        //            resizable: false,
        //            title: "收费列表详细"
        //        }).dialog("open");
        WatchToChargeData("chargeDataTableSee");
    });
    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#ImageButton1"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Corperation";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear = $("#drp_year").val();
    var autoComplete = new AutoComplete(paramEntity, $("#ctl00_ContentPlaceHolder1_txt_keyname"));
    autoComplete.GetAutoAJAX();

    //提示
    $(".cls_column").tipsy({ opacity: 1 });
});
/*
//添加计划收费弹出窗口
function ShowDialogWin_CprCharge(url) {
var feature = "dialogWidth:660px;dialogHeight:400px;center:yes";
var result = window.showModalDialog(url, "", feature);
if (!result) {
result = window.returnValue;
}
if (result == "1") {
window.document.location.href = window.document.location.href+"?"+Math.random();
}
}
//收费情况
function ShowDialogWin_ChargeCheck(url) {
var feature = "dialogWidth:700px;dialogHeight:350px;center:yes";
var result = window.showModalDialog(url, "", feature);
if (!result) {
result = window.returnValue;
}
if (result == "1") {
window.document.location.href = window.document.location.href + "?" + Math.random();
}
}*/

//查看收费详细
function WatchToChargeData(tableID) {
    //var projid = $(btnObj).next(":hidden").val();
    var projid = $("#hid_chgidNow").val();
    //var allcount = $(btnObj).parent().parent().find("td").eq(3).text();
    var allcount = $("#lbl_allcount").text();
    var data = "action=getChargeDataList&proSysNo=" + projid;
    $("#" + tableID + " tr:gt(0)").remove();
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null && result != NaN) {
                var dataObj = result.ds;
                $.each(dataObj, function (i, n) {
                    var trHtml = "";
                    var pres = ((n.Acount / allcount) * 100).toFixed(2) + "%";
                    var chargeStatu = "";
                    var status = $.trim(n.Status);
                    var dateShort = GetShortDate(n.InAcountTime);
                    var remarkSub = n.ProcessMark;
                    if (remarkSub.length > 10) {
                        remarkSub = remarkSub.substr(0, 10) + "...";
                    }
                    var fromUser = n.FromUser;
                    //if (fromUser.length > 10) {
                    //    fromUser = fromUser.substr(0, 10) + "...";
                    //}
                    //if (status == "A") {
                    //    chargeStatu = "财务待确认";
                    //} else if (status == "C") {
                    //    chargeStatu = "所长待确认";
                    //} else if (status == "B") {
                    //    chargeStatu = "财务不通过";
                    //} else if (status == "E") {
                    //    chargeStatu = "完成入账";
                    //}
                    //else if (status == "D") {
                    //    chargeStatu = "所长不通过";
                    //}
                    trHtml = '<tr><td style="background-color:#ffffff;">' + (i + 1) + '</td><td style="background-color:#ffffff;">' + n.Acount + '</td><td style="background-color:#ffffff;">' + pres + '</td><td style="background-color:#ffffff;" title="' + n.FromUser + '">' + fromUser + '</td><td style="background-color:#ffffff;">' + n.mem_Name + '</td><td style="background-color:#ffffff;">' + dateShort + '</td>';
                    //状态
                    //  trHtml = '<td style="background-color:#ffffff;">' + chargeStatu + '</td>';
                   // if (tableID == "chargeDataTableSee") {
                        trHtml += '<td style="background-color:#ffffff;" title="' + n.ProcessMark + '">' + remarkSub + '</td>';
                  //  }
                    if (tableID == "addChargeListTable") {
                        var oper = "<td style='background-color:#ffffff;'><a href='###' rel='" + n.projID + "' style=\"color:blue;\">删除</a>";
                        oper += "&nbsp;|&nbsp;<a href='###' rel='" + n.projID + "' style=\"color:blue;\">修改</a></td>";
                        trHtml += oper;
                    }
                    trHtml += '</tr>';
                    $("#" + tableID + "").append(trHtml);
                    //操作
                    $("#" + tableID + " tr:last").find("a").click(function () {
                        var operTxt = $(this).text();
                        if (operTxt == "删除") {
                           // if (status == "C" || status == "E") {
                           //     alert("已经完成入账确认,不可删除!");
                           //     return false;
                         //   } else {
                                if (confirm("确定删除这次收费记录?")) {
                                    $("#hid_chgidThis").val(n.ID);
                                    DelProCharge(this);
                                }
                          //  }
                        } else if (operTxt == "修改") {
                         //   if (status == "C" || status == "E") {
                          //      alert("已完成入账确认,不可修改!");
                         //       return false;
                         //   } else {
                                $("#hid_chgidThis").val(n.ID); //先绑定该项的ID
                                AddOrEditCharge("edit");
                                $("#txt_payCount").val(n.Acount);
                               // $("#txt_remitter").val($.trim(n.FromUser));
                                $("#txt_times").val(dateShort);
                                $("#txt_chargeRemark").val(n.ProcessMark);
                                $("#drp_subcprname").find("option[value='" + $.trim(n.FromUser) + "']").attr("selected", true);
                           // }
                        }
                    });
                });
                ControlTableCss(tableID);
            } else {
                NoDataMessageOnTable(tableID, 9);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!");
        }
    });
}
//删除收费
function DelProCharge(aObj) {
    var chg_id = $("#hid_chgidThis").val(); //每条项目收费数据ID
    var row = $(aObj).parent().parent();
    var url = "../../HttpHandler/ProjectChargeHandler.ashx";
    var data = { 'action': 'del', 'chgid': chg_id };
    $.post(url, data, function (rlt) {
        if (rlt == "0") {
            alert('删除成功！');
            BindCompUnitValue();
            row.remove();
        }
        else {
            alert('删除失败！');
        }
    }, 'text');
}
//添加收费
function BtnAddEditCount(flag) {
    //验证
    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
    var thisPayCount = $("#txt_payCount").val(); //本次入账金额
    //if ($("#hid_iscomplete").val() == "1") {
    //    alert("已完成入账，无需入账！");
    //    return false;
    //}
    ////入账金额
    if (thisPayCount == "" || thisPayCount == null) {
        $("#txt_paycount_valide").text("请输入入账金额!").show();
        return false;
    } else if (!reg.test(Math.abs(thisPayCount)) || isNaN(thisPayCount)) {
        $("#txt_paycount_valide").text("请输入正确的数字!").show();
        return false;
    } else if (flag == "add") {
        if (parseFloat(thisPayCount) > parseFloat($("#lbl_nopaycount").text())) {
            // $("#txt_paycount_valide").text("支付金额不能大于未支付金额！").show();
            // return false;
        }
    } else {
        $("#txt_paycount_valide").hide();
    }
    //入账单据号
    //    if (flag == "add") {
    //        if ($("#txt_billcode").val() == "") {
    //            $("#txt_billcode_valide").show();
    //            return false;
    //        } else {
    //            $("#txt_billcode_valide").hide();
    //        }
    //    }
    //汇款人
    //if ($("#txt_remitter").val() == "") {
    //    $("#txt_remitter_valide").show();
    //    return false;
    //} else {
    //    $("#txt_remitter_valide").hide();
    //}
    //时间
    if ($("#txt_times").val() == "") {
        $("#txt_time_valide").show();
        return false;
    } else {
        $("#txt_time_valide").hide();
    }
    //备注
    if ($.trim($("#txt_chargeRemark").val())=="") {
        $("#txt_chargeRemark_valide").show();
        return false;
    }
    else
    {
        $("#txt_chargeRemark_valide").hide();
    }
    //添加收款信息
    var projid = $("#hid_chgidNow").val();
    var chgid = $("#hid_chgidThis").val(); //每条项目收费数据ID
    var paycount = $("#lbl_allcount").text(); //总金额
    //var billcode = $("#txt_billcode").val();
    var time = $("#txt_times").val();
    var mark = $("#txt_chargeRemark").val();
    var remitter = $("#drp_subcprname").val();
    var curuser = $("#ctl00_ContentPlaceHolder1_hid_curuser").val();
    var url = "../../HttpHandler/ProjectChargeHandler.ashx";
    if (flag == "add") {
        var data = { "action": "add", "projid": "" + projid + "", "paycount": "" + thisPayCount + "", "time": "" + time + "", "mark": "" + mark + "", "remitter": remitter, "curuser": curuser, "billcode": "" };
        $.post(url, data, function (rlt) {
            if (rlt == "0") {
                alert("添加项目收费成功！");
                //重新判断是否收费完成
                BindCompUnitValue();
                WatchToChargeData("addChargeListTable");
                AddOrEditCharge("add");
            } else {
                alert("添加项目收费失败！");
            }
        });
    } else if (flag == "edit") {
        var data = { "action": "edit", "projid": "" + projid + "", "paycount": "" + thisPayCount + "", "time": "" + time + "", "mark": "" + mark + "", "remitter": remitter, "curuser": curuser, "chgid": chgid };
        $.post(url, data, function (rlt) {
            if (rlt == "0") {
                alert("项目收费修改成功！");
                //重新判断是否收费完成
                BindCompUnitValue();
                WatchToChargeData("addChargeListTable");
                AddOrEditCharge("add");
            }
            else {
                alert("项目收费修改失败！");
            }
        });
    }
}
//绑定页面参数值-是否收费完成/承接部门
function BindCompUnitValue() {
   
    var projid = $("#hid_chgidNow").val();
   
    var allCount = $("#lbl_allcount").text();
    var data = "action=getAddEditCharge&proSysNo=" + projid + "&projallcount=" + allCount;
    $.ajax({
        type: "Post",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            $.each(result, function (i, n) {
                $("#hid_iscomplete").val(n.payComplete);
                var sscount = n.noPayCount;
                if (parseFloat(sscount) < 0) {
                    sscount = "0";
                }
               // $("#lbl_nopaycount").text(sscount); //未支付金额
                // $("#hide_noPayCount").val(n.noPayCount);

              //  $("#lbl_unit").text(n.pmusername); 
              //  $("#hide_cprunit").val(n.pmusername);

                 $("#lbl_pmname").text(n.pmusername); //承接负责人
                 $("#hide_cprpmname").val(n.pmusername);

            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!");
        }
    });
}
//编辑与添加转换初始化
function AddOrEditCharge(flag) {
    if (flag == "add") {
       // $("#addChargeInputTable tr:eq(1)").show();
      //  $("#addChargeInputTable tr:eq(3)").show();
      //  $("#addChargeInputTable tr:eq(5)").show();
       // $("#ft_add").find("legend").text("添加项目收费");
        $("#btn_returnCharge").hide();
        //清空
        $("#txt_payCount").val("");
       // $("#txt_remitter").val("");
        $("#txt_times").val("");
        $("#txt_chargeRemark").val("");
       // $("#txt_billcode").val("");
        //隐藏
        $("#txt_paycount_valide").hide();
        $("#txt_billcode_valide").hide();
        $("#txt_remitter_valide").hide();
        $("#txt_chargeRemark_valide").hide();
        $("#txt_time_valide").hide();
        //重新绑定事件
        $("#btn_addcount").unbind('click').click(function () {
            BtnAddEditCount("add");
        });
    } else if (flag == "edit") {
       // $("#addChargeInputTable tr:eq(1)").hide();
       // $("#addChargeInputTable tr:eq(3)").hide();
      //  $("#addChargeInputTable tr:eq(5)").hide();
     //   $("#ft_add").find("legend").text("修改项目收费");
        $("#btn_returnCharge").show();
        //隐藏
        $("#txt_paycount_valide").hide();
        $("#txt_billcode_valide").hide();
        $("#txt_remitter_valide").hide();
        $("#txt_chargeRemark_valide").hide();
        $("#txt_time_valide").hide();
        $("#btn_addcount").unbind('click').click(function () {
            BtnAddEditCount("edit");
        });
    }
}




//无数据提示
function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td style='background-color:#ffffff;' colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}
//转换为短日期格式
function GetShortDate(dateString) {
    if (dateString != "" && dateString != null) {
        var index = 10;
        if (dateString.lastIndexOf("-") > 0) {
            index = dateString.lastIndexOf("-") + 3;
        } else if (dateString.lastIndexOf("/") > 0) {
            index = dateString.lastIndexOf("/") + 3;
        }
        return dateString.substr(0, index);
    } else {
        return "无";
    }
}
//表格样式-各行变色-鼠标覆盖事件
function ControlTableCss(tableId) {
    $("#" + tableId + " tr:gt(0):odd").attr("class", "trOddColor");
    $("#" + tableId + " tr:gt(0)").hover(function () {
        $(this).addClass("mouseOverColor");
    }, function () {
        $(this).removeClass("mouseOverColor");
    });
}