﻿$(document).ready(function () {
    CommonControl.SetFormWidth();
    var chargeid = "0";
    var subid = "0";
    //修改保存
    $("#btn_save").click(function () {
        var txtcpr_No = $("#txtcpr_No").val();
        var txtcCprName = $("#txt_cprName").val();
        var txtCprUnit = $("#txt_cprBuildUnit").val();
        var drpBuildType = $("#drp_buildtype").val();
        var txt_cjbm = $("#txt_cjbm").val();
        var txtRegisterDate = $("#txtRegisterDate").val();
        var txtcpr_Remark = $("#txtcpr_Remark").val();
        var txtproFuze = $("#txt_proFuze").val();
        var txtcjbm = $("#txt_cjbm").val();
        var txtFParty = $("#txtFParty").val();
        var txtBuildArea = $("#txt_buildArea").val();
        var txtCompleteDate = $("#txtCompleteDate").val();
        //数字验证正则
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        var msg = "";
        //联系人
        if ($("#hid_cstid").val() == "") {
            msg += "请选择合同联系人！</br>";
        }
        //合同编号
        if (txtcpr_No == "") {
            msg += "合同编号不能为空！</br>";
        }
        //合同分类
        if ($("#ddcpr_Type").val() == "-1") {
            msg += "请选择合同分类！</br>";
        }
        //合同类型
        if ($("#txt_cprType").val() == "") {
            msg += "请选择合同类型！</br>";
        }
        //合同名称
        if (txtcCprName == "") {
            msg += "工程名称不能为空！</br>";
        }
        //建筑类别
        if (drpBuildType == "-1") {
            msg += "请选择建筑类别！</br>";
        }

        //建设单位
        if (txtCprUnit == "") {
            msg += "建设单位不能为空！</br>";
        }
        //建设规模
        if (txtBuildArea != "") {
            if (!reg_math.test(txtBuildArea)) {
                msg += "建设规模只能是数值！</br>";
            }
        }
        else {
            msg += "建设规模不能为空！";
        }
        //结构形式

        if (!IsStructCheckNode('struct')) {
            msg += "请选择结构形式！</br>";
        }
        //建筑分类
        if (!IsStructCheckNode('structtype')) {
            msg += "请选择建筑分类！</br>";
        }
        //楼层数判断
        if ($("#txt_upfloor").val() != "") {
            var reg = /^[1-9]\d*$/;
            if (!reg.test($("#txt_upfloor").val())) {
                msg += "输入地上层数格式错误，请输入数字！</br>";
            }
        }
        if ($("#txt_downfloor").val() != "") {
            var reg = /^[1-9]\d*$/;
            if (!reg.test($("#txt_downfloor").val())) {
                msg += "输入地下层数格式错误，请输入数字！</br>";
            }
        }
        if ($.trim(txtproFuze) == "") {
            msg += "请选择工程负责人！</br>";
        }
        //电话
        if ($("#txt_fzphone").val() != "") {
            var reg = /^(0|86|17951)?(13[0-9]|15[012356789]|18[01236789]|14[57])[0-9]{8}$/;
            if (!reg.test($("#txt_fzphone").val())) {
                msg += "手机号码输入不正确！<br/>";
            }
        }
        //承接部门
        if (txt_cjbm == "") {
            msg += "请选择承接部门！</br>";
        }
        //甲方负责人
        if ($.trim(txtFParty) == "") {
            msg += "请填写甲方负责人！</br>";
        }
        //电话
        if ($("#txt_jiafphone").val() != "") {
            var reg = /^(0|86|17951)?(13[0-9]|15[012356789]|18[01236789]|14[57])[0-9]{8}$/;
            if (!reg.test($("#txt_jiafphone").val())) {
                msg += "手机号码输入格式不正确！<br/>";
            }
        }
        //建设地点
        if ($.trim($("#txt_ProjectPosition").val()) == "") {
            msg += "请填写工程地点！</br>";
        }
        //合同金额
        if ($("#txtcpr_Account").val() == "") {
            msg += "合同额不能为空！</br>";
        }
        else {
            if (!reg_math.test($("#txtcpr_Account").val())) {
                msg += "合同额请输入数字！</br>";
            }
        }
        //投资额
        if ($("#txtInvestAccount").val() == "") {
            msg += "投资额不能为空！</br>";
        }
        else {
            if (!reg_math.test($("#txtInvestAccount").val())) {
                msg += "投资额请输入数字！</br>";
            }
        }
        if ($.trim($("#ddProfessionType").val()) == "") {
            msg += "请选择行业性质！</br>";
        }
        //实际合同
        if ($("#txtcpr_Account0").val() != "") {
            if (!reg_math.test($("#txtcpr_Account0").val())) {
                msg += "实际合同额请输入数字！</br>";
            }
        }
        else {
            $("#txtcpr_Account0").val("0");
        }
        //实际投资
        if ($("#txtInvestAccount0").val() != "") {
            if (!reg_math.test($("#txtInvestAccount0").val())) {
                msg += "实际投资请输入数字！</br>";
            }
        }
        else {
            $("#txtInvestAccount0").val("0");
        }
        if ($.trim($("#ddSourceWay").val()) == "") {
            msg += "请选择工程来源！</br>";
        }
        //合同阶段
        if ($(":checkbox[checked=checked]").length == 0) {
            msg += "请选择合同阶段！</br>";
        }


        //合同签订日期
        if ($("#txtSingnDate").val() == "") {
            msg += "合同签订日期不能为空！</br>";
        }
        if (txtcpr_Remark == "") {
            $("#txtcpr_Remark").val("");
        }
        if (txtCompleteDate == "") {
            msg += "完成日期不能为空！</br>";
        }
        if (msg != "") {
            jAlert(msg, "提示");
            return false;
        }
    });
    //绑定权限
    showDivDialogClass.UserRolePower = {
        "previewPower": $("#previewPower").val(),
        "userSysNum": $("#userSysNum").val(),
        "userUnitNum": $("#userUnitNum").val(),
        "notShowUnitList": ""
    };
    //表格变色
    $(".cls_show_cst_jiben>tbody>tr:odd").attr("style", "background-color:#FFF");
    //查询按钮
    $("#btn_search").click(function () {
        chooseCustomer.ClearQueryCondition();
        $("#chooseCustomerContainer").dialog({
            autoOpen: false,
            modal: true,
            width: 740,
            resizable: false,
            title: "查询客户"
        }).dialog("open");
    });
    //合同ID
    $("#hid_cprno").val(cpr_no);
    //合同类型
    $("#btn_cprType").click(function () {
        //加载数据-先赋值
        showDivDialogClass.SetParameters({
            "pageSize": "10"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "cprType", CproTypeCallBack);
        $("#chooseCustomerCompact").dialog({
            autoOpen: false,
            modal: true,
            width: 500,
            top: 100,
            resizable: false,
            title: "合同类型"
        }).dialog("open");
    });
    //工程负责人
    $("#btn_gcfz").click(function () {
        //先赋值
        showDivDialogClass.SetParameters({
            "prevPage": "gcfzr_prevPage",
            "firstPage": "gcfzr_firstPage",
            "nextPage": "gcfzr_nextPage",
            "lastPage": "gcfzr_lastPage",
            "gotoPage": "gcfzr_gotoPageIndex",
            "allDataCount": "gcfzr_allDataCount",
            "nowIndex": "gcfzr_nowPageIndex",
            "allPageCount": "gcfzr_allPageCount",
            "gotoIndex": "gcfzr_pageIndex",
            "pageSize": "10"
        });
        var unit_ID = $("#select_gcFzr_Unit").val();
        if (unit_ID < 0 || unit_ID == null) {
            //绑定工程负责部门
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "gcfzrCprUnit", CprTypeUnitCallBack);
        }
        $("#gcFzr_Dialog").dialog({
            autoOpen: false,
            modal: true,
            width: 460,
            height: 380,
            resizable: false,
            title: "工程负责人"
        }).dialog("open");
    });
    //承接部门
    $("#btn_cjbm").click(function () {

        //加载数据-先赋值
        showDivDialogClass.SetParameters({
            "pageSize": "10"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "1", "proCjbm", ProCjbmCallBack);
        BindAllDataCount(); //绑定总数据
        //}

        $("#cpr_cjbmDiv").dialog({
            autoOpen: false,
            modal: true,
            width: 450,
            height: 340,
            resizable: false,
            title: "承接部门"
        }).dialog("open");
    });
    //甲方负责人
    $("#btn_jffz").click(function () {

        showDivDialogClass.SetParameters({
            "prevPage": "jffzr_prevPage",
            "firstPage": "jffzr_firstPage",
            "nextPage": "jffzr_nextPage",
            "lastPage": "jffzr_lastPage",
            "gotoPage": "jffzr_gotoPageIndex",
            "allDataCount": "jffzr_allDataCount",
            "nowIndex": "jffzr_nowPageIndex",
            "allPageCount": "jffzr_allPageCount",
            "gotoIndex": "jffzr_pageIndex",
            "pageSize": "10"
        });
        var selectVal = $("#select_jffzrMem").val();
        if (selectVal == null || selectVal == undefined) {
            $("#noCustMsg").hide(); //先隐藏无数据提示消息
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "jffzrCop", JffzrUnitCallBack);
        }
        $("#jffzr_dialogDiv").dialog({
            autoOpen: false,
            modal: true,
            width: 560,
            height: 400,
            resizable: false,
            title: "甲方负责人"
        }).dialog("open");
    });
    //甲方搜索
    $("#btn_SearchByCusName").click(function () {
        var custName = $("#txt_custNameSearch").val();
        if (custName != null && custName != undefined) {
            custName = showDivDialogClass.ReplaceChars(custName);
            showDivDialogClass.SetParameters({
                "pageSize": "10"
            });
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", custName, "true", "0", "jffzrCop", JffzrUnitCallBack);
        }
    });
    //添加分项
    $("#btn_addSub").click(function () {
        //总面积
        var cprbuildArea = $("#txt_buildArea").val();
        subid = "0";
        //总合同额
        var cprAllcount = $("#txtcpr_Account").val();

        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#txtcpr_Account").val())) {
                alert("合同金额格式不正确！");
                return false;
            } else if (!reg.test(cprbuildArea)) {
                alert("建筑规模格式不正确！");
                return false;
            }
            else {
                $("#addsubDivDialog").dialog({
                    autoOpen: false,
                    modal: true,
                    width: 490,
                    top: 200,
                    height: "auto",
                    resizable: false,
                    title: "添加工程子项"
                }).dialog("open");
                //清空
                $("#addsubName").val("");
                $("#addsubArea").val("");
                $("#addsubJine").val("");
                $("#addsubHouseArea").val("");
                $("#addsubEachAreaMoney").val("");
                $("#areaTxtAddSubRemark").val("");
            }
        }
        else {
            alert("请填写合同额！");
        }
    });
    //修改分项
    $("span[id=update]").live("click", function () {
        //总面积
        var cprbuildArea = $("#txt_buildArea").val();
        //总合同额
        var cprAllcount = $("#txtcpr_Account").val();
        //子项id
        subid = $(this).attr("rel");

        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#txtcpr_Account").val())) {
                alert("合同金额格式不正确！");
                return false;
            } else if (!reg.test(cprbuildArea)) {
                alert("建筑规模格式不正确！");
                return false;
            }
            else {
                $("#addsubDivDialog").dialog({
                    autoOpen: false,
                    modal: true,
                    width: 490,
                    top: 200,
                    height: "auto",
                    resizable: false,
                    title: "编辑添加工程子项"
                }).dialog("open");
                //查询信息
                var trtd = $(this).parent().parent().find("TD");
                $("#addsubName").val(trtd.eq(1).text());
                $("#addsubArea").val(trtd.eq(2).text());
                $("#addsubJine").val(trtd.eq(3).text());
                $("#addsubHouseArea").val("");
                $("#addsubEachAreaMoney").val("");
                $("#areaTxtAddSubRemark").val(trtd.eq(4).text());

            }
        }
        else {
            alert("请填写合同额！");
        }
    });
    //工程子项-按钮事件
    $("#btn_addsubChild").click(function () {
        var addsubName = $("#addsubName").val();
        var addsubArea = $("#addsubArea").val();
        var addsubJine = $("#addsubJine").val();
        var areaTxtAddSubRemark = $("#areaTxtAddSubRemark").val();
        //总合同面积
        var sumOfArea = $("#txt_buildArea").val();
        //总合同额
        var sumOfMoney = $("#txtcpr_Account").val();
        var cstno = $("#hid_cprid").val();

        var subAllArea = 0; //已添加的总面积
        var subAllMoney = 0; //已添加的总金额
        var tableTrCount = $("#datas tr:gt(0)").length;
        for (var i = 1; i <= tableTrCount; i++) {
            if ($.trim($("#datas tr:eq(" + i + ")").find("td").find("span").attr("rel")) != subid) {
                var trArea = $("#datas tr:eq(" + i + ")").find("#sub_area").html();
                var trMoney = $("#datas tr:eq(" + i + ")").find("#sub_money").html();
                subAllArea += Math.abs(trArea);
                subAllMoney += Math.abs(trMoney);
            }
        }

        var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+)$/;
        var data = "action=cpraddsub&flag=edit&cstno=" + cstno + "&subid=" + subid;

        //判断输入是否合法
        //子项名称
        if (addsubName == "" || addsubName == null || addsubName == NaN) {
            $("#subNameNull").show();
            return false;
        } else {
            $("#subNameNull").hide();
            data += "&subname=" + addsubName;
        }
        //建筑面积
        if (addsubArea == "" || addsubArea == null || addsubArea == NaN) {
            $("#subAreaNull").show();
            $("#subAreaNoInt").hide();
            $("#subAreaErr").hide();
            return false;
        } else {
            $("#subAreaNull").hide();
            if (!reg.test(addsubArea)) {
                $("#subAreaNoInt").show();
                $("#subAreaErr").hide();
                return false;
            } else {
                $("#subAreaNoInt").hide();
            }
            if ((Math.abs(Math.abs(addsubArea) + Math.abs(subAllArea))) > Math.abs(sumOfArea)) {
                $("#subAreaErr").show();
                return false;
            } else {
                $("#subAreaErr").hide();
            }
            data += "&subarea=" + addsubArea;
        }
        //金额
        if (addsubJine == "" || addsubJine == null || addsubJine == NaN) {
            $("#subJineNull").show();
            $("#subJineErr").hide();
            $("#subJineNoInt").hide();
            return false;
        } else {
            $("#subJineNull").hide();
            if (!reg.test(addsubJine)) {
                $("#subJineNoInt").show();
                $("#subJineErr").hide();
                return false;
            } else {
                $("#subJineNoInt").hide();
            }
            if (Math.abs(Math.abs(addsubJine) + Math.abs(subAllMoney)) > Math.abs(sumOfMoney)) {
                $("#subJineErr").show();
                return false;
            } else {
                $("#subJineErr").hide();
            }
            data += "&money=" + addsubJine;
        }
        data += "&remark=" + areaTxtAddSubRemark;

        $.ajax({
            type: "POST",
            url: "../HttpHandler/CommHandler.ashx",
            data: data,
            success: function (result) {
                if (result == "yes") {
                    //alert("添加子项成功！");
                    $("#addsubDivDialog").dialog().dialog("close");
                    LoadSubCopration();
                    $("#txtcpr_Account").attr("readonly", "true");
                    $("#txt_buildArea").attr("readonly", "true");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误！");
            }
        });
    });
    //添加计划收费
    $("#btn_AddSf").click(function () {
        var cprAllcount = $("#txtcpr_Account").val();
        chargeid = "0";
        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#txtcpr_Account").val())) {
                alert("合同金额格式不正确！");
                return false;
            }
            else {

                $("#tjsfPlan_dialogDiv").dialog({
                    autoOpen: false,
                    modal: true,
                    width: 540,
                    top: 100,
                    resizable: false,
                    title: "添加计划收费信息"
                }).dialog("open");

                //每次初始化内容
                $("#txt_planChargeNum").val("");
                $("#txt_datePicker").val("");
                $("#txt_chargeRemark").val("");
                var cprAllcount = $("#txtcpr_Account").val(); //总合同金额
                $("#lbl_copMoney").text(cprAllcount);
                $("#chargeType_notselect").hide();
                $("#jine_notnull").hide();
                $("#jine_notint").hide();
                $("#jine_xiaoyu").hide();
                $("#date_notnull").hide();
            }
        }
        else {
            alert("请填写合同额！");
        }
    });
    //修改计划收费
    $("span[class=update]").live("click", function () {
        var cprAllcount = $("#txtcpr_Account").val();
        chargeid = $.trim($(this).attr("rel"));
        if (cprAllcount != "" || cprAllcount == "0") {
            var reg = /^(-|\+)?\d+(\.)?(\d)+$/;
            if (!reg.test($("#txtcpr_Account").val())) {
                alert("合同金额格式不正确！");
                return false;
            }
            else {

                $("#tjsfPlan_dialogDiv").dialog({
                    autoOpen: false,
                    modal: true,
                    width: 540,
                    top: 100,
                    resizable: false,
                    title: "编辑计划收费信息"
                }).dialog("open");

                //每次初始化内容
                var trtd = $(this).parent().parent().find("TD");
                $("#txt_planChargeNum").val(trtd.eq(2).text());
                $("#txt_datePicker").val(trtd.eq(3).text());
                $("#txt_chargeRemark").val(trtd.eq(4).text());
                var cprAllcount = $("#txtcpr_Account").val(); //总合同金额
                $("#lbl_copMoney").text(cprAllcount);
                $("#chargeType_notselect").hide();
                $("#jine_notnull").hide();
                $("#jine_notint").hide();
                $("#jine_xiaoyu").hide();
                $("#date_notnull").hide();
            }
        }
        else {
            alert("请填写合同额！");
        }
    });
    //添加计划收费信息-确定按钮
    $("#btn_addPlanCharge").click(function () {
        var tempId = $("#hid_cprid").val();
        var txtPlanChargeNum = $("#txt_planChargeNum").val();
        var lblCprMoney = $("#lbl_copMoney").text();
        var txtDatePic = $("#txt_datePicker").val();
        var txtSkr = $("#userShortName").val();
        var txtRemark = $("#txt_chargeRemark").val();
        var data = "action=addcprsk&flag=edit";
        //金额
        if (txtPlanChargeNum == "") {
            $("#jine_xiaoyu").hide();
            $("#jine_notint").hide();
            $("#jine_notnull").show();
            return false;
        } else {
            $("#jine_notnull").hide();
            var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0\.[0-9]+)$/;
            if (!reg.test(txtPlanChargeNum)) {
                $("#jine_xiaoyu").hide();
                $("#jine_notint").show();
                return false;
            } else {
                $("#jine_notint").hide();
            }
            //判断合同额
            if (parseFloat(lblCprMoney) < parseFloat(txtPlanChargeNum)) {
                $("#jine_xiaoyu").show();
                return false;
            } else {
                $("#jine_xiaoyu").hide();
            }
            data += "&jine=" + escape(txtPlanChargeNum);
        }
        //合同总金额
        data += "&allcount=" + escape($("#lbl_copMoney").text());
        //时间
        if (txtDatePic == "") {
            $("#date_notnull").show();
            return false;
        } else {
            $("#date_notnull").hide();
            data += "&date=" + escape($("#txt_datePicker").val());
        }
        //收款人
        if (txtSkr == "") {
            $("#skr_notnull").show();
            return false;
        } else {
            $("#skr_notnull").hide();
            data += "&skr=" + escape(txtSkr);
        }
        data += "&mark=" + escape(txtRemark) + "&cprid=" + tempId + "&chargeid=" + chargeid + "&payType=" + escape($("#chargeTypeSelect option:selected").text());
        //data = encodeURI(data);
        //添加收款信息
        $.ajax({
            type: "GET",
            url: "../HttpHandler/CommHandler.ashx",
            data: data,
            dataType: "text",
            success: function (result) {
                if (result == "yes") {
                    //alert("添加收款信息成功!");
                    loadCprCharge();
                    $("#tjsfPlan_dialogDiv").dialog().dialog("close");
                    $("#txtcpr_Account").attr("readonly", "readonly");
                }
                else {
                    alert("计划收款金额超过合同总额！合同余额：" + result + "万元。");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统错误！");
            }
        });
    });
    //加载计划
    loadCprCharge();
    //加载分项信息
    LoadSubCopration();
    //加载附件信息
    LoadCoperationAttach();

    //检查是否允许编辑
    var hasAudit = $("#HiddenHasAudit").val();
    if (hasAudit == "1") {
        alert("合同不可编辑，已在审核队列中或审核结束！");
        CommonControl.CreateTransparentDiv();
        window.history.back();
    }
    //选择合同编号
    $("#btn_getcprnum").click(function () {

        //赋值
        showDivDialogClass.SetParameters({
            "pageSize": "0"
        });
        showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", "0", "cprNumUnit", CprNumUnitCallBack);
        $("#cpr_Number").dialog({
            autoOpen: false,
            modal: true,
            width: 340,
            resizable: false,
            title: "选择合同编号"
        }).dialog("open");
    });

    var chooseCustomer = new ChooseCustomer($("#chooseCustomerContainer"), chooseCustomerCallBack);

    //只能选择控制
    $(".cls_input_text_onlyslt").focus(function () {
        $(this).blur();
    });
    var editFlag = $("#isCanEdit").val();
    if (editFlag == "1") {
        $("#btn_search").text("");
        $("#btn_cprType").text("");
        $("#btn_gcfz").text("");
        $("#btn_jffz").text("");
        $("#btn_cjbm").text("");
        $("#btn_AddSf").text("");
        $("#btn_addSub").text("");
        $("#btn_getcprnum").text("");
    }

    //结构样式
    CommonControl.BindASTreeView("asTreeviewStruct_ulASTreeView", $("#structstring").val(), $("#StrructContainer"));
    //建筑分类
    CommonControl.BindASTreeView("asTreeviewStructType_ulASTreeView", $("#structtypestring").val(), $("#buildTypeContainer"));

});

//选择客户回调
function chooseCustomerCallBack(recordObj) {
    $("#txtCst_No").val(recordObj.CustomerNo);
    $("#txtCst_Name").val(recordObj.CustomerName);
    $("#txtCpy_Address").val(recordObj.Address);
    $("#txtCode").val(recordObj.ZipCode == "0" ? "" : recordObj.ZipCode);
    $("#txtLinkman").val(recordObj.LinkMan);
    $("#txtCpy_Phone").val(recordObj.Phone);
    $("#txtCpy_Fax").val(recordObj.Fax);
    $("#hid_cstid").val(recordObj.SysNo);
    $("#txtCst_Brief").val(recordObj.CustomerShortName);
}
//加载计划收费信息
function loadCprCharge() {
    var data = "action=getplancharge&cstno=" + $("#hid_cprid").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var data = result == null ? "" : result.ds;
                if ($("#sf_datas tr").length > 1) {
                    $("#sf_datas tr:gt(0)").remove();
                }
                $.each(data, function (i, n) {
                    var row = $("#sf_row").clone();
                    var oper = "<span style=\"cursor: pointer;color:Blue\" class='update' rel='" + n.ID + "'>编辑</span>&nbsp;<span style=\"cursor: pointer;color:Blue\" rel='" + n.ID + "' class='del'>删除</span>";
                    row.find("#sf_id").text(i+1);
                    row.find("#sf_bfb").text(n.persent + "%");
                    row.find("#sf_edu").text(n.payCount);
                    row.find("#sf_time").html(n.paytime);
                    var markShow = n.mark;
                    if (n.mark.length > 20) {
                        markShow = markShow.substr(0, 20) + "...";
                    }
                    row.find("#sf_mark").attr("title", n.mark);
                    row.find("#sf_mark").html(markShow); //新增,备注
                    row.find("#sf_oper").html(oper);
                    //row.find("#sub_money").html(n.Money);
                    //row.find("#sub_remark").html(n.Remark);
                    row.find("#sf_oper span[class=del]").click(function () {
                        if (confirm("确定要删除此条计划收费吗？")) {
                            //删除事件
                            delChargeItem($(this));
                        }
                    });
                    //修改样式
                    row.addClass("cls_TableRow");
                    row.appendTo("#sf_datas");
                });

                data = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}
//删除计划收费
function delChargeItem(link) {
    var data = "action=delchargeitem&chrgid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        dataType: "text",
        data: data,
        success: function (result) {
            if (result == "yes") {
                //删除本行数据
                link.parents("tr:first").remove();

                if (($("#sf_datas tr").length <= 1) && ($("#datas tr").length <= 1)) {
                    $("#txtcpr_Account").removeAttr("readonly");
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {

        }
    });
}
//加载子项信息
function LoadSubCopration(flag) {
    var data = "action=selectsubcpr&cprid=" + $("#hid_cprid").val();
    if (flag == "edit") {
        data = "action=getsubitemdata&flag=edit&cstno=" + $("#hid_cprid").val();
    }
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "json",
        success: function (result) {
            if (result != null) {
                var rlt_data = result == null ? "" : result.ds;
                if ($("#datas tr").length > 1) {
                    $("#datas tr:gt(0)").remove();
                }
                $.each(rlt_data, function (i, n) {
                    var row = $("#sub_row").clone();
                    var oper = "<span id='update' style=\"cursor: pointer;color:Blue\" rel='" + n.ID + "'>编辑</span>&nbsp;<span style=\"cursor: pointer;color:Blue\" id='del' rel='" + n.ID + "'>删除</span>";
                    row.find("#sub_id").text(i + 1);
                    row.find("#sub_name").text(n.Item_Name);
                    row.find("#sub_area").text(n.Item_Area);
                    row.find("#sub_money").text(n.Money);
                    var cprRemark = n.Remark;
                    if (cprRemark.length > 20) {
                        cprRemark = cprRemark.substr(0, 20) + "...";
                    }
                    row.find("#sub_remark").attr("title", n.Remark);
                    row.find("#sub_remark").html(cprRemark);
                    row.find("#sub_oper").html(oper);
                    row.find("#sub_oper span[id=del]").click(function () {
                        if (confirm("确定要删除本条分项信息吗？")) {
                            //删除事件
                            delSubItem($(this));
                        }
                    });
                    //修改样式
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas");
                });
                rlt_data = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("加载分项信息失败！");
        }
    });
}
//加载附件信息
function LoadCoperationAttach() {
    var data_att = "action=selectattach&cprid=" + $("#hid_cprid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data_att,
        dataType: "json",
        success: function (result) {
            if (result != null) {
                var filedata = result == null ? "" : result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").remove();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    var oper = "<span  style=\"cursor: pointer;color:Blue\" rel='" + n.ID + "'>删除</span>";
                    var oper2 = "<a href=\"/Coperation/DownLoadFile.aspx?fileName=" + escape(n.FileName) + "&FileURL=" + escape(n.FileUrl) + "\" target='_blank'>下载</a>";
                    var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").html(img + n.FileName);
                    row.find("#att_filename").attr("align", "left");
                    row.find("#att_filesize").text(n.FileSizeString);
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper").html(oper);
                    row.find("#att_oper2").html(oper2);
                    row.find("#att_oper span").click(function () {
                        if (confirm("确定要删除本条附件吗？")) {
                            delCprAttach($(this));
                        }
                    });
                    //修改样式
                    row.addClass("cls_TableRow");
                    row.appendTo("#datas_att");
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        }
    });
}
//添加计划收费弹出窗口
function ShowDialogWin_CprCharge(url) {
    var feature = "dialogWidth:500px;dialogHeight:230px;center:yes";
    var result = window.showModalDialog(url, "", feature);
    if (result == 1) {
        loadCprCharge();
    }
}
//添加子项
function ShowDialogWin_CprSubItem(url) {
    var feature = "dialogWidth:470px;dialogHeight:230px;center:yes";
    var result = window.showModalDialog(url, "", feature);
    if (result == "1") {
        LoadSubCopration("edit");
    }
}
//删除子项
function delSubItem(link) {
    var data = "action=delsubitem&subid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        dataType: "text",
        data: data,
        success: function (result) {
            if (result == "yes") {
                link.parents("tr:first").remove();
                // alert("删除子项成功！");
                if (($("#datas tr").length <= 1)) {
                    $("#txtcpr_Account").removeAttr("readonly");
                    $("#txt_buildArea").removeAttr("readonly");
                }
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}
//加载数据
function GetAttachData() {
    LoadCoperationAttach("edit");
}
//删除附件
function delCprAttach(link) {
    var data = "action=delcprattach&attid=" + link.attr("rel");
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        dataType: "text",
        success: function (result) {
            if (result == "yes") {
                link.parents("tr:first").remove();
                alert("附件删除成功！");
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });
}
//数据长度
function DataLength(fData) {
    var intLength = 0;
    for (var i = 0; i < fData.length; i++) {
        if ((fData.charCodeAt(i) < 0) || (fData.charCodeAt(i) > 255))
            intLength = intLength + 2;
        else
            intLength = intLength + 1;
    }
    return intLength;
}
//查询客户信息
function ShowDialogWin(url) {
    var feature = "dialogWidth:700px;dialogHeight:350px;center:yes";
    var result = window.showModalDialog(url, "", feature);
    $("#txtCst_No").val(result[0]);
    $("#txtCst_Name").val(result[1]);
    $("#txtCpy_Address").val(result[2]);
    $("#txtCode").val(result[3]);
    $("#txtLinkman").val(result[4]);
    $("#txtCpy_Phone").val(result[5]);
    $("#txtCpy_Fax").val(result[6]);
    $("#hid_cstid").val(result[7]);
    $("#txtCst_Brief").val(result[8]);
}
//***************************************************************************************************
//By  fbw 20130917/18 修改
//合同类型绑定表格CallBack
function CproTypeCallBack(result) {
    $("#customerCompactTable tr:gt(0)").remove();
    if (result != null) {
        var data = result.ds;
        $.each(data, function (i, n) {
            var oper = "<span rel='" + n.ID + "' style=\"color:blue;cursor: pointer;\">选择</span>";
            var trHtml = "<tr><td>" + (i + 1) + "</td><td>" + n.dic_Name + "</td><td>" + oper + "</td></tr>";
            $("#customerCompactTable").append(trHtml);
            $("#customerCompactTable span:last").click(function () {
                $("#txt_cprType").val(n.dic_Name);
                $("#chooseCustomerCompact").dialog().dialog("close");
            });
        });
        ControlTableCss("customerCompactTable");
    }
}

//承接部门表格数据绑定-CallBack函数
function ProCjbmCallBack(result) {
    if (result != null) {
        var data = result.ds;
        $("#pro_cjbmTable tr:gt(0)").remove();
        $.each(data, function (i, n) {
            var oper = "<span rel='" + n.unit_ID + "' style=\"color:blue;cursor: pointer;\">选择</span>";
            var trHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.unit_Name + "</td><td>" + oper + "</td></tr>";
            $("#pro_cjbmTable").append(trHtml);
            $("#pro_cjbmTable span:last").click(function () {
                $("#txt_cjbm").val($.trim(n.unit_Name));
                $("#hid_cjbm").val($.trim(n.unit_Name));
                $("#cpr_cjbmDiv").dialog().dialog("close");
            });
        });
        ControlTableCss("pro_cjbmTable");
    }
}
//获得承接部门数据总数
function BindAllDataCount() {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "cjbm_prevPage",
        "firstPage": "cjbm_firstPage",
        "nextPage": "cjbm_nextPage",
        "lastPage": "cjbm_lastPage",
        "gotoPage": "cjbm_gotoPageIndex",
        "allDataCount": "cjbm_allDataCount",
        "nowIndex": "cjbm_nowPageIndex",
        "allPageCount": "cjbm_allPageCount",
        "gotoIndex": "cjbm_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", "", "false", "proCjbm", GetCjbmAllDataCount);
    //注册事件,先注销,再注册
    $("#cjbmByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#cjbm_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "false", pageIndex, "proCjbm", ProCjbmCallBack);
        }
    });
}
//承接部门数据总数CallBack函数
function GetCjbmAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        NoDataMessageOnTable("pro_cjbmTable", 3);
    }
}


//工程负责人绑定部门CallBack
function CprTypeUnitCallBack(result) {
    $("#gcFzr_MemTable tr:gt(0)").remove();
    BindAllDataCountGcfzr(-1); //初始化分页信息
    if (result != null) {
        var data = result.ds;
        var gcFzr_UnitOptionHtml = '<option value="-1">---------请选择---------</option>';
        $.each(data, function (i, n) {
            gcFzr_UnitOptionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
        });
        $("#select_gcFzr_Unit").html(gcFzr_UnitOptionHtml);
        //注册部门选项改变事件
        showDivDialogClass.SetParameters({ "pageSize": "10" });
        $("#select_gcFzr_Unit").unbind('change').change(function () {
            var unit_ID = $("#select_gcFzr_Unit").val();
            if (Math.abs(unit_ID) > 0) {
                showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", "1", "gcfzrCprMem", BindGcfzrDataCallBack);
                BindAllDataCountGcfzr(unit_ID);
            }
        });
    }
}
//绑定工程负责人表格数据CallBack
function BindGcfzrDataCallBack(result) {
    if (result != null) {
        var obj = result.ds;
        var gcFzrMemTableHtml;
        $("#gcFzr_MemTable tr:gt(0)").remove();
        $.each(obj, function (i, n) {
            var oper = "<span rel='" + n.mem_Login + "' style=\"color:blue; cursor:pointer;\">选择</span>";
            gcFzrMemTableHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.mem_Name + "</td><td>" + oper + "</td></tr>";
            $("#gcFzr_MemTable").append(gcFzrMemTableHtml);
            $("#gcFzr_MemTable span:last").click(function () {
                $("#txt_proFuze").val(n.mem_Name);
                $("#txt_fzphone").val(n.mem_Mobile);
            //增加项目经理ID qpl 20131225
            $("#HiddenPMUserID").val(n.mem_ID);
                $("#gcFzr_Dialog").dialog().dialog("close");
            });
        });
        ControlTableCss("gcFzr_MemTable");
    }
}
//绑定工程负责人数据总数
function BindAllDataCountGcfzr(unit_ID) {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "gcfzr_prevPage",
        "firstPage": "gcfzr_firstPage",
        "nextPage": "gcfzr_nextPage",
        "lastPage": "gcfzr_lastPage",
        "gotoPage": "gcfzr_gotoPageIndex",
        "allDataCount": "gcfzr_allDataCount",
        "nowIndex": "gcfzr_nowPageIndex",
        "allPageCount": "gcfzr_allPageCount",
        "gotoIndex": "gcfzr_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", unit_ID, "true", "gcfzrCprMem", GetGcfzrAllDataCount);
    //注册事件,先注销,再注册
    $("#gcFzr_ForPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#gcfzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", pageIndex, "gcfzrCprMem", BindGcfzrDataCallBack);
        }
    });
}
//工程负责人数据总数CallBack
function GetGcfzrAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#gcFzr_MemTable tr:gt(0)").remove();
        $("#gcfzr_allDataCount").text(0);
        $("#gcfzr_nowPageIndex").text(0);
        $("#gcfzr_allPageCount").text(0);
        NoDataMessageOnTable("gcFzr_MemTable", 3);
    }
}


//甲方负责人部门CallBack
function JffzrUnitCallBack(result) {
    $("#jffzr_dataTable tr:gt(0)").remove();
    BindAllDataCountJffzr(-1);
    if (result == null || result == undefined) {
        $("#select_jffzrMem").html('<option value="-1">---------------------请选择---------------------</option>');
        $("#jffzr_dataTable tr:gt(0)").remove();
        NoDataMessageOnTable("jffzr_dataTable", 6);
        return false;
    }
    var data = result.ds;
    var optionHtml = '<option value="-1">---------------------请选择---------------------</option>';
    //客户数量
    var data_count = 0;
    $.each(data, function (i, n) {
        optionHtml += '<option value="' + n.Cst_Id + '">' + n.Cst_Name + '</option>';
        data_count++;
    });
    $("#select_jffzrMem").html(optionHtml);
    $("#span_count").text(data_count + "项结果");
    //注册部门选项改变事件
    showDivDialogClass.SetParameters({ "pageSize": "10" });
    $("#select_jffzrMem").unbind('change').change(function () {
        var Cst_Id = $("#select_jffzrMem").val();
        if (Math.abs(Cst_Id) > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", Cst_Id, "true", "1", "jffzrMem", BindJffzrDataCallBack);
            BindAllDataCountJffzr(Cst_Id);
        }
    });
}
//绑定表格数据CallBack
function BindJffzrDataCallBack(result) {
    if (result != null) {
        var obj = result.ds;
        var jffzrTableHtml;
        $("#jffzr_dataTable tr:gt(0)").remove();
        $.each(obj, function (i, n) {
            var oper = "<span rel='" + n.Cst_Id + "' style=\"color:blue;cursor:pointer;\">选择</span>";
            jffzrTableHtml = "<tr style='text-align:center'><td>" + n.Cst_Id + "</td><td>"
        + n.Name + "</td><td>" + n.Duties + "</td><td>" + n.Department + "</td><td>" + n.Phone + "</td><td>" + oper + "</td></tr>";
            $("#jffzr_dataTable").append(jffzrTableHtml);
            $("#jffzr_dataTable span:last").click(function () {
                $("#txtFParty").val($.trim(n.Name));
                $("#txt_jiafphone").val($.trim(n.Phone));
                $("#jffzr_dialogDiv").dialog().dialog("close");
            });
        });
        ControlTableCss("jffzr_dataTable");
    }
}
//甲方负责人数据总数
function BindAllDataCountJffzr(Cst_Id) {
    //设置参数
    showDivDialogClass.SetParameters({
        "prevPage": "jffzr_prevPage",
        "firstPage": "jffzr_firstPage",
        "nextPage": "jffzr_nextPage",
        "lastPage": "jffzr_lastPage",
        "gotoPage": "jffzr_gotoPageIndex",
        "allDataCount": "jffzr_allDataCount",
        "nowIndex": "jffzr_nowPageIndex",
        "allPageCount": "jffzr_allPageCount",
        "gotoIndex": "jffzr_pageIndex",
        "pageSize": "10"
    });
    //获取总数据
    showDivDialogClass.GetDataTotalCount("getDataAllCount", Cst_Id, "true", "jffzrMem", GetJffzrAllDataCount);
    //注册事件,先注销,再注册
    $("#jffzrByPageDiv span").unbind('click').click(function () {
        var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
        if (isRegex) {
            var pageIndex = $("#jffzr_nowPageIndex").text();
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", Cst_Id, "true", pageIndex, "jffzrMem", BindJffzrDataCallBack);
        }
    });
}
//获取甲方负责人总数CallBack
function GetJffzrAllDataCount(result) {
    if (result > 0) {
        showDivDialogClass.BindPageValueFirst(result);
    } else {
        $("#jffzr_dataTable tr:gt(0)").remove();
        $("#jffzr_allDataCount").text(0);
        $("#jffzr_nowPageIndex").text(0);
        $("#jffzr_allPageCount").text(0);
        NoDataMessageOnTable("jffzr_dataTable", 6);
    }
}

//合同编号部门CallBack
function CprNumUnitCallBack(result) {
    if ($("#cpr_typeSelect option:selected").val() == null) {
        var data = result == null ? "" : result.ds;
        var cpr_typeSelect_htm = '<option value="-1">--------请选择--------</option>';
        $.each(data, function (i, n) {
            cpr_typeSelect_htm += '<option value="' + n.ID + '">' + n.CprType + '</option>';
        });
        $("#cpr_typeSelect").html(cpr_typeSelect_htm);
        $('#cpr_numSelect').empty();
    }
    //注册合同类型选项改变的事件
    $("#cpr_typeSelect").unbind('change').change(function () {
        $("#noselectMsg").hide();
        var CprTypeID = $(this).val();
        if (CprTypeID > 0) {
            showDivDialogClass.GetDataByAJAX("getDataToDivDialog", CprTypeID, "false", "0", "cprNumUnit", CprNumCallBack);
        } else {
            $('#cpr_numSelect').empty();
        }

    });
    //注册确定按钮点击事件
    $("#btn_cprNum_close").unbind('click').click(function () {
        var CprNum = $("#cpr_numSelect").find("option:selected").text();
        if (CprNum != NaN && CprNum != "" && CprNum != undefined) {
            $("#noselectMsg").hide();
            $("#txtcpr_No").val(CprNum);
            $("#hid_cprno").val(CprNum);           
            $("#cpr_Number").dialog().dialog("close");
        } else {
            $("#noselectMsg").show();
        }
    });
}
function CprNumCallBack(result) {
    var obj = result == null ? "" : result.ds;
    var cpr_TypeSelectChangeHtml;
    var prevTxt;
    $.each(obj, function (i, n) {
        var startNums = n.CprNumStart.split('-');
        var endNums = n.CprNumEnd.split('-');
        prevTxt = startNums[0] + "-";
        var startNum = Math.abs(GetCprNumFix(startNums[1])); //开始编号
        var endNum = Math.abs(GetCprNumFix(endNums[1])); //结束编号
        for (var i = startNum; i <= endNum; i++) {
            var cprNumOk = prevTxt + GetCprNumFix(i);
            cpr_TypeSelectChangeHtml += '<option cprNum="' + cprNumOk + '" value="' + GetCprNumFix(i) + '">' + cprNumOk + '</option>';
        }
    });
    $('#cpr_numSelect').empty();
    $('#cpr_numSelect').html(cpr_TypeSelectChangeHtml);
    //去除已使用的编号
    BindCprNumber(prevTxt);
}
//处理合同编号数字
function GetCprNumFix(cprNo) {
    if (cprNo < 10) {
        return "00" + cprNo;
    } else if (10 <= cprNo && cprNo < 100) {
        return "0" + cprNo;
    } else { return cprNo; }
}
//去除已使用的合同编号
function BindCprNumber(fixstr) {
    var data = "action=removeUsedCprNum&fixStr=" + fixstr;
    $.ajax({
        type: "Get",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var obj = result.ds;
                $.each(obj, function (i, n) {
                    $("#cpr_numSelect option[cprNum=" + n.cpr_No + "]").remove();
                });
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统错误!未能去除已使用的合同编号!");
        }
    });
}

//无数据提示
function NoDataMessageOnTable(tableID, tdCount) {
    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='" + tdCount + "'>无数据!</td></tr>";
    $("#" + tableID + "").append(trHtml);
}
//表格样式-各行变色-鼠标覆盖事件
function ControlTableCss(tableId) {
    $("#" + tableId + " tr:gt(0):odd").attr("class", "trOddColor");
    $("#" + tableId + " tr:gt(0)").hover(function () {
        $(this).addClass("mouseOverColor");
    }, function () {
        $(this).removeClass("mouseOverColor");
    });
}