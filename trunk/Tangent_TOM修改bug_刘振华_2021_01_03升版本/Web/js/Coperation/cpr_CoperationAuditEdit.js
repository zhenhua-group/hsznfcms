﻿var auditRecordLength = 2;
var messageDialog; //消息框
var sendMessageClass;  //发送消息共通方法
var jsonDataEntity;
//获取消息列表参数
var pageIndex;
var MessageType;
var TypePost;
var MessageAction;
var Aflag;
var MessageKeys;

$(function () {
    $(":radio[name=BuildNature][value=" + $("#ctl00_ContentPlaceHolder1_HiddenBuildNature").val() + "]").attr("checked", "checked");
    $(":radio[name=BuildType][value=" + $("#ctl00_ContentPlaceHolder1_HiddenCoperationType").val() + "]").attr("checked", "checked");
    $("input", $("#CoperationBaseInfo")).attr("disabled", true);
    // 设置文本框样式
    CommonControl.SetTextBoxStyle();
    CommonControl.SetFormWidth();

    //获取消息列表参数
    pageIndex = $("#pageIndex").val();
    MessageType = $("#MessageType").val();
    TypePost = $("#TypePost").val();
    MessageAction = $("#MessageAction").val();
    Aflag = $("#Aflag").val();
    MessageKeys = $("#MessageKeys").val();

    //当前合同审核记录
    var auditRecordStatus = $("#AuditRecordStatus").val();

    //检查用户权限
    var hasPower = TG.Web.Coperation.cpr_CoperationAuditEditBymaster.CheckPower(auditRecordStatus, $("#HiddenLoginUser").val(), $("#HiddenNeedLegalAdviser").val());

    //取得审核用户
    var auditUserArr = $("#HiddenAuditUser").val().split(",");
    //取得审核时间
    var auditDate = $("#HiddenAuditDate").val().split(",");

    //消息审核状态
    var messageStauts = $("#hiddenMessageStatus").val();

    //判断审批阶段显示审批表单
    if (messageStauts == auditRecordStatus || messageStauts == "") {
        InitViewState(auditRecordStatus, hasPower.value);
    } else {
        InitViewMessageState(messageStauts);
    }


    //取得所长意见
    var UndertakeProposalArr = $("#HiddenUndertakeProposal").val();
    if ($("#HiddenUndertakeProposal").val().length > 0) {
        $("#UndertakeProposal").val(UndertakeProposalArr)
        //填充审核人和审核时间
        $("#AuditUser", $("#undertakeDepartment")).html(auditUserArr[0] + ":<br/>" + auditDate[0]);

    }
    //取得经营部门意见
    var OperateDepartmentProposalArr = $("#HiddenOperateDepartmentProposal").val();
    if ($("#HiddenOperateDepartmentProposal").val().length > 0) {

        //审核意见不为空的场合，表示当前选中的checkbox是否。更改checkbox是和否的选中状态，并且填充值
        $("#OperateDepartmentProposal").val(OperateDepartmentProposalArr)
        $("#AuditUser", $("#OperateDepartment")).html(auditUserArr[1] + ":<br/>" + auditDate[1]);

    }
    //审核通过按钮
    $("#btnApproval").click(function () {
        messageDialog = $("#auditShow").messageDialog;
        sendMessageClass = new MessageCommon(messageDialog);
        var auditObj = {
            SysNo: $("#HiddenAuditRecordSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            Status: auditRecordStatus
        };
        //判断当前是在哪个状态
        switch (auditRecordStatus) {
            //新规状态所长                                           
            case "A":
                auditObj.UndertakeProposal = $("#UndertakeProposal").val();

                break;
                //所长通过                                  
            case "B":
                //B为部门负责人审核通过之后再次审核通过。结果为D
                auditObj.OperateDepartmentProposal = $("#OperateDepartmentProposal").val();
                break;
        }
        //经营处通过                                    
        var jsonObj = Global.toJSON(auditObj);
        jsonDataEntity = jsonObj;
        //合同最后完成阶段
        if (auditRecordStatus == "B") {
            if ($.trim(auditObj.OperateDepartmentProposal).length < 1) {
                alert("评审意见不能为空！");
                return false;
            }
            getUserAndUpdateAudit('1', '1', jsonDataEntity);
            return false;
        } //所管项目到技术部/需要法律顾问

        else {
            if ($.trim(auditObj.UndertakeProposal).length < 1) {
                alert("评审意见不能为空！");
                return false;
            }
            getUserAndUpdateAudit('1', '0', jsonDataEntity);
        }
    });

    //审核不通过按钮
    $("#btnRefuse").click(function () {
        var auditObj = {
            SysNo: $("#HiddenAuditRecordSysNo").val(),
            AuditUser: $("#HiddenLoginUser").val(),
            Status: auditRecordStatus
        };
        switch (auditRecordStatus) {
            //新规状态                                           
            case "A":
                auditObj.UndertakeProposal = $("#UndertakeProposal").val();
                break;
                //所长通过                                  
            case "B":
                //B为部门负责人审核通过之后再次审核通过。结果为D
                auditObj.OperateDepartmentProposal = $("#OperateDepartmentProposal").val();
                break;
        }
        if (auditRecordStatus == "A") {
            if ($.trim(auditObj.UndertakeProposal).length < 1) {
                alert("不通过意见不能为空！");
                return false;
            }
        }
        if (auditRecordStatus == "B") {
            if ($.trim(auditObj.OperateDepartmentProposal).length < 1) {
                alert("不通过意见不能为空！");
                return false;
            }
        }
        var jsonObj = Global.toJSON(auditObj);

        Global.SendRequest("/HttpHandler/CoperationAuditEditHandler.ashx", { "Action": 2, "data": jsonObj, "flag": 1 }, null, null, function (jsonResult) {
            if (jsonResult == "0") {
                alert("评审失败！");
            } else {
                if (jsonResult == "1") {

                    //改变消息状态
                    var msg = new MessageCommProjAllot($("#msgno").val());
                    msg.ReadMsg();
                    //消息
                    alert("评审不通过成功，消息已发送给申请人！");
                }
                //查询系统新消息
                window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx";
            }
        });
    });
    $("#btn_Send").click(function () {
        var _$mesUser = $(":checkbox[name=messageUser]:checked");

        if (_$mesUser.length == 0) {
            alert("请至少选择一个流程审批人！");
            return false;
        } else {
            getUserAndUpdateAudit('1', '1', jsonDataEntity);
        }
    });
    //返回重新审核按钮
    $("#FallBackCoperaion").click(function () {
        $.post("/HttpHandler/CoperationAuditEditHandler.ashx", { "Action": 3, "CoperationAuditSysNo": $("#HiddenAuditRecordSysNo").val() }, function (jsonResult) {
            //查询系统新消息
            CommonControl.GetSysMsgCount("/Coperation/cpr_CoperationAuditListEditBymaster.aspx");
        });
    });

    //实例化类容



    $("#exprot").click(function () {
        window.location.href = "ExportWord.aspx?coperationSysNo=" + $.trim($("#HiddenCoperationSysNo").val());
    });

});
//查询下一流程审批用户，action 0发起，1更新，flag 0查询，1更新
//显示用户列表发送消息更新流程
function getUserAndUpdateAudit(action, flag, jsonData) {
    //地址
    var url = "/HttpHandler/CoperationAuditEditHandler.ashx";
    //数据
    var data = { "Action": action, "flag": flag, "data": jsonData };
    //提交数据
    Global.SendRequest(url, data, null, null, function (jsonResult) {
        //改变消息状态
        if (flag == "1") {
            var msg = new MessageCommProjAllot($("#msgno").val());
            msg.ReadMsg();
        }

        if (jsonResult == "0") {
            alert("流程评审错误，请联系管理员！");
        }
        else if (jsonResult == "1") {
            alert("合同修改评审流程完成，已全部通过！");
            //查询系统新消息
            //window.history.back();
            window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
        }
        else {
            renderUserOrSendMsg(flag, jsonResult);
        }
    });
}
//显示用户OR更新审批状态
function renderUserOrSendMsg(flag, jsonResult) {
    var obj = eval("(" + jsonResult + ")");
    if (flag === "0") {
        sendMessageClass.render(obj.UserList, obj.RoleName);
    }
    else {
        sendMessageClass.setMsgTemplate(obj);
        sendMessageClass.chooseUserForMessage(sendMessage);
    }
}
//发送消息方法
function sendMessage(jsonResult) {
    if (jsonResult == "1") {

        alert("合同修改评审已成功！\n消息已成功发送到评审人等待评审！");
        //查询系统新消息
        //window.history.back();
        window.location.href = "/Coperation/cpr_SysMsgListViewBymaster.aspx?flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "&typepost=" + TypePost + "&messagekeys=" + MessageKeys;
    } else {
        alert("消息发送失败！");
    }
}

//审核不通过按钮和审核通过按钮不可用
function DisableButton() {
    $(":button[name=controlBtn]").attr("disabled", true);
}

//重新申请审批按钮
function AuditAgain() {
    $("#FallBackCoperaion").show();
}

//没有权限审核
function NoPowerAudit() {
    $("#NoPowerTable").show();
}

//初始化页面状态
function InitViewState(auditStatus, hasPower) {
    var length = 0;
    switch (auditStatus) {
        case "B":
            $("#UndertakeProposal").attr("disabled", "disabled");
            length = 2;
            if (hasPower == "0") {
                //没有权限的场合
                length--;
                NoPowerAudit();
                DisableButton();
            }
            break;
        case "C":
            $("#UndertakeProposal").attr("disabled", "disabled");
            length = 1;
            DisableButton();
            AuditAgain();
            break;
        case "D":
            $("#UndertakeProposal").attr("disabled", "disabled");
            $("#OperateDepartmentProposal").attr("disabled", "disabled");

            //需要法律顾问的场合
            length = 2;
            $("#HiddenLegalAdviserProposal").attr("disabled", "disabled");

            //NoPowerAudit();
            DisableButton();

            break;
        case "E":
            $("#UndertakeProposal").attr("disabled", "disabled");
            $("#OperateDepartmentProposal").attr("disabled", "disabled");
            length = 2;
            DisableButton();
            AuditAgain();
            break;
        case "F":
            length = 4;
            if (hasPower == "0") {
                length--;
                NoPowerAudit();
                DisableButton();
            }
            break;
        case "G":
            length = 3;
            DisableButton();
            AuditAgain();
            break;
        case "H":
            length = 5;
            if ($("#HiddenManageLevel").val() == "1") {
                length--;
                $("input", $("#MainDiv")).attr("disabled", true);
                $("#btnApproval").hide();
                $("#btnRefuse").hide();
                $("#exprot").attr("style", "display:inline-block;");
                $("#back").css("display", "inline-block");

            } else {
                if (hasPower == "0") {
                    length--;
                    NoPowerAudit();
                    DisableButton();
                }
            }
            break;
        case "I":
            length = 4;
            DisableButton();
            AuditAgain();
            break;
        case "J":
        case "K":
            length = 5;
            $("input", $("#MainDiv")).attr("disabled", true);
            $("#btnApproval").hide();
            $("#btnRefuse").hide();
            $("#exprot").attr("style", "display:inline-block;");
            $("#back").css("display", "inline-block");
            break;
    }
    $("#Button1").attr("disabled", false);
    showTable(length);
}

//初始化消息状态页面信息--审核人只能显示自己审核信息
function InitViewMessageState(messageStauts) {
    var length = 0;
    $("table:eq(0)", $("#TableContainer")).hide();
    if (messageStauts == "A") {
        length = 0;
        NoPowerAudit();
        $("#UndertakeProposal").attr("disabled", "disabled");
        DisableButton();
    }
    if (messageStauts == "B" || messageStauts == "C") {
        length = 1;
        NoPowerAudit();
        $("#OperateDepartmentProposal").attr("disabled", "disabled");
        DisableButton();
    }
    if (messageStauts == "D" || messageStauts == "E") {
        $("#UndertakeProposal").attr("disabled", "disabled");
        $("#OperateDepartmentProposal").attr("disabled", "disabled");
        if ($("#HiddenNeedLegalAdviser").val() == "0") {
            //不需要法律顾问的场合
            length = 2;
        } else {
            //需要法律顾问的场合
            length = 2;
            $("#HiddenLegalAdviserProposal").attr("disabled", "disabled");
        }
    }
    if (messageStauts == "F" || messageStauts == "G") {
        length = 2;
    }
    if (messageStauts == "H") {

        length = 2;
        if ($("#HiddenManageLevel").val() == "1") {
            length--;
            $("input", $("#MainDiv")).attr("disabled", true);
        }
    }
    if (messageStauts == "I") {
        length = 4;
    }
    if (messageStauts == "J" || messageStauts == "K") {
        length = 4;
        $("input", $("#MainDiv")).attr("disabled", true);
    }

    showMessageTable(length);
}
function showTable(length) {
    //按审批流程显示审批表格
    $("table:lt(" + length + ")", $("#TableContainer")).show();
    if ($("#HiddenNeedLegalAdviser").val() == "0") {
        $("#LegalAdviserDepartment").hide();
    }
}
function showMessageTable(length) {
    $("table:eq(" + length + ")", $("#TableContainer")).show();
    if ($("#HiddenNeedLegalAdviser").val() == "0") {
        $("#LegalAdviserDepartment").hide();
    }
}
