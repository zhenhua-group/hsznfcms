﻿var flag = false;
$(function () {
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Coperation/ProjectChargeHandler.ashx?strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()) + '&year=' + $("#ctl00_ContentPlaceHolder1_drp_year").val() + '&starttime=' + $("#ctl00_ContentPlaceHolder1_txt_start").val() + '&endtime=' + $("#ctl00_ContentPlaceHolder1_txt_end").val(),
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['序号', '', '', '', '', '合同编号', '合同名称', '子公司', '合同额(万元)', '实际合同额(万元)', '实收总额(万元)', '合同分类', '甲方类型', '建筑规模(㎡)', '合同文本编号', '建设单位', '结构形式', '建筑分类', '层数', '工程负责人', '工程负责人电话', '甲方负责人', '甲方负责人电话', '工程地点', '行业性质', '投资额(万元)', '实际投资额(万元)', '签订日期', '完成日期', '签约日期', '工程来源', '合同阶段', '制表人', '多栋楼', '合同备注', '录入人', '录入时间', '', '收费进度', '详细', '收费'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                             { name: 'allacount', index: 'allacount', hidden: true, editable: true },
                             { name: 'allssze', index: 'allssze', hidden: true, editable: true },
                             { name: 'allshijiacount', index: 'allshijiacount', hidden: true, editable: true },
                             { name: 'cpr_No', index: 'cpr_No', width: 120, align: 'center' },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 300, formatter: colNameShowFormatter },
                             { name: 'cpr_Unit', index: 'cpr_Unit', width: 100, align: 'center' },
                             { name: 'cpr_Acount', index: 'cpr_Acount', width: 80, align: 'center' },
                              { name: 'cpr_ShijiAcount', index: 'cpr_ShijiAcount', width: 100, align: 'center' },
                               { name: 'ssze', index: 'ssze', width: 100, align: 'center' },
                              { name: 'cpr_Type', index: 'cpr_Type', width: 90, align: 'center', hidden: true },
                             { name: 'BuildType', index: 'BuildType', width: 60, align: 'center', hidden: true },
                             { name: 'BuildArea', index: 'BuildArea', width: 80, align: 'center', hidden: true },
                              { name: 'cpr_Type2', index: 'cpr_Type2', width: 80, align: 'center', hidden: true },
                             { name: 'BuildUnit', index: 'BuildUnit', width: 80, align: 'center', hidden: true },
                             { name: 'StructType', index: 'StructType', width: 80, align: 'center', hidden: true },
                             { name: 'BuildStructType', index: 'BuildStructType', width: 80, align: 'center', hidden: true },
                             {
                                 name: 'Floor', index: 'Floor', width: 80, align: 'center', hidden: true, formatter: function colShowName(celvalue, options, rowData) {
                                     if (celvalue != null && celvalue != "null" && celvalue != "") {
                                         var arr = $.trim(celvalue).split("|");
                                         return "地上:" + $.trim(arr[0]) + " 地下:" + $.trim(arr[1]);
                                     }
                                     return "";
                                 }
                             },
                              {
                                  name: 'PMUserName', index: 'ChgPeople', width: 60, align: 'center', formatter: function colShowName(celvalue, options, rowData) {
                                      return $.trim(celvalue);
                                  }
                              },
                             { name: 'ChgPhone', index: 'ChgPhone', width: 80, align: 'center', hidden: true },
                             { name: 'ChgJia', index: 'ChgJia', width: 80, align: 'center', hidden: true },
                             { name: 'ChgJiaPhone', index: 'ChgJiaPhone', width: 80, align: 'center', hidden: true },
                             { name: 'BuildPosition', index: 'BuildPosition', width: 80, align: 'center', hidden: true },
                             { name: 'Industry', index: 'Industry', width: 80, align: 'center', hidden: true },                            
                             { name: 'cpr_Touzi', index: 'cpr_Touzi', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_ShijiTouzi', index: 'cpr_ShijiTouzi', width: 80, align: 'center', hidden: true },
                              { name: 'qdrq', index: 'cpr_SignDate2', width: 80, align: 'center', hidden: true },
                             { name: 'wcrq', index: 'cpr_DoneDate', width: 80, align: 'center', hidden: true },
                             { name: 'tjrq', index: 'cpr_SignDate', width: 80, align: 'center', hidden: true },
                             { name: 'BuildSrc', index: 'BuildSrc', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_Process', index: 'cpr_Process', width: 100, align: 'center', hidden: true, formatter: colProcess },
                             { name: 'TableMaker', index: 'TableMaker', width: 80, align: 'center', hidden: true },
                             { name: 'MultiBuild', index: 'MultiBuild', width: 100, align: 'center', hidden: true },
                             { name: 'cpr_Mark', index: 'cpr_Mark', width: 80, align: 'center', hidden: true },
                             { name: 'InsertUser', index: 'InsertUserID', width: 100, align: 'center', hidden: true },
                             { name: 'lrsj', index: 'InsertUserDate', width: 80, align: 'center', hidden: true },
                             { name: 'subcprname', index: 'subcprname', align: 'center', hidden: true },
                             { name: 'sfjd', index: 'sfjd', width: 150, align: 'center', formatter: colSfjdFormatter },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/ProjectChargeHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
                        return value;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var starttime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endtime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/ProjectChargeHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, 'starttime': starttime, 'endtime': endtime },
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //生产部门
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var starttime = $("#ctl00_ContentPlaceHolder1_txt_start").val();
        var endtime = $("#ctl00_ContentPlaceHolder1_txt_end").val();
        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/ProjectChargeHandler.ashx?action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, 'starttime': starttime, 'endtime': endtime },
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'

        }).trigger("reloadGrid");
    });

    //导出
    $("#ctl00_ContentPlaceHolder1_btn_export").click(function () {

        //显示列信息
        var colsName = "";
        var colsValue = "";
        //循环选中的字段列
        $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
            colsName = colsName + $.trim($(this).parent().parent().parent().text()) + ",";
            colsValue = colsValue + $(this).val() + ",";
        });
        if (colsName != "" && colsValue != "") {
            colsName = colsName.substr(0, (colsName.length - 1));
            colsValue = colsValue.substr(0, (colsValue.length - 1));
            $("#ctl00_ContentPlaceHolder1_hid_cols").val(colsName);
            $("#ctl00_ContentPlaceHolder1_hid_colsvalue").val(colsValue);
        }

    });


    //隐藏收费列
    //首页参数不显示收费
    var paramtype = $("#HiddenParamType").val();
    //是否生产经营部
    var isflag = $("#HiddenisFlag").val();

    if (paramtype == "homepage" && isflag == "false") {
        $("table[class='ui-jqgrid-htable'] tr>th").eq(10).text("");
        flag = true;
    }

    //点击全部基本合同
    $(":checkbox:first", $("#columnsid")).click(function () {

        //获得显示的列，保存到cookies
        var columnslist = "";
        var checkedstate = $(this).parent().attr("class");
        if (checkedstate != "checked")//代表选中
        {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(value).attr("checked", true);
                $("#jqGrid").showCol($(value).val());
                //保存cookies值
                columnslist = columnslist + $(value).val() + ",";
            });
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        else {
            $(":checkbox:not(:first)", $("#columnsid")).each(function (i, value) {
                var span = $(value).parent();
                span.removeClass("checked");
                $(value).attr("checked", false);
                $("#jqGrid").hideCol($(value).val());
            });
            columnslist = "";
        }
        //保存到cookies
        $.cookie("columnslist_charge", columnslist, { expires: 365, path: "/" });
        // $("#jqGrid").trigger('reloadGrid');
    });

    //点击客户字段
    $(":checkbox:not(:first)", $("#columnsid")).click(function () {

        var checkedstate = $(this).parent().attr("class");
        var columnsname = $(this).val();

        if (($(":checkbox:not(:first):checked", $("#columnsid")).length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }
        else {
            $(":checkbox:first", $("#columnsid")).parent().removeClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", false);
        }

        if (checkedstate != "checked")//代表选中
        {
            $("#jqGrid").showCol(columnsname);
        }
        else {
            $(this).attr("checked", false);
            $("#jqGrid").hideCol(columnsname);
        }

        //获得显示的列，保存到cookies
        var columnslist = "";
        $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
            columnslist = columnslist + $(this).val() + ",";
        });
        if (columnslist != "") {
            columnslist = columnslist.substr(0, (columnslist.length - 1));
        }
        //保存到cookies
        $.cookie("columnslist_charge", columnslist, { expires: 365, path: "/" });

    });

    //初始化显示cookies字段列
    InitColumnsCookies();

});
//合同阶段
function colProcess(celvalue, options, rowData) {
    var temp = "";
    var arr = $.trim(celvalue).split(",");
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == "27") {
            temp = "方案,";
        }
        if (arr[i] == "28") {
            temp += "初设,";
        }
        if (arr[i] == "29") {
            temp += "施工图,";
        }
        if (arr[i] == "30") {
            temp += "其他,";
        }
    }
    return temp;
}
//名称连接
function colNameShowFormatter(celvalue, options, rowData) {
    var pageurl = "cpr_ShowCoprationBymaster.aspx?flag=cprlist&cprid=" + rowData["cpr_Id"];
    return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

}

function colSfjdFormatter(celvalue, options, rowData) {
    return '<div class="progressbar" style="height:20px;margin:1px 1px;" title="' + celvalue + '%" rel="' + celvalue + '"></div>';
}

//查看
function colShowFormatter(celvalue, options, rowData) {
    return '<a href="#showCharge" rel="' + celvalue + '" class="cls_chk" data-cprname=\"' + rowData["cpr_Name"] + '\" data-cpracount=\"' + rowData["cpr_ShijiAcount"] + '\" data-cprpmname=\"' + rowData["PMUserName"] + '\" data-toggle=\"modal\" alt="查看收费">查看</a>';

}
//收费
function colEditFormatter(celvalue, options, rowdata) {
    var url = "";
    if (celvalue != "" && flag == false) {
        url = '<a href="#addCharge" rel="' + celvalue + '" class="cls_add" data-cprname=\"' + rowdata["cpr_Name"] + '\" data-cpracount=\"' + rowdata["cpr_ShijiAcount"] + '\" data-cprpmname=\"' + rowdata["PMUserName"] + '\" data-subcprname=\"' + rowdata["subcprname"] + '\" data-toggle=\"modal\"  alt="添加收费" class="allowEdit">收费</a>';
    }
    return url;
}
//统计 
function completeMethod() {

    //隐藏收费列
    //首页参数不显示收费
    var paramtype = $("#HiddenParamType").val();
    //是否生产经营部
    var isflag = $("#HiddenisFlag").val();

    var rowIds = $("#jqGrid").jqGrid('getDataIDs');
    var sumSfjd = 0, allaccount = 0, allssze = 0, allarea = 0, allshijiacount;
    for (var i = 0, j = rowIds.length; i < j; i++) {
        $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
        if (i == 0) {
            allaccount = $.trim($("#" + rowIds[i]).find("td").eq(3).text());
            allssze = $.trim($("#" + rowIds[i]).find("td").eq(4).text());
            allshijiacount = $.trim($("#" + rowIds[i]).find("td").eq(5).text());
        }
       // var sfjd = parseFloat($("#" + rowIds[i]).find("td").find(".progressbar").attr("rel"));
       // sumSfjd = sumSfjd + sfjd;
    }

   // var sumAcount = parseFloat($("#jqGrid").getCol("cpr_Acount", false, 'sum')).toFixed(4);
    //var sumSsze = parseFloat($("#jqGrid").getCol("ssze", false, 'sum')).toFixed(4);
    // sumSfjd = parseFloat(sumSfjd).toFixed(2);
    sumSfjd = parseFloat(allssze) / parseFloat(allshijiacount) * 100;
    var aa = "<div class='progressbar' title='" + parseFloat(sumSfjd).toFixed(2) + "%' rel='" + sumSfjd + "'></div>";
    $("#jqGrid").footerData('set', { cpr_No: "合计:", cpr_Acount: allaccount, ssze: allssze, cpr_ShijiAcount: allshijiacount, sfjd: aa }, false);

    //进度条
    $.each($(".progressbar"), function (index, item) {
        var percent = parseFloat($(item).attr("rel"));

        $(item).progressbar({
            value: percent
        });
    });
}
//无数据
function loadCompMethod() {
    $("#jqGrid").setGridParam("").hideCol("cb").trigger("reloadGrid");
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        if ($("#nodata").text() == '') {
            $("#jqGrid").parent().append("<div id='nodata'>没有查询到数据!</div>");
        } else { $("#nodata").show(); }
    }
    else {
        $("#nodata").hide();
    }
}
//初始化显示cookies字段列
var InitColumnsCookies = function () {
    //基本合同列表
    var columnslist = $.cookie("columnslist_charge");
    if (columnslist != null && columnslist != undefined && columnslist != "undefined" && columnslist != "") {
        var list = columnslist.split(",");
      //  $.each(list, function (i, c) {
        $(":checkbox:not(:first)[value]", $("#columnsid")).each(function (j, n) {
                if (list.indexOf($.trim($(n).val())) > -1) {
                var span = $(n).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(n).attr("checked", true);
                $("#jqGrid").showCol($(n).val());
                }
                else {
                    $("#jqGrid").hideCol($(n).val());
                }

            });

       // });
        //是否需要选中 全选框
        if ((list.length) == ($(":checkbox:not(:first)", $("#columnsid")).length)) {
            $(":checkbox:first", $("#columnsid")).parent().addClass("checked");
            $(":checkbox:first", $("#columnsid")).attr("checked", true);
        }

    }
    else {
        //cookie未保存，显示默认的几个字段
        var list = new Array("cpr_No", "cpr_Name", "cpr_Unit", "cpr_Acount", "ssze", "cpr_ShijiAcount", "PMUserName");//直接定义并初始化

        $.each(list, function (i, c) {

            $(":checkbox:not(:first)[value='" + c + "']", $("#columnsid")).each(function (j, n) {
                var span = $(n).parent();
                if (!span.hasClass("checked")) {
                    span.addClass("checked");
                }
                $(n).attr("checked", true);
                // $("#jqGrid").showCol($(n).val());
            });

        });
    }
}