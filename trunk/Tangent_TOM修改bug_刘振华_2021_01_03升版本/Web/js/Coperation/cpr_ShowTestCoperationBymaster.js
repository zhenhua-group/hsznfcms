﻿//加载合同分项和附件信息
$(document).ready(function () {

    //加载分项信息
    //var data = "action=getsubitemdatatype&subtype=supersub&cstno=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    //$.ajax({
    //    type: "GET",
    //    url: "../HttpHandler/CommHandler.ashx",
    //    data: data,
    //    dataType: "json",
    //    success: function (result) {
    //        if (result != null) {
    //            var rlt_data = result.ds;
    //            if (rlt_data != null) {
    //                if ($("#datas tr").length > 1) {
    //                    $("#datas tr:gt(0)").empty();
    //                }
    //                //总建筑面积
    //                var totalArea = 0;
    //                var totalMoney = 0;
    //                var totalLiving_Area = 0;

    //                $.each(rlt_data, function (i, n) {
    //                    var row = $("#sub_row").clone();
    //                    row.find("#sub_id").text(i + 1);
    //                    row.find("#sub_name").text(n.Item_Name);
    //                    //                row.find("#struct_sub").text(n.StructType);
    //                    //                row.find("#type_sub").text(n.BuildStructType);
    //                    row.find("#sub_area").text(n.Item_Area);
    //                    row.find("#Money").text(n.Money);
    //                    //                row.find("#sub_livearea").html(n.Living_Area);
    //                    //                row.find("#sub_moneyunit").html(n.Money_Unit);
    //                    row.find("#Remark").text(n.Remark);

    //                    totalArea = totalArea + parseFloat(n.Item_Area);
    //                    totalMoney = totalMoney + parseFloat(n.Money);
    //                    totalLiving_Area = totalLiving_Area + parseFloat(n.Living_Area);
    //                    row.addClass("cls_TableRow");
    //                    row.appendTo("#datas");
    //                });
    //            }

    //            //            //添加合计
    //            //            var totalTr = $("<tr></tr>");
    //            //            var totalTd0 = $("<td></td>");
    //            //            var totalTd1 = $("<td style=\"font-weight: bold;\" align=\"center\">合计</td>");
    //            //            var totalTd2 = $("<td></td><td></td>");
    //            //            var totalTd3 = $("<td style=\"font-weight: bold;\" align=\"center\">" + totalArea.toFixed(2) + "</td>");
    //            //            var totalTd4 = $("<td style=\"font-weight: bold;\" align=\"center\">" + totalMoney + "</td>");
    //            //            var totalTd5 = $("<td style=\"font-weight: bold;\" align=\"center\">" + totalLiving_Area + "</td>");
    //            //            var totalTd6 = $("<td></td><td></td>");
    //            //            totalTr.append(totalTd0);
    //            //            totalTr.append(totalTd1);
    //            //            totalTr.append(totalTd2);
    //            //            totalTr.append(totalTd3);
    //            //            totalTr.append(totalTd4);
    //            //            totalTr.append(totalTd5);
    //            //            totalTr.append(totalTd6);
    //            //            $("#datas").append(totalTr);

    //            rlt_data = "";
    //        }

    //    },
    //    error: function (XMLHttpRequest, textStatus, errorThrown) {
    //        alert("加载分项信息失败！");
    //    }
    //});
    //表格变色
    $(".table table-striped table-bordered table-hover>tr:odd").attr("style", "background-color:#FFF");
    //加载附件信息
    var data_att = "action=selecttattachtype&cpr_type=measurefile&cprid=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        url: "../HttpHandler/CommHandler.ashx",
        data: data_att,
        dataType: "json",
        success: function (result) {
            if (result != null) {
                var filedata = result.ds;
                if ($("#datas_att tr").length > 1) {
                    $("#datas_att tr:gt(0)").empty();
                }
                $.each(filedata, function (i, n) {
                    var row = $("#att_row").clone();
                    var oper = "<a href=\"/Coperation/DownLoadFile.aspx?fileName=" + escape(n.FileName) + "&FileURL=" + escape(n.FileUrl) + "\" target='_blank'>下载</a>";
                    var img = "<img style='width:16px;height:16px;' src='" + n.FileTypeImg + "'/>";
                    row.find("#att_id").text(n.ID);
                    row.find("#att_filename").attr("align", "left").html(img + n.FileName);
                    row.find("#att_filesize").text(n.FileSizeString);
                    row.find("#att_filetype").text(n.FileType);
                    row.find("#att_uptime").text(n.UploadTime);
                    row.find("#att_oper").html(oper);

                    row.addClass("cls_TableRow");
                    row.appendTo("#datas_att");
                });
                filedata = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("加载附件信息错误！");
        }
    });
    //加载计划
    var data = "action=getplanchargetypeAll&cpr_type=measurecharge&cstno=" + $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../HttpHandler/CommHandler.ashx",
        data: data,
        success: function (result) {
            if (result != null) {
                var data = result.ds;
                if ($("#sf_datas tr").length > 1) {
                    $("#sf_datas tr:gt(0)").remove();
                }
                $.each(data, function (i, n) {
                    var row = $("#sf_row").clone();

                    row.find("#sf_id").text(i + 1);
                    row.find("#sf_bfb").text(n.persent + "%");
                    row.find("#sf_edu").text(n.payCount);
                    //                    row.find("#sf_type").text(n.payType == null ? "" : n.payType); //新增,收费类型
                    row.find("#sf_time").html(n.paytime);
                    var markShow = n.mark;
                    if (n.mark.length > 20) {
                        markShow = markShow.substr(0, 20) + "...";
                    }
                    row.find("#sf_mark").attr("title", n.mark);
                    row.find("#sf_mark").html(markShow); //新增,备注

                    row.addClass("cls_TableRow"); //样式
                    row.appendTo("#sf_datas");

                });
                data = "";
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("系统错误！");
        }
    });

    //导出
   // var cprname = $("#ctl00_ContentPlaceHolder1_hid_cprid").val();
   // $("#btn_output").attr("href", "../HttpHandler/CustemerToWord.ashx?flag=cstshow&cprname=" + cprname + "");

    //CommonControl.SetFormWidth();
});