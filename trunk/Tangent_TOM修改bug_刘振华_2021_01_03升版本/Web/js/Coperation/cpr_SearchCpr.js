﻿$(document).ready(function () {

    CommonControl.SetFormWidth();
   
    //隔行变色
    $("#gv_Coperation tr:even").css({ "background-color": "White" });
    $(".cls_show_cst_jiben tr:even").css({ "background-color": "White" });
    //判断合同额输入是否正确
    $("#ImageButton1").click(function () {
        var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
        if ($("#txtcpr_Account").val() != "") {
            if (!reg_math.test($("#txtcpr_Account").val())) {
                alert("合同额请输入数字！");
                return false;
            }
        }
        if ($("#txtcpr_Account2").val() != "") {
            if (!reg_math.test($("#txtcpr_Account2").val())) {
                alert("合同额请输入数字！");
                return false;
            }
        }
        if ($("#txt_buildArea").val() != "") {
            if (!reg_math.test($("#txt_buildArea").val())) {
                alert("建筑规模请输入数字！");
                return false;
            }
        }
        if ($("#txt_buildArea2").val() != "") {
            if (!reg_math.test($("#txt_buildArea2").val())) {
                alert("建筑规模请输入数字！");
                return false;
            }
        }
    });

    //列头排序
    $("#sorttable").tablesort({
        schbtn: $("#ImageButton1"),
        hidclm: $("#hid_column"),
        hidord: $("#hid_order")
    });

    //输入关键字名称提示下来框
    var paramEntity = {};
    paramEntity.action = "Corperation";
    paramEntity.previewPower = $("#previewPower").val();
    paramEntity.userSysNum = $("#userSysNum").val();
    paramEntity.userUnitNum = $("#userUnitNum").val();
    paramEntity.unitID = $("#drp_unit").val();
    paramEntity.currYear ="0";
    var autoComplete = new AutoComplete(paramEntity, $("#txt_cprname"));
    autoComplete.GetAutoAJAX();

    //提示
    $(".cls_column").tipsy({ opacity: 1 });
});