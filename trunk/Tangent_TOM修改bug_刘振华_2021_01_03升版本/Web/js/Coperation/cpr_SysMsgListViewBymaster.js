﻿$(function () {
    //定义变量
    var MessageType = escape($("#ctl00_ContentPlaceHolder1_hiddenMessageType").val()); //判断消息类型
    var MessageDone = escape($("#ctl00_ContentPlaceHolder1_hiddenMessageDone").val()); //判断是否已办
    var cpruserSysNum = escape($("#ctl00_ContentPlaceHolder1_userSysNum").val()); //系统自动登录号
    var MessageIsDoneStatus = escape($("#ctl00_ContentPlaceHolder1_hiddenMessageIsDoneStatus").val()); //判断已办的状态
    var MessageIsRead = escape($("#ctl00_ContentPlaceHolder1_hiddenMessageIsRead").val()); //判断是否已读
    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Coperation/cpr_SysMsgListViewBymasterashx.ashx',
        datatype: 'json',
        height: "auto",
        rowNum: 25,
        rowList: [25, 30],
        colNames: ['消息状态', '', '', '','', '消息内容', '消息类型', '发送时间', '发送人', '操作'],
        colModel: [
                                 { name: 'Status', index: 'Status', width: 80, formatter: colISReadFormatter },
                                 { name: 'SysNo', index: 'SysNo', hidden: true, editable: true },
                                  { name: 'FromUser', index: 'FromUser', hidden: true, editable: true },
                                 { name: 'ReferenceSysNo', index: 'ReferenceSysNo', hidden: true, editable: true },
                                  { name: 'QueryCondition', index: 'QueryCondition', hidden: true, editable: true },
                                 { name: 'MessageContent', index: 'MessageContent', width: 600, formatter: colHrefFormatter },
                                 { name: 'MsgType', index: 'MsgType', width: 150, formatter: colTypeFormatter },
                                  { name: 'InDate', index: 'InDate', width: 100, formatter: 'date', formatoptions: { srcformat: 'Y-m-d', newformat: 'Y-m-d' } },
                                 { name: 'FromUserName', index: 'FromUserName', width: 80 },
                                 { name: 'cz', index: 'cz', width: 30, align: 'center', sorttable: false, editable: false, formatter: colHrefFormatter }

        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { 'cpruserSysNum': cpruserSysNum, 'MessageType': MessageType, 'MessageDone': MessageDone, 'MessageIsDoneStatus': MessageIsDoneStatus, 'MessageIsRead': MessageIsRead },
        loadonce: false,
        sortname: 'S.SysNo',
        sortorder: 'desc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/cpr_SysMsgListViewBymasterashx.ashx",

        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });

    //multiselect: true,
    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false,
        refreshtext: "刷新",
        deltext: "删除"
    },
                {//添加
                },
                {//编辑
                },
                {//删除            
                },
                {//搜索
                    top: 200,
                    left: 400
                }
                );
    //查询
    $("#btn_Search").click(function () {
        var name = escape($("#txtCoperationName").val());
        var type = escape($("#ctl00_ContentPlaceHolder1_MsgTypeDropDownList option:selected").val());

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/cpr_SysMsgListViewBymasterashx.ashx?action=sel",
            postData: { 'cpruserSysNum': cpruserSysNum, 'name': name, 'type': type },
            sortname: 'S.SysNo',
            sortorder: 'desc'
            //,page:1

        }).trigger("reloadGrid");
    });
    //未读消息
    $("#ButtonUnread").click(function () {
        var name = escape($("#txtCoperationName").val());
        var type = escape($("#ctl00_ContentPlaceHolder1_MsgTypeDropDownList option:selected").val());

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/cpr_SysMsgListViewBymasterashx.ashx?action=sel",
            postData: { 'cpruserSysNum': cpruserSysNum, 'name': name, 'type': type, 'MessageType': '', 'MessageDone': '', 'MessageIsDoneStatus': '', 'MessageIsRead': 'A' },
            sortname: 'S.SysNo',
            sortorder: 'desc'
            //,page:1

        }).trigger("reloadGrid");
    });


    //已读消息 
    $("#ButtonRead").click(function () {
        var name = escape($("#txtCoperationName").val());
        var type = escape($("#ctl00_ContentPlaceHolder1_MsgTypeDropDownList option:selected").val());

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/cpr_SysMsgListViewBymasterashx.ashx?action=sel",
            postData: { 'cpruserSysNum': cpruserSysNum, 'name': name, 'type': type, 'MessageType': '', 'MessageDone': '', 'MessageIsDoneStatus': '', 'MessageIsRead': 'D' },
            sortname: 'S.SysNo',
            sortorder: 'desc'
            //,page:1

        }).trigger("reloadGrid");
    });
    //未办事项
    $("#ButtonDone").click(function () {
        var name = escape($("#txtCoperationName").val());
        var type = escape($("#ctl00_ContentPlaceHolder1_MsgTypeDropDownList option:selected").val());

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/cpr_SysMsgListViewBymasterashx.ashx?action=sel",
            postData: { 'cpruserSysNum': cpruserSysNum, 'name': name, 'type': type, 'MessageType': '', 'MessageDone': 'done', 'MessageIsDoneStatus': 'A', 'MessageIsRead': '' },
            sortname: 'S.SysNo',
            sortorder: 'desc'
            //,page:1

        }).trigger("reloadGrid");
    });
    //已办事项
    $("#ButtonUnDone").click(function () {
        var name = escape($("#txtCoperationName").val());
        var type = escape($("#ctl00_ContentPlaceHolder1_MsgTypeDropDownList option:selected").val());

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/cpr_SysMsgListViewBymasterashx.ashx?action=sel",
            postData: { 'cpruserSysNum': cpruserSysNum, 'name': name, 'type': type, 'MessageType': '', 'MessageDone': 'done', 'MessageIsDoneStatus': 'D', 'MessageIsRead': '' },
            sortname: 'S.SysNo',
            sortorder: 'desc'
            //,page:1

        }).trigger("reloadGrid");
    });

    $("a[id=redirectLink]").live("click", function () {
      
        var msgSysNo = $(this).attr("msgsysno");
        var hrf = $(this).attr("href");
        //考勤删除审批记录
        if (hrf.indexOf("delapply") > -1) {
          
            $.get("/HttpHandler/Calendar/CalendarHandler.ashx", { "action":"SelApplyInfo","msgSysNo": msgSysNo }, function (result) {
                if (result!="") {
                    var datatable = eval('(' + result + ')');
                    var data = datatable.ds;
                    if (data != null && data != "")
                    {
                       
                        var applytype = data[0]["applytype"];
                        var applyID =data[0]["ID"];
                        var reason = data[0]["reason"]; 
                        var starttime = data[0]["starttime"];
                        var endtime = data[0]["endtime"];
                        var remark = data[0]["remark"];
                        var memname = data[0]["memname"];
                        var totaltime = data[0]["totaltime"];
                        var sSort = "";
                        switch (applytype)
                        {
                            case "travel":
                                sSort = "出差";
                                break;
                            case "gomeet":
                                sSort = "外勤";
                                break;
                            case "forget":
                                sSort = "未打卡";
                                break;
                            case "company":
                                sSort = "公司活动";
                                break;
                            case "depart":
                                sSort = "部门活动";
                                break;
                            case "addwork":
                                sSort = "加班离岗";
                                break;
                            default:
                                sSort = "请假";
                                break;
                        }
                        if (sSort == "未打卡") {

                            $("#kqtj", "#AuditDel").attr("style", "display:none");
                        }
                        else {
                            $("#kqtj", "#AuditDel").attr("style", "display:''");
                        }
                      
                        $("#applyid").val(applyID);                      
                        $("#applysort").text(sSort);
                        $("#reason").text(reason);
                        $("#applytime").text(starttime + " - " + endtime);

                        $("#applyname").text(memname);
                        $("#remark").text(remark);
                        $("#totaltime").text(totaltime);
                    }
                }
            });
        }
        var result = TG.Web.Coperation.cpr_SysMsgListViewBymaster.UpdateMsgStatus(msgSysNo);
        if (result != null && result.value != null && result.value.length > 0) {
            if (parseInt(result.value, 10) <= 0) {
                alert("修改消息状态失败，请联系系统管理员！");
            }
            else {
                $("#jqGrid").trigger("reloadGrid");
            }

        }
    })

    //按钮事件通过或不通过
    $("input[name='btn_save']").click(function () {
        var ispass = $(this).val();
        var msgid = $("#hidmsgID").val();
        var applyid = $("#hidapplyID").val();
        var url = "/HttpHandler/Calendar/CalendarHandler.ashx";
        $.get(url, { "action": "delete", "msgID": msgid, "applyID": applyid, "ispass": ispass }, function (result) {
            if (result == "1") {
                alert("已审批，消息已发送申请人！");
                $("#jqGrid").trigger("reloadGrid");
            }
            else {
                alert("消息操作失败！");
            }
            $("#btn_CanceAudit").click();
        });
    });
   
});



//查看合同
function colShowFormatter(celvalue, options, rowData) {
    if (celvalue != "") {
        var pageurl = "cpr_ShowCoprationBymaster.aspx?cprid=" + celvalue;
        return '<a href="' + pageurl + '" title="查看合同">查看</a>';
    }
}
//已读和未读
function colISReadFormatter(celvalue, options, rowdata) {
    if (celvalue != "") {
        if (celvalue == "A") {
            return "<img src=\"/Images/unread.gif\" align=\"center\"> 未读"
        } else {
            return "<img src=\"/Images/read.gif\" align=\"center\">已读"
        }
    }
}
//绑定链接
function colHrefFormatter(celvalue, options, rowData) {
    var MsgType = rowData["MsgType"];
    var SysNo = rowData["SysNo"];
    var ReferenceSysNo = rowData["ReferenceSysNo"];
    var FromUser = rowData["FromUser"];
    var redirectString = "";
    switch (MsgType) {
        //合同审核                              
        case 1:
            if (ReferenceSysNo.indexOf("&") > -1) {
                redirectString = "cpr_CoperationAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            }
            else {
                redirectString = "cpr_CoperationAuditBymaster.aspx?MsgNo=" + SysNo + "&CoperationAuditSysNo=" + ReferenceSysNo;
            }
            break;
            //项目审核                              
        case 2:
            if (ReferenceSysNo.indexOf("&") > -1) {
                redirectString = "../ProjectManage/ProjectAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            }
            else {
                redirectString = "../ProjectManage/ProjectAuditBymaster.aspx?MsgNo=" + SysNo + "&projectAuditSysNo=" + ReferenceSysNo;
            }
            break;
            //工号申请                              
        case 3:
            redirectString = "/ProjectNumber/ProNumberAllotBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
            //审核策划                              
        case 4:
            if (ReferenceSysNo.indexOf("&") > -1) {
                redirectString = "/ProjectPlan/ProPlanAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            }
            else {
                redirectString = "/ProjectPlan/ProPlanAuditBymaster.aspx?MsgNo=" + SysNo + "&projectPlanAuditSysNo=" + ReferenceSysNo;
            }
            break;
            //产值分配                              
        case 5:
            // xiugai
            if (ReferenceSysNo.indexOf("&") > -1) {
                redirectString = "/ProjectValueandAllot/ProValueAllotAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            }
            else {
                redirectString = "/ProjectValueandAllot/ProjectIsEditBymaster.aspx?ValueAllotAuditSysNo=" + ReferenceSysNo + "&MsgNo=" + SysNo;
            }
            break;
            //入账确认                              
        case 6:
            redirectString = "/Coperation/AccountedForConfirmation.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
            //项目收费入账确认                              
        case 7:
            redirectString = "/Coperation/ProjectAcountConfirmBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
            //项目完成立项发送消息给项目总负责                              
        case 8:
            redirectString = "../ProjectPlan/ProPlanListBymaster.aspx?" + "projectSysNo=" + ReferenceSysNo + "&PMUserID=" + FromUser + "&MsgNo=" + SysNo;
            break;
            //工程出图                              
        case 9:
            //  xiugai
            if (ReferenceSysNo.indexOf("&") > -1) {
                redirectString = "/ProjectManage/ProImageAudit/ProImageAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            }
            else {
                redirectString = "/ProjectManage/ProImageAudit/ProImageAudit.aspx?projectImaAuditSysNo=" + ReferenceSysNo + "&MsgNo=" + SysNo;
            }
            break;
      
        case 17:
            redirectString = "../ProjectValueandAllot/ShowMsgValueEditBymaster.aspx?" + ReferenceSysNo;
            break;
            //项目审核                              
        case 11:
            if (ReferenceSysNo.indexOf("&") > -1) {
                redirectString = "../ProjectManage/ProjectAuditEditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo + "&msyType=11";
            }
            else {
                redirectString = "../ProjectManage/ProjectAuditEditBymaster.aspx?msyType=11&MsgNo=" + SysNo + "&projectAuditSysNo=" + ReferenceSysNo;
            }
            break;
            //合同审核                              
        case 12:
            if (ReferenceSysNo.indexOf("&") > -1) {
                redirectString = "cpr_CoperationAuditEditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo + "&msyType=12";
            }
            else {
                redirectString = "cpr_CoperationAuditEditBymaster.aspx?msyType=12&MsgNo=" + SysNo + "&CoperationAuditSysNo=" + ReferenceSysNo;
            }
            break;
        case 13:
            if (ReferenceSysNo.indexOf("&") > -1) {
                redirectString = "/ProjectPlan/ProPlanAuditEditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo + "&msyType=13";
            }
            else {
                redirectString = "/ProjectPlan/ProPlanAuditEditBymaster.aspx?msyType=13&MsgNo=" + SysNo + "&projectPlanAuditSysNo=" + ReferenceSysNo;
            }
            break;
        case 14:
            // xiugai
            redirectString = "/ProjectValueandAllot/ProSecondValueAllotAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 15:
            // xiugai
            redirectString = "/ProjectValueandAllot/HavcProjectValueAllotAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 16:
            // xiugai
            redirectString = "/ProjectValueandAllot/TranjjsProjectValueAllotAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 18:
            // xiugai
            redirectString = "/ProjectValueandAllot/ShowjjsProjectValueAllotBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 19:
            // xiugai
            redirectString = "/ProjectValueandAllot/ShowTranProjectValueandAllotBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 20:
            // xiugai
            redirectString = "/ProjectValueandAllot/TranBulidingProjectValueAllotAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 21:
            // xiugai
            redirectString = "/ProjectManage/ProImageAudit/ProjectDesignCardsAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 22:
            // xiugai
            redirectString = "/ProjectManage/ProImageAudit/ProjectFileInfoAudit.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 23:
            // xiugai
            redirectString = "cpr_SuperCoperationAuditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 24:
            // xiugai
            redirectString = "cpr_SuperCoperationAuditEditBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 33:
            //修改 2016年5月9日
            redirectString = "/DeptBpm/UserJudge.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 34:
            //修改 2016年5月18日
            redirectString = "/ProjBpm/SetSpePersent.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
        case 35:
            //修改 2017年3月24日
            var url = "LeaveListByMaster.aspx";
            var applytype = ReferenceSysNo.substring((ReferenceSysNo.indexOf("="))+1, (ReferenceSysNo.indexOf("&")));
            if (applytype == "travel") {
                url = "TravelListByMaster.aspx";
            }
            else if (applytype == "gomeet")
            {
                url = "GoMeetListByMaster.aspx";
            }
            else if (applytype == "forget")
            {
                url = "ForgetTimeListByMaster.aspx";
            }
            else if (applytype == "company") {
                url = "CompanyTimeListByMaster.aspx";
            }
            else if (applytype == "depart") {
                url = "DepartTimeListByMaster.aspx";
            }
            else if (applytype == "addwork") {
                url = "AddworkTimeListByMaster.aspx";
            }
            redirectString = "/Calendar/" + url + "?" + ReferenceSysNo + "&AuditUserID=" + FromUser + "&MsgNo=" + SysNo;
            break;
        case 36:
            //考勤删除
            //管理员审批过
            if (ReferenceSysNo.indexOf("applyType") > -1)
            {            
                var url = "LeaveListByMaster.aspx";
                var applytype = ReferenceSysNo.substring((ReferenceSysNo.indexOf("=")) + 1, (ReferenceSysNo.indexOf("&")));
                if (applytype == "travel") {
                    url = "TravelListByMaster.aspx";
                }
                else if (applytype == "gomeet") {
                    url = "GoMeetListByMaster.aspx";
                }
                else if (applytype == "forget") {
                    url = "ForgetTimeListByMaster.aspx";
                }
                else if (applytype == "company") {
                    url = "CompanyTimeListByMaster.aspx";
                }
                else if (applytype == "depart") {
                    url = "DepartTimeListByMaster.aspx";
                }
                else if (applytype == "addwork") {
                    url = "AddworkTimeListByMaster.aspx";
                }
                redirectString = "/Calendar/" + url + "?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            }
            else
            {
                var QueryCondition = rowData["QueryCondition"];
                //管理员审批中
                redirectString = "javascript:delapply(" + ReferenceSysNo + "," + SysNo + ",'"+QueryCondition+"');";
            }
            
            break;
        case 100:
            // 合同额修改
            redirectString = $.trim(ReferenceSysNo) + "&MsgNo=" + SysNo; 
            break;
        case 101:
            // 合同删除
            redirectString = "javascript:void(0);";
            break;
        case 102:
            redirectString = "/Coperation/ProjectInvoiceConfirmBymaster.aspx?" + ReferenceSysNo + "&MsgNo=" + SysNo;
            break;
    }
    if (MsgType == 101 && $.trim(celvalue)=="查看")
    {
        return "";
    }
    else
    {
        return '<a href="' + redirectString + '"  id="redirectLink" msgsysno="' + SysNo + '" >' + celvalue + '</a>';
    }    
}
//绑定类型
function colTypeFormatter(celvalue, options, rowdata) {
    var msgTypeString = "";
    if (celvalue != "") {
        switch (celvalue) {

            case 1: msgTypeString = "合同审核"; break;
            case 2: msgTypeString = "项目审核"; break;
            case 3: msgTypeString = "工号申请"; break;
            case 4: msgTypeString = "策划审核"; break;          
            case 6: msgTypeString = "合同入账"; break;
            case 7: msgTypeString = "项目入账"; break;
            case 8: msgTypeString = "项目策划"; break;
            case 9: msgTypeString = "工程出图"; break;          
            case 11: msgTypeString = "项目修改审核"; break;
            case 12: msgTypeString = "合同修改审核"; break;
            case 13: msgTypeString = "策划修改审核"; break;  
            case 23: msgTypeString = "监理公司合同审核"; break;
            case 24: msgTypeString = "监理公司合同修改审核"; break;
            case 100: msgTypeString = "合同额修改"; break;
            case 101: msgTypeString = "合同删除消息"; break;
            case 102: msgTypeString = "合同开票"; break;
            case 35: msgTypeString = "考勤申请审批"; break;
            case 36: msgTypeString = "考勤申请删除"; break;
        }
    }
    return msgTypeString;
}
//统计 
function completeMethod() {

}
//无数据
function loadCompMethod() {
    var rowcount = parseInt($("#jqGrid").getGridParam("records"));
    if (rowcount <= 0) {
        $("#nodata").show();
    }
    else {
        $("#nodata").hide();
    }
}
//删除方法
function delapply(applyid, msgid, QueryCondition)
{
    $("#lbl_title").text(QueryCondition + "审批");
    $("#hidmsgID").val(msgid);
    $("#hidapplyID").val(applyid);
    $("#AuditDel").modal();

}