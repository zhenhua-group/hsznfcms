﻿
$(function () {


    var reg = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

    //确认通过
    $("#ctl00_ContentPlaceHolder1_sureButton").click(function () {

        //增值税税号
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_InvoiceCode").val()).length == 0) {
            alert("增值税税号不能为空！");
            return false;
        }
        //国税注册地
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_CountryAddress").val()).length == 0) {
            alert("国税注册地不能为空！");
            return false;
        }
      
        var reg = /(^((0[1,2]{1}\d{1}-?\d{8})|(0[3-9]{1}\d{2}-?\d{7,8}))$)|(^(0|86|17951)?(13[0-9]|15[0-35-9]|18[01236789]|14[57])[0-9]{8}$)/;
       
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_CountryPhone").val()).length==0) {
            alert("电话不能为空！");
            return false;
        }
        else {
            if (!reg.test($("#ctl00_ContentPlaceHolder1_txt_CountryPhone").val())) {
                alert("电话格式输入不正确！");
                return false;
            }
        }

        //开户银行
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_Bankaccount").val()).length==0) {
            alert("开户银行不能为空！");
            return false;
        }

        //账号信息
        if ($.trim($("#ctl00_ContentPlaceHolder1_txt_Accountinfo").val()).length == 0) {
            alert("账号信息不能为空！");
            return false;
        }

        if (!confirm("是否确认开票?")) {
            return false;
        }
    });

    //确认通过
    $("#ctl00_ContentPlaceHolder1_btn_NotSure").click(function () {

        if (!confirm("是否不通过开票?")) {
            return false;
        }
    });
});
