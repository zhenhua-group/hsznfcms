﻿var PlanningCorperation = function () {

    var tempRandom = Math.random() + new Date().getMilliseconds();

    $("#jqGrid10").jqGrid({
        url: '/HttpHandler/Coperation/PlanningCoperationListHandler.ashx?n=' + (Math.random() + new Date().getMilliseconds()) + '&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '', '', '', '', '', '', '', '', '合同编号', '工程名称', '合同分类', '建筑规模', '合同额(万元)', '承接部门', '签订日期', '完成日期', '统计年份', '工程负责人', '合同类型', '建设单位', '建筑类别', '结构形式', '建筑分类', '层数', '工程负责人电话', '甲方负责人', '甲方负责人电话', '工程地点', '行业性质', '实际合同额(万元)', '投资额(万元)', '实际投资额(万元)', '工程来源', '合同阶段', '制表人', '多栋楼', '合同备注', '客户名称', '录入人', '录入时间', '', ''],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', hidden: true, editable: true },
                             { name: 'sumarea', index: 'sumarea', hidden: true, editable: true },
                             { name: 'sumaccount', index: 'sumaccount', hidden: true, editable: true },
                             { name: 'sumsjaccount', index: 'sumsjaccount', hidden: true, editable: true },
                             { name: 'sumtzaccount', index: 'sumtzaccount', hidden: true, editable: true },
                             { name: 'sumsjtzaccount', index: 'sumsjtzaccount', hidden: true, editable: true },
                             { name: 'sumysaccount', index: 'sumysaccount', hidden: true, editable: true },
                             { name: 'BuildAreaunit', index: 'BuildAreaunit', hidden: true, editable: true },
                             { name: 'cpr_No', index: 'cpr_No', width: 80, align: 'center' },
                             { name: 'cpr_Name', index: 'cpr_Name', width: 200, formatter: colNameShowFormatter },
                             { name: 'cpr_Type', index: 'cpr_Type', width: 80, align: 'center' },
                             {
                                 name: 'BuildArea', index: 'BuildArea', width: 100, align: 'center', formatter: function colShowName(celvalue, options, rowData) {
                                     return $.trim(celvalue) + $.trim(rowData["BuildAreaunit"]);
                                 }
                             },
                             { name: 'cpr_Acount', index: 'cpr_Acount', width: 80, align: 'center' },
                             { name: 'cpr_Unit', index: 'cpr_Unit', width: 70, align: 'center' },
                             { name: 'qdrq', index: 'cpr_SignDate2', width: 80, align: 'center' },
                             { name: 'wcrq', index: 'cpr_DoneDate', width: 80, align: 'center' },
                             { name: 'tjrq', index: 'cpr_SignDate', width: 80, align: 'center' },
                             {
                                 name: 'PMUserName', index: 'ChgPeople', width: 60, align: 'center', formatter: function colShowName(celvalue, options, rowData) {
                                     return $.trim(celvalue);
                                 }
                             },
                              { name: 'cpr_Type2', index: 'cpr_Type2', width: 80, align: 'center', hidden: true },
                             { name: 'BuildUnit', index: 'BuildUnit', width: 80, align: 'center', hidden: true },
                              { name: 'BuildType', index: 'BuildType', width: 60, align: 'center', hidden: true, editable: true },
                             { name: 'StructType', index: 'StructType', width: 80, align: 'center', hidden: true },
                             { name: 'BuildStructType', index: 'BuildStructType', width: 80, align: 'center', hidden: true },
                             {
                                 name: 'Floor', index: 'Floor', width: 80, align: 'center', hidden: true, formatter: function colShowName(celvalue, options, rowData) {
                                     if (celvalue != null && celvalue != "null" && celvalue != "") {
                                         var arr = $.trim(celvalue).split("|");
                                         return "地上:" + $.trim(arr[0]) + " 地下:" + $.trim(arr[1]);
                                     }
                                     return "";
                                 }
                             },
                             { name: 'ChgPhone', index: 'ChgPhone', width: 80, align: 'center', hidden: true },
                             { name: 'ChgJia', index: 'ChgJia', width: 80, align: 'center', hidden: true },
                             { name: 'ChgJiaPhone', index: 'ChgJiaPhone', width: 80, align: 'center', hidden: true },
                             { name: 'BuildPosition', index: 'BuildPosition', width: 80, align: 'center', hidden: true },
                             { name: 'Industry', index: 'Industry', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_ShijiAcount', index: 'cpr_ShijiAcount', width: 100, align: 'center', hidden: true },
                             { name: 'cpr_Touzi', index: 'cpr_Touzi', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_ShijiTouzi', index: 'cpr_ShijiTouzi', width: 80, align: 'center', hidden: true },
                             { name: 'BuildSrc', index: 'BuildSrc', width: 80, align: 'center', hidden: true },
                             { name: 'cpr_Process', index: 'cpr_Process', width: 100, align: 'center', hidden: true, formatter: colProcess },
                             { name: 'TableMaker', index: 'TableMaker', width: 80, align: 'center', hidden: true },
                             { name: 'MultiBuild', index: 'MultiBuild', width: 100, align: 'center', hidden: true },
                             { name: 'cpr_Mark', index: 'cpr_Mark', width: 80, align: 'center', hidden: true },
                              //{ name: 'ssze', index: 'ssze', width: 80, align: 'center' },
                              { name: 'cst_name', index: 'cst_name', width: 80, hidden: true, align: 'center' },
                             { name: 'InsertUser', index: 'InsertUserID', width: 100, align: 'center', hidden: true },
                             { name: 'lrsj', index: 'InsertDate', width: 70, align: 'center' },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter },
                             { name: 'cpr_Id', index: 'cpr_Id', width: 30, align: 'center', sorttable: false, editable: false, formatter: colEditFormatter }
        ],
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        loadonce: false,
        sortname: 'cpr_Id',
        sortorder: 'desc',
        pager: "#gridpager10",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Coperation/PlanningCoperationListHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid10").jqGrid("navGrid", "#gridpager10", {
        add: false,
        edit: false,
        del: true,
        search: false,
        deltext: "删除"
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid10").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {
                        var sel_id = $('#jqGrid10').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid10').jqGrid('getCell', sel_id[i], 'cpr_Id') + ",";
                            }
                        }
                        //var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        // var value = $('#jqGrid').jqGrid('getCell', sel_id, 'cpr_Id');
                        return values;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );



    //查询
    $("#btn_basesearch").click(function () {
        ClearjqGridParam("jqGrid10");
        //隐藏高级查询       
        //$("#div_cbx9").hide();
        //$("#div_sch9").hide();


        if ($("#ctl00_ContentPlaceHolder1_txt_account1").val() != "" && !reg_math.test($("#ctl00_ContentPlaceHolder1_txt_account1").val())) {
            $("#ctl00_ContentPlaceHolder1_txt_account1").val("");
        }
        if ($("#ctl00_ContentPlaceHolder1_txt_account2").val() != "" && !reg_math.test($("#ctl00_ContentPlaceHolder1_txt_account2").val())) {
            $("#ctl00_ContentPlaceHolder1_txt_account2").val("");
        }


        //设置值为0基本查询
        $("#hid_cxtype").val("0");

        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        // var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").find("option:selected").text();
        var unit = getCheckedUnitNodes();
        var year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
        var keyname = $("#ctl00_ContentPlaceHolder1_txt_keyname").val();
        var startTime = $("#ctl00_ContentPlaceHolder1_txt_startdate").val();
        var endTime = $("#ctl00_ContentPlaceHolder1_txt_enddate").val();
        var cprtype = $.trim($("#ctl00_ContentPlaceHolder1_drp_type").find("option:selected").text());
        var buildunit = $("#ctl00_ContentPlaceHolder1_txt_cprbuildunit").val();
        var account1 = $("#ctl00_ContentPlaceHolder1_txt_account1").val();
        var account2 = $("#ctl00_ContentPlaceHolder1_txt_account2").val();

        $(".norecords").hide();
        $("#jqGrid10").jqGrid('setGridParam', {
            url: "/HttpHandler/Coperation/PlanningCoperationListHandler.ashx?n='" + tempRandom + " '&action=sel",
            postData: { 'strwhere': strwhere, 'unit': unit, 'keyname': keyname, 'year': year, "startTime": startTime, "endTime": endTime, "cprtype": cprtype, "buildunit": buildunit, "account1": account1, "account2": account2 },
            page: 1,
            sortname: 'cpr_Id',
            sortorder: 'desc'
        }).trigger("reloadGrid");
    });


    //导出
    $("#ctl00_ContentPlaceHolder1_btn_export").click(function () {

        //显示列信息
        var colsName = "";
        var colsValue = "";
        //循环选中的字段列
        $(":checkbox:not(:first):checked", $("#columnsid")).each(function () {
            colsName = colsName + $.trim($(this).parent().parent().parent().text()) + ",";
            colsValue = colsValue + $(this).val() + ",";
        });
        if (colsName != "" && colsValue != "") {
            colsName = colsName.substr(0, (colsName.length - 1));
            colsValue = colsValue.substr(0, (colsValue.length - 1));
            $("#ctl00_ContentPlaceHolder1_hid_cols").val(colsName);
            $("#ctl00_ContentPlaceHolder1_hid_colsvalue").val(colsValue);
        }

        //高级导出
        if ($("#hid_cxtype").val() == "1") {

            //导出方法
            SearchExport();
            return false;
        }

    });

    //名称连接
    function colNameShowFormatter(celvalue, options, rowData) {
        var pageurl = "cpr_ShowPlanningCoperationBymaster.aspx?flag=cprlist&cprid=" + rowData["cpr_Id"];
        return '<a href="' + pageurl + '" alt="查看合同">' + celvalue + '</a>';

    }

    //合同阶段
    function colProcess(celvalue, options, rowData) {
        var temp = "";
        var arr = $.trim(celvalue).split(",");
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == "27") {
                temp = "方案,";
            }
            if (arr[i] == "28") {
                temp += "初设,";
            }
            if (arr[i] == "29") {
                temp += "施工图,";
            }
            if (arr[i] == "30") {
                temp += "其他,";
            }
        }
        return temp;
    }

    //查看
    function colShowFormatter(celvalue, options, rowData) {
        var pageurl = "cpr_ShowPlanningCoperationBymaster.aspx?flag=cprlist&cprid=" + celvalue;
        return '<a href="' + pageurl + '" alt="查看合同">查看</a>';

    }
    //编辑
    function colEditFormatter(celvalue, options, rowdata) {
        if (celvalue != "") {
            var pageurl = "cpr_ModifyPlanningCoperationBymaster.aspx?cprid=" + celvalue;
            return '<a href="' + pageurl + '" alt="编辑合同" class="allowEdit">编辑</a>';
        } //class="allowEdit"
    }
    //统计 
    function completeMethod() {
        var sumarea = 0, sumaccount = 0, sumsjaccount = 0, sumtzaccount = 0, sumsjtzaccount = 0, sumyfaccount = 0;
        var rowIds = $("#jqGrid10").jqGrid('getDataIDs');
        for (var i = 0, j = rowIds.length; i < j; i++) {
            $("#jqGrid10 tr[id=" + rowIds[i] + "]").children("td").eq(1).text((i + 1));
            if (i == 0) {
                sumarea = $.trim($("#jqGrid10 tr[id=" + rowIds[i] + "]").children("td").eq(3).text());
                sumaccount = $.trim($("#jqGrid10 tr[id=" + rowIds[i] + "]").children("td").eq(4).text());
                sumsjaccount = $.trim($("#jqGrid10 tr[id=" + rowIds[i] + "]").children("td").eq(5).text());
                sumtzaccount = $.trim($("#jqGrid10 tr[id=" + rowIds[i] + "]").children("td").eq(6).text());
                sumsjtzaccount = $.trim($("#jqGrid10 tr[id=" + rowIds[i] + "]").children("td").eq(7).text());
                sumyfaccount = $.trim($("#jqGrid10 tr[id=" + rowIds[i] + "]").children("td").eq(8).text());
            }
            //$("#" + rowIds[i]).find("td").eq(1).text((i + 1));
        }
        $("#jqGrid10").footerData('set', { cpr_No: "合计:", cpr_Acount: sumaccount, cpr_ShijiAcount: sumsjaccount, cpr_Touzi: sumtzaccount, cpr_ShijiTouzi: sumsjtzaccount, ssze: sumyfaccount });
    }
    //无数据
    function loadCompMethod() {
        var rowcount = parseInt($("#jqGrid10").getGridParam("records"));
        if (rowcount <= 0) {
            if ($("#nodata10").text() == '') {
                $("#jqGrid10").parent().append("<div id='nodata10'>没有查询到数据!</div>");
            }
            else { $("#nodata10").show(); }

        }
        else {
            $("#nodata10").hide();
        }
    }

}
