﻿
$(function () {
 
    //显示 需要审批记录
    $("#drp_audit").click(function () {
        $("#btn_search").click();
    });

    //选择部门显示人员
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var unitid = $(this).val();
        if (unitid!="-1") {
            var pp = $("#ctl00_ContentPlaceHolder1_previewPower").val();

            var url = "/HttpHandler/Calendar/CalendarHandler.ashx";
            var data = { "action": "seluser", "unitid": unitid, "year": $("#ctl00_ContentPlaceHolder1_drp_year").val(), "previewPower": pp, "userid": $("#ctl00_ContentPlaceHolder1_userSysNum").val() };
            $.get(url, data, function (result) {
                var dt = eval('(' + result + ')');
                var ds = dt != null ? dt.ds : null;
                if (ds != null && dt!=null) {                  
                    $("#ctl00_ContentPlaceHolder1_drp_user option:gt(0)").remove();
                    $.each(ds, function (i, k) {
                        var option = $("<option value='" + k["mem_ID"] + "'>" + k["mem_Name"] + "</option>");
                        $("#ctl00_ContentPlaceHolder1_drp_user").append(option);
                    });
                    
                }
                
            });
        }
        else 
        {
            $("#ctl00_ContentPlaceHolder1_drp_user option:gt(0)").remove();
        }

    });
    //同意事件
    $(":button[action='ApplyAgree']").live("click", function () {
        var $tr = $(this).parent().parent();
        var managerusers = $tr.children("td[aria-describedby='jqGrid_managerusers']").text();
        var auditsysno = $tr.children("td[aria-describedby='jqGrid_AuditSysNo']").text();
        var auditstatus = $tr.children("td[aria-describedby='jqGrid_AuditStatus']").text();
        var generalmanager = $tr.children("td[aria-describedby='jqGrid_generalmanager']").text();
        var managerid = "";
        //审批两个流程,第一个审批流程
        if (generalmanager != "" && auditstatus == "A") {
            managerid = managerusers;
        }

        var url = "/HttpHandler/Calendar/CalendarHandler.ashx";
        var data = { "action": "audit", "auditsysno": auditsysno, "auditstatus": auditstatus, "applybtn": "Agree", "managerid": managerid };
        $.get(url, data, function (result) {
            if (result == "1") {
                alert("审批通过，消息已发送给申请人！");
                $("#jqGrid").trigger("reloadGrid");
            }
            else if (result == "2") {
                alert("消息发送失败！");
            }
            else if (result == "3") {
                alert("审批通过，消息已发送给总经理！");
                $("#jqGrid").trigger("reloadGrid");
            }
            else {
                alert("审批更新失败！");
            }
        });
    });

    //不同意事件
    $(":button[action='ApplyDisagree']").live("click", function () {
        var $tr = $(this).parent().parent();

        var auditsysno = $tr.children("td[aria-describedby='jqGrid_AuditSysNo']").text();
        var auditstatus = $tr.children("td[aria-describedby='jqGrid_AuditStatus']").text();
        var generalmanager = $tr.children("td[aria-describedby='jqGrid_generalmanager']").text();

        var url = "/HttpHandler/Calendar/CalendarHandler.ashx";
        var data = { "action": "audit", "auditsysno": auditsysno, "auditstatus": auditstatus, "applybtn": "ApplyDisagree" };
        $.get(url, data, function (result) {
            if (result == "1") {
                alert("审批不通过，消息已发送给申请人！");
                $("#jqGrid").trigger("reloadGrid");
            }
            else if (result == "2") {
                alert("消息发送失败！");
            }
            else {
                alert("审批更新失败！");
            }
        });
    });
    //重新发起
    $("a[rel]").live("click", function () {
        //特殊人的审批人
        var SpecialPersonAuditID = $("#ctl00_ContentPlaceHolder1_SpecialPersonAuditID").val();
        var SpecialPerson = $("#ctl00_ContentPlaceHolder1_SpecialPerson").val();

        var $tr = $(this).parent().parent();
        var applyid = $(this).attr("rel");
        var unitusers = $tr.children("td[aria-describedby='jqGrid_unitusers']").text();
        var username = $tr.children("td[aria-describedby='jqGrid_username']").text() + ",";

        var strSpecialPerson = SpecialPerson;
        strSpecialPerson = strSpecialPerson.replace(/|/, ",") + ",";
        //判断是否特殊人,重新赋值特殊人的审批人
        if (strSpecialPerson.indexOf(username) > -1) {
            unitusers = SpecialPersonAuditID;
        }
        var url = "/HttpHandler/Calendar/CalendarHandler.ashx";
        $.get(url, { "action": "restart", "applyid": applyid, "unitid": unitusers }, function (result) {
            if (result == "1") {
                alert("发起审批成功，消息已发送部门经理！");
                $("#jqGrid").trigger("reloadGrid");
            }
            else if (result == "2") {
                alert("消息发送失败！");
            }
            else {
                alert("发起审批失败！");
            }
        });
    });

    //退回
    $("#btn_return").live("click",function () {
        if (confirm("确定退回？"))
        {
            var auditsysno = $(this).attr("AuditSysNo");
            var url = "/HttpHandler/Calendar/CalendarHandler.ashx";
            var data = { "action": "auditreturn", "auditsysno": auditsysno };
            $.get(url, data, function (result) {
                if (result == "1") {
                    alert("申请退回需重新填，消息已发送给申请人！");
                    $("#jqGrid").trigger("reloadGrid");
                }
                else if (result == "2") {
                    alert("消息发送失败！");
                }
            });
        }
       
    });

    //批量审批ID
    var auditsysnolist = "";//审批ID
    var manageridlist = "";//总经理id
    var nextprocesslist = "";//是否需要下一步审批。
    var usersysno = $("#ctl00_ContentPlaceHolder1_userSysNum").val();
    //部门经理批量审批
    $("#btn_unitaudit").click(function () {

      //特殊人的审批人
        var SpecialPersonAuditID = $("#ctl00_ContentPlaceHolder1_SpecialPersonAuditID").val();
        var SpecialPerson = $("#ctl00_ContentPlaceHolder1_SpecialPerson").val();
        var SpecialPersonList = SpecialPerson.split("|");
        var SpecialPersonAuditIDList = SpecialPersonAuditID.split(",");

        var strSpecialPerson = SpecialPerson;
        strSpecialPerson = strSpecialPerson.replace(/|/, ",") + ",";

        var selrowlist = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
        var len = selrowlist.length;
        if (len == 0) {
            alert("请选择申请记录！");
            return false;
        }
        else {
            auditsysnolist = "";
            manageridlist = "";
            nextprocesslist = "";
            //循环显示选中的行
            for (i = 0; i < len; i++) {
                var status = $('#jqGrid').jqGrid('getCell', selrowlist[i], 'AuditStatus');
                var sysno = $('#jqGrid').jqGrid('getCell', selrowlist[i], 'unitusers');
                var applyusername = $('#jqGrid').jqGrid('getCell', selrowlist[i], 'username');
                //如果是特殊人
                if (strSpecialPerson.indexOf(applyusername + ",") > -1)
                {                   
                    var ind = 0;
                    for (var k = 0; k < SpecialPersonList.length; k++) {
                        if (SpecialPersonList[k].indexOf(applyusername) > -1) {
                            ind = k;
                            break;
                        }
                    } 
                    //审批人的重新赋值特殊人审批
                    sysno = SpecialPersonAuditIDList[ind];
                }

                if (status == "A" && sysno == usersysno) {
                    auditsysnolist = auditsysnolist + $('#jqGrid').jqGrid('getCell', selrowlist[i], 'AuditSysNo') + ",";
                    manageridlist = manageridlist + $('#jqGrid').jqGrid('getCell', selrowlist[i], 'managerusers') + ",";
                    nextprocesslist = nextprocesslist + $('#jqGrid').jqGrid('getCell', selrowlist[i], 'generalmanager') + ",";
                }
               
                
                
            }
            if (auditsysnolist=="") {
                alert("请选择部门经理审批的申请记录！");
                return false;
            }
            $("#lbl_title").text("部门经理批量审批");
            $("#AuditList").modal();
            $("#listType").val("unit");
        }

    });
    //总经理批量审批
    $("#btn_manageraudit").click(function () {
        var selrowlist = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
        var len = selrowlist.length;
        if (len == 0) {
            alert("请选择申请记录！");
            return false;
        }
        else {
            auditsysnolist = "";
            manageridlist = "";
            nextprocesslist = "";
            for (i = 0; i < len; i++) {
                var status = $('#jqGrid').jqGrid('getCell', selrowlist[i], 'AuditStatus');
                var sysno = $('#jqGrid').jqGrid('getCell', selrowlist[i], 'managerusers');
                var generalmanager = $('#jqGrid').jqGrid('getCell', selrowlist[i], 'generalmanager');
                if (status == "B" && sysno == usersysno && generalmanager!="") {
                    auditsysnolist = auditsysnolist + $('#jqGrid').jqGrid('getCell', selrowlist[i], 'AuditSysNo') + ",";
                }
                
            }
            if (auditsysnolist == "") {
                alert("请选择总经理审批的申请记录！");
                return false;
            }
            $("#lbl_title").text("总经理批量审批");
            $("#AuditList").modal();
            $("#listType").val("manager");
        }

    });
    //批量通过
    $("#btn_yes").click(function () {

        var audittype = $("#listType").val();
        var auditstatus = ""; //审批状态
       
        //总经理审批
        if (audittype == "manager") {
            auditstatus = "B";          
        }
        else { //部门经理审批
            auditstatus = "A";
           
        }
        auditsysnolist = auditsysnolist == "" ? "" : auditsysnolist.substr(0, (auditsysnolist.length - 1));
        manageridlist = manageridlist == "" ? "" : manageridlist.substr(0, (manageridlist.length - 1));
        nextprocesslist = nextprocesslist == "" ? "" : nextprocesslist.substr(0, (nextprocesslist.length - 1));
       

        var url = "/HttpHandler/Calendar/CalendarHandler.ashx";
        var data = { "action": "auditlist", "auditsysnolist": auditsysnolist, "auditstatus": auditstatus, "applybtn": "Agree", "manageridlist": manageridlist, "nextprocesslist": nextprocesslist };
        $.get(url, data, function (result) {
            if (result == "1") {
                alert("批量审批通过，消息已经发送给申请人或下一步审批人！");
                $("#jqGrid").trigger("reloadGrid");
            }            
            else {
                alert("批量审批失败！");
            }
        });
        $("#btn_CanceAudit").click();
    });
    //批量不通过
    $("#btn_no").click(function () {

        var audittype = $("#listType").val();
        var auditstatus = ""; //审批状态

        //总经理审批
        if (audittype == "manager") {
            auditstatus = "B";
        }
        else { //部门经理审批
            auditstatus = "A";

        }
        auditsysnolist = auditsysnolist == "" ? "" : auditsysnolist.substr(0, (auditsysnolist.length - 1));
      
        var url = "/HttpHandler/Calendar/CalendarHandler.ashx";
        var data = { "action": "auditlist", "auditsysnolist": auditsysnolist, "auditstatus": auditstatus, "applybtn": "ApplyDisagree"};
        $.get(url, data, function (result) {
            if (result == "1") {
                alert("批量审批不通过，消息已经发送给申请人！");
                $("#jqGrid").trigger("reloadGrid");
            }
            else {
                alert("批量审批失败！");
            }
        });
        $("#btn_CanceAudit").click();
    });

    //已处理
    $("#ctl00_ContentPlaceHolder1_btn_done").click(function () {
        var selrowlist = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
        var len = selrowlist.length;
        if (len == 0) {
            alert("请选择申请记录！");
            return false;
        }
        else {
            var idlist = "";
            var noaudit = 0;
            //循环显示选中的行
            for (i = 0; i < len; i++) {
                var id = $('#jqGrid').jqGrid('getCell', selrowlist[i], 'ID');
                var AuditStatus = $('#jqGrid').jqGrid('getCell', selrowlist[i], 'AuditStatus');              
                var hours = $('#jqGrid').jqGrid('getCell', selrowlist[i], 'totaltime'); 
                var days = parseFloat(hours) / 7.5;
                var SpecialPerson = $("#ctl00_ContentPlaceHolder1_SpecialPerson").val();
                var strSpecialPerson = SpecialPerson;
                strSpecialPerson = strSpecialPerson.replace(/|/, ",") + ",";

                var applyusername = $('#jqGrid').jqGrid('getCell', selrowlist[i], 'username');
                //申请时间超过5天，并且不是特殊人员 状态“B”
                if (AuditStatus == "A" || (AuditStatus == "B" && days >= 5 && strSpecialPerson.indexOf(applyusername+",") < 0)) {
                    noaudit++;
                    break;
                }
                idlist = idlist + id+",";
            }
            if (noaudit>0) {
                alert("未审批完申请,不能处理！");
                return false;
            }

            idlist = idlist.substr(0, (idlist.length - 1));
            var url = "/HttpHandler/Calendar/CalendarHandler.ashx";
            var data = { "action": "done", "idlist": idlist};
            $.get(url, data, function (result) {
                if (result == "1") {
                    alert("批量已处理完成！");
                    $("#jqGrid").trigger("reloadGrid");
                    $("#jqGrid2").trigger("reloadGrid");
                }
                else {
                    alert("批量已处理失败！");
                }
            });
        }

    });
});