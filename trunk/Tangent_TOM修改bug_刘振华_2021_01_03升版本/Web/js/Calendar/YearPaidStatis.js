﻿
$(function () {
    var username = $("#ctl00_ContentPlaceHolder1_HiddenUserName").val();
    var drp_year = $("#ctl00_ContentPlaceHolder1_drp_year").val();
    var drp_month = $("#ctl00_ContentPlaceHolder1_drp_month").val();
    var next_year = parseInt(drp_year);
    var next_month = (parseInt(drp_month) - 1);
    if (drp_month == "1") {
        next_year = (parseInt(drp_year) - 1);
        next_month = 12;
    }
    $("#title").html(next_year + "." + next_month + ".16 - " + drp_year + "." + drp_month + ".15");

    //管理员不用看到最后三列
    var userlogin = $("#ctl00_ContentPlaceHolder1_userShortName").val();
    if (userlogin == "admin" || $("#ctl00_ContentPlaceHolder1_isapplymanager").val() == "True") {
        //     $("#tbl tr td[rel='no']").css("display", "none");

    }
    else {
        //不是管理员不用看到最后统计一行
       // $("#tbl tr:last").css("display", "none");
    }

    //选择部门显示人员
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var unitid = $(this).val();
        if (unitid != "-1") {
            var pp = $("#ctl00_ContentPlaceHolder1_previewPower").val();

            var url = "/HttpHandler/Calendar/CalendarHandler.ashx";
            var data = { "action": "seluser", "year": next_year, "month": next_month, "unitid": unitid, "previewPower": pp, "userid": $("#ctl00_ContentPlaceHolder1_userSysNum").val() };
            $.get(url, data, function (result) {
                var dt = eval('(' + result + ')');
                var ds = dt != null ? dt.ds : null;
                if (ds != null && dt != null) {
                    $("#ctl00_ContentPlaceHolder1_drp_user option:gt(0)").remove();
                    // $("#ctl00_ContentPlaceHolder1_drp_user").append("<option value='-1'>---全部---</option>");
                    $.each(ds, function (i, k) {
                        var option = $("<option value='" + k["mem_ID"] + "'>" + k["mem_Name"] + "</option>");
                        $("#ctl00_ContentPlaceHolder1_drp_user").append(option);
                    });

                }

            });
        }
        else {
            $("#ctl00_ContentPlaceHolder1_drp_user option:gt(0)").remove();
        }

    });
    //选择用户
    $("#ctl00_ContentPlaceHolder1_drp_user").change(function () {
        if ($(this).val() == "-1") {
            $("#ctl00_ContentPlaceHolder1_hid_userid").val("-1");
        }
        else {
            $("#ctl00_ContentPlaceHolder1_hid_userid").val($(this).val());
        }

    });

    //只要考勤管理员才可以修改数据
    if ($("#ctl00_ContentPlaceHolder1_isapplymanager").val() == "True") {
        $("#tbl_btn").css("display", "");
        $("#btn_lock").show();
        //添加文本框,只触发一次，或者移除绑定事件
        $("#tbl>tbody>tr>td[rel]:not(td[style*='green'])").bind("dblclick", function () {
            //var $td0 = $(this).parent().parent().find("tr:eq(0)>td:eq(" + $(this).index() + ")");
            //  var col_year = $td0.attr("year"); //年
            //   var col_month = $td0.attr("month"); //月
            //   var col_day = $td0.text(); //日
            var zhi = $(this).attr("value"); //
            $(this).html("<span class='input-group'><input type='text' memid='" + $(this).attr("rel") + "'  datatype='" + $(this).attr("datatype") + "' value='" + zhi + "' class='form-control input-sm' style='width:50px;'/><a href='javascript:void(0)' old='" + zhi + "' style='color:red;font-weight:bold;font-size:16px;' class='qx'>×</a></span>");
            //移除绑定事件
            // $(this).unbind("dblclick");
        });
        $(".qx").live("click", function () {
            var zhi = $(this).attr("old");
            $(this).parents("td").html(zhi);
        });
    }
    else {
        $(":text[datatype='beizhu']", "#tbl").attr("readonly", true);
    }

    //保存
    $("#btn_save").click(function () {
        var count = 0;
        var arr = new Array();
        //修改数据
        $(":text:not(input[datatype='beizhu'])", "#tbl").each(function (i, n) {
            //验证数字
            if (!reg_math.test($(n).val())) {
                alert("请输入数字");
                count++;
                return false;
            }
            else {
                var obj = {
                    mem_id: $(n).attr("memid"),
                    dataYear: $("#ctl00_ContentPlaceHolder1_drp_year").val(),
                    dataMonth: $("#ctl00_ContentPlaceHolder1_drp_month").val(),
                    dataValue: $(n).val(),
                    dataType: $(n).attr("datatype"),
                    dataSource: "YearPaidStatis",
                    dataContent: ""
                };
                arr.push(obj);
                //隐藏数据文本框
                var zhi = $(n).val();
                $(n).parents("td").html(zhi).css("background-color", "yellow");
            }

        });



        //备注
        $(":text[datatype='beizhu']", "#tbl").each(function (i, n) {
            var obj = {
                mem_id: $(n).attr("memid"),
                dataYear: $("#ctl00_ContentPlaceHolder1_drp_year").val(),
                dataMonth: $("#ctl00_ContentPlaceHolder1_drp_month").val(),
                dataValue: "0",
                dataType: $(n).attr("datatype"),
                dataSource: "YearPaidStatis",
                dataContent: $(n).val()
            };
            arr.push(obj);
        });

        if (count == 0 && arr.length > 0) {
            $.post("/HttpHandler/Calendar/CalendarHandler.ashx", { "action": "updatedata", "data": Global.toJSON(arr) }, function (result) {
                if (result == "1") {
                    alert("保存成功！");
                    $("#ctl00_ContentPlaceHolder1_btn_search").click();
                }
                else {
                    alert("保存失败！");
                }
            });

        }

    });

    //数字验证正则    
    var reg_math = /^[+-]?\d+(\.\d+)?$/;
    $(":text:not(input[datatype='beizhu'])", "#tbl").live("change", function () {
        var _inputValue = $(this).val();
        if (!reg_math.test(_inputValue)) {
            alert("请输入数字");
            $(this).val("0");
        }
    });
    //显示加载中
    $("#ctl00_ContentPlaceHolder1_btn_search").click(function () {
        $('body').modalmanager('loading');
    });

    //部门导出
    $("#ctl00_ContentPlaceHolder1_btn_export").click(function () {
        if ($("#ctl00_ContentPlaceHolder1_drp_unit").val() == "-1") {
            alert("请选择部门！");
            return false;
        }
    });
    //默认隐藏
    $("#ctl00_ContentPlaceHolder1_btn_export").hide();
    $("#ctl00_ContentPlaceHolder1_btn_allexport").hide();
    //权限控制
    if ($("#ctl00_ContentPlaceHolder1_previewPower").val() == "1") {
        //全部
        $("#ctl00_ContentPlaceHolder1_btn_export").show();
        $("#ctl00_ContentPlaceHolder1_btn_allexport").show();
    }
    else if ($("#ctl00_ContentPlaceHolder1_previewPower").val() == "2") {
        //部门
        $("#ctl00_ContentPlaceHolder1_btn_export").show();
    }

    //全部导出
    $("#ctl00_ContentPlaceHolder1_btn_allexport").click(function () {
        $("body").modalmanager('loading');
        //导出放在一个隐藏的iframe中，这样不影响当前页面上脚本的运行，才可以隐藏遮罩层
        setTimeout("querySession()", 10000);
    });
    //锁定
    $("#btn_lock").click(function () {

        $(":text:not(input[datatype='beizhu'])", "#tbl").each(function (i, n) {
          
            //验证数字
            if (!reg_math.test($(n).val())) {
                alert("请输入数字");
              
                return false;
            }
            else {
                //隐藏数据文本框
               var zhi = $(n).val();
                $(n).parents("td").html(zhi);
            }
        });

        var date = drp_year + "-" + (parseInt(drp_month) < 10 ? "0" + drp_month : drp_month);
        var arr = new Array();
        //循环日期
        $("#tbl tr:gt(0)").each(function (i, n) {
            //排除合计
            if ($(n).find("td:eq(0)").text().indexOf("合计") < 0) {

                var mem_id = $(n).find("td:eq(-13)").attr("mem_id");
                var memname = $(n).find("td:eq(-13)").text();
                var mem_unitid = $(n).find("td:eq(-13)").attr("mem_unitid");

                var obj = {
                    mem_id: mem_id,
                    mem_name: memname,
                    mem_unit_ID: mem_unitid,                 
                    dataDate: date,
                    time_leave: $.trim($(n).find("td:eq(-5)").text()),
                    time_sick: $.trim($(n).find("td:eq(-4)").text()),
                    time_over: $.trim($(n).find("td:eq(-10)").text()),
                    time_marry: $.trim($(n).find("td:eq(-8)").text()),
                    time_mater: $.trim($(n).find("td:eq(-7)").text()),
                    time_die: $.trim($(n).find("td:eq(-6)").text()),
                    time_late: $.trim($(n).find("td:eq(-11)").text()),
                    time_year: $.trim($(n).find("td:eq(-9)").text()),
                    time_sum: $.trim($(n).find("td:eq(-12)").text()),
                    privMonthDay: $.trim($(n).find("td:eq(-2)").text()),
                    currentMonthDay: $.trim($(n).find("td:eq(-1)").text()),
                    content: $.trim($(n).find("td:eq(-3) input").val())
                };
                arr.push(obj);
            }
        });


        $.post("/HttpHandler/Calendar/CalendarHandler.ashx", { "action": "lock", "source": "YearPaidStatis", "data": Global.toJSON(arr) }, function (result) {
            if (result == "1") {
                alert("锁定成功！");
            }
            else {
                alert("锁定失败！");
            }
        });
    });
    //漂浮表头
    fixTableHead();
});
function querySession() {
    var retValue = $.ajax({ url: '/HttpHandler/Calendar/CalendarHandler.ashx?action=GetDownloadState&Random=' + Math.floor(Math.random() * (1000000 + 1)), type: 'GET', async: false, cache: false }).responseText;
    //返回0代表Excel还没有准备完成，否则返回1，准备完成删除sesson,隐藏遮罩
    if (retValue * 1 == 0) {
        //一秒执行一次
        setTimeout("querySession()", 5000);
    }
    else {
        //隐藏遮罩层
        $("body").modalmanager('removeLoading');
    }
}
//固定表头
var fixTableHead = function () {

    $('#tbl').each(function () {
        if ($(this).find('thead').length > 0 && $(this).find('th').length > 0) {
            // Clone <thead>
            var $w = $(window),
                $t = $(this),
                $thead = $t.find('thead').clone(),
                $col = $t.find('thead, tbody').clone();

            // Add class, remove margins, reset width and wrap table
            $t
            .addClass('sticky-enabled')
            .css({
                margin: 0,
                width: '100%'
            }).wrap('<div class="sticky-wrap" />');

            if ($t.hasClass('overflow-y')) $t.removeClass('overflow-y').parent().addClass('overflow-y');

            // Create new sticky table head (basic)
            $t.after('<table class="sticky-thead" style="top:-60px;"/>');

            // If <tbody> contains <th>, then we create sticky column and intersect (advanced)
            if ($t.find('tbody th').length > 0) {
                $t.after('<table class="sticky-col" /><table class="sticky-intersect" />');
            }

            // Create shorthand for things
            var $stickyHead = $(this).siblings('.sticky-thead'),
                $stickyCol = $(this).siblings('.sticky-col'),
                $stickyInsct = $(this).siblings('.sticky-intersect'),
                $stickyWrap = $(this).parent('.sticky-wrap');

            $stickyHead.append($thead);

            $stickyCol
            .append($col)
                .find('thead th:gt(0)').remove()
                .end()
                .find('tbody td').remove();


            $stickyInsct.html('<thead><tr><th>' + $t.find('thead th:first-child').html() + '</th></tr></thead>');

            // Set widths
            var setWidths = function () {
                $t
                .find('thead th').each(function (i) {
                    $stickyHead.find('th').eq(i).width($(this).width());
                })
                .end()
                .find('tr').each(function (i) {
                    $stickyCol.find('tr').eq(i).height($(this).height());
                });

                // Set width of sticky table head
                $stickyHead.width($t.width());

                // Set width of sticky table col
                $stickyCol.find('th').add($stickyInsct.find('th')).width($t.find('thead th').width())
            },
                repositionStickyHead = function () {
                    // Return value of calculated allowance
                    var allowance = calcAllowance();

                    // Check if wrapper parent is overflowing along the y-axis
                    if ($t.height() > $stickyWrap.height()) {
                        // If it is overflowing (advanced layout)
                        // Position sticky header based on wrapper scrollTop()
                        if ($stickyWrap.scrollTop() > 0) {
                            // When top of wrapping parent is out of view
                            $stickyHead.add($stickyInsct).css({
                                opacity: 1,
                                top: $stickyWrap.scrollTop()
                            });
                        } else {
                            // When top of wrapping parent is in view
                            $stickyHead.add($stickyInsct).css({
                                opacity: 0,
                                top: 0
                            });
                        }
                    } else {
                        // If it is not overflowing (basic layout)
                        // Position sticky header based on viewport scrollTop
                        if ($w.scrollTop() > $t.offset().top && $w.scrollTop() < $t.offset().top + $t.outerHeight() - allowance) {
                            // When top of viewport is in the table itself
                            $stickyHead.add($stickyInsct).css({
                                opacity: 1,
                                top: $w.scrollTop() - $t.offset().top
                            });
                        } else {
                            // When top of viewport is above or below table
                            $stickyHead.add($stickyInsct).css({
                                opacity: 0,
                                top: -60
                            });
                        }
                    }
                },
                repositionStickyCol = function () {
                    if ($stickyWrap.scrollLeft() > 0) {
                        // When left of wrapping parent is out of view
                        $stickyCol.add($stickyInsct).css({
                            opacity: 1,
                            left: $stickyWrap.scrollLeft()
                        });
                    } else {
                        // When left of wrapping parent is in view
                        $stickyCol
                        .css({ opacity: 0 })
                        .add($stickyInsct).css({ left: 0 });
                    }
                },
                calcAllowance = function () {
                    var a = 0;
                    // Calculate allowance
                    $t.find('tbody tr:lt(1)').each(function () {
                        a += $(this).height();
                    });

                    // Set fail safe limit (last three row might be too tall)
                    // Set arbitrary limit at 0.25 of viewport height, or you can use an arbitrary pixel value
                    if (a > $w.height() * 0.25) {
                        a = $w.height() * 0.25;
                    }

                    // Add the height of sticky header
                    a += $stickyHead.height();
                    return a;
                };

            setWidths();

            $t.parent('.sticky-wrap').scroll($.throttle(250, function () {
                repositionStickyHead();
                repositionStickyCol();
            }));

            $w
            .load(setWidths)
            .resize($.debounce(250, function () {
                setWidths();
                repositionStickyHead();
                repositionStickyCol();
            }))
            .scroll($.throttle(250, repositionStickyHead));
        }
    });
}
