﻿$(document).ready(function () {

    var istater = $("#ctl00_ContentPlaceHolder1_hiddenIstaster").val();
    if (istater != "1") {
        var selectuserid = $("#UserID").val();//下拉框选择人员
        var currentuserid = $("#currentuserid").val();//登录用户
        $("#checkboxdiv").hide();
        $("#btn_sp").hide();
        $("#btn_count").val("申请统计").attr("title", "申请统计");

    } else {
        var selectuserid = $("#UserID").val();//下拉框选择人员
        var currentuserid = $("#currentuserid").val();//登录用户
        if ($.trim(selectuserid) == $.trim(currentuserid)) {
            $("#btn_count").val("申请统计").attr("title", "申请统计");
            $("#checkboxdiv").hide();
            $("#btn_sp").hide();
        } else {
            $("#btn_count").val("申请审批").attr("title", "申请审批");
            $("#checkboxdiv").show();
            $("#btn_sp").show();
        }
    }

    //选择部门显示人员
    $("#ctl00_ContentPlaceHolder1_drp_unit").change(function () {
        var unitid = $(this).val();   
        var pp = $("#ctl00_ContentPlaceHolder1_previewPower").val();
        var drp_year = $("#selectYear").val();
        var drp_month = $("#selectMonth").val();
        var url = "/HttpHandler/Calendar/CalendarHandler.ashx";
        var data = { "action": "seluser", "year": drp_year, "month": drp_month, "unitid": unitid, "previewPower": pp, "userid": $("#ctl00_ContentPlaceHolder1_userSysNum").val() };
        $.get(url, data, function (result) {
            var dt = eval('(' + result + ')');
            var ds = dt != null ? dt.ds : null;
            if (ds != null && dt != null) {
                $("#ctl00_ContentPlaceHolder1_userlist option").remove();
                // $("#ctl00_ContentPlaceHolder1_drp_user").append("<option value='-1'>---全部---</option>");
                $.each(ds, function (i, k) {
                    var option = $("<option value='" + k["mem_ID"] + "'>" + k["mem_Name"] + "</option>");
                    $("#ctl00_ContentPlaceHolder1_userlist").append(option);
                });

            }

        });


    });
    //选择用户
    $("#ctl00_ContentPlaceHolder1_userlist").change(function () {
       
        $("#ctl00_ContentPlaceHolder1_hid_userid").val($(this).val());
        My_Submit();
    });


    //单选按钮
    $("input[name='ckbname']").live("click", function () {
        if ($(this).parent().attr("class") == null || $(this).parent().attr("class") == "") {
            $(this).parent().attr("class", "checked");
        } else {
            $(this).parent().attr("class", "");
        }

    })
    //全选
    $("#checkall").live("click", function () {
        $(this).parent().attr("class", "checked");
        $("#checkreturn").parent().attr("class", "");
        $("#checkcancle").parent().attr("class", "");
        if ($(this).is(":checked")) {
            $("input[name='ckbname']").parent().addClass($(this).attr("checked"));
            $("#checkcancle").parent().removeClass();
            $("#checkreturn").parent().removeClass();
        }
        else {
            $("input[name='ckbname']").parent().removeClass();
        }
    })
    $("#checkreturn").live("click", function () {
        $("#checkall").parent().attr("class", "");
        $("#checkcancle").parent().attr("class", "");
        if ($("#checkreturn").is(":checked")) {
            $("#checkreturn").parent().removeClass();
            $("input[name='ckbname']").each(function () {
                if ($(this).parent().attr("class") == "checked") {
                    $(this).parent().attr("class", "");
                } else {
                    $(this).parent().attr("class", "checked");
                }
            })
        } else {
            $("#checkreturn").parent().attr("class", "checked");
            $("input[name='ckbname']").each(function () {
                if ($(this).parent().attr("class") == "") {
                    $(this).parent().attr("class", "checked");
                } else {
                    $(this).parent().attr("class", "");
                }
            })
        }
        $("#checkall").parent().removeClass();
        $("#checkcancle").parent().removeClass();
    })
    $("#checkcancle").live("click", function () {
        $(this).parent().attr("class", "checked");
        $("#checkall").parent().attr("class", "");
        $("#checkreturn").parent().attr("class", "");
        if ($(this).is(":checked")) {
            $("input[name='ckbname']").parent().removeClass();
            $("#checkall").parent().removeClass();
            $("#checkreturn").parent().removeClass();
        }
    })
    //审批
    $("#btn_sp").live("click", function () {
        var chk_value = [];
        var chk_valueerror = [];
        $('input[name="ckbname"]', $("#data")).each(function () {
            if ($.trim($(this).parent().attr("class")) == "checked") {
                if ($(this).attr("option") == "0") {
                    chk_value.push($(this).val());
                } else {
                    chk_valueerror.push($(this).val());
                }
            }
        });
        if (chk_valueerror.length > 0) {
            alert("选择人员存在已经审批！");
            return false;
        }
        if (chk_value.length == 0) {
            alert("你还没有选择任何人员！");
            return false;
        }
        var bsID = chk_value.join(",")
        //选中id
        var userid = $("#UserID").val();
        //当前id
        var taster = $("#ctl00_ContentPlaceHolder1_hiddentaster").val();
        $.post("/HttpHandler/WorkCheckHandler.ashx", { "action": "updateApp", "userid": userid, "bsID": bsID, "taster": taster, "n": Math.random() }, function (result) {
            if (parseInt(result) > 0) {
                alert("审批成功！");
                $("#ctl00_ContentPlaceHolder1_selectMonth").change();
                //window.location.reload(force = false);
            } else {
                alert("审批失败，请联系管理员！");
            }
        })
    })
    //选中下框中当前人
    var useid = $("#UserID").val();

    $("#ctl00_ContentPlaceHolder1_userlist").find("option[value='" + useid + "']").attr("selected", true);

    //点击审批按钮
    $("a[href='#AuditMsg']").live("click", function () {
        var applytype = $.trim($(this).attr("applytype"));
        var applyID = $.trim($(this).attr("applyID"));
        var reason = $.trim($(this).attr("reason"));
        var applycontent = $.trim($(this).attr("applycontent"));
        var applysort = $.trim($(this).attr("applysort"));
        var starttime = $.trim($(this).attr("starttime"));
        var endtime = $.trim($(this).attr("endtime"));
        var remark = $.trim($(this).attr("remark"));
        var memname = $.trim($(this).attr("memname"));
        var totaltime = $.trim($(this).attr("totaltime"));
      
        //if (applysort == "未打卡") {
           
        //    $("#kqtj", "#AuditMsg").attr("style", "display:none");
        //}
        //else {
        //    $("#kqtj", "#AuditMsg").attr("style", "display:''");
        //}
        applycontent=applycontent == "" ? "通过" : applycontent;
        $("#applyid").val(applyID);
        $("#applycontent").text(applycontent);
        $("#applysort").text(applysort);
        $("#reason").text(reason);
        $("#applytime").text(starttime + " - " + endtime);
       
        $("#applyname").text(memname);
        $("#remark").text(remark);
        $("#totaltime").text(totaltime);
        
    });
    //备注内容
    $("#sel_content").change(function () {
        $("#remark").val($(this).val());
    });
    //保存
    $("#btn_submit").click(function () {
        var applyid = $("#applyid").val();
        var remark = $.trim($("#remark").val());
        var managerpassword = $.trim($("#managerpassword").val());
        var currentpassword = $.trim($("#currentpassword").val());
        var currentuserid = $("#currentuserid").val();
        var ischeck = "0";
        if ($("#ischeck").is(":checked")) {
            ischeck = "1";
        }
        if (remark == "") {
            alert("备注不能为空!");
            return false;
        }

        if (currentpassword != managerpassword) {
            alert("管理员密码不正确!")
            return false;
        }

        $.post("/HttpHandler/WorkCheckHandler.ashx", { "action": "add", "applyid": applyid, "remark": remark, "ischeck": ischeck, "checkuserid": currentuserid, "n": Math.random() }, function (result) {
            if (result == "success") {
                alert("审批成功!");
                My_Submit();
            }
            else if (result == "sendmessage") {
                alert("给申请人发送消息失败!");
                return false;
            }
            else if (result == "audit") {
                alert("审批人不存在!");
                return false;
            }
            else if (result == "message") {
                alert("消息更新失败!");
                return false;
            }
            else {
                alert("审批失败!");
                return false;
            }
        });

    });

    //弹出框关闭
    $("#btn_gb").live("click", function () {
        $("#btn_close").trigger("click");
    });
    //弹出框关闭
    $("#btn_gb2").live("click", function () {
        $("#btn_close2").trigger("click");
    });
    //隔行换色
    $("#data>tbody>tr:even > td").css("background-color", "#f9f9f9");

    //月统计
    $("#btn_count").click(function () {
        var nian = $("#ctl00_ContentPlaceHolder1_selectYear").val();
        var yue = $("#ctl00_ContentPlaceHolder1_selectMonth").val();
        var selectuserid = $("#UserID").val();//下拉框选择人员
        var currentuserid = $("#currentuserid").val();//登录用户
        var username = $("#ctl00_ContentPlaceHolder1_userlist").find("option:selected").text();
        $("#divtitle2").text(username + " " + nian + "年" + yue + "月申请记录");

        $("#data tr:gt(0)").remove();
        $.post("/HttpHandler/WorkCheckHandler.ashx", { "action": "select", "userid": selectuserid, "daytime": nian + "-" + yue, "n": Math.random() }, function (result) {
            if (result != null && result != "") {
                var data = eval('(' + result + ')');
                $.each(data, function (i, att) {
                    var applytype, auditstr, daytime, applytitle, hrefurl;
                    switch (att.sort) {
                        case 1:
                            applytype = "市内公差";
                            break;
                        case 2:
                            applytype = "京外出差";
                            break;
                        case 3:
                            applytype = "未打卡";
                            break;
                        default:
                            applytype = "请假";
                            break;
                    }

                    if (att.dateTime != null && att.dateTime != "") {
                        daytime = ChangeDateFormat(att.dateTime, 0);
                    }
                    applytitle = daytime + " " + att.outTime;


                    if (att.ischeck == "1") {
                        auditstr = "<font color='green'>通过审批</font>";
                        hrefurl = "href='Apply.aspx?applyID=" + att.id + "&date=" + daytime + "'";
                    }
                    else {
                        if (att.remark != null && att.remark != "") {
                            auditstr = "未通过审批";
                            hrefurl = "href='Apply.aspx?applyID=" + att.id + "&date=" + daytime + "'";
                        }
                        else {
                            auditstr = "<font color='red'>尚未审批</font>";
                            //如果当前是自己只能预览
                            if (currentuserid == selectuserid) {
                                hrefurl = "href='Apply.aspx?applyID=" + att.id + "&date=" + daytime + "'";
                            }
                            else {
                                hrefurl = "applyID=\"" + att.id + "\" daytime=\"" + ChangeDateFormat(att.dateTime, 1) + "\" applyname=\"" + username + "\" applytime=\"" + applytitle + "\" applysort=\"" + applytype + "\" reason=\"" + (att.reason == null ? "" : att.reason) + "\" addtime=\"" + ChangeDateFormat(att.applyTime, 2) + "\" data-toggle=\"modal\" href=\"#AuditMsg\"";
                            }

                        }

                    }
                    //<label style=\"margin-right: 0px;\"> <input type=\"checkbox\" name=\"ckbname\"  value=" + att.id + "></label> 
                    var $tr = $("<tr></tr>");
                    $tr.append('<td> <label  class="checkbox-inline"><div class="checker"><span> <input type="checkbox" name="ckbname" option=' + att.ischeck + '  value=' + att.id + '></span></div></label></td>');
                    $tr.append("<td>" + username + "</td>");
                    $tr.append("<td align='left'><a " + hrefurl + ">" + applytitle + "</a></td>");
                    $tr.append("<td>" + applytype + "</td>");
                    $tr.append("<td align='left'>" + (att.reason == null ? "" : att.reason) + "</td>");
                    $tr.append("<td>" + auditstr + "</td>");
                    $("#data").append($tr);
                });

                //$("#data", $("#modalAddDiv")).next().append('<table class="table table-bordered table-hover" style="font-size: 15px;width:350px;" id="checkTab"><tr><td style=""><label><div class="checker"><span> <input type="checkbox" name="ckbname"  id="checkall"></span></div>全选</label></td><td style=""><label><div class="checker"><span> <input type="checkbox" name="ckbname"  id="checkreturn"></span></div>反选</label></td><td style=""><label><div class="checker"><span> <input type="checkbox" name="ckbname"  id="checkcancle"></span></div>取消</label></td></tr></table>')
            }
            else {
                $("#data").append("<tr><td colspan='6' style='color:red'>暂无记录</td></tr>");
            }
        });
    });

});
//日期格式转换
function ChangeDateFormat(cellval, type) {
    if (cellval != null) {
        var date = new Date(parseInt(cellval.replace("/Date(", "").replace(")/", ""), 10));
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var mine = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var secnds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        if (type == 1) {
            return date.getFullYear() + "年" + month + "月" + currentDate + "日";
        }
        else if (type == 2) {
            return date.getFullYear() + "-" + month + "-" + currentDate + " " + hour + ":" + mine + ":" + secnds;
        }
        return date.getFullYear() + "-" + month + "-" + currentDate;
    }
    else {
        return "";
    }
}

