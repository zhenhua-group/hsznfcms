﻿
$(function () {

    $("#jqGrid").jqGrid({
        url: '/HttpHandler/Calendar/ApplyStatisHandler.ashx?n=' + (Math.random() + new Date().getMilliseconds()) + '&strwhere=' + escape($("#ctl00_ContentPlaceHolder1_hid_where").val()),
        datatype: 'json',
        height: "auto",
        mytype: 'POST',
        rowNum: 20,
        rowList: [20, 25, 30],
        colNames: ['序号', '', '姓名','部门名称','专业', '操作'],
        colModel: [
                             { name: 'nid', index: 'nid', width: 30, align: 'center' },
                             { name: 'mem_ID', index: 'mem_ID', hidden: true, editable: true },
                             { name: 'mem_Name', index: 'mem_Name', width: 80 },
                             { name: 'unit_Name', index: 'unit_Name', width: 100 },
                             { name: 'spe_Name', index: 'spe_Name', width: 80 },
                             { name: 'mem_ID', index: 'mem_ID', width: 100, align: 'center', sorttable: false, editable: false, formatter: colShowFormatter }
        ],
        rowTotals: true,
        colTotals: true,
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.rows; },
            page: function (obj) { return obj.pageindex; },
            total: function (obj) { return obj.pagecount; },
            records: function (obj) { return obj.total; }
        },
        prmNames: {
            page: 'PageIndex',
            rows: 'PageSize',
            sort: 'OrderBy',
            order: 'Sort'
        },
        postData: { "applytype": "leave" },
        loadonce: false,
        sortname: 'mem_Order asc,mem_ID',
        sortorder: 'asc',
        pager: "#gridpager",
        viewrecords: true,
        shrinkToFit: false,
        autowidth: true,
        editurl: "/HttpHandler/Calendar/ApplyStatisHandler.ashx",
        multiselect: true,
        multiselectWidth: 25,
        footerrow: true,
        gridComplete: completeMethod,
        loadComplete: loadCompMethod
    });


    //显示查询
    $("#jqGrid").jqGrid("navGrid", "#gridpager", {
        add: false,
        edit: false,
        del: false,
        search: false
    },
            {//编辑
            },
            {//添加
            },
            {//删除
                top: 200,
                left: 400,
                reloadAfterSubmit: true,
                closeAfterDelete: true,
                closeOnEscape: true,
                afterSubmit: function (response, postdata) {
                    alert(response.responseText);
                    if (response.responseText == "") {
                        $("#jqGrid").trigger("reloadGrid", [{ current: true }]);
                        return [false, response.responseText]
                    }
                    else {
                        $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
                        return [true, response.responseText]
                    }
                },
                delData: {
                    EmpId: function () {

                        var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selarrrow');
                        var len = sel_id.length;
                        var values = "";
                        if (len == 0) {
                            return values;
                        } else {
                            for (i = 0; i < len; i++) {
                                values += $('#jqGrid').jqGrid('getCell', sel_id[i], 'ID') + ",";
                            }
                        }

                        //var sel_id = $('#jqGrid').jqGrid('getGridParam', 'selrow');
                        //var value = $('#jqGrid').jqGrid('getCell', sel_id, 'pro_ID');
                        return values;
                    }

                }
            },
            {//搜索
                top: 200,
                left: 400
            }
            );

    //查询
    $("#btn_search").click(function () {
        var strwhere = escape($("#ctl00_ContentPlaceHolder1_hid_where").val());
        var unit = $("#ctl00_ContentPlaceHolder1_drp_unit").val();
        var keyname = $.trim($("#ctl00_ContentPlaceHolder1_txt_keyname").val());      

        $("#jqGrid").jqGrid('setGridParam', {
            url: "/HttpHandler/Calendar/ApplyStatisHandler.ashx",
            postData: { "action": "sel", "strwhere": strwhere, "unit": unit, "keyname": keyname },
            page: 1,
            sortname: 'mem_ID',
            sortorder: 'asc'

        }).trigger("reloadGrid");
    });
   

    //总经理
    function colShowFormatter(celvalue, options, rowData) {
        return "<a class=\"btn default btn-xs red-stripe\" href='ApplyStatisDetail.aspx?mem_id=" + celvalue + "' target='_blank'>考勤详情</a>";
    }
 
    //统计 
    function completeMethod() {

     
        var rowIds = $("#jqGrid").jqGrid('getDataIDs');
        for (var i = 0, j = rowIds.length; i < j; i++) {
            $("#" + rowIds[i]).find("td").eq(1).text((i + 1));
        }       


    }
    //无数据
    function loadCompMethod() {
        var rowcount = parseInt($("#jqGrid").getGridParam("records"));
        if (rowcount <= 0) {
            if ($("#nodata").text() == '') {
                $("#jqGrid").append("<div id='nodata'>没有查询到数据!</div>");
            }
            else { $("#nodata").show(); }
        }
        else {
            $("#nodata").hide();
        }
    }
    var clearGridPostData = function () {
        var postData = $("#jqGrid").jqGrid("getGridParam", "postData");
        $.each(postData, function (k, v) {
            delete postData[k];
        });
    }
});