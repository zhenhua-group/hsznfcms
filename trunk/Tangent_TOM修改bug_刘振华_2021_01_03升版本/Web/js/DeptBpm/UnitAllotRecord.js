﻿
var allotRecord = function () {

    var _containerItem = {};
    var pageLoad = function () {

        //比例变化
        $(":text", $("#tbDataOne")).change(function () {

            var obj = $(this);
            var reg_math = /^[-+]?[0-9]+(\.[0-9]+)?$/;

            if (!reg_math.test(obj.val())) {
                obj.val("");
                return false;
            }

            //同步改变值
            changeAllotVal(obj);
        }).focus(function () {
            $(this).select();
        }).blur(function () {
            $(this).trigger('change');
        });

        if ($("#hidIsSubmit").val() == "True") {
            $(":text", $("#tbDataOne")).attr("disabled", true).attr("style", "border:none;width:60px;text-align: right;");

            _containerItem = $("#tbDataOne tbody tr");

            initOrderTable();
        }
        //保存数据
        $("#btnSave").click(function () {

            //按钮不可用
            $(this).attr("class", "btn btn-sm default disabled");
            //保存数据
            saveAllotDetail("save");
        });

        ////提交数据
        $("#btnSubmit").click(function () {

            //按钮不可用
           $(this).attr("class", "btn btn-sm default disabled");
            //保存数据
            saveAllotDetail("submit");
        });
        ////重新调整
        $("#Button1cx").click(function () {

            $(":text", "#tbDataOne").attr("readonly", false);
      
        });
        ////重新调整
        $("#btnSavesss").click(function () {
 
            saveAllotDetail("submit");
        });
        
        ////重新调整
        $("#btnSavess").click(function () {

            saveAllotDetail("submit");
        });


        //只限查看
        if ($("#hidIsCheck").val() == "chk") {
           //$(":text", "#tbDataOne").attr("readonly", true);  
            $(".btn.btn-sm.red").attr("class", "btn btn-sm red disabled");
            $("#ctl00_ContentPlaceHolder1_btn_Export").attr("class", "btn btn-sm red");

        }

        setHeadHeji();

        $("#ctl00_ContentPlaceHolder1_btn_Search").hide()
        $("#btnDefault").click(function () {

            $("#ctl00_ContentPlaceHolder1_btn_Search").trigger('click');
        });


        if ($("#hidIsSubmit").val() == "True") {
            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.DoneMsg();
        }
        else {
            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.ReadMsg();
        }
 
        

        //发起归档
        $("#btnBak").click(function () {

            saveBak();
        });

        //添加未参加考核人员
        $("#btnAddMems").click(function () {
            //获取数据
            var txt1 = $("#bmjl").val();
            var txt2 = $("#gstz").val();
            var txt3 = $("#yfjj").val();
            var txt4 = $("#xmjjjs").val();
            var txt5 = $("#bmnjjjs").val();
            var txt6 = $("#xmjj").val();
            var txt7 = $("#bmnjj").val();
            var txt8 = $("#xmglj").val();
            var txt9 = $("#yxyg").val();
            var txt10 = $("#tcgx").val();
            var txt11 = $("#bimjj").val();
            var txt12 = $("#ghbt").val();
            var txt13 = $("#hj").val();
            var txt14 = $("#hjyf").val();
            var txt15 = $("#zcfy").val();


            var reg = /^([1-9]\d*|[0]{1,1})$/;

            if (txt1 != '') {
                if (!reg.test(txt1)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt2 != '') {
                if (!reg.test(txt2)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt3 != '') {
                if (!reg.test(txt3)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt4 != '') {
                if (!reg.test(txt4)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt5 != '') {
                if (!reg.test(txt5)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt6 != '') {
                if (!reg.test(txt6)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt7 != '') {
                if (!reg.test(txt7)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt8 != '') {
                if (!reg.test(txt8)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt9 != '') {
                if (!reg.test(txt9)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt10 != '') {
                if (!reg.test(txt10)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt11 != '') {
                if (!reg.test(txt11)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt12 != '') {
                if (!reg.test(txt12)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt13 != '') {
                if (!reg.test(txt13)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt14 != '') {
                if (!reg.test(txt14)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            if (txt15 != '') {
                if (!reg.test(txt15)) {
                    alert('请输入数字！');
                    return false;
                }
            }
            //给当前表增加一行
            var trHtml = "<tr>";
            trHtml += "<td><span unitid=" + $("#drpMems option:checked").attr('uid') + ">" + $("#drpMems option:checked").attr('uname') + "</span></td>";
            trHtml += "<td><span memid=" + $("#drpMems").val() + ">" + $("#drpMems option:checked").text() + "</span></td>";
            trHtml += "<td><span isfired='0'>在职</span></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt1 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt1 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt2 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt2 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt4 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt4 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt5 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt5 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt6 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt6 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt7 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt7 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt8 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt8 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt9 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt9 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt10 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt10 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt11 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt11 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt15 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt15 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt12 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt12 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt14 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt14 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt3 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt3 + " /></td>";
            trHtml += "<td><input type='text' name='name' value=" + txt13 + " style='width: 60px; text-align: right;' readonly/>";
            trHtml += "<input type='hidden' name='name' value=" + txt13 + " /></td>";
            trHtml += "<td></td>";
            trHtml += "</tr>";

            var arrDetail = new Array();
            //保存这个人员
            arrDetail[0] = {
                ID: "0",
                RenwuID: $("#hidRenwuno").val(),
                UnitID: $("#drpMems option:checked").attr('uid'),
                UnitName: $("#drpMems option:checked").attr('uname'),
                MemID: $("#drpMems").val(),
                MemName: $("#drpMems option:checked").text(),
                IsFired: 0,
                bmjl: txt1,
                gstz: txt2,
                yfjj: txt3,
                xmjjjs: txt4,
                bmnjjjs: txt5,
                xmjj: txt6,
                bmnjj: txt7,
                xmglj: txt8,
                yxyg: txt9,
                tcgx: txt10,
                bimjj: txt11,
                zcfy: txt15,
                ghbt: txt12,
                hj: txt13,           
                insertUserID: $("#hidUserSysNo").val(),
                Stat: "0"
            };

            var jsonEntity = JSON.stringify(arrDetail);

            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var postaction = "saveallmemsallotdetails";
            var data = { "action": postaction, "data": jsonEntity };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //添加到表中
                        $("#tbDataOne>tbody").prepend(trHtml);
                        //隐藏当前人员下拉的
                        $("#drpMems option:checked").remove();
                        //关闭
                        $("#btnCancel").trigger('click');
                        //设置成功！
                        alert("数据保存成功！");
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });

        });
    }
    //填写表单计算
    var changeAllotVal = function (obj) {
        //施工图部门
        // var unitArr = [231, 232, 237, 238, 239];
        //所有施工图部门ID qpl 2016年11月23日
        var unitArr = $("#hidArchUnitStr").val();

        var prt = obj.parent().parent();
        //部门ID
        var unitid = prt.find("td").eq(0).children("span").attr("unitid");
        //部门经理调整
        var bmjjtz = prt.find("td").eq(3).children("input").val();
        //公司调整
        var gstz = prt.find("td").eq(4).children("input").val();
       
        //项目奖金计算值
        var xmjjjs = prt.find("td").eq(5).children("span").text();
        //部门内奖金计算值
        var bmnjjjs = prt.find("td").eq(6).children("span").text();
        //项目奖金
        var xmjj = prt.find("td").eq(7).children("span").text();
        //部门内奖金
        var bmnjj = prt.find("td").eq(8).children("span").text();
        //项目管理奖
        var xmglj = prt.find("td").eq(9).children("input").val();
        //优秀员工
        var yxyg = prt.find("td").eq(10).children("span").text();
        //突出贡献
        var tcgx = prt.find("td").eq(11).children("input").val();
        //Bim
        var bim = prt.find("td").eq(12).children("input").val();
        var zcfy = prt.find("td").eq(13).children("input").val();
        //工会补贴
        var ghbt = prt.find("td").eq(14).children("input").val();
        //预发奖金
        //var yfjj = prt.find("td").eq(5).children("span").text();
        var yfjj = prt.find("td").eq(16).children("span").text();

       //项目奖金
        if (bmjjtz != "" || gstz != "") {

            //如果是其他部门
            if (unitArr.indexOf(parseInt(unitid)) < 0) {

                //if ($("#hidIsSave").val() == "True") {
                //    var oldresult = parseFloat(prt.find("td").eq(9).children("input").val());
                //    var oldbmjjtz = prt.find("td").eq(3).children(":hidden").val();
                //    var oldgstz = prt.find("td").eq(4).children(":hidden").val();
                //    var result2 = oldresult + parseFloat(bmjjtz - oldbmjjtz) + parseFloat(gstz - oldgstz);
                //    prt.find("td").eq(9).children("span").text(result2.toFixed(0));
                //}
                //else {
                //部门内奖金
                var result2 = parseFloat(bmnjjjs) + parseFloat(bmjjtz) + parseFloat(gstz) - parseFloat(yfjj);;
                //prt.find("td").eq(9).children("span").text(result2.toFixed(0));
                prt.find("td").eq(8).children("span").text(result2.toFixed(0));
                //}
            }
            else {
                //项目奖金
                //if ($("#hidIsSave").val() == "True") {
                //    var oldresult = parseFloat(prt.find("td").eq(8).children("input").val());
                //    var oldbmjjtz = prt.find("td").eq(3).children(":hidden").val();
                //    var oldgstz = prt.find("td").eq(4).children(":hidden").val();
                //    var result = oldresult + parseFloat(bmjjtz - oldbmjjtz) + parseFloat(gstz - oldgstz);
                //    prt.find("td").eq(8).children("span").text(result.toFixed(0));
                //}
                //else {
                var result = parseFloat(xmjjjs) + parseFloat(bmjjtz) + parseFloat(gstz) - parseFloat(yfjj);
                //prt.find("td").eq(8).children("span").text(result.toFixed(0));
                prt.find("td").eq(7).children("span").text(result.toFixed(0));
                //}
            }
        }

        //部门内奖金
        var abc7 = parseFloat(prt.find("td").eq(8).children("span").text());
        //项目管理奖
        var abc8 = prt.find("td").eq(9).children("input").val() != "" ? parseFloat(prt.find("td").eq(9).children("input").val()) : 0;
        //优秀员工
        var abc9 = parseFloat(prt.find("td").eq(10).children("span").text());
        //突出贡献奖
        var abc10 = prt.find("td").eq(11).children("input").val() != "" ? parseFloat(prt.find("td").eq(11).children("input").val()) : 0;
        //BIM及标准化
        var abc11 = prt.find("td").eq(12).children("input").val() != "" ? parseFloat(prt.find("td").eq(12).children("input").val()) : 0;
       //工会补助
        var abc12 = prt.find("td").eq(14).children("input").val() != "" ? parseFloat(prt.find("td").eq(14).children("input").val()) : 0;
        //注册费用
        var zcfy = prt.find("td").eq(13).children("input").val() != "" ? parseFloat(prt.find("td").eq(13).children("input").val()) : 0;

        //合计
        var hj = 0;
        //施工图部门
        if (unitArr.indexOf(parseInt(unitid)) > -1) {
            hj = parseFloat(xmjjjs) + parseFloat(bmjjtz) + parseFloat(gstz) + abc7 + abc8 + abc9 + abc10 + abc11 + abc12 + zcfy - parseFloat(yfjj);
        }
        else {
            hj = parseFloat(bmnjjjs) + parseFloat(bmjjtz) + parseFloat(gstz) + abc8 + abc9 + abc10 + abc11 + abc12 + zcfy - parseFloat(yfjj);
        }

        prt.find("td").eq(17).children("span").text(hj);

    }
    //保存
    var saveAllotDetail = function (action) {

        //对象
        var arrDetail = new Array();
        var index = 0;
        $("#tbDataOne tbody tr").each(function (i) {

            var obj = $(this);

            var input1 = obj.find("td").eq(3).children("input").val();
            var input2 = obj.find("td").eq(4).children("input").val();
            var input3 = obj.find("td").eq(9).children("input").val();
            var input4 = obj.find("td").eq(11).children("input").val();
            var input5 = obj.find("td").eq(12).children("input").val();      
            var input7 = obj.find("td").eq(13).children("input").val();
            var input6 = obj.find("td").eq(14).children("input").val();
           
            if ($.trim(input1) != "" && $.trim(input2) != "" && $.trim(input3) != "" && $.trim(input4) != "" && $.trim(input5) != "" && $.trim(input6) != "" && $.trim(input7) != "") {
                arrDetail[index++] = {
                    ID: "0",
                    RenwuID: $("#hidRenwuno").val(),
                    UnitID: obj.find("td").eq(0).children("span").attr("unitid"),
                    UnitName: obj.find("td").eq(0).children("span").text(),
                    MemID: obj.find("td").eq(1).children("span").attr("memid"),
                    MemName: obj.find("td").eq(1).children("span").text(),
                    IsFired: obj.find("td").eq(2).children("span").attr("isfired"),
                    bmjl: obj.find("td").eq(3).children("input").val(),
                    gstz: obj.find("td").eq(4).children("input").val(),
                    yfjj: obj.find("td").eq(16).children("span").text(),
                    xmjjjs: obj.find("td").eq(5).children("span").text(),
                    bmnjjjs: obj.find("td").eq(6).children("span").text(),
                    xmjj: obj.find("td").eq(7).children("span").text(),
                    bmnjj: obj.find("td").eq(8).children("span").text(),
                    xmglj: obj.find("td").eq(9).children("input").val(),
                    yxyg: obj.find("td").eq(10).children("span").text(),
                    tcgx: obj.find("td").eq(11).children("input").val(),
                    bimjj: obj.find("td").eq(12).children("input").val(),
                    zcfy: obj.find("td").eq(13).children("input").val(),
                    ghbt: obj.find("td").eq(14).children("input").val(),
                    hj: obj.find("td").eq(17).children("span").text(),
                    insertUserID: $("#hidUserSysNo").val(),
                    Stat: action == "submit" ? "1" : "0"
                };
            }
        });



        if (arrDetail.length == 0) {
            alert('未填写任何数据！');
            return false;
        }

        var jsonEntity = JSON.stringify(arrDetail);

        var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        var postaction = "saveallmemsallotdetails";
        var data = { "action": postaction, "data": jsonEntity };
        $.ajaxSetup({
            async: false
        });
        $.post(url, data, function (result) {

            if (result) {
                if (result == "1") {
                    //设置成功！
                    if (action == "save") {
                        alert("数据保存成功！");

                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.ReadMsg();
                    }
                    else {
                        //确认提交
                        if (!confirm('确认要提交？')) {
                            return false;
                        }

                        alert("数据提交成功");
                        //改变消息状态
                        var msg = new MessageComm($("#msgno").val());
                        msg.DoneMsg();
                      
                    }

                    location.replace(location.href);
                }
            }
            else {
                //设置成功！
                alert("设置失败请重试！");
            }
        });
    }






    //复制合计到头部合计
    var setHeadHeji = function () {

        var arrFooter = $("#tbDataOne tfoot tr").find("td");
        var arrHeader = $("#tbDataOne thead tr").eq(1).find("th");
        if (arrFooter.length > 0) {
            arrFooter.each(function (i) {

                arrHeader.eq(i).text(arrFooter.eq(i).text());
            });
        }
    }


    //给table排序
    var initOrderTable = function () {

        $("#tbDataOne").dataTable({

            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [0] },
                { "bSortable": false, "aTargets": [1] },
                { "bSortable": false, "aTargets": [2] },
                { "bSortable": true, "aTargets": [3] },
                { "bSortable": true, "aTargets": [4] },
                { "bSortable": true, "aTargets": [5] },
                { "bSortable": true, "aTargets": [6] },
                { "bSortable": true, "aTargets": [7] },
                { "bSortable": true, "aTargets": [8] },
                { "bSortable": true, "aTargets": [9] },
                { "bSortable": true, "aTargets": [10] },
                { "bSortable": true, "aTargets": [11] },
                { "bSortable": true, "aTargets": [12] },
                { "bSortable": true, "aTargets": [13] },
                { "bSortable": true, "aTargets": [14] },
                { "bSortable": true, "aTargets": [15] },
                { "bSortable": false, "aTargets": [16] },
                { "bSortable": false, "aTargets": [17] }
              
            ],
            "iDisplayLength": 30
        });


        jQuery('#tbDataOne_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
        jQuery('#tbDataOne_wrapper .dataTables_length select').addClass("form-control input-small").parent().hide(); // modify table per page dropdown
        //jQuery('#tbData_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
    }

    //发起归档
    var saveBak = function () {

        if (!confirm("确定要发起年度总奖金归档吗？")) {
            return false;
        }
        if (_containerItem.length == 0) {
            alert("没有要归档的信息！");
            return false;
        }

        var renwuid = $("#hidRenwuno").val();

        //获取归档统计记录
        var arryData = new Array();
        _containerItem.each(function (index, domEle) {

            var rowdata = $(this);

            arryData[index] = {
                ID: 0,
                RenwuID: renwuid,
                UnitName: $.trim(rowdata.children().eq(0).text()),
                UserName: $.trim(rowdata.children().eq(1).text()),
                IsFired: $.trim(rowdata.children().eq(2).text()),
                BMJLTZ: $.trim(rowdata.children().eq(3).text()),
                GSTZ: $.trim(rowdata.children().eq(4).text()),
                YFJJ: $.trim(rowdata.children().eq(16).text()),
                XMJJJS: $.trim(rowdata.children().eq(5).text()),
                BMJJJS: $.trim(rowdata.children().eq(6).text()),
                XMJJ: $.trim(rowdata.children().eq(7).text()),
                BMNJJ: $.trim(rowdata.children().eq(8).text()),
                XMGLJ: $.trim(rowdata.children().eq(9).text()),
                YXYG: $.trim(rowdata.children().eq(10).text()),
                TCGX: $.trim(rowdata.children().eq(11).text()),
                BIM: $.trim(rowdata.children().eq(12).text()),
                ZCFY:$.trim(rowdata.children().eq(13).text()),
                GHBZ: $.trim(rowdata.children().eq(14).text()),
                HJ: $.trim(rowdata.children().eq(17).text()),
            }

        });

        var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
        var action = "allbak";

        var jsonData = JSON.stringify(arryData);

        var data = { "action": action, "isNew": 0, "data": jsonData, "unitname": $("#ctl00_ContentPlaceHolder1_drp_unit3 option:selected").text() };

        $.post(url, data, function (result) {

            if (result) {
                if (result == 1) {
                    alert("年度总奖金数据归档成功!");

                    $("#btnBak").attr('class', 'btn btn-sm default disabled').text('已归档');
                }
                else if (result == 2) {
                    alert('已归档，无需重复归档！');
                }
                else if (result == 3) {
                    alert("考核任务未归档，请先归档考核任务！");
                }

            }
            else {
                alert("错误！");
            }
        });
    }

    return {
        init: function () {
            pageLoad();

        }
    }
}();