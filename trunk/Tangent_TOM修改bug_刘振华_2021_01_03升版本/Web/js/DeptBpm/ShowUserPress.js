﻿
var userPress = function () {

    var pageLoad = function () {

        //发起部门经理
        $("#btnSendToMng").click(function () {

            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var action = "sendmsgtomng";
            //发送消息
            var senduserid=$("#hidUserSysNo").val();
            //部门ID
            var unitid = $("#hidUnitID").val();
            //任务ID
            var renwuid = $("#hidRenwuno").val();
            //部门考核ID
            var kaoheunitid = $("#hidKaoheUnitID").val();

            var data = { "action": action, "uid": unitid, "urenwuno": renwuid, "ukaoheid": kaoheunitid, "senduserid": senduserid ,"issendmng":"1"};

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("部门经理调整消息发送成功！");
                        $("#btnSendToMng").attr("class","btn btn-sm red disabled");
                    }
                    else
                    {
                        //设置成功！
                        alert("发送失败，未设置部门经理！");
                        return false;
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
    }

    return {
        init: function () {
            pageLoad();
        }
    }
}();