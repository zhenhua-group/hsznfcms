﻿
var unitAllot = function () {

    var pageLoad = function () {

        //权重值
        $(":text", $("#tbDataOne")).change(function () {

            var obj = $(this);
            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;

            if (!reg_math.test(obj.val())) {
                obj.val("");
                return false;
            }

            //计算占比
            changeAllotVal(obj);
        }).focus(function () {
            $(this).select();
        });

        //保存
        $("#btnSave").click(function () {
            saveAllotDetail('save');
        });
        //提交
        $("#btnSubmit").click(function () {
            saveAllotDetail('submit');
        });
        //加载
        if ($("#hidSaved").val() == "True") {

            if ($("#hidSubmit").val() == "False") {
                changeAllotVal();
            }
        }

        //插入备档数据‘
        $("#btnBak").click(function () {

            if ($("#hidSubmit").val() == "False") {
                alert("数据没有提交！无法归档！");
                return false;
            }

            SaveBakData();
        });
    }
    //计算占比
    var changeAllotVal = function (obj) {

        //所有部门绩效
        var allunitjx = 0;
        //是否有空值
        var isnull = false;
        $(":text", $("#tbDataOne")).each(function (i, item) {
            var val = $(item).val();
            //计算绩效
            if (val != "") {
                allunitjx += parseFloat(val);
            }
            else {
                isnull = true;
            }
        });
        //总奖金数 元
        var allcount = parseFloat($("#spAllcount").text());
        //计算
        if (!isnull) {
            //计算九个部门的平均工资*绩效
            var allUnitGzAndJx = 0;
            $("#tbDataOne tbody tr").each(function (i, item) {
                //本部门的平均月工资
                var unitval = parseFloat($(item).find(":text").attr("val"));
                //本部门绩效
                var curunitjx = parseFloat($(item).find(":text").val());

                allUnitGzAndJx += unitval * curunitjx;
            });


            //计算
            $("#tbDataOne tbody tr").each(function (i, item) {
                var result = 0;
                var unitallotcount = 0;
                var monthbeishu = 0;
                var projallotbi = 0;
                var unitIn = 0;
                //本部门的平均月工资
                var unitval = parseFloat($(item).find(":text").attr("val"));
                //所有部门平均工资
                var allunitval = parseFloat($(item).find(":text").attr("valall"));
                //本部门绩效
                var curunitjx = parseFloat($(item).find(":text").val());

                if (allUnitGzAndJx != 0) {
                    result = (unitval * curunitjx) / allUnitGzAndJx;
                }
                else {
                    result = 0;
                }
                //部门奖金占比
                $(item).find("td").eq(3).find("span").text((result * 100).toFixed(2));
                //总奖金
                unitallotcount = result * allcount;
                //舍去百位
                unitallotcount = Math.round(unitallotcount * 0.01) * 100;

                $(item).find("td").eq(4).find("span").text(Math.round(unitallotcount));
                //工资倍数
                if (unitval != 0) {
                    monthbeishu = unitallotcount / unitval;
                }
                else {
                    monthbeishu = 0;
                }
                $(item).find("td").eq(5).find("span").text(monthbeishu.toFixed(2));
                //项目奖金占比
                var projallotcount = parseFloat($(item).find("td").eq(6).find("span").text());
                if (unitallotcount != 0) {
                    projallotbi = projallotcount / unitallotcount;
                }
                $(item).find("td").eq(7).find("span").text((projallotbi * 100).toFixed(2));
                //部门内奖金
                unitIn = unitallotcount - projallotcount;
                //百位四舍五入 2016年9月29日
                unitIn = Math.round(unitIn * 0.01) * 100;
                $(item).find("td").eq(8).find("span").text(Math.round(unitIn));
            });
        }
    }

    //保存报表
    var saveAllotDetail = function (action) {
        var isnull = false;
        $(":text", $("#tbDataOne")).each(function (i, item) {
            var val = $(item).val();
            //计算绩效
            if ($.trim(val) == "") {
                isnull = true;
            }
        });

        if (isnull) {
            alert("请完整填写所有部门绩效！");
            return false;
        }
        if (!confirm('确认要提交？')) {
            return false;
        }
      
        //对象
        var arrDetail = new Array();
        var index = 0;
        $("#tbDataOne tbody tr").each(function (i) {

            var obj = $(this);

            if (obj.attr("class") != "active") {
                arrDetail[index++] = {
                    ID: "0",
                    RenwuID: $("#hidRenwuNo").val(),
                    UnitID: obj.find("td").eq(0).children("span").attr("unitid"),
                    UnitName: $.trim(obj.find("td").eq(0).children("span").text()),
                    AvgGongzi: obj.find("td").eq(1).children("span").text(),
                    Unitjx: obj.find("td").eq(2).children("input").val(),
                    Unitjiangbi: obj.find("td").eq(3).children("span").text(),
                    Unitjiangall: obj.find("td").eq(4).children("span").text(),
                    MonthBei: obj.find("td").eq(5).children("span").text(),
                    ProjCount: obj.find("td").eq(6).children("span").text(),
                    ProjCountbi: obj.find("td").eq(7).children("span").text(),
                    Unitjiang: obj.find("td").eq(8).children("span").text(),
                    Stat: action == "save" ? "0" : "1"
                };
            }
        });

        var jsonEntity = JSON.stringify(arrDetail);

        var url = "/HttpHandler/DeptBpm/ProjDeptHandler.ashx";
        var action = "saveunitreport";
        var data = { "action": action, "data": jsonEntity };
        //确认提交
        

            $.post(url, data, function (result) {

                if (result) {
                    
                        if (result == "1") {
                            //设置成功！
                            alert("部门绩效调整数据保存成功！");
                            //按钮不可用
                            $("#btnSave").attr("class", "btn btn-sm default disabled");
                            location.reload();
                        }
                    
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
  

       
    }

    //插入备档数据
    var SaveBakData = function () {

        var renwuName = $("#ctl00_ContentPlaceHolder1_drpRenwu option:checked").text();

        if (!confirm("确认要备档" + renwuName + "的部分奖金计算数据吗？")) {
            return false;
        }

        //考核任务id
        var renwuid = $("#ctl00_ContentPlaceHolder1_drpRenwu").val();
        //初始录入总金额
        var allCount = $("#spAllcount").text();
        var arryData = new Array();
        $("#tbDataOne tbody tr").each(function (index, domEle) {

            var rowdata = $(this);
            arryData[index] = {
                ID: 0,
                RenwuID: renwuid,
                RenwuName: renwuName,
                UnitName: $.trim(rowdata.children().eq(0).text()),
                PJYGZ: $.trim(rowdata.children().eq(1).text()),
                BMJX: $.trim(rowdata.children().eq(2).text()),
                BMJJZB: $.trim(rowdata.children().eq(3).text().replace(/%/, '')),
                BMZJJ: $.trim(rowdata.children().eq(4).text()),
                YGZBS: $.trim(rowdata.children().eq(5).text()),
                XMJJ: $.trim(rowdata.children().eq(6).text()),
                XMJJZB: $.trim(rowdata.children().eq(7).text()).replace(/%/, ''),
                BMNJJ: $.trim(rowdata.children().eq(8).text()),
                BeforeYear: $.trim(rowdata.children().eq(9).text()),
                AfterYear: $.trim(rowdata.children().eq(10).text()),
                AllCount: allCount,
                UnitID: $.trim(rowdata.children().eq(0).find('span').attr('unitid'))
            }
        });

        var url = "/HttpHandler/DeptBpm/ProjDeptHandler.ashx";
        var action = "bak";
        var jsonData = JSON.stringify(arryData);
        var data = { "action": action, "isNew": 0, "data": jsonData, "renwuid": renwuid };

        $.post(url, data, function (result) {

            if (result) {
                if (result == 1) {
                    alert("部门奖金分配归档成功");

                    $("#ctl00_ContentPlaceHolder1_btn_Search").trigger('click');
                }
                else if (result == 2) {
                    alert('部门奖金分配已归档，无需重复归档');
                }
                else if (result == 3) {
                    alert("考核任务未归档，部门奖金分配不能进行归档！");
                }
                else if (result == 2) {
                    alert("部门奖金分配已归档，无需重复归档！");
                }
                else {
                    alert("数据归档失败！");
                }

            }
            else {
                alert("错误！");
            }
        });
    }


    return {
        init: function () {
            pageLoad();
        }
    }
}();