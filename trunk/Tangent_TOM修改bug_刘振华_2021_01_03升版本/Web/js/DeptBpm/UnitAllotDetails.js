﻿
var unitAllot = function () {

    var pageLoad = function () {

        $("#tbDataOne tbody a").click(function () {

            var obj = $(this);
            //发送重新调整消息
            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var action = "resendmsgtomng";
            //发送消息
            var senduserid = $("#hidUserSysNo").val();
            //部门ID
            var unitid = obj.attr("unitid");
            //任务ID
            var renwuid = $("#hidRenwuID").val();
            //部门考核ID
            var kaoheunitid = obj.attr("kaoheid");
            //接收人
            var fromuserid = obj.attr("fromuserid");

            var data = { "action": action, "uid": unitid, "urenwuno": renwuid, "ukaoheid": kaoheunitid, "senduserid": senduserid, "fromuserid": fromuserid };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("部门经理重新调整消息发送成功！");
                        obj.attr("class", "btn default btn-xs red-stripe disabled");
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });

        //发起年度总奖金
        $("#btnSave").click(function () {

            $(this).attr("class", "btn btn-sm red disabled");

            //发送重新调整消息
            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var action = "sendmsgtoallallot";
            //发送消息
            var senduserid = $("#hidUserSysNo").val();
            //任务ID
            var renwuid = $("#hidRenwuID").val();
            //接收人（张维）
            var fromuserid = 1442;
            var Gid = 1;
            var data = { "action": action, "urenwuno": renwuid, "senduserid": senduserid, "fromuserid": fromuserid,"Gid":Gid };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("年度奖金系数调整消息已成功发送至【张维】【admin】！");
                        location.reload();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
    }

    return {
        init: function () {
            pageLoad();
        }
    }
}();