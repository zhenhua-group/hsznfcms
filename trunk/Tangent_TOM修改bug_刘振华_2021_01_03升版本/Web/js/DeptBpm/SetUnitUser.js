﻿
var SetUsers = function () {

    showDivDialogClass.UserRolePower = {
        "previewPower": "1",
        "userSysNum": "0",
        "userUnitNum": "0",
        "notShowUnitList": ""
    };

    //页面加载
    var pageLoad = function () {
        //全选按钮
        $("#chkAll").click(function () {
            var obj = $(this);

            if (!obj.get(0).checked) {
                $(":checkbox").not(this).attr("checked", !obj.checked).parent().attr("class", "");
            }
            else {
                $(":checkbox").not(this).attr("checked", true).parent().attr("class", "checked");
            }
        });
        //判断是否已经保存
        if ($("#divTips").text().indexOf('已保存') > -1) {
            $("#btnSetMems").html("更新参与考核人员&nbsp;<i class=\"fa fa-save\">");
        }
        //保存设置
        $("#btnSetMems").click(function () {
            //数组
            var memArr = new Array();

            $("#tbData tbody tr").each(function (index) {

                var obj = $(this);
                if (obj.find("td").eq(0).find(":checkbox").attr("checked") == "checked") {
                    memArr[index] = {
                        "ID": "0",
                        "KaoHeUnitID": $("#hidKaoHeUnitId").val(),
                        "memID": obj.find("td").eq(0).find(":checkbox").attr("uid"),
                        "memName": obj.find("td").eq(0).find(":checkbox").attr("uname"),
                        "memUnitID": $("#hidUnitid").val(),
                        "memUnitName": $("#hidUnitName").val(),
                        "Statu": obj.find("td").eq(4).find(":selected").val(),
                        "MagRole": obj.find("td").eq(5).find(":hidden").val(),
                        "NormalRole": obj.find("td").eq(6).find(":hidden").val(),
                        "Weights": obj.find("td").eq(7).find(":hidden").val(),
                        "IsOutUnitID": obj.find("td").eq(8).find(":hidden").val()
                    };
                }
            });

            var jsonData = JSON.stringify(memArr);

            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var action = "savemems";
            var data = { "action": action, "data": jsonData };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("保存考核人员成功！");
                        $("#divTips").html("<span class=\"badge badge-success\">已保存考核人员</span>");
                        location.href = $("#hidLocalUrl").val()+"&timespan="+new Date().getTime();
                    }
                }
                else {
                    //设置成功！
                    alert("保存失败请重试！");
                }
            });
        });
        //添加外所人员
        $("#btnSetOutmems").click(function () {
            //先赋值
            showDivDialogClass.SetParameters({
                "prevPage": "gcfzr_prevPage",
                "firstPage": "gcfzr_firstPage",
                "nextPage": "gcfzr_nextPage",
                "lastPage": "gcfzr_lastPage",
                "gotoPage": "gcfzr_gotoPageIndex",
                "allDataCount": "gcfzr_allDataCount",
                "nowIndex": "gcfzr_nowPageIndex",
                "allPageCount": "gcfzr_allPageCount",
                "gotoIndex": "gcfzr_pageIndex",
                "pageSize": "10"
            });
            var unit_ID = $("#select_gcFzr_Unit").val();
            if (unit_ID < 0 || unit_ID == null) {
                //绑定工程负责部门
                showDivDialogClass.GetDataByAJAX("getDataToDivDialog", "", "true", "0", "gcfzrCprUnit", CprTypeUnitCallBack);
            }

        });
    }
    /*弹出选择人员*/
    function CprTypeUnitCallBack(result) {
        if (result != null) {

            var data = result.ds;
            var gcFzr_UnitOptionHtml = '<option value="-1">---------请选择---------</option>';
            $.each(data, function (i, n) {
                //过滤本部门
                if (n.unit_ID != $("#hidUnitid").val()) {
                    gcFzr_UnitOptionHtml += '<option value="' + n.unit_ID + '">' + n.unit_Name + '</option>';
                }
            });
            $("#select_gcFzr_Unit").html(gcFzr_UnitOptionHtml);
            //注册部门选项改变事件
            showDivDialogClass.SetParameters({ "pageSize": "10" });
            $("#select_gcFzr_Unit").unbind('change').change(function () {
                var unit_ID = $("#select_gcFzr_Unit").val();
                if (Math.abs(unit_ID) > 0) {
                    showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", "1", "gcfzrCprMem", BindGcfzrDataCallBack);
                    BindAllDataCountGcfzr(unit_ID);
                }
            });
        }
    }
    function BindGcfzrDataCallBack(result) {
        if (result != null) {

            var obj = result.ds;
            var gcFzrMemTableHtml;
            $("#gcFzr_MemTable tr:gt(0)").remove();
            $.each(obj, function (i, n) {
                var oper = "<span rel='" + n.mem_Login + "' style=\"color:blue;cursor:pointer\" data-dismiss=\"modal\" >选择</span>";
                gcFzrMemTableHtml = "<tr style='text-align:center'><td>" + (i + 1) + "</td><td>" + n.mem_Name + "</td><td>" + oper + "</td></tr>";
                $("#gcFzr_MemTable").append(gcFzrMemTableHtml);
                $("#gcFzr_MemTable span:last").click(function () {
                    //大表
                    var tableContainer = $("#tbData");
                    var rowIndex = tableContainer.find("tr").length;
                    var unitName = tableContainer.find("tr").eq(1).find("td").eq(3).text();

                    //查询当前选择用户的权重
                    var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
                    var action = "getmemexten";
                    var data = { "action": action, "id": n.mem_ID };
                    var memsArr = new Array();
                    $.get(url, data, function (rlt) {

                        if (rlt) {
                            if (rlt != "1") {
                                //得到
                                memsArr = eval("(" + rlt + ")");
                                //创建
                                var rowHtml = "";
                                rowHtml += "<tr>";
                                rowHtml += "<td>";
                                rowHtml += "<div class=\"checker\"><span class=\"checked\">";
                                rowHtml += "<input type=\"checkbox\" name=\"name\" value=\"0\" class=\"chkitem\" uname=\"" + n.mem_Name + "\" uid=\"" + n.mem_ID + "\" checked=\"checked\"></span></div></td>";
                                rowHtml += "<td>" + rowIndex + "</td>";
                                rowHtml += "<td>" + n.mem_Name + "</td>";
                                rowHtml += "<td>" + unitName + "</td>";
                                rowHtml += "<td>";
                                rowHtml += "<select>";
                                rowHtml += "<option value=\"0\" selected=\"selected\">是</option>";
                                rowHtml += "<option value=\"1\">否</option>";
                                rowHtml += "</select></td>";
                                rowHtml += "<td><input type=\"hidden\" name=\"name\" value=\"" + memsArr[0].RoleIdMag + "\"><span id=\"spRoleMag\">" + memsArr[0].RoleMagName + "</span></td>";
                                rowHtml += "<td><input type=\"hidden\" name=\"name\" value=\"" + memsArr[0].RoleIdTec + "\"><span id=\"spRoleTec\">" + memsArr[0].RoleTecName + "</span></td>";
                                //如果是部门经理
                                if (memsArr[0].RoleIdMag == 5) {
                                    rowHtml += "<td><input type=\"hidden\" name=\"name\" value=\"5\"><span id=\"spWeights\">5</span></td>";
                                }
                                else {
                                    rowHtml += "<td><input type=\"hidden\" name=\"name\" value=\"5\"><span id=\"spWeights\">5</span></td>";
                                    //rowHtml += "<td><input type=\"hidden\" name=\"name\" value=\"" + memsArr[0].Weights + "\"><span id=\"spWeights\">" + memsArr[0].Weights + "</span></td>";
                                }
                                rowHtml += "<td><input type=\"hidden\" name=\"name\" value=\"" + n.mem_Unit_ID + "\"><span id=\"spOutUnit\">" + $("#select_gcFzr_Unit option:selected").text() + "</span></td>";
                                rowHtml += "</tr>";

                                //追加行
                                tableContainer.append(rowHtml);
                            }
                            else {
                                alert("查询无权重信息请重试！");
                            }
                        }
                        else {
                            //设置成功！
                            alert("保存失败请重试！");
                        }
                    });

                });
            });

        }
    }
    function BindAllDataCountGcfzr(unit_ID) {
        //设置参数
        showDivDialogClass.SetParameters({
            "prevPage": "gcfzr_prevPage",
            "firstPage": "gcfzr_firstPage",
            "nextPage": "gcfzr_nextPage",
            "lastPage": "gcfzr_lastPage",
            "gotoPage": "gcfzr_gotoPageIndex",
            "allDataCount": "gcfzr_allDataCount",
            "nowIndex": "gcfzr_nowPageIndex",
            "allPageCount": "gcfzr_allPageCount",
            "gotoIndex": "gcfzr_pageIndex",
            "pageSize": "10"
        });
        //获取总数据
        showDivDialogClass.GetDataTotalCount("getDataAllCount", unit_ID, "true", "gcfzrCprMem", GetGcfzrAllDataCount);
        //注册事件,先注销,再注册
        $("#gcFzr_ForPageDiv span").unbind('click').click(function () {
            var isRegex = showDivDialogClass.IsRegex_ByPage($(this).attr("id"));
            if (isRegex) {
                var pageIndex = $("#gcfzr_nowPageIndex").text();
                showDivDialogClass.GetDataByAJAX("getDataToDivDialog", unit_ID, "true", pageIndex, "gcfzrCprMem", BindGcfzrDataCallBack);
            }
        });
    }
    function GetGcfzrAllDataCount(result) {
        if (result > 0) {
            showDivDialogClass.BindPageValueFirst(result);
        } else {
            $("#gcFzr_MemTable tr:gt(0)").remove();
            $("#gcfzr_allDataCount").text(0);
            $("#gcfzr_nowPageIndex").text(0);
            $("#gcfzr_allPageCount").text(0);
            NoDataMessageOnTable("gcFzr_MemTable", 3);
        }
    }
    /*弹出选择人员 END*/

    return {
        init: function () {
            pageLoad();
        }
    };
}();