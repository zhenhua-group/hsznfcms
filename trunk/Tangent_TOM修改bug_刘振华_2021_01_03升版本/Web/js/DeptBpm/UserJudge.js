﻿
var UserJudge = function () {
    var pageLoad = function () {

        //输入字符串为数字
        $("#tbData tbody :text").live('keyup', function () {
            var reg_math = /^(?:[1-9][0-9]*(?:\.[0-9]+)?|0(?:\.[0-9]+)?)$/;
            //输入.
            if ($(this).val().indexOf('.') > -1) {
                if ($(this).val().indexOf('.') <= 0 || $(this).val().split('.').length > 2) {
                    $(this).val("");
                    return false;
                }
            }
            //小数点后没有数不计算
            if ($(this).val().indexOf('.') == $(this).val().length - 1) {
                return false;
            }

            if (!reg_math.test($(this).val())) {
                $(this).val("");
                return false;
            }

            var obj = $(this);
            if (parseFloat(obj.val()) > 5) {
                alert("评分不能超过5分");
                $(this).val("5");
            }
            //计算得分
            sumScore(obj);
        }).blur(function () {
            var obj = $(this);
            //计算得分
            sumScore(obj);
        });

        //保存
        $("#btnSave").click(function () {
            var memArr = new Array();

            $("#tbData tbody tr").each(function (index) {
                var obj = $(this);

                memArr[index] = {
                    "MemID": obj.find("td").eq(0).children("span").attr("memid"),
                    "MemName": obj.find("td").eq(0).children("span").attr("memname"),
                    "Judge1": obj.find("td").eq(1).children(":text").val() == '' ? null : obj.find("td").eq(1).children(":text").val(),
                    "Judge2": obj.find("td").eq(2).children(":text").val() == '' ? null : obj.find("td").eq(2).children(":text").val(),
                    "Judge3": obj.find("td").eq(3).children(":text").val() == '' ? null : obj.find("td").eq(3).children(":text").val(),
                    "Judge4": obj.find("td").eq(4).children(":text").val() == '' ? null : obj.find("td").eq(4).children(":text").val(),
                    "Judge5": obj.find("td").eq(5).children(":text").val() == '' ? null : obj.find("td").eq(5).children(":text").val(),
                    "Judge6": obj.find("td").eq(6).children(":text").val() == '' ? null : obj.find("td").eq(6).children(":text").val(),
                    "Judge7": obj.find("td").eq(7).children(":text").val() == '' ? null : obj.find("td").eq(7).children(":text").val(),
                    "Judge8": obj.find("td").eq(8).children(":text").val() == '' ? null : obj.find("td").eq(8).children(":text").val(),
                    "Judge9": obj.find("td").eq(9).children(":text").val() == '' ? null : obj.find("td").eq(9).children(":text").val(),
                    "Judge10": obj.find("td").eq(10).children(":text").val() == '' ? null : obj.find("td").eq(10).children(":text").val(),
                    "Judge11": obj.find("td").eq(11).children(":text").val() == '' ? null : obj.find("td").eq(11).children(":text").val(),
                    "Judge12": obj.find("td").eq(12).children(":text").val() == '' ? null : obj.find("td").eq(12).children(":text").val(),
                    "SaveStatu": "0",
                    "KaoHeUnitID": $("#hidKaoHeUnitID").val(),
                    "InsertUserId": $("#hidInserUserID").val(),
                    "JudgeCount": obj.find("td").eq(13).children("span").text()
                };
            });

            var jsonData = JSON.stringify(memArr);

            var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
            var action = "savereport";
            var data = { "action": action, "data": jsonData };

            $.post(url, data, function (result) {

                if (result) {
                    if (result == "1") {
                        //设置成功！
                        alert("用户评分表保存成功！");
                        location.reload();
                    }
                }
                else {
                    //设置成功！
                    alert("设置失败请重试！");
                }
            });
        });
        //提交
        $("#btnSave2").click(function () {

            if (confirm("确认要提交？")) {
                var memArr = new Array();

                $("#tbData tbody tr").each(function (index) {
                    var obj = $(this);

                    memArr[index] = {
                        "MemID": obj.find("td").eq(0).children("span").attr("memid"),
                        "MemName": obj.find("td").eq(0).children("span").attr("memname"),
                        "Judge1": obj.find("td").eq(1).children(":text").val() == '' ? null : obj.find("td").eq(1).children(":text").val(),
                        "Judge2": obj.find("td").eq(2).children(":text").val() == '' ? null : obj.find("td").eq(2).children(":text").val(),
                        "Judge3": obj.find("td").eq(3).children(":text").val() == '' ? null : obj.find("td").eq(3).children(":text").val(),
                        "Judge4": obj.find("td").eq(4).children(":text").val() == '' ? null : obj.find("td").eq(4).children(":text").val(),
                        "Judge5": obj.find("td").eq(5).children(":text").val() == '' ? null : obj.find("td").eq(5).children(":text").val(),
                        "Judge6": obj.find("td").eq(6).children(":text").val() == '' ? null : obj.find("td").eq(6).children(":text").val(),
                        "Judge7": obj.find("td").eq(7).children(":text").val() == '' ? null : obj.find("td").eq(7).children(":text").val(),
                        "Judge8": obj.find("td").eq(8).children(":text").val() == '' ? null : obj.find("td").eq(8).children(":text").val(),
                        "Judge9": obj.find("td").eq(9).children(":text").val() == '' ? null : obj.find("td").eq(9).children(":text").val(),
                        "Judge10": obj.find("td").eq(10).children(":text").val() == '' ? null : obj.find("td").eq(10).children(":text").val(),
                        "Judge11": obj.find("td").eq(11).children(":text").val() == '' ? null : obj.find("td").eq(11).children(":text").val(),
                        "Judge12": obj.find("td").eq(12).children(":text").val() == '' ? null : obj.find("td").eq(12).children(":text").val(),
                        "SaveStatu": "1",
                        "KaoHeUnitID": $("#hidKaoHeUnitID").val(),
                        "InsertUserId": $("#hidInserUserID").val(),
                        "JudgeCount": obj.find("td").eq(13).children("span").text()
                    };
                });

                var jsonData = JSON.stringify(memArr);

                var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
                var action = "savereport";
                var data = { "action": action, "data": jsonData };

                $.post(url, data, function (result) {

                    if (result) {
                        if (result == "1") {
                            //设置成功！
                            alert("用户评分表提交成功！");
                            //改变消息状态
                            var msg = new MessageComm($("#msgno").val());
                            msg.DoneMsg();

                            location.reload();
                        }
                    }
                    else {
                        //设置成功！
                        alert("设置失败请重试！");
                    }
                });
            }
        });
        //如果已经提交
        if ($("#hidIsSubmit").val() == "True") {
            $("#btnSave").hide();
            $("#btnSave2").hide();

            //改变消息状态
            var msg = new MessageComm($("#msgno").val());
            msg.DoneMsg();
        }

        //初次加载为已读
        var msg = new MessageComm($("#msgno").val());
        msg.ReadMsg();
    }

    var sumScore = function (obj) {
        //计算结果
        var result = 0;
        //总权重
        var allWgts = 79;
        //循环分
        var tdobjs = obj.parent().parent().find("td");
        tdobjs.each(function (index) {
            //if (index > 0 && index < 13) {
            var obj = $(this);
            var score = 0;
            if (obj.children(":text").val() != '' && obj.children(":text").val() != undefined) {
                score = parseFloat(obj.children(":text").val());
            }
            else {
                return true;
            }
            var weights = parseFloat(obj.children(":text").attr("weights"));

            result += score * weights;
            //}
        });

        result = 20 * result / allWgts;

        obj.parent().parent().find("td").eq(13).children("span").text(parseFloat(result.toString()).toFixed(2));
    }

    return {
        init: function () {
            pageLoad();
        }
    }
}();