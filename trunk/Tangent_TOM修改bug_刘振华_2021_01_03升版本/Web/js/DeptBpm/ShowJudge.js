﻿
var showJudge = function () {

    var pageLoad = function () {

        $("#btnSync").click(function () {

            if (confirm("确定要同步数据？")) {
                var url = "/HttpHandler/DeptBpm/DeptBpmHandler.ashx";
                var action = "snyc";
                var unitid = $("#hidUnitID").val();
                var userid = $("#hidInserUserID").val();
                var kaoheunitid = $("#hidKaoHeUnitID").val();
                var data = { "action": action, "ukaoheid": kaoheunitid, "uid": unitid, "insertmemid": userid };

                $.post(url, data, function (result) {

                    if (result) {
                        if (result == "0") {
                            alert("部门经理未填写考核单无法同步！");
                        }
                        else if (result == "00") {
                            alert("部门经理考核单评价不完整无法同步！");
                        }
                        else if (result == "000") {
                            alert("未找到部门经理的考核数据无法同步！");
                        }
                        else if (result == "1") {
                            alert("数据已经同步！");
                            location.reload();
                        }
                    }
                    else {
                        //设置成功！
                        alert("同步失败请重试！");
                    }
                });
            }
        });

        $("#btnOrderTable").click(function () {

            initOrderTable();
        });

        $("#btnDefault").click(function () {

            window.location.reload();
        });
    }

    //给table排序
    var initOrderTable = function () {

        $("#tbData").dataTable({
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [0] },
                { "bSortable": true, "aTargets": [1] },
                { "bSortable": true, "aTargets": [2] },
                { "bSortable": true, "aTargets": [3] },
                { "bSortable": true, "aTargets": [4] },
                { "bSortable": true, "aTargets": [5] },
                { "bSortable": true, "aTargets": [6] },
                { "bSortable": true, "aTargets": [7] },
                { "bSortable": true, "aTargets": [8] },
                { "bSortable": true, "aTargets": [9] },
                { "bSortable": true, "aTargets": [10] },
                { "bSortable": true, "aTargets": [11] },
                { "bSortable": true, "aTargets": [12] },
                { "bSortable": false, "aTargets": [13] },
                { "bSortable": true, "aTargets": [14] },
                { "bSortable": false, "aTargets": [15] }
            ],
            "iDisplayLength": 50
        });


        jQuery('#tbData_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
        jQuery('#tbData_wrapper .dataTables_length select').addClass("form-control input-small").parent().hide(); // modify table per page dropdown
        //jQuery('#tbData_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
    }

    return {
        init: function () {
            pageLoad();

            // 2016年10月20日 
            //initOrderTable();
        }
    };
}();