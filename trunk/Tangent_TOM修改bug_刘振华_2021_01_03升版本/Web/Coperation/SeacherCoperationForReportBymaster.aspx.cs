﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Data;

namespace TG.Web.Coperation
{
    public partial class SeacherCoperationForReportBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定数据
                BindDataList();
                BindCprType();
                Bindgv_Coperation();
            }
        }
        //是否检查权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //检查浏览权限
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //绑定客户类型
        protected void BindCprType()
        {
            //合同类型
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.drp_type.DataSource = bll_dic.GetList(str_where);
            this.drp_type.DataTextField = "dic_Name";
            this.drp_type.DataValueField = "ID";
            this.drp_type.DataBind();
        }
        /// <summary>
        /// 绑定下拉框
        /// </summary>
        protected void BindDataList()
        {
            //承接部门
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            //不显示的单位
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                strWhere += " 1=1 AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            else
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //查询
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Bindgv_Coperation();
        }
        //绑定查询数据
        private void Bindgv_Coperation()
        {
            TG.BLL.cm_Coperation_back bll = new TG.BLL.cm_Coperation_back();

            StringBuilder builder1 = new StringBuilder("");
            //if (this.txt_cprname.Text != "")
            //{
            //    string keyname = TG.Common.StringPlus.SqlSplit(this.txt_cprname.Text.Trim());
            //    builder1.Append(" And cpr_Name LIKE '%" + keyname + "%'");
            //}
            //if (this.drp_type.SelectedIndex > 0)
            //{
            //    builder1.Append(" And cpr_Type='" + this.drp_type.SelectedItem.Text.Trim() + "'");
            //}
            //if (this.drp_unit.SelectedIndex > 0)
            //{
            //    builder1.Append(" And cpr_Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "'");
            //}
            //if (this.txt_start.Text != "" && this.txt_end.Text != "")
            //{
            //    builder1.Append(" And (RegTime BETWEEN '" + this.txt_start.Text.Trim() + "' AND '" + this.txt_end.Text.Trim() + "')");
            //}

            //  if (builder1.ToString() != "")
            //  {
            GetPreviewPowerSql(ref builder1);
            this.hid_where.Value = builder1.ToString();
            //数量
            //this.AspNetPager1.RecordCount = int.Parse(bll.GetListPageProcCount(builder1.ToString()).ToString());
            //排序
            //  string orderString = " order by cpr_ID DESC ";
            // builder1.Append(orderString);
            //分页
            // this.gv_Coperation.DataSource = bll.GetListByPageProc(builder1.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            //  this.gv_Coperation.DataBind();
            // }
        }
        TG.BLL.tg_member bll_mem = new TG.BLL.tg_member();
        protected void gv_Coperation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string mem_id = e.Row.Cells[6].Text.Trim();
                if (mem_id != "&nbsp;" && mem_id != "-1")
                {
                    e.Row.Cells[6].Text = bll_mem.GetModel(int.Parse(mem_id)).mem_Name;
                }
            }
        }

        protected void AspNetPager1_PageChanged(object src, EventArgs e)
        {
            Bindgv_Coperation();
        }

        protected void btn_export_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();
            StringBuilder strWhere = new StringBuilder("1=1");
            if (this.txt_cprname.Value != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_cprname.Value.Trim());
                strWhere.Append(" And cpr_Name LIKE '%" + keyname + "%'");
            }
            if (this.drp_type.SelectedIndex > 0)
            {
                strWhere.Append(" And cpr_Type='" + this.drp_type.SelectedItem.Text.Trim() + "'");
            }
            if (this.drp_unit.SelectedIndex > 0)
            {
                strWhere.Append(" And cpr_Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "'");
            }
            if (this.txt_start.Value != "" && this.txt_end.Value != "")
            {
                strWhere.Append(" And (RegTime BETWEEN '" + this.txt_start.Value.Trim() + "' AND '" + this.txt_end.Value.Trim() + "')");
            }

         
        
            
          
            //判断个人或全部
            GetPreviewPowerSql(ref strWhere);

            DataTable dt = bll.GetList(strWhere.ToString()).Tables[0];

            ExportDataToExcel(dt, "~/TemplateXls/ReportCoperationList.xls", "合同报备高级查询列表");

        }
        private void ExportDataToExcel(DataTable dt, string modelPath, string pathname)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            int index = 2;

            //ws.SetColumnWidth(2, 25 * 256);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + index);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + index);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["cpr_No"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["cpr_Name"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["cpr_Type"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["BuildType"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["BuildArea"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDecimal(dt.Rows[i]["cpr_Acount"]).ToString("f4"));

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["cpr_Unit"].ToString());

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["cpr_SignDate"]).ToString("yyyy-MM-dd"));

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["cpr_DoneDate"]).ToString("yyyy-MM-dd"));

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ChgPeople"].ToString());
            }

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }

    }
}