﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using TG.BLL;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;

namespace TG.Web.Coperation
{
    public partial class CoperationForReportListBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定合同
                Bindgv_Coperation();
                //绑定单位
                BindUnit();
                //绑定权限
                BindPreviewPower();
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }

        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND InsertUserID =" + UserSysNo + "");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND InsertUserID IN (Select mem_ID From tg_member Where mem_Unit_ID=" + UserUnitNo + ")");
            }
        }
        //条件查询加绑定gv_Coperation 的gridview
        public void Bindgv_Coperation()
        {
            cm_Coperation_back bll = new cm_Coperation_back();
            DataSet ds = new DataSet();
            StringBuilder builder1 = new StringBuilder("");
            //名称
            //if (this.txt_keyname.Value != "")
            //{
            //    string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
            //    builder1.Append(" And cpr_Name LIKE '%" + keyname + "%'");
            //}
            ////部门
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    builder1.AppendFormat(" AND (cpr_Unit= '{0}')", this.drp_unit.SelectedItem.Text);
            //}

            //判断个人或全部
            GetPreviewPowerSql(ref builder1);
            this.hid_where.Value = builder1.ToString();
            //数量
            // this.AspNetPager1.RecordCount = int.Parse(bll.GetListPageProcCount(builder1.ToString()).ToString());
            //排序
            // string orderString = " Order By " + OrderColumn + " " + Order;
            // builder1.Append(orderString);
            //分页
            //  this.gv_Coperation.DataSource = bll.GetListByPageProc(builder1.ToString(),this.AspNetPager1.StartRecordIndex,this.AspNetPager1.EndRecordIndex);
            // this.gv_Coperation.DataBind();
        }


        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            //不显示的单位
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                strWhere += " 1=1 AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            else
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        protected void btn_export_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();
            StringBuilder strWhere = new StringBuilder("1=1");
            //合同名称
            if (this.txt_keyname.Value != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                strWhere.AppendFormat(" AND cpr_Name LIKE '%{0}%'", keyname);
            }
            //按照部门
            if (this.drp_unit.SelectedIndex != 0)
            {
                strWhere.AppendFormat(" AND (cpr_Unit= '{0}')", this.drp_unit.SelectedItem.Text);

            }
           
            //判断个人或全部
            GetPreviewPowerSql(ref strWhere);

            DataTable dt = bll.GetList(strWhere.ToString()).Tables[0];

            ExportDataToExcel(dt, "~/TemplateXls/ReportCoperationList.xls", "合同报备列表");

        }
        private void ExportDataToExcel(DataTable dt, string modelPath, string pathname)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            int index = 2;

            //ws.SetColumnWidth(2, 25 * 256);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + index);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + index);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["cpr_No"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["cpr_Name"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["cpr_Type"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["BuildType"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["BuildArea"].ToString());

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDecimal(dt.Rows[i]["cpr_Acount"]).ToString("f4"));

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["cpr_Unit"].ToString());

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["cpr_SignDate"]).ToString("yyyy-MM-dd"));

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["cpr_DoneDate"]).ToString("yyyy-MM-dd"));

                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(dt.Rows[i]["ChgPeople"].ToString());
            }

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}