﻿using AjaxPro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.Coperation
{
    public partial class cpr_SysValueMsgListViewBymaster : PageBase
    {
        protected override bool IsAuth
        {
            get
            {
                return true;
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {
                return Request["MessageType"] ?? "";
            }
        }
        //消息类别
        public string MessageDone
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Flag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //未办已办
        public string MessageIsDoneStatus { get; set; }
        //已读未读
        public string MessageIsRead { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

            Utility.RegisterTypeForAjax(typeof(cpr_SysValueMsgListViewBymaster));


            if (string.IsNullOrEmpty(MessageType))
            {
                this.MsgTypeDropDownList.SelectedValue = "-1";
            }
            else
            {
                this.MsgTypeDropDownList.SelectedValue = MessageType;
            }
            if (!IsPostBack)
            {
                this.userSysNum.Value = base.UserSysNo.ToString();
                //未办事项,否则是未读消息
                if (MessageDone == "done")
                {
                    this.hiddenMessageIsDoneStatus.Value = Flag;
                }
                else
                {
                    this.hiddenMessageIsRead.Value = Flag;
                }
                this.hiddenMessageDone.Value = MessageDone;
                this.hiddenMessageType.Value = MessageType;

            }
        }
        [AjaxMethod]
        public string UpdateMsgStatus(string msgSysNo)
        {
            int count = new TG.BLL.cm_SysMsg().UpdateSysMsgStatus(int.Parse(msgSysNo));
            return count + "";
        }
    }
}