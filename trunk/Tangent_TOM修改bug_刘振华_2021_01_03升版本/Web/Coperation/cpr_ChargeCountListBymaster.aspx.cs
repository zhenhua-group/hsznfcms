﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;

namespace TG.Web.Coperation
{
    public partial class cpr_ChargeCountListBymaster : PageBase
    { //判断是否是全部查看
        protected string IsCheckAllPower
        {
            get
            {
                if (base.RolePowerParameterEntity.PreviewPattern == 1)
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
        }
        //收款开始时间
        protected string ChargeStartTime { get; set; }
        //收款结束时间
        protected string ChargeEndTime { get; set; }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //// 统计类型
        //protected string CountType
        //{
        //    //get
        //    //{
        //    //    return this.drp_type.SelectedItem.Text;
        //    //}
        //}
        //统计提示
        protected string ChartTip { get; set; }
        /// <summary>
        /// 页面加载
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //绑定部门
                BindUnit();
                //初始部门
                InitUnitSelect();
                //绑定合同年份
                BindYear();
                SelectCurrentYear();
                //初始时间
                InitDate();
                //总数统计

                GetCountByZong();


                GetCountByJidu();


                GetCountByMonth();

                GetCountByType();

            }


        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //总额统计
        protected void GetCountByZong()
        {
            //统计提示
            CreateTipQuery();
            //收费时间
            CreateTimeQuery();
            //年份
            string year = this.drp_year.SelectedItem.Value;
            if (this.drp_year.SelectedIndex == 0)
            {
                year = null;
            }
            //单位
            int? unitid = int.Parse(this.drp_unit.SelectedItem.Value);
            //部门查询
            string type = "0";
            if (this.drp_unit.SelectedIndex == 0)
            {
                type = "1";
                //全部
                if (IsCheckAllPower == "1")
                {
                    unitid = null;
                }
                else//本部门
                {
                    unitid = UserUnitNo;
                }
            }
            //绑定
            this.GridView1.DataSource = new TG.BLL.cm_Coperation().CountCoperationZong(year, ChargeStartTime, ChargeEndTime, unitid, type);
            this.GridView1.DataBind();

            this.GridView1.UseAccessibleHeader = true;
            this.GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        //根据季度统计
        protected void GetCountByJidu()
        {
            //统计提示
            CreateTipQuery();
            //收费时间
            CreateTimeQuery();
            //年份
            string year = this.drp_year.SelectedItem.Value;
            //修改 选择全部 月份和季度隐藏
            if (this.drp_year.SelectedIndex == 0)
            {
                year = null;
            }
            //if (this.drp_year.SelectedIndex == 0)
            //{
            //    year = SelectCurrentYear();
            //}
            //单位
            int? unitid = int.Parse(this.drp_unit.SelectedItem.Value);
            //部门/全部
            string type = "0";
            if (this.drp_unit.SelectedIndex == 0)
            {
                type = "1";
                //全部
                if (IsCheckAllPower == "1")
                {
                    unitid = null;
                }
                else//本部门
                {
                    unitid = UserUnitNo;
                }
            }
            //绑定
            this.GridView2.DataSource = new TG.BLL.cm_Coperation().CountCoperationJD(year, ChargeStartTime, ChargeEndTime, unitid, type);
            this.GridView2.DataBind();

            this.GridView2.UseAccessibleHeader = true;
            this.GridView2.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        //按月份统计
        protected void GetCountByMonth()
        {
            //统计提示
            CreateTipQuery();
            //收费时间
            CreateTimeQuery();
            //年份
            string year = this.drp_year.SelectedItem.Value;
            // 选择全部年份 月份和季度隐藏
            if (this.drp_year.SelectedIndex == 0)
            {
                year = null;
            }
            //if (this.drp_year.SelectedIndex == 0)
            //{
            //    year = SelectCurrentYear();
            //}
            //单位
            int? unitid = int.Parse(this.drp_unit.SelectedItem.Value);
            //部门/全部
            string type = "0";
            if (this.drp_unit.SelectedIndex == 0)
            {
                type = "1";
                //全部
                if (IsCheckAllPower == "1")
                {
                    unitid = null;
                }
                else//本部门
                {
                    unitid = UserUnitNo;
                }
            }
            //绑定
            this.GridView3.DataSource = new TG.BLL.cm_Coperation().CountCoperationMonth(year, ChargeStartTime, ChargeEndTime, unitid, type);
            this.GridView3.DataBind();

            this.GridView3.UseAccessibleHeader = true;
            this.GridView3.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        //按合同类型统计
        protected void GetCountByType()
        {
            //统计提示
            CreateTipQuery();
            //收费时间
            CreateTimeQuery();
            //年份
            string year = this.drp_year.SelectedItem.Value;
            //选择全部年份 取得所有的合同信息
            if (this.drp_year.SelectedIndex == 0)
            {
                year = null;
            }
            //if (this.drp_year.SelectedIndex == 0)
            //{
            //    year = SelectCurrentYear();
            //}
            //单位
            int? unitid = int.Parse(this.drp_unit.SelectedItem.Value);
            //部门/全部
            string type = "0";
            if (this.drp_unit.SelectedIndex == 0)
            {
                type = "1";
                //全部
                if (IsCheckAllPower == "1")
                {
                    unitid = null;
                }
                else//本部门
                {
                    unitid = UserUnitNo;
                }
            }
            //绑定
            this.GridView4.DataSource = new TG.BLL.cm_Coperation().CountCoperationType(year, ChargeStartTime, ChargeEndTime, unitid, type);
            this.GridView4.DataBind();

            this.GridView4.UseAccessibleHeader = true;
            this.GridView4.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        //查询
        protected void btn_ok_Click(object sender, EventArgs e)
        {
            if (this.drp_year.SelectedIndex == 0)
            {
                liTwo.Style.Add("display", "none");
                liThree.Style.Add("display", "none");
            }
            else
            {
                liTwo.Style.Add("display", "block");
                liThree.Style.Add("display", "block");
            }
            BindDataSetPageIndex();
        }
        //查询数据
        protected void GetCountData()
        {

            ShowCountTypeControl("0");
            GetCountByZong();

            ShowCountTypeControl("1");
            GetCountByJidu();

            ShowCountTypeControl("2");
            GetCountByMonth();

            ShowCountTypeControl("3");
            GetCountByType();

        }
        //初始单位选中信息
        protected void InitUnitSelect()
        {
            if (this.drp_unit.Items.FindByValue(UserUnitNo.ToString()) != null)
            {
                this.drp_unit.Items.FindByValue(UserUnitNo.ToString()).Selected = true;
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        //初始时间设置
        protected void InitDate()
        {
            string str_year = DateTime.Now.Year.ToString();
            this.txt_start.Value = str_year + "-01-01";
            this.txt_end.Value = str_year + "-12-31";
        }
        //创建时间查询条件
        protected void CreateTimeQuery()
        {
            string start = this.txt_start.Value.Trim();
            string end = this.txt_end.Value.Trim();
            string curtime = DateTime.Now.ToString("yyyy-MM-dd");

            if (start == "" && end != "")
            {
                start = curtime;
            }
            else if (start != "" && end == "")
            {
                end = curtime;
            }
            else if (start == "" && end == "")
            {
                start = curtime;
                end = curtime;
            }

            ChargeStartTime = start;
            ChargeEndTime = end;
        }
        //统计内容提示
        protected void CreateTipQuery()
        {
            string strTip = "";
            if (this.drp_unit.SelectedIndex == 0)
            {
                strTip += "全院"; 
            }
            else
            {
                strTip += this.drp_unit.SelectedItem.Text.Trim();
            }
            //年份
            if (this.drp_year.SelectedIndex == 0)
            {
                strTip += "全部年份";
            }
            else
            {
                strTip += this.drp_year.SelectedItem.Text.Trim() + "年";
            }

            ChartTip = strTip;
        }
        //控制对应统计类型
        protected void ShowCountTypeControl(string type)
        {
            //this.div_Container1.Visible = type == "0" ? true : false;
            //this.div_Container2.Visible = type == "1" ? true : false;
            //this.div_Container3.Visible = type == "2" ? true : false;
            //this.div_Container4.Visible = type == "3" ? true : false;
        }
        //选中当前年份
        protected string SelectCurrentYear()
        {
            string year = DateTime.Now.Year.ToString();
            //选择当前年份
            this.drp_year.ClearSelection();
            if (this.drp_year.Items.FindByValue(year) != null)
            {
                this.drp_year.Items.FindByValue(year).Selected = true;
                //统计提示
                CreateTipQuery();
            }
            return year;
        }
        //公用格式化数据显示方法
        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex == 0)
                {
                    foreach (TableCell cell in e.Row.Cells)
                    {
                        if (e.Row.Cells.GetCellIndex(cell) > 0)
                        {
                            cell.Text = cell.Text.Substring(0, cell.Text.IndexOf('.'));
                        }
                    }
                }
                if (e.Row.RowIndex == 1)
                {
                    foreach (TableCell cell in e.Row.Cells)
                    {
                        if (e.Row.Cells.GetCellIndex(cell) > 0)
                        {
                            cell.Text = Convert.ToDecimal(cell.Text).ToString("f2");
                        }
                    }
                }
            }
        }

        //生产部门改变重新绑定数据
        protected void drp_unit_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataSetPageIndex();
        }
        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>
        protected void BindDataSetPageIndex()
        {
            //绑定数据
            GetCountData();
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_export_Click(object sender, EventArgs e)
        {
            DataTable dt1 = getData("1");
            DataTable dt2 = getData("2");
            DataTable dt3 = getData("3");
            DataTable dt4 = getData("4");
            string modelPath = " ~/TemplateXls/AllDetail.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            int row = 2;
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dt1.Rows[i]["CprCount"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt1.Rows[i]["CprChgCount"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt1.Rows[i]["CprNotChgCount"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt1.Rows[i]["CprAllChargeCount"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt1.Rows[i]["CprAllCount"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt1.Rows[i]["CprChargeCount"].ToString());
            }
            int row1 = 7;
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row1);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row1);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);


                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt2.Rows[i]["A"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt2.Rows[i]["B"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt2.Rows[i]["C"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt2.Rows[i]["D"].ToString());
            }
            int row2 = 14;
            for (int i = 0; i < dt3.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row2);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row2);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);


                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt3.Rows[i]["A"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt3.Rows[i]["B"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt3.Rows[i]["C"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt3.Rows[i]["D"].ToString());
                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt3.Rows[i]["E"].ToString());
                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(dt3.Rows[i]["F"].ToString());
                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt3.Rows[i]["G"].ToString());
                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(dt3.Rows[i]["H"].ToString());
                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(dt3.Rows[i]["I"].ToString());
                cell = dataRow.CreateCell(10);
                cell.CellStyle = style2;
                cell.SetCellValue(dt3.Rows[i]["J"].ToString());
                cell = dataRow.CreateCell(11);
                cell.CellStyle = style2;
                cell.SetCellValue(dt3.Rows[i]["K"].ToString());
                cell = dataRow.CreateCell(12);
                cell.CellStyle = style2;
                cell.SetCellValue(dt3.Rows[i]["L"].ToString());

            }
            int row3 = 22;
            for (int i = 0; i < dt4.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row3);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row3);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);


                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dt4.Rows[i]["A"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(dt4.Rows[i]["B"].ToString());

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(dt4.Rows[i]["C"].ToString());

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dt4.Rows[i]["D"].ToString());
                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(dt4.Rows[i]["E"].ToString());
                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(dt4.Rows[i]["F"].ToString());
                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(dt4.Rows[i]["G"].ToString());
                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(dt4.Rows[i]["H"].ToString());
                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(dt4.Rows[i]["I"].ToString());
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("合同综合列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        /// <summary>
        /// 得到数据信息
        /// </summary>
        /// <returns></returns>
        private DataTable getData(string index)
        {
            //zonghe
            if (index == "1")
            {
                //统计提示
                CreateTipQuery();
                //收费时间
                CreateTimeQuery();
                //年份
                string year = this.drp_year.SelectedItem.Value;
                if (this.drp_year.SelectedIndex == 0)
                {
                    year = null;
                }
                //单位
                int? unitid = int.Parse(this.drp_unit.SelectedItem.Value);
                //部门查询
                string type = "0";
                if (this.drp_unit.SelectedIndex == 0)
                {
                    type = "1";
                    //全部
                    if (IsCheckAllPower == "1")
                    {
                        unitid = null;
                    }
                    else//本部门
                    {
                        unitid = UserUnitNo;
                    }
                }
                //绑定
                DataTable dt1 = new TG.BLL.cm_Coperation().CountCoperationZong(year, ChargeStartTime, ChargeEndTime, unitid, type).Tables[0];
                return dt1;
            }
            else if (index == "2")
            {
                //统计提示
                CreateTipQuery();
                //收费时间
                CreateTimeQuery();
                //年份
                string year = this.drp_year.SelectedItem.Value;
                //修改 选择全部 月份和季度隐藏
                if (this.drp_year.SelectedIndex == 0)
                {
                    year = null;
                }
                //if (this.drp_year.SelectedIndex == 0)
                //{
                //    year = SelectCurrentYear();
                //}
                //单位
                int? unitid = int.Parse(this.drp_unit.SelectedItem.Value);
                //部门/全部
                string type = "0";
                if (this.drp_unit.SelectedIndex == 0)
                {
                    type = "1";
                    //全部
                    if (IsCheckAllPower == "1")
                    {
                        unitid = null;
                    }
                    else//本部门
                    {
                        unitid = UserUnitNo;
                    }
                }
                //绑定
                DataTable dt2 = new TG.BLL.cm_Coperation().CountCoperationJD(year, ChargeStartTime, ChargeEndTime, unitid, type).Tables[0];
                return dt2;
            }
            else if (index == "3")
            {
                //统计提示
                CreateTipQuery();
                //收费时间
                CreateTimeQuery();
                //年份
                string year = this.drp_year.SelectedItem.Value;
                // 选择全部年份 月份和季度隐藏
                if (this.drp_year.SelectedIndex == 0)
                {
                    year = null;
                }
                //if (this.drp_year.SelectedIndex == 0)
                //{
                //    year = SelectCurrentYear();
                //}
                //单位
                int? unitid = int.Parse(this.drp_unit.SelectedItem.Value);
                //部门/全部
                string type = "0";
                if (this.drp_unit.SelectedIndex == 0)
                {
                    type = "1";
                    //全部
                    if (IsCheckAllPower == "1")
                    {
                        unitid = null;
                    }
                    else//本部门
                    {
                        unitid = UserUnitNo;
                    }
                }
                //绑定
                DataTable dt3 = new TG.BLL.cm_Coperation().CountCoperationMonth(year, ChargeStartTime, ChargeEndTime, unitid, type).Tables[0];
                return dt3;
            }
            else
            {
                //统计提示
                CreateTipQuery();
                //收费时间
                CreateTimeQuery();
                //年份
                string year = this.drp_year.SelectedItem.Value;
                //选择全部年份 取得所有的合同信息
                if (this.drp_year.SelectedIndex == 0)
                {
                    year = null;
                }
                //if (this.drp_year.SelectedIndex == 0)
                //{
                //    year = SelectCurrentYear();
                //}
                //单位
                int? unitid = int.Parse(this.drp_unit.SelectedItem.Value);
                //部门/全部
                string type = "0";
                if (this.drp_unit.SelectedIndex == 0)
                {
                    type = "1";
                    //全部
                    if (IsCheckAllPower == "1")
                    {
                        unitid = null;
                    }
                    else//本部门
                    {
                        unitid = UserUnitNo;
                    }
                }
                //绑定
                DataTable dt4 = new TG.BLL.cm_Coperation().CountCoperationType(year, ChargeStartTime, ChargeEndTime, unitid, type).Tables[0];
                return dt4;
            }

        }

    }
}