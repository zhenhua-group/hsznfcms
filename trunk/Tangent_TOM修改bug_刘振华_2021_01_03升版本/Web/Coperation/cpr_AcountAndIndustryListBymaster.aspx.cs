﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using TG.Common.EntityBuilder;
using TG.Model;
using TG.DBUtility;
using System.Data.SqlClient;
using AjaxPro;
using InfoSoftGlobal;

namespace TG.Web.Coperation
{

    public partial class cpr_AcountAndIndustryListBymaster : PageBase
    {
        public int PageSize { get { return 13; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(cpr_AcountAndIndustryListBymaster));
            if (!IsPostBack)
            {
                InitDropDownByYear();
                InitDropDownByMonth();
                BindUnit();
                BindPreviewPower();
                this.labYear.Text = this.drp_year.Text;
                this.labTime.Text = DateTime.Now.ToString("yyyy年MM月dd日") + "制表";
                this.labYear2.Text = this.drp_year.Text;
                GetTu();
            }
            else
            {

                GetTu();
            }
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;

                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }

        /// <summary>
        ///初始年份
        /// </summary>
        protected void InitDropDownByYear()
        {
            this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
        }
        /// <summary>
        ///初始月份
        /// </summary>
        protected void InitDropDownByMonth()
        {
            this.drp_month.Items.FindByText(DateTime.Now.Month.ToString()).Selected = true;
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }

        private void GetTu()
        {
            string unitId = this.drp_unit.SelectedItem.Value;
            string year = this.drp_year.SelectedItem.Value;
            string month = this.drp_month.SelectedItem.Value;
            string beginTime = month + "-01";
            string endTime = month + "-" + TG.Common.TimeParser.GetMonthLastDate(DateTime.Now.Year, int.Parse(month));
            TG.BLL.cpr_AcountAndIndustry bll = new BLL.cpr_AcountAndIndustry();
            if (unitId == "-1")
            {
                if (base.RolePowerParameterEntity.PreviewPattern == 0 || base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    //
                    unitId = base.UserUnitNo.ToString();
                }
            }
            List<TG.Model.cpr_AcountAndIndustry> list = bll.GetList(unitId, year, beginTime, endTime);
            StringBuilder xmlData = new StringBuilder();
            xmlData.Append("<chart caption='" + year + "年（1-" + month + "月）【已签订合同额】与" + (Convert.ToInt32(year) - 1) + "同期对比图' numberPrefix='' formatNumberScale='0' rotateValues='1' placeValuesInside='1' decimals='0' numberSuffix='万元' baseFontSize='12' xAxisName='生产部门' yAxisName='合同额'><categories>");

            xmlData.Append("<category name='公建' />");
            xmlData.Append("<category name='市政' />");
            xmlData.Append("<category name='房地产' />");
            xmlData.Append("<category name='综合' />");
            xmlData.Append("<category name='合计' />");
            xmlData.Append("</categories>");
            xmlData.Append("<dataset seriesName='" + (int.Parse(year) - 1) + "'>");
            TG.Model.cpr_AcountAndIndustry model = new Model.cpr_AcountAndIndustry();
            foreach (TG.Model.cpr_AcountAndIndustry item in list)
            {
                model.LastPublicBuild += item.LastPublicBuild;
                model.LastAffordHouse += item.LastAffordHouse;
                model.LastEstate += item.LastEstate;
                model.LastComprehensive += item.LastComprehensive;
                model.LastTotal += item.LastTotal;
            }
            xmlData.Append("<set value='" + model.LastPublicBuild / 3 + "' />");
            xmlData.Append("<set value='" + model.LastAffordHouse / 3 + "' />");
            xmlData.Append("<set value='" + model.LastEstate / 3 + "' />");
            xmlData.Append("<set value='" + model.LastComprehensive / 3 + "' />");
            xmlData.Append("<set value='" + model.LastTotal / 3 + "' />");
            xmlData.Append("</dataset>");
            xmlData.Append("<dataset seriesName='" + year + "'>");
            foreach (TG.Model.cpr_AcountAndIndustry item in list)
            {
                model.NowPublicBuild += item.NowPublicBuild;
                model.NowAffordHouse += item.NowAffordHouse;
                model.NowEstate += item.NowEstate;
                model.NowComprehensive += item.NowComprehensive;
                model.NowTotal += item.NowTotal;
            }
            xmlData.Append("<set value='" + model.NowPublicBuild / 3 + "' />");
            xmlData.Append("<set value='" + model.NowAffordHouse / 3 + "' />");
            xmlData.Append("<set value='" + model.NowEstate / 3 + "' />");
            xmlData.Append("<set value='" + model.NowComprehensive / 3 + "' />");
            xmlData.Append("<set value='" + model.NowTotal / 3 + "' />");
            xmlData.Append("</dataset>");
            xmlData.Append("</chart>");
            Literal4.Text = FusionCharts.RenderChart("../js/FunctionChart/MSColumn2D.swf", "", xmlData.ToString(), "productSales", "100%", "500", false, true);
            if (model.LastPublicBuild == decimal.Zero && model.LastAffordHouse == decimal.Zero && model.LastEstate == decimal.Zero && model.LastComprehensive == decimal.Zero)
            {
                Literal1.Text = CreateEmptyImage();
            }
            //显示饼状图
            StringBuilder strXML = new StringBuilder();
            strXML.Append("<chart caption='" + (int.Parse(this.drp_year.SelectedItem.Value) - 1) + "年各类项目【已签订合同额】统计' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='万元' baseFontSize='12'>");
            strXML.AppendFormat("<set label='{0}'  value='{1}' link='javascript:GerCoperationList({2},0);'/>", "公建", model.LastPublicBuild / 3, drp_year.SelectedItem.Value);
            strXML.AppendFormat("<set label='{0}'  value='{1}' link='javascript:GerCoperationList({2},1);'/>", "市政", model.LastAffordHouse / 3, drp_year.SelectedItem.Value);
            strXML.AppendFormat("<set label='{0}'  value='{1}' link='javascript:GerCoperationList({2},2);'/>", "房地产", model.LastEstate / 3, drp_year.SelectedItem.Value);
            strXML.AppendFormat("<set label='{0}'  value='{1}' link='javascript:GerCoperationList({2},3);'/>", "综合", model.LastComprehensive / 3, drp_year.SelectedItem.Value);

            strXML.Append("</chart>");
            Literal1.Text = FusionCharts.RenderChart("../js/FunctionChart/Pie2D.swf", "", strXML.ToString(), "sdew", "100%", "300", false, true, false);

            if (model.NowPublicBuild == decimal.Zero && model.NowAffordHouse == decimal.Zero && model.NowEstate == decimal.Zero && model.NowComprehensive == decimal.Zero)
            {
                Literal2.Text = CreateEmptyImage();
            }
            StringBuilder strXML2 = new StringBuilder();
            strXML2.Append("<chart caption='" + this.drp_year.SelectedItem.Value + "年各类项目【已签订合同额】统计' subCaption='' pieSliceDepth='30' showBorder='1' formatNumberScale='0' numberSuffix='万元' baseFontSize='12'>");
            strXML2.AppendFormat("<set label='{0}'  value='{1}' link='javascript:GerCoperationList({2},0);'/>", "公建", model.NowPublicBuild / 3, int.Parse(this.drp_year.Text) - 1);
            strXML2.AppendFormat("<set label='{0}'  value='{1}' link='javascript:GerCoperationList({2},1);'/>", "市政", model.NowAffordHouse / 3, int.Parse(this.drp_year.Text) - 1);
            strXML2.AppendFormat("<set label='{0}'  value='{1}' link='javascript:GerCoperationList({2},2);'/>", "房地产", model.NowEstate / 3, int.Parse(this.drp_year.Text) - 1);
            strXML2.AppendFormat("<set label='{0}'  value='{1}' link='javascript:GerCoperationList({2},3);'/>", "综合", model.NowComprehensive / 3, int.Parse(this.drp_year.Text) - 1);
            strXML2.Append("</chart>");
            Literal2.Text = FusionCharts.RenderChart("../js/FunctionChart/Pie2D.swf", "", strXML2.ToString(), "ss", "100%", "300", false, true, false);

        }

        [AjaxMethod]
        public string GetCoperationListByIsPay(int year, int isPay, int pageCurrent, int pageSize, int month)
        {
            string sql = "";
            string endmonth = month + "-" + TG.Common.TimeParser.GetMonthLastDate(year, month);
            string countSql = "";

            if (isPay == 0)
            {
                sql = string.Format(@"select * from 
	                                             cm_Coperation c 
	                                             where c.cpr_id in(
                                            select TOP {0} c.cpr_Id 
		                                            from cm_Coperation c 
		                                            left join cm_CoperationCharge cc on cc.cpr_ID = c.cpr_Id 
		                                            left join cm_RealCprChg rc on rc.chg_id = cc.ID  
		                                            where c.cpr_SignDate between N'{1}-01-01' and N'{1}-{4}' and c.Industry ='公建' 
		                                            group by c.cpr_Id having SUM(isnull(rc.payCount,0))=0
		                                            and c.cpr_id not in 
		                                            (
			                                            select TOP {3} c.cpr_Id 
				                                            from cm_Coperation c 
				                                            left join cm_CoperationCharge cc on cc.cpr_ID = c.cpr_Id 
				                                            left join cm_RealCprChg rc on rc.chg_id = cc.ID  
				                                            where c.cpr_SignDate between N'{1}-01-01' and N'{1}-{4}' and c.Industry ='公建' 
				                                            group by c.cpr_Id having SUM(isnull(rc.payCount,0))=0
				                                            order by c.cpr_id desc
		                                            )order by c.cpr_id desc)", pageSize, year, pageCurrent - 1, pageSize * (pageCurrent - 1), endmonth);

                countSql = string.Format(@"select COUNT(*) from(
                                                        select c.cpr_Id from cm_Coperation c left join cm_CoperationCharge cc on cc.cpr_ID = c.cpr_Id left join cm_RealCprChg rc on rc.chg_id = cc.ID  
                                                        where c.cpr_SignDate between N'{0}-01-01' and N'{0}-{1}' and c.Industry ='公建' 
                                                        group by c.cpr_Id having SUM(isnull(rc.payCount,0))=0
                                                        )TT", year, endmonth);
            }
            else if (isPay == 1)
            {
                sql = string.Format(@"select * from 
	                                             cm_Coperation c 
	                                             where c.cpr_id in(
                                            select TOP {0} c.cpr_Id 
		                                            from cm_Coperation c 
		                                            left join cm_CoperationCharge cc on cc.cpr_ID = c.cpr_Id 
		                                            left join cm_RealCprChg rc on rc.chg_id = cc.ID  
		                                            where c.cpr_SignDate between N'{1}-01-01' and N'{1}-{4}' and c.Industry ='市政' 
		                                            group by c.cpr_Id having SUM(isnull(rc.payCount,0))=0
		                                            and c.cpr_id not in 
		                                            (
			                                            select TOP {3} c.cpr_Id 
				                                            from cm_Coperation c 
				                                            left join cm_CoperationCharge cc on cc.cpr_ID = c.cpr_Id 
				                                            left join cm_RealCprChg rc on rc.chg_id = cc.ID  
				                                            where c.cpr_SignDate between N'{1}-01-01' and N'{1}-{4}' and c.Industry ='市政' 
				                                            group by c.cpr_Id having SUM(isnull(rc.payCount,0))=0
				                                            order by c.cpr_id desc
		                                            )order by c.cpr_id desc)", pageSize, year, pageCurrent - 1, pageSize * (pageCurrent - 1), endmonth);

                countSql = string.Format(@"select COUNT(*) from(
                                                        select c.cpr_Id from cm_Coperation c left join cm_CoperationCharge cc on cc.cpr_ID = c.cpr_Id left join cm_RealCprChg rc on rc.chg_id = cc.ID  
                                                        where c.cpr_SignDate between N'{0}-01-01' and N'{0}-{1}' and c.Industry ='市政' 
                                                        group by c.cpr_Id having SUM(isnull(rc.payCount,0))=0
                                                        )TT", year, endmonth);
            }
            else if (isPay == 2)
            {
                sql = string.Format(@"select * from 
	                                             cm_Coperation c 
	                                             where c.cpr_id in(
                                            select TOP {0} c.cpr_Id 
		                                            from cm_Coperation c 
		                                            left join cm_CoperationCharge cc on cc.cpr_ID = c.cpr_Id 
		                                            left join cm_RealCprChg rc on rc.chg_id = cc.ID  
		                                            where c.cpr_SignDate between N'{1}-01-01' and N'{1}-{4}' and c.Industry ='房地产' 
		                                            group by c.cpr_Id having SUM(isnull(rc.payCount,0))=0
		                                            and c.cpr_id not in 
		                                            (
			                                            select TOP {3} c.cpr_Id 
				                                            from cm_Coperation c 
				                                            left join cm_CoperationCharge cc on cc.cpr_ID = c.cpr_Id 
				                                            left join cm_RealCprChg rc on rc.chg_id = cc.ID  
				                                            where c.cpr_SignDate between N'{1}-01-01' and N'{1}-{4}' and c.Industry ='房地产' 
				                                            group by c.cpr_Id having SUM(isnull(rc.payCount,0))=0
				                                            order by c.cpr_id desc
		                                            )order by c.cpr_id desc)", pageSize, year, pageCurrent - 1, pageSize * (pageCurrent - 1), endmonth);

                countSql = string.Format(@"select COUNT(*) from(
                                                        select c.cpr_Id from cm_Coperation c left join cm_CoperationCharge cc on cc.cpr_ID = c.cpr_Id left join cm_RealCprChg rc on rc.chg_id = cc.ID  
                                                        where c.cpr_SignDate between N'{0}-01-01' and N'{0}-{1}' and c.Industry ='房地产' 
                                                        group by c.cpr_Id having SUM(isnull(rc.payCount,0))=0
                                                        )TT", year, endmonth);
            }
            else
            {
                sql = string.Format(@"select * from 
	                                             cm_Coperation c 
	                                             where c.cpr_id in(
                                            select TOP {0} c.cpr_Id 
		                                            from cm_Coperation c 
		                                            left join cm_CoperationCharge cc on cc.cpr_ID = c.cpr_Id 
		                                            left join cm_RealCprChg rc on rc.chg_id = cc.ID  
		                                            where c.cpr_SignDate between N'{1}-01-01' and N'{1}-{4}' and c.Industry not in ('公建','市政','房地产') 
		                                            group by c.cpr_Id having SUM(isnull(rc.payCount,0))=0
		                                            and c.cpr_id not in 
		                                            (
			                                            select TOP {3} c.cpr_Id 
				                                            from cm_Coperation c 
				                                            left join cm_CoperationCharge cc on cc.cpr_ID = c.cpr_Id 
				                                            left join cm_RealCprChg rc on rc.chg_id = cc.ID  
				                                            where c.cpr_SignDate between N'{1}-01-01' and N'{1}-{4}' and c.Industry  not in ('公建','市政','房地产') 
				                                            group by c.cpr_Id having SUM(isnull(rc.payCount,0))=0
				                                            order by c.cpr_id desc
		                                            )order by c.cpr_id desc)", pageSize, year, pageCurrent - 1, pageSize * (pageCurrent - 1), endmonth);

                countSql = string.Format(@"select COUNT(*) from(
                                                        select c.cpr_Id from cm_Coperation c left join cm_CoperationCharge cc on cc.cpr_ID = c.cpr_Id left join cm_RealCprChg rc on rc.chg_id = cc.ID  
                                                        where c.cpr_SignDate between N'{0}-01-01' and N'{0}-{1}' and c.Industry  not in ('公建','市政','房地产') 
                                                        group by c.cpr_Id having SUM(isnull(rc.payCount,0))=0
                                                        )TT", year, endmonth);
            }
            SqlDataReader reader = DbHelperSQL.ExecuteReader(sql);

            List<CoperationListViewEntity> coperationList = EntityBuilder<CoperationListViewEntity>.BuilderEntityList(reader);

            object objResult = DbHelperSQL.GetSingle(countSql);

            List<object> ResultLIst = new List<object> { objResult == null ? 0 : Convert.ToInt32(objResult), coperationList };

            return Newtonsoft.Json.JsonConvert.SerializeObject(ResultLIst);
        }

        private string CreateColorDiv(Dictionary<string, string> descriptionArray)
        {

            string str = "<table style=\"height: 10px;\" class=\"colorDivTableStyle\">";
            str += "<tr>";
            foreach (KeyValuePair<string, string> description in descriptionArray)
            {
                str += "<td>";
                str += "<div style=\"width: 10px; height: 10px; background-color: " + description.Value + ";\">";
                str += "</div>";
                str += "</td>";
                str += "<td>";
                str += description.Key;
                str += "</td>";
            }

            str += "</tr>";
            str += "</table>";
            return str;
        }
        //如果没有数据显示的图片
        private string CreateEmptyImage()
        {
            return "<img src=\"#\" style=\"width:565px;height:306px;\">";
        }





        protected void btn_report_Click(object sender, EventArgs e)
        {
            string year = this.drp_year.Text;
            string title = year + "年(1-" + this.drp_month.SelectedItem.Value + "月)已签订合同额【项目分类】与" + (int.Parse(year) - 1) + "年同期对比表";
            ToExcel(title);
        }
        public void ToExcel(string title)
        {
            TG.BLL.cpr_AcountAndIndustry acountbll = new BLL.cpr_AcountAndIndustry();

            string unitId = this.drp_unit.SelectedItem.Value;
            string year = this.drp_year.SelectedItem.Value;
            string month = this.drp_month.SelectedItem.Value;
            string beginTime = month + "-01";
            string endTime = month + "-" + TG.Common.TimeParser.GetMonthLastDate(DateTime.Now.Year, int.Parse(month));

            TG.BLL.CopContrast bll = new BLL.CopContrast();
            List<TG.Model.cpr_AcountAndIndustry> listSC = acountbll.GetList(unitId, year, beginTime, endTime);

            ExportDataToExcel(listSC, "~/TemplateXls/cpr_AcountAndIndustryList.xls", year);
            //获取excel的文件名称（Guid是一个全球表示，使excel的文件名不同）


        }
        private void ExportDataToExcel(List<TG.Model.cpr_AcountAndIndustry> listSC, string modelPath, string year)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            ws.GetRow(0).GetCell(0).SetCellValue(this.drp_year.SelectedItem.Value + "年（1-" + this.drp_month.SelectedItem.Value + "月)已签订合同额【项目分类】与" + (Convert.ToInt32(this.drp_year.SelectedItem.Value) - 1) + "年同期对比表");
            ws.GetRow(1).GetCell(0).SetCellValue(DateTime.Now.ToString("单位：万元          yyyy年-MM月-dd日") + "制表");
            ws.GetRow(2).GetCell(2).SetCellValue(int.Parse(year) - 1 + "年按类别划分");
            ws.GetRow(2).GetCell(7).SetCellValue(int.Parse(year) + "年按类别划分");
            //ws.GetRow(3).GetCell(4).SetCellValue(int.Parse(year) + "年");
            //ws.GetRow(3).GetCell(5).SetCellValue(int.Parse(year) + "年");

            //ws.GetRow(4).GetCell(1).SetCellValue(int.Parse(month) + "月");
            //ws.GetRow(4).GetCell(2).SetCellValue(DateTime.Now.Month + "月");
            //ws.GetRow(4).GetCell(4).SetCellValue(int.Parse(month) + "月");
            //ws.GetRow(4).GetCell(5).SetCellValue(DateTime.Now.Month + "月");
            //设置样式
            ICellStyle style = wb.CreateCellStyle();

            //设置字体
            IFont font = wb.CreateFont();
            font.FontHeightInPoints = 9;//字号
            font.FontName = "宋体";
            style.SetFont(font);
            int index = 4;
            //设置边框
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //设置宽度
            // ws.SetColumnWidth(0, 15 * 256);
            //ws.SetColumnWidth(2, 25 * 256);
            if (listSC.Count > 0)
            {
                int i = 0;
                foreach (TG.Model.cpr_AcountAndIndustry item in listSC)
                {
                    //     Unit AcountName LastPublicBuild LastAffordHouse LastEstate LastComprehensive LastTotal NowPublicBuild NowAffordHouse NowEstate NowComprehensive NowTotal PublicBuildContrast AffordHouseContrast EstateContrast ComprehensiveContrast TotalContrast
                    i++;
                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(item.Unit);
                    cell0.CellStyle = style;
                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.AcountName);
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(Convert.ToDouble(item.LastPublicBuild));
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(Convert.ToDouble(item.LastAffordHouse));
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(Convert.ToDouble(item.LastEstate)); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(Convert.ToDouble(item.LastComprehensive)); cell5.CellStyle = style;
                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(Convert.ToDouble(item.LastTotal)); cell6.CellStyle = style;
                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(Convert.ToDouble(item.NowPublicBuild)); cell7.CellStyle = style;
                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(Convert.ToDouble(item.NowAffordHouse)); cell8.CellStyle = style;
                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(Convert.ToDouble(item.NowEstate)); cell9.CellStyle = style;
                    ICell cell10 = row.CreateCell(10);
                    //NowComprehensive NowTotal PublicBuildContrast AffordHouseContrast EstateContrast ComprehensiveContrast TotalContrast
                    cell10.SetCellValue(Convert.ToDouble(item.NowComprehensive)); cell10.CellStyle = style;
                    ICell cell11 = row.CreateCell(11);
                    cell11.SetCellValue(Convert.ToDouble(item.NowTotal)); cell11.CellStyle = style;
                    ICell cell12 = row.CreateCell(12);
                    cell12.SetCellValue(item.PublicBuildContrast); cell12.CellStyle = style;
                    ICell cell13 = row.CreateCell(13);
                    cell13.SetCellValue(item.AffordHouseContrast); cell13.CellStyle = style;
                    ICell cell14 = row.CreateCell(14);
                    cell14.SetCellValue(item.EstateContrast); cell14.CellStyle = style;
                    ICell cell15 = row.CreateCell(15);
                    cell15.SetCellValue(item.ComprehensiveContrast); cell15.CellStyle = style;
                    ICell cell16 = row.CreateCell(16);
                    cell16.SetCellValue(item.TotalContrast); cell16.CellStyle = style;

                    index++;
                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(this.drp_year.SelectedItem.Value + "年（1-" + this.drp_month.SelectedItem.Value + "月)已签订合同额【项目分类】与" + (Convert.ToInt32(this.drp_year.SelectedItem.Value) - 1) + "年同期对比表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}