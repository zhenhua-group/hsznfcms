﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.Coperation
{
    public partial class cpr_CoperationPreviewBymaster : PageBase
    {
        protected override bool IsAuth
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 合同系统自增号
        /// </summary>
        public int CoperationSysNo
        {
            get
            {
                int coperationSysNo = 0;
                int.TryParse(Request["coperationSysNo"], out coperationSysNo);
                return coperationSysNo;
            }
        }

        /// <summary>
        /// 客户基本信息
        /// </summary>
        public TG.Model.cm_CustomerInfo CustomerInfo { get; set; }

        /// <summary>
        /// 合同基本信息
        /// </summary>
        public TG.Model.cm_CoperationAuditListView CoperationAuditEntity { get; set; }

        /// <summary>
        /// 合同分项信息
        /// </summary>
        public List<TG.Model.cm_SubCoperation> SubCoperationInfoList { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //合同信息
            GetCoperationInfo();
            //客户信息
            GetCustomerInfo();
            //合同子项
            GetSubCoperationInfo();
        }
        /// <summary>
        /// 查询客户信息
        /// </summary>
        private void GetCustomerInfo()
        {
            CustomerInfo = new TG.BLL.cm_CustomerInfo().GetModel(Convert.ToInt32(CoperationAuditEntity.cst_Id));
        }

        /// <summary>
        /// 查询合同基本信息
        /// </summary>
        private void GetCoperationInfo()
        {
            List<TG.Model.cm_CoperationAuditListView> coperationListView = new TG.BLL.cm_CoperationAudit().GetCoperationListView(" and cpr_Id=" + CoperationSysNo);
            if (coperationListView != null && coperationListView.Count > 0)
            {
                CoperationAuditEntity = coperationListView[0];
            }
        }

        /// <summary>
        /// 得到合同分项信息
        /// </summary>
        private void GetSubCoperationInfo()
        {
            SubCoperationInfoList = new TG.BLL.cm_SubCoperation().GetSubCoperationByCoperationSysNo(CoperationSysNo);
            RepeaterSubCoperation.DataSource = SubCoperationInfoList;
            RepeaterSubCoperation.DataBind();
        }
    }
}