﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using System.Configuration;
using TG.Model;

namespace TG.Web.Coperation
{
    public partial class AccountedForConfirmation : PageBase
    {
        #region QueryString
        private string WarningDay
        {
            get
            {
                return ConfigurationManager.AppSettings["WarningDay"].ToString();
            }
        }
        protected override bool IsAuth
        {
            get
            {
                return true;
            }
        }
        public string CoperationName
        {
            get
            {
                return Request["coperationName"];
            }
        }

        public string DepartmentName
        {
            get
            {
                return Request["deparmentName"];
            }
        }
        public int FromUser
        {
            get
            {
                int fromUser = 0;
                int.TryParse(Request["fromUser"], out fromUser);
                return fromUser;
            }
        }

        public int RealPaySysNo
        {
            get
            {
                int realPaySysNo = 0;
                int.TryParse(Request["sysNo"], out realPaySysNo);
                return realPaySysNo;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetCoperationPayRecord();
                GetRealPayRecord();
            }
        }

        protected void gv_Coperation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Text = e.Row.Cells[1].Text + "%";
                //状态
                decimal plancount = Convert.ToDecimal(e.Row.Cells[2].Text);
                Label lbl_shicount = e.Row.Cells[4].FindControl("Label1") as Label;
                decimal shicount = 0.00m;
                if (!string.IsNullOrEmpty(lbl_shicount.Text))
                {
                    shicount = Convert.ToDecimal(lbl_shicount.Text);
                }
                lbl_shicount.Text = shicount.ToString();
                //是否完成收款
                char flag = shicount >= plancount ? '1' : '0';
                //计划收款时间
                string plantime = e.Row.Cells[5].Text.ToString();
                //收款id
                HiddenField hid_chg = e.Row.Cells[4].FindControl("Hidden1") as HiddenField;

                if (GetPlanStatus(plantime, flag) == -1)
                {
                    e.Row.Cells[5].Text = "完成";
                    e.Row.Cells[5].CssClass = "cls_status_green";
                    //有逾期的收款
                    if (IsOverInput(hid_chg.Value))
                    {
                        e.Row.ForeColor = System.Drawing.Color.Red;
                    }
                }
                else if (GetPlanStatus(plantime, flag) == 0)
                {
                    e.Row.Cells[5].Text = "正常";
                    e.Row.Cells[5].CssClass = "cls_status_blue";
                }
                else if (GetPlanStatus(plantime, flag) == 1)
                {
                    e.Row.Cells[5].Text = "计划";
                    e.Row.Cells[5].CssClass = "cls_status_yellow";
                }
                else
                {
                    e.Row.Cells[5].Text = "逾期";
                    e.Row.Cells[5].CssClass = "cls_status_red";
                }
            }
        }

        //查看是否有逾期收款
        protected bool IsOverInput(string chgid)
        {
            TG.BLL.cm_RealCprChg bll_rl = new TG.BLL.cm_RealCprChg();
            string strWhere = " isover=1 AND chg_Id=" + chgid;
            List<TG.Model.cm_RealCprChg> list = bll_rl.GetModelList(strWhere);
            if (list.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //返回收费计划状态
        protected int GetPlanStatus(string plantime, char flag)
        {
            DateTime nowdate = DateTime.Now;
            DateTime plandate = Convert.ToDateTime(plantime);
            //DiffDate
            TimeSpan t1 = new TimeSpan(nowdate.Ticks);
            TimeSpan t2 = new TimeSpan(plandate.Ticks);
            TimeSpan t3 = t2.Subtract(t1);
            if (flag == '1')
            {
                return -1;
            }
            else if (t3.Days > int.Parse(WarningDay))
            {
                return 0;
            }
            else if (t3.Days < int.Parse(WarningDay) && t3.Days >= 0)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }

        /// <summary>
        /// 得到合同本次收款信息
        /// </summary>
        private void GetCoperationPayRecord()
        {
            object chargeSysNoObj = TG.DBUtility.DbHelperSQL.GetSingle(string.Format("select chg_id from cm_RealCprChg where ID = {0}", RealPaySysNo));

            int chrageSysNo = chargeSysNoObj == null ? 0 : Convert.ToInt32(chargeSysNoObj);

            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            string strSql = " SELECT ID,Times,persent,payCount,paytime,(SELECT SUM(payCount) FROM cm_RealCprChg WHERE chg_id=cm_CoperationCharge.ID) AS payShiCount FROM cm_CoperationCharge WHERE ID=" + chrageSysNo;
            this.gv_Coperation.DataSource = bll_db.GetList(strSql);
            this.gv_Coperation.DataBind();
        }

        /// <summary>
        /// 得到实际收费情报
        /// </summary>
        private void GetRealPayRecord()
        {
            TG.Model.cm_RealCprChg realCprChg = new TG.BLL.cm_RealCprChg().GetModel(RealPaySysNo);

            if (realCprChg == null)
            {
                Response.Write("<script type=\"text/javascript\">alert(\"本次收费信息已经被财务拒结！\"); window.location.href=\"/Coperation/CoperationCharge.aspx\"</script>");
                return;
            }

            payCount.Text = realCprChg.payCount.ToString();
            remitter.Text = realCprChg.Remitter;
            times.Text = Convert.ToDateTime(realCprChg.payTime).ToString("yyyy-MM-dd");
            user.Text = realCprChg.acceptuser;
            BillNo.Text = realCprChg.BillNo;
            mark.Text = realCprChg.mark;

            if (realCprChg.Status == "D" || realCprChg.Status == "S")
            {
                sureButton.Enabled = false;
                notSureButton.Enabled = false;
            }
            else
            {
                sureButton.Enabled = true;
                notSureButton.Enabled = true;
            }
        }

        protected void sureButton_Click(object sender, ImageClickEventArgs e)
        {
            UpDateCoperationChargeRecord("S", "到账确认通过");
        }

        protected void notSureButton_Click(object sender, ImageClickEventArgs e)
        {
            string deleteSql = string.Format("delete cm_RealCprChg where ID={0};", RealPaySysNo);

            int count = TG.DBUtility.DbHelperSQL.ExecuteSql(deleteSql);

            if (count > 0)
            {
                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("coperationName={0}&deparmentName={1}&fromUser={2}&sysNo={3}", CoperationName, DepartmentName, UserSysNo, RealPaySysNo),
                    FromUser = FromUser,
                    InUser = UserSysNo,
                    MsgType = 6,
                    ToRole = "0",
                    MessageContent = string.Format("关于{0}部门的{1}合同{2}！", DepartmentName, CoperationName, "确认到账不通过！"),
                    QueryCondition = CoperationName
                };
                int resultCount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);

                if (resultCount > 0)
                {
                    Response.Write("<script type=\"text/javascript\">window.location.href=\"/Coperation/CoperationCharge.aspx\"</script>");
                }
                else
                {
                    Response.Write("<script type=\"text/javascript\">window.location.href=\"/Coperation/CoperationCharge.aspx\"</script>");
                }
            }

            //UpDateCoperationChargeRecord("D", "到账确认不通过");
        }

        private void UpDateCoperationChargeRecord(string status, string msgContent)
        {
            string updateSql = string.Format("update cm_RealCprChg set Status =N'{0}' where ID = {1}", status, RealPaySysNo);

            int count = TG.DBUtility.DbHelperSQL.ExecuteSql(updateSql);

            if (count > 0)
            {
                //发送消息给财务
                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("coperationName={0}&deparmentName={1}&fromUser={2}&sysNo={3}", CoperationName, DepartmentName, UserSysNo, RealPaySysNo),
                    FromUser = FromUser,
                    InUser = UserSysNo,
                    MsgType = 6,
                    ToRole = "0",
                    MessageContent = string.Format("关于{0}部门的{1}合同{2}！", DepartmentName, CoperationName, msgContent),
                    QueryCondition = CoperationName
                };
                int resultCount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                if (resultCount > 0)
                {
                    Response.Write("<script type=\"text/javascript\">window.location.href=\"/Coperation/CoperationCharge.aspx\"</script>");
                }
                else
                {
                    Response.Write("<script type=\"text/javascript\">window.location.href=\"/Coperation/CoperationCharge.aspx\"</script>");
                }
            }
        }
    }
}