﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.BLL;
using TG.Model;

namespace TG.Web.Coperation
{
    public partial class cpr_ModifyConstruCoperartionBymaster : PageBase
    {
        //是否具有修改权限
        public string HasAudit { get; set; }
        //审批流程获取的修改权限
        public string HasEditAudit
        {
            get
            {
                return Request["audit"] ?? "0";
            }
        }
        ////审批流程获取的修改权限ID
        public int AuditEditSysNo
        {
            get
            {
                int auditid = 0;
                int.TryParse(Request["auditeditsysno"] ?? "0", out auditid);
                return auditid;
            }
        }
        public int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["msgno"] ?? "0", out msgid);
                return msgid;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string str_cprid = Request.QueryString["cprid"] ?? "";
            if (!IsPostBack)
            {
                //保存合同编号
                this.hid_cprid.Value = str_cprid;
                //绑定合同类别
                BindCorpType();
                //行业性质
                BindCorpHyxz();
                //工程来源
                BindCorpSrc();
                //设计等级
                BindBuildType();

                //显示联系人
                ShowContractInfo(str_cprid);
                //显示合同信息
                ShowCoperation(str_cprid);
                //如果是审批修改流程
                //HasAudit = CheckAudit(int.Parse(str_cprid)) == true ? "1" : "0";
                //if (HasAudit == "1")
                //{
                //    Response.Write("<script language='javascript' >alert('该合同已提交审批，不能被修改！');window.history.back();</script>");
                //}

                //绑定权限
                BindPreviewPower();
            }
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Save_Click(object sender, EventArgs e)
        {
            if (UpdateCoperation())
            {
                //弹出提示
                TG.Common.MessageBox.ResponseScriptBack(this, "合同信息更新成功！");
            }
        }

        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;
                //string NotShowUnitList = base.NotShowUnitList;
                ChooseCustomer1.UserSysNo = base.UserSysNo;
                ChooseCustomer1.PreviewPower = base.RolePowerParameterEntity.PreviewPattern;
                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
                //this.NotShowUnitList.Value = NotShowUnitList;
            }
        }
        //获取页面权限
        public int GetPreviewPower()
        {
            int UserSysNo = int.Parse(GetCurMemID());
            string PageName = "cpr_CorperationList.aspx";
            List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, PageName);

            //浏览权限
            int Power = 0;
            if ((from role in rolePowerParameterList where role.PreviewPattern == "2" select role).Count() > 0)
            {
                //部门
                Power = 2;
            }
            if ((from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0)
            {
                //全部
                Power = 1;
            }
            return Power;
        }
        //返回一个文件上传的随机ID
        public string GetCoperationID()
        {
            return this.hid_cprid.Value;
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.ddcpr_Type.DataSource = bll_dic.GetList(str_where);
            this.ddcpr_Type.DataTextField = "dic_Name";
            this.ddcpr_Type.DataValueField = "ID";
            this.ddcpr_Type.DataBind();

        }

        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz'";
            this.ddProfessionType.DataSource = bll_dic.GetList(str_where);
            this.ddProfessionType.DataTextField = "dic_Name";
            this.ddProfessionType.DataValueField = "ID";
            this.ddProfessionType.DataBind();
        }

        //建筑类型
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_kcbuildtype'";
            this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "ID";
            this.drp_buildtype.DataBind();
        }
        //工程来源
        protected void BindCorpSrc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_src'";
            this.ddSourceWay.DataSource = bll_dic.GetList(str_where);
            this.ddSourceWay.DataTextField = "dic_Name";
            this.ddSourceWay.DataValueField = "ID";
            this.ddSourceWay.DataBind();
        }

        //显示联系人
        protected void ShowContractInfo(string cprid)
        {
            string strSql = " Select cst_Id From [cm_ConstruCoperartion] Where cpr_Id=" + cprid;
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            DataSet ds_cst = bll_db.GetList(strSql);
            if (ds_cst.Tables.Count > 0)
            {
                if (ds_cst.Tables[0].Rows.Count > 0)
                {
                    string str_cstid = ds_cst.Tables[0].Rows[0][0].ToString();
                    //保存客户ID
                    this.hid_cstid.Value = str_cstid;

                    TG.BLL.cm_CustomerInfo bll_cst = new TG.BLL.cm_CustomerInfo();
                    string str_where = " Cst_ID=" + str_cstid;
                    TG.Model.cm_CustomerInfo model_cst = bll_cst.GetModel(int.Parse(str_cstid));
                    if (model_cst != null)
                    {
                        this.txtCst_No.Value = model_cst.Cst_No.Trim();
                        this.txtCst_Name.Value = model_cst.Cst_Name.Trim();
                        this.txtCpy_Address.Value = model_cst.Cpy_Address.Trim();
                        this.txtCst_Brief.Value = model_cst.Cst_Brief;
                        this.txtCode.Value = model_cst.Code ?? "";
                        this.txtLinkman.Value = model_cst.Linkman ?? "";
                        this.txtCpy_Phone.Value = model_cst.Cpy_Phone ?? "";
                        this.txtCpy_Fax.Value = model_cst.Cpy_Fax ?? "";
                    }
                }
            }
        }

        //显示合同信息
        protected void ShowCoperation(string cprid)
        {
            TG.BLL.cm_ConstruCoperartion bll_cpr = new TG.BLL.cm_ConstruCoperartion();
            TG.Model.cm_ConstruCoperartion model_cpr = bll_cpr.GetModel(int.Parse(cprid));
            if (model_cpr != null)
            {

                //赋值
                this.txtcpr_No.Value = model_cpr.cpr_No == null ? "" : model_cpr.cpr_No.Trim();
                this.ddcpr_Type.Items.FindByText(model_cpr.cpr_Type.Trim()).Selected = true;
                this.txt_cprType.Value = Convert.ToString(model_cpr.cpr_Type2 ?? "").Trim();
                //合同名称
                this.txt_cprName.Value = model_cpr.cpr_Name.Trim();
                //建设单位
                this.txt_cprBuildUnit.Value = model_cpr.BuildUnit == null ? "" : model_cpr.BuildUnit.Trim();
                //建筑类别
                this.drp_buildtype.Items.FindByText(model_cpr.BuildType.Trim()).Selected = true;

                this.txtcpr_Account.Value = model_cpr.cpr_Acount.ToString();
                //实际合同额
                this.txtcpr_Account0.Value = model_cpr.cpr_ShijiAcount.ToString();
                //投资额
                this.txtInvestAccount.Value = model_cpr.cpr_Touzi.ToString();
                //统计日期
                this.txtSingnDate.Value = Convert.ToDateTime(model_cpr.cpr_SignDate).ToString("yyyy-MM-dd");
                //签订日期
                this.txtSingnDate2.Value = Convert.ToDateTime(model_cpr.cpr_SignDate2).ToString("yyyy-MM-dd");
                //完成日期
                this.txtCompleteDate.Value = model_cpr.cpr_DoneDate == null ? "" : Convert.ToDateTime(model_cpr.cpr_DoneDate).ToString("yyyy-MM-dd");

                //项目经理
                this.txt_proFuze.Value = model_cpr.ChgPeople.Trim();
                //添加项目经理ID 
                this.HiddenPMUserID.Value = model_cpr.PMUserID.ToString();
                //经理电话
                this.txt_fzphone.Value = model_cpr.ChgPhone.Trim();
                //甲方负责人
                this.txtFParty.Value = model_cpr.ChgJia.Trim();
                //甲方负责人电话
                this.txt_jiafphone.Value = model_cpr.ChgJiaPhone.Trim();
                //工程地点
                this.txt_ProjectPosition.Value = model_cpr.BuildPosition.Trim();
                //行业性质
                if (model_cpr.Industry.Trim() != "" && model_cpr.Industry.Trim() != "-1")
                {
                    this.ddProfessionType.Items.FindByText(model_cpr.Industry.Trim()).Selected = true;
                }
                //工程来源
                if (model_cpr.BuildSrc.Trim() != "" && model_cpr.BuildSrc.Trim() != "-1")
                {
                    this.ddSourceWay.Items.FindByText(model_cpr.BuildSrc.Trim()).Selected = true;
                }

                this.txt_cjbm.Value = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();
                //承接部门
                this.hid_cjbm.Value = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();
                //制表人
                this.txt_tbcreate.Value = model_cpr.TableMaker == null ? "" : model_cpr.TableMaker.Trim();
                //录入时间
                this.hid_cprtime.Value = model_cpr.RegTime.ToString();

                //层数
                string[] floors = model_cpr.Floor.Split(new char[] { '|' }, StringSplitOptions.None);
                this.txt_upfloor.Value = floors[0].ToString().Trim();
                this.txt_downfloor.Value = floors[1].ToString().Trim();
                //多建筑
                this.txt_MultiBuild.Value = model_cpr.MultiBuild;
                //备注
                this.txtcpr_Remark.Value = model_cpr.cpr_Mark == null ? "" : model_cpr.cpr_Mark.Trim();
                //建筑面积
                txt_buildArea.Value = model_cpr.BuildArea.ToString();
                //监理单位
                txt_SupervisorUnit.Value = model_cpr.SupervisorUnit;
                //设计单位
                txt_DesignUnit.Value = model_cpr.DesignUnit;
                //总监
                txt_Director.Value = model_cpr.Director;
                //电话
                txt_DirectorPhone.Value = model_cpr.DirectorPhone;
                //占地面积
                txt_AreaUnit.Value = model_cpr.AreaUnit;
                //工期
                txt_ProjectDate.Value = model_cpr.ProjectDate.ToString();

                //外键ID
                hid_cprFid.Value = model_cpr.Cpr_FID == null ? "0" : model_cpr.Cpr_FID.ToString();
            }
        }
        // 检查是否在审核队列中
        private bool CheckAudit(int coperationSysNo)
        {
            return new TG.BLL.cm_CoperationAudit().IsExist(coperationSysNo);
        }
        //修改合同信息
        protected bool UpdateCoperation()
        {
            TG.BLL.cm_ConstruCoperartion bll_cpr = new TG.BLL.cm_ConstruCoperartion();
            TG.Model.cm_ConstruCoperartion model_cpr = bll_cpr.GetModel(int.Parse(this.hid_cprid.Value));
            TG.Model.cm_Coperation model_cprMain = new TG.Model.cm_Coperation();

            //更新ad
            bool affectRow = false;
            //合同ID
            model_cpr.cpr_Id = int.Parse(this.hid_cprid.Value);
            //客户ID
            model_cprMain.cst_Id = int.Parse(this.hid_cstid.Value);
            model_cpr.cst_Id = int.Parse(this.hid_cstid.Value);
            //合同编号
            model_cprMain.cpr_No = this.txtcpr_No.Value.Trim();
            model_cpr.cpr_No = this.txtcpr_No.Value.Trim();

            //合同分类
            model_cprMain.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            model_cpr.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            //合同类型
            model_cprMain.cpr_Type2 = this.txt_cprType.Value;
            model_cpr.cpr_Type2 = this.txt_cprType.Value;
            //合同名称
            model_cprMain.cpr_Name = this.txt_cprName.Value;
            model_cpr.cpr_Name = this.txt_cprName.Value;
            //建设单位
            model_cprMain.BuildUnit = this.txt_cprBuildUnit.Value;
            model_cpr.BuildUnit = this.txt_cprBuildUnit.Value;
            //建筑类型
            model_cprMain.BuildType = this.drp_buildtype.SelectedItem.Text.Trim();
            model_cpr.BuildType = this.drp_buildtype.SelectedItem.Text.Trim();
            //建筑层数
            model_cprMain.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            model_cpr.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            //工程负责人
            model_cprMain.ChgPeople = this.txt_proFuze.Value;
            model_cprMain.ChgPhone = this.txt_fzphone.Value;
            model_cpr.ChgPeople = this.txt_proFuze.Value;
            model_cpr.ChgPhone = this.txt_fzphone.Value;
            //甲方负责人
            model_cprMain.ChgJia = this.txtFParty.Value;
            model_cprMain.ChgJiaPhone = this.txt_jiafphone.Value;
            model_cpr.ChgJia = this.txtFParty.Value;
            model_cpr.ChgJiaPhone = this.txt_jiafphone.Value;
            //承接部门
            if (this.txt_cjbm.Value.Trim() != "")
            {
                model_cprMain.cpr_Unit = this.hid_cjbm.Value;
                model_cpr.cpr_Unit = this.hid_cjbm.Value;
            }
            //工程地点
            if (this.txt_ProjectPosition.Value.Trim() != "")
            {
                model_cprMain.BuildPosition = this.txt_ProjectPosition.Value;
                model_cpr.BuildPosition = this.txt_ProjectPosition.Value;
            }
            else
            {
                model_cprMain.BuildPosition = "";
                model_cpr.BuildPosition = "";
            }

            //合同额
            if (this.txtcpr_Account.Value.Trim() == "")
            {
                model_cprMain.cpr_Acount = 0;
                model_cpr.cpr_Acount = 0;
            }
            else
            {
                model_cprMain.cpr_Acount = Convert.ToDecimal(this.txtcpr_Account.Value);
                model_cpr.cpr_Acount = Convert.ToDecimal(this.txtcpr_Account.Value);
            }
            //实际合同额
            if (this.txtcpr_Account0.Value.Trim() == "")
            {
                model_cprMain.cpr_ShijiAcount = 0;
                model_cpr.cpr_ShijiAcount = 0;
            }
            else
            {
                model_cprMain.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);
                model_cpr.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);
            }
            //投资额
            if (this.txtInvestAccount.Value.Trim() == "")
            {
                model_cprMain.cpr_Touzi = 0;
                model_cpr.cpr_Touzi = 0;
            }
            else
            {
                model_cprMain.cpr_Touzi = Convert.ToDecimal(this.txtInvestAccount.Value);
                model_cpr.cpr_Touzi = Convert.ToDecimal(this.txtInvestAccount.Value);
            }
            //行业性质
            if (this.ddProfessionType.SelectedIndex != 0)
            {
                model_cprMain.Industry = this.ddProfessionType.SelectedItem.Text;
                model_cpr.Industry = this.ddProfessionType.SelectedItem.Text;
            }
            else
            {
                model_cprMain.Industry = this.ddProfessionType.Items[0].Value;
                model_cpr.Industry = this.ddProfessionType.Items[0].Value;
            }
            //工程来源
            if (this.ddSourceWay.SelectedIndex != 0)
            {
                model_cprMain.BuildSrc = this.ddSourceWay.SelectedItem.Text;
                model_cpr.BuildSrc = this.ddSourceWay.SelectedItem.Text;
            }
            else
            {
                model_cprMain.BuildSrc = this.ddSourceWay.Items[0].Value;
                model_cpr.BuildSrc = this.ddSourceWay.Items[0].Value;
            }
            //制表人
            model_cprMain.TableMaker = this.txt_tbcreate.Value;
            model_cpr.TableMaker = this.txt_tbcreate.Value;


            //统计年份
            model_cprMain.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            model_cpr.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            //开始日期
            model_cprMain.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value);
            model_cpr.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value );
            //完成日期
            model_cprMain.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            model_cpr.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            //工期
            model_cpr.ProjectDate = txt_ProjectDate.Value == "" ? 0 : int.Parse(txt_ProjectDate.Value);
            //备注
            model_cprMain.cpr_Mark = this.txtcpr_Remark.Value;
            model_cpr.cpr_Mark = this.txtcpr_Remark.Value;
            //多栋楼备注
            model_cprMain.MultiBuild = this.txt_MultiBuild.Value;
            model_cpr.MultiBuild = this.txt_MultiBuild.Value;

            model_cprMain.RegTime = Convert.ToDateTime(this.hid_cprtime.Value);
            model_cprMain.UpdateBy = UserSysNo.ToString();
            model_cprMain.LastUpdate = DateTime.Now;

            model_cpr.RegTime = Convert.ToDateTime(this.hid_cprtime.Value);
            model_cpr.UpdateBy = UserSysNo.ToString();
            model_cpr.LastUpdate = DateTime.Now;

            //建筑面积
            model_cprMain.BuildArea = txt_buildArea.Value == "" ? "0" : txt_buildArea.Value;
            model_cpr.BuildArea = txt_buildArea.Value == "" ? 0 : decimal.Parse(txt_buildArea.Value);
            //监理单位
            model_cpr.SupervisorUnit = txt_SupervisorUnit.Value;
            //设计单位
            model_cpr.DesignUnit = txt_DesignUnit.Value;
            //总监
            model_cpr.Director = txt_Director.Value;
            //电话
            model_cpr.DirectorPhone = txt_DirectorPhone.Value;
            //占地面积
            model_cpr.AreaUnit = txt_AreaUnit.Value;

            try
            {
                //更新主合同
                model_cprMain.cpr_Id = int.Parse(hid_cprFid.Value);
                new TG.BLL.cm_Coperation().Update(model_cprMain);
                affectRow = bll_cpr.Update(model_cpr);

            }
            catch (System.Exception ex)
            {
            }

            return affectRow;
        }

    }
}