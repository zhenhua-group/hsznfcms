﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
using Geekees.Common.Controls;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace TG.Web.Coperation
{
    public partial class CoperationStandBook : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                SelectedCurYear();
                LoadUnitTree();
                SelectedCurUnit();
                //统计数据
                GetAllData();
            }
        }

        #region 基本设置 2015年6月9日
        /// <summary>
        /// 统计语言
        /// </summary>
        public string CountTip
        {
            get;
            set;
        }
        /// <summary>
        /// 当前年
        /// </summary>
        public string CurYear { get; set; }
        public string CurMonth { get; set; }
        /// <summary>
        /// 前一年
        /// </summary>
        public string BeforeYear { get; set; }
        public string BeforeMonth { get; set; }
        /// <summary>
        /// 表格数据
        /// </summary>
        public string HtmlTable { get; set; }
        /// <summary>
        /// 绑定多选年
        /// </summary>
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();

            if (list.Count > 0)
            {
                foreach (string str in list)
                {
                    //加载年
                    drpYear.Items.Add(new ListItem(str, str));
                    //drpYear2.Items.Add(new ListItem(str, str));
                }
            }
        }
        /// <summary>
        /// 选中当前年
        /// </summary>
        protected void SelectedCurYear()
        {
            int curyear = DateTime.Now.Year;
            int curmonth = DateTime.Now.Month;
            //当前年
            //if (drpYear2.Items.FindByValue(curyear.ToString()) != null)
            //{
            //    drpYear2.Items.FindByValue(curyear.ToString()).Selected = true;
            //}
            if (drpYear.Items.FindByValue(curyear.ToString()) != null)
            {
                drpYear.Items.FindByValue(curyear.ToString()).Selected = true;
            }

            //当前月
            if (curmonth == 1)
            {
                curmonth = 1;
            }
            drpMonth.Items.FindByValue((curmonth - 1).ToString()).Selected = true;
            //drpMonth2.Items.FindByValue(curmonth.ToString()).Selected = true;
        }

        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { UserUnitNo.ToString() };
            this.drp_unit.CheckNodes(curUnit, true);
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = " 1=1 ";

            if (base.RolePowerParameterEntity != null)
            {

                //个人
                if (base.RolePowerParameterEntity.PreviewPattern == 0)
                {
                    strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
                }
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    strWhere = " unit_ID =" + UserUnitNo;
                }
                else
                {
                    strWhere = " 1=1 ";
                }
            }
            else
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }

        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
            else
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                Model.tg_unit unit = new TG.BLL.tg_unit().GetModel(UserUnitNo);
                if (unit != null)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(unit.unit_Name.Trim(), unit.unit_ID.ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }

            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drp_unit.Theme = macOS;
        }
        /// <summary>
        /// 选中当前年
        /// </summary>
        protected void GetSelectYear()
        {
            this.BeforeYear = this.drpYear.SelectedValue;//年
            this.BeforeMonth = this.drpMonth.SelectedValue;//月
            //this.CurYear = this.drpYear2.SelectedValue;//当年
            //this.CurMonth = this.drpMonth2.SelectedValue;//当月
        }
        #endregion

        #region 统计表


        /// <summary>
        /// 获取统计表
        /// </summary>
        protected DataTable GetCountData()
        {
            StringBuilder sbTable = new StringBuilder();

            List<ASTreeViewNode> checkedNods = this.drp_unit.GetCheckedNodes(false);
            //每月的天数
            string days = "30";
            if (BeforeMonth != "0")
            {
                days = DateTime.DaysInMonth(int.Parse(BeforeYear), int.Parse(BeforeMonth)).ToString();
            }

            //查询语句
            StringBuilder strSql = new StringBuilder();
            //全没选
            if (checkedNods.Count == 0)
            {
                //循环部门
                DataTable dt = GetUnit();
                //部门名称

                foreach (DataRow dr in dt.Rows)
                {
                    string unitname = dr["unit_Name"].ToString();
                    strSql.AppendFormat(@"
                                        Select cpr_ID,cpr_No
	                                   ,cpr_Name
	                                   ,BuildArea
	                                   ,cpr_Acount
	                                   ,ISNULL(convert(decimal(18,4),(
			                                Select SUM(Acount)
			                                From cm_ProjectCharge
			                                Where cprID=C.cpr_Id 
		                                )),0) AS ChgAcount
	                                   ,(
			                                convert(decimal(18,4),cpr_Acount-ISNULL((
								            Select SUM(Acount)
								            From cm_ProjectCharge
								            Where cprID=C.cpr_Id
		                                ),0))) AS ChgNoAcount
		                                ,cpr_Process
		                                ,ISNULL((
			                                Select pro_status
			                                From cm_Project
			                                Where ReferenceSysNo=cpr_Id
		                                 ),'') AS ProjProcess
                                        From cm_Coperation C
                                        Where cpr_Unit ='{0}' 
	                                    AND (Select COUNT(*)
	                                         From cm_ProjectCharge
	                                         Where cprID=c.cpr_Id ", unitname);
                    //收款年
                    if (drpMonth.SelectedValue == "0")
                    {
                        strSql.AppendFormat(" AND YEAR(InAcountTime)={0} ", BeforeYear);
                    }
                    else
                    {
                        strSql.AppendFormat(" AND InAcountTime BETWEEN '{0}-{1}-01' AND '{0}-{1}-{2}' ", BeforeYear, BeforeMonth, days);
                    }
                    strSql.Append(")>0 ");
                    //项目名称
                    if (this.txtProjName.Text != "")
                    {
                        strSql.AppendFormat(" AND c.cpr_Name LIKE '%{0}%' ", txtProjName.Text.Trim());
                    }
                    strSql.Append(" UNION ");
                }
            }
            else
            {
                foreach (ASTreeViewNode node in checkedNods)
                {
                    if (node.NodeValue == "0")
                        continue;
                    //部门名称
                    string unitname = node.NodeText.Trim();
                    strSql.AppendFormat(@"
                                        Select cpr_ID,cpr_No
	                                   ,cpr_Name
	                                   ,BuildArea
	                                   ,cpr_Acount
	                                   ,ISNULL((
			                                Select SUM(Acount)
			                                From cm_ProjectCharge
			                                Where cprID=C.cpr_Id 
		                                ),0) AS ChgAcount
	                                   ,(
			                                cpr_Acount-ISNULL((
								                                Select SUM(Acount)
								                                From cm_ProjectCharge
								                                Where cprID=C.cpr_Id
		                                ),0)) AS ChgNoAcount
		                                ,cpr_Process
		                                ,ISNULL((
			                                Select pro_status
			                                From cm_Project
			                                Where ReferenceSysNo=cpr_Id
		                                 ),'') AS ProjProcess
                                        From cm_Coperation C
                                        Where cpr_Unit ='{0}' 
	                                    AND (Select COUNT(*)
	                                         From cm_ProjectCharge
	                                         Where cprID=c.cpr_Id ", unitname);
                    //收款年
                    if (drpMonth.SelectedValue == "0")
                    {
                        strSql.AppendFormat(" AND YEAR(InAcountTime)={0} ", BeforeYear);
                    }
                    else
                    {
                        strSql.AppendFormat(" AND InAcountTime BETWEEN '{0}-{1}-01' AND '{0}-{1}-{2}' ", BeforeYear, BeforeMonth, days);
                    }

                    strSql.Append(")>0 ");
                    //项目名称
                    if (this.txtProjName.Text != "")
                    {
                        strSql.AppendFormat(" AND c.cpr_Name LIKE '%{0}%' ", txtProjName.Text.Trim());
                    }
                    strSql.Append(" UNION ");
                }
            }

            strSql = strSql.Remove(strSql.ToString().LastIndexOf("UNION") - 1, 6);

            DataTable dtData = DBUtility.DbHelperSQL.Query(strSql.ToString()).Tables[0];
            return dtData;
        }
        /// <summary>
        /// 创建统计行
        /// </summary>
        /// <param name="unitname"></param>
        /// <returns></returns>
        private void CreateTableRowByUnit()
        {
            StringBuilder sbTable = new StringBuilder();
            //获取数据表
            DataTable dtData = GetCountData();

            int index = 1;
            foreach (DataRow dr in dtData.Rows)
            {
                sbTable.Append("<tr>");
                sbTable.AppendFormat("<td>{0}</td>", index++);
                sbTable.AppendFormat("<td>{0}</td>", dr["cpr_No"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", dr["cpr_Name"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", dr["BuildArea"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", dr["cpr_Acount"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", dr["ChgAcount"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", dr["ChgNoAcount"].ToString());
                string datelist = GetChgCountList(dr["cpr_ID"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", datelist);//收款日期
                //excel
                datelist = GetChgCountListExcel(dr["cpr_ID"].ToString());
                sbTable.AppendFormat("<td>{0}</td>", "");//dr["cpr_Process"].ToString()
                sbTable.AppendFormat("<td>{0}</td>", "");//dr["ProjProcess"].ToString()
                sbTable.AppendFormat("<td>{0}</td>", "");//备注
                sbTable.Append("</tr>");

                //创建对象列表
                TG.Model.CoperationStandBookExcel excel = new TG.Model.CoperationStandBookExcel()
                {
                    CprNo = dr["cpr_No"].ToString(),
                    CprName = dr["cpr_Name"].ToString(),
                    BuildArea = dr["BuildArea"].ToString(),
                    CprAcount = dr["cpr_Acount"].ToString(),
                    ChgAcount = dr["ChgAcount"].ToString(),
                    NoChgAcount = dr["ChgNoAcount"].ToString(),
                    ChgDate = datelist,
                    ProjProcess = "",
                    CprProcess = "",
                    Sub = ""
                };

                this.ImportExcelList.Add(excel);
            }

            this.HtmlTable = sbTable.ToString();

            //保存导出列表对象
            ViewState["ExcelList"] = this.ImportExcelList;
        }


        /// <summary>
        /// 统计提示
        /// </summary>
        protected void GetCountCondition()
        {
            string strTip = "";
            strTip += this.drpYear.SelectedValue + "年";
            strTip += this.drpMonth.SelectedValue + "月";
            //选中的部门
            foreach (ASTreeViewNode node in this.drp_unit.GetCheckedNodes(false))
            {
                if (node.NodeValue != "0")
                {
                    strTip += "[" + node.NodeText + "]";
                }
                else
                {
                    strTip += "全部生产部门";
                }
            }
            strTip += "设计合同及合同收费详情";

            this.CountTip = strTip;
        }
        #endregion

        protected void GetAllData()
        {
            //选中当前年
            GetSelectYear();
            //组织话术
            GetCountCondition();
            //获取数据
            CreateTableRowByUnit();
        }


        /// <summary>
        /// 查询时间
        /// </summary>
        /// <param name="unitid"></param>
        /// <returns></returns>
        protected string GetChgCountList(string cprID)
        {
            string strSql = "";
            //收费
            strSql = string.Format(@" Select (convert(varchar(10),InAcountTime,120)+'收款'
		                                    +convert(varchar,(convert(decimal(18,4),Acount)))+'万元') 
		                                    AS ChgList
                                    From cm_ProjectCharge
                                    Where cprID={0} Order By ID DESC ", cprID);
            //收款列表
            DataTable dt = DBUtility.DbHelperSQL.Query(strSql).Tables[0];

            string html = "";
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    html += "<span>" + dr["ChgList"].ToString() + "</span><br/>";
                }
            }
            return html;
        }
        /// <summary>
        /// 查询时间
        /// </summary>
        /// <param name="unitid"></param>
        /// <returns></returns>
        protected string GetChgCountListExcel(string cprID)
        {
            string strSql = "";
            //收费
            strSql = string.Format(@" Select (convert(varchar(10),InAcountTime,120)+'收款'
		                                    +convert(varchar,(convert(decimal(18,4),Acount)))+'万元') 
		                                    AS ChgList
                                    From cm_ProjectCharge
                                    Where cprID={0} Order By ID DESC ", cprID);
            //收款列表
            DataTable dt = DBUtility.DbHelperSQL.Query(strSql).Tables[0];

            string html = "";
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    html += dr["ChgList"].ToString() + "\n";
                }
            }
            return html;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GetAllData();
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcelByData();
        }

        /// <summary>
        /// 导出列表列表集合
        /// </summary>
        protected List<TG.Model.CoperationStandBookExcel> ImportExcelList =
            new List<TG.Model.CoperationStandBookExcel>();
        /// <summary>
        /// 导出Excel
        /// </summary>
        protected void ExportExcelByData()
        {
            GetCountCondition();
            //导出列表
            List<TG.Model.CoperationStandBookExcel> excellist = ViewState["ExcelList"] as List<TG.Model.CoperationStandBookExcel>;
            //模板路径
            string pathModel = "~/TemplateXls/CoperationStandBookTlt.xls";

            //导出Excel
            ExportDataToExcel(excellist, pathModel);
        }
        private void ExportDataToExcel(List<TG.Model.CoperationStandBookExcel> excellist, string modelPath)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);
            //导出表标题
            string title = this.drpYear.SelectedValue + "年" + this.drpMonth.SelectedValue + "月设计合同及合同收费情况";
            if (this.drpMonth.SelectedValue == "0")
            {
                title = this.drpYear.SelectedValue + "年设计合同及合同收费情况";
            }
            ws.GetRow(0).GetCell(0).SetCellValue(title);

            //设置样式
            ICellStyle style = wb.CreateCellStyle();

            //设置字体
            IFont font = wb.CreateFont();
            font.FontHeightInPoints = 9;//字号
            font.FontName = "宋体";
            style.SetFont(font);
            int index = 2;
            //设置边框
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            style.WrapText = true;//自动换行
            style.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;//居中
            style.VerticalAlignment = VerticalAlignment.CENTER;//
            //设置宽度
            // ws.SetColumnWidth(0, 15 * 256);
            //ws.SetColumnWidth(2, 25 * 256);
            if (excellist.Count > 0)
            {
                int i = 0;
                foreach (TG.Model.CoperationStandBookExcel item in excellist)
                {
                    i++;
                    //CprNo CprName BuildArea CprAcount Acount Owe Chargedetail ProStage CopStage Mark
                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(i);
                    cell0.CellStyle = style;
                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.CprNo);
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(item.CprName);
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.BuildArea);
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(Convert.ToDouble(item.CprAcount)); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(Convert.ToDouble(item.ChgAcount)); cell5.CellStyle = style;
                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(Convert.ToDouble(item.NoChgAcount)); cell6.CellStyle = style;
                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.ChgDate); cell7.CellStyle = style;
                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.ProjProcess); cell8.CellStyle = style;
                    ICell cell9 = row.CreateCell(9);
                    cell9.SetCellValue(item.CprProcess); cell9.CellStyle = style;
                    ICell cell10 = row.CreateCell(10);
                    cell10.SetCellValue(item.Sub); cell10.CellStyle = style;

                    index++;
                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);
                //导出表标题
                string name = System.Web.HttpContext.Current.Server.UrlEncode(title + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}