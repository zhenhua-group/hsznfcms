﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="cpr_SysValueMsgListViewBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_SysValueMsgListViewBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script src="../js/Coperation/cpr_SysValueMsgListViewBymaster.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">首页 <small>项目产值消息审批列表</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>项目产值消息审批列表</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>查询消息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>名称:</td>
                            <td>
                                <input type="text" class="form-control" id="txtCoperationName" /></td>
                            <td>消息类型:</td>
                            <td>
                                <asp:DropDownList ID="MsgTypeDropDownList" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="-1">-----消息类型-----</asp:ListItem>

                                    <asp:ListItem Value="5">产值分配</asp:ListItem>
                                    <asp:ListItem Value="10">个人分配明细</asp:ListItem>
                                    <asp:ListItem Value="14">二次分配审核</asp:ListItem>
                                    <asp:ListItem Value="15">暖通所分配</asp:ListItem>
                                    <asp:ListItem Value="16">转经济所分配</asp:ListItem>
                                    <asp:ListItem Value="17">分配系数审核</asp:ListItem>
                                    <asp:ListItem Value="18">经济所分配</asp:ListItem>
                                    <asp:ListItem Value="19">转暖通所分配</asp:ListItem>
                                    <asp:ListItem Value="20">转土建所分配</asp:ListItem>
                                    <asp:ListItem Value="25">个人产值确认</asp:ListItem>
                                    <asp:ListItem Value="26">专业负责人重新分配</asp:ListItem>
                                    <asp:ListItem Value="27">经济所长重新分配</asp:ListItem>
                                    <asp:ListItem Value="28">转经济所长重新分配</asp:ListItem>
                                    <asp:ListItem Value="29">转暖通所长重新分配</asp:ListItem>
                                    <asp:ListItem Value="30">转土建所长重新分配</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>
                                <button type="button" class="btn blue " id="btn_Search">
                                    <i class="fa fa-search"></i>
                                </button>
                            </td>
                            <td>
                                <input type="button" class="btn blue " value="未读消息" id="ButtonUnread" /></td>
                            <td>
                                <input type="button" class="btn blue" value="已读消息" id="ButtonRead" /></td>
                            <td>
                                <input type="button" class="btn blue " value="未办事项" id="ButtonDone" /></td>
                            <td>
                                <input type="button" class="btn blue " value="已办事项" id="ButtonUnDone" /></td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>消息审批列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                                没有符合条件数据！
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hiddenMessageType" runat="server" />
    <input type="hidden" id="hiddenMessageDone" runat="server" />
    <input type="hidden" id="hiddenMessageIsDoneStatus" runat="server" />
    <input type="hidden" id="userSysNum" runat="server" />
    <input type="hidden" id="hiddenMessageIsRead" runat="server" />
</asp:Content>
