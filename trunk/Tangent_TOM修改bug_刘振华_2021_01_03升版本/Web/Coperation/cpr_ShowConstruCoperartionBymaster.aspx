﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="cpr_ShowConstruCoperartionBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_ShowConstruCoperartionBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/Coperation/cpr_ShowConstruCoperartionBymaster.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>岩土工程施工合同信息查看</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>合同信息管理</a><i class="fa fa-angle-right"> </i><a>合同管理</a><i class="fa fa-angle-right">
    </i><a>岩土工程施工合同信息查看</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>岩土工程施工合同信息查看
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>

                <div class="portlet-body" style="display: block;">
                    <h4 class="form-section">客户信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 13%">客户编号:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCst_No" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 15%">客户简称:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_JC" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>客户名称:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCst_Name" runat="server"></asp:Label>
                                        </td>
                                        <td>公司地址:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCpy_Address" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>邮政编码:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCode" runat="server"></asp:Label>
                                        </td>
                                        <td>联系人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtLinkman" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>公司电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCpy_Phone" runat="server"></asp:Label>
                                        </td>
                                        <td>传真号:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_Fax" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                    <h4 class="form-section">合同信息</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 100px;">合同编号:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtcpr_No" runat="server"></asp:Label>
                                            <asp:HiddenField ID="hid_cprid" runat="server" Value="" />
                                        </td>
                                        <td style="width: 100px;">合同分类:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddcpr_Type" runat="Server"> </asp:Label>
                                        </td>
                                        <td style="width: 100px;">合同类型:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_cprType" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同名称:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="txt_cprName" runat="server"> </asp:Label>
                                        </td>
                                        <td>建筑类型:
                                        </td>
                                        <td>
                                            <asp:Label ID="drp_buildtype" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建设单位:
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="txt_cprBuildUnit" runat="server"></asp:Label>
                                        </td>
                                        <td>建设规模:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_buildArea" runat="server"></asp:Label>㎡
                                        </td>

                                    </tr>

                                    <tr>
                                        <td>工程负责人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_proFuze" runat="server"></asp:Label>
                                        </td>
                                        <td>电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_fzphone" runat="server"></asp:Label>
                                        </td>
                                        <td>层数:
                                        </td>
                                        <td>地上：<asp:Label ID="lbl_upfloor" runat="server" Text="0"></asp:Label>
                                            层 地下：<asp:Label ID="lbl_downfloor" runat="server" Text="0"></asp:Label>
                                            层

                                        </td>
                                        <%----%>
                                    </tr>
                                    <tr>
                                        <td>甲方负责人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtFParty" runat="server"></asp:Label>
                                        </td>
                                        <td>电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_jiafphone" runat="server"></asp:Label>
                                        </td>
                                        <td>承接部门:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_cjbm" runat="server"></asp:Label>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>工程地点:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddProjectPosition" runat="Server">
                                            </asp:Label>
                                        </td>
                                        <td>行业性质:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddProfessionType" runat="Server">
                                            </asp:Label>
                                        </td>
                                        <td>工程来源:
                                        </td>
                                        <td>
                                            <asp:Label ID="ddSourceWay" runat="Server">
                                            </asp:Label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>合同额:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtcpr_Account" runat="server"></asp:Label>
                                            万元
                                        </td>
                                        <td>实际合同额:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtcpr_Account0" runat="server"></asp:Label>
                                            万元
                                        </td>
                                        <td>投资额:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtInvestAccount" runat="server"></asp:Label>
                                            万元
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>制表人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_tbcreate" runat="server"></asp:Label>
                                        </td>
                                        <td>监理单位:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_SupervisorUnit" runat="Server"></asp:Label>
                                        </td>
                                        <td>设计单位:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_DesignUnit" runat="Server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>总监:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_Director" runat="Server"></asp:Label>
                                        </td>

                                        <td>电话:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_DirectorPhone" runat="Server"></asp:Label>
                                        </td>
                                        <td>占地面积:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_AreaUnit" runat="Server"></asp:Label>亩
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>合同开工日期:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtSingnDate2" runat="server"></asp:Label>
                                        </td>
                                        <td>合同完成日期:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCompleteDate" runat="server"></asp:Label>
                                        </td>
                                        <td>合同统计年份:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtSingnDate" runat="server"></asp:Label>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>工期：</td>
                                        <td>
                                            <asp:Label ID="txt_ProjectDate" runat="Server"></asp:Label>天
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>工程概况及建设规模:
                                        </td>
                                        <td colspan="5">
                                            <asp:Literal ID="txt_multibuild" runat="server" Text=""></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同备注:
                                        </td>
                                        <td colspan="5">
                                            <asp:Literal ID="txtcpr_Remark" runat="server" Text=""></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                    <h4 class="form-section">收费计划</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="sf_datas" style="width: 98%;" align="center">
                                        <tr id="sf_row">
                                            <td align="center" id="sf_id">收费次数
                                            </td>
                                            <td align="center" id="sf_bfb">百分比
                                            </td>
                                            <td align="center" id="sf_edu">付费额(万元)
                                            </td>
                                            <%--                                                <td align="center" id="sf_type">
                                                    收费类型
                                                </td>--%>
                                            <td align="center" id="sf_time">付款时间
                                            </td>
                                            <td align="center" id="sf_mark">备注
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>

                    <h4 class="form-section">承包范围及工程量</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="datas" style="width: 98%;" align="center">
                                        <tr id="sub_row">
                                            <td align="center" id="sub_id">序号
                                            </td>
                                            <td align="center" id="sub_name">施工内容
                                            </td>
                                            <td align="center" id="sub_Diameter">工程桩径(<span style="font-size: 10pt">mm</span>)
                                            </td>
                                            <td align="center" id="sub_Quantity">数量(<span style="font-size: 10pt">根</span>)
                                            </td>
                                            <td align="center" id="sub_Length">桩长(<span style="font-size: 10pt">m</span>)
                                            </td>
                                            <td align="center" id="sub_PitArea">基坑面积(<span style="font-size: 10pt">㎡</span>)
                                            </td>
                                            <td align="center" id="sub_Depth">深度(<span style="font-size: 10pt">m</span>)
                                            </td>
                                            <td align="center" id="sub_Form">支护形式
                                            </td>
                                            <td align="center" id="sub_Well">降水井数量
                                            </td>
                                            <td align="center" id="sub_Concrete">混凝土计划用量
                                            </td>
                                            <td align="center" id="sub_Reinforced">钢筋计划用量
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                    <h4 class="form-section">合同附件</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="datas_att" style="width: 98%;" align="center">
                                        <tr id="att_row">
                                            <td align="center" id="att_id">序号
                                            </td>
                                            <td align="center" id="att_filename">文件名称
                                            </td>
                                            <td align="center" id="att_filesize">文件大小
                                            </td>
                                            <td align="center" id="att_filetype">文件类型
                                            </td>
                                            <td align="center" id="att_uptime">上传时间
                                            </td>
                                            <td align="center" id="att_oper">操作
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-offset-12 col-md-12">
                                    <button type="submit" class="btn green" id="btn_output">
                                        导出</button>
                                    <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                        返回</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hid_flag" runat="server" />
</asp:Content>

