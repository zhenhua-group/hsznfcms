﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SmallShowCharge.aspx.cs"
    Inherits="TG.Web.Coperation.SmallShowCharge" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title></title>
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../css/CprChargStatus.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#gv_Coperation tr").hover(function() {
                $(this).addClass("tr_in");
            }, function() {
                $(this).removeClass("tr_in");
            });
            //关闭
            $("#btn_close").click(function() {
                if (window.opener != undefined) {
                    //for chrom
                    window.opener.returnValue = $("#hid_isdel").val();
                }
                window.returnValue = $("#hid_isdel").val();
                window.close();
            });
            //查看详细
            $(".cls_chk").click(function() {
                //fieldset Title
                $("#title").text($(this).parent().parent().find("td:first").text() + "明细");
                var url = "../HttpHandler/CoperationChargeHandler.ashx";
                var chgid = $(this).next(":hidden").val();
                var data = { "action": "select", "id": "" + chgid + "" };
                //请求数据
                $.post(url, data, function(result) {
                    if (result == "0") {
                        $("#details_data").hide()
                        $("#td_nodata").show();
                        return false;
                    } else {
                        $("#details_panel").show();
                        $("#details_data").show()
                        $("#td_nodata").hide();
                    }
                    var table = result.ds;
                    if ($("#details_data tr").length > 1) {
                        $("#details_data tr:gt(0)").remove();
                    }
                    //生成行
                    $.each(table, function(i, n) {
                        var row = $("#details_row").clone();
                        if (i % 2 == 0) {
                            row.addClass("tr_in");
                        }
                        var oper = "<a href='###' rel='" + n.ID + "'>删除</a>";
                        var img_status = "../images/status/green.gif";
                        var title = "正常收款";
                        if (n.isover == '1') {
                            img_status = "../images/status/red.gif";
                            title = "逾期收款";
                        }
                        var status = "<img title='" + title + "' src='" + img_status + "' style='width:16px;height:16px;'/>";
                        row.find("#d_count").text(n.payCount);
                        row.find("#d_time").text(n.payTime);
                        row.find("#d_user").text(n.acceptuser);
                        row.find("#d_mark").text(n.mark);
                        row.find("#d_status").html(status);
                        row.find("#d_oper").html(oper);
                        row.find("#d_oper a").click(function() {
                            if (confirm("确定要删除此条收款记录吗？")) {
                                //删除事件
                                delSubItem($(this));
                                $(this).parent().parent().remove();
                            }
                        });
                        row.appendTo("#details_data");
                    });

                    data = "";
                }, "json");
            });
        });

        //删除收款信息
        function delSubItem(link) {
            var url = "../HttpHandler/CoperationChargeHandler.ashx";
            var data = { "action": "del", "id": "" + link.attr("rel") + "" };
            $.post(url, data, function(result) {
                if (result == "ok") {
                    alert("删除成功！");
                    var url = document.location.href
                    if (url == "") {
                        url = "SmallShowCharge.aspx?cprid=" + $("#hid_cprid").val() + "&math=" + Math.random();
                    }
                    else {
                        url = url.replace(/###/, '') + "&math=" + Math.random();
                    }
                    $("#reload").attr("href", url);
                    $("#reload").get(0).click();
                }
            });
        }
    </script>

</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <div class="cls_data">
        <table style="width: 690px;" class="cls_content_head">
            <tr>
                <td style="width: 90px;" align="center">
                    计划收费次数
                </td>
                <td style="width: 50px;" align="center">
                    百分比
                </td>
                <td style="width: 100px;" align="center">
                    计划付款(万元)
                </td>
                <td style="width: 100px;" align="center">
                    实际收款(万元)
                </td>
                <td style="width: 100px;" align="center">
                    计划付款时间
                </td>
                <td style="width: 60px;" align="center">
                    状态
                </td>
                <td style="width: 60px;" align="center">
                    收款明细
                </td>
                <td style="width: 130px;" align="center">
                    备备注
                </td>
            </tr>
        </table>
        <asp:GridView ID="gv_Coperation" runat="server" AutoGenerateColumns="False" ShowHeader="False"
            Font-Size="12px" Width="690px" OnRowDataBound="gv_Coperation_RowDataBound">
            <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
            <Columns>
                <asp:BoundField DataField="Times" HeaderText="收款次数">
                    <ItemStyle Width="90px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="persent" HeaderText="百分比">
                    <ItemStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="payCount" HeaderText="计划收款">
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="实际收款">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("payShiCount") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="100px" />
                </asp:TemplateField>
                <asp:BoundField DataField="paytime" HeaderText="计划时间">
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="paytime" HeaderText="状态">
                    <ItemStyle Width="60px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="详细">
                    <ItemTemplate>
                        <a href="###" class="cls_chk">查看</a>
                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("ID") %>' />
                    </ItemTemplate>
                    <ControlStyle Width="60px" />
                    <ItemStyle Width="60px" />
                </asp:TemplateField>
                <asp:BoundField DataField="mark" HeaderText="备注">
                    <ItemStyle Width="130px" />
                </asp:BoundField>
            </Columns>
            <EmptyDataTemplate>
                无收款记录！
            </EmptyDataTemplate>
        </asp:GridView>
        <table border="0" cellspacing="0" cellpadding="0" style="width: 690px; margin: 0 auto;">
            <tr>
                <td>
                    <fieldset style="width: 540px; display: none;" id="details_panel">
                        <legend id="title"></legend>
                        <table class="cls_show_cst_jiben" id="details_data">
                            <tr id="details_row">
                                <td style="width: 100px;" align="center" id="d_count">
                                    金额(万元)
                                </td>
                                <td style="width: 140px;" align="center" id="d_time">
                                    入账时间
                                </td>
                                <td style="width: 100px;" align="center" id="d_user">
                                    入账人
                                </td>
                                <td style="width: 160px;" align="center" id="d_mark">
                                    备注
                                </td>
                                <td style="width: 40px;" align="center" id="d_status">
                                    状态
                                </td>
                                <td style="width: 60px;" align="center" id="d_oper">
                                </td>
                            </tr>
                        </table>
                        <table class="cls_show_cst_jiben" id="td_nodata" style="display: none;">
                            <tr>
                                <td>
                                    没有收款记录！
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </div>
    <div class="cls_data_bottom" style="text-align: center;">
        <a href="#" id="btn_close" style="color: Black; font-size: 10pt;">关闭</a>
    </div>
    <!-- 隐藏控件 -->
    <input id="hid_cprid" type="hidden" value='<%= CprId %>' />
    <input id="hid_isdel" type="hidden" value='<%=IsDelele %>' />
    <a href="" id="reload" style="display: none;"></a>
    </form>
</body>
</html>
