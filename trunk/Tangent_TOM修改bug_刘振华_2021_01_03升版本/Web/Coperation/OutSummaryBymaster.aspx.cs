﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPOI.SS.UserModel;
using System.IO;
using NPOI.HSSF.UserModel;
using System.Text;

namespace TG.Web.Coperation
{
    public partial class OutSummaryBymaster : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                InitDropDownByYear();
                BindYear();
                BindUnit();
                BindPreviewPower();
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;

                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }
        /// <summary>
        ///初始年份
        /// </summary>
        protected void InitDropDownByYear()
        {
            //  this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_Name";
            this.drp_unit.DataBind();
            string sql = @"SELECT u.unit_Name FROM tg_member t LEFT JOIN tg_unit u ON t.mem_Unit_ID=u.unit_ID where mem_ID=" + UserSysNo;
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            this.drp_unit.Text = o.ToString();
        }


        protected void btn_report_Click(object sender, EventArgs e)
        {
            string dateYear = this.drp_year.SelectedItem.Value;
            string title = "";
            if (dateYear != "-1")
            {
                title = dateYear + "年" + this.drp_unit.SelectedItem.Value + "产值汇总表";
            }
            else
            {
                title = "全部年份" + this.drp_unit.SelectedItem.Value + "产值汇总表";
            }


            ToExcel(title);
        }
        public void ToExcel(string title)
        {
            TG.BLL.StandBookBp standbll = new BLL.StandBookBp();
            string unitId = this.drp_unit.SelectedItem.Value;
            string dateYear = this.drp_year.SelectedItem.Value;
            string cprName = this.txt_cprName.Value;
            TG.BLL.CopContrast bll = new BLL.CopContrast();
            List<TG.Model.Outsummary> listSC = new List<TG.Model.Outsummary>();
            if (cprName.Trim() != "")
            {
                listSC = standbll.GetListSummary(unitId, dateYear, cprName);
            }
            else
            {
                listSC = standbll.GetListSummary(unitId, dateYear);
            }
            ExportDataToExcel(listSC, "~/TemplateXls/OutSummary.xls", dateYear);
            //获取excel的文件名称（Guid是一个全球表示，使excel的文件名不同）


        }
        private void ExportDataToExcel(List<TG.Model.Outsummary> listSC, string modelPath, string year)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);
            if (year == "-1")
            {
                year = DateTime.Now.Year.ToString();
            }
            ws.GetRow(0).GetCell(0).SetCellValue(year + "产值汇总表");

            //ws.GetRow(3).GetCell(1).SetCellValue(int.Parse(year) + "年");
            //ws.GetRow(3).GetCell(2).SetCellValue(int.Parse(year) + "年");
            //ws.GetRow(3).GetCell(4).SetCellValue(int.Parse(year) + "年");
            //ws.GetRow(3).GetCell(5).SetCellValue(int.Parse(year) + "年");

            //ws.GetRow(4).GetCell(1).SetCellValue(int.Parse(month) + "月");
            //ws.GetRow(4).GetCell(2).SetCellValue(DateTime.Now.Month + "月");
            //ws.GetRow(4).GetCell(4).SetCellValue(int.Parse(month) + "月");
            //ws.GetRow(4).GetCell(5).SetCellValue(DateTime.Now.Month + "月");
            //设置样式
            ICellStyle style = wb.CreateCellStyle();

            //设置字体
            IFont font = wb.CreateFont();
            font.FontHeightInPoints = 9;//字号
            font.FontName = "宋体";
            style.SetFont(font);
            int index = 2;
            //设置边框
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            style.WrapText = true;//自动换行
            style.Alignment = NPOI.SS.UserModel.HorizontalAlignment.CENTER;//居中
            style.VerticalAlignment = VerticalAlignment.CENTER;//
            //设置宽度
            // ws.SetColumnWidth(0, 15 * 256);
            //ws.SetColumnWidth(2, 25 * 256);
            if (listSC.Count > 0)
            {
                int i = 0;
                foreach (TG.Model.Outsummary item in listSC)
                {
                    i++;
                    //C CprName Charge WeiFenValue ToOtherValue ToOtherValue Mark
                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(i);
                    cell0.CellStyle = style;
                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(item.CprName);
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(Convert.ToDouble(item.Charge));
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(Convert.ToDouble(item.WeiFenValue));
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(Convert.ToDouble(item.ToOtherValue)); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(Convert.ToDouble(item.ActualValue)); cell5.CellStyle = style;
                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.Mark); cell6.CellStyle = style;
                    //ICell cell7 = row.CreateCell(7);
                    //cell7.SetCellValue(item.Mark); cell7.CellStyle = style;
                    //ICell cell8 = row.CreateCell(8);
                    //cell8.SetCellValue(item.ProStage); cell8.CellStyle = style;
                    //ICell cell9 = row.CreateCell(9);
                    //cell9.SetCellValue(item.CopStage); cell9.CellStyle = style;
                    //ICell cell10 = row.CreateCell(10);
                    //cell10.SetCellValue(item.Mark); cell10.CellStyle = style;

                    index++;
                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(year + "年" + this.drp_unit.SelectedItem.Text.Trim() + "产值汇总表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
    }
}