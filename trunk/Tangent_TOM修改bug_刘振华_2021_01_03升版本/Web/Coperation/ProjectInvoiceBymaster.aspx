﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="ProjectInvoiceBymaster.aspx.cs" Inherits="TG.Web.Coperation.ProjectInvoiceBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="/css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="/css/Commjqgrid.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="/js/grid.locale-cn.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="/js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery.tablesort.js"></script>
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    <script type="text/javascript" src="/js/Common/CommonControl.js"></script>
    <script type="text/javascript" src="../js/Global.js"></script>
    <script type="text/javascript" src="../js/Common/SendMessageCommon.js"></script>
    <script type="text/javascript" src="../js/Coperation/ProjectInvoice_jq.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>合同开票管理</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>收费管理 </a><i class="fa fa-angle-right"></i><a>合同开票管理</a> </li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>查询合同开票
                    </div>
                    <div class="actions">
                        <asp:Button Text="导出" runat="server" CssClass="btn red btn-sm" ID="btn_export" OnClick="btn_export_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive" id="tbl_id">
                        <tr>
                            <td>生产部门:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_unit" Width="150px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>年份:
                            </td>
                            <td>
                                <asp:DropDownList ID="drp_year" Width="100px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">--全部--</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                           
                            <td>合同名称:
                            </td>
                            <td>
                                <input type="text" class="form-control input-sm" id="txt_keyname" runat="server" />
                            </td>
                            <td>开票时间:<input type="text" name="txt_date" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" /></td>
                            <td>截止时间:<input type="text" name="txt_date" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" /></td>
                            <td>
                                <input type="button" class="btn blue" value="查询" id="btn_search" /></td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同开票列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                                没有符合条件数据！
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 首页参数和是否生产经营部 -->
    <input type="hidden" id="HiddenParamType" value="<%=ParamType %>" />
    <input type="hidden" id="HiddenisFlag" value="<%=isFlag%>" />
    <!--权限绑定-->

    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <asp:HiddenField ID="hid_wheredate" runat="server" Value="" />
    <!-- 是否已经收完款 -->
    <asp:HiddenField ID="hid_iscomplete" runat="server" Value="0" />
    <!-- 当前用户 -->
    <asp:HiddenField ID="hid_curuser" runat="server" Value='0' />
    <!-- 修改收款，收款ID -->
    <input type="hidden" id="hid_chgidThis" value="0" />
    <input type="hidden" id="hid_chgidNow" value="0" />
    <input type="hidden" id="hid_cprid" value="0" />
    <!-- 修改前的付款额度 -->
    <input type="hidden" id="hid_tempnopay" value="0" />
    <!--弹出层-->
    <!--查看-->
    <div id="showInvoioce" class="modal
    fade yellow"  tabindex="-1" data-width="1100" aria-hidden="true" style="display: none; width: 1100px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">开票列表详细
            </h4>
        </div>
        <div class="modal-body">
            <div id="seeChargeDialogDiv" style="color: #222222;">
                合同名称：<span id="span_cprName"></span>
                <table id="tb_InvoiceList" class="table table-striped  table-bordered table-advance table-hover">
                    <tr class="trBackColor">
                        <th style="width: 40px;" align="center">序号
                        </th>                        
                        <th style="width: 110px;" align="center">开票金额(万元)
                        </th>
                        <th style="width: 100px;" align="center">开票单位全称
                        </th>
                        <th style="width: 110px;" align="center">票据类型
                        </th>
                        <th style="width: 70px;" align="center">开票日期
                        </th>
                        <th style="width: 90px;" align="center">增值税税号
                        </th>  
                        <th style="width: 90px;" align="center">国税注册地
                        </th> 
                        <th style="width: 110px;" align="center">国税注册地电话
                        </th> 
                        <th style="width: 70px;" align="center">开户银行
                        </th> 
                        <th style="width: 70px;" align="center">账号信息
                        </th>                       
                        <th style="width: 60px;" align="center">申请人
                        </th>  
                        <th style="width: 80px;" align="center">备注
                        </th>                       
                         <th style="width:100px;" align="center">状态
                        </th>

                    </tr>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!-- 添加开票 -->
  <div id="addCharge" class="modal
    fade yellow"
        tabindex="-1" data-width="1100" aria-hidden="true" style="display: none; width: 1100px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">开票列表详细
            </h4>
        </div>
        <div class="modal-body">
            <div id="addChargeDialogDiv" style="color: #222222;">
                <table id="addChargeListTable" style="text-align: center;" class="table table-striped table-bordered table-advance table-hover">
                    <tr class="trBackColor">
                       <th style="width: 40px;" align="center">序号
                        </th>                        
                         <th style="width: 110px;" align="center">开票金额(万元)
                        </th>
                        <th style="width: 100px;" align="center">开票单位全称
                        </th>
                        <th style="width:110px;" align="center">票据类型
                        </th>
                        <th style="width: 70px;" align="center">开票日期
                        </th>
                        <th style="width: 90px;" align="center">增值税税号
                        </th>  
                        <th style="width: 90px;" align="center">国税注册地
                        </th> 
                        <th style="width: 110px;" align="center">国税注册地电话
                        </th> 
                        <th style="width: 70px;" align="center">开户银行
                        <th style="width: 70px;" align="center">账号信息
                        </th>                       
                        <th style="width: 60px;" align="center">申请人
                        </th>             
                         <th style="width: 100px;" align="center">状态
                        </th>
                        <th style="width: 80px;" align="center">操作
                        </th>
                    </tr>
                </table>
                <br />
             
                <table class="table table-bordered">
                    <tr>
                        <td>合同名称:
                        </td>
                        <td>
                            <label id="lbl_cprname">
                            </label>                           
                        </td>
                        <td>合同号:
                        </td>
                        <td>
                            <label id="lbl_number">
                            </label>
                          
                        </td>
                        <td>项目名称:
                        </td>
                        <td>
                            <label id="lbl_proname">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>合同总额:
                        </td>
                        <td>
                            <label id="lbl_allcount">
                            </label>
                            (万元)                           
                        </td>
                        <td>开票金额:
                        </td>
                        <td>
                            <input type="text" id="txt_payCount" maxlength="15" class="form-control input-sm"
                                placeholder="万元" style="border: 1px solid #ccc;" />
                            <span style="color: red; display: none;" id="txt_paycount_valide"></span>
                        </td>
                        <td>票据类型:
                        </td>
                        <td>
                            <select id="drop_type" class="form-control droplist_BackColor" style="width:200px;">                                        
                                        <option value="增值税普通发票">增值税普通发票</option>
                                        <option Value="增值税专用发票">增值税专用发票</option>
                                    </select>
                        </td>
                    </tr>
                    <tr>
                        <td>开票单位全称:
                        </td>
                        <td>
                            <input type="text" id="txt_invoicename" class="form-control input-sm"  style="border: 1px solid #ccc;"/>
                            <span style="color: red; display: none;" id="txt_invoicename_valide">请输入开票单位全称!</span>
                        </td>                        
                        <td>开票日期:
                        </td>
                        <td>
                            <input id="txt_times" type="text" class="Wdate"
                                onclick="WdatePicker({ readOnly: true })" style="height: 22px; border: 1px solid #ccc;" />
                            <span style="color: red; display: none;" id="txt_time_valide">请选择开票日期!</span>
                        </td>
                        <td>备注:
                        </td>
                        <td >
                            <textarea rows="3" cols="30" id="txt_chargeRemark" class="form-control" style="max-height: 100px; min-height: 10px;border: 1px solid #ccc;"></textarea>
                            <span style="color: red; display: none;" id="txt_remark_valide">请输入备注!</span>
                        </td>
                    </tr>
                </table>
              
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn red btn-default" id="btn_addcount">
                收款</button>
            <button type="button" onclick="AddOrEditCharge('add')" style="display: none;" class="btn
    btn-default"
                id="btn_returnCharge">
                返回收费</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>

    <%--<div id="AuditUser" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body">
            <span id="span_proName"></span>
            <div id="auditShow">
            </div>
            <table class="table">
                <tr>
                    <td>备注:</td>
                    <td>
                        <input type="text" id="txt_ApplyText" class="form-control" /></td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>--%>
    
</asp:Content>
