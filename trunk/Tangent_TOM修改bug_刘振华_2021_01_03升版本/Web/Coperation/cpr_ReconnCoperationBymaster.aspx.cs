﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Geekees.Common.Controls;
using System.Xml;
using TG.Model;
using TG.BLL;

namespace TG.Web.Coperation
{
    public partial class cpr_ReconnCoperationBymaster : PageBase
    {
        //建筑结构
        //public string asTreeviewStructObjID
        //{
        //    get
        //    {
        //        return this.asTreeviewStruct.GetClientTreeObjectId();
        //    }
        //}
        //建筑分类
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructType.GetClientTreeObjectId();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //绑定合同类别
                BindCorpType();
                //工程地点
                //BindCorpAddr();
                //行业性质
                BindCorpHyxz();
                //工程来源
                BindCorpSrc();
                //合同阶段
                //BindCorpProc();
                //设置样式
                SetDropDownTreeThem();
                //绑定建筑结构样式
                //BindStructType();
                //绑定建筑分类
                BindBuildStuctType();
                //绑定设计等级
                BindBuildType();
                //结构特征
                //BindSpec();
                SetUserSysNoAndRole();
                BindPreviewPower();
                //收费类型
                BindCorChange();
                //new勘察等级
                //建筑类型
                //合同阶段
                BindNewProc();

            }
            else
            {
                Save();
            }
        }
        //阶段
        protected void BindNewProc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_kcjd'";
            string str_where1 = " dic_Type='cpr_kcdj'";
            string str_where2 = " dic_Type='cpr_kcbuildtype'";
            this.chk_cprjd.DataSource = bll_dic.GetList(str_where);
            this.chk_cprjd.DataTextField = "dic_Name";
            this.chk_cprjd.DataValueField = "ID";
            this.chk_cprjd.DataBind();
            this.ddcpr_SurveyClass.DataSource = bll_dic.GetList(str_where1);
            this.ddcpr_SurveyClass.DataTextField = "dic_Name";
            this.ddcpr_SurveyClass.DataValueField = "ID";
            this.ddcpr_SurveyClass.DataBind();
            this.ddcpr_BuildType.DataSource = bll_dic.GetList(str_where2);
            this.ddcpr_BuildType.DataTextField = "dic_Name";
            this.ddcpr_BuildType.DataValueField = "ID";
            this.ddcpr_BuildType.DataBind();
        }
        protected void Save()
        {
            //添加合同
            int i_cprid = AddCoperation();
            if (i_cprid > 0)
            {
                //添加计划收费信息
                AddChargeItem(i_cprid.ToString());
                //添加合同子项
                AddSubCoperation(i_cprid.ToString());
                //添加附件
                AddCoperationAttach(i_cprid.ToString());
                //弹出并返回到合同列表
                TG.Common.MessageBox.ShowAndRedirect(this, "合同添加成功！", "cpr_CorperationListBymaster.aspx");
            }
        }
        //添加合同信息
        public int AddCoperation()
        {
            TG.BLL.cm_ReconnCoperation bll_cpr = new TG.BLL.cm_ReconnCoperation();
            TG.BLL.cm_Coperation bll_cop = new TG.BLL.cm_Coperation();
            TG.Model.cm_ReconnCoperation model_cpr = new TG.Model.cm_ReconnCoperation();
            TG.Model.cm_Coperation model_cop = new TG.Model.cm_Coperation();
            //赋值客户id
            model_cpr.cst_Id = int.Parse(this.hid_cstid.Value);
            model_cop.cst_Id = int.Parse(this.hid_cstid.Value);
            //合同编号
            model_cpr.cpr_No = this.txtcpr_No.Value;
            model_cop.cpr_No = this.txtcpr_No.Value;
            if (this.hid_cprno.Value != "")
            {
                model_cpr.cpr_No = this.hid_cprno.Value;
                model_cop.cpr_No = this.hid_cprno.Value;
            }
            //合同分类
            model_cpr.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            model_cop.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            //合同类型
            model_cpr.cpr_Type2 = this.txt_cprType.InnerText;
            model_cop.cpr_Type2 = this.txt_cprType.InnerText;
            //合同名称
            model_cpr.cpr_Name = this.txt_cprName.Value;
            model_cop.cpr_Name = this.txt_cprName.Value;
            //勘察等级
            model_cpr.SurveyClass = this.ddcpr_SurveyClass.SelectedItem.Text;
            //建设单位
            model_cpr.BuildUnit = this.txt_cprBuildUnit.Value;
            model_cop.BuildUnit = this.txt_cprBuildUnit.Value;
            //占地面积
            model_cpr.BuildArea = Convert.ToDecimal(this.txt_BuildArea.Value);
            //单位为亩
            model_cop.BuildArea = "0";
            //建筑类型
            model_cpr.BuildType = this.ddcpr_BuildType.SelectedItem.Text.Trim();
            model_cop.BuildType = this.ddcpr_BuildType.SelectedItem.Text.Trim();
            //地貌单元
            model_cpr.EarthUnit = this.txt_EarthUnit.Value;
            //建筑层数
            model_cpr.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            model_cop.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            //工程负责人电话
            model_cpr.ChgPeople = this.txt_proFuze.Value;
            model_cop.ChgPeople = this.txt_proFuze.Value;
            model_cpr.ChgPhone = this.txt_fzphone.Value;
            model_cop.ChgPhone = this.txt_fzphone.Value;
            //承接部门
            model_cpr.cpr_Unit = this.txt_cjbm.Value;
            model_cop.cpr_Unit = this.txt_cjbm.Value;
            if (this.txt_cjbm.Value.Trim() == "")
            {
                model_cpr.cpr_Unit = this.hid_cjbm.Value;
                model_cop.cpr_Unit = this.hid_cjbm.Value;
            }
            //甲方负责人电话
            model_cpr.ChgJia = this.txtFParty.Value;
            model_cop.ChgJia = this.txtFParty.Value;
            model_cpr.ChgJiaPhone = this.txt_jiafphone.Value;
            model_cop.ChgJiaPhone = this.txt_jiafphone.Value;
            //工程地点
            if (this.txt_ProjectPosition.Value.Trim() != "")
            {
                model_cpr.BuildPosition = this.txt_ProjectPosition.Value;
                model_cop.BuildPosition = this.txt_ProjectPosition.Value;
            }
            else
            {
                model_cpr.BuildPosition = "";
                model_cop.BuildPosition = "";
            }
            //合同额
            if (this.txtcpr_Account.Value.Trim() == "")
            {
                model_cpr.cpr_Acount = 0;
                model_cop.cpr_Acount = 0;
            }
            else
            {
                model_cpr.cpr_Acount = Convert.ToDecimal(this.txtcpr_Account.Value);
                model_cop.cpr_Acount = Convert.ToDecimal(this.txtcpr_Account.Value);
            }
            //实际合同额
            if (this.txtcpr_Account0.Value.Trim() == "")
            {
                model_cpr.cpr_ShijiAcounnt = 0;
                model_cop.cpr_ShijiAcount = 0;
            }
            else
            {
                model_cpr.cpr_ShijiAcounnt = Convert.ToDecimal(this.txtcpr_Account0.Value);
                model_cop.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);
            }
            //行业性质
            if (this.ddProfessionType.SelectedIndex != 0)
            {
                model_cpr.Industry = this.ddProfessionType.SelectedItem.Text;
                model_cop.Industry = this.ddProfessionType.SelectedItem.Text;
            }
            else
            {
                model_cpr.Industry = this.ddProfessionType.Items[0].Value;
                model_cop.Industry = this.ddProfessionType.Items[0].Value;
            }
            //钻孔数量
            if (this.txt_HoleNumber.Value.Trim() == "")
            {
                model_cpr.HoleNumber = 0;
            }
            else
            {
                model_cpr.HoleNumber = Convert.ToInt32(this.txt_HoleNumber.Value.Trim());
            }
            //钻孔深度
            if (this.txt_HoleHeight.Value.Trim() == "")
            {
                model_cpr.HoleHeight = 0;
            }
            else
            {
                model_cpr.HoleHeight = Convert.ToDecimal(this.txt_HoleHeight.Value.Trim());
            }
            //建筑物数量
            if (this.txt_BuildNumber.Value.Trim() == "")
            {
                model_cpr.BuildNumber = 0;
            }
            else
            {
                model_cpr.BuildNumber = Convert.ToInt32(this.txt_BuildNumber.Value.Trim());
            }
            //探井数量
            if (this.txt_WellsNumber.Value.Trim() == "")
            {
                model_cpr.WellsNumber = 0;
            }
            else
            {
                model_cpr.WellsNumber = Convert.ToInt32(this.txt_WellsNumber.Value.Trim());
            }
            //探井深度
            if (this.txt_WellsHeight.Value.Trim() == "")
            {
                model_cpr.WellsHeight = 0;
            }
            else
            {
                model_cpr.WellsHeight = Convert.ToDecimal(this.txt_WellsHeight.Value.Trim());
            }
            //总进尺
            if (this.txt_TotalLength.Value.Trim() == "")
            {
                model_cpr.TotalLength = 0;
            }
            else
            {
                model_cpr.TotalLength = Convert.ToDecimal(this.txt_TotalLength.Value.Trim());
            }
            //添加
            int affectRow = 0;
            //结构样式
            //if (this.txtInvestAccount.Value.Trim() == "")
            //{
            //    model_cpr.cpr_Touzi = 0;
            //}
            //else
            //{
            //    model_cpr.cpr_Touzi = Convert.ToDecimal(this.txtInvestAccount.Value);
            //}
            //if (txtInvestAccount0.Value.Trim() == "")
            //{
            //    model_cpr.cpr_ShijiTouzi = 0;
            //}
            //else
            //{
            //    model_cpr.cpr_ShijiTouzi = Convert.ToDecimal(txtInvestAccount0.Value);
            //}
            //newAdd

            //合同阶段
            string str_process = "";
            foreach (ListItem chk in this.chk_cprjd.Items)
            {
                if (chk.Selected)
                {
                    str_process += chk.Value + ",";
                }
            }
            str_process = str_process.IndexOf(",") > -1 ? str_process.Remove(str_process.Length - 1) : "";
            model_cpr.cpr_Process = str_process;
            model_cop.cpr_Process = str_process;
            //工程来源
            if (this.ddSourceWay.SelectedIndex != 0)
            {
                model_cpr.BuildSrc = this.ddSourceWay.SelectedItem.Text;
                model_cop.BuildSrc = this.ddSourceWay.SelectedItem.Text;
            }
            else
            {
                model_cpr.BuildSrc = this.ddSourceWay.Items[0].Value;
                model_cop.BuildSrc = this.ddSourceWay.Items[0].Value;
            }
                      //合同统计年份
            model_cpr.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            model_cop.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
  //合同签订日期
            model_cpr.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value );
            model_cop.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value );
            //合同开工日期
            model_cpr.cpr_StartDate = Convert.ToDateTime(this.txtStartDate.Value);
            //合同完成日期
            model_cpr.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            model_cop.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            //制表人
            model_cpr.TableMaker = this.txt_tbcreate.Value;
            model_cop.TableMaker = this.txt_tbcreate.Value;
            //工期====
            //勘探点数量
            if (this.txt_PointNumber.Value.Trim() == "")
            {
                model_cpr.PointNumber = 0;
            }
            else
            {
                model_cpr.PointNumber = Convert.ToInt32(this.txt_PointNumber.Value.Trim());
            }
            //波速实验钻孔数量
            if (this.txt_WaveNumber.Value.Trim() == "")
            {
                model_cpr.WaveNumber = 0;
            }
            else
            {
                model_cpr.WaveNumber = Convert.ToInt32(this.txt_WaveNumber.Value.Trim());
            }
            //控制性钻孔数量
            if (this.txt_ControlNumber.Value.Trim() == "")
            {
                model_cpr.ControlNumber = 0;
            }
            else
            {
                model_cpr.ControlNumber = Convert.ToInt32(this.txt_ControlNumber.Value.Trim());
            }
            //一般性钻孔数量
            if (this.txt_GenerNumber.Value.Trim() == "")
            {
                model_cpr.GenerNumber = 0;
            }
            else
            {
                model_cpr.GenerNumber = Convert.ToInt32(this.txt_GenerNumber.Value.Trim());
            }
            //波速试验钻孔深度、
            if (this.txt_WaveHeight.Value.Trim() == "")
            {
                model_cpr.WaveHeight = 0;
            }
            else
            {
                model_cpr.WaveHeight = Convert.ToDecimal(this.txt_WaveHeight.Value.Trim());
            }
            //控制性钻孔深度
            if (this.txt_ControlHeight.Value.Trim() == "")
            {
                model_cpr.ControlHeight = 0;
            }
            else
            {
                model_cpr.ControlHeight = Convert.ToDecimal(this.txt_ControlHeight.Value.Trim());
            }
            //一般性钻孔深度
            if (this.txt_GenerHeight.Value.Trim() == "")
            {
                model_cpr.GenerHeight = 0;
            }
            else
            {
                model_cpr.GenerHeight = Convert.ToDecimal(this.txt_GenerHeight.Value.Trim());
            }
            //工程规模
            if (this.txt_ProjectArea.Value.Trim() == "")
            {
                model_cpr.ProjectArea = 0;
            }
            else
            {
                model_cpr.ProjectArea = Convert.ToDecimal(this.txt_ProjectArea.Value.Trim());
            }
            //工程特征
            model_cpr.Features = this.txt_Features.Value.Trim();
            //工期
            if (this.txt_ProjectDate.Value.Trim() == "")
            {
                model_cpr.ProjectDate = 0;
            }
            else
            {
                model_cpr.ProjectDate = Convert.ToInt32(this.txt_ProjectDate.Value.Trim());
            }
            //多吨楼备注--拟采用基础选型及地基处理方案
            model_cpr.MultiBuild = this.txt_MultiBuild.Value;
            model_cop.MultiBuild = this.txt_MultiBuild.Value;
            //合同备注
            model_cpr.cpr_Mark = this.txtcpr_Remark.Value;
            model_cop.cpr_Mark = this.txtcpr_Remark.Value;
            //model_cpr.BuildArea = this.txt_buildArea.Value;

            model_cpr.RegTime = DateTime.Now;
            model_cpr.UpdateBy = UserSysNo.ToString();
            model_cpr.LastUpdate = DateTime.Now;

            model_cop.RegTime = DateTime.Now;
            model_cop.UpdateBy = UserSysNo.ToString();
            model_cop.LastUpdate = DateTime.Now;
            //结构形式
            //update 20130530 qpl
            //model_cpr.StructType = GetDropDownTreeCheckedValue(this.asTreeviewStruct.RootNode.ChildNodes);
            //建筑分类
            //update 20130530 qpl
            model_cpr.BuildStructType = GetDropDownTreeCheckedValue(this.asTreeviewStructType.RootNode.ChildNodes);
            model_cop.BuildStructType = GetDropDownTreeCheckedValue(this.asTreeviewStructType.RootNode.ChildNodes);

            //专业特征

            //录入人
            model_cpr.InsertUserID = UserSysNo;
            model_cop.InsertUserID = UserSysNo;
            //项目经理人ID  qpl  20131225
            model_cpr.PMUserID = int.Parse(this.HiddenPMUserID.Value);
            model_cop.PMUserID = int.Parse(this.HiddenPMUserID.Value);
            model_cpr.InsertDate = DateTime.Now;
            //model_cop.InsertDate = DateTime.Now;
            //保存合同信息
            try
            {
                int coperID = bll_cop.Add(model_cop);
                model_cpr.cpr_FID = coperID;
                affectRow = bll_cpr.Add(model_cpr);
            }
            catch (System.Exception ex)
            { }

            return affectRow;
        }
        //更新收费信息
        protected void AddChargeItem(string cprid)
        {
            //更新计划收费信息  qpl  20140114
            string strSql = " UPDATE [cm_CoperationChargeType ] SET [cpr_Id]=" + cprid + " WHERE [cpr_Id]=" + this.hid_cprid.Value + " AND acceptuser='" + UserShortName + "' AND paytype='reconn'";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            bll_db.ExcuteBySql(strSql);

        }
        //添加子项
        protected void AddSubCoperation(string cprid)
        {
            //更新计划收费信息  qpl  20140114
            string strSql = " UPDATE [cm_SubCoperation] SET [cpr_Id] = " + cprid + ",[cpr_No]=" + cprid + "  WHERE [cpr_No] ='" + this.hid_cprid.Value + "' AND UpdateBy='" + UserShortName + "'";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            bll_db.ExcuteBySql(strSql);


        }
        //添加附件
        protected void AddCoperationAttach(string cprid)
        {
            //更新计划收费信息  qpl  20140114
            string strSql = " UPDATE [cm_AttachInfo] SET [Cpr_Id] =" + cprid + " WHERE [Temp_No] = '" + this.hid_cprid.Value + "' AND UploadUser='" + UserSysNo + "' AND OwnType='reconn'";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            bll_db.ExcuteBySql(strSql);



        }
        /// <summary>
        /// 获取客户ID
        /// </summary>
        /// <returns></returns>
        public string getCst_Id()
        {
            DateTime dt = DateTime.Now;
            string tempid = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString();
            return tempid;
        }
        //返回一个文件上传的随机ID
        public string GetCoperationID()
        {
            DateTime dt = DateTime.Now;
            string tempid = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString();
            return tempid;
        }
        /// <summary>
        /// 获取登录ID(登录界面开始进行重新设置.....)
        /// </summary>
        /// <returns></returns>
        public string GetUserFlag()
        {
            return UserSysNo.ToString();
        }
        /// <summary>
        /// 返回文件夹ID (若是根据登录ID,此项可不要？待商讨中.......)
        /// </summary>
        /// <returns></returns>
        public string GetParentID()
        {
            return UserShortName;
        }
        //设置选择合同额方法
        public void SetUserSysNoAndRole()
        {
            //用户ID
            this.ChooseCustomer1.UserSysNo = int.Parse(GetCurMemID());
            //权限
            this.ChooseCustomer1.PreviewPower = GetPreviewPower();
        }
        //获取页面权限  by long 20130510
        public int GetPreviewPower()
        {
            int UserSysNo = int.Parse(GetCurMemID());
            string PageName = "cpr_CorperationList.aspx";
            List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, PageName);

            //浏览权限
            int Power = 0;
            if ((from role in rolePowerParameterList where role.PreviewPattern == "2" select role).Count() > 0)
            {
                //部门
                Power = 2;
            }
            if ((from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0)
            {
                //全部
                Power = 1;
            }
            return Power;
        }

        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.ddcpr_Type.DataSource = bll_dic.GetList(str_where);
            this.ddcpr_Type.DataTextField = "dic_Name";
            this.ddcpr_Type.DataValueField = "ID";
            this.ddcpr_Type.DataBind();

        }
        //合同建筑类别
        //protected void BindBuildType()
        //{
        //    TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
        //    string str_where = " dic_Type='cpr_buildtype'";
        //    this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
        //    this.drp_buildtype.DataTextField = "dic_Name";
        //    this.drp_buildtype.DataValueField = "ID";
        //    this.drp_buildtype.DataBind();
        //}
        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz'";
            this.ddProfessionType.DataSource = bll_dic.GetList(str_where);
            this.ddProfessionType.DataTextField = "dic_Name";
            this.ddProfessionType.DataValueField = "ID";
            this.ddProfessionType.DataBind();
        }
        //收费类型
        protected void BindCorChange()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            //string str_where = " dic_Type='cpr_charge'";
            //this.chargeTypeSelect.DataSource = bll_dic.GetList(str_where);
            //this.chargeTypeSelect.DataTextField = "dic_Name";
            //this.chargeTypeSelect.DataValueField = "ID";
            //this.chargeTypeSelect.DataBind();
        }
        //建筑分类
        protected void BindBuildStuctType()
        {
            BindProInfoConfig("BuildType", this.asTreeviewStructType.RootNode);
            this.asTreeviewStructType.CollapseAll();
            // 子项建筑分类 qpl  20131225
            //BindProInfoConfig("BuildType", this.asTreeviewStructType_sub.RootNode);
            //this.asTreeviewStructType_sub.CollapseAll();
        }
        //结构形式
        //protected void BindStructType()
        //{
        //    BindProInfoConfig("StructType", this.asTreeviewStruct.RootNode);
        //    this.asTreeviewStruct.CollapseAll();
        //    //子项结构样式  qpl  20131225
        //    //BindProInfoConfig("StructType", this.asTreeviewStruct_sub.RootNode);
        //    //this.asTreeviewStruct_sub.CollapseAll();
        //}
        //设计等级  
        protected void BindBuildType()
        {
            //TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            //string str_where = " dic_Type='cpr_buildtype'";
            //this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            //this.drp_buildtype.DataTextField = "dic_Name";
            //this.drp_buildtype.DataValueField = "ID";
            //this.drp_buildtype.DataBind();
        }
        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }
        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            //this.asTreeviewStruct.Theme = macOS;
            this.asTreeviewStructType.Theme = macOS;
            //qpl 20131225
            // this.asTreeViewBuildType.Theme = macOS;
            //qpl 20131225
            //this.asTreeviewStructType_sub.Theme = macOS;
            //this.asTreeviewStruct_sub.Theme = macOS;
        }
        /// <summary>
        /// 绑定树列表  qpl 20140108 
        /// </summary>
        /// <param name="infokey"></param>
        /// <param name="rootnode"></param>
        /// <param name="flagrootbox">根节点是否显示CheckBox</param>
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode, bool showsubbox)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root, showsubbox);
            }
        }
        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        /// <summary>
        /// 绑定树列表  qpl 20140108 
        /// </summary>
        /// <param name="xmlroot"></param>
        /// <param name="root"></param>
        /// <param name="flagrootbox">根节点是否显示CheckBox</param>
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root, bool showsubbox)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                //标示子节点是否显示CheckBox qpl 20140108
                switch (name)
                {
                    case "特级":
                    case "一级":
                    case "二级":
                    case "三级":
                        linknode.EnableCheckbox = !showsubbox;
                        break;
                    default:
                        linknode.EnableCheckbox = showsubbox;
                        break;
                }

                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);

                        //子节点的checkbox 不可用  qpl 20140108
                        sublinknode.EnableCheckbox = showsubbox;

                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode, showsubbox);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        //来源
        protected void BindCorpSrc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_src'";
            this.ddSourceWay.DataSource = bll_dic.GetList(str_where);
            this.ddSourceWay.DataTextField = "dic_Name";
            this.ddSourceWay.DataValueField = "ID";
            this.ddSourceWay.DataBind();
        }
        //阶段
        protected void BindCorpProc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_jd'";
            this.chk_cprjd.DataSource = bll_dic.GetList(str_where);
            this.chk_cprjd.DataTextField = "dic_Name";
            this.chk_cprjd.DataValueField = "ID";
            this.chk_cprjd.DataBind();
        }
        // update by 20130530 qpl
        //获取选中树所有节点值并返回值
        protected string GetDropDownTreeCheckedValue(List<ASTreeViewNode> allnodes)
        {
            //最终生成字符串
            string rootvalue = "";
            foreach (ASTreeViewNode node in allnodes)
            {
                string secondvalue = "";
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                    GetSecondNodeList(node, ref secondvalue);
                }
                rootvalue += secondvalue;
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }

            return rootvalue;
        }
        //获取等级选中节点 qpl 20140108
        protected string GetDropDownTreeCheckedValue(List<ASTreeViewNode> allnodes, bool flag)
        {
            //最终生成字符串
            string rootvalue = "";
            foreach (ASTreeViewNode node in allnodes)
            {
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                }
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }

            return rootvalue;
        }
        //获取第二级的节点拼接
        protected void GetSecondNodeList(ASTreeViewNode node, ref string value)
        {
            if (node.ChildNodes.Count > 0)
            {
                //返回值
                StringBuilder sbresult = new StringBuilder();

                foreach (ASTreeViewNode snode in node.ChildNodes)
                {
                    if (snode.CheckedState == ASTreeViewCheckboxState.Checked || snode.CheckedState == ASTreeViewCheckboxState.HalfChecked)
                    {
                        //拼接第二级
                        value = "^" + snode.NodeValue;
                        string subvalue = "";
                        subvalue = value;
                        GetChildNodes(snode, ref subvalue);
                        foreach (string key in sblist)
                        {
                            sbresult.Append(key);
                        }
                        //清空当前列表
                        sblist.Clear();
                    }
                }
                value = sbresult.ToString();
            }
        }
        //查询数据
        List<string> sblist = new List<string>();
        protected void GetChildNodes(ASTreeViewNode node, ref string value)
        {
            StringBuilder sb = new StringBuilder();
            if (node.ChildNodes.Count > 0)
            {
                foreach (ASTreeViewNode childnode in node.ChildNodes)
                {
                    if ((childnode.CheckedState == ASTreeViewCheckboxState.Checked) || (childnode.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                    {
                        string tempvalue = value + "*" + childnode.NodeValue;

                        if (childnode.ChildNodes.Count > 0)
                        {
                            //如果还有子节点，继续遍历
                            GetChildNodes(childnode, ref tempvalue);
                        }
                        else
                        {
                            //添加末节点
                            sb.Append(tempvalue);
                        }
                    }
                }
            }
            else
            {
                //添加末节点
                sb.Append(value);
            }
            //赋值value
            sblist.Add(sb.ToString());
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;
                ChooseCustomer1.UserSysNo = base.UserSysNo;
                ChooseCustomer1.PreviewPower = base.RolePowerParameterEntity.PreviewPattern;

                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }
    }
}