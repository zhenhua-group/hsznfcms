﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_SearchCoperationBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_SearchCoperationBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>

    <script src="../js/Common/AutoComplete.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/Coperation/cpr_SearchCpr_jq.js"></script>
     <style type="text/css">
        .defaultDropdownIconTD
        {
            background-color: #FFF;
        }
        .defaultDropdownTextContainer
        {
            background-color: #FFF;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>合同高级查询</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>合同信息管理</a><i class="fa fa-angle-right"> </i><a>合同高级查询</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同高级查询
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="jffzr_dataTable" class="table table-bordered" align="center">
                                    <tr>
                                        <td style="width: 10%">合同编号:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" placeholder="" id="txtcpr_No" />
                                        </td>
                                        <td style="width: 10%">建设单位:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" placeholder="" id="txt_cprBuildUnit" />
                                        </td>
                                        <td style="width: 10%">工程地点:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" placeholder="" id="drp_position" />
                                        </td>
                                        <td style="width: 10%">合同名称:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" placeholder="" id="txt_keyname" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同分类:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drp_type" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm">
                                                <asp:ListItem Value="0">------请选择------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>承接部门:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drp_unit" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm">
                                                <asp:ListItem Value="0">------请选择------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>行业性质:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drp_xz" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm">
                                                <asp:ListItem Value="0">------请选择------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>工程来源:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drp_src" runat="server" AppendDataBoundItems="True" CssClass="form-control input-sm">
                                                <asp:ListItem Value="0">------请选择------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>甲方负责人:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" placeholder="" id="txtFParty" />
                                        </td>
                                        <td>工程负责人:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" placeholder="" id="txt_fzr" />
                                        </td>
                                        <td>按照客户:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-sm" placeholder="" id="txt_cst" />
                                        </td>
                                        <td>建筑类别:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drp_buildtype" runat="server" AppendDataBoundItems="True" CssClass="form-control">
                                                <asp:ListItem Value="0">------请选择------</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>合同签订日期:
                                        </td>
                                        <td colspan="3">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input type="text" name="txt_date" id="txt_signdate" onclick="WdatePicker({ readOnly: true })"
                                                        class="Wdate" runat="Server" style="width: 100%; height: 30px; border: 1px solid #e5e5e5" />
                                                </div>
                                                <div class="col-md-1">
                                                    至
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" name="txt_date" id="txt_signdate2" onclick="WdatePicker({ readOnly: true })"
                                                        class="Wdate" runat="Server" style="width: 100%; height: 30px; border: 1px solid #e5e5e5" />

                                                </div>
                                            </div>
                                        </td>
                                        <td>合同额:
                                        </td>
                                        <td colspan="3">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control input-sm" id="txtcpr_Account" />
                                                </div>
                                                <div class="col-md-1">
                                                    至
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control input-sm" id="txtcpr_Account2" />
                                                </div>
                                                <div class="col-md-2" style="padding-top: 3px;">
                                                    万元
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>建筑规模:
                                        </td>
                                        <td colspan="3">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control input-sm" id="txt_buildArea" />
                                                </div>
                                                <div class="col-md-1">
                                                    至
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control input-sm" id="txt_buildArea2" />
                                                </div>
                                                <div class="col-md-2" style="padding-top: 3px;">
                                                    平米
                                                </div>
                                            </div>
                                        </td>
                                        <td>合同阶段:
                                        </td>
                                        <td colspan="3">
                                            <div class="col-md-10">
                                                <asp:CheckBoxList ID="chk_cprjd" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                </asp:CheckBoxList>

                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" align="center">
                                            <input type="button" class="btn blue" id="btn_search" value="查询" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>高级查询列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                                没有符合条件数据！
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
</asp:Content>
