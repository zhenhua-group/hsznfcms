﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using System.Xml;
using AjaxPro;
using System.Text;
using TG.Model;
using TG.BLL;
namespace TG.Web.Coperation
{
    public partial class cpr_SuperCoperationEditChooseBymaster : PageBase
    {
        public string asTreeviewStructObjID
        {
            get
            {
                return this.asTreeviewStructEdit.GetClientTreeObjectId();
            }
        }
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructTypeEdit.GetClientTreeObjectId();
            }
        }

        public int CoperationSysno
        {
            get
            {
                int coperationSysNo = 0;
                int.TryParse(Request["coperationSysNo"], out coperationSysNo);
                return coperationSysNo;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(cpr_SuperCoperationEditChooseBymaster));
            //获取权限
            SetUserSysNoAndRole();
            //绑定权限
            BindPreviewPower();
            //合同类别绑定
            BindCorpType();
            //行业性质
            BindCorpHyxz();
            //工程来源
            BindCorpSrc();
            //合同阶段
            BindCorpProc();
            //设置样式
            SetDropDownTreeThem();
            //绑定建筑结构样式修改时
            BindStructTypeEdit();
            //绑定建筑分类修改时
            BindBuildStuctTypeEdit();
            //合同建筑类别             
            BindBuildType();
            ShowCoperation();
        }
        //设置选择合同额方法
        public void SetUserSysNoAndRole()
        {
            //用户ID
            this.ChooseCustomer1.UserSysNo = int.Parse(GetCurMemID());
            //权限
            this.ChooseCustomer1.PreviewPower = GetPreviewPower();
        }
        //合同建筑类别
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
            this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "ID";
            this.drp_buildtype.DataBind();
        }
        //获取页面权限
        public int GetPreviewPower()
        {
            int UserSysNo = int.Parse(GetCurMemID());
            string PageName = "cpr_CorperationList.aspx";
            List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, PageName);

            //浏览权限
            int Power = 0;
            if ((from role in rolePowerParameterList where role.PreviewPattern == "2" select role).Count() > 0)
            {
                //部门
                Power = 2;
            }
            if ((from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0)
            {
                //全部
                Power = 1;
            }
            return Power;
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;
                //string NotShowUnitList = base.NotShowUnitList;
                ChooseCustomer1.UserSysNo = base.UserSysNo;
                ChooseCustomer1.PreviewPower = base.RolePowerParameterEntity.PreviewPattern;
                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
                //this.NotShowUnitList.Value = NotShowUnitList;
            }
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.txt_ddcpr_TypeEdit.DataSource = bll_dic.GetList(str_where);
            this.txt_ddcpr_TypeEdit.DataTextField = "dic_Name";
            this.txt_ddcpr_TypeEdit.DataValueField = "ID";
            this.txt_ddcpr_TypeEdit.DataBind();
        }
        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz' OR  dic_Type='cpr_hyxzByJL'";
            this.txt_ddProfessionTypeEdit.DataSource = bll_dic.GetList(str_where);
            this.txt_ddProfessionTypeEdit.DataTextField = "dic_Name";
            this.txt_ddProfessionTypeEdit.DataValueField = "ID";
            this.txt_ddProfessionTypeEdit.DataBind();
        }
        //工程来源
        protected void BindCorpSrc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_src'";
            this.txt_ddSourceWayEdit.DataSource = bll_dic.GetList(str_where);
            this.txt_ddSourceWayEdit.DataTextField = "dic_Name";
            this.txt_ddSourceWayEdit.DataValueField = "ID";
            this.txt_ddSourceWayEdit.DataBind();
        }
        //合同阶段
        protected void BindCorpProc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_jd' or dic_Type='cpr_jdByJL'";
            this.chk_cprjd.DataSource = bll_dic.GetList(str_where);
            this.chk_cprjd.DataTextField = "dic_Name";
            this.chk_cprjd.DataValueField = "ID";
            this.chk_cprjd.DataBind();
        }
        //显示合同信息
        protected void ShowCoperation()
        {
            TG.BLL.cm_SuperCoperation bll_cpr = new TG.BLL.cm_SuperCoperation();
            TG.Model.cm_SuperCoperation model_cpr = bll_cpr.GetFatherModel(CoperationSysno);
            if (model_cpr != null)
            {
                this.hid_cprid.Value = model_cpr.cpr_Id.ToString();
                //赋值
                this.txtcpr_No.Text = model_cpr.cpr_No ?? "";
                this.txt_ddcpr_Type.Text = model_cpr.cpr_Type.Trim();
                this.txt_cprType.Text = Convert.ToString(model_cpr.cpr_Type2 ?? "").Trim();
                this.txt_cprName.Text = model_cpr.cpr_Name.Trim();
                this.txt_cprBuildUnit.Text = model_cpr.BuildUnit == null ? "" : model_cpr.BuildUnit.Trim();
                //设计等级
                this.txt_buildType.Text = model_cpr.BuildType == null ? "" : model_cpr.BuildType.Trim();
                this.txtcpr_Account.Text = model_cpr.cpr_Acount.ToString();
                this.txtcpr_Account0.Text = model_cpr.cpr_ShijiAcount.ToString();
                this.txtInvestAccount.Text = model_cpr.cpr_Touzi.ToString();
                this.txtInvestAccount0.Text = model_cpr.cpr_ShijiTouzi.ToString();
                string result = "";
                if (model_cpr.cpr_Process.Trim() != "")
                {
                    string[] array = model_cpr.cpr_Process.Split(new char[] { ',' }, StringSplitOptions.None);
                    TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

                    for (int i = 0; i < array.Length; i++)
                    {
                        result += bll_dic.GetModel(int.Parse(array[i])).dic_Name + ",";
                    }
                    result = result.IndexOf(",") > -1 ? result.Remove(result.Length - 1) : "";
                }
                this.txt_ddcpr_Stage.Text = result;
                this.txtSingnDate.Text = Convert.ToDateTime(model_cpr.cpr_SignDate).ToShortDateString();
                this.txtSingnDate2.Text = Convert.ToDateTime(model_cpr.cpr_SignDate2).ToShortDateString();
                this.txtCompleteDate.Text = model_cpr.cpr_DoneDate == null ? "" : Convert.ToDateTime(model_cpr.cpr_DoneDate).ToShortDateString();
                this.txtcpr_Remark.InnerText = model_cpr.cpr_Mark == null ? "" : model_cpr.cpr_Mark.Trim();
                this.txt_buildArea.Text = model_cpr.BuildArea == null ? "0" : Convert.ToString(model_cpr.BuildArea);
                this.txt_AreaUnit.Text = model_cpr.AreaUnit;
                this.txt_proFuze.Text = model_cpr.ChgPeople.Trim();
                this.txt_fzphone.Text = model_cpr.ChgPhone.Trim();
                this.txtFParty.Text = model_cpr.ChgJia.Trim();
                this.txt_jiafphone.Text = model_cpr.ChgJiaPhone.Trim();
                if (model_cpr.BuildPosition.Trim() == "-1")
                {
                    this.txt_ddProjectPosition.Text = "";
                }
                else
                {
                    this.txt_ddProjectPosition.Text = model_cpr.BuildPosition.Trim();
                }
                if (model_cpr.Industry.Trim() == "-1")
                {
                    this.txt_ddProfessionType.Text = "";
                }
                else
                {
                    this.txt_ddProfessionType.Text = model_cpr.Industry.Trim();
                }
                if (model_cpr.BuildSrc.Trim() == "-1")
                {
                    this.txt_ddSourceWay.Text = "";
                }
                else
                {
                    this.txt_ddSourceWay.Text = model_cpr.BuildSrc.Trim();
                }
                this.txt_cjbm.Text = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit;
                this.txt_tbcreate.Text = model_cpr.TableMaker == null ? "" : model_cpr.TableMaker.Trim();


                //设计等级
                //if (model_cpr.BuildType != null)
                //{
                //    this.lbl_BuildTypeLevel.Text = model_cpr.BuildType.Trim().Replace('+', ',');
                //}
                //结构形式
                if (model_cpr.StructType != null)
                {
                    this.lbl_StructType.Text = TG.Common.StringPlus.ResolveStructString(model_cpr.StructType.ToString());
                }
                //建筑分类
                if (model_cpr.BuildStructType != null)
                {

                    //建筑分类
                    this.lbl_BuildStructType.Text = TG.Common.StringPlus.ResolveStructString(model_cpr.BuildStructType.ToString());
                }
                //层数
                string[] floors = model_cpr.Floor.Split(new char[] { '|' }, StringSplitOptions.None);
                this.lbl_upfloor.Text = floors[0].ToString();
                this.lbl_downfloor.Text = floors[1].ToString();
                //多栋楼
                this.txt_multibuild.InnerText = model_cpr.MultiBuild;
            }

        }
        //建筑分类
        protected void BindBuildStuctTypeEdit()
        {
            BindProInfoConfig("SuperBuildType", this.asTreeviewStructTypeEdit.RootNode);
            this.asTreeviewStructTypeEdit.CollapseAll();
        }
        //结构形式
        protected void BindStructTypeEdit()
        {
            BindProInfoConfig("SuperStructType", this.asTreeviewStructEdit.RootNode);
            this.asTreeviewStructEdit.CollapseAll();
        }

        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }
        /// <summary>
        /// 绑定树列表  qpl 20140108 
        /// </summary>
        /// <param name="infokey"></param>
        /// <param name="rootnode"></param>
        /// <param name="flagrootbox">根节点是否显示CheckBox</param>
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode, bool showsubbox)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root, showsubbox);
            }
        }
        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            //this.asTreeviewStruct.Theme = macOS;
            //this.asTreeviewStructType.Theme = macOS;
            //this.asTreeViewBuildType.Theme = macOS;
            this.asTreeviewStructEdit.Theme = macOS;
            this.asTreeviewStructTypeEdit.Theme = macOS;

        }
        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        /// <summary>
        /// 绑定树列表  qpl 20140108 
        /// </summary>
        /// <param name="xmlroot"></param>
        /// <param name="root"></param>
        /// <param name="flagrootbox">根节点是否显示CheckBox</param>
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root, bool showsubbox)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                //标示子节点是否显示CheckBox qpl 20140108
                switch (name)
                {
                    case "特级":
                    case "一级":
                    case "二级":
                    case "三级":
                        linknode.EnableCheckbox = !showsubbox;
                        break;
                    default:
                        linknode.EnableCheckbox = showsubbox;
                        break;
                }

                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);

                        //子节点的checkbox 不可用  qpl 20140108
                        sublinknode.EnableCheckbox = showsubbox;

                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode, showsubbox);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        //获取所有树文字数据
        protected void GetAllNodeTex(ASTreeViewNode nodes, ref List<string> alltext)
        {
            foreach (ASTreeViewNode node in nodes.ChildNodes)
            {
                alltext.Add(node.NodeText);
                if (node.ChildNodes.Count > 0)
                {
                    GetAllNodeTex(node, ref alltext);
                }
            }
        }
        [AjaxMethod]
        protected void output_Click(object sender, EventArgs e)
        {
            GetDropDownTreeCheckedValue();
        }
        //加载存储的树节点
        protected void LoadStructData(string strData, ASDropDownTreeView contrl)
        {
            if (!string.IsNullOrEmpty(strData))
            {
                //加载所有选中节点
                List<string> nodelist = new List<string>();
                //第一级
                string[] strarray = strData.Split('+');

                for (int i = 0; i < strarray.Length; i++)
                {
                    //特殊第二级
                    string[] strsubarray = strarray[i].Split('^');
                    for (int j = 0; j < strsubarray.Length; j++)
                    {
                        //递归的三级以后
                        string[] strendarray = strsubarray[j].Split('*');
                        nodelist.Add(strendarray[strendarray.Length - 1]);
                    }
                }
                //选中节点
                foreach (string nodevalue in nodelist)
                {
                    if (contrl.FindByValue(nodevalue) != null)
                    {
                        contrl.FindByValue(nodevalue).CheckedState = ASTreeViewCheckboxState.Checked;
                    }
                }
            }
        }
        [AjaxMethod]
        public string GetDropDownTreeCheckedValue()
        {
            string rootvalue = "";

            List<ASTreeViewNode> allnodes = this.asTreeviewStructEdit.RootNode.ChildNodes;
            //最终生成字符串

            foreach (ASTreeViewNode node in allnodes)
            {
                string secondvalue = "";
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                    GetSecondNodeList(node, ref secondvalue);
                }
                rootvalue += secondvalue;
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }
            return rootvalue;
        }
        protected void GetSecondNodeList(ASTreeViewNode node, ref string value)
        {
            if (node.ChildNodes.Count > 0)
            {
                //返回值
                StringBuilder sbresult = new StringBuilder();

                foreach (ASTreeViewNode snode in node.ChildNodes)
                {
                    if (snode.CheckedState == ASTreeViewCheckboxState.Checked || snode.CheckedState == ASTreeViewCheckboxState.HalfChecked)
                    {
                        //拼接第二级
                        value = "^" + snode.NodeValue;
                        string subvalue = "";
                        subvalue = value;
                        GetChildNodes(snode, ref subvalue);
                        foreach (string key in sblist)
                        {
                            sbresult.Append(key);
                        }
                        //清空当前列表
                        sblist.Clear();
                    }
                }
                value = sbresult.ToString();
            }
        }
        //查询数据
        List<string> sblist = new List<string>();
        protected void GetChildNodes(ASTreeViewNode node, ref string value)
        {
            StringBuilder sb = new StringBuilder();
            if (node.ChildNodes.Count > 0)
            {
                foreach (ASTreeViewNode childnode in node.ChildNodes)
                {
                    if ((childnode.CheckedState == ASTreeViewCheckboxState.Checked) || (childnode.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                    {
                        string tempvalue = value + "*" + childnode.NodeValue;

                        if (childnode.ChildNodes.Count > 0)
                        {
                            //如果还有子节点，继续遍历
                            GetChildNodes(childnode, ref tempvalue);
                        }
                        else
                        {
                            //添加末节点
                            sb.Append(tempvalue);
                        }
                    }
                }
            }
            else
            {
                //添加末节点
                sb.Append(value);
            }
            //赋值value
            sblist.Add(sb.ToString());
        }
        //加载存储的树节点
    }
}