﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using NPOI.HSSF.UserModel;
using System.IO;
using NPOI.SS.UserModel;

namespace TG.Web.Coperation
{
    public partial class cpr_CoperationAuditListBymaster : PageBase
    {
        public string ColumnsContent
        {
            get;
            set;
        }
        public string GCJL_ColumnsContent
        {
            get;
            set;
        }
        /// <summary>
        /// 是否需要登录
        /// </summary>
        protected override bool IsAuth
        {
            get
            {
                return true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定字段
                BindColumns();
                BindGCJLColumns();
                //绑定单位信息
                BindUnit();
                //绑定年份
                BindYear();
                //绑定合同信息
                GetCoperationInfo();

                //绑定权限
                BindPreviewPower();

            }
        }
        protected void BindColumns()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("全选", "all");
            dic.Add("合同编号", "cpr_No");
            dic.Add("合同名称", "cpr_Name");
            dic.Add("甲方负责人", "ChgJia");
            dic.Add("工程地点", "BuildPosition");           
            dic.Add("承接部门", "cpr_Unit");
            dic.Add("签订日期", "qdrq");
            dic.Add("完成日期", "wcrq");
            dic.Add("统计年份", "tjrq");    
            dic.Add("录入时间", "lrsj");
            dic.Add("合同分类", "cpr_Type");
            dic.Add("合同类型", "cpr_Type2");
            dic.Add("建筑类别", "BuildType");
            dic.Add("建设单位", "BuildUnit");
            dic.Add("建设规模(㎡)", "BuildArea");
            dic.Add("结构形式", "StructType");
            dic.Add("建筑分类", "BuildStructType");
            dic.Add("层数", "Floor");
            dic.Add("工程负责人", "ChgPeople");
            dic.Add("工程负责人电话", "ChgPhone");
            dic.Add("甲方负责人电话", "ChgJiaPhone");
            dic.Add("合同额(万元)", "cpr_Acount");
            dic.Add("实际合同额(万元)", "cpr_ShijiAcount");
            dic.Add("投资额(万元)", "cpr_Touzi");
            dic.Add("实际投资额(万元)", "cpr_ShijiTouzi");
            dic.Add("行业性质", "Industry");
            dic.Add("工程来源", "BuildSrc");
            dic.Add("合同阶段", "cpr_Process");
            dic.Add("制表人", "TableMaker");
            dic.Add("多栋楼", "MultiBuild");
            dic.Add("合同备注", "cpr_Mark");
            dic.Add("录入人", "InsertUser");


            foreach (KeyValuePair<string, string> pair in dic)
            {
                ColumnsContent += "<label><input type='checkbox' value='" + pair.Value + "' />" + pair.Key + "</label>";
            }


            //  BindProInfoConfig(this.asTreeviewStruct.RootNode,dic);
            //this.asTreeviewStruct.CollapseAll();           
        }
        //监理合同
        protected void BindGCJLColumns()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("全选", "all");
            dic.Add("合同编号", "cpr_No");
            dic.Add("合同名称", "cpr_Name");
            dic.Add("甲方负责人", "ChgJia");
            dic.Add("工程地点", "BuildPosition");           
            dic.Add("承接部门", "cpr_Unit");
            dic.Add("签订日期", "qdrq");
            dic.Add("完成日期", "wcrq");
            dic.Add("统计年份", "tjrq");    
            dic.Add("录入时间", "lrsj");
            dic.Add("建筑类别", "BuildType");
            dic.Add("合同分类", "cpr_Type");
            dic.Add("合同类型", "cpr_Type2");
            dic.Add("建设单位", "BuildUnit");
            dic.Add("建设规模", "BuildArea");
            dic.Add("结构形式", "StructType");
            dic.Add("建筑分类", "BuildStructType");
            dic.Add("层数", "Floor");
            dic.Add("工程负责人", "ChgPeople");
            dic.Add("工程负责人电话", "ChgPhone");
            dic.Add("外聘人员信息", "ExternalMember");
            dic.Add("甲方负责人电话", "ChgJiaPhone");
            dic.Add("行业性质", "Industry");
            dic.Add("合同额(万元)", "cpr_Acount");
            dic.Add("实际合同额(万元)", "cpr_ShijiAcount");
            dic.Add("投资额(万元)", "cpr_Touzi");
            dic.Add("实际投资额(万元)", "cpr_ShijiTouzi");
            dic.Add("工程来源", "BuildSrc");
            dic.Add("合同阶段", "cpr_Process");
            dic.Add("制表人", "TableMaker");
            dic.Add("多栋楼", "MultiBuild");
            dic.Add("合同备注", "cpr_Mark");
            dic.Add("录入人", "InsertUser");

            foreach (KeyValuePair<string, string> pair in dic)
            {
                GCJL_ColumnsContent += "<label><input type='checkbox' value='" + pair.Value + "' />" + pair.Key + "</label>";
            }


            //  BindProInfoConfig(this.asTreeviewStruct.RootNode,dic);
            //this.asTreeviewStruct.CollapseAll();           
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
            this.drp_year.SelectedValue = DateTime.Now.Year.ToString();
        }
        //是否检查权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        //判断权限
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        /// <summary>
        /// 得到合同基本信息
        /// </summary>
        private void GetCoperationInfo()
        {
            TG.BLL.cm_CoperationAudit cpraudit = new TG.BLL.cm_CoperationAudit();
            //通过评审项目
            StringBuilder sb = new StringBuilder("");

            //权限
            GetPreviewPowerSql(ref sb);
            this.hid_where.Value = sb.ToString();
        }

        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_export_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();

            string modelPath = " ~/TemplateXls/CoperationAuditList.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            IRow dataRowTitle = ws.GetRow(1);      

            string str_columnschinaname = this.hid_cols.Value.Trim();
            string str_columnsname = this.hid_colsvalue.Value.Trim();

            //字段标题
            if (!string.IsNullOrEmpty(str_columnschinaname))
            {
                string[] columnsnamelist = str_columnschinaname.Split(',');
                for (int j = 0; j < columnsnamelist.Length; j++)
                {
                    ICell celltitle = dataRowTitle.CreateCell(j + 1);
                    celltitle.CellStyle = style2;
                    celltitle.SetCellValue(columnsnamelist[j]);
                }
                //评审状态 标题
                ICell cellaudit = dataRowTitle.CreateCell((columnsnamelist.Length + 1));
                cellaudit.CellStyle = style2;
                cellaudit.SetCellValue("评审状态");
            }

            int row = 2;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                //cell = dataRow.CreateCell(1);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["cpr_No"].ToString());

                //cell = dataRow.CreateCell(2);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["cpr_Name"].ToString());

                //cell = dataRow.CreateCell(3);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["ChgJia"].ToString());

                //cell = dataRow.CreateCell(4);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["BuildPosition"].ToString());

                //cell = dataRow.CreateCell(5);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["cpr_Unit"].ToString());

                //cell = dataRow.CreateCell(6);
                //cell.CellStyle = style2;
                //cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["cpr_SignDate"]).ToString("yyyy-MM-dd"));

                //cell = dataRow.CreateCell(7);
                //cell.CellStyle = style2;
                //cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["cpr_DoneDate"]).ToString("yyyy-MM-dd"));

                //cell = dataRow.CreateCell(8);
                //cell.CellStyle = style2;

                //cell.SetCellValue(setAuditStatus(dt.Rows[i]["AuditSysNo"].ToString(), dt.Rows[i]["ManageLevel"].ToString(), dt.Rows[i]["NeedLegalAdviser"].ToString(), dt.Rows[i]["AuditStatus"].ToString()));

                //cell = dataRow.CreateCell(9);
                //cell.CellStyle = style2;
                //if (!string.IsNullOrEmpty(dt.Rows[i]["InsertDate"].ToString()))
                //{
                //    cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["InsertDate"]).ToString("yyyy-MM-dd"));
                //}
                if (!string.IsNullOrEmpty(str_columnsname))
                {
                    string[] columnslist = str_columnsname.Split(',');
                    for (int j = 0; j < columnslist.Length; j++)
                    {
                        cell = dataRow.CreateCell(j + 1);
                        cell.CellStyle = style2;
                        string cellvalue = dt.Rows[i][columnslist[j].Trim()].ToString();
                        if (columnslist[j].Trim() == "Floor")
                        {
                            if (!string.IsNullOrEmpty(cellvalue))
                            {
                                string[] arr = cellvalue.Split('|');
                                cellvalue = "地上:" + arr[0] + " 地下:" + arr[1];
                            }

                        }
                        else if (columnslist[j].Trim() == "cpr_Process")
                        {
                            if (!string.IsNullOrEmpty(cellvalue))
                            {
                                string temp = "";
                                string[] arr = cellvalue.Split(',');
                                for (int k = 0; k < arr.Length; k++)
                                {
                                    if (arr[k] == "27")
                                    {
                                        temp = "方案,";
                                    }
                                    if (arr[k] == "28")
                                    {
                                        temp += "初设,";
                                    }
                                    if (arr[k] == "29")
                                    {
                                        temp += "施工图,";
                                    }
                                    if (arr[k] == "30")
                                    {
                                        temp += "其他,";
                                    }
                                }
                                cellvalue = temp;
                            }

                        }
                        cell.SetCellValue(cellvalue);
                    }
                    //评审状态
                    cell = dataRow.CreateCell((columnslist.Length + 1));
                    cell.CellStyle = style2;
                    cell.SetCellValue(setAuditStatus(dt.Rows[i]["AuditSysNo"].ToString(), dt.Rows[i]["ManageLevel"].ToString(), dt.Rows[i]["NeedLegalAdviser"].ToString(), dt.Rows[i]["AuditStatus"].ToString()));

                }
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("合同审批列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        /// <summary>
        /// 得到数据信息
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {
            StringBuilder sb = new StringBuilder("");
            //时间
            if (this.drp_year.SelectedIndex != 0)
            {
                sb.AppendFormat(" AND year(cpr_SignDate)={0} ", this.drp_year.SelectedValue);
            }
            //名字不为空
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.Append(" AND cpr_Name LIKE '%" + keyname + "%'  ");
            }
            //绑定单位
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND cpr_Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            }
            sb.Append("AND cpr_Type not  like'%项目协议%'");

            //录入时间  
            string startTime = txt_start.Value;//录入时间
            string endTime = txt_end.Value;//录入时间
            if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                sb.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
            }
            else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                sb.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
            }
            else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                sb.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
            }

            //只显示生产经营的合同类型
            sb.Append(" and cpr_Type2 in (select dic_Name from cm_Dictionary where dic_Type='cpr_lx') ");

            //检查权限
            GetPreviewPowerSql(ref sb);

            TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();

            DataTable dt = bll.GetCoperationAuditListExport(sb.ToString()).Tables[0];

            return dt;
        }

        /// <summary>
        /// 设置审批状态
        /// </summary>
        /// <param name="auditid"></param>
        /// <param name="aduitStaues"></param>
        /// <returns></returns>
        private string setAuditStatus(string AuditSysNo, string ManageLevel, string NeedLegalAdviser, string aduitStaues)
        {
            string strStatus = "";
            if (AuditSysNo == null || AuditSysNo == "")
            {
                strStatus = "未发起评审";
            }
            if (aduitStaues == "A")
            {
                strStatus = "承接部门未审批";
            }
            if (aduitStaues == "B")
            {
                strStatus = "承接部门审批通过";
            }
            if (aduitStaues == "C")
            {
                strStatus = "承接部门未通过";
            }
            if (aduitStaues == "J")
            {
                strStatus = "全部通过";
            }
            if (aduitStaues == "K")
            {
                strStatus = "院长未通过";
            }
            if (aduitStaues == "I")
            {
                aduitStaues = "所长未通过";
            }
            if (aduitStaues == "G")
            {
                strStatus = "技术质量部未通过";
            }
            if (aduitStaues == "D")
            {
                strStatus = "生产经营部通过";
            }
            if (aduitStaues == "E")
            {
                strStatus = "生产经营部未通过";
            }
            if (aduitStaues == "H" && ManageLevel == "1")
            {
                strStatus = "全部通过";
            }
            if (aduitStaues == "H" && ManageLevel == "0")
            {
                strStatus = "所长通过";
            }
            if (aduitStaues == "F")
            {
                strStatus = "技术质量部通过";
            }
            return strStatus;
        }
    }
}