﻿using Geekees.Common.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace TG.Web.Coperation
{
    public partial class SearchCoperationBymaster : PageBase
    {
        public string ColumnsContent
        {
            get;
            set;
        }
        public string asTreeviewStructObjID
        {
            get
            {
                return this.asTreeviewStruct.GetClientTreeObjectId();
            }
        }
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructType.GetClientTreeObjectId();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定字段
                BindColumns();
                //合同类别
                BindCorpType();
                //合同阶段
                BindCorpProc();
                //项目地点
                BindDdList();
                //行业性质
                BindXzList();
                //绑定承接部门
                BindUnit();
                //绑定权限
                BindPreviewPower();
                //设置样式
                SetDropDownTreeThem();
                //绑定建筑结构样式修改时
                BindStructTypeEdit();
                //绑定建筑分类修改时
                BindBuildStuctTypeEdit();
                //设计等级
                BindBuildType();

            }
        }
        protected void BindColumns()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("全选", "all");
            dic.Add("合同编号", "cpr_No");
            dic.Add("合同分类", "cpr_Type");
            dic.Add("建筑类别", "BuildType");
            dic.Add("建筑规模", "BuildArea");
            dic.Add("合同类型", "cpr_Type2");
            dic.Add("建设单位", "BuildUnit");
            dic.Add("结构形式", "StructType");
            dic.Add("建筑分类", "BuildStructType");
            dic.Add("层数", "Floor");           
            dic.Add("工程负责人电话", "ChgPhone");
            dic.Add("甲方负责人", "ChgJia");
            dic.Add("甲方负责人电话", "ChgJiaPhone");
            dic.Add("工程地点", "BuildPosition");
            dic.Add("行业性质", "Industry");
            dic.Add("实际合同额", "cpr_ShijiAcount");
            dic.Add("投资额", "cpr_Touzi");
            dic.Add("实际投资额", "cpr_ShijiTouzi");
            dic.Add("工程来源", "BuildSrc");          
            dic.Add("制表人", "TableMaker");
            dic.Add("多栋楼", "MultiBuild");
            dic.Add("合同备注", "cpr_Mark");
            dic.Add("录入人", "InsertUser");

            foreach (KeyValuePair<string, string> pair in dic)
            {
                ColumnsContent += "<label><input type='checkbox' value='" + pair.Value + "' />" + pair.Key + "</label>";
            }


            //  BindProInfoConfig(this.asTreeviewStruct.RootNode,dic);
            //this.asTreeviewStruct.CollapseAll();           
        }
        //合同阶段
        protected void BindCorpProc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_jd'";
            this.chk_cprjd.DataSource = bll_dic.GetList(str_where);
            this.chk_cprjd.DataTextField = "dic_Name";
            this.chk_cprjd.DataValueField = "ID";
            this.chk_cprjd.DataBind();
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.txt_ddcpr_Type.DataSource = bll_dic.GetList(str_where);
            this.txt_ddcpr_Type.DataTextField = "dic_Name";
            this.txt_ddcpr_Type.DataValueField = "ID";
            this.txt_ddcpr_Type.DataBind();
        }
        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            //this.asTreeviewStruct.Theme = macOS;
            //this.asTreeviewStructType.Theme = macOS;
            //this.asTreeViewBuildType.Theme = macOS;
            this.asTreeviewStruct.Theme = macOS;
            this.asTreeviewStructType.Theme = macOS;

        }
        //建筑类别
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
            this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "ID";
            this.drp_buildtype.DataBind();
        }
        //建筑分类
        protected void BindBuildStuctTypeEdit()
        {
            BindProInfoConfig("BuildType", this.asTreeviewStructType.RootNode);
            this.asTreeviewStructType.CollapseAll();
        }
        //结构形式
        protected void BindStructTypeEdit()
        {
            BindProInfoConfig("StructType", this.asTreeviewStruct.RootNode);
            this.asTreeviewStruct.CollapseAll();
        }
        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }
        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //是否需要权限判断
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //绑定承接部门
        public void BindUnit()
        {
            //承接部门 
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            //不显示的单位
            if (base.RolePowerParameterEntity.PreviewPattern == 1)
            {
                strWhere += " 1=1 AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            else
            {
                strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            }
            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //绑定项目来源
        public void BindDdList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();

            DataSet dic_proly = dic.GetList(" dic_type='cpr_src'");
            ddsource.DataValueField = "id";
            ddsource.DataTextField = "dic_name";
            ddsource.DataSource = dic_proly;
            ddsource.DataBind();
        }
        //绑定行业性质
        protected void BindXzList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            DataSet dic_proly = dic.GetList(" dic_type='cpr_hyxz'");
            ddType.DataValueField = "id";
            ddType.DataTextField = "dic_name";
            ddType.DataSource = dic_proly;
            ddType.DataBind();
        }
        public void GetList()
        {
            StringBuilder sb = new StringBuilder("");

            //检查权限
            GetPreviewPowerSql(ref sb);
            this.hid_where.Value = sb.ToString();

        }

    }
}