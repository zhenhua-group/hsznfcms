﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;

namespace TG.Web.Coperation
{
    public partial class ProjectInvoiceConfirmBymaster : PageBase
    {
        /// <summary>
        /// 项目系统编号
        /// </summary>
        public int proSysNo
        {
            get
            {
                int proSysNo = 0;
                int.TryParse(Request["projID"], out proSysNo);
                return proSysNo;
            }
        }
        /// <summary>
        /// 合同系统编号
        /// </summary>
        public int cprSysNo
        {
            get
            {
                int proSysNo = 0;
                int.TryParse(Request["cprID"], out proSysNo);
                return proSysNo;
            }
        }
        public int sysNo
        {
            get
            {
                int allotID = 0;
                int.TryParse(Request["sysNo"], out allotID);
                return allotID;
            }
        }



        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }
        public string Status { get; set; }

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getDataInfo();
            }
        }

        private void getDataInfo()
        {
            TG.BLL.cm_ProjectInvoice bll = new TG.BLL.cm_ProjectInvoice();
            TG.Model.cm_Coperation_Info cpr_model = bll.GetCprModelInfo(cprSysNo);
            TG.Model.cm_Project pro_model = new TG.BLL.cm_Project().GetModel(proSysNo);
            TG.Model.cm_ProjectInvoice invoice_model = bll.GetModel(sysNo);
            if (cpr_model != null)
            {
                lbl_cprname.Text = cpr_model.cpr_Name;
                lbl_number.Text = cpr_model.cpr_No;
                lbl_allcount.Text = cpr_model.cpr_Acount;               
            }
            if (pro_model!=null)
            {
                lbl_proname.Text = pro_model.pro_name;
            }
            if (invoice_model != null)
            {
                txt_payCount.Text = invoice_model.InvoiceAmount.ToString();
                drop_type.Text = invoice_model.InvoiceType;
                txt_invoicename.Text = invoice_model.InvoiceName;
                txt_times.Text = Convert.ToDateTime(invoice_model.InvoiceDate).ToString("yyyy-MM-dd");
                txt_chargeRemark.Text = invoice_model.ApplyMark;
                lbl_ApplyUserName.Text = invoice_model.InsertUserName;
                string status = invoice_model.Status.Trim();

                if (status != "A")
                {
                    lbl_status.Text = "<span class=\"label label-sm label-danger\" >不通过</span>";
                    if (status != "B")
                    {
                        txt_InvoiceCode.Value = invoice_model.InvoiceCode;
                        txt_CountryAddress.Value = invoice_model.CountryAddress;
                        txt_CountryPhone.Value = invoice_model.CountryPhone;
                        txt_Bankaccount.Value = invoice_model.Bankaccount;
                        txt_Accountinfo.Value = invoice_model.Accountinfo;
                        txt_Mark.Text = invoice_model.CaiwuMark;
                        lbl_status.Text = "<span class=\"label label-sm label-success\" >&nbsp;通过&nbsp;</span>";
                    }

                    txt_InvoiceCode.Disabled = true;
                    txt_CountryAddress.Disabled = true;
                    txt_Accountinfo.Disabled = true;
                    txt_Bankaccount.Disabled = true;
                    txt_CountryPhone.Disabled = true;
                    txt_Mark.Enabled = false;

                    sureButton.Visible = false;
                    btn_NotSure.Visible = false;

                }
            }
        }

        /// <summary>
        /// 确认通过
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void sureButton_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_ProjectInvoice bll = new TG.BLL.cm_ProjectInvoice();
            TG.Model.cm_ProjectInvoice invoice_model = bll.GetModel(sysNo);
            
            invoice_model.projID = proSysNo;
            invoice_model.cprID = cprSysNo;
            invoice_model.ID = sysNo;
            invoice_model.Status = "C";
            invoice_model.InvoiceCode = txt_InvoiceCode.Value;
            invoice_model.CountryAddress = txt_CountryAddress.Value;
            invoice_model.CountryPhone = txt_CountryPhone.Value;
            invoice_model.Bankaccount = txt_Bankaccount.Value;
            invoice_model.Accountinfo = txt_Accountinfo.Value; 
            invoice_model.CaiwuMark = txt_Mark.Text;
            invoice_model.AuditUserID = UserSysNo;
            invoice_model.AuditDate = DateTime.Now;

            int count = bll.Update(invoice_model);
            if (count > 0)
            {
                TG.Model.cm_ProjectInvoice model = bll.GetModel(sysNo);
                //给申请人发消息
                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("fromUser={0}&sysNo={1}&projID={2}&cprID={3}&auditstatus={4}",UserSysNo,sysNo,proSysNo,cprSysNo,"C"),
                    FromUser = int.Parse(model.InsertUserID.ToString()),
                    InUser = UserSysNo,
                    MsgType = 102,
                    ToRole = "0",
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", lbl_cprname.Text, "申请开票通过"),
                    QueryCondition = lbl_cprname.Text,
                    IsDone = "B"
                };
                int resultCount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                //待办状态修改
                UpdateWorkDoneStatus();

                TG.Common.MessageBox.ShowAndRedirect(this, "财务开票通过！", "cpr_SysMsgListViewBymaster.aspx?typepost=" + TypePost + "&flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "");

            }
        }

        protected void btn_NotSure_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_ProjectInvoice bll = new TG.BLL.cm_ProjectInvoice();
            TG.Model.cm_ProjectInvoice invoice_model = bll.GetModel(sysNo);

            invoice_model.cprID = cprSysNo;
            invoice_model.ID = sysNo;
            invoice_model.Status = "B";
            invoice_model.InvoiceCode = txt_InvoiceCode.Value;
            invoice_model.CountryAddress = txt_CountryAddress.Value;
            invoice_model.CountryPhone = txt_CountryPhone.Value;
            invoice_model.Bankaccount = txt_Bankaccount.Value;
            invoice_model.Accountinfo = txt_Accountinfo.Value; 
            invoice_model.CaiwuMark = txt_Mark.Text;
            invoice_model.AuditUserID = UserSysNo;
            invoice_model.AuditDate = DateTime.Now;

            int count = bll.Update(invoice_model);
            if (count > 0)
            {
                TG.Model.cm_ProjectInvoice model = bll.GetModel(sysNo);
                //给申请人发消息
                SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                {
                    ReferenceSysNo = string.Format("fromUser={0}&sysNo={1}&projID={2}&cprID={3}&auditstatus={4}", UserSysNo, sysNo, proSysNo, cprSysNo, "B"),
                    FromUser = int.Parse(model.InsertUserID.ToString()),
                    InUser = UserSysNo,
                    MsgType = 102,
                    ToRole = "0",
                    MessageContent = string.Format("关于 \"{0}\" 的{1}消息！", lbl_cprname.Text, "申请开票不通过"),
                    QueryCondition = lbl_cprname.Text,
                    IsDone = "B"
                };
                int resultCount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                //待办状态修改
                UpdateWorkDoneStatus();
            }
            TG.Common.MessageBox.ShowAndRedirect(this, "财务开票不通过！", "cpr_SysMsgListViewBymaster.aspx?typepost=" + TypePost + "&flag=" + Aflag + "&action=" + MessageAction + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "");

        }

        //更新待办状态
        protected void UpdateWorkDoneStatus()
        {
            //修改办公状态
            int count = new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatus(MessageID);
        }
    }
}