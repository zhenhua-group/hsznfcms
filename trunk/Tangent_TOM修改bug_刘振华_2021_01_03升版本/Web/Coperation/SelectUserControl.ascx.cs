﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using AjaxPro;
using Newtonsoft.Json;

namespace TG.Web.Coperation
{
	public partial class SelectUserControl : System.Web.UI.UserControl
	{

		public List<TG.Model.tg_member> UserList { get; set; }

		public string HTML { get; set; }

		public int DefaultDeparmentSysNo { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			Utility.RegisterTypeForAjax(typeof(SelectUserControl));
			//GetRoleList();
			GetUserDepartments();
		}


		private void GetRoleList()
		{
			string userShortName = Request["userShortName"];
			string whereSql = "";
			if (!string.IsNullOrEmpty(userShortName))
			{
				whereSql += " AND mem_Login=N'" + userShortName + "'";
			}
			UserList = new TG.BLL.tg_member().GetUsers(whereSql);

			JoinHTML();
		}

		/// <summary>
		/// 拼接前台HTML
		/// </summary>
		private void JoinHTML()
		{
			string html = "<tr>";
			int i = 0;
			UserList.ForEach(user =>
			{
				if (i % 6 == 0 && i != 0)
				{
					html += "</tr><tr>";
				}
				html += "<td><input type=\"checkbox\" name=\"roleUsers\" username=\"" + user.mem_Name + "\" value=\"" + user.mem_ID + "\"  /><span>" + user.mem_Name + "<span></td>";
				i++;
			});
			html = html += "</tr>";
			HTML = html;
		}

		private void GetUserDepartments()
		{
			List<TG.Model.tg_unit> departmentsList = new TG.BLL.tg_member().GetDepartments();
			RepeaterDepartment.DataSource = departmentsList;
			RepeaterDepartment.DataBind();

			DefaultDeparmentSysNo = TG.BLL.cm_CoperationAuditConfig.GetDeparmentSysNo(ref departmentsList, "网络");
		}

		#region AjaxMethod
		/// <summary>
		/// 根据条件得到用户
		/// </summary>
		/// <param name="userShortName"></param>
		/// <param name="departmentSysNo"></param>
		/// <returns></returns>
		[AjaxMethod]
		public string GetUserList(string userShortName, string departmentSysNo)
		{
			string whereSql = "";
			if (!string.IsNullOrEmpty(userShortName))
			{
				whereSql += " AND (mem_Login like N'%" + userShortName + "%' OR mem_Name like N'%" + userShortName + "%')";
			}
			if (!string.IsNullOrEmpty(departmentSysNo) && departmentSysNo != "0")
			{
				whereSql += " AND mem_Unit_ID=" + int.Parse(departmentSysNo);
			}
			List<TG.Model.tg_member> UserList = new TG.BLL.tg_member().GetUsers(whereSql);

			string jsonResult = Newtonsoft.Json.JsonConvert.SerializeObject(UserList);

			return jsonResult;
		}
		#endregion
	}
}