﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace TG.Web.Coperation
{
    public partial class DownLoadFile : System.Web.UI.Page
    {
        public string FileName
        {
            get
            {
                return Request["fileName"];
            }
        }

        public string FilePath
        {
            get
            {
                return Request["FileURL"];
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.ContentType = "application/x-zip-compressed";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(FileName));
            string type = Request["type"] ?? "";
            string path;
            if (type == "proj")
            {
                path = Server.MapPath("/Attach_User/filedata/projfile/" + FilePath);
            }
            else
            {
                path = Server.MapPath("/Attach_User/filedata/cprfile/" + FilePath);

            }


            if (File.Exists(path))
            {
                Response.WriteFile(path);
            }
            else
            {
                Response.Write("<script type=\"text/javascript\">alert(\"文件不存在，或者登陆超时！\");</script>");
            }
            Response.End();
        }
    }
}