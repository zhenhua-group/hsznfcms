﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;

namespace TG.Web.Coperation
{
    public partial class ProjectChargeBymaster : PageBase
    {
        public string ColumnsContent
        {
            get;
            set;
        }

        //项目信息
        protected TG.BLL.cm_Project CMProject
        {
            get
            {
                return new TG.BLL.cm_Project();
            }
        }
        //合同信息
        protected TG.BLL.cm_Coperation CMCoperation
        {
            get
            {
                return new TG.BLL.cm_Coperation();
            }
        }
        //合同额
        protected decimal CprAcount { get; set; }
        //已收
        protected decimal CprAcountCharge { get; set; }
        //来自首页参数
        public string ParamType
        {
            get
            {
                return Request.QueryString["ParamType"] ?? "";
            }
        }
        //是否生产经营部
        public string isFlag;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定字段
                BindColumns();
                //初始化时间
                InitDate();                
                //单位信息
                BindUnit();
                //绑定年份
                BindYear();
                //项目信息
                BindDoneProject();
                //绑定初始值
                BindInitValue();

                //绑定权限
                BindPreviewPower();

                //选中时间
                SelectCurrentYear();

                //查询用户的角色
                List<TG.Model.cm_Role> roles = new TG.BLL.cm_Role().GetRoleList(UserSysNo);
                isFlag = "false";
                if ((from role in roles where role.RoleName == "生产经营部" select role).Count() > 0)
                {
                    isFlag = "true";
                }

            }
        }
        protected void BindColumns()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("全选", "all");
            dic.Add("合同编号", "cpr_No");
            dic.Add("合同名称", "cpr_Name");
            dic.Add("子公司", "cpr_Unit");
            dic.Add("合同额(万元)", "cpr_Acount");
            dic.Add("实际合同额(万元)", "cpr_ShijiAcount");
            dic.Add("实收总额(万元)", "ssze");
            dic.Add("合同分类", "cpr_Type");
            dic.Add("甲方类型", "BuildType");
            dic.Add("建筑规模(㎡)", "BuildArea");
            dic.Add("合同文本编号", "cpr_Type2");
            dic.Add("建设单位", "BuildUnit");
            dic.Add("结构形式", "StructType");
            dic.Add("建筑分类", "BuildStructType");
           // dic.Add("层数", "Floor");
            dic.Add("工程负责人", "PMUserName");
            dic.Add("工程负责人电话", "ChgPhone");
            dic.Add("甲方负责人", "ChgJia");
            dic.Add("甲方负责人电话", "ChgJiaPhone");
            dic.Add("工程地点", "BuildPosition");
           // dic.Add("行业性质", "Industry");
         
          //  dic.Add("投资额(万元)", "cpr_Touzi");
          //  dic.Add("实际投资额(万元)", "cpr_ShijiTouzi");
          //  dic.Add("签订日期", "qdrq");
            //dic.Add("完成日期", "wcrq");
              dic.Add("签约日期", "tjrq");
           // dic.Add("工程来源", "BuildSrc");
          //  dic.Add("合同阶段", "cpr_Process");
           // dic.Add("制表人", "TableMaker");
          //  dic.Add("多栋楼", "MultiBuild");
            dic.Add("合同备注", "cpr_Mark");
            dic.Add("录入人", "InsertUser");
            dic.Add("录入时间", "lrsj");

            foreach (KeyValuePair<string, string> pair in dic)
            {
                ColumnsContent += "<label><input type='checkbox' value='" + pair.Value + "' />" + pair.Key + "</label>";
            }


            //  BindProInfoConfig(this.asTreeviewStruct.RootNode,dic);
            //this.asTreeviewStruct.CollapseAll();           
        }
        //初始时间设置
        protected void InitDate()
        {
            string str_year = DateTime.Now.Year.ToString();
            this.txt_start.Value = str_year + "-01-01";
            this.txt_end.Value = str_year + "-12-31";
        }

        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }

        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
        //绑定页面初始值
        protected void BindInitValue()
        {
            //绑定登陆用户ID
            this.hid_curuser.Value = UserSysNo.ToString();
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        //检查是否需要权限
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        /// <summary>
        /// 绑定生产部门
        /// </summary>
        protected void BindUnit()
        {
            TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            this.drp_unit.DataSource = bll_unit.GetList(strWhere);
            this.drp_unit.DataTextField = "unit_Name";
            this.drp_unit.DataValueField = "unit_ID";
            this.drp_unit.DataBind();
        }
        //通过项目审批工号分配完项目信息
        protected void BindDoneProject()
        {
            StringBuilder sb = new StringBuilder("");
            //名字不为空
            //if (this.txt_keyname.Text.Trim() != "")
            //{
            //    string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Text.Trim());
            //    sb.Append(" AND cpr_Name LIKE '%" + keyname + "%'  ");
            //}
            ////绑定单位
            //if (this.drp_unit.SelectedIndex != 0)
            //{
            //    sb.Append(" AND cpr_Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            //}
            ////按照年份
            //if (this.drp_year.SelectedIndex != 0)
            //{
            //    sb.AppendFormat(" AND year(cpr_SignDate)={0} ", this.drp_year.SelectedValue);
            //}

            ////收费时间判断
            //string start = this.txt_start.Value.Trim();
            //string end = this.txt_end.Value.Trim();
            //string curtime = DateTime.Now.ToString("yyyy-MM-dd");

            //if (start == "" && end != "")
            //{
            //    start = curtime;
            //}
            //else if (start != "" && end == "")
            //{
            //    end = curtime;
            //}
            //else if (start == "" && end == "")
            //{
            //    start = curtime;
            //    end = curtime;
            //}
            ////实际收款时间
            //string strWhere = string.Format(" and InAcountTime Between '{0}' AND '{1}'", start, end);

            //判断权限
            GetPreviewPowerSql(ref sb);

            this.hid_where.Value = sb.ToString();
            //数量
            // this.AspNetPager1.RecordCount = int.Parse(CMCoperation.GetListPageProcCount(sb.ToString()).ToString());
            //计算合同额
            // CountCharge(sb.ToString());
            //排序
            // string orderString = " Order by " + OrderColumn + " " + Order;
            // sb.Append(orderString);
            //分页
            // this.gv_Coperation.DataSource = CMCoperation.GetListByPageProc(sb.ToString(), this.AspNetPager1.StartRecordIndex, this.AspNetPager1.EndRecordIndex);
            //  this.gv_Coperation.DataBind();
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_export_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();

            string modelPath = " ~/TemplateXls/ProjectCharge.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }
            //标题
            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            //style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 12;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);

            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);

            IRow dataRowTitle = ws.GetRow(1);

            string str_columnschinaname = this.hid_cols.Value.Trim();
            string str_columnsname = this.hid_colsvalue.Value.Trim();

            //字段标题
            if (!string.IsNullOrEmpty(str_columnschinaname))
            {
                string[] columnsnamelist = str_columnschinaname.Split(',');
                for (int j = 0; j < columnsnamelist.Length; j++)
                {
                    ICell celltitle = dataRowTitle.CreateCell(j + 1);
                    celltitle.CellStyle = style2;
                    celltitle.SetCellValue(columnsnamelist[j]);
                }
                //收费进度 标题
                ICell cellaudit = dataRowTitle.CreateCell((columnsnamelist.Length + 1));
                cellaudit.CellStyle = style2;
                cellaudit.SetCellValue("收费进度(%)");
            }

            decimal sumarea = 0, sumaccount = 0, sumssze = 0;
            int row = 2, areaIndex=0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                //cell = dataRow.CreateCell(1);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["cpr_No"].ToString());

                //cell = dataRow.CreateCell(2);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["cpr_Name"].ToString());

                //cell = dataRow.CreateCell(3);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["cpr_Unit"].ToString());

                //cell = dataRow.CreateCell(4);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["cpr_Acount"].ToString());
                //sumaccount += Convert.ToDecimal(dt.Rows[i]["cpr_Acount"].ToString());

                //cell = dataRow.CreateCell(5);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["ssze"].ToString());
                //sumssze += Convert.ToDecimal(dt.Rows[i]["ssze"].ToString());


                //cell = dataRow.CreateCell(6);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["sfjd"].ToString());

                if (!string.IsNullOrEmpty(str_columnsname))
                {
                    string[] columnslist = str_columnsname.Split(',');
                    for (int j = 0; j < columnslist.Length; j++)
                    {
                        cell = dataRow.CreateCell(j + 1);
                        cell.CellStyle = style2;
                        string cellvalue = dt.Rows[i][columnslist[j].Trim()].ToString();
                        if (columnslist[j].Trim() == "Floor")
                        {
                            if (!string.IsNullOrEmpty(cellvalue))
                            {
                                string[] arr = cellvalue.Split('|');
                                cellvalue = "地上:" + arr[0] + " 地下:" + arr[1];
                            }

                        }
                        else if (columnslist[j].Trim() == "cpr_Process")
                        {
                            if (!string.IsNullOrEmpty(cellvalue))
                            {
                                string temp = "";
                                string[] arr = cellvalue.Split(',');
                                for (int k = 0; k < arr.Length; k++)
                                {
                                    if (arr[k] == "27")
                                    {
                                        temp = "方案,";
                                    }
                                    if (arr[k] == "28")
                                    {
                                        temp += "初设,";
                                    }
                                    if (arr[k] == "29")
                                    {
                                        temp += "施工图,";
                                    }
                                    if (arr[k] == "30")
                                    {
                                        temp += "其他,";
                                    }
                                }
                                cellvalue = temp;
                            }
                            else if (columnslist[j].Trim() == "BuildArea")
                            {
                                areaIndex = j + 7;
                            }

                        }
                        
                        cell.SetCellValue(cellvalue);
                    }
                    //收费进度
                    cell = dataRow.CreateCell((columnslist.Length + 1));
                    cell.CellStyle = style2;
                    cell.SetCellValue(dt.Rows[i]["sfjd"].ToString());

                }
                
            }
            //IRow dataRowbottom = ws.CreateRow(dt.Rows.Count + 2);//最后一个
            //ICell cellbottom = dataRowbottom.CreateCell(1);
            //cellbottom.SetCellValue("合计：");
            //cellbottom.CellStyle = style1;

            //if (str_columnsname.Contains("BuildArea"))
            //{
            //    cellbottom = dataRowbottom.CreateCell(areaIndex);
            //    cellbottom.SetCellValue(sumarea.ToString("f2"));
            //    cellbottom.CellStyle = style1;
            //}
          //  if (str_columnsname.Contains("cpr_Acount"))
          //  {
                //cellbottom = dataRowbottom.CreateCell(4);
                //cellbottom.SetCellValue(sumaccount.ToString("f4"));
                //cellbottom.CellStyle = style1;
          //  }
           // if (str_columnsname.Contains("ssze"))
           // {
                //cellbottom = dataRowbottom.CreateCell(5);
                //cellbottom.SetCellValue(sumssze.ToString("f4"));
                //cellbottom.CellStyle = style1;
           // }

                //cellbottom = dataRowbottom.CreateCell(6);
                //cellbottom.SetCellValue(sumssze.ToString("f4"));
                //cellbottom.CellStyle = style1;

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode("合同收费列表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        /// <summary>
        /// 得到数据信息
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {
            StringBuilder sb = new StringBuilder("");
            if (this.txt_keyname.Value.Trim() != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                sb.Append(" AND cpr_Name LIKE '%" + keyname + "%'  ");
            }
            //绑定单位
            if (this.drp_unit.SelectedIndex != 0)
            {
                sb.Append(" AND cpr_Unit='" + this.drp_unit.SelectedItem.Text.Trim() + "' ");
            }
            //按照年份
            if (this.drp_year.SelectedIndex != 0)
            {
                sb.AppendFormat(" AND year(cpr_SignDate)={0} ", this.drp_year.SelectedValue);
            }

            //收费时间判断
            string start = this.txt_start.Value.Trim();
            string end = this.txt_end.Value.Trim();
            string curtime = DateTime.Now.ToString("yyyy-MM-dd");

            if (start == "" && end != "")
            {
                start = curtime;
            }
            else if (start != "" && end == "")
            {
                end = curtime;
            }
            else if (start == "" && end == "")
            {
                start = curtime;
                end = curtime;
            }
            //实际收款时间
            string strWhere = string.Format(" and InAcountTime Between '{0}' AND '{1}'", start, end);
            //检查权限
            GetPreviewPowerSql(ref sb);

            TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();

            DataTable dt = bll.GetCoperationChargeExport(sb.ToString(), strWhere).Tables[0];

            return dt;
        }


    }
}