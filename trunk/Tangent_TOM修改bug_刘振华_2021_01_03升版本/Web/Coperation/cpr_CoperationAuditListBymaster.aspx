﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_CoperationAuditListBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_CoperationAuditListBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/tipsy/tipsy.css" rel="Stylesheet" type="text/css" />
    <link href="../css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="../css/CommjqGrid.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Common/AutoComplete.js"></script>
    <script src="../js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jQuery-Plugs.js"></script>
    <script type="text/javascript" src="../js/Common/SendMessageCommon.js"></script>
    <script src="/js/Coperation/cpr_CoperationAuditListView.js" type="text/javascript"></script>    
    <script type="text/javascript" src="/js/Coperation/cpr_CoperationAuditListView_jq.js"></script>
    <script type="text/javascript" src="/js/Coperation/cpr_SuperCorperationAuditList_jq.js"></script>
    <script type="text/javascript">
        $(function () {
            //初始化
            var tabs = $.trim($.cookie('tabs'));
            coperationList(tabs);
            //初始化
            //  coperation();

            //判断加载合同列表
            $("a[data-toggle='tab']").click(function () {
                var coperationtype = $(this).attr("href");
                coperationList(coperationtype);
            });

        });
       
        function coperationList(coperationtype) {

            switch (coperationtype) {
                case "#tab_1_4": //监理合同
                    SuperCoperation();
                    $("#columnsid").css("display", "none");
                    $("#columnsid_gcjl").css("display", "");

                    //初始化显示cookies字段列
                    InitColumnsCookies("columnslist_gcjlcopaudit", "jqGrid3", "columnsid_gcjl");
                    //隐藏导出
                    $("#ctl00_ContentPlaceHolder1_btn_export").hide();
                    break;
                default: //原始合同
                    coperation();
                    $("#columnsid").css("display", "");
                    $("#columnsid_gcjl").css("display", "none");

                    //初始化显示cookies字段列
                    InitColumnsCookies("columnslist_copaudit", "jqGrid", "columnsid");
                    //显示
                    $("#ctl00_ContentPlaceHolder1_btn_export").show();
                    break;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>合同评审</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>合同信息管理</a><i class="fa fa-angle-right"> </i><a>合同管理</a><i class="fa fa-angle-right"> </i><a>合同评审</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>查询评审记录
                    </div>

                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year" Width="90px" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---全部---</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>合同名称:</td>
                            <td>
                                <input type="text" class="form-control input-sm" id="txt_keyname" runat="server" /></td>
                            <td>录入时间:<input type="text" name="txt_date" id="txt_start" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" /></td>
                            <td>截止时间:<input type="text" name="txt_date" id="txt_end" onclick="WdatePicker({ readOnly: true })"
                                class="Wdate" runat="Server" style="width: 90px; height: 22px; border: 1px solid #e5e5e5;" /></td>
                            <td>
                                <input type="button" class="btn blue" value="查询" id="btn_search" /></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同评审信息列表
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn btn-sm default" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">请选择显示列
									<i class="fa fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsid">
                                <%=ColumnsContent %>
                            </div>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes pull-right" id="columnsid_gcjl" style="display: none;">
                                <%=GCJL_ColumnsContent %>
                            </div>
                        </div>
                        <asp:Button Text="导出Excel" runat="server" CssClass="btn red btn-sm"  ID="btn_export" OnClick="btn_export_Click" />
                    </div>

                </div>
                <div class="portlet-body form" style="display: block;">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1_1" data-toggle="tab">生产部门信息列表</a></li>
                        <li class=""><a href="#tab_1_4" data-toggle="tab">工程监理合同</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab_1_1" style="width: 100%">
                            <table id="jqGrid">
                            </table>
                            <div id="gridpager">
                            </div>
                            <div id="nodata" class="norecords">
                                没有符合条件数据！
                            </div>
                        </div>
                        <div class="tab-pane fade " id="tab_1_4" style="width: 100%">
                            <table id="jqGrid3">
                            </table>
                            <div id="gridpager3">
                            </div>
                            <div id="nodata3" class="norecords">
                                没有符合条件数据！
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--权限绑定-->
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
     <asp:HiddenField ID="hid_cols" runat="server" Value="" />
    <asp:HiddenField ID="hid_colsvalue" runat="server" Value="" />
    <!--HiddenArea-->
    <input type="hidden" id="HiddenUserSysNo" value="<%=UserSysNo%>" />
    <asp:HiddenField ID="hid_where" runat="server" Value="" />
    <div id="AuditUser" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
