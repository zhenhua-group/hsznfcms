﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="cpr_SuperCoperationAuditBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_SuperCoperationAuditBymaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/tipsy/tipsy.css" rel="Stylesheet" type="text/css" />
    <link href="../css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
     <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Common/AutoComplete.js"></script>
     <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script src="../js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jQuery-Plugs.js"></script>
    <script type="text/javascript" src="../js/Common/SendMessageCommon.js"></script>
    <script type="text/javascript" src="/js/MessageComm.js"></script>
    <script src="/js/Coperation/cpr_SuperCoperationAudit.js" type="text/javascript"></script>
    <style type="text/css">
        /* 表格基本样式*/
        .cls_show_cst_jiben
        {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
            font-size: 12px;
            font-family: "微软雅黑";
        }
        
        .cls_show_cst_jiben > tbody > tr > td
        {
            border: solid 1px #CCC;
            height: 50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        监理合同信息管理 <small>监理合同审批</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i><a href="cpr_SysMsgListViewBymaster.aspx">消息审批列表</a> <i class="fa fa-angle-right">
    </i><a>监理合同审批 </a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>监理合同评审记录
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">

                            <div class="table-responsive">
                                <table class="table table-bordered" style="margin-bottom:0px;">
                                    <tr>
                                        <td style="width: 100px;">
                                            建设单位:
                                        </td>
                                        <td>
                                            <asp:Label ID="ConstructionCompany" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td style="width: 100px;">
                                            承接部门:
                                        </td>
                                        <td>
                                            <asp:Label ID="UndertakeDepartment" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            项目名称:
                                        </td>
                                        <td>
                                            <asp:Label ID="ProjectName" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td>
                                            建设规模:
                                        </td>
                                        <td>
                                            <asp:Label ID="BuildScope" runat="server" Text=""></asp:Label><asp:Label ID="txt_AreaUnit" runat="Server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            建设地点:
                                        </td>
                                        <td>
                                            <asp:Label ID="BuildAddress" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td>
                                            工程负责人:
                                        </td>
                                        <td>
                                            <asp:Label ID="txt_proFuze" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            建筑类别:
                                        </td>
                                        <td>
                                            <asp:Label ID="buildType" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td>
                                            层数:
                                        </td>
                                        <td>
                                            <asp:Label ID="floorNum" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            合同额:
                                        </td>
                                        <td>
                                            <asp:Label ID="CoperationAmount" runat="server" Text=""></asp:Label>万元
                                        </td>
                                        <td>
                                            投资额:
                                        </td>
                                        <td>
                                            <asp:Label ID="Investment" runat="server" Text=""></asp:Label>万元
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            行业性质:
                                        </td>
                                        <td colspan="3">
                                             <asp:Repeater ID="RepeaterCoperationBuildNature" runat="server">
                                                <ItemTemplate>
                                            <input type="radio" name="BuildNature" value="<%#Eval("dic_Name") %>" disabled=""><%#Eval("dic_Name") %>                                            
                                                     </ItemTemplate>
                                            </asp:Repeater>
                                            <input type="hidden" id="HiddenBuildNature" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            合同分类:
                                        </td>
                                        <td colspan="3">
                                            <asp:Repeater ID="RepeaterCoperationBuildType" runat="server">
                                                <ItemTemplate>
                                                    <input type="radio" name="BuildType" value="<%#Eval("dic_Name") %>" disabled="">
                                                    <%#Eval("dic_Name") %>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <input type="hidden" id="HiddenCoperationType" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>

            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>审批信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <!--审核区域-->
                            <%=AuditHTML%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--HiddenArea-->
    <input type="hidden" id="HiddenSysMsgSysNo" value="0" />
    <input type="hidden" id="AuditRecordStatus" value="<%=coperationAuditRecordEntity.Status %>" />
    <input type="hidden" id="HiddenAuditRecordSysNo" value="<%=coperationAuditRecordEntity.SysNo %>" />
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />   
    <input type="hidden" id="HiddenCoperationSysNo" value="<%=CoperationSysNo %>" />
    <input type="hidden" id="HiddenCoperationName" value="<%=CoperationName%>" />
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <input type="hidden" id="hiddenMessageStatus" value="<%=MessageStatus %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <!--选择消息接收着-->
    <div id="AuditUserList" class="modal fade yellow" tabindex="-1" data-width="450"
        aria-hidden="true" style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">
                审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
    <div id="msgReceiverContaineredit" style="width: 400px; height: 200px; display: none;">
    </div>
    <!--承接部门层-->
    
</asp:Content>
