﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_CoperationPreviewBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_CoperationPreviewBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">
        合同信息管理 <small>合同审核预览</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right">
    </i>合同信息管理<i class="fa fa-angle-right"> </i>合同管理<i class="fa fa-angle-right"> </i>合同审核预览</li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同信息查看</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <h3 class="form-section">
                        客户信息</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td style="width: 13%">
                                            客户编号:
                                        </td>
                                        <td>
                                            <%= CustomerInfo.Cst_No %>
                                        </td>
                                        <td style="width: 15%">
                                            客户简称:
                                        </td>
                                        <td>
                                            <%=CustomerInfo.Code%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            客户名称:
                                        </td>
                                        <td>
                                            <%=CustomerInfo.Cst_Brief%>
                                        </td>
                                        <td>
                                            公司地址:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtCpy_Address" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            邮政编码:
                                        </td>
                                        <td>
                                            <%=CustomerInfo.Cpy_Address%>
                                        </td>
                                        <td>
                                            建设单位负责人:
                                        </td>
                                        <td>
                                            <%=CustomerInfo.Linkman%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            公司电话:
                                        </td>
                                        <td>
                                            <%=CustomerInfo.Cpy_Phone%>
                                        </td>
                                        <td>
                                            传真号:
                                        </td>
                                        <td>
                                            <% =CustomerInfo.Cpy_Fax%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                    <h3 class="form-section">
                        合同信息</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover" style="width: 98%;" align="center">
                                    <tr>
                                        <td>
                                            合同编号:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.cpr_No%>
                                        </td>
                                        <td>
                                            合同分类:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.cpr_Type%>
                                        </td>
                                        <td>
                                            合同类型:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.cpr_Type2%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            合同名称:
                                        </td>
                                        <td colspan="3">
                                            <%=CoperationAuditEntity.cpr_Name%>
                                        </td>
                                        <td>
                                            建筑类别:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.BuildType%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            建设单位:
                                        </td>
                                        <td colspan="3">
                                            <%=CoperationAuditEntity.BuildUnit%>
                                        </td>
                                        <td>
                                            建设规模:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.BuildArea%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            结构形式:
                                        </td>
                                        <td>
                                            <div class="struct_tree">
                                                <%=CoperationAuditEntity.StructTypeString%></div>
                                        </td>
                                        <td>
                                            建筑分类:
                                        </td>
                                        <td>
                                            <div class="struct_tree">
                                                <%=CoperationAuditEntity.BuildStructTypeString%></div>
                                        </td>
                                        <td>
                                            层数:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.FloorString%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            工程负责人:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.ChgPeople%>
                                        </td>
                                        <td>
                                            电话:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.ChgPhone%>
                                        </td>
                                        <td>
                                            承接部门:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.cpr_Unit%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            甲方负责人:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.ChgJia%>
                                        </td>
                                        <td>
                                            电话:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.ChgJiaPhone%>
                                        </td>
                                        <td>
                                            工程地点:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.BuildPosition%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            合同额:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.cpr_Acount%>
                                            万元
                                        </td>
                                        <td>
                                            投资额:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.cpr_Touzi%>
                                            万元
                                        </td>
                                        <td>
                                            行业性质:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.Industry%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            实际合同额:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.cpr_ShijiAcount%>
                                            万元
                                        </td>
                                        <td>
                                            实际投资额:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.cpr_ShijiTouzi%>
                                            万元
                                        </td>
                                        <td>
                                            工程来源:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.BuildSrc%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            合同阶段:
                                        </td>
                                        <td colspan="3">
                                            <%=CoperationAuditEntity.cpr_Process%>
                                        </td>
                                        <td>
                                            制表人:
                                        </td>
                                        <td>
                                            <%=CoperationAuditEntity.TableMaker%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            合同签订日期:
                                        </td>
                                        <td>
                                            <%= Convert.ToDateTime(CoperationAuditEntity.cpr_SignDate).ToString("yyyy-MM-dd")%>
                                        </td>
                                        <td>
                                            合同完成日期:
                                        </td>
                                        <td>
                                            <%=Convert.ToDateTime(CoperationAuditEntity.cpr_DoneDate).ToString("yyyy-MM-dd")%>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            多栋楼:
                                        </td>
                                        <td colspan="5">
                                            <%=CoperationAuditEntity.MultiBuild%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            合同备注:
                                        </td>
                                        <td colspan="5">
                                            <%=CoperationAuditEntity.cpr_Mark%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            工程子项：
                                        </td>
                                        <td colspan="5" align="left">
                                            <table class="table table-striped table-bordered table-hover" style="width: 400px;"
                                                id="datas">
                                                <tr id="sub_row">
                                                    <td style="width: 30px;" align="center" id="sub_id">
                                                        序号
                                                    </td>
                                                    <td style="width: 150px;" align="center" id="sub_name">
                                                        子项名称
                                                    </td>
                                                    <td style="width: 160px" align="center" id="sub_area">
                                                        面积（平方米）
                                                    </td>
                                                </tr>
                                                <asp:Repeater ID="RepeaterSubCoperation" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="background-color: #ffffff;">
                                                                <%#Eval("ID") %>
                                                            </td>
                                                            <td style="background-color: #ffffff;">
                                                                <%#Eval("Item_Name")%>
                                                            </td>
                                                            <td style="background-color: #ffffff;">
                                                                <%#Eval("Item_Area")%>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END FORM-->
                    <div class="form-actions fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-offset-12 col-md-12">
                                    <button type="button" class="btn default" onclick="javascript:window.history.back();">
                                        返回</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
