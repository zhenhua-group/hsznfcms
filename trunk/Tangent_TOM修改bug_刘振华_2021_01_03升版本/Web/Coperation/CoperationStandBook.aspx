﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true" CodeBehind="CoperationStandBook.aspx.cs" Inherits="TG.Web.Coperation.CoperationStandBook" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="/js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <!--JS--->
    <script src="/js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="/js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>

    <script src="/js/assets/plugins/select2/select2.min.js"></script>
    <script src="/js/assets/plugins/data-tables/jquery.dataTables.min.js"></script>
    <script src="/js/assets/plugins/data-tables/DT_bootstrap.js"></script>
    <script src="/js/Coperation/CoperationStandBook.js"></script>
    

    <style type="text/css">
        #tablecontent th {
            text-align: center !important;
            vertical-align: middle !important;
        }

        #tablecontent tbody > tr:hover {
            background-color: #e5e5e5 !important;
            font-weight: 400;
        }

        #tablecontent tbody {
            font-size: 10pt;
            font-weight: 300;
            font-family:"微软雅黑";
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>合同收费详情</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>数据统计</a><i class="fa fa-angle-right"> </i><a>合同收费详情</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>统计条件
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table-responsive">
                                    <tr>
                                        <td>生产部门：</td>
                                        <td>
                                            <cc1:ASDropDownTreeView ID="drp_unit" runat="server" BasePath="/js/astreeview/astreeview/" DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true" EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false" EnableDebugMode="false" EnableRequiredValidator="true" EnableParentNodeExpand="true" EnableTheme="true" Width="150px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true" RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconDownDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" DropdownIconUp="/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif" Font-Size="12px" Font-Strikeout="False" Height="28px" InitialDropdownText="-------全体部门-------" />
                                        </td>
                                        <td>收款时间：
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpYear" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>月:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpMonth" runat="server">
                                                <asp:ListItem Value="0" Text="全年"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                                <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                                <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <%--<td>月
                                        </td>
                                        <td>对比:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpYear2" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>年
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpMonth2" runat="server">

                                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                                <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                                <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>月
                                        </td>--%>
                                        <td>
                                            项目名称
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtProjName" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSearch" runat="server" Text="查询" CssClass="btn blue btn-default" OnClick="btnSearch_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-table"></i>统计结果
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                    <div class="actions">
                        <asp:Button ID="btnExportExcel" Text="导出Excel" CssClass="btn red btn-sm " runat="server" OnClick="btnExportExcel_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-full-width" id="tablecontent">
                            <thead>
                                <tr>
                                    <th colspan="11"><span style="font-size: 12pt; height: 30px;"><%= CountTip %></span></th>
                                </tr>
                                <tr>
                                    <th colspan="11"><span style="float: right;color:red;">(单位:万元)</span></th>
                                </tr>
                                <tr>
                                    <th nowarp width="50">序号</th>
                                    <th nowarp width="130">合同编号</th>
                                    <th nowarp width="350">项目名称</th>
                                    <th nowarp width="80">面积(㎡)</th>
                                    <th nowarp width="80">合同额</th>
                                    <th nowarp width="80">已收费</th>
                                    <th nowarp width="80">尚欠</th>
                                    <th nowarp width="250">收款日期</th>
                                    <th nowarp width="80">工程阶段</th>
                                    <th nowarp width="80">合同阶段</th>
                                    <th nowarp>备注</th>
                                </tr>
                                
                            </thead>
                            <tbody>
                                <%= HtmlTable %>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>
    </div>


    <script type="text/javascript">

        $(function() {
            TableAdvanced.init();
        });
    </script>
</asp:Content>
