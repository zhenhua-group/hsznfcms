﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;
using AjaxPro;

namespace TG.Web.Coperation
{
    public partial class cpr_SuperCoperationAuditBymaster : PageBase
    {
        #region QueryString

        protected override bool IsAuth
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// 合同系统号
        /// </summary>
        public int CoperationSysNo { get; set; }

        /// <summary>
        /// 合同审核记录系统编号
        /// </summary>
        public int CoperationAuditSysNo
        {
            get
            {
                int coperationAuditSysNo = 0;
                int.TryParse(Request["CoperationAuditSysNo"], out coperationAuditSysNo);
                return coperationAuditSysNo;
            }
        }

        /// <summary>
        /// 审核记录实体
        /// </summary>
        public TG.Model.cm_SuperCoperationAudit coperationAuditRecordEntity { get; set; }

        //消息ID
        public int MessageID
        {
            get
            {
                int megNo = 0;
                int.TryParse(Request["MsgNo"], out megNo);
                return megNo;
            }
        }
        /// <summary>
        /// 消息审批状态
        /// </summary>
        public string MessageStatus
        {
            get
            {
                return Request["MessageStatus"];
            }

        }
        #endregion

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string MessageAction
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion

        #region Property
        /// <summary>
        /// 合同基本信息实体
        /// </summary>
        public TG.Model.cm_SuperCoperation Coperation { get; set; }

        /// <summary>
        /// 合同审核状态
        /// </summary>
        public string AuditStatus { get; set; }
        /// <summary>
        /// 合同名称
        /// </summary>
        public string CoperationName { get; set; }

        /// <summary>
        /// 是否有权限
        /// </summary>
        public string HasPower { get; set; }

        public string CoperationTypeHTMLString { get; set; }

        public string AuditHTML { get; set; }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(cpr_SuperCoperationAuditBymaster));
            if (!IsPostBack)
            {
                //查询合同系统号
                coperationAuditRecordEntity = new TG.BLL.cm_SuperCoperationAudit().GetModel(CoperationAuditSysNo);
                if (coperationAuditRecordEntity != null)
                {
                    CoperationSysNo = coperationAuditRecordEntity.CoperationSysNo;
                    //加载合同信息
                    GetConperationBaseInfo();
                    //加载合同审批信息
                    GetCoperationAuditRecord();
                    CoperationName = Coperation.cpr_Name;
                }
            }
        }
        /// <summary>
        /// 得到合同基本信息
        /// </summary>
        private void GetConperationBaseInfo()
        {
            TG.BLL.cm_SuperCoperation coperationBLL = new TG.BLL.cm_SuperCoperation();
            Coperation = coperationBLL.GetFatherModel(CoperationSysNo);
            //建设单位
            ConstructionCompany.Text = Coperation.BuildUnit;
            //承接部门
            UndertakeDepartment.Text = Coperation.cpr_Unit;
            //项目名称
            ProjectName.Text = Coperation.cpr_Name;
            //建设规模
            BuildScope.Text = Convert.ToString(Coperation.BuildArea ?? 0).Trim(); ;
            this.txt_AreaUnit.Text = Coperation.AreaUnit.Trim();
            //建设地点
            BuildAddress.Text = Coperation.BuildPosition;
            //工程负责人
            this.txt_proFuze.Text = Coperation.ChgPeople.Trim();
            //投资额
            Investment.Text = Coperation.cpr_Touzi.ToString();
            //建设性质
            HiddenBuildNature.Value = Coperation.Industry;
            //合同额 
            CoperationAmount.Text = Coperation.cpr_Acount.ToString();
            //合同分类
            HiddenCoperationType.Value = Coperation.cpr_Type;
            //建筑类别
            buildType.Text = Coperation.BuildType.Trim();
            //层数
            string[] floorArray = Coperation.Floor.Split('|');
            floorNum.Text = string.Format("地上：{0}层 地下：{1}层", floorArray[0].Trim().Length == 0 ? "0" : floorArray[0], floorArray[1].Trim().Length == 0 ? "0" : floorArray[1]);
            //保密情况
            //SecretSituation.Text =;

            //行业性质
            this.RepeaterCoperationBuildNature.DataSource = TG.DBUtility.DbHelperSQL.Query("SELECT [dic_Name] FROM [dbo].[cm_Dictionary] where dic_Type=N'cpr_hyxz' OR  dic_Type=N'cpr_hyxzByJL'");
            this.RepeaterCoperationBuildNature.DataBind();
            //合同分类
            this.RepeaterCoperationBuildType.DataSource = TG.DBUtility.DbHelperSQL.Query("SELECT [dic_Name] FROM [dbo].[cm_Dictionary] where dic_Type =N'cpr_fl'");
            this.RepeaterCoperationBuildType.DataBind();
        }

        /// <summary>
        /// 查询审核实体的方法
        /// </summary>
        public void GetCoperationAuditRecord()
        {
            AuditStatus = coperationAuditRecordEntity.Status;

            //查询监理公司审核流程信息
            List<string> processDescription = new TG.BLL.cm_SuperCoperationAudit().GetAuditProcessDescription();

            string[] auditUserArray = coperationAuditRecordEntity.AuditUserArray;
            //审批时间
            string[] auditUserDate = coperationAuditRecordEntity.AuditDateArray;

            string html = "";
            html = "<table class=\"cls_show_cst_jiben\" style=\"width: 100%; font-size: 12px\">";
            if (coperationAuditRecordEntity != null && coperationAuditRecordEntity.SuggestionArray != null)
            {
                TG.BLL.tg_member uBp = new BLL.tg_member();
                int i = 0;

                if ((AuditStatus != MessageStatus) && (MessageStatus == "A" || MessageStatus == "C"))
                {
                    TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[0]));
                    string usreName = user == null ? "" : user.mem_Name;
                    string auditDate = auditUserDate[0];
                    html += JoinAuditHTML(coperationAuditRecordEntity.SuggestionArray[0], usreName, processDescription[0], auditDate);
                }
                else if ((AuditStatus != MessageStatus) && (MessageStatus == "B" || MessageStatus == "E"))
                {
                    TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[1]));
                    string usreName = user == null ? "" : user.mem_Name;
                    string auditDate = auditUserDate[1].ToString();
                    html += JoinAuditHTML(coperationAuditRecordEntity.SuggestionArray[1], usreName, processDescription[1], auditDate);
                }                
                else
                {
                    foreach (string suggestion in coperationAuditRecordEntity.SuggestionArray)
                    {
                        TG.Model.tg_member user = uBp.GetModel(int.Parse(auditUserArray[i]));
                        string usreName = user == null ? "" : user.mem_Name;
                        string auditDate = auditUserDate[i].ToString();
                        html += JoinAuditHTML(suggestion, usreName, processDescription[i], auditDate);
                        i++;
                    }

                   
                }
                if ((AuditStatus != MessageStatus) || (AuditStatus != "A" && AuditStatus != "B"))
                {
                    html += "<tr><td align=\"center\" colspan=\"3\"><input type=\"button\" id=\"Button1\" style=\"padding:5px 10px;\" name=\"controlBtn\" class=\"btn default\" value=\"返回\" onclick=\"javascript:history.back();\" /></td></tr>";

                }
            }
            if (AuditStatus == MessageStatus)
            {
                if (AuditStatus != "C" && AuditStatus != "E" && AuditStatus != "D")
                {
                    if (CheckPower())
                    {
                        html += JoinAuditHTML("", "", "", "");
                    }
                    else
                    {
                        html += "<tr><td align=\"center\" colspan=\"3\" style=\"width:100%;\">您没有权限评审该项！</td></tr>";
                        html += "<tr><td align=\"center\" colspan=\"3\"><input type=\"button\" id=\"Button1\" style=\"padding:5px 10px;\"  name=\"controlBtn\" class=\"btn default\" value=\"返回\" onclick=\"javascript:history.back();\" /></td></tr>";
                    }
                }
            }
            html += "  </table>";
            AuditHTML = html;
        }

        private string JoinAuditHTML(string suggestion, string userName, string roleName, string auditDate)
        {
            string disableFlag = "";
            string idFlag = "";
            if (!string.IsNullOrEmpty(suggestion))
            {
                disableFlag = "disabled=disabled";
            }
            else
            {
                idFlag = "suggestionTextArea";
            }

            string html = "";
            html += "<tr>";
            html += "<td style=\"width: 20%\" align=\"center\">";
            string sugesstionString = "意见";
            if (!string.IsNullOrEmpty(roleName) && !string.IsNullOrEmpty(userName))
            {
                sugesstionString = string.Format("{0} : <b>{1}</b>的意见", roleName, userName);
            }
            html += sugesstionString;
            html += "</td>";
            html += "<td style=\"width:60%\">";
            html += "&nbsp;";
            html += " <textarea style=\"height:60px;width:98%;border:solid 1px gray;\" id=\"" + idFlag + "\" " + disableFlag + ">" + suggestion + "</textarea>";
            html += "</td>";
            html += "<td style=\"width:20%;\">";
            html += auditDate;
            html += "</td>";
            html += "</tr>";
            //审批
            if (string.IsNullOrEmpty(suggestion))
            {
                html += "<tr><td align=\"center\" colspan=\"3\">";
                html += "<input type=\"button\" id=\"AgreeButton\" data-toggle= \"modal\" modal=\"#AuditUserList\" style=\"padding:5px 10px;\"  value=\"通过\" class=\"btn green\"/>&nbsp;&nbsp;";
                html += "<input type=\"button\" id=\"DisAgreeButton\" value=\"不通过\" style=\"padding:5px 10px;\" class=\"btn red\"/>&nbsp;&nbsp;";
                html += "<input type=\"button\" id=\"Button1\"  name=\"controlBtn\" class=\"btn default\" style=\"padding:5px 10px;\" value=\"返回\" onclick=\"javascript:history.back();\" />";
                html += " </td></tr>";
            }


            return html;
        }
        /// <summary>
        /// 检查权限
        /// </summary>
        /// <returns></returns>
        public bool CheckPower()
        {
            int position = 0;
            switch (AuditStatus)
            {
                case "A":
                    position = 1;
                    break;
                case "B":
                    position = 2;
                    break;
                case "D":
                    position = 2;
                    break;              
            }
            string sql = "select RoleSysNo from cm_SuperCoperationAuditConfig WHERE Position=" + position;

            object obj = TG.DBUtility.DbHelperSQL.GetSingle(sql);         
          
            int roleSysNo = obj != null ? (int)obj : 0;

            bool hasPower = new TG.BLL.cm_Role().CheckPower(roleSysNo, UserSysNo);

            return hasPower;
        }
    }
}