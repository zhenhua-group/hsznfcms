﻿using Aspose.Words;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.Coperation
{
    public partial class cpr_ShowMeasureCoperationBymaster : System.Web.UI.Page
    {
        //返回标示
        public string BackFlag
        {
            get
            {
                return Request["flag"].ToString();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //合同ID
            string str_cprid = Request.QueryString["cprid"] ?? "";
            string str_flag = Request.QueryString["flag"] ?? "";
            if (!IsPostBack)
            {
                this.hid_cprid.Value = str_cprid;
                this.hid_flag.Value = str_flag;
                //显示联系人
                ShowContractInfo(str_cprid);
                //显示合同信息
                ShowCoperation(str_cprid);
            }
            else
            {
                OutputLocation();
            }
        }

        private void OutputLocation()
        {
            TG.BLL.cm_TestCoperation bllcop = new TG.BLL.cm_TestCoperation();
            string cprid = Request.QueryString["cprid"];
            TG.Model.cm_TestCoperation model = bllcop.GetModel(Convert.ToInt32(cprid));
            int cstid = this.GetCstId(Convert.ToInt32(cprid));
            TG.Model.cm_CustomerInfo conmodel = this.GetConst(cstid);

            string tmppath = Server.MapPath("~/TemplateWord/MeasureCoprationReport.doc");
            Document doc = new Document(tmppath);
            //载入模板

            //建设地点
            if (doc.Range.Bookmarks["BuildPosition"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildPosition"];
                mark.Text = model.BuildPosition == null ? "" : model.BuildPosition;
            }
            //工程来源
            if (doc.Range.Bookmarks["BuildSrc"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildSrc"];
                mark.Text = model.BuildSrc == null ? "" : model.BuildSrc;
            }
            //建筑类型
            if (doc.Range.Bookmarks["BuildType"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildType"];
                mark.Text = model.BuildType == null ? "" : model.BuildType;
            }
            //建设单位
            if (doc.Range.Bookmarks["BuildUnit"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BuildUnit"];
                mark.Text = model.BuildUnit == null ? "" : model.BuildUnit;
            }
            //甲方
            if (doc.Range.Bookmarks["ChgJia"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgJia"];
                mark.Text = model.ChgJia == null ? "" : model.ChgJia;
            }
            if (doc.Range.Bookmarks["ChgJiaPhone"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgJiaPhone"];
                mark.Text = model.ChgJiaPhone == null ? "" : model.ChgJiaPhone;
            }
            //项目负责人
            if (doc.Range.Bookmarks["ChgPeople"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgPeople"];
                mark.Text = model.ChgPeople == null ? "" : model.ChgPeople;
            }
            if (doc.Range.Bookmarks["ChgPhone"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ChgPhone"];
                mark.Text = model.ChgPhone == null ? "" : model.ChgPhone;
            }
            //合同额
            if (doc.Range.Bookmarks["cpr_Acount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Acount"];
                mark.Text = model.cpr_Acount.ToString() == "0.00" ? "" : model.cpr_Acount.ToString() + "万元";
            }

            //备注
            if (doc.Range.Bookmarks["cpr_Mark"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Mark"];
                mark.Text = model.cpr_Mark == null ? "" : model.cpr_Mark;
            }
            //项目名称
            if (doc.Range.Bookmarks["cpr_Name"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Name"];
                mark.Text = model.cpr_Name == null ? "" : model.cpr_Name;
            }
            //编号
            if (doc.Range.Bookmarks["cpr_No"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_No"];
                mark.Text = model.cpr_No == null ? "" : model.cpr_No;
            }
            //实际收款
            if (doc.Range.Bookmarks["cpr_ShijiAcount"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_ShijiAcount"];
                mark.Text = model.cpr_ShijiAcount.ToString() == "0.00" ? "" : model.cpr_ShijiAcount.ToString() + "万元";
            }

            if (doc.Range.Bookmarks["cpr_SignDate"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_SignDate"];
                mark.Text = model.cpr_SignDate2 == null ? "" : Convert.ToDateTime(model.cpr_SignDate2).ToString("yyyy-MM-dd");
            }

            if (doc.Range.Bookmarks["cpr_SignDate2"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_SignDate2"];
                mark.Text = Convert.ToDateTime(model.cpr_SignDate.ToString()).ToString("yyyy-MM-dd");
            }
            //完成日期
            if (doc.Range.Bookmarks["cpr_DoneDate"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_DoneDate"];
                mark.Text = Convert.ToDateTime(model.cpr_DoneDate.ToString()).ToString("yyyy-MM-dd");
            }
            //类型
            if (doc.Range.Bookmarks["cpr_Type"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Type"];
                mark.Text = model.cpr_Type == null ? "" : model.cpr_Type;
            }
            if (doc.Range.Bookmarks["cpr_Type2"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Type2"];
                mark.Text = model.cpr_Type2 == null ? "" : model.cpr_Type2;
            }
            //承接部门
            if (doc.Range.Bookmarks["cpr_Unit"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["cpr_Unit"];
                mark.Text = model.cpr_Unit == null ? "" : model.cpr_Unit;
            }
            if (doc.Range.Bookmarks["TableMaker"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["TableMaker"];
                mark.Text = model.TableMaker == null ? "" : model.TableMaker;
            }
            if (doc.Range.Bookmarks["Floor"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Floor"];
                mark.Text = GetFloor(model.Floor);
            }
            if (doc.Range.Bookmarks["Industry"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["Industry"];
                mark.Text = model.Industry == null ? "" : model.Industry;
            }
            if (doc.Range.Bookmarks["MultiBuild"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["MultiBuild"];
                mark.Text = model.MultiBuild == null ? "" : model.MultiBuild;
            }

            //静载试验点数
            if (doc.Range.Bookmarks["StaticPoint"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["StaticPoint"];
                mark.Text = model.StaticPoint == null ? "" : model.StaticPoint.ToString();
            }
            //最大加载量
            if (doc.Range.Bookmarks["StaticMax"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["StaticMax"];
                mark.Text = model.StaticMax == null ? "" : model.StaticMax.ToString();
            }
            //堆载试验点数
            if (doc.Range.Bookmarks["LoadingPoint"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["LoadingPoint"];
                mark.Text = model.LoadingPoint == null ? "" : model.LoadingPoint.ToString();
            }
            //堆载最大加载量
            if (doc.Range.Bookmarks["LoadingMax"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["LoadingMax"];
                mark.Text = model.LoadingMax == null ? "" : model.LoadingMax.ToString();
            }

            //大应变检测点数
            if (doc.Range.Bookmarks["BigPoint"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["BigPoint"];
                mark.Text = model.BigPoint == null ? "" : model.BigPoint.ToString();
            }
            //小应变检测点数
            if (doc.Range.Bookmarks["SmallPoint"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["SmallPoint"];
                mark.Text = model.SmallPoint == null ? "" : model.SmallPoint.ToString();
            }
            //取样探井数量
            if (doc.Range.Bookmarks["SampleNumber"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["SampleNumber"];
                mark.Text = model.SampleNumber == null ? "" : model.SampleNumber.ToString();
            }

            //其他检测方法
            if (doc.Range.Bookmarks["OtherMethod"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["OtherMethod"];
                mark.Text = model.OtherMethod == null ? "" : model.OtherMethod;
            }
            //工期
            if (doc.Range.Bookmarks["ProjectDate"] != null)
            {
                Bookmark mark = doc.Range.Bookmarks["ProjectDate"];
                mark.Text = model.ProjectDate == null ? "" : model.ProjectDate.ToString() + "天";
            }
            //chargeplan  收费计划
            if (doc.Range.Bookmarks["chargeplan"] != null)
            {
                string sqlcharge = @"SELECT * From cm_CoperationChargeType Where paytype='testcharge' AND cpr_ID=" + Convert.ToInt32(cprid);
                StringBuilder builder = new StringBuilder();
                DataTable dt = TG.DBUtility.DbHelperSQL.Query(sqlcharge).Tables[0];
                if (dt.Rows.Count > 0)
                {
                    int index = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        index++;
                        builder.Append(index + "、  ");
                        builder.Append(dr["Times"] + "   ");
                        builder.Append("百分之");
                        builder.Append(dr["persent"] + "   ");
                        builder.Append("金额");
                        builder.Append(dr["payCount"] + "万元   ");
                        builder.Append("收款时间是");
                        builder.Append(Convert.ToDateTime(dr["paytime"]).ToString("yyyy-MM-dd") + "。        ");
                    }
                }
                Bookmark mark = doc.Range.Bookmarks["chargeplan"];
                mark.Text = builder.ToString();
            }

            doc.Save(model.cpr_Name + "-岩土工程检测合同.doc", SaveFormat.Doc, SaveType.OpenInWord, Response);  //保存为doc，并打开
        }

        //根据合同id获得用户id。
        public int GetCstId(int cpr_Id)
        {
            string sql = "select  cst_Id from cm_DesignCoperation where cpr_Id=" + cpr_Id;
            return Convert.ToInt32(TG.DBUtility.DbHelperSQL.GetSingle(sql));
        }
        //根据用户id获得用户model
        public TG.Model.cm_CustomerInfo GetConst(int cstid)
        {
            TG.BLL.cm_CustomerInfo dalcust = new TG.BLL.cm_CustomerInfo();
            return dalcust.GetModel(cstid);
        }
        //显示联系人
        protected void ShowContractInfo(string cprid)
        {
            string strSql = " Select cst_Id From [cm_TestCoperation] Where cpr_Id=" + cprid;
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            DataSet ds_cst = bll_db.GetList(strSql);
            if (ds_cst.Tables.Count > 0)
            {
                if (ds_cst.Tables[0].Rows.Count > 0)
                {
                    string str_cstid = ds_cst.Tables[0].Rows[0][0].ToString();
                    TG.BLL.cm_CustomerInfo bll_cst = new TG.BLL.cm_CustomerInfo();
                    string str_where = " Cst_ID=" + str_cstid;
                    TG.Model.cm_CustomerInfo model_cst = bll_cst.GetModel(int.Parse(str_cstid));
                    if (model_cst != null)
                    {
                        this.txtCst_No.Text = model_cst.Cst_No;
                        this.txtCode.Text = model_cst.Code;
                        this.txtCst_Name.Text = model_cst.Cst_Name;
                        this.txtLinkman.Text = model_cst.Linkman;
                        this.txtCpy_Address.Text = model_cst.Cpy_Address;
                        this.txtCpy_Phone.Text = model_cst.Cpy_Phone;
                        this.txt_JC.Text = model_cst.Cst_Brief;
                        this.txt_Fax.Text = model_cst.Cpy_Fax;
                    }
                }
            }
        }
        //显示合同信息
        protected void ShowCoperation(string cprid)
        {
            TG.BLL.cm_TestCoperation bll_cpr = new TG.BLL.cm_TestCoperation();
            TG.Model.cm_TestCoperation model_cpr = bll_cpr.GetModel(int.Parse(cprid));
            if (model_cpr != null)
            {
                //赋值
                this.txtcpr_No.Text = model_cpr.cpr_No ?? "";
                this.ddcpr_Type.Text = model_cpr.cpr_Type.Trim();
                this.txt_cprType.Text = Convert.ToString(model_cpr.cpr_Type2 ?? "").Trim();
                //合同名称
                this.txt_cprName.Text = model_cpr.cpr_Name.Trim();
                //建设单位
                this.txt_cprBuildUnit.Text = model_cpr.BuildUnit == null ? "" : model_cpr.BuildUnit.Trim();
                //合同额
                this.txtcpr_Account.Text = model_cpr.cpr_Acount.ToString();
                //实际合同额
                this.txtcpr_Account0.Text = model_cpr.cpr_ShijiAcount.ToString();

                //签订日期
                this.txtSingnDate.Text = Convert.ToDateTime(model_cpr.cpr_SignDate).ToShortDateString();
                this.txtSingnDate2.Text = Convert.ToDateTime(model_cpr.cpr_SignDate2).ToShortDateString();
                //完成日期
                this.txtCompleteDate.Text = model_cpr.cpr_DoneDate == null ? "" : Convert.ToDateTime(model_cpr.cpr_DoneDate).ToShortDateString();

                //负责人
                this.txt_proFuze.Text = model_cpr.ChgPeople.Trim();
                this.txt_fzphone.Text = model_cpr.ChgPhone.Trim();
                //甲方负责人
                this.txtFParty.Text = model_cpr.ChgJia.Trim();
                this.txt_jiafphone.Text = model_cpr.ChgJiaPhone.Trim();
                if (!string.IsNullOrEmpty(model_cpr.BuildPosition))
                {
                    if (model_cpr.BuildPosition == "-1")
                    {
                        this.ddProjectPosition.Text = "";
                    }
                    else
                    {
                        this.ddProjectPosition.Text = model_cpr.BuildPosition.Trim();
                    }
                }
                if (model_cpr.Industry.Trim() == "-1")
                {
                    this.ddProfessionType.Text = "";
                }
                else
                {
                    this.ddProfessionType.Text = model_cpr.Industry.Trim();
                }
                if (model_cpr.BuildSrc.Trim() == "-1")
                {
                    this.ddSourceWay.Text = "";
                }
                else
                {
                    this.ddSourceWay.Text = model_cpr.BuildSrc.Trim();
                }
                //承接部门
                this.txt_cjbm.Text = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit;
                this.txt_tbcreate.Text = model_cpr.TableMaker == null ? "" : model_cpr.TableMaker.Trim();
                //建筑类型
                this.drp_buildtype.Text = model_cpr.BuildType == null ? "" : model_cpr.BuildType.Trim();
                //层数
                string[] floors = model_cpr.Floor.Split(new char[] { '|' }, StringSplitOptions.None);
                this.lbl_upfloor.Text = floors[0].ToString();
                this.lbl_downfloor.Text = floors[1].ToString();
                //多栋楼
                this.txt_multibuild.Text = model_cpr.MultiBuild;
                //备注
                this.txtcpr_Remark.Text = model_cpr.cpr_Mark == null ? "" : model_cpr.cpr_Mark.Trim();
                //工期
                txt_ProjectDate.Text = model_cpr.ProjectDate.ToString();

                //静载试验点数
                this.txt_StaticPoint.Text = model_cpr.StaticPoint.ToString();
                //最大加载量
                this.txt_StaticMax.Text = model_cpr.StaticMax.ToString();
                //堆载试验点数
                this.txt_LoadingPoint.Text = model_cpr.LoadingPoint.ToString();
                //堆载最大加载量
                this.txt_LoadingMax.Text = model_cpr.LoadingMax.ToString();
                //大应变检测点数
                this.txt_BigPoint.Text = model_cpr.BigPoint.ToString();
                //小应变检测点数
                this.txt_SmallPoint.Text = model_cpr.SmallPoint.ToString();
                //取样探井数量
                this.txt_SampleNumber.Text = model_cpr.SampleNumber.ToString();
                //其他检测方法
                this.txt_OtherMethod.Text = model_cpr.OtherMethod;
            }

        }
        //  转换地上和地下楼层数
        protected string GetFloor(string floor)
        {
            string[] floors = floor.Split('|');
            if (floors.Length > 0)
            {
                return "地上:" + floors[0] + "地下:" + floors[1];
            }
            else
            {
                return "";
            }
        }
    }
}