﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.BLL;
using TG.Model;

namespace TG.Web.Coperation
{
    public partial class cpr_DesignCoperationBymaster : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //绑定合同类别
                BindCorpType();
                //行业性质
                BindCorpHyxz();
                //工程来源
                BindCorpSrc();
                //绑定设计等级
                BindBuildType();

                SetUserSysNoAndRole();
                BindPreviewPower();
            }
            else
            {
                Save();
            }
        }

        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;
                ChooseCustomer1.UserSysNo = base.UserSysNo;
                ChooseCustomer1.PreviewPower = base.RolePowerParameterEntity.PreviewPattern;

                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }

        /// <summary>
        /// 保存
        /// </summary>
        protected void Save()
        {
            //添加合同
            int i_cprid = AddCoperation();
            if (i_cprid > 0)
            {
                //添加计划收费信息
                AddChargeItem(i_cprid.ToString());

                //添加附件
                AddCoperationAttach(i_cprid.ToString());
                //弹出并返回到合同列表
                TG.Common.MessageBox.ShowAndRedirect(this, "岩土工程合同添加成功！", "cpr_CorperationListBymaster.aspx");
            }
        }
        //添加合同信息
        public int AddCoperation()
        {
            TG.BLL.cm_DesignCoperation bll_cpr = new TG.BLL.cm_DesignCoperation();
            TG.BLL.cm_Coperation bll_cop = new TG.BLL.cm_Coperation();
            TG.Model.cm_DesignCoperation model_cpr = new TG.Model.cm_DesignCoperation();
            TG.Model.cm_Coperation model_cop = new TG.Model.cm_Coperation();
            //赋值
            model_cpr.cst_Id = int.Parse(this.hid_cstid.Value);
            model_cop.cst_Id = int.Parse(this.hid_cstid.Value);
            model_cpr.cpr_No = this.txtcpr_No.Value;
            model_cop.cpr_No = this.txtcpr_No.Value;
            if (this.hid_cprno.Value != "")
            {
                model_cpr.cpr_No = this.hid_cprno.Value;
                model_cop.cpr_No = this.hid_cprno.Value;
            }
            //合同分类
            model_cpr.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            model_cop.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            //合同类型
            model_cpr.cpr_Type2 = this.txt_cprType.Value;
            model_cop.cpr_Type2 = this.txt_cprType.Value;
            //合同名称
            model_cpr.cpr_Name = this.txt_cprName.Value;
            model_cop.cpr_Name = this.txt_cprName.Value;
            //建设单位
            model_cpr.BuildUnit = this.txt_cprBuildUnit.Value;
            model_cop.BuildUnit = this.txt_cprBuildUnit.Value;
            //建筑类型
            model_cpr.BuildType = this.drp_buildtype.SelectedItem.Text.Trim();
            model_cop.BuildType = this.drp_buildtype.SelectedItem.Text.Trim();
            //建筑层数
            model_cpr.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            model_cop.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            //工程负责人
            model_cpr.ChgPeople = this.txt_proFuze.Value;
            model_cpr.ChgPhone = this.txt_fzphone.Value;
            model_cop.ChgPeople = this.txt_proFuze.Value;
            model_cop.ChgPhone = this.txt_fzphone.Value;
            //甲方负责人
            model_cpr.ChgJia = this.txtFParty.Value;
            model_cpr.ChgJiaPhone = this.txt_jiafphone.Value;
            model_cop.ChgJia = this.txtFParty.Value;
            model_cop.ChgJiaPhone = this.txt_jiafphone.Value;
            //承接部门
            if (this.txt_cjbm.Value.Trim() != "")
            {
                model_cpr.cpr_Unit = this.hid_cjbm.Value;
                model_cop.cpr_Unit = this.hid_cjbm.Value;
            }
            //工程地点
            if (this.txt_ProjectPosition.Value.Trim() != "")
            {
                model_cpr.BuildPosition = this.txt_ProjectPosition.Value;
                model_cop.BuildPosition = this.txt_ProjectPosition.Value;
            }
            else
            {
                model_cpr.BuildPosition = "";
                model_cop.BuildPosition = "";
            }

            //合同额
            if (this.txtcpr_Account.Value.Trim() == "")
            {
                model_cpr.cpr_Acount = 0;
                model_cop.cpr_Acount = 0;
            }
            else
            {
                model_cpr.cpr_Acount = Convert.ToDecimal(this.txtcpr_Account.Value);
                model_cop.cpr_Acount = Convert.ToDecimal(this.txtcpr_Account.Value);
            }
            //实际合同额
            if (this.txtcpr_Account0.Value.Trim() == "")
            {
                model_cpr.cpr_ShijiAcount = 0;
                model_cop.cpr_ShijiAcount = 0;
            }
            else
            {
                model_cpr.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);
                model_cop.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);
            }

            //行业性质
            if (this.ddProfessionType.SelectedIndex != 0)
            {
                model_cpr.Industry = this.ddProfessionType.SelectedItem.Text;
                model_cop.Industry = this.ddProfessionType.SelectedItem.Text;
            }
            else
            {
                model_cpr.Industry = this.ddProfessionType.Items[0].Value;
                model_cop.Industry = this.ddProfessionType.Items[0].Value;
            }
            //工程来源
            if (this.ddSourceWay.SelectedIndex != 0)
            {
                model_cpr.BuildSrc = this.ddSourceWay.SelectedItem.Text;
                model_cop.BuildSrc = this.ddSourceWay.SelectedItem.Text;
            }
            else
            {
                model_cpr.BuildSrc = this.ddSourceWay.Items[0].Value;
                model_cop.BuildSrc = this.ddSourceWay.Items[0].Value;
            }
            //制表人
            model_cpr.TableMaker = this.txt_tbcreate.Value;
            model_cop.TableMaker = this.txt_tbcreate.Value;

            //添加
            int affectRow = 0;

            //地下层数
            model_cpr.DugoutNumber = this.txt_DugoutNumber.Value == "" ? 0 : int.Parse(this.txt_DugoutNumber.Value);

            //基坑深度
            model_cpr.PitHeight = this.txt_PitHeight.Value == "" ? 0 : decimal.Parse(txt_PitHeight.Value);
            //占地面积
            model_cpr.BuildArea = this.txt_BuildArea.Value == "" ? 0 : decimal.Parse(txt_BuildArea.Value);
            //基坑安全等级txt_SafeClass
            model_cpr.SafeClass = txt_SafeClass.Value;
            //基础类型
            model_cpr.BaseType = txt_BaseType.Value;
            //地基处理方式
            model_cpr.GroundHandle = txt_GroundHandle.Value;
            //统计年份
            model_cpr.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            model_cop.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            //开始日期 
            model_cpr.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value);
            model_cop.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value);
            //完成日期
            model_cpr.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            model_cop.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            //工期
            model_cpr.ProjectDate = txt_ProjectDate.Value == "" ? 0 : int.Parse(txt_ProjectDate.Value);
            //备注
            model_cpr.cpr_Mark = this.txtcpr_Remark.Value;
            model_cop.cpr_Mark = this.txtcpr_Remark.Value;
            //多栋楼备注
            model_cpr.MultiBuild = this.txt_MultiBuild.Value;
            model_cop.MultiBuild = this.txt_MultiBuild.Value;
            model_cpr.RegTime = DateTime.Now;
            model_cpr.UpdateBy = UserSysNo.ToString();
            model_cpr.LastUpdate = DateTime.Now;
            model_cop.RegTime = DateTime.Now;
            model_cop.UpdateBy = UserSysNo.ToString();
            model_cop.LastUpdate = DateTime.Now;
            //录入人
            model_cpr.InsertUserID = UserSysNo;
            model_cop.InsertUserID = UserSysNo;
            //项目经理人ID  qpl  20131225
            model_cpr.PMUserID = int.Parse(this.HiddenPMUserID.Value);
            model_cop.PMUserID = int.Parse(this.HiddenPMUserID.Value);
            model_cpr.InsertDate = DateTime.Now;
            //model_cop.InsertDate = DateTime.Now;
            //保存合同信息
            try
            {
                int fid = bll_cop.Add(model_cop);
                model_cpr.cpr_FID = fid;
                affectRow = bll_cpr.Add(model_cpr);

            }
            catch (System.Exception ex)
            { }

            return affectRow;
        }
        //更新收费信息
        protected void AddChargeItem(string cprid)
        {
            //更新计划收费信息  qpl  20140114
            string strSql = " UPDATE [cm_CoperationChargeType] SET [cpr_Id]=" + cprid + " WHERE [cpr_Id]=" + this.hid_cprid.Value + " AND acceptuser='" + UserShortName + "' AND paytype='designcharge'";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            bll_db.ExcuteBySql(strSql);
        }

        //添加附件
        protected void AddCoperationAttach(string cprid)
        {
            //更新计划收费信息  qpl  20140114
            string strSql = " UPDATE [cm_AttachInfo] SET [Cpr_Id] =" + cprid + " WHERE [Temp_No] = '" + this.hid_cprid.Value + "' AND UploadUser='" + UserSysNo + "' AND OwnType='designtype'";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            bll_db.ExcuteBySql(strSql);
        }

        /// <summary>
        /// 获取客户ID
        /// </summary>
        /// <returns></returns>
        public string getCst_Id()
        {
            DateTime dt = DateTime.Now;
            string tempid = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString();
            return tempid;
        }
        //返回一个文件上传的随机ID
        public string GetCoperationID()
        {
            DateTime dt = DateTime.Now;
            string tempid = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString();
            return tempid;
        }
        /// <summary>
        /// 获取登录ID(登录界面开始进行重新设置.....)
        /// </summary>
        /// <returns></returns>
        public string GetUserFlag()
        {
            return UserSysNo.ToString();
        }
        /// <summary>
        /// 返回文件夹ID (若是根据登录ID,此项可不要？待商讨中.......)
        /// </summary>
        /// <returns></returns>
        public string GetParentID()
        {
            return UserShortName;
        }
        //设置选择合同额方法
        public void SetUserSysNoAndRole()
        {
            //用户ID
            this.ChooseCustomer1.UserSysNo = int.Parse(GetCurMemID());
            //权限
            this.ChooseCustomer1.PreviewPower = GetPreviewPower();
        }
        //获取页面权限  by long 20130510
        public int GetPreviewPower()
        {
            int UserSysNo = int.Parse(GetCurMemID());
            string PageName = "cpr_CorperationListBymaster.aspx";
            List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, PageName);

            //浏览权限
            int Power = 0;
            if ((from role in rolePowerParameterList where role.PreviewPattern == "2" select role).Count() > 0)
            {
                //部门
                Power = 2;
            }
            if ((from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0)
            {
                //全部
                Power = 1;
            }
            return Power;
        }

        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.ddcpr_Type.DataSource = bll_dic.GetList(str_where);
            this.ddcpr_Type.DataTextField = "dic_Name";
            this.ddcpr_Type.DataValueField = "ID";
            this.ddcpr_Type.DataBind();

        }

        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz'";
            this.ddProfessionType.DataSource = bll_dic.GetList(str_where);
            this.ddProfessionType.DataTextField = "dic_Name";
            this.ddProfessionType.DataValueField = "ID";
            this.ddProfessionType.DataBind();
        }

        //建筑类型
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_kcbuildtype'";
            this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "ID";
            this.drp_buildtype.DataBind();
        }
        //工程来源
        protected void BindCorpSrc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_src'";
            this.ddSourceWay.DataSource = bll_dic.GetList(str_where);
            this.ddSourceWay.DataTextField = "dic_Name";
            this.ddSourceWay.DataValueField = "ID";
            this.ddSourceWay.DataBind();
        }

    }
}


