﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;

namespace TG.Web.Coperation
{
    public partial class ChargeContrastMonth : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindYear();
                SelectedCurYear();
                LoadUnitTree();
                SelectedCurUnit();
                //统计数据
                GetAllData();
            }
        }

        #region 基本设置 2015年6月9日
        /// <summary>
        /// 统计语言
        /// </summary>
        public string CountTip
        {
            get;
            set;
        }
        /// <summary>
        /// 当前年
        /// </summary>
        public string CurYear { get; set; }
        public string CurMonth { get; set; }
        /// <summary>
        /// 前一年
        /// </summary>
        public string BeforeYear { get; set; }
        public string BeforeMonth { get; set; }
        /// <summary>
        /// 表格数据
        /// </summary>
        public string HtmlTable { get; set; }
        /// <summary>
        /// 绑定多选年
        /// </summary>
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();

            if (list.Count > 0)
            {
                foreach (string str in list)
                {
                    //加载年
                    drpYear.Items.Add(new ListItem(str, str));
                    drpYear2.Items.Add(new ListItem(str, str));
                }
            }
        }
        /// <summary>
        /// 选中当前年
        /// </summary>
        protected void SelectedCurYear()
        {
            int curyear = DateTime.Now.Year;
            string curmonth = DateTime.Now.Month.ToString();
            //当前年
            if (drpYear2.Items.FindByValue(curyear.ToString()) != null)
            {
                drpYear2.Items.FindByValue(curyear.ToString()).Selected = true;
            }

            //前一年
            string beforeyear = (curyear - 1).ToString();
            if (drpYear.Items.FindByValue(beforeyear) != null)
            {
                drpYear.Items.FindByValue(beforeyear).Selected = true;
            }
            else
            {
                var select = drpYear.Items.FindByValue(curyear.ToString());
                if (select!=null)drpYear.Items.FindByValue(curyear.ToString()).Selected = true;
            }

            //当前月
            drpMonth.Items.FindByValue(curmonth).Selected = true;
            drpMonth2.Items.FindByValue(curmonth).Selected = true;
        }

        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { "0" };
            this.drp_unit.CheckNodes(curUnit, true);
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
        protected DataTable GetUnit()
        {
            string strWhere = " 1=1 ";

            if (base.RolePowerParameterEntity != null)
            {

                //个人
                if (base.RolePowerParameterEntity.PreviewPattern == 0)
                {
                    strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
                }
                else if (base.RolePowerParameterEntity.PreviewPattern == 2)
                {
                    strWhere = " unit_ID =" + UserUnitNo;
                }
                else
                {
                    strWhere = " 1=1 ";
                }
            }
            else
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }

        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
            else
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                Model.tg_unit unit = new TG.BLL.tg_unit().GetModel(UserUnitNo);
                if (unit != null)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(unit.unit_Name.Trim(), unit.unit_ID.ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }

            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drp_unit.Theme = macOS;
        }
        /// <summary>
        /// 选中当前年
        /// </summary>
        protected void GetSelectYear()
        {
            this.BeforeYear = this.drpYear.SelectedValue;
            this.BeforeMonth = this.drpMonth.SelectedValue;
            this.CurYear = this.drpYear2.SelectedValue;
            this.CurMonth = this.drpMonth2.SelectedValue;
        }
        #endregion

        #region 统计表


        /// <summary>
        /// 统计部门数据
        /// </summary>
        protected void GetCountDataByUnit()
        {
            StringBuilder sbTable = new StringBuilder();

            List<ASTreeViewNode> checkedNods = this.drp_unit.GetCheckedNodes(false);

            //全没选
            if (checkedNods.Count == 0)
            {
                //循环部门
                DataTable dt = GetUnit();
                //部门名称

                foreach (DataRow dr in dt.Rows)
                {
                    string unitname = dr["unit_Name"].ToString();
                    sbTable.Append(CreateTableRowByUnit(unitname));
                }
            }
            else
            {
                foreach (ASTreeViewNode node in checkedNods)
                {
                    if (node.NodeValue == "0")
                        continue;
                    //部门名称
                    string unitname = node.NodeText.Trim();
                    sbTable.Append(CreateTableRowByUnit(unitname));
                }
            }


            HtmlTable = sbTable.ToString();

            //保存导出列表对象
            ViewState["ExcelList"] = this.ImportExcelList;
        }
        /// <summary>
        /// 创建统计行
        /// </summary>
        /// <param name="unitname"></param>
        /// <returns></returns>
        private string CreateTableRowByUnit(string unitname)
        {
            StringBuilder sbTable = new StringBuilder();

            DataTable dtData = GetCountData(unitname, BeforeYear, CurYear, BeforeMonth, CurMonth);
            DataRow drData = dtData.Rows[0];
            sbTable.Append("<tr>");
            sbTable.AppendFormat("<td>{0}</td>", unitname);//生产部门
            sbTable.AppendFormat("<td>{0}</td>", drData["TargetAllot"].ToString());//全年目标
            sbTable.AppendFormat("<td>{0}</td>", drData["AcountMonth"].ToString());//本月
            //sbTable.AppendFormat("<td>{0}</td>", drData["AcountYear"].ToString());
            sbTable.AppendFormat("<td>{0}</td>", drData["CompletePersent"].ToString() + "%");//完成百分比
            sbTable.AppendFormat("<td>{0}</td>", drData["TargetAllot2"].ToString());//全年目标
            sbTable.AppendFormat("<td>{0}</td>", drData["AcountMonth2"].ToString());//本月
            string compareMonth = GetCompareValue(drData["AcountMonth2"].ToString(), drData["AcountMonth"].ToString());
            sbTable.AppendFormat("<td>{0}</td>", compareMonth);//月同比
            //excel
            compareMonth = GetCompareValueExcel(drData["AcountMonth2"].ToString(), drData["AcountMonth"].ToString());
            //sbTable.AppendFormat("<td>{0}</td>", drData["AcountYear2"].ToString());
            //sbTable.AppendFormat("<td>{0}</td>",GetCompareValue(drData["AcountYear2"].ToString(), drData["AcountYear"].ToString()));
            sbTable.AppendFormat("<td>{0}</td>", drData["CompletePersent2"].ToString() + "%");
            string compareAll = GetCompareValue(drData["CompletePersent2"].ToString(), drData["CompletePersent"].ToString());
            sbTable.AppendFormat("<td>{0}</td>", compareAll);
            //excel
            compareAll = GetCompareValueExcel(drData["CompletePersent2"].ToString(), drData["CompletePersent"].ToString());
            sbTable.Append("</tr>");

            //创建对象列表
            TG.Model.ChargeContrastMonthExcel excel = new TG.Model.ChargeContrastMonthExcel()
            {
                UnitID = drData["unit_ID"].ToString(),
                UnitName = unitname,
                BeforeTarget = drData["TargetAllot"].ToString(),
                BeforeCurMonth = drData["AcountMonth"].ToString(),
                CompleteBeforeVal = drData["CompletePersent"].ToString() + "%",
                CurrentTarget = drData["TargetAllot2"].ToString(),
                CurrentMonth = drData["AcountMonth2"].ToString(),
                CompareMonthVal = compareMonth,
                CompleteCurrentVal = drData["CompletePersent2"].ToString() + "%",
                CompareAllVal = compareAll
            };

            this.ImportExcelList.Add(excel);

            return sbTable.ToString();
        }

        /// <summary>
        /// 获取同比值
        /// </summary>
        /// <param name="curValue"></param>
        /// <param name="beforeValue"></param>
        /// <returns></returns>
        public string GetCompareValue(string curValue, string beforeValue)
        {
            //本年度值
            decimal curCount = Convert.ToDecimal(curValue);
            //上年度值
            decimal beforeCount = Convert.ToDecimal(beforeValue);

            string result = "";
            if (beforeCount != 0)
            {
                //同比对比表
                decimal growPersent = ((curCount - beforeCount) / beforeCount) * 100;
                if (growPersent > 0)
                {
                    result = string.Format("<span style=\"color:red;font-weight:500;\">{0}%</span>", growPersent.ToString("f2"));
                }
                else if (growPersent < 0)
                {
                    result = string.Format("<span style=\"color:green;font-weight:500;\">{0}%</span>", growPersent.ToString("f2"));
                }
                else
                {
                    result = string.Format("<span style=\"font-weight:500;\">{0}%</span>", growPersent.ToString("f2"));
                }
            }
            else
            {
                result = "------";
            }

            return result;
        }

        /// <summary>
        /// 获取同比值
        /// </summary>
        /// <param name="curValue"></param>
        /// <param name="beforeValue"></param>
        /// <returns></returns>
        public string GetCompareValueExcel(string curValue, string beforeValue)
        {
            //本年度值
            decimal curCount = Convert.ToDecimal(curValue);
            //上年度值
            decimal beforeCount = Convert.ToDecimal(beforeValue);
            //增长率
            //返回结果
            string result = "";
            if (beforeCount != 0)
            {
                //同比增长
                decimal growPersent = ((curCount - beforeCount) / beforeCount) * 100;

                if (growPersent > 0)
                {
                    result = string.Format("{0}%",
                        growPersent.ToString("f2"));
                }
                else if (growPersent < 0)
                {
                    result = string.Format("{0}%",
                        growPersent.ToString("f2"));
                }
                else
                {
                    result = string.Format("{0}%",
                        growPersent.ToString("f2"));
                }
            }
            else
            {
                result = "";
            }


            return result;
        }
        protected DataTable GetCountData(string unitname, string beforeyear, string curyear, string beformonth,
            string curmonth)
        {
            StringBuilder sbSql = new StringBuilder("");
            //获取月最后一天
            string curdays = DateTime.DaysInMonth(int.Parse(curyear), int.Parse(curmonth)).ToString();
            string beforedays = DateTime.DaysInMonth(int.Parse(beforeyear), int.Parse(beformonth)).ToString();

            sbSql.AppendFormat(@"Select *,
	                            ISNULL((
		                            Select Top 1 UnitAllot 
		                            From cm_UnitCharge
		                            Where UnitID=U.unit_ID AND AllotYear='{0}'
	                            ),0) AS TargetAllot ---上一年目标值
	                            ,(
		                            Select ISNULL(SUM(Acount),0)
		                            From cm_ProjectCharge
		                            Where (cprID IN (
							                            Select cpr_ID 
							                            From cm_Coperation 
							                            Where cpr_Unit=U.unit_Name)
			                                        ) AND (InAcountTime BETWEEN '{0}-{1}-01' AND '{0}-{1}-{2}')
	                            ) AS AcountMonth ---本月完成
	                            ,(
		                            Select ISNULL(SUM(cpr_Acount),0)
		                            From cm_Coperation
		                            Where cpr_Unit=U.unit_Name AND (cpr_SignDate BETWEEN '{0}-01-01' AND '{0}-{1}-{2}')
	                            ) AS AcountYear ---截止到本月累计
	                            ,ISNULL((
		                            convert(decimal(18,2),((
			                                            Select ISNULL(SUM(Acount),0)
		                                                From cm_ProjectCharge
		                                                Where (cprID IN (
							                                                Select cpr_ID 
							                                                From cm_Coperation 
							                                                Where cpr_Unit=U.unit_Name)
			                                        ) AND (InAcountTime BETWEEN '{0}-{1}-01' AND '{0}-{1}-{2}')
		                            )/(
			                            Select Top 1 UnitAllot 
			                            From cm_UnitCharge
			                            Where UnitID=U.unit_ID AND AllotYear='{0}'
		                            ))*100)  
	                            ),0) AS CompletePersent  ---截止到本月完成百分比
	                            ,ISNULL((
		                            Select Top 1 UnitAllot
		                            From cm_UnitCharge
		                            Where UnitID=U.unit_ID AND AllotYear='{3}'
	                            ),0) AS TargetAllot2  ---下年目标值
	                            ,(
		                            Select ISNULL(SUM(Acount),0)
		                            From cm_ProjectCharge
		                            Where (cprID IN (
							                            Select cpr_ID 
							                            From cm_Coperation 
							                            Where cpr_Unit=U.unit_Name)
			                                        ) AND (InAcountTime BETWEEN '{3}-{4}-01' AND '{3}-{4}-{5}')
	                            ) AS AcountMonth2 ---下年月完成
	                            ,(
		                            Select ISNULL(SUM(cpr_Acount),0)
		                            From cm_Coperation
		                            Where cpr_Unit=U.unit_Name AND (cpr_SignDate BETWEEN '{3}-01-01' AND '{3}-{4}-{5}')
	                            ) AS AcountYear2 ---下年截止到本月累计
	                            ,ISNULL((
		                            convert(decimal(18,2),((
			                                                Select ISNULL(SUM(Acount),0)
		                                                    From cm_ProjectCharge
		                                                    Where (cprID IN (
							                                                Select cpr_ID 
							                                                From cm_Coperation 
							                                                Where cpr_Unit=U.unit_Name)
			                                                ) AND (InAcountTime BETWEEN '{3}-{4}-01' AND '{3}-{4}-{5}')
		                            )/(
			                            Select Top 1 UnitAllot 
			                            From cm_UnitCharge
			                            Where UnitID=U.unit_ID AND AllotYear='{3}'
		                            ))*100)
	                            ),0) AS CompletePersent2  ---截止到本月完成百分比
                            From tg_unit U
                            Where unit_Name='{6}'", beforeyear, beformonth, beforedays, curyear, curmonth, curdays, unitname);

            DataTable dt = DBUtility.DbHelperSQL.Query(sbSql.ToString()).Tables[0];
            return dt;
        }

        /// <summary>
        /// 统计提示
        /// </summary>
        protected void GetCountCondition()
        {
            string strTip = "";
            strTip += this.drpYear.SelectedValue + "年";
            strTip += this.drpMonth.SelectedValue + "月";
            strTip += "[收费]与";
            strTip += this.drpYear2.SelectedValue + "年";
            strTip += this.drpMonth2.SelectedValue + "月";
            strTip += "同期对比统计表";

            this.CountTip = strTip;
        }
        #endregion

        #region 统计图表基本设置 2015年6月10日
        /// <summary>
        /// Y坐标数据
        /// </summary>
        public string xAxis { get; set; }
        /// <summary>
        /// y坐标数据
        /// </summary>
        public string yAxis { get; set; }
        /// <summary>
        /// 图例
        /// </summary>
        public string LegendData { get; set; }
        /// <summary>
        /// 统计数据
        /// </summary>
        public string SeriesData { get; set; }
        #endregion

        #region 图表统计数据 2015年6月10日
        /// <summary>
        /// 获取图例数据
        /// </summary>
        protected void GetLegendValue()
        {
            //范围图
            StringBuilder legendData = new StringBuilder();
            //数据图例
            legendData.Append("[");

            string beforetime = this.drpYear.SelectedValue + "年" + this.drpMonth.SelectedValue + "月";
            legendData.AppendFormat("\"{0}\",", beforetime + "(当月)");
            //legendData.AppendFormat("\"{0}\",", beforetime + "(累计)");
            string curtime = this.drpYear2.SelectedValue + "年" + this.drpMonth2.SelectedValue + "月";
            legendData.AppendFormat("\"{0}\",", curtime + "(当月)");
            // legendData.AppendFormat("\"{0}\",", curtime + "(累计)");

            //x坐标
            legendData.Remove(legendData.ToString().LastIndexOf(','), 1);
            legendData.Append("]");

            LegendData = legendData.ToString();

        }

        /// <summary>
        ///  获取X坐标数据
        /// </summary>
        private void GetxAxisValue()
        {
            //横向坐标
            StringBuilder sbxAxis = new StringBuilder();

            sbxAxis.Append("[");

            List<ASTreeViewNode> nodes = this.drp_unit.GetCheckedNodes(false);
            //判断是否全选
            bool isCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;

            if (isCheckAll)
            {
                DataTable dt = GetUnit();
                int index = 0;
                foreach (DataRow row in dt.Rows)
                {
                    string unitname = row["unit_Name"].ToString().Trim();
                    //X坐标数据
                    if (index % 2 == 0)
                    {
                        sbxAxis.Append("\"" + unitname + "\",");
                    }
                    else
                    {
                        sbxAxis.Append("\"\\n" + unitname + "\",");
                    }

                    index++;
                }

                index = 0;
            }
            else
            {
                int index = 0;
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    if (nodes.Count >= 10)
                    {

                        if (index % 2 == 0)
                        {
                            sbxAxis.Append("\"" + unitname + "\",");
                        }
                        else
                        {
                            sbxAxis.Append("\"\\n" + unitname + "\",");
                        }
                        index++;
                    }
                    else
                    {
                        sbxAxis.Append("\"" + unitname + "\",");
                    }
                }

                index = 0;
            }
            //x坐标
            sbxAxis.Remove(sbxAxis.ToString().LastIndexOf(','), 1);
            sbxAxis.Append("]");

            xAxis = sbxAxis.ToString();
        }

        /// <summary>
        /// 设置Y坐标数据
        /// </summary>
        private void SetyAxisValue()
        {
            StringBuilder sbyAxis = new StringBuilder();

            sbyAxis.Append(@"{
                            type : 'value',
                            name : '收款额',
                            axisLabel : {
                                formatter: '{value} 万元'}
                        }");

            yAxis = sbyAxis.ToString();
        }

        /// <summary>
        /// 获取实际数据
        /// </summary>
        private void GetSeriesData()
        {
            StringBuilder sbSeries = new StringBuilder();
            //年
            string beforeyear = this.drpYear.SelectedValue;
            string curyear = this.drpYear2.SelectedValue;
            //月
            string beforemonth = this.drpMonth.SelectedValue;
            string curmonth = this.drpMonth2.SelectedValue;
            //日
            string curdays = DateTime.DaysInMonth(int.Parse(curyear), int.Parse(curmonth)).ToString();
            string beforedays = DateTime.DaysInMonth(int.Parse(beforeyear), int.Parse(beforemonth)).ToString();

            //加载去年数据
            //当月
            string strData = GetCountDataByYear(beforeyear, beforemonth, beforedays, "0");
            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(当月)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", beforeyear, strData, beforemonth);
            //累计
            //            strData = GetCountDataByYear(beforeyear, beforemonth, beforedays, "1");
            //            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(累计)',
            //                                        type:'bar',
            //                                        data:{1},
            //                                        markPoint : {{
            //                                            data : [
            //                                                {{type : 'max', name: '最大值'}},
            //                                                {{type : 'min', name: '最小值'}}
            //                                            ]
            //                                        }},
            //                                        markLine : {{
            //                                            data : [
            //                                                {{type : 'average', name: '平均值'}}
            //                                            ]
            //                                        }}
            //                                    }},
            //                                ", beforeyear, strData, beforemonth);
            //加载今年数据
            //当月
            strData = GetCountDataByYear(curyear, curmonth, curdays, "0");
            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(当月)',
                                        type:'bar',
                                        data:{1},
                                        markPoint : {{
                                            data : [
                                                {{type : 'max', name: '最大值'}},
                                                {{type : 'min', name: '最小值'}}
                                            ]
                                        }},
                                        markLine : {{
                                            data : [
                                                {{type : 'average', name: '平均值'}}
                                            ]
                                        }}
                                    }},
                                ", curyear, strData, curmonth);
            //累计
            //            strData = GetCountDataByYear(curyear, curmonth, curdays, "1");
            //            sbSeries.AppendFormat(@"{{name:'{0}年{2}月(累计)',
            //                                        type:'bar',
            //                                        data:{1},
            //                                        markPoint : {{
            //                                            data : [
            //                                                {{type : 'max', name: '最大值'}},
            //                                                {{type : 'min', name: '最小值'}}
            //                                            ]
            //                                        }},
            //                                        markLine : {{
            //                                            data : [
            //                                                {{type : 'average', name: '平均值'}}
            //                                            ]
            //                                        }}
            //                                    }},
            //                                ", curyear, strData, curmonth);

            sbSeries.Remove(sbSeries.ToString().LastIndexOf(','), 1);
            //返回数据
            SeriesData = sbSeries.ToString();

        }
        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="year"></param>
        /// <param name="slttype"></param>
        /// <param name="typename"></param>
        /// <returns></returns>
        private string GetCountDataByYear(string year, string month, string days, string type)
        {
            //横向坐标
            //统计值
            StringBuilder sbyAxis = new StringBuilder();
            sbyAxis.Append("[");

            List<ASTreeViewNode> nodes = this.drp_unit.GetCheckedNodes(false);
            //判断是否全选
            bool isCheckAll = nodes.Any(n => n.NodeValue == "0") || nodes.Count == 0;

            if (isCheckAll)
            {
                DataTable dt = GetUnit();
                foreach (DataRow row in dt.Rows)
                {
                    string unitname = row["unit_Name"].ToString().Trim();
                    sbyAxis.Append(GetCprCountByUnit(unitname, year, month, days, type) + ",");
                }
            }
            else
            {
                foreach (ASTreeViewNode nd in nodes)
                {
                    string unitname = nd.NodeText.Trim();
                    sbyAxis.Append(GetCprCountByUnit(unitname, year, month, days, type) + ",");
                }
            }
            //y坐标
            sbyAxis.Remove(sbyAxis.ToString().LastIndexOf(','), 1);
            sbyAxis.Append("]");

            return sbyAxis.ToString();
        }

        #endregion
        protected void GetAllData()
        {
            //选中当前年
            GetSelectYear();
            //组织话术
            GetCountCondition();
            //获取数据
            GetCountDataByUnit();

            //图表开始
            GetLegendValue();
            //设置横坐标
            GetxAxisValue();
            //设置y单位
            SetyAxisValue();
            //统计值
            GetSeriesData();
        }
        /// <summary>
        /// 查询时间
        /// </summary>
        /// <param name="unitid"></param>
        /// <returns></returns>
        protected string GetCprCountByUnit(string unitname, string year, string month, string days, string type)
        {
            string strSql = string.Format(@" Select ISNULL(SUM(Acount),0)
		                                    From cm_ProjectCharge
		                                    Where (cprID IN (
							                                    Select cpr_ID 
							                                    From cm_Coperation 
							                                    Where cpr_Unit='{0}')
			  ) ", unitname);
            //当月
            if (type == "0")
            {
                strSql += string.Format(" AND (InAcountTime BETWEEN '{0}-{1}-01' AND '{0}-{1}-{2}')", year, month, days);
            }
            else//累计
            {
                strSql += string.Format(" AND (InAcountTime BETWEEN '{0}-01-01' AND '{0}-{1}-{2}')", year, month, days);
            }

            string result = Convert.ToString(DBUtility.DbHelperSQL.GetSingle(strSql));

            return result;
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GetAllData();
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcelByData();
        }

        /// <summary>
        /// 导出列表列表集合
        /// </summary>
        protected List<TG.Model.ChargeContrastMonthExcel> ImportExcelList =
            new List<TG.Model.ChargeContrastMonthExcel>();
        /// <summary>
        /// 导出Excel
        /// </summary>
        protected void ExportExcelByData()
        {
            GetCountCondition();
            //导出列表
            List<TG.Model.ChargeContrastMonthExcel> excellist = ViewState["ExcelList"] as List<TG.Model.ChargeContrastMonthExcel>;
            //模板路径
            string pathModel = "~/TemplateXls/ChargeContrastMonthTlt.xls";

            //导出Excel
            ExportDataToExcel(excellist, pathModel);
        }
        private void ExportDataToExcel(List<TG.Model.ChargeContrastMonthExcel> excellist, string modelPath)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            string sheetName = "sheet1";
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            ws.GetRow(0).GetCell(0).SetCellValue(this.drpYear.SelectedValue + "年（" + this.drpMonth.SelectedValue + "月)【收费】与" + this.drpYear2.SelectedValue + "年(" + this.drpMonth2.SelectedValue + "月)同期对比表");
            ws.GetRow(1).GetCell(8).SetCellValue(DateTime.Now.ToString("yyyy年MM月dd日") + "制表");
            ws.GetRow(2).GetCell(1).SetCellValue(this.drpYear.SelectedValue + "年收费目标值");
            ws.GetRow(2).GetCell(2).SetCellValue(this.drpYear.SelectedValue + "年");
            ws.GetRow(3).GetCell(2).SetCellValue(this.drpMonth.SelectedValue + "月");
            ws.GetRow(2).GetCell(3).SetCellValue(this.drpYear.SelectedValue + "年完成目标值(%)");
            ws.GetRow(2).GetCell(4).SetCellValue(this.drpYear2.SelectedValue + "年收费目标值");
            ws.GetRow(2).GetCell(5).SetCellValue(this.drpYear2.SelectedValue + "年");
            ws.GetRow(3).GetCell(5).SetCellValue(this.drpMonth2.SelectedValue + "月");
            ws.GetRow(2).GetCell(7).SetCellValue(this.drpYear2.SelectedValue + "年完成收费目标值(%)");
            //设置样式
            ICellStyle style = wb.CreateCellStyle();

            //设置字体
            IFont font = wb.CreateFont();
            font.FontHeightInPoints = 9;//字号
            font.FontName = "宋体";
            style.SetFont(font);
            int index = 4;
            style.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style.VerticalAlignment = VerticalAlignment.CENTER;
            //设置边框
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            //设置宽度
            //ws.SetColumnWidth(0, 15 * 256);
            //ws.SetColumnWidth(2, 25 * 256);
            //进行切割数据集合
            List<TG.Model.ChargeContrastMonthExcel> listGL = new List<TG.Model.ChargeContrastMonthExcel>();
            List<TG.Model.ChargeContrastMonthExcel> listSJ = new List<TG.Model.ChargeContrastMonthExcel>();
            List<TG.Model.ChargeContrastMonthExcel> listKC = new List<TG.Model.ChargeContrastMonthExcel>();
            List<TG.Model.ChargeContrastMonthExcel> listJL = new List<TG.Model.ChargeContrastMonthExcel>();
            List<TG.Model.ChargeContrastMonthExcel> listDJ = new List<TG.Model.ChargeContrastMonthExcel>();
            foreach (TG.Model.ChargeContrastMonthExcel item in excellist)
            {
                if (unitparenrID(item.UnitID.ToString()) == "238")
                {
                    if (item.UnitID.ToString() == "250")//勘察
                    {
                        listKC.Add(item);
                    }
                    else if (item.UnitID.ToString() == "253")//监理
                    {
                        listJL.Add(item);
                    }
                    else//其他
                    {
                        listSJ.Add(item);
                    }
                }
                else if (unitparenrID(item.UnitID.ToString()) == "254")
                {
                    listDJ.Add(item);
                }
                else
                {
                    if (unitparenrID(item.UnitID.ToString()) != "0" || item.UnitID.ToString() == "230")
                    {
                        listGL.Add(item);
                    }

                }
            }
            if (excellist.Count > 0)
            {

                //管理部门表头
                if (listGL.Count > 0)
                {
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("管理部门部分");
                    index = index + 1;
                    ws.AddMergedRegion(new CellRangeAddress(index - 1, index - 1, 0, 8));
                }
                //遍历管理部门列表
                foreach (TG.Model.ChargeContrastMonthExcel item in listGL)
                {
                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(item.UnitName);
                    cell0.CellStyle = style;
                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(Convert.ToDouble(item.BeforeTarget));
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(Convert.ToDouble(item.BeforeCurMonth));
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.CompleteBeforeVal);
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(Convert.ToDouble(item.CurrentTarget)); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(Convert.ToDouble(item.CurrentMonth)); cell5.CellStyle = style;
                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.CompareMonthVal); cell6.CellStyle = style;
                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.CompleteCurrentVal); cell7.CellStyle = style;
                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.CompareAllVal); cell8.CellStyle = style;

                    index = index + 1;
                }
                //增加管理部门小合计
                if (listGL.Count > 0)
                {
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("管理合计");
                    index = index + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(B3:B" + (index - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(C3:C" + (index - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(B" + index + "=0,\" \",(C" + index + "/B" + index + ")*100)");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(D3:E" + (index - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(F3:F" + (index - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(C" + index + "=0,\" \",(F" + index + "-C" + index + ")/C" + index + "*100)");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(E" + index + "=0,\" \",(F" + index + "/E" + index + ")*100)");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style;
                    cell.SetCellValue("------");
                    //cell = dataRow.CreateCell(9);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("IF(D" + index + "=0,\" \",(I" + index + "-D" + index + ")/D" + index + "*100)");
                    //cell = dataRow.CreateCell(10);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("(I" + index + "/F" + index + ")*100");
                    //cell = dataRow.CreateCell(11);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("IF(E" + index + "=0,\" \",(K" + index + "-E" + index + ")/E" + index + "*100)");
                }
                //设计所表头
                if (listSJ.Count > 0)
                {
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("设计、监理、勘察部分");
                    index = index + 1;
                    ws.AddMergedRegion(new CellRangeAddress(index - 1, index - 1, 0, 8));
                }
                //遍历设计设计所的部门列表
                foreach (TG.Model.ChargeContrastMonthExcel item in listSJ)
                {
                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(item.UnitName);
                    cell0.CellStyle = style;
                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(Convert.ToDouble(item.BeforeTarget));
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(Convert.ToDouble(item.BeforeCurMonth));
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.CompleteBeforeVal);
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(Convert.ToDouble(item.CurrentTarget)); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(Convert.ToDouble(item.CurrentMonth)); cell5.CellStyle = style;
                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.CompareMonthVal); cell6.CellStyle = style;
                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.CompleteCurrentVal); cell7.CellStyle = style;
                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.CompareAllVal); cell8.CellStyle = style;

                    index = index + 1;
                }
                //设计所小计
                if (listSJ.Count > 0)
                {
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("设计所合计");
                    index = index + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(B" + (index - listSJ.Count) + ":B" + (index - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(C" + (index - listSJ.Count) + ":C" + (index - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(B" + index + "=0,\" \",(C" + index + "/B" + index + ")*100)");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(E" + (index - listSJ.Count) + ":E" + (index - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(F" + (index - listSJ.Count) + ":F" + (index - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(C" + index + "=0,\" \",(F" + index + "-C" + index + ")/C" + index + "*100)");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(E" + index + "=0,\" \",(F" + index + "/E" + index + ")*100)");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style;
                    cell.SetCellValue("------");
                    //cell = dataRow.CreateCell(9);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("IF(D" + index + "=0,\" \",(I" + index + "-D" + index + ")/D" + index + "*100)");
                    //cell = dataRow.CreateCell(10);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("(I" + index + "/F" + index + ")*100");
                    //cell = dataRow.CreateCell(11);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("IF(E" + index + "=0,\" \",(K" + index + "-E" + index + ")/E" + index + "*100)");
                }
                //勘察和设计所小合计
                if (listKC.Count > 0)
                {
                    foreach (TG.Model.ChargeContrastMonthExcel item in listKC)
                    {
                        IRow row = ws.CreateRow(index);
                        ICell cell0 = row.CreateCell(0);
                        cell0.SetCellValue(item.UnitName);
                        cell0.CellStyle = style;
                        ICell cell1 = row.CreateCell(1);
                        cell1.SetCellValue(Convert.ToDouble(item.BeforeTarget));
                        cell1.CellStyle = style;
                        ICell cell2 = row.CreateCell(2);
                        cell2.SetCellValue(Convert.ToDouble(item.BeforeCurMonth));
                        cell2.CellStyle = style;
                        ICell cell3 = row.CreateCell(3);
                        cell3.SetCellValue(item.CompleteBeforeVal);
                        cell3.CellStyle = style;
                        ICell cell4 = row.CreateCell(4);
                        cell4.SetCellValue(Convert.ToDouble(item.CurrentTarget)); cell4.CellStyle = style;
                        ICell cell5 = row.CreateCell(5);
                        cell5.SetCellValue(Convert.ToDouble(item.CurrentMonth)); cell5.CellStyle = style;
                        ICell cell6 = row.CreateCell(6);
                        cell6.SetCellValue(item.CompareMonthVal); cell6.CellStyle = style;
                        ICell cell7 = row.CreateCell(7);
                        cell7.SetCellValue(item.CompleteCurrentVal); cell7.CellStyle = style;
                        ICell cell8 = row.CreateCell(8);
                        cell8.SetCellValue(item.CompareAllVal); cell8.CellStyle = style;

                        index = index + 1;
                    }
                    //顺便把合计给加上
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("勘察、设计合计");
                    index = index + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(B" + (index - 2) + ":B" + (index - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(C" + (index - 2) + ":C" + (index - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(B" + index + "=0,\" \",(C" + index + "/B" + index + ")*100)");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(E" + (index - 2) + ":E" + (index - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(F" + (index - 2) + ":F" + (index - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(C" + index + "=0,\" \",(F" + index + "-C" + index + ")/C" + index + "*100)");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(E" + index + "=0,\" \",(F" + index + "/E" + index + ")*100)");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style;
                    cell.SetCellValue("------");
                    //cell = dataRow.CreateCell(9);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("IF(D" + index + "=0,\" \",(I" + index + "-D" + index + ")/D" + index + "*100)");
                    //cell = dataRow.CreateCell(10);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("(I" + index + "/F" + index + ")*100");
                    //cell = dataRow.CreateCell(11);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("IF(E" + index + "=0,\" \",(K" + index + "-E" + index + ")/E" + index + "*100)");
                }
                //接下来是监理合计
                if (listJL.Count > 0)
                {
                    //遍历把监理加上
                    foreach (TG.Model.ChargeContrastMonthExcel item in listJL)
                    {
                        IRow row = ws.CreateRow(index);
                        ICell cell0 = row.CreateCell(0);
                        cell0.SetCellValue(item.UnitName);
                        cell0.CellStyle = style;
                        ICell cell1 = row.CreateCell(1);
                        cell1.SetCellValue(Convert.ToDouble(item.BeforeTarget));
                        cell1.CellStyle = style;
                        ICell cell2 = row.CreateCell(2);
                        cell2.SetCellValue(Convert.ToDouble(item.BeforeCurMonth));
                        cell2.CellStyle = style;
                        ICell cell3 = row.CreateCell(3);
                        cell3.SetCellValue(item.CompleteBeforeVal);
                        cell3.CellStyle = style;
                        ICell cell4 = row.CreateCell(4);
                        cell4.SetCellValue(Convert.ToDouble(item.CurrentTarget)); cell4.CellStyle = style;
                        ICell cell5 = row.CreateCell(5);
                        cell5.SetCellValue(Convert.ToDouble(item.CurrentMonth)); cell5.CellStyle = style;
                        ICell cell6 = row.CreateCell(6);
                        cell6.SetCellValue(item.CompareMonthVal); cell6.CellStyle = style;
                        ICell cell7 = row.CreateCell(7);
                        cell7.SetCellValue(item.CompleteCurrentVal); cell7.CellStyle = style;
                        ICell cell8 = row.CreateCell(8);
                        cell8.SetCellValue(item.CompareAllVal); cell8.CellStyle = style;

                        index = index + 1;
                    }
                    //顺便把合计给加上
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("主营合计");
                    index = index + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(B" + (index - 2) + ":B" + (index - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(C" + (index - 2) + ":C" + (index - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(B" + index + "=0,\" \",(C" + index + "/B" + index + ")*100)");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(E" + (index - 2) + ":E" + (index - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(F" + (index - 2) + ":F" + (index - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(C" + index + "=0,\" \",(F" + index + "-C" + index + ")/C" + index + "*100)");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(E" + index + "=0,\" \",(F" + index + "/E" + index + ")*100)");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style;
                    cell.SetCellValue("------");
                    //cell = dataRow.CreateCell(9);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("IF(D" + index + "=0,\" \",(I" + index + "-D" + index + ")/D" + index + "*100)");
                    //cell = dataRow.CreateCell(10);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("(I" + index + "/F" + index + ")*100");
                    //cell = dataRow.CreateCell(11);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("IF(E" + index + "=0,\" \",(K" + index + "-E" + index + ")/E" + index + "*100)");
                }
                //多经部门头
                //多经部门
                if (listDJ.Count > 0)
                {
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("多种经营部分");
                    index = index + 1;
                    ws.AddMergedRegion(new CellRangeAddress(index - 1, index - 1, 0, 8));
                }
                //遍历多经部门列表
                foreach (TG.Model.ChargeContrastMonthExcel item in listDJ)
                {
                    IRow row = ws.CreateRow(index);
                    ICell cell0 = row.CreateCell(0);
                    cell0.SetCellValue(item.UnitName);
                    cell0.CellStyle = style;
                    ICell cell1 = row.CreateCell(1);
                    cell1.SetCellValue(Convert.ToDouble(item.BeforeTarget));
                    cell1.CellStyle = style;
                    ICell cell2 = row.CreateCell(2);
                    cell2.SetCellValue(Convert.ToDouble(item.BeforeCurMonth));
                    cell2.CellStyle = style;
                    ICell cell3 = row.CreateCell(3);
                    cell3.SetCellValue(item.CompleteBeforeVal);
                    cell3.CellStyle = style;
                    ICell cell4 = row.CreateCell(4);
                    cell4.SetCellValue(Convert.ToDouble(item.CurrentTarget)); cell4.CellStyle = style;
                    ICell cell5 = row.CreateCell(5);
                    cell5.SetCellValue(Convert.ToDouble(item.CurrentMonth)); cell5.CellStyle = style;
                    ICell cell6 = row.CreateCell(6);
                    cell6.SetCellValue(item.CompareMonthVal); cell6.CellStyle = style;
                    ICell cell7 = row.CreateCell(7);
                    cell7.SetCellValue(item.CompleteCurrentVal); cell7.CellStyle = style;
                    ICell cell8 = row.CreateCell(8);
                    cell8.SetCellValue(item.CompareAllVal); cell8.CellStyle = style;
                    index = index + 1;
                }
                //多经营合计
                if (listDJ.Count > 0)
                {
                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("多经营所合计");
                    index = index + 1;
                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(B" + (index - listDJ.Count) + ":B" + (index - 1) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(C" + (index - listDJ.Count) + ":C" + (index - 1) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(B" + index + "=0,\" \",(C" + index + "/B" + index + ")*100)");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(E" + (index - listDJ.Count) + ":E" + (index - 1) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(F" + (index - listDJ.Count) + ":F" + (index - 1) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(C" + index + "=0,\" \",(F" + index + "-C" + index + ")/C" + index + "*100)");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(E" + index + "=0,\" \",(F" + index + "/E" + index + ")*100)");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style;
                    cell.SetCellValue("------");
                    //cell = dataRow.CreateCell(9);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("IF(D" + index + "=0,\" \",(I" + index + "-D" + index + ")/D" + index + "*100)");
                    //cell = dataRow.CreateCell(10);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("(I" + index + "/F" + index + ")*100");
                    //cell = dataRow.CreateCell(11);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("IF(E" + index + "=0,\" \",(K" + index + "-E" + index + ")/E" + index + "*100)");
                }
                //全院总合计
                if (listDJ.Count != 0 && listSJ.Count != 0 && listGL.Count != 0 && listKC.Count != 0 && listJL.Count != 0)
                {

                    var dataRow = ws.GetRow(index);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(index);//生成行
                    var cell = dataRow.CreateCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);
                    cell.CellStyle = style;
                    cell.SetCellValue("全院总合计");

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(B" + (index - listDJ.Count - 2) + ",B" + (index) + ",B" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(C" + (index - listDJ.Count - 2) + ",C" + (index) + ",C" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(B" + (index + 1) + "=0,\" \",(C" + (index + 1) + "/B" + (index + 1) + ")*100)");
                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(E" + (index - listDJ.Count - 2) + ",E" + (index) + ",E" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    //cell.SetCellValue()
                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style;
                    cell.SetCellFormula("sum(F" + (index - listDJ.Count - 2) + ",F" + (index) + ",F" + (index - listDJ.Count - listKC.Count - listJL.Count - 2 - 2 - listSJ.Count - 2) + ")");
                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(C" + (index + 1) + "=0,\" \",(F" + (index + 1) + "-C" + (index + 1) + ")/C" + (index + 1) + "*100)");
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style;
                    cell.SetCellFormula("IF(E" + (index + 1) + "=0,\" \",(F" + (index + 1) + "/E" + (index + 1) + ")*100)");
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style;
                    cell.SetCellValue("------");
                    //cell = dataRow.CreateCell(9);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("IF(D" + (index + 1) + "=0,\" \",(I" + (index + 1) + "-D" + (index + 1) + ")/D" + (index + 1) + "*100)");
                    //cell = dataRow.CreateCell(10);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("(I" + (index + 1) + "/F" + (index + 1) + ")*100");
                    //cell = dataRow.CreateCell(11);
                    //cell.CellStyle = style;
                    //cell.SetCellFormula("IF(E" + (index + 1) + "=0,\" \",(K" + (index + 1) + "-E" + (index + 1) + ")/E" + (index + 1) + "*100)");
                }
                #region 注释
                //foreach (TG.Model.ChargeContrastMonthExcel item in excellist)
                //{
                //    //    Unit AllotUnitNow AcountMonth beforeMubiao AcountMonthNow TongBiMonthNow nowMubiao TongBiMuBiao AllotUnitLast

                //    IRow row = ws.CreateRow(index);
                //    ICell cell0 = row.CreateCell(0);
                //    cell0.SetCellValue(item.UnitName);
                //    cell0.CellStyle = style;
                //    ICell cell1 = row.CreateCell(1);
                //    cell1.SetCellValue(item.BeforeTarget);
                //    cell1.CellStyle = style;
                //    ICell cell2 = row.CreateCell(2);
                //    cell2.SetCellValue(Convert.ToDouble(item.BeforeCurMonth));
                //    cell2.CellStyle = style;
                //    ICell cell3 = row.CreateCell(3);
                //    cell3.SetCellValue(item.CompleteBeforeVal);
                //    cell3.CellStyle = style;
                //    ICell cell4 = row.CreateCell(4);
                //    cell4.SetCellValue(Convert.ToDouble(item.CurrentTarget)); cell4.CellStyle = style;
                //    ICell cell5 = row.CreateCell(5);
                //    cell5.SetCellValue(item.CurrentMonth); cell5.CellStyle = style;
                //    ICell cell6 = row.CreateCell(6);
                //    cell6.SetCellValue(item.CompareMonthVal); cell6.CellStyle = style;
                //    ICell cell7 = row.CreateCell(7);
                //    cell7.SetCellValue(item.CompleteCurrentVal); cell7.CellStyle = style;
                //    ICell cell8 = row.CreateCell(8);
                //    cell8.SetCellValue(item.CompareAllVal); cell8.CellStyle = style;

                //    index++;
                //} 
                #endregion
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(this.drpYear.SelectedValue + "年（" + this.drpMonth.SelectedValue + "月)【收费】与" + this.drpYear2.SelectedValue + "年(" + this.drpMonth2.SelectedValue + "月)同期对比表.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        /// <summary>
        /// 写单元格
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="value"></param>
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }
        public string unitparenrID(string unitid)
        {
            return bllUnit.GetModel(int.Parse(unitid)).unit_ParentID.ToString();
        }
    }
}