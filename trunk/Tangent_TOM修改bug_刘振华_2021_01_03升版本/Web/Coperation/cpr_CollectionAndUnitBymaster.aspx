﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_CollectionAndUnitBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_CollectionAndUnitBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/FunctionChart/FusionCharts.js"></script>
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="../js/jquery.chromatable.js" type="text/javascript"></script>
    <script src="../js/Coperation/cpr_CollectionAndUnit.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>财务报表显示</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>生产经营统计报表</a><i class="fa fa-angle-right">
    </i><a>财务报表显示</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>查询财务报表显示
                    </div>
                    <div class="actions">
                        <asp:Button ID="btn_report" runat="server" Text="导出" CssClass="btn red btn-sm"
                            OnClick="btn_report_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>收款年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem>2010</asp:ListItem>
                                    <asp:ListItem>2011</asp:ListItem>
                                    <asp:ListItem>2012</asp:ListItem>
                                    <asp:ListItem>2013</asp:ListItem>
                                    <asp:ListItem>2014</asp:ListItem>
                                    <asp:ListItem>2015</asp:ListItem>
                                    <asp:ListItem>2016</asp:ListItem>
                                    <asp:ListItem>2017</asp:ListItem>
                                    <asp:ListItem>2018</asp:ListItem>
                                    <asp:ListItem>2019</asp:ListItem>
                                    <asp:ListItem>2020</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>收款月份:</td>
                            <td>
                                <asp:DropDownList ID="drp_month" runat="server" CssClass="form-control">
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>6</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                    <asp:ListItem>8</asp:ListItem>
                                    <asp:ListItem>9</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>11</asp:ListItem>
                                    <asp:ListItem>12</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>
                                <input type="button" class="btn blue" value="查询" id="btn_ok" /></td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>财务报表显示列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <div class="row">
                        <div class="col-md-12">
                            <%--   <table style="width: 100%;" class="cls_show_cst_jiben">
                                    <tr>
                                        <td style="border: none;">--%>
                            <div class="cls_Container_Report">
                                <div class="cls_Container_Tip">
                                    <label id="title">
                                    </label>
                                    年<label id="month"></label>月财务报表
                                </div>
                                <div style="margin: 0 auto; width: 99%;">
                                    <table id="mytab" runat="server" class="table table-bordered" style="width: 99%;">
                                        <tr>
                                            <td style="width: 10%;">月份
                                            </td>
                                            <td style="width: 15%;">凭单号
                                            </td>
                                            <td style="width: 45%;">收款部门及交款单位
                                            </td>
                                            <td style="width: 15%;">收款日期
                                            </td>
                                            <td style="width: 15%;">金额(万元)
                                            </td>
                                        </tr>
                                    </table>
                                    <table id="AddTable" class="table table-bordered table-data" style="width: 99%;">
                                    </table>
                                </div>
                            </div>
                            <%--  </td>
                                    </tr>
                                </table>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
