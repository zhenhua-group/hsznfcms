﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace TG.Web.Coperation.ProjChgWin
{
    public partial class AddProjCharge : PageBase
    {
        //入账人
        public string CurrentUser
        {
            get
            {
                return UserSysNo.ToString();
            }
        }
        //项目ID
        public string CoperationSysNo
        {
            get
            {
                int projid = 0;
                string obj = Request.Params["projno"] ?? "0";
                bool flag = int.TryParse(obj, out projid);
                return projid.ToString();
            }
        }
        //项目合同额
        public decimal CoperationAllCount
        {
            get
            {
                decimal allcount = 0m;
                string obj = Request.Params["projallcount"] ?? "0";
                decimal.TryParse(obj, out allcount);
                return allcount;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindChargeList();
                //总金额
                this.lbl_allcount.Text = CoperationAllCount.ToString();
                this.lbl_allcount2.Text = this.lbl_allcount.Text;
                //未收款
                this.lbl_nopaycount.Text = GetNoPayCount().ToString();

                //项目名称
                GetCprName(CoperationSysNo);
                InitBaseInfo();
            }
        }
        //初始化基本 信息
        protected void InitBaseInfo()
        {
            //是否已经收完款
            //this.hid_iscomplete.Value = IsInputComplete();
            //项目ID
            this.hid_projID.Value = CoperationSysNo;
            //当前用户
            this.hid_curuser.Value = CurrentUser;
        }
        //获取项目名称
        protected void GetCprName(string cprid)
        {
            TG.Model.cm_Coperation model = new TG.BLL.cm_Coperation().GetModel(int.Parse(cprid));
            if (model != null)
            {
                this.lbl_projname.Text = model.cpr_Name.Trim();
                this.lbl_projname2.Text = this.lbl_projname.Text;
                this.lbl_unit.Text = model.cpr_Unit;
            }
        }
        //已经确认收款
        protected decimal GetNoPayCount()
        {
            string strSql = " Select Sum(Acount) From cm_ProjectCharge Where cprID=" + CoperationSysNo;
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            string rlt = bll_db.GetList(strSql).Tables[0].Rows[0][0].ToString();

            if (rlt == "")
            {
                rlt = "0.000000";
            }
            return (CoperationAllCount - Convert.ToDecimal(rlt));
        }
        //绑定收费列表
        protected void BindChargeList()
        {
            string strWhere = " cprID=" + CoperationSysNo;
            this.gv_Coperation.DataSource = new TG.BLL.cm_ProjectCharge().GetList(strWhere);
            this.gv_Coperation.DataBind();
        }

        //是否已经收款完成
        //public string IsInputComplete()
        //{
        //    //计算已经收款
        //    string strSql = " Select Sum(Acount) From cm_ProjectCharge Where Status='E' AND cprID=" + CoperationSysNo;
        //    TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
        //    string rlt = bll_db.GetList(strSql).Tables[0].Rows[0][0].ToString();
        //    if (rlt == "")
        //    {
        //        rlt = "0.000000";
        //    }
        //    //已收款
        //    decimal paycount = Convert.ToDecimal(rlt);

        //    if (paycount >= CoperationAllCount)
        //    {
        //        return "1";
        //    }
        //    else
        //    {
        //        return "0";
        //    }
        //}
        //行绑定
        protected void gv_Coperation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string status = e.Row.Cells[6].Text.Trim();
                if (status == "A")
                {
                    status = "财务确认";
                }
                else if (status == "B")
                {
                    status = "财务不通过";
                }
                else if (status == "C")
                {
                    status = "所长确认";
                }
                else if (status == "D")
                {
                    status = "所长不通过";
                }
                else if (status == "E")
                {
                    status = "完成入账";
                }
                e.Row.Cells[6].Text = status;
                //计算比例
                Label lbl_persent = e.Row.Cells[2].FindControl("lbl_persent") as Label;
                decimal paycount = Convert.ToDecimal(e.Row.Cells[1].Text);
                if (paycount > 0)
                {
                    lbl_persent.Text = ((paycount / CoperationAllCount) * 100).ToString("f2") + "%";
                }
                //入款人
                e.Row.Cells[4].Text = GetUserNameByID(e.Row.Cells[4].Text);
            }
        }
        // 获取用户名称
        protected string GetUserNameByID(string memid)
        {
            if (memid != "")
            {
                return new TG.BLL.tg_member().GetModel(int.Parse(memid)).mem_Name.ToString();
            }
            else
            {
                return "";
            }
        }
    }
}