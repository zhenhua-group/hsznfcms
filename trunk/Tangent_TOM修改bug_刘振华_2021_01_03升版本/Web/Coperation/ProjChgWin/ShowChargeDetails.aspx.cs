﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TG.Web.Coperation.ProjChgWin
{
    public partial class ShowChargeDetails : System.Web.UI.Page
    {
        //项目ID
        public string CoperationSysNo
        {
            get
            {
                int projid = 0;
                string obj = Request.Params["projno"] ?? "0";
                bool flag = int.TryParse(obj, out projid);
                return projid.ToString();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindChargeList();
            }
        }
        //绑定收费列表
        protected void BindChargeList()
        {
            string strWhere = " cprID=" + CoperationSysNo;
            this.gv_Coperation.DataSource = new TG.BLL.cm_ProjectCharge().GetList(strWhere);
            this.gv_Coperation.DataBind();
        }
        //行绑定
        protected void gv_Coperation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string status = e.Row.Cells[6].Text.Trim();
                if (status == "A")
                {
                    status = "财务确认";
                }
                else if (status=="B")
                {
                    status = "财务不通过";
                }
                else if (status == "C")
                {
                    status = "所长确认";
                }
                else if (status == "D")
                {
                    status = "所长不通过";
                }
                else if (status == "E")
                {
                    status = "完成入账";
                }
                e.Row.Cells[6].Text = status;
                //计算比例
                Label lbl_persent = e.Row.Cells[2].FindControl("lbl_persent") as Label;
                decimal paycount = Convert.ToDecimal(e.Row.Cells[1].Text);
                if (paycount > 0)
                {
                    lbl_persent.Text = ((paycount / ProjectAllCount) * 100).ToString("f2") + "%";
                }
                //入款人
                e.Row.Cells[4].Text = GetUserNameByID(e.Row.Cells[4].Text);

            }
        }
        //项目合同额
        public decimal ProjectAllCount
        {
            get
            {
                decimal allcount = 0m;
                string obj = Request.Params["projallcount"] ?? "0";
                decimal.TryParse(obj, out allcount);
                return allcount;
            }
        }
        // 获取用户名称
        protected string GetUserNameByID(string memid)
        {
            if (memid != "")
            {
                return new TG.BLL.tg_member().GetModel(int.Parse(memid)).mem_Name.ToString();
            }
            else
            {
                return "";
            }
        }
    }
}