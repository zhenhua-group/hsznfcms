﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.Model;
using TG.BLL;

namespace TG.Web.Coperation.ProjChgWin
{
    public partial class ProjectAcountConfirm : PageBase
    {
        //项目名
        public string CoperationName
        {
            get;
            set;
        }
        //承接部门
        public string CoperationUnit
        {
            get;
            set;
        }
        //项目ID
        public string CoperationSysNo
        {
            get
            {
                return Request["projID"];
            }
        }

        //消息发送者
        public int FromUser
        {
            get
            {
                int fromUser = 0;
                int.TryParse(Request["fromUser"], out fromUser);
                return fromUser;
            }
        }
        //入账ID
        public int CprPayNo
        {
            get
            {
                int realPaySysNo = 0;
                int.TryParse(Request["sysNo"], out realPaySysNo);
                return realPaySysNo;
            }
        }
        //审批消息ID
        public int MessageID
        {
            get
            {
                int msgSysNo = 0;
                int.TryParse(Request["MsgNo"], out msgSysNo);
                return msgSysNo;
            }
        }
        //审批状态
        public string ChargeAuditStatus
        {
            get
            {
                return Request["auditstatus"] ?? "";
            }
        }

        #region 接收审批消息列表参数
        //列表页码数
        public string pageIndex
        {
            get
            {
                return Request["pageIndex"];               
            }
        }
        //消息类型
        public string MessageType
        {
            get
            {

                return Request["messagetype"];
            }
        }
        //消息状态
        public string TypePost
        {
            get
            {

                return Request["typepost"];
            }
        }
        //消息类别
        public string Action
        {
            get
            {
                return Request["action"] ?? "";
            }
        }
        //消息标示
        public string Aflag
        {
            get
            {
                return Request["flag"] ?? "";
            }
        }
        //消息关键字
        public string MessageKeys
        {
            get
            {
                return Request["messagekeys"] ?? "";
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //项目入账纪录
                GetProjAcountRecord();
                //基本信息
                GetCurrentAcountRecord();
            }
        }
        //获取项目入账纪录
        protected void GetProjAcountRecord()
        {
            string strWhere = " ID=" + CprPayNo;
            this.gv_Coperation.DataSource = new TG.BLL.cm_ProjectCharge().GetList(strWhere);
            this.gv_Coperation.DataBind();

            GetCoperationEntity();
        }

        //本次付款的基本信息
        protected void GetCurrentAcountRecord()
        {
            TG.Model.cm_ProjectCharge model = new TG.BLL.cm_ProjectCharge().GetModel(CprPayNo);
            if (model != null)
            {
                this.lbl_acount.Text = Convert.ToDecimal(model.Acount).ToString("f6");
                this.lbl_remitter.Text = model.FromUser;
                this.lbl_times.Text = Convert.ToDateTime(model.InAcountTime).ToShortDateString();
                this.lbl_user.Text = new TG.BLL.tg_member().GetModel(int.Parse(model.InAcountUser)).mem_Name;
                this.lbl_mark.Text = model.ProcessMark;
                this.txt_caiwu.Text = model.CaiwuMark;
                this.txt_suozhang.Text = model.SuoZhangMark;

                if (model.Status.Trim() == "A" || model.Status.Trim() == "B")
                {
                    this.lbl_status.Text = "财务";
                    //this.lbl_BillCode.Enabled = false;
                    if (model.Status.Trim() == "B")
                    {
                        this.btn_cprType.InnerHtml = "";
                        this.lbl_BillCode.Enabled = false;
                        this.txt_caiwu.Enabled = false;
                        this.sureButton.Enabled = false;
                        this.sureButton.Visible = false;
                        this.chk_no.Enabled = false;
                        this.chk_yes.Enabled = false;
                    }
                }
                else if (model.Status.Trim() == "C" || model.Status.Trim() == "D")
                {
                    this.lbl_status.Text = "所长";
                    this.lbl_BillCode.Enabled = false;
                    this.btn_cprType.InnerHtml = "";
                    this.lbl_BillCode.Text = model.InAcountCode.Trim();

                    TG.BLL.cm_SysMsg bll = new TG.BLL.cm_SysMsg();

                    //判断该审批是否完成
                    string isDone = bll.IsDone(MessageID);

                    if (isDone.Trim() == "D")
                    {
                        this.chk_no.Enabled = false;
                        this.chk_yes.Enabled = false;
                        this.sureButton.Visible = false;
                    }

                    if (model.Status.Trim() == "D")
                    {
                        this.lbl_BillCode.Enabled = false;
                        this.txt_suozhang.Enabled = false;
                        this.sureButton.Enabled = false;
                        this.chk_no.Enabled = false;
                        this.chk_yes.Enabled = false;
                        this.sureButton.Visible = false;
                    }
                }
                else if (model.Status.Trim() == "E" || model.Status.Trim() == "F")
                {
                    this.lbl_status.Text = "完成";
                    this.lbl_BillCode.Enabled = false;
                    this.btn_cprType.InnerHtml = "";
                    chk_yes.Enabled = false;
                    chk_no.Enabled = false;
                    this.sureButton.Visible = false;
                    this.lbl_BillCode.Text = model.InAcountCode.Trim();
                }
            }
            else
            {
                this.sureButton.Visible = false;
                this.hid_flag.Value = "1";
            }
        }
        //确认入账
        protected void sureButton_Click(object sender, ImageClickEventArgs e)
        {
            GetCoperationEntity();
            //更新入账状态
            TG.Model.cm_ProjectCharge model = new TG.BLL.cm_ProjectCharge().GetModel(CprPayNo);
            if (model != null)
            {
                //通过
                if (this.chk_yes.Checked)
                {
                    //收款状态为财务确认
                    if (model.Status.Trim() == "A")
                    {
                        model.Status = "C";
                        model.InAcountCode = this.lbl_BillCode.Text;
                        if (new TG.BLL.cm_ProjectCharge().Update(model))
                        {
                            //更新单据号状态
                            if (!string.IsNullOrEmpty(Hiddencm_FinancialNum.Value))
                            {
                                int id = int.Parse(Hiddencm_FinancialNum.Value);
                                ImportFinancialData bll = new ImportFinancialData();
                                bll.UpdateFinanalStatus(id);
                            }
                            //发消息到生产部门确认
                            SendMessageToLeader(CoperationSysNo.ToString(), CprPayNo.ToString(), "C");
                        }
                    }
                    else if (model.Status.Trim() == "C")
                    {
                        model.Status = "E";
                        if (new TG.BLL.cm_ProjectCharge().Update(model))
                        {

                            //发送到生产经营出
                            // TG.Common.MessageBox.ResponseScriptBack(this, "项目入账完成！");
                            TG.Common.MessageBox.ShowAndRedirect(this, "项目入账完成！", "../cpr_SysMsgListView.aspx?typepost=" + TypePost + "&flag=" + Aflag + "&action=" + Action + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "");
                        }
                    }

                    UpdateWorkDoneStatus();
                }
                else
                {
                    //财务意见
                    string caiwumark = this.txt_caiwu.Text.Trim();
                    string zuozhangmark = this.txt_suozhang.Text.Trim();
                    string auditstatus = "";
                    //修改意见，状态改变
                    string strSql = "";
                    if (model.Status.Trim() == "A")
                    {
                        auditstatus = "B";
                        strSql = string.Format("update cm_ProjectCharge set status='B',CaiwuMark='{1}' where ID={0};", CprPayNo, caiwumark);
                    }
                    else if (model.Status.Trim() == "C")
                    {
                        auditstatus = "D";
                        strSql = string.Format("update cm_ProjectCharge set status='D',SuoZhangMark='{1}' where ID={0};", CprPayNo, zuozhangmark);
                    }

                    int count = TG.DBUtility.DbHelperSQL.ExecuteSql(strSql);

                    if (count > 0)
                    {
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("fromUser={0}&sysNo={1}&projID={2}&auditstatus={3}", UserSysNo, CprPayNo, CoperationSysNo, auditstatus),
                            FromUser = FromUser,
                            InUser = UserSysNo,
                            MsgType = 7,
                            ToRole = "0",
                            MessageContent = string.Format("关于{0}部门的{1}项目收款{2}！", CoperationUnit, CoperationName, "确认到账不通过！"),
                            QueryCondition = CoperationName,
                            IsDone = "B"
                        };
                        int resultCount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                        //待办状态修改
                        UpdateWorkDoneStatus();

                        if (resultCount > 0)
                        {
                                                      
                            TG.Common.MessageBox.ResponseScriptBack(this, "操作成功！");
                        }
                        else
                        {
                           
                            TG.Common.MessageBox.ResponseScriptBack(this, "操作成功！");
                        }
                    }
                }
            }
        }

        //发送消息到财务
        protected void SendMessageToLeader(string cprid, string identity, string type)
        {
            string roleString = "";
            string roleUnit = "";
            string msgType = "";
            if (type == "C")
            {
                roleString = "承接部门负责人";
                roleUnit = CoperationUnit.Trim();
                msgType = "部门所长";
            }
            //查询负责人角色用户
            string sql1 = string.Format("SELECT TOP 1 [Users] FROM [dbo].[cm_Role] where Rtrim(RoleName)= N'{0}' order by SysNo DESC", roleString);
            object objRoleSysNo = TG.DBUtility.DbHelperSQL.GetSingle(sql1);
            string rolesID = objRoleSysNo == null ? "0" : objRoleSysNo.ToString();
            //部门负责人角色ID
            sql1 = string.Format("SELECT TOP 1 [SysNo] FROM [dbo].[cm_Role] where Rtrim(RoleName)=N'{0}' order by SysNo DESC", roleString);
            object RoleSysNo = TG.DBUtility.DbHelperSQL.GetSingle(sql1);
            string roleSysNo = RoleSysNo == null ? "0" : RoleSysNo.ToString();
            //查询对应承接部门下的负责人
            string strWhere = " mem_unit_ID IN ( Select unit_ID From tg_unit Where unit_Name = N'" + roleUnit.Trim() + "') AND mem_ID IN (" + rolesID + ")";
            List<TG.Model.tg_member> list_mem = new TG.BLL.tg_member().GetModelList(strWhere);
            if (list_mem.Count > 0)
            {
                foreach (TG.Model.tg_member mem in list_mem)
                {
                    //查询项目信息
                    TG.Model.cm_Coperation model = new TG.BLL.cm_Coperation().GetModel(int.Parse(cprid));
                    if (model != null)
                    {
                        string cprname = model.cpr_Name;
                        string cprunit = model.cpr_Unit;

                        //发送消息给财务
                        SysMessageViewEntity sysMessageViewEntity = new SysMessageViewEntity
                        {
                            ReferenceSysNo = string.Format("fromUser={0}&sysNo={1}&projID={2}&auditstatus={3}", UserSysNo, identity, cprid, type),
                            FromUser = mem.mem_ID,
                            InUser = UserSysNo,
                            MsgType = 7,//项目入账
                            ToRole = "0",//发送到个人
                            MessageContent = string.Format("关于{0}的{1}项目收款入账{2}确认！", cprunit, cprname, msgType),
                            QueryCondition = cprname,
                            IsDone = "A"
                        };
                        int resultCount = new TG.BLL.cm_SysMsg().InsertSysMessage(sysMessageViewEntity);
                    }
                }
                //消息入账
                if (type == "C")
                {
                    //TG.Common.MessageBox.ResponseScriptBack(this, "财务入账确认成功，消息发送至部门负责人确认入账！");
                    TG.Common.MessageBox.ShowAndRedirect(this, "财务入账确认成功，消息发送至部门负责人确认入账！", "../cpr_SysMsgListView.aspx?typepost=" + TypePost + "&flag=" + Aflag + "&action=" + Action + "&pageIndex=" + pageIndex + "&MessageType=" + MessageType + "");
                }
            }
            else
            {
                TG.Common.MessageBox.Show(this, "项目承接部门负责人没有指定！");
            }
        }
        //未到账
        //protected void notSureButton_Click(object sender, ImageClickEventArgs e)
        //{
        //    Response.Redirect("../cpr_SysMsgListView.aspx");
        //}
        //行绑定
        protected void gv_Coperation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string status = e.Row.Cells[6].Text.Trim();
                if (status == "A")
                {
                    status = "财务确认";
                }
                else if (status == "B")
                {
                    status = "财务不通过";
                }
                else if (status == "C")
                {
                    status = "所长确认";
                }
                else if (status == "D")
                {
                    status = "所长不通过";
                }
                else if (status == "E")
                {
                    status = "完成入账";
                }
                e.Row.Cells[6].Text = status;
                //入款人
                e.Row.Cells[3].Text = GetUserNameByID(e.Row.Cells[3].Text);
            }
        }
        // 获取用户名称
        protected string GetUserNameByID(string memid)
        {
            if (!string.IsNullOrEmpty(memid))
            {
                return new TG.BLL.tg_member().GetModel(int.Parse(memid)).mem_Name.ToString();
            }
            else
            {
                return "";
            }
        }

        //获取合同实体信息
        protected void GetCoperationEntity()
        {
            //获取合同信息
            TG.Model.cm_Coperation model = new TG.BLL.cm_Coperation().GetModel(int.Parse(CoperationSysNo));
            if (model != null)
            {
                CoperationName = model.cpr_Name.Trim();
                CoperationUnit = model.cpr_Unit.Trim();
            }
        }

        //更新待办状态
        protected void UpdateWorkDoneStatus()
        {
            //修改办公状态
            int count = new TG.BLL.cm_SysMsg().UpdateSysMsgDoneStatus(MessageID);
        }
    }
}