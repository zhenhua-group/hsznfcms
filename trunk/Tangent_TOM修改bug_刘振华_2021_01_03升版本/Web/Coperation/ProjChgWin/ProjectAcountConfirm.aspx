﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProjectAcountConfirm.aspx.cs"
    Inherits="TG.Web.Coperation.ProjChgWin.ProjectAcountConfirm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../../css/CprChargStatus.css" rel="stylesheet" type="text/css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
    <script type="text/javascript" src="../../js/jquery-1.8.0.min.js"></script>
    <script src="../../js/Common/CommonControl.js" type="text/javascript"></script>
    <script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/Common/AutoComplete.js"></script>
    <script type="text/javascript" src="../../js/MessageComm.js"></script>
    <script type="text/javascript" src="/js/jquery.alerts.js"></script>
    <script src="/js/Jquery-extend.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            CommonControl.SetFormWidth();
            CommonControl.SetTableStyle("td_input", "need");

            //记录是否存在
            var flag = $("#hid_flag").val();
            if (flag == "0") {
                $("#td_input").show();
                $("#tb_noDetails").hide();
            }
            else {
                $("#td_input").hide();
                $("#tb_noDetails").show();
            }

            //只能选择控制
            //            $(".cls_input_text_valid").focus(function () {
            //                $(this).blur();
            //            });

            //查询
            $("#btn_cprType").click(function () {

                $("#customerCompactTable tr").remove();
                LoadAjaxData();
                $("#chooseCustomerCompact").dialog({
                    autoOpen: false,
                    modal: true,
                    width: 600,
                    top: 100,
                    resizable: false,
                    title: "入账单据号"
                }).dialog("open");
            });

            //审批状态
            var status = $.trim($("#HiddenAuditStatus").val());


            $("#sureButton").click(function () {
                //入账单号是否为空
                var billno = $("#lbl_BillCode").val();

                if (!$("#chk_no").get(0).checked) {
                    if ($.trim(billno) == "") {
                        alert("请输入单据号！");
                        return false;
                    }
                }

                $("#lbl_BillCode").val(billno);


                //如果不通过填写原因
                if ($("#chk_no").get(0).checked) {
                    if (status == "A") {
                        if ($.trim($("#txt_caiwu").val()) == "") {
                            alert("不通过原因不能为空！");
                            return false;
                        }
                    }
                    else if (status == "C") {
                        if ($.trim($("#txt_suozhang").val()) == "") {
                            alert("不通过原因不能为空！");
                            return false;
                        }
                    }
                }
            });
            //通过按钮
            $("#chk_yes").click(function () {
                if ($(this).attr("checked") == "checked") {
                    $("#Caiwu").hide();
                    $("#Suozhang").hide();

                    $("#DanJu").show();
                    if (status == "A") {

                        $("#lbl_BillCode").val("");
                        //$("#lbl_BillCode").attr("disabled", true);
                    }
                    else {
                        $("#lbl_BillCode").attr("disabled", true);
                    }
                }
            });
            $("#chk_no").click(function () {
                if ($(this).attr("checked") == "checked") {
                    if (status == "A") {
                        $("#Caiwu").show();
                        $("#Suozhang").hide();
                        $("#lbl_BillCode").val("");
                        $("#DanJu").hide();
                    }
                    else if (status == "C") {
                        $("#Caiwu").hide();
                        $("#Suozhang").show();
                        // $("#lbl_BillCode").attr("disabled", true);
                    }
                }
            });
            //判断审批状态
            if (status == "B") {
                $("#Caiwu").show();
            }
            else if (status == "D") {
                $("#Suozhang").show();
            }

            //弹层实现-加载数据
            function LoadAjaxData() {

                var name = $("#HiddenCprName").val();
                var account = $("#lbl_acount").text();
                var inTime = $("#lbl_times").text();
                var unit = $("#HiddenCprUnit").val();
                var data = "action=getFinancial&name=" + name + "&inTime=" + inTime + "&unit=" + unit + "&account=" + account;

                $.ajax({
                    type: "Post",
                    dataType: "json",
                    url: "/HttpHandler/CommHandler.ashx",
                    data: data,
                    success: function (result) {
                        AjaxSuccess_Financial(result);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("系统错误!");
                    }
                });
            }

            //返回成功的 执行方法
            function AjaxSuccess_Financial(result) {
                var data = result == null ? "" : result.ds;

                if (data != "") {
                    $.each(data, function (i, n) {
                        var name = n.Name;
                        if (name.length > 20) {
                            name = name.substr(0, 20) + "...";
                        }
                        var oper = "<span  style=\"cursor: pointer; color: Blue;\" rel='" + n.ID + "' style=\"color:blue;\">选择</span>";
                        var trHtml = "<tr><td width=\"100px\">" + n.FinancialNum + "</td><td width=\"260px\" title=" + n.Name + ">" + name + "</td><td width=\"100px\">" + n.Account + "</td><td width=\"100px\">" + oper + "</td></tr>";
                        $("#customerCompactTable").append(trHtml);
                        $("#customerCompactTable span:last").click(function () {
                            $("#lbl_BillCode").val(n.FinancialNum);
                            $("#Hiddencm_FinancialNum").val(n.ID);
                            $("#chooseCustomerCompact").dialog().dialog("close");
                        });
                    });
                }
                else {
                    var trHtml = "<tr style='color:Red; text-align:center;'><td colspan='4'>无数据!</td></tr>";
                    $("#customerCompactTable").append(trHtml);
                }
            }
        });
    </script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <table class="cls_container" style="width: 100%;">
        <tr>
            <td class="cls_head">
                &nbsp;&nbsp;当前位置：[<asp:Label ID="lbl_status" runat="server" Text=""></asp:Label>确认项目入账]
            </td>
        </tr>
    </table>
    <div class="cls_data">
        <table style="width: 800px; margin: 0 auto;" class="table table-border">
            <tr>
                <td style="width: 450px;">
                    合同名称：<%=CoperationName%>
                </td>
                <td>
                    承接部门：<%=CoperationUnit%>
                </td>
            </tr>
        </table>
        <table style="width: 800px;" class="cls_content_head">
            <tr>
                <td style="width: 50px;" align="center">
                    序号
                </td>
                <td style="width: 100px;" align="center">
                    入账额(万元)
                </td>
                <td style="width: 200px;" align="center">
                    汇款人
                </td>
                <td style="width: 100px;" align="center">
                    入账人
                </td>
                <td style="width: 150px;" align="center">
                    汇款单据号
                </td>
                <td style="width: 100px;" align="center">
                    入账时间
                </td>
                <td style="width: 100px;" align="center">
                    状态
                </td>
            </tr>
        </table>
        <asp:GridView ID="gv_Coperation" runat="server" AutoGenerateColumns="False" ShowHeader="False"
            CssClass="gridView_comm" Font-Size="12px" Width="800px" OnRowDataBound="gv_Coperation_RowDataBound"
            EnableModelValidation="True">
            <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
            <Columns>
                <asp:TemplateField HeaderText="序号">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Container.DataItemIndex+1%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="50px" />
                </asp:TemplateField>
                <asp:BoundField DataField="Acount" HeaderText="入账额">
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="FromUser" HeaderText="汇款人">
                    <ItemStyle Width="200px" />
                </asp:BoundField>
                <asp:BoundField DataField="InAcountUser" HeaderText="入账人">
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="InAcountCode" HeaderText="汇款单据号">
                    <ItemStyle Width="150px" />
                </asp:BoundField>
                <asp:BoundField DataField="InAcountTime" HeaderText="入账时间" DataFormatString="{0:d}">
                    <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Status" HeaderText="状态">
                    <ItemStyle Width="100px" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
        <table class="cls_show_cst_jiben" style="width: 800px; margin: 0 auto;" id="td_input">
            <tr>
                <td style="width: 150px;">
                    入账金额：
                </td>
                <td>
                    <asp:Label ID="lbl_acount" runat="server"></asp:Label>(万元)
                </td>
            </tr>
            <tr>
                <td>
                    汇款人：
                </td>
                <td>
                    <asp:Label ID="lbl_remitter" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    入账时间：
                </td>
                <td>
                    <asp:Label ID="lbl_times" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    入 账 人：
                </td>
                <td class="TextBoxBorder">
                    <asp:Label ID="lbl_user" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    备注：
                </td>
                <td>
                    <asp:Label ID="lbl_mark" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="DanJu">
                <td>
                    入账单据号：
                </td>
                <td class="TextBoxBorder">
                    <%-- <input id="lbl_BillCode" name="txt_keyname" type="text" runat="server" value="" class="cls_input_text_valid"
                        width="250px" />--%>
                    <asp:TextBox ID="lbl_BillCode" runat="server" CssClass="cls_input_text_valid" Width="200px"></asp:TextBox>
                    <span style="cursor: pointer; color: Blue;" id="btn_cprType" runat="server">查询</span>
                    <%--<a
                        href="#" id="btn_cprType">查询</a>--%>
                </td>
            </tr>
            <tr>
                <td>
                    确定入账：
                </td>
                <td class="TextBoxBorder">
                    <asp:RadioButton ID="chk_yes" runat="server" Text="是" Checked="true" GroupName="A" />
                    <asp:RadioButton ID="chk_no" runat="server" Text="否" GroupName="A" />
                </td>
            </tr>
            <tr id="Caiwu" style="display: none;">
                <td>
                    不通过原因：
                </td>
                <td class="TextBoxBorder">
                    <asp:TextBox ID="txt_caiwu" runat="server" MaxLength="100" TextMode="MultiLine" Height="50"
                        Width="300"></asp:TextBox>
                </td>
            </tr>
            <tr id="Suozhang" style="display: none;">
                <td>
                    不通过原因：
                </td>
                <td class="TextBoxBorder">
                    <asp:TextBox ID="txt_suozhang" runat="server" MaxLength="100" TextMode="MultiLine"
                        Height="50" Width="300"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:ImageButton ID="sureButton" runat="server" ImageUrl="~/Images/buttons/btn_save2.gif"
                        OnClick="sureButton_Click" />
                    <a href="javascript:history.back();">
                        <img src="/Images/buttons/btn_back2.gif" alt="返回" style="border: none;" /></a>
                </td>
            </tr>
        </table>
        <table class="cls_show_cst_jiben" style="width: 800px; margin: 0 auto; font-size: 10pt;
            display: none; color:red" id="tb_noDetails">
            <tr>
                <td>
                    入账不通过或消息被删除！
                </td>
            </tr>
            <tr>
                <td align="center">
                    <a href="javascript:history.back();">
                        <img src="/Images/buttons/btn_back2.gif" alt="返回" style="border: none;" /></a>
                </td>
            </tr>
        </table>
    </div>
    <!--入账单据号-->
    <div id="chooseCustomerCompact" style="display: none;">
        <table style="width: 560px;" class="cls_content_head">
            <tr>
                <td style="width: 100px;" align="center">
                    入账单据号
                </td>
                <td style="width: 260px;" align="center">
                    汇款单位
                </td>
                <td style="width: 100px;" align="center">
                    金额
                </td>
                <td style="width: 100px;" align="center">
                    操作
                </td>
            </tr>
        </table>
        <table id="customerCompactTable" class="cls_show_cst_jiben" align="center" style="width: 560px;">
        </table>
    </div>
    <input id="HiddenAuditStatus" type="hidden" value='<%= ChargeAuditStatus %>' />
    <input id="HiddenCprName" type="hidden" value='<%=CoperationName %>' />
    <input id="HiddenCprUnit" type="hidden" value='<%=CoperationUnit %>' />
    <input id="Hiddencm_FinancialNum" type="hidden" runat="server" />
  
    <input type="hidden" id="hid_flag" runat="Server" value="0"/> 
    </form>
</body>
</html>
