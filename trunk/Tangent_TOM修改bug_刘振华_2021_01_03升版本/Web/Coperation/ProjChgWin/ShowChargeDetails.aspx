﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowChargeDetails.aspx.cs"
    Inherits="TG.Web.Coperation.ProjChgWin.ShowChargeDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../../css/Corperation.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../js/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="../../js/Coperation/ProjChgWin/AddProjCharge.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <fieldset>
        <legend>收费列表详细</legend>
        <div class="cls_data">
            <table style="width: 630px;" class="cls_content_head">
                <tr>
                    <td style="width: 50px;" align="center">
                        序号
                    </td>
                    <td style="width: 90px;" align="center">
                        入账额(万元)
                    </td>
                    <td style="width: 90px;" align="center">
                        比例
                    </td>
                    <td style="width: 80px;" align="center">
                        汇款人
                    </td>
                    <td style="width: 80px;" align="center">
                        入账人
                    </td>
                    <td style="width: 80px;" align="center">
                        收款时间
                    </td>
                    <td style="width: 70px;" align="center">
                        状态
                    </td>
                    <td style="width: 90px;" align="center">
                        备注
                    </td>
                </tr>
            </table>
             <asp:GridView ID="gv_Coperation" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                Font-Size="12px" Width="630px" OnRowDataBound="gv_Coperation_RowDataBound" >
                <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                <Columns>
                    <asp:TemplateField HeaderText="序号">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Container.DataItemIndex+1%>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="50px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Acount" HeaderText="入账额">
                        <ItemStyle Width="90px" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lbl_persent" runat="server" Text="0.00%"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="90px" />
                    </asp:TemplateField>
                  
                    <asp:BoundField DataField="FromUser" HeaderText="汇款人">
                        <ItemStyle Width="80px" />
                    </asp:BoundField>
                      <asp:BoundField DataField="InAcountUser" HeaderText="入账人">
                        <ItemStyle Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="InAcountTime" HeaderText="时间" DataFormatString="{0:d}">
                        <ItemStyle Width="80px" />
                    </asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="状态">
                        <ItemStyle Width="70px" />
                    </asp:BoundField>
                        <asp:BoundField DataField="ProcessMark" HeaderText="备注">
                        <ItemStyle Width="90px" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataTemplate>
                    无收款记录！
                </EmptyDataTemplate>
            </asp:GridView>
        </div>
    </fieldset>
    </form>
</body>
</html>
