﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddProjCharge.aspx.cs" Inherits="TG.Web.Coperation.ProjChgWin.AddProjCharge" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <base target="_self" />
    <link href="../../css/m_comm.css" rel="stylesheet" type="text/css" />

    <link href="../../css/Corperation.css" rel="stylesheet" type="text/css" />

     <script type="text/javascript" src="../../js/jquery-1.8.0.min.js"></script>

     <script type="text/javascript" src="../../js/Coperation/ProjChgWin/AddProjCharge.js"></script>

     <script type="text/javascript" src="../../js/wdate/WdatePicker.js"></script>
</head>
<body bgcolor="f0f0f0">
    <form id="form1" runat="server">
    <fieldset>
        <legend>收费列表</legend>
        <div  class="cls_data">
    <table style="width: 630px;" class="cls_content_head">
                <tr>
                    <td style="width: 50px;" align="center">序号
                    </td>
                    <td style="width: 90px;" align="center">入账额(万元)
                    </td>
                    <td style="width: 90px;" align="center">比例
                    </td>
                    <td style="width: 80px;" align="center">汇款人
                    </td>
                    <td style="width: 80px;" align="center">入账人
                    </td>
                    <td style="width: 110px;" align="center">收款时间
                    </td>
                    <td style="width: 70px;" align="center">状态
                    </td>
                    <td style="width: 60px;" align="center">操作</td>
                </tr>
            </table>
                <asp:GridView ID="gv_Coperation" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                Font-Size="12px" Width="630px" OnRowDataBound="gv_Coperation_RowDataBound" >
                <RowStyle HorizontalAlign="Center" Height="22px"></RowStyle>
                <Columns>
                    <asp:TemplateField HeaderText="序号">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Container.DataItemIndex+1%>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="50px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Acount" HeaderText="入账额">
                        <ItemStyle Width="90px" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label ID="lbl_persent" runat="server" Text="0.00%"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="90px" />
                    </asp:TemplateField>
                  
                    <asp:BoundField DataField="FromUser" HeaderText="汇款人">
                        <ItemStyle Width="80px" />
                    </asp:BoundField>
                      <asp:BoundField DataField="InAcountUser" HeaderText="入账人">
                        <ItemStyle Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="InAcountTime" HeaderText="时间" DataFormatString="{0:d}">
                        <ItemStyle Width="110px" />
                    </asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="状态">
                        <ItemStyle Width="70px" />
                    </asp:BoundField>
                      <asp:TemplateField HeaderText="操作">
                          <ItemTemplate>
                              <a href="###" alt='<%# Eval("ID") %>' allowedit='<%# Eval("Status") %>' class='cls_edit'>修改</a>|<a href='###' allowdel='<%# Eval("Status") %>'  alt='<%# Eval("ID") %>' class='cls_del'>删除</a>
                          </ItemTemplate>
                          <ItemStyle Width="60px" />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    无收款记录！
                </EmptyDataTemplate>
            </asp:GridView>
    </div>
    </fieldset>
    &nbsp;&nbsp;<input type="button" id='btn_showadd' value='显示收费' style='width:65px;height:25px;display:none;'/>
    <fieldset id="ft_add">
        <legend>项目收费</legend>
        <table class="cls_show_cst_jiben" style="width: 630px; margin: 0 auto;"
                id="td_input">
                <tr>
                    <td style="width: 100px;">项目名称：
                    </td>
                    <td>
                        <asp:Label ID="lbl_projname" runat="server"></asp:Label>
                        <span id="txt_inputtimes"></span>
                    </td>
                </tr>

                <tr>
                    <td style="width: 100px;">承接部门：</td>
                    <td>
                        <asp:Label ID="lbl_unit" runat="server" Text=""></asp:Label></td>
                </tr>

                <tr>
                    <td style="width: 100px;">项目总金额：</td>
                    <td>
                        <asp:Label ID="lbl_allcount" runat="server"></asp:Label>
                        (万元)</td>
                </tr>

                <tr>
                    <td>未支付金额：
                    </td>
                    <td>
                        <asp:Label ID="lbl_nopaycount" runat="server"></asp:Label>
                        (万元)</td>
                </tr>

                <tr>
                    <td>入账金额：
                    </td>
                    <td>
                        <input id="txt_payCount" maxlength="15" type="text" class="TextBoxBorder" />(万元)<span
                            style="color: red; display: none;" id="txt_paycount_valide">请输入数字！</span>
                    </td>
                </tr>
                <tr>
                    <td>汇款人：
                    </td>
                    <td>
                        <input id="txt_remitter" maxlength="50" type="text" class="TextBoxBorder" />
                        <span  style="color: red; display: none;" id="txt_remitter_valide">请输入汇款人！</span>
                        </td>
                </tr>
                <tr>
                    <td>入账时间：
                    </td>
                    <td>
                        <input id="txt_times" type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" />
                        <span style="color: red; display: none;" id="txt_time_valide">请输入入账时间！</span></td>
                </tr>
                <tr>
                    <td>备注：
                    </td>
                    <td>
                        <textarea maxlength="150" id="txt_mark" class="TextBoxBorder" style="width: 400px; height: 60px;"></textarea></td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <a href="###" id="btn_addcount">
                            <img src="../../Images/buttons/btn_shoukuan.gif" style="border: none;" /></a>
                    </td>
                </tr>
            </table>
    </fieldset>
    
    <fieldset id="ft_edit" style="display:none;">
        <legend>修改收费</legend>
        <table class="cls_show_cst_jiben" style="width: 630px; margin: 0 auto;"
                id="td_edit">
                <tr>
                <td style="width: 100px;">项目名称：</td>
                <td>
                <asp:Label ID="lbl_projname2" runat="server"></asp:Label></td>
                </tr>
                <tr>
                <td style="width: 100px;">项目总金额：</td>
                <td>
                    <asp:Label ID="lbl_allcount2" runat="server"></asp:Label>
                        (万元)</td>
                </tr>
                <tr>
                <td style="width: 100px;">本次入账金额：</td>
                <td>
                    <input id="txt_curpaycount" maxlength="15" type="text" class="TextBoxBorder" />(万元)
                    <span
                            style="color: red; display: none;" id="sp_curpaycount">请输入数字！</span></td>
                </tr>
                <tr>
                <td style="width: 100px;">汇款人：</td>
                <td>
                    <input id="txt_remitter2" maxlength="50" type="text" class="TextBoxBorder" />
                    <span  style="color: red; display: none;" id="sp_remitter2">请输入汇款人！</span></td>
                </tr>
                <tr>
                <td style="width: 100px;">入账时间：</td>
                <td>
                    <input id="txt_paytime2" type="text" class="Wdate" onclick="WdatePicker({ readOnly: true })" />
                    <span style="color: red; display: none;" id="sp_paytime2">请输入入账时间！</span></td>
                </tr>
                <tr>
                <td style="width: 100px;">备注：</td>
                <td>
                    <textarea maxlength="150" id="txt_mark2" class="TextBoxBorder" style="width: 400px; height: 60px;"></textarea></td>
                </tr>
                <tr>
                <td colspan="2" align="center">
                        <a href="###" id="btn_addcount0">
                            <img src="../../Images/buttons/btn_shoukuan.gif" style="border: none;" /></a></td>
                </tr>
                </table>
    </fieldset>
    <!-- 项目ID -->
    <asp:HiddenField ID="hid_projID" runat="server" Value="0"/>
    <!-- 是否已经收完款 -->
     <!-- <asp:HiddenField ID="hid_iscomplete" runat="server" Value="0" /> -->
    <!-- 当前用户 -->
    <asp:HiddenField ID="hid_curuser" runat="server" Value="0"/>
    <!-- 修改收款，收款ID -->
    <input type="hidden" id="hid_chgid" value='0'/>
    <!-- 修改前的付款额度 -->
    <input type="hidden" id="hid_tempnopay" value='0' />
    <!-- 刷新 -->
    <a href="#" style='display:none;' id='reload'></a>
    </form>
</body>
</html>
