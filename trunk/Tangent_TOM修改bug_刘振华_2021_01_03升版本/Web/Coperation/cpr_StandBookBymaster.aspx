﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_StandBookBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_StandBookBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/ReportAndAllotTable.css" rel="stylesheet" type="text/css" />
    <link href="../css/Corperation.css" rel="stylesheet" type="text/css" />
    <link href="../css/tipsy/tipsy.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .cls_chart {
            font-size: 9pt;
            width: 100%;
        }

        .cls_nav {
            width: 70px;
        }

            .cls_nav a {
                text-decoration: none;
            }

        .show_projectNumber {
            width: 860px;
            border: solid 1px #CCC;
            border-collapse: collapse;
        }

            .show_projectNumber td {
                border: solid 1px #CCC;
                font-size: 9pt;
                font-family: "微软雅黑";
                height: 20px;
            }

        .cls_show_cst_jiben_2 {
            border-collapse: collapse;
            border: solid 0px black;
            font-size: 9pt;
        }


        #labTime {
            float: right;
            font-size: 12px;
            font-family: 微软雅黑;
        }

        #labDanW {
            float: right;
            margin-right: 10px;
            font-size: 12px;
            font-family: 微软雅黑;
        }

        .cls_GridView_Style2 .cls_Column_Short {
            overflow: hidden;
            text-overflow: ellipsis;
            -o-text-overflow: ellipsis;
            white-space: nowrap;
        }
    </style>
    <script type="text/javascript" src="../js/FunctionChart/FusionCharts.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    <script src="../js/Coperation/cpr_StandBook.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">领导驾驶舱 <small>合同收费情况</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>领导驾驶舱</a><i class="fa fa-angle-right"> </i><a>生产经营统计报表</a><i class="fa fa-angle-right"> </i>
        <a>合同收费情况</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>查询合同收费情况
                    </div>
                    <div class="actions">
                        <asp:Button ID="btn_report" runat="server" Text="导出" CssClass="btn red btn-sm"
                            OnClick="btn_report_Click" />
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">
                    <table class="table-responsive">
                        <tr>
                            <td>生产部门:</td>
                            <td>
                                <asp:DropDownList ID="drp_unit" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">-----全院部门-----</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>合同年份:</td>
                            <td>
                                <asp:DropDownList ID="drp_year" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem Value="-1">---选择年份---</asp:ListItem>
                                </asp:DropDownList></td>
                            <td>项目名称:</td>
                            <td>
                                <input type="text" runat="server" class="form-control input-sm" value="" id="txt_cprName" /></td>
                            <td>
                                <input type="button" class="btn blue" value="查询" id="btn_ok" /></td>
                            <td>
                                <asp:HiddenField ID="hidnowmonth" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>合同收费情况列表
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="cls_Container_Report">
                                    <div class="cls_Container_Tip">
                                        <label id="title">
                                        </label>
                                        <label id="unit"></label>
                                        设计合同及合同收费情况
                                    </div>
                                    <div style="margin: 0 auto; width: 99%;">
                                        <table id="mytab" runat="server" class="table table-bordered" style="width: 99%;">
                                            <tr>
                                                <td style="width: 3%;">序号
                                                </td>
                                                <td style="width: 9%;">合同编号
                                                </td>
                                                <td style="width: 21%;" align="left">项目名称
                                                </td>
                                                <td style="width: 6%;">面积(㎡)
                                                </td>
                                                <td style="width: 9%;">合同额(万元)
                                                </td>
                                                <td style="width: 9%;">已收费(万元)
                                                </td>
                                                <td style="width: 9%;">尚欠(万元)
                                                </td>
                                                <td style="width: 16%;">收款日期
                                                </td>
                                                <td style="width: 6%;">工程阶段
                                                </td>
                                                <td style="width: 6%;">合同阶段
                                                </td>
                                                <td style="width: 6%;">备 注
                                                </td>
                                            </tr>
                                        </table>
                                        <table id="AddTable" class="table table-bordered table-data" style="width: 99%;">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" runat="Server" id="userShortName" value="" />
    <input type="hidden" runat="Server" id="previewPower" value="" />
    <input type="hidden" runat="Server" id="userSysNum" value="" />
    <input type="hidden" runat="Server" id="userUnitNum" value="" />
</asp:Content>
