﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_CoperationEditChooseBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_CoperationEditChooseBymaster" %>

<%@ Register Assembly="ASTreeView" Namespace="Geekees.Common.Controls" TagPrefix="cc1" %>
<%@ Register Src="../UserControl/ChooseCustomer.ascx" TagName="ChooseCustomer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="../css/m_comm.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" />
    <link href="../css/swfupload/default_cpr.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/astreeview/astreeview.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/contextmenu/contextmenu.css" rel="stylesheet" type="text/css" />
    <link href="../js/astreeview/asdropdowntreeview/dropdowntreeview.css" rel="stylesheet"
        type="text/css" />
    <link href="/css/smoothness/jquery-ui-1.8.20.custom.css" rel="stylesheet" />
    <script src="../js/astreeview/astreeview/astreeview_packed.js" type="text/javascript"></script>
    <script src="../js/astreeview/contextmenu/contextmenu_packed.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/swfupload/swfupload.js"></script>
    <script type="text/javascript" src="../js/swfupload/handlers_green.js"></script>
    <script src="../js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="../js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.alerts.js"></script>
    <script type="text/javascript" src="../js/wdate/WdatePicker.js"></script>
    <script src="../js/Jquery-extend.js" type="text/javascript"></script>
    <script src="../js/UserControl/ChooseCustomer.js" type="text/javascript"></script>
    <script src="/js/jQuery-Plugs.js" type="text/javascript"></script>
    <script src="../js/Common/SendMessageCommon.js" type="text/javascript"></script>
    <script src="../js/Common/ShowDivDialog.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/MessageComm.js"></script>
    <script src="../js/Coperation/cpr_CoperationEditChoose.js" type="text/javascript"></script>
    <script type="text/javascript">   
        //判断是否选中节点
        var nodecount;
        var nodeLenght;
        function IsStructCheckNode(obj) {
            nodecount = 0;
            nodeLenght="";
           
            if (obj == 'struct') {
                <%= asTreeviewStructObjID %>.traverseTreeNode(displayNodeFun);
                <%= asTreeviewStructObjID %>.traverseTreeNode(displayNodeFuns);
            }
            else if (obj == 'structtype') {
                <%= asTreeviewStructTypeObjID %>.traverseTreeNode(displayNodeFun);
                    <%= asTreeviewStructTypeObjID %>.traverseTreeNode(displayNodeFuns);
                }
            
       
            if (nodecount > 0 ) {
                if (nodeLenght.length<500) {
                    return true;
                }else{
                    return false;
                }            
            }
            else {
                return false;
            }
        }
        //选中与半选中  qpl 20140115
        function displayNodeFun(elem) {
            if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
                nodecount++;
            }
        }
        function displayNodeFuns(elem) {
            if (elem.getAttribute("checkedState") == "0"||elem.getAttribute("checkedState") == "1") {
                nodeLenght+=  elem.getElementsByTagName("checkedState");
            }
        }
 
        function ChooseNodesValue()
        {
            var result="<%= GetDropDownTreeCheckedValue()%>"; 
            return result;
      
        }
        function displayNodeFun(elem) {
            if (elem.getAttribute("checkedState") == "0") {
                nodecount++;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>合同修改申请</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a>合同信息管理</a><i class="fa fa-angle-right"> </i><a>合同管理</a><i class="fa fa-angle-right"> </i><a>合同修改申请</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>请选择修改项
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <div class="row">
                        <div class="col-md-12">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td align="left">
                                        <input id="cpr_No" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_No">合同编号</span>
                                    </td>
                                    <td align="left">
                                        <input id="ddcpr_Type" name="bb" class="cls_audit" type="checkbox" /><span for="ddcpr_Type">合同分类</span>
                                    </td>
                                    <td align="left">
                                        <input id="cprName" name="bb" class="cls_audit" type="checkbox" /><span for="cprName">合同名称</span>
                                    </td>
                                    <td align="left">
                                        <input id="buildArea" name="bb" class="cls_audit" type="checkbox" /><span for="buildArea">建筑面积</span>
                                    </td>
                                    <td align="left">
                                        <input id="cprBuildUnit" name="bb" class="cls_audit" type="checkbox" /><span for="cprBuildUnit">建设单位</span>
                                    </td>
                                    <td align="left">
                                        <input id="cprType" name="bb" class="cls_audit" type="checkbox" /><span for="cprType">合同类型</span>
                                    </td>
                                    <td align="left">
                                        <input id="StructType" name="bb" class="cls_audit" type="checkbox" /><span for="StructType">结构形式</span>
                                    </td>
                                    <td align="left">
                                        <input id="BuildStructType" name="bb" class="cls_audit" type="checkbox" /><span for="BuildStructType">建筑分类</span>
                                    </td>
                                    <td align="left">
                                        <input id="buildType" name="bb" class="cls_audit" type="checkbox" /><span for="buildType">建筑类别</span>
                                    </td>
                                    <td align="left">
                                        <input id="proFuze" name="bb" class="cls_audit" type="checkbox" /><span for="proFuze">工程负责人</span>
                                    </td>
                                    <td align="left">
                                        <input id="floor" name="bb" class="cls_audit" type="checkbox" /><span for="floor">层数</span>
                                    </td>
                                    <td align="left">
                                        <input id="FParty" name="bb" class="cls_audit" type="checkbox" /><span for="FParty">甲方负责人</span>
                                    </td>
                                    <td align="left">
                                        <input id="cjbm" name="bb" class="cls_audit" type="checkbox" /><span for="cjbm">承接部门</span>
                                    </td>
                                      <td align="left">
                                        <input id="cpr_Account" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Account">合同额</span>
                                    </td>
                                </tr>
                                <tr>                                  
                                    <td align="left">
                                        <input id="InvestAccount" name="bb" class="cls_audit" type="checkbox" /><span for="InvestAccount">投资额</span>
                                    </td>
                                    <td align="left">
                                        <input id="ddProjectPosition" name="bb" class="cls_audit" type="checkbox" /><span
                                            for="ddProjectPosition">工程地点</span>
                                    </td>
                                    <td align="left">
                                        <input id="cpr_Account0" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Account0">实际合同额</span>
                                    </td>
                                    <td align="left">
                                        <input id="InvestAccount0" name="bb" class="cls_audit" type="checkbox" /><span for="InvestAccount0">实际投资额</span>
                                    </td>
                                    <td align="left">
                                        <!---->
                                        <input id="ddProfessionType" name="bb" class="cls_audit" type="checkbox" /><span
                                            for="ddProfessionType">行业性质</span>
                                    </td>
                                    <td align="left">
                                        <input id="ddcpr_Stage" name="bb" class="cls_audit" type="checkbox" /><span for="ddcpr_Stage">合同阶段</span>
                                    </td>
                                    <td align="left">
                                        <input id="tbcreate" name="bb" class="cls_audit" type="checkbox" /><span for="tbcreate">制表人</span>
                                    </td>
                                    <td align="left">
                                        <input id="ddSourceWay" name="bb" class="cls_audit" type="checkbox" /><span for="ddSourceWay">工程来源</span>
                                    </td>
                                    <td align="left">
                                        <input id="SingnDate2" name="bb" class="cls_audit" type="checkbox" /><span for="SingnDate2">合同签订日期</span>
                                    </td>
                                    <td align="left">
                                        <input id="CompleteDate" name="bb" class="cls_audit" type="checkbox" /><span for="CompleteDate">合同完成日期</span>
                                    </td>
                                     <td align="left">
                                        <input id="SingnDate" name="bb" class="cls_audit" type="checkbox" /><span for="SingnDate">合同统计年份</span>
                                    </td>
                                    <td align="left">
                                        <input id="multibuild" name="bb" class="cls_audit" type="checkbox" /><span for="multibuild">多栋楼</span>
                                    </td>
                                    <td align="left">
                                        <input id="cpr_Remark" name="bb" class="cls_audit" type="checkbox" /><span for="cpr_Remark">合同备注</span>
                                    </td>
                                    <td align="left">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>修改合同信息
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;">

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover" align="center" style="width: 100%;"
                                id="EditDetail">
                                <tr option="cpr_No" style="display:none;">
                                    <td style="width: 80px;">合同编号：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txtcpr_No" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hid_cprid" runat="server" />
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txtcpr_NoEdit" runat="server" CssClass="form-control input-sm"
                                                MaxLength="25"></asp:TextBox>

                                        </div>
                                        <div class="col-md-2">
                                            <a href="#CprNo" id="btn_getcprnum" class="btn blue btn-sm" data-toggle="modal"><i
                                                class="fa fa-search"></i></a>
                                        </div>
                                        <asp:HiddenField ID="hid_cprno" runat="server" Value="" />
                                        <asp:HiddenField ID="hid_cprtime" runat="server" Value="" />
                                    </td>
                                </tr>
                                <tr option="ddcpr_Type" style="display:none;">
                                    <td style="width: 80px;">合同分类：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_ddcpr_Type" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 400px;">
                                        <div class="col-md-5">
                                            <asp:DropDownList ID="txt_ddcpr_TypeEdit" runat="Server" AppendDataBoundItems="True"
                                                CssClass="form-control input-sm">
                                                <asp:ListItem Value="-1">----选择合同类别----</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr option="cprName" style="display:none;">
                                    <td style="width: 80px;">合同名称：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_cprName" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txt_cprNameEdit" runat="server" CssClass="form-control input-sm"
                                                MaxLength="100"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">建筑规模：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_buildArea" runat="server"></asp:Label>(㎡)
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-5">
                                            <asp:TextBox ID="txt_buildAreaEdit" runat="server" CssClass="form-control input-sm"
                                                MaxLength="25"></asp:TextBox>
                                        </div>
                                        <div class="col-md-1">
                                            (<span style="font-size: 10pt">㎡</span>)
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">建设单位：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_cprBuildUnit" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txt_cprBuildUnitEdit" runat="server" CssClass="form-control input-sm"
                                                MaxLength="50"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">合同类型：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_cprType" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txt_cprTypeEdit" runat="server" CssClass="form-control input-sm"
                                                MaxLength="25" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="#HTLX" id="btn_cprType" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">结构形式：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="struct_tree">
                                            <asp:Literal ID="lbl_StructType" runat="server"></asp:Literal>
                                        </div>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <cc1:ASDropDownTreeView ID="asTreeviewStructEdit" runat="server" BasePath="~/js/astreeview/astreeview/"
                                            DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                            EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                            EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="----------请选择结构形式----------"
                                            Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                            RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                            DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                            DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                            Font-Size="12px" Font-Strikeout="False" EnableTheme="true" />
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">建筑分类：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="struct_tree">
                                            <asp:Literal ID="lbl_BuildStructType" runat="server"></asp:Literal>
                                        </div>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <cc1:ASDropDownTreeView ID="asTreeviewStructTypeEdit" runat="server" BasePath="~/js/astreeview/astreeview/"
                                            DataTableRootNodeValue="0" EnableRoot="false" EnableNodeSelection="false" EnableCheckbox="true"
                                            EnableDragDrop="true" EnableTreeLines="true" EnableNodeIcon="false" EnableCustomizedNodeIcon="false"
                                            EnableDebugMode="false" EnableRequiredValidator="true" InitialDropdownText="----------请选择建筑分类----------"
                                            Width="200px" EnableCloseOnOutsideClick="true" EnableHalfCheckedAsChecked="true"
                                            RequiredValidatorValidationGroup="vgCheck" EnableContextMenuAdd="false" DropdownIconDown="~/js/astreeview/asdropdowntreeview/images/windropdown.gif"
                                            DropdownIconDownDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                            DropdownIconUp="~/js/astreeview/asdropdowntreeview/images/windropdown.gif" DropdownIconUpDisabled="~/js/astreeview/asdropdowntreeview/images/windropdown-disabled.gif"
                                            Font-Size="12px" Font-Strikeout="False" EnableTheme="true" />
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">建筑类别：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_buildType" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-5">
                                            <asp:DropDownList ID="drp_buildtype" runat="server" AppendDataBoundItems="true" CssClass="form-control input-sm">
                                                <asp:ListItem Value="-1">------选择类别------</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">工程负责人：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_proFuze" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txt_proFuzeEdit" runat="server" CssClass="form-control input-sm"
                                                ReadOnly="true" MaxLength="10"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                            <input id="HiddenPMUserID" type="hidden" value="0" runat="Server" />
                                            <a href="#PMName" id="btn_gcfz" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">电话：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_fzphone" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txt_fzphoneEdit" runat="server" CssClass="form-control input-sm"
                                                MaxLength="15"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">层数：
                                    </td>
                                    <td style="width: 300px;">地上：<asp:Label ID="lbl_upfloor" runat="server" Text="0"></asp:Label>
                                        层 地下：<asp:Label ID="lbl_downfloor" runat="server" Text="0"></asp:Label>
                                        层
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_upfloor" runat="server" placeholder="地上" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_downfloor" runat="server" placeholder="地下" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">甲方负责人：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txtFParty" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txtFPartyEdit" runat="server" CssClass="form-control input-sm" MaxLength="10"
                                                ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="#JFFZDiv" id="btn_jffz" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">电话：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_jiafphone" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txt_jiafphoneEdit" runat="server" CssClass="form-control input-sm"
                                                MaxLength="15"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">承接部门：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_cjbm" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txt_cjbmEdit" runat="server" CssClass="form-control input-sm" Width="120px"
                                                MaxLength="25" ReadOnly="true"></asp:TextBox><asp:HiddenField ID="hid_cjbm" runat="server"
                                                    Value=""></asp:HiddenField>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="#CJBMDiv" id="btn_cjbm" class="btn blue btn-sm" data-toggle="modal"><i class="fa fa-search"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">合同额：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txtcpr_Account" runat="server"></asp:Label>万元
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txtcpr_AccountEdit" runat="server" CssClass="form-control input-sm"
                                                MaxLength="12"></asp:TextBox>
                                        </div>
                                        万元
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">投资额：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txtInvestAccount" runat="server"></asp:Label>万元
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txtInvestAccountEdit" runat="server" CssClass="form-control input-sm"
                                                MaxLength="12"></asp:TextBox>
                                        </div>
                                        万元
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">工程地点：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_ddProjectPosition" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txt_ProjectPosition" MaxLength="25" runat="server" CssClass="form-control input-sm"
                                                Width="120px"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">实际合同额：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txtcpr_Account0" runat="server"></asp:Label>万元
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txtcpr_AccountEdit0" runat="server" CssClass="form-control input-sm"
                                                MaxLength="12"></asp:TextBox>
                                        </div>
                                        万元
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">实际投资额：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txtInvestAccount0" runat="server"></asp:Label>万元
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txtInvestAccountEdit0" runat="server" CssClass="form-control input-sm"
                                                MaxLength="12"></asp:TextBox>
                                        </div>
                                        万元
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">行业性质：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_ddProfessionType" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-5">
                                            <asp:DropDownList ID="txt_ddProfessionTypeEdit" runat="Server" AppendDataBoundItems="True"
                                                CssClass="form-control input-sm">
                                                <asp:ListItem Value=" ">--请选择--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">合同阶段：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_ddcpr_Stage" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:CheckBoxList ID="chk_cprjd" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">制表人：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_tbcreate" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-10">
                                            <asp:TextBox ID="txt_tbcreateEdit" runat="server" CssClass="form-control input-sm"
                                                Width="120px" MaxLength="10"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">工程来源：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txt_ddSourceWay" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-5">
                                            <asp:DropDownList ID="txt_ddSourceWayEdit" runat="Server" Width="120px" AppendDataBoundItems="True"
                                                CssClass="form-control input-sm">
                                                <asp:ListItem Value=" ">--请选择--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">合同签订日期：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txtSingnDate2" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <input type="text" name="txt_date2" id="txtSingnDateEdit2" onclick="WdatePicker({ readOnly: true})"
                                            class="Wdate" runat="Server" size="20" style="width: 120px; height: 30px; background-color: #FFC;" />
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">合同完成日期：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txtCompleteDate" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <input type="text" name="txt_date1" id="txtCompleteDateEdit" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" size="20" style="width: 120px; height: 30px; background-color: #FFC;" />
                                    </td>
                                </tr>
                                 <tr style="display:none;">
                                    <td style="width: 80px;">合同统计年份：
                                    </td>
                                    <td style="width: 300px;">
                                        <asp:Label ID="txtSingnDate" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <input type="text" name="txt_date1" id="txtSingnDateEdit" onclick="WdatePicker({ readOnly: true })"
                                            class="Wdate" runat="Server" size="20" style="width: 120px; height: 30px; background-color: #FFC;" />
                                    </td>
                                </tr>
                                 
                                <tr style="display:none;">
                                    <td style="width: 80px;">多栋楼：
                                    </td>
                                    <td style="width: 300px;">
                                        <span id="txt_multibuild" runat="server" style="width: 400px; height: 80px;"></span>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txt_multibuildEdit" runat="server" Height="80px" TextMode="MultiLine"
                                                CssClass="form-control input-sm" MaxLength="300"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td style="width: 80px;">合同备注：
                                    </td>
                                    <td style="width: 300px;">
                                        <span id="txtcpr_Remark" runat="server" style="width: 400px; height: 60px;"></span>
                                    </td>
                                    <td style="width: 80px;">修改后：
                                    </td>
                                    <td style="width: 300px;">
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtcpr_RemarkEdit" runat="server" Height="60px" TextMode="MultiLine"
                                                CssClass="form-control input-sm" MaxLength="300"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-12 col-md-12">
                            <input type="button" value="提交申请" id="btnsub" href="#AuditUser" data-toggle="modal"
                                class="btn blue" />
                            <input type="button" value="返回" id="btncancl" class="btn btn-default" />
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="previewPower" Value="" />
    <asp:HiddenField runat="server" ID="userSysNum" Value="" />
    <asp:HiddenField runat="server" ID="userUnitNum" Value="" />
    <asp:HiddenField runat="server" ID="userShortName" Value="" />
    <asp:HiddenField runat="server" ID="NotShowUnitList" Value="" />
    <input type="hidden" id="HiddenCoperationSysNo" value="<%=CoperationSysno %>" />
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <asp:LinkButton runat="server" ID="output" OnClick="output_Click"></asp:LinkButton>
    <!--用户控件-->
    <div id="AuditUser" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!-- 选择客户弹出层 -->
    <div id="CustomerList" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">查询客户</h4>
        </div>
        <div class="modal-body">
            <div id="chooseCustomerContainer">
                <uc1:ChooseCustomer ID="ChooseCustomer1" runat="server" PageSize="15" UserSysNo="0"
                    PreviewPower="0" />
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--选择合同编号弹出层-->
    <div id="CprNo" class="modal fade yellow" tabindex="-1" data-width="350" aria-hidden="true"
        style="display: none; width: 350px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">查询编号</h4>
        </div>
        <div class="modal-body">
            <div id="cpr_Number">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td>合同类型</td>
                                <td>
                                    <select id="cpr_typeSelect" class="form-control input-sm">
                                    </select></td>
                            </tr>
                            <tr>
                                <td>合同编号</td>
                                <td>
                                    <select id="cpr_numSelect" class="form-control input-sm">
                                    </select><span id="noselectMsg" class="valide"></span></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="btn_cprNum_close" class="btn green btn-default">
                确定</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <!--项目经理弹出层-->
    <div id="PMName" class="modal fade yellow" tabindex="-1" data-width="400" aria-hidden="true"
        style="display: none; width: 400; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">工程负责人</h4>
        </div>
        <div class="modal-body">
            <div id="gcFzr_Dialog">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>生产部门:</td>
                                    <td>
                                        <select id="select_gcFzr_Unit" class="from-control input-sm">
                                        </select></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table id="gcFzr_MemTable" class="table table-bordered"
                            style="text-align: center">
                            <tr class="trBackColor">
                                <td style="width: 60px;">序号
                                </td>
                                <td style="width: 180px;">人员名称
                                </td>
                                <td align="center" style="width: 60px;">操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="gcFzr_ForPageDiv" class="divNavigation pageDivPosition">
                            总<label id="gcfzr_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="gcfzr_nowPageIndex">0</label>/<label id="gcfzr_allPageCount">0</label>页&nbsp;&nbsp;
                            <span id="gcfzr_firstPage">首页</span>&nbsp; <span id="gcfzr_prevPage">&lt;&lt;</span>&nbsp;
                            <span id="gcfzr_nextPage">&gt;&gt;</span>&nbsp; <span id="gcfzr_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="gcfzr_pageIndex" style="height: 3; width: 20px; border-style: none none solid
    none;" />
                            页&nbsp;&nbsp; <span id="gcfzr_gotoPageIndex">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn
    btn-default">
                关闭</button>
        </div>
    </div>
    <!-- 承接部门层 -->
    <div id="CJBMDiv" class="modal fade yellow" tabindex="-1" data-width="400" aria-hidden="true"
        style="display: none; width: 400px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">承接部门</h4>
        </div>
        <div class="modal-body">
            <div id="pro_cjbmDiv">
                <div class="row">
                    <div class="col-md-12">
                        <table id="pro_cjbmTable" class="table table-bordered"
                            align="center">
                            <tr class="trBackColor">
                                <td style="width: 60px;" align="center">序号
                                </td>
                                <td style="width: 180px;" align="center">单位名称
                                </td>
                                <td align="center" style="width: 60px;">操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <div id="cjbmByPageDiv" class="divNavigation pageDivPosition">
                            总<label id="cjbm_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="cjbm_nowPageIndex">0</label>/<label id="cjbm_allPageCount">0</label>页&nbsp;&nbsp;
                            <span id="cjbm_firstPage">首页</span>&nbsp; <span id="cjbm_prevPage">&lt;&lt;</span>&nbsp;
                            <span id="cjbm_nextPage">&gt;&gt;</span>&nbsp; <span id="cjbm_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="cjbm_pageIndex" style="height: 3; width: 20px; border-style: none none solid
    none;" />
                            页&nbsp;&nbsp; <span id="cjbm_gotoPageIndex">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn
    btn-default">
                关闭</button>
        </div>
    </div>
    <!-- 甲方负责人 -->
    <div id="JFFZDiv" class="modal fade yellow" tabindex="-1" data-width="760" aria-hidden="true"
        style="display: none; width: 760px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">甲方负责人</h4>
        </div>
        <div class="modal-body">
            <div id="jffzr_dialogDiv">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td>客户列表</td>
                                    <td>
                                        <select id="select_jffzrMem" style="width: 350px;" class="form-control input-sm"></select>
                                        <span id="noCustMsg" class="valide">无数据!请先添加客户!</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table id="jffzr_dataTable" class="table table-bordered"
                            align="center">
                            <tr class="trBackColor">
                                <td>序号
                                </td>
                                <td>姓名
                                </td>
                                <td>职务
                                </td>
                                <td>部门
                                </td>
                                <td>商务电话
                                </td>
                                <td>操作
                                </td>
                            </tr>
                        </table>
                        <br />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="jffzrByPageDiv" class="divNavigation pageDivPosition">
                            总<label id="jffzr_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
                            第<label id="jffzr_nowPageIndex">0</label>/<label id="jffzr_allPageCount">0</label>页&nbsp;&nbsp;
                            <span id="jffzr_firstPage">首页</span>&nbsp; <span id="jffzr_prevPage">&lt;&lt;</span>&nbsp;
                            <span id="jffzr_nextPage">&gt;&gt;</span>&nbsp; <span id="jffzr_lastPage">末页</span>&nbsp;&nbsp;&nbsp;&nbsp;
                            跳至<input type="text" id="jffzr_pageIndex" style="height: 3; width: 20px; border-style: none none solid
    none;" />页&nbsp;&nbsp; <span id="jffzr_gotoPageIndex">GO</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn
    btn-default">
                关闭</button>
        </div>
    </div>
    <!--合同类型层-->
    <div id="HTLX" class="modal fade yellow" tabindex="-1" data-width="450" aria-hidden="true"
        style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">选择合同类型</h4>
        </div>
        <div class="modal-body">
            <div id="chooseCustomerCompact">
                <div class="row">
                    <div class="col-md-12">
                        <table id="customerCompactTable" class="table table-bordered"
                            style="text-align: center;">
                            <tr class="trBackColor">
                                <td>序号
                                </td>
                                <td>合同类型
                                </td>
                                <td>操作
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
</asp:Content>
