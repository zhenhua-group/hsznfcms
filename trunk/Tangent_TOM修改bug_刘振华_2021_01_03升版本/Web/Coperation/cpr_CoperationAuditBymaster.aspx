﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.Master" AutoEventWireup="true"
    CodeBehind="cpr_CoperationAuditBymaster.aspx.cs" Inherits="TG.Web.Coperation.cpr_CoperationAuditBymaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/flick/jquery-ui-1.9.2.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/jQuery-Pulgs-Styles/Common/common.css" rel="stylesheet" type="text/css" />
    <link href="../css/jqgrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../css/tipsy/tipsy.css" rel="Stylesheet" type="text/css" />
    <link href="../css/AuditLocusCommon.css" rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css"
        rel="stylesheet" type="text/css" />
    <link href="../js/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="../js/grid.locale-cn.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery-ui-1.8.23.custom.min.js"></script>
    <script src="/js/Global.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="../js/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/Common/AutoComplete.js"></script>
    <script src="../js/Common/AuditLocusCommon.js" type="text/javascript"></script>
    <script src="/js/Common/CommonControl.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jQuery-Plugs.js"></script>
    <script type="text/javascript" src="../js/Common/SendMessageCommon.js"></script>
    <script type="text/javascript" src="/js/MessageComm.js"></script>
    <script src="/js/Coperation/cpr_CoperationAudit.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {

            //点击否显示文本框
            $(".no").click(function () {
                $(this).parent().parent().parent().parent().next(":text").show();
            });
            $(".yes").click(function () {
                $(this).parent().parent().parent().parent().nextAll(":text").hide();
            });
            $("#btnquit").live("click", function () {
                $("#showEdit").hide();
            })
        });  //清空数据  
    </script>
    <style type="text/css">
        /* 表格基本样式*/
        .cls_show_cst_jiben {
            width: 100%;
            border: solid 1px #CCC;
            border-collapse: collapse;
            font-size: 12px;
            font-family: "微软雅黑";
        }

            .cls_show_cst_jiben > tbody > tr > td {
                border: solid 1px #CCC;
                height: 18px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h3 class="page-title">合同信息管理 <small>合同审批</small>
    </h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <li><i class="fa fa-home"></i><a href="../mainpage/WelcomePage.aspx">首页</a> <i class="fa fa-angle-right"></i><a href="cpr_SysMsgListViewBymaster.aspx">消息审批列表</a> <i class="fa fa-angle-right"></i><a>合同审批 </a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i>合同评审记录
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form" style="display: block;">

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 100px;">建设单位:
                                </td>
                                <td>
                                    <asp:Label ID="ConstructionCompany" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 100px;">承接部门:
                                </td>
                                <td>
                                    <asp:Label ID="UndertakeDepartment" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>项目名称:
                                </td>
                                <td>
                                    <asp:Label ID="ProjectName" runat="server" Text=""></asp:Label>
                                </td>
                                <td>建设规模:
                                </td>
                                <td>
                                    <asp:Label ID="BuildScope" runat="server" Text=""></asp:Label>㎡
                                </td>
                            </tr>
                            <tr>
                                <td>建设地点:
                                </td>
                                <td>
                                    <asp:Label ID="BuildAddress" runat="server" Text=""></asp:Label>
                                </td>
                                <td>投资额:
                                </td>
                                <td>
                                    <asp:Label ID="Investment" runat="server" Text=""></asp:Label>万元
                                </td>
                            </tr>
                            <tr>
                                <td>建筑类别:
                                </td>
                                <td>
                                    <asp:Label ID="buildType" runat="server" Text=""></asp:Label>
                                </td>
                                <td>层数:
                                </td>
                                <td>
                                    <asp:Label ID="floorNum" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>合同额:
                                </td>
                                <td>
                                    <asp:Label ID="CoperationAmount" runat="server" Text=""></asp:Label>万元
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>行业性质:
                                </td>
                                <td colspan="3">
                                    <input type="radio" name="BuildNature" value="公建" disabled="">公建
                                            <input type="radio" name="BuildNature" value="房地产" disabled="">房地产
                                            <input type="radio" name="BuildNature" value="市政" disabled="">市政
                                            <input type="radio" name="BuildNature" value="医院" disabled="">医院
                                            <input type="radio" name="BuildNature" value="电力" disabled="">电力
                                            <input type="radio" name="BuildNature" value="通信" disabled="">通信
                                            <input type="radio" name="BuildNature" value="银行" disabled="">银行
                                            <input type="radio" name="BuildNature" value="学校" disabled="">学校
                                            <input type="radio" name="BuildNature" value="涉外" disabled="">涉外
                                            <input type="hidden" id="HiddenBuildNature" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>合同分类:
                                </td>
                                <td colspan="3">
                                    <asp:Repeater ID="RepeaterCoperationBuildType" runat="server">
                                        <ItemTemplate>
                                            <input type="radio" name="BuildType" value="<%#Eval("dic_Name") %>" disabled="">
                                            <%#Eval("dic_Name") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <input type="hidden" id="HiddenCoperationType" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>


                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tr>
                                <td style="width: 10%; text-align: center;">评审部门
                                </td>
                                <td style="width: 30%; text-align: center;">评审要点
                                </td>
                                <td style="width: 40%; text-align: center;">评审意见
                                </td>
                                <td style="width: 20%; text-align: center;">评审人/日期
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="TableContainer">
                        <!--承接部门Table-->
                        <table style="width: 100%; height: auto;" class="table table-bordered" id="undertakeDepartment"
                            audittable="auditTable">
                            <tr>
                                <td style="width: 10%; text-align: center;" rowspan="6">承接部门
                                </td>
                                <td style="width: 30%; color: Red;">1、业务类容是否明确
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="1" class="yes" id="Radio1" checked="checked" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="1" class="no" id="Radio2" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%--<input id="Radio1" type="radio" class="yes" name="1" checked="checked" />是 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
                                                        id="Radio2" type="radio" class="no" name="1" />否<input type="text" style="display: none;"
                                                            maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                                <td style="width: 20%; text-align: center;" rowspan="6" id="AuditUser"></td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">2、有关文件及资料是否齐备
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="2" class="yes" id="Radio3" checked="checked" value="1" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="2" class="no" id="Radio4" value="2" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%--     <input id="Radio3" type="radio" class="yes" name="2" value="1" checked="checked" />是<input
                                                    id="Radio4" type="radio" class="no" name="2" value="2" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">3、约定工期能否按时履行
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="3" class="yes" id="Radio5" checked="checked" value="3" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="3" class="no" id="Radio6" value="4" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%--<input id="Radio5" type="radio" class="yes" name="3" value="3" checked="checked" />是<input
                                                    id="Radio6" type="radio" class="no" name="3" value="4" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">4、人力资源是否配备完毕
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="4" class="yes" id="Radio7" checked="checked" value="5" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="4" class="no" id="Radio8" value="6" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%--<input id="Radio7" type="radio" class="yes" name="4" value="5" checked="checked" />是<input
                                                    id="Radio8" type="radio" class="no" name="4" value="6" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">5、有无特殊约定，能否履行
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="5" class="yes" id="Radio9" checked="checked" value="7" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="5" class="no" id="Radio10" value="8" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%--<input id="Radio9" type="radio" class="yes" name="5" value="7" checked="checked" />是<input
                                                    id="Radio10" type="radio" class="no" name="5" value="8" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">6、其他
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="6" class="yes" id="Radio11" checked="checked" value="9" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="6" class="no" id="Radio12" value="10" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%--  <input id="Radio11" type="radio" class="yes" name="6" value="9" checked="checked" />是<input
                                                    id="Radio12" type="radio" class="no" name="6" value="10" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                            </tr>
                        </table>
                        <!--经营处Table-->
                        <table style="width: 100%; height: auto; display: none;" class="table table-bordered"
                            id="OperateDepartment" audittable="auditTable">
                            <tr>
                                <td style="width: 10%; text-align: center;" rowspan="8">经营处
                                </td>
                                <td style="width: 30%; color: Red;">1、是否符合有关法律、法规
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="7" class="yes" id="Radio13" checked="checked" value="11" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="7" class="no" id="Radio14" value="12" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%-- <input id="Radio13" type="radio" class="yes" name="7" value="11" checked="checked" />是<input
                                                    id="Radio14" type="radio" class="no" name="7" value="12" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                                <td style="width: 20%; text-align: center;" rowspan="8" id="AuditUser"></td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">2、收费是否合理、明确
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="8" class="yes" id="Radio15" checked="checked" value="13" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="8" class="no" id="Radio16" value="14" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%--  <input id="Radio15" type="radio" class="yes" name="8" value="13" checked="checked" />是<input
                                                    id="Radio16" type="radio" class="no" name="8" value="14" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">3、费用支付进度是否合理
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="9" class="yes" id="Radio17" checked="checked" value="15" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="9" class="no" id="Radio18" value="16" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%--<input id="Radio17" type="radio" class="yes" name="9" value="15" checked="checked" />是<input
                                                    id="Radio18" type="radio" class="no" name="9" value="16" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">4、双方责任是否齐全、合理
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="10" class="yes" id="Radio19" checked="checked" value="17" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="10" class="no" id="Radio20" value="18" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%-- <input id="Radio19" type="radio" class="yes" name="10" value="17" checked="checked" />是<input
                                                    id="Radio20" type="radio" class="no" name="10" value="18" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">5、有无风险条款
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="11" class="yes" id="Radio21" checked="checked" value="19" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="11" class="no" id="Radio22" value="20" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%--  <input id="Radio21" type="radio" class="yes" name="11" value="19" checked="checked" />是<input
                                                    id="Radio22" type="radio" class="no" name="11" value="20" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">6、其他
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="12" class="yes" id="Radio23" checked="checked" value="21" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="12" class="no" id="Radio24" value="22" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%-- <input id="Radio23" type="radio" class="yes" name="12" value="21" checked="checked" />是<input
                                                    id="Radio24" type="radio" class="no" name="12" value="22" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">7、管理级别
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="coperationAuditMamageLevel" id="coperationAuditManageLevelOneRadioButton"
                                                    value="0" /></span> 院管
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="coperationAuditMamageLevel" id="coperationAuditManageLevelTwoRadioButton"
                                                    checked="checked" value="1" /></span> 所管
                                        </label>
                                    </div>
                                    <%--      <input type="radio" name="coperationAuditMamageLevel" id="coperationAuditManageLevelOneRadioButton"
                                                    value="0" checked="checked" />院管
                                                <input type="radio" name="coperationAuditMamageLevel" id="coperationAuditManageLevelTwoRadioButton"
                                                    value="1" />所管--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">8、是否需要法律顾问
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="coperationNeedLegalAdviser" id="NeedLegalAdviserRadioButton"
                                                  value="1" /></span> 是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="coperationNeedLegalAdviser" id="UnNeedLegalAdviserRadioButton"
                                                    value="0" checked="checked"  /></span> 否
                                        </label>
                                    </div>
                                    <%--<input type="radio" name="coperationNeedLegalAdviser" id="NeedLegalAdviserRadioButton"
                                                    value="1" checked="checked" />是
                                                <input type="radio" name="coperationNeedLegalAdviser" id="UnNeedLegalAdviserRadioButton"
                                                    value="0" />否--%>
                                </td>
                            </tr>
                        </table>
                        <!--法律顾问-->
                        <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                            id="LegalAdviserDepartment" audittable="auditTable">
                            <tr>
                                <td style="width: 10%; text-align: center;">法律意见
                                </td>
                                <td style="width: 70%;">
                                    <textarea style="width: 100%; height: 100px;" id="LegalAdviserTextarea" class="TextBoxBorder"></textarea>
                                </td>
                                <td style="width: 20%; text-align: center;" id="AuditUser"></td>
                            </tr>
                        </table>
                        <!--技术处Table-->
                        <table style="width: 100%; height: auto; display: none;" class="table table-bordered"
                            id="TechnologyDepartment" audittable="auditTable">
                            <tr>
                                <td style="width: 10%; text-align: center;" rowspan="6">技术处
                                </td>
                                <td style="width: 30%; color: Red;">1、技术及质量要求是否明确
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="14" class="yes" id="Radio25" checked="checked" value="23" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="14" class="no" id="Radio26" value="24" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%-- <input id="Radio25" type="radio" class="yes" name="14" value="23" checked="checked" />是<input
                                                    id="Radio26" type="radio" class="no" name="14" value="24" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                                <td style="width: 20%; text-align: center;" rowspan="6" id="AuditUser"></td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">2、技术要求能否充分实施
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="15" class="yes" id="Radio27" checked="checked" value="25" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="15" class="no" id="Radio28" value="26" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%--   <input id="Radio27" type="radio" class="yes" name="15" value="25" checked="checked" />是<input
                                                    id="Radio28" type="radio" class="no" name="15" value="26" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">3、是否存在技术风险
                                </td>
                                <td style="width: 40%;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="16" class="yes" id="Radio29" checked="checked" value="27" /></span>
                                            是
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="16" class="no" id="Radio30" value="28" /></span> 否
                                        </label>
                                        <input type="text" style="display: none;" maxlength="200" class="TextBoxBorder auditTextBox" />
                                    </div>
                                    <%--<input id="Radio29" type="radio" class="yes" name="16" checked="checked" value="27" />是<input
                                                    id="Radio30" type="radio" class="no" name="16" value="28" />否<input type="text" style="display: none;"
                                                        maxlength="200" class="TextBoxBorder auditTextBox" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%; color: Red;">4、管理级别
                                </td>
                                <td style="width: 40%; color: Red;">
                                    <div class="radio-list">
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="ManagementLevel" class="yes" id="ManagementLevelOne" checked="checked"
                                                    value="0" /></span> 一级
                                        </label>
                                        <label class="radio-inline">
                                            <span class="checked">
                                                <input type="radio" name="ManagementLevel" class="no" id="ManagementLevelTwo" value="1" /></span>
                                            二级
                                        </label>
                                    </div>
                                    <%--     <input type="radio" name="ManagementLevel" value="0" id="ManagementLevelOne" checked="checked" />一级
                                                <input type="radio" name="ManagementLevel" value="1" id="ManagementLevelTwo" />二级--%>
                                </td>
                            </tr>
                        </table>
                        <!--下面部分的Div-->
                        <table style="width: 100%; height: auto; display: none;" class="table table-bordered"
                            id="GeneralManagerTable" audittable="auditTable">
                            <tr>
                                <td style="width: 10%; text-align: center; height: 100px;">院长意见
                                </td>
                                <td style="width: 95%; color: Red; height: 100px;" colspan="4">
                                    <textarea style="width: 100%; height: 100px;" id="GeneralManagerProposalTextarea"
                                        class="TextBoxBorder"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%; height: 20px; color: Red;"></td>
                                <td style="width: 20%;">院长
                                </td>
                                <td style="width: 25%;" id="AuditUser"></td>
                                <td style="width: 20%;">日期
                                </td>
                                <td style="width: 25%;" id="AuditDate"></td>
                            </tr>
                        </table>
                        <!--没有权限审核-->
                        <table style="width: 100%; height: auto; display: none;" class="cls_show_cst_jiben"
                            id="NoPowerTable">
                            <tr>
                                <td style="width: 100%; text-align: center; height: 100px; line-height: 100px;">您没有权限审核该项
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table style="width: 100%; height: auto;" class="cls_show_cst_jiben" id="Table2">
                        <tr>
                            <td style="width: 100%; text-align: center; height: 40px;">
                                <input type="button" id="btnApproval" name="controlBtn" class="btn btn-sm blue" data-toggle="modal"
                                    href="#AuditUserList" value="通过" />
                                &nbsp;
                                        <input type="button" id="btnRefuse" name="controlBtn" class="btn btn-sm blue" value="不通过" />
                                &nbsp;
                                <input type="button" id="Button1" class="btn btn-sm blue" value="返回">&nbsp;
                                <input type="button" id="btnReport"
                                    class="btn btn-sm blue" style="display: none;" value="导出">
                                <input type="button" value="返回合同页面，重新申请审核" id="FallBackCoperaion" style="display: none;"
                                    class="btn btn-sm green" />
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <!--HiddenArea-->
    <input type="hidden" id="HiddenSysMsgSysNo" value="0" />
    <input type="hidden" id="AuditRecordStatus" value="<%=coperationAuditRecordEntity.Status %>" />
    <input type="hidden" id="HiddenAuditRecordSysNo" value="<%=coperationAuditRecordEntity.SysNo %>" />
    <input type="hidden" id="HiddenLoginUser" value="<%=UserSysNo %>" />
    <input type="hidden" id="HiddenUndertakeProposal" value="<%=coperationAuditRecordEntity.UndertakeProposal %>" />
    <input type="hidden" id="HiddenOperateDepartmentProposal" value="<%=coperationAuditRecordEntity.OperateDepartmentProposal %>" />
    <input type="hidden" id="HiddenLegalAdviserProposal" value="<%=coperationAuditRecordEntity.LegalAdviserProposal %>" />
    <input type="hidden" id="HiddenTechnologyDepartmentProposal" value="<%=coperationAuditRecordEntity.TechnologyDepartmentProposal %>" />
    <input type="hidden" id="HiddenGeneralManagerProposal" value="<%=coperationAuditRecordEntity.GeneralManagerProposal  %>" />
    <input type="hidden" id="HiddenBossProposal" value="<%=coperationAuditRecordEntity.BossProposal   %>" />
    <input type="hidden" id="HiddenAuditUser" value="<%=coperationAuditRecordEntity.AuditUserString %>" />
    <input type="hidden" id="HiddenAuditDate" value="<%=coperationAuditRecordEntity.AuditDate %>" />
    <input type="hidden" id="HiddenCoperationSysNo" value="<%=CoperationSysNo %>" />
    <input type="hidden" id="HiddenManageLevel" value="<%=coperationAuditRecordEntity.ManageLevel %>" />
    <input type="hidden" id="HiddenNeedLegalAdviser" value="<%=coperationAuditRecordEntity.NeedLegalAdviser %>" />
    <input type="hidden" id="HiddenCoperationName" value="<%=CoperationName%>" />
    <!--消息ID-->
    <input type="hidden" id="msgno" value="<%= MessageID %>" />
    <input type="hidden" id="hiddenMessageStatus" value="<%=MessageStatus %>" />
    <!--消息列表参数-->
    <input type="hidden" id="pageIndex" value="<%=pageIndex %>" />
    <input type="hidden" id="MessageType" value="<%=MessageType %>" />
    <input type="hidden" id="TypePost" value="<%=TypePost %>" />
    <input type="hidden" id="MessageAction" value="<%=MessageAction %>" />
    <input type="hidden" id="Aflag" value="<%=Aflag %>" />
    <input type="hidden" id="MessageKeys" value="<%=MessageKeys %>" />
    <!--选择消息接收着-->
    <div id="AuditUserList" class="modal fade yellow" tabindex="-1" data-width="450"
        aria-hidden="true" style="display: none; width: 450px; margin-left: -379px; margin-top: -266px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            </button>
            <h4 class="modal-title">审批人员</h4>
        </div>
        <div class="modal-body" id="auditShow">
        </div>
        <div class="modal-footer">
            <button type="button" id="btn_Send" data-dismiss="modal" class="btn green btn-default">
                发送消息</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">
                关闭</button>
        </div>
    </div>
    <div id="msgReceiverContainer" style="width: 400px; height: 200px; display: none;">
    </div>
    <div id="msgReceiverContaineredit" style="width: 400px; height: 200px; display: none;">
    </div>
    <!--承接部门层-->
    <div id="cpr_cjbmDiv" style="display: none;">
        <table id="cpr_cjbmTable" class="show_project" style="text-align: center;">
            <tr class="trBackColor">
                <td style="width: 60px;">序号
                </td>
                <td style="width: 340px;">单位名称
                </td>
                <td style="width: 60px;">操作
                </td>
            </tr>
        </table>
        <div id="cjbmByPageDiv" class="divNavigation pageDivPosition">
            总<label id="cjbm_allDataCount" style="font-weight: bold;">0</label>项&nbsp;&nbsp;&nbsp;&nbsp;
            第<label id="cjbm_nowPageIndex">0</label>/<label id="cjbm_allPageCount">0</label>页&nbsp;&nbsp;
            <a id="cjbm_firstPage" href="#">首页</a>&nbsp; <a id="cjbm_prevPage" href="#">&lt;&lt;</a>&nbsp;
            <a id="cjbm_nextPage" href="#">&gt;&gt;</a>&nbsp; <a id="cjbm_lastPage" href="#">末页</a>&nbsp;&nbsp;&nbsp;&nbsp;
            跳至<input type="text" id="cjbm_pageIndex" style="height: 3; width: 20px; border-style: hidden; border-bottom-style: solid;" />页&nbsp;&nbsp; <a href="#" id="cjbm_gotoPageIndex">GO</a>
        </div>
    </div>
    <!--修改内容层-->
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Button1").click(function () {
                window.history.back();
            });
        });
    </script>
</asp:Content>
