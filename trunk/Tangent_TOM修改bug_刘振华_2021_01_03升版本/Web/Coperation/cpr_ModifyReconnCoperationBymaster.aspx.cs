﻿using Geekees.Common.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TG.BLL;
using TG.Model;

namespace TG.Web.Coperation
{
    public partial class cpr_ModifyReconnCoperationBymaster : PageBase
    {
        //public string asTreeviewStructObjID
        //{
        //get
        //{
        //    //return this.asTreeviewStruct.GetClientTreeObjectId();
        //}
        //}
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructType.GetClientTreeObjectId();
            }
        }

        //是否具有修改权限
        public string HasAudit { get; set; }
        //审批流程获取的修改权限
        public string HasEditAudit
        {
            get
            {
                return Request["audit"] ?? "0";
            }
        }
        ////审批流程获取的修改权限ID
        public int AuditEditSysNo
        {
            get
            {
                int auditid = 0;
                int.TryParse(Request["auditeditsysno"] ?? "0", out auditid);
                return auditid;
            }
        }
        public int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["msgno"] ?? "0", out msgid);
                return msgid;
            }
        }
        //建筑结构值  qpl 20140115
        public string StructString { get; set; }
        public string StructTypeString { get; set; }
        public string BuildTypeString { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            string str_cprid = Request.QueryString["cprid"] ?? "";
            if (!IsPostBack)
            {
                //保存合同编号
                this.hid_cprid.Value = str_cprid;
                //绑定合同类别
                BindCorpType();
                //合同阶段
                BindNewProc();
                //行业性质
                BindCorpHyxz();
                //工程来源
                BindCorpSrc();
                //合同阶段
                BindCorpProc();
                //设计等级
                BindBuildType();
                //设置下拉树的样式
                SetDropDownTreeThem();
                //绑定建筑结构样式
                BindStructType();
                //绑定建筑分类
                BindBuildStuctType();
                //显示联系人
                ShowContractInfo(str_cprid);
                //显示合同信息
                ShowCoperation(str_cprid);
                //如果是审批修改流程
                HasAudit = CheckAudit(int.Parse(str_cprid)) == true ? "1" : "0";
                if (HasAudit == "1")
                {
                    Response.Write("<script language='javascript' >alert('该合同已提交审批，不能被修改！');window.history.back();</script>");
                }

                //获取权限
                SetUserSysNoAndRole();
                //绑定权限
                BindPreviewPower();
                //收费类型
                BindCorChange();

            }
            else
            {
                EditADD();
            }
        }
        //阶段
        protected void BindNewProc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_kcjd'";
            string str_where1 = " dic_Type='cpr_kcdj'";
            string str_where2 = " dic_Type='cpr_kcbuildtype'";
            this.chk_cprjd.DataSource = bll_dic.GetList(str_where);
            this.chk_cprjd.DataTextField = "dic_Name";
            this.chk_cprjd.DataValueField = "ID";
            this.chk_cprjd.DataBind();
            this.ddcpr_SurveyClass.DataSource = bll_dic.GetList(str_where1);
            this.ddcpr_SurveyClass.DataTextField = "dic_Name";
            this.ddcpr_SurveyClass.DataValueField = "ID";
            this.ddcpr_SurveyClass.DataBind();
            this.ddcpr_BuildType.DataSource = bll_dic.GetList(str_where2);
            this.ddcpr_BuildType.DataTextField = "dic_Name";
            this.ddcpr_BuildType.DataValueField = "ID";
            this.ddcpr_BuildType.DataBind();
        }
        private void EditADD()
        {
            if (UpdateCoperation())
            {
                //弹出提示
                TG.Common.MessageBox.ResponseScriptBack(this, "合同信息更新成功！");
            }
        }
        //修改合同信息
        protected bool UpdateCoperation()
        {
            TG.BLL.cm_ReconnCoperation bll_cpr = new TG.BLL.cm_ReconnCoperation();
            TG.BLL.cm_Coperation bll_cop = new TG.BLL.cm_Coperation();
            TG.Model.cm_ReconnCoperation model_cpr = bll_cpr.GetModel(int.Parse(this.hid_cprid.Value));
            TG.Model.cm_Coperation model_cop = new TG.Model.cm_Coperation();
            if (model_cpr.cpr_FID != null)
            {
                model_cop = bll_cop.GetModel(int.Parse(model_cpr.cpr_FID.ToString()));
            }
            //合同ID
            model_cpr.cpr_Id = int.Parse(this.hid_cprid.Value);
            //model_cop.cpr_Id = int.Parse(this.hid_cprid.Value);
            //赋值客户id
            model_cpr.cst_Id = int.Parse(this.hid_cstid.Value);
            model_cop.cst_Id = int.Parse(this.hid_cstid.Value);
            //合同编号
            model_cpr.cpr_No = this.txtcpr_No.Value;
            model_cop.cpr_No = this.txtcpr_No.Value;
            if (this.hid_cprno.Value != "")
            {
                model_cpr.cpr_No = this.hid_cprno.Value;
                model_cop.cpr_No = this.hid_cprno.Value;
            }
            //合同分类
            model_cpr.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            model_cop.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            //合同类型
            model_cpr.cpr_Type2 = this.txt_cprType.InnerText;
            model_cop.cpr_Type2 = this.txt_cprType.InnerText;
            //合同名称
            model_cpr.cpr_Name = this.txt_cprName.Value;
            model_cop.cpr_Name = this.txt_cprName.Value;
            //勘察等级
            model_cpr.SurveyClass = this.ddcpr_SurveyClass.SelectedItem.Text;
            //建设单位
            model_cpr.BuildUnit = this.txt_cprBuildUnit.Value;
            model_cop.BuildUnit = this.txt_cprBuildUnit.Value;
            //占地面积
            model_cpr.BuildArea = Convert.ToDecimal(this.txt_BuildArea.Value);
            model_cop.BuildArea = "0";
            //建筑类型
            model_cpr.BuildType = this.ddcpr_BuildType.SelectedItem.Text.Trim();
            model_cop.BuildType = this.ddcpr_BuildType.SelectedItem.Text.Trim();
            //地貌单元
            model_cpr.EarthUnit = this.txt_EarthUnit.Value;
            //建筑层数
            model_cpr.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            model_cop.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            //工程负责人电话
            model_cpr.ChgPeople = this.txt_proFuze.Value;
            model_cpr.ChgPhone = this.txt_fzphone.Value;
            model_cop.ChgPeople = this.txt_proFuze.Value;
            model_cop.ChgPhone = this.txt_fzphone.Value;
            //承接部门
            model_cpr.cpr_Unit = this.txt_cjbm.Value;
            model_cop.cpr_Unit = this.txt_cjbm.Value;
            if (this.txt_cjbm.Value.Trim() == "")
            {
                model_cpr.cpr_Unit = this.hid_cjbm.Value;
                model_cop.cpr_Unit = this.hid_cjbm.Value;
            }
            //甲方负责人电话
            model_cpr.ChgJia = this.txtFParty.Value;
            model_cpr.ChgJiaPhone = this.txt_jiafphone.Value;
            model_cop.ChgJia = this.txtFParty.Value;
            model_cop.ChgJiaPhone = this.txt_jiafphone.Value;
            //工程地点
            if (this.txt_ProjectPosition.Value.Trim() != "")
            {
                model_cpr.BuildPosition = this.txt_ProjectPosition.Value;
                model_cop.BuildPosition = this.txt_ProjectPosition.Value;
            }
            else
            {
                model_cpr.BuildPosition = "";
                model_cop.BuildPosition = "";
            }
            //合同额
            if (this.txtcpr_Account.Value.Trim() == "")
            {
                model_cpr.cpr_Acount = 0;
                model_cop.cpr_Acount = 0;
            }
            else
            {
                model_cpr.cpr_Acount = Convert.ToDecimal(this.txtcpr_Account.Value);
                model_cop.cpr_Acount = Convert.ToDecimal(this.txtcpr_Account.Value);
            }
            //实际合同额
            if (this.txtcpr_Account0.Value.Trim() == "")
            {
                model_cpr.cpr_ShijiAcounnt = 0;
                model_cop.cpr_ShijiAcount = 0;
            }
            else
            {
                model_cpr.cpr_ShijiAcounnt = Convert.ToDecimal(this.txtcpr_Account0.Value);
                model_cop.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);
            }
            //行业性质
            if (this.ddProfessionType.SelectedIndex != 0)
            {
                model_cpr.Industry = this.ddProfessionType.SelectedItem.Text;
                model_cop.Industry = this.ddProfessionType.SelectedItem.Text;
            }
            else
            {
                model_cpr.Industry = this.ddProfessionType.Items[0].Value;
                model_cop.Industry = this.ddProfessionType.Items[0].Value;
            }
            //钻孔数量
            if (this.txt_HoleNumber.Value.Trim() == "")
            {
                model_cpr.HoleNumber = 0;
            }
            else
            {
                model_cpr.HoleNumber = Convert.ToInt32(this.txt_HoleNumber.Value.Trim());
            }
            //钻孔深度
            if (this.txt_HoleHeight.Value.Trim() == "")
            {
                model_cpr.HoleHeight = 0;
            }
            else
            {
                model_cpr.HoleHeight = Convert.ToDecimal(this.txt_HoleHeight.Value.Trim());
            }
            //建筑物数量
            if (this.txt_BuildNumber.Value.Trim() == "")
            {
                model_cpr.BuildNumber = 0;
            }
            else
            {
                model_cpr.BuildNumber = Convert.ToInt32(this.txt_BuildNumber.Value.Trim());
            }
            //探井数量
            if (this.txt_WellsNumber.Value.Trim() == "")
            {
                model_cpr.WellsNumber = 0;
            }
            else
            {
                model_cpr.WellsNumber = Convert.ToInt32(this.txt_WellsNumber.Value.Trim());
            }
            //探井深度
            if (this.txt_WellsHeight.Value.Trim() == "")
            {
                model_cpr.WellsHeight = 0;
            }
            else
            {
                model_cpr.WellsHeight = Convert.ToDecimal(this.txt_WellsHeight.Value.Trim());
            }
            //总进尺
            if (this.txt_TotalLength.Value.Trim() == "")
            {
                model_cpr.TotalLength = 0;
            }
            else
            {
                model_cpr.TotalLength = Convert.ToDecimal(this.txt_TotalLength.Value.Trim());
            }

            //更新ad
            bool affectRow = false;
            //结构样式
            //if (this.txtInvestAccount.Value.Trim() == "")
            //{
            //    model_cpr.cpr_Touzi = 0;
            //}
            //else
            //{
            //    model_cpr.cpr_Touzi = Convert.ToDecimal(this.txtInvestAccount.Value);
            //}
            //if (txtInvestAccount0.Value.Trim() == "")
            //{
            //    model_cpr.cpr_ShijiTouzi = 0;
            //}
            //else
            //{
            //    model_cpr.cpr_ShijiTouzi = Convert.ToDecimal(txtInvestAccount0.Value);
            //}
            //newAdd
            //建筑分类
            model_cpr.BuildStructType = GetDropDownTreeCheckedValue(this.asTreeviewStructType.RootNode.ChildNodes);
            model_cop.BuildStructType = GetDropDownTreeCheckedValue(this.asTreeviewStructType.RootNode.ChildNodes);
            //合同阶段
            string str_process = "";
            foreach (ListItem chk in this.chk_cprjd.Items)
            {
                if (chk.Selected)
                {
                    str_process += chk.Value + ",";
                }
            }
            str_process = str_process.IndexOf(",") > -1 ? str_process.Remove(str_process.Length - 1) : "";
            model_cpr.cpr_Process = str_process;
            model_cop.cpr_Process = str_process;
            //工程来源
            if (this.ddSourceWay.SelectedIndex != 0)
            {
                model_cpr.BuildSrc = this.ddSourceWay.SelectedItem.Text;
                model_cop.BuildSrc = this.ddSourceWay.SelectedItem.Text;
            }
            else
            {
                model_cpr.BuildSrc = this.ddSourceWay.Items[0].Value;
                model_cop.BuildSrc = this.ddSourceWay.Items[0].Value;
            }
            //合同签订日期
            model_cpr.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value);
            model_cpr.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value);
            //合同统计年份
            model_cpr.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            model_cpr.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            //合同开工日期
            model_cpr.cpr_StartDate = Convert.ToDateTime(this.txtStartDate.Value);
            //合同完成日期
            model_cpr.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            model_cop.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            //制表人
            model_cpr.TableMaker = this.txt_tbcreate.Value;
            model_cop.TableMaker = this.txt_tbcreate.Value;
            //工期====
            //勘探点数量
            if (this.txt_PointNumber.Value.Trim() == "")
            {
                model_cpr.PointNumber = 0;
            }
            else
            {
                model_cpr.PointNumber = Convert.ToInt32(this.txt_PointNumber.Value.Trim());
            }
            //波速实验钻孔数量
            if (this.txt_WaveNumber.Value.Trim() == "")
            {
                model_cpr.WaveNumber = 0;
            }
            else
            {
                model_cpr.WaveNumber = Convert.ToInt32(this.txt_WaveNumber.Value.Trim());
            }
            //控制性钻孔数量
            if (this.txt_ControlNumber.Value.Trim() == "")
            {
                model_cpr.ControlNumber = 0;
            }
            else
            {
                model_cpr.ControlNumber = Convert.ToInt32(this.txt_ControlNumber.Value.Trim());
            }
            //一般性钻孔数量
            if (this.txt_GenerNumber.Value.Trim() == "")
            {
                model_cpr.GenerNumber = 0;
            }
            else
            {
                model_cpr.GenerNumber = Convert.ToInt32(this.txt_GenerNumber.Value.Trim());
            }
            //波速试验钻孔深度、
            if (this.txt_WaveHeight.Value.Trim() == "")
            {
                model_cpr.WaveHeight = 0;
            }
            else
            {
                model_cpr.WaveHeight = Convert.ToDecimal(this.txt_WaveHeight.Value.Trim());
            }
            //控制性钻孔深度
            if (this.txt_ControlHeight.Value.Trim() == "")
            {
                model_cpr.ControlHeight = 0;
            }
            else
            {
                model_cpr.ControlHeight = Convert.ToDecimal(this.txt_ControlHeight.Value.Trim());
            }
            //一般性钻孔深度
            if (this.txt_GenerHeight.Value.Trim() == "")
            {
                model_cpr.GenerHeight = 0;
            }
            else
            {
                model_cpr.GenerHeight = Convert.ToDecimal(this.txt_GenerHeight.Value.Trim());
            }
            //工程规模
            if (this.txt_ProjectArea.Value.Trim() == "")
            {
                model_cpr.ProjectArea = 0;
            }
            else
            {
                model_cpr.ProjectArea = Convert.ToDecimal(this.txt_ProjectArea.Value.Trim());
            }
            //工程特征
            model_cpr.Features = this.txt_Features.Value.Trim();
            //工期
            if (this.txt_ProjectDate.Value.Trim() == "")
            {
                model_cpr.ProjectDate = 0;
            }
            else
            {
                model_cpr.ProjectDate = Convert.ToInt32(this.txt_ProjectDate.Value.Trim());
            }
            //多吨楼备注--拟采用基础选型及地基处理方案
            model_cpr.MultiBuild = this.txt_MultiBuild.Value;
            model_cop.MultiBuild = this.txt_MultiBuild.Value;
            //合同备注
            model_cpr.cpr_Mark = this.txtcpr_Remark.Value;
            model_cop.cpr_Mark = this.txtcpr_Remark.Value;
            //model_cpr.BuildArea = this.txt_buildArea.Value;

            //项目经理
            model_cpr.PMUserID = int.Parse(this.HiddenPMUserID.Value);
            model_cop.PMUserID = int.Parse(this.HiddenPMUserID.Value);



            //model_cpr.RegTime = DateTime.Now;
            model_cpr.UpdateBy = UserSysNo.ToString();
            model_cpr.LastUpdate = DateTime.Now;
            model_cop.UpdateBy = UserSysNo.ToString();
            model_cop.LastUpdate = DateTime.Now;
            model_cpr.cpr_FID = model_cop.cpr_Id;
            try
            {

                affectRow = bll_cpr.Update(model_cpr);
                bll_cop.Update(model_cop);

                //修改合同状态
                if (HasEditAudit == "1")
                {
                    UpdateAuditEditStatus();
                }
            }
            catch (System.Exception ex)
            {
            }

            return affectRow;
        }

        //设置选择合同额方法
        public void SetUserSysNoAndRole()
        {
            //用户ID
            this.ChooseCustomer1.UserSysNo = int.Parse(GetCurMemID());
            //权限
            this.ChooseCustomer1.PreviewPower = GetPreviewPower();
        }
        //获取页面权限
        public int GetPreviewPower()
        {
            int UserSysNo = int.Parse(GetCurMemID());
            string PageName = "cpr_CorperationList.aspx";
            List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, PageName);

            //浏览权限
            int Power = 0;
            if ((from role in rolePowerParameterList where role.PreviewPattern == "2" select role).Count() > 0)
            {
                //部门
                Power = 2;
            }
            if ((from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0)
            {
                //全部
                Power = 1;
            }
            return Power;
        }
        //返回一个文件上传的随机ID
        public string GetCoperationID()
        {
            return this.hid_cprid.Value;
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.ddcpr_Type.DataSource = bll_dic.GetList(str_where);
            this.ddcpr_Type.DataTextField = "dic_Name";
            this.ddcpr_Type.DataValueField = "ID";
            this.ddcpr_Type.DataBind();
        }
        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz'";
            this.ddProfessionType.DataSource = bll_dic.GetList(str_where);
            this.ddProfessionType.DataTextField = "dic_Name";
            this.ddProfessionType.DataValueField = "ID";
            this.ddProfessionType.DataBind();
        }
        //收费类型
        protected void BindCorChange()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            //string str_where = " dic_Type='cpr_charge'";

        }
        //工程来源
        protected void BindCorpSrc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_src'";
            this.ddSourceWay.DataSource = bll_dic.GetList(str_where);
            this.ddSourceWay.DataTextField = "dic_Name";
            this.ddSourceWay.DataValueField = "ID";
            this.ddSourceWay.DataBind();
        }
        //合同阶段
        protected void BindCorpProc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            //string str_where = " dic_Type='cpr_jd'";
            //this.chk_cprjd.DataSource = bll_dic.GetList(str_where);
            //this.chk_cprjd.DataTextField = "dic_Name";
            //this.chk_cprjd.DataValueField = "ID";
            //this.chk_cprjd.DataBind();
        }
        //合同设计等级
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            //string str_where = " dic_Type='cpr_buildtype'";
            //this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
            //this.drp_buildtype.DataTextField = "dic_Name";
            //this.drp_buildtype.DataValueField = "ID";
            //this.drp_buildtype.DataBind();
        }
        //显示联系人
        protected void ShowContractInfo(string cprid)
        {
            string strSql = " Select cst_Id From [cm_ReconnCoperation] Where cpr_Id=" + cprid;
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            DataSet ds_cst = bll_db.GetList(strSql);
            if (ds_cst.Tables.Count > 0)
            {
                if (ds_cst.Tables[0].Rows.Count > 0)
                {
                    string str_cstid = ds_cst.Tables[0].Rows[0][0].ToString();
                    //保存客户ID
                    this.hid_cstid.Value = str_cstid;

                    TG.BLL.cm_CustomerInfo bll_cst = new TG.BLL.cm_CustomerInfo();
                    string str_where = " Cst_ID=" + str_cstid;
                    TG.Model.cm_CustomerInfo model_cst = bll_cst.GetModel(int.Parse(str_cstid));
                    if (model_cst != null)
                    {
                        this.txtCst_No.Value = model_cst.Cst_No.Trim();
                        this.txtCst_Name.Value = model_cst.Cst_Name.Trim();
                        this.txtCpy_Address.Value = model_cst.Cpy_Address.Trim();
                        this.txtCst_Brief.Value = model_cst.Cst_Brief;
                        this.txtCode.Value = model_cst.Code ?? "";
                        this.txtLinkman.Value = model_cst.Linkman ?? "";
                        this.txtCpy_Phone.Value = model_cst.Cpy_Phone ?? "";
                        this.txtCpy_Fax.Value = model_cst.Cpy_Fax ?? "";
                    }
                }
            }
        }
        //显示合同信息
        protected void ShowCoperation(string cprid)
        {
            //得到审批
            string sql = @"SELECT top 1 ChangeDetails FROM 
cm_AuditRecordEdit WHERE CoperationSysNo=" + int.Parse(cprid) + "ORDER BY SysNo desc";
            object o = TG.DBUtility.DbHelperSQL.GetSingle(sql);
            TG.BLL.cm_ReconnCoperation bll_cpr = new TG.BLL.cm_ReconnCoperation();
            TG.Model.cm_ReconnCoperation model_cpr = bll_cpr.GetModel(int.Parse(cprid));
            //合同编号
            this.txtcpr_No.Value = model_cpr.cpr_No == null ? "" : model_cpr.cpr_No.Trim();
            //合同分类
            this.ddcpr_Type.Items.FindByText(model_cpr.cpr_Type.Trim()).Selected = true;
            //this.txt_cprType.Value = Convert.ToString(model_cpr.cpr_Type2 ?? "").Trim();
            //合同名称
            this.txt_cprName.Value = model_cpr.cpr_Name.Trim();
            //勘察等级
            this.ddcpr_SurveyClass.Items.FindByText(model_cpr.SurveyClass.Trim()).Selected = true;
            //建设单位
            this.txt_cprBuildUnit.Value = model_cpr.BuildUnit == null ? "" : model_cpr.BuildUnit.Trim();
            //占地面积
            this.txt_BuildArea.Value = model_cpr.BuildArea.ToString();
            //建筑类型
            this.ddcpr_BuildType.Items.FindByText(model_cpr.BuildType.Trim()).Selected = true;
            //地貌单元
            this.txt_EarthUnit.Value = model_cpr.EarthUnit.Trim();
            //层数
            string[] floors = model_cpr.Floor.Split(new char[] { '|' }, StringSplitOptions.None);
            this.txt_upfloor.Value = floors[0].ToString().Trim();
            this.txt_downfloor.Value = floors[1].ToString().Trim();
            //项目经理
            this.txt_proFuze.Value = model_cpr.ChgPeople.Trim();
            //添加项目经理ID  qpl 20131225
            this.HiddenPMUserID.Value = model_cpr.PMUserID.ToString();
            //经理电话
            this.txt_fzphone.Value = model_cpr.ChgPhone.Trim();
            //承接部门
            this.hid_cjbm.Value = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();
            this.txt_cjbm.Value = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();
            //甲方负责人
            this.txtFParty.Value = model_cpr.ChgJia.Trim();
            //甲方负责人电话
            this.txt_jiafphone.Value = model_cpr.ChgJiaPhone.Trim();
            this.txt_ProjectPosition.Value = model_cpr.BuildPosition.Trim();
            //建筑类别
            //this.drp_buildtype.Items.FindByText(model_cpr.BuildType.Trim()).Selected = true;
            //合同额
            this.txtcpr_Account.Value = model_cpr.cpr_Acount.ToString();
            //实际合同额
            this.txtcpr_Account0.Value = model_cpr.cpr_ShijiAcounnt.ToString();
            //投资额
            //this.txtInvestAccount.Value = model_cpr.cpr_Touzi.ToString();
            ////实际投资额
            //this.txtInvestAccount0.Value = model_cpr.cpr_ShijiTouzi.ToString();
            //行业性质
            if (model_cpr.Industry.Trim() != "" && model_cpr.Industry.Trim() != "-1")
            {
                this.ddProfessionType.Items.FindByText(model_cpr.Industry.Trim()).Selected = true;
            }

            //钻孔数量
            this.txt_HoleNumber.Value = model_cpr.HoleNumber == null ? "" : model_cpr.HoleNumber.ToString();
            //钻孔深度
            this.txt_HoleHeight.Value = model_cpr.HoleHeight == null ? "" : model_cpr.HoleHeight.ToString();
            //建筑物数量、
            this.txt_BuildNumber.Value = model_cpr.BuildNumber == null ? "" : model_cpr.BuildNumber.ToString();
            //探井数量
            this.txt_WellsNumber.Value = model_cpr.WellsNumber == null ? "" : model_cpr.WellsNumber.ToString();
            //探井深度
            this.txt_WellsHeight.Value = model_cpr.WellsHeight == null ? "" : model_cpr.WellsHeight.ToString();
            //总进尺
            this.txt_TotalLength.Value = model_cpr.TotalLength == null ? "" : model_cpr.TotalLength.ToString();
            //合同阶段
            if (model_cpr.cpr_Process.Trim() != "")
            {
                string[] array = model_cpr.cpr_Process.Split(new char[] { ',' }, StringSplitOptions.None);

                for (int i = 0; i < array.Length; i++)
                {
                    for (int j = 0; j < this.chk_cprjd.Items.Count; j++)
                    {
                        if (array[i].Trim() == this.chk_cprjd.Items[j].Value)
                        {
                            this.chk_cprjd.Items[j].Selected = true;
                        }
                    }
                }
            }
            //工程来源
            if (model_cpr.BuildSrc.Trim() != "" && model_cpr.BuildSrc.Trim() != "-1")
            {
                this.ddSourceWay.Items.FindByText(model_cpr.BuildSrc.Trim()).Selected = true;
            }
            //合同签订日期
            this.txtSingnDate.Value = Convert.ToDateTime(model_cpr.cpr_SignDate).ToString("yyyy-MM-dd");
            this.txtSingnDate2.Value = Convert.ToDateTime(model_cpr.cpr_SignDate2).ToString("yyyy-MM-dd");
            //合同开工日期
            this.txtStartDate.Value = model_cpr.cpr_StartDate == null ? "" : Convert.ToDateTime(model_cpr.cpr_StartDate).ToString("yyyy-MM-dd");
            //合同完成日期
            this.txtCompleteDate.Value = model_cpr.cpr_DoneDate == null ? "" : Convert.ToDateTime(model_cpr.cpr_DoneDate).ToString("yyyy-MM-dd");
            //制表人
            this.txt_tbcreate.Value = model_cpr.TableMaker == null ? "" : model_cpr.TableMaker.Trim();
            //勘探点数量
            this.txt_PointNumber.Value = model_cpr.PointNumber == null ? "" : model_cpr.PointNumber.ToString();
            //波速试验钻孔数量及深度
            this.txt_WaveNumber.Value = model_cpr.WaveNumber == null ? "" : model_cpr.WaveNumber.ToString();
            this.txt_WaveHeight.Value = model_cpr.WaveHeight == null ? "" : model_cpr.WaveHeight.ToString();
            //控制性钻孔数量及深度
            this.txt_ControlNumber.Value = model_cpr.ControlNumber == null ? "" : model_cpr.ControlNumber.ToString();
            this.txt_ControlHeight.Value = model_cpr.ControlHeight == null ? "" : model_cpr.ControlHeight.ToString();
            //一般性钻孔数量和深度
            this.txt_GenerNumber.Value = model_cpr.GenerNumber == null ? "" : model_cpr.GenerNumber.ToString();
            this.txt_GenerHeight.Value = model_cpr.GenerHeight == null ? "" : model_cpr.GenerHeight.ToString();
            //工程规模
            this.txt_ProjectArea.Value = model_cpr.ProjectArea == null ? "" : model_cpr.ProjectArea.ToString();
            //工程特征
            this.txt_Features.Value = model_cpr.Features == null ? "" : model_cpr.Features.ToString();
            //工期
            this.txt_ProjectDate.Value = model_cpr.ProjectDate == null ? "" : model_cpr.ProjectDate.ToString();
            //多栋楼--拟采用基础选型及地基处理方案
            this.txt_MultiBuild.Value = model_cpr.MultiBuild == null ? "" : model_cpr.MultiBuild.Trim();
            //备注
            this.txtcpr_Remark.Value = model_cpr.cpr_Mark == null ? "" : model_cpr.cpr_Mark.Trim();
            //建筑面积
            //this.txt_buildArea.Value = Convert.ToString(model_cpr.BuildArea ?? "").Trim();


            //if (model_cpr.BuildPosition.Trim() != "" && model_cpr.BuildPosition.Trim() != "-1")
            //{
            //    this.ddProjectPosition.Items.FindByText(model_cpr.BuildPosition.Trim()).Selected = true;
            //}



            //if (o != null)
            //{
            //    if (o.ToString().Split(',')[1].Length > 0)
            //    {
            //        this.txt_cjbm.Enabled = true;
            //    }

            //}
            //this.txt_cjbm.Value = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();


            //录入时间
            //this.hid_cprtime.Value = model_cpr.RegTime.ToString();
            //结构形式
            //string StrStruct = model_cpr.StructType == null ? "" : model_cpr.StructType.Trim();
            //注销  20140115
            //LoadStructData(StrStruct, this.asTreeviewStruct);
            //结构形式值 qpl  20140115
            //StructString = StrStruct;
            //建筑分类
            string structtype = model_cpr.BuildStructType == null ? "" : model_cpr.BuildStructType.Trim();
            //注销  20140115
            LoadStructData(structtype, this.asTreeviewStructType);
            //建筑分类值  qpl  20140115
            StructTypeString = structtype;

            //多吨建筑

        }

        //更新合同修改流程状态
        protected void UpdateAuditEditStatus()
        {
            //TG.BLL.cm_CoperationEditRecord bll = new TG.BLL.cm_CoperationEditRecord();
            ////TG.Model.cm_CoperationEditRecord model = bll.GetModel(AuditEditSysNo);
            //model.Status = "F";
            //bll.Update(model);
            ////消息状态
            //string strSql = " Update cm_SysMsg Set IsDone='D' Where [SysNo]=" + MessageID;
            //TG.DBUtility.DbHelperSQL.ExecuteSql(strSql);Cst_Id
        }
        // 检查是否在审核队列中
        private bool CheckAudit(int coperationSysNo)
        {
            return new TG.BLL.cm_CoperationAudit().IsExist(coperationSysNo);

        }
        //建筑分类
        protected void BindBuildStuctType()
        {
            BindProInfoConfig("BuildType", this.asTreeviewStructType.RootNode);
            this.asTreeviewStructType.CollapseAll();
            //子项建筑分类 qpl  20131225
            //BindProInfoConfig("BuildType", this.asTreeviewStructType_sub.RootNode);
            //this.asTreeviewStructType_sub.CollapseAll();
        }
        //结构形式
        protected void BindStructType()
        {
            //BindProInfoConfig("StructType", this.asTreeviewStruct.RootNode);
            //this.asTreeviewStruct.CollapseAll();
            //子项结构样式  qpl  20131225
            //BindProInfoConfig("StructType", this.asTreeviewStruct_sub.RootNode);
            //this.asTreeviewStruct_sub.CollapseAll();
        }
        //设计等级
        //protected void BindBuildType()
        //{
        //    //BindProInfoConfig("BuildTypelevel", this.asTreeViewBuildType.RootNode, false);
        //    ////去掉选中联动  qpl  20140108
        //    //this.asTreeViewBuildType.EnableThreeStateCheckbox = false;
        //    //this.asTreeViewBuildType.CollapseAll();
        //}
        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }
        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        /// <summary>
        /// 绑定树列表  qpl 20140108 
        /// </summary>
        /// <param name="infokey"></param>
        /// <param name="rootnode"></param>
        /// <param name="flagrootbox">根节点是否显示CheckBox</param>
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode, bool showsubbox)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root, showsubbox);
            }
        }
        /// <summary>
        /// 绑定树列表  qpl 20140108 
        /// </summary>
        /// <param name="xmlroot"></param>
        /// <param name="root"></param>
        /// <param name="flagrootbox">根节点是否显示CheckBox</param>
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root, bool showsubbox)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                //标示子节点是否显示CheckBox qpl 20140108
                switch (name)
                {
                    case "特级":
                    case "一级":
                    case "二级":
                    case "三级":
                        linknode.EnableCheckbox = !showsubbox;
                        break;
                    default:
                        linknode.EnableCheckbox = showsubbox;
                        break;
                }

                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);

                        //子节点的checkbox 不可用  qpl 20140108
                        sublinknode.EnableCheckbox = showsubbox;

                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode, showsubbox);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        //获取所有树文字数据
        protected void GetAllNodeTex(ASTreeViewNode nodes, ref List<string> alltext)
        {
            foreach (ASTreeViewNode node in nodes.ChildNodes)
            {
                alltext.Add(node.NodeText);
                if (node.ChildNodes.Count > 0)
                {
                    GetAllNodeTex(node, ref alltext);
                }
            }
        }
        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            //this.asTreeviewStruct.Theme = macOS;
            this.asTreeviewStructType.Theme = macOS;
            //qpl 20131225
            //this.asTreeViewBuildType.Theme = macOS;
            //qpl 20131225
            //this.asTreeviewStructType_sub.Theme = macOS;
            //this.asTreeviewStruct_sub.Theme = macOS;
        }

        //update by 20130530 qpl
        //获取选中树所有节点值并返回值
        protected string GetDropDownTreeCheckedValue(List<ASTreeViewNode> allnodes)
        {
            //最终生成字符串
            string rootvalue = "";
            foreach (ASTreeViewNode node in allnodes)
            {
                string secondvalue = "";
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                    GetSecondNodeList(node, ref secondvalue);
                }
                rootvalue += secondvalue;
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }

            return rootvalue;
        }
        //获取等级选中节点 qpl 20140108
        protected string GetDropDownTreeCheckedValue(List<ASTreeViewNode> allnodes, bool flag)
        {
            //最终生成字符串
            string rootvalue = "";
            foreach (ASTreeViewNode node in allnodes)
            {
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                }
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }

            return rootvalue;
        }
        protected void GetSecondNodeList(ASTreeViewNode node, ref string value)
        {
            if (node.ChildNodes.Count > 0)
            {
                //返回值
                StringBuilder sbresult = new StringBuilder();

                foreach (ASTreeViewNode snode in node.ChildNodes)
                {
                    if (snode.CheckedState == ASTreeViewCheckboxState.Checked || snode.CheckedState == ASTreeViewCheckboxState.HalfChecked)
                    {
                        //拼接第二级
                        value = "^" + snode.NodeValue;
                        string subvalue = "";
                        subvalue = value;
                        GetChildNodes(snode, ref subvalue);
                        foreach (string key in sblist)
                        {
                            sbresult.Append(key);
                        }
                        //清空当前列表
                        sblist.Clear();
                    }
                }
                value = sbresult.ToString();
            }
        }
        //查询数据
        List<string> sblist = new List<string>();
        protected void GetChildNodes(ASTreeViewNode node, ref string value)
        {
            StringBuilder sb = new StringBuilder();
            if (node.ChildNodes.Count > 0)
            {
                foreach (ASTreeViewNode childnode in node.ChildNodes)
                {
                    if ((childnode.CheckedState == ASTreeViewCheckboxState.Checked) || (childnode.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                    {
                        string tempvalue = value + "*" + childnode.NodeValue;

                        if (childnode.ChildNodes.Count > 0)
                        {
                            //如果还有子节点，继续遍历
                            GetChildNodes(childnode, ref tempvalue);
                        }
                        else
                        {
                            //添加末节点
                            sb.Append(tempvalue);
                        }
                    }
                }
            }
            else
            {
                //添加末节点
                sb.Append(value);
            }
            //赋值value
            sblist.Add(sb.ToString());
        }
        //加载存储的树节点
        protected void LoadStructData(string strData, ASDropDownTreeView contrl)
        {
            if (!string.IsNullOrEmpty(strData))
            {
                //加载所有选中节点
                List<string> nodelist = new List<string>();
                //第一级
                string[] strarray = strData.Split('+');

                for (int i = 0; i < strarray.Length; i++)
                {
                    //特殊第二级
                    string[] strsubarray = strarray[i].Split('^');
                    for (int j = 0; j < strsubarray.Length; j++)
                    {
                        //递归的三级以后
                        string[] strendarray = strsubarray[j].Split('*');
                        nodelist.Add(strendarray[strendarray.Length - 1]);
                    }
                }
                //选中节点
                foreach (string nodevalue in nodelist)
                {
                    if (contrl.FindByValue(nodevalue) != null)
                    {
                        contrl.FindByValue(nodevalue).CheckedState = ASTreeViewCheckboxState.Checked;
                    }

                }
            }
        }
        //加载存储的树节点  qpl   20140108  
        protected void LoadStructData(string strData, ASDropDownTreeView contrl, bool flag)
        {
            if (!string.IsNullOrEmpty(strData))
            {
                //加载所有选中节点
                List<string> nodelist = new List<string>();

                if (strData.IndexOf('+') > -1)
                {
                    //第一级
                    string[] strarray = strData.Split('+');
                    for (int i = 0; i < strarray.Length; i++)
                    {
                        nodelist.Add(strarray[i].ToString());
                    }
                }
                else
                {
                    nodelist.Add(strData);
                }
                //选中节点
                foreach (string nodevalue in nodelist)
                {
                    if (contrl.FindByValue(nodevalue) != null)
                    {
                        contrl.FindByValue(nodevalue).CheckedState = ASTreeViewCheckboxState.Checked;
                    }
                }
            }
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;
                //string NotShowUnitList = base.NotShowUnitList;
                ChooseCustomer1.UserSysNo = base.UserSysNo;
                ChooseCustomer1.PreviewPower = base.RolePowerParameterEntity.PreviewPattern;
                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
                //this.NotShowUnitList.Value = NotShowUnitList;
            }
        }
    }
}