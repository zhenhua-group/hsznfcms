﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TG.BLL;
using TG.Model;

namespace TG.Web.Coperation
{
    public partial class cpr_ModifyPitCoperationBymaster : PageBase
    {
        //是否具有修改权限
        public string HasAudit { get; set; }
        //审批流程获取的修改权限
        public string HasEditAudit
        {
            get
            {
                return Request["audit"] ?? "0";
            }
        }
        ////审批流程获取的修改权限ID
        public int AuditEditSysNo
        {
            get
            {
                int auditid = 0;
                int.TryParse(Request["auditeditsysno"] ?? "0", out auditid);
                return auditid;
            }
        }
        public int MessageID
        {
            get
            {
                int msgid = 0;
                int.TryParse(Request["msgno"] ?? "0", out msgid);
                return msgid;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string str_cprid = Request.QueryString["cprid"] ?? "";
            if (!IsPostBack)
            {
                //保存合同编号
                this.hid_cprid.Value = str_cprid;
                //绑定合同类别
                BindCorpType();
                //行业性质
                BindCorpHyxz();
                //工程来源
                BindCorpSrc();
                //设计等级
                BindBuildType();

                //显示联系人
                ShowContractInfo(str_cprid);
                //显示合同信息
                ShowCoperation(str_cprid);
                //如果是审批修改流程
                //HasAudit = CheckAudit(int.Parse(str_cprid)) == true ? "1" : "0";
                //if (HasAudit == "1")
                //{
                //    Response.Write("<script language='javascript' >alert('该合同已提交审批，不能被修改！');window.history.back();</script>");
                //}

                //绑定权限
                BindPreviewPower();
            }
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_Save_Click(object sender, EventArgs e)
        {
            if (UpdateCoperation())
            {
                //弹出提示
                TG.Common.MessageBox.ResponseScriptBack(this, "合同信息更新成功！");
            }
        }

        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;
                //string NotShowUnitList = base.NotShowUnitList;
                ChooseCustomer1.UserSysNo = base.UserSysNo;
                ChooseCustomer1.PreviewPower = base.RolePowerParameterEntity.PreviewPattern;
                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
                //this.NotShowUnitList.Value = NotShowUnitList;
            }
        }
        //获取页面权限
        public int GetPreviewPower()
        {
            int UserSysNo = int.Parse(GetCurMemID());
            string PageName = "cpr_CorperationList.aspx";
            List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, PageName);

            //浏览权限
            int Power = 0;
            if ((from role in rolePowerParameterList where role.PreviewPattern == "2" select role).Count() > 0)
            {
                //部门
                Power = 2;
            }
            if ((from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0)
            {
                //全部
                Power = 1;
            }
            return Power;
        }
        //返回一个文件上传的随机ID
        public string GetCoperationID()
        {
            return this.hid_cprid.Value;
        }
        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.ddcpr_Type.DataSource = bll_dic.GetList(str_where);
            this.ddcpr_Type.DataTextField = "dic_Name";
            this.ddcpr_Type.DataValueField = "ID";
            this.ddcpr_Type.DataBind();

        }

        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz'";
            this.ddProfessionType.DataSource = bll_dic.GetList(str_where);
            this.ddProfessionType.DataTextField = "dic_Name";
            this.ddProfessionType.DataValueField = "ID";
            this.ddProfessionType.DataBind();
        }

        //建筑类型
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();

            string str_where1 = " dic_Type='cpr_kcdj'";
            string str_where2 = " dic_Type='cpr_kcbuildtype'";
            //this.chk_cprjd.DataSource = bll_dic.GetList(str_where);
            //this.chk_cprjd.DataTextField = "dic_Name";
            //this.chk_cprjd.DataValueField = "ID";
            //this.chk_cprjd.DataBind();
            this.ddcpr_PitLevel.DataSource = bll_dic.GetList(str_where1);
            this.ddcpr_PitLevel.DataTextField = "dic_Name";
            this.ddcpr_PitLevel.DataValueField = "ID";
            this.ddcpr_PitLevel.DataBind();
            this.ddcpr_BuildType.DataSource = bll_dic.GetList(str_where2);
            this.ddcpr_BuildType.DataTextField = "dic_Name";
            this.ddcpr_BuildType.DataValueField = "ID";
            this.ddcpr_BuildType.DataBind();
        }
        //工程来源
        protected void BindCorpSrc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_src'";
            this.ddSourceWay.DataSource = bll_dic.GetList(str_where);
            this.ddSourceWay.DataTextField = "dic_Name";
            this.ddSourceWay.DataValueField = "ID";
            this.ddSourceWay.DataBind();
        }

        //显示联系人
        protected void ShowContractInfo(string cprid)
        {
            string strSql = " Select cst_Id From [cm_PitCoperation] Where cpr_Id=" + cprid;
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            DataSet ds_cst = bll_db.GetList(strSql);
            if (ds_cst.Tables.Count > 0)
            {
                if (ds_cst.Tables[0].Rows.Count > 0)
                {
                    string str_cstid = ds_cst.Tables[0].Rows[0][0].ToString();
                    //保存客户ID
                    this.hid_cstid.Value = str_cstid;

                    TG.BLL.cm_CustomerInfo bll_cst = new TG.BLL.cm_CustomerInfo();
                    string str_where = " Cst_ID=" + str_cstid;
                    TG.Model.cm_CustomerInfo model_cst = bll_cst.GetModel(int.Parse(str_cstid));
                    if (model_cst != null)
                    {
                        this.txtCst_No.Value = model_cst.Cst_No.Trim();
                        this.txtCst_Name.Value = model_cst.Cst_Name.Trim();
                        this.txtCpy_Address.Value = model_cst.Cpy_Address.Trim();
                        this.txtCst_Brief.Value = model_cst.Cst_Brief;
                        this.txtCode.Value = model_cst.Code ?? "";
                        this.txtLinkman.Value = model_cst.Linkman ?? "";
                        this.txtCpy_Phone.Value = model_cst.Cpy_Phone ?? "";
                        this.txtCpy_Fax.Value = model_cst.Cpy_Fax ?? "";
                    }
                }
            }
        }

        //显示合同信息
        protected void ShowCoperation(string cprid)
        {
            TG.BLL.cm_PitCoperation bll_cpr = new TG.BLL.cm_PitCoperation();
            TG.Model.cm_PitCoperation model_cpr = bll_cpr.GetModel(int.Parse(cprid));
            if (model_cpr != null)
            {

                //赋值
                this.txtcpr_No.Value = model_cpr.cpr_No == null ? "" : model_cpr.cpr_No.Trim();
                this.ddcpr_Type.Items.FindByText(model_cpr.cpr_Type.Trim()).Selected = true;
                //this.txt_cprType.Value = Convert.ToString(model_cpr.cpr_Type2 ?? "").Trim();
                //合同名称
                this.txt_cprName.Value = model_cpr.cpr_Name.Trim();
                //建设单位
                this.txt_cprBuildUnit.Value = model_cpr.BuildUnit == null ? "" : model_cpr.BuildUnit.Trim();
                //基坑等级
                this.ddcpr_PitLevel.Items.FindByText(model_cpr.PitLevel.Trim()).Selected = true;
                //建筑类别
                this.ddcpr_BuildType.Items.FindByText(model_cpr.BuildType.Trim()).Selected = true;

                this.txtcpr_Account.Value = model_cpr.cpr_Acount.ToString();
                //实际合同额
                this.txtcpr_Account0.Value = model_cpr.cpr_ShijiAcount.ToString();

                //合同统计年份
                this.txtSingnDate.Value = Convert.ToDateTime(model_cpr.cpr_SignDate).ToString("yyyy-MM-dd");
                //签订日期
                this.txtSingnDate2.Value = Convert.ToDateTime(model_cpr.cpr_SignDate2).ToString("yyyy-MM-dd");
                //完成日期
                this.txtCompleteDate.Value = model_cpr.cpr_DoneDate == null ? "" : Convert.ToDateTime(model_cpr.cpr_DoneDate).ToString("yyyy-MM-dd");

                //占地面积
                this.txt_BuildArea.Value = Convert.ToString(model_cpr.BuildArea.ToString() ?? "0").Trim();
                //项目经理
                this.txt_proFuze.Value = model_cpr.ChgPeople.Trim();
                //添加项目经理ID 
                this.HiddenPMUserID.Value = model_cpr.PMUserID.ToString();
                //经理电话
                this.txt_fzphone.Value = model_cpr.ChgPhone.Trim();
                //甲方负责人
                this.txtFParty.Value = model_cpr.ChgJia.Trim();
                //甲方负责人电话
                this.txt_jiafphone.Value = model_cpr.ChgJiaPhone.Trim();
                //工程地点
                this.txt_ProjectPosition.Value = model_cpr.BuildPosition.Trim();
                //行业性质
                if (model_cpr.Industry.Trim() != "" && model_cpr.Industry.Trim() != "-1")
                {
                    this.ddProfessionType.Items.FindByText(model_cpr.Industry.Trim()).Selected = true;
                }
                //工程来源
                if (model_cpr.BuildSrc.Trim() != "" && model_cpr.BuildSrc.Trim() != "-1")
                {
                    this.ddSourceWay.Items.FindByText(model_cpr.BuildSrc.Trim()).Selected = true;
                }

                this.txt_cjbm.Value = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();
                //承接部门
                this.hid_cjbm.Value = model_cpr.cpr_Unit == null ? "" : model_cpr.cpr_Unit.Trim();
                //制表人
                this.txt_tbcreate.Value = model_cpr.TableMaker == null ? "" : model_cpr.TableMaker.Trim();


                //层数
                string[] floors = model_cpr.Floor.Split(new char[] { '|' }, StringSplitOptions.None);
                this.txt_upfloor.Value = floors[0].ToString().Trim();
                this.txt_downfloor.Value = floors[1].ToString().Trim();
                //多建筑
                this.txt_MultiBuild.Value = model_cpr.MultiBuild;
                //备注
                this.txtcpr_Remark.Value = model_cpr.cpr_Mark == null ? "" : model_cpr.cpr_Mark.Trim();
                //基坑深度
                this.txt_PitHeight.Value = model_cpr.PitHeight.ToString();
                //边坡高度

                this.txt_SlopeHeight.Value = Convert.ToDecimal(model_cpr.SlopeHeight).ToString();

                //位移监测点数量
                this.txt_PointNumber.Value = Convert.ToInt32(model_cpr.PointNumber).ToString();

                //沉降观测点数量
                this.txt_TestNumber.Value = Convert.ToInt32(model_cpr.TestNumber).ToString();

                //监测次数
                this.txt_TestCount.Value = Convert.ToInt32(model_cpr.TestCount).ToString();

                //累计监测点次
                this.txt_PointSum.Value = Convert.ToInt32(model_cpr.PointSum).ToString();
                //工期
                this.txt_ProjectDate.Value = model_cpr.ProjectDate.ToString();
                //model_cpr.projextDateOther = this.ProjectDateB.Value + "|" + this.ProjectDateE.Value;
                //model_cpr.ProjectDate = Convert.ToDateTime(this.ProjectDateB.Value).CompareTo(Convert.ToDateTime(this.ProjectDateE.Value));
                //this.ProjectDateB.Value = model_cpr.projextDateOther == null ? "" : Convert.ToDateTime(model_cpr.projextDateOther.Split('|')[0]).ToString("yyyy-MM-dd");
                //this.ProjectDateE.Value = model_cpr.projextDateOther == null ? "" : Convert.ToDateTime(model_cpr.projextDateOther.Split('|')[1]).ToString("yyyy-MM-dd");



            }
        }
        // 检查是否在审核队列中
        private bool CheckAudit(int coperationSysNo)
        {
            return new TG.BLL.cm_CoperationAudit().IsExist(coperationSysNo);
        }
        //修改合同信息
        protected bool UpdateCoperation()
        {
            TG.BLL.cm_PitCoperation bll_cpr = new TG.BLL.cm_PitCoperation();
            TG.BLL.cm_Coperation bll_cop = new TG.BLL.cm_Coperation();
            TG.Model.cm_PitCoperation model_cpr = bll_cpr.GetModel(int.Parse(this.hid_cprid.Value));
            TG.Model.cm_Coperation model_cop = new TG.Model.cm_Coperation();
            if (model_cpr.cpr_FID != null)
            {
                model_cop = bll_cop.GetModel(int.Parse(model_cpr.cpr_FID.ToString()));
            }
            //赋值客户id
            model_cpr.cst_Id = int.Parse(this.hid_cstid.Value);
            model_cop.cst_Id = int.Parse(this.hid_cstid.Value);
            //合同编号
            model_cpr.cpr_No = this.txtcpr_No.Value;
            model_cop.cpr_No = this.txtcpr_No.Value;
            if (this.hid_cprid.Value != "")
            {
                model_cpr.cpr_Id = Convert.ToInt32(this.hid_cprid.Value);
            }
            //合同分类
            model_cpr.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            model_cop.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            //合同类型
            model_cpr.cpr_Type2 = this.txt_cprType.InnerText;
            model_cop.cpr_Type2 = this.txt_cprType.InnerText;
            //合同名称
            model_cpr.cpr_Name = this.txt_cprName.Value;
            model_cop.cpr_Name = this.txt_cprName.Value;
            //基坑等级
            model_cpr.PitLevel = this.ddcpr_PitLevel.SelectedItem.Text;
            //建设单位
            model_cpr.BuildUnit = this.txt_cprBuildUnit.Value;
            model_cop.BuildUnit = this.txt_cprBuildUnit.Value;
            //占地面积
            model_cpr.BuildArea = Convert.ToDecimal(this.txt_BuildArea.Value);
            model_cop.BuildArea = "0";
            //建筑类型
            model_cpr.BuildType = this.ddcpr_BuildType.SelectedItem.Text.Trim();
            model_cop.BuildType = this.ddcpr_BuildType.SelectedItem.Text.Trim();
            //地貌单元
            //model_cpr.EarthUnit = this.txt_EarthUnit.Value;
            //建筑层数
            model_cpr.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            model_cop.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            //工程负责人电话
            model_cpr.ChgPeople = this.txt_proFuze.Value;
            model_cop.ChgPeople = this.txt_proFuze.Value;
            model_cpr.ChgPhone = this.txt_fzphone.Value;
            model_cop.ChgPhone = this.txt_fzphone.Value;
            //承接部门
            model_cpr.cpr_Unit = this.txt_cjbm.Value;
            model_cop.cpr_Unit = this.txt_cjbm.Value;
            if (this.txt_cjbm.Value.Trim() == "")
            {
                model_cpr.cpr_Unit = this.hid_cjbm.Value;
                model_cop.cpr_Unit = this.hid_cjbm.Value;
            }
            //甲方负责人电话
            model_cpr.ChgJia = this.txtFParty.Value;
            model_cop.ChgJia = this.txtFParty.Value;
            model_cpr.ChgJiaPhone = this.txt_jiafphone.Value;
            model_cop.ChgJiaPhone = this.txt_jiafphone.Value;
            //工程地点
            if (this.txt_ProjectPosition.Value.Trim() != "")
            {
                model_cpr.BuildPosition = this.txt_ProjectPosition.Value;
                model_cop.BuildPosition = this.txt_ProjectPosition.Value;
            }
            else
            {
                model_cpr.BuildPosition = "";
                model_cop.BuildPosition = "";
            }
            //合同额
            if (this.txtcpr_Account.Value.Trim() == "")
            {
                model_cpr.cpr_Acount = 0;
                model_cop.cpr_Acount = 0;
            }
            else
            {
                model_cpr.cpr_Acount = Convert.ToDecimal(this.txtcpr_Account.Value);
                model_cop.cpr_Acount = Convert.ToDecimal(this.txtcpr_Account.Value);
            }
            //实际合同额
            if (this.txtcpr_Account0.Value.Trim() == "")
            {
                model_cpr.cpr_ShijiAcount = 0;
                model_cop.cpr_ShijiAcount = 0;
            }
            else
            {
                model_cpr.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);
                model_cop.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);
            }
            //行业性质
            if (this.ddProfessionType.SelectedIndex != 0)
            {
                model_cpr.Industry = this.ddProfessionType.SelectedItem.Text;
                model_cop.Industry = this.ddProfessionType.SelectedItem.Text;
            }
            else
            {
                model_cpr.Industry = this.ddProfessionType.Items[0].Value;
                model_cop.Industry = this.ddProfessionType.Items[0].Value;
            }
            //基坑深度
            if (this.txt_PitHeight.Value.Trim() == "")
            {
                model_cpr.PitHeight = 0;
            }
            else
            {
                model_cpr.PitHeight = Convert.ToDecimal(this.txt_PitHeight.Value.Trim());
            }
            //边坡高度
            if (this.txt_SlopeHeight.Value.Trim() == "")
            {
                model_cpr.SlopeHeight = 0;
            }
            else
            {
                model_cpr.SlopeHeight = Convert.ToDecimal(this.txt_SlopeHeight.Value.Trim());
            }
            //位移监测点数量
            if (this.txt_PointNumber.Value.Trim() == "")
            {
                model_cpr.PointNumber = 0;
            }
            else
            {
                model_cpr.PointNumber = Convert.ToInt32(this.txt_PointNumber.Value.Trim());
            }
            //沉降观测点数量
            if (this.txt_TestNumber.Value.Trim() == "")
            {
                model_cpr.TestNumber = 0;
            }
            else
            {
                model_cpr.TestNumber = Convert.ToInt32(this.txt_TestNumber.Value.Trim());
            }
            //监测次数
            if (this.txt_TestCount.Value.Trim() == "")
            {
                model_cpr.TestCount = 0;
            }
            else
            {
                model_cpr.TestCount = Convert.ToInt32(this.txt_TestCount.Value.Trim());
            }
            //累计监测点次
            if (this.txt_PointSum.Value.Trim() == "")
            {
                model_cpr.PointSum = 0;
            }
            else
            {
                model_cpr.PointSum = Convert.ToInt32(this.txt_PointSum.Value.Trim());
            }
            //更新ad
            bool affectRow = false;
            //结构样式
            //if (this.txtInvestAccount.Value.Trim() == "")
            //{
            //    model_cpr.cpr_Touzi = 0;
            //}
            //else
            //{
            //    model_cpr.cpr_Touzi = Convert.ToDecimal(this.txtInvestAccount.Value);
            //}
            //if (txtInvestAccount0.Value.Trim() == "")
            //{
            //    model_cpr.cpr_ShijiTouzi = 0;
            //}
            //else
            //{
            //    model_cpr.cpr_ShijiTouzi = Convert.ToDecimal(txtInvestAccount0.Value);
            //}
            //newAdd

            //合同阶段
            //string str_process = "";
            //foreach (ListItem chk in this.chk_cprjd.Items)
            //{
            //    if (chk.Selected)
            //    {
            //        str_process += chk.Value + ",";
            //    }
            //}
            //str_process = str_process.IndexOf(",") > -1 ? str_process.Remove(str_process.Length - 1) : "";
            //model_cpr.cpr_Process = str_process;
            //工期
            //model_cpr.projextDateOther = this.ProjectDateB.Value + "|" + this.ProjectDateE.Value;
            model_cpr.ProjectDate = Convert.ToInt32(this.txt_ProjectDate.Value);
            //工程来源
            if (this.ddSourceWay.SelectedIndex != 0)
            {
                model_cpr.BuildSrc = this.ddSourceWay.SelectedItem.Text;
                model_cop.BuildSrc = this.ddSourceWay.SelectedItem.Text;
            }
            else
            {
                model_cpr.BuildSrc = this.ddSourceWay.Items[0].Value;
                model_cop.BuildSrc = this.ddSourceWay.Items[0].Value;
            }
            //合同统计年份
            model_cpr.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            model_cop.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
            //合同开工日期
            model_cpr.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value);
            model_cop.cpr_SignDate2 = Convert.ToDateTime(this.txtSingnDate2.Value);
            //合同完成日期
            model_cpr.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            model_cop.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            //制表人
            model_cpr.TableMaker = this.txt_tbcreate.Value;
            model_cop.TableMaker = this.txt_tbcreate.Value;
            //工期====
            //勘探点数量
            //if (this.txt_PointNumber.Value.Trim() == "")
            //{
            //    model_cpr.PointNumber = 0;
            //}
            //else
            //{
            //    model_cpr.PointNumber = Convert.ToInt32(this.txt_PointNumber.Value.Trim());
            //}
            //波速实验钻孔数量
            //if (this.txt_WaveNumber.Value.Trim() == "")
            //{
            //    model_cpr.WaveNumber = 0;
            //}
            //else
            //{
            //    model_cpr.WaveNumber = Convert.ToInt32(this.txt_WaveNumber.Value.Trim());
            //}
            //控制性钻孔数量
            //if (this.txt_ControlNumber.Value.Trim() == "")
            //{
            //    model_cpr.ControlNumber = 0;
            //}
            //else
            //{
            //    model_cpr.ControlNumber = Convert.ToInt32(this.txt_ControlNumber.Value.Trim());
            //}
            //一般性钻孔数量
            //if (this.txt_GenerNumber.Value.Trim() == "")
            //{
            //    model_cpr.GenerNumber = 0;
            //}
            //else
            //{
            //    model_cpr.GenerNumber = Convert.ToInt32(this.txt_GenerNumber.Value.Trim());
            //}
            //波速试验钻孔深度、
            //if (this.txt_WaveHeight.Value.Trim() == "")
            //{
            //    model_cpr.WaveHeight = 0;
            //}
            //else
            //{
            //    model_cpr.WaveHeight = Convert.ToDecimal(this.txt_WaveHeight.Value.Trim());
            //}
            //控制性钻孔深度
            //if (this.txt_ControlHeight.Value.Trim() == "")
            //{
            //    model_cpr.ControlHeight = 0;
            //}
            //else
            //{
            //    model_cpr.ControlHeight = Convert.ToDecimal(this.txt_ControlHeight.Value.Trim());
            //}
            //一般性钻孔深度
            //if (this.txt_GenerHeight.Value.Trim() == "")
            //{
            //    model_cpr.GenerHeight = 0;
            //}
            //else
            //{
            //    model_cpr.GenerHeight = Convert.ToDecimal(this.txt_GenerHeight.Value.Trim());
            //}
            //工程规模
            //if (this.txt_ProjectArea.Value.Trim() == "")
            //{
            //    model_cpr.ProjectArea = 0;
            //}
            //else
            //{
            //    model_cpr.ProjectArea = Convert.ToDecimal(this.txt_ProjectArea.Value.Trim());
            //}
            //工程特征
            //model_cpr.Features = this.txt_Features.Value.Trim();
            //工期
            //if (this.txt_ProjectDate.Value.Trim() == "")
            //{
            //    model_cpr.ProjectDate = 0;
            //}
            //else
            //{
            //    model_cpr.ProjectDate = Convert.ToInt32(this.txt_ProjectDate.Value.Trim());
            //}
            //多吨楼备注--拟采用基础选型及地基处理方案
            model_cpr.MultiBuild = this.txt_MultiBuild.Value;
            model_cop.MultiBuild = this.txt_MultiBuild.Value;
            //合同备注
            model_cpr.cpr_Mark = this.txtcpr_Remark.Value;
            model_cop.cpr_Mark = this.txtcpr_Remark.Value;
            //model_cpr.BuildArea = this.txt_buildArea.Value;
            //model_cpr.RegTime = DateTime.Now;
            model_cpr.UpdateBy = UserSysNo.ToString();
            model_cop.UpdateBy = UserSysNo.ToString();
            model_cpr.LastUpdate = DateTime.Now;
            model_cop.LastUpdate = DateTime.Now;
            //结构形式
            //update 20130530 qpl
            //model_cpr.StructType = GetDropDownTreeCheckedValue(this.asTreeviewStruct.RootNode.ChildNodes);
            //建筑分类
            //update 20130530 qpl
            //model_cpr.BuildStructType = GetDropDownTreeCheckedValue(this.asTreeviewStructType.RootNode.ChildNodes);


            //专业特征

            //录入人
            //model_cpr.InsertUserID = UserSysNo;
            //项目经理人ID  qpl  20131225
            //model_cpr.PMUserID = int.Parse(this.HiddenPMUserID.Value);
            try
            {
                affectRow = bll_cpr.Update(model_cpr);
                bll_cop.Update(model_cop);

            }
            catch (System.Exception ex)
            {
            }

            return affectRow;
        }

    }
}