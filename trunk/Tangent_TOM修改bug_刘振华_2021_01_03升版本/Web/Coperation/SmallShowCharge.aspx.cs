﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace TG.Web.Coperation
{
    public partial class SmallShowCharge : System.Web.UI.Page
    {
        //提醒时间
        private string WarningDay
        {
            get
            {
                return ConfigurationManager.AppSettings["WarningDay"].ToString();
            }
        }
        //合同ID
        public string CprId
        {
            get
            {
                return Request.QueryString["cprid"] ?? "";
            }
        }
        //操作
        public string IsDelele
        {
            get
            {
                string val = Request.QueryString["math"] ?? "";
                return val == "" ? "0" : "1";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Expires = -1;
            if (!IsPostBack)
            {
                //绑定数据
                BindChargeList();
            }
        }
        //绑定收款信息
        protected void BindChargeList()
        {
            string str_cprid = Request.QueryString["cprid"] ?? "0";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            string strSql = " SELECT ID,Times,persent,payCount,paytime,(SELECT SUM(payCount) FROM cm_RealCprChg WHERE chg_id=cm_CoperationCharge.ID) AS payShiCount,mark FROM cm_CoperationCharge WHERE cpr_ID=" + str_cprid;
            this.gv_Coperation.DataSource = bll_db.GetList(strSql);
            this.gv_Coperation.DataBind();
        }
        //行绑定
        protected void gv_Coperation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[1].Text = e.Row.Cells[1].Text + "%";
                //状态
                decimal plancount = Convert.ToDecimal(e.Row.Cells[2].Text);
                Label lbl_shicount = e.Row.Cells[4].FindControl("Label1") as Label;
                decimal shicount = 0.00m;
                if (!string.IsNullOrEmpty(lbl_shicount.Text))
                {
                    shicount = Convert.ToDecimal(lbl_shicount.Text);
                }
                lbl_shicount.Text = shicount.ToString();
                //是否完成收款
                char flag = shicount >= plancount ? '1' : '0';
                //计划时间
                string plantime = e.Row.Cells[5].Text.ToString();
                //收款id
                HiddenField hid_chg = e.Row.Cells[6].FindControl("HiddenField1") as HiddenField;
                if (GetPlanStatus(plantime, flag) == -1)
                {
                    e.Row.Cells[5].Text = "完成";
                    e.Row.Cells[5].CssClass = "cls_status_green";
                    //有逾期的收款
                    if (IsOverInput(hid_chg.Value))
                    {
                        e.Row.ForeColor = System.Drawing.Color.Red;
                    }
                }
                else if (GetPlanStatus(plantime, flag) == 0)
                {
                    e.Row.Cells[5].Text = "正常";
                    e.Row.Cells[5].CssClass = "cls_status_blue";
                }
                else if (GetPlanStatus(plantime, flag) == 1)
                {
                    e.Row.Cells[5].Text = "提醒";
                    e.Row.Cells[5].CssClass = "cls_status_yellow";
                }
                else
                {
                    e.Row.Cells[5].Text = "逾期";
                    e.Row.Cells[5].CssClass = "cls_status_red";
                }
            }
        }
        //查看是否有逾期收款
        protected bool IsOverInput(string chgid)
        {
            TG.BLL.cm_RealCprChg bll_rl = new TG.BLL.cm_RealCprChg();
            string strWhere = " isover=1 AND chg_Id=" + chgid;
            List<TG.Model.cm_RealCprChg> list = bll_rl.GetModelList(strWhere);
            if (list.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //返回收费计划状态
        protected int GetPlanStatus(string plantime, char flag)
        {
            DateTime nowdate = DateTime.Now;
            DateTime plandate = Convert.ToDateTime(plantime);
            //DiffDate
            TimeSpan t1 = new TimeSpan(nowdate.Ticks);
            TimeSpan t2 = new TimeSpan(plandate.Ticks);
            TimeSpan t3 = t2.Subtract(t1);
            if (flag == '1')
            {
                return -1;
            }
            else if (t3.Days > int.Parse(WarningDay))
            {
                return 0;
            }
            else if (t3.Days < int.Parse(WarningDay) && t3.Days >= 0)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }
    }
}
