﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Data;
using TG.Web.HttpHandler;
using Geekees.Common.Controls;
using System.Xml;

namespace TG.Web.Coperation
{
    public partial class cpr_CorperationListBymaster : PageBase
    {
        public string ColumnsContent
        {
            get;
            set;
        }
        
        public string asTreeviewStructObjID
        {
            get
            {
                return this.asTreeviewStruct.GetClientTreeObjectId();
            }
        }
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructType.GetClientTreeObjectId();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定字段
                BindColumns();
            
                //绑定树形部门
                LoadUnitTree();
                //默认选中单位
                SelectedCurUnit();
                //绑定单位
                //BindUnit();
                //绑定年份
                BindYear();
                //选中当前年份
                SelectCurrentYear();
                //绑定合同
                BindCoperation();
                //绑定权限
                BindPreviewPower();

                //高级查询
                //合同类别
                BindCorpType();
                //合同阶段
                BindCorpProc();
                //项目地点
                BindDdList();
                //行业性质
                BindXzList();
                //绑定建筑结构样式修改时
                BindStructTypeEdit();
                //绑定建筑分类修改时
                BindBuildStuctTypeEdit();
                //设计等级
                BindBuildType();

                //绑定登陆用户ID
                this.hid_curuser.Value = UserSysNo.ToString();
            }
        }
        //基本合同
        protected void BindColumns()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("全选", "all");
            dic.Add("合同编号", "cpr_No");
            dic.Add("合同文本编号", "cpr_Type2");
            dic.Add("工程名称", "cpr_Name");
            dic.Add("合同分类", "cpr_Type");
            dic.Add("建筑规模(㎡)", "BuildArea");
            dic.Add("合同额(万元)", "cpr_Acount");
            dic.Add("实际合同额(万元)", "cpr_ShijiAcount");
            dic.Add("子公司", "cpr_Unit");
            dic.Add("签约日期", "tjrq");
           // dic.Add("完成日期", "wcrq");
          //  dic.Add("统计年份", "tjrq");
            dic.Add("工程负责人", "PMUserName");
            dic.Add("已收费(万元)", "ssze");
            dic.Add("录入时间", "lrsj");
          //  dic.Add("合同类型", "cpr_Type2");
            dic.Add("建设单位", "BuildUnit");
            dic.Add("甲方类型", "BuildType");
            dic.Add("结构形式", "StructType");
            dic.Add("建筑分类", "BuildStructType");
         //   dic.Add("层数", "Floor");
            dic.Add("工程负责人电话", "ChgPhone");
            dic.Add("甲方负责人", "ChgJia");
            dic.Add("甲方负责人电话", "ChgJiaPhone");
            dic.Add("工程地点", "BuildPosition");
          //  dic.Add("行业性质", "Industry");
            
         //   dic.Add("投资额(万元)", "cpr_Touzi");
          //  dic.Add("实际投资额(万元)", "cpr_ShijiTouzi");
         //   dic.Add("工程来源", "BuildSrc");
         //   dic.Add("合同阶段", "cpr_Process");
         //   dic.Add("制表人", "TableMaker");
        //    dic.Add("多栋楼", "MultiBuild");
            dic.Add("合同备注", "cpr_Mark");
            dic.Add("录入人", "InsertUser");
            dic.Add("客户名称", "cstName");

            foreach (KeyValuePair<string, string> pair in dic)
            {
                ColumnsContent += "<label><input type='checkbox' value='" + pair.Value + "' />" + pair.Key + "</label>";
            }


            //  BindProInfoConfig(this.asTreeviewStruct.RootNode,dic);
            //this.asTreeviewStruct.CollapseAll();           
        }
 
        /// <summary>
        /// 默认选中所有单位
        /// </summary>
        protected void SelectedCurUnit()
        {
            string[] curUnit = { "0" };
            this.drp_unit.CheckNodes(curUnit, true);
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            TG.BLL.tg_unit bllUnit = new TG.BLL.tg_unit();
            string strWhere = "";
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID =" + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";
            DataTable dt = bllUnit.GetList(strWhere).Tables[0];

            return dt;
        }
        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drp_unit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全院部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
        }

        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drp_unit.Theme = macOS;
            this.asTreeviewStruct.Theme = macOS;
            this.asTreeviewStructType.Theme = macOS;
        }
        /// <summary>
        /// 选择年份
        /// </summary>
        protected void SelectCurrentYear()
        {
            string curyear = DateTime.Now.Year.ToString();
            if (this.drp_year.Items.FindByText(curyear) != null)
            {
                this.drp_year.Items.FindByText(curyear).Selected = true;
            }
        }
        //绑定权限
        public void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                this.userShortName.Value = base.UserShortName;
                this.previewPower.Value = base.RolePowerParameterEntity.PreviewPattern.ToString();
                this.userSysNum.Value = base.UserSysNo.ToString();
                this.userUnitNum.Value = base.UserUnitNo.ToString();
            }
        }
        /// <summary>
        /// 权限控制
        /// </summary>
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// 控制权限
        /// </summary>
        /// <param name="sb"></param>
        private void GetPreviewPowerSql(ref StringBuilder sb)
        {
            //个人
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                sb.Append(" AND (InsertUserID =" + UserSysNo + " OR PMUserID=" + UserSysNo + ") ");
            }//部门
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                sb.Append(" AND cpr_Unit = (Select unit_Name From tg_unit Where unit_ID=" + UserUnitNo + ")");
            }
        }
        //合同阶段
        protected void BindCorpProc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_jd'";          
            this.chk_cprjd.DataSource = bll_dic.GetList(str_where);
            this.chk_cprjd.DataTextField = "dic_Name";
            this.chk_cprjd.DataValueField = "ID";
            this.chk_cprjd.DataBind();
           
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            //绑定合同类别
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            DataSet ds = bll_dic.GetList(str_where);
            this.drp_type.DataSource = ds;
            this.drp_type.DataTextField = "dic_Name";
            this.drp_type.DataValueField = "ID";
            this.drp_type.DataBind();

            str_where = " dic_Type='cpr_lx'";
            DataSet ds1 = bll_dic.GetList(str_where);
            this.txt_ddcpr_Type.DataSource = ds1;
            this.txt_ddcpr_Type.DataTextField = "dic_Name";
            this.txt_ddcpr_Type.DataValueField = "ID";
            this.txt_ddcpr_Type.DataBind();           
          
        }
        //建筑类别
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
            DataSet ds = bll_dic.GetList(str_where);
            this.drp_buildtype.DataSource = ds;
            this.drp_buildtype.DataTextField = "dic_Name";
            this.drp_buildtype.DataValueField = "ID";
            this.drp_buildtype.DataBind();

        }
        //建筑分类
        protected void BindBuildStuctTypeEdit()
        {
            BindProInfoConfig("BuildType", this.asTreeviewStructType.RootNode);
            this.asTreeviewStructType.CollapseAll();
        }
        //结构形式
        protected void BindStructTypeEdit()
        {
            BindProInfoConfig("StructType", this.asTreeviewStruct.RootNode);
            this.asTreeviewStruct.CollapseAll();
        }
        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }
        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }

                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        //绑定项目来源
        public void BindDdList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();

            DataSet dic_proly = dic.GetList(" dic_type='cpr_src'");
            ddsource.DataValueField = "id";
            ddsource.DataTextField = "dic_name";
            ddsource.DataSource = dic_proly;
            ddsource.DataBind();
      
        }
        //绑定行业性质
        protected void BindXzList()
        {
            TG.BLL.cm_Dictionary dic = new TG.BLL.cm_Dictionary();
            DataSet dic_proly = dic.GetList(" dic_type='cpr_hyxz'");
            ddType.DataValueField = "id";
            ddType.DataTextField = "dic_name";
            ddType.DataSource = dic_proly;
            ddType.DataBind();          
        }
        /// <summary>
        /// 数据绑定
        /// </summary>
        public void BindCoperation()
        {
            TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();
            StringBuilder strWhere = new StringBuilder("");
            //合同名称

            //判断个人或全部
            GetPreviewPowerSql(ref strWhere);
            this.hid_where.Value = strWhere.ToString();


        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }


        }
        ///// <summary>
        ///// 绑定生产部门
        ///// </summary>
        //protected void BindUnit()
        //{
        //    TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
        //    string strWhere = "";
        //    //如果只能查看个人数据
        //    if (base.RolePowerParameterEntity.PreviewPattern == 0)
        //    {
        //        strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
        //    }
        //    else if (base.RolePowerParameterEntity.PreviewPattern == 2)
        //    {
        //        strWhere = " unit_ID= " + UserUnitNo;
        //    }
        //    else
        //    {
        //        strWhere = " 1=1 ";
        //    }
        //    //不显示的单位
        //    strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

        //    this.drp_unit.DataSource = bll_unit.GetList(strWhere);
        //    this.drp_unit.DataTextField = "unit_Name";
        //    this.drp_unit.DataValueField = "unit_ID";
        //    this.drp_unit.DataBind();
        //}

        /// <summary>
        /// 获取所有选中部门名称
        /// </summary>
        /// <returns></returns>
        public string GetCheckedNodesName()
        {
            StringBuilder sbUnitlist = new StringBuilder("");
            List<ASTreeViewNode> nodes = this.drp_unit.GetCheckedNodes(false);

            foreach (ASTreeViewNode node in nodes)
            {
                if (node.NodeValue == "0")
                    break;

                sbUnitlist.AppendFormat("'{0}',", node.NodeText);
            }
            //判断部门不为空
            if (sbUnitlist.ToString() != "")
                sbUnitlist.Remove(sbUnitlist.ToString().LastIndexOf(','), 1);

            return sbUnitlist.ToString();
        }
        protected void btn_export_Click(object sender, EventArgs e)
        {
            TG.BLL.cm_Coperation bll = new TG.BLL.cm_Coperation();
            StringBuilder strWhere = new StringBuilder("");
            //合同名称
            if (this.txt_keyname.Value != "")
            {
                string keyname = TG.Common.StringPlus.SqlSplit(this.txt_keyname.Value.Trim());
                strWhere.AppendFormat(" AND cpr_Name LIKE '%{0}%'", keyname);
            }
            //按照部门
            string columnsname = GetCheckedNodesName();
            if (columnsname != "")
            {
                strWhere.AppendFormat(" AND (cpr_Unit in ({0}))", columnsname);

            }
            //按照年份
            if (this.drp_year.SelectedIndex != 0)
            {
                strWhere.AppendFormat(" AND year(cpr_SignDate)={0}", this.drp_year.SelectedValue);
            }


            //录入时间  
            string startTime = txt_start.Value;//录入时间
            string endTime = txt_end.Value;//录入时间
            if (!string.IsNullOrEmpty(startTime) && string.IsNullOrEmpty(endTime))
            {
                strWhere.AppendFormat(" AND InsertDate>='{0}' ", startTime + "  00:00:00");
            }
            else if (string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                strWhere.AppendFormat(" AND InsertDate<='{0}' ", endTime + " 23:59:59");
            }
            else if (!string.IsNullOrEmpty(startTime) && !string.IsNullOrEmpty(endTime))
            {
                strWhere.AppendFormat(" AND InsertDate between '{0}' and '{1}' ", startTime + "  00:00:00", endTime + " 23:59:59");
            }

            //合同分类
            if (this.drp_type.SelectedIndex > 0)
            {
                strWhere.AppendFormat(" AND cpr_Type='{0}' ", this.drp_type.Items[this.drp_type.SelectedIndex].Text);
            }

            string buildunit = this.txt_cprbuildunit.Value.Trim();
            //按建设单位查询
            if (buildunit != "")
            {
                buildunit = TG.Common.StringPlus.SqlSplit(buildunit);
                strWhere.AppendFormat(" AND BuildUnit like '%{0}%'", buildunit);
            }
            //合同额
            if (!string.IsNullOrEmpty(this.txt_account1.Value) && string.IsNullOrEmpty(this.txt_account2.Value))
            {
                strWhere.AppendFormat(" AND cpr_Acount>={0} ", this.txt_account1.Value);
            }
            else if (string.IsNullOrEmpty(this.txt_account1.Value) && !string.IsNullOrEmpty(this.txt_account2.Value))
            {
                strWhere.AppendFormat(" AND cpr_Acount<={0} ", this.txt_account2.Value);
            }
            else if (!string.IsNullOrEmpty(this.txt_account1.Value) && !string.IsNullOrEmpty(this.txt_account2.Value))
            {
                strWhere.AppendFormat(" AND cpr_Acount between {0} and {1} ", this.txt_account1.Value, this.txt_account2.Value);
            }
          
            //判断个人或全部
            GetPreviewPowerSql(ref strWhere);

            DataTable dt = bll.GetCoperationExportInfo(strWhere.ToString()).Tables[0];

            ExportDataToExcel(dt, "~/TemplateXls/ReportCoperationList.xls", "合同列表");

        }
        private void ExportDataToExcel(DataTable dt, string modelPath, string pathname)
        {
            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet
            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);
                    fileStream.Close();
                }
            }

            ICellStyle style1 = wb.CreateCellStyle();
            style1.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style1.VerticalAlignment = VerticalAlignment.CENTER;
            style1.WrapText = true;
            //style1.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            //style1.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font1 = wb.CreateFont();
            font1.FontHeightInPoints = 12;//字号
            font1.FontName = "宋体";//字体
            font1.Boldweight = (short)700;
            style1.SetFont(font1);


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            string sheetName = "sheet1";
            //("{0}年(1-{1})【已签订合同额】与{2} 年同期对比表"),, this.drp_month.SelectedItem.Value, (Convert.ToInt32(this.drp_year.SelectedItem.Value)-1)
            var ws = wb.GetSheet(sheetName);
            if (ws == null)
                ws = wb.GetSheetAt(0);

            wb.SetSheetName(0, sheetName);

            IRow dataRowTitle = ws.GetRow(1);
            //ws.SetColumnWidth(2, 25 * 256);
            string str_columnschinaname = this.hid_cols.Value.Trim();
            string str_columnsname = this.hid_colsvalue.Value.Trim();
            //字段标题
            if (!string.IsNullOrEmpty(str_columnschinaname))
            {
                string[] columnsnamelist = str_columnschinaname.Split(',');
                for (int j = 0; j < columnsnamelist.Length; j++)
                {
                    ICell celltitle = dataRowTitle.CreateCell(j + 1);
                    celltitle.CellStyle = style2;
                    celltitle.SetCellValue(columnsnamelist[j]);
                }
            }

            int index = 2;
            decimal sumarea = 0, sumaccount = 0, sumssze = 0;
            int areaIndex = 4, acountIndex = 5, sszeIndex = 10;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(i + index);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(i + index);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(i + 1);

                //cell = dataRow.CreateCell(1);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["cpr_No"].ToString());

                //cell = dataRow.CreateCell(2);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["cpr_Name"].ToString());

                //cell = dataRow.CreateCell(3);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["cpr_Type"].ToString());


                //cell = dataRow.CreateCell(4);
                //cell.CellStyle = style2;
                //decimal buildare = Convert.ToDecimal(dt.Rows[i]["BuildArea"]);
                //cell.SetCellValue(buildare.ToString());
                //sumarea += buildare;

                //cell = dataRow.CreateCell(5);
                //cell.CellStyle = style2;
                //decimal account = Convert.ToDecimal(dt.Rows[i]["cpr_Acount"]);
                //cell.SetCellValue(account.ToString("f4"));
                //sumaccount += account;

                //cell = dataRow.CreateCell(6);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["cpr_Unit"].ToString());

                //cell = dataRow.CreateCell(7);
                //cell.CellStyle = style2;
                //cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["cpr_SignDate"]).ToString("yyyy-MM-dd"));

                //cell = dataRow.CreateCell(8);
                //cell.CellStyle = style2;
                //cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["cpr_DoneDate"]).ToString("yyyy-MM-dd"));

                //cell = dataRow.CreateCell(9);
                //cell.CellStyle = style2;
                //cell.SetCellValue(dt.Rows[i]["ChgPeople"].ToString());

                //cell = dataRow.CreateCell(10);
                //cell.CellStyle = style2;
                //decimal ssze = 0;
                //if (!string.IsNullOrEmpty(dt.Rows[i]["ssze"].ToString()))
                //{
                //    ssze = Convert.ToDecimal(dt.Rows[i]["ssze"]);
                //}
                //cell.SetCellValue(ssze.ToString());
                //sumssze += ssze;

                //cell = dataRow.CreateCell(11);
                //cell.CellStyle = style2;
                //if (!string.IsNullOrEmpty(dt.Rows[i]["InsertDate"].ToString()))
                //{
                //    cell.SetCellValue(Convert.ToDateTime(dt.Rows[i]["InsertDate"]).ToString("yyyy-MM-dd"));
                //}

                if (!string.IsNullOrEmpty(str_columnsname))
                {
                    decimal buildare = Convert.ToDecimal(dt.Rows[i]["BuildArea"]);
                    sumarea += buildare;
                    decimal ssze = 0;
                    if (!string.IsNullOrEmpty(dt.Rows[i]["ssze"].ToString()))
                    {
                        ssze = Convert.ToDecimal(dt.Rows[i]["ssze"]);
                    }
                    sumssze += ssze;
                    decimal account = Convert.ToDecimal(dt.Rows[i]["cpr_Acount"]);
                    sumaccount += account;
                    string[] columnslist = str_columnsname.Split(',');
                    for (int j = 0; j < columnslist.Length; j++)
                    {
                        cell = dataRow.CreateCell(j + 1);
                        cell.CellStyle = style2;
                        string cellvalue = "";
                        if (columnslist[j].Trim() == "Floor")
                        {
                            if (!string.IsNullOrEmpty(cellvalue))
                            {
                                string cellvalueitem = dt.Rows[i][columnslist[j].Trim()].ToString();
                                string[] arr = cellvalueitem.Split('|');
                                cellvalue = "地上:" + arr[0] + " 地下:" + arr[1];
                            }

                        }
                        else if (columnslist[j].Trim() == "cpr_Process")
                        {
                            string cellvalueitem = dt.Rows[i][columnslist[j].Trim()].ToString();
                            if (!string.IsNullOrEmpty(cellvalueitem))
                            {
                                string temp = "";
                                string[] arr = cellvalueitem.Split(',');
                                for (int k = 0; k < arr.Length; k++)
                                {
                                    if (arr[k] == "27")
                                    {
                                        temp = "方案,";
                                    }
                                    if (arr[k] == "28")
                                    {
                                        temp += "初设,";
                                    }
                                    if (arr[k] == "29")
                                    {
                                        temp += "施工图,";
                                    }
                                    if (arr[k] == "30")
                                    {
                                        temp += "其他,";
                                    }
                                }
                                cellvalue = temp;
                            }

                        }
                        else
                        {
                            cellvalue = dt.Rows[i][columnslist[j].Trim()].ToString();
                        }
                        if (columnslist[j].Trim() == "BuildArea")
                        {
                            areaIndex = j + 1;
                        }
                        if (columnslist[j].Trim() == "cpr_Acount")
                        {
                            acountIndex = j + 1;
                        }
                        if (columnslist[j].Trim() == "ssze")
                        {
                            sszeIndex = j + 1;
                        }
                        cell.SetCellValue(cellvalue);
                    }
                }


            }
            IRow dataRowbottom = ws.CreateRow(dt.Rows.Count + 2);//最后一个
            ICell cellbottom = dataRowbottom.CreateCell(1);
            cellbottom.SetCellValue("合计：");
            cellbottom.CellStyle = style1;

            if (str_columnsname.Contains("BuildArea"))
            {
                cellbottom = dataRowbottom.CreateCell(areaIndex);
                cellbottom.SetCellValue(sumarea.ToString("f2"));
                cellbottom.CellStyle = style1;
            }
            if (str_columnsname.Contains("cpr_Acount"))
            {
                cellbottom = dataRowbottom.CreateCell(acountIndex);
                cellbottom.SetCellValue(sumaccount.ToString("f4"));
                cellbottom.CellStyle = style1;
            }
            if (str_columnsname.Contains("ssze"))
            {
                cellbottom = dataRowbottom.CreateCell(sszeIndex);
                cellbottom.SetCellValue(sumssze.ToString("f4"));
                cellbottom.CellStyle = style1;
            }

            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(pathname + ".xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }
        public void WriteExcelValue(NPOI.SS.UserModel.ICell cell, object value)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                Type[] typeStrings = new Type[] { typeof(string) };
                Type[] typeNumbers = new Type[] { typeof(int), typeof(short), typeof(long), typeof(byte), typeof(float), typeof(double), typeof(decimal) };
                Type[] typeDateTimes = new Type[] { typeof(DateTime) };
                Type[] typeBools = new Type[] { typeof(bool) };


                if (typeStrings.Contains(value.GetType()))
                {
                    cell.SetCellValue(value.ToString());

                }
                else if (typeNumbers.Contains(value.GetType()))
                {
                    cell.SetCellValue(Convert.ToDouble(value));

                }
                else if (typeDateTimes.Contains(value.GetType()))
                {
                    cell.SetCellValue((DateTime)value);

                }
                else if (typeBools.Contains(value.GetType()))
                {
                    cell.SetCellValue((bool)value);

                }
                else
                {
                    cell.SetCellValue(value.ToString());

                }
            }
        }

    }
}