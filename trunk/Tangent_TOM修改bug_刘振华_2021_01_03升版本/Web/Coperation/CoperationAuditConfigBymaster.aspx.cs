﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using TG.Web.UserControl;

namespace TG.Web.Coperation
{
    public partial class CoperationAuditConfigBymaster : PageBase
    {
        protected override bool IsAuth
        {
            get
            {
                return base.IsAuth;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Utility.RegisterTypeForAjax(typeof(CoperationAuditConfigBymaster));
        }
    }
}