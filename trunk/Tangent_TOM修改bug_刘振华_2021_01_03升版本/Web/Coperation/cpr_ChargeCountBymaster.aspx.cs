﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Geekees.Common.Controls;
using NPOI.SS.Util;

namespace TG.Web.Coperation
{
    public partial class cpr_ChargeCountBymaster : PageBase
    {
        //判断是否是全部查看
        protected string IsCheckAllPower
        {
            get
            {
                if (base.RolePowerParameterEntity.PreviewPattern == 1)
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
        }
        //收款开始时间
        protected string ChargeStartTime { get; set; }
        //收款结束时间
        protected string ChargeEndTime { get; set; }
        //报表名称
        protected string ChartTip { get; set; }
        // 权限控制
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }

        TG.BLL.tg_unit bll_unit = new TG.BLL.tg_unit();
        //加载
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //绑定部门
                LoadUnitTree();
                //绑定年份
                BindYear();
                //初始年
                InitDropDownByYear();
                //初始收款时间
                InitDate();
                //初始查询
                GetCoperationTarget();
            }
        }
        /// <summary>
        /// 加载单位
        /// </summary>
        protected void LoadUnitTree()
        {
            SetDropDownTreeThem();

            DataTable dt = GetUnit();
            if (dt.Rows.Count > 0)
            {
                //声明根节点
                ASTreeViewNode root = this.drpunit.RootNode;
                ASTreeViewNode firstnode = new ASTreeViewNode("全部部门", "0");
                root.AppendChild(firstnode);
                //初始化树控件
                foreach (DataRow dr in dt.Rows)
                {
                    ASTreeViewNode linknode = new ASTreeViewNode(dr["unit_Name"].ToString(), dr["unit_ID"].ToString());
                    linknode.EnableCheckbox = true;
                    firstnode.AppendChild(linknode);
                }
            }
        }
        /// <summary>
        /// 下拉复选框的样式
        /// </summary>
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "/js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.drpunit.Theme = macOS;
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <returns></returns>
        protected DataTable GetUnit()
        {
            string strWhere = "";
            //如果只能查看个人数据
            if (base.RolePowerParameterEntity.PreviewPattern == 0)
            {
                strWhere = " unit_ID IN (Select mem_unit_ID From tg_member Where mem_ID=" + UserSysNo + ")";
            }
            else if (base.RolePowerParameterEntity.PreviewPattern == 2)
            {
                strWhere = " unit_ID= " + UserUnitNo;
            }
            else
            {
                strWhere = " 1=1 ";
            }
            //不显示的单位
            strWhere += " AND unit_ID NOT IN (" + base.NotShowUnitList + ")";

            DataTable dt = bll_unit.GetList(strWhere).Tables[0];

            return dt;
        }
        //初始年份
        protected void InitDropDownByYear()
        {
            if (this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()) != null)
            {
                this.drp_year.Items.FindByText(DateTime.Now.Year.ToString()).Selected = true;
            }
        }
        //绑定年份
        protected void BindYear()
        {
            List<string> list = new TG.BLL.cm_Coperation().GetCoperationYear();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    this.drp_year.Items.Add(list[i]);
                }
            }
        }


        //初始时间设置
        protected void InitDate()
        {
            string str_year = DateTime.Now.Year.ToString();
            this.txt_start.Value = str_year + "-01-01";
            this.txt_end.Value = str_year + "-12-31";
            this.txt_year1.Value = str_year + "-01-01";
            this.txt_year2.Value = str_year + "-12-31";
        }
        //创建时间查询条件
        protected void CreateTimeQuery()
        {
            string start = this.txt_start.Value.Trim();
            string end = this.txt_end.Value.Trim();
            string curtime = DateTime.Now.Year.ToString();

            if (string.IsNullOrEmpty(start))
            {
                start = curtime+"-01-01";
            }
            if (string.IsNullOrEmpty(end))
            {
                end = curtime+"-12-31";
            }           

            ChargeStartTime = start;
            ChargeEndTime = end;

            //合同时间
            string startyear = this.txt_year1.Value.Trim();
            string endyear = this.txt_year2.Value.Trim();

            if (string.IsNullOrEmpty(startyear))
            {
                startyear = curtime + "-01-01";
                this.txt_year1.Value = startyear;
            }
            if (string.IsNullOrEmpty(endyear))
            {
                endyear = curtime + "-12-31";
                this.txt_year2.Value = endyear;
            }           

        }
        //统计提示话术
        protected void CreateTipQuery()
        {
            string strTip = "";
            //遍历
            List<ASTreeViewNode> nodes = drpunit.GetCheckedNodes(false);
            foreach (ASTreeViewNode node in nodes)
            {
                if (node.NodeValue == "0")
                {
                    strTip += "全院";
                    break;
                }

                strTip += "[" + node.NodeText + "]";
            }
            if (this.hid_time.Value == "1")
            {
                strTip += this.txt_year1.Value+"至"+this.txt_year2.Value+"区间";
            }
            else
            {
                //年份
                if (this.drp_year.SelectedIndex == 0)
                {
                    strTip += "全部年份";
                }
                else
                {
                    strTip += this.drp_year.SelectedItem.Text.Trim() + "年";
                    if (this.drpJidu.SelectedIndex > 0 && this.drpMonth.SelectedIndex == 0)
                    {
                        strTip += this.drpJidu.SelectedItem.Text.Trim();
                    }
                    else if (this.drpJidu.SelectedIndex == 0 && this.drpMonth.SelectedIndex > 0)
                    {
                        strTip += this.drpMonth.SelectedValue.Trim() + "月";
                    }
                }
            }
            

            ChartTip = strTip;
        }
        //统计
        protected void GetCoperationTarget()
        {
            //初始时间
            CreateTimeQuery();
            //初始提示
            CreateTipQuery();            
            //合同年份
            string year = this.drp_year.SelectedItem.Value.Trim();
            //开始年份
            string start = "";
            //结束年份
            string end = "";
            if (this.hid_time.Value == "1")
            {
                start = this.txt_year1.Value;
                end = this.txt_year2.Value;               
                year = Convert.ToDateTime(start).Year.ToString();
            }
            else
            {
                //全年全度全月
                if (this.drp_year.SelectedIndex == 0)
                {
                    //所有年
                    year = null;
                    start = null;
                    end = null;
                }
                else
                {
                    //年
                    string stryear = this.drp_year.SelectedValue;
                    //季度
                    string strjidu = this.drpJidu.SelectedValue;
                    //月
                    string stryue = this.drpMonth.SelectedValue;
                    //年
                    if (strjidu == "0" && stryue == "0")
                    {
                        start = stryear + "-01-01 00:00:00";
                        end = stryear + "-12-31 23:59:59 ";
                    }
                    else if (strjidu != "0" && stryue == "0") //年季度
                    {
                        start = stryear;
                        end = stryear;
                        switch (strjidu)
                        {
                            case "1":
                                start += "-01-01 00:00:00";
                                end += "-03-31 23:59:59";
                                break;
                            case "2":
                                start += "-04-01 00:00:00";
                                end += "-06-30 23:59:59";
                                break;
                            case "3":
                                start += "-07-01 00:00:00";
                                end += "-09-30 23:59:59";
                                break;
                            case "4":
                                start += "-10-01 00:00:00";
                                end += "-12-31 23:59:59";
                                break;
                        }
                    }
                    else if (strjidu == "0" && stryue != "0")//年月份
                    {
                        //当月有几天
                        int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                        start = stryear + "-" + stryue + "-01 00:00:00";
                        end = stryear + "-" + stryue + "-" + days + " 23:59:59";
                    }

                }
            }


            //部门ID
            int? unitid = null;
            //部门ID
            string unitList = "";
            List<ASTreeViewNode> nodes = this.drpunit.GetCheckedNodes(false);
            //得到选中部门
            if (nodes.Count > 0)
            {
                unitList = "0";
                //选择查看的部门
                foreach (ASTreeViewNode node in nodes)
                {
                    unitList += "," + node.NodeValue;
                }

                DataView dv = new TG.BLL.cm_Coperation().CountCoperationTarget(year,start,end, ChargeStartTime,
                    ChargeEndTime, unitid).Tables[0].DefaultView;
                dv.RowFilter = " ID IN (" + unitList + ")";
                this.GridView1.DataSource = dv;
                this.GridView1.DataBind();
            }
            else
            {
                //全部
                if (IsCheckAllPower == "1")
                {
                    unitid = null;
                    DataView dv = new TG.BLL.cm_Coperation().CountCoperationTarget(year, start, end, ChargeStartTime,
                    ChargeEndTime, unitid).Tables[0].DefaultView;
                    dv.RowFilter = " ID NOT IN (" + base.NotShowUnitList + ")";
                    this.GridView1.DataSource = dv;
                }
                else
                {
                    //只能查看本部门
                    unitid = UserUnitNo;
                    this.GridView1.DataSource = new TG.BLL.cm_Coperation().CountCoperationTarget(year,start, end, ChargeStartTime,
                    ChargeEndTime, unitid);
                }
                //绑定
                this.GridView1.DataBind();
            }

            //GridView 标准格式
            this.GridView1.UseAccessibleHeader = true;
            this.GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        //生产部门改变重新绑定数据
        protected void btn_search_Click(object sender, EventArgs e)
        {
            BindDataSetPageIndex();
        }
        /// <summary>
        /// //重新查询后页面索引跳转到第一页 qpl 20131221
        /// </summary>
        protected void BindDataSetPageIndex()
        {
            //绑定数据
            GetCoperationTarget();
        }
        /// <summary>
        /// 导出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_export_Click(object sender, EventArgs e)
        {
            DataTable dt = getData();
            //在这里进行Excel分类的切割
            //设计
            DataTable dtsj = dt.Clone();
            //多营
            DataTable dtdj = dt.Clone();
            //增加监理和勘察分开
            DataTable dtkc = dt.Clone();
            DataTable dtjl = dt.Clone();
            //管理部门
            DataTable dtel = dt.Clone();
            //238是设计所的
            //254是多经部门
            //其他的就是管理部门可以
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (unitparenrID(dt.Rows[i]["ID"].ToString()) == "238")
                {
                    if (dt.Rows[i]["ID"].ToString() == "250")//勘察
                    {
                        dtkc.ImportRow(dt.Rows[i]);
                    }
                    else if (dt.Rows[i]["ID"].ToString() == "253")//监理
                    {
                        dtjl.ImportRow(dt.Rows[i]);
                    }
                    else//其他
                    {
                        dtsj.ImportRow(dt.Rows[i]);
                    }
                }
                else if (unitparenrID(dt.Rows[i]["ID"].ToString()) == "254")
                {
                    dtdj.ImportRow(dt.Rows[i]);
                }
                else
                {
                    if (unitparenrID(dt.Rows[i]["ID"].ToString()) != "0" || dt.Rows[i]["ID"].ToString() == "230")
                    {
                        dtel.ImportRow(dt.Rows[i]);
                    }

                }
            } 
            string modelPath = " ~/TemplateXls/CoperAndCharge.xls";

            HSSFWorkbook wb = null;

            //如果没有模板路径，则创建一个空的workbook和一个空的sheet

            if (string.IsNullOrEmpty(modelPath))
            {
                wb = new HSSFWorkbook();
                wb.CreateSheet();
                wb.GetSheetAt(0).CreateRow(0);
            }
            else
            {
                using (var fileStream = System.IO.File.Open(Server.MapPath(modelPath), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    wb = new HSSFWorkbook(fileStream);

                    fileStream.Close();
                }
            }


            //内容样式
            ICellStyle style2 = wb.CreateCellStyle();
            style2.Alignment = HorizontalAlignment.CENTER;//居中对齐
            style2.VerticalAlignment = VerticalAlignment.CENTER;
            style2.WrapText = true;
            style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style2.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            IFont font2 = wb.CreateFont();
            //font2.FontHeightInPoints = 10;//字号
            font2.FontName = "宋体";//字体
            style2.SetFont(font2);

            //获得工作表对象，若不指定工作表或指定的工作表不存在则添加在第一个工作表
            var ws = wb.GetSheet("Sheet1");
            ISheet isheet = wb.GetSheet("Sheet1");
            if (ws == null)
                ws = wb.GetSheetAt(0);
            ws.GetRow(0).GetCell(0).SetCellValue(ChartTip + "合同综合目标值统计与" + ChargeStartTime + "到" + ChargeEndTime + "合同收款统计");
            int row = 2;
            if (dtel.Rows.Count > 0)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行
                var cell = dataRow.CreateCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue("管理部门部分");
                row = row + 1;
                isheet.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
            }
            //CellRangeAddress四个参数为：起始行，结束行，起始列，结束列

            for (int i = 0; i < dtel.Rows.Count; i++)
            {

                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dtel.Rows[i]["Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dtel.Rows[i]["cpryear"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["cprTarget"].ToString()));

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["cprAllCount"].ToString()));

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dtel.Rows[i]["Cprprt"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dtel.Rows[i]["Starttime"]).ToString("yyyy-MM-dd"));

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dtel.Rows[i]["EndTime"]).ToString("yyyy-MM-dd"));
                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["AllotTarget"].ToString()));
                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtel.Rows[i]["AllotCount"].ToString()));
                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(dtel.Rows[i]["AllotPrt"].ToString());
                row = row + 1;
            }
            if (dtel.Rows.Count > 0)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行
                var cell = dataRow.CreateCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue("管理合计");
                row = row + 1;
                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(C3:C" + (row - 1) + ")");
                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(D3:D" + (row - 1) + ")");
                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellFormula("(D" + row + "/C" + row + ")*100");
                //cell.SetCellValue()
                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(H3:H" + (row - 1) + ")");
                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(I3:I" + (row - 1) + ")");
                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellFormula("(I" + row + "/H" + row + ")*100");
            }
            //===设计
            if (dtsj.Rows.Count > 0)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行
                var cell = dataRow.CreateCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue("设计、监理、勘察部分");
                row = row + 1;
                isheet.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
            }

            for (int i = 0; i < dtsj.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);

                cell.CellStyle = style2;
                cell.SetCellValue(dtsj.Rows[i]["Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dtsj.Rows[i]["cpryear"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["cprTarget"].ToString()));

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["cprAllCount"].ToString()));

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dtsj.Rows[i]["Cprprt"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dtsj.Rows[i]["Starttime"]).ToString("yyyy-MM-dd"));

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dtsj.Rows[i]["EndTime"]).ToString("yyyy-MM-dd"));
                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["AllotTarget"].ToString()));
                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtsj.Rows[i]["AllotCount"].ToString()));
                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(dtsj.Rows[i]["AllotPrt"].ToString());
                row = row + 1;
            }
            if (dtsj.Rows.Count > 0)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行
                var cell = dataRow.CreateCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue("设计所合计");
                row = row + 1;
                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(C" + (row - dtsj.Rows.Count) + ":C" + (row - 1) + ")");

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(D" + (row - dtsj.Rows.Count) + ":D" + (row - 1) + ")");

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellFormula("(D" + row + "/C" + row + ")*100");

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(H" + (row - dtsj.Rows.Count) + ":H" + (row - 1) + ")");

                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(I" + (row - dtsj.Rows.Count) + ":I" + (row - 1) + ")");

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellFormula("(I" + row + "/H" + row + ")*100");
                isheet.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
            }
            //勘察&设计所合计一下
            if (dtkc.Rows.Count > 0)
            {
                for (int i = 0; i < dtkc.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtkc.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtkc.Rows[i]["cpryear"].ToString());

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["cprTarget"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["cprAllCount"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtkc.Rows[i]["Cprprt"].ToString());

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDateTime(dtkc.Rows[i]["Starttime"]).ToString("yyyy-MM-dd"));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDateTime(dtkc.Rows[i]["EndTime"]).ToString("yyyy-MM-dd"));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["AllotTarget"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtkc.Rows[i]["AllotCount"].ToString()));
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtkc.Rows[i]["AllotPrt"].ToString());
                    row = row + 1;
                }
                var dataRowk = ws.GetRow(row);//读行
                if (dataRowk == null)
                    dataRowk = ws.CreateRow(row);//生成行
                var cellk = dataRowk.CreateCell(0);
                if (cellk == null)
                    cellk = dataRowk.CreateCell(0);
                cellk.CellStyle = style2;
                cellk.SetCellValue("勘察、设计合计");
                row = row + 1;
                cellk = dataRowk.CreateCell(2);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(3);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(4);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("(D" + row + "/C" + row + ")*100");

                cellk = dataRowk.CreateCell(7);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(8);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(9);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("(I" + row + "/H" + row + ")*100");
                isheet.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 1));
            }
            //最后是监理
            if (dtjl.Rows.Count > 0)
            {
                for (int i = 0; i < dtjl.Rows.Count; i++)
                {
                    var dataRow = ws.GetRow(row);//读行
                    if (dataRow == null)
                        dataRow = ws.CreateRow(row);//生成行

                    var cell = dataRow.GetCell(0);
                    if (cell == null)
                        cell = dataRow.CreateCell(0);

                    cell.CellStyle = style2;
                    cell.SetCellValue(dtjl.Rows[i]["Name"].ToString());

                    cell = dataRow.CreateCell(1);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtjl.Rows[i]["cpryear"].ToString());

                    cell = dataRow.CreateCell(2);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["cprTarget"].ToString()));

                    cell = dataRow.CreateCell(3);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["cprAllCount"].ToString()));

                    cell = dataRow.CreateCell(4);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtjl.Rows[i]["Cprprt"].ToString());

                    cell = dataRow.CreateCell(5);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDateTime(dtjl.Rows[i]["Starttime"]).ToString("yyyy-MM-dd"));

                    cell = dataRow.CreateCell(6);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDateTime(dtjl.Rows[i]["EndTime"]).ToString("yyyy-MM-dd"));
                    cell = dataRow.CreateCell(7);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["AllotTarget"].ToString()));
                    cell = dataRow.CreateCell(8);
                    cell.CellStyle = style2;
                    cell.SetCellValue(Convert.ToDouble(dtjl.Rows[i]["AllotCount"].ToString()));
                    cell = dataRow.CreateCell(9);
                    cell.CellStyle = style2;
                    cell.SetCellValue(dtjl.Rows[i]["AllotPrt"].ToString());
                    row = row + 1;
                }
                var dataRowk = ws.GetRow(row);//读行
                if (dataRowk == null)
                    dataRowk = ws.CreateRow(row);//生成行
                var cellk = dataRowk.CreateCell(0);
                if (cellk == null)
                    cellk = dataRowk.CreateCell(0);
                cellk.CellStyle = style2;
                cellk.SetCellValue("主营合计");
                row = row + 1;
                cellk = dataRowk.CreateCell(2);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(C" + (row - 2) + ":C" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(3);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(D" + (row - 2) + ":D" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(4);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("(D" + row + "/C" + row + ")*100");

                cellk = dataRowk.CreateCell(7);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(H" + (row - 2) + ":H" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(8);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("sum(I" + (row - 2) + ":I" + (row - 1) + ")");

                cellk = dataRowk.CreateCell(9);
                cellk.CellStyle = style2;
                cellk.SetCellFormula("(I" + row + "/H" + row + ")*100");

            }
            //多经部门
            if (dtdj.Rows.Count > 0)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行
                var cell = dataRow.CreateCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue("多种经营部分");
                row = row + 1;
                isheet.AddMergedRegion(new CellRangeAddress(row - 1, row - 1, 0, 9));
            }

            for (int i = 0; i < dtdj.Rows.Count; i++)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行

                var cell = dataRow.GetCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue(dtdj.Rows[i]["Name"].ToString());

                cell = dataRow.CreateCell(1);
                cell.CellStyle = style2;
                cell.SetCellValue(dtdj.Rows[i]["cpryear"].ToString());

                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["cprTarget"].ToString()));

                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["cprAllCount"].ToString()));

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellValue(dtdj.Rows[i]["Cprprt"].ToString());

                cell = dataRow.CreateCell(5);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dtdj.Rows[i]["Starttime"]).ToString("yyyy-MM-dd"));

                cell = dataRow.CreateCell(6);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDateTime(dtdj.Rows[i]["EndTime"]).ToString("yyyy-MM-dd"));
                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotTarget"].ToString()));
                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellValue(Convert.ToDouble(dtdj.Rows[i]["AllotCount"].ToString()));
                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellValue(dtdj.Rows[i]["AllotPrt"].ToString());
                row = row + 1;
            }
            if (dtdj.Rows.Count > 0)
            {
                var dataRow = ws.GetRow(row);//读行
                if (dataRow == null)
                    dataRow = ws.CreateRow(row);//生成行
                var cell = dataRow.CreateCell(0);
                if (cell == null)
                    cell = dataRow.CreateCell(0);
                cell.CellStyle = style2;
                cell.SetCellValue("多经营所合计");
                row = row + 1;
                cell = dataRow.CreateCell(2);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(C" + (row - dtdj.Rows.Count) + ":C" + (row - 1) + ")");
                cell = dataRow.CreateCell(3);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(D" + (row - dtdj.Rows.Count) + ":D" + (row - 1) + ")");

                cell = dataRow.CreateCell(4);
                cell.CellStyle = style2;
                cell.SetCellFormula("(D" + row + "/C" + row + ")*100");

                cell = dataRow.CreateCell(7);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(H" + (row - dtdj.Rows.Count) + ":H" + (row - 1) + ")");
                cell = dataRow.CreateCell(8);
                cell.CellStyle = style2;
                cell.SetCellFormula("sum(I" + (row - dtdj.Rows.Count) + ":I" + (row - 1) + ")");

                cell = dataRow.CreateCell(9);
                cell.CellStyle = style2;
                cell.SetCellFormula("(I" + row + "/H" + row + ")*100");
            }
            if (dtdj.Rows.Count != 0 && dtsj.Rows.Count != 0 && dtel.Rows.Count != 0 && dtkc.Rows.Count != 0 && dtjl.Rows.Count != 0)
            {

                var dataRowa = ws.GetRow(row);//读行
                if (dataRowa == null)
                    dataRowa = ws.CreateRow(row);//生成行
                var cella = dataRowa.CreateCell(0);
                if (cella == null)
                    cella = dataRowa.CreateCell(0);
                cella.CellStyle = style2;
                cella.SetCellValue("全院总合计");

                cella = dataRowa.CreateCell(2);
                cella.CellStyle = style2;

                cella.SetCellFormula("sum(C" + (row - dtdj.Rows.Count - 2) + ",C" + (row) + ",C" + (row - dtdj.Rows.Count - dtkc.Rows.Count - dtjl.Rows.Count - 2 - 2 - dtsj.Rows.Count - 2) + ")");
                cella = dataRowa.CreateCell(3);
                cella.CellStyle = style2;
                cella.SetCellFormula("sum(D" + (row - dtdj.Rows.Count - 2) + ",D" + (row) + ",D" + (row - dtdj.Rows.Count - dtkc.Rows.Count - dtjl.Rows.Count - 2 - 2 - dtsj.Rows.Count - 2) + ")");

                cella = dataRowa.CreateCell(4);
                cella.CellStyle = style2;
                cella.SetCellFormula("(D" + (row + 1) + "/C" + (row + 1) + ")*100");

                cella = dataRowa.CreateCell(7);
                cella.CellStyle = style2;
                cella.SetCellFormula("sum(H" + (row - dtdj.Rows.Count - 2) + ",H" + (row) + ",H" + (row - dtdj.Rows.Count - dtkc.Rows.Count - dtjl.Rows.Count - 2 - 2 - dtsj.Rows.Count - 2) + ")");
                cella = dataRowa.CreateCell(8);
                cella.CellStyle = style2;
                cella.SetCellFormula("sum(I" + (row - dtdj.Rows.Count - 2) + ",I" + (row) + ",I" + (row - dtdj.Rows.Count - dtkc.Rows.Count - dtjl.Rows.Count - 2 - 2 - dtsj.Rows.Count - 2) + ")");

                cella = dataRowa.CreateCell(9);
                cella.CellStyle = style2;
                cella.SetCellFormula("(I" + (row + 1) + "/H" + (row + 1) + ")*100");
            }
            using (MemoryStream memoryStream = new MemoryStream())
            {

                wb.Write(memoryStream);

                string name = System.Web.HttpContext.Current.Server.UrlEncode(ChartTip + "合同综合目标值统计与" + ChargeStartTime + "到" + ChargeEndTime + "合同收款统计.xls");
                Response.AddHeader("Content-Disposition", "attachment; filename=" + name + "");
                Response.ContentType = "application/vnd.ms-excel;charset=UTF-8";
                Response.BinaryWrite(memoryStream.ToArray());
                Response.ContentEncoding = Encoding.UTF8;
                wb = null;
                Response.End();
            }
        }

        /// <summary>
        /// 得到数据信息
        /// </summary>
        /// <returns></returns>
        private DataTable getData()
        {
            //初始提示
            CreateTipQuery();
            //初始时间
            CreateTimeQuery();
            //合同年份
            string year = this.drp_year.SelectedItem.Value.Trim();
            //开始年份
            string start = "";
            //结束年份
            string end = "";
            if (this.hid_time.Value == "1")
            {
                start = this.txt_year1.Value;
                end = this.txt_year2.Value;               
                year = Convert.ToDateTime(start).Year.ToString();
            }
            else
            {
                //全年全度全月
                if (this.drp_year.SelectedIndex == 0)
                {
                    //所有年
                    year = null;
                    start = null;
                    end = null;
                }
                else
                {
                    //年
                    string stryear = this.drp_year.SelectedValue;
                    //季度
                    string strjidu = this.drpJidu.SelectedValue;
                    //月
                    string stryue = this.drpMonth.SelectedValue;
                    //年
                    if (strjidu == "0" && stryue == "0")
                    {
                        start = stryear + "-01-01 00:00:00";
                        end = stryear + "-12-31 23:59:59 ";
                    }
                    else if (strjidu != "0" && stryue == "0") //年季度
                    {
                        start = stryear;
                        end = stryear;
                        switch (strjidu)
                        {
                            case "1":
                                start += "-01-01 00:00:00";
                                end += "-03-31 23:59:59";
                                break;
                            case "2":
                                start += "-04-01 00:00:00";
                                end += "-06-30 23:59:59";
                                break;
                            case "3":
                                start += "-07-01 00:00:00";
                                end += "-09-30 23:59:59";
                                break;
                            case "4":
                                start += "-10-01 00:00:00";
                                end += "-12-31 23:59:59";
                                break;
                        }
                    }
                    else if (strjidu == "0" && stryue != "0")//年月份
                    {
                        //当月有几天
                        int days = DateTime.DaysInMonth(int.Parse(stryear), int.Parse(stryue));
                        start = stryear + "-" + stryue + "-01 00:00:00";
                        end = stryear + "-" + stryue + "-" + days + " 23:59:59";
                    }

                }
            }

            //声明返回数据 
            DataTable dt;
            //部门ID
            int? unitid = null;
            //部门ID
            string unitList = "";
            List<ASTreeViewNode> nodes = this.drpunit.GetCheckedNodes(false);
            //没有选中
            if (nodes.Count > 0)
            {
                unitList = "0";
                //选择查看的部门
                foreach (ASTreeViewNode node in nodes)
                {
                    unitList += "," + node.NodeValue;
                }

                DataView dv = new TG.BLL.cm_Coperation().CountCoperationTarget(year,start,end, ChargeStartTime,
                    ChargeEndTime, unitid).Tables[0].DefaultView;
                dv.RowFilter = " ID IN (" + unitList + ")";
                dt = dv.ToTable();
            }
            else
            {
                //全部
                if (IsCheckAllPower == "1")
                {
                    unitid = null;
                }
                else
                {
                    //只能查看本部门
                    unitid = UserUnitNo;
                }

                dt = new TG.BLL.cm_Coperation().CountCoperationTarget(year, start, end, ChargeStartTime, ChargeEndTime, unitid).Tables[0];
            }
            return dt;
        }
        public string unitparenrID(string unitid)
        {
            return bll_unit.GetModel(int.Parse(unitid)).unit_ParentID.ToString();
        }
    }
}