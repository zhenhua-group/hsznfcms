﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Geekees.Common.Controls;
using System.Xml;
using TG.Model;
using TG.BLL;

namespace TG.Web.Coperation
{
    public partial class cpr_AddCoperationBymaster : PageBase
    {
        //建筑结构
        public string asTreeviewStructObjID
        {
            get
            {
                return this.asTreeviewStruct.GetClientTreeObjectId();
            }
        }
        //建筑分类
        public string asTreeviewStructTypeObjID
        {
            get
            {
                return this.asTreeviewStructType.GetClientTreeObjectId();
            }
        }
        ////子项结构
        //public string asTreeviewStructSubObjID
        //{
        //    get
        //    {
        //        return this.asTreeviewStruct_sub.GetClientTreeObjectId();
        //    }
        //}
        //子项分类
        //public string asTreeviewStructTypeSubObjID
        //{
        //    get
        //    {
        //        return this.asTreeviewStructType_sub.GetClientTreeObjectId();
        //    }
        //}
        //设计等级
        //public string asTreeViewBuildTypeObjID
        //{
        //    get
        //    {
        //        return this.asTreeViewBuildType.GetClientTreeObjectId();
        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //绑定合同类别
                BindCorpType();
                //工程地点
                //BindCorpAddr();
                //行业性质
                BindCorpHyxz();
                //工程来源
                BindCorpSrc();
                //合同阶段
                BindCorpProc();
                //设置样式
                SetDropDownTreeThem();
                //绑定建筑结构样式
                BindStructType();
                //绑定建筑分类
                BindBuildStuctType();
                //绑定设计等级
                BindBuildType();
                //结构特征
                //BindSpec();
                SetUserSysNoAndRole();
                BindPreviewPower();
                //收费类型
                BindCorChange();
            }
           
        }

        protected int Save()
        {
            //添加合同
            int i_cprid = AddCoperation();
            if (i_cprid > 0)
            {
                //添加计划收费信息
                AddChargeItem(i_cprid.ToString());
                //添加合同子项
                AddSubCoperation(i_cprid.ToString());
                //添加附件
                AddCoperationAttach(i_cprid.ToString());               
            }
            return i_cprid;
        }
        //添加合同信息
        public int AddCoperation()
        {
            TG.BLL.cm_Coperation bll_cpr = new TG.BLL.cm_Coperation();
            TG.Model.cm_Coperation model_cpr = new TG.Model.cm_Coperation();
            //赋值
            model_cpr.cst_Id = int.Parse(this.hid_cstid.Value);
            model_cpr.cpr_No = this.txtcpr_No.Value;
            if (this.hid_cprno.Value != "")
            {
                model_cpr.cpr_No = this.hid_cprno.Value;
            }

            if (this.ddcpr_Type.SelectedIndex != 0)
            {
                model_cpr.cpr_Type = this.ddcpr_Type.SelectedItem.Text;
            }            
          
            model_cpr.cpr_Type2 = this.txt_cprType.Value;
            model_cpr.cpr_Name = this.txt_cprName.Value;
            model_cpr.BuildUnit = this.txt_cprBuildUnit.Value;
            //甲方类型
            model_cpr.BuildType = this.drp_ChgJiaType.SelectedItem.Text.Trim();
            //添加
            int affectRow = 0;
          

            if (this.txtcpr_Account.Value.Trim() == "")
            {
                model_cpr.cpr_Acount = 0;
            }
            else
            {
                model_cpr.cpr_Acount = Convert.ToDecimal(this.txtcpr_Account.Value);
            }
            if (this.txtcpr_Account0.Value.Trim() == "")
            {
                model_cpr.cpr_ShijiAcount = 0;
            }
            else
            {
                model_cpr.cpr_ShijiAcount = Convert.ToDecimal(this.txtcpr_Account0.Value);
            }
            //if (this.txtInvestAccount.Value.Trim() == "")
            //{
            //    model_cpr.cpr_Touzi = 0;
            //}
            //else
            //{
            //    model_cpr.cpr_Touzi = Convert.ToDecimal(this.txtInvestAccount.Value);
            //}
            //if (txtInvestAccount0.Value.Trim() == "")
            //{
            //    model_cpr.cpr_ShijiTouzi = 0;
            //}
            //else
            //{
            //    model_cpr.cpr_ShijiTouzi = Convert.ToDecimal(txtInvestAccount0.Value);
            //}
            //string str_process = "";
            //foreach (ListItem chk in this.chk_cprjd.Items)
            //{
            //    if (chk.Selected)
            //    {
            //        str_process += chk.Value + ",";
            //    }
            //}
            //str_process = str_process.IndexOf(",") > -1 ? str_process.Remove(str_process.Length - 1) : "";
            //model_cpr.cpr_Process = str_process;
            //model_cpr.cpr_SignDate = Convert.ToDateTime(this.txtSingnDate.Value);
           // model_cpr.cpr_DoneDate = Convert.ToDateTime(this.txtCompleteDate.Value);
            model_cpr.cpr_Mark = this.txtcpr_Remark.Value;
            model_cpr.BuildArea = this.txt_buildArea.Value;
            model_cpr.ChgPeople = this.txt_proFuze.Value;
            model_cpr.ChgPhone = this.txt_fzphone.Value;
            model_cpr.ChgJia = this.txtFParty.Value;
            model_cpr.ChgJiaPhone = this.txt_jiafphone.Value;
            //工程地点
            if (this.txt_ProjectPosition.Value.Trim() != "")
            {
                model_cpr.BuildPosition = this.txt_ProjectPosition.Value;
            }
            else
            {
                model_cpr.BuildPosition = "";
            }
            //行业性质
            //if (this.ddProfessionType.SelectedIndex != 0)
            //{
            //    model_cpr.Industry = this.ddProfessionType.SelectedItem.Text;
            //}
            //else
            //{
            //    model_cpr.Industry = this.ddProfessionType.Items[0].Value;
            //}
            ////工程来源
            //if (this.ddSourceWay.SelectedIndex != 0)
            //{
            //    model_cpr.BuildSrc = this.ddSourceWay.SelectedItem.Text;
            //}
            //else
            //{
            //    model_cpr.BuildSrc = this.ddSourceWay.Items[0].Value; ;
            //}
          
            //子公司
            if (this.txt_cjbm.Items.Count>0)
            {
                model_cpr.cpr_Unit = this.txt_cjbm.SelectedItem.Text;               
            }
          
            model_cpr.RegTime = DateTime.Now;
            model_cpr.UpdateBy = UserSysNo.ToString();
            model_cpr.LastUpdate = DateTime.Now;
            //结构形式
            //update 20130530 qpl
            model_cpr.StructType = GetDropDownTreeCheckedValue(this.asTreeviewStruct.RootNode.ChildNodes);
            //建筑分类
            //update 20130530 qpl
            model_cpr.BuildStructType = GetDropDownTreeCheckedValue(this.asTreeviewStructType.RootNode.ChildNodes);
            //建筑层数
        //    model_cpr.Floor = this.txt_upfloor.Value + "|" + this.txt_downfloor.Value;
            //多吨楼备注
           // model_cpr.MultiBuild = this.txt_MultiBuild.Value;

            //合同允许状态
            model_cpr.TableMaker = this.cpr_status.Value;
            //专业特征
            model_cpr.JieGou = this.txt_sumprint.Value;
            model_cpr.Geips = this.txt_print.Value;
            model_cpr.Nuant = this.txt_sumtravel.Value;
            model_cpr.Dianq = this.txt_travel.Value;
            //录入人
            model_cpr.InsertUserID = UserSysNo;
            //项目经理人ID  qpl  20131225
            model_cpr.PMUserID = int.Parse(this.HiddenPMUserID.Value);
            model_cpr.InsertDate = DateTime.Now;
            //签订日期 zxq 20151027
            model_cpr.cpr_SignDate = Convert.ToDateTime(txtSingnDate.Value);
            //保存合同信息
            try
            {
                affectRow = bll_cpr.Add(model_cpr);
            }
            catch (System.Exception ex)
            { }

            return affectRow;
        }
        //更新收费信息
        protected void AddChargeItem(string cprid)
        {
            //更新计划收费信息  qpl  20140114
            string strSql = " UPDATE [cm_CoperationCharge] SET [cpr_Id]=" + cprid + " WHERE [cpr_Id]=" + this.hid_cprid.Value + " AND acceptuser='" + UserShortName + "'";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            bll_db.ExcuteBySql(strSql);

        }
        //添加子项
        protected void AddSubCoperation(string cprid)
        {
            //更新计划收费信息  qpl  20140114
            string strSql = " UPDATE [cm_SubCoperation] SET [cpr_Id] = " + cprid + ",[cpr_No]=" + cprid + "  WHERE [cpr_No] ='" + this.hid_cprid.Value + "' AND UpdateBy='" + UserShortName + "'";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            bll_db.ExcuteBySql(strSql);


        }
        //添加附件
        protected void AddCoperationAttach(string cprid)
        {
            //更新计划收费信息  qpl  20140114
            string strSql = " UPDATE [cm_AttachInfo] SET [Cpr_Id] =" + cprid + " WHERE [Temp_No] = '" + this.hid_cprid.Value + "' AND UploadUser='" + UserSysNo + "' AND OwnType='cpr'";
            TG.BLL.CommDBHelper bll_db = new TG.BLL.CommDBHelper();
            bll_db.ExcuteBySql(strSql);



        }
        /// <summary>
        /// 获取客户ID
        /// </summary>
        /// <returns></returns>
        public string getCst_Id()
        {
            DateTime dt = DateTime.Now;
            string tempid = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString();
            return tempid;
        }
        //返回一个文件上传的随机ID
        public string GetCoperationID()
        {
            DateTime dt = DateTime.Now;
            string tempid = dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() + dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + dt.Millisecond.ToString();
            return tempid;
        }
        /// <summary>
        /// 获取登录ID(登录界面开始进行重新设置.....)
        /// </summary>
        /// <returns></returns>
        public string GetUserFlag()
        {
            return UserSysNo.ToString();
        }
        /// <summary>
        /// 返回文件夹ID (若是根据登录ID,此项可不要？待商讨中.......)
        /// </summary>
        /// <returns></returns>
        public string GetParentID()
        {
            return UserShortName;
        }
        //设置选择合同额方法
        public void SetUserSysNoAndRole()
        {
            //用户ID
            this.ChooseCustomer1.UserSysNo = int.Parse(GetCurMemID());
            //权限
            this.ChooseCustomer1.PreviewPower = GetPreviewPower();
        }
        //获取页面权限  by long 20130510
        public int GetPreviewPower()
        {
            int UserSysNo = int.Parse(GetCurMemID());
            string PageName = "cpr_CorperationList.aspx";
            List<RolePowerParameterEntity> rolePowerParameterList = new RolePowerBP().GetRolePowerViewEntityList(UserSysNo, PageName);

            //浏览权限
            int Power = 0;
            if ((from role in rolePowerParameterList where role.PreviewPattern == "2" select role).Count() > 0)
            {
                //部门
                Power = 2;
            }
            if ((from role in rolePowerParameterList where role.PreviewPattern == "1" select role).Count() > 0)
            {
                //全部
                Power = 1;
            }
            return Power;
        }

        //返回当前用户
        public string GetCurMemID()
        {
            return UserSysNo.ToString();
        }
        //合同类别绑定
        protected void BindCorpType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_fl'";
            this.ddcpr_Type.DataSource = bll_dic.GetList(str_where);
            this.ddcpr_Type.DataTextField = "dic_Name";
            this.ddcpr_Type.DataValueField = "ID";
            this.ddcpr_Type.DataBind();

        }
        //合同建筑类别
        //protected void BindBuildType()
        //{
        //    TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
        //    string str_where = " dic_Type='cpr_buildtype'";
        //    this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
        //    this.drp_buildtype.DataTextField = "dic_Name";
        //    this.drp_buildtype.DataValueField = "ID";
        //    this.drp_buildtype.DataBind();
        //}
        //行业性质
        protected void BindCorpHyxz()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_hyxz'";
            //this.ddProfessionType.DataSource = bll_dic.GetList(str_where);
            //this.ddProfessionType.DataTextField = "dic_Name";
            //this.ddProfessionType.DataValueField = "ID";
            //this.ddProfessionType.DataBind();
        }
        //收费类型
        protected void BindCorChange()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_charge'";
            //this.chargeTypeSelect.DataSource = bll_dic.GetList(str_where);
            //this.chargeTypeSelect.DataTextField = "dic_Name";
            //this.chargeTypeSelect.DataValueField = "ID";
            //this.chargeTypeSelect.DataBind();
        }
        //建筑分类
        protected void BindBuildStuctType()
        {
            BindProInfoConfig("BuildType", this.asTreeviewStructType.RootNode);
            this.asTreeviewStructType.CollapseAll();
            //子项建筑分类 qpl  20131225
            //BindProInfoConfig("BuildType", this.asTreeviewStructType_sub.RootNode);
            //this.asTreeviewStructType_sub.CollapseAll();
        }
        //结构形式
        protected void BindStructType()
        {
            BindProInfoConfig("StructType", this.asTreeviewStruct.RootNode);
            this.asTreeviewStruct.CollapseAll();
            //子项结构样式  qpl  20131225
            //BindProInfoConfig("StructType", this.asTreeviewStruct_sub.RootNode);
            //this.asTreeviewStruct_sub.CollapseAll();
        }
        //设计等级  
        protected void BindBuildType()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_buildtype'";
         //  this.drp_buildtype.DataSource = bll_dic.GetList(str_where);
         //   this.drp_buildtype.DataTextField = "dic_Name";
         //   this.drp_buildtype.DataValueField = "ID";
         //   this.drp_buildtype.DataBind();

            //甲方类型
            this.drp_ChgJiaType.DataSource = bll_dic.GetList(" dic_Type='cpr_chajia'");
            this.drp_ChgJiaType.DataTextField = "dic_Name";
            this.drp_ChgJiaType.DataValueField = "ID";
            this.drp_ChgJiaType.DataBind();

            //子公司
            this.txt_cjbm.DataSource = bll_dic.GetList(" dic_Type='cpr_subcompany'");
            this.txt_cjbm.DataTextField = "dic_Name";
            this.txt_cjbm.DataValueField = "ID";
            this.txt_cjbm.DataBind();

        }
        //通用ProInitInfo表绑定方法
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root);
            }
        }
        //下拉复选框的样式
        protected void SetDropDownTreeThem()
        {
            ASTreeViewTheme macOS = new ASTreeViewTheme();
            macOS.BasePath = "../js/astreeview/astreeview/themes/macOS/";
            macOS.CssFile = "macOS.css";
            this.asTreeviewStruct.Theme = macOS;
            this.asTreeviewStructType.Theme = macOS;
            //qpl 20131225
            // this.asTreeViewBuildType.Theme = macOS;
            //qpl 20131225
            //this.asTreeviewStructType_sub.Theme = macOS;
            //this.asTreeviewStruct_sub.Theme = macOS;
        }
        /// <summary>
        /// 绑定树列表  qpl 20140108 
        /// </summary>
        /// <param name="infokey"></param>
        /// <param name="rootnode"></param>
        /// <param name="flagrootbox">根节点是否显示CheckBox</param>
        protected void BindProInfoConfig(string infokey, ASTreeViewNode rootnode, bool showsubbox)
        {
            TG.BLL.tg_ProInitInfo bll = new TG.BLL.tg_ProInitInfo();
            string strWhere = " Info_Name='" + infokey + "'";
            List<TG.Model.tg_ProInitInfo> models = bll.GetModelList(strWhere);
            if (models.Count > 0)
            {
                string xmlText = models[0].Info_Val;
                //读取xml文本
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlText);
                XmlNode xmlroot = xmldoc.ChildNodes[0];
                XmlNodeList nodes = xmlroot.ChildNodes;
                //声明根节点
                ASTreeViewNode root = rootnode;
                //初始化树控件
                InitBuildStructTree(xmlroot, ref root, showsubbox);
            }
        }
        //迭代xml树
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);
                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        /// <summary>
        /// 绑定树列表  qpl 20140108 
        /// </summary>
        /// <param name="xmlroot"></param>
        /// <param name="root"></param>
        /// <param name="flagrootbox">根节点是否显示CheckBox</param>
        protected void InitBuildStructTree(XmlNode xmlroot, ref ASTreeViewNode root, bool showsubbox)
        {
            XmlNodeList nodes = xmlroot.ChildNodes;
            foreach (XmlNode node in nodes)
            {
                string name = node.Attributes["name"].InnerText;
                ASTreeViewNode linknode = new ASTreeViewNode(name, name);
                if (root.ParentNode == null)
                {
                    linknode.EnableCheckbox = false;
                }
                //标示子节点是否显示CheckBox qpl 20140108
                switch (name)
                {
                    case "特级":
                    case "一级":
                    case "二级":
                    case "三级":
                        linknode.EnableCheckbox = !showsubbox;
                        break;
                    default:
                        linknode.EnableCheckbox = showsubbox;
                        break;
                }

                if (node.ChildNodes.Count > 0)
                {
                    XmlNodeList subnodes = node.ChildNodes;
                    foreach (XmlNode subnode in subnodes)
                    {
                        string subname = subnode.Attributes["name"].InnerText;
                        //子节点
                        ASTreeViewNode sublinknode = new ASTreeViewNode(subname, subname);

                        //子节点的checkbox 不可用  qpl 20140108
                        sublinknode.EnableCheckbox = showsubbox;

                        linknode.AppendChild(sublinknode);
                        //迭代
                        if (subnode.ChildNodes.Count > 0)
                        {
                            InitBuildStructTree(subnode, ref sublinknode, showsubbox);
                        }
                    }
                }
                root.AppendChild(linknode);
            }
        }
        //来源
        protected void BindCorpSrc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_src'";
            //this.ddSourceWay.DataSource = bll_dic.GetList(str_where);
            //this.ddSourceWay.DataTextField = "dic_Name";
            //this.ddSourceWay.DataValueField = "ID";
            //this.ddSourceWay.DataBind();
        }
        //阶段
        protected void BindCorpProc()
        {
            TG.BLL.cm_Dictionary bll_dic = new TG.BLL.cm_Dictionary();
            string str_where = " dic_Type='cpr_jd'";
            //this.chk_cprjd.DataSource = bll_dic.GetList(str_where);
            //this.chk_cprjd.DataTextField = "dic_Name";
            //this.chk_cprjd.DataValueField = "ID";
            //this.chk_cprjd.DataBind();
        }
        // update by 20130530 qpl
        //获取选中树所有节点值并返回值
        protected string GetDropDownTreeCheckedValue(List<ASTreeViewNode> allnodes)
        {
            //最终生成字符串
            string rootvalue = "";
            foreach (ASTreeViewNode node in allnodes)
            {
                string secondvalue = "";
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                    GetSecondNodeList(node, ref secondvalue);
                }
                rootvalue += secondvalue;
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }

            return rootvalue;
        }
        //获取等级选中节点 qpl 20140108
        protected string GetDropDownTreeCheckedValue(List<ASTreeViewNode> allnodes, bool flag)
        {
            //最终生成字符串
            string rootvalue = "";
            foreach (ASTreeViewNode node in allnodes)
            {
                //获取选中的第二级别节点
                if ((node.CheckedState == ASTreeViewCheckboxState.Checked) || (node.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                {
                    rootvalue += "+" + node.NodeValue;
                }
            }
            if (!string.IsNullOrEmpty(rootvalue))
            {
                rootvalue = rootvalue.Remove(0, 1);
            }

            return rootvalue;
        }
        //获取第二级的节点拼接
        protected void GetSecondNodeList(ASTreeViewNode node, ref string value)
        {
            if (node.ChildNodes.Count > 0)
            {
                //返回值
                StringBuilder sbresult = new StringBuilder();

                foreach (ASTreeViewNode snode in node.ChildNodes)
                {
                    if (snode.CheckedState == ASTreeViewCheckboxState.Checked || snode.CheckedState == ASTreeViewCheckboxState.HalfChecked)
                    {
                        //拼接第二级
                        value = "^" + snode.NodeValue;
                        string subvalue = "";
                        subvalue = value;
                        GetChildNodes(snode, ref subvalue);
                        foreach (string key in sblist)
                        {
                            sbresult.Append(key);
                        }
                        //清空当前列表
                        sblist.Clear();
                    }
                }
                value = sbresult.ToString();
            }
        }
        //查询数据
        List<string> sblist = new List<string>();
        protected void GetChildNodes(ASTreeViewNode node, ref string value)
        {
            StringBuilder sb = new StringBuilder();
            if (node.ChildNodes.Count > 0)
            {
                foreach (ASTreeViewNode childnode in node.ChildNodes)
                {
                    if ((childnode.CheckedState == ASTreeViewCheckboxState.Checked) || (childnode.CheckedState == ASTreeViewCheckboxState.HalfChecked))
                    {
                        string tempvalue = value + "*" + childnode.NodeValue;

                        if (childnode.ChildNodes.Count > 0)
                        {
                            //如果还有子节点，继续遍历
                            GetChildNodes(childnode, ref tempvalue);
                        }
                        else
                        {
                            //添加末节点
                            sb.Append(tempvalue);
                        }
                    }
                }
            }
            else
            {
                //添加末节点
                sb.Append(value);
            }
            //赋值value
            sblist.Add(sb.ToString());
        }
        //是否需要权限检查
        protected override bool CheckPreviewPower
        {
            get
            {
                return true;
            }
        }
        ///获得并绑定权限
        protected void BindPreviewPower()
        {
            if (base.RolePowerParameterEntity != null)
            {
                int previewPower = base.RolePowerParameterEntity.PreviewPattern;
                int userSysNum = base.UserSysNo;
                int userUnitNum = base.UserUnitNo;
                string userShortName = base.UserShortName;
                ChooseCustomer1.UserSysNo = base.UserSysNo;
                ChooseCustomer1.PreviewPower = base.RolePowerParameterEntity.PreviewPattern;

                this.userShortName.Value = userShortName;
                this.previewPower.Value = previewPower.ToString();
                this.userSysNum.Value = userSysNum.ToString();
                this.userUnitNum.Value = userUnitNum.ToString();
            }
        }

        protected void btn_Save_Click(object sender, EventArgs e)
        {
            int i_cprid = Save();
            if (i_cprid > 0)
            {
                //弹出并返回到合同列表
                TG.Common.MessageBox.ShowAndRedirect(this, "合同添加成功！", "cpr_CorperationListBymaster.aspx");
            }
            else
            {
                Response.Write("<script language='javascript'>alert('合同添加失败！');history.back();</script>");
            }

        }

        protected void btn_SaveAudit_Click(object sender, EventArgs e)
        {
            int i_cprid = Save();
            if (i_cprid > 0)
            {
                //弹出并到审批界面
               // TG.Common.MessageBox.ShowAndRedirect(this, "合同添加成功！", "CprMiddlePage.aspx?cprid=" + i_cprid);
                Response.Redirect("CprMiddlePage.aspx?cprid=" + i_cprid);
            }             
            else
            {
                Response.Write("<script language='javascript'>alert('合同添加失败！');history.back();</script>");
            }
        }
    }
}